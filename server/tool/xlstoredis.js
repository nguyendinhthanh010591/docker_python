'use strict'
const vMap = {"A":"key","B":"value"}
const vFilename = `E:\\Temp\\abc.xlsx`

const xlsx = require("xlsx")
const objectMapper = require("object-mapper")
const config = require("../api/config")
const redis = require("../api/redis")
const util = require('../api/util')
const vXlsOpt = { type: "file", cellText: true, cellDates: true, dateNF: "dd/mm/yyyy" }
const wb = xlsx.read(vFilename, vXlsOpt)
const sheetName = wb.SheetNames[0], ws = wb.Sheets[sheetName]
const vopts = { blankrows: false, defval: null }
const arr = xlsx.utils.sheet_to_json(ws, vopts)
if (util.isEmpty(arr)) throw new Error("Không đọc được số liệu \n (Data cannot be read)")
let rows = []
for (const r of arr) {
    rows.push(r)
} 
for (const row of rows) {
    redis.set(row.key, row.value)
}    
