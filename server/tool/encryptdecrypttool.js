const encrypt = require("../api/encrypt")
//console.log(process.argv)
let arr = process.argv.slice(2)
if (!(arr && arr.length == 2)) process.exit(0)
let type = String(arr[0]).toLowerCase(), vinput = arr[1], voutput
switch (type) {
    case "e":
        voutput = encrypt.encrypt(vinput)
        console.log('Encrypt Output :', voutput)
        break
    case "d":
        voutput = encrypt.decrypt(vinput)
        console.log('Decrypt Output :', voutput)
        break
    default:
        break
}
process.exit(0)