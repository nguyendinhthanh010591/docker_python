'use strict'
const util = require('../api/util')

const dbs = require('../api/orcl/dbs')
const rbi = (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            const sql = `select doc "doc" from s_inv where id=:id`
            const result = await dbs.query(sql, [id])
            const rows = result.rows
            if (rows.length == 0) reject(new Error(`Không tìm thấy Hóa đơn ${id}`))
            const doc = JSON.parse(rows[0].doc)
            resolve(doc)
        } catch (err) {
            reject(err)
        }
    })
}
const updateNow = async (id) => {
    await dbs.initialize()
    let doc = await rbi(id)
    doc.status = 3
    delete doc["cde"]
    await dbs.query(`update s_inv set doc=:doc,cid=null,cde=null where id=:id`, [JSON.stringify(doc), id])
    return id
}
const argv = process.argv
if (argv.length == 3) {
    const arr = argv.slice(2), id = arr[0]
    updateNow(id).then(e=>{console.log("DONE! id: "+e)})
}