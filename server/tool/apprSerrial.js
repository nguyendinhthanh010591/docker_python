let redis = require("../api/redis")
const inc = require("../api/inc")
const config = require("../api/config")
const dbCache = config.dbCache
const moment = require("moment")
const Redis = require('ioredis')
const decrypt = require("../api/encrypt")
const getSerial = {
    getSerial_mssql: `select id "id", taxc "taxc",form "form",serial "serial",min "min",max "max",priority "priority",fd "fd" from s_serial where LEN(form) = 1 and (serial like 'K23%' or serial like 'C23%') and status = 3`,
    getSerial_mysql: `select id "id", taxc "taxc",form "form",serial "serial",min "min",max "max",priority "priority",fd "fd" from s_serial where length(form) = 1 and (serial like 'K23%' or serial like 'C23%') and status = 3`,
    getSerial_orcl: `select id "id", taxc "taxc",form "form",serial "serial",min "min",max "max",priority "priority",fd "fd" from s_serial where length(form) = 1 and (serial like 'K23%' or serial like 'C23%') and status = 3`,
    getSerial_pgsql: `select id "id", taxc "taxc",form "form",serial "serial",min "min",max "max",priority "priority",fd "fd" from s_serial where length(form) = 1 and (serial like 'K23%' or serial like 'C23%') and status = 3`
}
const ApproveSerial = async () => {
    // Duyệt dải
    try {
        if (!dbCache) {
            let redisConfig = JSON.parse(JSON.stringify(config.redisConfig))
            if (redisConfig.password) redisConfig.password = decrypt.decrypt(redisConfig.password)
            redis = new Redis(redisConfig)
        }
        let resultAppr = await inc.execsqlselect(`${getSerial[`getSerial_${config.dbtype}`]}`, [])
        for(let row of resultAppr) {
            let uk = `${row.taxc}.${row.form}.${row.serial}`, key = `SERIAL.${uk}`, kax = `${key}.max`,kreq=`${key}.req`
            let min = row.min, max = row.max, val = min-1,condition = `${row.min}.${row.max}.${moment(row.fd).format('YYYYMMDD')}.${row.priority}__`
    
            sql = `select * from s_serial where taxc=? and form=? and serial=? and status=1`
            let resultss = await inc.execsqlselect(sql, [row.taxc,row.form,row.serial])

            for (const rowc of resultss) {
                condition += `${rowc.min}.${rowc.max}.${moment(rowc.fd).format('YYYYMMDD')}.${rowc.priority}__`
            }
            const valc = await redis.get(key)
            //Sửa lại hàm set multi thành nhiều hàm set do DB không có hàm này (áp dụng cho SCB)
            if (dbCache) {
                if (valc != null) {
                    redis.set(kax, max)
                    redis.set(kreq, condition)
                    await inc.execsqlinsupddel(`update s_serial set status=?,td=? where id=? and status=3 and max>cur`, [1, null, row.id])
                } else {
                    redis.set(key, val)
                    redis.set(kax, max)
                    redis.set(kreq, condition)
                    await inc.execsqlinsupddel(`update s_serial set status=?,td=? where id=? and status=3 and max>cur`, [1, null, row.id])
                }
            } else {
                if (valc != null) {
                    redis.multi().set(kax, max).set(kreq, condition).exec(async (err, results) => {
                        if (err) {
                            console.trace("Approve error: "+uk)
                            console.trace(err)
                        }
                        else {
                            await inc.execsqlinsupddel(`update s_serial set status=?,td=? where id=? and status=3 and max>cur`, [1, null, row.id])
                        }
                    })
                } else {
    
                    redis.multi().set(key, val).set(kax, max).set(kreq, condition).exec(async (err, results) => {
                        if (err) {
                            console.trace("Approve error: "+uk)
                            console.trace(err)
                        }
                        else {
                            await inc.execsqlinsupddel(`update s_serial set status=?,td=? where id=? and status=3 and max>cur`, [1, null, row.id])
                        }
                    })
    
                }
            }
        }
        console.log("Duyệt dải 23 thành công")
    } catch (err) {
        console.log("Duyệt dải 23 lỗi")
        console.log(err)
    }
}

ApproveSerial()