const rsmq = require("../api/rsmq")
const config = require("../api/config")
const test = async () => {
    try {
        const now = new Date()
        const content = { from: config.mail, to: "trungpq10@fpt.com.vn", subject: "Kiểm tra mail", html: now }
        const msgid = await rsmq.sendMessageAsync({ qname: config.qname, message: JSON.stringify(content) })
        console.log(msgid)
    } catch (e) {
        console.error(e)
    }
    finally {
        process.exit(0)
    }
}
test()






