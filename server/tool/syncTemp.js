const Redis = require('ioredis')
const decrypt = require("../api/encrypt")
const inc = require("../api/inc")
const config = require("../api/config")
let redis = require("../api/redis")
const getSerial = {
    getSerial_mssql: `select taxc "taxc",type "type",form "form",serial "serial",min "min",max "max",cur "cur",priority "priority",fd "fd" from s_serial where LEN(form) = 1 and (serial like 'K22%' or serial like 'C22%') and status=1`,
    getSerial_mysql: `select taxc "taxc",type "type",form "form",serial "serial",min "min",max "max",cur "cur",priority "priority",fd "fd" from s_serial where length(form) = 1 and (serial like 'K22%' or serial like 'C22%') and status=1`,
    getSerial_orcl: `select taxc "taxc",type "type",form "form",serial "serial",min "min",max "max",cur "cur",priority "priority",fd "fd" from s_serial where length(form) = 1 and (serial like 'K22%' or serial like 'C22%') and status=1`,
    getSerial_pgsql: `select taxc "taxc",type "type",form "form",serial "serial",min "min",max "max",cur "cur",priority "priority",fd "fd" from s_serial where length(form) = 1 and (serial like 'K22%' or serial like 'C22%') and status=1`
}
const syncTemp = async () => {
    try {
        let result = await inc.execsqlselect(`${getSerial[`getSerial_${config.dbtype}`]}`, [])
        if (!config.dbCache) {
            let redisConfig = JSON.parse(JSON.stringify(config.redisConfig))
            if (redisConfig.password) redisConfig.password = decrypt.decrypt(redisConfig.password)
            redis = new Redis(redisConfig)
        }
        for (let row of result) {
            if (!config.dbCache) {
                let keys = await redis.keys(`TMP.${row.type}.${row.form}.${row.serial}.*`)
                for (let key of keys) {
                    let fn = key.split(".")
                    let tmp = await redis.get(key)
                    if (fn[3] && fn[3].includes('22') && tmp) {
                        // console.log(fn[3])
                        fn[3] = fn[3].replace('22', '23')
                        // console.log(fn[3])
                        // console.log(`${fn[0]}.${fn[1]}.${fn[2]}.${fn[3]}.${fn[4]}`)
                        await redis.set(`${fn[0]}.${fn[1]}.${fn[2]}.${fn[3]}.${fn[4]}`, tmp)
                    }
                }
            }
            else {
                let temps = await inc.execsqlselect(`select keyname "keyname", keyvalue "keyvalue" from s_listvalues where keyname like 'TMP.${row.type}.${row.form}.${row.serial}.%'`, [])
                //console.log(temps)
                for (let temp of temps) {
                    let keyname = temp.keyname
                    let fn = keyname.split(".")
                    let keyvalue = temp.keyvalue
                    if (fn[3] && fn[3].includes('22') && keyvalue) {
                        //console.log(fn[3])
                        fn[3] = fn[3].replace('22', '23')
                        // console.log(fn[3])
                        // console.log(`${fn[0]}.${fn[1]}.${fn[2]}.${fn[3]}.${fn[4]}`)
                        await inc.execsqlinsupddel(`update s_listvalues set keyvalue = ? where keyname = ?`, [keyvalue,`${fn[0]}.${fn[1]}.${fn[2]}.${fn[3]}.${fn[4]}`])
                    }
                }
            }
        }
        console.log("Chuyển mẫu dải 22 sang 23 thành công")
    } catch (err) {
		console.log("Chuyển mẫu dải 22 sang 23 lỗi")
        console.log(err)
    }
}
syncTemp()