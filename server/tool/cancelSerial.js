let redis = require("../api/redis")
const Redis = require('ioredis')
const decrypt = require("../api/encrypt")
const inc = require("../api/inc")
const util = require("../api/util")
const config = require("../api/config")
const dbCache = config.dbCache
const moment = require("moment")
const getSerial = {
    getSerial_mssql: `select id "id", taxc "taxc",form "form",serial "serial",min "min",max "max",cur "cur" from s_serial where LEN(form) = 1 and (serial like 'K22%' or serial like 'C22%') and status=1`,
    getSerial_mysql: `select id "id", taxc "taxc",form "form",serial "serial",min "min",max "max",cur "cur" from s_serial where length(form) = 1 and (serial like 'K22%' or serial like 'C22%') and status=1`,
    getSerial_orcl: `select id "id", taxc "taxc",form "form",serial "serial",min "min",max "max",cur "cur" from s_serial where length(form) = 1 and (serial like 'K22%' or serial like 'C22%') and status=1`,
    getSerial_pgsql: `select id "id", taxc "taxc",form "form",serial "serial",min "min",max "max",cur "cur" from s_serial where length(form) = 1 and (serial like 'K22%' or serial like 'C22%') and status=1`
}
const Cancelserial = async () => {
    // Hủy dải
    try {
        if (!dbCache) {
            let redisConfig = JSON.parse(JSON.stringify(config.redisConfig))
            if (redisConfig.password) redisConfig.password = decrypt.decrypt(redisConfig.password)
            redis = new Redis(redisConfig)
        }
        let result = await inc.execsqlselect(`${getSerial[`getSerial_${config.dbtype}`]}`, [])
        for (let row of result) {
            let uk = `${row.taxc}.${row.form}.${row.serial}`, key = `SERIAL.${uk}`, kax = `${key}.max`, ker = `${key}.err`,kreq=`${key}.req`
            let cur = await redis.get(key)
            if (dbCache) {
                redis.del(key)
                redis.del(kax)
                redis.del(ker)
                redis.del(kreq)
                result = await inc.execsqlinsupddel(`update s_serial set status=?,td=? where taxc=? and form=? and serial=? and status=1 and id =?`, [2, moment(new Date('2022-12-31 23:59:59')).format('YYYY-MM-DD HH:mm:ss'), row.taxc, row.form, row.serial, row.id])
                result = await inc.execsqlinsupddel(`update s_serial set cur=? where taxc=? and form=? and serial=? and status=1 and min<=? and max>? and id =?`, [cur, row.taxc, row.form, row.serial, cur, cur, row.id])
            }
            else {
                redis.multi().del(key).del(kax).del(ker).del(kreq).exec(async (err, results) => {
                    if (err) {
                        console.trace("Cancel error: "+uk)
                        console.trace(err)
                    }
                    else {
                        result = await inc.execsqlinsupddel(`update s_serial set status=?,td=? where taxc=? and form=? and serial=? and status=1 and id =?`, [2, util.convertDate(moment(new Date('2022-12-31 23:59:59')).format('YYYY-MM-DD HH:mm:ss')), row.taxc, row.form, row.serial, row.id])
                        result = await inc.execsqlinsupddel(`update s_serial set cur=? where taxc=? and form=? and serial=? and status=1 and min<=? and max>? and id =?`, [cur, row.taxc, row.form, row.serial, cur, cur, row.id])
                    }
                })
            }
        }
        console.log("Hủy dải 22 thành công")
    } catch (err) {
		console.log("Hủy dải 22 lỗi")
        console.log(err)
    }
}

Cancelserial()