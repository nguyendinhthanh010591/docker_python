"use strict"
process.env.UV_THREADPOOL_SIZE = 32
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0"

const cookieParser = require('cookie-parser');
const cron = require('node-cron')
const path = require("path")
const http = require("http")
const https = require('https');
const fs = require('fs');
const express = require("express")
const bodyParser = require("body-parser")
const passport = require("passport")
const cors = require("cors")
const helmet = require("helmet")
const morgan = require("morgan")
const LdapStrategy = require("passport-ldapauth")
const SamlStrategy = require('passport-saml').Strategy;
const favicon = require("serve-favicon")
const he = require('he')
const config = require("./api/config")
const logger4app = require("./api/logger4app")
let worker = /*config.dbtype == "orcl" ? {} : */require("./api/worker")
const { xss } = require('express-xss-sanitizer')
const sec = require("./api/sec")
const sign = require("./api/sign")
const dbs = require(`./api/${config.dbtype}/dbs`)
const schedule = require(`./api/schedule`)
const decrypt = require("./api/encrypt")
const logging = require("./api/logging")
const passportsaml = config.passportsaml
const user_adfs = config.user_adfs

//For Azure AD login
const isUsingAzureAD = config.useAzureAD;
const azureADConfig = config.azureADConfig;
const OIDCStrategy = require("passport-azure-ad").OIDCStrategy;
const expressSession = require("express-session");

const uid = config.ldap === "AD" ? "sAMAccountName" : (config.ldap === "OPENLDAP" ? "uid" : "cn")
let adminPW = JSON.parse(JSON.stringify(config.adminPW))
adminPW = decrypt.decrypt(adminPW)
const ldap = { server: { url: config.ldapsConfig.url, bindDN: config.adminDN, bindCredentials: adminPW, searchBase: config.baseDN, searchFilter: config.searchFilter }, searchAttributes: [uid, "mail", "DisplayName"], tlsOptions: config.tlsOptions }
if (user_adfs) {
  passport.serializeUser(function (user, done) {
    done(null, user);
  });

  passport.deserializeUser(function (user, done) {
    done(null, user);
  });
  passport.use(new SamlStrategy(
    {
      callbackUrl: config.passportsaml.saml.callbackUrl, //'https://10.15.68.212/login/callback',
      //protocol: "https://",
      //path: config.passport.saml.path,
      entryPoint: config.passportsaml.saml.entryPoint,
      issuer: config.passportsaml.saml.issuer,
      //cert: process.env.SAML_CERT || null,
      //authnContext: 'http://schemas.microsoft.com/ws/2008/06/identity/authenticationmethod/windows',
      acceptedClockSkewMs: -1,
      identifierFormat: null
      //disableRequestedAuthnContext:false,
      //authnContext: 'http://schemas.microsoft.com/ws/2008/06/identity/authenticationmethod/password'
    },
    function (profile, done) {
      return done(null,
        {
          id: profile.uid,
          email: profile.email,
          displayName: profile.cn,
          firstName: profile.givenName,
          lastName: profile.sn
        });
    })
  );
} 
//For Azure AD login
else if(isUsingAzureAD) {
  passport.serializeUser(function(user, done) {
    done(null, user);
  });
  
  passport.deserializeUser(function(user, done) {
    done(null, user);
  });
  
  passport.use(new OIDCStrategy({
      identityMetadata: azureADConfig.identityMetadata,
      clientID: azureADConfig.clientID,
      responseType: azureADConfig.responseType,
      responseMode: azureADConfig.responseMode,
      redirectUrl: azureADConfig.redirectUrl,
      allowHttpForRedirectUrl: azureADConfig.allowHttpForRedirectUrl,
      clientSecret: azureADConfig.clientSecret,
      validateIssuer: azureADConfig.validateIssuer,
      isB2C: azureADConfig.isB2C,
      issuer: azureADConfig.issuer,
      passReqToCallback: azureADConfig.passReqToCallback,
      scope: azureADConfig.scope,
      loggingLevel: azureADConfig.loggingLevel,
      nonceLifetime: azureADConfig.nonceLifetime,
      nonceMaxAmount: azureADConfig.nonceMaxAmount,
      useCookieInsteadOfSession: azureADConfig.useCookieInsteadOfSession,
      cookieEncryptionKeys: azureADConfig.cookieEncryptionKeys,
      // cookieSameSite: azureADConfig.cookieSameSite,
      clockSkew: azureADConfig.clockSkew,
    },
    function(iss, sub, profile, accessToken, refreshToken, done) {
      if (!profile.oid) {
        return done(new Error("No oid found"), null);
      }

      return done(null, profile);
    }
  ));
} else {
  passport.use(new LdapStrategy(ldap))
}
let httpServer
function initialize() {
  return new Promise((resolve, reject) => {
    //Xem config có khai báo schedule không
    if (config.scheduleslist) {
      //Lấy danh sách schedule từ config
      for (const scheduletask of config.scheduleslist) {
        const schedulename = scheduletask.schedulename
        const scheduleinterval = scheduletask.scheduleinterval
        const schedulemethod = scheduletask.schedulemethod
        const schedulestatus = scheduletask.schedulestatus
        const momenttemp = require("moment")
        if (schedulestatus) {
          cron.schedule(scheduleinterval, () => {
            //logger4app.debug(`Run ${schedulename} at ${momenttemp((new Date())).format("DD/MM/YYYY HH:mm:ss")}`)
            eval(schedulemethod)
          })
        }
      }
    }
    /*
    // Chỉ chạy cho HDB
    if(config.ent == "hdb") {
        // * * * * * *   tương đương   giây phút giờ  DofMonth Month DofWeek
        cron.schedule('0 * * * * *', () => {
            schedule.delInvNotSeq()
        })
    }
    // Chạy cho các KH dùng job gửi mail, quét bảng tạm và tạo file gửi mail
    if (config.useJobSendmail) {
      // * * * * * *   tương đương   giây phút giờ  DofMonth Month DofWeek
      cron.schedule('0 * * * * *', () => {
        schedule.sendEmailApprInv()
      })
    }
    // Chạy cho các KH dùng job gửi mail, xóa bảng tạm chỉ giữ lại một số ngày gần nhất
    if (config.useJobSendmail) {
      // * * * * * *   tương đương   giây phút giờ  DofMonth Month DofWeek
      cron.schedule('0 22 * * *', () => {
        schedule.delTempDocMail()
      })
    }
    // Chạy cho Henessy gửi báo cáo hàng ngày
    if (config.ent == "hsy") {
      // * * * * * *   tương đương   giây phút giờ  DofMonth Month DofWeek
      cron.schedule('0 1 * * *', () => {
        logger4app.debug('--------------------------------Mail Daily')
        schedule.dailymailhsy()
      })
    }
    // Chạy cho Mizuho đồng bộ tỷ giá
    if (config.ent == "mzh") {
      // * * * * * *   tương đương   giây phút giờ  DofMonth Month DofWeek
      cron.schedule('* 5 * * *', () => {
        logger4app.debug('--------------------------------tygia')
        schedule.syncExchangeRate()
      })
    }
    // Chạy cho AIA, dùng đồng bộ CA theo MST
    /*
    if (config.ent == "aia") {
      // * * * * * *   tương đương   giây phút giờ  DofMonth Month DofWeek
      cron.schedule('0 * * * * *', () => {
        logger4app.debug('--------------------------------CA AIA')
        sign.cakeyvault()
      })
    }
    */
    //
    const app = express()
    const options = {
      allowedKeys: ['']
   }
   
   //app.use(xss(options));
    app.use(helmet())
   // app.use(helmet.xssFilter());
   if (config.ent != "scb") app.use(cors())
    app.use(morgan("dev"))
    app.use(bodyParser.json({ limit: '200mb' }))
    app.use(bodyParser.urlencoded({ limit: '200mb', extended: true }))
    app.use(cookieParser());
    if(isUsingAzureAD) {
      app.use(expressSession({ 
        secret: azureADConfig.sessionSecretKey, 
        // cookie: { secure: false, maxAge: new Date(Date.now() + 3600000), sameSite: 'none' },
        resave: true,
        // store: new MemoryStore(),
        saveUninitialized: true
      }));
    }
    
    app.use(passport.initialize())
    app.use(passport.session())
    app.use(express.static("public"))
    app.use(favicon(path.resolve(__dirname, "../public/favicon.ico")))
    app.all("/api/*", sec.verify)
   // app.use("/api",xss(), require("./api/router"))
    app.use("/api", require("./api/router"))
    app.use(async (err, req, res, next) => {
      logger4app.error(err.stack)
      let msg, msgcode = null
      if (err.errorNum) {
        // Oracle
        switch (err.errorNum) {
            case 1:
                msg = "Bản ghi đã tồn tại/The record already exists"
                break
            case 2292:
                msg = "Không xóa được bản ghi cha/Cannot delete parent record"
                break
            default:
                msg = err.message
        }
        msgcode = `ORA-${err.errorNum}`
      } else if (err.errno) {
        // MYSQL
        switch (err.errno) {
            case 1062:
                msg = "Bản ghi đã tồn tại/The record already exists"
                break
            case 1451:
                msg = "Không xóa được bản ghi cha/Cannot delete parent record"
                break
            default:
                msg = err.message
        }
        msgcode = `MYS-${err.errno}`
      } else if (err.number) {
        // MSSQL
        switch (err.number) {
            case 2627:
                msg = "Bản ghi đã tồn tại/The record already exists"
                break
            case 2601:
                msg = "Bản ghi đã tồn tại/The record already exists"
                break
            case 547:
                msg = "Không xóa được bản ghi cha/Cannot delete parent record"
                break
            default:
                msg = err.message
        }
        msgcode = `MSQ-${err.number}`
      }
      else {
        msg = err.message
        if (msg == "Account disabled") msg = "Tài khoản hết hiệu lực"
        if (msg.indexOf(`USR`) < 0) msgcode = err.message
      }

      if (msgcode != null){
        //msg = await logging.infoMSGExp((new Error(msgcode)).message, err.message,`vi`,null,`MSG`)
      }

      res.status(500).send(he.encode(msg.replace(/ORA-|SYS_C/gi, "INV"), {'encodeEverything': true, 'useNamedReferences': true, 'strict': true}))
    })
    if (!config.https_server_option) {
      httpServer = http.createServer(app)
      httpServer.listen(config.port, err => {
        if (err) {
          reject(err)
          return
        }
        resolve()
      })
      httpServer.setTimeout(config.sessionTimeout)
    } else {
      let https_option = (config.https_server_option) ? config.https_server_option : {}
      for (let key in https_option) {
        if (typeof https_option[key] === "string" && ["ca","key", "cert"].includes(key)) {
          https_option[key] = fs.readFileSync(https_option[key])
        }
      }
      /*
      https_option.key = fs.readFileSync(config.https_server.option.key)
      https_option.cert = fs.readFileSync(config.https_server.option.cert)
      */
      httpServer = https.createServer(https_option, app)
      httpServer.listen(config.port, err => {
        if (err) {
          reject(err)
          return
        }
        resolve()
      })
      httpServer.setTimeout(config.sessionTimeout)
    }
  })
}

function close() {
  return new Promise((resolve, reject) => {
   
    if (!config.dbCache) {
      if (worker.quit) worker.quit()
    }
    httpServer.close(err => {
      if (err) {
        reject(err)
        return
      }
      resolve()
    })
  })
}

async function startup() {
  try {
    // if (dbs.initialize) {
    //   logger4app.info("Initializing database")
    //   await dbs.initialize()
    // }
  }
  catch (err) {
    logger4app.error(err)
    process.exit(1)
  }
  // if (config.dbtype == "orcl") worker = require("./api/worker")
  try {
    logger4app.info("Initializing application")
    console.log("Initializing application")
    await initialize()
    console.log("initialize success")
  }
  catch (err) {
    logger4app.error(err)
    process.exit(1)
  }
}

async function shutdown(err) {
  try {
    logger4app.info("Closing application")
    console.log("Closing application")
    await close()
    console.log("Closing success")
  }
  catch (e) {
    logger4app.error(e)
  }
  try {
    // if (dbs.close) {
    //   logger4app.info("Closing database")
    //   await dbs.close()
    // }
  }
  catch (e) {
    logger4app.error(e)
  }
  if (err) process.exit(1)
  else process.exit(0)
}
startup()
process.on("SIGTERM", () => {
  shutdown()
}).on("SIGINT", () => {
  shutdown()
}).on("unhandledRejection", (reason, promise) => {
  logger4app.error("unhandledRejection at:", promise, "reason:", reason)
}).on("uncaughtException", err => {
  logger4app.error(err)
  shutdown(err)
})
