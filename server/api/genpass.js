"use strict"
var generator = require('generate-password')
const config = require("./config")
const logger4app = require("./logger4app")

const generate = async (doc, org) => {
    try {
        let genpassrule = {length: 10,numbers: true, symbols:true,lowercase:true,uppercase:true,strict:true}
        if (config.genpassrule) genpassrule = config.genpassrule
        return generator.generate(genpassrule)
    } catch (err) {
        logger4app.debug(err)
    }
}
const generate_kbank = async (doc, org) => {
    try {
        const password_generate = doc.c0

        return password_generate
    }
    catch (err) {
        logger4app.debug(err)
    }
}
const generate_cimb = async (doc, org) => {
    try {
        let password_generate
        if(doc.c6=="DN")
            password_generate= `${doc.bname.substr(0,1)}@${doc.btax}`
        else if(doc.c6=="CN")
            password_generate= `${doc.bname.substr(0,1)}@${doc.c7}`

        return password_generate
    }
    catch (err) {
        logger4app.debug(err)
    }
}
/*
const generate_scb = async (doc, org) => {
    try {
        const jsongenerate = { doc: doc, org: org }
        const password_generate = await hbs.j2sms(jsongenerate, config.attachment_password)

        return password_generate
    }
    catch (err) {
        logger4app.debug(err)
    }
}
*/
const Service = {
    generate : 
    async (doc, org) => {
        let returnmmsg=""
        try {
            returnmmsg = await eval(`generate_${config.ent}(doc, org)`)
        } catch (err) {
            if(config.ent == "cimb") return ""
            returnmmsg = await eval(`generate(doc, org)`)
        }
        if (!returnmmsg && config.ent != "cimb") throw new Error(`Mật khẩu không hợp lệ \n Password is invalid`)
        return returnmmsg
    }
}
module.exports = Service