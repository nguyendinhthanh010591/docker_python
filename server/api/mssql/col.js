
"use strict"
const ajv = require("ajv")
const dbs = require("./dbs")
const redis = require("../redis")
const config = require("../config")
const logger4app = require("../logger4app")
const dbCache = config.dbCache
const jv = new ajv({ allErrors: true })
const URL = "http://json-schema.org/draft-07/schema#"
const STR = { "type": "string" }
const INT = { "type": "integer" }
const BOL = { "type": "boolean" }
const ATR = { "type": "object", "properties": { "maxlength": INT, "step": INT, "min": INT, "max": INT } }
const ARR = { "type": "array", "items": { "id": STR, "value": STR }, "minitems": 1 }
const SUG = { "type": "object", "properties": { "data": ARR, "url": STR }, "oneOf": [{ "required": ["data"] }, { "required": ["url"] }] }
const SCHEMAS = {
    "text": { "$schema": URL, "type": "object", "properties": { "attributes": ATR, "type": { "type": "string", "enum": ["tel", "email", "number"] }, "value": STR, "required": BOL } },
    "datepicker": { "$schema": URL, "type": "object", "properties": { "value": STR, "required": BOL } },
    "checkbox": { "$schema": URL, "type": "object", "properties": { "checkValue": STR, "uncheckValue": STR, "value": STR, "required": BOL } },
    "combo": { "$schema": URL, "type": "object", "properties": { "suggest": SUG, "value": STR, "required": BOL }, "required": ["suggest"] },
    "multicombo": { "$schema": URL, "type": "object", "properties": { "suggest": SUG, "value": STR, "required": BOL }, "required": ["suggest"] }
}
const FORMAT = {
    "text": ["attributes", "value", "type", "required"],
    "datepicker": ["value", "required"],
    "checkbox": ["checkValue", "uncheckValue", "value", "required"],
    "combo": ["suggest", "value", "required"],
    "multicombo": ["suggest", "value", "required"]
}
const init = async (itype) => {
    let binds = []
    for (let idx = 0; idx < 10; idx++) {
        binds.push([itype, idx, `Trường ${idx}`, "text"])
    }
    await dbs.queries(`insert into s_dcm (itype,idx,lbl,typ) values (@1,@2,@3,@4)`, binds)
    let keys
    if (!dbCache) {
        keys = await redis.keys(`COL.${itype}.*`)
    } else {
        keys = await redis.keys(`COL.${itype}`)
    }
    if (keys && keys.length > 0) await redis.del(keys)
    await redis.del([`XLS.${itype}`, `CHK.${itype}`])
}
const dtlinit = async (itype) => {
    let binds = []
    for (let idx = 0; idx < 10; idx++) {
        binds.push([itype, idx, 1, `Cột ${idx}`, "text"])
    }
    await dbs.queries(`insert into s_dcm(itype,idx,dtl,lbl,typ) values (@1,@2,@3,@4,@5)`, binds)
    await redis.del([`XLS.${itype}`, `CHK.${itype}`, `DTL.${itype}`])
}
const dtlcache = (itype) => {
    return new Promise(async (resolve, reject) => {
        try {
            const key = `DTL.${itype}`
            let rows = await redis.get(key)
            if (rows) resolve(JSON.parse(rows))
            const sql = `select idx,lbl,typ,atr from s_dcm where itype=@1 and dtl=1 and status=1`
            const result = await dbs.query(sql, [itype])
            rows = result.recordset
            let arr = []
            for (const row of rows) {
                const id = `c${row.idx}`, typ = row.typ, atr = row.atr
                let obj = { id: id, header: { text: row.lbl, css: "header" }, adjust: true }
                if (typ == "number") {
                    obj.editor = "text"
                    obj.css = "right"
                    obj.inputAlign = "right"
                }
                if (typ == "datepicker") {
                    obj.editor = "date"
                    obj.stringResult = true
                    obj.editable = false
                }
                else {
                    obj.editor = typ
                }
                if (atr) {
                    const js = JSON.parse(atr)
                    Object.keys(js).forEach(e => obj[e] = js[e])
                }
                arr.push(obj)
            }
            await redis.set(key, JSON.stringify(arr))
            resolve(arr)
        }
        catch (err) {
            reject(err)
        }
    })
}
const dtlcachelang = (itype,lang) => {
    return new Promise(async (resolve, reject) => {
        try {
            const key_vi = `DTL.${itype}.vi`
            const key_en = `DTL.${itype}.en`
            let rows

            if (lang =="en") {
                rows  = await redis.get(key_en)
                if (rows) resolve(JSON.parse(rows))
            }else{
                rows  = await redis.get(key_vi)
                if (rows) resolve(JSON.parse(rows))
            }
            let sql = ``

            if (lang =="en")
            sql = `select idx,lbl_en "lbl",typ,atr from s_dcm where itype=@1 and dtl=1 and status=1`
            else
            sql = `select idx,lbl,typ,atr from s_dcm where itype=@1 and dtl=1 and status=1`
            const result = await dbs.query(sql, [itype])
            rows = result.recordset
            let arr = []
            for (const row of rows) {
                const id = `c${row.idx}`, typ = row.typ, atr = row.atr
                let obj = { id: id, header: { text: row.lbl, css: "header" }, adjust: true }
                if (typ == "number") {
                    obj.editor = "text"
                    obj.css = "right"
                    obj.inputAlign = "right"
                }
                if (typ == "datepicker") {
                    obj.editor = "date"
                    obj.stringResult = true
                    obj.editable = false
                }
                else {
                    obj.editor = typ
                }
                if (atr) {
                    const js = JSON.parse(atr)
                    Object.keys(js).forEach(e => obj[e] = js[e])
                }
                arr.push(obj)
            }
            if (lang =="en") {
                await redis.set(key_en, JSON.stringify(arr))
            }else{
                await redis.set(key_vi, JSON.stringify(arr))
            }
            resolve(arr)
        }
        catch (err) {
            reject(err)
        }
    })
}
const Service = {
    cache: (itype) => {
        return new Promise(async (resolve, reject) => {
            try {
                const key = `COL.${itype}`
                let rows = await redis.get(key)
                if (rows) resolve(JSON.parse(rows))
                const sql = `select idx,lbl,typ,atr from s_dcm where itype=@1 and dtl=2 and status=1 order by idx`
                const result = await dbs.query(sql, [itype])
                rows = result.recordset
                let arr = []
                for (const row of rows) {
                    const id = `c${row.idx}`, typ = row.typ, atr = row.atr
                    let obj = { id: id, name: id, label: row.lbl, labelWidth: 85 }
                    if (typ == "number") {
                        obj.view = "text"
                        obj.type = typ
                    }
                    else if (typ == "datepicker") {
                        obj.view = typ
                        obj.stringResult = true
                        obj.editable = false
                    }
                    else {
                        obj.view = typ
                    }
                    if (atr) {
                        const js = typeof atr == "object" ? atr : JSON.parse(atr)
                        Object.keys(js).forEach(e => obj[e] = js[e])
                    }
                    arr.push(obj)
                }
                await redis.set(key, JSON.stringify(arr))
                resolve(arr)
            }
            catch (err) {
                reject(err)
            }
        })
    },
    //Ham dung cho ket xuat XML
    cachedtls: (itype, lang) => {
        return new Promise(async (resolve, reject) => {
            try {
                let arr = dtlcachelang(itype,lang)
                resolve(arr)
            }
            catch (err) {
                reject(err)
            }
        })
    },
    cacheLang: (itype,lang) => {
        return new Promise(async (resolve, reject) => {
            try {
                const key_vi = `COL.${itype}.vi`
                const key_en = `COL.${itype}.en`
                let rows

                if (lang =="en") {
                    rows  = await redis.get(key_en)
                    if (rows) resolve(JSON.parse(rows))
                }else{
                    rows  = await redis.get(key_vi)
                    if (rows) resolve(JSON.parse(rows))
                }
               
              
                let sql=''
                if (lang =="en")
                    sql = `select idx,lbl_en "lbl",typ,atr from s_dcm where itype=@1 and dtl=2 and status=1 order by idx`
                else
                 sql = `select idx,lbl,typ,atr from s_dcm where itype=@1 and dtl=2 and status=1 order by idx`
                const result = await dbs.query(sql, [itype])
                rows = result.recordset
                let arr = []
                for (const row of rows) {
                    const id = `c${row.idx}`, typ = row.typ, atr = row.atr
                    let obj = { id: id, name: id, label: row.lbl, labelWidth: 85 }
                    if (typ == "number") {
                        obj.view = "text"
                        obj.type = typ
                    }
                    else if (typ == "datepicker") {
                        obj.view = typ
                        obj.stringResult = true
                        obj.editable = false
                    }
                    else {
                        obj.view = typ
                    }
                    if (atr) {
                        const js = typeof atr == "object" ? atr : JSON.parse(atr)
                        Object.keys(js).forEach(e => obj[e] = js[e])
                    }
                    arr.push(obj)
                }
                if (lang =="en") {
                    await redis.set(key_en, JSON.stringify(arr))
                }else{
                    await redis.set(key_vi, JSON.stringify(arr))
                }
                resolve(arr)
            }
            catch (err) {
                reject(err)
            }
        })
    },
    dts: (req, res, next) => {
        try {
            const itype = req.params.itype
            dtlcache(itype).then(result => res.json(result)).catch(err => next(err))
        }
        catch (err) {
            next(err)
        }
    },
    dtsLang: (req, res, next) => {
        try {
            const itype = req.params.itype,lang = req.params.lang
            dtlcachelang(itype,lang).then(result => res.json(result)).catch(err => next(err))
        }
        catch (err) {
            next(err)
        }
    },
    cus: (req, res, next) => {
        try {
            const itype = req.params.itype
            Service.cache(itype).then(result => res.json(result)).catch(err => next(err))
        }
        catch (err) {
            next(err)
        }
    },
    cusLang: (req, res, next) => {
        try {
            const itype = req.params.itype,lang = req.params.lang
            Service.cacheLang(itype,lang).then(result => res.json(result)).catch(err => next(err))
        }
        catch (err) {
            next(err)
        }
    },
    get: async (req, res, next) => {
        try {
            const itype = req.params.itype
            let sql, result, rows
            sql = `select id,itype,idx,lbl,lbl_en,typ,status,xc,atr from s_dcm where itype=@1 and dtl=2 order by idx`
            result = await dbs.query(sql, [itype])
            rows = result.recordset
            for (let i of rows) {
                if (i.atr && typeof i.atr == "object") i.atr = JSON.stringify(i.atr)
            }
            if (rows.length == 0) {
                await init(itype)
                result = await dbs.query(sql, [itype])
                rows = result.recordset
            }
            res.json(rows)
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const body = req.body, itype = body.itype, typ = body.typ
            let atr = body.atr
            if (atr) {
                atr = JSON.parse(atr)
                const type = typ == "number" ? "text" : typ, format = FORMAT[type]
                for (let a in atr) {
                    if (!format.includes(a)) delete atr[a]
                }
                const schema = SCHEMAS[type], ret = jv.validate(schema, atr)
                if (!ret) throw new Error(`Sai định dạng ${jv.errorsText()} \n (Incorrect format ${jv.errorsText()})`)
                atr = JSON.stringify(atr)
            } else {
                atr = null
            }
            await dbs.query("update s_dcm set lbl=@1,typ=@2,status=@3,xc=@4,atr=@5,lbl_en=@6 where id=@7", [body.lbl, typ, body.status, body.xc, atr,body.lbl_en, body.id])
            await redis.del([`XLS.${itype}`, `CHK.${itype}`])
            let keys
            if (!dbCache) {
                keys = await redis.keys(`COL.${itype}.*`)
                if (keys && keys.length > 0) await redis.del(keys)
            } else {
                keys = await redis.keys(`COL.${itype}.`)
                if (keys && keys.length > 0) {
                    for (let key of keys) {
                        await redis.del(key)
                    }
                }
            }
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },
    getdtl: async (req, res, next) => {
        try {
            const itype = req.params.itype
            let sql, result, rows
            sql = `select id,itype,idx,lbl,lbl_en,typ,status,xc,atr from s_dcm where itype=@1 and dtl=1 order by idx`
            result = await dbs.query(sql, [itype])
            rows = result.recordset
            if (rows.length == 0) {
                await dtlinit(itype)
                result = await dbs.query(sql, [itype])
                rows = result.recordset
            }
            res.json(rows)
        }
        catch (err) {
            next(err)
        }
    },
    postdtl: async (req, res, next) => {
        try {
            const body = req.body, itype = body.itype, typ = body.typ
            let atr = body.atr ? body.atr : null
            if (atr) {
                atr = JSON.parse(atr)
                const type = typ == "number" ? "text" : typ, format = FORMAT[type]
                for (let a in atr) {
                    if (!format.includes(a)) delete atr[a]
                }
                const schema = SCHEMAS[type], ret = jv.validate(schema, atr)
                if (!ret) throw new Error(`Sai định dạng ${jv.errorsText()} \n (Incorrect format ${jv.errorsText()})`)
                atr = JSON.stringify(atr)
            }
            await dbs.query("update s_dcm set lbl=@1,typ=@2,status=@3,xc=@4,atr=@5,lbl_en=@6 where id=@7", [body.lbl, typ, body.status, body.xc, atr,body.lbl_en, body.id])
            await redis.del([`XLS.${itype}`, `DTL.${itype}`, `CHK.${itype}`])
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },
    selectDC: async (req, res, next) => {
        try {
            const type = req.query.type
            const result = await dbs.query(`select id, name value from s_dcd where type=@1`, [type])
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    dcd: async (req, res, next) => {
        try {
            const query = req.query, type = query.type
            const sql = `select id,name value from s_dcd where type=@1`
            const result = await dbs.query(sql, [type])
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    }
}
module.exports = Service