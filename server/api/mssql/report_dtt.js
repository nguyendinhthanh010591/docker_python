"use strict"
const path = require("path")
const fs = require("fs")
const moment = require("moment")
const xlsxtemp = require("xlsx-template")
const dbs = require("./dbs")
const config = require("../config")
const logger4app = require("../logger4app")
const sec = require("../sec")
const util = require("../util")
const grant = config.ou_grant
const tenhd = (type) => {
    let result = config.ITYPE.find(item => item.id === type)
    return result.value
}
const Service = {
    get: async (req, res, next) => {
        try {
            const token = sec.decode(req), query = req.query, taxc = token.taxc
            const tn = query.tn, type = query.type, report = query.report,ou=(typeof  query.ou=="undefined"?"*":query.ou)
            const type_re = query.type_date
          
            const fm = moment(query.fd), tm = moment(query.td)
            const fd = fm.startOf("day").format("YYYY-MM-DD HH:mm:ss") ,td = tm.endOf("day").format("YYYY-MM-DD HH:mm:ss")

            const fr = fm.format(config.mfd), to = tm.format(config.mfd), rt = moment().format(config.mfd)
            const now = new Date(), ndate = now.getDate(), nmonth = now.getMonth() + 1, nyear = now.getFullYear()
            let sql, result, where, binds, json, fn, rows,i,where_ft="",val
            if(query.serial!="" && (typeof  query.serial!="undefined")){//dieu kiện an hien tuy du an
                  
                val = query.serial
                let str = "", arr = val.split(",")
                for (const row of arr) {
                    str += `'${row}',`
                }
                where_ft += ` and serial in (${str.slice(0, -1)})`
             } 
             if( query.ou!="" && (typeof  query.ou!="undefined")){//dieu kiện an hien tuy du an
                  
                    val = query.ou
                    let str = "", arr = val.split(",__")
                    for (const row of arr) {
                        let d=row.split("__")
                        str += `${d[0]},`
                    }
                    where_ft += ` and ou in (${str.slice(0, -1)})`
                        
               
            }
            if (report == 1) {
                fn = "temp/08BKHDVAT.xlsx"
                where = `where a.idt between @1 and @2 and a.stax=@3 and a.status=3 and a.type='01GTKT' and a.cid is null`
                binds = [fd, td, taxc]
                i=4
                      
               
                where += where_ft
                //Lấy các hóa đơn đã duyệt, thêm trường factor đánh dấu hệ số dương
                sql = `select FORMAT(idt,'dd/MM/yyyy') idt,form,serial,seq,btax,bname,adjtyp,JSON_QUERY(doc,'$.root') root,case when adjtyp=2 then JSON_QUERY(doc,'$.tax') else JSON_QUERY(doc,'$.tax') end taxs,note,1 as factor from s_inv a ${where}`
              //  binds = [fd, td, taxc]
                if (config.BKHDVAT_EXRA_OPTION == '2')
                {
                    //Làm riêng cho Deloitte
                    
                    //Lấy hóa đơn hủy thay thế => status = 4, ngày lấy theo dt, factor đánh dấu hệ số âm
                    
                    where =`where cdt between @1 and @2 and stax=@3  and status=4 and type='01GTKT' and xml is not null`
                   
                    where += where_ft
                    sql+= ` union all 
                    select FORMAT(cdt,'dd/MM/yyyy') idt,form,serial,seq,btax,bname,adjtyp,JSON_QUERY(doc,'$.root') root,case when adjtyp=2 then JSON_QUERY(doc,'$.tax') else JSON_QUERY(doc,'$.tax') end taxs,CASE WHEN JSON_VALUE ([doc], '$.cancel.rea') IS NOT NULL THEN JSON_VALUE ([doc], '$.cancel.rea') ELSE cde END note,-1 as factor from s_inv ${where}`
                 

                    //Hóa đơn bị hủy, thay thế => status = 4, ngày lấy theo idt, factor đánh dấu hệ số dương
                    where =`where idt between @1 and @2  and stax=@3 and status=4 and type='01GTKT' and xml is not null`
                    
                    where += where_ft
                    sql+= ` union all 
                    select FORMAT(idt,'dd/MM/yyyy') idt,form,serial,seq,btax,bname,adjtyp,JSON_QUERY(doc,'$.root') root,case when adjtyp=2 then JSON_QUERY(doc,'$.tax') else JSON_QUERY(doc,'$.tax') end taxs,note,1 as factor from s_inv ${where}`
                    //binds.push(fd), binds.push(td),binds.push(taxc)
                }
                sql+= ' order by idt,form,serial,seq'
                result = await dbs.query(sql, binds)
                rows = result.recordset
                let a0 = [], a5 = [], a10 = [], a1 = [], a2 = []
                let i0 = 0, i5 = 0, i10 = 0, i1 = 0, i2 = 0
                let s0 = 0, s5 = 0, s10 = 0, s1 = 0, s2 = 0, v5 = 0, v10 = 0
                for (const row of rows) {
                    let idt, form, serial, seq, btax, bname, taxs =  JSON.parse(row.taxs)
                    if (row.adjtyp == 2) {
                        let root =  JSON.parse(row.root)
                        idt = moment(root.idt).format(config.mfd)
                        form = root.form
                        serial = root.serial
                        seq = root.seq
                        btax = root.btax
                        bname = root.bname
                    }
                    else {
                        idt = row.idt
                        form = row.form
                        serial = row.serial
                        seq = row.seq
                        btax = row.btax
                        bname = row.bname
                    }
                    for (const tax of taxs) {
                        let vrt = Number(tax.vrt), s = tax.amtv * row.factor, v = tax.vatv * row.factor, r = { idt: idt, form: form, serial: serial, seq: seq, btax: btax, bname, sumv: s, vatv: v, note: row.note }
                        switch (vrt) {
                            case 0:
                                s0 += s
                                r.index = ++i0
                                a0.push(r)
                                break
                            case 5:
                                s5 += s
                                v5 += v
                                r.index = ++i5
                                a5.push(r)
                                break
                            case 10:
                                s10 += s
                                v10 += v
                                r.index = ++i10
                                a10.push(r)
                                break
                            case -1:
                                s1 += s
                                r.index = ++i1
                                a1.push(r)
                                break
                            case -2:
                                s2 += s
                                r.index = ++i2
                                a2.push(r)
                                break
                            default:
                                break
                        }
                    }
                }
                json = { stax: taxc, sname: token.on, fd: fr, td: to, rt: rt, a0: a0, s0: s0, a5: a5, s5: s5, v5: v5, a10: a10, s10: s10, v10: v10, a1: a1, s1: s1, a2: a2, s2: s2, s: (s0 + s5 + s10 + s1 + s2), v: (v5 + v10) }
            }
            else if (report == 2) {
                fn = "temp/08BKHDXT.xlsx"
                where = "where idt between @1 and @2 and stax=@3"
                binds = [fd, td, taxc]
               let ij=4
                if (type !== "*") {
                    where += ` and type=@${ij++}`
                    binds.push(type)
                }
                
                where += where_ft
               
                sql = `select FORMAT(idt,'dd/MM/yyyy') idt,form,serial,seq,btax,bname,buyer,CAST((CASE WHEN status=1 THEN N'Chờ cấp số' WHEN status=2 THEN N'Chờ duyệt' WHEN status=3 THEN N'Đã duyệt' WHEN status=4 THEN N'Đã hủy' WHEN status=6 THEN N'Chờ hủy' ELSE CAST(status as nvarchar(22)) END) as nvarchar(22)) status,status cstatus,sumv,vatv,CONCAT(note,'-',cde,'-',adjdes) note from s_inv ${where} order by idt,form,serial,seq`
                result = await dbs.query(sql, binds)
                rows = result.recordset
                let i = 0
                for (const row of rows) {
                    row.index = ++i
                    if (row.cstatus == 4) {
                        delete row["sumv"]
                        delete row["vatv"]
                    }
                }
                json = { stax: taxc, sname: token.on, fd: fr, td: to, rt: rt, tn: tn, table: rows }
            }
            else if (report == 3 ) {
                fn = "temp/05BCHD.xlsx"
                let where = "", binds = [taxc, fd, td], type_name,where_iv="",where_seri=""
                let  ij=4
                if (type !== "*") {
                    where = ` and type=@${ij++}`
                    binds.push(type)
                    type_name = tenhd(type)
                }
                //{trung sua them chi nhanh,ky hieu in bao cao
                if(query.serial!="" && (typeof  query.serial!="undefined")){//dieu kiện an hien tuy du an
                  
                    val = query.serial
                    let str = "", arr = val.split(",")
                    for (const row of arr) {
                        str += `'${row}',`
                    }
                    where_seri += ` and serial in (${str.slice(0, -1)})`
                 } 
                where_iv=where
                where_seri=where+where_seri
                where_iv += where_ft         
               
                if(type_re!=2){//mac dinh theo ngay lap hoa don:cdt
                    sql = `
                    with inv as (select form,serial,CAST(seq as int) seq from s_inv where idt between @2 and @3  and  stax=@1 and status=4 ${where_iv}) 
                    select a.form form,a.serial serial,sum(a.min1) min1,sum(a.max1) max1,sum(a.min0) min0,sum(a.max0) max0,sum(a.maxu0) maxu0,sum(a.minu) minu,sum(a.maxu) maxu,sum(a.cancel) cancel,max(a.clist) clist from (
                    select form,serial,0 min1,0 max1,min min0,max max0,0 maxu0,0 minu,0 maxu,0 cancel,null clist from s_serial where taxc=@1 and  fd<@2 and (td is null or td>@3) and status !=3 ${where_seri}
                    union
                    select form,serial,min min1,max max1,0 min0,0 max0,0 maxu0,0 minu,0 maxu,0 cancel,null clist from s_serial where taxc=@1 and  fd between @2 and @3 and status !=3 ${where_seri}
                    union
                    select form,serial,0 min1,0 max1,0 min0,0 max0,max(CAST(seq as int)) maxu0,0 minu,0 maxu,0 cancel,null clist from s_inv where idt<@2  and stax=@1 and  status>2 ${where_iv} group by form,serial
                    union
                    select form,serial,0 min1,0 max1,0 min0,0 max0,0 maxu0,min(CAST(seq as int)) minu,max(CAST(seq as int)) maxu,sum(CASE WHEN status=4 THEN 1 ELSE 0 END) cancel,null clist from s_inv where idt between @2 and @3  and stax=@1 and  status>2 ${where_iv} group by form,serial
                    union 
                    select distinct b.form,b.serial,0 min1,0 max1,0 min0,0 max0,0 maxu0,0 minu,0 maxu,0 cancel,ISNULL(STUFF((select concat(',',seq) as [text()] from inv where form=b.form and serial=b.serial FOR XML PATH('')), 1, 1, NULL),'') clist from s_inv b where idt between @2 and @3  and stax=@1 and  status=4 ${where_iv} group by form,serial
                    ) a group by a.form,a.serial`
                }else{// truong hop bao cao theo ngay tạo create date:dt
                    sql = `
                    with inv as (select form,serial,CAST(seq as int) seq from s_inv where idt between @2 and @3  and  stax=@1 and status=4 ${where_iv}) 
                    select a.form form,a.serial serial,sum(a.min1) min1,sum(a.max1) max1,sum(a.min0) min0,sum(a.max0) max0,sum(a.maxu0) maxu0,sum(a.minu) minu,sum(a.maxu) maxu,sum(a.cancel) cancel,max(a.clist) clist from (
                    select form,serial,0 min1,0 max1,min min0,max max0,0 maxu0,0 minu,0 maxu,0 cancel,null clist from s_serial where taxc=@1 and  fd<@2 and (td is null or td>@3) and status !=3 ${where_seri}
                    union
                    select form,serial,min min1,max max1,0 min0,0 max0,0 maxu0,0 minu,0 maxu,0 cancel,null clist from s_serial where taxc=@1 and  fd between @2 and @3 and status !=3 ${where_seri}
                    union
                    select form,serial,0 min1,0 max1,0 min0,0 max0,max(CAST(seq as int)) maxu0,0 minu,0 maxu,0 cancel,null clist from s_inv where dt<@2  and stax=@1 and  status>2 ${where_iv} group by form,serial
                    union
                    select form,serial,0 min1,0 max1,0 min0,0 max0,0 maxu0,min(CAST(seq as int)) minu,max(CAST(seq as int)) maxu,sum(CASE WHEN status=4 THEN 1 ELSE 0 END) cancel,null clist from s_inv where dt between @2 and @3  and stax=@1 and  status>2 ${where_iv} group by form,serial
                    union 
                    select distinct b.form,b.serial,0 min1,0 max1,0 min0,0 max0,0 maxu0,0 minu,0 maxu,0 cancel,ISNULL(STUFF((select concat(',',seq) as [text()] from inv where form=b.form and serial=b.serial FOR XML PATH('')), 1, 1, NULL),'') clist from s_inv b where dt between @2 and @3  and stax=@1 and  status=4 ${where_iv} group by form,serial
                    ) a group by a.form,a.serial`
                }
                
                result = await dbs.query(sql, binds)
                rows = result.recordset
                let i = 0
                for (const row of rows) {
                    row.index = ++i
                    row.type_name = type == "*" ? tenhd(row.form.substr(0, 6)) : type_name

                    const clist = (row.clist != null) ? row.clist.split(",") : null
                    if (util.isEmpty(clist)) {
                        delete row.cancel
                    }
                    else {
                        let arr = clist.sort((a, b) => a - b), len = arr.length, str
                        if (len === 1) {
                            str = arr[0].toString().padStart(7, "0")
                        }
                        else if (len === 2) {
                            const cs = arr[0] + 1 === arr[1] ? "-" : ";"
                            for (let i = 0; i < len; i++) {
                                arr[i] = arr[i].toString().padStart(7, "0")
                            }
                            str = arr.join(cs)
                        }
                        else {
                            let del = []
                            for (let i = 1; i < len; i++) {
                                if (arr[i - 1] + 2 === arr[i + 1]) del.push(i)
                            }
                            for (let i = 0; i < len; i++) {
                                if (del.includes(i)) arr[i] = null
                                else arr[i] = arr[i].toString().padStart(7, "0")
                            }
                            str = arr.join(";").replace(/\;\;+/g, '-')
                        }
                        row.clist = str
                    }
                    const max0 = parseInt(row.max0), min0 = parseInt(row.min0), max1 = parseInt(row.max1), min1 = parseInt(row.min1), maxu0 = parseInt(row.maxu0), minu = parseInt(row.minu), maxu = parseInt(row.maxu), cancel = parseInt(row.cancel)
                    //use
                    let su = 0, s0 = 0
                    if (maxu > 0) {
                        su = maxu - minu + 1
                        row.su = su
                        row.fu = minu.toString().padStart(7, '0')
                        row.tu = maxu.toString().padStart(7, '0')
                        row.used = cancel > 0 ? (su - cancel) : su
                    }
                    //tondau
                    if (max1 > 0) {
                        s0 = max1 - min1 + 1
                        row.s0 = s0
                        row.f1 = min1.toString().padStart(7, '0')
                        row.t1 = max1.toString().padStart(7, '0')
                    }
                    else if (max0 > 0) {
                        if (maxu0 > 0) {
                            if (max0 > maxu0) {
                                s0 = max0 - maxu0
                                row.s0 = s0
                                row.f0 = (maxu0 + 1).toString().padStart(7, '0')
                                row.t0 = max0.toString().padStart(7, '0')
                            }
                        }
                        else {
                            s0 = max0 - min0 + 1
                            row.s0 = s0
                            row.f0 = min0.toString().padStart(7, '0')
                            row.t0 = max0.toString().padStart(7, '0')
                        }
                    }
                    //toncuoi
                    if (maxu > 0) {
                        if (s0 > su) {
                            row.s2 = s0 - su
                            row.f2 = (maxu + 1).toString().padStart(7, '0')
                            if (max1 > 0) row.t2 = row.t1
                            else if (max0 > 0) row.t2 = row.t0
                        }
                    }
                    else {
                        if (max1 > 0) {
                            row.s2 = s0
                            row.f2 = row.f1
                            row.t2 = row.t1
                        }
                        else if (max0 > 0) {
                            row.s2 = s0
                            row.f2 = row.f0
                            row.t2 = row.t0
                        }
                    }

                }
                json = { stax: taxc, now, ndate, nmonth, nyear, sname: token.on, period: `Kỳ báo cáo ${query.period}`, fd: fr, td: to, rt: rt, tn: tn, table: rows }
                
            }
            const file = path.join(__dirname, "..", "..", "..", fn)
            const xlsx = fs.readFileSync(file)
            const template = new xlsxtemp(xlsx)
            template.substitute(1, json)
            res.end(template.generate(), "binary")
        } catch (err) {
            next(err)
        }
    }
}
module.exports = Service