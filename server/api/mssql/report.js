"use strict"
const excel = require("exceljs")
const numeral = require("numeral")
const path = require("path")
const fs = require("fs")
const moment = require("moment")
const xlsxtemp = require("xlsx-template")
const dbs = require("./dbs")
const config = require("../config")
const logger4app = require("../logger4app")
const sec = require("../sec")
const ous = require("./ous")
const util = require("../util")
const ENT = config.ent
const grant = config.ou_grant
const tenhd = (type) => {
    let result = config.ITYPE.find(item => item.id === type)
    return result.value
}
const charList = (a, z, d = 1) => (a=a.charCodeAt(),z=z.charCodeAt(),[...Array(Math.floor((z-a)/d)+1)].map((_,i)=>String.fromCharCode(a+i*d)))
const rbi = (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            const sql = `select doc from s_inv where id=@1`
            const result = await dbs.query(sql, [id])
            const rows = result.recordset
            if (rows.length == 0) reject(new Error(`Không tìm thấy Hóa đơn ${id} \n (Invoice not found ${id})`))
            let doc = JSON.parse(rows[0].doc)

            resolve(doc)
        } catch (err) {
            reject(err)
        }
    })
}
const Service = {
    VATVCM: async (req, res, next) => {
        const token = sec.decode(req), query = req.body, taxc = token.taxc
        const tn = query.tn, type = query.type, report = query.report, ou = (typeof query.ou == "undefined" ? "*" : query.ou)
        const type_re = query.type_date
        const firstlist = (query.firstlist && query.firstlist == "1") ? "X" : "", updatelist = query.updatelist
        const fm = moment(query.fd), tm = moment(query.td)
        const fd = fm.startOf("day").format("YYYY-MM-DD HH:mm:ss"), td = tm.endOf("day").format("YYYY-MM-DD HH:mm:ss")

        const fr = fm.format(config.mfd), to = tm.format(config.mfd), rt = moment().format(config.mfd)
        const now = new Date(), ndate = now.getDate(), nmonth = now.getMonth() + 1, nyear = now.getFullYear()
        let sql, result, where, binds, json, fn, rows, i, where_ft = "", val, extcol = ""
        result = await dbs.query("select top 1 fadd from s_ou where id=@1", [token.ou])
        let sfaddr
        if (result.recordset.length > 0) sfaddr = result.recordset[0].fadd

        if (query.serial != "" && (typeof query.serial != "undefined")) {//dieu kiện an hien tuy du an

            val = query.serial
            let str = "", arr = val.split(",")
            for (const row of arr) {
                str += `'${row}',`
            }
            where_ft += ` and serial in (${str.slice(0, -1)})`
        }
        if (query.ou != "" && (typeof query.ou != "undefined")) {//dieu kiện an hien tuy du an

            val = query.ou
            let str = "", arr = val.split(",__")
            for (const row of arr) {
                let d = row.split("__")
                str += `${d[0]},`
            }
            where_ft += ` and ou in (${str.slice(0, -1)})`


        }
        if (ENT == "vcm") {
            fn = "temp/BKHDVAT_VCM.xlsx"
            extcol = `,totalv,FORMAT(TRY_CAST(businessdate as date),'dd/MM/yyyy') c2,c3,c6 sec,ou,stax,extra extra, case when adjtyp in (1,2) then adjdes else cde end adjdes, status`
        } else if (ENT == "mzh") {
            fn = "temp/BKHDVAT_MZH.xlsx"
        }

        where = `where a.idt between @1 and @2 and a.stax=@3 and a.status=3 and a.type='01GTKT'`// and a.cid is null`
        if (ENT == "vcm") {
            where = `where a.idt between @1 and @2 and a.stax=@3 and a.status in (3,4) and a.type='01GTKT'`
        }
        binds = [fd, td, taxc]
        i = 4

        let taxsql = /*(ENT != "vcm") ? `case when adjtyp=2 then JSON_QUERY(doc,'$.tax') else JSON_QUERY(doc,'$.tax') end` : */`tax`
        where += where_ft
        //Lấy các hóa đơn đã duyệt, thêm trường factor đánh dấu hệ số dương
        sql = `select FORMAT(idt,'dd/MM/yyyy') idt,form,serial,seq,btax,bname,adjdes,cde,adjtyp,dif dif,adj adj,${taxsql} taxs,note,1 as factor,FORMAT(idt,'dd/MM/yyyy') rdt${extcol} from s_inv a ${where}`
        //  binds = [fd, td, taxc]
        if (config.BKHDVAT_EXRA_OPTION == '2') {
            //Làm riêng cho Deloitte

            //Lấy hóa đơn hủy thay thế => status = 4, ngày lấy theo dt, factor đánh dấu hệ số âm

            where = `where cdt between @1 and @2 and stax=@3  and status=4 and type='01GTKT' and xml is not null`

            where += where_ft
            sql += ` union all 
            select FORMAT(${(ENT == "vcm") ? 'idt' : 'cdt'},'dd/MM/yyyy') idt,form,serial,seq,btax,bname,adjdes,cde,adjtyp,dif dif,adj adj,${taxsql} taxs,crea note,-1 as factor,FORMAT(idt,'dd/MM/yyyy') rdt${extcol} from s_inv ${where}`


            //Hóa đơn bị hủy, thay thế => status = 4, ngày lấy theo idt, factor đánh dấu hệ số dương
            where = `where idt between @1 and @2  and stax=@3 and status=4 and type='01GTKT' and xml is not null`

            where += where_ft
            sql += ` union all 
            select FORMAT(idt,'dd/MM/yyyy') idt,form,serial,seq,btax,bname,adjdes,cde,adjtyp,dif dif,adj adj,${taxsql} taxs,note,1 as factor,FORMAT(idt,'dd/MM/yyyy') rdt${extcol} from s_inv ${where}`
            //binds.push(fd), binds.push(td),binds.push(taxc)
        }
        sql += ' order by idt,form,serial,seq'
        result = await dbs.query(sql, binds)
        rows = result.recordset
        let a0 = [], a5 = [], a10 = [], a1 = [], a2 = []
        let i0 = 0, i5 = 0, i10 = 0, i1 = 0, i2 = 0
        let s0 = 0, s5 = 0, s10 = 0, s1 = 0, s2 = 0, v0 = 0, v5 = 0, v10 = 0, v1 = 0, v2 = 0
        for (const row of rows) {
            let idt, form, serial, seq, btax, bname, taxs
            try {
                taxs = JSON.parse(row.taxs)
            }
            catch (ex) {
                logger4app.debug('_____________tax null id', row.form, row.serial, row.seq)
                taxs = []
            }
            if (ENT != "vcm") {
                if (row.adjtyp == 2) {
                    row.note = `Bị điều chỉnh bởi HĐ ${row.form}-${row.serial}-${row.seq}`
                }

                //Dữ liệu của hóa đơn nào lấy của hóa đơn đấy
                idt = row.idt
                form = row.form
                serial = row.serial
                seq = row.seq
                btax = row.btax
                bname = row.bname
            } else {

                if (row.adjdes != null) {
                    row.note = row.adjdes
                }
                else {
                    if (row.cde != null) {
                        row.note = row.cde
                    }
                }
                //Dữ liệu của hóa đơn nào lấy của hóa đơn đấy
                idt = row.idt
                form = row.form
                serial = row.serial
                seq = row.seq
                btax = row.btax
                bname = row.bname
            }
            if (row.factor == -1) {
                row.note = "Hóa đơn gốc ngày " + row.rdt
            }
            if (ENT != "vcm") {
                if (row.adjtyp == 2) {
                    //Nếu chênh lệch dương thì gán số ấm vì là điều chỉnh giảm
                    if (row.dif) {
                        let dif = JSON.parse(row.dif)
                        if (dif.total > 0) row.factor = -1 * row.factor
                    }
                    else {
                        if (row.adj) {
                            let adj = JSON.parse(row.adj)
                            if (adj.adjType && adj.adjType == '3') row.factor = -1 * row.factor
                        }
                    }
                }
            }
            let vbname = {bname : bname}
                        
                        for (const c of config.SPECIAL_CHAR) {
                            vbname = JSON.parse(JSON.stringify(vbname).replace(c, ""))
                            bname = vbname.bname
                        }
            for (const tax of taxs) {
                if (tax.amtv != 0 || (row.totalv == 0) || (tax.amtv == 0 && tax.vatv != 0)) {

                    let vrt = Number(tax.vrt), s = util.isNumber(tax.amtv) ? tax.amtv * row.factor : 0, v = util.isNumber(tax.vatv) ? (tax.vatv * row.factor) : 0, r
                    if (ENT == "vcm") {

                        row.note = row.status != 4 ? row.adjdes : "Xóa bỏ"

                        row.bname = (row.bname != null && row.bname != "") ? row.bname : row.buyer
                        if (row.adjtyp == 2) {
                            if (String(row.adjdes).includes("giảm")) {
                                s = (util.isNumber(s) ? ((-1) * s) : 0)
                                v = (util.isNumber(v) ? ((-1) * v) : 0)
                            } else if (String(row.adjdes).includes("tăng")) {
                                s = (util.isNumber(s) ? (s) : 0)
                                v = (util.isNumber(v) ? (v) : 0)
                            }
                        }
                        if (row.status == 4) {
                            s = 0
                            v = 0
                        }

                        r = { idt: idt, form: form, serial: serial, seq: seq, btax: btax, bname, sumv: s, vatv: v, note: (row.note) ? String(row.note) : "", vrn: tax.vrn, sec: row.sec, ou: row.ou, bname: row.bname, btax: row.btax, c2: row.c2, c3: row.c3, stax: row.stax, totalv: row.totalv, extra: row.extra }

                        try {
                            let invou = await ous.obid(row.ou)
                            r.ou = invou.name
                        }
                        catch (err) {
                            r.ou = ``
                        }

                    } else {
                        r = { idt: idt, form: form, serial: serial, seq: seq, btax: btax, bname, sumv: s, vatv: v, note: row.note, vrn: tax.vrn }
                    }

                    switch (vrt) {
                        case 0:
                            s0 += s
                            v0 += v
                            r.totalv = s + v
                            r.index = ++i0
                            a0.push(r)
                            break
                        case 5:
                            s5 += s
                            v5 += v
                            r.totalv = s + v
                            r.index = ++i5
                            a5.push(r)
                            break
                        case 10:
                            s10 += s
                            v10 += v
                            r.totalv = s + v
                            r.index = ++i10
                            a10.push(r)
                            break
                        case -1:
                            s1 += s
                            v1 += v
                            r.totalv = s + v
                            r.index = ++i1
                            a1.push(r)
                            break
                        case -2:
                            s2 += s
                            v2 += v
                            r.totalv = s + v
                            r.index = ++i2
                            a2.push(r)
                            break
                        default:
                            break
                    }
                }

            }
        }
        json = { stax: taxc, sname: token.on, fd: fr, td: to, rt: rt, a0: a0, s0: s0, v0: v0, a5: a5, s5: s5, v5: v5, a10: a10, s10: s10, v10: v10, a1: a1, s1: s1, v1: v1, a2: a2, s2: s2, v2: v2, s: (s0 + s5 + s10 + s1 + s2), v: (v5 + v10 + v0 + v1 + v2), t2: s2, t1: s1, t10: (s10 + v10), t5: (s5 + v5), t0: s0 }
        return json
    },
    VAT: async (req, res, next) => {
        const token = sec.decode(req), query = req.body, taxc = token.taxc
        const tn = query.tn, type = query.type, report = query.report, ou = (typeof query.ou == "undefined" ? "*" : query.ou)
        const type_re = query.type_date
        const firstlist = (query.firstlist && query.firstlist == "1") ? "X" : "", updatelist = query.updatelist
        const fm = moment(query.fd), tm = moment(query.td)
        const fd = fm.startOf("day").format("YYYY-MM-DD HH:mm:ss"), td = tm.endOf("day").format("YYYY-MM-DD HH:mm:ss")

        let workbook = new excel.Workbook()
        let worksheet = workbook.addWorksheet("sheet 1")

                numeral.locale("vi")
                const borderStyles = {
                    top: { style: "thin" },
                    left: { style: "thin" },
                    bottom: { style: "thin" },
                    right: { style: "thin" }
                };

                worksheet.columns = [
                    { key: "index", width: 6, style: { alignment: { vertical: 'middle', horizontal: 'center' } } },
                    { key: "form", width: 15 },
                    { key: "serial", width: 20 },
                    { key: "seq", width: 20 },
                    { key: "idt", width: 25 },
                    // { key: "ma_cqthu", width: 15 },
                    { key: "bname", width: 40 },
                    { key: "btax", width: 25 },
                    { key: "curr", width: 25 },
                    { key: "exrt", width: 25 },
                    { key: "sumv", width: 25 },
                    { key: "vatv", width: 25 },
                    { key: "sum", width: 25 },
                    { key: "vat", width: 25 },
                    { key: "note", width: 60 },
                ];

                worksheet.mergeCells('A1:J1');
                worksheet.getCell('A1').value = 'CỘNG HOÀ XÃ HỘI CHỦ NGHĨA VIỆT NAM'
                worksheet.getCell('A1').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('A1').alignment = { horizontal: 'center' }

                worksheet.mergeCells('A2:J2');
                worksheet.getCell('A2').value = 'ĐỘC LẬP TỰ DO HẠNH PHÚC'
                worksheet.getCell('A2').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('A2').alignment = { horizontal: 'center' }

                worksheet.mergeCells('A6:J6');
                worksheet.getCell('A6').value = 'BẢNG KÊ HOÁ ĐƠN ĐÃ SỬ DỤNG THEO NGƯỜI BÁN'
                worksheet.getCell('A6').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('A6').alignment = { horizontal: 'center' }

                worksheet.mergeCells('A7:J7')
                worksheet.getCell('A7').value = `Từ ngày ${moment(fd).format('DD/MM/YYYY')} đến ngày ${moment(td).format('DD/MM/YYYY')}`
                worksheet.getCell('A7').font = { name: 'Times New Roman', italic: true }
                worksheet.getCell('A7').alignment = { horizontal: 'center' }

                worksheet.mergeCells('I3:J3');
                worksheet.getCell('I3').value = ' Mẫu số: 08/BK-HĐXT'
                worksheet.getCell('I3').border = borderStyles
                worksheet.getCell('I3').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('I3').alignment = { horizontal: 'center' }

                worksheet.mergeCells('I4:J4');
                worksheet.getCell('I4').value = 'Ban hành kèm theo Quyết định số 1209/QĐ-BTC ngày 23/06/2015 của Bộ Tài chính Mẫu biểu báo cáo theo nghị định 119/2018'
                worksheet.getCell('I4').border = borderStyles
                worksheet.getCell('I4').font = { name: 'Times New Roman'}
                worksheet.getCell('I4').alignment = { horizontal: 'center' }

                let sname= token.on 
                worksheet.mergeCells('A8:F8')
                worksheet.getCell('A8').value = `Tên người bán: ${sname||""}`
                worksheet.getCell('A8').font = { name: 'Times New Roman' }
                worksheet.getCell('A8').alignment = { horizontal: 'left' }

                worksheet.mergeCells('G8:J8')
                worksheet.getCell('G8').value = `Đơn vị báo cáo: ${sname||""}`
                worksheet.getCell('G8').font = { name: 'Times New Roman' }
                worksheet.getCell('G8').alignment = { horizontal: 'left' }

                let stax= taxc
                worksheet.mergeCells('A9:F9')
                worksheet.getCell('A9').value = `MST người bán: ${stax}`
                worksheet.getCell('A9').font = { name: 'Times New Roman' }
                worksheet.getCell('A9').alignment = { horizontal: 'left' }

                worksheet.mergeCells('A10:A11')
                worksheet.getCell('A10').value = 'STT'
                worksheet.getCell('A10').border = borderStyles
                worksheet.getCell('A10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('A10').alignment = { horizontal: 'center' }

                worksheet.mergeCells('B10:E10')
                worksheet.getCell('B10').value = 'Hóa đơn, chứng từ bán ra'
                worksheet.getCell('B10').border = borderStyles
                worksheet.getCell('B10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('B10').alignment = { horizontal: 'center' }

                worksheet.mergeCells('F10:F11')
                worksheet.getCell('F10').value = 'Tên đơn vị mua'
                worksheet.getCell('F10').border = borderStyles
                worksheet.getCell('F10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('F10').alignment = { horizontal: 'center' }

                worksheet.mergeCells('G10:G11')
                worksheet.getCell('G10').value = 'Mã số thuế người mua'
                worksheet.getCell('G10').border = borderStyles
                worksheet.getCell('G10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('G10').alignment = { horizontal: 'center' }

                worksheet.mergeCells('H10:H11')
                worksheet.getCell('H10').value = 'Loại Tiền'
                worksheet.getCell('H10').border = borderStyles
                worksheet.getCell('H10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('H10').alignment = { horizontal: 'center' }

                worksheet.mergeCells('I10:I11')
                worksheet.getCell('I10').value = 'Tỷ Giá'
                worksheet.getCell('I10').border = borderStyles
                worksheet.getCell('I10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('I10').alignment = { horizontal: 'center' }

                worksheet.mergeCells('J10:J11')
                worksheet.getCell('J10').value = 'Doanh thu chưa có thuế GTGT'
                worksheet.getCell('J10').border = borderStyles
                worksheet.getCell('J10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('J10').alignment = { horizontal: 'center' }

                worksheet.mergeCells('K10:K11')
                worksheet.getCell('K10').value = 'Thuế GTGT'
                worksheet.getCell('K10').border = borderStyles
                worksheet.getCell('K10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('K10').alignment = { horizontal: 'center' }

                worksheet.mergeCells('L10:L11')
                worksheet.getCell('L10').value = 'Doanh thu chưa có thuế GTGT nguyên tệ'
                worksheet.getCell('L10').border = borderStyles
                worksheet.getCell('L10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('L10').alignment = { horizontal: 'center' }

                worksheet.mergeCells('M10:M11')
                worksheet.getCell('M10').value = 'Thuế GTGT nguyên tệ'
                worksheet.getCell('M10').border = borderStyles
                worksheet.getCell('M10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('M10').alignment = { horizontal: 'center' }

                worksheet.mergeCells('N10:N11')
                worksheet.getCell('N10').value = 'Ghi chú'
                worksheet.getCell('N10').border = borderStyles
                worksheet.getCell('N10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('N10').alignment = { horizontal: 'center' }

                // worksheet.mergeCells('M10:M11')
                // worksheet.getCell('M10').value = ''
                // worksheet.getCell('M10').border = borderStyles
                // worksheet.getCell('M10').font = { name: 'Times New Roman', bold: true }
                // worksheet.getCell('M10').alignment = { horizontal: 'center' }

                worksheet.mergeCells('B11:D11')
                worksheet.getCell('B11').value = 'Ký hiệu mẫu hóa đơn, ký hiệu hóa đơn, số hóa đơn'
                worksheet.getCell('B11').border = borderStyles
                worksheet.getCell('B11').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('B11').alignment = { horizontal: 'center' }

                worksheet.getCell('E11').value = 'Ngày, tháng, năm lập hóa đơn'
                worksheet.getCell('E11').border = borderStyles
                worksheet.getCell('E11').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('E11').alignment = { horizontal: 'center' }

                worksheet.getCell('A12').value = '(1)'
                worksheet.getCell('A12').border = borderStyles
                worksheet.getCell('A12').font = { name: 'Times New Roman' }
                worksheet.getCell('A12').alignment = { horizontal: 'center' }

                worksheet.getCell('B12').value = '(2)'
                worksheet.getCell('B12').border = borderStyles
                worksheet.getCell('B12').font = { name: 'Times New Roman' }
                worksheet.getCell('B12').alignment = { horizontal: 'center' }

                worksheet.getCell('C12').value = '(3)'
                worksheet.getCell('C12').border = borderStyles
                worksheet.getCell('C12').font = { name: 'Times New Roman' }
                worksheet.getCell('C12').alignment = { horizontal: 'center' }

                worksheet.getCell('D12').value = '(4)'
                worksheet.getCell('D12').border = borderStyles
                worksheet.getCell('D12').font = { name: 'Times New Roman' }
                worksheet.getCell('D12').alignment = { horizontal: 'center' }

                worksheet.getCell('E12').value = '(5)'
                worksheet.getCell('E12').border = borderStyles
                worksheet.getCell('E12').font = { name: 'Times New Roman' }
                worksheet.getCell('E12').alignment = { horizontal: 'center' }

                worksheet.getCell('F12').value = '(6)'
                worksheet.getCell('F12').border = borderStyles
                worksheet.getCell('F12').font = { name: 'Times New Roman' }
                worksheet.getCell('F12').alignment = { horizontal: 'center' }

                worksheet.getCell('G12').value = '(7)'
                worksheet.getCell('G12').border = borderStyles
                worksheet.getCell('G12').font = { name: 'Times New Roman' }
                worksheet.getCell('G12').alignment = { horizontal: 'center' }

                worksheet.getCell('H12').value = '(8)'
                worksheet.getCell('H12').border = borderStyles
                worksheet.getCell('H12').font = { name: 'Times New Roman' }
                worksheet.getCell('H12').alignment = { horizontal: 'center' }

                worksheet.getCell('I12').value = '(9)'
                worksheet.getCell('I12').border = borderStyles
                worksheet.getCell('I12').font = { name: 'Times New Roman' }
                worksheet.getCell('I12').alignment = { horizontal: 'center' }

                worksheet.getCell('J12').value = '(10)'
                worksheet.getCell('J12').border = borderStyles
                worksheet.getCell('J12').font = { name: 'Times New Roman' }
                worksheet.getCell('J12').alignment = { horizontal: 'center' }

                worksheet.getCell('K12').value = '(11)'
                worksheet.getCell('K12').border = borderStyles
                worksheet.getCell('K12').font = { name: 'Times New Roman' }
                worksheet.getCell('K12').alignment = { horizontal: 'center' }

                worksheet.getCell('L12').value = '(12)'
                worksheet.getCell('L12').border = borderStyles
                worksheet.getCell('L12').font = { name: 'Times New Roman' }
                worksheet.getCell('L12').alignment = { horizontal: 'center' }
                
                worksheet.getCell('M12').value = '(13)'
                worksheet.getCell('M12').border = borderStyles
                worksheet.getCell('M12').font = { name: 'Times New Roman' }
                worksheet.getCell('M12').alignment = { horizontal: 'center' }

                worksheet.getCell('N12').value = '(14)'
                worksheet.getCell('N12').border = borderStyles
                worksheet.getCell('N12').font = { name: 'Times New Roman' }
                worksheet.getCell('N12').alignment = { horizontal: 'center' }

                // worksheet.getCell('M12').value = '(13)'
                // worksheet.getCell('M12').border = borderStyles
                // worksheet.getCell('M12').font = { name: 'Times New Roman' }
                // worksheet.getCell('M12').alignment = { horizontal: 'center' }

                let row_stt = 13
                let dataRange = charList('A', 'N')
        const fr = fm.format(config.mfd), to = tm.format(config.mfd), rt = moment().format(config.mfd)
        const now = new Date(), ndate = now.getDate(), nmonth = now.getMonth() + 1, nyear = now.getFullYear()
        let sql, result, where, binds, json, fn, rows, i, where_ft = "", val, extcol = ""
        result = await dbs.query("select top 1 fadd from s_ou where id=@1", [token.ou])
        let sfaddr
        if (result.recordset.length > 0) sfaddr = result.recordset[0].fadd

        if (query.serial != "" && (typeof query.serial != "undefined")) {//dieu kiện an hien tuy du an

            val = query.serial
            let str = "", arr = val.split(",")
            for (const row of arr) {
                str += `'${row}',`
            }
            where_ft += ` and serial in (${str.slice(0, -1)})`
        }
        if (query.ou != "" && (typeof query.ou != "undefined")) {//dieu kiện an hien tuy du an

            val = query.ou
            let str = "", arr = val.split(",__")
            for (const row of arr) {
                let d = row.split("__")
                str += `${d[0]},`
            }
            where_ft += ` and ou in (${str.slice(0, -1)})`


        }
        if (query.cqtstatus != "*" ) {
            where_ft += ` and status_received = ${query.cqtstatus}`
        }
        if (ENT == "vcm") {
            fn = "temp/BKHDVAT_VCM.xlsx"
            extcol = `,totalv,FORMAT(TRY_CAST(JSON_VALUE ([doc], '$.businessDate') as date),'dd/MM/yyyy') c2,c3,c6 sec,ou,stax,JSON_VALUE([doc], '$.extra') extra, case when adjtyp in (1,2) then JSON_VALUE ([doc], '$.adj.des') else cde end adjdes, status`
        } else if (ENT == "mzh") {
            fn = "temp/BKHDVAT_MZH.xlsx"
        }

        where = `where a.idt between @1 and @2 and a.stax=@3 and a.status=3 and a.type='01GTKT'`// and a.cid is null`
        if (ENT == "vcm") {
            where = `where a.idt between @1 and @2 and a.stax=@3 and a.status in (3,4) and a.type='01GTKT'`
        }
        binds = [fd, td, taxc]
        i = 4

        let taxsql = /*(ENT != "vcm") ? `case when adjtyp=2 then JSON_QUERY(doc,'$.tax') else JSON_QUERY(doc,'$.tax') end` : */`JSON_QUERY(doc,'$.tax')`
        where += where_ft
        //Lấy các hóa đơn đã duyệt, thêm trường factor đánh dấu hệ số dương
        sql = `select FORMAT(idt,'dd/MM/yyyy') idt,form,serial,seq,btax,bname,adjdes,cde,adjtyp,JSON_QUERY(doc,'$.dif') dif,JSON_QUERY(doc,'$.root') root,JSON_QUERY(doc,'$.adj') adj,${taxsql} taxs,note,1 as factor,FORMAT(idt,'dd/MM/yyyy') rdt${extcol}, id, ou,a.sumv,a.vatv,a.sum,a.vat,a.totalv,a.curr,a.exrt  from s_inv a ${where}`
        //  binds = [fd, td, taxc]
        if (config.BKHDVAT_EXRA_OPTION == '2') {
            //Làm riêng cho Deloitte

            //Lấy hóa đơn hủy thay thế => status = 4, ngày lấy theo dt, factor đánh dấu hệ số âm

            where = `where cdt between @1 and @2 and stax=@3  and status=4 and type='01GTKT' and xml is not null`

            where += where_ft
            sql += ` union all 
                    select FORMAT(${(ENT == "vcm") ? 'idt' : 'cdt'},'dd/MM/yyyy') idt,form,serial,seq,btax,bname,adjdes,cde,adjtyp,JSON_QUERY(doc,'$.dif') dif,JSON_QUERY(doc,'$.root') root,JSON_QUERY(doc,'$.adj') adj,${taxsql} taxs,CASE WHEN JSON_VALUE ([doc], '$.cancel.rea') IS NOT NULL THEN JSON_VALUE ([doc], '$.cancel.rea') ELSE cde END note,-1 as factor,FORMAT(idt,'dd/MM/yyyy') rdt${extcol}, id, ou,sumv,vatv,sum,vat,totalv,curr,exrt  from s_inv ${where}`


            //Hóa đơn bị hủy, thay thế => status = 4, ngày lấy theo idt, factor đánh dấu hệ số dương
            where = `where idt between @1 and @2  and stax=@3 and status=4 and type='01GTKT' and xml is not null`

            where += where_ft
            sql += ` union all 
                    select FORMAT(idt,'dd/MM/yyyy') idt,form,serial,seq,btax,bname,adjdes,cde,adjtyp,JSON_QUERY(doc,'$.dif') dif,JSON_QUERY(doc,'$.root') root,JSON_QUERY(doc,'$.adj') adj,${taxsql} taxs,note,1 as factor,FORMAT(idt,'dd/MM/yyyy') rdt${extcol}, id, ou, sumv,vatv,sum,vat,totalv,curr,exrt  from s_inv ${where}`
            //binds.push(fd), binds.push(td),binds.push(taxc)
        }
        sql += ' order by idt,form,serial,seq'
        result = await dbs.query(sql, binds)
        rows = result.recordset
        //Begin HUNGLQ them doan lay du lieu dong theo danh muc loai thue
        //Lay ra danh muc loai thue suat truoc
        let arr = JSON.parse(JSON.stringify(config.CATTAX))
        //Sap xep theo ty le thue vrt truoc, phai nhan voi 100 khong thi vrt 7 lai lon hon 10
        for (let itemArr of arr) {
            itemArr.sortprop = String(itemArr.vrt * 100).padStart(7, "0")
        }
        arr = util.sortobj(arr, 'sortprop', 1)
        let objRep = {}
        //Lap danh muc loai thue suat de tao key object truoc
        for (let vVat of arr) {
            //Dat key la ky tu dau va vrt nhan voi 100 de co so nguyen (de phong truong hop vrt = 5.26, 3.5)
            let vatKey = `VAT${vVat.vrt * 100}`
            //Neu dinh thay bang key la orriginal vrt thi bo comment doan duoi nay
            //let vatKey = `VAT${vVat.original_vrt * 100}`
            objRep[vatKey] = {
                totalSumByVrt: 0, //Tong cua du lieu sum theo tung vrt, gan truoc bang 0
                totalVatByVrt: 0, //Tong cua du lieu vat theo tung vrt, gan truoc bang 0
                totalSumvByVrt: 0, //Tong cua du lieu vat theo tung vrt, gan truoc bang 0
                totalVatvByVrt: 0, //Tong cua du lieu vat theo tung vrt, gan truoc bang 0
                index: 0, //gan so thu tu, gan truoc bang 0
                key_vrn: vVat.vrnVATRep, // danh dáu key len dong dien dai
                dataVat: [] //Du lieu bao cao, gan bang mang rong truoc, ti nua push du lieu sau
            }
        }
        //End HUNGLQ them doan lay du lieu dong theo danh muc loai thue

        for (const row of rows) {
            let idt, form, serial, seq, btax, bname, taxs = util.parseJson(row.taxs), ouname
            if (config.ent == 'sgr') {
                let doc = await rbi(row.id)
                taxs = doc.items

            }
            if (ENT == "vcm" || ENT == "sgr" || ENT == "scb") {
                if (row.adjdes != null) {
                    row.note = row.adjdes
                }
                else {
                    if (row.cde != null) {
                        row.note = row.cde
                    }
                }
                //Dữ liệu của hóa đơn nào lấy của hóa đơn đấy
                idt = row.idt
                form = row.form
                serial = row.serial
                seq = row.seq
                btax = row.btax
                bname = row.bname
            } else {
                // if (row.adjtyp == 2) {
                //     row.note = `Điều chỉnh tăng/ giảm thông tin cho HĐ ${row.form}-${row.serial}-${row.seq}`
                // }else row.note = `Bị điều chỉnh cho HĐ ${row.form}-${row.serial}-${row.seq}`
                //
                if (row.adjtyp == 2) {
                    let hd 
                    if (row.adj) {
                        hd = JSON.parse(row.adj)
                    }
                    if(row.note && row.note != null) 
                        row.note += `Điều chỉnh tăng/ giảm thông tin cho HĐ ${hd.seq}`
                    else
                        row.note = `Điều chỉnh tăng/ giảm thông tin cho HĐ ${hd.seq}`
                }   
                if (row.adjtyp == 1) {
                    let hdtt
                    if (row.adj) {
                        hdtt = JSON.parse(row.adj)
                    }
                    if(row.note && row.note != null)
                        row.note += `Thay thế cho hóa đơn ${hdtt.seq}`
                    else
                        row.note = `Thay thế cho hóa đơn ${hdtt.seq}`
                }
                if(row.cde && row.cde.includes('điều chỉnh')){
                    let result_seq = await dbs.query('select seq from s_inv where pid=@1 and form=@2 and serial=@3', [row.id,row.form,row.serial])
                    let row_seq = result_seq.recordset
                    let seq_new
                    if(row_seq.length>0) seq_new = row_seq[0].seq         
                    if(row.note && row.note != null) 
                        row.note += `${row.note ? '\n':''}Bị điều chỉnh cho HĐ ${row.form}-${row.serial}`+`${seq_new ? "-" + seq_new : ''}`
                    else 
                        row.note = `${row.note ? '\n':''}Bị điều chỉnh cho HĐ ${row.form}-${row.serial}`+`${seq_new ? "-" + seq_new : ''}`
                }
                if(row.cde && row.cde.includes('thay thế')){
                    let result_seq = await dbs.query('select seq from s_inv where pid=@1 and form=@2 and serial=@3', [row.id,row.form,row.serial])
                    let row_seq = result_seq.recordset
                    let seq_new
                    if(row_seq.length>0) seq_new = row_seq[0].seq
                    if(row.note && row.note != null) 
                        row.note += `${row.note ? '\n':''}Bị thay thế cho HĐ ${row.form}-${row.serial}`+`${seq_new ? "-" + seq_new : ''}`
                    else
                        row.note = `${row.note ? '\n':''}Bị thay thế cho HĐ ${row.form}-${row.serial}`+`${seq_new ? "-" + seq_new : ''}`
                }
                //Dữ liệu của hóa đơn nào lấy của hóa đơn đấy
                idt = row.idt
                form = row.form
                serial = row.serial
                seq = row.seq
                btax = row.btax
                bname = row.bname
            }
            if (row.factor == -1) {
                row.note = "Hóa đơn gốc ngày " + row.rdt
            }

            if (ENT != "vcm") {
                if (row.adjtyp == 2) {
                    //Nếu chênh lệch dương thì gán số ấm vì là điều chỉnh giảm
                    if (row.dif) {
                        let dif = JSON.parse(row.dif)
                        if (dif.total > 0) row.factor = -1 * row.factor
                    }
                    else {
                        if (row.adj) {
                            let adj = JSON.parse(row.adj)
                            if (adj.adjType && adj.adjType == '3') row.factor = -1 * row.factor
                        }
                    }
                }
            }
            let vbname = {bname : bname}
                        
            for (const c of config.SPECIAL_CHAR) {
                vbname = JSON.parse(JSON.stringify(vbname).replace(c, ""))
                bname = vbname.bname
            }
            for (const tax of taxs) {
                
                if (ENT == "sgr") {
                    tax.amtv = tax.amount
                    tax.vatv = tax.vat
                }
                //let vrt = Number(tax.vrt), s = util.isNumber(tax.amtv) ? tax.amtv * row.factor : 0, v = util.isNumber(tax.vatv) ? (tax.vatv * row.factor) : 0, r
                let vrt = Number(tax.vrt), ma = row.ma_cqthu, cqthu = (ma!=null&&ma.length>2) ? ma.substring(1,(ma.length-1)) : "" , s,v,r,sf,vf
                    s = util.isNumber(tax.amtv) ? Math.abs(tax.amtv) * row.factor : 0
                    v = util.isNumber(tax.vatv) ? (Math.abs(tax.vatv) * row.factor) : 0
                    sf = util.isNumber(tax.amt) ? Math.abs(tax.amt) * row.factor : 0
                    vf = util.isNumber(tax.vat) ? (Math.abs(tax.vat) * row.factor) : 0
                if (ENT == "vcm") {

                    row.note = row.status != 4 ? row.adjdes : "Xóa bỏ"

                    row.bname = (row.bname != null && row.bname != "") ? row.bname : row.buyer
                    if (row.adjtyp == 2) {
                        if (String(row.adjdes).includes("giảm")) {
                            s = (util.isNumber(s) ? ((-1) * s) : 0)
                            v = (util.isNumber(v) ? ((-1) * v) : 0)
                        } else if (String(row.adjdes).includes("tăng")) {
                            s = (util.isNumber(s) ? (s) : 0)
                            v = (util.isNumber(v) ? (v) : 0)
                        }
                    }
                    if (row.status == 4) {
                        s = 0
                        v = 0
                    }

                    r = { idt: idt, form: form, serial: serial, seq: seq, btax: btax, bname, sumv: s, vatv: v, note: row.note, vrn: tax.vrn, sec: row.sec, ou: row.ou, bname: row.bname, btax: row.btax, c2: row.c2, c3: row.c3, stax: row.stax, totalv: row.totalv, extra: row.extra }

                    try {
                        let invou = await ous.obid(row.ou)
                        r.ou = invou.name
                    }
                    catch (err) {
                        r.ou = ``
                    }

                } else if (ENT == "sgr") {
                    r = { idt: idt, form: form, serial: serial, seq: seq, btax: btax, bname, sumv: s, vatv: v, note: row.note }
                    r.itemname = tax.name
                    //___Thu them 2 cot trong bao cao
                    r.quantity = tax.quantity
                    r.price = tax.price

                } else {
                    r = { idt: idt, form: form, serial: serial, seq: seq, btax: btax, ma_cqthu:cqthu, bname, curr:row.curr, exrt:row.exrt, sumv: s, vatv: v,sum: sf, vat: vf, note: row.note }
                    //r = { idt: idt, form: form, serial: serial, seq: seq, btax: btax, bname, sumv: s, vatv: v, note: row.note, vrn: tax.vrn }
                }
                //Begin HUNGLQ them doan lay du lieu dong theo danh muc loai thue
                //Bat dau day du lieu vao objRep, neu doan nay on bo cai switch o duoi di
                const vatKey = `VAT${vrt * 100}`
                objRep[vatKey].dataVat.push(r)
                objRep[vatKey].totalSumByVrt += sf
                objRep[vatKey].totalVatByVrt += vf
                objRep[vatKey].totalSumvByVrt += s
                objRep[vatKey].totalVatvByVrt += v
                //End HUNGLQ them doan lay du lieu dong theo danh muc loai thue
            }
        }
        //Begin HUNGLQ them doan lay du lieu dong theo danh muc loai thue
        //Lay du lieu va day ra bien lu danh sach bao cao 
        //Khai bao bien day ra bao cao
        let arrRep = [],key_new,s=0,v=0,sf=0,vf=0
        for (var key in objRep) {
            //Neu mang co du lieu thi moi xu
            if (objRep[key].dataVat.length <= 0) continue
            //push cai dong dien giai truoc
            let rowname
            rowname = worksheet.addRow({ index: objRep[key].key_vrn , form: "", serial: "", seq: "", idt: "",bname: "",btax: "",sumv: "",vatv: "",sum: "",vat: "", note: ""})
            worksheet.mergeCells(row_stt, 1, row_stt, 14)
            rowname.getCell('A').border = borderStyles
            rowname.alignment = { horizontal: 'left' }
            rowname.font = { name: 'Times New Roman', bold: true }
            row_stt = row_stt + 1
            //Push tiep du lieu bao cao
            for (const rep of objRep[key].dataVat) {
                rep.index = ++objRep[key].index
                //danh dau de format font chữ checksum
                // rep.checksum = 0
                // arrRep.push(rep)
                let rowdata = worksheet.addRow(rep)
                rowdata.font = { name: 'Times New Roman' }
                //set border for range of data
                dataRange.map(x => {
                    rowdata.getCell(x).border = borderStyles
                    if(x=='L') rowdata.getCell(x).alignment = { wrapText:true }
                })
                row_stt = row_stt + 1
            }
            // arrRep.push({index:`TỔNG`,sumv:objRep[key].totalSumByVrt,vatv:objRep[key].totalVatByVrt,checksum:1})
            rowname = worksheet.addRow({ index: `TỔNG` , form: "", serial: "", seq: "", idt: "",bname: "",btax: "",sum:objRep[key].totalSumByVrt,vat: objRep[key].totalVatByVrt,sumv:objRep[key].totalSumvByVrt,vatv: objRep[key].totalVatvByVrt, note: ""})
            worksheet.mergeCells(row_stt, 1, row_stt, 9)
            worksheet.getCell(`J${row_stt}`).border = borderStyles
            worksheet.getCell(`K${row_stt}`).border = borderStyles
            worksheet.getCell(`L${row_stt}`).border = borderStyles
            worksheet.getCell(`M${row_stt}`).border = borderStyles
            worksheet.getCell(`N${row_stt}`).border = borderStyles

            rowname.getCell('A').border = borderStyles
            worksheet.getCell(`A${row_stt}`).alignment = { horizontal: 'left' }
            // rowname.alignment = { horizontal: 'left' }
            rowname.font = { name: 'Times New Roman', bold: true }
            row_stt = row_stt + 1
            sf+=objRep[key].totalSumByVrt
            vf+=objRep[key].totalVatByVrt
            s+=objRep[key].totalSumvByVrt
            v+=objRep[key].totalVatvByVrt
            //Push cai dong tong
        }
        //End HUNGLQ them doan lay du lieu dong theo danh muc loai thue 
        // json = { stax: taxc, sname: token.on, fd: fr, td: to, rt: rt, arrRep: arrRep,s:s,v:v }

        // return json
        worksheet.mergeCells(row_stt, 1, row_stt, 9)
        worksheet.getCell(`A${row_stt}`).value = `${rt}`
        worksheet.getCell(`A${row_stt}`).border = borderStyles
        worksheet.getCell(`A${row_stt}`).font = { name: 'Times New Roman' }
        worksheet.getCell(`A${row_stt}`).alignment = { horizontal: 'left' }

        worksheet.getCell(`J${row_stt}`).value = `${Number(s.toFixed(config.GRIDNFCONF))}`
        worksheet.getCell(`J${row_stt}`).border = borderStyles
        worksheet.getCell(`J${row_stt}`).font = { name: 'Times New Roman', bold: true }
        worksheet.getCell(`J${row_stt}`).alignment = { horizontal: 'right' }


        worksheet.getCell(`K${row_stt}`).value = `${Number(v.toFixed(config.GRIDNFCONF))}`
        worksheet.getCell(`K${row_stt}`).border = borderStyles
        worksheet.getCell(`K${row_stt}`).font = { name: 'Times New Roman', bold: true }
        worksheet.getCell(`K${row_stt}`).alignment = { horizontal: 'right' }

        worksheet.getCell(`L${row_stt}`).value = `${Number(sf.toFixed(config.GRIDNFCONF))}`
        worksheet.getCell(`L${row_stt}`).border = borderStyles
        worksheet.getCell(`L${row_stt}`).font = { name: 'Times New Roman', bold: true }
        worksheet.getCell(`L${row_stt}`).alignment = { horizontal: 'right' }


        worksheet.getCell(`M${row_stt}`).value = `${Number(vf.toFixed(config.GRIDNFCONF))}`
        worksheet.getCell(`M${row_stt}`).border = borderStyles
        worksheet.getCell(`M${row_stt}`).font = { name: 'Times New Roman', bold: true }
        worksheet.getCell(`M${row_stt}`).alignment = { horizontal: 'right' }


        const buffer = await workbook.xlsx.writeBuffer()
        res.end(buffer, "binary")
        return
    },
    get: async (req, res, next) => {
        try {
            const token = sec.decode(req), query = req.body, taxc = token.taxc
            const tn = query.tn, type = query.type, report = query.report, ou = (typeof query.ou == "undefined" ? "*" : query.ou)
            const type_re = query.type_date
            const firstlist = (query.firstlist && query.firstlist == "1") ? "X" : "", updatelist = query.updatelist
            const fm = moment(query.fd), tm = moment(query.td)
            const fd = fm.startOf("day").format("YYYY-MM-DD HH:mm:ss"), td = tm.endOf("day").format("YYYY-MM-DD HH:mm:ss")

            const fr = fm.format(config.mfd), to = tm.format(config.mfd), rt = moment().format(config.mfd)
            const now = new Date(), ndate = now.getDate(), nmonth = now.getMonth() + 1, nyear = now.getFullYear()
            let sql, result, where, binds, json, fn, rows, i, where_ft = "", val, extcol = ""
            result = await dbs.query("select top 1 fadd from s_ou where id=@1", [token.ou])
            let sfaddr
            if (result.recordset.length > 0) sfaddr = result.recordset[0].fadd

            if (query.serial != "" && (typeof query.serial != "undefined")) {//dieu kiện an hien tuy du an

                val = query.serial
                let str = "", arr = val.split(",")
                for (const row of arr) {
                    str += `'${row}',`
                }
                where_ft += ` and serial in (${str.slice(0, -1)})`
            }
            if (query.ou != "" && (typeof query.ou != "undefined")) {//dieu kiện an hien tuy du an

                val = query.ou
                let str = "", arr = val.split(",__")
                for (const row of arr) {
                    let d = row.split("__")
                    str += `${d[0]},`
                }
                where_ft += ` and ou in (${str.slice(0, -1)})`


            }
            if (report == 1) {
                fn = "temp/08BKHDVAT.xlsx"
                if (ENT == "sgr") {
                    fn = "temp/08BKHDVAT_SGR.xlsx"
                }
                if (ENT == "vcm") {
                    fn = "temp/BKHDVAT_VCM.xlsx"
                    extcol = `,totalv,FORMAT(TRY_CAST(JSON_VALUE ([doc], '$.businessDate') as date),'dd/MM/yyyy') c2,c3,c6 sec,ou,stax,JSON_VALUE([doc], '$.extra') extra, case when adjtyp in (1,2) then JSON_VALUE ([doc], '$.adj.des') else cde end adjdes, status`
                } else if(ENT == "mzh"){
                    fn = "temp/BKHDVAT_MZH.xlsx"
                }

                if (ENT == "vcm") {
                    json = await Service.VATVCM(req, res, next) 
                } else {
                    json = await Service.VAT(req, res, next)
                }
            }
            else if (report == 2) {
                fn = "temp/08BKHDXT.xlsx"
                where = "where idt between @1 and @2 and stax=@3"
                binds = [fd, td, taxc]
                let ij = 4
                if (type !== "*") {
                    where += ` and type=@${ij++}`
                    binds.push(type)
                }

                where += where_ft
                if(query.cqtstatus && query.cqtstatus!="*") where += ` and status_received = ${query.cqtstatus}`

                sql = `select FORMAT(idt,'dd/MM/yyyy') idt,form,serial,seq,btax,bname,buyer,CAST((CASE WHEN status=1 THEN N'Chờ cấp số' WHEN status=2 THEN N'Chờ duyệt' WHEN status=3 THEN N'Đã duyệt' WHEN status=4 THEN N'Đã hủy' WHEN status=6 THEN N'Chờ hủy' ELSE CAST(status as nvarchar(22)) END) as nvarchar(22)) status,status cstatus,sumv,vatv,CONCAT(note,'-',cde,'-',adjdes) note from s_inv ${where}`
                if (config.BKHDVAT_EXRA_OPTION == '2') {
                    //Làm riêng cho Deloitte, lấy thêm số âm
                    where += ` and status=4`
                    where = where.replace("idt", "cdt")
                    sql += ` union all select FORMAT(idt,'dd/MM/yyyy') idt,form,serial,seq,btax,bname,buyer,CAST((CASE WHEN status=1 THEN N'Chờ cấp số' WHEN status=2 THEN N'Chờ duyệt' WHEN status=3 THEN N'Đã duyệt' WHEN status=4 THEN N'Đã hủy' WHEN status=6 THEN N'Chờ hủy' ELSE CAST(status as nvarchar(22)) END) as nvarchar(22)) status,status cstatus,-1*sumv sumv,-1*vatv vatv,CONCAT(note,'-',cde,'-',adjdes) note from s_inv ${where}`
                }
                sql += ` order by idt,form,serial,seq`
                result = await dbs.query(sql, binds)
                rows = result.recordset
                let i = 0
                for (const row of rows) {
                    row.index = ++i
                    if (config.BKHDVAT_EXRA_OPTION != '2') {
                        if (row.cstatus == 4) {
                            delete row["sumv"]
                            delete row["vatv"]
                        }
                    }
                    let arr = row.note.split("-"), arr1 = []
                    for (let i = 0; i <= arr.length - 1;i++) {
                        if (String(arr[i]).length > 0) arr1.push(arr[i])
                    }
                    row.note = arr1.join("-")
                }
                json = { stax: taxc, sname: token.on, fd: fr, td: to, rt: rt, tn: tn, table: rows }
            }
            else if (report == 3) {
                fn = "temp/05BCHD.xlsx"
                let data = []
                let where = "", binds = [taxc, fd, td], type_name, where_iv = "", where_seri = ""
                let ij = 4
                if (type !== "*") {
                    where = ` and type=@${ij++}`
                    binds.push(type)
                    type_name = tenhd(type)
                }
                //{trung sua them chi nhanh,ky hieu in bao cao
                if (query.serial != "" && (typeof query.serial != "undefined")) {//dieu kiện an hien tuy du an

                    val = query.serial
                    let str = "", arr = val.split(",")
                    for (const row of arr) {
                        str += `'${row}',`
                    }
                    where_seri += ` and serial in (${str.slice(0, -1)})`
                }
                where_iv = where
                where_seri = where + where_seri
                where_iv += where_ft
                // sua dap ung nhieu dai so khac nhau chung ky hieu
                sql = `select form,serial,0 min1,0 max1,min min0,max max0,0 maxu0,0 minu,0 maxu, 0 minc,0 maxc,0 cancel,null clist from s_serial where taxc=@1 and fd<@2 and (status not in (2,3,4) or (td>=@2 and status=2 ) or (td>=@2 and status=4 )) ${where} order by form,serial,min`
                result = await dbs.query(sql, [taxc, fd, fd, type])
                rows = result.recordset
                let rows_serial = [], formc = "", serialc = "", bindsc = [], rows_serialarr = []
                for (const row of rows) {
                    if (formc == row.form && serialc == row.serial) {
                        for (let rowc of rows_serial) {
                            if (rowc.form == row.form && rowc.serial == row.serial) {
                                rowc.max0 = row.max0
                            }

                        }
                    } else {
                        rows_serial.push(row)
                    }
                    formc = row.form
                    serialc = row.serial
                }
                let c_id = moment(new Date()).format("YYYYMMDDHHHHmmssSSS")
                for (let rowc of rows_serial) {

                    rowc.c_id = c_id
                    rowc = Object.values(rowc)
                    rows_serialarr.push(rowc)
                }

                if (rows_serialarr.length > 0) {
                    await dbs.queries("insert into s_report_tmp(form,serial,min1, max1, min0, max0, maxu0,minu, maxu,minc,maxc, cancel,clist,c_id) values  (@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14)", rows_serialarr)
                }


                sql = `select form,serial,min min1,max max1,0 min0,0 max0,0 maxu0,0 minu,0 maxu,0 minc,0 maxc,0 cancel,null clist from s_serial where taxc=@1 and fd between @2 and @3 and status !=3 ${where} order by form,serial,min`
                result = await dbs.query(sql, [taxc, fd, td, type])
                rows = result.recordset
                rows_serial = [], formc = "", serialc = "", bindsc = [], rows_serialarr = []
                for (const row of rows) {
                    if (formc == row.form && serialc == row.serial) {
                        for (let rowc of rows_serial) {
                            if (rowc.form == row.form && rowc.serial == row.serial) {
                                rowc.max1 = row.max1
                            }

                        }
                    } else {
                        rows_serial.push(row)
                    }
                    formc = row.form
                    serialc = row.serial
                }
                let c_id2 = moment(new Date()).format("YYYYMMDDHHHHmmssSSS")
                for (let rowc of rows_serial) {

                    rowc.c_id = c_id2
                    rowc = Object.values(rowc)
                    rows_serialarr.push(rowc)
                }

                if (rows_serialarr.length > 0) {
                    await dbs.queries("insert into s_report_tmp(form,serial,min1, max1, min0, max0, maxu0,minu, maxu,minc,maxc,  cancel,clist,c_id) values  (@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14)", rows_serialarr)
                }

                sql = `select form,serial,0 min1,0 max1,0 min0,0 max0,0 maxu0,0 minu,0 maxu, cur minc,max maxc,0 cancel,null clist from s_serial where taxc=@1 and fd < @3 and td between @2 and @3 and status =2 ${where} order by form,serial,min`
                result = await dbs.query(sql, [taxc, fd, td, type])
                rows = result.recordset
                rows_serial = [], formc = "", serialc = "", bindsc = [], rows_serialarr = []
                for (const row of rows) {
                    if (formc == row.form && serialc == row.serial) {
                        for (let rowc of rows_serial) {
                            if (rowc.form == row.form && rowc.serial == row.serial) {
                                rowc.maxc = row.maxc
                            }

                        }
                    } else {
                        rows_serial.push(row)
                    }
                    formc = row.form
                    serialc = row.serial
                }
                let c_id3 = moment(new Date()).format("YYYYMMDDHHHHmmssSSS")
                for (let rowc of rows_serial) {

                    rowc.c_id = c_id3
                    rowc = Object.values(rowc)
                    rows_serialarr.push(rowc)
                }

                if (rows_serialarr.length > 0) {
                    await dbs.queries("insert into s_report_tmp(form,serial,min1, max1, min0, max0, maxu0,minu, maxu,minc,maxc, cancel,clist,c_id) values  (@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14)", rows_serialarr)
                }
                if (type_re != 2) {//mac dinh theo ngay lap hoa don:cdt
                    sql = `
                    with inv as (select form,serial,CAST(seq as int) seq from s_inv where idt between @2 and @3  and  stax=@1 and status=4 ${where_iv}) 
                    select a.form form,a.serial serial,sum(a.min1) min1,sum(a.max1) max1,sum(a.min0) min0,sum(a.max0) max0,sum(a.maxu0) maxu0,sum(a.minu) minu,sum(a.maxu) maxu,sum(a.minc) minc,sum(a.maxc) maxc,sum(a.cancel) cancel,max(a.clist) clist from (
                    select form,serial, min1, max1,min0,max0, maxu0, minu, maxu, minc, maxc, cancel,null clist from s_report_tmp where c_id= ${c_id}
                    union
                    select form,serial,min1,max1,min0,max0,maxu0,minu,maxu,minc, maxc,cancel,null clist from s_report_tmp where c_id= ${c_id2}
                    union
                    select form,serial,min1,max1,min0,max0,maxu0,minu,maxu,minc,maxc,cancel,null clist from s_report_tmp where c_id= ${c_id3}    
                    union
                    select form,serial,0 min1,0 max1,0 min0,0 max0,max(CAST(seq as int)) maxu0,0 minu,0 maxu,0 minc,0 maxc,0 cancel,null clist from s_inv where idt<@2  and stax=@1 and  status>2 ${where_iv} group by form,serial
                    union
                    select form,serial,0 min1,0 max1,0 min0,0 max0,0 maxu0,min(CAST(seq as int)) minu,max(CAST(seq as int)) maxu,0 minc,0 maxc,sum(CASE WHEN status=4 THEN 1 ELSE 0 END) cancel,null clist from s_inv where idt between @2 and @3  and stax=@1 and  status>2 ${where_iv} group by form,serial
                    union 
                    select distinct b.form,b.serial,0 min1,0 max1,0 min0,0 max0,0 maxu0,0 minu,0 maxu,0 minc,0 maxc,0 cancel,ISNULL(STUFF((select concat(',',seq) as [text()] from inv where form=b.form and serial=b.serial FOR XML PATH('')), 1, 1, NULL),'') clist from s_inv b where idt between @2 and @3  and stax=@1 and  status=4 ${where_iv} group by form,serial
                    ) a group by a.form,a.serial`
                } else {// truong hop bao cao theo ngay tạo create date:dt
                    sql = `
                    with inv as (select form,serial,CAST(seq as int) seq from s_inv where idt between @2 and @3  and  stax=@1 and status=4 ${where_iv}) 
                    select a.form form,a.serial serial,sum(a.min1) min1,sum(a.max1) max1,sum(a.min0) min0,sum(a.max0) max0,sum(a.maxu0) maxu0,sum(a.minu) minu,sum(a.maxu) maxu,sum(a.minc) minc,sum(a.maxc) maxc,sum(a.cancel) cancel,max(a.clist) clist from (
                    select form,serial, min1, max1,min0,max0, maxu0, minu, maxu, minc, maxc, cancel,null clist from s_report_tmp where c_id= ${c_id}
                    union
                    select form,serial,min1,max1,min0,max0,maxu0,minu,maxu, minc, maxc,cancel,null clist from s_report_tmp where c_id= ${c_id2}
                    union
                    select form,serial,min1,max1,min0,max0,maxu0,minu,maxu,minc,maxc,cancel,null clist from s_report_tmp where c_id= ${c_id3}  
                    union
                    select form,serial,0 min1,0 max1,0 min0,0 max0,max(CAST(seq as int)) maxu0,0 minu,0 maxu,0 minc,0 maxc,0 cancel,null clist from s_inv where dt<@2  and stax=@1 and  status>2 ${where_iv} group by form,serial
                    union
                    select form,serial,0 min1,0 max1,0 min0,0 max0,0 maxu0,min(CAST(seq as int)) minu,max(CAST(seq as int)) maxu,0 minc,0 maxc,sum(CASE WHEN status=4 THEN 1 ELSE 0 END) cancel,null clist from s_inv where dt between @2 and @3  and stax=@1 and  status>2 ${where_iv} group by form,serial
                    union 
                    select distinct b.form,b.serial,0 min1,0 max1,0 min0,0 max0,0 maxu0,0 minu,0 maxu,0 minc,0 maxc,0 cancel,ISNULL(STUFF((select concat(',',seq) as [text()] from inv where form=b.form and serial=b.serial FOR XML PATH('')), 1, 1, NULL),'') clist from s_inv b where dt between @2 and @3  and stax=@1 and  status=4 ${where_iv} group by form,serial
                    ) a group by a.form,a.serial`
                }

                result = await dbs.query(sql, binds)
                rows = result.recordset
                await dbs.query(`Delete from s_report_tmp where c_id=${c_id}`)
                await dbs.query(`Delete from s_report_tmp where c_id=${c_id2}`)
                await dbs.query(`Delete from s_report_tmp where c_id=${c_id3}`)
                let i = 0

                for (let row of rows) {
                    row.index = ++i
                    row.type_name = type == "*" ? tenhd(row.form.substr(0, 6)) : type_name

                    const clist = (row.clist != null) ? row.clist.split(",") : null
                    if (util.isEmpty(clist)) {
                        delete row.cancel
                    }
                    else {
                        let arr = clist.sort((a, b) => a - b), len = arr.length, str
                        if (len === 1) {
                            str = arr[0].toString().padStart(config.SEQ_LEN, "0")
                        }
                        else if (len === 2) {
                            const cs = arr[0] + 1 === arr[1] ? "-" : ";"
                            for (let i = 0; i < len; i++) {
                                arr[i] = arr[i].toString().padStart(config.SEQ_LEN, "0")
                            }
                            str = arr.join(cs)
                        }
                        else {
                            let del = []
                            for (let i = 1; i < len; i++) {
                                if (Number(arr[i - 1]) + 2 === Number(arr[i + 1])) del.push(i)
                            }
                            for (let i = 0; i < len; i++) {
                                if (del.includes(i)) arr[i] = null
                                else arr[i] = arr[i].toString().padStart(config.SEQ_LEN, "0")
                            }
                            str = arr.join(";").replace(/\;\;+/g, '-')
                        }
                        row.clist = str
                    }
                    let max0 = parseInt(row.max0), min0 = parseInt(row.min0), max1 = parseInt(row.max1), min1 = parseInt(row.min1), maxu0 = parseInt(row.maxu0), minu = parseInt(row.minu), maxu = parseInt(row.maxu), cancel = parseInt(row.cancel), minc = parseInt(row.minc), maxc = parseInt(row.maxc)
                     //use
                     let su = 0, s0 = 0
                     if (maxu > 0) {
                         su = maxu - minu + 1
                         row.su = su
                         row.fu = minu.toString().padStart(config.SEQ_LEN, '0')
                         row.tu = maxu.toString().padStart(config.SEQ_LEN, '0')
                         row.used = cancel > 0 ? (su - cancel) : su
                     }
                     //tondau
                     if (max1 > 0) {
                         if(max0 > 0){
                             s0 = max1 - min0 + 1
                             row.s0 = s0
                            
                         }else{
                             s0 = max1 - min1 + 1
                             row.s0 = s0
                            
                         }
                         row.f1 = min1.toString().padStart(config.SEQ_LEN, '0')
                         row.t1 = max1.toString().padStart(config.SEQ_LEN, '0')
                     }
                      if (max0 > 0) {
                         if (maxu0 > 0) {
                             if (max0 > maxu0) {
                                 if(max1 > 0){
                                     s0 = max1 - maxu0
                                 }else{
                                     s0 = max0 - maxu0
                                 }
                                
                                 row.s0 = s0
                                 row.f0 = (maxu0 + 1).toString().padStart(config.SEQ_LEN, '0')
                                 row.t0 = max0.toString().padStart(config.SEQ_LEN, '0')
                             }
                             if (max0 == maxu0 && max1>maxu0) {
                                 s0 = max1 - maxu0
                                 row.s0 = s0
                                
                             }
                         }
                         else {
                             if(max1 > 0){
                                 s0 = max1 - min0 + 1
                             }else{
                                 s0 = max0 - min0 + 1
                             }
                            // s0 = max0 - min0 + 1
                             row.s0 = s0
                             row.f0 = min0.toString().padStart(config.SEQ_LEN, '0')
                             row.t0 = max0.toString().padStart(config.SEQ_LEN, '0')
                         }
                     }
                     
                     //toncuoi
                     if (maxu > 0) {
                         if (s0 > su) {
                             row.s2 = s0 - su
                             row.f2 = (maxu + 1).toString().padStart(config.SEQ_LEN, '0')
                             if (max1 > 0) row.t2 = row.t1
                             else if (max0 > 0) row.t2 = row.t0
                         }
                     }
                     else {
                         if (max1 > 0) {
                             if(min0 > 0){
                                 row.s2=  max1 - (maxu0==0?min0:maxu0+1) + 1
                                 row.f2 = (maxu0==0?min0:maxu0+1).toString().padStart(config.SEQ_LEN, '0')
                                 row.t2 = row.t1
                             }else{
                                 row.s2=  max1 - min1 + 1
                                 row.f2 = min1.toString().padStart(config.SEQ_LEN, '0')
                                 row.t2 = row.t1
                             }
                           
                         }
                         else if (max0 > 0) {
                             row.s2 = s0
                             row.f2 = row.f0
                             row.t2 = row.t0
                         }
                     }
                     //huy ph
                     if(maxc>0){
                        
                        
                         minc= (minc==0?(maxu0==0?(min0>0?min0-1:(maxu>0?(min1+maxu-1):min1-1)):maxu0):minc)+1
                         minc= (minc==0?1:minc)
                         row.countc = maxc - (minc)+1
                         row.listc = `${minc.toString().padStart(config.SEQ_LEN, '0')} - ${maxc.toString().padStart(config.SEQ_LEN, '0')}`
                         if(Number(maxc) >=Number( row.t2)){
                             row.s2=  0
                             row.f2 = ""
                             row.t2 = ""
                         }
                        
                         if ( row.used >0) row.used=row.used- row.countc

                     }else{
                        
                     }
                  //Tổng số sử dụng, xóa bỏ, mất, hủy

                   if(maxc>0)  
                   {   
                       if(maxc>maxu)
                       {
                           su = maxc - (minu==0?minc:minu) 
                           row.su = su+1
                           row.fu =(minu==0? minc.toString().padStart(config.SEQ_LEN, '0'):minu.toString().padStart(config.SEQ_LEN, '0'))
                           row.tu = maxc.toString().padStart(config.SEQ_LEN, '0')
                       }else{
                        // su = maxc - (minu==0?minc:minu) 
                        // row.su = su+1
                        //   row.fu =(minu==0? minc.toString().padStart(7, '0'):minu.toString().padStart(7, '0'))
                          // row.tu = maxc.toString().padStart(7, '0')
                       }
                    
                   }  
                   else  
                   {
                     if(maxu>0){
                       row.tu = maxu.toString().padStart(config.SEQ_LEN, '0')
                     }
                   }
                    if (typeof row.countc == "undefined") row.countc = 0
                    if (typeof row.s0 == "undefined") row.s0 = 0
                    if (typeof row.su == "undefined") row.su = 0
                    if (typeof row.used == "undefined") row.used = 0
                    if (typeof row.cancel == "undefined") row.cancel = 0
                    if (typeof row.s2 == "undefined") row.s2 = 0
                    if ((row.countc == 0) && (row.s0 == 0) && (row.su == 0) && (row.used == 0) && (row.cancel == 0) && (row.s2 == 0)) {

                    } else {
                        data.push(row)
                    }

                }
                json = { stax: taxc, sfaddr: sfaddr, now, ndate, nmonth, nyear, sname: token.on, period: `Kỳ báo cáo ${query.period}`, fd: fr, td: to, rt: rt, tn: tn, table: data }

            } else if (report == 8) {
                if (ENT == "vcm") {
                    fn = "temp/BKHHDVAT_VCM.xlsx"
                }else if(ENT == "aia"){
                    fn = "temp/BKHHDVAT_AIA.xlsx"
                }
                
                where = "where idt between @1 and @2 and stax=@3"
                binds = [fd, td, taxc]
                let ij = 4
                if (type !== "*") {
                    where += ` and type=@${ij++}`
                    binds.push(type)
                }

                where += where_ft
                where += ` and status = 4`
                if (ENT == "vcm"){
                    sql = `select FORMAT(idt,'dd/MM/yyyy') idt,form,serial,seq,btax,bname,buyer,extra extra,canceller canceller,FORMAT(cdt,'dd/MM/yyyy') cdt,ou,stax,cde from s_inv ${where} order by idt,form,serial,seq`
                }
                else if (ENT == "aia"){
                    sql = `select FORMAT(idt,'dd/MM/yyyy') idt,form,serial,seq,btax,bname,buyer,TRY_CAST(JSON_VALUE ([doc], '$.canceller') as [nvarchar](500)) canceller,FORMAT(cdt,'dd/MM/yyyy') cdt,ou,stax,JSON_VALUE ([doc], '$.cancel.rea') cde from s_inv ${where} order by idt,form,serial,seq`
                }
                result = await dbs.query(sql, binds)
                rows = result.recordset
                let i = 0
                for (const row of rows) {
                    row.index = ++i
                    try {
                        let invou = await ous.obid(row.ou)
                        row.ou = invou.name
                    }
                    catch (err) {
                        row.ou = ``
                    }
                    row.bname = (row.bname != null && row.bname != "") ? row.bname : row.buyer
                }

                json = { stax: taxc, sname: token.on, sfaddr: sfaddr, fd: fr, td: to, totalinv: i, table: rows }
            } else if (report == 9) {
                fn = "temp/BKDCHDVAT_VCM.xlsx"
                where = "where idt between @1 and @2 and stax=@3"
                binds = [fd, td, taxc]
                let ij = 4
                if (type !== "*") {
                    where += ` and type=@${ij++}`
                    binds.push(type)
                }

                where += where_ft
                where += ` and adjtyp = 2 and status = 3`

                sql = `select pid, FORMAT(idt, 'dd/MM/yyyy') idt,form,serial,seq,btax,bname,buyer,extra extra,canceller canceller,FORMAT(dt,'dd/MM/yyyy') cdt,ou,stax,sumv,vatv,c6 sec,baddr,uc, case when adjtyp in (1,2) then adjdes else note end adjdes from s_inv ${where} order by idt,form,serial,seq`
                result = await dbs.query(sql, binds)
                rows = result.recordset
                let rowbc = []
                let i = 0

                //Lấy danh sách các hóa đơn điều chỉnh
                for (const row of rows) {

                    try {
                        let invou = await ous.obid(row.ou)
                        row.ou = invou.name
                    }
                    catch (err) {
                        row.ou = ``
                    }

                    let sqlu = `SELECT name name FROM s_user where id = @1`  
                    
                    let resultu = await dbs.query(sqlu, [row.uc])
                    let rowsu = resultu.recordset
                    if (rowsu.length != 0) {
                        row.uc = rowsu[0].name
                    }

                    row.bname = (row.bname != null && row.bname != "") ? row.bname : row.buyer
                    let vpid = row.pid.split(",")
                    //Tìm hóa đơn gốc bị điều chỉnh
                    for (const pid of vpid) {
                        let sqlroot = `select FORMAT(idt, 'dd/MM/yyyy') idt,form,serial,seq,sumv,vatv from s_inv where id = @1`
                        let resultroot = await dbs.query(sqlroot, [pid])
                        let rowsroot = resultroot.recordset
                        if (rowsroot.length > 0) {
                            i++
                            let rowdc = { pid: row.pid, idt: row.idt, form: row.form, serial: row.serial, seq: row.seq, btax: row.btax, bname: row.bname, buyer: row.buyer, extra: row.extra, canceller: row.canceller, cdt: row.cdt, ou: row.ou, stax: row.stax, sumv: row.sumv, vatv: row.vatv, sec: row.sec, baddr: row.baddr, uc: row.uc, adjdes: row.adjdes }

                            rowdc.rootform = rowsroot[0].form
                            rowdc.rootserial = rowsroot[0].serial
                            rowdc.rootseq = rowsroot[0].seq
                            rowdc.rootidt = rowsroot[0].idt
                            rowdc.rootsumv = rowsroot[0].sumv
                            rowdc.rootvatv = rowsroot[0].vatv

                            if (String(rowdc.adjdes).includes("giảm")) 
                            {
                                rowdc.sumv = (util.isNumber(rowdc.sumv) ? ((-1)*rowdc.sumv) : 0)
                                rowdc.vatv = (util.isNumber(rowdc.vatv) ? ((-1)*rowdc.vatv) : 0)
                            } else if (String(rowdc.adjdes).includes("tăng")) 
                            {
                                rowdc.sumv = (util.isNumber(rowdc.sumv) ? (rowdc.sumv) : 0)
                                rowdc.vatv = (util.isNumber(rowdc.vatv) ? (rowdc.vatv) : 0)
                            }

                            rowdc.index = i
                            rowbc.push(rowdc)
                        }

                    }
                }

                json = { stax: taxc, sname: token.on, sfaddr: sfaddr, fd: fr, td: to, totalinv: i, table: rowbc }
            } else if (report == 10) {
                fn = "temp/BKTTHDVAT_VCM.xlsx"
                where = "where idt between @1 and @2 and stax=@3"
                binds = [fd, td, taxc]
                let ij = 4
                if (type !== "*") {
                    where += ` and type=@${ij++}`
                    binds.push(type)
                }

                where += where_ft
                where += ` and adjtyp = 1 and status = 3`

                sql = `select pid, FORMAT(idt, 'dd/MM/yyyy') idt,form,serial,seq,btax,bname,buyer,extra extra,canceller canceller,FORMAT(dt,'dd/MM/yyyy') cdt,ou,stax,sumv,vatv,c6 sec,baddr,uc from s_inv ${where} order by idt,form,serial,seq`
                result = await dbs.query(sql, binds)
                rows = result.recordset
                let i = 0
                for (const row of rows) {
                    row.index = ++i
                    try {
                        let invou = await ous.obid(row.ou)
                        row.ou = invou.name
                    }
                    catch (err) {
                        row.ou = ``
                    }

                    let sqlu = `SELECT name name FROM s_user where id = @1`  
                    
                    let resultu = await dbs.query(sqlu, [row.uc])
                    let rowsu = resultu.recordset
                    if (rowsu.length != 0) {
                        row.uc = rowsu[0].name
                    }

                    row.bname = (row.bname != null && row.bname != "") ? row.bname : row.buyer
                    let sqlroot = `select FORMAT(idt, 'dd/MM/yyyy') idt,form,serial,seq,sumv,vatv from s_inv where id = @1`
                    let resultroot = await dbs.query(sqlroot, [row.pid])
                    let rowsroot = resultroot.recordset
                    if (rowsroot.length > 0) {
                        row.rootform = rowsroot[0].form
                        row.rootserial = rowsroot[0].serial
                        row.rootseq = rowsroot[0].seq
                        row.rootidt = rowsroot[0].idt
                        row.rootsumv = rowsroot[0].sumv
                        row.rootvatv = rowsroot[0].vatv
                    }
                }

                json = { stax: taxc, sname: token.on, sfaddr: sfaddr, fd: fr, td: to, totalinv: i, table: rows }
            } else if (report == 11) {
                fn = "temp/BKHDDTCQT_VCM.xlsx"
                where = "where idt between @1 and @2 and stax=@3"
                binds = [fd, td, taxc]
                let ij = 4
                if (type !== "*") {
                    where += ` and type=@${ij++}`
                    binds.push(type)
                }

                where += where_ft
                where += ` and status = 3`

                sql = `select form,serial,seq,FORMAT(idt, 'dd/MM/yyyy') idt,btax,bname,buyer,JSON_QUERY(doc,'$.items') items, note from s_inv ${where} order by idt,form,serial,seq`
                result = await dbs.query(sql, binds)
                rows = result.recordset

                let rowbc = []
                let i = 0

                //Lấy danh sách các hóa đơn điều chỉnh
                for (const row of rows) {
                    let items = JSON.parse(row.items)

                    //Lấy danh sách các item hóa đơn
                    for (const item of items) {
                        i++
                        let rowdc = item
                        rowdc.formserial = `${row.form} - ${row.serial}`
                        rowdc.seq = row.seq
                        rowdc.idt = row.idt
                        rowdc.bname = (row.bname != null && row.bname != "") ? row.bname : row.buyer
                        rowdc.btax = row.btax
                        rowdc.itemname = item.name
                        rowdc.itemunit = item.unit
                        rowdc.itemquantity = item.quantity
                        rowdc.itemamount = item.amount
                        rowdc.itemvrn = item.vrn
                        rowdc.itemvat = item.vat
                        rowdc.itemtotal = item.total
                        rowdc.itemnote = row.note

                        rowdc.index = i
                        rowbc.push(rowdc)
                    }

                }

                json = { stax: taxc, sname: token.on, firstlist: firstlist, updatelist: updatelist, sfaddr: sfaddr, fd: fr, td: to, table: rowbc }
            } else if (report == 14){ // Báo cáo giảm doanh thu aia
                // sql = Select * from INVOICE_DETAIL_BASELINE where IS_DEDUCTION = ‘Y’ AND POS_REMARK is null OR POS_REMARK not in ('I', 'D', 'N') AND PROCESS_DATE = “selected date”
                fn = "temp/BCGDT_AIA.xlsx"
                where = "where IS_DEDUCTION = 'Y' AND (POS_REMARK is null OR POS_REMARK not in ('I', 'D', 'N')) AND PROCESS_DATE between @1 AND @2"
                let pfd = fm.startOf("day").format("YYYY-MM-DD HH:mm:ss"), ptd = fm.endOf("day").format("YYYY-MM-DD HH:mm:ss")

                binds = [pfd, ptd]
                
                let IntegrationConfigDB = config.poolIntegrationConfig.database

                sql = `select POLICY_NUMBER,PO_NAME,ADDRESS,PREMIUM,TRANSACTION_CODE,OLAS_TRANS_CODE,ACCOUNTING_CODE,CAMPAIGN_CODE,TAX_CODE,RECEIVE_INVOICE,POLICY_DURATION, FORMAT(EFFECTIVE_DATE,'dd/MM/yyyy') EFFECTIVE_DATE,FORMAT(EFFECTIVE_END_DATE,'dd/MM/yyyy') EFFECTIVE_END_DATE,INVOICE_CONTENT,POS_REMARK from ${IntegrationConfigDB}.dbo.INVOICE_DETAIL_BASELINE ${where}`
                
                result = await dbs.query(sql, binds)
                rows = result.recordset
                let i = 0
                for (const row of rows) {
                    row.index = ++i
                }
                json = { stax: taxc, sname: token.on, fd: fr, rt: rt, tn: tn, table: rows }

            }

            if(report != 1 || report ==1 &&ENT=='vcm'){
                const file = path.join(__dirname, "..", "..", "..", fn)
                const xlsx = fs.readFileSync(file)
                const template = new xlsxtemp(xlsx)
                template.substitute(1, json)
                res.end(template.generate(), "binary")
            }
            
        } catch (err) {
            next(err)
        }
    }
}
module.exports = Service