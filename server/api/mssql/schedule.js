"use strict"
const { Console } = require("console")
const moment = require("moment")
var uuid = require('uuid');
const dbs = require("./dbs")
const exch = require("./exch")
const fxp = require("fast-xml-parser")
const Parser = fxp.j2xParser
const Json2csvParser = require("json2csv").Parser;
const inv = require("./inv")
const util = require("../util")
const ftp = require("../ftp")
const email = require("../email")
const config = require("../config")
const logger4app = require("../logger4app")
const objectMapper = require("object-mapper")
const sms = require("../sms")
const ads = require("../ads")
const groups = require("./groups")
const redis = require("../redis")
const tvan = require("../tvan")
const path = require("path")
const fs = require("fs")
const dtf = config.dtf
const xlsxtemp = require("xlsx-template")
const sql_del_exp = {
    sql_del_exp_mssql: `DELETE FROM s_listvalues WHERE keyname=@1 AND getdate() > TRY_CAST(keyvalue as datetime)`,
    sql_del_exp_mysql: `DELETE FROM s_listvalues WHERE keyname=?`,
    sql_del_exp_orcl: `DELETE FROM s_listvalues WHERE keyname=:1 AND SYSDATE > TO_DATE(keyvalue, 'YYYY-MM-DD HH24:MI:SS')`,
    sql_del_exp_pgsql: `DELETE FROM s_listvalues WHERE keyname=$1`,
}
const sql_del = {
    sql_del_mssql: `DELETE FROM s_listvalues WHERE keyname=@1`,
    sql_del_mysql: `DELETE FROM s_listvalues WHERE keyname=?`,
    sql_del_orcl: `DELETE FROM s_listvalues WHERE keyname=:1`,
    sql_del_pgsql: `DELETE FROM s_listvalues WHERE keyname=$1`,
}
const sql_set_ins = {
    sql_set_ins_mssql: `INSERT INTO s_listvalues (keyname, keyvalue, keytype) VALUES(@1, @2, 'String')`,
    sql_set_ins_mysql: `INSERT INTO s_listvalues (keyname, keyvalue, keytype) VALUES(?, ?, 'String')`,
    sql_set_ins_orcl: `INSERT INTO s_listvalues (keyname, keyvalue, keytype) VALUES(:1, :2, 'String')`,
    sql_set_ins_pgsql: `INSERT INTO s_listvalues (keyname, keyvalue, keytype) VALUES($1, $2, 'String')`,
}
function convertHTML(str) {
    // &colon;&rpar;
    var Regcheck = /\W\s/gi;
    var htmlListObj = {
        //'&': "&amp;",
        //'"': "&quot;",
        //"'": "&apos;",
        "<": "&lt;",
        ">": "&gt;"
    };

    var a = str.split("");
    var b = [];


    for (var i in a) {

        if (htmlListObj.hasOwnProperty(a[i])) {
            b.push(htmlListObj[a[i]]);
        } else { b.push(a[i]); }
    }

    var c = b.join("");

    return c;

}
  
const MAP_CTBC_MAIL = {
    //user
    "ic":"BSNS_JournalSeqNO",
    "bcode":"BSNS_CustomerID",
    "bname":"BSNS_CustomerName",
    "idt":"BSNS_TxnDate",
    "bmail":"BSNS_ContactAddress"
}

const scb_mail = (config.ent == "scb") ? {
    "sendEmailRequest": {
       "scb_header": {
          "scb_messageDetails": {
             "scb_messageVersion": `${config.mdis.scb_messageVersion}`,
             "scb_messageType": {
                "scb_typeName": `${config.mdis.scb_typeName}`,
                "scb_subType": {
                   "scb_subTypeName": `${config.mdis.scb_subTypeName}`
                }
             }
          },
          "scb_originationDetails": {
             "scb_messageSender": {
                "scb_messageSender": `${config.mdis.scb_messageSender}`,
                "scb_senderDomain": {
                   "scb_domainName": `${config.mdis.scb_domainName}`, 
                   "scb_subDomainName": {
                      "scb_subDomainType": `${config.mdis.scb_subDomainType}`
                   }
                },
                "scb_countryCode": `${config.mdis.scb_countryCode}`
             },
             "scb_messageTimestamp": "2020-06-24T16:39:03.410+08:00", //Edit
             "scb_initiatedTimestamp": "2020-06-24T16:39:03.410+08:00", //Edit
             "scb_trackingId": "3cc2adc1-f733-460f-b81e-e395713f2e26", //Edit
             "scb_possibleDuplicate": `${config.mdis.scb_possibleDuplicate}`
          },
          "scb_captureSystem": `${config.mdis.scb_captureSystem}`
       },
       "scb_sendEmailReqPayload": {
          "scb_payloadFormat": "XML",
          "scb_payloadVersion": `${config.mdis.scb_payloadVersion}`,
          "ext1_sendEmailReq": {
             "ext1_communicationInfo": {
                "ext1_uniqueSenderReference": "3cc2adc1-f733-460f-b81e-e395713f2e26", //Edit
                "ext1_dateTimeStamp": "2020-06-24T16:39:03.410+08:00", //Edit
                "ext1_DestinationChannelInfo": {
                   "ext1_Email": {
                      "ext1_toAddress": {
                         "ext1_toEmailID": "Robertson.Jesuraj@sc.com"
                      }, //Edit
                      "ext1_ccAddress": [
                         {
                            "ext1_ccEmailID": "SurendraBabu.Ravella@sc.com"
                         },
                         {
                            "ext1_ccEmailID": "Sreenath.Soman@sc.com"
                         }
                      ], //Edit
                      "ext1_senderAddress": "no.reply.my@sc.com", //Edit
                      "ext1_messageSubject": "", //Edit
                      "ext1_messageContent": "", //Edit and Encode Base 64
                      "ext1_mimeType": "",
                      "ext1_language": `${config.mdis.ext1_language}`,
                      "ext1_enableTemplateFlag": `${config.mdis.ext1_enableTemplateFlag}`,
                      "ext1_utfIndicator": `${config.mdis.ext1_utfIndicator}`,
                      "ext1_attachmentEncryptionPassPhrase": `${config.mdis.ext1_attachmentEncryptionPassPhrase}`,
                      "ext1_mulitpleRecepientsIndicator": `${config.mdis.ext1_mulitpleRecepientsIndicator}`
                   }
                },
                "ext1_Attachments": [ //Edit
                   
                ]
             }
          }
       },
    }
 } : {}
const Service = {
    delInvNotSeq: async (req, res, next) => {
        // try {
        //     let sql, result, binds
        //     sql = `delete from s_inv a where  status=1 and idt < @1 
        //     and exists(select 1 from s_serial where a.stax=taxc and a.form=form and a.serial=serial and uses=@2)`
        //     let yestday = moment(new Date).subtract(1,"days").endOf("day").toDate()
        //     binds = [yestday,1]
        //     result = await dbs.query(sql, binds)
        //     // result = await dbs.query(sql, binds)
        //     if(result.rowsAffected[0]) logger4app.debug("Đã xóa", result.rowsAffected[0])   
        // } catch (err) {
        //     next(err)
        // }
    },
    // sendEmailApprInv : async (req, res, next) => {
    //     // try {
    //     //     let sql, result, binds
    //     //     sql = `delete from s_inv a where  status=1 and idt < ? 
    //     //     and exists(select 1 from s_serial where a.stax=taxc and a.form=form and a.serial=serial and uses=?)`
    //     //     let yestday = moment(new Date).subtract(1,"days").endOf("day").toDate()
    //     //     binds = [yestday,1]
    //     //     result = await dbs.query(sql, binds)
    //     //     // result = await dbs.query(sql, binds)
    //     //     if(result.affectedRows) logger4app.debug("Đã xóa", result.affectedRows)   
    //     // } catch (err) {
    //     //     next(err)
    //     // }
    // },
    sendEmailApprInv : async (pkey, ptimeout) => {
        //Goi ham check va update trang thai job dang chay hay khong
        if (config.disable_worker) return
        let vcheck = await Service.UpdateRunningJob(pkey, 0, ptimeout)
        logger4app.debug('sendEmailApprInv','vcheck',vcheck)
        //Neu co job dang chay (vcheck = 0) thi thoat ra, khong thuc hien lenh tiep theo
        if (!vcheck) return
        try {
            let sql, result, binds, rows, subSQL, subRS, subROWS
            sql = `SELECT id, invid, doc, dt, status, xml FROM s_invdoctemp WHERE status = 0 order by id, dt OFFSET 0 ROWS FETCH NEXT ${config.MAX_ROW_JOB_GET} ROWS ONLY `
            binds = []
            result = await dbs.query(sql, binds)
            rows = result.recordset
            for (const row of rows) {
                try {
                    let doc = JSON.parse(row.doc), xml = row.xml 
                    if (row.invid == 0) {
                        //Voi truong hop invid = 0 thi gui mail luon, noi dung gui mail la truong doc da insert vao
                        await email.sendfromdoctemp(JSON.stringify(doc))
                    } else {//Neu invid khac 0 thi dung ham gui mail hoa don
                        //Check xem co du thong tin hoa don khong, neu khong du thi truy van bang s_inv lay thong tin hoa don
                        const vcheckinvinfo = (doc.stax) && (doc.form) && (doc.serial) && (doc.seq) && (doc.status) && (doc.bmail)
                        if (!vcheckinvinfo) doc = await inv.invdocbid(row.invid)
                        //Quét để FTP hoặc gửi mail
                        if (config.useFtpAttach && !['ctbc', 'scb'].includes(config.ent)) {
                            await ftp.uploadattach(doc, xml)
                        }
                        if (!util.isEmpty(doc.bmail)) {
                            //Cập nhật ngày và trạng thái gửi mail
                            if (!['ctbc', 'scb'].includes(config.ent)) {
                                let mailstatus = await email.senddirectmail(doc, xml)
                                if (mailstatus) await inv.updateSendMailStatus(doc)
                            } else if (config.ent == 'ctbc') {
                                //Nếu là CTBC thì lấy dữ liệu mail để build XML
                                let arrmail = await email.senddirectmail(doc, xml)
                                for (let mail of arrmail) {
                                    let xmlmail = 'abc'
                                    let BlueStar = objectMapper(doc, MAP_CTBC_MAIL)
                                    //Gán giá trị mặc định cho object
                                    BlueStar.BSNS_TelegramSource = `VN-EVAT`
                                    BlueStar.BSNS_ProductID = `VX`
                                    BlueStar.BSNS_NotificationClassID = `01`
                                    BlueStar.BSNS_IsBypassBSNS = `1`
                                    BlueStar.BSNS_IsBypassMMAS = `Y`
                                    const vnum = `${moment(new Date()).format('YYYYMMDD')}${moment(new Date()).format('HHmmssSSS')}`
                                    BlueStar.BSNS_JournalSeqNO = `EVAT${vnum}`
                                    BlueStar.NotificationTitle = `增值稅電子化發票通知(Value Added Tax Electronic Invoices Notice)`
                                    let dt = BlueStar.BSNS_TxnDate
                                    BlueStar.BSNS_TxnDate = moment(dt).format('YYYYMMDD')
                                    BlueStar.BSNS_SentDate = BlueStar.BSNS_TxnDate
                                    BlueStar.BSNS_SentTime = moment(dt).format('HHmmss')
                                    BlueStar.BSNS_ContactWindowSet = {}
                                    BlueStar.BSNS_ContactWindowSet.BSNS_ContactWindow = {}
                                    BlueStar.BSNS_ContactWindowSet.BSNS_ContactWindow.BSNS_Language = `vi-VN`
                                    BlueStar.BSNS_ContactWindowSet.BSNS_ContactWindow.BSNS_ContactName = doc.bname
                                    BlueStar.BSNS_ContactWindowSet.BSNS_ContactWindow.BSNS_DeliveryChannel = `N`
                                    BlueStar.BSNS_ContactWindowSet.BSNS_ContactWindow.BSNS_ContactAddress = doc.bmail
                                    //Gán giá trị content
                                    BlueStar.ContentXML = {}
                                    //BlueStar.ContentXML.Content = "<![CDATA[" + String(JSON.stringify(mail.html)) + "]]"
                                    BlueStar.ContentXML.Content = convertHTML(mail.html)
                                    //BlueStar.ContentXML.Content = convertHTML(BlueStar.ContentXML.Content)
                                    if (mail.attachments && mail.attachments.length > 0) {
                                        //Gán giá trị attach
                                        BlueStar.Attachments = []
                                        let i = 0

                                        for (let attach of mail.attachments) {
                                            BlueStar.Attachments.push({ FileName: attach.filename, Type: attach.filetype, Index: ++i, contentatach: attach.content })
                                        }
                                    }
                                    const parser = new Parser()
                                    xmlmail = parser.parse(BlueStar, {
                                        ignoreAttributes: false, attrValueProcessor: a => he.decode(a, { isAttributeValue: true }),
                                        tagValueProcessor: a => he.decode(a)
                                    })//.replace(/&/g, "&amp;")
                                    if (mail.attachments && mail.attachments.length > 0) {
                                        let i = 0
                                        for (let attach of mail.attachments) {
                                            ++i
                                            logger4app.debug(`<FileName>${attach.filename}</FileName><Type>${attach.filetype}</Type><Index>${i}</Index><contentatach>`)
                                            xmlmail = xmlmail.replace(`<FileName>${attach.filename}</FileName><Type>${attach.filetype}</Type><Index>${i}</Index><contentatach>`, `<Attachment FileName="${attach.filename}" Type="${attach.filetype}" Index="${i}">`)
                                        }
                                        xmlmail = xmlmail.replace('</contentatach>', '</Attachment>')
                                    }
                                    xmlmail = `<?xml version="1.0" encoding="UTF-8" standalone="no"?><BlueStar>${xmlmail}</BlueStar>`
                                    await ftp.uploadmailctbc(doc, xmlmail)
                                }

                            } else {
                                //Tạo email
                                let arrmail = await email.senddirectmail(doc, xml)
                                for (let mail of arrmail) {
                                    for (let tomail of mail.to) {
                                        let xmlmail = 'abc'
                                        //let mailobj = scb_mail
                                        //Sửa header
                                        let vuuid = uuid.v4()
                                        scb_mail.sendEmailRequest.scb_header.scb_originationDetails.scb_messageTimestamp = new Date().toISOString()
                                        scb_mail.sendEmailRequest.scb_header.scb_originationDetails.scb_initiatedTimestamp = new Date().toISOString()
                                        scb_mail.sendEmailRequest.scb_header.scb_originationDetails.scb_trackingId = vuuid //uuid.v4()

                                        //Sửa nội dung mail
                                        scb_mail.sendEmailRequest.scb_sendEmailReqPayload.ext1_sendEmailReq.ext1_communicationInfo.ext1_uniqueSenderReference = vuuid //uuid.v4()
                                        scb_mail.sendEmailRequest.scb_sendEmailReqPayload.ext1_sendEmailReq.ext1_communicationInfo.ext1_dateTimeStamp = new Date().toISOString()
                                        //To mail
                                        scb_mail.sendEmailRequest.scb_sendEmailReqPayload.ext1_sendEmailReq.ext1_communicationInfo.ext1_DestinationChannelInfo.ext1_Email.ext1_toAddress.ext1_toEmailID = tomail
                                        //CC Mail
                                        let ext1_ccAddress = []
                                        for (const ccmail of mail.cc) {
                                            ext1_ccEmailID = ccmail
                                            ext1_ccAddress.push({ ext1_ccEmailID: ext1_ccEmailID })
                                        }
                                        scb_mail.sendEmailRequest.scb_sendEmailReqPayload.ext1_sendEmailReq.ext1_communicationInfo.ext1_DestinationChannelInfo.ext1_Email.ext1_ccAddress = ext1_ccAddress
                                        //Sender
                                        scb_mail.sendEmailRequest.scb_sendEmailReqPayload.ext1_sendEmailReq.ext1_communicationInfo.ext1_DestinationChannelInfo.ext1_Email.ext1_senderAddress = config.mdis.senderAddress
                                        //Content Mail
                                        scb_mail.sendEmailRequest.scb_sendEmailReqPayload.ext1_sendEmailReq.ext1_communicationInfo.ext1_DestinationChannelInfo.ext1_Email.ext1_messageSubject = "#HUNGLQ#subject#HUNGLQ#" //Buffer.from(mail.subject, 'utf8').toString('base64')
                                        scb_mail.sendEmailRequest.scb_sendEmailReqPayload.ext1_sendEmailReq.ext1_communicationInfo.ext1_DestinationChannelInfo.ext1_Email.ext1_messageContent = "#HUNGLQ#content#HUNGLQ#" //Buffer.from(mail.html, 'utf8').toString('base64')

                                        //Attachments
                                        if (mail.attachments && mail.attachments.length > 0) {
                                            //Gán giá trị attach
                                            scb_mail.sendEmailRequest.scb_sendEmailReqPayload.ext1_sendEmailReq.ext1_communicationInfo.ext1_Attachments = []

                                            for (let attach of mail.attachments) {
                                                scb_mail.sendEmailRequest.scb_sendEmailReqPayload.ext1_sendEmailReq.ext1_communicationInfo.ext1_Attachments.push({ ext1_messageAttachmentName: attach.filename, ext1_messageAttachmentType: `application/${attach.filetype}`, ext1_messageAttachmentContent: attach.content })
                                            }
                                        }

                                        const parser = new Parser()
                                        xmlmail = parser.parse(scb_mail.sendEmailRequest, { ignoreAttributes: false, attrValueProcessor: a => he.decode(a, { isAttributeValue: true }), tagValueProcessor: a => he.decode(a) }).replace(/&/g, "&amp;")
                                        xmlmail = String(xmlmail).split("scb_").join("scb:")
                                        xmlmail = String(xmlmail).split("ext1_").join("ext1:")
                                        xmlmail = String(xmlmail).split("#HUNGLQ#subject#HUNGLQ#").join(Buffer.from(mail.subject, 'utf8').toString('base64'))
                                        let b64content = Buffer.from(mail.html, 'utf8').toString('base64')
                                        let tmpxmlmail = String(xmlmail).split("#HUNGLQ#content#HUNGLQ#")
                                        xmlmail = tmpxmlmail[0] + b64content + tmpxmlmail[1]
                                        xmlmail = `<sendEmailRequest xmlns:scb="http://www.sc.com/SCBML-1" xmlns:ext1="http://www.sc.com/GroupFunctions/Communications/v3/ExternalCommunication">${xmlmail}</sendEmailRequest>`

                                        //Đút vào bảng s_solace_msg_temp
                                        let sqltempsolace = `INSERT INTO s_solace_msg_temp (topic_name, queue_name, msg_content, inv_id, inv_doctemp_id, co_id) VALUES(@1, @2, @3, @4, @5, @6)`
                                        let topicsolace = ""
                                        if (doc.status == 3) {
                                            topicsolace = `${config.mdis.root_topic}mailapprove`
                                        } else if (doc.status == 4) {
                                            topicsolace = `${config.mdis.root_topic}mailcancel`
                                        }
                                        let bindstempsolace = [topicsolace, '', xmlmail, doc.id, row.id, vuuid]
                                        await dbs.query(sqltempsolace, bindstempsolace)

                                    }
                                }
                            }

                        }
                        if (!util.isEmpty(doc.btel)) {
                            //Cập nhật ngày và trạng thái gửi sms
                            let smsstatus = await sms.smssend(doc)
                            if (smsstatus) await inv.updateSendSMSStatus(doc, smsstatus)
                        }
                    }
                    //Cập nhật trạng thái bảng tạm s_invdoctemp
                    sql = `UPDATE s_invdoctemp SET status=1, iserror = @1, error_desc = @2 WHERE id=@3`
                    binds = [0, '', row.id]
                    await dbs.query(sql, binds)

                } catch (err) {
                    let err_mes = `sendEmailApprInv ${row.id}: ${err}` 
                    logger4app.debug(err_mes)
                    //Cập nhật trạng thái bảng tạm trường hợp xử lý lỗi
                    sql = `UPDATE s_invdoctemp SET status=1, iserror = @1, error_desc = @2 WHERE id=@3`
                    binds = [1, err_mes, row.id]
                    await dbs.query(sql, binds)
                }
            }
            
        } catch (err) {
            logger4app.debug(`sendEmailApprInv `,err)
        }
        vcheck = await Service.UpdateRunningJob(pkey, 1)
    },
    delTempDocMail : async (req, res, next) => {
        try {
            let sql, result, binds, rows
            sql = `DELETE FROM s_invdoctemp WHERE dt <= DATEADD(day, -${config.MAX_DATE_TO_KEEP_TEMP_ROW}, GETDATE()) and status = 1 and iserror = 0`
            binds = []
            await dbs.query(sql, binds)
        } catch (err) {
            logger4app.debug(`delTempDocMail `,err)
        }
    },
    syncExchangeRate : async (req, res, next) => {
        try {
            let status, message, resultexch
            try {
               var date =(moment(new Date()).startOf("day")).add(-1, 'days').format("YYYY-MM-DD HH:mm:ss")
                resultexch = await dbs.exch(date)
                const rows = resultexch.recordset
                logger4app.debug('syncExchangeRate____'+ resultexch.recordset.length)
                await exch.sync(rows,date)
                
                status = 1
                message = 'Success'
            } catch (e) {
                //dbs.getConn()
                status = 2
                throw e
            }

        } catch (err) {
            logger4app.debug(`syncExchangeRate `, err.message)
        }
    },
    syncUsersFromOUD: async () => {
        try {
            ads.getUserFromOUD()
        } catch (e) {
            logger4app.debug(`syncUsersFromOUD : `)
            logger4app.debug(e)
        }
    },
    socD: async () => {
        try {
            let result, records
            let date =  moment(new Date()).format("YYYY-MM-DD")
            let idtdate =  moment(new Date()).format("YYYYMMDD")
            let sql = `select 'VN-EVAT' System_Code, user_id "Login_Account_Nbr", FORMAT(dt,'yyyy-MM-dd HH:mm:ss') "Query_Datetime", fnc_id "AP_Txn_Code", fnc_url "Server_Name", NULL "User_Terminal", NULL "AP_Account_Nbr", NULL "Txn_Type_Code", REPLACE(dtl, ',', ' ' ) "Statement_Text", NULL "Object_Name", NULL "Txn_Status_Code", NULL "Customer_Id", NULL "Account_Nbr", NULL "Branch_Nbr", NULL "Role_Id", 'VN-EVAT_APLOG' "Import_Source", '${idtdate}' "As_Of_Date" from s_sys_logs ssl  where dt >= '${date}' order by FORMAT(dt,'yyyyMMdd') DESC`
            result = await dbs.query(sql);
            records = result.recordset
            if (records.length <= 0) return
            const jsonData = JSON.parse(JSON.stringify(records));
            const json2csvParser = new Json2csvParser({ header: false, default: '', quote: '', encoding: 'utf8' });
            const csv = json2csvParser.parse(jsonData);
            let checkdh = 0
            await ftp.uploadsocctbcDH(csv, checkdh)
            
        } catch (err) {
            logger4app.debug(err);
        }
    },
    socH: async () => {
        try {
            let result, records
            let date =  moment(new Date()).format("YYYY-MM-DD")
            let idtdate =  moment(new Date()).format("YYYYMMDD")
            let sql = `select concat('VN-EVAT_',FORMAT(dt,'yyyyMMdd')) "System_Code", count(dtl) "totaldtl", '${idtdate}' "Query_Datetime" from s_sys_logs ssl where dt >= '${date}' group by FORMAT(dt, 'yyyyMMdd')`
            result = await dbs.query(sql);
            records = result.recordset
            if (records.length <= 0) return
            const jsonData = JSON.parse(JSON.stringify(records));
            const json2csvParser = new Json2csvParser({ header: false, default: '', quote: '', encoding: 'utf8' });
            const csv = json2csvParser.parse(jsonData);
            let checkdh = 1
            await ftp.uploadsocctbcDH(csv, checkdh)
        } catch (err) {
            logger4app.debug(err);
        }
    },
    SCO2_user: async () => {
        try {
            let binds, result, records
            let where = `where uc = 1 `
            let sql = `select 'VN-EVAT' System_code, s.name "Name", s.id "Login_Account_Nbr", gr.NAME "Name Gruop", format(last_login, 'yyyyMMdd') "Last_Login_Date" from s_user s inner join s_group_user h on s.id = h.user_id inner join s_group gr on group_id = gr.id ${where} group by code, s.id, group_id, last_login, gr.name, s.name order by s.id`
            result = await dbs.query(sql, binds);
            records = result.recordset
            const jsonData = JSON.parse(JSON.stringify(records));
            const json2csvParser = new Json2csvParser({ header: false, default: '', quote: '', encoding: 'utf8' });
            const csv = json2csvParser.parse(jsonData);
            let check = 2
            await ftp.uploadsocctbcACCGR(csv, check)
        } catch (err) {
            logger4app.debug(err);
        }
    },
    SCO2_group: async () => {
        try {
            let binds, result, records
            let where = `where 1 = 1`
            let sql = `select 'VN-EVAT' System_code, s.name "UserGroup_Name", s.des "DES", rl.name "Function1ID_Name", rl.name "Function2ID_Name", null "blank", null "blank1" from s_group s inner join s_group_role grl on s.id = grl.group_id inner join s_role rl on grl.role_id = rl.id and rl.active = 1 ${where} group by s.name, s.id,grl.ROLE_ID, rl.name,s.des`
            result = await dbs.query(sql, binds);

            records = result.recordset
            if (records == undefined || records.length == 0) {
                throw new Error("Group rỗng không có dữ liệu")
            }
            const jsonData = JSON.parse(JSON.stringify(records));
            const json2csvParser = new Json2csvParser({ header: false, default: '', quote: '', encoding: 'utf8' });
            const csv = json2csvParser.parse(jsonData);
            let check = 3
            await ftp.uploadsocctbcACCGR(csv, check)
        } catch (err) {
            logger4app.debug(err);
        }
    },
    AUMSER1: async () => {
        try {
            let result, records, fd, td
            let vdt_redis = await redis.get('AUMSER.EINVOICE.Audit')
            if (vdt_redis) {

                fd = moment(vdt_redis, dtf, true)
                if (!fd.isValid()) {
                    fd.setDate(fd.getDate() - 30)
                } else {
                    fd = fd.format(dtf)
                }
            } else {
                fd = new Date(); // today!
                fd.setDate(fd.getDate() - 30)
            }
            td = (moment(new Date()).format(dtf))
            let sql = `SELECT u.id "Audit_id",
            u.name "Audit_name",
            IIF(s.msg_status='2', 'FAILED', 'SUCCESS') "Audit_Status",
            'VN' "Audit_Country",
            ' ' "Access_SourceHost",
            s.ip "Access_SourceIP",
            FORMAT (u.last_login, 'dd/MM/yyyy HH:mm:ss') "Audit_LoginTimeStamp" 
            FROM s_user u, s_sys_logs s 
            WHERE u.id = s.user_id and dt BETWEEN @1 AND @2 AND user_id NOT like '[__]%'`
            result = await dbs.query(sql,[fd,td]);
            records = result.recordset
            if (records.length <= 0) return
            const jsonData = JSON.parse(JSON.stringify(records));
            const json2csvParser = new Json2csvParser({ header: true, default: '', quote: '', delimiter: '|', encoding: 'utf8' });
            const csv = json2csvParser.parse(jsonData);
            //logger4app.debug(csv);
            let check = 1
            await ftp.uploadsplunkscb(csv,check)
            await redis.set('AUMSER.EINVOICE.Audit', moment(td).format(dtf))
        } catch (err) {
            logger4app.debug(err);
        }
    },
    AUMSER2: async () => {
        try {
            let result, records
            let sql = `SELECT
            su.id "AppID_ID",
            ' ' "AppID_Profile",
            ' ' "AppID_Name",
            ' ' "AppID_Type",
            su.id "AppID_Implemented_By",
            FORMAT (su.update_date,
            'dd/MM/yyyy HH:mm:ss') "AppID_Implemented_TimeStamp",
                'VN' "AppID_Country",
            ' ' "AppID_Group",
            case
                when su.update_date is not null then 'CREATE'
                else 'MODIFY' end "AppID_Status"
            FROM
                s_user su
            WHERE
                id NOT like '[__]%'`
            result = await dbs.query(sql);
            records = result.recordset
            for (let r of records) {
                let grname = ``, grp
                if (r.AppID_ID) {
                    grp = await groups.getgrpbyusser(r.AppID_ID)
                    for (let gr of grp) {
                        grname += gr.name + ','
                    }
                    if (grname != '') {
                        grname = grname.substring(0, grname.length - 1)
                        r.AppID_Name = grname
                    }
                }
            }
            if (records.length <= 0) return
            const jsonData = JSON.parse(JSON.stringify(records));
            const json2csvParser = new Json2csvParser({ header: true, default: '', quote: '', delimiter: '|', encoding: 'utf8' });
            const csv = json2csvParser.parse(jsonData);
            //logger4app.debug(csv);
            let check = 2
            await ftp.uploadsplunkscb(csv, check)

        } catch (err) {
            logger4app.debug(err);
        }
    },
    AUMSER3: async () => {
        try {
            let result, records, fd, td
            let vdt_redis = await redis.get('AUMSER.EINVOICE.Login')
            if (vdt_redis) {

                fd = moment(vdt_redis, dtf, true)
                if (!fd.isValid()) {
                    fd.setDate(fd.getDate() - 30)
                } else {
                    fd = fd.format(dtf)
                }
            } else {
                fd = new Date(); // today!
                fd.setDate(fd.getDate() - 30)
            }
            td = (moment(new Date()).format(dtf))
            let sql = `SELECT
            sl.[user_id] "Access_ID",
            sl.[user_name] "Access_Name",
            'WEB' "Access_Type",
            'VN' "Access_Country",
            ' ' "Access_Profile",
            ' ' "Access_SourceHost",
            sl.ip "Access_SourceIP",
            FORMAT (sl.dt,
            'dd/MM/yyyy HH:mm:ss') "Access_LoginTimeStamp",
            FORMAT (sl.dt,
            'dd/MM/yyyy HH:mm:ss') "Access_LogoutTimeStamp"
        FROM
            s_sys_logs sl
        WHERE
            sl.fnc_id = 'user_login'
            AND dt BETWEEN @1 AND @2
            AND user_id NOT like '[__]%'`
            result = await dbs.query(sql,[fd,td]);
            records = result.recordset
            for (let r of records) {
                let grname = ``, grp
                if (r.Access_ID) {
                    grp = await groups.getgrpbyusser(r.Access_ID)
                    for (let gr of grp) {
                        grname += gr.name + ','
                    }
                    if (grname != '') {
                        grname = grname.substring(0, grname.length - 1)
                        r.Access_Profile = grname
                    }
                }
            }
            if (records.length <= 0) return
            const jsonData = JSON.parse(JSON.stringify(records));
            const json2csvParser = new Json2csvParser({ header: true, default: '', quote: '', delimiter: '|', encoding: 'utf8' });
            const csv = json2csvParser.parse(jsonData);
            //logger4app.debug(csv);
            let check = 3
            await ftp.uploadsplunkscb(csv,check)
            await redis.set('AUMSER.EINVOICE.Login', moment(td).format(dtf))
        } catch (err) {
            logger4app.debug(err);
        }
    },
    AUMSER4: async () => {
        try {
            let result, records, fd, td
            let vdt_redis = await redis.get('AUMSER.EINVOICE.Login')
            if (vdt_redis) {

                fd = moment(vdt_redis, dtf, true)
                if (!fd.isValid()) {
                    fd.setDate(fd.getDate() - 30)
                } else {
                    fd = fd.format(dtf)
                }
            } else {
                fd = new Date(); // today!
                fd.setDate(fd.getDate() - 30)
            }
            td = (moment(new Date()).format(dtf))
            let sql = `SELECT sg.ID "Group_ID",
            sg.[NAME]"Group_Name",
            ' ' "Group_Type",
            sl.[user_id]"Group_Implemented_By",
            FORMAT (sl.dt, 'dd/MM/yyyy HH:mm:ss')"Group_Implemented_TimeStamp",
            CASE
               WHEN (   CHARINDEX ('INS', upper (sl.fnc_id)) > 0
                     OR CHARINDEX ('POST', upper (sl.fnc_id)) > 0)
               THEN
                  'CREATE'
               WHEN (CHARINDEX ('DEL', upper (sl.fnc_id)) > 0)
               THEN
                  'DELETE'
               ELSE
                  'MODIFY'
            END "Group_Status",
            'VN' "Group_Country"
     FROM s_sys_logs sl, s_group sg, s_group_user sgu
     WHERE     sl.[user_id] = sgu.[USER_ID]
           AND sgu.GROUP_ID = sg.ID
           AND dt BETWEEN @1 AND @2
           AND sl.user_id NOT like '[__]%'`
           result = await dbs.query(sql,[fd,td]);
           records = result.recordset
            if (records.length <= 0) return
            const jsonData = JSON.parse(JSON.stringify(records));
            const json2csvParser = new Json2csvParser({ header: true, default: '', quote: '', delimiter: '|', encoding: 'utf8' });
            const csv = json2csvParser.parse(jsonData);
            //logger4app.debug(csv);
            let check = 4
            await ftp.uploadsplunkscb(csv,check)
            
        } catch (err) {
            logger4app.debug(err);
        }
    },
    pwdExpirationReminder : async (req, res, next) => {
        if (!config.is_use_local_user) return
        let  binds, sql, data
        let days_valid
        try {
            days_valid = Number(config.days_change_pass) - Number(config.days_warning_change_pass)
        } catch (err) {
            days_valid = 80
        }
        sql = `SELECT id, mail, change_pass_date, create_date, DATEDIFF(day, ISNULL(change_pass_date, create_date), getdate()) day_diff FROM s_user WHERE uc = 1 AND local = 1 and DATEDIFF(day, ISNULL(change_pass_date, create_date), getdate()) >= @1`
        binds = [days_valid]
        data = await dbs.query(sql, binds)
        const rows = data.recordset
        for (let row of rows) {
            try {
                let uid = row.id
                let mail = row.mail
                let days_left = Number(config.days_change_pass) - Number(row.day_diff)
                //Khóa tài khoản nếu quá hạn
                if (days_left < 0) {
                    await dbs.query(`update s_user set uc=@1 where id=@2`, [2, uid])
                    continue;
                }
                //Gửi mail thông báo nhắc đổi mật khẩu
                if (days_left <= config.days_warning_change_pass) {
                    await ads.change_pass_reminder(uid, mail, days_left)
                }
            } catch (err) {
                logger4app.error(err);
            }
        }
    
    },
    delHistoricalData: async (req, res, next) => {
        try {
            let sql, binds, artabs = config.array_table_setting
            for (const tab of artabs) {
                try {
                    if (!tab.enable) continue;
                    sql = `DELETE FROM ${tab.tablename} WHERE ${tab.conditionfield} <= DATEADD(day, -${tab.numberofdaystokeep}, GETDATE()) ${tab.extendcondition}`
                    binds = []
                    await dbs.query(sql, binds)
                } catch {
                    logger4app.error(`schedule delHistoricalData => `, err)
                }
            }
        } catch (err) {
            logger4app.debug(`delHistoricalData `, err)
        }
    },
    UpdateRunningJob: async (pkey, preset, jobtimeout) => {//Ham thuc hien update job dang chay bang cach insert vao bang s_listvalues theo key. Tham so pkey la key truyen vao tu khai bao job. Tham so preset dung cho truong hop xoa theo key khi da chay xong
        if (!pkey) return 0
        let vjobtimeout = 300 //Mac dinh thoi gian timeout la 5 phut neu khong cau hinh
        if (util.isNumber(jobtimeout)) vjobtimeout = jobtimeout
        if (!preset) {//Thuc hien danh dau job dang chay
            logger4app.debug('UpdateRunningJob','pkey',pkey)
            let vReturn = 1 //Mac dinh tra ra trang thai la job chua chay
            try {
                let vtime = (new Date()).getTime() //Lay gio hien tai
                let expiredate = new Date(vtime + (vjobtimeout * 1000)) //Tinh thoi gian expire de sau nay xoa trong truong hop ung dung bi restart bat ngo. Se co job khac quet de xoa cac key expire nay
                //logger4app.debug('expiredate ',moment(expiredate).format("YYYY-MM-DD HH:mm:ss"))
                await dbs.query(sql_set_ins[`sql_set_ins_${config.dbtype}`], [pkey, moment(expiredate).format("YYYY-MM-DD HH:mm:ss")]) //Insert du lieu vao bang s_listvalues theo key vaf ngay gio qua han
                logger4app.debug('UpdateRunningJob','vReturn',vReturn)
            } catch (err) {
                //Truong hop insert loi tuc la co job khac dang chay, tra ra trang thai la da chay
                logger4app.debug('UpdateRunningJob err','vReturn',vReturn)
                logger4app.debug('UpdateRunningJob err','err',err)
                vReturn = 0
            }
        
            return vReturn
        }
        else {
            //Neu chay xong thi xoa key di
            await dbs.query(sql_del[`sql_del_${config.dbtype}`], [pkey])
            return 1
        }
    },
    DropRunningJobExpire: async (pkey) => {//Ham thuc hien xoa cac key cua cac job bi expire. Tham so pkey la key truyen vao tu khai bao job.
        //Neu chay xong thi xoa key di
        //logger4app.debug('sql_del_exp',sql_del_exp[`sql_del_exp_${config.dbtype}`])
        await dbs.query(sql_del_exp[`sql_del_exp_${config.dbtype}`], [pkey])
        return 1

    },
    UpdateMsgTvan: async (pkey, ptimeout, ptype) => {//Ham thuc hien xoa cac key cua cac job bi expire. Tham so pkey la key truyen vao tu khai bao job.
        //Goi ham check va update trang thai job dang chay hay khong
        let vcheck = await Service.UpdateRunningJob(pkey, 0, ptimeout)
        try {
            logger4app.debug('sendEmailApprInv', 'vcheck', vcheck)
            //Neu co job dang chay (vcheck = 0) thi thoat ra, khong thuc hien lenh tiep theo
            if (!vcheck) return
            await tvan.getMsgFromTvanQueue(ptype)
        } catch (err) {
            logger4app.error(err)
            throw err
        } finally {
            vcheck = await Service.UpdateRunningJob(pkey, 1)
        }
    }
}

module.exports = Service