'use strict'
const config = require("../config")
const logger4app = require("../logger4app")
const ENT = config.ent
const sql =require("mssql")
const sql2 = require("mssql")
const moment = require("moment")
const decrypt = require('../encrypt');
const dtf = config.dtf
let pool,pool2
const Service = {
    close: async () => {
        try {
            if (pool) {
                await pool.close()
                pool = null
            }
        } catch (err) {
            pool = null
            logger4app.error("closePool", err)
        }
    },
    close2: async () => {
        try {
            if (pool2) {
                await pool2.close()
                pool2 = null
            }
        } catch (err) {
            pool2 = null
            logger4app.error("closePool", err)
        }
    },
    getConn: async () => {
        try {
            if (pool) return pool
            let poolConfig = JSON.parse(JSON.stringify(config.poolConfig))
            poolConfig.password = decrypt.decrypt(poolConfig.password)
            pool = await sql.connect(poolConfig)
            pool.on("error", async err => {
                logger4app.error("mssql error", err)
                await Service.close()
            })
            return pool
        } catch (err) {
            pool = null
            throw err
        }
    },
    getConn2: async () => {
        try {
          
         pool2 = new sql.ConnectionPool(config.poolConfigdds);
          //pool2 = new sql.ConnectionPool(config.poolConfigdds);
            pool2.on("error", async err => {
                logger4app.error("mssql error", err)
                await Service.close2()
            })
            await pool2.connect();
           // pool2 = await sql2.connect(util.decryptDS(config.poolConfigdds))
           
            return pool2
        } catch (err) {
            pool2 = null
            throw err
        }
    },
    query: (sql, binds = []) => {
        return new Promise(async (resolve, reject) => {
            try {
                const conn = await Service.getConn()
                const request = await conn.request()
                let i = 1
                for (const bind of binds) {
                    request.input((i++).toString(), bind)
                }
                const result = await request.query(sql)
               // await Service.close()
                resolve(result)
            }
            catch (err) {
                reject(err)
            }
        })
    },
    queryConn: (sql, binds = [], conn) => {
        return new Promise(async (resolve, reject) => {
            try {
                const request = await conn.request()
                let i = 1
                for (const bind of binds) {
                    request.input((i++).toString(), bind)
                }
                const result = await request.query(sql)
               // await Service.close()
                resolve(result)
            }
            catch (err) {
                reject(err)
            }
        })
    },
    ddsStt: (fd,td) => {
        return new Promise(async (resolve, reject) => {
            let prc =''
            if(config.evm =='FPT') prc = 'pro_VAT_GetDDSStatus'
            else prc = '[DDSData].[dbo].[pro_VAT_GetDDSStatus]'
            try {
                const conn = await Service.getConn2()
                const  resultsdds = await conn.request()
                .input('FromDate', sql2.VarChar, moment(new Date(fd)).format(dtf))
                .input('ToDate', sql2.VarChar, moment(new Date(td)).format(dtf))
              
                .execute(prc)
            
                resolve(resultsdds)

            }
            catch (err) {
                reject(err)
            }
        })
    },
    dds: () => {
        return new Promise(async (resolve, reject) => {
            let prc =''
            if(config.evm =='FPT') prc = 'proc_VAT_GetEmailOfCustomer4VAT'
            else prc = '[MizuhoData].[dbo].[proc_VAT_GetEmailOfCustomer4VAT]'
            try {
                const conn = await Service.getConn2()
                const  resultsdds = await conn.request()
                .execute(prc)
              //  await Service.close2()
                resolve(resultsdds)

            }
            catch (err) {
                reject(err)
            }
        })
    },
    exch: (date) => {
        return new Promise(async (resolve, reject) => {
            let prc =''
            if(config.evm =='FPT') prc = 'proc_VAT_CollectionExchangeRate'
            else prc = '[MizuhoData].[dbo].[proc_VAT_CollectionExchangeRate]'
            try {
                const conn = await Service.getConn2()
                const  resultexch = await conn.request()
                //Thay tên hàm sau khi Nhờ Trung hỏi Mizuho
                .input('DataDate', sql2.VarChar, date)
                .execute(prc)
              
                resolve(resultexch)

            }
            catch (err) {
                reject(err)
            }
        })
    },
    syn: (prc,fd,td) => {
        return new Promise(async (resolve, reject) => {
            try {
                const conn = await Service.getConn2()
                const  resultsdds = await conn.request()
                .input('FromDate', sql2.VarChar, moment(new Date(fd)).format(dtf))
                .input('ToDate', sql2.VarChar, moment(new Date(td)).format(dtf))
                .execute(prc)
              //  await Service.close2()
                resolve(resultsdds)

            }
            catch (err) {
                reject(err)
            }
        })
    },
    ddspass: () => {
        return new Promise(async (resolve, reject) => {
            let prc =''
            if(config.evm =='FPT') prc = 'pro_VAT_GetDDSPassword'
            else prc = '[DDSData].[dbo].[pro_VAT_GetDDSPassword]'
            try {
                const conn = await Service.getConn2()
                const  resultsdds = await conn.request()
                .execute(prc)
               // await Service.close2()
                resolve(resultsdds)

            }
            catch (err) {
                reject(err)
            }
        })
    },
    ddsIns: (inv,date) => {
        return new Promise(async (resolve, reject) => {
            let prc =''
            if(config.evm =='FPT') prc = 'pro_VAT_Add2DocumentDispatching'
            else prc = '[DDSData].[dbo].[pro_VAT_Add2DocumentDispatching]'
            try {
                const year =(moment(date)).format('YYYY')
                const Month = (moment(date)).format('MMM')
                const day = (moment(date)).format('DD')
                const conn = await Service.getConn2()
                
                const  resultsdds = await conn.request()
                .input('CustomerAbbr', sql2.VarChar, inv.c9)
                .input('ReferenceNo', sql2.VarChar, inv.sec)
                .input('FileName', sql2.VarChar, `${year}\\${Month}\\${day}\\` + `${inv.c9}-${inv.curr}-${inv.seq}.zip`.split("/").join("."))
                .input('ValueDate', sql2.DateTime, date)
                .execute(prc)
              //  await Service.close2()
                resolve(resultsdds)

            }
            catch (err) {
                reject(err)
            }
        })
    },
    queries: (sql, arr) => {
        return new Promise(async (resolve, reject) => {
            try {
                const conn = await Service.getConn()
                for (const binds of arr) {
                    const request = await conn.request()
                    let i = 1
                    for (const bind of binds) {
                        request.input((i++).toString(), bind)
                    }
                    await request.query(sql)
                   /// await Service.close()
                }
                resolve("ok")
            }
            catch (err) {
                reject(err)
            }
        })
    },
    queriestran: (arr) => {
        return new Promise(async (resolve, reject) => {
            try {
                const conn = await Service.getConn()
                const transaction = await conn.transaction()
                transaction.begin(3, async err => {
                    try{
                        let hasError = false
                            transaction.on('rollback', aborted => {
                                hasError = true
                        })
                        let request = conn.request(transaction)
                        await request.query(`drop table IF EXISTS ##s_tmp`)
                        await request.query(`create table ##s_tmp(id bigint, pid varchar(3000), sec nvarchar(225), ic nvarchar(225), idt datetime, ou nvarchar(225), uc nvarchar(225), doc nvarchar(MAX))`)
                        
                        let sqltemp = `insert into ##s_tmp (id,pid,sec,ic,idt,ou,uc,doc) values (@1,@2,@3,@4,@5,@6,@7,@8)`
                        for (const binds of arr) {
                            request = conn.request(transaction)
                            //const request = await conn.request()
                            let i = 1
                            for (const bind of binds) {
                                request.input((i++).toString(), bind)
                            }
                            await request.query(sqltemp)
                           /// await Service.close()
                           if(hasError){
                               break;
                            }
                        }
                        const data = await request.query(`insert into s_inv (id,pid,sec,ic,idt,ou,uc,doc) select id,pid,sec,ic,idt,ou,uc,doc from ##s_tmp`)
                        if(hasError){
                            transaction.rollback()
                        }else{
                            transaction.commit()
                            resolve(data)
                        }

                      }catch(err){
                        transaction.rollback()
                        reject(err)
                      }
                })
            }
            catch (err) {
                reject(err)
            }
        })
    },
}
module.exports = Service
