"use strict"
const path = require("path")
const fs = require("fs")
const docxt = require("docxtemplater")
const xlsxtemp = require("xlsx-template")
const jszip = require("jszip")
const moment = require("moment")
const util = require("../util")
const config = require("../config")
const logger4app = require("../logger4app")
const redis = require("../redis")
const sec = require("../sec")
const dbs = require("./dbs")
const dtf=config.dtf


const Service = {
    
    get: async (req, res, next) => {
        try {
            const query = req.query, filter = JSON.parse(query.filter)
            const sort = query.sort, start = query.start ? query.start : 0, count = query.count ? query.count : 10
            let where = " where dt between @1 and @2", order, sql, result, rows, ret, val
            const fm = moment(filter.fd), tm = moment(filter.td)
            const fd = fm.startOf("day").format("YYYY-MM-DD HH:mm:ss"), td = tm.endOf("day").format("YYYY-MM-DD HH:mm:ss")
            let binds = [fd, td]
            let i = 3
            Object.keys(filter).forEach((key) => {
                val = filter[key]
                if (val) {
                    switch (key) {
                        case "fd":
                            break
                        case "td":
                            break
                        case "srclog":
                            where += ` and SRC=@${i++}`
                            binds.push(val)
                            break
                        case "type":
                            where += ` and FNC_ID=@${i++}`
                            binds.push(val)
                            break
                        case "user":
                            where += ` and USER_ID like @${i++}`
                            binds.push('%'+val+'%')
                            break
                        case "msg_id":
                            where += ` and MSG_ID like @${i++}`
                            binds.push('%'+val+'%')
                            break
                        case "msg_status":
                            where += ` and MSG_STATUS = @${i++}`
                            binds.push(val)
                            break
                        default:
                            where += ` and ${key} like @${i++}`
                            binds.push('%'+val+'%')
                            break
                    }
                }
            })
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = ` order by ${key} ${sort[key]}`
                })
            }
            else order = " order by dt_order desc, id"
            sql = `select null id,format(dt,'dd/MM/yyyy HH:mm:ss') dt,dt dt_order,fnc_id,fnc_name,fnc_url,action,user_id,user_name,src,dtl,msg_id,CASE WHEN msg_status=0 THEN N'Lỗi' WHEN msg_status=1 THEN N'Thành công' ELSE '' END  msg_status,r1,r2,r3,r4,doc from s_sys_logs ${where} ${order}  offset @${i++} rows fetch next @${i++} rows only`
            binds.push(parseInt(start), parseInt(count))
            result = await dbs.query(sql, binds)
            ret = { data: result.recordset, pos: start }
            if (start == 0) {
                sql = `select count(*) "total" from s_sys_logs ${where}`
                const result = await dbs.query(sql, binds)
                rows = result.rows
                ret.total_count = result.recordset[0].total
            }
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    xls: async (req, res, next) => {
        try {
            const query = req.query, filter = JSON.parse(query.filter), type = query.type
            const sort = query.sort
            let where = " where dt between @1 and @2", order, sql, result, rows, val, json
            const fm = moment(filter.fd), tm = moment(filter.td)
            const fd = fm.startOf("day").format("YYYY-MM-DD HH:mm:ss"), td = tm.endOf("day").format("YYYY-MM-DD HH:mm:ss")
            let binds = [fd, td]
           
            let i = 3
            Object.keys(filter).forEach((key) => {
                val = filter[key]
                logger4app.debug(val)
                if (val) {
                    switch (key) {
                        case "fd":
                            break
                        case "td":
                            break
                        case "srclog":
                            where += ` and SRC=@${i++}`
                            binds.push(val)
                            break
                        case "type":
                            where += ` and FNC_ID=@${i++}`
                            binds.push(val)
                            break
                        case "user":
                            where += ` and USER_ID like @${i++}`
                            binds.push('%'+val+'%')
                            break
                        case "msg_id":
                            where += ` and MSG_ID like @${i++}`
                            binds.push('%'+val+'%')
                            break
                        case "msg_status":
                            where += ` and MSG_STATUS = @${i++}`
                            binds.push(val)
                            break
                        default:
                            where += ` and ${key} like @${i++}`
                            binds.push('%'+val+'%')
                            break
                    }
                }
            })
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = ` order by ${key} ${sort[key]}`
                })
            }
            else order = " order by dt desc, id"
            sql = `select null id,format(dt,'dd/MM/yyyy HH:mm:ss') dt,fnc_id,fnc_name,fnc_url,action,user_id,user_name,src,dtl,msg_id,CASE WHEN msg_status=0 THEN N'Lỗi' WHEN msg_status=1 THEN N'Thành công' ELSE '' END  msg_status,r1,r2,r3,r4,doc from s_sys_logs ${where} ${order} offset 0 rows fetch next ${config.MAX_ROW_EXCEL_EXPORT} rows only `
            result = await dbs.query(sql, binds)
            
            rows = result.recordset
            let fn = "temp/SYSLOG.xlsx"
            if(type ==1){
                fn = "temp/AUDIT_LOG_SCB.xlsx"
            }
            json = { table: rows }
            const file = path.join(__dirname, "..", "..", "..", fn)
            const xlsx = fs.readFileSync(file)
            const template = new xlsxtemp(xlsx)
            template.substitute(1, json)
            res.end(template.generate(), "binary")
        }
        catch (err) {
            logger4app.debug("excel" + err)

            next(err)


        }
    }
}
module.exports = Service   