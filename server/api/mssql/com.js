"use strict"
const config = require("../config")
const logger4app = require("../logger4app")
const redis = require("../redis")
const dbs = require("./dbs")
const moment = require("moment")
const Service = {
    isLocal: async (username, next) => {
        try {
            let local
            const uid = username.uid
            const sql = `select local from s_user where id = @1`
            const result = await dbs.query(sql, [uid])
            let row = result.recordset[0]
            local = row.local
            return next(null, local)
        } catch (err) {
            next(err)
        }
    },
    obt: async (taxc) => {
        const key = `OUS.${taxc}`
        let row = await redis.get(key)
        if (row) return JSON.parse(row)
        const sql = `select id,taxc,taxo,name,fadd addr,mail,tel,acc,bank,seq,sign,usr,pwd,temp from s_ou where taxc=@1`
        const result = await dbs.query(sql, [taxc])
        row = result.recordset[0]
        await redis.set(key, JSON.stringify(row))
        return row
    },
    mst: async (uid) => {
        let rows, row
        let sql = `select b.taxc,concat(b.taxc,'-',b.name) name from s_ou b,s_manager a where b.taxc=a.taxc and a.user_id=@1 order by b.taxc`
        let result = await dbs.query(sql, [uid])
        rows = result.recordset
        let mst = []
        for (row of rows) {
            mst.push({ id: row.taxc, value: row.name })
        }

        return mst
    },
    init: async (user, next) => {
        try {
            const uid = user.sAMAccountName ? user.sAMAccountName : user.uid
            let rows, row, result, json, sql, jsontoken
            //khong check ad
            if (user._CHEKC_AD == 0) {
                //thêm login_number để check lần đăng nhập đầu
                let buff = new Buffer(user.pass);
                let base64data = buff.toString('base64');
                sql = `select a.name fn,a.ou,a.mail mail,a.uc, b.mst taxc,b.name,b.status,b.c0,b.sign sign_type${(config.is_use_local_user) ? `, a.login_number, a.local, a.change_pass, FORMAT(a.change_pass_date,'yyyy-MM-dd HH:mm:ss') change_pass_date` : ``} from s_user a,s_ou b where a.ou=b.id and a.id=@1 and a.pass=@2`
                result = await dbs.query(sql, [uid, base64data])
                rows = result.recordset
                if (rows.length == 0) throw new Error(`Tài khoản hoặc mật khẩu không đúng \n (Invalid username/password)`)
                row = rows[0]
                if (row.uc == 2) throw new Error(`Tài khoản ${uid} đã bị hủy \n (Account ${uid} has been canceled)`)
                if (row.local && !row.change_pass) {
                    if (!row.change_pass_date) {
                        throw new Error(`Mật khẩu đã hết hạn \n (Password is expired)`)
                    }
                    else {
                        const ddate = Number(config.local_password_expire)
                        const curdate = moment(new Date()).toDate()
                        const changedate = moment(row.change_pass_date, config.dtf).toDate()
                        const dtime = ((curdate - changedate) / (1000 * 24 * 3600)) // Chuyển ra số ngày
                        if (dtime > ddate) throw new Error(`Mật khẩu đã hết hạn \n (Password is expired)`)
                    }
                }
                if (row.status == 2) throw new Error(`Đơn vị ${row.name} của tài khoản ${uid} đã bị hủy \n (Unit ${row.on} of account ${uid} has been canceled)`)
                json = { uid: uid, loginnum: row.change_pass, mail: row.mail, fn: row.fn, ou: row.ou, on: row.name, taxc: row.taxc, u: row.ou, is_use_local_user: config.is_use_local_user, localusr: row.local, ldap_private: config.ldapPrivate, c0: row.c0, sign_type: row.sign_type }
            } else {
                sql = `select a.name fn,a.ou,a.uc,b.mst taxc,b.name,b.status,b.c0,b.sign sign_type${(config.is_use_local_user) ? `, a.login_number, a.local, a.change_pass, FORMAT(a.change_pass_date,'yyyyMMddHHmm') change_pass_date` : ``} from s_user a,s_ou b where a.ou=b.id and a.id=@1`
                result = await dbs.query(sql, [uid])
                rows = result.recordset
                if (rows.length == 0) throw new Error(`Tài khoản ${uid} chưa được gán đơn vị \n (The account ${uid} has not been assigned a unit)`)
                row = rows[0]
                if (row.uc == 2) throw new Error(`Tài khoản ${uid} đã bị hủy \n (The account ${uid} has been canceled)`)
                if (row.status == 2) throw new Error(`Đơn vị ${row.name} của tài khoản ${uid} đã bị hủy \n (Unit ${row.on} of account ${uid} has been canceled)`)
                json = { uid: uid, mail: user.mail, fn: row.fn, ou: row.ou, on: row.name, taxc: row.taxc, u: row.ou, loginnum: row.change_pass, is_use_local_user: config.is_use_local_user, localusr: row.local, ldap_private: config.ldapPrivate, c0: row.c0, sign_type: row.sign_type }
            }
            //sql = `select a.name fn,a.ou,a.uc,b.mst taxc,b.name,b.status from s_user a,s_ou b where a.ou=b.id and a.id=@1`
            // result = await dbs.query(sql, [uid])
            //rows = result.recordset
            // if (rows.length == 0) throw new Error(`Tài khoản ${uid} chưa được gán đơn vị`)
            // row = rows[0]
            // if (row.uc == 2) throw new Error(`Tài khoản ${uid} đã bị hủy`)
            // if (row.status == 2) throw new Error(`Đơn vị ${row.name} của tài khoản ${uid} đã bị hủy`)
            // json = { uid: uid, mail: user.mail, fn: row.fn, ou: row.ou, on: row.name, taxc: row.taxc, u: row.ou, ldap_private: config.ldapPrivate }
            sql = `select b.taxc,concat(b.taxc,'-',b.name) name from s_ou b,s_manager a where b.taxc=a.taxc and a.user_id=@1 order by b.taxc`
            result = await dbs.query(sql, [uid])
            rows = result.recordset
            let mst = []
            for (row of rows) {
                mst.push({ id: row.taxc, value: row.name })
            }


            let role = [], path = [], menu_cliet = []
            //sql = `select role_id from s_member where user_id=@1`
            sql = 'SELECT x.role_id "role_id", x.sort "sort", x.menu_detail "menu_detail", x.id "id", x.pid "pid" FROM(select r.code "role_id", r.sort "sort", r.menu_detail "menu_detail", r.id "id", r.pid "pid"  from s_role r,s_group_role gr,s_group g,s_group_user gu,s_user u where r.id=gr.ROLE_ID and gr.GROUP_ID=g.id and gu.USER_ID =U.id and GU.GROUP_ID=g.id and u.id=@1 and g.status=1 and r.active = 1  group by r.sort,r.id,r.pid,r.code,r.menu_detail) x ORDER BY x.sort'
            if (config.ent == "scb" && (!uid.includes(config.USER_NO_LDAP))) {
                const ads = require("../ads")
                const filter = `(cn=${uid})`
                const ruser = await ads.search({ filter: filter, scope: "sub", attributes: ["cn", "mail", "displayName", "ismemberof"], sizeLimit: 1 })
                let mbr = [], str = `'HUNGLQ',`
                if (ruser && ruser.length > 0 && ruser[0].ismemberof) {
                    if (!Array.isArray(ruser[0].ismemberof))
                        mbr.push(ruser[0].ismemberof)
                    else
                        mbr = ruser[0].ismemberof
                    //logger4app.debug(`mbr ${mbr}`)                       
                    for (let m of mbr) {
                        let gr = m.split(',')[0].split('=')[1], vcheckinvoice = m.split(',')[1].split('=')[1]
                        if (vcheckinvoice == 'einvoice') str += `'${gr}',`
                    }

                }

                let where = ` and g.NAME in (${str.slice(0, -1)})`
                sql = `select r.code "role_id" from s_role r,s_group_role gr,s_group g,s_group_user gu,s_user u where r.id=gr.ROLE_ID and gr.GROUP_ID=g.id and gu.USER_ID =U.id and GU.GROUP_ID=g.id and u.id=@1 and g.status=1 and r.active = 1 ${where} group by r.code`
            }

            result = await dbs.query(sql, [uid])
            rows = result.recordset
            for (row of rows) {
                const rid = row.role_id, arr = config.PATH_ROLE[rid], menu_detail = row.menu_detail
                role.push(rid)
                menu_cliet.push({ id: row.id, pid: row.pid, menu_detail: menu_detail })
                path = path.concat(arr)
            }
            json.path = Array.from(new Set(path))
            //Them bien jsontoken chi luu thong tin token, tranh luu du thua
            jsontoken = JSON.parse(JSON.stringify(json))
            const ous = require("./ous")
            const org = await ous.org(json)
            json.org = org
            json.mst = mst
            json.ent = config.ent
            json.serial_grant = config.serial_grant
            json.serial_usr_grant = config.serial_usr_grant
            json.role = role
            json.menu_cliet = menu_cliet
            json.ORG_EDIT = config.ORG_EDIT
            const inc = require("../inc")
            json.statement = await inc.getstatementinfo(json.taxc)
            json.degree_config = config.DEGREE_CONFIG
            json.jsontoken = jsontoken
            json.grindfconf = config.GRIDNFCONF
            //Lay mot so danh muc lay tu redis hoac bang s_listvalues thay vi ngay truoc toan fix vao code theo ten bien config catfromredis
            for (let vcat of config.catfromredis) {
                let cattmp = JSON.parse(JSON.stringify(config[String(vcat).toUpperCase()])), catobj = []
                //Check danh sach loai hoa don voi thong tin dang ky dich vu, hoa don nao dang ky dich vu moi tra ra
                if (String(vcat).toUpperCase() == "ITYPE") {
                    //Nhân 1 bản full trả ra cho iType
                    let cattmpfitype = cattmp, fitype = []
                    for (let obj of cattmpfitype) {
                        if (obj.hasOwnProperty("status")) {
                            if (obj.status == 1) fitype.push(obj)
                        } else
                            fitype.push(obj)
                    }
                    json["fitype"] = fitype
                    
                    if (json.statement && json.statement.invtype) {
                        let arritype = json.statement.invtype.split(",")
                        for (let obj of cattmp) {
                            let objtmp = arritype.find(x => x == obj.id)
                            if (objtmp) catobj.push(obj)
                        }
                        cattmp = catobj
                        catobj = []
                    }
                }
                for (let obj of cattmp) {
                    if (obj.hasOwnProperty("status")) {
                        if (obj.status == 1) catobj.push(obj)
                    } else
                        catobj.push(obj)
                }
                json[vcat] = catobj
            }
            const usercl = require("./user")
            await usercl.upduserst(uid)
            return next(null, json)
        }
        catch (err) {
            next(err)
        }
    }
}
module.exports = Service