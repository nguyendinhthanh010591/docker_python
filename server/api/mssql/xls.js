"use strict"
const xlsx = require("xlsx")
const objectMapper = require("object-mapper")
const moment = require("moment")
const ajv = require("ajv")
const jv = new ajv({ allErrors: true })
const redis = require("../redis")
const util = require("../util")
const n2w = require("../n2w")
const config = require("../config")
const logger4app = require("../logger4app")
const SEC = require("../sec")
const dbs = require("./dbs")
const logging = require("../logging")
const ent = config.ent
const xls_idt = config.xls_idt
const xls_data = config.xls_data
const xls_ccy = config.xls_ccy
const mfd = config.mfd
const dtf = config.dtf
const GRANT_USR = config.serial_usr_grant
const grant = config.ou_grant
const SER_USES_APP = config.SER_USES_APP
const KM = "Khuyến mãi"
const CK = "Chiết khấu"
const MT = "Mô tả"
const KCT = "Không chịu thuế"
const KKK = "Không kê khai nộp thuế"
const KMMT = [KM, MT]
const GNS = ["code", "name", "unit", "price"]
const ORG = ["name", "name_en", "taxc", "code", "addr", "tel", "mail", "acc", "bank"]
const PXC = ["ic", "ordno", "orddt", "ordou", "ordre", "recvr", "trans", "vehic", "contr", "whsfr", "whsto", "curr", "exrt", "note", "sum", "items.type", "items.name", "items.unit", "items.price", "items.quantity", "items.amount"]
const HDC = (!"vcm".includes(config.ent)) ? ["ic", "form", "serial", "seq", "idt", "buyer", "bname", "btax", "baddr", "btel", "bmail", "bacc", "bbank", "paym", "curr", "exrt", "note", "sum", "vat", "total", "items.type", "items.name", "items.unit", "items.price", "items.quantity", "items.vrt", "items.amount", "rform", "rserial", "rseq", "rref", "rrdt", "rrea"] : ["ic", "form", "serial", "seq", "idt", "buyer", "bname", "btax", "baddr", "btel", "bmail", "bacc", "bbank", "paym", "curr", "exrt", "note", "sum", "vat", "total", "items.type", "items.name", "items.unit", "items.price", "items.quantity", "items.vrt", "items.amount", "rform", "rserial", "rseq", "rref", "rrdt", "rrea", "score", "sum2", /*"vat0",*/ "sum0", "vat5", "sum5", "vat10", "sum10", "sum1"]
const URL = "http://json-schema.org/draft-07/schema#"
const STT = { "type": "integer", "minimum": 1 }
const STR = { "type": "string", "maxLength": 255 }
const NUM = { "type": "number", "minimum": 0 }
const NUMSCB = { "type": "number" }
const PAYM = (!"vcm".includes(config.ent)) ? { "description": "HTTT", "type": "string", "default": "TM", "enum": ["CK", "TM", "TM/CK", "DTCN"] } : { "description": "HTTT", "type": "string", "default": "TM", "enum": ["CK", "TM", "TM/CK/Khác", "DTCN"] }
const CURR = { "description": "Loại tiền", "type": "string", "default": "VND", "enum": ["AUD", "CAD", "CHF", "DKK", "EUR", "GBP", "HKD", "INR", "JPY", "KRW", "KWD", "MYR", "NOK", "RUB", "SAR", "SEK", "SGD", "THB", "USD", "VND"] }
const ITEM = (!"vcm".includes(config.ent)) ? { "type": "object", "properties": { "line": STT, "type": { "type": "string", "enum": ["", KM, CK, MT] }, "name": STR, "price": NUM, "quantity": NUM, "amount": NUM }, "required": ["line", "name"] } : { "type": "object", "properties": { "line": STT, "type": { "type": "string", "enum": ["", KM, CK, MT] }, "name": STR, "price": NUM, "quantity": NUM, "amount": NUM, "c0": STR }, "required": ["line", "name"] }
const EXRT = { "type": "number", "minimum": 1/* , "default": 1 */ }
const chkschema = {
    "SCHEMAS_VCM": {
        "GNS": { "$schema": URL, "type": "object", "properties": { "code": { "type": ["integer", "string"], "minimum": 0, "maxLength": 30 }, "name": STR, "unit": { "type": "string", "maxLength": 30 }, "price": NUM }, "required": ["code", "name", "unit"] },
        "00ORGS": { "$schema": URL, "type": "object", "properties": { "name": STR, "addr": STR, "mail": STR }, "required": ["name", "addr"] },
        "03XKNB": {
            "$schema": URL, "type": "object", "definitions": { "item": { "type": "object", "properties": { "line": STT, "type": { "type": "string", "enum": ["", KM, CK, MT] }, "name": STR, "price": NUM, "quantity": NUM, "amount": NUM, "c0": STR }, "required": ["line", "name"] } },
            "properties": { "c0": STR, "c2": STR, "c3": STR, "note": STR, "contr": STR, "vehic": STR, "trans": STR, "recvr": STR, "ordou": STR, "whsfr": STR, "whsto": STR, "curr": CURR, "exrt": EXRT, "sum": NUM, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 } },
            "required": ["form", "serial", "idt", "ic", "ordno", "orddt", "whsfr", "whsto", "curr", "exrt", "items", "c0", "c2", "c3"]
        },
        "01GTKT": {
            "$schema": URL, "type": "object", "definitions": {
                "item": { "type": "object", "properties": { "line": STT, "type": { "type": "string", "enum": ["", KM, CK, MT] }, "name": STR, "price": NUM, "quantity": NUM, "amount": NUM, "vat": NUM, "total": NUM, "vrt": { "type": "string", "enum": ["-2", "-1", "0", "5", "10"] } }, "required": ["line", "name", "vrt", "amount"] }
            },
            "properties": {
                STR, "buyer": STR, "bname": STR, "baddr": STR, "bmail": STR, "paym": { "description": "HTTT", "type": "string", "default": "TM", "enum": ["CK", "TM", "TM/CK/Khác", "DTCN"] }, "curr": CURR, "exrt": EXRT, "sum": NUM, "total": NUM, "vat": NUM, "score": NUM, "sum2": NUM, /*"vat0": NUM, */"sum0": NUM, "vat5": NUM, "sum5": NUM, "vat10": NUM, "sum10": NUM, "sum1": NUM, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 }
            },
            "required": ["form", "serial", "idt", "ic", "bname", "baddr", "paym", "curr", "exrt", "items"]
        },
        "02GTTT": {
            "$schema": URL, "type": "object", "definitions": { "item": { "type": "object", "properties": { "line": STT, "type": { "type": "string", "enum": ["", KM, CK, MT] }, "name": STR, "price": NUM, "quantity": NUM, "amount": NUM, "c0": STR }, "required": ["line", "name"] } },
            "properties": {
                "buyer": STR, "bname": STR, "baddr": STR, "bmail": STR, "paym": { "description": "HTTT", "type": "string", "default": "TM", "enum": ["CK", "TM", "TM/CK/Khác", "DTCN"] }, "curr": CURR, "exrt": EXRT, "sum": NUM, "total": NUM, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 }
            },
            "required": ["ic", "bname", "baddr", "paym", "curr", "exrt", "sum", "total", "items"]
        }
    },
    "SCHEMAS_AIA": {
        "GNS": { "$schema": URL, "type": "object", "properties": { "code": { "type": ["integer", "string"], "minimum": 0, "maxLength": 30 }, "name": STR, "unit": { "type": "string", "maxLength": 30 }, "price": NUM }, "required": ["code", "name", "unit"] },
        "00ORGS": { "$schema": URL, "type": "object", "properties": { "name": STR, "addr": STR, "mail": STR }, "required": ["name", "addr"] },
        "03XKNB": {
            "$schema": URL, "type": "object", "definitions": { "item": { "type": "object", "properties": { "line": STT, "type": { "type": "string", "enum": ["", KM, CK, MT] }, "name": STR, "price": NUM, "quantity": NUM, "amount": NUM }, "required": ["line", "name"] } },
            "properties": { "note": STR, "contr": STR, "vehic": STR, "trans": STR, "recvr": STR, "ordou": STR, "whsfr": STR, "whsto": STR, "curr": CURR, "exrt": EXRT, "sum": NUM, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 } },
            "required": ["ic", "whsfr", "whsto", "curr", "exrt", "items"]
        },
        "01GTKT": {
            "$schema": URL, "type": "object", "definitions": {
                "item": { "type": "object", "properties": { "line": STT, "type": { "type": "string", "enum": ["", KM, CK, MT] }, "name": STR, "price": NUM, "quantity": NUM, "amount": NUM, "vat": NUM, "total": NUM, "vrt": { "type": "string", "enum": ["-2", "-1", "0", "5", "10"] } }, "required": ["line", "name", "vrt"] }
            },
            "properties": {
                STR, "buyer": STR, "bname": STR, "baddr": STR, "bmail": STR, "paym": { "description": "HTTT", "type": "string", "default": "TM", "enum": ["CK", "TM", "TM/CK", "DTCN"] }, "curr": CURR, "exrt": EXRT, "sum": NUM, "total": NUM, "vat": NUM, "c5": { "type": "string", "enum": ["Y", "N"] }, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 }
            },
            "required": ["idt", "ic", "baddr", "paym", "curr", "vat", "sum", "total", "items"]
        },
        "02GTTT": {
            "$schema": URL, "type": "object", "definitions": { "item": { "type": "object", "properties": { "line": STT, "type": { "type": "string", "enum": ["", KM, CK, MT] }, "name": STR, "price": NUM, "quantity": NUM, "amount": NUM }, "required": ["line", "name"] } },
            "properties": {
                "buyer": STR, "bname": STR, "baddr": STR, "bmail": STR, "paym": { "description": "HTTT", "type": "string", "default": "TM", "enum": ["CK", "TM", "TM/CK", "DTCN"] }, "curr": CURR, "exrt": EXRT, "sum": NUM, "total": NUM, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 }
            },
            "required": ["ic", "bname", "baddr", "paym", "curr", "exrt", "sum", "total", "items"]
        }
    },
    "SCHEMAS_SGR": {
        "GNS": { "$schema": URL, "type": "object", "properties": { "code": { "type": ["integer", "string"], "minimum": 0, "maxLength": 30 }, "name": STR, "unit": { "type": "string", "maxLength": 30 }, "price": NUM }, "required": ["code", "name", "unit"] },
        "00ORGS": { "$schema": URL, "type": "object", "properties": { "name": STR, "addr": STR, "mail": STR }, "required": ["name", "addr"] },
        "03XKNB": {
            "$schema": URL, "type": "object", "definitions": { "item": { "type": "object", "properties": { "line": STT, "type": { "type": "string", "enum": ["", KM, CK, MT] }, "name": STR, "price": NUM, "quantity": NUM, "amount": NUM }, "required": ["line", "name"] } },
            "properties": { "note": STR, "contr": STR, "vehic": STR, "trans": STR, "recvr": STR, "ordou": STR, "whsfr": STR, "whsto": STR, "curr": CURR, "exrt": EXRT, "sum": NUM, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 } },
            "required": ["form", "serial", "ic", "whsfr", "whsto", "curr", "exrt", "items"]
        },
        "01GTKT": {
            "$schema": URL, "type": "object", "definitions": {
                "item": { "type": "object", "properties": { "line": STT, "type": { "type": "string", "enum": ["", KM, CK, MT] }, "name": STR, "price": NUMSCB, "quantity": NUMSCB, "amount": NUM, "vat": NUM, "total": NUM, "vrt": { "type": "string", "enum": ["-2", "-1", "0", "5", "10"] } }, "required": ["line", "name", "amount", "vrt", "vat", "total"] }
            },
            "properties": {
                STR, "buyer": STR, "bname": STR, "baddr": STR, "bmail": STR, "paym": { "description": "HTTT", "type": "string", "default": "TM", "enum": ["CK", "TM", "TM/CK", "DTCN"] }, "curr": CURR, "exrt": EXRT, "sum": NUM, "total": NUM, "vat": NUM, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 }
            },
            "required": ["form", "serial", "seq", "ic", "bname", "baddr", "paym", "curr", "vat", "sum", "total", "items"]
        },
        "02GTTT": {
            "$schema": URL, "type": "object", "definitions": { "item": { "type": "object", "properties": { "line": STT, "type": { "type": "string", "enum": ["", KM, CK, MT] }, "name": STR, "price": NUM, "quantity": NUM, "amount": NUM }, "required": ["line", "name"] } },
            "properties": {
                "buyer": STR, "bname": STR, "baddr": STR, "bmail": STR, "paym": { "description": "HTTT", "type": "string", "default": "TM", "enum": ["CK", "TM", "TM/CK", "DTCN"] }, "curr": CURR, "exrt": EXRT, "sum": NUM, "total": NUM, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 }
            },
            "required": ["ic", "bname", "baddr", "paym", "curr", "exrt", "sum", "total", "items"]
        }
    },
    "SCHEMAS_SCB": {
        "GNS": { "$schema": URL, "type": "object", "properties": { "code": { "type": ["integer", "string"], "minimum": 0, "maxLength": 30 }, "name": STR, "unit": { "type": "string", "maxLength": 30 }, "price": NUM }, "required": ["code", "name", "unit"] },
        "00ORGS": { "$schema": URL, "type": "object", "properties": { "name": STR, "addr": STR, "mail": STR }, "required": ["name", "addr"] },
        "03XKNB": {
            "$schema": URL, "type": "object", "definitions": { "item": { "type": "object", "properties": { "line": STT, "type": { "type": "string", "enum": ["", KM, CK, MT] }, "name": STR, "price": NUM, "quantity": NUM, "amount": NUM }, "required": ["line", "name"] } },
            "properties": { "note": STR, "contr": STR, "vehic": STR, "trans": STR, "recvr": STR, "ordou": STR, "whsfr": STR, "whsto": STR, "curr": CURR, "exrt": EXRT, "sum": NUM, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 } },
            "required": ["ic", "whsfr", "whsto", "curr", "exrt", "items"]
        },
        "01GTKT": {
            "$schema": URL, "type": "object", "definitions": {
                "item": { "type": "object", "properties": { "line": STT, "type": { "type": "string", "enum": ["", KM, CK, MT] }, "name": STR, "amount": NUM, "vat": NUMSCB, "total": NUMSCB, "vrt": { "type": "string", "enum": ["-2", "-1", "0", "5", "10"] } }, "required": ["line", "name", "vrt"] }
            },
            "properties": {
                STR, "buyer": STR, "bname": STR, "baddr": STR, "bmail": STR, "paym": { "description": "HTTT", "type": "string", "default": "TM", "enum": ["CK", "TM", "TM/CK", "DTCN"] }, "curr": CURR, "exrt": EXRT, "sum": NUM, "total": NUMSCB, "vat": NUMSCB, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 }
            },
            "required": ["idt", "ic", "bname", "baddr", "paym", "curr", "exrt", "vat", "sum", "total", "c0", "items"]
        },
        "02GTTT": {
            "$schema": URL, "type": "object", "definitions": { "item": { "type": "object", "properties": { "line": STT, "type": { "type": "string", "enum": ["", KM, CK, MT] }, "name": STR, "price": NUM, "quantity": NUM, "amount": NUM }, "required": ["line", "name"] } },
            "properties": {
                "buyer": STR, "bname": STR, "baddr": STR, "bmail": STR, "paym": { "description": "HTTT", "type": "string", "default": "TM", "enum": ["CK", "TM", "TM/CK", "DTCN"] }, "curr": CURR, "exrt": EXRT, "sum": NUM, "total": NUM, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 }
            },
            "required": ["ic", "bname", "baddr", "paym", "curr", "exrt", "sum", "total", "items"]
        }
    },
    "DEFAULT": {
        "GNS": { "$schema": URL, "type": "object", "properties": { "code": { "type": ["integer", "string"], "minimum": 0, "maxLength": 30 }, "name": STR, "unit": { "type": "string", "maxLength": 30 }, "price": NUM }, "required": ["code", "name", "unit"] },
        "00ORGS": { "$schema": URL, "type": "object", "properties": { "name": STR, "addr": STR, "mail": STR, "acc": STR, "bank": STR }, "required": ["code", "name", "addr"] },
        "03XKNB": {
            "$schema": URL, "type": "object", "definitions": { "item": { "type": "object", "properties": { "line": STT, "type": { "type": "string", "enum": ["", KM, CK, MT] }, "name": STR, "price": NUM, "quantity": NUM, "amount": NUM, "unit": STR }, "required": ["line", "name"] } },
            "properties": { "note": STR, "contr": STR, "vehic": STR, "trans": STR, "recvr": STR, "ordou": STR, "whsfr": STR, "whsto": STR, "curr": CURR, "exrt": EXRT, "sum": NUM, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 } },
            "required": ["ic", "whsfr", "whsto", "curr", "exrt", "items"]
        },
        "01GTKT": {
            "$schema": URL, "type": "object", "definitions": {
                "item": { "type": "object", "properties": { "line": STT, "type": { "type": "string", "enum": ["", KM, CK, MT] }, "name": STR, "price": NUM, "quantity": NUM, "amount": NUM, "vat": NUM, "total": NUM, "vrt": STR, "unit": STR }, "required": ["line", "name", "vrt", 'vatCode'] }
            },
            "properties": {
                STR, "buyer": STR, "bname": STR, "baddr": STR, "btel": STR, "bacc": STR, "bbank": STR, "note": STR, "bmail": STR, "paym": { "description": "HTTT", "type": "string", "default": "TM", "enum": ["CK", "TM", "TM/CK", "DTCN"] }, "curr": CURR, "exrt": EXRT, "sum": NUM, "total": NUM, "vat": NUM, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 }
            },
            "required": ["idt", "ic", "bname", "baddr", "paym", "curr", "exrt", "vat", "sum", "total", "items"]
        },
        "02GTTT": {
            "$schema": URL, "type": "object", "definitions": { "item": { "type": "object", "properties": { "line": STT, "type": { "type": "string", "enum": ["", KM, CK, MT] }, "name": STR, "price": NUM, "quantity": NUM, "amount": NUM, "unit": STR }, "required": ["line", "name"] } },
            "properties": {
                "buyer": STR, "bname": STR, "baddr": STR, "btel": STR, "bacc": STR, "bbank": STR, "note": STR, "bmail": STR, "paym": { "description": "HTTT", "type": "string", "default": "TM", "enum": ["CK", "TM", "TM/CK", "DTCN"] }, "curr": CURR, "exrt": EXRT, "sum": NUM, "total": NUM, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 }
            },
            "required": ["ic", "bname", "baddr", "paym", "curr", "exrt", "sum", "total", "items"]
        }
    }
}

const getSCHEMAS = () => {
    try {
        return chkschema[`SCHEMAS_${config.ent}`.toUpperCase()] ? chkschema[`SCHEMAS_${config.ent}`.toUpperCase()] : chkschema[`DEFAULT`.toUpperCase()]
    } catch (err) {
        return chkschema[`DEFAULT`.toUpperCase()]
    }
}

const SCHEMAS = getSCHEMAS()

// const SCHEMAS = {
//     "GNS": { "$schema": URL, "type": "object", "properties": { "code": { "type": ["integer", "string"], "minimum": 0, "maxLength": 30 }, "name": STR, "unit": { "type": "string", "maxLength": 30 }, "price": NUM }, "required": ["code", "name", "unit"] },
//     "00ORGS": { "$schema": URL, "type": "object", "properties": { "name": STR, "addr": STR, "mail": STR }, "required": ["name", "addr"] },
//     "03XKNB": (!"vcm".includes(config.ent)) ? {
//         "$schema": URL, "type": "object", "definitions": { "item": ITEM },
//         "properties": { "note": STR, "contr": STR, "vehic": STR, "trans": STR, "recvr": STR, "ordou": STR, "whsfr": STR, "whsto": STR, "curr": CURR, "exrt": EXRT, "sum": NUM, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 } },
//         "required": ["ic", "whsfr", "whsto", "curr", "exrt", "items"]
//     } : {
//         "$schema": URL, "type": "object", "definitions": { "item": ITEM },
//         "properties": { "c0": STR, "c2": STR, "c3": STR, "note": STR, "contr": STR, "vehic": STR, "trans": STR, "recvr": STR, "ordou": STR, "whsfr": STR, "whsto": STR, "curr": CURR, "exrt": EXRT, "sum": NUM, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 } },
//         "required": ["form", "serial", "idt", "ic", "ordno", "orddt", "whsfr", "whsto", "curr", "exrt", "items", "c0", "c2", "c3"]
//     },
//     "01GTKT": {
//         "$schema": URL, "type": "object", "definitions": {
//             "item": { "type": "object", "properties": { "line": STT, "type": { "type": "string", "enum": ["", KM, CK, MT] }, "name": STR, "price": NUM, "quantity": NUM, "amount": NUM, "vat": NUM, "total": NUM, "vrt": { "type": "string", "enum": ["-2", "-1", "0", "5", "10"] } }, "required":  (!"vcm".includes(config.ent)) ? ["line", "name", "vrt"] : ["line", "name", "vrt", "amount"] }
//         },
//         "properties": (!"vcm".includes(config.ent)) ? {
//             STR,"buyer": STR, "bname": STR, "baddr": STR, "bmail": STR, "paym": PAYM, "curr": CURR, "exrt": EXRT, "sum": NUM, "total": NUM, "vat": NUM, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 }
//         } : {
//             STR,"buyer": STR, "bname": STR, "baddr": STR, "bmail": STR, "paym": PAYM, "curr": CURR, "exrt": EXRT, "sum": NUM, "total": NUM, "vat": NUM, "score": NUM, "sum2": NUM, /*"vat0": NUM, */"sum0": NUM, "vat5": NUM, "sum5": NUM, "vat10": NUM, "sum10": NUM, "sum1": NUM, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 }
//         },
//         "required": ("aia".includes(config.ent)) ? ["idt", "ic","buyer","bmail", "btel", "bname", "baddr", "paym", "curr", "vat", "sum", "total", "items"]:((!"vcm".includes(config.ent)) ? ["idt", "ic", "bname", "baddr", "paym", "curr", "exrt", "vat", "sum", "total", "items"] : ["form", "serial", "idt", "ic", "bname", "baddr", "paym", "curr", "exrt", "items"])
//     },
//     "02GTTT": {
//         "$schema": URL, "type": "object", "definitions": { "item": ITEM },
//         "properties": {
//             "buyer": STR, "bname": STR, "baddr": STR, "bmail": STR, "paym": PAYM, "curr": CURR, "exrt": EXRT, "sum": NUM, "total": NUM, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 }
//         },
//         "required": ["ic", "bname", "baddr", "paym", "curr", "exrt", "sum", "total", "items"]
//     }
// }
const trans = {
    ic: "Mã hóa đơn",
    form: "Mẫu số",
    serial: "Ký hiệu",
    seq: "Số HĐ",
    idt: "Ngày HĐ",
    buyer: "Người mua",
    bname: "Đơn vị",
    btax: "MST",
    baddr: "Địa chỉ",
    btel: "Điện thoại",
    bmail: "Mail",
    bacc: "Số tài khoản",
    bbank: "Ngân hàng",
    paym: "Hình thức thanh toán",
    recvr: "Người nhận",
    trans: "Người vận chuyển",
    vehic: "Phương tiện",
    contr: "Số hợp đồng",
    ordno: "Lệnh số",
    orddt: "Lệnh ngày",
    ordou: "Đơn vị",
    ordre: "Lý do",
    whsfr: "Nơi xuất",
    whsto: "Nơi nhập",
    curr: "Loại tiền",
    exrt: "Tỷ giá",
    note: "Ghi chú",
    sum: "Tổng tiền hàng",
    vat: "Tổng thuế",
    total: "Tổng tiền thanh toán",
    name: "Tên khách hàng",
    name_en: "Tên tiếng Anh",
    taxc: "MST",
    code: "Mã",
    addr: "Địa chỉ",
    tel: "Điện thoại",
    mail: "Mail",
    acc: "Số tài khoản",
    bank: "Ngân hàng",
    rform: "Mẫu số cần thay thế",
    rserial: "Ký hiệu cần thay thế",
    rseq: "Số hóa đơn cần thay thế",
    rref: "Số văn bản",
    rrdt: "Ngày văn bản",
    rrea: "Lý do",
    score: "Điểm tiêu",
    sum2: "Không chịu thuế",
    //vat0: "Thuế 0%" ,
    sum0: "Thành tiền chưa thuế 0%",
    vat5: "Thuế 5%",
    sum5: "Thành tiền chưa thuế 5%",
    vat10: "Thuế 10%",
    sum10: "Thành tiền chưa thuế 10%",
    _type: "Hình thức",
    _name: "Hàng hóa, dịch vụ",
    _unit: "Đơn vị tính",
    _price: "Đơn giá",
    _quantity: "Số lượng",
    _vrt: "Thuế suất chi tiết",
    _amount: "Thành tiền chi tiết",
    _vat: "Tiền thuế chi tiết",
    _total: "Thành tiền sau thuế chi tiết",
    sum1: "Thành tiền không chịu thuế VAT"

}
const trans_en = {
    ic: "Invoice Code",
    form: "Form",
    serial: "Serial",
    seq: "Invoice No",
    idt: "Ngày HĐ",
    buyer: "Buyer",
    bname: "Buyer Name",
    btax: "Tax No",
    baddr: "Buyer Adress",
    btel: "Buyer Phone",
    bmail: "Buyer Mail",
    bacc: "Buyer Account",
    bbank: "Buyer Bank",
    paym: "Payment Mode",
    recvr: "Receiver",
    trans: "Transporter",
    vehic: "Vehicle",
    contr: "Contract No",
    ordno: "Order No",
    orddt: "Order Date",
    ordou: "Organization Unit",
    ordre: "Reason",
    whsfr: "Export location",
    whsto: "Import location",
    curr: "Currency",
    exrt: "Exchange Rate",
    note: "Note",
    sum: "Summary",
    vat: "Sum VAT",
    total: "Total",
    name: "Customer Name",
    name_en: "Customer Name (ENG)",
    taxc: "Tax No",
    code: "Code",
    addr: "Adress",
    tel: "Phone",
    mail: "Mail",
    acc: "Account",
    bank: "Bank",
    rform: "Replacing form",
    rserial: "Replacing serial",
    rseq: "Replacing invoice no",
    rref: "Document No",
    rrdt: "Document Date",
    rrea: "Reason",
    score: "Score",
    sum2: "Summary (without VAT)",
    //vat0: "Tax Rate 0%" ,
    sum0: "Summary (without VAT) 0%",
    vat5: "Sum VAT 5%",
    sum5: "Summary (without VAT) 5%",
    vat10: "Sum VAT 10%",
    sum10: "Summary (without VAT) 10%",
    _type: "Type",
    _name: "Goods, Services",
    _unit: "Unit",
    _price: "Price",
    _quantity: "Quantity",
    _vrt: "Detailed tax rate",
    _amount: "Amount in details",
    _vat: "Detailed tax money",
    _total: "Amount after detailed tax",
    sum1: "Amount not subject to VAT"
}
const gns = {
    "code": "Mã hàng",
    "name": "Tên hàng",
    "unit": "Đơn vị tính",
    "price": "Đơn giá"
}
const gns_en = {
    "code": "Commodity code",
    "name": "Name of goods",
    "unit": "Unit",
    "price": "Unit price"
}
const org = {
    "name": "Tên KH",
    "name_en": "Tên KH tiếng Anh",
    "taxc": "Mã số thuế",
    "code": "Mã KH",
    "addr": "Địa chỉ",
    "tel": "Số điện thoại",
    "mail": "Email",
    "acc": "Tài khoản NH",
    "bank": "Tên NH"
}
const org_en = {
    "name": "Customer name",
    "name_en": "English customer name",
    "taxc": "TIN",
    "code": "KH code",
    "addr": "Address",
    "tel": "Phone number",
    "mail": "Email",
    "acc": "Bank Account",
    "bank": "Bank name"
}
const init = async (type) => {
    const sql = "insert into s_xls(itype,jc,xc) values (@1,@2,@3)"
    let i = 65, binds = []
    switch (type) {
        case "03XKNB":
            for (const rec of PXC) {
                binds.push([type, rec, String.fromCharCode(i++)])
            }
            await dbs.queries(sql, binds)
            break
        case "02GTTT":
            for (const rec of HDC) {
                if (!["items.vrt", "vat"].includes(rec)) {
                    binds.push([type, rec, String.fromCharCode(i++)])
                }
            }
            await dbs.queries(sql, binds)
            break
        case "01GTKT":
            for (const rec of HDC) {
                binds.push([type, rec, i > 90 ? `A${String.fromCharCode(i++ - 26)}` : String.fromCharCode(i++)])
            }
            await dbs.queries(sql, binds)
            break
        case "00ORGS":
            for (const rec of ORG) {
                binds.push([type, rec, String.fromCharCode(i++)])
            }
            await dbs.queries(sql, binds)
            break
        default:
            break
    }
}

const cache = async (type) => {
    let obj, key = `XLS.${type}`
    obj = await redis.get(key)
    if (obj) return JSON.parse(obj)
    const sql = `select xc,CONCAT('c',idx) jc from s_dcm where itype=@1 and xc is not null and xc != '' and status=1 and dtl=2 union select xc,jc from s_xls where itype=@1 and xc is not null and xc != ''`
    let result
    if (type == "00ORGS") result = await dbs.query(sql, [type])
    else result = await dbs.query(`select xc,CONCAT('items.c',idx) jc from s_dcm where itype=@1 and xc is not null and xc != '' and status=1 and dtl=1 union ${sql}`, [type])
    const rows = result.recordset
    obj = {}
    for (const row of rows) {
        obj[row.xc] = row.jc
    }
    await redis.set(key, JSON.stringify(obj))
    return obj
}
const get_schema = async (type, dtl) => {
    try {
        const result = await dbs.query(`select CONCAT('c',idx) jc,typ,atr from s_dcm where itype=@1 and xc is not null and xc != '' and status=1 and dtl=@2 and typ not in ('text','checkbox','popup')`, [type, dtl])
        const rows = result.recordset
        let obj = {}
        let dts = []
        for (const row of rows) {
            const typ = row.typ, jc = row.jc
            if (typ == "number") obj[jc] = NUM
            else if (typ == "datepicker") dts.push(jc)
            else if (typ == "date") dts.push(jc)
            else {
                let atr = row.atr
                if (atr) {
                    atr = JSON.parse(atr)
                    let arr = []
                    if (atr.suggest.data) {
                        for (const item of atr.suggest.data) {
                            arr.push(item.id)
                        }
                        if (arr.length > 0) obj[jc] = { enum: arr }
                    }
                }
            }
        }
        await redis.set(`DTS.${type}.${dtl}`, JSON.stringify(dts))
        return obj
    } catch (err) {
        throw err
    }
}

const cache_dts = async (type, dtl) => {
    const key = `DTS.${type}.${dtl}`
    let obj = await redis.get(key)
    if (obj) return JSON.parse(obj)
    else return []
}

const cache_schema = async (type) => {
    try {
        const key = `CHK.${type}`
        let schema
        schema = await redis.get(key)
        if (schema) return JSON.parse(schema)
        schema = SCHEMAS[type]
        const hdr = await get_schema(type, 2), dtl = await get_schema(type, 1)
        if (!util.isEmpty(hdr)) schema.properties = { ...schema.properties, ...hdr }
        if (!util.isEmpty(dtl)) schema.definitions.item.properties = { ...schema.definitions.item.properties, ...dtl }
        await redis.set(key, JSON.stringify(schema))
        return schema
    } catch (err) {
        throw err
    }
}
const tax = async (rows, exrt) => {
    let catobj = JSON.parse(JSON.stringify(config.CATTAX))
    let arr = catobj
    for (let v of arr) {
        delete v["vrnc"]
        delete v["status"]
    }
    for (let row of rows) {
        if (row.type == MT) continue
        let amt = row.amount
        if (!util.isNumber(amt)) continue
        const vrt = row.vrt
        let vat = row.vat
        if (!util.isNumber(vat)) continue
        const vatCode = row.vatCode
        let obj = arr.find(x => x.vatCode == vatCode)
        if (typeof obj == "undefined") continue
        if (row.type == CK) amt = -amt
        obj.amt += amt
        if (amt == 0) obj.isZero = true
        if (obj.hasOwnProperty("vat")) obj.vat += (xls_data) ? vat : amt * vrt / 100
    }
    let tar = arr.filter(obj => { return obj.amt !== 0 || (obj.hasOwnProperty("isZero") && delete obj.isZero) })
    for (let row of tar) {
        row.amtv = Math.round(row.amt * exrt)
        if (row.hasOwnProperty("vat")) row.vatv = Math.round(row.vat * exrt)
    }
    return tar
}
const getKeyByValue = (object, value) => {
    return Object.keys(object).find(key => object[key] === value)
}

const Service = {
    get: async (req, res, next) => {
        try {
            const itype = req.params.itype
            let sql, result, rows
            sql = `select id,itype,jc,xc from s_xls where itype=@1 order by len(xc),xc`
            result = await dbs.query(sql, [itype])
            rows = result.recordset
            if (rows.length == 0) {
                await init(itype)
                result = await dbs.query(sql, [itype])
                rows = result.recordset
            }
            res.json(rows)
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const body = req.body, itype = body.itype, key = `XLS.${itype}`
            const result = await dbs.query("update s_xls set xc=@1 where id=@2", [body.xc, body.id])
            await redis.del(key)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    upload: async (req, res, next) => {
        try {
            //console.time("read")
            const file = req.file, body = req.body, type = body.type, token = SEC.decode(req), taxc = token.taxc, now = new Date(), NOW = moment(now).format(dtf)

            const wb = xlsx.read(file.buffer, { type: "buffer", cellDates: true, dateNF: mfd, sheetRows: config.MAX_ROW_EXCEL }) //cellText: false
            const sheetName = wb.SheetNames[0], ws = wb.Sheets[sheetName]
            for (let i in ws) {
                const wsi = ws[i]
                if (wsi.t == "d") wsi.v = moment(wsi.v, mfd).add(1, 'minutes').format(mfd)
            }
            const opts = ["01GTKT", "02GTTT", "03XKNB"].includes(type) ? { blankrows: false, header: "A", defval: null } : { blankrows: false, range: 1, header: type == "GNS" ? GNS : "A", defval: null }

            const arr = xlsx.utils.sheet_to_json(ws, opts)
            if (util.isEmpty(arr)) throw new Error(`Không đọc được số liệu \n (Can't read the data)`)
            let header
            if (type == "01GTKT") {
                header = arr[0]
                arr.splice(0, 1)
            } else if (type == "02GTTT") {
                header = arr[0]
                arr.splice(0, 1)
            } else if (type == "03XKNB") {
                header = arr[0]
                arr.splice(0, 1)
            }
            //console.timeEnd("read")
            let result = []
            const checkerr = () => {
                for (const row of result) {
                    if (!util.isEmpty(row.error)) return true
                }
                return false
            }
            if (type == "GNS") {
                const schema = SCHEMAS[type]
                for (const row of arr) {
                    const ret = jv.validate(schema, row)
                    row.error = ""
                    if (!ret) {
                        let msgArr = []
                        for (let i of jv.errors) {
                            let errorsText = i, prop
                            if (errorsText) prop = errorsText.dataPath.slice(1)
                            if (!prop) prop = errorsText.params.missingProperty
                            else if (prop.endsWith("[0]")) prop = "_".concat(errorsText.params.missingProperty)
                            if (prop.includes("[") || prop.includes("]")) prop = "_".concat(prop.split(".")[1]) || prop
                            switch (errorsText.keyword) {
                                case "required":
                                    msgArr.push(`Nhập thiếu thông tin ${gns[prop] || prop} `)
                                    msgArr.push(`(Lack of ${gns_en[prop] || prop}).`)
                                    break
                                case "enum":
                                    msgArr.push(`Dữ liệu ${gns[prop] || prop} không hợp lệ `)
                                    msgArr.push(`(Data ${gns_en[prop] || prop} is invalid).`)
                                    break
                                case "maxLength":
                                    msgArr.push(`Dữ liệu ${gns[prop] || prop} vượt quá ký tự cho phép ${errorsText.params.limit} ký tự `)
                                    msgArr.push(`(Data ${gns_en[prop] || prop} exceed allowed characters number ${errorsText.params.limit} characters).`)
                                    break
                                case "type":
                                    msgArr.push(`${gns[prop] || prop} phải là ${errorsText.params.type == "number" ? 'Số' : errorsText.params.type} `)
                                    msgArr.push(`(${gns_en[prop] || prop} should be ${errorsText.params.type == "number" ? 'Number' : errorsText.params.type}).`)
                                    break
                                case "minimum":
                                    msgArr.push(`Dữ liệu ${gns[prop] || prop} phải lớn hơn ${errorsText.params.limit} `)
                                    msgArr.push(`(Data ${gns_en[prop] || prop} should be greater than ${errorsText.params.limit}).`)
                                    break
                                default:
                                    msgArr.push(i.message)
                                    break
                            }
                            row.error += msgArr.join(", ")
                        }
                    }
                    result.push(row)
                }
                res.json({ err: checkerr(), data: result })
            }
            else if (type == "00ORGS") {
                const schema = SCHEMAS[type], map = await cache(type)
                for (const r of arr) {
                    const row = objectMapper(r, map)
                    let error = ""
                    if (row.taxc && !util.checkmst(row.taxc.toString())) error += "MST không hợp lệ." + " (Invalid Tax Identification Number)"
                    else if (row.mail && !util.checkemail(row.mail)) error += "Email không hợp lệ." + " (Invalid email)"
                    else {
                        const ret = jv.validate(schema, row)
                        if (!ret) {
                            let msgArr = []
                            for (let i of jv.errors) {
                                let errorsText = i, prop
                                if (errorsText) prop = errorsText.dataPath.slice(1)
                                if (!prop) prop = errorsText.params.missingProperty
                                else if (prop.endsWith("[0]")) prop = "_".concat(errorsText.params.missingProperty)
                                if (prop.includes("[") || prop.includes("]")) prop = "_".concat(prop.split(".")[1]) || prop
                                switch (errorsText.keyword) {
                                    case "required":
                                        msgArr.push(`Nhập thiếu thông tin ${org[prop] || prop} `)
                                        msgArr.push(`(Lack of ${org_en[prop] || prop}).`)
                                        break
                                    case "enum":
                                        msgArr.push(`Dữ liệu ${org[prop] || prop} không hợp lệ `)
                                        msgArr.push(`(Data ${org_en[prop] || prop} is invalid).`)
                                        break
                                    case "maxLength":
                                        msgArr.push(`Dữ liệu ${org[prop] || prop} vượt quá ký tự cho phép ${errorsText.params.limit} ký tự `)
                                        msgArr.push(`(Data ${org_en[prop] || prop} exceed allowed characters number ${errorsText.params.limit} characters).`)
                                        break
                                    case "type":
                                        msgArr.push(`${org[prop] || prop} phải là ${errorsText.params.type == "number" ? 'Số' : errorsText.params.type} `)
                                        msgArr.push(`(${org_en[prop] || prop} should be ${errorsText.params.type == "number" ? 'Number' : errorsText.params.type}).`)
                                        break
                                    case "minimum":
                                        msgArr.push(`Dữ liệu ${org[prop] || prop} phải lớn hơn ${errorsText.params.limit} `)
                                        msgArr.push(`(Data ${org_en[prop] || prop} should be greater than ${errorsText.params.limit}).`)
                                        break
                                    default:
                                        msgArr.push(i.message)
                                        break
                                }
                                error += msgArr.join(", ")
                            }
                        }
                    }
                    //if (util.isEmpty(row.code)) error += "Mã khách hàng không được để trống"+" (Customer code cannot be left blank)"
                    row.error = error
                    result.push(row)
                }
                res.json({ err: checkerr(), data: result })
            }
            else if (type == "CAN") {
                for (const row of arr) {
                    row.G = row.G ? moment(row.G, mfd).endOf("day").subtract(1, "seconds").format(dtf) : NOW
                    let obj = { form: row.A, serial: row.B, seq: row.C, c0: row.D, rea: row.E ? row.E : "", ref: row.F, rdt: row.G, error: "" }
                    if (!(row.A && row.B && row.C)) obj.error = "Thiếu mẫu số hoặc ký hiệu hoặc số hóa đơn." + "(Missing Form or Serial or Invoice code) "
                    result.push(obj)
                }
                let binds = [], i = 1
                for (const row of result) {
                    const ic = i++, seq = row.seq ? row.seq.toString().padStart(config.SEQ_LEN, "0") : "", c0 = util.isEmpty(row.c0) ? "0" : row.c0, rdt = moment(row.rdt, dtf).toDate()
                    row.ic = ic
                    row.seq = seq
                    if (rdt > moment(now, dtf).endOf("day").toDate()) row.error += "Ngày văn bản lớn hơn hiện tại." + " (Date of text is larger than current)"
                    if (!row.error) binds.push([ic, rdt, row.form, row.serial, seq, c0])
                }
                if (binds.length > 0) {
                    const conn = await dbs.getConn()
                    const request = await conn.request()
                    await request.query(`drop table IF EXISTS ##s_tmp`)
                    await request.query(`create table ##s_tmp(ic varchar(36),idt datetime,form varchar(11),serial varchar(6),seq varchar(7),c0 varchar(255))`)
                    for (const bind of binds) {
                        for (let index = 0; index < bind.length; index++) {
                            request.input(index.toString(), bind[index])
                        }
                        await request.query(`insert into ##s_tmp (ic,idt,form,serial,seq,c0) values (@0,@1,@2,@3,@4,@5)`)
                    }
                    let objs = {}, rs
                    rs = await request.query(`select a.ic,b.id,b.pid,b.adjtyp from ##s_tmp a,s_inv b where b.stax='${taxc}' and b.status=3 and b.form=a.form and b.serial=a.serial and b.seq=a.seq and (a.c0='0' or a.c0=b.c0) and a.idt>b.idt`)
                    for (const row of rs.recordset) {
                        objs[row.ic] = [row.id, (row.pid && row.adjtyp == 2) ? row.pid : null]
                    }
                    for (const row of result) {
                        let obj = objs[row.ic]
                        if (obj) {
                            row.id = obj[0]
                            row.pid = obj[1]
                            row.ref = row.ref ? row.ref : row.id
                        }
                        else row.error += "Không tìm thấy hoặc ngày văn bản sai." + " (Cannot found or the date of text is wrong)"
                    }
                }
                res.json({ err: checkerr(), data: result })
            }
            else {
                //console.time("chk1")

                const map = await cache(type), mapkeys = Object.keys(map), getKeyByValue = (v) => { return mapkeys.find(key => map[key] === v) }
                const iform = body.form, iserial = body.serial, VAT = (type === "01GTKT"), schema = await cache_schema(type)
                const dts2 = await cache_dts(type, 2), dts1 = await cache_dts(type, 1), len2 = dts2.length > 0, len1 = dts1.length > 0
                let ic = "^_^", obj, items

                for (const r of arr) {
                    const row = objectMapper(r, map)
                    if (!row.ic) throw new Error("Thiếu mã hóa đơn \n (Missing invoice code)")
                    if (ic == row.ic) {
                        items.push(row.items)
                    }
                    else {
                        if (obj) {
                            obj.items = items
                            result.push(obj)
                        }
                        obj = row
                        items = [row.items]
                        ic = row.ic
                    }
                }
                if (["vcm", "sgr"].includes(config.ent)) {
                    for (const row of result) {
                        //const row = objectMapper(r, map)
                        if (!row.form || !row.form.includes(type)) throw new Error("Mẫu hóa đơn không đúng \n (Invoice template is wrong)")
                    }
                }

                if (obj) {
                    obj.items = items
                    result.push(obj)
                }

                for (const row of result) {
                    //logger4app.debug(row.ic)
                    row.error = ""
                    let curr = row.curr, exrt = row.exrt, error = "", i = 1, vat = 0, sum = 0, total = 0, totalv, items = row.items
                    if (row.seq && ((row.seq).length < 7 || (row.seq).length > 7) && ent == "sgr") error += "Số hóa đơn phải có 7 ký tự. " + "(The invoice number must have 7 characters) "
                    if (row.ic && String(row.ic).length > 50) error += "Độ dài mã hóa đơn vượt quá ký tự cho phép (50 ký tự)." + "(The invoice code length exceeds the allowed characters) "
                    if (row.form && String(row.form).length > 11) error += "Độ dài mẫu số hóa đơn vượt quá ký tự cho phép (11 ký tự)." + " (The invoice form length exceeds the allowed characters)"
                    if (row.serial && String(row.serial).length > 6) error += "Độ dài ký hiệu hóa đơn vượt quá ký tự cho phép (6 ký tự)." + " (The invoice serial length exceeds the allowed characters) "
                    if (config.ent == "sgr") { if (row.seq && String(row.seq).length > 7) error += "Độ dài số hóa đơn vượt quá ký tự cho phép (7 ký tự)." + " (Invoice number length exceeds allowed characters)" }
                    row.ic = row.ic.toString()
                    if (!util.isNumber(exrt)) {
                        if (!(["vcm"].includes(config.ent))) {
                            if ((!xls_ccy) || (type == "03XKNB")) {
                                if (!util.isNumber(exrt)) {
                                    exrt = 1
                                    row.exrt = 1
                                }
                                if (!curr) {
                                    curr = 'VND'
                                    row.curr = 'VND'
                                }
                                if (curr == 'VND' && exrt !== 1) row.error += "VND tỉ giá phải bằng 1 \n (VND exchange rate must be 1)"
                            } else {
                                if (!curr) curr = 'VND'
                                if (!util.isNumber(exrt)) exrt = 1
                                if (row.curr == 'VND' && row.exrt !== 1) row.error += "VND tỉ giá phải bằng 1 \n (VND exchange rate must be 1)"
                            }
                        }
                    }
                    if (curr == 'VND' && exrt !== 1) error += "VND tỉ giá phải bằng 1." + "(VND Rate must be by 1)"
                    if (row.btax && !util.checkmst(row.btax.toString())) error += "MST không hợp lệ." + " (Invalid Tax Identification Number) "
                    if (row.bmail && !util.checkemail(row.bmail)) error += "Email không hợp lệ." + " (Invalid email) "
                    if (config.ent == "aia") {
                        if (((!row.bname) || row.bname == null || row.bname == '') && ((!row.buyer) || row.buyer == null || row.buyer == '')) error += "Thiếu Tên đơn vị hoặc Họ tên người mua." + " (Unit Name or Full Buyer Name is missing) "
                        if (((!row.btel) || row.btel == null || row.btel == '') && ((!row.bmail) || row.bmail == null || row.bmail == '')) error += "Thiếu Số điện thoại hoặc Email." + " (Missing Email or Phone Number) "
                    }
                    if (row.idt) {
                        const idt = moment(row.idt, mfd, true)
                        if (idt.isValid()) {
                            row.idt = idt.format(dtf)
                            if (idt.toDate() > now) error += "Ngày hđ lớn hơn ngày hiện tại." + "(The invoice date is greater than the current date) "
                        }
                        else error += "Ngày hđ sai định dạng." + "(The format of the invoice date is incorrect) "
                    }
                    else {
                        if (xls_idt) error += "Thiếu ngày hóa đơn." + " (Missing invoice date) "
                        else row.idt = NOW
                    }

                    if (config.ent == "scb") {
                        if (row.c1) {
                            const subRS = await dbs.query(`select * from s_segment where id = '${row.c1}'`)
                            let cSB = subRS.recordset
                            if (cSB.length == 0) {
                                error += "Không tồn tại nhóm segment này." + " (Doesn't exit this segment group)"
                            }
                        } else {
                            error += "Thiếu nhóm segment." + " (Missing segement group ID)"
                        }
                    }

                    if (row.orddt) {
                        const orddt = moment(row.orddt, mfd, true)
                        if (orddt.isValid()) {
                            row.orddt = orddt.format(dtf)
                        } else error += "Ngày lệnh sai định dạng." + "(Incorrect command date) "
                    }
                    if (ent == "sgr") {
                        if (row.c0) {
                            const c0 = moment(row.c0, mfd, true)
                            if (c0.isValid()) {
                                row.c0 = c0.format(dtf)
                            } else error += "Ngày đến sai định dạng." + "(Incorrect check-in date) "
                        }
                        if (row.c1) {
                            const c1 = moment(row.c1, mfd, true)
                            if (c1.isValid()) {
                                row.c1 = c1.format(dtf)
                            } else error += "Ngày đi sai định dạng." + "(Incorrect check-out date) "
                        }
                    }
                    if (len2) {
                        for (const col of dts2) {
                            if (row[col]) {
                                const idt = moment(row[col], mfd, true)
                                if (idt.isValid()) row[col] = idt.format(dtf)
                                else error += `Cột ${ws[`${getKeyByValue(col)}1`].v} sai định dạng ngày.` + `(Column ${ws[`${getKeyByValue(col)}1`].v} is wrong date format.) `
                            }
                        }
                    }
                    let catobj = JSON.parse(JSON.stringify(config.CATTAX))
                    for (const item of items) {
                        let erri = ""
                        item.line = i++
                        let objtax = catobj.find(x => x.cusVatCode == item.vatCode)
                        if (objtax) {
                            item.vrt = objtax.vrt
                            item.vatCode = objtax.vatCode
                        } else {
                            let objvrttotax = catobj.find(x => x.id == item.vrt)
                            if (objvrttotax) {
                                objtax = objvrttotax
                                item.vatCode = objvrttotax.vatCode
                            } else {
                                erri += "Sai mã loại thuế. (Wrong VAT code)"
                            }
                        }
                        //parse về số sgr price

                        if (item.price) {
                            try {
                                item.price = Number(item.price)
                            } catch {
                                item.price = item.price
                            }
                        }
                        //parse về số sgr quantity
                        if (item.quantity) {
                            try {
                                item.quantity = Number(item.quantity)
                            } catch {
                                item.quantity = item.quantity
                            }
                        }

                        //parse về số sgr amount
                        try {
                            item.amount = Number(item.amount)
                        } catch {
                            item.amount = item.amount
                        }
                        item.type = item.type ? item.type.toString().toUpperCase() : ""
                        switch (item.type) {
                            case "KM":
                                item.type = KM
                                break
                            case "CK":
                                item.type = CK
                                break
                            case "MT":
                                item.type = MT
                                break
                            case "MÔ TẢ":
                                item.type = MT
                                break
                            case "KHUYẾN MÃI":
                                item.type = KM
                                break
                            case "CHIẾT KHẤU":
                                item.type = CK
                                break
                            default:
                                item.type = ""
                                break
                        }
                        if (VAT && !util.isNumber(item.vrt)) erri += `thiếu thuế suất. ` + `(Missing tax rates)`
                        if (len1) {
                            for (const col of dts1) {
                                if (item[col]) {
                                    const idt = moment(item[col], mfd)
                                    if (idt.isValid()) item[col] = idt.format(dtf)
                                    else erri += `sai định dạng ngày.` + `(Wrong date format)`
                                }
                            }
                        }
                        if (["scb"].includes(config.ent) && item.c0) {
                            const trandate = moment(item.c0, mfd, true)
                            if (trandate.isValid()) {
                                item.c0 = trandate.format(dtf)
                            } else error += "Ngày giao dịch sai định dạng." + "(Incorrect transaction date) "
                        }
                        let p = item.price, q = item.quantity, amt = 0, vmt = 0, vrt = 0
                        if ("aia".includes(config.ent)) {
                            if (util.isNumber(p) && util.isNumber(q)) item.amount = p * q
                            else {
                                if (!p) delete item.price
                                if (!q) delete item.quantity
                            }
                        }
                        if ("sgr".includes(config.ent)) {
                            if (!xls_data) {
                                if (util.isNumber(p) && util.isNumber(q))
                                    item.amount = p * q
                                else {
                                    if (!p) delete item.price
                                    if (!q) delete item.quantity
                                }
                            }
                        }
                        if (KMMT.includes(item.type)) {
                            item.amount = 0
                            vrt = Number(item.vrt)
                        }
                        else {
                            if (type == "03XKNB" && !item.amount) {
                                item.amount = 0
                                vrt = Number(item.vrt)
                            }
                            if (util.isNumber(item.amount)) {
                                amt = item.amount
                                if (item.type == CK) amt = -amt
                                sum += amt
                                if (VAT) {
                                    vrt = Number(item.vrt)
                                    if (vrt > 0) {
                                        vmt = (xls_data) ? item.vat : (amt * vrt / 100)
                                        vat += vmt
                                    }
                                }
                            }
                            //else erri += `Thiếu thành tiền.`+` (Missing total amount)`
                        }
                        if (!(["vcm", "sgr"].includes(config.ent))) {
                            item.total = amt + vmt
                        } else {
                            if (type == "03XKNB") item.total = amt + vmt
                        }
                        item.total = (xls_data) ? item.total : amt + vmt
                        if (VAT) {
                            if (objtax) item.vrn = objtax.vrn
                            else item.vrn = "\\"
                            if (!(["vcm", "sgr"].includes(config.ent))) {
                                item.vat = vmt
                            } else {
                                if (type == "03XKNB") item.vat = vmt
                                item.vat = (xls_data) ? ((item.vat) ? item.vat : 0) : vmt
                            }
                            item.vrt = `${vrt}`
                        }
                        if (!util.isEmpty(erri)) error += `Lỗi chi tiết ${i}: ${erri}.` + `(Error detail ${i}: ${erri})`
                    }

                    // Duy fix lam tron
                    vat = parseFloat(vat.toFixed(2))
                    total = sum + vat
                    //Làm tạm cho Sun vì bên SAP chưa sửa được. Lúc nào nó sửa xong thì xóa đi
                    if ((["sgr"].includes(config.ent))) {
                        try {
                            row.sum = parseFloat(sum.toFixed(2))
                            sum = row.sum
                        } catch (ex) {
                            sum = 0
                            error += `Dữ liệu Thành tiền không hợp lệ (Invalid Amount Data). `
                        }
                        try {
                            row.total = parseFloat(total.toFixed(2))
                            total = row.total
                        } catch (ex) {
                            total = 0
                            error += `Dữ liệu Tổng tiền không hợp lệ (Invalid Total Data). `
                        }
                    }
                    //Hết đoạn Làm tạm cho Sun vì bên SAP chưa sửa được. Lúc nào nó sửa xong thì xóa đi
                    if (!(["vcm", "sgr"].includes(config.ent))) {
                        //Nếu phiếu xuất kho thì gán mặc định sum, total = 0 khi không có giá trị
                        if (type == "03XKNB") {
                            row.sum = (row.sum) ? row.sum : 0
                            row.total = (row.total) ? row.total : 0
                        }
                        try {
                            row.sum = (xls_data) ? parseFloat(row.sum.toFixed(2)) : parseFloat(sum.toFixed(2))
                            sum = row.sum
                        } catch (ex) {
                            sum = 0
                            error += `Dữ liệu Thành tiền không hợp lệ (Invalid Amount Data). `
                        }
                        try {
                            row.total = (xls_data) ? parseFloat(row.total.toFixed(2)) : parseFloat(total.toFixed(2))
                            total = row.total
                        } catch (ex) {
                            total = 0
                            error += `Dữ liệu Tổng tiền không hợp lệ (Invalid Total Data). `
                        }
                    }

                    if ((["vcm", "sgr"].includes(config.ent))) {
                        //Nếu là phiếu xuất kiêm vận chuyển kho thì gán
                        if (!VAT) {
                            if (!row.sum) row.sum = 0
                            row.total = row.sum
                        }
                    }

                    if (!(["vcm", "sgr"].includes(config.ent))) {
                        row.sumv = Math.round(row.sum * exrt)
                        totalv = Math.round(row.total * exrt)
                        row.totalv = totalv
                    } else {
                        if (util.isNumber(row.sum)) row.sumv = row.sum * exrt
                        if (util.isNumber(row.total)) row.totalv = row.total * exrt
                    }
                    if (["sgr"].includes(config.ent)) {//tong tien +phi dv+thu dac biet
                        if (row.total) row.total = row.total + (row.c7 ? row.c7 : 0) + (row.c9 ? row.c9 : 0)
                        row.totalv = Math.round(row.total * exrt)
                    }

                    if (VAT) {
                        if (!(["vcm", "sgr"].includes(config.ent))) {
                            try {
                                row.vat = (xls_data) ? parseFloat(row.vat.toFixed(2)) : vat
                                vat = row.vat
                            } catch (ex) {
                                vat = 0
                                error += `Dữ liệu Thuế suất không hợp lệ (Invalid Tax Data). `
                            }
                            row.vatv = Math.round(vat * exrt)
                        } else {
                            //Làm tạm cho Sun vì bên SAP chưa sửa được. Lúc nào nó sửa xong thì xóa đi
                            if ((["sgr"].includes(config.ent))) {
                                try {
                                    row.vat = vat
                                    vat = row.vat
                                } catch (ex) {
                                    vat = 0
                                    error += `Dữ liệu Thuế suất không hợp lệ (Invalid Tax Data). `
                                }
                                row.vatv = Math.round(vat * exrt)
                            }
                            //Hết đoạn Làm tạm cho Sun vì bên SAP chưa sửa được. Lúc nào nó sửa xong thì xóa đi
                            if (util.isNumber(row.vat)) row.vatv = Math.round(row.vat * exrt)

                        }
                        row.tax = await tax(items, exrt)
                    }

                    if (util.isNumber(row.totalv) && !isNaN(row.totalv)) {
                        row.word = n2w.n2w(row.totalv, 'VND')
                    }

                    //Lấy tổng theo các loại thuế đưa vào đối với VCM
                    //if ((["vcm","sgr"].includes(config.ent))  && VAT) 
                    //Làm tạm cho Sun vì bên SAP chưa sửa được. Lúc nào nó sửa xong thì thay cho đoạn dưới
                    if ((["vcm"].includes(config.ent)) && VAT) {
                        for (let tx of row.tax) {

                            switch (parseInt(tx.vrt)) {
                                case 0:
                                    tx.amt = row.sum0
                                    tx.amtv = row.sum0
                                    break
                                case 5:
                                    tx.amt = row.sum5
                                    tx.amtv = row.sum5
                                    tx.vat = row.vat5
                                    tx.vatv = row.vat5
                                    break
                                case 10:
                                    tx.amt = row.sum10
                                    tx.amtv = row.sum10
                                    tx.vat = row.vat10
                                    tx.vatv = row.vat10
                                    break
                                case -1:
                                    tx.amt = row.sum1
                                    tx.amtv = row.sum1
                                    break
                                case -2:
                                    tx.amt = row.sum2
                                    tx.amtv = row.sum2
                                    break
                                default:
                                    break
                            }
                        }

                    }

                    row.stax = taxc
                    if (!["vcm", "sgr"].includes(config.ent)) {
                        if (!row.form) row.form = iform
                        if (!row.serial) row.serial = iserial
                    }
                    if ("aia".includes(config.ent)) {
                        row.c6 = "Thanh lý tài sản, tổng đại lý"
                    }

                    row.error = util.isEmpty(error) ? "" : error
                }

                if ("vcm".includes(config.ent)) {
                    if (type == "03XKNB") {
                        trans.c0 = "Kho xuất"
                        trans.c1 = "MST xuất"
                        trans.c2 = "Số xe"
                        trans.c3 = "Kho nhập"
                        trans.c4 = "MST nhập"
                        trans._c0 = "Ghi chú"
                        trans_en.c0 = "Export warehouse"
                        trans_en.c1 = "Tax code export"
                        trans_en.c2 = "Vehicle number"
                        trans_en.c3 = "Import warehouse"
                        trans_en.c4 = "Tax code import"
                        trans_en._c0 = "Note"
                    }
                    if (type == "01GTKT") {
                        trans.c1 = "Điểm tích"
                        trans_en.c1 = "Earning points"
                    }
                }
                if ("sgr".includes(config.ent)) {
                    if (type == "03XKNB") {
                        trans.c0 = "Kho xuất"
                        trans.c1 = "MST xuất"
                        trans.c2 = "Số xe"
                        trans.c3 = "Kho nhập"
                        trans.c4 = "MST nhập"
                        trans._c0 = "Ghi chú"
                        trans_en.c0 = "Export warehouse"
                        trans_en.c1 = "Tax code export"
                        trans_en.c2 = "Vehicle number"
                        trans_en.c3 = "Import warehouse"
                        trans_en.c4 = "Tax code import"
                        trans_en._c0 = "Note"
                    }
                    if (type == "01GTKT") {
                        trans.c0 = "Ngày đến"
                        trans.c1 = "Ngày đi"
                        trans.c2 = "Giá Phòng"
                        trans.c3 = "Số Phòng"
                        trans.c4 = "Số lượng khách"
                        trans.c5 = "Mã PNR"
                        trans.c6 = "Thuế suất thuế TTĐB"
                        trans.c7 = "Thuế TTĐB"
                        trans.c8 = "Phí phục vụ (SVC) %"
                        trans.c9 = "Phí dịch vụ"
                        trans_en.c0 = "Arrival date"
                        trans_en.c1 = "Departure date"
                        trans_en.c2 = "Room Rate"
                        trans_en.c3 = "Number of Rooms"
                        trans_en.c4 = "Number of guests"
                        trans_en.c5 = "PNR code"
                        trans_en.c6 = "Special excise tax rate"
                        trans_en.c7 = "Special excise tax"
                        trans_en.c8 = "Service charge (SVC)%"
                        trans_en.c9 = "Service fee"
                    }
                }
                if ("aia".includes(config.ent)) {
                    trans.c5 = "Khách hàng đăng ký nhận hóa đơn"
                    trans_en.c5 = "Customers register to receive invoices"
                }
                if ("scb".includes(config.ent)) {
                    trans.c0 = "Hệ thống"
                    trans.c9 = "Mã khách hàng"
                    trans_en.c0 = "System"
                    trans_en.c9 = "Customer Code"
                }
                for (const row of result) {
                    const ret = jv.validate(schema, row)

                    if (!ret) {
                        let msgArr = []
                        for (let i of jv.errors) {
                            let errorsText = i, prop
                            if (errorsText) prop = errorsText.dataPath.slice(1)
                            for (let i = 0; i < row.items.length; i++) {
                                if (!prop) {
                                    prop = errorsText.params.missingProperty
                                    break
                                }
                                else if (prop.endsWith(`[${i}]`)) {
                                    prop = "_".concat(errorsText.params.missingProperty)
                                    break
                                }
                            }
                            if (prop.includes("[") || prop.includes("]")) prop = "_".concat(prop.split(".")[1]) || prop
                            switch (errorsText.keyword) {
                                case "required":
                                    msgArr.push(`Nhập thiếu thông tin ${trans[prop] || prop} `)
                                    msgArr.push(`(Lack of ${trans_en[prop] || prop}).`)
                                    break
                                case "enum":
                                    msgArr.push(`Dữ liệu ${trans[prop] || prop} không hợp lệ `)
                                    msgArr.push(`(Data ${trans_en[prop] || prop} is invalid).`)
                                    break
                                case "maxLength":
                                    msgArr.push(`Dữ liệu ${trans[prop] || prop} vượt quá ký tự cho phép ${errorsText.params.limit} ký tự `)
                                    msgArr.push(`(Data ${trans_en[prop] || prop} exceed allowed characters number ${errorsText.params.limit} characters).`)
                                    break
                                case "type":
                                    msgArr.push(`${trans[prop] || prop} phải là ${errorsText.params.type == "number" ? 'Số' : errorsText.params.type} `)
                                    msgArr.push(`(${trans_en[prop] || prop} should be ${errorsText.params.type == "number" ? 'Number' : errorsText.params.type}).`)
                                    break
                                case "minimum":
                                    msgArr.push(`Dữ liệu ${trans[prop] || prop} phải lớn hơn ${errorsText.params.limit} `)
                                    msgArr.push(`(Data ${trans_en[prop] || prop} should be greater than ${errorsText.params.limit}).`)
                                    break
                                default:
                                    msgArr.push(i.message)
                                    break
                            }
                        }
                        row.error += msgArr.join(", ")
                        // row.error += jv.errorsText()
                    }
                }
                //console.timeEnd("chk1")
                if (!checkerr()) {
                    //console.time("chk2")
                    let bindss = []
                    if ((["vcm", "sgr"].includes(config.ent)) && VAT) {
                        for (const row of result) {
                            delete row["sum2"]
                            //delete row["vat0"]
                            delete row["sum0"]
                            delete row["vat5"]
                            delete row["sum5"]
                            delete row["vat10"]
                            delete row["sum10"]
                            delete row["sum1"]
                            //row.word = n2w.n2w(row.totalv, row.curr)
                        }
                    }

                    const conn = await dbs.getConn()
                    const request = await conn.request()
                    await request.query(`drop table IF EXISTS ##s_tmp`)
                    await request.query(`create table ##s_tmp(ic varchar(50),idt datetime,form varchar(11),serial varchar(6),seq varchar(7))`)
                    for (const row of result) {
                        bindss.push([row.ic, moment(row.idt, dtf).endOf("day").subtract(1, "seconds").format(dtf), row.form, row.serial])
                        // request.input("1", row.ic)
                        // request.input("2", moment(row.idt, dtf).endOf("day").subtract(1, "seconds").toDate())
                        // request.input("3", row.form)
                        // request.input("4", row.serial)
                        // await request.query(`insert into ##s_tmp (ic,idt,form,serial) values (@1,@2,@3,@4)`)
                    }
                    await dbs.queries(`insert into ##s_tmp (ic,idt,form,serial) values (@1,@2,@3,@4)`, bindss)
                    let rs, ecs, rows
                    let sqlcheckser = `select a.ic from ##s_tmp a where not exists (select 1 from s_serial where taxc='${taxc}' and form=a.form and serial=a.serial and status=1 and fd<=a.idt)`

                    rs = await request.query(sqlcheckser)
                    rows = rs.recordset
                    if (rows.length > 0) {
                        ecs = []
                        for (const row of rows) {
                            ecs.push(row.ic)
                        }
                        for (const row of result) {
                            if (ecs.includes(row.ic)) row.error += "Dải số hóa đơn không tồn tại." + " (An invoice number range does not exist.) "
                        }
                    }

                    if (GRANT_USR) {
                        let suapp = (SER_USES_APP) ? `and uses in ${SER_USES_APP}` : ``

                        sqlcheckser = `select a.ic from ##s_tmp a where not exists (select 1 from s_serial se, s_seusr seu where se.taxc='${taxc}' and se.form=a.form and se.serial=a.serial and se.status=1 ${suapp} and se.fd<=a.idt and se.id=seu.se and seu.usrid='${token.uid}' )`

                        rs = await request.query(sqlcheckser)
                        rows = rs.recordset
                        if (rows.length > 0) {
                            ecs = []
                            for (const row of rows) {
                                ecs.push(row.ic)
                            }
                            for (const row of result) {
                                if (ecs.includes(row.ic)) row.error += "Dải số hđ không hợp lệ." + " (Invalid invoice number range) "
                            }
                        }
                    } else {
                        if (SER_USES_APP) {
                            sqlcheckser = `select a.ic from ##s_tmp a where not exists (select 1 from s_serial where taxc='${taxc}' and form=a.form and serial=a.serial and status=1 and uses in ${SER_USES_APP} and fd<=a.idt)`

                            rs = await request.query(sqlcheckser)
                            rows = rs.recordset
                            if (rows.length > 0) {
                                ecs = []
                                for (const row of rows) {
                                    ecs.push(row.ic)
                                }
                                for (const row of result) {
                                    if (ecs.includes(row.ic)) row.error += "Kiểu nhập dải số hđ không hợp lệ." + "(Invalid invoice number range input type.)"
                                }
                            }
                        }
                    }

                    rs = await request.query(`select a.ic from ##s_tmp a where exists (select 1 from s_inv where ic=a.ic)`)
                    rows = rs.recordset
                    if (rows.length > 0) {
                        ecs = []
                        for (const row of rows) {
                            ecs.push(row.ic)
                        }
                        for (const row of result) {
                            if (ecs.includes(row.ic)) row.error += "Mã hđ đã tồn tại." + " (Invoice code already exists) "
                        }
                    }

                    rs = await request.query(`with imax as (select form,serial,max(idt) idt from s_inv where stax='${taxc}' and status>1 ${(["sgr"].includes(config.ent)) ? ' and status<>4' : ''} group by form,serial) select a.ic from ##s_tmp a,imax b where a.form=b.form and a.serial=b.serial and cast(b.idt As Date)>cast(a.idt As Date)`)
                    rows = rs.recordset
                    if (rows.length > 0) {
                        ecs = []
                        for (const row of rows) {
                            ecs.push(row.ic)
                        }
                        for (const row of result) {
                            if (ecs.includes(row.ic)) row.error += "Đã có hđ có ngày lớn hơn đã cấp số." + " (Already have a invoice with a larger date that issued numbers.) "
                        }
                    }

                    //replace
                    let binds = []
                    for (const row of result) {
                        if (row.rform && row.rserial && row.rseq) {
                            const rrdt = row.rrdt
                            if (rrdt) {
                                const dt = moment(rrdt, mfd)
                                if (dt.toDate() > now) row.error += "Ngày văn bản lớn hơn hiện tại." + " (Date of text is larger than current)"
                                else row.rrdt = dt.format(dtf)
                                if (moment(row.rrdt).toDate() > moment(row.idt).toDate()) row.error += "Ngày văn bản lớn hơn Ngày hóa đơn thay thế." + " (Date of text is larger than date of the replacement invoice) "
                            }
                            else row.rrdt = row.idt
                            binds.push([row.ic, moment(row.rrdt, dtf).endOf("day").subtract(1, "seconds").toDate(), row.rform, row.rserial, row.rseq])
                        }
                    }

                    if (binds.length > 0) {
                        await request.query(`truncate table ##s_tmp`)
                        for (const bind of binds) {
                            for (let index = 0; index < bind.length; index++) {
                                request.input(index.toString(), bind[index])
                            }
                            await request.query(`insert into ##s_tmp (ic,idt,form,serial,seq) values (@0,@1,@2,@3,@4)`)
                        }
                        rs = await request.query("select form,serial,seq,count(form),count(serial),count(seq) from ##s_tmp group by form,serial,seq having count(form)>1 and count(serial)>1 and count(seq)>1")
                        rows = rs.recordset
                        if (rows.length > 0) {
                            ecs = []
                            for (const row of rows) {
                                ecs.push(`${row.form}-${row.serial}-${row.seq}`)
                            }
                            throw new Error(`HĐ bị thay thế bị trùng : ${ecs.join()} \n (The replacement invoice is identical : ${ecs.join()})`)
                        }
                        rs = await request.query(`select a.ic,b.id,b.idt,b.sec,b.btax,b.bname,JSON_QUERY(b.doc,'$.root') root from ##s_tmp a,s_inv b where b.stax='${taxc}' and b.status=3 and b.form=a.form and b.serial=a.serial and CAST(b.seq AS int)=CAST(a.seq AS int) and a.idt>=b.idt`)
                        rows = rs.recordset
                        if (rows.length > 0) {
                            let ps = {}
                            for (const row of rows) {
                                ps[row.ic] = { id: row.id, idt: row.idt, sec: row.sec, btax: row.btax, bname: row.bname, root: row.root }
                            }
                            for (const row of result) {
                                if (row.rform && row.rserial && row.rseq) {
                                    const p = ps[row.ic]
                                    if (p) {
                                        row.pid = p.id
                                        if (moment(row.idt, dtf).endOf("day").toDate() < p.idt) row.error += "Ngày HĐ thay thế bé hơn ngày HĐ bị thay thế. " + "(The date of the replacement invoice is less than the date of the replaced Invoice)"
                                        row.adj = {
                                            ref: row.rref ? row.rref : row.pid,
                                            rdt: row.rrdt,
                                            rea: row.rrea,
                                            des: `Thay thế cho HĐ số ${row.rseq} ký hiệu ${row.rserial} mẫu số ${row.rform} ngày ${moment(p.idt).format(mfd)} mã ${p.sec}`,
                                            seq: `${row.rform}-${row.rserial}-${row.rseq}`,
                                            idt: p.idt,
                                            typ: 1
                                        }
                                        row.root = util.isEmpty(p.root) ? { sec: p.sec, form: row.rform, serial: row.rserial, seq: row.rseq, idt: moment(p.idt).format(mfd), btax: p.btax, bname: p.bname } : p.root
                                    }
                                    else row.error += "Hđ bị thay thế không tồn tại." + " (The replaced invoice does not exist) "
                                } else row.error += "Thông tin của HĐ bị thay thế không hợp lệ." + " (The information of the replaced invoice is invalid) "
                            }
                        }
                        else {
                            for (const row of result) {
                                if (row.rform && row.rserial && row.rseq) row.error += `Thông tin của HĐ bị thay thế hoặc HĐ thay thế không hợp lệ hoặc sai ngày văn bản.` + ` (Information of the replaced contract or the replacement contract is invalid or the wrong date of document) `
                            }
                        }
                    }
                    //replace
                    //console.timeEnd("chk2")
                }
                if (checkerr()) {

                    //console.time("error")
                    const key = getKeyByValue("ic")
                    let errs = []
                    for (const row of result) {
                        if (row.error) {
                            let err = { error: row.error }
                            err[key] = row.ic
                            errs.push(err)
                        }
                    }
                    let arr1 = [], ic0 = "^_^", ic1
                    for (const a of arr) {
                        ic1 = a[key]
                        if (ic1 === ic0) {
                            arr1.push(a)
                        }
                        else {
                            ic0 = ic1
                            arr1.push({ ...a, ...(errs.find(err => String(err[key]) === String(ic1))) })
                        }
                    }
                    header.error = "Chi tiết lỗi" + " (Error Details)"
                    arr1.unshift(header)
                    const ewb = xlsx.utils.book_new(), ews = xlsx.utils.json_to_sheet(arr1, { skipHeader: 1 })
                    xlsx.utils.book_append_sheet(ewb, ews, sheetName)
                    res.json({ buffer: xlsx.write(ewb, { bookType: "xlsx", type: "buffer" }) })
                    // console.timeEnd("error")
                }
                else {

                    res.json({ data: result })
                }
            }
            // const sSysLogs = { fnc_id: 'xls_upl', src: config.SRC_LOGGING_DEFAULT, dtl: `Đọc file hóa đơn ${file.originalname}`, msg_id: file.originalname};
            // logging.ins(req,sSysLogs,next)
        }
        catch (err) {
            res.json({ err: err.message })
        }
    },
    uplosg: async (req, res, next) => {
        try {
            //console.time("read")
            const file = req.file, body = req.body, type = body.type, token = SEC.decode(req), taxc = token.taxc, now = new Date(), NOW = moment(now).format(dtf)
            // if (token.ou != token.u) {throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)}
            const wb = xlsx.read(file.buffer, { type: "buffer", cellDates: true, dateNF: mfd, sheetRows: config.MAX_ROW_EXCEL }) //cellText: false
            const sheetName = wb.SheetNames[0], ws = wb.Sheets[sheetName]
            for (let i in ws) {
                const wsi = ws[i]
                if (wsi.t == "d") wsi.v = moment(wsi.v, mfd).add(1, 'minutes').format(mfd)
            }
            const opts = ["01GTKT", "02GTTT", "03XKNB"].includes(type) ? { blankrows: false, header: "A", defval: null } : { blankrows: false, range: 1, header: type == "GNS" ? GNS : "A", defval: null }

            const arr = xlsx.utils.sheet_to_json(ws, opts)
            if (util.isEmpty(arr)) throw new Error(`Không đọc được số liệu \n (Can't read the data)`)
            let header
            if (type == "01GTKT") {
                header = arr[0]
                arr.splice(0, 1)
            } else if (type == "02GTTT") {
                header = arr[0]
                arr.splice(0, 1)
            } else if (type == "03XKNB") {
                header = arr[0]
                arr.splice(0, 1)
            }
            //console.timeEnd("read")
            let result = []
            const checkerr = () => {
                for (const row of result) {
                    if (!util.isEmpty(row.error)) return true
                }
                return false
            }
            //console.time("chk1")

            const map = await cache(type), mapkeys = Object.keys(map), getKeyByValue = (v) => { return mapkeys.find(key => map[key] === v) }
            const iform = body.form, iserial = body.serial, VAT = (type === "01GTKT"), schema = await cache_schema(type)
            const dts2 = await cache_dts(type, 2), dts1 = await cache_dts(type, 1), len2 = dts2.length > 0, len1 = dts1.length > 0
            let ic = "^_^", obj, items, countS, rsCount

            for (const r of arr) {
                const row = objectMapper(r, map)
                countS = await dbs.query(`select count(*) count from s_serial where taxc = '${token.taxc}' and form = '${row.form}' and serial = '${row.serial}'`)
                rsCount = countS.recordset
                if (rsCount[0].count == 0) throw new Error("Ký hiệu không tồn tại \n (Serial doesn't exist)")
                if (!row.ic) throw new Error("Thiếu mã hóa đơn \n (Missing invoice code)")
                if (ic == row.ic) {
                    items.push(row.items)
                }
                else {
                    if (obj) {
                        obj.items = items
                        result.push(obj)
                    }
                    obj = row
                    items = [row.items]
                    ic = row.ic
                }
            }
            if (["vcm", "sgr"].includes(config.ent)) {
                for (const row of result) {
                    //const row = objectMapper(r, map)
                    if (!row.form || !row.form.includes(type)) throw new Error("Mẫu hóa đơn không đúng \n (Invoice template is wrong)")
                }
            }

            if (obj) {
                obj.items = items
                result.push(obj)
            }

            for (const row of result) {
                //logger4app.debug(row.ic)
                row.error = ""
                let curr = row.curr, exrt = row.exrt, error = "", i = 1, vat = 0, sum = 0, total = 0, totalv, items = row.items
                if (row.seq && ((row.seq).length < 7 || (row.seq).length > 7) && ent == "sgr") error += "Số hóa đơn phải có 7 ký tự. " + "(The invoice number must have 7 characters) "
                if (row.ic && String(row.ic).length > 50) error += "Độ dài mã hóa đơn vượt quá ký tự cho phép." + "(The invoice code length exceeds the allowed characters) "
                if (row.form && String(row.form).length > 11) error += "Độ dài mẫu số hóa đơn vượt quá ký tự cho phép." + " (The invoice form length exceeds the allowed characters)"
                if (row.serial && String(row.serial).length > 6) error += "Độ dài ký hiệu hóa đơn vượt quá ký tự cho phép." + " (The invoice serial length exceeds the allowed characters) "
                if (config.ent == "sgr") { if (row.seq && String(row.seq).length > 7) error += "Độ dài số hóa đơn vượt quá ký tự cho phép." + " (Invoice number length exceeds allowed characters)" }
                row.ic = row.ic.toString()
                if (!util.isNumber(exrt)) {
                    if (!(["vcm"].includes(config.ent))) {
                        if ((!xls_ccy) || (type == "03XKNB")) {
                            if (!util.isNumber(exrt)) {
                                exrt = 1
                                row.exrt = 1
                            }
                            if (!curr) {
                                curr = 'VND'
                                row.curr = 'VND'
                            }
                            if (curr == 'VND' && exrt !== 1) row.error += "VND tỉ giá phải bằng 1 \n (VND exchange rate must be 1)"
                        } else {
                            if (!curr) curr = 'VND'
                            if (!util.isNumber(exrt)) exrt = 1
                            if (row.curr == 'VND' && row.exrt !== 1) row.error += "VND tỉ giá phải bằng 1 \n (VND exchange rate must be 1)"
                        }
                    }
                }
                if (curr == 'VND' && exrt !== 1) error += "VND tỉ giá phải bằng 1." + "(VND Rate must be by 1)"
                if (row.btax && !util.checkmst(row.btax.toString())) error += "MST không hợp lệ." + " (Invalid Tax Identification Number) "
                if (row.bmail && !util.checkemail(row.bmail)) error += "Email không hợp lệ." + " (Invalid email) "
                if (config.ent == "aia") {
                    if (((!row.bname) || row.bname == null || row.bname == '') && ((!row.buyer) || row.buyer == null || row.buyer == '')) error += "Thiếu Tên đơn vị hoặc Họ tên người mua." + " (Unit Name or Full Buyer Name is missing) "
                    if (((!row.btel) || row.btel == null || row.btel == '') && ((!row.bmail) || row.bmail == null || row.bmail == '')) error += "Thiếu Số điện thoại hoặc Email." + " (Missing Email or Phone Number) "
                }
                if (row.idt) {
                    const idt = moment(row.idt, mfd, true)
                    if (idt.isValid()) {
                        row.idt = idt.format(dtf)
                        if (idt.toDate() > now) error += "Ngày hđ lớn hơn ngày hiện tại." + "(The invoice date is greater than the current date) "
                    }
                    else error += "Ngày hđ sai định dạng." + "(The format of the invoice date is incorrect) "
                }
                else {
                    if (xls_idt) error += "Thiếu ngày hóa đơn." + " (Missing invoice date) "
                    else row.idt = NOW
                }

                if (config.ent == "scb") {
                    if (row.c1) {
                        const subRS = await dbs.query(`select * from s_segment where id = '${row.c1}'`)
                        let cSB = subRS.recordset
                        if (cSB.length == 0) {
                            error += "Không tồn tại nhóm segment này." + " (Doesn't exit this segment group)"
                        }
                    } else {
                        error += "Thiếu nhóm segment." + " (Missing segement group ID)"
                    }
                }

                if (row.orddt) {
                    const orddt = moment(row.orddt, mfd, true)
                    if (orddt.isValid()) {
                        row.orddt = orddt.format(dtf)
                    } else error += "Ngày lệnh sai định dạng." + "(Incorrect command date) "
                }
                if (ent == "sgr") {
                    if (row.c0) {
                        const c0 = moment(row.c0, mfd, true)
                        if (c0.isValid()) {
                            row.c0 = c0.format(dtf)
                        } else error += "Ngày đến sai định dạng." + "(Incorrect check-in date) "
                    }
                    if (row.c1) {
                        const c1 = moment(row.c1, mfd, true)
                        if (c1.isValid()) {
                            row.c1 = c1.format(dtf)
                        } else error += "Ngày đi sai định dạng." + "(Incorrect check-out date) "
                    }
                }
                if (len2) {
                    for (const col of dts2) {
                        if (row[col]) {
                            const idt = moment(row[col], mfd, true)
                            if (idt.isValid()) row[col] = idt.format(dtf)
                            else error += `Cột ${ws[`${getKeyByValue(col)}1`].v} sai định dạng ngày.` + `(Column ${ws[`${getKeyByValue(col)}1`].v} is wrong date format.) `
                        }
                    }
                }
                let catobj = JSON.parse(JSON.stringify(config.CATTAX))
                for (const item of items) {
                    let erri = ""
                    item.line = i++
                    let objtax = catobj.find(x => x.vatCode == item.vatCode)
                    if (objtax) {
                        item.vrt = objtax.vrt
                    } else {
                        erri += "Sai mã loại thuế. (Wrong VAT code)"
                    }

                    //parse về số sgr price
                    try {
                        item.price = Number(item.price)
                    } catch {
                        item.price = item.price
                    }
                    //parse về số sgr quantity
                    try {
                        item.quantity = Number(item.quantity)
                    } catch {
                        item.quantity = item.quantity
                    }
                    //parse về số sgr amount
                    try {
                        item.amount = Number(item.amount)
                    } catch {
                        item.amount = item.amount
                    }
                    item.type = item.type ? item.type.toString().toUpperCase() : ""
                    switch (item.type) {
                        case "KM":
                            item.type = KM
                            break
                        case "CK":
                            item.type = CK
                            break
                        case "MT":
                            item.type = MT
                            break
                        case "MÔ TẢ":
                            item.type = MT
                            break
                        case "KHUYẾN MÃI":
                            item.type = KM
                            break
                        case "CHIẾT KHẤU":
                            item.type = CK
                            break
                        default:
                            item.type = ""
                            break
                    }
                    if (VAT && !util.isNumber(item.vrt)) erri += `thiếu thuế suất. ` + `(Missing tax rates)`
                    if (len1) {
                        for (const col of dts1) {
                            if (item[col]) {
                                const idt = moment(item[col], mfd)
                                if (idt.isValid()) item[col] = idt.format(dtf)
                                else erri += `sai định dạng ngày.` + `(Wrong date format)`
                            }
                        }
                    }
                    if (["scb"].includes(config.ent) && item.c0) {
                        const trandate = moment(item.c0, mfd, true)
                        if (trandate.isValid()) {
                            item.c0 = trandate.format(dtf)
                        } else error += "Ngày giao dịch sai định dạng." + "(Incorrect transaction date) "
                    }
                    let p = item.price, q = item.quantity, amt = 0, vmt = 0, vrt = 0
                    if ("aia".includes(config.ent)) {
                        if (util.isNumber(p) && util.isNumber(q)) item.amount = p * q
                        else {
                            if (!p) delete item.price
                            if (!q) delete item.quantity
                        }
                    }
                    if ("sgr".includes(config.ent)) {
                        if (!xls_data) {
                            if (util.isNumber(p) && util.isNumber(q))
                                item.amount = p * q
                            else {
                                if (!p) delete item.price
                                if (!q) delete item.quantity
                            }
                        }
                    }
                    if (KMMT.includes(item.type)) {
                        item.amount = 0
                        vrt = Number(item.vrt)
                    }
                    else {
                        if (type == "03XKNB" && !item.amount) {
                            item.amount = 0
                            vrt = Number(item.vrt)
                        }
                        if (util.isNumber(item.amount)) {
                            amt = item.amount
                            if (item.type == CK) amt = -amt
                            sum += amt
                            if (VAT) {
                                vrt = Number(item.vrt)
                                if (vrt > 0) {
                                    vmt = (xls_data) ? item.vat : (amt * vrt / 100)
                                    vat += vmt
                                }
                            }
                        }
                        //else erri += `Thiếu thành tiền.`+` (Missing total amount)`
                    }
                    if (!(["vcm", "sgr"].includes(config.ent))) {
                        item.total = amt + vmt
                    } else {
                        if (type == "03XKNB") item.total = amt + vmt
                    }
                    item.total = (xls_data) ? item.total : amt + vmt
                    if (VAT) {
                        if (vrt >= 0) item.vrn = `${vrt}%`
                        else item.vrn = "\\"
                        if (!(["vcm", "sgr"].includes(config.ent))) {
                            item.vat = vmt
                        } else {
                            if (type == "03XKNB") item.vat = vmt
                            item.vat = (xls_data) ? ((item.vat) ? item.vat : 0) : vmt
                        }
                        item.vrt = `${vrt}`
                    }
                    if (!util.isEmpty(erri)) error += `Lỗi chi tiết ${i}: ${erri}.` + `(Error detail ${i}: ${erri})`
                }

                // Duy fix lam tron
                vat = parseFloat(vat.toFixed(2))
                total = sum + vat
                //Làm tạm cho Sun vì bên SAP chưa sửa được. Lúc nào nó sửa xong thì xóa đi
                if ((["sgr"].includes(config.ent))) {
                    try {
                        row.sum = parseFloat(sum.toFixed(2))
                        sum = row.sum
                    } catch (ex) {
                        sum = 0
                        error += `Dữ liệu Thành tiền không hợp lệ (Invalid Amount Data). `
                    }
                    try {
                        row.total = parseFloat(total.toFixed(2))
                        total = row.total
                    } catch (ex) {
                        total = 0
                        error += `Dữ liệu Tổng tiền không hợp lệ (Invalid Total Data). `
                    }
                }
                //Hết đoạn Làm tạm cho Sun vì bên SAP chưa sửa được. Lúc nào nó sửa xong thì xóa đi
                if (!(["vcm", "sgr"].includes(config.ent))) {
                    //Nếu phiếu xuất kho thì gán mặc định sum, total = 0 khi không có giá trị
                    if (type == "03XKNB") {
                        row.sum = (row.sum) ? row.sum : 0
                        row.total = (row.total) ? row.total : 0
                    }
                    try {
                        row.sum = (xls_data) ? parseFloat(row.sum.toFixed(2)) : parseFloat(sum.toFixed(2))
                        sum = row.sum
                    } catch (ex) {
                        sum = 0
                        error += `Dữ liệu Thành tiền không hợp lệ (Invalid Amount Data). `
                    }
                    try {
                        row.total = (xls_data) ? parseFloat(row.total.toFixed(2)) : parseFloat(total.toFixed(2))
                        total = row.total
                    } catch (ex) {
                        total = 0
                        error += `Dữ liệu Tổng tiền không hợp lệ (Invalid Total Data). `
                    }
                }

                if ((["vcm", "sgr"].includes(config.ent))) {
                    //Nếu là phiếu xuất kiêm vận chuyển kho thì gán
                    if (!VAT) {
                        if (!row.sum) row.sum = 0
                        row.total = row.sum
                    }
                }

                if (!(["vcm", "sgr"].includes(config.ent))) {
                    row.sumv = Math.round(row.sum * exrt)
                    totalv = Math.round(row.total * exrt)
                    row.totalv = totalv
                } else {
                    if (util.isNumber(row.sum)) row.sumv = row.sum * exrt
                    if (util.isNumber(row.total)) row.totalv = row.total * exrt
                }
                if (["sgr"].includes(config.ent)) {//tong tien +phi dv+thu dac biet
                    if (row.total) row.total = row.total + (row.c7 ? row.c7 : 0) + (row.c9 ? row.c9 : 0)
                    row.totalv = Math.round(row.total * exrt)
                }

                if (VAT) {
                    if (!(["vcm", "sgr"].includes(config.ent))) {
                        try {
                            row.vat = (xls_data) ? parseFloat(row.vat.toFixed(2)) : vat
                            vat = row.vat
                        } catch (ex) {
                            vat = 0
                            error += `Dữ liệu Thuế suất không hợp lệ (Invalid Tax Data). `
                        }
                        row.vatv = Math.round(vat * exrt)
                    } else {
                        //Làm tạm cho Sun vì bên SAP chưa sửa được. Lúc nào nó sửa xong thì xóa đi
                        if ((["sgr"].includes(config.ent))) {
                            try {
                                row.vat = vat
                                vat = row.vat
                            } catch (ex) {
                                vat = 0
                                error += `Dữ liệu Thuế suất không hợp lệ (Invalid Tax Data). `
                            }
                            row.vatv = Math.round(vat * exrt)
                        }
                        //Hết đoạn Làm tạm cho Sun vì bên SAP chưa sửa được. Lúc nào nó sửa xong thì xóa đi
                        if (util.isNumber(row.vat)) row.vatv = Math.round(row.vat * exrt)

                    }
                    row.tax = await tax(items, exrt)
                }

                if (util.isNumber(row.totalv) && !isNaN(row.totalv)) row.word = n2w.n2w(row.totalv, row.curr)

                //Lấy tổng theo các loại thuế đưa vào đối với VCM
                //if ((["vcm","sgr"].includes(config.ent))  && VAT) 
                //Làm tạm cho Sun vì bên SAP chưa sửa được. Lúc nào nó sửa xong thì thay cho đoạn dưới
                if ((["vcm"].includes(config.ent)) && VAT) {
                    for (let tx of row.tax) {

                        switch (parseInt(tx.vrt)) {
                            case 0:
                                tx.amt = row.sum0
                                tx.amtv = row.sum0
                                break
                            case 5:
                                tx.amt = row.sum5
                                tx.amtv = row.sum5
                                tx.vat = row.vat5
                                tx.vatv = row.vat5
                                break
                            case 10:
                                tx.amt = row.sum10
                                tx.amtv = row.sum10
                                tx.vat = row.vat10
                                tx.vatv = row.vat10
                                break
                            case -1:
                                tx.amt = row.sum1
                                tx.amtv = row.sum1
                                break
                            case -2:
                                tx.amt = row.sum2
                                tx.amtv = row.sum2
                                break
                            default:
                                break
                        }
                    }

                }

                row.stax = taxc
                if (!["vcm", "sgr"].includes(config.ent)) {
                    if (!row.form) row.form = iform
                    if (!row.serial) row.serial = iserial
                }
                if ("aia".includes(config.ent)) {
                    row.c6 = "Thanh lý tài sản, tổng đại lý"
                }
                let docP, rsP, subQR, pay = {}, adj = {}, root = {}, dif = {}
                if (!row.oseq) error += "Thiếu thông tin số hóa đơn gốc cần điều chỉnh" + "(Missing information about parent invoice's number)"
                if (!row.sic) error += "Thiếu thông tin số chứng từ tham chiếu" + "(Missing information about number of reference documents)"
                if (!row.adjust) error += "Thiếu thông tin điều chỉnh" + "(Missing adjustable information)"
                if (row.oseq && row.sic && row.adjust) {
                    let stringSP = (row.oseq).split("#")
                    if (stringSP[0].length != 6 || stringSP[1].length != 7 || !Number(stringSP[1])) {
                        error += "Số hóa đơn gốc cần điều chỉnh sai định dạng" + "(The format of parent invoice's number is incorrect)"
                    }
                    root.serial = stringSP[0]
                    root.seq = stringSP[1]
                    root.form = row.form
                    // if (root.serial != row.serial) throw new Error(`Hóa đơn không cùng ký hiệu với hóa đơn gốc!`) 
                    subQR = await dbs.query(`select doc,status,adjtyp,FORMAT(idt,'dd/MM/yyyy') idt,ou,cde from s_inv where serial='${root.serial}' and seq='${root.seq}' and ic = '${row.sic}'`)
                    rsP = subQR.recordset
                    if (rsP.length == 0) {
                        throw new Error(`Không tìm thấy hóa đơn gốc!`)
                    }
                    if (rsP[0].ou != token.ou) { throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`) }
                    //Check trạng thái tính chất ngày hóa đơn gốc
                    if (!(!rsP[0].adjtyp && !rsP[0].cid && rsP[0].status == 3) || (rsP[0].status == 3 && rsP[0].cde)) {
                        throw new Error(`Chỉ được phép lập hóa đơn điều chỉnh cho hóa đơn gốc, chưa được thay thế và điều chỉnh`)
                    }
                    const idt = moment(row.idt).format("DD/MM/YYYY")
                    const datenew = new Date(idt)
                    const dateorigin = new Date(rsP[0].idt)
                    if (datenew < dateorigin) {
                        throw new Error(`Ngày hóa đơn điều chỉnh phải lớn hơn hoặc bằng ngày hóa đơn gốc`)
                    }
                    docP = JSON.parse(rsP[0].doc)
                    //set pid cho inv
                    row.pid = docP.id
                    //Thẻ root trong inv - LuaDTN
                    root.sec = docP.sec
                    root.idt = docP.idt
                    root.btax = docP.btax
                    root.bname = docP.bname
                    root.tax = docP.tax
                    //Thẻ pay trong root - LuaDTN
                    pay.note = docP.note
                    pay.exrt = docP.exrt
                    pay.curr = docP.curr
                    pay.word = docP.word
                    pay.sum = docP.sum
                    pay.vat = docP.vat
                    pay.total = docP.total
                    pay.sumv = docP.sumv
                    pay.vatv = docP.vatv
                    pay.totalv = docP.totalv
                    pay.score = docP.score
                    root.pay = pay
                    //Gán root cho inv
                    row.root = root
                    //Thẻ adj trong inv
                    adj.typ = 2
                    adj.seq = row.form + "-" + row.serial + "-" + root.seq
                    adj.idt = docP.idt
                    adj.ref = docP.id
                    adj.rdt = obj.idt
                    adj.rea = `${row.adjust} cho hóa đơn số ${root.seq} ký hiệu ${root.serial} mẫu số ${root.form} ngày ${moment(new Date(root.idt)).format("DD/MM/YYYY")}`
                    adj.des = `${row.adjust} cho hóa đơn số ${root.seq} ký hiệu ${root.serial} mẫu số ${root.form} ngày ${moment(new Date(root.idt)).format("DD/MM/YYYY")}`
                    row.des = adj.des
                    //gán adj cho inv
                    row.adj = adj
                    //Thẻ dif trong inv

                    let dc = row.adjust
                    if (dc.toString() === "Điều chỉnh tăng") {
                        dif.sum = row.sum * -1
                        dif.total = row.total * -1
                        dif.sumv = row.sumv * -1
                        dif.totalv = row.totalv * -1
                        dif.vat = row.vat * -1
                        dif.vatv = row.vatv * -1
                    } else {
                        dif.sum = row.sum
                        dif.total = row.total
                        dif.sumv = row.sumv
                        dif.totalv = row.totalv
                        dif.vat = row.vat
                        dif.vatv = row.vatv
                    }
                    row.dif = dif
                }
                row.error = util.isEmpty(error) ? "" : error

            }

            if ("vcm".includes(config.ent)) {
                if (type == "03XKNB") {
                    trans.c0 = "Kho xuất"
                    trans.c1 = "MST xuất"
                    trans.c2 = "Số xe"
                    trans.c3 = "Kho nhập"
                    trans.c4 = "MST nhập"
                    trans._c0 = "Ghi chú"
                    trans_en.c0 = "Export warehouse"
                    trans_en.c1 = "Tax code export"
                    trans_en.c2 = "Vehicle number"
                    trans_en.c3 = "Import warehouse"
                    trans_en.c4 = "Tax code import"
                    trans_en._c0 = "Note"
                }
                if (type == "01GTKT") {
                    trans.c1 = "Điểm tích"
                    trans_en.c1 = "Earning points"
                }
            }
            if ("sgr".includes(config.ent)) {
                if (type == "03XKNB") {
                    trans.c0 = "Kho xuất"
                    trans.c1 = "MST xuất"
                    trans.c2 = "Số xe"
                    trans.c3 = "Kho nhập"
                    trans.c4 = "MST nhập"
                    trans._c0 = "Ghi chú"
                    trans_en.c0 = "Export warehouse"
                    trans_en.c1 = "Tax code export"
                    trans_en.c2 = "Vehicle number"
                    trans_en.c3 = "Import warehouse"
                    trans_en.c4 = "Tax code import"
                    trans_en._c0 = "Note"
                }
                if (type == "01GTKT") {
                    trans.c0 = "Ngày đến"
                    trans.c1 = "Ngày đi"
                    trans.c2 = "Giá Phòng"
                    trans.c3 = "Số Phòng"
                    trans.c4 = "Số lượng khách"
                    trans.c5 = "Mã PNR"
                    trans.c6 = "Thuế suất thuế TTĐB"
                    trans.c7 = "Thuế TTĐB"
                    trans.c8 = "Phí phục vụ (SVC) %"
                    trans.c9 = "Phí dịch vụ"
                    trans_en.c0 = "Arrival date"
                    trans_en.c1 = "Departure date"
                    trans_en.c2 = "Room Rate"
                    trans_en.c3 = "Number of Rooms"
                    trans_en.c4 = "Number of guests"
                    trans_en.c5 = "PNR code"
                    trans_en.c6 = "Special excise tax rate"
                    trans_en.c7 = "Special excise tax"
                    trans_en.c8 = "Service charge (SVC)%"
                    trans_en.c9 = "Service fee"
                }
            }
            if ("aia".includes(config.ent)) {
                trans.c5 = "Khách hàng đăng ký nhận hóa đơn"
                trans_en.c5 = "Customers register to receive invoices"
            }
            if ("scb".includes(config.ent)) {
                trans.c0 = "Hệ thống"
                trans.c9 = "Mã khách hàng"
                trans_en.c0 = "System"
                trans_en.c9 = "Customer Code"
            }
            for (const row of result) {
                const ret = jv.validate(schema, row)

                if (!ret) {
                    let msgArr = []
                    for (let i of jv.errors) {
                        let errorsText = i, prop
                        if (errorsText) prop = errorsText.dataPath.slice(1)
                        if (!prop) prop = errorsText.params.missingProperty
                        else if (prop.endsWith("[0]")) prop = "_".concat(errorsText.params.missingProperty)
                        if (prop.includes("[") || prop.includes("]")) prop = "_".concat(prop.split(".")[1]) || prop
                        switch (errorsText.keyword) {
                            case "required":
                                msgArr.push(`Nhập thiếu thông tin ${trans[prop] || prop} `)
                                msgArr.push(`(Lack of ${trans_en[prop] || prop}).`)
                                break
                            case "enum":
                                msgArr.push(`Dữ liệu ${trans[prop] || prop} không hợp lệ `)
                                msgArr.push(`(Data ${trans_en[prop] || prop} is invalid).`)
                                break
                            case "maxLength":
                                msgArr.push(`Dữ liệu ${trans[prop] || prop} vượt quá ký tự cho phép ${errorsText.params.limit} ký tự `)
                                msgArr.push(`(Data ${trans_en[prop] || prop} exceed allowed characters number ${errorsText.params.limit} characters).`)
                                break
                            case "type":
                                msgArr.push(`${trans[prop] || prop} phải là ${errorsText.params.type == "number" ? 'Số' : errorsText.params.type} `)
                                msgArr.push(`(${trans_en[prop] || prop} should be ${errorsText.params.type == "number" ? 'Number' : errorsText.params.type}).`)
                                break
                            case "minimum":
                                msgArr.push(`Dữ liệu ${trans[prop] || prop} phải lớn hơn ${errorsText.params.limit} `)
                                msgArr.push(`(Data ${trans_en[prop] || prop} should be greater than ${errorsText.params.limit}).`)
                                break
                            default:
                                msgArr.push(i.message)
                                break
                        }
                    }
                    row.error += msgArr.join(", ")
                    // row.error += jv.errorsText()
                }
            }
            //console.timeEnd("chk1")
            if (!checkerr()) {
                //console.time("chk2")
                let bindss = []
                if ((["vcm", "sgr"].includes(config.ent)) && VAT) {
                    for (const row of result) {
                        delete row["sum2"]
                        //delete row["vat0"]
                        delete row["sum0"]
                        delete row["vat5"]
                        delete row["sum5"]
                        delete row["vat10"]
                        delete row["sum10"]
                        delete row["sum1"]
                        //row.word = n2w.n2w(row.totalv, row.curr)
                    }
                }

                const conn = await dbs.getConn()
                const request = await conn.request()
                await request.query(`drop table IF EXISTS ##s_tmp`)
                await request.query(`create table ##s_tmp(ic varchar(50),idt datetime,form varchar(11),serial varchar(6),seq varchar(7))`)
                for (const row of result) {
                    bindss.push([row.ic, moment(row.idt, dtf).endOf("day").subtract(1, "seconds").toDate(), row.form, row.serial])
                    // request.input("1", row.ic)
                    // request.input("2", moment(row.idt, dtf).endOf("day").subtract(1, "seconds").toDate())
                    // request.input("3", row.form)
                    // request.input("4", row.serial)
                    // await request.query(`insert into ##s_tmp (ic,idt,form,serial) values (@1,@2,@3,@4)`)
                }
                await dbs.queries(`insert into ##s_tmp (ic,idt,form,serial) values (@1,@2,@3,@4)`, bindss)
                let rs, ecs, rows
                let sqlcheckser = `select a.ic from ##s_tmp a where not exists (select 1 from s_serial where taxc='${taxc}' and form=a.form and serial=a.serial and status=1 and fd<=a.idt)`

                rs = await request.query(sqlcheckser)
                rows = rs.recordset
                if (rows.length > 0) {
                    ecs = []
                    for (const row of rows) {
                        ecs.push(row.ic)
                    }
                    for (const row of result) {
                        if (ecs.includes(row.ic)) row.error += "Dải số hóa đơn không tồn tại." + " (An invoice number range does not exist.) "
                    }
                }

                if (GRANT_USR) {
                    let suapp = (SER_USES_APP) ? `and uses in ${SER_USES_APP}` : ``

                    sqlcheckser = `select a.ic from ##s_tmp a where not exists (select 1 from s_serial se, s_seusr seu where se.taxc='${taxc}' and se.form=a.form and se.serial=a.serial and se.status=1 ${suapp} and se.fd<=a.idt and se.id=seu.se and seu.usrid='${token.uid}' )`

                    rs = await request.query(sqlcheckser)
                    rows = rs.recordset
                    if (rows.length > 0) {
                        ecs = []
                        for (const row of rows) {
                            ecs.push(row.ic)
                        }
                        for (const row of result) {
                            if (ecs.includes(row.ic)) row.error += "Dải số hđ không hợp lệ." + " (Invalid invoice number range) "
                        }
                    }
                } else {
                    if (SER_USES_APP) {
                        sqlcheckser = `select a.ic from ##s_tmp a where not exists (select 1 from s_serial where taxc='${taxc}' and form=a.form and serial=a.serial and status=1 and uses in ${SER_USES_APP} and fd<=a.idt)`

                        rs = await request.query(sqlcheckser)
                        rows = rs.recordset
                        if (rows.length > 0) {
                            ecs = []
                            for (const row of rows) {
                                ecs.push(row.ic)
                            }
                            for (const row of result) {
                                if (ecs.includes(row.ic)) row.error += "Kiểu nhập dải số hđ không hợp lệ." + "(Invalid invoice number range input type.)"
                            }
                        }
                    }
                }

                rs = await request.query(`select a.ic from ##s_tmp a where exists (select 1 from s_inv where ic=a.ic)`)
                rows = rs.recordset
                if (rows.length > 0) {
                    ecs = []
                    for (const row of rows) {
                        ecs.push(row.ic)
                    }
                    for (const row of result) {
                        if (ecs.includes(row.ic)) row.error += "Mã hđ đã tồn tại." + " (Invoice code already exists) "
                    }
                }

                rs = await request.query(`with imax as (select form,serial,max(idt) idt from s_inv where stax='${taxc}' and status>1 ${(["sgr"].includes(config.ent)) ? ' and status<>4' : ''} group by form,serial) select a.ic from ##s_tmp a,imax b where a.form=b.form and a.serial=b.serial and cast(b.idt As Date)>cast(a.idt As Date)`)
                rows = rs.recordset
                if (rows.length > 0) {
                    ecs = []
                    for (const row of rows) {
                        ecs.push(row.ic)
                    }
                    for (const row of result) {
                        if (ecs.includes(row.ic)) row.error += "Đã có hđ có ngày lớn hơn đã cấp số." + " (Already have a invoice with a larger date that issued numbers.) "
                    }
                }

                //replace
                let binds = []
                for (const row of result) {
                    if (row.rform && row.rserial && row.rseq) {
                        const rrdt = row.rrdt
                        if (rrdt) {
                            const dt = moment(rrdt, mfd)
                            if (dt.toDate() > now) row.error += "Ngày văn bản lớn hơn hiện tại." + " (Date of text is larger than current)"
                            else row.rrdt = dt.format(dtf)
                            if (moment(row.rrdt).toDate() > moment(row.idt).toDate()) row.error += "Ngày văn bản lớn hơn Ngày hóa đơn thay thế." + " (Date of text is larger than date of the replacement invoice) "
                        }
                        else row.rrdt = row.idt
                        binds.push([row.ic, moment(row.rrdt, dtf).endOf("day").subtract(1, "seconds").toDate(), row.rform, row.rserial, row.rseq])
                    }
                }

                if (binds.length > 0) {
                    await request.query(`truncate table ##s_tmp`)
                    for (const bind of binds) {
                        for (let index = 0; index < bind.length; index++) {
                            request.input(index.toString(), bind[index])
                        }
                        await request.query(`insert into ##s_tmp (ic,idt,form,serial,seq) values (@0,@1,@2,@3,@4)`)
                    }
                    rs = await request.query("select form,serial,seq,count(form),count(serial),count(seq) from ##s_tmp group by form,serial,seq having count(form)>1 and count(serial)>1 and count(seq)>1")
                    rows = rs.recordset
                    if (rows.length > 0) {
                        ecs = []
                        for (const row of rows) {
                            ecs.push(`${row.form}-${row.serial}-${row.seq}`)
                        }
                        throw new Error(`HĐ bị thay thế bị trùng : ${ecs.join()} \n (The replacement invoice is identical : ${ecs.join()})`)
                    }
                    rs = await request.query(`select a.ic,b.id,b.idt,b.sec,b.btax,b.bname,JSON_QUERY(b.doc,'$.root') root from ##s_tmp a,s_inv b where b.stax='${taxc}' and b.status=3 and b.form=a.form and b.serial=a.serial and CAST(b.seq AS int)=CAST(a.seq AS int) and a.idt>=b.idt`)
                    rows = rs.recordset
                    if (rows.length > 0) {
                        let ps = {}
                        for (const row of rows) {
                            ps[row.ic] = { id: row.id, idt: row.idt, sec: row.sec, btax: row.btax, bname: row.bname, root: row.root }
                        }
                        for (const row of result) {
                            if (row.rform && row.rserial && row.rseq) {
                                const p = ps[row.ic]
                                if (p) {
                                    row.pid = p.id
                                    if (moment(row.idt, dtf).endOf("day").toDate() < p.idt) row.error += "Ngày HĐ thay thế bé hơn ngày HĐ bị thay thế. " + "(The date of the replacement invoice is less than the date of the replaced Invoice)"
                                    row.adj = {
                                        ref: row.rref ? row.rref : row.pid,
                                        rdt: row.rrdt,
                                        rea: row.rrea,
                                        des: `Thay thế cho HĐ số ${row.rseq} ký hiệu ${row.rserial} mẫu số ${row.rform} ngày ${moment(p.idt).format(mfd)} mã ${p.sec}`,
                                        seq: `${row.rform}-${row.rserial}-${row.rseq}`,
                                        idt: p.idt,
                                        typ: 1
                                    }
                                    row.root = util.isEmpty(p.root) ? { sec: p.sec, form: row.rform, serial: row.rserial, seq: row.rseq, idt: moment(p.idt).format(mfd), btax: p.btax, bname: p.bname } : p.root
                                }
                                else row.error += "Hđ bị thay thế không tồn tại." + " (The replaced invoice does not exist) "
                            } else row.error += "Thông tin của HĐ bị thay thế không hợp lệ." + " (The information of the replaced invoice is invalid) "
                        }
                    }
                    else {
                        for (const row of result) {
                            if (row.rform && row.rserial && row.rseq) row.error += `Thông tin của HĐ bị thay thế hoặc HĐ thay thế không hợp lệ hoặc sai ngày văn bản.` + ` (Information of the replaced contract or the replacement contract is invalid or the wrong date of document) `
                        }
                    }
                }
                //replace
                //console.timeEnd("chk2")
            }
            if (checkerr()) {

                //console.time("error")
                const key = getKeyByValue("ic")
                let errs = []
                for (const row of result) {
                    if (row.error) {
                        let err = { error: row.error }
                        err[key] = row.ic
                        errs.push(err)
                    }
                }
                let arr1 = [], ic0 = "^_^", ic1
                for (const a of arr) {
                    ic1 = a[key]
                    if (ic1 === ic0) {
                        arr1.push(a)
                    }
                    else {
                        ic0 = ic1
                        arr1.push({ ...a, ...(errs.find(err => String(err[key]) === String(ic1))) })
                    }
                }
                header.error = "Chi tiết lỗi" + " (Error Details)"
                arr1.unshift(header)
                const ewb = xlsx.utils.book_new(), ews = xlsx.utils.json_to_sheet(arr1, { skipHeader: 1 })
                xlsx.utils.book_append_sheet(ewb, ews, sheetName)
                res.json({ buffer: xlsx.write(ewb, { bookType: "xlsx", type: "buffer" }) })
                // console.timeEnd("error")
            }
            else {

                res.json({ data: result })
            }
            // const sSysLogs = { fnc_id: 'xls_upl', src: config.SRC_LOGGING_DEFAULT, dtl: `Đọc file hóa đơn ${file.originalname}`, msg_id: file.originalname};
            // logging.ins(req,sSysLogs,next)
        }
        catch (err) {
            res.json({ err: err.message })
        }
    },
    uplupd: async (req, res, next) => {
        try {
            const file = req.file, body = req.body, type = body.type, token = SEC.decode(req), taxc = token.taxc, now = new Date(), NOW = moment(now).format(dtf)
            const wb = xlsx.read(file.buffer, { type: "buffer", cellText: false, cellDates: true, dateNF: "dd/mm/yyyy", sheetRows: config.MAX_ROW_EXCEL })
            const sheetName = wb.SheetNames[0], ws = wb.Sheets[sheetName]
            if (!["GNS", "00ORGS"].includes(type)) {
                for (let i in ws) {
                    if (ws[i].t == "d") ws[i].v = moment(ws[i].v).add(1, 'hours').format(mfd)
                }
            }
            //const opts = { blankrows: false, range: 1, header: type == "GNS" ? GNS : "A", raw: true, defval: null }
            const opts = ["01GTKT", "02GTTT"].includes(type) ? { blankrows: false, header: "A", defval: null } : { blankrows: false, range: 1, header: type == "GNS" ? GNS : "A" }
            const arr = xlsx.utils.sheet_to_json(ws, opts)
            if (util.isEmpty(arr)) throw new Error(`Không đọc được số liệu \n (Can't read the data)`)
            let header
            if (type == "01GTKT") {
                header = arr[0]
                arr.splice(0, 1)
            } else if (type == "02GTTT") {
                header = arr[0]
                arr.splice(0, 1)
            }
            let result = [], error = false
            const checkerr = () => {
                for (const row of result) {
                    if (!util.isEmpty(row.error)) return true
                }
                return false
            }
            if (type) {
                const map = await cache(type)
                if (["01GTKT", "02GTTT"].includes(type)) {
                    const iform = body.form, iserial = body.serial, VAT = (type === "01GTKT"), schema = await cache_schema(type), HDT = await cache_dts(type, 2), DDT = await cache_dts(type, 1)
                    let key = "^_^", obj, items
                    let count = 1, icCol = getKeyByValue(map, "ic")
                    for (let r of arr) {
                        const row = objectMapper(r, map)
                        row.error = ""
                        if (!row.form || !row.serial || !row.seq) throw new Error(`Thiếu thông tin xác định hóa đơn \n (Missing information to identify the invoice)`)
                        row.seq = row.seq.toString().padStart(config.SEQ_LEN, "0")
                        let key2 = row.form + row.serial + row.seq
                        if (key == key2) {
                            r[icCol] = obj.ic
                            items.push(row.items)
                        }
                        else {
                            if (obj) {
                                obj.items = items
                                result.push(obj)
                            }
                            obj = row
                            obj.ic = r[icCol] = "random" + count++
                            items = [row.items]
                            key = key2
                        }
                    }
                    if (obj) {
                        obj.items = items
                        result.push(obj)
                        arr[arr.length - 1][icCol] = obj.ic
                    }
                    for (const row of result) {
                        row.ic = row.ic.toString()
                        if (row.idt) {
                            const idt = moment(row.idt, mfd)
                            if (idt.isValid()) {
                                row.idt = idt.format(dtf)
                                if (idt.toDate() > now) row.error += "Ngày hđ lớn hơn ngày hiện tại." + " (The invoice date is greater than the current date) "

                            }
                            else row.error += "Ngày hđ sai định dạng." + " (The format of the invoice date is incorrect) "
                        }
                        else {
                            row.idt = NOW
                        }
                        for (const hdt of HDT) {
                            let val = row[hdt]
                            if (val) {
                                const idt = moment(val, mfd)
                                if (idt.isValid()) row[hdt] = idt.format(dtf)
                                else row.error += `${hdt} sai định dạng ngày.` + ` (${hdt} is wrong date format)`
                            }
                        }

                        let curr = row.curr, exrt = row.exrt
                        if (!util.isNumber(exrt)) {
                            exrt = 1
                            row.exrt = 1
                        }
                        if (curr == 'VND' && exrt !== 1) throw new Error("VND tỉ giá phải bằng 1 \n (VND Rate must be by 1)")
                        if (row.btax && !util.checkmst(row.btax.toString())) row.error += "MST không hợp lệ." + " (Invalid Tax Identification Number)"
                        if (row.bmail && !util.checkemail(row.bmail)) row.error += "Email không hợp lệ." + "(Invalid email)"
                        let i = 1, vat = 0, sum = 0, total = 0, totalv, items = row.items
                        let catobj = JSON.parse(JSON.stringify(config.CATTAX))
                        for (const item of items) {
                            if (VAT && !util.isNumber(item.vrt)) throw new Error("Thiếu thuế suất \n (Missing tax rates)")
                            for (const ddt of DDT) {
                                if (item[ddt]) {
                                    const idt = moment(item[ddt], mfd)
                                    if (idt.isValid()) item[ddt] = idt.format(dtf)
                                    else throw new Error(`items.${ddt} sai định dạng ngày. \n (items.${ddt} is wrong date format)`)
                                }
                            }
                            let p = item.price, q = item.quantity, amt = 0, vmt = 0, vrt = 0
                            if (util.isNumber(p) && util.isNumber(q)) item.amount = p * q
                            else {
                                if (!p) delete item.price
                                if (!q) delete item.quantity
                            }
                            if (!util.isNumber(item.amount)) throw new Error("Thiếu thành tiền \n (Missing total amount)")
                            item.line = i++
                            let objtax = catobj.find(x => x.vatCode == item.vatCode)
                            if (objtax) {
                                item.vrt = objtax.vrt
                            } else {
                                throw new Error("Sai mã loại thuế \n (Wrong VAT code)")
                            }
                            item.type = item.type ? item.type.toString().toUpperCase() : ""
                            switch (item.type) {
                                case "KM":
                                    item.type = KM
                                    break
                                case "CK":
                                    item.type = CK
                                    break
                                case "MT":
                                    item.type = MT
                                    break
                                default:
                                    item.type = ""
                                    break
                            }
                            if (KMMT.includes(item.type)) item.amount = 0
                            else {
                                amt = item.amount
                                if (item.type == CK) amt = -amt
                                sum += amt
                                if (VAT) {
                                    vrt = Number(item.vrt)
                                    if (vrt > 0) {
                                        vmt = amt * vrt / 100
                                        vat += vmt
                                    }
                                }
                            }
                            item.total = amt + vmt
                            if (VAT) {
                                if (vrt >= 0) item.vrn = `${vrt}%`
                                else item.vrn = "\\"
                                item.vat = vmt
                                item.vrt = `${vrt}`
                            }
                        }
                        // Duy fix lam tron
                        vat = parseFloat(vat.toFixed(2))
                        total = sum + vat
                        row.sum = sum
                        row.total = total
                        row.sumv = Math.round(sum * exrt)
                        totalv = Math.round(total * exrt)
                        row.totalv = totalv
                        row.word = n2w.n2w(totalv, curr)
                        if (VAT) {
                            row.vat = vat
                            row.vatv = Math.round(vat * exrt)
                            row.tax = await tax(items, exrt)
                        }
                        row.stax = taxc
                        if (!row.form) row.form = iform
                        if (!row.serial) row.serial = iserial
                        else if (!(/^(([ABCDEGHKLMNPQRSTUVXY]{2})\/[0-9]{2}E)$/.test(row.serial))) row.error += "Sai định dạng ký hiệu." + " (Wrong serial format) "
                    }
                    for (const row of result) {
                        const ret = jv.validate(schema, row)
                        if (!ret) row.error += jv.errorsText()
                    }
                    error = checkerr()
                    if (!error) {
                        const conn = await dbs.getConn()
                        const request = await conn.request()
                        await request.query(`drop table IF EXISTS ##s_tmp`)
                        await request.query(`create table ##s_tmp(ic varchar(36),form varchar(11),serial varchar(6),seq varchar(7))`)
                        for (const row of result) {
                            request.input("1", row.ic)
                            request.input("2", row.form)
                            request.input("3", row.serial)
                            request.input("4", row.seq.toString().padStart(config.SEQ_LEN, "0"))
                            await request.query(`insert into ##s_tmp (ic,form,serial,seq) values (@1,@2,@3,@4)`)
                        }
                        let rs, rows
                        rs = await request.query(`select a.ic, b.id from ##s_tmp a,s_inv b where b.stax='${taxc}' and b.status=2 and a.form=b.form and a.serial=b.serial and a.seq=b.seq`)
                        rows = rs.recordset
                        let ps = {}
                        for (const row of rows) {
                            ps[row.ic] = row.id
                        }
                        for (const row of result) {
                            const r = ps[row.ic]
                            if (r) {
                                row.pid = r
                            } else {
                                row.error += "Không tìm thấy hóa đơn phù hợp để cập nhật." + " (Cannot find suitable invoice to update)"
                            }
                        }
                        error = checkerr()
                    }
                    if (error) {
                        const key = getKeyByValue(map, "ic")
                        for (const row of result) {
                            if (row.error) {
                                const ic = row.ic
                                for (const a of arr) {
                                    if (a[key] == ic) {
                                        a.error = row.error
                                        break
                                    }
                                }
                            }
                        }
                        for (const a of arr) {
                            a[key] = ""
                        }
                        header.error = "Lỗi" + " (Error)"
                        arr.unshift(header)
                        const ewb = xlsx.utils.book_new()
                        const ews = xlsx.utils.json_to_sheet(arr, { skipHeader: 1 })
                        xlsx.utils.book_append_sheet(ewb, ews, sheetName)
                        const buffer = xlsx.write(ewb, { bookType: 'xlsx', type: "buffer" })
                        res.json({ buffer: buffer })
                    }
                    else {
                        for (const a of result) {
                            a.ic = ""
                        }
                        res.json({ data: result })
                    }
                }
            }
            const sSysLogs = { fnc_id: 'xls_upl', src: config.SRC_LOGGING_DEFAULT, dtl: `Đọc file hóa đơn ${file.originalname}`, msg_id: file.originalname };
            logging.ins(req, sSysLogs, next)
        }
        catch (err) {
            res.json({ err: err.message })
        }
    }
}
module.exports = Service