"use strict"
const Minizip = require('minizip-asm.js')
var generator = require('generate-password')
var genpass = require('../genpass')
const handlebars = require("handlebars")
const dbs = require("./dbs")
const ext = require("../ext")
const util = require(`../util`)
const ous = require(`./ous`)
const config = require("../config")
const logger4app = require("../logger4app")
var fs = require('fs')
var sevenBin = require('7zip-bin')
var seven = require('node-7z')
const pathTo7zip = sevenBin.path7za
var uuid = require('uuid');
const moment = require("moment")
const path = require("path")
const dbtype = config.dbtype
const sendmail_pdf_xml_notzip = config.sendmail_pdf_xml_notzip
const genPassFile=config.genPassFile
var archiver = require('archiver');
archiver.registerFormat('zip-encryptable', require('archiver-zip-encryptable'));
const sql_view = {
    sql_view_mssql: `select xml, doc from s_inv where id=@1`,
    sql_view_mysql: `select xml xml, doc doc from s_inv where id=?`,
    sql_view_orcl: `select xml "xml", doc "doc" from s_inv where id = :id`,
    sql_view_pgsql: `select xml, doc from s_inv where id=$1`
}

const execsqlselect = (sql, binds) => {
    return new Promise(async (resolve, reject) => {
        try {
            const result = await dbs.query(sql, binds)
            let rows
            switch (dbtype) {
                case "mssql":
                    rows = result.recordset
                    break
                case "mysql":
                    rows = result[0]
                    break
                case "orcl":
                    rows = result.rows
                    break
                case "pgsql":
                    rows = result.rows
                    break
            }
            resolve(rows)
        } catch (err) {
            reject(err)
        }
    })
}
const create_zip = (file, doc,pass) => {
    return new Promise(async (resolve, reject) => {
        try {
           
            let filename = `${doc.c9}-${doc.curr}-${doc.seq}`.split("/").join(".")
            const xml = fs.readFileSync(`${file}.xml`)
            const pdf = fs.readFileSync(`${file}.pdf`)
            var output = fs.createWriteStream(`${file}.zip`);
            
            var archive = archiver('zip-encryptable', {
                zlib: { level: 9 },
                forceLocalTime: true,
                password: pass,
                encryptionMethod: 'zip20'
            });
            
            archive.pipe(output);
            archive.append(xml, { name: `${filename}.xml` });
            archive.append(pdf, { name: `${filename}.pdf` });
           
            archive.finalize();
           
             output.on("finish", () => { resolve(true) });
          
           /// resolve('output.getContents()')
        } catch (err) {
            reject(err)
        }
    })
}
const renderAsync = async (json) => {
    let items = json.data.items
    for (let i of items) {
        i.price = i.price || ""
        i.quantity = i.quantity || ""
        i.total = i.total || ""
        //i.vat = i.vat || ""
    }
    let bodyBuffer = await util.jsReportRenderAttach(json)
    
    return bodyBuffer
    
}

const create7zip = async (pdf, xml, password, filename) => {
    //VinhHQ code them chuc nang gen file 7z voi cac tham so config_7zip
    let file, config_7zip = config.config_7zip, vuuid = uuid.v4()
    let filePDF = config_7zip.PathFileToZip + vuuid + ".pdf", fileXML = config_7zip.PathFileToZip + vuuid + ".xml",
        file7z = config_7zip.PathSave7zipFile + vuuid + ".7z", tmpfile
    try {
        //Tao file tam tu cac bien pdf, xml va luu vao thu muc config_7zip.PathFileToZip, ten file su dung uuid de tao
        let PDFFile = fs.openSync(filePDF, 'w')
        logger4app.debug(`create7zip `, filename, filePDF)
        fs.writeFileSync(PDFFile, pdf, (err) => {
            if (err) throw err;
        })
        fs.fsyncSync(PDFFile);
        fs.closeSync(PDFFile);

        let XMLFile = fs.openSync(fileXML, 'w')
        logger4app.debug(`create7zip `, filename, fileXML)
        fs.writeFileSync(XMLFile, xml, (err) => {
            if (err) throw err;
        })
        fs.fsyncSync(XMLFile);
        fs.closeSync(XMLFile);
        //Chuyen thanh file 7z lu tai thu muc config_7zip.PathSave7zipFile, su dung uuid de dat ten file
        logger4app.debug(`create7zip `, filename, file7z)
        var myStream = await seven.add(file7z, [filePDF, fileXML], {
            recursive: true,
            $bin: pathTo7zip,
            password: password,
            deleteFilesAfter: true
        })

        // myStream = seven.rename(file7z, [
        //     [`${vuuid}.pdf`, `${filename}.pdf`],
        //     [`${vuuid}.xml`, `${filename}.xml`]
        //   ])

        //Luu noi dung file vao bien file de tra ra

        try {
            do {

            } while (fs.existsSync(fileXML) && fs.existsSync(filePDF))

            tmpfile = fs.readFileSync(file7z)
            file = new Buffer(tmpfile).toString('base64')
            logger4app.debug(`create7zip return base64`, filename, file)

        } catch (ex) {
            logger4app.error(ex)
        }

    } catch (err) {
        //Throw ra loi
        throw new Error(err)
    } finally {
        try {
            logger4app.debug(`create7zip delete zip file`, filename, file7z)
            fs.unlinkSync(file7z)
        } catch (ex) {
            logger4app.error(ex)
        }
    }
    return file
}

const Service = {
    getFileMail: async (id) => {    
        try {
            let sql, rows, row, binds = [id], file = {}, doc
            let arrfile = [] //Khởi tạo mảng lưu file trả ra
            sql = sql_view[`sql_view_${dbtype}`]
            rows = await execsqlselect(sql, binds)
            if (rows.length) {
                row = rows[0]
                file.xml = row.xml
                doc = JSON.parse(row.doc)
            }
            //Create PDF
            const status = doc.status, org = await ous.obt(doc.stax), idx = org.temp
            if (!([3, 4].includes(status))) doc.sec = ""
            const fn = util.fn(doc.form, idx), tmp = await util.template(fn)
            let obj = { status: status, doc: doc, tmp: tmp }
            let reqjsr = ext.createRequest(obj)
            file.pdf = await renderAsync(reqjsr)
            
            //var password = generator.generate({length: 10,numbers: true, symbols:true,lowercase:true,uppercase:true,strict:true})
            var password = await genpass.generate(doc, {})
            let filename = `${doc.form}-${doc.serial}-${doc.seq}`.split("/").join(".")
            //Tạo object password
            let passwordobj = {}
            //Nếu có password thì gán vào object password
            if (genPassFile) {
                passwordobj = {password: password}
            }
            // Kiểm tra tham số sendmail_pdf_xml_notzip, nếu = 0 thì zip
            if (!sendmail_pdf_xml_notzip) {
                let fzip, extfile = 'zip'
                if (!config.config_7zip) {
                    //Create Zip
                    var mz = new Minizip()
                    // mz.append("haha/abc.txt", "duy ham", {password: "~~~"})
                    if (file.xml) mz.append(`${filename}.xml`, file.xml, passwordobj)
                    if (file.pdf) mz.append(`${filename}.pdf`, file.pdf, passwordobj)
                    fzip = mz.zip()
                    fzip = fzip.toString("base64")
                } else {
                    fzip = await create7zip(file.pdf, file.xml, passwordobj.password, filename)
                    extfile = '7z'
                }
                //Create mau password
                const source = await util.tempmail(-2)
                const template = handlebars.compile(source)
                const htmPassword = (genPassFile) ? template({ ...doc, ...{ password, filename: `${filename}.${extfile}` } }) : null
                const subjectMailPassword = (genPassFile) ? (await util.getSubjectMailPassword({ ...doc, ...{ password, filename: `${filename}.${extfile}` } }, '')) : null
                arrfile.push({ content: fzip, encoding: 'base64', subjectmailpass: subjectMailPassword, password: htmPassword, passwordtext: password, filename: `${filename}.${extfile}`, filetype: `${extfile}` })
            } //Nếu không thì không zip và chẳng đặt mật khẩu
            else {
                const htmPassword = null
                arrfile.push({content:Buffer.from(file.xml, 'utf8').toString('base64'), encoding: 'base64', password:htmPassword, passwordtext: password, filename:`${filename}.xml`, filetype: 'xml'})
                arrfile.push({content:file.pdf.toString("base64"), encoding: 'base64', password:htmPassword, passwordtext: password, filename:`${filename}.pdf`, filetype: 'pdf'})
            }
            
            let res = arrfile
            return res
        }
        catch (err) {
            throw new Error(err)
        }
    },
    getFileMailPass: async (id,pass) => {    
        try {
            let sql, result, row, binds = [id], file ,doc,filename
            sql = `select file_name,doc from s_inv where id=@1`
            result = await dbs.query(sql, binds)
            if (result.recordset.length) {
                row = result.recordset[0]
                file = row.file_name
                doc = JSON.parse(row.doc)
            }

            let a =  await create_zip(file,doc,pass)
            if(config.ent=='mzh') filename = `${doc.c9}-${doc.curr}-${doc.seq}`.split("/").join(".")
           
            const zip = fs.readFileSync(`${file}.zip`)
            filename = [`${filename}.zip`].join('');
            let res = {zip:zip, filename:`${filename}`}
            return res
        }
        catch (err) {
            throw new Error(err)
        }
    },
    getFileMailToFolder: async (id) => {    
        try {
            let sql, result, row, binds = [id], file = {}, doc,idt
            sql = `select xml, doc,idt from s_inv where id=@1`
            result = await dbs.query(sql, binds)
            if (result.recordset.length) {
                row = result.recordset[0]
                file.xml = row.xml
                doc = JSON.parse(row.doc)
                idt = row.idt
            }
            //Create PDF
            const status = doc.status, org = await ous.obt(doc.stax), idx = org.temp
            if (!([3, 4].includes(status))) doc.sec = ""
            const fn = util.fn(doc.form, idx), tmp = await util.template(fn)
            let obj = { status: status, doc: doc, tmp: tmp }
            let reqjsr = ext.createRequest(obj)
            file.pdf = await renderAsync(reqjsr)
            //Create Zip
            //var mz = new Minizip()
            // mz.append("haha/abc.txt", "duy ham", {password: "~~~"})
          //  var password = generator.generate({length: 10,numbers: true, symbols:true,lowercase:true,uppercase:true,strict:true})
            let filename = `${doc.form}-${doc.serial}-${doc.seq}`.split("/").join(".")
            if(config.ent=='mzh') filename = `${doc.c9}-${doc.curr}-${doc.seq}`.split("/").join(".")
            logger4app.debug('creating ' + filename);
            const year =(moment(idt)).format('YYYY')
            const Month = (moment(idt)).format('MMM')
            const day = (moment(idt)).format('DD')
            var dir = config.path_zip + `${year}/${Month}/${day}/`
            if (!fs.existsSync(config.path_zip + `${year}`)){
                fs.mkdirSync(config.path_zip + `${year}`);
            }
            if (!fs.existsSync(config.path_zip + `${year}/${Month}`)){
                fs.mkdirSync(config.path_zip + `${year}/${Month}`);
            }
            if (!fs.existsSync(config.path_zip + `${year}/${Month}`)){
                fs.mkdirSync(config.path_zip + `${year}/${Month}`);
            }
            if (!fs.existsSync(config.path_zip + `${year}/${Month}/${day}`)){
                fs.mkdirSync(config.path_zip + `${year}/${Month}/${day}`);
            }
            fs.writeFileSync(dir + `${filename}.xml`, file.xml);
            fs.writeFileSync(dir +`${filename}.pdf`, file.pdf);
            
            
          return config.path_zip + `${year}/${Month}/${day}/`+filename
            //
           
        }
        catch (err) {
            throw new Error(err)
        }
    },
    getFileMailFromJob: async (doc,xml) => {
        try {
            let sql, rows, row, binds = [doc.id], file = {xml: xml}
            let arrfile = [] //Khởi tạo mảng lưu file trả ra
            sql = sql_view[`sql_view_${dbtype}`]
            rows = await execsqlselect(sql, binds)
            if (rows.length) {
                row = rows[0]
                file.xml = (xml) ? xml : row.xml
                //doc = JSON.parse(row.doc)
            }
            //Create PDF
            const status = doc.status, org = await ous.obt(doc.stax), idx = org.temp
            if (!([3, 4].includes(status))) doc.sec = ""
            const fn = util.fn(doc.form, idx), tmp = await util.template(fn)
            let obj = { status: status, doc: doc, tmp: tmp }
            let reqjsr = ext.createRequest(obj)
            file.pdf = await renderAsync(reqjsr)
            
            //var password = generator.generate({length: 10,numbers: true, symbols:true,lowercase:true,uppercase:true,strict:true})
            var password = await genpass.generate(doc, {})
            let filename = `${doc.form}-${doc.serial}-${doc.seq}`.split("/").join(".")
            //Tạo object password
            let passwordobj = {}
            //Nếu có password thì gán vào object password
            if (genPassFile) {
                passwordobj = {password: password}
            }
            // Kiểm tra tham số sendmail_pdf_xml_notzip, nếu = 0 thì zip
            if (!sendmail_pdf_xml_notzip) {
                let fzip, extfile = 'zip'
                if (!config.config_7zip) {
                    //Create Zip
                    var mz = new Minizip()
                    // mz.append("haha/abc.txt", "duy ham", {password: "~~~"})
                    if (file.xml) mz.append(`${filename}.xml`, file.xml, passwordobj)
                    if (file.pdf) mz.append(`${filename}.pdf`, file.pdf, passwordobj)
                    fzip = mz.zip()
                    fzip = fzip.toString("base64")
                } else {
                    fzip = await create7zip(file.pdf, file.xml, passwordobj.password, filename)
                    extfile = '7z'
                }
                //Create mau password
                const source = await util.tempmail(-2)
                const template = handlebars.compile(source)
                const htmPassword = (genPassFile) ? template({ ...doc, ...{ password, filename: `${filename}.${extfile}` } }) : null
                const subjectMailPassword = (genPassFile) ? (await util.getSubjectMailPassword({ ...doc, ...{ password, filename: `${filename}.${extfile}` } }, '')) : null
                arrfile.push({ content: fzip, encoding: 'base64', subjectmailpass: subjectMailPassword, password: htmPassword, passwordtext: password, filename: `${filename}.${extfile}`, filetype: `${extfile}` })
            } //Nếu không thì không zip và chẳng đặt mật khẩu
            else {
                const htmPassword = null
                arrfile.push({content:Buffer.from(file.xml, 'utf8').toString('base64'), encoding: 'base64', password:htmPassword, passwordtext: password, filename:`${filename}.xml`, filetype: 'xml'})
                arrfile.push({content:file.pdf.toString("base64"), encoding: 'base64', password:htmPassword, passwordtext: password, filename:`${filename}.pdf`, filetype: 'pdf'})
            }
            
            let res = arrfile
            return res
        }
        catch (err) {
            throw new Error(err)
        }
    }
}
module.exports = Service