
"use strict"
const moment = require("moment")
const sec = require("../sec")
const dbs = require("./dbs")
const util = require("../util")
const config = require("../config")
const logger4app = require("../logger4app")
const UPLOAD_MINUTES = config.upload_minute
const mfd = config.mfd
const ENT = config.ent
const formatItems = (rows) => {
    for (const row of rows) {
		if (!row.unit) row.unit = ""
        row.price = util.fn2(row.price)
        row.amount = util.fn2(row.amount)
        if (!row.quantity) row.quantity = ""
        row.name=String(row.name).replace("<br>","")
    }
    return rows
}
const Service = {
    minutesReplace: async (req, res, next) => {
        try {
            const token = sec.decode(req), id = req.body.id, adj = req.body.inv.adj
            const result = await dbs.query(`select doc from s_inv where id=@1`, [id])
            const doc = JSON.parse(result.recordset[0].doc)
            doc.idt = moment(doc.idt).format(mfd)
            doc.items = formatItems(doc.items)
            doc.vatv = util.fn2(doc.vatv)
            doc.sumv = util.fn2(doc.sumv)
            doc.totalv = util.fn2(doc.totalv)
            doc.ref = adj.ref
            doc.rea = adj.rea
            doc.btel= (doc.btel) ? doc.btel : ''
            doc.bmail = (doc.bmail) ? doc.bmail : ''
            doc.btax= (doc.btax) ? doc.btax : ''
            doc.rdt = moment(adj.rdt).format(mfd)
            doc.isvat = doc.type == "01GTKT" ? true : false
            let file_name=""
            if (ENT=="vcm") file_name="VCM-BBThaythe.docx"
            else file_name= doc.type.toLowerCase() + "mrep.docx"
            const out = util.docxrender(doc, file_name)
            res.end(out, "binary")
        } catch (err) {
            next(err)
        }
    },
    minutesCancel: async (req, res, next) => {
        try {
            const doc = req.body, adj = doc.adj
            doc.idt = moment(doc.idt).format(mfd)
            doc.items = formatItems(doc.items)
            doc.vatv = util.fn2(doc.vatv)
            doc.sumv = util.fn2(doc.sumv)
            doc.totalv = util.fn2(doc.totalv)
            doc.ref = adj.ref
            doc.rea = adj.rea
            doc.btel= (doc.btel) ? doc.btel : ''
            doc.bmail = (doc.bmail) ? doc.bmail : ''
            doc.btax= (doc.btax) ? doc.btax : ''
            doc.rdt = moment(adj.rdt).format(mfd)
            doc.isvat = doc.type == "01GTKT" ? true : false
            let file_name=""
            if (ENT=="vcm") file_name="VCM-BBHuy.docx"
            else file_name= doc.type.toLowerCase() + "mcan.docx"
            const out = util.docxrender(doc, file_name)
            res.end(out, "binary")
        } catch (err) {
            next(err)
        }
    },
    minutesCreate: async (req, res, next) => {
        try {
            const doc = req.body, adj = doc.adj
            doc.idt = moment(doc.idt).format(mfd)
            doc.ref = adj.ref
            doc.rea = adj.rea
            doc.des = adj.des
			doc.btel= (doc.btel) ? doc.btel : ''
            doc.bmail = (doc.bmail) ? doc.bmail : ''
            doc.btax= (doc.btax) ? doc.btax : ''
            doc.rdt = moment(adj.rdt).format(mfd)
            doc.items0 = formatItems(doc.items0)
            doc.items1 = formatItems(doc.items1)
            doc.isvat = doc.type == "01GTKT" ? true : false
            if (!doc.seq) doc.seq = "............"
            let file_name=""
            if (ENT=="vcm"){
                file_name="VCM-BBDieuchinh.docx"
                doc.root.idt = moment(doc.root.idt).format(mfd)
            } 
            else file_name= doc.type.toLowerCase() + "madj.docx"
            const out = util.docxrender(doc, file_name)
            res.end(out, "binary")
        } catch (err) {
            next(err)
        }
    },
    minutesCustom: async (req, res, next) => {
        try {
            const token = sec.decode(req), id = req.params.id, download = req.query.download
            let result = await dbs.query(`select pid,type,status,adjtyp,doc,ic from s_inv where id=@1`, [id])
            let row = result.recordset[0],ic = row.ic, type = row.type, status = row.status, doc = JSON.parse(row.doc), fn, pid = row.pid
            if (UPLOAD_MINUTES && download) {
                let typ, iid = pid
                if (status == 4) {
                    typ = "can"
                    iid = id
                } else {
                    let adjtyp = row.adjtyp
                    if (adjtyp == 1) {
                        typ = "rep"
                    } else if (adjtyp == 2) {
                        typ = "adj"
                    }
                }
                if (!typ) {
                    iid = id
                    typ = "normal"
                }
                let minutes = await dbs.query(`select id, type, content from s_inv_file where id=@1 and type=@2`, [iid, typ])
                if (!minutes.recordset.length) throw new Error("Không tìm thấy biên bản! \n (Cannot found minutes)")
                let file = Buffer.from(minutes.recordset[0].content,"base64")
                res.end(file, "binary")
            } else {
                if (status == 4) {
                    let cancel = doc.cancel
                    if (!cancel) throw new Error(`Hóa đơn không có biên bản. \n (Invoices don't have Minutes)`)
                    doc.idt = moment(doc.idt).format(mfd)
                    doc.items = formatItems(doc.items)
                    doc.vatv = util.fn2(doc.vatv)
                    doc.sumv = util.fn2(doc.sumv)
                    doc.totalv = util.fn2(doc.totalv)
                    doc.ref = cancel.ref
                    doc.rea = cancel.rea
                    doc.cdt_cancel = moment(new Date()).format(mfd)
                    doc.cancel_rea = ic
					doc.btel= (doc.btel) ? doc.btel : ''
					doc.bmail = (doc.bmail) ? doc.bmail : ''
					doc.btax= (doc.btax) ? doc.btax : ''
                    doc.rdt = moment(cancel.rdt).format(mfd)
                    doc.isvat = type == "01GTKT" ? true : false
                    if (ENT=="vcm") fn="VCM-BBHuy.docx"
                    else if(ENT == "kbank") fn="01gtktmcan_kbank.docx"
                    else fn= doc.type.toLowerCase() + "mcan.docx"
                   // fn = "mcan.docx"
                }
                else {
                    let adjtyp = row.adjtyp
                    if (adjtyp && pid) {
                        let adj = doc.adj
                        result = await dbs.query(`select doc from s_inv where id=@1`, [pid])
                        row = result.recordset[0]
                        let pdoc = JSON.parse(row.doc)
                        if (adjtyp == 1) {
                            doc = pdoc
                            doc.idt = moment(doc.idt).format(mfd)
                            doc.items = formatItems(doc.items)
                            doc.vatv = util.fn2(doc.vatv)
                            doc.sumv = util.fn2(doc.sumv)
                            doc.totalv = util.fn2(doc.totalv)
                            doc.ref = adj.ref
                            doc.rea = adj.rea
							doc.btel= (doc.btel) ? doc.btel : ''
							doc.bmail = (doc.bmail) ? doc.bmail : ''
							doc.btax= (doc.btax) ? doc.btax : ''
                            doc.rdt = moment(adj.rdt).format(mfd)
                            doc.isvat = type == "01GTKT" ? true : false
                            if (ENT=="vcm") fn="VCM-BBThaythe.docx"
                           
                            else fn= doc.type.toLowerCase() + "mrep.docx"
                          //  fn = "mrep.docx"
                        }
                        else if (adjtyp == 2) {
                            let items1 = doc.items, items0 = pdoc.items
                            doc.idt = moment(doc.idt).format(mfd)
                            doc.ref = adj.ref
                            doc.rea = adj.rea
                            doc.des = adj.des
							doc.btel= (doc.btel) ? doc.btel : ''
							doc.bmail = (doc.bmail) ? doc.bmail : ''
							doc.btax= (doc.btax) ? doc.btax : ''
                            doc.rdt = moment(adj.rdt).format(mfd)
                            doc.items0 = formatItems(items0)
                            doc.items1 = formatItems(items1)
                            doc.isvat = type == "01GTKT" ? true : false
                            if (!doc.seq) doc.seq = "............"
                            if (ENT=="vcm"){
                                fn="VCM-BBDieuchinh.docx"
                                doc.root.idt = moment(doc.root.idt).format(mfd)
                            } else
                            fn = doc.type.toLowerCase() + "madj.docx"
                        }
                    }
                }
                if (fn) {
                    for (let i in doc) if (util.isEmpty(doc[i])) doc[i] = ""
                    for (let row of doc.items) 
                    {
                        for (let i in row)
                        {
                            if (util.isEmpty(row[i])) row[i] = ""
                        }
                       
                        if (typeof  row.unit=="undefined") 
                        {
                            row.unit="";
                        }
                    }
                    const out = util.docxrender(doc, fn)
                    res.end(out, "binary")
                }
                else {
                    throw new Error(`Hóa đơn không có biên bản. \n (Invoices don't have Minutes)`)
                }
            }

        } catch (err) {
            next(err)
        }
    },
    upload: async (req, res, next) => {
        let ok = false,err
        try {
            const token = sec.decode(req), taxc = token.taxc
            const file = req.file, body = req.body, type = body.type.toString().toLowerCase(), id = body.id
            let content = file.buffer, contentb64 = content.toString('base64')
            let sql, result, binds
            sql = `select 1 from s_inv_file where id=@1 and type=@2`
            binds = [id, type]
            result = await dbs.query(sql, binds)
            if (result.recordset.length) {
                sql = "update s_inv_file set id=@1, type=@2, content=@3 where id=@4 and type=@5"
                binds = [id, type, contentb64, id, type]
            } else {
                sql = "insert into s_inv_file (id, type, content) VALUES (@1,@2,@3)"
                binds = [id, type, contentb64]
            }
            result = await dbs.query(sql, binds)
            if (result.rowsAffected[0]) ok=true
        } catch (error) {
            err = error.message
        }
        res.json({ok, err})
    }
}
module.exports = Service 