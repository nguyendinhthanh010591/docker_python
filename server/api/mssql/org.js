"use strict"
const config = require("../config")
const logger4app = require("../logger4app")
const dbs = require("./dbs")
const logging = require("../logging")
const ENT = config.ent
const Service = {
    fbi: async (req, res, next) => {
        try {
            const result = await dbs.query(`select id,taxc,name,fadd,mail,tel,acc,bank,code from s_org where id=@1`, [req.params.id])
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    ous: async (req, res, next) => {
        try {
            const query = req.query, filter = query.filter
            let rows
            if (filter && filter.value) {
                    const sql = `select id,name,taxc,addr,mail,tel,acc,bank,prov,dist,ward,fadd,code from s_org where contains(*,@1) order by taxc OFFSET 0 ROWS fetch first ${config.limit} rows only`
                    const result = await dbs.query(sql, [ `"${filter.value}"`])
                    rows = result.recordset
            }
            res.json(rows)
        }
        catch (err) {
            next(err)
        }
    },
    fbn: async (req, res, next) => {
        try {
            const query = req.query, filter = query.filter
            let rows
            if (filter && filter.value)  {
                    const sql = `select taxc,name,fadd,mail,tel,acc,bank,code from s_org where contains(*,@1) order by taxc OFFSET 0 ROWS fetch first ${config.limit} rows only`
                    const result = await dbs.query(sql, [`"${filter.value}"`])
                    rows = result.recordset
            }
            res.json(rows)
        }
        catch (err) {
            next(err)
        }
    },
    get: async (req, res, next) => {
        try {
            const query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 10,sort = query.sort
            let rows, where='',sql,result, order = "",where1=''
            let name= query.name, code= query.code, addr= query.addr

            
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = ` order by ${key} ${sort[key]}`
                })
            } else order = ` order by taxc `
            if (code || name || addr) {
                if(`${config.dbtype}` == "pgsql") {
                    name = name.trim()
                    let phrase
                    if (name.indexOf(' ') == -1) {
                        name = `${name}:*`
                        phrase = ""
                    }
                    else phrase = "phrase"
                    where_get[where_get_pgsql] = `,${phrase}to_tsquery($1) tsq`
                }
                // where = `${where_get[`where_get_${config.dbtype}`]}`
                if(code != '') where1 += `code like '%${code}%'`
                if(name != '') where1 += ` and name like N'%${name}%'`
                if(addr != '') where1 += ` and addr like N'%${addr}%'`
                
                if(where1.startsWith(' and')) where = where1.slice(5, where1.length)
                else where = where1
                where = 'where ' + where
                console.log(where)
                let binds = []
                // binds.push(name)
                for (let t of pagerobj.vsqlpagerbind) binds.push(t)
                sql = `select id "id",code "code",taxc "taxc",name "name",name_en "name_en",addr "addr",prov "prov",dist "dist",ward "ward",mail "mail",tel "tel",acc "acc",bank "bank",fadd "fadd",c0 "c0",c1 "c1",c2 "c2",c3 "c3",c4 "c4",c5 "c5",c6 "c6",c7 "c7",c8 "c8",c9 "c9",type "type" from s_org ${where} ${order} ${pagerobj.vsqlpagerstr}`
                result = await inc.execsqlselect(sql, binds)
               
                rows = result
            } else {
                 sql = `select id,code,taxc,name,name_en,addr,prov,dist,ward,mail,tel,acc,bank,fadd,c0,c1,c2,c3,c4,c5,c6,c7,c8,c9,type from s_org ${order} OFFSET  @1 ROWS FETCH NEXT @2 ROWS ONLY`
                 result = await inc.execsqlselect(sql, pagerobj.vsqlpagerbind)
               
                rows = result.recordset
            }
            let ret = { data: rows, pos: start }
            if (start == 0) {
                if (name || code || addr){
                    sql = `select count(*) total from s_org where ${where}`
                    result = await inc.execsqlselect(sql, [])
                }else  {
                    sql = `select count(*) total from s_org `
                    result = await inc.execsqlselect(sql, [])
                }
               
                ret.total_count = result.recordset[0].total
            }
            return res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const body = req.body, operation = body.webix_operation
            let binds, sql, result, sSysLogs = { fnc_id: '', src: config.SRC_LOGGING_DEFAULT, dtl: ``}
            let email = ''
            email = body.mail
            //mails ngăn cách với nhau bằng dấu ; với ctbc
            if( ENT == 'ctbc') email = email.replace(/,/g, ';')
            const taxc = body.taxc ? body.taxc : null, code = body.code ? body.code : null
            if (operation == "update") {
                sql = `update s_org set code=@1,taxc=@2,name=@3,name_en=@4,prov=@5,dist=@6,ward=@7,addr=@8,mail=@9,tel=@10,acc=@11,bank=@12,c0=@13,c1=@14,c2=@15,c3=@16,c4=@17,c5=@18,c6=@19,c7=@20,c8=@21,c9=@22 where id=@23`
                binds = [code, taxc, body.name, body.name_en, body.prov, body.dist, body.ward, body.addr, email, body.tel, body.acc, body.bank, body.c0, body.c1, body.c2, body.c3, body.c4, body.c5, body.c6, body.c7, body.c8, body.c9, body.id]
                sSysLogs = { fnc_id: 'org_upd', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật thông tin tổ chức id ${body.id}`, msg_id: body.id, doc: JSON.stringify(body) };
            }
            else if (operation == "insert") {
                sql = `insert into s_org(code,taxc,name,name_en,prov,dist,ward,addr,mail,tel,acc,bank,c0,c1,c2,c3,c4,c5,c6,c7,c8,c9) values (@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21,@22);select scope_identity() as id`
                binds = [code, taxc, body.name, body.name_en, body.prov, body.dist, body.ward, body.addr, email, body.tel, body.acc, body.bank, body.c0, body.c1, body.c2, body.c3, body.c4, body.c5, body.c6, body.c7, body.c8, body.c9]
                sSysLogs = { fnc_id: 'org_ins', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm mới thông tin tổ chức: ${JSON.stringify(body)}`, msg_id: body.id, doc: JSON.stringify(body)};
            }
            else if (operation == "delete") {
                sql = `delete from s_org where id=@1`
                binds = [body.id]
                sSysLogs = { fnc_id: 'org_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa thông tin tổ chức id ${body.id}`, msg_id: body.id, doc: JSON.stringify(body)};
            }
            result = await dbs.query(sql, binds)
            if (operation == "insert") {
                let result_id = await dbs.query(`select id from s_org where code=@1`, [body.code])
                let id_ins, rows_ins = result_id.recordset
                if (rows_ins.length > 0) id_ins = rows_ins[0].id
                sSysLogs.msg_id = id_ins
            }
            logging.ins(req,sSysLogs,next) 
            if (operation == "insert") res.json(result.recordset[0])
            else res.json(result.rowsAffected[0])
        }
        catch (err) {
            next(err)
        }
    },
    ins: async (req, res, next) => {
        try {
            const body = req.body
            const cols = ['name', 'name_en', 'taxc', 'code', 'addr', 'tel', 'mail', 'acc', 'bank', 'c0', 'c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9']
            let arr = []
            const sql = `insert into s_org(name,name_en,taxc,code,addr,tel,mail,acc,bank,c0,c1,c2,c3,c4,c5,c6,c7,c8,c9) values (@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19)`
            for (const row of body) {
                let id = row.id
                try {
                    for (const col of cols) {
                        if (!row.hasOwnProperty(col)) row[col] = null
                    }
                    delete row["id"]
                    delete row["chk"]
                    //await dbs.query(sql, row)
                    await dbs.query(sql, [row.name,row.name_en,row.taxc,row.code,row.addr,row.tel,row.mail,row.acc,row.bank,row.c0,row.c1,row.c2,row.c3,row.c4,row.c5,row.c6,row.c7,row.c8,row.c9])
                    const sSysLogs = { fnc_id: 'org_ins', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm mới thông tin tổ chức: ${JSON.stringify(row)}`, msg_id: id, doc: JSON.stringify(row)};
                    logging.ins(req,sSysLogs,next) 
                    arr.push({ id: id, error: "OK" })
                } catch (err) {
                    let msg
                    switch (err.number) {
                        case 2627:
                            msg = "Bản ghi đã tồn tại/The record already exists"
                            break
                        case 2601:
                            msg = "Bản ghi đã tồn tại/The record already exists"
                            break
                        case 547:
                            msg = "Không xóa được bản ghi cha/Cannot delete parent record"
                            break
                        default:
                            msg = err.message
                    }
                    arr.push({ id: id, error: msg })
                }
            }
            res.json(arr)
        }
        catch (err) {
            next(err)
        }
    },
    obbcode: async (code) => {
        try {
            let rows, ob
            const sql = `select id "id",code "code",taxc "taxc",name "name",name_en "name_en",addr "addr",prov "prov",dist "dist",ward "ward",mail "mail",tel "tel",acc "acc",bank "bank",fadd "fadd",c0 "c0",c1 "c1",c2 "c2",c3 "c3",c4 "c4",c5 "c5",c6 "c6",c7 "c7",c8 "c8",c9 "c9",type "type" from s_org where code = :code`
            const result = await dbs.query(sql, [code])
            rows = result.rows
            ob = (rows && rows.length > 0) ? rows[0] : null
            return ob
        }
        catch (err) {
            next(err)
        }
    }
}
module.exports = Service