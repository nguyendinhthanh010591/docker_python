"use strict"
const dbs = require("./dbs")
const util = require("../util")
const redis = require("../redis")
const sec = require("../sec")
const config = require("../config")
const logger4app = require("../logger4app")
const logging = require("../logging")
const ENT = config.ent
const Service = {
    taxc: async (req, res, next) => {
        try {
            const sql = `select taxc id,taxc value from s_ou where taxc is not null order by taxc`
            const result = await dbs.query(sql)
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    taxn: async (req, res, next) => {
        try {
            const result = await dbs.query(`select taxc id,name value from s_ou where taxc is not null order by taxc`)
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    taxt: async (req, res, next) => {
        try {
            const result = await dbs.query(`select taxc id,CONCAT(taxc,'-',name) value from s_ou where taxc is not null order by taxc`)
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    seq: async (req, res, next) => {
        try {
            const mst = req.params.mst
            const result = await dbs.query(`select count(*) rc from s_inv where stax=@1 and status=@2`, [mst, 1])
            const row = result.recordset[0]
            res.json(row)
        }
        catch (err) {
            next(err)
        }
    },
    get: async (req, res, next) => {
      
        try {
            const sql = `with tree as (
                select id,taxc,mst,status,code,name,pid,addr,fadd,prov,dist,ward,mail,tel,acc,bank,paxo,taxo,sign,seq,0 level,'('+CONVERT(VARCHAR(max),id)+'-0)' idx, temp temp,c0 c0,receivermail,dsignissuer,dsignto,dsignfrom,dsignsubject,dsignserial from s_ou where pid is null
                union all
                select c.id,c.taxc,c.mst,c.status,c.code,c.name,c.pid,c.addr,c.fadd,c.prov,c.dist,c.ward,c.mail,c.tel,c.acc,c.bank,c.paxo,c.taxo,c.sign,c.seq,p.level+1 level,p.idx+'*('+CONVERT(VARCHAR(max),c.id)+'-'+CONVERT(VARCHAR(max),p.level)+')' idx, c.temp temp,c.c0 c0,c.receivermail,c.dsignissuer,c.dsignto,c.dsignfrom,c.dsignsubject,c.dsignserial from s_ou c,tree p where p.id=c.pid) 
                select * from tree order by idx `
            const result = await dbs.query(sql)
            const rows = result.recordset
            let arr = [], obj
            for (const row of rows) {
                if (!row.pid) arr.push(row)
                else {
                    obj = util.findDFS(arr, row.pid)
                    if (obj) {
                        if (!obj.hasOwnProperty('data')) obj.data = []
                        obj.data.push(row)
                    }
                }
            }
            res.json(arr)
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const body = req.body, operation = body.webix_operation
            let binds, sql, result, taxc = body.taxc ? body.taxc : null, sSysLogs = { fnc_id: '', src: config.SRC_LOGGING_DEFAULT, dtl: ``}
            if (taxc) await redis.del(`OUS.${taxc}`)
            switch (operation) {
                case "update":
                    const seq = body.seq ? body.seq : 1, sign = body.sign ? body.sign : 1, pwd = body.pwd
                    body.code = body.code ? body.code : null
                    sql = "update s_ou set name=@1,addr=@2,taxc=@3,prov=@4,dist=@5,ward=@6,mail=@7,tel=@8,acc=@9,bank=@10,paxo=@11,taxo=@12,seq=@13,sign=@14,usr=@15,mst=@16,status=@17,code=@18,temp=@19,c0=@20,receivermail=@21,dsignissuer=@22,dsignserial=@23,dsignsubject=@24,dsignfrom=@25,dsignto=@26"
                    binds = [body.name, body.addr, taxc, body.prov, body.dist, body.ward, body.mail, body.tel, body.acc, body.bank, body.paxo, body.taxo, seq, sign, body.usr, body.mst, body.status, body.code, body.temp,body.c0,body.receivermail,body.dsignissuer,body.dsignserial,body.dsignsubject,body.dsignfrom,body.dsignto]
                    let i = 27
                    if (pwd) {
                        sql += `,pwd=@${i++}`
                        binds.push(util.encrypt(pwd))
                    }
                    sql += ` where id=@${i}`
                    binds.push(body.id)
                    sSysLogs = { fnc_id: 'ou_upd', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật thông tin đơn vị id ${body.id}`, msg_id: body.id, doc: JSON.stringify(body)};
                    break
                case "insert":
                    sql = "insert into s_ou (name,pid,mst,code,temp) values (@1,@2,@3,@4,@5);select scope_identity() as id"
                    binds = [body.name, body.parent, body.mst, body.code, body.temp]
                    sSysLogs = { fnc_id: 'ou_ins', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm mới thông tin đơn vị: ${JSON.stringify(body)}`, msg_id: body.id, doc: JSON.stringify(body)};
                    break
                case "delete":
                    sql = "delete from s_ou where id=@1"
                    binds = [body.id]
                    sSysLogs = { fnc_id: 'ou_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa thông tin đơn vị id ${body.id}`, msg_id: body.id, doc: JSON.stringify(body)};
                    break
                default:
                    throw new Error(`${operation} hoạt động không hợp lệ \n (${operation} is invalid operation)`)
            }
            result = await dbs.query(sql, binds)
            logging.ins(req,sSysLogs,next) 
            if (operation == "insert") res.json(result.recordset[0])
            else res.json(result.rowsAffected[0])
        }
        catch (err) {
            next(err)
        }
    },
    bytaxc: async (req, res, next) => {
        try {
            const taxc = req.params.taxc
            const sql = `with tree as (
                select id,name value,0 level,'('+CONVERT(VARCHAR(max),id)+'-0)' idx from s_ou where mst=@1
                union all
                select c.id,c.name value,p.level+1 level,p.idx+'*('+CONVERT(VARCHAR(max),c.id)+'-'+CONVERT(VARCHAR(max),p.level)+')' idx  from s_ou c,tree p where p.id=c.pid
                ) select id,value from tree order by idx`
            const result = await dbs.query(sql, [taxc])
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    byid: async (req, res, next) => {
        try {
            const id = req.params.id
            const sql = `with tree as (
                select id,name value,0 level,'('+CONVERT(VARCHAR(max),id)+'-0)' idx from s_ou where id=@1
                union all
                select c.id,c.name value,p.level+1 level,p.idx+'*('+CONVERT(VARCHAR(max),c.id)+'-'+CONVERT(VARCHAR(max),p.level)+')' idx  from s_ou c,tree p where p.id=c.pid
                ) select id,value from tree order by idx`
            const result = await dbs.query(sql, [id])
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    bytoken: async (req, res, next) => {
        try {
            const token = sec.decode(req)
            const sql = `with tree as (
                select id,mst,code,name value,0 level,'('+CONVERT(VARCHAR(max),id)+'-0)' idx from s_ou where id=@1
                union all
                select c.id,c.mst,c.code,c.name value,p.level+1 level,p.idx+'*('+CONVERT(VARCHAR(max),c.id)+'-'+CONVERT(VARCHAR(max),p.level)+')' idx  from s_ou c,tree p where p.id=c.pid
                ) select id,mst,code,value from tree order by idx`
            const result = await dbs.query(sql, [token.ou])
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
//theo chi nhanh sgr
    byousgr: async (req, res, next) => {
        try {
            const token = sec.decode(req)
            let sql = ` select id,taxc,concat(taxc,'-',name) value from s_ou where id=@1 or pid = @1`
            const result = await dbs.query(sql, [token.ou])
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    //theo chi nhanh dc chon
    bytokenou: async (req, res, next) => {
        try {
            const token = sec.decode(req)
            let sql = `with tree as (
                select id,mst,taxc,code,name value,0 level,'('+CONVERT(VARCHAR(max),id)+'-0)' idx from s_ou where id=@1
                union all
                select c.id,c.mst,c.taxc,c.code,c.name value,p.level+1 level,p.idx+'*('+CONVERT(VARCHAR(max),c.id)+'-'+CONVERT(VARCHAR(max),p.level)+')' idx  from s_ou c,tree p where p.id=c.pid
                ) select id,mst,taxc,code,value from tree where mst in (select b.taxc from s_ou b,s_manager a where b.taxc=a.taxc and a.user_id=@2) order by idx`
            const result = await dbs.query(sql, [token.ou,token.uid])
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    obt: async (taxc) => {
        let row
        const sql = `select so.id "id",so.taxc "taxc",so.paxo "paxo",st2.name "paxoname",so.taxo "taxo",st.name "taxoname",so.name "name",so.fadd "addr",so.mail "mail",so.tel "tel",so.acc "acc",so.bank "bank",so.seq "seq",so.sign "sign",so.usr "usr",so.pwd "pwd",so.sc "sc",so.sct "sct",so.bcc "bcc",so.qrc "qrc",so.na "na",so.np "np",so.nq "nq",so.itype "itype",so.ts "ts",so.degree_config "degree_config" from s_ou so left join s_taxo st on st.id=so.taxo left join s_taxo st2 on st2.id=so.paxo where taxc=@1`
        const result = await dbs.query(sql, [taxc])
        row = result.recordset[0]
        return row
    },
    gettosign: async (taxc) => {
        const sql = `select id,taxc,taxo,name,fadd addr,mail,tel,acc,bank,seq,sign,usr,pwd,temp from s_ou where taxc=@1`
        const result = await dbs.query(sql, [taxc])
        let row = result.recordset[0]
        return row
    },
    org: async (token) => {
        return await Service.obt(token.taxc)
    },
    obid: async (id) => {
        const sql = `select so.id "id",so.taxc "taxc",so.paxo "paxo",st2.name "paxoname",so.taxo "taxo",st.name "taxoname",so.name "name",so.fadd "addr",so.mail "mail",so.tel "tel",so.acc "acc",so.bank "bank",so.seq "seq",so.sign "sign",so.usr "usr",so.pwd "pwd",so.sc "sc",so.sct "sct",so.bcc "bcc",so.qrc "qrc",so.na "na",so.np "np",so.nq "nq",so.itype "itype",so.ts "ts",so.degree_config "degree_config" from s_ou so left join s_taxo st on st.id=so.taxo left join s_taxo st2 on st2.id=so.paxo where so.id=@1`
        const result = await dbs.query(sql, [id])
        let row = result.recordset[0]
        return row
    },
    pidtaxc: async (id) => {
        const sql = `select top 1 taxc from s_ou where id in (select pid from s_ou where id=@1) or (id = @1 and taxc in ('0104918404','0106827752'))`
        const result = await dbs.query(sql, [id])
        let row = result.recordset[0]
        return row.taxc
    },
 
    oball: async () => {
        const sql = `select id,taxc,taxo,name,fadd addr,mail,tel,acc,bank,seq,sign,usr,pwd,temp from s_ou order by id`
        const result = await dbs.query(sql, [])
        let rows = result.recordset
        return rows
    }
}
module.exports = Service