"use strict"
const redis = require("../redis")
const dbs = require("./dbs")
const config = require("../config")
const logger4app = require("../logger4app")
const logging = require("../logging")
const Service = {
    get: async (req, res, next) => {
        try {
            const type = req.params.type.toUpperCase()
            const sql = `select id,name,des from s_cat where type=@1`
            const result = await dbs.query(sql, [type])
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    chkserdes: async (id) => {//Thêm riêng đoạn check mục đích sử dụng TBPH cho VCM
        let sqlcheck, resultcheck, rowtype, rowname, rownum
        sqlcheck = `select name "name", type "type" from s_cat where id=@1`
        resultcheck = await dbs.query(sqlcheck, [id])
        rowtype = resultcheck.recordset[0].type
        rowname = resultcheck.recordset[0].name
    
        if (String(rowtype).toLowerCase() == "serialdes") {
            sqlcheck = `select count(*) "total" from s_serial where des=@1`
            resultcheck = await dbs.query(sqlcheck, [rowname])
            rownum = resultcheck.recordset[0].total
        }
        return { rownum: rownum, rowtype: rowtype, rowname: rowname }
    },
    post: async (req, res, next) => {
        try {
            let body = req.body, binds, sql, result, operation = body.webix_operation, rownum, sSysLogs = { fnc_id: '', src: config.SRC_LOGGING_DEFAULT, dtl: ``}
            switch (operation) {
                case "update":
                    sql = "update s_cat set name=@1,des=@2 where id=@3"
                    binds = [body.name,body.des, body.id]
                    //Thêm riêng đoạn check mục đích sử dụng TBPH cho VCM
                    let result_id = await dbs.query(`select type from s_cat where id=@1`, [body.id])
                    let id_ins, rows_ins = result_id.recordset
                    if(rows_ins.length >0) id_ins=rows_ins[0].type
                    if (config.ent == "vcm") {
                        rownum = await Service.chkserdes(body.id)
                        if (rownum.rowtype.toLowerCase() == "serialdes") {
                            if (rownum.rownum > 0) {
                                sql = "update s_cat set des=@1 where id=@2"
                                binds = [body.des, body.id]
                                sSysLogs = { fnc_id: 'cat_upd', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật danh mục ${id_ins} (${operation})`, msg_id: body.id, doc: JSON.stringify(body) };
                            }
                        }
                    }else sSysLogs = { fnc_id: 'cat_upd', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật danh mục ${id_ins} (${operation})`, msg_id: body.id, doc: JSON.stringify(body) };
                    break
                case "insert":
                    sql = "insert into s_cat(name,type) values (@1,@2);select scope_identity() as id"
                    binds = [ body.name.toString() , body.type.toUpperCase() ]
                    sSysLogs = { fnc_id: 'cat_ins', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm mới danh mục ${body.type} (${operation})`, msg_id: body.id, doc: JSON.stringify(body) };
                    break
                case "delete":
                    sql = "delete from s_cat where id=@1"
                    binds = [body.id]
                    sSysLogs = { fnc_id: 'cat_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa danh mục ${body.type} (${operation})`, msg_id: body.id, doc: JSON.stringify(body) };
                    //Thêm riêng đoạn check mục đích sử dụng TBPH cho VCM
                    if (config.ent == "vcm") {
                        rownum = await Service.chkserdes(body.id)
                        if (rownum.rowtype.toLowerCase() == "serialdes") {
                            if (rownum.rownum > 0) throw new Error("Không xóa được bản ghi cha \n (The parent record can't be deleted)")
                        }
                    }
                    break
                default:
                    throw new Error(operation + " là hoạt động không hợp lệ ("+operation + " is invalid operation)")
            }
            result = await dbs.query(sql, binds)
            if (operation == "insert"){
                let result_id = await dbs.query(`select id from s_cat where name=@1`, [body.name])
                let id_ins, rows_ins = result_id.recordset
                if(rows_ins.length >0) id_ins=rows_ins[0].id
                sSysLogs.msg_id=id_ins
            }
            logging.ins(req, sSysLogs, next)
            if (operation == "insert")  res.json(result.recordset[0])
            else res.json(result.rowsAffected[0])
        }
        catch (err) {
            next(err)
        }
    },
    cache: (type, key) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result, json, sql = `select name id,name value from s_cat where type=@1 order by name`, binds = []
                switch (type) {
                    case "PROV":
                        sql = `select id id,name value from s_loc where pid is null order by name`
                        break
                    case "ROLE":
                        sql = `select id id,name value from s_role where active = 1 order by id`
                        break
                    case "TAXO":
                        sql = `select id id,name value from s_taxo where id like @1 order by id`
                        binds.push("%00")
                        break
                    case "SEG":
                        sql = `select id id,CONCAT(id ,'-' ,seg_name) value from s_segment order by id`
                        break
                    default:
                        binds.push(type)
                }
                result = await dbs.query(sql, binds)
                json = JSON.stringify(result.recordset)
                await redis.set(key, json)
                resolve(result.recordset)
            }
            catch (err) {
                reject(err)
            }
        })
    },
    kache: async (req, res, next) => {
        try {
            let type = req.params.type.toUpperCase(), id = req.params.id, key = `${type}.${id}`, data
            //data = await redis.get(key)
            //if (data) return res.json(JSON.parse(data))
            let sql, result
            switch (type) {
                case "LOCAL":
                    sql = `select id id,name value from s_loc where pid=@1`
                    result = await dbs.query(sql, [id])
                    break
                case "TAXO":
                    sql = `select id id,name value from s_taxo where id like @1`
                    result = await dbs.query(sql, [`${id.substr(0, 3)}%`])
                    break
                default:
                    throw new Error(type+" là key không hợp lệ"+"("+type + " is invalid key)")
            }
            const rows = result.recordset
            //await redis.set(key, JSON.stringify(rows))
            res.json(rows)
        }
        catch (err) {
            next(err)
        }
    },
    nokache: async (req, res, next) => {
        try {
            const type = req.params.type.toUpperCase()
            let sql, result
            switch (type) {
                case "GNS":
                    sql = `select id id,name value,unit unit,price price from s_gns order by name`
                    result = await dbs.query(sql)
                    break
                case "OU":
                    sql = `select id id,name value from s_ou order by name`
                    result = await dbs.query(sql)
                    break
                case "SYSTEM_SCB":
                    sql = `select name id,name value from s_cat where type='SYSTEM_SCB' order by name`
                    result = await dbs.query(sql)
                    break
                case "SERIALDES":
                    sql = `select name id,des value from s_cat where type='SERIALDES' order by right(replicate('_', 5) + name, 5)`
                    result = await dbs.query(sql)
                    break
                default:
                    throw new Error(type+"là key không hợp lệ"+"("+type + " is invalid key)")
            }
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    byidtype: async (id, type) => {
        let ret = ""
        try {
            logger4app.debug(`cat byidtype id ${id} type ${type}`)
            const sql = `SELECT id id, [name] name, [type] type, des des FROM s_cat WHERE name = @1 AND type = @2`
            const result = await dbs.query(sql, [id, type])
            let row = result.recordset[0]
            ret = row.des
            logger4app.debug(`cat byidtype ret ${ret}`)
        }
        catch (err) {
            ret = ""
        }
        return ret
    }
}
module.exports = Service   