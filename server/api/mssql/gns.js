"use strict"
const config = require("../config")
const logger4app = require("../logger4app")
const logging = require("../logging")
const util = require("../util")
const dbs = require("./dbs")
const Service = {
    fbn: async (req, res, next) => {
        try {
            const query = req.query, filter = query.filter
            let sql, result, where = " where 1=1", binds = [], val
            if (filter) {
                val = `%${filter.value.trim().toUpperCase()}%`
                where += " and (upper(code) like @1 or upper(name) like @1)"
                binds.push(val)
            }
            sql = `select code,name,unit,price from s_gns ${where} order by code OFFSET 0 ROWS FETCH FIRST ${config.limit} ROWS ONLY`
            result = await dbs.query(sql, binds)
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    fbw: async (req, res, next) => {
        try {
            const query = req.query, filter = query.filter, sort = query.sort
            let sql, result, where = " where 1=1", order, binds = [], val
            if (filter) {
                let i = 1
                Object.keys(filter).forEach(key => {
                    val = filter[key]
                    if (val) {
                        if (key == "price") {
                            if (util.isNumber(val)) {
                                where += ` and price=@${i++}`
                                binds.push(val)
                            }
                            else where += ` and price = ${val}`
                        }
                        else {
                            where += ` and upper(${key}) like @${i++}`
                            binds.push(`%${val.toUpperCase()}%`)
                        }
                    }
                })
            }
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = ` order by ${key} ${sort[key]}`
                })
            }
            else order = ` order by code`
            sql = `select id,code,name,unit,price from s_gns ${where} ${order} OFFSET 0 ROWS FETCH FIRST ${config.limit} ROWS ONLY`
            result = await dbs.query(sql, binds)
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    get: async (req, res, next) => {
        try {
            const query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 10, sort = query.sort
            let sql, result, ret, where = " where 1=1", order, binds = [], i = 1
            if (query.filter) {
                let filter = JSON.parse(query.filter), val
                Object.keys(filter).forEach(key => {
                    val = filter[key]
                    if (val) {
                        if (key == "unit") {
                            where += ` and ${key}=@${i++}`
                            binds.push(val)
                        }
                        else {
                            where += ` and upper(${key}) like @${i++}`
                            binds.push(`%${val.toUpperCase()}%`)
                        }
                    }
                })
            }
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = ` order by ${key} ${sort[key]}`
                })
            } else order = ` order by code`
            sql = `select id,code,name,unit,price from s_gns ${where} ${order} offset @${i++} rows fetch next @${i++} rows only`
            binds.push(parseInt(start), parseInt(count))
            result = await dbs.query(sql, binds)
            ret = { data: result.recordset, pos: start }
            if (start == 0) {
                sql = `select count(*) "total" from s_gns ${where}`
                result = await dbs.query(sql, binds)
                ret.total_count = result.recordset[0].total
            }
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const body = req.body, operation = body.webix_operation
            let sql, result, binds, price = body.price ? body.price : null, sSysLogs = { fnc_id: '', src: config.SRC_LOGGING_DEFAULT, dtl: ``}
            switch (operation) {
                case "update":
                    sql = `update s_gns set code=@1,name=@2,price=@3,unit=@4 where id=@5`
                    binds = [body.code, body.name, price, body.unit, body.id]
                    sSysLogs = { fnc_id: 'gns_upd', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật danh mục hàng hóa, dịch vụ ${body.name} (${operation})`, msg_id: body.id, doc: JSON.stringify(body) };
                    break
                case "insert":
                    sql = `insert into s_gns(code,name,price,unit) values(@1,@2,@3,@4);select scope_identity() as id`
                    binds = [body.code, body.name, price, body.unit]
                    sSysLogs = { fnc_id: 'gns_ins', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm mới danh mục hàng hóa, dịch vụ ${body.name} (${operation})`, msg_id: body.id, doc: JSON.stringify(body) };
                    break
                case "delete":
                    sql = `delete from s_gns where id=@1`
                    binds = [body.id]
                    sSysLogs = { fnc_id: 'gns_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa danh mục hàng hóa, dịch vụ ${body.name} (${operation})`, msg_id: body.id, doc: JSON.stringify(body) };
                    break
                default:
                    throw new Error(`${operation} là không hợp lệ \n (${operation} is invalid)`)
            }
            result = await dbs.query(sql, binds)
            if (operation == "insert"){
                let result_id = await dbs.query(`select id from s_gns where code=@1`, [body.code])
                let id_ins, rows_ins = result_id.recordset
                if(rows_ins.length >0) id_ins=rows_ins[0].id
                sSysLogs.msg_id=id_ins
            }
            logging.ins(req, sSysLogs, next)
            if (operation == "insert") res.json(result.recordset[0])
            else res.json(result.rowsAffected[0])
        }
        catch (err) {
            next(err)
        }
    },
    ins: async (req, res, next) => {
        try {
            const body = req.body, cols = ['code', 'name', 'unit', 'price']
            const sql = `insert into s_gns (code, name, unit, price) values (@1,@2,@3,@4);select scope_identity() as id`
            let arr = []
            for (const row of body) {
                let id = row.id
                try {
                    for (const col of cols) {
                        if (!row.hasOwnProperty(col)) row[col] = null
                    }
                    delete row["id"]
                    delete row["chk"]
                    await dbs.query(sql, Object.values(row))
                    const sSysLogs = { fnc_id: 'gns_ins', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm mới danh mục hàng hóa, dịch vụ ${body.name}`, msg_id: body.id, doc: JSON.stringify(body) };
                    logging.ins(req, sSysLogs, next)
                    arr.push({ id: id, error: "OK" })
                } catch (err) {
                    const msg = (err.code == "EREQUEST") ? "Bản ghi đã tồn tại \n The record has existed" : err.message
                    arr.push({ id: id, error: msg })
                }
            }
            res.json(arr)
        }
        catch (err) {
            next(err)
        }
    }
}
module.exports = Service