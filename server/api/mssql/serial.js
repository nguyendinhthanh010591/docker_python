"use strict"
const path = require("path")
const fs = require("fs")
const docxt = require("docxtemplater")
const jszip = require("jszip")
const moment = require("moment")
const util = require("../util")
const config = require("../config")
const logger4app = require("../logger4app")
const redis = require("../redis")
const sec = require("../sec")
const dbs = require("./dbs")
const ous = require("./ous")
const logging = require("../logging")
const ENT = config.ent
const dbCache = config.dbCache
const GRANT = config.serial_grant
const GRANT_USR = config.serial_usr_grant
const taxon = async (id) => {
    const i3 = id.substr(0, 3), key = `TAXO.${i3}00`
    let rows = await redis.get(key)
    if (rows) rows = JSON.parse(rows)
    else {
        const result = await dbs.query(`select id,name value from s_taxo where id like @1`, [`${i3}%`])
        rows = result.recordset
        await redis.set(key, JSON.stringify(rows))
    }
    let obj = rows.find(x => x.id === id)
    if (typeof obj == "undefined") return ""
    else return obj.value
}

const Service = {
    getFormByType: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, id = req.query.id, now = new Date()
            let sql, result, binds, uses = 2
			if (ENT == "dtt") uses = -1
            if (GRANT) {
                sql = `select distinct a.form id,a.form value from s_serial a,s_seou b where a.id=b.se and b.ou=@1 and a.type=@2 and a.status=1 and a.uses<>@3 and a.fd<=@4`
                binds = [token.ou, id, uses, now]
            }
            else {
                sql = `select distinct form id,form value from s_serial where taxc=@1 and type=@2 and status=1 and uses<>@3 and fd<=@4`
                binds = [taxc, id, uses, now]
            }
            result = await dbs.query(sql, binds)
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    getFormByTypeInv: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, id = req.query.id, now = new Date(), seruseapp = config.SER_USES_APP,ou = req.query.id
            let sql, result, binds, uses = 2
			if (ENT == "dtt") uses = -1
            if (GRANT_USR) {
                sql = `select distinct a.form id,a.form value from s_serial a,s_seusr b where a.id=b.se and b.usrid=@1 and a.type=@2 and a.status=1 and a.uses<>@3 and a.fd<=@4 and a.uses in ${seruseapp}`
                binds = [token.uid, id, uses, now]
            } else {
                if (GRANT) {
                    sql = `select distinct a.form id,a.form value from s_serial a,s_seou b where a.id=b.se and b.ou=@1 and a.type=@2 and a.status=1 and a.uses<>@3 and a.fd<=@4 and a.uses in ${seruseapp}`
                    binds = [token.ou, id, uses, now]
                }
                else {
                    sql = `select distinct form id,form value from s_serial where taxc=@1 and type=@2 and status=1 and uses<>@3 and fd<=@4 and uses in ${seruseapp}`
                    binds = [taxc, id, uses, now]
                }
            }
            result = await dbs.query(sql, binds)
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    getFormByTypeInvAndOu: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, id = req.query.id, now = new Date(), seruseapp = config.SER_USES_APP,ou = req.query.ou
            let sql, result, binds, uses = 2
            sql = `select distinct s.form id,s.form value from s_serial s,s_ou ou where s.taxc=ou.mst and ou.id=@1 and s.type=@2 and s.status=1 and s.uses<>@3 and s.fd<=@4 and s.uses in ${seruseapp}`
            binds = [ou, id, uses, now]
            result = await dbs.query(sql, binds)
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    getAllFormByType: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, id = req.query.id, now = new Date()
            let sql, result, binds
            if (GRANT) {
                sql = `select distinct a.form id,a.form value from s_serial a,s_seou b where a.id=b.se and b.ou=@1 and a.taxc=@2 and a.type=@3 `
                binds = [token.ou, taxc, id]
            }
            else {
                sql = `select distinct form id,form value from s_serial where taxc=@1 and type=@2 `
                binds = [taxc, id]
            }
            result = await dbs.query(sql, binds)
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    getAllFormByTypeInv: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, id = req.query.id, now = new Date()
            let sql, result, binds
            if (GRANT_USR) {
                sql = `select distinct a.form id,a.form value from s_serial a,s_seusr b where a.id=b.se and b.usrid=@1 and a.taxc=@2 and a.type=@3 and a.fd<=@4`
                binds = [token.uid, taxc, id, now]
            } else {
                if (GRANT) {
                    sql = `select distinct a.form id,a.form value from s_serial a,s_seusr b where a.id=b.se and b.usrid=@1 and a.taxc=@2 and a.type=@3 and a.fd<=@4`
                    binds = [token.uid, taxc, id, now]
                }
                else {
                    sql = `select distinct form id,form value from s_serial where taxc=@1 and type=@2 and fd<=@3 and status !=3`
                    binds = [taxc, id, now]
                }
            }
            result = await dbs.query(sql, binds)
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    getSerialByForm: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, id = req.query.id, now = new Date()
            let sql, result, binds, uses = 2
			if(ENT == "dtt") uses = -1
            if (GRANT) {
                sql = `select distinct a.serial id,a.serial value from s_serial a,s_seou b where a.id=b.se and b.ou=@1 and a.taxc=@2 and a.form=@3 and a.status=1 and a.uses<>@4 and a.fd<=@5`
                binds = [token.ou, taxc, id, uses, now]
            }
            else {
                sql = `select distinct serial id,serial value from s_serial where taxc=@1 and form=@2 and status=1 and uses<>@3 and fd<=@4`
                binds = [taxc, id, uses, now]
            }
            result = await dbs.query(sql, binds)
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    getSerialByFormInv: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, id = req.query.id, now = new Date(), seruseapp = config.SER_USES_APP
            let sql, result, binds, uses = 2
			if(ENT == "dtt") uses = -1
            if (GRANT_USR) {
                sql = `select distinct a.serial id,a.serial value from s_serial a,s_seusr b where a.id=b.se and b.usrid=@1 and a.taxc=@2 and a.form=@3 and a.status=1 and a.uses<>@4 and a.fd<=@5 and a.uses in ${seruseapp}`
                binds = [token.uid, taxc, id, uses, now]
            } else {
                if (GRANT) {
                    sql = `select distinct a.serial id,a.serial value from s_serial a,s_seou b where a.id=b.se and b.ou=@1 and a.taxc=@2 and a.form=@3 and a.status=1 and a.uses<>@4 and a.fd<=@5 and a.uses in ${seruseapp}`
                    binds = [token.ou, taxc, id, uses, now]
                }
                else {
                    sql = `select distinct serial id,serial value from s_serial where taxc=@1 and form=@2 and status=1 and uses<>@3 and fd<=@4 and uses in ${seruseapp}`
                    binds = [taxc, id, uses, now]
                }
            }
            result = await dbs.query(sql, binds)
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    getSerialByFormInvAndOu: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, id = req.query.id,ou = req.query.ou, now = new Date(), seruseapp = config.SER_USES_APP
            let sql, result, binds, uses = 2
			sql = `select distinct s.serial id,s.serial value from s_serial s,s_ou ou where s.taxc=ou.mst and ou.id=@1  and s.form=@2 and s.status=1 and s.uses<>@3 and s.fd<=@4 and s.uses in ${seruseapp}`
            binds = [ou, id, uses, now]
            result = await dbs.query(sql, binds)
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    getAllSerial: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, now = new Date()
            let sql, result, binds, uses = 2
			// if(ENT == "dtt") uses = -1
            // if (GRANT) {
            //     sql = `select distinct a.serial id,a.serial value from s_serial a,s_seou b where a.id=b.se and b.ou=@1 and a.taxc=@2   order by a.serial`
            //     binds = [token.ou, taxc]
            // }
            // else {
            //     sql = `select distinct serial id,serial value from s_serial where taxc=@1  order by serial`
            //     binds = [taxc]
            // }
            sql = `select distinct serial id,serial value from s_serial where serial is not null  order by serial`
            result = await dbs.query(sql, [])
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    getAllSerialByForm: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, id = req.query.id, now = new Date()
            let sql, result, binds
            if (GRANT) {
                sql = `select distinct a.serial id,a.serial value from s_serial a,s_seou b where a.id=b.se and b.ou=@1 and a.taxc=@2 and a.form=@3 and a.fd<=@4`
                binds = [token.ou, taxc, id, now]
            }
            else {
                sql = `select distinct serial id,serial value from s_serial where taxc=@1 and form=@2 and fd<=@3`
                binds = [taxc, id, now]
            }
            result = await dbs.query(sql, binds)
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    getAllSerialByFormInv: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, id = req.query.id, now = new Date()
            let sql, result, binds
            if (GRANT_USR) {
                sql = `select distinct a.serial id,a.serial value from s_serial a,s_seusr b where a.id=b.se and b.usrid=@1 and a.taxc=@2 and a.form=@3 and a.fd<=@4`
                binds = [token.uid, taxc, id, now]
            } else {
                if (GRANT) {
                    sql = `select distinct a.serial id,a.serial value from s_serial a,s_seou b where a.id=b.se and b.ou=@1 and a.taxc=@2 and a.form=@3 and a.fd<=@4`
                    binds = [token.ou, taxc, id, now]
                }
                else {
                    sql = `select distinct serial id,serial value from s_serial where taxc=@1 and form=@2 and fd<=@3 and status !=3 `
                    binds = [taxc, id, now]
                }
            }
            result = await dbs.query(sql, binds)
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    err: async (taxc, form, serial, seq) => {
        if (seq) await redis.lpush(`SERIAL.${taxc}.${form}.${serial}.err`, seq)
    },
    sequence: (taxc, form, serial,idt) => {
        return new Promise(async (resolve, reject) => {
            try {
                const uk = `${taxc}.${form}.${serial}`, msg = `Ký hiệu (Serial) ${uk}`, key = `SERIAL.${uk}`, kax = `${key}.max`, ker = `${key}.err`,kreq=`${key}.req`
                const max = await redis.get(kax)
                if (!max) throw new Error(`${msg} đã hủy hoặc đã hết số \n (${msg} was cancelled or out of numbers)`)
                let val = await redis.rpop(ker)
                if (!val) val = await redis.incr(key)//val = await redis.get(key)
                val = Number(val)
               // val = await redis.incr(key)
                let reqc=await redis.get(kreq)
                if (reqc){
                    let reqcs=reqc.split("__")
                    for (const row of reqcs) {
                        let k=row.split(".")
                        if(k.length>1){
                            if ( val==Number(k[1])){
                                await dbs.query(`update s_serial set status=@1,cur=@2,td=@3 where taxc=@4 and form=@5 and serial=@6 and priority=@7`, [4, Number(k[1]), new Date(), taxc, form, serial,Number(k[3])])
                            }
                            //check hieu luc dai so
                            if (val>=Number(k[0]) && val<=Number(k[1])){
                                let result = await dbs.query(`update s_serial set cur=@1 where taxc=@2 and form=@3 and serial=@4 and min=@5 and max=@6 and status=1`, [val, taxc, form, serial, Number(k[0]), Number(k[1])])
                                let dt= moment(idt).format("YYYYMMDD")
                                if (Number(dt) < Number(k[2])) 
                                {
                                    await redis.lpush(ker, val)
                                    throw new Error(`${msg} đã hết số hoặc hết hiệu lực \n ${msg} out of number or out of date`)
                                }
                            }
                         }
                    }
                }
               
                //val = await redis.incr(key)
                if (val >= Number(max)) {
                    await dbs.query(`update s_serial set status=@1,cur=@2,td=@3 where taxc=@4 and form=@5 and serial=@6 and (@2 between min and max)`, [4, max, new Date(), taxc, form, serial])
                    await redis.del([key, kax, ker,kreq])
                    if (val > Number(max)) throw new Error(`${msg} đã hết số \n (${msg} was out of numbers)`)
                    else resolve(val)
                }
                else resolve(val)
            }
            catch (err) {
                reject(err)
            }
        })
    },
    approve: async (req, res, next) => {
        try {
            let id = req.params.id, result, rows, row,sql,error=""
            result = await dbs.query(`select taxc,type,form,serial,min,max,cur,priority,fd from s_serial where id=@1 and status=3 and max>cur`, [id])
            rows = result.recordset
            if (rows.length == 0) throw new Error(`Không tìm thấy ${id} để duyệt phát hành \n (Cannot found ${id} to approval for releasing)`)
            row = rows[0]
             // check dai so chua duyet
             sql = `select min(priority) priority from s_serial where taxc=@1 and form=@2 and serial=@3 and status=3`
             result = await dbs.query(sql, [row.taxc,row.form,row.serial])
             let   rowss = result.recordset[0]
             if (result.recordset.length > 0 && rowss.priority!=null) {
               
                 if (row.priority > (rowss.priority)) {
                        error+= ' Bạn cần duyệt dải có số thứ tự ưu tiên thấp đến cao, dải độ ưu tiên  '+(rowss.priority) +' chưa duyệt' + '(You need to approve the range with low to high precedence, the precedence range '+(rowss.priority) +' unapproved)';
                       
                 }
             }
             //end check
            // check dai so da duyet
            sql = `select max(max) max from s_serial where taxc=@1 and form=@2 and serial=@3 and status=1`
            result = await dbs.query(sql, [row.taxc,row.form,row.serial])
               rowss = result.recordset[0]
            if (result.recordset.length > 0 && rowss.max!=null) {
              
                if (row.min != (Number(rowss.max) + 1)) {
                       error+= ' Dải số phải liên tiếp với các dải đã duyệt, Từ số phải bắt đầu từ '+(Number(rowss.max) + 1)+'(The range of numbers must be consecutive with the approved ranges, The word number must start from '+(Number(rowss.max) + 1)+')';
                      
                }
            }
            if (!util.isEmpty(error)) throw new Error(error)
            //end check
            const uk = `${row.taxc}.${row.form}.${row.serial}`, key = `SERIAL.${uk}`, kax = `${key}.max`,kreq=`${key}.req`
            let cur = row.cur, min = row.min, max = row.max, val = min-1,condition = `${row.min}.${row.max}.${moment(row.fd).format('YYYYMMDD')}.${row.priority}__`

            sql = `select * from s_serial where taxc=@1 and form=@2 and serial=@3 and status=1`
            result = await dbs.query(sql, [row.taxc,row.form,row.serial])
            rowss = result.recordset

            for (const rowc of rowss) {
                condition += `${rowc.min}.${rowc.max}.${moment(rowc.fd).format('YYYYMMDD')}.${rowc.priority}__`
            }
            const valc = await redis.get(key)
            //Sửa lại hàm set multi thành nhiều hàm set do DB không có hàm này (áp dụng cho SCB)
            if (dbCache) {
                if (valc != null) {
                    redis.set(kax, max)
                    redis.set(kreq, condition)
                    result = await dbs.query(`update s_serial set status=@1,td=@2 where id=@3 and status=3 and max>cur`, [1, null, id])
                    res.json(result.rowsAffected[0])
                    const sSysLogs = { fnc_id: 'ser_apr', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt thông báo phát hành: ${id}`, msg_id: id, doc: JSON.stringify(row) };
                    logging.ins(req, sSysLogs, next)

                } else {

                    redis.set(key, val)
                    redis.set(kax, max)
                    redis.set(kreq, condition)
                    result = await dbs.query(`update s_serial set status=@1,td=@2 where id=@3 and status=3 and max>cur`, [1, null, id])
                    res.json(result.rowsAffected[0])
                    const sSysLogs = { fnc_id: 'ser_apr', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt thông báo phát hành: ${id}`, msg_id: id, doc: JSON.stringify(row) };
                    logging.ins(req, sSysLogs, next)


                }
            } else {
                if (valc != null) {
                    redis.multi().set(kax, max).set(kreq, condition).exec(async (err, results) => {
                        if (err) throw new Error(err)
                        result = await dbs.query(`update s_serial set status=@1,td=@2 where id=@3 and status=3 and max>cur`, [1, null, id])
                        res.json(result.rowsAffected[0])
                        const sSysLogs = { fnc_id: 'ser_apr', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt thông báo phát hành: ${id}`, msg_id: id, doc: JSON.stringify(row) };
                        logging.ins(req, sSysLogs, next)
                    })
                } else {

                    redis.multi().set(key, val).set(kax, max).set(kreq, condition).exec(async (err, results) => {
                        if (err) throw new Error(err)
                        result = await dbs.query(`update s_serial set status=@1,td=@2 where id=@3 and status=3 and max>cur`, [1, null, id])
                        res.json(result.rowsAffected[0])
                        const sSysLogs = { fnc_id: 'ser_apr', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt thông báo phát hành: ${id}`, msg_id: id, doc: JSON.stringify(row) };
                        logging.ins(req, sSysLogs, next)
                    })

                }
            }
           
        }
        catch (err) {
            next(err)
        }
    },
    cancel: async (req, res, next) => {
        try {
            let id = req.params.id, result, rows, row
            result = await dbs.query(`select taxc,form,serial,min from s_serial where id=@1 and status=1`, [id])
            rows = result.recordset
            if (rows.length == 0) throw new Error(`Không tìm thấy ${id} để hủy phát hành \n (Cannot found ${id} to cancel release)`)
            row = rows[0]
            const uk = `${row.taxc}.${row.form}.${row.serial}`, key = `SERIAL.${uk}`, kax = `${key}.max`, ker = `${key}.err`,kreq=`${key}.req`
            let min = row.min, cur = await redis.get(key)
           // if (!cur || cur < min) cur = min
            //Sửa lại hàm set multi thành nhiều hàm set do DB không có hàm này (áp dụng cho SCB)
            if (dbCache) {
                redis.del(key)
                redis.del(kax)
                redis.del(ker)
                redis.del(kreq)
                result = await dbs.query(`update s_serial set status=@1,td=@3 where taxc=@4 and form=@5 and serial=@6 and status=1`, [2, cur, moment(new Date()).format('YYYY-MM-DD HH:mm:ss'), row.taxc, row.form, row.serial])

                res.json(result.rowsAffected[0])
                result = await dbs.query(`update s_serial set cur=@1 where taxc=@2 and form=@3 and serial=@4 and status=1 and min<=@1 and max>@1`, [cur, row.taxc, row.form, row.serial])
                const sSysLogs = { fnc_id: 'ser_cancel', src: config.SRC_LOGGING_DEFAULT, dtl: `Hủy thông báo phát hành: ${id}`, msg_id: id, doc: JSON.stringify(row) };
                logging.ins(req, sSysLogs, next)
            }
            else {
                redis.multi().del(key).del(kax).del(ker).del(kreq).exec(async (err, results) => {
                    if (err) throw new Error(err)
                    result = await dbs.query(`update s_serial set status=@1,td=@3 where taxc=@4 and form=@5 and serial=@6 and status=1`, [2, cur, moment(new Date()).format('YYYY-MM-DD HH:mm:ss'), row.taxc, row.form, row.serial])

                    res.json(result.rowsAffected[0])
                    result = await dbs.query(`update s_serial set cur=@1 where taxc=@2 and form=@3 and serial=@4 and status=1 and min<=@1 and max>@1`, [cur, row.taxc, row.form, row.serial])
                    const sSysLogs = { fnc_id: 'ser_cancel', src: config.SRC_LOGGING_DEFAULT, dtl: `Hủy thông báo phát hành: ${id}`, msg_id: id, doc: JSON.stringify(row) };
                    logging.ins(req, sSysLogs, next)
                })
            }
        }
        catch (err) {
            next(err)
        }
    },
    getsexou: async (req, res, next) => {
        try {
            const params = req.params, mst = params.mst, se = params.se
            const sql = `select x.id id,x.name name,x.sel sel from (select a.id,a.name,0 sel from s_ou a where a.mst=@1 and not exists (select 1 from s_seou where se=@2 and ou=a.id) union select a.id,a.name,1 sel from s_ou a,s_seou b where a.mst=@1 and b.se=@2 and b.ou=a.id) x order by x.id`
            const result = await dbs.query(sql, [mst, se])
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    getseou: async (req, res, next) => {
        try {
            const token = sec.decode(req), query = req.query, sort = query.sort, filter = query.filter
            let order, where = "where status=1 and uses<>2 and taxc=@1", sql, result
            let binds = [token.taxc]
            if (filter) {
                let val, i = 2
                Object.keys(filter).forEach(key => {
                    val = filter[key]
                    if (val && val !== "null") {
                        if (key == "fd") {
                            where += ` and ${key}>=@${i++}`
                            binds.push(new Date(val))
                        }
                        else {
                            where += ` and ${key}=@${i++}`
                            binds.push(val)
                        }
                    }
                })
            }
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = `order by ${key} ${sort[key]}`
                })
            }
            else order = "order by id"
            sql = `select id,taxc,type,form,serial,min,max,cur,fd,uses from s_serial ${where} ${order}`
            result = await dbs.query(sql, binds)
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    postseou: async (req, res, next) => {
        try {
            const token = sec.decode(req), uid = token.uid, body = req.body, se = body.se, arr = body.arr
            await dbs.query(`delete from s_seou where se=@1`, [se])
            let binds
            if (arr.length > 0) {
                binds = []
                for (const ou of arr) {
                    binds.push([se, ou])
                }
                await dbs.queries(`insert into s_seou (se,ou) values (@1,@2)`, binds)
            }
            const sSysLogs = { fnc_id: 'ser_grant', src: config.SRC_LOGGING_DEFAULT, dtl: `Gán thông báo phát hành`, doc: JSON.stringify(binds)};
            logging.ins(req,sSysLogs,next) 
            res.json(1)
        } catch (err) {
            next(err)
        }
    },
    //HungLQ them phan quyen serial theo user
    getsexusr: async (req, res, next) => {
        try {
            const token = sec.decode(req), params = req.params, query = req.query, sort = query.sort, filter = query.filter, usid = params.usid, seruseapp = config.SER_USES_GRANT
            let order, where = `where status in (1,2,4) and uses in ${seruseapp} and taxc=@1`, sql, result, sqlexists, sqlnotexists
            let binds = [token.taxc], i = 1
            if (ENT == "vcm") {
                where = `where status in (1,2,4) and uses in ${seruseapp}`
                binds = []
                i = 0
            }
            if (filter) {
                let val
                Object.keys(filter).forEach(key => {
                    val = filter[key]
                    if (val && val !== "null") {
                        if (key == "fd") {
                            where += ` and ${key}>=@${i++}`
                            binds.push(new Date(val))
                        }
                        else {
                            where += ` and ${key}=@${i++}`
                            binds.push(val)
                        }
                    }
                })
            }
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = `order by ${key} ${sort[key]}`
                })
            }
            else order = "order by taxc"
            i++

            sqlnotexists = `select 0 sel,id,taxc,type,form,serial,min,max,cur,fd,uses,status from s_serial a ${where} and not exists (select 1 from s_seusr where usrid=@${i} and se=a.id)`
            
            sqlexists = `select 1 sel,id,taxc,type,form,serial,min,max,cur,fd,uses,status from s_serial a, s_seusr b ${where} and usrid=@${i} and b.se=a.id`
            
            sql = `select x.sel,id,taxc,type,form,serial,min,max,cur,fd,uses,status from (${sqlnotexists} union ${sqlexists}) x ${order}`
            binds.push(usid)
            result = await dbs.query(sql, binds)
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    getseusr: async (req, res, next) => {
        try {
            const token = sec.decode(req)
            const params = req.params, se = params.se
            let sql = `select id id,name name from s_user where ou=@1 order by id`, binds = [token.ou]
            if (ENT == "vcm") {
                sql = `select id id,name name from s_user order by id`
                binds = []
            }
            const result = await dbs.query(sql, binds)
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
        
    },
    postseusr: async (req, res, next) => {
        try {
            const token = sec.decode(req), uid = token.uid, body = req.body, usid = body.usid, arr = body.arr
            await dbs.query(`delete from s_seusr where usrid=@1`, [usid])
            let binds = []
            if (arr.length > 0) {
                for (const se of arr) {
                    binds.push([se, usid])
                }
                await dbs.queries(`insert into s_seusr (se,usrid) values (@1,@2)`, binds)
            }
            const sSysLogs = { fnc_id: 'ser_grant', src: config.SRC_LOGGING_DEFAULT, dtl: `Gán thông báo phát hành cho NSD`, doc: JSON.stringify(binds)};
            logging.ins(req,sSysLogs,next) 
            res.json(1)
        } catch (err) {
            next(err)
        }
    }, //HungLQ them phan quyen serial theo user    
    get: async (req, res, next) => {
        try {
            const query = req.query, filter = JSON.parse(query.filter)
            const sort = query.sort, start = query.start ? query.start : 0, count = query.count ? query.count : 10
            let where = " where fd between @1 and @2", order, sql, result, ret, val, i = 3
            let binds = [new Date(filter.fd), new Date(moment(filter.td).endOf("day"))],str,arr=[]
            Object.keys(filter).forEach((key) => {
                val = filter[key]
                if (val) {
                    switch (key) {
                        case "fd":
                            break
                        case "td":
                            break
                        case ("serial") :
                        case ("taxc") :
                                let str = "", arr = String(val).split(",")
                                for (const row of arr) {
                                    let d = row.split("|")
                                    str += `'${d[0]}',`
                                }
                                where += ` and ${key} in (${str.slice(0, -1)})`
                                break
                        default:
                            where += ` and ${key}=@${i++}`
                            binds.push(val)
                            break
                    }
                }
            })
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = `order by ${key} ${sort[key]}`
                })
            }
            else order = "order by id"
            sql = `select id,taxc,type,form,serial,min,max,cur,status,fd,td,uses,CAST(RIGHT(form,3) as int) idx,des,priority,dt,sendtype,invtype,degree_config from s_serial ${where} ${order} offset @${i++} rows fetch next @${i++} rows only`
            binds.push(parseInt(start), parseInt(count))
            result = await dbs.query(sql, binds)
            ret = { data: result.recordset, pos: start }
            if (start == 0) {
                sql = `select count(*) total from s_serial ${where}`
                result = await dbs.query(sql, binds)
                ret.total_count = result.recordset[0].total
            }
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const token = sec.decode(req), body = req.body, operation = body.webix_operation, org = await ous.org(token), degree_config = config.DEGREE_CONFIG
            let binds, sql, result
            if (operation == "update") {
                sql = `update s_serial set type=@1,form=@2,serial=@3,min=@4,max=@5,cur=@6,fd=@7,uc=@8,uses=@9,taxc=@10,des=@11,priority=@12,sendtype=@13,invtype=@14,degree_config=@15 where id=@16`
                const uid = token.uid, type = body.type, idx = body.idx
                const form = degree_config == "119" ? `${type}0/${idx.padStart(3, "0")}` : body.form, seri = body.serial, min = body.min, max = body.max, arr = body.taxc.split(","), fd = new Date(body.fd), uses = body.uses, id =body.id,des=body.des,priority=body.priority, sendtype = body.sendtype, invtype = body.invtype
                body.form = form
                binds = []
                // let row = { type: type, form: form, serial: seri, min: min, max: max, cur: min, fd: fd, uc: uid, uses: uses, id: id }
                for (const taxc of arr) {
                    // row.taxc = taxc
                    binds =[type, form, seri, min, max, min, fd, uid, uses, taxc,des,priority,sendtype,invtype,degree_config,id]
                    await dbs.query(sql, binds)
                }
                const sSysLogs = { fnc_id: 'ser_upd', src: config.SRC_LOGGING_DEFAULT, dtl: `Sửa thông báo phát hành:  ${id}`, msg_id: id, doc: JSON.stringify(binds)};
                logging.ins(req,sSysLogs,next)
                //update
            } else {
                if (operation == "insert") {
                    const uid = token.uid, type = body.type, idx = body.idx
                    const form = degree_config == "119" ? `${type}0/${idx.padStart(3, "0")}` : body.form, seri = body.serial, min = body.min, max = body.max, arr = body.taxc.split(","), fd = new Date(body.fd), uses = body.uses,des=body.des,priority=body.priority, sendtype = body.sendtype, invtype = body.invtype
                    body.form = form
                    binds = []
                    let  error = ""
                    for (const taxc of arr) {
                        let bindroot = [type, form, seri, min, max, 0, moment(fd).format('YYYY-MM-DD'), uid, uses, taxc,des,priority]
                        if (degree_config == "123") {
                            bindroot.push(sendtype)
                            bindroot.push(invtype)
                            bindroot.push(degree_config)
                        } else {
                            bindroot.push(null)
                            bindroot.push(null)
                            bindroot.push(degree_config)
                        }
                        binds.push(bindroot)
                        //check ban ghi ton tại
                        sql = `select *  from s_serial where taxc=@1 and form=@2 and serial=@3`
                        result = await dbs.query(sql, [taxc,form,seri])
                        let   rows = result.recordset
                        if (rows.length > 0) {
                           for(const row of rows) {
                            if (priority == row.priority) {
                                error+= ' Bản ghi đã tồn tại' + '(The record already exits)'
                                throw new Error(error)
                                  }
                           }
                            
                            
                        }
                        sql = `select max(max) max,max(fd) fd from s_serial where taxc=@1 and form=@2 and serial=@3`
                        result = await dbs.query(sql, [taxc,form,seri])
                        rows = result.recordset
                        if (rows.length > 0 && rows[0].max!=null) {
                           
                            if (min != (rows[0].max + 1)) {
                                   error+= ' Dải số phải liên tiếp với các dải đã khai, Từ số phải bắt đầu từ '+(rows[0].max + 1)+'. (The range of numbers must be consecutive with the declared ranges, The word number must start from '+ (rows[0].max + 1) 
                                   throw new Error(error)
                            }
                            if (moment(fd).format("YYYYMMDD") < moment(rows[0].fd).format("YYYYMMDD")) {
                                error+= ' Ngày hiệu lực phải lớn hơn hoặc bằng các dải đã khai '+'(The effective date must be greater than or equal to the declared ranges)'
                                throw new Error(error)
                         }
                            
                        }
                        sql = `select max(priority) max from s_serial where taxc=@1 and form=@2 and serial=@3`
                        result = await dbs.query(sql, [taxc,form,seri])
                        let   rowss = result.recordset
                        if (rowss.length > 0 && rowss[0].max!=null) {
                          
                            if (priority != (rowss[0].max + 1)) {
                                   error+= ' Độ ưu tiên phải liên tiếp với các dải đã khai, Độ ưu tiên mới phải là '+(rowss[0].max + 1)+'.(Priority must be consecutive with declared ranges, New precedence must be '+ (rowss[0].max + 1)+'.)'
                                   throw new Error(error)
                            }
                            
                        }
                        //end check
                    }
                    if (util.isEmpty(error)){
                        result = await dbs.queries(`insert into s_serial (type,form,serial,min,max,cur,fd,uc,uses,taxc,des,priority,sendtype,invtype,degree_config) values (@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15)`, binds)
                        for(const bind of binds){
                            let result_id = await dbs.query(`select *  from s_serial where taxc=@1 and form=@2 and serial=@3`, [bind[9],bind[1],bind[2]])
                            let id, rows = result_id.recordset
                            if(rows.length >0) id=rows[0].id
                            const sSysLogs = { fnc_id: 'ser_ins', src: config.SRC_LOGGING_DEFAULT, dtl: `Tạo thông báo phát hành : ${id}`, msg_id:id , doc: JSON.stringify(body)};
                            logging.ins(req,sSysLogs,next)
                        }      
                    }else{
                        throw new Error(error)
                       
                    }
                  
                }
                else if (operation == "delete") {
                    sql = `delete from s_serial where id=@1 and status=3 `
                    binds = [body.id]
                    result = await dbs.query(sql, binds)
                    const sSysLogs = { fnc_id: 'ser_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa thông báo phát hành: ${body.id}`, msg_id: body.id, doc: JSON.stringify(body)};
                    logging.ins(req,sSysLogs,next)
                }
            }
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },
    syncs: (taxc) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result, rows, binds,str = "",where=""
                let arr = taxc.split(",")
                for (const row of arr) {
                    str += `'${row}',`
                }
                where += `  taxc in (${str.slice(0, -1)})`
                result = await dbs.query(`select id,form,serial,cur,min,max,taxc from s_serial where ${where} and status=1`, [])
                rows = result.recordset
                if (rows.length > 0) {
                    binds = []
                    for (const row of rows) {
                        const key = `SERIAL.${row.taxc}.${row.form}.${row.serial}`
                        const cur = await redis.get(key)
                        if (cur && cur > row.cur && cur >= row.min && cur<=row.max) {
                            result = await dbs.query(`update s_serial set cur=@1 where id=@2 and status=1`, [cur, row.id])
                        }
                    }
                }
                result = await dbs.query(`select id,form,serial,max from s_serial where ${where} and status=1 and cur=max`, [])
                rows = result.recordset
                if (rows.length > 0) {
                    const dt = new Date()
                    binds = []
                    for (const row of rows) {
                        const key = `SERIAL.${row.taxc}.${row.form}.${row.serial}`, kax = `${key}.max`,kreq=`${key}.req`
                        let val = await redis.get(kax)
                        if (val == row.max)
                        {
                            await redis.del([key, kax,kreq])
                            binds.push([dt, row.id])
                        }
                    }
                    if (binds.length > 0) {
                        result = await dbs.queries(`update s_serial set status=4,td=@1 where id=@2 and status=1`, binds)
                    }
                }

                resolve()
            }
            catch (err) {
                reject(err)
            }
        })
    },
    sync: async (req, res, next) => {
        try {
            const taxc = req.params.taxc
            await Service.syncs(taxc)
            res.json("Đã đồng bộ số hiện tại /n (Current number synchronized)")
        }
        catch (err) {
            next(err)
        }
    },
    apiseq: async (req, res, next) => {
        try {
            let json = req.body, idt = (moment(json.idt, 'YYYY-MM-DD')).startOf("day").format("YYYY-MM-DD HH:mm:ss"), taxc = json.taxc, form = json.form, serial = json.serial
            logger4app.debug(`apiseq : idt - ${idt}, taxc - ${taxc}, form - ${form}, serial - ${serial}`)
            const seq = await Service.sequence(taxc, form, serial, idt)
            res.json({ result: seq})
        }
        catch (err) {
            logger4app.debug(`apiseq error : `,err)
            next(err)
        }
    },
    syncredisdb: async (req, res, next) => {
        try {
            let id = req.params.id, result, rows, row,sql,error=""
            //result = await dbs.query(`delete from s_serial where priority>1 `, [])
            result = await dbs.query(`select taxc,type,form,serial,min,max,cur,priority,fd ,status from s_serial where status=1 ${(!id) ? 'and uses = 2' : ''} order by taxc,form,serial desc`, [])
            rows = result.recordset
            if (rows.length == 0) logger4app.debug( result.toString('khong tin thay dai so trong DB'));
         
            for (let row of rows) {
                let uk = `${row.taxc}.${row.form}.${row.serial}`, key = `SERIAL.${uk}`, kax = `${key}.max`,kreq=`${key}.req`,ker = `${key}.err`
                let cur = row.cur, min = row.min, max = row.max, val = min-1 , condition = `${row.min}.${row.max}.${moment(row.fd).format('YYYYMMDD')}.${row.priority}__`
               if(row.status == 1){
                //await redis.del([key, kax, ker,kreq])
                   //Sửa lại hàm set multi thành nhiều hàm set do DB không có hàm này (áp dụng cho SCB)
                   
                  let curred = await redis.get(key)
                  if(!curred || cur > curred){
                        if (dbCache) {
                            redis.set(key, cur)
                            redis.set(kax, max)
                            redis.set(kreq, condition)
                        } else {
                            redis.multi().set(key, cur).set(kax, max).set(kreq, condition).exec(async (err, results) => {
                                if (err) throw new Error(err)
                                logger4app.debug('Dong bo dai: ' + key)
    
                            })
                        }
                  }else{
                      if(curred > cur) await dbs.query(`update s_serial set cur = @1 where id = @2 and status = 1 and min <= @1 and max >= @1`, [curred, row.id])
                      if(curred == max) await dbs.query(`update s_serial set cur = @1,status = 4 where id = @2 and status = 1 and min <= @1 and max >= @1`, [curred, row.id])
                    
                 }
                   
               }
               error = error+ ' Dong bo dai: ' + key
            }
            res.json({ result: "1"})
        }
        catch (err) {
            res.json({ result: "0"})
        }
    },
    syncredisdbsgr: async (taxc, form, serial) => {
        try {
            let result, rows
            //result = await dbs.query(`delete from s_serial where priority>1 `, [])
 
            result = await dbs.query(`select max(CAST(seq AS int)) seq from s_inv where stax = @1 and form = @2 and serial = @3`, [taxc, form, serial])
            rows = result.recordset
            let vseq = rows[0].seq
            await dbs.query(`update s_serial set cur = @1 where taxc = @2 and form = @3 and serial = @4`, [vseq, taxc, form, serial])
            
            const uk = `${taxc}.${form}.${serial}`, key = `SERIAL.${uk}`
            await redis.set(key, vseq)
        }
        catch (err) {
            throw err
        }
    },
    docx: async (req, res, next) => {
        try {
            const token = sec.decode(req)
            const type = req.params.type, taxc = req.params.taxc, org = await ous.obt(taxc)
            const fn = path.join(__dirname, "..", "..", "..", `temp/${type}.docx`), file = fs.readFileSync(fn)
            const time = moment().format(config.mfd).split("/"), srdt = `ngày ${time[0]} tháng ${time[1]} năm ${time[2]}`
            let rs, rows, arr = [], i = 1, data
            for (let i in org) if (!org[i]) org[i] = ""
            if (type == "sqdsd") {
                rs = await dbs.query(`select type,form,serial from s_serial where taxc=@1 and status=3`, [taxc])
                rows = rs.recordset
                for (const row of rows) {
                    arr.push({ sindex: i++, stype: util.tenhd(row.type), sform: row.form, sserial: row.serial })
                }
                const staxo = await taxon(org.taxo)
                data = { stax: taxc, sname: org.name.toUpperCase(), saddress: org.addr, stel: org.tel, staxo: staxo, srdt: srdt, arrSer: arr }
            }
            else if (type == "stbph") {
                rs = await dbs.query(`select type,form,serial,min,max,fd from s_serial where taxc=@1 and status=3`, [taxc])
                rows = rs.recordset
                for (const row of rows) {
                    arr.push({ id: i++, serName: util.tenhd(row.type), serForm: row.form, serSerial: row.serial, serSum: row.max, fromNum: row.min.toString().padStart(config.SEQ_LEN, '0'), toNum: row.max.toString().padStart(config.SEQ_LEN, '0'), fd: moment(row.fd).format(config.mfd) })
                }
                const staxo = await taxon(org.taxo)
                data = { stax: taxc, sname: org.name, saddress: org.addr, stel: org.tel, staxo: staxo, srdt: srdt, arrSerial: arr }
            }
            else if (type == "sdktd") {
                // rs = await dbs.query("select * from s_ca where taxc=?", [token.taxc])
                // rows = rs.rows
                // for (const row of rows) {
                //     //sIssuer: (row.issuer).split('=')[1].split(',')[0],
                //     arr.push({ id: i++, sSerial: row.serial, sSubject: row.subject, sIssuer: row.issuer, fd: moment(row.fd).format(config.mfd), td: moment(row.td).format(config.mfd) })
                // }
                arr.push({id:"", sIssuer:"", sSerial:"", fd:"", td:""})
                data = { stax: token.taxc, sname: token.on, saddress: org.addr, stel: org.tel, smail: org.mail, srdt: srdt, ser: token.fn, arrCa: arr }
            }
            const zip = new jszip(file)
            const doc = new docxt()
            doc.loadZip(zip)
            doc.setData(data)
            doc.render()
            const out = doc.getZip().generate({ type: "nodebuffer" })
            res.end(out, "binary")
        } catch (err) {
            next(err)
        }
    },
    xmlall: async (req, res, next) => {
        try {
            const token = sec.decode(req)
            const fullname = token.fn
            logger4app.debug(req.params.taxc)
            let str = "",taxc=req.params.taxc, arr = taxc.split(","),where=""
            for (const row of arr) {
                str += `'${row}',`
            }
            where = `in (${str.slice(0, -1)})`
            let rs, rows ,rs2, rows2
            var Parser = require("fast-xml-parser").j2xParser;
            rs = await dbs.query(`select ROW_NUMBER() OVER(ORDER BY A.ID ASC) AS STT,A.ID,A.TAXC,A.TYPE,A.FORM,A.SERIAL,A.MIN,A.MAX,A.CUR,A.STATUS,format(A.FD,'yyyy-MM-dd')as FD,A.TD,A.DT,A.UC,A.USES,B.TAXO,B.TEL,B.MAIL,B.NAME,
            (select top 1 name from  S_loc where id =B.PROV) as PROV,
            (select top 1 name from  S_loc where id =B.DIST) as DIST,
            (select top 1 name from  S_loc where id =B.WARD) as WARD,
            B.ADDR,B.FADD,B.PROV as PROV_ID,B.DIST as DIST_ID,B.WARD as WARD_ID,
            (select top 1 NAME from s_ou where ID = (select top 1 PID from s_ou where Taxc=B.TAXC)) as NAMECHUQUAN,
            (select top 1 TAXC from s_ou where ID = (select top 1 PID from s_ou where Taxc=B.TAXC)) as TAXCCHUQUAN,
            (select top 1 NAME from S_TAXO where ID = B.TAXO ) as COQUANTIEPNHAN
            from s_serial A 
            inner join s_ou B on (A.TAXC=B.TAXC) 
            left join s_taxo C on (B.taxo=C.ID) where A.status=3 and A.TAXC ${where}`, []) 
            rows = rs.recordset
            var jsonchitiet =[]
            rows.forEach(row => {
                var chitiet = {
                        "tenLoaiHDon": config.ITYPE.find(item => item.id === row.TYPE).value,
                        "mauSo": row.FORM === null?"":row.FORM,
                        "kyHieu": row.SERIAL === null?"":row.SERIAL,
                        "soLuong": Number(row.MAX)-Number(row.MIN)+1,
                        "tuSo": row.MIN,
                        "denSo": row.MAX,
                        "ngayBDauSDung": row.FD === null?"":row.FD,
                        "DoanhNghiepIn": {
                            "ten":"",
                            "mst": ""
                        },
                        "HopDongDatIn": {
                            "so":"",
                            "ngay": ""
                        }
                }
                jsonchitiet.push(chitiet)
            });
            var json = {}
            if(rows.length==0){
                rs2 = await dbs.query(`select ROW_NUMBER() OVER(ORDER BY B.ID ASC) as STT,B.TAXC,B.TAXO,B.TEL,B.MAIL,B.NAME,
                (select top 1 name from  S_loc where id =B.PROV) as PROV,
                (select top 1 name from  S_loc where id =B.DIST) as DIST,
                (select top 1 name from  S_loc where id =B.WARD) as WARD,
                B.ADDR,B.FADD,B.PROV as PROV_ID,B.DIST as DIST_ID,B.WARD as WARD_ID,
                (select top 1 NAME from s_ou where ID = (select top 1 PID from s_ou where Taxc=B.TAXC)) as NAMECHUQUAN,
                (select top 1 TAXC from s_ou where ID = (select top 1 PID from s_ou where Taxc=B.TAXC)) as TAXCCHUQUAN,
                (select top 1 NAME from S_TAXO where ID = B.TAXO ) as COQUANTIEPNHAN
                from s_ou B  
                left join s_taxo C on (B.taxo=C.ID) where B.TAXC ${where}`, [])
                rows2 = rs2.recordset
                json = {
                    "HSoThueDTu": {
                        "HSoKhaiThue": {
                            "TTinChung": {
                                "TTinDVu": {
                                "maDVu": "ETAX",
                                "tenDVu": "ETAX 1.0",
                                "pbanDVu": "1.0",
                                "ttinNhaCCapDVu": "ETAX_TCT"
                                },
                                "TTinTKhaiThue": {
                                "TKhaiThue": {
                                "maTKhai": "106",
                                "tenTKhai": "Thông báo phát hành hóa đơn",
                                "moTaBMau":"",
                                "pbanTKhaiXML": "2.1.2",
                                "loaiTKhai": "C",
                                "soLan": "0",
                                "KyKKhaiThue": {
                                    "kieuKy": "D",
                                    "kyKKhai": moment().format('DD/MM/YYYY'),
                                    "kyKKhaiTuNgay": moment().format('DD/MM/YYYY'),
                                    "kyKKhaiDenNgay": moment().format('DD/MM/YYYY'),
                                    "kyKKhaiTuThang": "",
                                    "kyKKhaiDenThang": ""
                                },
                                "maCQTNoiNop": rows2[0].TAXO === null?"":rows2[0].TAXO,
                                "tenCQTNoiNop": rows2[0].NAME === null?"":rows2[0].NAME,
                                "ngayLapTKhai": moment().format('DD/MM/YYYY'),
                                "nguoiKy": fullname === null?"":fullname,
                                "ngayKy": moment().format('DD/MM/YYYY'),
                                "nganhNgheKD":""
                                },
                                "NNT": {
                                "mst": rows2[0].TAXC === null?"":rows2[0].TAXC,
                                "tenNNT": rows2[0].NAME === null?"":rows2[0].NAME,
                                "dchiNNT": rows2[0].ADDR === null?"":rows2[0].ADDR,
                                "phuongXa": rows2[0].WARD === null?"":rows2[0].WARD,
                                "maHuyenNNT":rows2[0].DIST_ID === null?"":rows2[0].DIST_ID,
                                "tenHuyenNNT": rows2[0].DIST === null?"":rows2[0].DIST,
                                "maTinhNNT":rows2[0].PROV_ID === null?"":rows2[0].PROV_ID,
                                "tenTinhNNT": rows2[0].PROV === null?"":rows2[0].PROV,
                                "dthoaiNNT": rows2[0].TEL === null?"":rows2[0].TEL,
                                "faxNNT": "",
                                "emailNNT": rows2[0].MAIL === null?"":rows2[0].MAIL
                                }
                                }
                            },
                            "CTieuTKhaiChinh": {
                                "HoaDon": {
                                    "ChiTiet":{}
                                },
                                "DonViChuQuan": {
                                    "ten": rows2[0].NAMECHUQUAN === null?"":rows2[0].NAMECHUQUAN,
                                    "mst": rows2[0].TAXCCHUQUAN === null?"":rows2[0].TAXCCHUQUAN
                                },
                                "tenCQTTiepNhan": rows2[0].COQUANTIEPNHAN === null?"":rows2[0].COQUANTIEPNHAN,
                                "nguoiDaiDien": fullname === null?"":fullname,
                                "ngayBCao": moment().format('DD/MM/YYYY')
                            }
                        },
                        "CKyDTu":""
                    }
                }
            }else{
                json = {
                    "HSoThueDTu": {
                        "HSoKhaiThue": {
                            "TTinChung": {
                                "TTinDVu": {
                                "maDVu": "ETAX",
                                "tenDVu": "ETAX 1.0",
                                "pbanDVu": "1.0",
                                "ttinNhaCCapDVu": "ETAX_TCT"
                                },
                                "TTinTKhaiThue": {
                                "TKhaiThue": {
                                "maTKhai": "106",
                                "tenTKhai": "Thông báo phát hành hóa đơn",
                                "moTaBMau":"",
                                "pbanTKhaiXML": "2.1.2",
                                "loaiTKhai": "C",
                                "soLan": "0",
                                "KyKKhaiThue": {
                                    "kieuKy": "D",
                                    "kyKKhai": moment().format('DD/MM/YYYY'),
                                    "kyKKhaiTuNgay": moment().format('DD/MM/YYYY'),
                                    "kyKKhaiDenNgay": moment().format('DD/MM/YYYY'),
                                    "kyKKhaiTuThang": "",
                                    "kyKKhaiDenThang": ""
                                },
                                "maCQTNoiNop": rows[0].TAXO === null?"":rows[0].TAXO,
                                "tenCQTNoiNop": rows[0].NAME === null?"":rows[0].NAME,
                                "ngayLapTKhai": moment().format('DD/MM/YYYY'),
                                "nguoiKy": fullname === null?"":fullname,
                                "ngayKy": moment().format('DD/MM/YYYY'),
                                "nganhNgheKD":""
                                },
                                "NNT": {
                                "mst": rows[0].TAXC === null?"":rows[0].TAXC,
                                "tenNNT": rows[0].NAME === null?"":rows[0].NAME,
                                "dchiNNT": rows[0].ADDR === null?"":rows[0].ADDR,
                                "phuongXa": rows[0].WARD === null?"":rows[0].WARD,
                                "maHuyenNNT":rows[0].DIST_ID === null?"":rows[0].DIST_ID,
                                "tenHuyenNNT": rows[0].DIST === null?"":rows[0].DIST,
                                "maTinhNNT":rows[0].PROV_ID === null?"":rows[0].PROV_ID,
                                "tenTinhNNT": rows[0].PROV === null?"":rows[0].PROV,
                                "dthoaiNNT": rows[0].TEL === null?"":rows[0].TEL,
                                "faxNNT": "",
                                "emailNNT": rows[0].MAIL === null?"":rows[0].MAIL
                                }
                                }
                            },
                            "CTieuTKhaiChinh": {
                                "HoaDon": {
                                    ChiTiet:jsonchitiet
                                },
                                "DonViChuQuan": {
                                    "ten": rows[0].NAMECHUQUAN === null?"":rows[0].NAMECHUQUAN,
                                    "mst": rows[0].TAXCCHUQUAN === null?"":rows[0].TAXCCHUQUAN
                                },
                                "tenCQTTiepNhan": rows[0].COQUANTIEPNHAN === null?"":rows[0].COQUANTIEPNHAN,
                                "nguoiDaiDien": fullname === null?"":fullname,
                                "ngayBCao": moment().format('DD/MM/YYYY')
                            }
                        },
                        "CKyDTu":""
                    }
                }
            }
            var parser = new Parser();
            
            var xml = parser.parse(json);
            xml = xml.replace(`<HSoThueDTu>`, `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n<HSoThueDTu xmlns="http://kekhaithue.gdt.gov.vn/TKhaiThue" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">`)
            xml= xml.replace(`<mst/>`,`<mst xsi:nil="true"/>`)
            xml= xml.replace(`<ngay/>`,`<ngay xsi:nil="true"/>`)
            rows.forEach(row => {
                var bienid=`<ChiTiet ID="${row.STT}">`
                xml= xml.replace(`<ChiTiet>`,bienid)
            });
            res.end(xml)
        } catch (err) {
            next(err)
        }
    },
    getAllTypeByTax: async (req, res, next) => {
        try {
            const token = sec.decode(req)
            const result = await dbs.query(`select distinct type as "id", type as "value" from s_serial where taxc=@1 and degree_config = '123'`, [token.taxc])
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    }
}
module.exports = Service   