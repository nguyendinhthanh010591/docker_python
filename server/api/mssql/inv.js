"use strict"
const Hashids = require("hashids/cjs")
const moment = require("moment")
const sql = require("mssql")
const config = require("../config")
const logger4app = require("../logger4app")
const email = require("../email")
const sign = require("../sign")
const spdf = require("../spdf")
const util = require("../util")
const tvan = require("../tvan")
const redis = require("../redis")
const hbs = require("../hbs")
const fcloud = require("../fcloud")
const hsm = require("../hsm")
const SEC = require("../sec")
const SERIAL = require("./serial")
const dbs = require("./dbs")
const COL = require("./col")
const ous = require("./ous")
const user = require("./user")
const path = require("path")
const fs = require("fs")
const ifile = require(`./ifile`)
const xlsxtemp = require("xlsx-template")
const logging = require("../logging")
const { error } = require("console")
const ftp = require("../ftp")
const sms = require("../sms")
const rsmq = require("../rsmq")
const qname = config.qname
const hashids = new Hashids(config.secret, 10, "abcdefghijklmnopqrstuvwxyz1234567890")
const CARR = config.CARR
const grant = config.ou_grant
const serial_usr_grant = config.serial_usr_grant
const mfd = config.mfd
const dtf = config.dtf
const ENT = config.ent
const invoice_seq = config.invoice_seq
const UPLOAD_MINUTES = config.upload_minute
const useJobSendmail = config.useJobSendmail
const rbi = (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            const result = await dbs.query("select doc doc, uc uc, ou ou, c0 c0, c1 c1 , c2 c2 ,c3 c3 ,c4 c4, c5 c5, c6 c6, c7 c7,c8 c8,c9 c9 from s_inv where id=@1", [id])
            const rows = result.recordset
            if (rows.length == 0) reject(new Error(`Không tìm thấy Hóa đơn ${id} \n (Invoice not found ${id})`))
            let doc = JSON.parse(rows[0].doc), uc = rows[0].uc, ou = rows[0].ou, c0 = rows[0].c0, c1 = rows[0].c1, c2 = rows[0].c2, c3 = rows[0].c3,
                c4 = rows[0].c4, c5 = rows[0].c5, c6 = rows[0].c6, c7 = rows[0].c7, c8 = rows[0].c8, c9 = rows[0].c9
            doc.uc = uc
            doc.ou = ou
            doc.c0 = c0
            doc.c1 = c1
            doc.c2 = c2
            doc.c3 = c3
            doc.c4 = c4
            doc.c5 = c5
            doc.c6 = c6
            doc.c7 = c7
            doc.c8 = c8
            doc.c9 = c9
            doc.ent = ENT
            if (ENT == 'vcm') {
                let taxc_root = await ous.pidtaxc(ou)
                doc.taxc_root = taxc_root
            }
            //Lấy thông tin user của user lập hóa đơn
            let usercreate = await user.obid(uc)
            //Lấy thông tin ou của user lập hóa đơn
            let useroucreate = await ous.obid(ou)
            //Gán vào doc và trả ra
            doc.usercreate = usercreate
            doc.useroucreate = useroucreate
            doc.ouname = useroucreate.name
            doc.acccf = useroucreate.acc //config thong tin cho sgr
            //Lay thong tin so thu tu template de sau nay dung truy van template hoa don
            const org = await ous.obt(doc.stax), idx = org.temp, fn = util.fn(doc.form, idx)
            doc.tempfn = fn
            resolve(doc)
        } catch (err) {
            reject(err)
        }
    })
}
const oubi = (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            const result = await dbs.query("select name name from s_ou where id=@1", [id])
            let name
            const rows = result.recordset
            if (rows.length == 0)
                name = ""
            else
                name = rows[0].name
            resolve(name)
        } catch (err) {
            reject(err)
        }
    })
}
const chkou = async (id, token) => {
    if (grant) {
        if (token.ou !== token.u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)
        const result = await dbs.query(`select ou from s_inv where id=@1`, [id])
        const rows = result.recordset
        if (rows.length > 0 && rows[0].ou !== token.u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)
    }
}
const chkDds = async (ic, rows) => {
    let check = false
    for (let row of rows) {
        if (ic == row.CustomerAbbr) check = true
    }

    if (check) return 1
    else return 0

}
const chkDdsPass = async (ic, rows) => {
    let pass = 0
    for (let row of rows) {
        if (ic == row.CustomerAbbr) return pass = row.PASSWORD
    }
    return pass
}
const Service = {
    getcol: async (json) => {
        try {
            let doc = JSON.parse(JSON.stringify(json))
            let configfield = {}
            let configfieldhdr = await COL.cache(doc.type, 'vi')
            let configfielddtl = await COL.cachedtls(doc.type, 'vi')
            configfield.configfieldhdr = configfieldhdr
            configfield.configfielddtl = configfielddtl
            doc.configfield = configfield
            let doctmp = await rbi(json.id)
            doc.org = doctmp.useroucreate

            return doc
        }
        catch (err) {
            throw err
        }
    },
    invdocbid: async (invid) => {
        try {
            let doc = await rbi(invid)
            return doc
        }
        catch (err) {
            logger4app.debug(`invdocbid ${invid}:`, err)
        }
    },
    insertDocTempJob: async (invid, doc, xml) => {
        try {
            if (!config.useRedisQueueSendmail) {
                await dbs.query(`INSERT INTO s_invdoctemp (invid, doc, status, xml) values (@1,@2,@3,@4)`, [invid, JSON.stringify(doc), 0, xml])
            } else {
                await rsmq.sendMessageAsync({ qname: qname, message: JSON.stringify({ invid: invid, status: doc.status, subject: doc.subject, bmail: doc.bmail, sendmanual: doc.sendmanual }) })
            }
        }
        catch (err) {
            logger4app.debug(`insertDocTempJob ${invid}:`, err.message)
        }
    },
    insertDocTempJobAPI: async (invid, doc, xml) => {
        try {
            if (!config.useRedisQueueSendmailAPI) {
                await dbs.query(`INSERT INTO s_invdoctemp (invid, doc, status, xml) values (@1,@2,@3,@4)`, [invid, JSON.stringify(doc), 0, xml])
            } else {
                await rsmq.sendMessageAsync({ qname: qname, message: JSON.stringify({ invid: invid, status: doc.status, subject: doc.subject, bmail: doc.bmail, sendmanual: doc.sendmanual }) })
            }
        }
        catch (err) {
            logger4app.debug(`insertDocTempJob API ${invid}:`, err.message)
        }
    },
    updateSendMailStatus: async (doc) => {
        try {
            let sql, binds
            //Cập nhật ngày và trạng thái gửi mail, số lần gửi mail
            logger4app.debug(`worker.mailshortmsg doc ${JSON.stringify(doc)}`)
            let docupd = await rbi(doc.id)
            logger4app.debug(`worker.mailshortmsg docupd ${JSON.stringify(docupd)}`)
            if (docupd.SendMailNum) {
                try {
                    docupd.SendMailNum = parseInt(docupd.SendMailNum) + 1
                } catch (err) { docupd.SendMailNum = 1 }
            } else {
                docupd.SendMailNum = 1
            }
            docupd.SendMailStatus = 1
            docupd.SendMailDate = moment(new Date()).format("YYYY-MM-DD HH:mm:ss")
            sql = `update s_inv set doc=@1 where id=@2`
            binds = [JSON.stringify(docupd), doc.id]
            await dbs.query(sql, binds)
        }
        catch (err) {
            throw err
        }
    },
    updateSendSMSStatus: async (doc, smsstatus) => {
        try {
            let sql, binds
            let docupd = await rbi(doc.id)
            if ((config.sendApprSMS) || (config.sendCancSMS)) {
                //Cập nhật ngày và trạng thái gửi SMS, số lần gửi SMS
                if (docupd.SendSMSNum) {
                    try {
                        docupd.SendSMSNum = parseInt(docupd.SendSMSNum) + 1
                    } catch (err) { docupd.SendSMSNum = 1 }
                } else {
                    docupd.SendSMSNum = 1
                }
                docupd.SendSMSStatus = smsstatus.errnum == 0 ? 1 : -1
                docupd.SendSMSDate = moment(new Date()).format("YYYY-MM-DD HH:mm:ss")
                docupd.SendSMSDesc = smsstatus.resdesc
                sql = `update s_inv set doc=@1 where id=@2`
                binds = [JSON.stringify(docupd), doc.id]
                await dbs.query(sql, binds)
            }
        }
        catch (err) {
            throw err
        }
    },
    checkrefno: async (doc) => {
        try {
            if (!config.refno) return []
            let str = "", sql, arr = [], binds = [], where = "where 1=1 ", mappingfield = config.refno.mappingfield, rows
            if (mappingfield.split(".")[0] == "doc") {
                rows = [doc]
            } else {
                rows = doc.items
            }
            const crr = eval(`[...new Set(rows.map(x => x.${mappingfield.split(".")[1]}))]`)
            crr.forEach(function (value, i) {
                str += `@rn${i},`
                binds.push(value)
            })
            where += ` and ref_no in (${str.slice(0, -1)}) `
            sql = `select ref_no "ref_no" from s_invd ${where}`
            const result = await dbs.query(sql, binds)
            rows = result.rows
            rows.forEach((v, i) => {
                arr.push(v.ref_no)
            })
            return arr
        } catch (err) {
            throw err
        }
    },
    checkrefdate: async (doc) => {
        try {
            if (!config.refdate) return []
            let arr = [], mappingfield = config.refdate.mappingfield, rows
            const idt = moment(doc.idt, dtf).toDate()
            if (mappingfield.split(".")[0] == "doc") {
                rows = [doc]
            } else {
                rows = doc.items
            }
            const crr = eval(`[...new Set(rows.map(x => x.${mappingfield.split(".")[1]}))]`)
            crr.forEach(function (value, i) {
                let refdate = moment(value, dtf).toDate()
                if (refdate > idt) arr.push(value)
            })

            return arr
        } catch (err) {
            throw err
        }
    },
    saverefno: async (doc) => {
        try {
            const id = doc.id
            if (!config.refno) return 1
            let numc = 0, sql = `INSERT INTO s_invd (ref_no,inv_id) VALUES (@refno,@id) `, mappingfield = config.refno.mappingfield, rows
            if (mappingfield.split(".")[0] == "doc") {
                rows = [doc]
            } else {
                rows = doc.items
            }
            const crr = eval(`[...new Set(rows.map(x => x.${mappingfield.split(".")[1]}))]`)
            for (let i = 0; i < crr.length; i++) {
                let value = crr[i]
                try {
                    await dbs.query(sql, [value, id])
                    numc++
                } catch (error) {
                    logger4app.error(error)
                }
            }
            return numc
        } catch (err) {
            throw err
        }
    },
    delrefno: async (doc) => {
        try {
            if (!config.refno) return 1
            let numc = 0, sql = `DELETE from s_invd where ref_no = @1 `, mappingfield = config.refno.mappingfield, rows
            if (mappingfield.split(".")[0] == "doc") {
                rows = [doc]
            } else {
                rows = doc.items
            }
            const crr = eval(`[...new Set(rows.map(x => x.${mappingfield.split(".")[1]}))]`)
            for (let i = 0; i < crr.length; i++) {
                let value = crr[i]
                try {
                    await dbs.query(sql, [value])
                    numc++
                } catch (error) {
                    logger4app.error(error)
                }
            }
            return numc
        } catch (err) {
            throw err
        }
    },
    count12: async (req, res, next) => {
        try {
            const token = SEC.decode(req), query = req.query
            const result = await dbs.query(`select count(*) rc from s_inv where stax=@1 and form=@2 and serial=@3 and status in (1, 2)`, [token.taxc, query.form, query.serial])
            res.json(result.recordset[0])
        }
        catch (err) {
            next(err)
        }
    },
    seqget: async (req, res, next) => {
        try {
            // const token = SEC.decode(req), query = req.query, filter = JSON.parse(query.filter), start = query.start ? query.start : 0, count = query.count ? query.count : 10
            // let td = filter.td
            // let where = " where stax=? and form=? and serial=? and dt<=? and status=1"
            // let binds = [token.taxc, filter.form, filter.serial,td]
            // let sql = `select id id,idt idt,status status,ou ou,type type,form form,serial serial,seq seq,btax btax,bname bname,buyer buyer,baddr baddr,sumv sumv,vatv vatv,totalv totalv,sum "sum",vat "vat",total "total",adjdes adjdes,uc uc,ic ic from s_inv ${where} order by idt,id LIMIT ${count} OFFSET ${start}`
            // let result = await dbs.query(sql, binds)
            // let ret = { data: result[0], pos: start }
            // if (start == 0) {
            //     sql = `select count(*) total from s_inv ${where}`
            //     result = await dbs.query(sql, binds)
            //     ret.total_count = result[0][0].total
            // }
            // res.json(ret)
            const token = SEC.decode(req), query = req.query, filter = JSON.parse(query.filter), start = query.start ? query.start : 0, count = query.count ? query.count : 10
            let td = moment(filter.td).endOf("day").format("YYYY-MM-DD HH:mm:ss")
            let where = "where stax=@1 and form=@2 and serial=@3 and idt<=@4 and status=1"
            // if(ENT=='scb')where +=` and c0 in (select system from s_user_sys where userid='${token.uid}') `

            let binds = [token.taxc, filter.form, filter.serial, td]
            let sql = `select id,idt,status,ou,type,form,serial,seq,btax,bname,buyer,baddr,sumv,sum,vatv,vat,totalv,total,adjdes,uc,ic,c9 from s_inv ${where} order by idt,id  OFFSET ${start} ROWS FETCH NEXT ${count} ROWS ONLY`

            let result = await dbs.query(sql, binds)
            let ret = { data: result.recordset, pos: start }
            if (start == 0) {
                sql = `select count(*) total from s_inv ${where}`
                result = await dbs.query(sql, binds)
                ret.total_count = result.recordset[0].total
            }
            res.json(ret)
        } catch (error) {
            next(error)
        }
    },
    seqpost: async (req, res, next) => {
        let key
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const wait = 60, taxc = token.taxc, body = req.body, form = body.form, serial = body.serial, idt = moment(body.idt).format("YYYY-MM-DD HH:mm:ss"), seqList = body.seqList, idSD = (body.id) ? body.id : 0
            key = `APPR.${taxc}.${form}.${serial}`
            const val = await redis.get(key)

            if (val == "1") throw new Error(`Đang cấp số cho HĐ MST=${taxc} mẫu số=${form} ký hiệu=${serial}. Bạn hãy đợi ${wait}s ! \n (Sequencing for invoice Tax.No=${taxc} Form=${form} Serial=${serial}. Should be waiting for ${wait}s !)`)
            await redis.set(key, "1", "EX", wait)
            let sql, binds, where = ''
            if (seqList.length > 0) {
                // cấp 1 hoặc lỗ chỗ trong 1 ngày
                binds = [taxc, form, serial]
                let str = "", i = 4
                for (const row of seqList) {
                    str += `@${i++},`
                    binds.push(row)
                }
                where = ` where stax=@1 and form=@2 and serial=@3 and status=1 and id IN (${str.slice(0, -1)}) `
                //  if(ENT=='scb')where +=` and c0 in (select system from s_user_sys where userid='${token.uid}') `
                sql = `select id,pid,ou,adjtyp,idt,sec,c6,total from s_inv ${where} order by idt,id`
            } else {
                let docRS = await dbs.query(`select dt "dt" from s_inv where id = @1`, [idSD])
                const dt = (docRS.recordset && docRS.recordset.length > 0 && docRS.recordset[0].dt) ? docRS.recordset[0].dt : moment(moment(idt).endOf("day")).format(dtf)
                where = ` where dt <=@1 and stax=@2 and form=@3 and serial=@4 and status=1`
                //if(ENT=='scb')where +=` and c0 in (select system from s_user_sys where userid='${token.uid}') `
                // cấp theo dải bé hơn ngày hiện tại
                sql = `select id,pid,ou,adjtyp,idt,sec,c6,total from s_inv ${where} order by idt,dt,id`
                binds = [dt, taxc, form, serial]
            }
            const result = await dbs.query(sql, binds)
            const rows = result.recordset
            const sup = `update s_inv set doc=JSON_MODIFY(JSON_MODIFY(doc,'$.seq',@1),'$.status',2) where id=@2 and stax=@3 and status=1`, pup = `update s_inv set cde=@1,doc=@2 where id=@3`
            let count = 0, countER = 0
            for (const row of rows) {
                let seq, s7, rs, id, pid, cER = 0
                if (ENT == "scb") {
                    let docRS = await rbi(row.id)
                    for (const item of docRS.items) {
                        if (item.statusGD == 4) {
                            countER++
                            cER++
                            break
                        }
                    }
                    if (row.total < 0) {
                        cER++
                        countER++
                    }
                }
                if (cER != 0) {
                    continue
                }
                try {
                    id = row.id
                    seq = await SERIAL.sequence(taxc, form, serial, row.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    rs = await dbs.query(sup, [s7, id, taxc])
                    if (rs.rowsAffected[0] > 0) {
                        count++
                        pid = row.pid
                        if (!util.isEmpty(pid)) {
                            try {
                                let pdoc = await rbi(pid)
                                if (ENT == "sgr" && row.adjtyp == 1) {
                                    await dbs.query(`update s_inv set doc=@1 where id=@2`, [JSON.stringify(pdoc), pid])
                                }
                                else {
                                    const cde = `${(row.adjtyp == 1 ? "Bị thay thế" : "Bị điều chỉnh")} bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${moment(row.idt).format(mfd)} mã ${ENT != "vcm" ? row.sec : row.c6}`
                                    let pdoc = await rbi(pid)
                                    pdoc.cde = cde
                                    await dbs.query(pup, [cde, JSON.stringify(pdoc), pid])
                                }
                            } catch (err) { logger4app.error(err) }
                        }
                    }
                    else throw new Error(`Không cấp số cho hóa đơn ${id} \n (Don't provide numbers for invoice ${id})`)
                    const rtolog = await rbi(id)
                    const sSysLogs = { fnc_id: 'inv_seqpost', src: config.SRC_LOGGING_DEFAULT, dtl: `Cấp số hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(rtolog) };
                    logging.ins(req, sSysLogs, next)
                } catch (err) {
                    if (!((err.number == 2627) && (String(err.message).toUpperCase().indexOf("S_INV_UID_01") >= 0))) await SERIAL.err(taxc, form, serial, seq)
                    throw err
                }
            }
            if (ENT == "scb") {
                res.json({ count: count, countER: countER })
            } else {
                res.json(count)
            }
        }
        catch (err) {
            next(err)
        }
        finally {
            if (!util.isEmpty(key)) await redis.del(key)
        }
    },
    apprseqpost: async (req, res, next) => {
        let key
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const wait = 60, taxc = token.taxc, body = req.body, ids = body.ids, form = body.form, serial = body.serial, idt = new Date(body.idt)
            let i = 1, str = "", binds = []
            binds = [idt, taxc, form, serial]
            let sql, result
            sql = `select id,pid,ou,adjtyp,idt,sec,doc,bmail from s_inv where idt<=@1 and stax=@2 and form=@3 and serial=@4 and status=1 order by idt,id`

            result = await dbs.query(sql, binds)
            const rows = result.recordset
            if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
            if (rows.length > config.MAX_ROW_APPROVE) throw new Error(`Dữ liệu ký duyệt vượt quá ${config.MAX_ROW_APPROVE} cho phép \n (Signing data exceeds ${config.MAX_ROW_APPROVE} characters for allowed)`)
            key = `APPR.${taxc}.${form}.${serial}`
            const val = await redis.get(key)
            if (val == "1") throw new Error(`Đang duyệt cấp số cho HĐ MST=${taxc} mẫu số=${form} ký hiệu=${serial}. Bạn hãy đợi ${wait}s ! \n ( Approving to sequence for invoice Tax.No=${taxc} Form=${form} Serial=${serial}. Should be waiting for ${wait}s !)`)
            await redis.set(key, "1", "EX", wait)

            const org = await ous.obt(taxc), signtype = org.sign
            if (signtype == 1) {
                let arr = []
                for (const row of rows) {
                    const doc = JSON.parse(row.doc), id = row.id, xml = util.j2x(doc, id)
                    // push {inc: id,...} thay vi {id: id,...} vì con ký anh Đại đang để là inc thay vì id CLOUD
                    arr.push({ inc: id, xml: Buffer.from(xml).toString("base64") })
                }
                res.json({ mst: taxc, date: new Date(), ref: config.PATH_XML, arr: arr })
            }
            else {
                let count = 0, seq, s7
                sql = `update s_inv set doc=JSON_MODIFY(JSON_MODIFY(doc,'$.seq',@1),'$.status',3),xml=@2,dt=@3 where id=@4 and stax=@5 and status=1`

                if (signtype == 2) {
                    //hunglq chinh sua thong bao loi
                    let ca, pwd
                    try {
                        pwd = util.decrypt(org.pwd)
                        ca = await sign.ca(taxc, pwd)
                    } catch (err) {
                        throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)')
                    }
                    for (const row of rows) {
                        let doc = JSON.parse(row.doc)
                        const id = row.id

                        seq = await SERIAL.sequence(taxc, form, serial, row.idt)
                        s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                        doc.seq = s7
                        const xml = util.signature(sign.sign(ca, util.j2x((await Service.getcol(doc)), id), id), doc.idt)
                        await dbs.query(sql, [s7, xml, moment(new Date()).format(dtf), id, taxc])

                        if (doc.adj) {
                            let cdt = moment(new Date()).format(dtf)
                            if (!util.isEmpty(row.pid)) {
                                try {
                                    // let cde = `${(row.adjtyp == 1 ? "Bị thay thế" : "Bị điều chỉnh")} bởi HĐ số ${s7} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)} mã ${doc.sec}`
                                    // let pdoc = await rbi(row.pid)

                                    let cde = ''
                                    result = await dbs.query("select doc,cid,cde from s_inv where id=@1", [row.pid])
                                    const rows = result.recordset
                                    let cid, pdoc = JSON.parse(rows[0].doc)
                                    if (row.adjtyp == 2) {
                                        if (rows[0].cde) cde = rows[0].cde + `_/_`
                                        else cde = `Bị điều chỉnh`
                                    }

                                    if (row.adjtyp == 1) {
                                        cde = `Bị thay thế bởi HĐ số ${s7} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)}`
                                    }
                                    if (rows[0].cid) cid = rows[0].cid + "," + row.id
                                    else cid = row.id

                                    //HungLQ them lan dieu chinh cho VCM, neu on on thi sua chung
                                    if (config.ent == "vcm" && row.adjtyp == 2) {
                                        if (pdoc.adjnum) {
                                            pdoc.adjnum = parseInt(pdoc.adjnum) + 1
                                        } else {
                                            pdoc.adjnum = 1
                                        }
                                        //   cde = `Lần điều chỉnh thứ ${pdoc.adjnum}.` + cde
                                    }
                                    if (config.ent == "vcm" && row.adjtyp == 1) {
                                        pdoc.cdetemp = null
                                        pdoc.cdt = cdt
                                        pdoc.canceller = token.uid
                                        pdoc.status = 4

                                    }
                                    pdoc.cde = cde
                                    await dbs.query(`update s_inv set cde=@1,doc=@2,cid=@3 where id=@4`, [cde, JSON.stringify(pdoc), cid, row.pid])
                                } catch (err) { logger4app.error(err) }
                            }

                        }
                        doc.status = 3
                        if (useJobSendmail) {
                            await Service.insertDocTempJob(id, doc, xml)
                        } else {
                            if (!util.isEmpty(doc.bmail)) {
                                let mailstatus = await email.rsmqmail(doc)
                                if (mailstatus) await Service.updateSendMailStatus(doc)
                            }
                            if (!util.isEmpty(doc.btel)) {
                                let smsstatus = await sms.smssend(doc)
                                if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                            }
                        }
                        const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                        logging.ins(req, sSysLogs, next)
                        count++
                    }
                }
                else {

                    let kqs, usr, pwd
                    try {
                        if (signtype == 3) {
                            usr = org.usr
                            pwd = util.decrypt(org.pwd)
                            kqs = await fcloud.xml(usr, pwd, taxc, rows)
                        }
                        else kqs = await hsm.xml(rows)
                    } catch (err) {
                        throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)')
                    }
                    for (const row of kqs) {
                        let doc = JSON.parse(row.doc)
                        const id = row.id, xml = row.xml
                        seq = await SERIAL.sequence(taxc, form, serial, row.idt)
                        s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                        await dbs.query(sql, [s7, xml, id, taxc])
                        if (doc.adj) {
                            let cdt = moment(new Date()).format(dtf)
                            if (!util.isEmpty(row.pid)) {
                                try {
                                    let cde = `${(row.adjtyp == 1 ? "Bị thay thế" : "Bị điều chỉnh")} bởi HĐ số ${s7} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)} mã ${ENT != "vcm" ? doc.sec : doc.c6}`
                                    //let cde = row.adjtyp == 1?` Bị thay thế bởi HĐ số ${s7} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)} mã ${doc.sec}`:``
                                    let pdoc = await rbi(row.pid)
                                    //HungLQ them lan dieu chinh cho VCM, neu on on thi sua chung
                                    if (config.ent == "vcm" && row.adjtyp == 2) {
                                        if (pdoc.adjnum) {
                                            pdoc.adjnum = parseInt(pdoc.adjnum) + 1
                                        } else {
                                            pdoc.adjnum = 1
                                        }
                                        //cde= `Lần điều chỉnh thứ ${pdoc.adjnum}.` + cde
                                    }
                                    if (config.ent == "vcm" && row.adjtyp == 1) {
                                        pdoc.cdetemp = null
                                        pdoc.cdt = cdt
                                        pdoc.canceller = token.uid
                                        pdoc.status = 4

                                    }
                                    pdoc.cde = cde
                                    await dbs.query(`update s_inv set cde=@1,doc=@2 where id=@3`, [cde, JSON.stringify(pdoc), row.pid])
                                } catch (err) { logger4app.error(err) }
                            }

                        }

                        doc.status = 3
                        if (useJobSendmail) {
                            await Service.insertDocTempJob(id, doc, xml)
                        } else {
                            if (!util.isEmpty(doc.bmail)) {
                                let mailstatus = await email.rsmqmail(doc)
                                if (mailstatus) await Service.updateSendMailStatus(doc)
                            }
                            if (!util.isEmpty(doc.btel)) {
                                let smsstatus = await sms.smssend(doc)
                                if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                            }
                        }
                        const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                        logging.ins(req, sSysLogs, next)
                        count++
                    }
                }


                res.json(count)
            }
        }
        catch (err) {
            next(err)
        }
        finally {
            if (!util.isEmpty(key)) await redis.del(key)
        }
    },
    apprall: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, org = await ous.obt(taxc), signtype = org.sign
            if (signtype !== 2) throw new Error("Chỉ hỗ trợ kiểu ký bằng file \n (Only support sign by file type)")
            const iwh = await Service.iwh(req, true)
            const sql = `select id,doc,bmail from s_inv ${iwh.where}`
            const result = await dbs.query(sql, iwh.binds)
            const rows = result.recordset
            if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
            if (rows.length > config.MAX_ROW_APPROVE) throw new Error(`Dữ liệu ký duyệt vượt quá ${config.MAX_ROW_APPROVE} cho phép \n (Signing data exceeds ${config.MAX_ROW_APPROVE} characters for allowed)`)
            let pwd, ca
            //hunglq chinh sua thong bao loi
            try {
                pwd = util.decrypt(org.pwd)
                ca = await sign.ca(taxc, pwd)
            } catch (err) {
                throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)')
            }
            let count = 0
            const sql2 = `update s_inv set doc=JSON_MODIFY(doc,'$.status',3),xml=@1,dt=@2 where id=@3 and status=2`
            for (const row of rows) {
                let doc = JSON.parse(row.doc)
                const id = row.id, xml = util.signature(sign.sign(ca, util.j2x((await Service.getcol(doc)), id), id), doc.idt), result2 = await dbs.query(sql2, [xml, moment(new Date()).format(dtf), id])
                if (ENT == 'mzh') {
                    let filename = await ifile.getFileMailToFolder(doc.id)
                    await dbs.query("update  s_inv set file_name=@1 where id=@2", [filename, doc.id])
                } else {
                    if (result2.rowsAffected[0] > 0) {
                        doc.status = 3
                        if (useJobSendmail) {
                            await Service.insertDocTempJob(id, doc, xml)
                        } else {
                            if (!util.isEmpty(doc.bmail)) {
                                let mailstatus = await email.rsmqmail(doc)
                                if (mailstatus) await Service.updateSendMailStatus(doc)
                            }
                            if (!util.isEmpty(doc.btel)) {
                                let smsstatus = await sms.smssend(doc)
                                if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                            }
                        }
                        count++
                    }
                }

                const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                logging.ins(req, sSysLogs, next)
            }
            res.json(count)
        }
        catch (err) {
            next(err)
        }
    },
    apprpost: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const taxc = token.taxc, body = req.body, ids = body.ids
            let i = 1, str = "", binds = []
            for (const row of ids) {
                str += `@${i++},`
                binds.push(row)
            }
            binds.push(taxc)
            let sql, result
            sql = `select id,doc,bmail,pid from s_inv where id in (${str.slice(0, -1)}) and stax=@${i++} and status=2`
            result = await dbs.query(sql, binds)
            const rows = result.recordset
            if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
            if (rows.length > config.MAX_ROW_APPROVE) throw new Error(`Dữ liệu ký duyệt vượt quá ${config.MAX_ROW_APPROVE} cho phép \n (Signing data exceeds ${config.MAX_ROW_APPROVE} characters for allowed)`)
            const org = await ous.obt(taxc), signtype = org.sign
            if (signtype == 1) {
                let arr = []
                for (const row of rows) {
                    const doc = JSON.parse(row.doc), id = row.id, xml = util.j2x(doc, id)
                    // push {inc: id,...} thay vi {id: id,...} vì con ký anh Đại đang để là inc thay vì id CLOUD
                    arr.push({ inc: id, xml: Buffer.from(xml).toString("base64") })
                }
                res.json({ mst: taxc, date: new Date(), ref: config.PATH_XML, arr: arr })
            }
            else {
                let count = 0, seq, s7
                sql = `update s_inv set doc=@1,xml=@2,dt=@3 where id=@4 and stax=@5 and status=2`
                let subquery = `select pid from s_inv where id=@1 and pid is not null`

                if (signtype == 2) {
                    //hunglq chinh sua thong bao loi
                    let ca, pwd
                    try {
                        pwd = util.decrypt(org.pwd)
                        ca = await sign.ca(taxc, pwd)
                    } catch (err) {
                        throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)')
                    }
                    for (const row of rows) {
                        let doc = JSON.parse(row.doc)
                        const id = row.id
                        if (config.DEGREE_CONFIG == '123') {
                            util.viewadj(doc)
                            doc.adt = moment().format(dtf)
                            const inc = row.inc
                                , str = await util.j2x(doc, inc)
                                , PATH_XML = await util.getPathXml(doc.type)
                                , signSample = await util.getSignSample(doc.type, degree_config == "123" ? doc.adt : doc.idt, inc)
                                , sendtype = row.sendtype
                            if (util.isTicket(doc.type) && ts == "0") xml = str
                            else {
                                // xml = util.signature(sign.sign(ca, str, inc, PATH_XML, signSample), signSample)
                                xml = sign.sign(ca, str, inc, PATH_XML, signSample)
                            }

                            doc.msgType = 'INV'
                            let stt_cqt = tvan.sendMsgToTvanQueue(doc)
                            if (stt_cqt == 1) {
                                doc.statustaxo = 3
                                doc.status = 3
                            } else {
                                throw new Error('Lỗi không thể duyệt hoá đơn \n (Error cannot browse invoices)')
                            }
                            sql = `update s_inv set doc=@1,dt=@2 where id=@3 and stax=@4 and status=2`
                            await dbs.query(sql, [JSON.stringify(doc), moment(new Date()).format(dtf), id, taxc])
                        } else if (config.DEGREE_CONFIG == '119') {
                            const xml = util.signature(sign.sign(ca, util.j2x((await Service.getcol(doc)), id), id), doc.idt)
                            const subResult = await dbs.query(subquery, [id])
                            let subRows = subResult.recordset
                            if (subRows.length > 0 && ENT == "sgr" && doc.adj && doc.adj.typ == 1) {
                                const cde = `Bị thay thế bởi HĐ số ${doc.seq} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)} mã ${doc.sec}`
                                let cdt = moment(new Date()).format(dtf)
                                // let subQu=`update s_inv set doc=JSON_MODIFY(doc,'$.status',4) where id='${subRows[0].pid}'`
                                // await dbs.query(subQu)
                                await dbs.query(`update s_inv set doc=JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(doc,'$.cdt',@1),'$.status',4),'$.cde',@2),'$.canceller',@3),cid=@4,cde=@5 where id=@6`, [cdt, cde, token.uid, id, cde, subRows[0].pid])
                            }
                            await dbs.query(sql, [xml, moment(new Date()).format(dtf), id, taxc])
                            doc.status = 3
                            if (ENT == 'mzh') {
                                let filename = await ifile.getFileMailToFolder(doc.id)
                                await dbs.query("update  s_inv set file_name=@1 where id=@2", [filename, doc.id])
                            } else {
                                if (useJobSendmail) {
                                    await Service.insertDocTempJob(id, doc, xml)
                                } else {
                                    if (!util.isEmpty(doc.bmail)) {
                                        let mailstatus = await email.rsmqmail(doc)
                                        if (mailstatus) await Service.updateSendMailStatus(doc)
                                    }
                                    if (!util.isEmpty(doc.btel)) {
                                        let smsstatus = await sms.smssend(doc)
                                        if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                                    }
                                }
                            }
                        }
                        const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                        logging.ins(req, sSysLogs, next)
                        count++

                    }
                }
                else {
                    //hunglq chinh sua thong bao loi
                    let kqs, usr, pwd
                    try {
                        if (signtype == 3) {
                            usr = org.usr
                            pwd = util.decrypt(org.pwd)
                            kqs = await fcloud.xml(usr, pwd, taxc, rows)
                        }
                        else kqs = await hsm.xml(rows)
                    } catch (err) {
                        throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)')
                    }
                    for (const row of kqs) {
                        let doc = JSON.parse(row.doc)
                        const id = row.id, xml = util.signature(row.xml, doc.idt)
                        const subResult = await dbs.query(subquery, [id])
                        let subRows = subResult.recordset
                        if (subRows.length > 0 && ENT == "sgr" && doc.adj && doc.adj.typ == 1) {
                            let subQu = `update s_inv set doc=JSON_MODIFY(doc,'$.status',4) where id='${subRows[0].pid}'`
                            await dbs.query(subQu)
                        }
                        await dbs.query(sql, [xml, moment(new Date()).format(dtf), id, taxc])
                        doc.status = 3
                        if (useJobSendmail) {
                            await Service.insertDocTempJob(id, doc, xml)
                        } else {
                            if (!util.isEmpty(doc.bmail)) {
                                let mailstatus = await email.rsmqmail(doc)
                                if (mailstatus) await Service.updateSendMailStatus(doc)
                            }
                            if (!util.isEmpty(doc.btel)) {
                                let smsstatus = await sms.smssend(doc)
                                if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                            }
                        }
                        const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                        logging.ins(req, sSysLogs, next)
                        count++
                    }
                }
                if (config.DEGREE_CONFIG == '123') {
                    res.json({ count: count, nd123: 'nd123' })
                } else res.json(count)
            }
        }
        catch (err) {
            next(err)
        }
    },
    apprput: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, body = req.body, incs = body.incs, signs = body.signs
            let i = 1, str = "", binds = []
            for (const row of incs) {
                str += `@${i++},`
                binds.push(row)
            }
            binds.push(taxc)
            let sql, result, rows, count = 0
            sql = `select id,doc,bmail from s_inv where id in (${str.slice(0, -1)}) and stax=@${i++} and status=2 `
            result = await dbs.query(sql, binds)
            rows = result.recordset
            if (rows.length > 0) {
                sql = `update s_inv set doc=JSON_MODIFY(doc,'$.status',3),xml=@1,dt=@2 where id=@3 and stax=@4 and status=2`
                for (const row of rows) {
                    let doc = JSON.parse(row.doc)
                    const id = row.id, sign = signs.find(item => item.inc === id)
                    if (sign && sign.xml) {
                        const xml = util.signature(Buffer.from(sign.xml, "base64").toString(), doc.idt)
                        await dbs.query(sql, [xml, moment(new Date()).format(dtf), id, taxc])
                        doc.status = 3
                        if (useJobSendmail) {
                            await Service.insertDocTempJob(id, doc, xml)
                        } else {
                            if (!util.isEmpty(doc.bmail)) {
                                let mailstatus = await email.rsmqmail(doc)
                                if (mailstatus) await Service.updateSendMailStatus(doc)
                            }
                            if (!util.isEmpty(doc.btel)) {
                                let smsstatus = await sms.smssend(doc)
                                if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                            }
                        }
                        const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                        logging.ins(req, sSysLogs, next)

                        count++
                    }
                }
            }
            res.json(count)
        }
        catch (err) {
            next(err)
        }
    },
    iwh: async (req, all, decodereq) => {
        const token = (!decodereq) ? SEC.decode(req) : req.token, u = token.u, query = req.query, filter = JSON.parse(query.filter), sort = query.sort
        let keys = Object.keys(filter), kar = ["ou", "status", "type", "form", "serial", "paym", "curr", "cvt"], i = 4
        let tree = "", val, ext = "", extxls = "", cols, where, binds = [moment(new Date(filter.fd)).format(dtf), moment(moment(filter.td).endOf("day")).format(dtf), token.taxc], order, resultsgr, taxcsgr
        if (ENT == "sgr" || ENT == "vcm") {
            where = `where idt  between @1 and @2`
        } else where = `where idt  between @1 and @2 and stax=@3`
        //  if(ENT=='scb')where +=` and c0 in (select system from s_user_sys where userid='${token.uid}') `

        if (grant && token.ou == u) {
            //TODO
            if (all) {
                //tree = `with tree as (select ${u} id union all select child.id from s_ou child,tree parent where parent.id=child.pid)`
                //where += ` and ou in (select id from tree)`
            }
            else {
                where += ` and ou=@${i++}`
                binds.push(u)
            }
        }
        if (keys.includes("type")) {
            cols = await COL.cache(filter["type"])
            if (cols.length > 0) {
                let exts = []
                let extsxls = []
                for (const col of cols) {
                    let cid = col.id
                    exts.push(`${cid} ${cid}`)
                    //Xu ly rieng cho excel neu la truong date thi format lai
                    if (col.view == "datepicker")
                        extsxls.push(`FORMAT(CONVERT(DATETIME, ${cid}, 102),'dd/MM/yyyy') ${cid}`)
                    else
                        extsxls.push(`${cid} ${cid}`)
                }
                ext = `,${exts.join()}`
                extxls = `,${extsxls.join()}`
            }
        }
        for (const key of kar) {
            if (config.ent == "vcm" || config.ent == "sgr") {//tra cuu voi mutiselect
                if (keys.includes(key) && key.includes("serial")) {

                    val = filter[key]
                    let str = "", arr = String(val).split(",")
                    for (const row of arr) {
                        str += `'${row}',`
                    }
                    where += ` and ${key} in (${str.slice(0, -1)})`
                }
                else if (keys.includes(key) && key.includes("ou")) {

                    val = filter[key]
                    let str = "", arr = String(val).split(",")
                    for (const row of arr) {
                        let d = row.split("__")
                        str += `${d[0]},`
                    }
                    where += ` and ${key} in (${str.slice(0, -1)})`
                    if (ENT == "sgr" || ENT == "vcm") {
                        resultsgr = await dbs.query(`select taxc from s_ou where id in (${str.slice(0, -1)})`)
                        if (resultsgr.recordset.length > 0) {
                            taxcsgr = resultsgr.recordset
                            let str = ""
                            for (const oldtaxcsgr of taxcsgr) {
                                str += `'${oldtaxcsgr.taxc}',`
                            }
                            where += ` and stax in (${str.slice(0, -1)})`

                        }

                    }
                    //binds.push(filter[key])
                } else {
                    if (keys.includes(key)) {
                        where += ` and ${key}=@${i++}`
                        binds.push(filter[key])
                    }
                }
            } else {
                if (keys.includes(key)) {
                    where += ` and ${key}=@${i++}`
                    binds.push(filter[key])
                }
            }

        }

        kar.push("fd", "td")
        for (const key of kar) {
            delete keys[key]
        }
        for (const key of keys) {
            val = filter[key]
            if (val && !kar.includes(key)) {
                switch (key) {
                    case "seq":
                        where += ` and seq like @${i++}`
                        binds.push(`%${val}`)
                        break
                    case "btax":
                        where += ` and btax like @${i++}`
                        binds.push(`%${val}%`)
                        break
                    case "bacc":
                        where += ` and bacc like @${i++}`
                        binds.push(`%${val}%`)
                        break
                    case "id":
                        where += ` and id = @${i++}`
                        binds.push(val)
                        break
                    case "user":
                        where += ` and uc like @${i++}`
                        binds.push(`%${val}%`)
                        break
                    case "vat":
                        let catobj = JSON.parse(JSON.stringify(config.CATTAX))
                        let objtax = catobj.find(x => x.vatCode == val)
                        if (ENT == "vcm") {
                            where += ` and ((SELECT count(*) abc FROM OPENJSON (JSON_QUERY (doc,'$.tax')) WITH ( vrt numeric(26,4) '$.vrt',amt numeric(26,4) '$.amt',vat numeric(26,4) '$.vat') as a where (a.vrt =  @${i++} and (a.amt > 0 or (a.amt = 0 and a.vat != 0))) or (a.vrt =  @${i++} and  (TRY_CAST(json_value([doc],'$.score') AS numeric(26,4)) != 0 and a.amt > 0))) > 0)`
                            binds.push(String(objtax.vrt))
                            binds.push(String(objtax.vrt))
                        } else {
                            where += ` and ((SELECT count(*) abc FROM OPENJSON (JSON_QUERY (doc, '$.tax')) WITH ( vrt varchar(10) '$.vrt') as a  where a.vrt = @${i++}) > 0 or (SELECT count(*) abc FROM OPENJSON (JSON_QUERY (doc, '$.tax')) WITH ( vrt varchar(10) '$.vrt') as a  where a.vrt = @${i++}) > 0)`
                            binds.push(String(objtax.vrt))
                            binds.push(String(objtax.vrt))
                        }
                        break
                    case "adjtyp":
                        switch (val) {
                            case "3":
                                where += ` and cid is not null and cde like @${i++}`
                                binds.push("Bị thay thế%")
                                break
                            case "4":
                                where += ` and cid is not null and cde like @${i++}`
                                binds.push("Bị điều chỉnh%")
                                break
                            default:
                                where += ` and adjtyp=@${i++}`
                                binds.push(val)
                                break
                        }
                        break
                    case "sendmailstatus":
                        switch (val) {
                            case "0":
                                where += ` and sendmailstatus = @${i++}`
                                binds.push(val)
                                break
                            case "1":
                                where += ` and sendmailstatus = @${i++}`
                                binds.push(val)
                                break
                            default:
                                break
                        }
                        break
                    case "sendsmsstatus":
                        switch (val) {
                            case "0":
                                where += ` and sendmailstatus = @${i++}`
                                binds.push(val)
                                break
                            case "1":
                                where += ` and sendsmsstatus = @${i++}`
                                binds.push(val)
                                break
                            case "-1":
                                where += ` and sendsmsstatus = @${i++}`
                                binds.push(val)
                                break
                            default:
                                break
                        }
                        break
                    case "mail":
                        break
                    default:
                        let b = true
                        if (CARR.includes(key) && cols) {
                            let obj = cols.find(x => x.id == key)
                            if (typeof obj !== "undefined") {
                                const view = obj.view
                                if (view == "datepicker") {
                                    b = false
                                    where += ` and date(${key})=@${i++}`
                                    binds.push(new Date(val))
                                }
                                else if (view == "multicombo") {
                                    b = false
                                    where += ` and ${key} in (${val.split(",")})`
                                }
                                else if (view == "combo") {
                                    b = false
                                    where += ` and ${key}=@${i++}`
                                    binds.push(val)
                                }
                                else if (view == "text") {
                                    if (obj.type == "number") {
                                        b = false
                                        where += ` and ${key} like @${i++}`
                                        binds.push(`%${val}%`)
                                    }
                                }
                            }
                        }
                        if (b) {
                            where += ` and upper(${key}) like @${i++}`
                            binds.push(`%${val.toString().toUpperCase()}%`)
                        }
                        break
                }
            }
        }
        // if (serial_usr_grant) {
        //     where += ` and stax = ss.taxc and type = ss.types and form = ss.forms and serial = ss.serials`
        // }
        if (serial_usr_grant) {
            where += ` and exists (select 1 from s_serial ss, s_seusr ss2 where ss.id = ss2.se and ss2.usrid = '${token.uid}' and s_inv.stax = ss.taxc and s_inv.type = ss.type and s_inv.form = ss.form and s_inv.serial = ss.serial)`

        }
        // if (ENT == "scb" && config.scb_dept_check) {
        //     where += ` and ((c2 like '') or exists (select 1 from s_deptusr dd where s_inv.c2 = dd.deptid and dd.usrid = '${token.uid}')) `
        // }
        if (sort) {
            Object.keys(sort).forEach(key => {
                order = `order by ${key} ${sort[key]}`
            })
        }
        else order = "order by idt desc,ABS(id) desc, type, form, serial, seq"
        return { where: where, binds: binds, ext: ext, order: order, extxls: extxls, paramindex: i }
    },
    xls: async (req, res, next) => {
        try {
            const query = req.query, filter = JSON.parse(query.filter)
            let json, rows, fn
            if (ENT == "mzh") {
                fn = "temp/KQTCHDMZH.xlsx"
            } else if (ENT == "ctbc") {
                fn = "temp/KQTCHD_CTBC.xlsx"
            } else {
                fn = "temp/KQTCHD.xlsx"
            }
            const iwh = await Service.iwh(req, true)
            let cols = await COL.cacheLang(filter["type"], req.query.lang)
            let colhds = req.query.colhd.split(",")
            let extsh = []
            for (const col of cols) {
                let clbl = col.label
                extsh.push(clbl)
            }
            let sql = ''
            if (ENT == "mzh") {
                sql = `select id,sec,ic,uc,FORMAT(idt,'dd/MM/yyyy') idt,case when status=1 then N'Chờ cấp số' when status=2 then N'Chờ duyệt' when status=3 then N'Đã duyệt' when status=4 then N'Đã hủy' when status=5 then N'Đã gửi' when status=6 then N'Chờ hủy' end status,ou,type,form,serial,seq,btax,bname,buyer,baddr,note,sumv,vatv,totalv,sum,vat,total,adjdes,cde,bmail,curr,JSON_VALUE(JSON_QUERY (doc, '$.tax[0]'),'$.vrn') vrn,case when dds_sent=0 then N'Chưa gửi' when dds_sent=1 then N'Đã gửi' else  N'' end dds_sent,case when dds_stt=0 then N'Sent Successfully' when dds_stt=1 then N'Send Failed' when dds_stt=2 then N'Send To Custome' when dds_stt=3 then N'New' else  N'' end dds_stt,file_name filen,exrt exrt,sname sname${iwh.extxls} from s_inv ${iwh.where} ${iwh.order} `
            } else if (ENT == "ctbc") {
                sql = `select id,sec,ic,uc,FORMAT(idt,'dd/MM/yyyy') idt,case when status=1 then N'Chờ cấp số' when status=2 then N'Chờ duyệt' when status=3 then N'Đã duyệt' when status=4 then N'Đã hủy' when status=5 then N'Đã gửi' when status=6 then N'Chờ hủy' end status,ou,type,form,serial,seq,btax,bname,buyer,baddr,note,sumv,vatv,totalv,sum,vat,total,adjdes,cde,bmail,curr,sname sname${iwh.extxls}, bcode, bacc from s_inv ${iwh.where} ${iwh.order} `
            } else {
                sql = `select id,sec,ic,uc,FORMAT(idt,'dd/MM/yyyy') idt,case when status=1 then N'Chờ cấp số' when status=2 then N'Chờ duyệt' when status=3 then N'Đã duyệt' when status=4 then N'Đã hủy' when status=5 then N'Đã gửi' when status=6 then N'Chờ hủy' end status,ou,type,form,serial,seq,btax,bname,buyer,baddr,note,sumv,vatv,totalv,sum,vat,total,adjdes,cde,bmail,curr,sname sname${iwh.extxls} from s_inv ${iwh.where} ${iwh.order} `
            }
            const result = await dbs.query(sql, iwh.binds)
            rows = result.recordset
            //Them ham tra gia tri theo mang de hien thi tương ứng voi tung cot
            for (const row of rows) {
                row.sumv = parseInt(row.sumv)
                //row.vatv = parseInt(row.vatv)
                row.totalv = parseInt(row.totalv)
                if (row.c0 == "01/01/1900") {
                    row.c0 = ""
                }
                if (row.c1 == "01/01/1900") {
                    row.c1 = ""
                }
                try {
                    row.ou = await oubi(row.ou)
                }
                catch (err) {

                }
                let extsval = []
                for (const col of cols) {
                    extsval.push(row[col.id])
                }
                row.extsval = extsval
            }
            json = { table: rows, extsh: extsh, colhds: colhds }
            const file = path.join(__dirname, "..", "..", "..", fn)
            const xlsx = fs.readFileSync(file)
            const template = new xlsxtemp(xlsx)
            template.substitute(1, json)
            res.end(template.generate(), "binary")
        }
        catch (err) {
            next(err)
        }
    },
    appget: async (req, res, next) => {
        try {
            const query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 10, iwh = await Service.iwh(req, false)
            let sql, result, ret, i = iwh.paramindex
            sql = `select 0 chk,id,sec,ic,uc,curr,pid,cid,idt,status,ou,type,form,serial,seq,btax,bname,buyer,baddr,note,sumv,vatv,totalv,sum,vat,total,adjdes,adjtyp,cde,cvt,bmail${iwh.ext}
                     from s_inv ${iwh.where} ${iwh.order} OFFSET @${i++} ROWS FETCH NEXT @${i++} ROWS ONLY`
            iwh.binds.push(parseInt(start), parseInt(count))
            result = await dbs.query(sql, iwh.binds)
            ret = { data: result.recordset, pos: start }
            if (start == 0) {
                sql = `select count(*) total from s_inv ${iwh.where}`
                result = await dbs.query(sql, iwh.binds)
                ret.total_count = result.recordset[0].total
            }
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    get: async (req, res, next) => {
        try {
            //console.time("timeget")
            const query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 10, iwh = await Service.iwh(req, true)
            let sql, result, ret, i = iwh.paramindex
            if (ENT == 'mzh') {
                sql = `select 0 chk,id,sec,ic,uc,curr,pid,cid,cast(idt as date) idt,status,ou,type,form,serial,seq,btax,bname,buyer,baddr,note,sum sumv,vat vatv,total totalv,sum,vat,total,adjdes,adjtyp,cde,cvt,exrt,JSON_VALUE(JSON_QUERY (doc, '$.tax[0]'),'$.vrn') vrn,dds_sent,dds_stt,file_name filen,file_dds,bmail${iwh.ext} from s_inv ${iwh.where} ${iwh.order} OFFSET  @${i++} ROWS FETCH NEXT @${i++} ROWS ONLY`

            } else {
                switch (ENT) {
                    case 'vcm': sql = `select 0 chk,id,sec,ic,uc,curr,pid,cid,cast(idt as date) idt,status,ou,type,form,serial,seq,btax,bname,buyer,baddr,note,sumv,vatv,totalv,sum,vat,total,adjdes,adjtyp,cde,cvt,exrt,bmail${iwh.ext}  from s_inv WITH(INDEX(s_inv_idx_final)) ${iwh.where} ${iwh.order} OFFSET @${i++} ROWS FETCH NEXT @${i++} ROWS ONLY`
                        break
                    case 'ctbc': sql = `select 0 chk,id,sec,ic,uc,curr,pid,cid,cast(idt as date) idt,status,ou,type,form,serial,seq,btax,bname,buyer,baddr,btel,note,sumv,vatv,totalv,sum,vat,total,adjdes,adjtyp,cde,cvt,exrt,bmail${iwh.ext}${['vib', 'aia'].includes(ENT) ? `,sendmailnum sendmailnum, sendmailstatus sendmailstatus, cast(sendmaildate as date) sendmaildate, sendsmsnum sendsmsnum, sendsmsstatus sendsmsstatus, cast(sendsmsdate as date) sendsmsdate` : ``}, bacc, bcode from s_inv ${iwh.where} ${iwh.order} OFFSET @${i++} ROWS FETCH NEXT @${i++} ROWS ONLY`
                        break
                    // case 'sgr': sql = `select 0 chk,id,sec,ic,uc,curr,pid,cid,cast(idt as date) idt,status,ou,type,form,serial,seq,btax,bname,buyer,baddr,btel,note,sumv,vatv,totalv,sum,vat,total,adjdes,adjtyp,cde,cvt,exrt,bmail,sendmailnum sendmailnum, sendmailstatus sendmailstatus,cast(sendmaildate as datetime) sendmaildatedetail, cast(sendmaildate as date) sendmaildate, sendsmsnum sendsmsnum, sendsmsstatus sendsmsstatus, cast(sendsmsdate as date) sendsmsdate from s_inv ${iwh.where} ${iwh.order} OFFSET @${i++} ROWS FETCH NEXT @${i++} ROWS ONLY`
                    //     break  
                    case 'sgr': sql = `select 0 chk,id,sec,ic,uc,curr,pid,cid,cast(idt as date) idt,status,ou,type,form,serial,seq,btax,bname,buyer,baddr,btel,note,sumv,vatv,totalv,sum,vat,total,adjdes,adjtyp,cde,cvt,exrt,bmail,sendmailnum sendmailnum, sendmailstatus sendmailstatus,sendmaildate sendmaildate, sendsmsnum sendsmsnum, sendsmsstatus sendsmsstatus, sendsmsdate sendsmsdate${iwh.ext} from s_inv ${iwh.where} ${iwh.order} OFFSET @${i++} ROWS FETCH NEXT @${i++} ROWS ONLY`
                        break
                    default: sql = `select 0 chk,id,sec,ic,uc,curr,pid,cid,cast(idt as date) idt,status,ou,type,form,serial,seq,btax,bname,buyer,baddr,btel,note,sumv,vatv,totalv,sum,vat,total,adjdes,adjtyp,cde,cvt,exrt,bmail${iwh.ext}${['vib', 'aia'].includes(ENT) ? `,sendmailnum sendmailnum, sendmailstatus sendmailstatus, cast(sendmaildate as date) sendmaildate, sendsmsnum sendsmsnum, sendsmsstatus sendsmsstatus, cast(sendsmsdate as date) sendsmsdate` : ``} from s_inv ${iwh.where} ${iwh.order} OFFSET @${i++} ROWS FETCH NEXT @${i++} ROWS ONLY`
                }
            }
            iwh.binds.push(parseInt(start), parseInt(count))
            result = await dbs.query(sql, iwh.binds)
            ret = { data: result.recordset, pos: start }
            if (start == 0) {
                sql = `select count(*) total from s_inv ${iwh.where}`
                result = await dbs.query(sql, iwh.binds)
                ret.total_count = result.recordset[0].total
            }
            //console.timeEnd("timeget")
            const sSysLogs = { fnc_id: 'inv_get', src: config.SRC_LOGGING_DEFAULT, dtl: `Tra cứu hóa đơn ${query.filter}`, msg_id: "", doc: query.filter };
            logging.ins(req, sSysLogs, next)
            res.json(ret)
        }
        catch (err) {
            logger4app.debug(err)
            next(err)
        }
    },
    signget: async (req, res, next) => {
        try {
            const token = SEC.decode(req), u = token.u
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const id = req.query.id, taxc = token.taxc
            let sql, result, binds = [id, taxc]
            if (ENT == 'vcm') {//ko co cho cap so
                sql = `select id,doc,bmail,pid from s_inv where id=@1 and stax=@2 and status=1 `
            } else {
                sql = `select id,doc,bmail,pid from s_inv where id=@1 and stax=@2 and status=2 `
            }

            if (grant) {
                sql += ' and ou=@3'
                binds.push(u)
            }
            result = await dbs.query(sql, binds)
            const rows = result.recordset
            if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
            const org = await ous.obt(taxc), signtype = org.sign, row = rows[0]
            let doc = JSON.parse(row.doc)
            let xml, ca
            if (signtype == 1) {
                const usr = org.usr, pwd = util.decrypt(org.pwd)
                xml = util.j2x(doc, id)
                const arr = [{ inc: id, xml: Buffer.from(xml).toString("base64") }]
                res.json({ lib: usr, sid: 0, pin: pwd, mst: taxc, date: new Date(), ref: config.PATH_XML, arr: arr })
            }
            else {
                try {
                    if (signtype == 2) {
                        const pwd = util.decrypt(org.pwd)
                        ca = await sign.ca(taxc, pwd)
                        xml = util.signature(sign.sign(ca, util.j2x((await Service.getcol(doc)), id), id), doc.idt)
                    }
                    else if (signtype == 3) {


                        const usr = org.usr, pwd = util.decrypt(org.pwd)
                        const kqs = await fcloud.xml(usr, pwd, taxc, rows), kq = kqs[0]
                        xml = kq.xml


                    }
                    else {
                        const kqs = await hsm.xml(rows), kq = kqs[0]
                        xml = util.signature(kq.xml, doc.idt)
                    }
                } catch (err) {
                    //logger4app.debug("signtype :"+ signtype +"_" + err.message)
                    throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)')
                }

                if (ENT == 'vcm') {//ko co cho cap so
                    let seq = await SERIAL.sequence(taxc, doc.form, doc.serial, doc.idt)
                    let s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    doc.seq = s7
                    const xml = sign.sign(ca, util.j2x(doc, id), id)
                    doc.status = 3
                    await dbs.query(`update s_inv set doc=@1,xml=@2,dt=@3 where id=@4 and stax=@5 and status=1`, [JSON.stringify(doc), xml, moment(new Date()).format(dtf), id, taxc])
                    if (doc.adj && doc.adj.typ == 1) {
                        let cdt = moment(new Date()).format(dtf)
                        if (!util.isEmpty(row.pid)) {
                            try {
                                // let cde = `${(row.adjtyp == 1 ? "Bị thay thế" : "Bị điều chỉnh")} bởi HĐ số ${s7} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)} mã ${doc.sec}`
                                // let pdoc = await rbi(row.pid)

                                let cde = ''
                                result = await dbs.query("select doc,cid,cde from s_inv where id=@1", [row.pid])
                                const rows2 = result.recordset
                                let cid, pdoc = JSON.parse(rows2[0].doc)
                                cde = `Bị thay thế bởi HĐ số ${doc.seq} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)}`

                                cid = doc.id


                                pdoc.cdetemp = null
                                pdoc.cdt = cdt
                                pdoc.canceller = token.uid
                                pdoc.status = 4
                                pdoc.cde = cde
                                await dbs.query(`update s_inv set cde=@1,doc=@2,cid=@3 where id=@4`, [cde, JSON.stringify(pdoc), cid, row.pid])
                            } catch (err) { logger4app.error(err) }
                        }

                    }

                } else {
                    await dbs.query(`update s_inv set doc=JSON_MODIFY(doc,'$.status',3),xml=@1,dt=@2 where id=@3 and stax=@4 and status=2`, [xml, moment(new Date()).format(dtf), id, taxc])
                }
                if (ENT == "sgr" && doc.adj && doc.adj.typ == 1) {
                    if (!util.isEmpty(row.pid)) {
                        const cde = `Bị thay thế bởi HĐ số ${doc.seq} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)} mã ${doc.sec}`
                        let cdt = moment(new Date()).format(dtf)
                        await dbs.query(`update s_inv set doc=JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(doc,'$.cdt',@1),'$.status',4),'$.cde',@2),'$.canceller',@3),cid=@4,cde=@5 where id=@6`, [cdt, cde, token.uid, id, cde, row.pid])

                    }


                }
                doc.status = 3
                if (useJobSendmail) {
                    await Service.insertDocTempJob(doc.id, doc, xml)
                } else {
                    if (!util.isEmpty(doc.bmail)) {
                        let mailstatus = await email.rsmqmail(doc)
                        if (mailstatus) await Service.updateSendMailStatus(doc)
                    }
                    if (!util.isEmpty(doc.btel)) {
                        let smsstatus = await sms.smssend(doc)
                        if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                    }
                }
                const sSysLogs = { fnc_id: 'inv_signget', src: config.SRC_LOGGING_DEFAULT, dtl: `Ký số hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                logging.ins(req, sSysLogs, next)
                res.json(1)
            }

        }
        catch (err) {
            next(err)
        }
    },

    signput: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, body = req.body, signs = body.signs
            let sign = signs[0], id = sign.inc, xml = Buffer.from(sign.xml, "base64").toString()
            let result
            if (ENT == 'vcm') {//ko co cho cap so
                result = await dbs.query(`select doc,bmail,pid from s_inv where id=@1 and stax=@2 and status=1`, [id, taxc])
            } else {
                result = await dbs.query(`select doc,bmail,pid from s_inv where id=@1 and stax=@2 and status=2`, [id, taxc])
            }
            const rows = result.recordset
            if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
            const row = rows[0]
            let doc = JSON.parse(row.doc)
            xml = util.signature(xml, doc.idt)
            if (ENT == 'vcm') {//ko co cho cap so
                await dbs.query(`update s_inv set doc=JSON_MODIFY(doc,'$.status',3),xml=@1,dt=@2 where id=@3 and stax=@4 and status=1`, [xml, moment(new Date()).format(dtf), id, taxc])

                if (doc.adj && doc.adj.typ == 1) {
                    let cdt = moment(new Date()).format(dtf)
                    if (!util.isEmpty(row.pid)) {
                        try {
                            // let cde = `${(row.adjtyp == 1 ? "Bị thay thế" : "Bị điều chỉnh")} bởi HĐ số ${s7} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)} mã ${doc.sec}`
                            // let pdoc = await rbi(row.pid)

                            let cde = ''
                            result = await dbs.query("select doc,cid,cde from s_inv where id=@1", [row.pid])
                            const rows2 = result.recordset
                            let cid, pdoc = JSON.parse(rows2[0].doc)
                            cde = `Bị thay thế bởi HĐ số ${doc.seq} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)}`

                            cid = doc.id


                            pdoc.cdetemp = null
                            pdoc.cdt = cdt
                            pdoc.canceller = token.uid
                            pdoc.status = 4
                            pdoc.cde = cde
                            await dbs.query(`update s_inv set cde=@1,doc=@2,cid=@3 where id=@4`, [cde, JSON.stringify(pdoc), cid, row.pid])
                        } catch (err) { logger4app.error(err) }
                    }

                }
            } else {
                await dbs.query(`update s_inv set doc=JSON_MODIFY(doc,'$.status',3),xml=@1,dt=@2 where id=@3 and stax=@4 and status=2`, [xml, moment(new Date()).format(dtf), id, taxc])
            }
            if (ENT == "sgr") {
                let subquery = `select pid from s_inv where id=@1 and pid is not null`
                const subResult = await dbs.query(subquery, [id])
                let subRows = subResult.recordset
                const cde = `Bị thay thế bởi HĐ số ${doc.seq} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)} mã ${doc.sec}`
                let cdt = moment(new Date()).format(dtf)
                await dbs.query(`update s_inv set doc=JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(doc,'$.cdt',@1),'$.status',4),'$.cde',@2),'$.canceller',@3),cid=@4,cde=@5 where id=@6`, [cdt, cde, token.uid, id, cde, subRows[0].pid])
            }

            doc.status = 3
            if (useJobSendmail) {
                await Service.insertDocTempJob(doc.id, doc, xml)
            } else {
                if (!util.isEmpty(doc.bmail)) {
                    let mailstatus = await email.rsmqmail(doc)
                    if (mailstatus) await Service.updateSendMailStatus(doc)
                }
                if (!util.isEmpty(doc.btel)) {
                    let smsstatus = await sms.smssend(doc)
                    if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                }
            }
            const sSysLogs = { fnc_id: 'inv_signget', src: config.SRC_LOGGING_DEFAULT, dtl: `Ký số hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
            logging.ins(req, sSysLogs, next)
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },
    put: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, id = req.params.id
            const dt = moment(body.idt), eod = dt.endOf('day').toDate(), dtsc = dt.format('YYYYMMDD'), idtc = (moment(new Date())).format('YYYYMMDD'), taxc = token.taxc
            if (dtsc != idtc) {
                let result = await dbs.query(`select count(*) rc from s_inv where idt>@1 and form=@2 and serial=@3 and stax=@4 and (status in (2,3,5,6)${(!["sgr"].includes(config.ent)) ? ' or (status = 4 and xml is not null)' : ''})`, [eod, body.form, body.serial, taxc])
                let row = result.recordset[0]
                if (row.rc > 0) throw new Error(`Tồn tại HĐ có ngày lớn hơn ${moment(body.idt).format('DD/MM/YYYY')} và đã cấp số \n (Invoice has a date larger than  ${moment(body.idt).format('DD/MM/YYYY')} and issued a number)`)
            }
            await chkou(id, token)
            await Service.delrefno(body)
            const hdb = await Service.checkrefno(body)
            if (hdb.length > 0) throw new Error(`Số bút toán đã tồn tại: ${hdb.join()} \n(Entry number already exists: ${hdb.join()})`)
            const checkrefdate = await Service.checkrefdate(body)
            if (checkrefdate.length > 0) throw new Error(`Ngày bút toán lớn hơn ngày hóa đơn: ${checkrefdate.join(',')} \n(Ref Date is greater than Invoice Date: ${checkrefdate.join(',')})`)
            await dbs.query(`update s_inv set doc=@1,idt=@2 where id=@3 and status=@4`, [JSON.stringify(body), moment(body.idt).format("YYYY-MM-DD HH:mm:ss"), id, body.status])
            await Service.saverefno(body)
            const sSysLogs = { fnc_id: 'inv_put', src: config.SRC_LOGGING_DEFAULT, dtl: `Sửa hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(body) };
            logging.ins(req, sSysLogs, next)
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },
    ins: async (req, res, next) => {
        try {
            if (ENT == "sgr") {
                Service.insSGR(req, res, next)
            } else {
                const token = SEC.decode(req)
                if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
                const taxc = token.taxc, uid = token.uid, ou = token.ou, org = await ous.obt(taxc), AUTO = (org.seq === 1), body = req.body
                let invs = body.invs
                const sql = `insert into s_inv(id,pid,sec,ic,idt,ou,uc,doc) values (@1,@2,@3,@4,@5,@6,@7,@8)`
                //Thêm đoạn sort lại danh sách hóa đơn trước khi check số
                //Ghep dữ liệu cần sort
                for (let inv of invs) {
                    inv.sortprop = `#${inv.form}#${inv.serial}#${inv.seq}`
                }
                invs = util.sortobj(invs, 'sortprop', 0)
                //check rieng cho sgr khi hoa don co so
                let check_seq = true
                if (ENT == 'sgr') {
                    let sequence = 0, oldseq, oldform, oldserial
                    let seqc = invs[0].seq, formc = invs[0].form, serialc = invs[0].serial
                    for (let inv of invs) {
                        let seqc = inv.seq
                        if (Number(inv.seq) < Number(seqc)) {
                            seqc = inv.seq
                        }
                        if (sequence > 0) {
                            if ((Number(oldseq) + 1) != Number(inv.seq) && oldform == inv.form && oldserial == inv.serial) {
                                throw new Error(`Số hóa đơn không liên tiếp trên file excel \n The number of invoices is not continuous on Excel file `)
                            }
                        }
                        oldseq = inv.seq
                        oldform = inv.form
                        oldserial = inv.serial
                        sequence++
                    }
                    const uk = `${taxc}.${invs[0].form}.${invs[0].serial}`, key = `SERIAL.${uk}`
                    const curc = await redis.get(key)
                    if (Number(seqc) != Number(curc) + 1) throw new Error(`Số hóa đơn không liên tiếp với số trên hệ thống. Số hiện tại (${curc}) `)
                }
                if (AUTO) {
                    //Ghep dữ liệu cần sort
                    for (let inv of invs) {
                        inv.sortprop = `#${(moment(inv.idt, dtf).format('YYYYMMDD'))}#${inv.ic}`
                    }
                    invs = util.sortobj(invs, 'sortprop', 0)
                }
                for (let inv of invs) {
                    let ic, id, sec, form, serial, type, pid, idt, seq, s7, rs
                    try {
                        form = inv.form
                        serial = inv.serial
                        type = form.substr(0, 6)
                        ic = inv.ic
                        inv.sid = ic
                        pid = inv.pid
                        idt = moment(inv.idt, dtf)
                        id = await redis.incr("INC")
                        sec = util.generateSecCode(config.FNC_GEN_SEC, id)
                        inv.id = id
                        inv.sec = sec
                        inv.type = type
                        inv.name = util.tenhd(type)
                        inv.stax = taxc
                        inv.sname = org.name
                        inv.saddr = org.addr
                        inv.smail = org.mail
                        inv.stel = org.tel
                        inv.taxo = org.taxo
                        inv.sacc = org.acc
                        inv.sbank = org.bank
                        const ouob = await ous.obid(ou)
                        inv.systemCode = ouob.code

                        if (ENT == 'vcm') {
                            inv.c6 = sec
                        }
                        if (AUTO) {
                            const sqlcheck = `select count(*) rcount from s_inv where stax=@1 and (status in (2,3,5,6)${(!["sgr"].includes(config.ent)) ? ' or (status = 4 and xml is not null)' : ''}) and form=@2 and serial=@3 and idt>@4`
                            const resultcheck = await dbs.query(sqlcheck, [taxc, form, serial, (moment(idt.toDate()).endOf("day")).toDate()])
                            const rowscheck = resultcheck.recordset
                            if (rowscheck[0].rcount > 0) throw new Error("Đã tồn tại hóa đơn được cấp số có ngày hóa đơn lớn hơn hóa đơn hiện tại \n (A numbered invoice already exists that has a larger invoice date than the current invoice)")
                            seq = await SERIAL.sequence(taxc, form, serial, inv.idt)
                            s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                            inv.seq = s7
                            inv.status = 2
                        }
                        else {
                            s7 = "......."
                            inv.status = 1
                        }
                        if (ENT == 'sgr') {
                            inv.status = 2
                            s7 = inv.seq
                            if (inv.adjust) {
                                delete inv["sic"]
                                delete inv["adjust"]
                                delete inv["oseq"]
                                delete inv["des"]
                            }
                        }
                        rs = await dbs.query(sql, [id, pid, sec, ic, moment(idt.toDate()).format(dtf), ou, uid, JSON.stringify(inv)])
                        //Đồng bộ lại serial nếu là SGR
                        if (ENT == 'sgr') {
                            await SERIAL.syncredisdbsgr(taxc, form, serial)
                        }
                        if (rs.rowsAffected[0] > 0) {
                            if (pid) {
                                try {
                                    let cde
                                    if (inv.adj.typ == 2) {
                                        cde = `Bị điều chỉnh bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${idt.format(mfd)} mã ${ENT != "vcm" ? inv.sec : inv.c6}`
                                        let cdt = moment(idt.toDate()).format(dtf)
                                        await dbs.query(`update s_inv set doc=JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(doc,'$.cdt',@1),'$.status',3),'$.cde',@2),'$.canceller',@3),cid=@4,cde=@5 where id=@6 and status=3`, [cdt, cde, token.uid, id, cde, pid])
                                    } else if (inv.adj.typ == 1) {
                                        cde = `Bị thay thế bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${idt.format(mfd)} mã ${ENT != "vcm" ? inv.sec : inv.c6}`
                                        let cdt = moment(idt.toDate()).format(dtf)
                                        await dbs.query(`update s_inv set doc=JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(doc,'$.cdt',@1),'$.status',4),'$.cde',@2),'$.canceller',@3),cid=@4,cde=@5 where id=@6 and status=3`, [cdt, cde, token.uid, id, cde, pid])
                                    }

                                } catch (error) {
                                    logger4app.error(error)
                                }
                            }
                        }
                        else throw new Error(`Không cấp số cho hóa đơn ${id} \n (Don't provide numbers for invoice ${id})`)
                        const sSysLogs = { fnc_id: 'xls_upl', src: config.SRC_LOGGING_DEFAULT, dtl: 'Đọc hóa đơn từ excel', msg_id: id, doc: JSON.stringify(inv) };
                        logging.ins(req, sSysLogs, next)
                    } catch (e) {
                        if (AUTO) {
                            if (!((e.number == 2627) && (String(e.message).toUpperCase().indexOf("S_INV_UID_01") >= 0))) await SERIAL.err(taxc, form, serial, seq)
                        }
                        throw e
                    }
                }
                res.json(invs.length)
            }
        }
        catch (err) {
            next(err)
        }
    },
    insSGR: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const taxc = token.taxc, uid = token.uid, ou = token.ou, org = await ous.obt(taxc), AUTO = (org.seq === 1), body = req.body
            let invs = body.invs
            //Thêm đoạn sort lại danh sách hóa đơn trước khi check số
            //Ghep dữ liệu cần sort
            for (let inv of invs) {
                inv.sortprop = `#${inv.form}#${inv.serial}#${inv.seq}`
                inv.sortidt = `${(moment(inv.idt, dtf).format('YYYYMMDD'))}`
            }
            invs = util.sortobj(invs, 'sortprop', 0)
            //check rieng cho sgr khi hoa don co so
            let check_seq = true, binds = [], rs
            let sequence = 0, oldseq, oldform, oldserial, oldidt
            let seqc = invs[0].seq, formc = invs[0].form, serialc = invs[0].serial
            for (let inv of invs) {
                let seqc = inv.seq, idtc = inv.sortidt
                if (Number(inv.seq) < Number(seqc)) {
                    seqc = inv.seq
                }
                if (sequence > 0) {
                    if ((Number(oldseq) + 1) != Number(inv.seq) && oldform == inv.form && oldserial == inv.serial) {
                        throw new Error(`Số hóa đơn không liên tiếp trên file excel \n The number of invoices is not continuous on Excel file `)
                    }
                    if (oldidt > idtc && oldseq < seqc) {
                        throw new Error(`Ngày hóa đơn không liên tiếp, theo thứ tự số hóa đơn \n (The invoice date is not continuous, in order of the number of invoices)`)
                    }
                }
                oldidt = inv.sortidt
                oldseq = inv.seq
                oldform = inv.form
                oldserial = inv.serial
                sequence++
            }
            const uk = `${taxc}.${invs[0].form}.${invs[0].serial}`, key = `SERIAL.${uk}`
            const curc = await redis.get(key)
            if (Number(seqc) != Number(curc) + 1) throw new Error(`Số hóa đơn không liên tiếp với số trên hệ thống. Số hiện tại (${curc}) `)
            if (AUTO) {
                //Ghep dữ liệu cần sort
                for (let inv of invs) {
                    inv.sortprop = `#${(moment(inv.idt, dtf).format('YYYYMMDD'))}#${inv.ic}`
                }
                invs = util.sortobj(invs, 'sortprop', 0)
            }
            let arrsql = [], arrserial = []
            for (let inv of invs) {
                let ic, id, sec, form, serial, type, pid, idt, seq, s7
                try {
                    form = inv.form
                    serial = inv.serial
                    type = form.substr(0, 6)
                    ic = inv.ic
                    pid = inv.pid
                    idt = moment(inv.idt, dtf)
                    id = await redis.incr("INC")
                    sec = util.generateSecCode(config.FNC_GEN_SEC, id)
                    inv.id = id
                    inv.sec = sec
                    inv.type = type
                    inv.name = util.tenhd(type)
                    inv.stax = taxc
                    inv.sname = org.name
                    inv.saddr = org.addr
                    inv.smail = org.mail
                    inv.stel = org.tel
                    inv.taxo = org.taxo
                    inv.sacc = org.acc
                    inv.sbank = org.bank
                    inv.ou = ou
                    const ouob = await ous.obid(ou)
                    inv.systemCode = ouob.code

                    if (ENT == 'vcm') {
                        inv.c6 = sec
                    }
                    if (AUTO) {
                        const sqlcheck = `select count(*) rcount from s_inv where stax=@1 and (status in (2,3,5,6)${(!["sgr"].includes(config.ent)) ? ' or (status = 4 and xml is not null)' : ''}) and form=@2 and serial=@3 and idt>@4`
                        const resultcheck = await dbs.query(sqlcheck, [taxc, form, serial, (moment(idt.toDate()).endOf("day")).toDate()])
                        const rowscheck = resultcheck.recordset
                        if (rowscheck[0].rcount > 0) throw new Error("Đã tồn tại hóa đơn được cấp số có ngày hóa đơn lớn hơn hóa đơn hiện tại \n (A numbered invoice already exists that has a larger invoice date than the current invoice)")
                        seq = await SERIAL.sequence(taxc, form, serial, inv.idt)
                        s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                        inv.seq = s7
                        inv.status = 2
                    }
                    else {
                        s7 = "......."
                        inv.status = 1
                    }
                    if (ENT == 'sgr') {
                        inv.status = 2
                        s7 = inv.seq
                        if (inv.adjust) {
                            delete inv["sic"]
                            delete inv["adjust"]
                            delete inv["oseq"]
                            delete inv["des"]
                        }
                    }
                    binds.push([id, pid, sec, ic, moment(idt.toDate()).format(dtf), ou, uid, JSON.stringify(inv)])
                    arrserial.push({ taxc, form, serial })

                    if (pid) {
                        try {
                            let cde
                            if (inv.adj.typ == 2) {
                                cde = `Bị điều chỉnh bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${idt.format(mfd)} mã ${ENT != "vcm" ? inv.sec : inv.c6}`
                                let cdt = moment(idt.toDate()).format(dtf)
                                //await dbs.query(`update s_inv set doc=JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(doc,'$.cdt',@1),'$.status',3),'$.cde',@2),'$.canceller',@3),cid=@4,cde=@5 where id=@6 and status=3`, [cdt, cde, token.uid, id, cde, pid])
                                arrsql.push({ sql: `update s_inv set doc=JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(doc,'$.cdt',@1),'$.status',3),'$.cde',@2),'$.canceller',@3),cid=@4,cde=@5 where id=@6 and status=3`, bind: [cdt, cde, token.uid, id, cde, pid], slog: { fnc_id: 'xls_upl', src: config.SRC_LOGGING_DEFAULT, dtl: 'Đọc hóa đơn từ excel', msg_id: id, doc: JSON.stringify(inv) } })
                            } else if (inv.adj.typ == 1) {
                                cde = `Bị thay thế bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${idt.format(mfd)} mã ${ENT != "vcm" ? inv.sec : inv.c6}`
                                let cdt = moment(idt.toDate()).format(dtf)
                                arrsql.push({ sql: `update s_inv set doc=JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(doc,'$.cdt',@1),'$.status',4),'$.cde',@2),'$.canceller',@3),cid=@4,cde=@5 where id=@6 and status=3`, bind: [cdt, cde, token.uid, id, cde, pid], slog: { fnc_id: 'xls_upl', src: config.SRC_LOGGING_DEFAULT, dtl: 'Đọc hóa đơn từ excel', msg_id: id, doc: JSON.stringify(inv) } })
                                //await dbs.query(`update s_inv set doc=JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(doc,'$.cdt',@1),'$.status',4),'$.cde',@2),'$.canceller',@3),cid=@4,cde=@5 where id=@6 and status=3`, [cdt, cde, token.uid, id, cde, pid])
                            }

                        } catch (error) {
                            logger4app.error(error)
                        }
                    }

                } catch (e) {
                    if (AUTO) {
                        if (!((e.number == 2627) && (String(e.message).toUpperCase().indexOf("S_INV_UID_01") >= 0))) await SERIAL.err(taxc, form, serial, seq)
                    }
                    throw e
                }
            }
            rs = await dbs.queriestran(binds)
            //Đồng bộ lại serial nếu là SGR
            for (let ser of arrserial) {
                await SERIAL.syncredisdbsgr(ser.taxc, ser.form, ser.serial)
            }
            for (let s of arrsql) {
                await dbs.query(s.sql, s.bind)
                const sSysLogs = s.slog
                logging.ins(req, sSysLogs, next)
            }
            res.json(rs.rowsAffected[0])
        }
        catch (err) {
            next(err)
        }
    },
    upds: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const body = req.body, invs = body.invs
            for (let inv of invs) {
                try {
                    const pid = inv.pid, doc = await rbi(pid), idt = doc.idt
                    delete inv["pid"]
                    for (let i in inv) {
                        doc[i] = inv[i]
                    }
                    doc.idt = idt
                    await dbs.query(`update s_inv set doc=@1 where id=@2 and status=2`, [JSON.stringify(doc), pid])
                    const sSysLogs = { fnc_id: 'inv_adj', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật hóa đơn id ${pid}`, msg_id: pid, doc: JSON.stringify(doc) };
                    logging.ins(req, sSysLogs, next)
                } catch (e) {
                    throw e
                }
            }
            res.json(invs.length)
        }
        catch (err) {
            next(err)
        }
    },
    wcan: async (req, res, next) => {
        const token = SEC.decode(req), taxc = token.taxc, body = req.body, invs = body.invs
        const sql = `update s_inv set doc=JSON_MODIFY(JSON_MODIFY(doc,'$.status',6),'$.cancel',JSON_QUERY(@1)) where id=@2 and stax=@3 and status in (2,3)`
        for (const item of invs) {
            try {
                const cancel = { typ: 3, ref: item.ref, rdt: item.rdt, rea: item.rea }
                const result = await dbs.query(sql, [JSON.stringify(cancel), item.id, taxc])
                if (result.rowsAffected[0] > 0) {
                    if (item.pid) await dbs.query(`update s_inv set cid=null,cde=null where id=@1`, [item.pid])
                    item.error = 'OK'
                }
                else item.error = 'NOT OK'
            }
            catch (e) {
                item.error = e.message
            }
        }
        res.json({ save: true, data: invs })
    },
    sav: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body
            if (grant && token.ou !== token.u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)
            const hdb = await Service.checkrefno(body)
            if (hdb.length > 0) throw new Error(`Số bút toán đã tồn tại: ${hdb.join()} \n(Entry number already exists: ${hdb.join()})`)
            const checkrefdate = await Service.checkrefdate(body)
            if (checkrefdate.length > 0) throw new Error(`Ngày bút toán lớn hơn ngày hóa đơn: ${checkrefdate.join(',')} \n(Ref Date is greater than Invoice Date: ${checkrefdate.join(',')})`)

            const taxc = token.taxc, uid = token.uid, ou = token.ou, org = await ous.obt(taxc), form = body.form, serial = body.serial, id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC, id)
            let seq, s7, result, row
            const dt = moment(body.idt), eod = dt.endOf('day').toDate(), dts = dt.format(mfd), dtsc = dt.format('YYYYMMDD'), idtc = moment(new Date()).format('YYYYMMDD')
            try {
                if (dtsc != idtc) {
                    result = await dbs.query(`select count(*) rc from s_inv where idt>@1 and form=@2 and serial=@3 and (status in (2,3,5,6)${(!["sgr"].includes(config.ent)) ? ' or (status = 4 and xml is not null)' : ''})`, [eod, form, serial])
                    row = result.recordset[0]
                    if (row.rc > 0) throw new Error(`Tồn tại HĐ có ngày lớn hơn ${moment(body.idt).format('DD/MM/YYYY')} và đã cấp số \n (Invoice has a date larger than ${moment(body.idt).format('DD/MM/YYYY')} and issued a number)`)
                }

                body.id = id
                body.sec = sec
                body.stax = org.taxc
                body.sname = org.name
                body.saddr = org.addr
                body.smail = org.mail
                body.stel = org.tel
                body.taxo = org.taxo
                body.sacc = org.acc
                body.sbank = org.bank
                const ouob = await ous.obid(ou)
                body.systemCode = ouob.code
                if (ENT != 'vcm') {
                    seq = await SERIAL.sequence(taxc, form, serial, body.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    body.seq = s7
                    if (ENT == 'onprem') {
                        body.status = 3
                    } else body.status = 2

                } else {
                    body.status = 1
                    body.c6 = sec

                }
                await dbs.query(`insert into s_inv(id,sec,idt,ou,uc,doc) values (@1,@2,@3,@4,@5,@6)`, [id, sec, moment(body.idt).format("YYYY-MM-DD HH:mm:ss"), ou, uid, JSON.stringify(body)])
                await Service.saverefno(body)
                const sSysLogs = { fnc_id: 'inv_post', src: config.SRC_LOGGING_DEFAULT, dtl: `Lập hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(body) };
                logging.ins(req, sSysLogs, next)
                res.json(id)
            }
            catch (err) {
                if (!((err.number == 2627) && (String(err.message).toUpperCase().indexOf("S_INV_UID_01") >= 0))) await SERIAL.err(taxc, form, serial, seq)
                throw err
            }
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body
            if (grant && token.ou !== token.u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)
            const hdb = await Service.checkrefno(body)
            if (hdb.length > 0) throw new Error(`Số bút toán đã tồn tại: ${hdb.join()} \n(Entry number already exists: ${hdb.join()})`)
            const checkrefdate = await Service.checkrefdate(body)
            if (checkrefdate.length > 0) throw new Error(`Ngày bút toán lớn hơn ngày hóa đơn: ${checkrefdate.join(',')} \n(Ref Date is greater than Invoice Date: ${checkrefdate.join(',')})`)
            const taxc = token.taxc, uid = token.uid, ou = token.ou, org = await ous.obt(taxc), AUTO = (org.seq === 1), form = body.form, serial = body.serial, id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC, id)
            let seq, s7, result, row
            const dt = moment(body.idt), eod = dt.endOf('day').toDate(), dts = dt.format(mfd), dtsc = dt.format('YYYYMMDD'), idtc = (moment(new Date())).format('YYYYMMDD')
            try {
                if (dtsc != idtc) {
                    result = await dbs.query(`select count(*) rc from s_inv where idt>@1 and form=@2 and serial=@3 and stax=@4 and (status in (2,3,5,6)${(!["sgr"].includes(config.ent)) ? ' or (status = 4 and xml is not null)' : ''})`, [eod, form, serial, taxc])
                    row = result.recordset[0]
                    if (row.rc > 0) throw new Error(`Tồn tại HĐ có ngày lớn hơn ${moment(body.idt).format('DD/MM/YYYY')} và đã cấp số \n (Invoice has a date larger than  ${moment(body.idt).format('DD/MM/YYYY')} and issued a number)`)
                }

                result = await dbs.query(`select count(*) rc from s_serial where fd>@1 and form=@2 and serial=@3 and status=1 and taxc=@4 `, [eod, form, serial, taxc])
                row = result.recordset[0]
                if (row.rc > 0) throw new Error(`Ngày hiệu lực TBPH phải nhỏ hơn hoặc bằng ngày hóa đơn ${moment(body.idt).format('DD/MM/YYYY')}  \n (The serial effective date must be less than or equal to the invoice date ${moment(row.fd).format('DD/MM/YYYY')})`)

                body.id = id
                body.sec = sec
                body.stax = org.taxc
                body.sname = org.name
                body.saddr = org.addr
                body.smail = org.mail
                body.stel = org.tel
                body.taxo = org.taxo
                body.sacc = org.acc
                body.sbank = org.bank

                const ouob = await ous.obid(ou)
                body.systemCode = ouob.code
                if (AUTO) {
                    seq = await SERIAL.sequence(taxc, form, serial, body.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    body.seq = s7
                    body.status = 2
                }
                else body.status = 1
                if (ENT == 'vcm') {
                    body.c6 = sec
                }
                await dbs.query(`insert into s_inv(id,sec,idt,ou,uc,doc) values (@1,@2,@3,@4,@5,@6)`, [id, sec, moment(body.idt).format("YYYY-MM-DD HH:mm:ss"), ou, uid, JSON.stringify(body)])
                await Service.saverefno(body)
                const sSysLogs = { fnc_id: 'inv_post', src: config.SRC_LOGGING_DEFAULT, dtl: `Lập hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(body) };
                logging.ins(req, sSysLogs, next)
                res.json(id)
            }
            catch (err) {
                if (AUTO) {
                    if (!((err.number == 2627) && (String(err.message).toUpperCase().indexOf("S_INV_UID_01") >= 0))) await SERIAL.err(taxc, form, serial, seq)
                }
                throw err
            }
        }
        catch (err) {
            next(err)
        }
    },
    adj: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, pid = body.pid, sumv = body.sumv
            await chkou(pid, token)
            const uid = token.uid, ou = token.ou, taxc = body.stax, form = body.form, serial = body.serial, org = await ous.obt(taxc), AUTO = (org.seq === 1), id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC, id), idt = moment(body.idt).format('YYYY-MM-DD 00:00:00')
            let seq, s7, result, insid
            if (ENT == "vcm") {//check tong tien nhieu hoa don dieu chinh cho 1 hoa don
                result = await dbs.query("select doc,cid,pid,adjtyp from s_inv where id=@1", [pid])
                const rows = result.recordset
                if (!util.isEmpty(rows[0].pid) && body.sumv != 0 && rows[0].adjtyp != 1) throw new Error(`Chỉ được phép điều chỉnh thông tin cho hóa đơn điều chỉnh ${pid} \n (Adjustment is only allowed for adjustment invoice ${pid})`)
                if (body.dif && body.dif.sumv > 0) {

                    if (rows.length == 0) throw (new Error(`Không tìm thấy Hóa đơn gốc ${pid} \n (Cannot find the original invoice ${pid})`))
                    const cid = rows[0].cid, sumg = JSON.parse(rows[0].doc).sumv
                    if (cid) {
                        result = await dbs.query(`select doc from s_inv where id in (${cid})`, [])
                        const rowsi = result.recordset
                        if (rowsi.length == 0) throw (new Error(`Không tìm thấy Hóa đơn điều chỉnh của hóa đơn gốc ${cid} \n (Cannot found the invoice adjustment of the original invoice ${cid})`))
                        let suml = 0, sumi = 0
                        for (let row of rowsi) {
                            let doc = JSON.parse(row.doc)
                            if (doc.dif && doc.dif.sumv > 0) suml += doc.sumv
                            if (doc.dif && doc.dif.sumv < 0) sumi += doc.sumv
                        }
                        if (suml + sumv > sumg + sumi) throw (new Error(`Tổng thành tiền của các hóa đơn điều chỉnh giảm cho cùng một hóa đơn gốc phải <= Thành tiền của các hóa đơn điều chỉnh tăng cho cùng hóa đơn gốc + Thành tiền chưa thuế trên hóa đơn gốc ${pid} \n (The total amount of invoice reduction adjustments for the same original invoice must <= the total amount of invoice increasing adjustments for the same original invoice + the total amount without tax of the original invoice ${pid})`))
                    }
                }
                body.c6 = sec

            }
            try {
                body.id = id
                body.sec = sec
                const ouob = await ous.obid(ou)
                body.systemCode = ouob.code
                if (AUTO) {
                    seq = await SERIAL.sequence(taxc, form, serial, body.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    body.seq = s7
                    body.status = 2
                }
                else {
                    s7 = "......."
                    body.status = 1
                }
                result = await dbs.query(`insert into s_inv (id,pid,sec,idt,ou,uc,doc) OUTPUT inserted.id values (@1,@2,@3,@4,@5,@6,@7)`, [id, pid, sec, idt, ou, uid, JSON.stringify(body)])
                if (result.rowsAffected[0]) insid = result.recordset[0].id
                let cde = ''
                result = await dbs.query("select doc,cid,cde from s_inv where id=@1", [pid])
                const rows = result.recordset
                let cid, pdoc = JSON.parse(rows[0].doc)
                if (ENT == "vcm") {//nhieu điều chỉnh cho 1
                    if (rows[0].cde) cde = rows[0].cde + `_/_`
                    else cde = `Bị điều chỉnh`

                    if (rows[0].cid) cid = rows[0].cid + "," + id
                    else cid = id
                } else {
                    cde = `Bị điều chỉnh bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${moment(idt).format(mfd)} mã ${sec}.`
                    cid = id
                }

                pdoc.cde = cde

                // let cdt = moment(new Date()).format(dtf)
                // pdoc.cdt = cdt
                // pdoc.canceller=token.uid

                await dbs.query(`update s_inv set cid=@1,cde=@2,doc=@3 where id=@4`, [cid, cde, JSON.stringify(pdoc), pid])
                const sSysLogs = { fnc_id: 'inv_adj', src: config.SRC_LOGGING_DEFAULT, dtl: `Điều chỉnh hóa đơn id ${pid}`, msg_id: pid, doc: JSON.stringify(pdoc) };
                logging.ins(req, sSysLogs, next)
                res.json(insid)
            }
            catch (err) {
                if (AUTO) {
                    if (!((err.number == 2627) && (String(err.message).toUpperCase().indexOf("S_INV_UID_01") >= 0))) await SERIAL.err(taxc, form, serial, seq)
                }
                throw err
            }
        }
        catch (err) {
            next(err)
        }
    },
    adjniss: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, pid = body.pid, sumv = body.sumv

            await chkou(pid, token)
            const uid = token.uid, ou = token.ou, u = token.u, taxc = body.stax, form = body.form, serial = body.serial, org = await ous.obt(taxc), AUTO = (org.seq === 1), id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC, id), idt = new Date(body.idt)
            let seq, s7, result, insid, chk = 0
            if (ENT == "vcm") {//check tong tien nhieu hoa don dieu chinh cho 1 hoa don
                result = await dbs.query("select doc,cid,pid,adjtyp from s_inv where id=@1", [pid])
                let rows = result.recordset
                if (!util.isEmpty(rows[0].pid) && body.sumv != 0 && rows[0].adjtyp != 1) throw new Error(`Chỉ được phép điều chỉnh thông tin cho hóa đơn điều chỉnh ${pid} \n (Adjustment is only allowed for adjustment invoice ${pid})`)
                if (body.dif && body.dif.sumv > 0) {

                    if (rows.length == 0) throw (new Error(`Không tìm thấy Hóa đơn gốc ${pid} \n (Cannot find the original invoice ${pid})`))
                    const cid = rows[0].cid, sumg = JSON.parse(rows[0].doc).sumv
                    if (cid) {
                        result = await dbs.query(`select doc from s_inv where id in (${cid})`, [])
                        rows = result.recordset
                        if (rows.length == 0) throw (new Error(`Không tìm thấy Hóa đơn điều chỉnh của hóa đơn gốc ${cid} \n (Cannot found the invoice adjustment of the original invoice ${cid})`))
                        let suml = 0, sumi = 0
                        for (let row of rows) {
                            let doc = JSON.parse(row.doc)
                            if (doc.dif.sumv > 0) suml += doc.sumv
                            if (doc.dif.sumv < 0) sumi += doc.sumv
                        }
                        if (suml + sumv > sumg + sumi) throw (new Error(`Tổng thành tiền của các hóa đơn điều chỉnh giảm cho cùng một hóa đơn gốc phải <= Thành tiền của các hóa đơn điều chỉnh tăng cho cùng hóa đơn gốc + Thành tiền chưa thuế trên hóa đơn gốc ${pid} \n (The total amount of invoice reduction adjustments for the same original invoice must <= the total amount of invoice increasing adjustments for the same original invoice + the total amount without tax of the original invoice ${pid})`))
                    }
                }
                body.c6 = sec

            }
            try {
                body.id = id
                body.sec = sec
                const ouob = await ous.obid(ou)
                body.systemCode = ouob.code


                body.status = 1

                result = await dbs.query(`insert into s_inv (id,pid,sec,idt,ou,uc,doc) OUTPUT inserted.id values (@1,@2,@3,@4,@5,@6,@7)`, [id, pid, sec, idt, ou, uid, JSON.stringify(body)])
                chk = 1
                if (result.rowsAffected[0]) insid = result.recordset[0].id
                //const cde = `Bị điều chỉnh bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${moment(idt).format(mfd)} mã ${sec}.`
                //const pdoc = await rbi(pid)

                let cde = ''
                result = await dbs.query("select doc,cid,cde from s_inv where id=@1", [pid])
                rows = result.recordset
                let cid, pdoc = JSON.parse(rows[0].doc)
                if (ENT == "vcm") {//nhieu điều chỉnh cho 1
                    if (rows[0].cde) cde = rows[0].cde + `_/_`
                    else cde = `Bị điều chỉnh`

                    if (rows[0].cid) cid = rows[0].cid + "," + id
                    else cid = id
                } else {
                    cde = `Bị điều chỉnh bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${moment(idt).format(mfd)} mã ${sec}.`
                    cid = id
                }
                pdoc.cde = cde
                //HungLQ them lan dieu chinh cho VCM, neu on on thi sua chung
                if (config.ent == "vcm") {
                    if (pdoc.adjnum) {
                        pdoc.adjnum = parseInt(pdoc.adjnum) + 1
                    } else {
                        pdoc.adjnum = 1
                    }
                    // pdoc.cde = `Lần điều chỉnh thứ ${pdoc.adjnum}.` + cde
                }
                let cdt = moment(new Date()).format(dtf)
                pdoc.cdt = cdt
                pdoc.canceller = token.uid
                await dbs.query(`update s_inv set cid=@1,cde=@2,doc=@3 where id=@4`, [cid, cde, JSON.stringify(pdoc), pid])

                // ky xml
                let sql, binds = [id, taxc]
                sql = `select id,doc,bmail from s_inv where id=@1 and stax=@2 and status=1 `
                if (grant) {
                    sql += ' and ou=@3'
                    binds.push(u)
                }
                result = await dbs.query(sql, binds)
                rows = result.recordset
                if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
                const org = await ous.obt(taxc), signtype = org.sign, row = rows[0]
                let doc = JSON.parse(row.doc)
                let xml
                if (signtype == 1) {
                    const usr = org.usr, pwd = util.decrypt(org.pwd)
                    xml = util.j2x(doc, id)
                    const arr = [{ id: id, xml: Buffer.from(xml).toString("base64") }]
                    res.json({ lib: usr, sid: 0, pin: pwd, mst: taxc, date: new Date(), ref: config.PATH_XML, arr: arr })
                }
                else {
                    try {
                        if (signtype == 2) {
                            const pwd = util.decrypt(org.pwd)
                            const ca = await sign.ca(taxc, pwd)
                            xml = sign.sign(ca, util.j2x(doc, id), id)
                        }
                        else if (signtype == 3) {
                            const usr = org.usr, pwd = util.decrypt(org.pwd)
                            const kqs = await fcloud.xml(usr, pwd, taxc, rows), kq = kqs[0]
                            xml = kq.xml
                        }
                        else {
                            const kqs = await hsm.xml(rows), kq = kqs[0]
                            xml = kq.xml
                        }
                    } catch (err) {

                        // if(chk==1) err.message=`Đã lưu hóa đơn ${insid} .`+ err.message
                        throw new Error(`Đã lưu tạm hóa đơn ${insid}. Cấu hình chữ ký số không hợp lệ ! \n (${insid} has been temporarily saved. Invalid digital signature configuration!)`)
                    }
                    seq = await SERIAL.sequence(taxc, form, serial, body.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    body.seq = s7
                    body.status = 3
                    await dbs.query(`update s_inv set doc=@1,xml=@2,dt=@3 where id=@4 and stax=@5 and status=1`, [JSON.stringify(body), xml, moment(new Date()).format(dtf), id, taxc])
                    doc.status = 3
                    doc.seq = s7
                    if (useJobSendmail) {
                        await Service.insertDocTempJob(doc.id, doc, xml)
                    } else {
                        if (!util.isEmpty(doc.bmail)) {
                            let mailstatus = await email.rsmqmail(doc)
                            if (mailstatus) await Service.updateSendMailStatus(doc)
                        }
                        if (!util.isEmpty(doc.btel)) {
                            let smsstatus = await sms.smssend(doc)
                            if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                        }
                    }

                    const sSysLogs = { fnc_id: 'inv_adj', src: config.SRC_LOGGING_DEFAULT, dtl: `Điều chỉnh hóa đơn id ${pid}`, msg_id: pid, doc: JSON.stringify(pdoc) };
                    logging.ins(req, sSysLogs, next)
                    res.json(insid)
                }
            }
            catch (err) {
                if (AUTO) {
                    if (!((err.number == 2627) && (String(err.message).toUpperCase().indexOf("S_INV_UID_01") >= 0))) await SERIAL.err(taxc, form, serial, seq)
                }
                // if(chk==1) err.message=`Đã lưu hóa đơn ${insid} .`+ err.message
                throw err
            }
        }
        catch (err) {

            next(err)
        }
    },
    rep: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            let body = req.body, pid = body.pid
            await chkou(pid, token)
            const hdb = await Service.checkrefno(body)
            if (hdb.length > 0) throw new Error(`Số bút toán đã tồn tại: ${hdb.join()} \n (Entry number already exists: ${hdb.join()})`)
            const checkrefdate = await Service.checkrefdate(body)
            if (checkrefdate.length > 0) throw new Error(`Ngày bút toán lớn hơn ngày hóa đơn: ${checkrefdate.join(',')} \n(Ref Date is greater than Invoice Date: ${checkrefdate.join(',')})`)
            const uid = token.uid, ou = token.ou, taxc = body.stax, form = body.form, serial = body.serial, org = await ous.obt(taxc), AUTO = (org.seq === 1), id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC, id), idt = moment(new Date()).format(dtf)
            let seq, s7, result, insid
            try {
                body.id = id
                body.sec = sec
                const ouob = await ous.obid(ou)
                body.systemCode = ouob.code
                // AutoSeq exists nghĩa là PHAT HANH ở THAY THE, voi vcm ky mới cấp số
                if ((AUTO || body.AutoSeq) && ENT != 'vcm') {
                    if (body.AutoSeq) delete body["AutoSeq"]
                    seq = await SERIAL.sequence(taxc, form, serial, body.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    body.seq = s7
                    body.status = 2
                }
                else {
                    s7 = "......."
                    body.status = 1
                }
                if (ENT == 'vcm') body.c6 = sec
                result = await dbs.query(`insert into s_inv (id,pid,sec,idt,ou,uc,doc) OUTPUT inserted.id values (@1,@2,@3,@4,@5,@6,@7)`, [id, pid, sec, idt, ou, uid, JSON.stringify(body)])
                if (result.rowsAffected[0]) insid = result.recordset[0].id
                const cde = `Bị thay thế bởi HĐ số ${s7} ký hiệu ${body.serial} mẫu số ${body.form} ngày ${moment(idt).format(mfd)} mã ${ENT != "vcm" ? sec : body.c6}`
                let cdt = moment(new Date()).format(dtf)
                if (invoice_seq == 0 && ENT == "scb") {
                    await dbs.query(`update s_inv set doc=JSON_MODIFY(JSON_MODIFY(doc,'$.cdetemp',@1),'$.status',4) where id=@2`, [cde, pid])
                } else if (invoice_seq == 0) {
                    await dbs.query(`update s_inv set doc=JSON_MODIFY(doc,'$.cdetemp',@1) where id=@2`, [cde, pid])
                } else if (ENT != "sgr") {
                    await dbs.query(`update s_inv set doc=JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(doc,'$.cdt',@1),'$.status',4),'$.cde',@2),'$.canceller',@3),cid=@4,cde=@5 where id=@6`, [cdt, cde, token.uid, id, cde, pid])
                }
                await Service.saverefno(body)
                const sSysLogs = { fnc_id: 'inv_replace', src: config.SRC_LOGGING_DEFAULT, dtl: `Thay thế hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(body) };
                logging.ins(req, sSysLogs, next)
                res.json(insid)
            }
            catch (err) {
                if (AUTO) {
                    if (!((err.number == 2627) && (String(err.message).toUpperCase().indexOf("S_INV_UID_01") >= 0))) await SERIAL.err(taxc, form, serial, seq)
                }
                throw err
            }
        }
        catch (err) {
            next(err)
        }
    },
    go24: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, body = req.body, form = body.form, serial = body.serial, seq = body.seq, id = body.id, d = body.d, direct = d > 0 ? "tiến" : "lùi"
            await chkou(id, token)
            const idt = new Date(body.idt)
            idt.setDate(idt.getDate() + d)
            const dt = moment(idt), sod = dt.startOf('day').toDate(), eod = dt.endOf('day').toDate(), dts = dt.format(mfd)
            let result, row
            if (d > 0) {
                const today = moment().endOf('day').toDate()
                if (eod > today) throw new Error(`Ngày tiến hóa đơn ${dts} lớn hơn ngày hiện tại \n (The forward date of the invoice ${dts} is greater than the current date)`)
            }
            else {
                result = await dbs.query(`select count(*) rc from s_serial where taxc=@1 and form=@2 and serial=@3 and fd>@4`, [taxc, form, serial, eod])
                row = result.recordset[0]
                if (row.rc > 0) throw new Error(`Ngày lùi hóa đơn ${dts} không có TBPH \n (The backdate of the invoice ${dts} doesn't have released information)`)
            }
            if (seq) {
                const iseq = parseInt(seq)
                result = await dbs.query(`select count(*) rc from s_inv where idt<@1 and form=@2 and serial=@3 and (status=1 or ((status in (2,3,5,6)${(!["sgr"].includes(config.ent)) ? ' or (status = 4 and xml is not null)' : ''}) and cast(seq as int)>@4))`, [sod, form, serial, iseq])
                row = result.recordset[0]
                if (row.rc > 0) throw new Error(`Hóa đơn không ${direct} được: tồn tại HĐ có ngày nhỏ hơn ${dts} chưa cấp số hoặc có số lớn hơn ${seq}  \n (Invoice can not ${direct}: Existing invoices with dates less than ${dts} and not issued a number or number greater than ${seq})`)
                result = await dbs.query(`select count(*) rc from s_inv where idt>@1 and form=@2 and serial=@3 and (status in (2,3,5,6)${(!["sgr"].includes(config.ent)) ? ' or (status = 4 and xml is not null)' : ''}) and cast(seq as int)<@4`, [eod, form, serial, iseq])
                row = result.recordset[0]
                if (row.rc > 0) throw new Error(`Hóa đơn không ${direct} được: tồn tại HĐ có ngày lớn hơn ${dts} và số lớn hơn ${seq} \n (Invoice can not ${direct}: Existing invoice has a date larger than ${dts} and a number larger ${seq}) `)
            }
            else {
                result = await dbs.query(`select count(*) rc from s_inv where idt>@1 and form=@2 and serial=@3 and (status in (2,3,5,6)${(!["sgr"].includes(config.ent)) ? ' or (status = 4 and xml is not null)' : ''})`, [eod, form, serial])
                row = result.recordset[0]
                if (row.rc > 0) throw new Error(`Hóa đơn không ${direct} được : tồn tại HĐ có ngày lớn hơn ${dts} và đã cấp số \n (Invoice can not ${direct}: Existing invoice has a date larger than${dts} and issued a number)`)
            }
            await dbs.query(`update s_inv set idt=@1,doc=JSON_MODIFY(doc,'$.idt',@2) where id=@3 and status in (1, 2)`, [sod, moment(sod).format(dtf), id])
            res.json(1)
        } catch (err) {
            next(err)
        }
    },
    status: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, id = req.params.id
            let prstatus = await dbs.query(`select status from s_inv where id=${id}`)
            if (prstatus.recordset[0].status == 4) {
                throw new Error("Hóa đơn này đã được duyệt hủy \n (This invoice has been approved for cancellation)")
            } else {
                let doc, status = req.params.status
                await chkou(id, token)
                doc = await rbi(id)
                let oldstatus = doc.status
                if (status == 3 || status == 2) {
                    await dbs.query(`update s_inv set doc=JSON_MODIFY(doc,'$.cancel',NULL) where id=@1`, [id])
                    const r = await dbs.query(`select top 1 1 from s_inv where id=@1 and xml is not null`, [id])
                    if (r.recordset.length > 0 && config.ent != "scb") status = 3
                    else status = 2
                    //Xóa biên bản
                    if (UPLOAD_MINUTES) await dbs.query(`DELETE FROM s_inv_file where id=@1`, [id])
                }
                else if (status == 4) {
                    doc = await rbi(id)
                    const cancel = { typ: 3, ref: id, rdt: moment().format(dtf) }
                    if (doc.pid && doc.adj.typ == 2) await dbs.query(`update s_inv set cid=null,cde=null where id=@1`, [doc.pid])
                    let cdt = moment(new Date()).format(dtf)
                    await dbs.query(`update s_inv set doc=JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(doc,'$.cdt',@1),'$.canceller',@2),'$.cancel',JSON_QUERY(@3)) where id=@4 and stax=@5`, [cdt, token.uid, JSON.stringify(cancel), id, taxc])
                }
                let result = await dbs.query(`update s_inv set doc=JSON_MODIFY(doc,'$.status',@1) where id=@2 and stax=@3`, [status, id, taxc])
                if (result.rowsAffected[0] > 0 && (oldstatus == 3 || oldstatus == 6)) {
                    if (useJobSendmail) {
                        if (((status == 4) && !util.isEmpty(doc.bmail)) || ((status == 4) && !util.isEmpty(doc.btel))) {
                            doc.status = 4
                            await Service.insertDocTempJob(doc.id, doc, ``)
                        }
                    } else {
                        if ((status == 4) && !util.isEmpty(doc.bmail)) {
                            doc.status = 4
                            if (!util.isEmpty(doc.bmail)) {
                                let mailstatus = await email.rsmqmail(doc)
                                if (mailstatus) await Service.updateSendMailStatus(doc)
                            }
                        }
                        if ((status == 4) && !util.isEmpty(doc.btel)) {
                            doc.status = 4
                            if (!util.isEmpty(doc.btel)) {
                                let smsstatus = await sms.smssend(doc)
                                if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                            }
                        }
                    }
                    const sSysLogs = { fnc_id: 'inv_istatus', src: config.SRC_LOGGING_DEFAULT, dtl: `Chuyển đổi trạng thái hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                    logging.ins(req, sSysLogs, next)
                }
                res.json(1)
            }
        } catch (err) {
            next(err)
        }
    },
    cancel: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, id = req.params.id
            await chkou(id, token)
            let doc = await rbi(id), cancel = { typ: body.typ, ref: body.ref, rdt: body.rdt, rea: body.rea }
            let result, rows
            if (doc.pid && doc.adj.typ == 2) {
                result = await dbs.query("select cid,cde from s_inv where id=@1", [doc.pid])
                rows = result.recordset
                let cid = rows[0].cid
                if (cid && cid.split(',').length > 1) {
                    let cids = cid.split(','), cidst = ''
                    for (let c of cids) {
                        if (c != id) cidst = cidst + ',' + c
                    }
                    cidst = cidst.slice(1)
                    await dbs.query(`update s_inv set cid=@1 where id=@2`, [cidst, doc.pid])
                } else {
                    await dbs.query(`update s_inv set cid=null,cde=null where id=@1`, [doc.pid])
                }

            }
            let cdt = moment(new Date()).format(dtf)
            doc = await rbi(id)
            let oldstatus = doc.status
            if (ENT == 'sgr') {
                doc.cdt = doc.idt
            } else doc.cdt = cdt
            doc.canceller = token.uid
            doc.status = 4
            doc.cancel = cancel
            result = await dbs.query(`update s_inv set doc=@1 where id=@2 and stax=@3`, [JSON.stringify(doc), id, token.taxc])
            if (ENT == 'mzh') {
                await dbs.query(`update  s_trans set status=4,seq=@2 where inv_id=@1`, [id, doc.seq])

            }
            if (result.rowsAffected[0] && oldstatus == 3) {
                if (useJobSendmail) {
                    if ((!util.isEmpty(doc.bmail)) || (!util.isEmpty(doc.btel))) {
                        doc.status = 4
                        await Service.insertDocTempJob(doc.id, doc, ``)
                    }
                } else {
                    if (!util.isEmpty(doc.bmail)) {
                        doc.status = 4
                        if (!util.isEmpty(doc.bmail)) {
                            let mailstatus = await email.rsmqmail(doc)
                            if (mailstatus) await Service.updateSendMailStatus(doc)
                        }
                    }
                    if (!util.isEmpty(doc.btel)) {
                        doc.status = 4
                        if (!util.isEmpty(doc.btel)) {
                            let smsstatus = await sms.smssend(doc)
                            if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                        }
                    }
                }
            }
            const sSysLogs = { fnc_id: 'inv_cancel', src: config.SRC_LOGGING_DEFAULT, dtl: `Hủy hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
            logging.ins(req, sSysLogs, next)
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },
    trash: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, id = req.params.id
            await chkou(id, token)
            //Lay du lieu doc truoc khi xoa de luu log
            let doclog = await rbi(id)
            const cancel = { typ: body.typ, ref: body.ref, rdt: body.rdt, rea: body.rea }
            await dbs.query(`update s_inv set doc=JSON_MODIFY(JSON_MODIFY(doc,'$.status',6),'$.cancel',JSON_QUERY(@1)) where id=@2 and stax=@3 and status in (2,3)`, [JSON.stringify(cancel), id, token.taxc])
            res.json(1)
            const sSysLogs = { fnc_id: 'inv_adj', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doclog) };
            logging.ins(req, sSysLogs, next)
        }
        catch (err) {
            next(err)
        }
    },
    trashAll: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, invs = body.invs, note = body.note
            for (let inv of invs) {
                const cancel = { typ: 3, ref: inv.id, rdt: moment(new Date()).format("YYYY-MM-DD"), rea: note }
                await dbs.query(`update s_inv set doc=JSON_MODIFY(JSON_MODIFY(doc,'$.status',6),'$.cancel',JSON_QUERY(@1)) where id=@2 and stax=@3 and status in (2,3)`, [JSON.stringify(cancel), inv.id, token.taxc])

            }

            res.json(1)

        }
        catch (err) {
            next(err)
        }
    },
    istatus: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, body = req.body, ids = body.ids, os = parseInt(body.os), now = moment().format(dtf)
            let rc = 0, ns = body.ns
            for (let id of ids) {
                try {
                    let doc, sql, binds, doclog, sSysLogs
                    doclog = await rbi(id)
                    let oldstatus = doclog.status
                    if (ns == 4) {
                        const re = await dbs.query("select doc,cdt,seq from s_inv where id=@1", [id])
                        const rws = re.recordset
                        if (rws.length == 0) throw new Error(`Không tìm thấy Hóa đơn ${id} \n (Invoice not found ${id})`)
                        doc = JSON.parse(rws[0].doc)
                        //Lay du lieu doc truoc khi xoa de luu log
                        doclog = await rbi(id)
                        let cdt = rws[0].cdt
                        if (cdt) {
                            sql = `update s_inv set doc=JSON_MODIFY(doc,'$.status',@1) where id=@2 and stax=@3 and status=@4`
                            binds = [ns, id, taxc, os]
                        } else {
                            if (ENT == 'sgr') {
                                cdt = doc.idt
                            } else cdt = moment(new Date()).format(dtf)
                            sql = `update s_inv set doc=JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(doc,'$.cdt',@1),'$.status',@2),'$.canceller',@3) where id=@4 and stax=@5 and status=@6`
                            binds = [cdt, ns, token.uid, id, taxc, os]

                        }
                        if (ENT == 'mzh') {
                            await dbs.query(`update  s_trans set status=4,seq=@2 where inv_id=@1`, [id, rws[0].seq])

                        }
                        if (doc.pid && doc.adj.typ == 2) await dbs.query(`update s_inv set cid=null,cde=null where id=@1`, [doc.pid])
                        if (!doc.cancel) await dbs.query(`update s_inv set doc=JSON_MODIFY(doc,'$.cancel',JSON_QUERY(@1)) where id=@2 and stax=@3 and status=@4`, [JSON.stringify({ typ: 3, ref: id, rdt: now }), id, taxc, os])
                        sSysLogs = { fnc_id: 'inv_cancel', src: config.SRC_LOGGING_DEFAULT, dtl: `Hủy hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doclog) };
                    } else {
                        //Lay du lieu doc truoc khi xoa de luu log
                        doclog = await rbi(id)
                        if (ns == 2 || ns == 3) {
                            const r = await dbs.query(`select top 1 1 from s_inv where id=@1 and xml is not null`, [id])
                            if (r.recordset.length > 0) ns = 3
                            else ns = 2
                        }
                        sql = `update s_inv set doc=JSON_MODIFY(doc,'$.status',@1) where id=@2 and stax=@3 and status=@4`
                        binds = [ns, id, taxc, os]
                        sSysLogs = { fnc_id: 'inv_istatus', src: config.SRC_LOGGING_DEFAULT, dtl: `Chuyển đổi trạng thái hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doclog) };
                    }
                    const result = await dbs.query(sql, binds)
                    if (result.rowsAffected[0] > 0 && (oldstatus == 3 || oldstatus == 6 || oldstatus == 2)) {
                        if (useJobSendmail) {
                            if (((ns == 4) && !util.isEmpty(doc.bmail)) || ((ns == 4) && !util.isEmpty(doc.btel))) {
                                doc.status = 4
                                await Service.insertDocTempJob(doc.id, doc, ``)
                            }
                        } else {
                            if ((ns == 4) && !util.isEmpty(doc.bmail)) {
                                doc.status = 4
                                if (!util.isEmpty(doc.bmail)) {
                                    let mailstatus = await email.rsmqmail(doc)
                                    if (mailstatus) await Service.updateSendMailStatus(doc)
                                }
                            }
                            if ((ns == 4) && !util.isEmpty(doc.btel)) {
                                doc.status = 4
                                if (!util.isEmpty(doc.btel)) {
                                    let smsstatus = await sms.smssend(doc)
                                    if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                                }
                            }
                        }
                        rc++
                    }

                    logging.ins(req, sSysLogs, next)
                } catch (e) {
                    logger4app.error(e)
                }
            }
            res.json(rc)
        }
        catch (err) {
            next(err)
        }
    },
    del: async (req, res, next) => {
        try {
            const token = SEC.decode(req), id = req.params.id, pid = req.params.pid
            await chkou(id, token)
            //Lay du lieu doc truoc khi xoa de luu log
            let doclog = await rbi(id)
            if (ENT == "scb") {
                for (const item of doclog.items) {
                    if (item.tranID) {
                        await dbs.query(`update s_trans set status = @1, inv_id = null, inv_date = null where tranID = @2`, [item.statusGD, item.tranID])
                    }
                }
            }
            const result = await dbs.query(`delete from s_inv where id=@1 and stax=@2 and status=@3`, [id, token.taxc, 1])
            if (result.rowsAffected[0] > 0 && pid > 0) {
                let doc = await rbi(pid)
                doc.status = 3
                //Xóa doc.cde , nếu không có cũng ko ảnh hưởng.
                delete doc["cde"]
                delete doc["cdt"]
                delete doc["canceller"]
                // await dbs.query(`update s_inv set doc=JSON_MODIFY(doc,'$.status',3),cid=null,cde=null where id=@1`, [pid])
                await dbs.query(`update s_inv set doc=@1,cid=null,cde=null where id=@2`, [JSON.stringify(doc), pid])
                //Xóa biên bản
                await dbs.query(`DELETE FROM s_inv_file where id=@1`, [pid])
            }
            const sSysLogs = { fnc_id: 'inv_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doclog) };
            logging.ins(req, sSysLogs, next)
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },
    ddsSent: async (req, res, next) => {
        let pooldds
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const taxc = token.taxc, uid = token.uid, ou = token.ou,
                org = await ous.obt(taxc), AUTO = (org.seq === 1), body = req.body,
                invs = body.invs
            let count = 1
            let status, message, results, resultsdds, resultsddspas
            try {
                const token = SEC.decode(req)
                let configdds = config.poolConfigdds

                resultsdds = await dbs.dds()

                if (resultsdds.recordset.length > 0) {

                    resultsddspas = await dbs.ddspass()

                }

                if (resultsddspas && resultsddspas.recordset.length > 0) {
                    for (let inv of invs) {
                        let chk = await chkDds(inv.c9, resultsdds.recordset)
                        if (chk == 1) {
                            let pass = await chkDdsPass(inv.c9, resultsddspas.recordset)
                            if (pass) {


                                let file = await ifile.getFileMailPass(inv.id, pass)
                                let date = new Date();
                                await ftp.uploadattach_mzh(file.zip, file.filename, date)
                                const pooldds2 = dbs.getConn2()

                                await dbs.ddsIns(inv, date)

                                //logger4app.debug('gửi dds success hoa đơn id :'+ inv.id)
                                await dbs.query("update  s_inv set dds_sent=1,file_dds=@1 where id=@2", [`${inv.c9}-${inv.curr}-${inv.seq}.zip`.split("/").join("."), inv.id])
                            }

                        }
                    }
                }

                status = 1
                message = 'Success'
            } catch (e) {
                //dbs.getConn()
                status = 2
                throw e


            }

            res.json({ message: message, status: status })
        }
        catch (err) {
            next(err)
        }
    },
    ddsStt: async (req, res, next) => {
        let pooldds
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const body = req.body,
                invs = body.invs,
                td = body.td,
                fd = body.fd

            let status, message, resultsdds
            try {

                resultsdds = await dbs.ddsStt(fd, td)
                const rows = resultsdds.recordset
                for (let inv of invs) {
                    for (let row of rows) {
                        if (row.CustomerAbbr == inv.c9 && row.FilePath == inv.file_dds) {
                            await dbs.query("update  s_inv set dds_stt=@1 where c9=@2 and file_dds=@3", [row.StatusCode, row.CustomerAbbr, row.FilePath])
                        }

                    }
                }

                status = 1
                message = 'Success'
            } catch (e) {
                //dbs.getConn()
                status = 2
                throw e


            }

            res.json({ message: message, status: status })
        }
        catch (err) {
            next(err)
        }
    },
    relate: async (req, res, next) => {
        try {
            const token = SEC.decode(req), id = req.params.id
            let sql, result, colvcm = ENT == "vcm" ? ",c6 c6" : ""
            result = await dbs.query("select doc,pid from s_inv where id=@1", [id])
            const rows = result.recordset
            if (rows.length == 0) reject(new Error(`Không tìm thấy Hóa đơn ${id} \n (Invoice not found ${id})`))
            const doc = JSON.parse(rows[0].doc), sec = doc.sec, root = doc.root, rsec = root ? root.sec : null, pid = rows[0].pid
            let binds = [token.taxc, sec]
            sql = `select id,idt,type,form,serial,seq,sec,status,adjdes,cde${colvcm} from s_inv where stax=@1 and sec<>@2 and (JSON_VALUE(doc,'$.root.sec')=@2`
            if (rsec) {
                if (pid) {
                    sql += ` or sec=@3 or JSON_VALUE(doc,'$.root.sec')=@3 or id in (${pid})`
                } else {
                    sql += ` or sec=@3 or JSON_VALUE(doc,'$.root.sec')=@3 `
                }

                binds.push(rsec)
            }

            sql += ") order by id"
            result = await dbs.query(sql, binds)
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    relateadjs: async (req, res, next) => {
        try {
            const token = SEC.decode(req), id = req.params.id
            let sql, result
            sql = `select id id,idt idt,type type,form form,serial serial,seq seq,sec sec,status status,adjdes adjdes,cde cde${colvcm} from s_inv a where id in (${id}) order by id`

            result = await dbs.query(sql, [])
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    deleteinv: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const taxc = token.taxc, uid = token.uid, ou = token.ou,
                org = await ous.obt(taxc), AUTO = (org.seq === 1), body = req.body,
                invs = body.invs
            let count = 1, countseq = 0
            let status, message, seqsgrstt2 = [], idsgr = []

            try {
                const token = SEC.decode(req)
                //xoa nhieu hoa don cho sgr khong lung so
                if (config.ent == "sgr") {
                    for (let invsgr of invs) {
                        if (invsgr.status == 2) {
                            seqsgrstt2.push(invsgr.seq)
                            idsgr.push(invsgr.id)
                        }
                        if (invsgr.status == 1) {
                            await chkou(invsgr.id, token)
                            //Lay du lieu doc truoc khi xoa de luu log
                            let doclog = await rbi(invsgr.id)
                            await dbs.query(`delete from s_inv where id=@1 and status in (1) `, [invsgr.id])
                            await Service.delrefno(doclog)
                            await dbs.query(`update  s_inv set cid=null,cde=null where id=@1 and cid=@2`, [invsgr.pid, invsgr.id])
                            countseq++
                            const sSysLogs = { fnc_id: 'inv_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa hóa đơn id ${invsgr.id}`, msg_id: invsgr.id, doc: JSON.stringify(doclog) };
                            logging.ins(req, sSysLogs, next)
                        }
                    }
                    if (seqsgrstt2.length > 0) {
                        let minseq = Math.min.apply(Math, seqsgrstt2), sql, result, rowseq, uk, max, key, sqlidsgr, rowsgr, idtsgr, checksgr, seqsgr, invsgrcd
                        max = minseq - 1
                        sqlidsgr = await dbs.query(`select stax,form,serial from s_inv where id in (@1)`, idsgr)
                        rowsgr = sqlidsgr.recordset[0]
                        //kiểm tra xem có tồn tại shd đã duyệt lơn hơn shd hiện tại
                        seqsgr = await dbs.query(`select count(*) dem from s_inv where stax=@1 and form=@2 and serial=@3 and seq>@4 and status not in(1,2)`, [rowsgr.stax, rowsgr.form, rowsgr.serial, minseq])
                        if (seqsgr.recordset[0].dem > 0) {
                            throw new Error(`Đã có hóa đơn có số lớn hơn đã được duyệt./nThere have been a larger number of invoices that have been approved `)
                        }
                        //xoá hoá đơn chờ cấp số có ngày lơn hơn ngày được chọn
                        idtsgr = await dbs.query(`select idt from s_inv where stax=@1 and form=@2 and serial=@3 and seq=@4`, [rowsgr.stax, rowsgr.form, rowsgr.serial, minseq])
                        invsgrcd = await dbs.query(`select * from s_inv where stax=@1 and form=@2 and serial=@3 and idt>=@4 and status in (1) `, [rowsgr.stax, rowsgr.form, rowsgr.serial, idtsgr.recordset[0].idt])
                        let rowinvsgrcd = invsgrcd.recordset
                        for (let rowinvcd of rowinvsgrcd) {
                            await chkou(rowinvcd.id, token)
                            //Lay du lieu doc truoc khi xoa de luu log
                            let doclog = await rbi(rowinvcd.id)
                            await dbs.query(`delete from s_inv where id=@1 and status in (1) `, [rowinvcd.id])
                            await Service.delrefno(doclog)
                            await dbs.query(`update  s_inv set cid=null,cde=null where id=@1 and cid=@2`, [rowinvcd.pid, rowinvcd.id])
                            countseq++
                            const sSysLogs = { fnc_id: 'inv_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa hóa đơn id ${rowinvcd.id}`, msg_id: rowinvcd.id, doc: JSON.stringify(doclog) };
                            logging.ins(req, sSysLogs, next)

                        }
                        sql = `select * from s_inv where stax=@1 and form=@2 and serial=@3 and seq>=@4 and status in (2)`
                        result = await dbs.query(sql, [rowsgr.stax, rowsgr.form, rowsgr.serial, minseq])
                        rowseq = result.recordset
                        for (let oldrowseq of rowseq) {
                            uk = `${oldrowseq.stax}.${oldrowseq.form}.${oldrowseq.serial}`
                            key = `SERIAL.${uk}`
                            await redis.set(key, max)
                            await dbs.query(`update s_serial set cur=@1 where taxc=@2 and form=@3 and serial=@4 and status=1 and min<=@1 and max>@1`, [max, oldrowseq.stax, oldrowseq.form, oldrowseq.serial])
                            await chkou(oldrowseq.id, token)
                            //Lay du lieu doc truoc khi xoa de luu log
                            let doclog = await rbi(oldrowseq.id)
                            await dbs.query(`delete from s_inv where id=@1 `, [oldrowseq.id])
                            countseq++
                            await Service.delrefno(doclog)
                            await dbs.query(`update  s_inv set cid=null,cde=null,doc=JSON_MODIFY(doc,'$.status','3') where id=@1 and cid=@2`, [oldrowseq.pid, oldrowseq.id])
                            const sSysLogs = { fnc_id: 'inv_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa hóa đơn id ${oldrowseq.id}`, msg_id: oldrowseq.id, doc: JSON.stringify(doclog) };
                            logging.ins(req, sSysLogs, next)
                        }
                    }

                } else {
                    for (let inv of invs) {
                        await chkou(inv.id, token)
                        //Lay du lieu doc truoc khi xoa de luu log
                        let doclog = await rbi(inv.id)
                        if (ENT == "scb") {
                            for (const item of doclog.items) {
                                if (item.tranID) {
                                    await dbs.query(`update s_trans set status = @1, inv_id = null, inv_date = null where tranID = @2`, [item.statusGD, item.tranID])
                                }
                            }
                        }
                        await dbs.query(`delete from s_inv where id=@1 and status in (1) `, [inv.id])
                        await Service.delrefno(doclog)
                        await dbs.query(`update  s_inv set cid=null,cde=null where id=@1 and cid=@2`, [inv.pid, inv.id])
                        countseq++
                        if (config.ent == 'mzh') {
                            await dbs.query(`update  s_trans set status=stattus_temp,inv_id=inv_id_tmp,exrt=null,inv_date=null,ma_nv=null,trancol=null where inv_id=@1`, [inv.id])
                        }
                        const sSysLogs = { fnc_id: 'inv_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa hóa đơn id ${inv.id}`, msg_id: inv.id, doc: JSON.stringify(doclog) };
                        logging.ins(req, sSysLogs, next)
                    }
                }
                status = 1
                message = 'Success'
            } catch (e) {
                status = 2
                throw e


            }

            res.json({ message: message, status: status, countseq: countseq })
        }
        catch (err) {
            next(err)
        }
    },
    mailinvs: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, invs = body.invs
            let status, message

            try {
                for (let inv of invs) {
                    let docmail = await rbi(inv.id)
                    let doc = JSON.parse(JSON.stringify(docmail)), xml = ''
                    if (config.ent == 'vcm') {
                        let taxc_root = await ous.pidtaxc(doc.ou)
                        doc.taxc_root = taxc_root
                    }
                    //logger4app.debug(`begin send mail : ${doc.stax} - ${doc.form} - ${doc.serial} - ${doc.seq}`)
                    if (useJobSendmail) {
                        await Service.insertDocTempJob(doc.id, doc, xml)
                    } else {
                        if (!util.isEmpty(doc.bmail)) {
                            let mailstatus = await email.senddirectmail(doc, xml)
                            if (mailstatus) await Service.updateSendMailStatus(doc)
                        }
                        if (!util.isEmpty(doc.btel)) {
                            let smsstatus = await sms.smssend(doc)
                            if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                        }
                    }
                }
                status = 1
                message = 'Success'
            } catch (e) {
                status = 2
                throw e
            }

            res.json({ message: message, status: status })
        }
        catch (err) {
            next(err)
        }
    },
    sendmaildoc: async (req, res, next) => {
        try {
            let doc = req.body, xml = doc.xml
            logger4app.debug(`inv.sendmaildoc doc ${JSON.stringify(doc)}`)
            if (ENT == 'vcm') {
                let taxc_root = await ous.pidtaxc(doc.ou)
                doc.taxc_root = taxc_root
            }
            logger4app.debug(`begin send mail : ${doc.stax} - ${doc.form} - ${doc.serial} - ${doc.seq}`)
            if (config.useJobSendmailAPI) {
                await Service.insertDocTempJob(doc.id, doc, xml)
            } else {
                if (!util.isEmpty(doc.bmail)) {
                    let mailstatus = await email.senddirectmail(doc, xml)
                    if (mailstatus) await Service.updateSendMailStatus(doc)
                }
                if (!util.isEmpty(doc.btel)) {
                    let smsstatus = await sms.smssend(doc)
                    if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                }
            }
            logger4app.debug(`end send mail : ${doc.stax} - ${doc.form} - ${doc.serial} - ${doc.seq}`)
            res.json({ result: "1" })
        }
        catch (err) {
            throw err
        }

    },
    signdoc: async (req, res, next) => {
        try {
            let doc = req.body
            const org = await ous.gettosign(doc.stax)
            //logger4app.debug(org)
            let signtype = org.sign
            let xml, binds, result
            let sql

            if (signtype == 1) {
                const usr = org.usr, pwd = util.decrypt(org.pwd)
                xml = util.j2x(doc, doc.id)
                const arr = [{ inc: id, xml: Buffer.from(xml).toString("base64") }]
                res.json({ lib: usr, sid: 0, pin: pwd, mst: doc.stax, date: new Date(), ref: config.PATH_XML, arr: arr })
            }
            else {
                if (signtype == 2) {
                    const pwd = util.decrypt(org.pwd)
                    const ca = await sign.ca(doc.stax, pwd)
                    xml = util.signature(sign.sign(ca, util.j2x((await Service.getcol(doc)), doc.id), doc.id), doc.idt)
                }
                else if (signtype == 3) {
                    sql = `select id,doc,bmail,pid from s_inv where id=@1 and stax=@2 and status=2`
                    binds = [doc.id, doc.stax]
                    result = await dbs.query(sql, binds)
                    const rows = result.recordset
                    if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
                    const usr = org.usr, pwd = util.decrypt(org.pwd)
                    const kqs = await fcloud.xml(usr, pwd, doc.stax, rows), kq = kqs[0]
                    xml = kq.xml
                }
                else {
                    xml = await hsm.signxml(doc.stax, util.j2x(doc, doc.id))
                    xml = util.signature(xml, doc.idt)
                }
            }
            res.json({ xml: xml })
        }
        catch (err) {
            next(err)
        }

    },
    signxml: async (req, res, next) => {
        try {
            const xml = req.body.xml, stax = req.body.stax, id = req.body.id, idt = req.body.idt
            const org = await ous.gettosign(stax)
            //logger4app.debug(org)
            let signtype = org.sign
            let binds, result, xmlresult
            let sql
            //console.time(`${stax} - ${id}`);
            if (signtype == 1) {
                const usr = org.usr, pwd = util.decrypt(org.pwd)
                xmlresult = util.j2x(doc, doc.id)
                const arr = [{ inc: id, xml: Buffer.from(xml).toString("base64") }]
                res.json({ lib: usr, sid: 0, pin: pwd, mst: stax, date: new Date(), ref: config.PATH_XML, arr: arr })
            }
            else {
                if (signtype == 2) {
                    const pwd = util.decrypt(org.pwd)
                    const ca = await sign.ca(stax, pwd)
                    xmlresult = util.signature(sign.sign(ca, xml, id), idt)
                }
                else if (signtype == 3) {
                    sql = `select id,doc,bmail,pid from s_inv where id=@1 and stax=@2 and status=2`
                    binds = [id, stax]
                    result = await dbs.query(sql, binds)
                    const rows = result.recordset
                    if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
                    const usr = org.usr, pwd = util.decrypt(org.pwd)
                    const kqs = await fcloud.xml(usr, pwd, stax, rows), kq = kqs[0]
                    xmlresult = kq.xml
                }
                else {
                    xmlresult = await hsm.signxml(stax, xml)
                    xmlresult = util.signature(xmlresult, idt)
                }
            }
            //console.timeEnd(`${stax} - ${id}`);
            res.json({ xml: xmlresult })
        }
        catch (err) {
            next(err)
        }

    },
    syncRedisDb: async (req, res, next) => {
        try {
            var sqlinv = 'select max(id) id from s_inv'
            //logger4app.debug('sqlinv : ', sqlinv)
            var resultinv = await dbs.query(sqlinv, [])
            var maxid = resultinv.recordset[0].id
            //logger4app.debug('resultinv.recordset[0] : ', resultinv.recordset[0])
            //logger4app.debug('maxid : ', maxid)
            var keyidinv = `INC`, curid = await redis.get(keyidinv)
            if (curid < maxid) {
                await redis.set(keyidinv, maxid)
            }
            res.json({ result: "1" })
        } catch (err) {
            next(err)
        }
    },
    print: async (req, res, next) => {
        try {
            const iwh = await Service.iwh(req, true)
            let sql = `select doc from s_inv  ${iwh.where} order by serial,seq`//ko dung index chi dinh vi se cham
            if (ENT == 'vcm') sql = `select doc from s_inv WITH(INDEX(s_inv_idx_final)) ${iwh.where} order by serial,seq`//dung index chi dinh nhanh
            const result = await dbs.query(sql, iwh.binds)
            const rows = result.recordset
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn \n No invoice be found`)
            const token = SEC.decode(req), org = await ous.org(token), idx = org.temp, filter = JSON.parse(req.query.filter), fn = util.fn(filter.form, idx), tmp = await util.template(fn)
            // const token = SEC.decode(req), mst = token.mst, org = await ous.org(token), qrc = org.qrc == 1, filter = JSON.parse(req.query.filter)
            // for (const row of rows) {
            //     const doc = row.doc
            //     util.viewadj(doc)
            //if (qrc) row.qrcode = await util.qrcode(row.doc)
            // }
            for (const row of rows) {
                let doc = JSON.parse(row.doc)
                doc.tempfn = fn
                row.doc = doc
            }
            res.json({ docs: rows, tmp: tmp })
        } catch (err) {
            next(err)
        }
    },
    printmerge: async (req, res, next) => {
        try {
            const iwh = await Service.iwh(req, true)
            let sql = `select doc from s_inv  ${iwh.where} order by serial,seq`//ko dung index chi dinh vi se cham
            if (ENT == 'vcm') sql = `select doc from s_inv WITH(INDEX(s_inv_idx_final)) ${iwh.where} order by serial,seq`//dung index chi dinh nhanh
            const result = await dbs.query(sql, iwh.binds)
            const rows = result.recordset
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn \n No invoice be found`)
            const token = SEC.decode(req), org = await ous.org(token), idx = org.temp, filter = JSON.parse(req.query.filter), fn = util.fn(filter.form, idx), tmp = await util.template(fn)
            // const token = SEC.decode(req), mst = token.mst, org = await ous.org(token), qrc = org.qrc == 1, filter = JSON.parse(req.query.filter)
            // for (const row of rows) {
            //     const doc = row.doc
            //     util.viewadj(doc)
            //if (qrc) row.qrcode = await util.qrcode(row.doc)
            // }
            let jsons = []
            for (const row of rows) {
                let doc = JSON.parse(row.doc)
                doc.tempfn = fn
                row.doc = doc
                jsons.push({ doc: row.doc })
            }
            let respdf = await util.jsReportRenderAttachPdfs(jsons)
            res.json(respdf)
        } catch (err) {
            next(err)
        }
    },
    printall: async (req, res, next) => {
        try {
            const iwh = await Service.iwh(req, true)
            let where = iwh.where, binds = iwh.binds
            let oldid = req.query.arr
            where += ` and id in('${oldid}')`
            let sql = `select id, doc from s_inv  ${where} order by serial,seq`//ko dung index chi dinh vi se cham
            const result = await dbs.query(sql, binds)
            const rows = result.recordset
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn \n No invoice be found`)
            const token = SEC.decode(req), org = await ous.org(token), idx = org.temp, filter = JSON.parse(req.query.filter), fn = util.fn(filter.form, idx), tmp = await util.template(fn)
            for (const row of rows) {
                row.doc = JSON.parse(row.doc)
                row.doc.tempfn = fn
            }
            res.json({ docs: rows, tmp: tmp })
        } catch (err) {
            next(err)
        }
    },
    synReport: async (req, res, next) => {
        try {
            const body = req.body
            let sql, binds, fn, rows, json, result, status, fd = body.fd, td = body.td, rowd = []
            let c0, c1 = 0, c2 = 0, c3 = 0

            //so sánh status
            if (body.status == 1) {
                status = "Chờ cấp số"
            } else if (body.status == 2) {
                status = "Chờ duyệt"
            } else if (body.status == 6) {
                status = "Chờ hủy"
            } else if (body.status == 3) {
                status = "Đã duyệt"
            }

            let where = '1=1 '

            fn = "temp/MZH_INVSReport.xlsx"

            sql = `select a.curr,JSON_VALUE(JSON_QUERY (doc, '$.tax[0]'),'$.vrt') vrt,sum(a.sum) as sum, sum(a.sumv) as sumv, sum(a.vat) as vat, sum(a.vatv) as vatv from s_inv a where idt between @1 and @2 and ${where} and a.status=@3 group by a.curr,JSON_VALUE(JSON_QUERY (doc, '$.tax[0]'),'$.vrt') order by curr ,cast(JSON_VALUE(JSON_QUERY (doc, '$.tax[0]'),'$.vrt') as int)`
            binds = [fd, td, body.status]

            result = await dbs.query(sql, binds)
            rows = result.recordset
            let lable = '', sumv = 0, vatv = 0
            for (let row of rows) {
                if (lable == '') {
                    row.c0 = 'CURRENCY ( Loại tiền)'
                    row.c1 = row.curr
                    row.c2 = ''
                    row.c3 = ''
                    lable = row.curr
                    rowd.push(row)
                    switch (row.vrt) {
                        case "-1":
                            c0 = "-       Not subject to VAT ( Không chịu thuế)"
                            break;
                        case "0":
                            c0 = "-       EPZ customer ( 0%)"
                            break;
                        case "5":
                            c0 = "-       5%"

                            break;
                        case "10":
                            c0 = "-       10%"

                            break
                        default:
                            logger4app.debug("Không có giá trị tương ứng");
                            break;
                    }

                    c1 = ''
                    c2 = row.sum
                    c3 = row.vat
                    rowd.push({ c0: c0, c1: c1, c2: c2, c3: c3 })
                    sumv += row.sum
                    vatv += row.vat
                } else {
                    if (row.curr != lable) {
                        row.c0 = 'TOTAL'
                        row.c1 = lable
                        row.c2 = sumv
                        row.c3 = vatv
                        rowd.push(row)
                        sumv = 0
                        vatv = 0

                        rowd.push({ c0: "", c1: "", c2: "", c3: "" })
                        // rowd.push(row)

                        lable = row.curr
                        rowd.push({ c0: 'CURRENCY ( Loại tiền)', c1: row.curr, c2: "", c3: "" })
                        switch (row.vrt) {
                            case "-1":
                                c0 = "-       Not subject to VAT ( Không chịu thuế)"
                                break;
                            case "0":
                                c0 = "-       EPZ customer ( 0%)"
                                break;
                            case "5":
                                c0 = "-       5%"
                                break;
                            case "10":
                                c0 = "-       10%"
                                break
                            default:
                                logger4app.debug("Không có giá trị tương ứng");
                                break;
                        }

                        c1 = ''
                        c2 = row.sum
                        c3 = row.vat
                        rowd.push({ c0: c0, c1: c1, c2: c2, c3: c3 })
                        sumv += row.sum
                        vatv += row.vat
                        //rowd.push(row)
                    } else {
                        switch (row.vrt) {
                            case "-1":
                                row.c0 = "-      Not subject to VAT ( Không chịu thuế)"
                                break;
                            case "0":
                                row.c0 = "-       EPZ customer ( 0%)"
                                break;
                            case "5":
                                row.c0 = "-       5%"
                                break;
                            case "10":
                                row.c0 = "-       10%"
                                break
                            default:
                                logger4app.debug("Không có giá trị tương ứng");
                                break;
                        }

                        row.c1 = ''
                        row.c2 = row.sum
                        row.c3 = row.vat
                        rowd.push(row)
                        sumv += row.sum
                        vatv += row.vat
                    }
                }

            }
            if (rows.length > 0) {
                rowd.push({ c0: 'TOTAL', c1: lable, c2: sumv, c3: vatv })
            }
            json = { fd: moment(fd).format("DD/MM/YYYY"), ft: moment(td).format("DD/MM/YYYY"), status: status, table: rowd }

            const file = path.join(__dirname, "..", "..", "..", fn)
            const xlsx = fs.readFileSync(file)
            const template = new xlsxtemp(xlsx)
            template.substitute(1, json)
            res.end(template.generate(), "binary")

        } catch (err) {
            next(err)
        }
    },
    detailS: async (req, res, next) => {
        try {
            let fn, json, where, body = req.body, sql, where1, result, rows, where2
            let month = body.month, year = body.year
            fn = "temp/MZH_INVSDetailS.xlsx"

            where = `where MONTH(idt)=${month} and YEAR(idt)=${year} and status=3   `

            let name = `JSON_VALUE(JSON_QUERY (doc, '$.items[0]'),'$.name')`

            sql = `select form, serial, seq, idt, bname, buyer, baddr,btax,curr,sum,status, note, ${name} name,JSON_VALUE(JSON_QUERY (doc, '$.tax[0]'),'$.vrt') vrt,JSON_VALUE(JSON_QUERY (doc, '$.tax[0]'),'$.vrn') vrn,vat,total,exrt,adjtyp from s_inv ${where} `

            if (config.BKHDVAT_EXRA_OPTION == '2') {
                //Hóa đơn bị hủy
                where1 = `where MONTH(cdt)=${month} and YEAR(cdt)=${year} and status=4 and xml is not null `
                sql += ` union all select form, serial, seq, cdt as idt, bname, buyer, baddr,btax,curr,sum,status, note, ${name} name,JSON_VALUE(JSON_QUERY (doc, '$.tax[0]'),'$.vrt') vrt,JSON_VALUE(JSON_QUERY (doc, '$.tax[0]'),'$.vrn') vrn,vat,total,exrt,adjtyp from s_inv ${where1}`
            }

            //Hóa đơn thay thế
            where2 = `where MONTH(idt)=${month} and YEAR(idt)=${year} and status=3 and pid is not null`
            sql += `union all select form, serial, seq, idt, bname, buyer, baddr,btax,curr,sum,status, note, ${name} name,JSON_VALUE(JSON_QUERY (doc, '$.tax[0]'),'$.vrt') vrt,JSON_VALUE(JSON_QUERY (doc, '$.tax[0]'),'$.vrn') vrn,vat,total,exrt,adjtyp from s_inv ${where2}`

            // sql='select * from (' + sql + ') t order by curr,form,serial,seq'
            sql = `select * from (${sql}) t order by curr,cast(vrt as int),form,serial,seq`
            result = await dbs.query(sql)
            rows = result.recordset

            //Lấy tỉ suất vào ngày cuối cùng của tháng
            let sql2, result2, subrows, usdEx, vndEx, moneyEx, objectMoneyEx = {}
            let today = new Date();
            let dd = String(today.getDate()).padStart(2, '0');
            let mm = String(today.getMonth() + 1).padStart(2, '0');
            let lastMonth = parseInt(month) - 1
            let lastYear = parseInt(year) - 1
            let lastDateMonth

            lastDateMonth = new Date(parseInt(year), parseInt(month), 0)

            if (lastDateMonth.getDay() == 6) {
                const t = lastDateMonth.getDate() + (6 - lastDateMonth.getDay() - 1);
                lastDateMonth.setDate(t);
            } else if (lastDateMonth.getDay() == 0) {
                const t = lastDateMonth.getDate() + (6 - lastDateMonth.getDay() - 1) - 7;
                lastDateMonth.setDate(t);
            }

            let whereDate = lastDateMonth.getDate()
            let whereMonth = lastDateMonth.getMonth() + 1
            let whereYear = lastDateMonth.getFullYear()

            sql2 = `select  val,cur,dt from s_ex where MONTH(dt)=${whereMonth} and YEAR(dt)=${whereYear} order by cur,dt desc`
            // and DAY(dt)=${whereDate}`
            result2 = await dbs.query(sql2)
            subrows = result2.recordset
            // logger4app.debug(subrows)

            for (const rowsub of subrows) {
                if (!objectMoneyEx[rowsub.cur]) {
                    objectMoneyEx[rowsub.cur] = rowsub.val
                    moneyEx = rowsub.val
                }


            }

            let a0 = [], a3 = [], a4 = [], a10 = [], a1 = [], a2 = []
            let i0 = 0, i3 = 0, i4 = 0, i10 = 0, i1 = 0, i2 = 0
            let s0 = 0, s1 = 0, s3 = 0, s4 = 0, s10 = 0
            let v0 = 0, v1 = 0, v3 = 0, v4 = 0, v10 = 0
            let t0 = 0, t1 = 0, t3 = 0, t4 = 0, t10 = 0
            let sumExC01 = 0, sumExC02 = 0, sumExC03 = 0, sumExC04 = 0, sumVrt01 = 0, sumVrt02 = 0, sumVrt03 = 0, sumVrt04 = 0, totalIvs2 = 0
            let nextCurr01 = '', lastCurr01 = '', nextCurr03 = '', lastCurr03 = '', nextCurr04 = '', lastCurr04 = ''
            let objectSum, textSum, textCurr, s = 0, v = 0, t = 0, sumExC = 0, sumVrt = 0, totalIvs = 0
            let sumExC3 = 0, sumVrt3 = 0, totalIvs3 = 0
            let sumExC4 = 0, sumVrt4 = 0, totalIvs4 = 0
            let sumExC10 = 0, sumVrt10 = 0, totalIvs10 = 0
            let sumExC1 = 0, sumVrt1 = 0, totalIvs1 = 0
            let finalSumExc = 0, finalVrt = 0, finaltotalIvs = 0
            let indexA1 = [], indexA3 = [], indexA4 = [], indexA10 = []
            let countA1 = 0, countA3 = 0, countA4 = 0, countA10 = 0
            let countExist = 0

            for (const element of rows) {
                if (element.adjtyp == 2 || element.adjtyp == 3) {
                    indexA3.push(rows.indexOf(element))
                }
                if (element.status == 3 && element.adjtyp != 2 && element.adjtyp != 3 && element.adjtyp != 1) {
                    indexA1.push(rows.indexOf(element))
                }
                if (element.status == 4 && element.adjtyp != 2 && element.adjtyp != 3 && element.adjtyp != 1) {
                    indexA10.push(rows.indexOf(element))
                }
                if (element.adjtyp == 1) {
                    indexA4.push(rows.indexOf(element))
                }

                // Remove exist
                for (const keyRows in rows) {
                    if (element.seq == rows[keyRows].seq) {
                        countExist++
                    }
                    if (countExist > 1) {
                        rows.splice(keyRows, 1)
                        break
                    }
                }
                countExist = 0
            }


            for (const row of rows) {
                let form, serial, seq, idt, bname, buyer, baddr, btax, name, curr, sum, vrn, vat, exrt, total, exChange, vrtChange, sumTotal = 0
                moneyEx = objectMoneyEx[row.curr]

                form = row.form
                serial = row.serial
                seq = row.seq
                idt = row.idt
                baddr = row.baddr
                buyer = row.bname
                btax = row.btax
                name = row.name
                curr = row.curr


                sum = row.sum
                vrn = row.vrn
                vat = row.vat
                exrt = row.exrt
                total = row.total

                exChange = row.sum * moneyEx
                vrtChange = row.vat * moneyEx
                sumTotal = exChange + vrtChange

                let r = {
                    form: form,
                    serial: serial,
                    seq: seq,
                    idt: moment(idt).format("DD/MM/YYYY"),
                    buyer: buyer,
                    baddr: baddr,
                    btax: btax,
                    name: name,
                    curr: curr,
                    sum: sum,
                    vrn: vrn,
                    vat: vat,
                    exrt: exrt,
                    total: total,
                    exChange: exChange,
                    vrtChange: vrtChange,
                    sumTotal: sumTotal
                }
                if (row.adjtyp == 2 || row.adjtyp == 3) {
                    lastCurr03 = row.curr
                    if (rows[rows.indexOf(row) + 1]) {
                        nextCurr03 = rows[rows.indexOf(row) + 1].curr
                    }


                    // r.name = (row.adjtyp == 2) ? 'Hóa đơn điều chỉnh tăng' :'Hóa đơn điều chỉnh giảm'
                    r.name = row.note
                    r.moneyEx = moneyEx
                    r.index = ++i3
                    a3.push(r)

                    s3 += row.sum
                    v3 += row.vat
                    t3 += row.total
                    sumExC3 += exChange
                    sumVrt3 += vrtChange
                    totalIvs3 += sumTotal

                    if (rows[indexA3[countA3]] && (!rows[indexA3[countA3 + 1]] || row.curr != rows[indexA3[countA3 + 1]].curr)) {
                        textSum = 'TỔNG'
                        textCurr = row.curr

                        objectSum = {
                            index: textSum,
                            curr: textCurr,
                            sum: s3,
                            vat: v3,
                            total: t3,
                            exChange: sumExC3,
                            vrtChange: sumVrt3,
                            sumTotal: totalIvs3
                        }
                        finalSumExc += sumExC
                        finalVrt += sumVrt
                        r = { ...objectSum }
                        // r.index = ++i1
                        a3.push(r)

                        lastCurr03 = row.curr
                        s3 = 0
                        v3 = 0
                        t3 = 0
                        sumExC3 = 0
                        sumVrt3 = 0
                        totalIvs3 = 0
                    }
                    countA3++
                }
                else if (row.adjtyp == 1) {
                    // if(row.curr=='USD'){
                    //     sumExC03+=exChange
                    //     sumVrt03+=vrtChange
                    //     totalIvs3+=sumTotal
                    //     s3+=row.sum
                    //     v3+=row.vat
                    //     t3+=row.total
                    //     r.index = ++i3
                    //     a3.push(r)
                    // }
                    // else if(row.curr=='VND'){
                    //     sumExC04+=exChange
                    //     sumVrt04+=vrtChange
                    //     totalIvs4+=sumTotal
                    //     s4+=row.sum
                    //     v4+=row.vat 
                    //     t4+=row.total
                    //     r.index=++i4
                    //     a4.push(r)
                    // }
                    lastCurr04 = row.curr
                    if (rows[rows.indexOf(row) + 1]) {
                        nextCurr04 = rows[rows.indexOf(row) + 1].curr
                    }

                    s0 += row.sum
                    v0 += row.vat
                    t0 += row.total
                    r.moneyEx = moneyEx
                    r.index = ++i4
                    a4.push(r)

                    s4 += row.sum
                    v4 += row.vat
                    t4 += row.total
                    sumExC4 += exChange
                    sumVrt4 += vrtChange
                    totalIvs4 += sumTotal

                    if (rows[indexA4[countA4]] && (!rows[indexA4[countA4 + 1]] || row.curr != rows[indexA4[countA4 + 1]].curr)) {
                        textSum = 'TỔNG'
                        textCurr = lastCurr04

                        objectSum = {
                            index: textSum,
                            curr: textCurr,
                            sum: s4,
                            vat: v4,
                            total: t4,
                            exChange: sumExC4,
                            vrtChange: sumVrt4,
                            sumTotal: totalIvs4
                        }
                        finalSumExc += sumExC
                        finalVrt += sumVrt
                        r = { ...objectSum }
                        // r.index = ++i1
                        a4.push(r)


                        s4 = 0
                        v4 = 0
                        t4 = 0
                        sumExC4 = 0
                        sumVrt4 = 0
                        totalIvs4 = 0
                    }
                    countA4++
                }
                else if (row.status == 4) {
                    r.name = 'HỦY'
                    // r = {
                    //     form:form,
                    //     serial:serial,
                    //     seq:seq,
                    //     idt:moment(idt).format("DD/MM/YYYY"),
                    //     buyer:buyer,
                    //     baddr:baddr,
                    //     btax:btax,
                    //     name:name,
                    //     curr:curr,
                    //     sum:sum,
                    //     vrn:vrn,
                    //     vat:vat,
                    //     exrt:exrt,
                    //     exChange:exChange,
                    //     vrtChange:vrtChange,
                    //     sumTotal:sumTotal
                    // }
                    r.index = ++i10
                    a10.push(r)

                    s10 += row.sum
                    v10 += row.vat
                    t10 += row.total
                    sumExC10 += exChange
                    sumVrt10 += vrtChange
                    totalIvs10 += sumTotal

                    if (rows[indexA10[countA10]] && (!rows[indexA10[countA10 + 1]] || row.curr != rows[indexA10[countA10 + 1]].curr)) {
                        textSum = 'TỔNG'
                        textCurr = row.curr

                        objectSum = {
                            index: textSum,
                            curr: textCurr,
                            sum: s10,
                            vat: v10,
                            total: t10,
                            exChange: sumExC10,
                            vrtChange: sumVrt10,
                            sumTotal: totalIvs10
                        }
                        finalSumExc += sumExC
                        finalVrt += sumVrt
                        r = { ...objectSum }
                        // r.index = ++i1
                        a10.push(r)

                        s10 = 0
                        v10 = 0
                        t10 = 0
                        sumExC10 = 0
                        sumVrt10 = 0
                        totalIvs10 = 0
                    }
                    countA10++
                }
                else if (row.status == 3) {
                    // logger4app.debug(row.curr)
                    // if(row.curr=='USD'){	  
                    // }
                    // else if(row.curr='VND'){
                    //     sumExC02+=exChange
                    //     sumVrt02+=vrtChange
                    //     totalIvs2+=sumTotal
                    //     s1+=row.sum


                    //     v1+=row.vat 
                    //     t1+=row.total
                    //     r.index=++i2
                    //     a2.push(r)
                    //     // logger4app.debug(a2)
                    // }

                    if (rows[rows.indexOf(row) + 1]) {
                        nextCurr01 = rows[rows.indexOf(row) + 1].curr
                    }

                    r.moneyEx = moneyEx
                    r.index = ++i1
                    a1.push(r)

                    s1 += row.sum
                    v1 += row.vat
                    t1 += row.total
                    sumExC1 += exChange
                    sumVrt1 += vrtChange
                    totalIvs1 += sumTotal

                    if (rows[indexA1[countA1]] && (!rows[indexA1[countA1 + 1]] || row.curr != rows[indexA1[countA1 + 1]].curr)) { //(rows.length - 2) == rows.indexOf(row)
                        textSum = 'TỔNG'


                        objectSum = {
                            index: textSum,
                            curr: row.curr,
                            sum: s1,
                            vat: v1,
                            total: t1,
                            exChange: sumExC1,
                            vrtChange: sumVrt1,
                            sumTotal: totalIvs1
                        }
                        finalSumExc += sumExC
                        finalVrt += sumVrt
                        r = { ...objectSum }
                        // r.index = ++i1
                        a1.push(r)


                        s1 = 0
                        v1 = 0
                        t1 = 0
                        sumExC1 = 0
                        sumVrt1 = 0
                        totalIvs1 = 0
                    }
                    countA1++
                }
            }

            finaltotalIvs = finalSumExc + finalVrt



            json = {
                month: month,
                year: year,
                a0: a0,
                a10: a10,
                s0: s0,
                v0: v0,
                t0: t0,
                a1: a1,
                s1: s1,
                v1: v1,
                t1: t1,
                a2: a2,
                a3: a3,
                a4: a4,
                s3: s3,
                v3: v3,
                t3: t3,
                s4: s4,
                v4: v4,
                t4: t4,
                vndEx: vndEx,
                usdEx: usdEx,
                sumExC01: sumExC01,
                sumExC02: sumExC02,
                sumVrt01: sumVrt01,
                sumVrt02: sumVrt02,
                totalIvs2: totalIvs2,
                totalIvs1: totalIvs1,
                finalSumExc: finalSumExc,
                finalVrt: finalVrt,
                finaltotalIvs: finaltotalIvs
            }
            // logger4app.debug(json)

            const file = path.join(__dirname, "..", "..", "..", fn)
            const xlsx = fs.readFileSync(file)
            const template = new xlsxtemp(xlsx)
            template.substitute(1, json)
            res.end(template.generate(), "binary")

        } catch (err) {
            next(err)
        }
    },
    detailTran: async (req, res, next) => {
        try {
            const param = req.params
            let sql, binds, result, dataQuery, dataDetail = []
            sql = `select JSON_QUERY(doc,'$.items'), status 'status', c0 'c0' from s_inv where id=@1`
            binds = [param.id]
            result = await dbs.query(sql, binds)
            dataQuery = result.recordset
            dataDetail = JSON.parse(result.recordset[0][Object.keys(result.recordset[0])[0]])
            for (let row of dataDetail) {
                if ((result.recordset[0].status == 2 || result.recordset[0].status == 3)) row.statusGD = 1
                // row.status = result.recordset[0].status
                row.trantype = result.recordset[0].c0
            }

            res.json({
                data: dataDetail
            })
        }
        catch (err) {
            next(err)
        }
    },
    cancelSGR: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, body = req.body
            let doc, arr = body.arr, status = 4, cdt
            for (let item of arr) {
                logger4app.debug(item);
                await chkou(item.id, token)
                doc = await rbi(item.id)
                const cancel = { typ: 3, ref: item.id, rdt: moment().format(dtf) }
                if (doc.adj && doc.pid && doc.adj.typ == 2) await dbs.query(`update s_inv set cid=null,cde=null where id=@1`, [doc.pid])
                if (ENT == 'sgr') {
                    cdt = doc.idt
                } else cdt = moment(new Date()).format(dtf)
                await dbs.query(`update s_inv set doc=JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(doc,'$.cdt',@1),'$.canceller',@2),'$.cancel',JSON_QUERY(@3)) where id=@4 and stax=@5`, [cdt, token.uid, JSON.stringify(cancel), item.id, taxc])
                let result = await dbs.query(`update s_inv set doc=JSON_MODIFY(doc,'$.status',@1) where id=@2 and stax=@3`, [status, item.id, taxc])
                if (result.rowsAffected[0] > 0) {
                    if (useJobSendmail) {
                        if (((status == 4) && !util.isEmpty(doc.bmail)) || ((status == 4) && !util.isEmpty(doc.btel))) {
                            doc.status = 4
                            await Service.insertDocTempJob(doc.id, doc, ``)
                        }
                    } else {
                        if ((status == 4) && !util.isEmpty(doc.bmail)) {
                            doc.status = 4
                            if (!util.isEmpty(doc.bmail)) {
                                let mailstatus = await email.rsmqmail(doc)
                                if (mailstatus) await Service.updateSendMailStatus(doc)
                            }
                        }
                        if ((status == 4) && !util.isEmpty(doc.btel)) {
                            doc.status = 4
                            if (!util.isEmpty(doc.btel)) {
                                let smsstatus = await sms.smssend(doc)
                                if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                            }
                        }
                    }
                    const sSysLogs = { fnc_id: 'inv_istatus', src: config.SRC_LOGGING_DEFAULT, dtl: `Chuyá»ƒn Ä‘á»•i tráº¡ng thÃ¡i hÃ³a Ä‘Æ¡n id ${item.id}`, msg_id: item.id, doc: JSON.stringify(doc) };
                    logging.ins(req, sSysLogs, next)
                }
            }
            res.json(1)
        } catch (err) {
            next(err)
        }
    },

    depINV: async (req, res, next) => {
        try {
            const token = SEC.decode(req), uid = token.uid
            let sql, binds, result, data
            sql = `select deptid id, deptid value from s_dept where deptid in (select deptid from s_deptusr where usrid = @1) `
            binds = [uid]
            result = await dbs.query(sql, binds)
            data = result.recordset
            res.json(data)

        } catch (err) {
            next(err)
        }
    },
    delAll: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body
            let sql, result, binds, rows, sqlinv, resultinv, query = { filter: JSON.stringify(body) }, reqtmp = { query: query, token: token }, iwh = await Service.iwh(reqtmp, false, true), countseq = 0
            //Chi cho phép xóa các hóa đơn với trạng thai nhất định
            if (!['1'].includes(body.status)) throw new Error(`Chỉ được xóa các hóa đơn với các trạng thái được phép xóa \n (Only invoices with statuses allowed to be deleted can be deleted)`)
            //Check so ban ghi truoc khi thuc hien xoa
            const sqlcount = `select count(*) total from s_inv ${iwh.where}`
            const resultcount = await dbs.query(sqlcount, iwh.binds)
            if (resultcount.recordset[0].total > config.MAX_ROW_DELETE_ALL) throw new Error(`Số lượng bản ghi cần xóa vượt quá giới hạn cho phép \n (The number of records to be deleted exceeds the allowed limit)`)
            //Lấy các hóa đơn cần xóa
            sqlinv = `select pid, id from s_inv ${iwh.where}`
            resultinv = await dbs.query(sqlinv, iwh.binds)
            rows = resultinv.recordset
            for (let inv of rows) {
                await chkou(inv.id, token)
                //Lay du lieu doc truoc khi xoa de luu log
                let doclog = await rbi(inv.id)
                if (config.ent == "scb") {
                    for (const item of doclog.items) {
                        if (item.tranID) {
                            await dbs.query(`update s_trans set status = @1, inv_id = null, inv_date = null where tranID = @2`, [item.statusGD, item.tranID])
                        }
                    }
                }
                await dbs.query(`delete from s_inv where id=@1`, [inv.id])
                await Service.delrefno(doclog)
                await dbs.query(`update  s_inv set cid=null,cde=null where id=@1 and cid=@2`, [inv.pid, inv.id])
                countseq++
                if (config.ent == 'mzh') {
                    await dbs.query(`update  s_trans set status=stattus_temp,inv_id=inv_id_tmp,exrt=null,inv_date=null,ma_nv=null,trancol=null where inv_id=@1`, [inv.id])
                }
                const sSysLogs = { fnc_id: 'inv_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa hóa đơn id ${inv.id}`, msg_id: inv.id, doc: JSON.stringify(doclog) };
                logging.ins(req, sSysLogs, next)
            }

            res.json({ countseq: countseq })
        } catch (err) {
            next(err)
        }
    }
}
module.exports = Service