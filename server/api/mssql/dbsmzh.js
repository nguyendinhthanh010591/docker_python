'use strict'
const sql = require("mssql")
const config = require("../config")
const logger4app = require("../logger4app")
let pool
const Service = {
    close: async () => {
        try {
            if (pool) {
                await pool.close()
                pool = null
            }
        } catch (err) {
            pool = null
            logger4app.error("closePool", err)
        }
    },
    getConn: async () => {
        try {
            if (pool) return pool
            pool = await sql.connect(config.poolConfig)
            pool.on("error", async err => {
                logger4app.error("mssql error", err)
                await Service.close()
            })
            return pool
        } catch (err) {
            pool = null
            throw err
        }
    },
    query: (sql, binds = []) => {
        return new Promise(async (resolve, reject) => {
            try {
                const conn = await Service.getConn()
                const request = await conn.request()
                let i = 1
                for (const bind of binds) {
                    request.input((i++).toString(), bind)
                }
                const result = await request.query(sql)
                resolve(result)
            }
            catch (err) {
                reject(err)
            }
        })
    },
    queries: (sql, arr) => {
        return new Promise(async (resolve, reject) => {
            try {
                const conn = await Service.getConn()
                for (const binds of arr) {
                    const request = await conn.request()
                    let i = 1
                    for (const bind of binds) {
                        request.input((i++).toString(), bind)
                    }
                    await request.query(sql)
                }
                resolve("ok")
            }
            catch (err) {
                reject(err)
            }
        })
    }
}
module.exports = Service
