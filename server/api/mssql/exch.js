"use strict"
const moment = require("moment")
const dbs = require("./dbs")
const config = require("../config")
const logger4app = require("../logger4app")
const logging = require("../logging")
const Service = {
    get: async (req, res, next) => {
        try {
            const query = req.query, filter = JSON.parse(query.filter)
            const sort = query.sort, start = query.start ? query.start : 0, count = query.count ? query.count : 10
            let where = ` where dt between @1 and @2`, order, sql, result, ret, i = 2
            let binds = [new Date(filter.fd), new Date(moment(filter.td).endOf("day"))]
            Object.keys(filter).forEach(key => {
                if (key == "curr") {
                    where += ` and cur=@3`
                    binds.push(filter[key])
                    i = 3
                }
            })
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = ` order by ${key} ${sort[key]}`
                })
            }
            else order = " order by dt desc"
            sql = `select id,cur,val,dt from s_ex ${where} ${order} offset @${++i} rows fetch next @${++i} rows only`
            binds.push(parseInt(start), parseInt(count))
            result = await dbs.query(sql, binds)
            ret = { data: result.recordset, pos: start }
            if (start == 0) {
                sql = `select count(*) total from s_ex ${where}`
                result = await dbs.query(sql, binds)
                ret.total_count = result.recordset[0].total
            }
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const body = req.body, operation = body.webix_operation
            let binds, sql
            switch (operation) {
                case "update":
                    sql = `update s_ex set val=@1 where id=@2`
                    binds = [body.val, body.id]
                    break
                case "insert":
                    sql = `insert into s_ex (cur,val,dt) values (@1,@2,@3)`
                    binds = [body.cur, body.val, new Date(body.dt)]
                    break
                case "delete":
                    sql = `delete from s_ex where id=@1`
                    binds = [body.id]
                    break
                default:
                    throw new Error(`${operation} là hoạt động không hợp lệ \n (${operation} is invalid operation)`)
            }
            const result = await dbs.query(sql, binds)
            const sSysLogs = { fnc_id: 'exch_post', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật thông tin tỷ giá ngoại tệ ${body.cur} (${operation})`, msg_id: body.id, doc: JSON.stringify(body) };
            logging.ins(req, sSysLogs, next)
            res.json(result.rowsAffected[0])
        }
        catch (err) {
            next(err)
        }
    },
    rate: async (req, res, next) => {
        try {
            const query = req.query, cur = query.cur, dt = new Date()
            const sql = `select val from s_ex where cur=@1 and dt=(select max(dt) from s_ex where cur=@1 and dt<=@2)`
            const result = await dbs.query(sql, [cur, dt])
            const rows = result.recordset
            const val = rows.length>0 ? rows[0].val : 0
            res.json(val)
        }
        catch (err) {
            next(err)
        }
    },
    sync: async (rows,date) => {
        try {
            let sql, binds
            sql = `delete from s_ex where CONVERT(DATETIME, CONVERT(DATE, dt)) = @1`

            binds = [date]
            await dbs.query(sql, binds)
            
            for (let row of rows) {
                sql = `insert into s_ex (cur,val,dt) values (@1,@2,@3)`
                logger4app.debug('ty gia dong :'+ row.CCY + '_'+row.TTM)
                binds = [row.CCY, row.TTM, date]
                await dbs.query(sql, binds)
            }
        }
        catch (err) {
            logger4app.debug(err.message)
           // next(err)
        }
    }
}
module.exports = Service   