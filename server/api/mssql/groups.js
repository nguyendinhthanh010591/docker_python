"use strict"
const util = require("../util")
const sec = require("../sec")
const dbs = require("./dbs")
const path = require("path")
const config = require("../config")
const logger4app = require("../logger4app")
const logging = require("../logging")
const fs = require("fs")
const xlsxtemp = require("xlsx-template")
const Json2csvParser = require("json2csv").Parser;
const Service = {
  add: async (req, res, next) => {
    let conn
    try {
     
      const body = req.body, name = body.name, des = body.des,status=body.status
      let sql, result,binds=[]
      binds.push([name,des,status])
      sql = `insert into s_group(name,des,status) values (@1,@2,@3)`
      result = await dbs.queries(sql, binds)
      let result_id = await dbs.query(`select id from s_group where name=@1`, [body.name])
      let id_ins, rows_ins = result_id.recordset
      if(rows_ins.length >0){
        id_ins=rows_ins[0].id
        body.id=id_ins
      } 
      const sSysLogs = { fnc_id: 'group_insert', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm thông tin nhóm`, msg_id: id_ins,doc: JSON.stringify(body) };
      logging.ins(req, sSysLogs, next)
      res.send(`Nhóm thêm thành công \n (Successfully added group)`)
    }
    catch (err) {
    
      next(err)
    }
  
},
update: async (req, res, next) => {
  let conn
  try {
   
    const body = req.body,id = body.id, name = body.name, des = body.des,status=body.status
    let sql, result,binds=[]
    binds.push([name,des,status,id,])
    sql = `update s_group set name=@1,des=@2,status=@3 where id=@4`
    result = await dbs.queries(sql, binds)
    const sSysLogs = { fnc_id: 'group_update', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật thông tin nhóm`, msg_id: body.id, doc: JSON.stringify(body) };
    logging.ins(req, sSysLogs, next)
    res.send(`Cập nhật thành công \n (Successfully updated)`)
  }
  catch (err) {
   
    next(err)
  }
  
},
  member: async (req, res, next) => {
    let conn
    try {
     
      const body = req.body, uid = body.uid, arr = body.arr
      let sql, result
      sql = `delete from s_group_role where group_id=@1`
      result = await dbs.query(sql, [uid])
      if (arr.length > 0) {
        let binds = []
        for (const rid of arr) {
          binds.push([uid, rid])
        }
        sql = `insert into s_group_role(group_id,role_id) values (@1,@2)`
        result = await dbs.queries(sql, binds)
      }
      const sSysLogs = { fnc_id: 'group_rolemember', src: config.SRC_LOGGING_DEFAULT, dtl: `Phân quyền theo nhóm`, msg_id: "", doc: JSON.stringify(body) };
      logging.ins(req, sSysLogs, next)
      res.json(1)
    }
    catch (err) {
     
      next(err)
    }
  
  },
  manager: async (req, res, next) => {
    // let conn
    // try {
    //   conn = await dbs.getConn()
    //   const body = req.body, uid = body.uid, arr = body.arr
    //   let sql, result
    //   sql = `delete from s_manager where user_id=@1`
    //   result = await conn.execute(sql, [uid], opt)
    //   if (arr.length > 0) {
    //     let binds = []
    //     for (const oid of arr) {
    //       binds.push([uid, oid])
    //     }
    //     sql = `insert into s_manager values (@1,@2)`
    //     result = await conn.executeMany(sql, binds, opt)
    //   }
    //   await conn.commit()
    //   res.json(result)
    // }
    // catch (err) {
    //   await conn.rollback()
    //   next(err)
    // }
    // finally {
    //   await dbs.closeConn(conn)
    // }
  },
  role: async (req, res, next) => {
    try {
      const uid = req.params.uid
      const sql = `select x.id "id",x.name "name",x.pid "pid",x.sel "checked" from (select a.id,a.name,a.pid,0 sel from s_role a left join s_group_role b on a.id = b.role_id and b.group_id=@1 where b.role_id is null and a.active = 1 union select a.id,a.name,a.pid,1 sel from s_role a inner join s_group_role b on a.id = b.role_id and b.group_id=@1 and a.active = 1) x order by x.id`
      const result = await dbs.query(sql, [uid])
      const rows = result.recordset
      let arr = [], obj
      for (const row of rows) {
        if (!row.pid) {
          row.open = true
          arr.push(row)
        }
        else {
          obj = util.findDFS(arr, row.pid)
          if (obj) {
            if (!obj.hasOwnProperty('data')) obj.data = []
            obj.data.push(row)
          }
        }
      }
      res.json(arr)
      // res.json(result.recordset)
    }
    catch (err) {
      next(err)
    }
  },
  roleAll: async (req, res, next) => {
    try {
      const uid = req.params.uid
      const sql = `select id "id",pid "pid",name "name" from s_role where active = 1 order by id`
      const result = await dbs.query(sql, [])
      const rows = result.recordset
      let arr = [], obj
      for (const row of rows) {
        if (!row.pid) {
          row.open = true
          arr.push(row)
        }
        else {
          obj = util.findDFS(arr, row.pid)
          if (obj) {
            if (!obj.hasOwnProperty('data')) obj.data = []
            obj.data.push(row)
          }
        }
      }
      res.json(arr)
      // res.json(result.recordset)
    }
    catch (err) {
      next(err)
    }
  },
  ou: async (req, res, next) => {
    // try {
    //   const token = sec.decode(req), ou = token.ou, uid = req.params.uid
    //   const sou = "(select id,pid,taxc,name from s_ou where taxc is not null start with id=:ou connect by prior id=pid)"
    //   const sql = `select x.id "id",x.pid "pid",x.taxc "taxc",x.name "name",x.sel "checked" from (select a.id,a.pid,a.taxc,a.name,0 sel from ${sou} a left join s_manager b on a.taxc=b.taxc and b.user_id=:user_id where b.taxc is null union select a.id,a.pid,a.taxc,a.name,1 sel from ${sou} a inner join s_manager b on a.taxc=b.taxc and b.user_id=:user_id) x start with x.id=:ou connect by prior x.id=x.pid`
    //   const result = await dbs.query(sql, [ou, uid])
    //   const rows = result.rows
    //   let arr = [], obj
    //   for (const row of rows) {
    //     if (row.id == ou) {
    //       row.open = true
    //       arr.push(row)
    //     }
    //     else {
    //       obj = util.findDFS(arr, row.pid)
    //       if (obj) {
    //         if (!obj.hasOwnProperty('data')) obj.data = []
    //         obj.data.push(row)
    //       }
    //     }
    //   }
    //   res.json(arr)
    // }
    // catch (err) {
    //   next(err)
    // }
  },
  get: async (req, res, next) => {
    try {
      const token = sec.decode(req), query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 10, sort = query.sort
      let where = "where 1=1 ", order = "", sql, result, ret, binds =[], i=1
      if (query.filter) {
        let filter = JSON.parse(query.filter), val
        Object.keys(filter).forEach((key) => {
          val = filter[key]
          if (val) {
            if (key == "name") {
              where += ` and upper(${key}) like @${i++}`
              binds.push(`%${val.toUpperCase()}%`)
            }
            else {
              where += ` and ${key}=@${i++}`
              binds.push(val)
            }
          }
        })
      }
      if (sort) {
        Object.keys(sort).forEach((key) => {
          order = `order by ${key}  ${sort[key]}`
        })
      }else order = `order by id`
      sql = `select id "id",name "name",status "status",des "des" from s_group ${where} ${order} offset @${i++} rows fetch next @${i++} rows only`
      binds.push(parseInt(start), parseInt(count))
      result = await dbs.query(sql, binds)
      ret = { data: result.recordset, pos: start }
      if (start == 0) {
        sql = `select count(*) "total" from s_group ${where}`
        result = await dbs.query(sql, binds)
        ret.total_count = result.recordset[0].total
      }
      return res.json(ret)
    }
    catch (err) {
      next(err)
    }
  },
  getgrpbyusser: async (uid) => {
    try {
      let sql = `SELECT g.[NAME] "name" 
      FROM s_group_user gu, s_group g 
      WHERE g.ID = gu.GROUP_ID 
      AND gu.[USER_ID] = @1 
      ORDER BY g.[NAME]`
      let result, binds =[uid]
      result = await dbs.query(sql, binds)
      return result.recordset
    }
    catch (err) {
      throw err
    }
  },
  post: async (req, res, next) => {
    // try {
    //   const body = req.body, operation = body.webix_operation
    //   let sql, result, binds
    //   if (operation == "update") {
    //     sql = `update s_user set ou=:ou,name=:name,pos=:pos,code=:code,mail=:mail where id=:id`
    //     binds = [body.ou, body.name, body.pos, body.code, body.mail, body.id]
    //   }
    //   result = await dbs.query(sql, binds)
    //   res.json(result)
    // }
    // catch (err) {
    //   next(err)
    // }
  },
  ubr: async (req, res, next) => {
    // try {
    //   const token = sec.decode(req), rid = req.params.rid
    //   const where = "and a.ou in (select id from s_ou start with id=:ou connect by prior id=pid)"
    //   const sql = `select x.id "acc",x.name "name",x.sel "sel",x.mail "mail",x.ou "ou",x.uc "uc",x.pos "pos" from (select a.id,a.name,0 sel,a.mail,a.ou,a.uc,a.pos from s_user a left join s_member b on a.id = b.user_id and b.role_id=:role_id where b.user_id is null ${where} union select a.id,a.name,1 sel,a.mail,a.ou,a.uc,a.pos from s_user a inner join s_member b on a.id = b.user_id and b.role_id=:role_id ${where}) x order by x.id`
    //   const result = await dbs.query(sql, [rid, token.ou])
    //   res.json(result.rows)
    // } catch (err) {
    //   next(err)
    // }
  },
  gir: async (req, res, next) => {
    try {
      const token = sec.decode(req), rid = req.params.rid
      
      const sql = `select x.id "id", x.name "name",x.sel "sel",x.des "des",x.status "status" from (select a.id,a.name,a.des,a.status,0 sel from s_group a left join s_group_role b on a.id = b.group_id and b.role_id=@1 where b.group_id is null  union select a.id,a.name,a.des,a.status,1 sel from s_group a inner join s_group_role b on a.id = b.group_id and b.role_id=@2 ) x order by x.id`
      const result = await dbs.query(sql, [rid, rid])
      res.json(result.recordset)
    } catch (err) {
      next(err)
    }
  },
  mbr: async (req, res, next) => {
    let conn
    try {
     
      const body = req.body, rid = body.rid, groups = body.groups
      let sql, result
      sql = `delete from s_group_role where role_id=@1`
      result = await dbs.query(sql, [rid])
      if (groups.length > 0) {
        let binds = []
        for (const uid of groups) {
          binds.push([rid, uid])
        }
        sql = `insert into s_group_role (role_id, group_id) values (@1,@2)`
        result = await dbs.queries(sql, binds)
      }
      const sSysLogs = { fnc_id: 'group_del_mbr', src: config.SRC_LOGGING_DEFAULT, dtl: 'Phân quyền nhóm', msg_id: "", doc: JSON.stringify(body) };
      logging.ins(req, sSysLogs, next)
      res.json(1)
    } catch (err) {
    
      next(err)
    }
    
  },
  disable: async (req, res, next) => {
    try {
      const uid = req.params.uid
      await dbs.query(`update s_group set status=@1 where id=@2`, [2, uid])
      const sSysLogs = { fnc_id: 'group_disable', src: config.SRC_LOGGING_DEFAULT, dtl: 'Hủy nhóm NSD', msg_id: uid, doc: JSON.stringify({id: uid, status: 2})};
      logging.ins(req, sSysLogs, next)
      res.send(`Đã hủy bỏ nhóm \n Group is cancelled`)
    } catch (err) {
      next(err)
    }
  },
  enable: async (req, res, next) => {
    try {
      const uid = req.params.uid
      await dbs.query(`update s_group set status=@1 where id=@2`, [1, uid])
      const sSysLogs = { fnc_id: 'group_enable', src: config.SRC_LOGGING_DEFAULT, dtl: 'Kích hoạt nhóm NSD', msg_id: uid, doc: JSON.stringify({id: uid, status: 1})};
      logging.ins(req, sSysLogs, next)
      res.send(`Nhóm đã được kích hoạt \n Group is active`)
    } catch (err) {
      next(err)
    }
  },
  ins: async (req, obj) => {
    // try {
    //   let ou = obj.ou
    //   if (!ou) {
    //     const token = sec.decode(req)
    //     ou = token.ou
    //   }
    //   const result = await dbs.query(`insert into s_user(id,code,name,ou,mail,pos) values (:id,:code,:name,:ou,:mail,:pos)`, { id: obj.id, code: obj.code, name: obj.name, ou: ou, mail: obj.mail, pos: obj.pos })
    //   return result.rowsAffected
    // } catch (err) {
    //   throw err
    // }
  },
  xls: async (req, res, next) => {
    try {
      const token = sec.decode(req), query = req.query, sort = query.sort
      let where = "where 1=1 ", order = "", sql, result, ret, binds = [], i = 1, rows, json, colhds1 = [], rs = []
      sql = `select scbname "name" from s_role where active = 1`
      result = await dbs.query(sql, binds)
      const cols = result.recordset
      for (const col of cols) {
        let clbl = col.name
        colhds1.push(clbl)
      }
      if (query.filter) {
        let filter = JSON.parse(query.filter), val
        Object.keys(filter).forEach((key) => {
          val = filter[key]
          if (val) {
            if (key == "name") {
              where += ` and upper(${key}) like @${i++}`
              binds.push(`%${val.toUpperCase()}%`)
            }
            else {
              where += ` and ${key}=@${i++}`
              binds.push(val)
            }
          }
        })
      }
      if (sort) {
        Object.keys(sort).forEach((key) => {
          order = `order by ${key}  ${sort[key]}`
        })
      } else order = `order by id`
      sql = `select id "id",name "name" from s_group ${where} ${order} `
      result = await dbs.query(sql, binds)
      rows = result.recordset

      for (const row of rows) {
        sql = `select scbname "name" from s_role where active = 1 id in (select role_id from s_group_role where group_id=@1)`
        result = await dbs.query(sql, [row.id])
        let rowrs = result.recordset
        let rsc = []
        rsc.push(row.name)
        for (const colhd of colhds1) {
          let b = false
          for (const rowr of rowrs) {
            if (rowr.name == colhd) {
              b = true
              break
            }
          }
          if (b) { rsc.push("Y") }
          else rsc.push("N")
        }
        rs.push(rsc)
      }


      let fn = "temp/Security_Matrix_SCB.xlsx"

      json = { extsh: rs, colhds: colhds1 }
      const file = path.join(__dirname, "..", "..", "..", fn)
      const xlsx = fs.readFileSync(file)
      const template = new xlsxtemp(xlsx)
      template.substitute(1, json)
      res.end(template.generate(), "binary")
    }
    catch (err) {
      logger4app.debug("excel" + err)
      next(err)
    }
  },
  soc: async (req, res, next) => {
    try {
      const query = req.query
      let binds, result, records
      //chỉ lấy những trạng thái là hiệu lực
      let where = `where 1 = 1`
      binds = []
      let i = 1
      if(query.name){
        where+=` and s.name like @${i++}`
        binds.push(`%${query.name}%`)
      }
      if(query.status != "*"){
        where += ` and s.status = @${i++}`
        binds.push(query.status)
      }
      // let sql = `select s.name "UserGroup_Name", group_id "UserGroup", role_id "Function1ID", k.name "Function1ID_Name", 'VNEINV' System_code  from s_group s inner join s_group_role h on s.id=h.group_id inner join s_role k on h.role_id=k.id ${where} group by s.name,group_id,role_id,k.name order by group_id`
      let sql = `select 'VN-EVAT' System_code, s.name "UserGroup_Name", s.des "DES", rl.name "Function1ID_Name", rl.name "Function2ID_Name" from s_group s inner join s_group_role grl on s.id = grl.group_id inner join s_role rl on grl.role_id = rl.id and rl.active = 1 ${where} group by s.name, s.id,grl.ROLE_ID, rl.name,s.des`
      result = await dbs.query(sql, binds);
  
      records = result.recordset
      if (records == undefined || records.length == 0 ) {
         throw new Error ("Group rỗng không có dữ liệu")
      }
      const jsonData = JSON.parse(JSON.stringify(records));
      const json2csvParser = new Json2csvParser({ header: false, default: '', quote: '', encoding: 'utf8' });
      const csv = json2csvParser.parse(jsonData);
      res.end(csv, 'utf8')
    } catch (err) {
      logger4app.debug(err);
    }
  }
}
module.exports = Service 