"use strict"
const config = require("./config")
const logger4app = require("./logger4app")
const dbtype = config.dbtype
const dbs = require(`./${dbtype}/dbs`)
const dbCache = config.dbCache
const decrypt = require("./encrypt")
let redis
/*
const Redis = require("redis")
const asyncRedis = require("async-redis")
const client = Redis.createClient(config.redisConfig)
const redis = asyncRedis.decorate(client)

const Redis = require("ioredis")
const redis = new Redis(config.redisConfig)
redis.on("connect", () => { console.info("Redis connected") })
redis.on("ready", () => { console.info("Redis ready") })
redis.on("error", (err) => { logger4app.error("Redis error", err) })
redis.on("close", () => { console.info("Redis closed") })
redis.on("reconnecting", () => { console.info("Redis reconnecting") })
redis.on("end", () => { console.info("Redis end") })
*/
const sql_exists = {
    sql_exists_mssql: `SELECT count(*) "rcount" FROM s_listvalues where keyname=@1`,
    sql_exists_mysql: `SELECT count(*) "rcount" FROM s_listvalues where keyname=?`,
    sql_exists_orcl: `SELECT count(*) "rcount" FROM s_listvalues where keyname=:1`,
    sql_exists_pgsql: `SELECT count(*) "rcount" FROM s_listvalues where keyname=$1`,
}

const sql_get = {
    sql_get_mssql: `SELECT keyvalue "keyvalue" FROM s_listvalues where keyname=@1`,
    sql_get_mysql: `SELECT keyvalue keyvalue FROM s_listvalues where keyname=?`,
    sql_get_orcl: `SELECT keyvalue "keyvalue" FROM s_listvalues where keyname=:1`,
    sql_get_pgsql: `SELECT keyvalue keyvalue FROM s_listvalues where keyname=$1`
}

const sql_set_ins = {
    sql_set_ins_mssql: `INSERT INTO s_listvalues (keyname, keyvalue, keytype) VALUES(@1, @2, 'String')`,
    sql_set_ins_mysql: `INSERT INTO s_listvalues (keyname, keyvalue, keytype) VALUES(?, ?, 'String')`,
    sql_set_ins_orcl: `INSERT INTO s_listvalues (keyname, keyvalue, keytype) VALUES(:1, :2, 'String')`,
    sql_set_ins_pgsql: `INSERT INTO s_listvalues (keyname, keyvalue, keytype) VALUES($1, $2, 'String')`,
}

const sql_set_upd = {
    sql_set_upd_mssql: `UPDATE s_listvalues SET keyvalue=@1, keytype='String' WHERE keyname=@2`,
    sql_set_upd_mysql: `UPDATE s_listvalues SET keyvalue=?, keytype='String' WHERE keyname=?`,
    sql_set_upd_orcl: `UPDATE s_listvalues SET keyvalue=:1, keytype='String' WHERE keyname=:2`,
    sql_set_upd_pgsql: `UPDATE s_listvalues SET keyvalue=$1, keytype='String' WHERE keyname=$2`,
}

const sql_del = {
    sql_del_mssql: `DELETE FROM s_listvalues WHERE keyname=@1`,
    sql_del_mysql: `DELETE FROM s_listvalues WHERE keyname=?`,
    sql_del_orcl: `DELETE FROM s_listvalues WHERE keyname=:1`,
    sql_del_pgsql: `DELETE FROM s_listvalues WHERE keyname=$1`,
}
const sql_keys = {
    sql_keys_mssql: `SELECT keyname "keyname",CHARINDEX(@1, keyname) FROM s_listvalues WHERE CHARINDEX(@1, keyname) = 1`,
    sql_keys_mysql: `SELECT keyname "keyname", locate(?,keyname) FROM s_listvalues where locate(?,keyname) = 1`,
    sql_keys_orcl: `SELECT keyname "keyname",INSTR(keyname,:1) FROM s_listvalues where INSTR(keyname,:1) = 1 `,
    sql_keys_pgsql: `SELECT keyname "keyname",strpos(keyname,$1)   FROM s_listvalues where strpos(keyname,$1)  =1`
}
const execsqlselect = (sql, binds) => {
    return new Promise(async (resolve, reject) => {
        try {
            const result = await dbs.query(sql, binds)
            let rows
            switch (dbtype) {
                case "mssql":
                    rows = result.recordset
                    break
                case "mysql":
                    rows = result[0]
                    break
                case "orcl":
                    rows = result.rows
                    break
                case "pgsql":
                    rows = result.rows
                    break
            }
            resolve(rows)
        } catch (err) {
            reject(err)
        }
    })
}

const Service = {
    loadredis: () => {
        const Redis = require("ioredis")
        let redisConfig = JSON.parse(JSON.stringify(config.redisConfig))
        redisConfig.password = decrypt.decrypt(redisConfig.password)
        redis = new Redis(redisConfig)
        return redis
    },
    get: async (keyname) => {
        let retvalue
        try {
            if (dbCache) {
                const rows = await execsqlselect(sql_get[`sql_get_${dbtype}`], [keyname])
                if(rows.length != 0){
                    retvalue = rows[0].keyvalue
                }
            } else {
                Service.loadredis()
                retvalue = await redis.get(keyname)
            }
            return retvalue
        } catch (err) {
            throw err
        }
    },
    apiget: async (req, res, next) => {
        try {
            let json = req.body, keyname = json.keyname
            let retvalue = await Service.get(keyname)
            res.json({ result: retvalue })
        } catch (err) {
            next(err)
        }
    },
    set: async (keyname, keyvalue) => {
        try {
            if (dbCache) {
                const result = await dbs.query(sql_set_upd[`sql_set_upd_${dbtype}`], [keyvalue, keyname])
                let vexists = await Service.exists(keyname)
                if (!vexists) await dbs.query(sql_set_ins[`sql_set_ins_${dbtype}`], [keyname, keyvalue])
            } else {
                Service.loadredis()
                await redis.set(keyname, keyvalue)
            }
        } catch (err) {
            throw err
        }
    },
    apiset: async (req, res, next) => {
        try {
            let json = req.body, keyname = json.keyname, keyvalue = json.keyvalue
            await Service.set(keyname, keyvalue)
            res.json({ result: "1" })
        } catch (err) {
            next(err)
        }
    },
    incr: async (keyname) => {
        try {
            let keyvalue = await Service.get(keyname)
            keyvalue = Number(keyvalue) 
            ++keyvalue
            await Service.set(keyname, keyvalue)
            return keyvalue
        } catch (err) {
            throw err
        }
    },
    apiincr: async (req, res, next) => {
        try {
            let json = req.body, keyname = json.keyname
            let retvalue = await Service.incr(keyname)
            res.json({ result: retvalue })
        } catch (err) {
            next(err)
        }
    },
    del: async (keyname) => {
        try {
            let vkeyname
            if (typeof keyname === "string"){
                vkeyname = [keyname]
            }
            if (typeof keyname === "object"){
                vkeyname = keyname
            }
            for (let vkey of vkeyname) {
                if (dbCache) {
                    const result = await dbs.query(sql_del[`sql_del_${dbtype}`], [vkey])
                } else {
                    Service.loadredis()
                    await redis.del(vkey)
                }
            }
        } catch (err) {
            next(err)
        }
    },
    exists: async (keyname) => {
        let retvalue = false
        try {
            if (dbCache) {
                const rows = await execsqlselect(sql_exists[`sql_exists_${dbtype}`], [keyname])
                retvalue = rows[0].rcount
            } else {
                Service.loadredis()
                retvalue = await redis.exists(keyname)
            }
            return retvalue
        } catch (err) {
            throw err
        }
    },
    lpush: async (keyname,keyvalue) => {
        try {
            if (dbCache) {
                const result = await dbs.query(sql_set_upd[`sql_set_upd_${dbtype}`], [keyvalue, keyname])
                let vexists = await Service.exists(keyname)
                if (!vexists) await dbs.query(sql_set_ins[`sql_set_ins_${dbtype}`], [keyname, keyvalue])
            } else {
                Service.loadredis()
                await redis.set(keyname, keyvalue)
            }
        } catch (err) {
            next(err)
        }
    },
    rpop: async (keyname) => {
        let retvalue
        try {
            if (dbCache) {
                const rows = await execsqlselect(sql_get[`sql_get_${dbtype}`], [keyname])
                if(rows.length != 0){
                    retvalue = rows[0].keyvalue
                    await dbs.query(sql_del[`sql_del_${dbtype}`], [keyname])
                }
            } else {
                Service.loadredis()
                retvalue = await redis.rpop(keyname)
            }
            return retvalue
        } catch (err) {
            throw err
        }
    },
    keys: async(keyname)=>{
        let retvalue = []
        try {
            if (dbCache) {
                let bind 
                if (dbtype=='mysql') bind = [keyname,keyname] 
                else bind = [keyname]
                const rows = await execsqlselect(sql_keys[`sql_keys_${dbtype}`], bind)
                if(rows.length != 0){
                    for(let row of rows){
                        retvalue.push(row.keyname)
                    }
                    
                }
            } else {
                Service.loadredis()
                retvalue = await redis.keys(keyvalue+`*`)
            }
            return retvalue
        } catch (err) {
            throw err
        }
    }
}

if (!dbCache) {
    const Redis = require("ioredis")
    let redisConfig = JSON.parse(JSON.stringify(config.redisConfig))
    if (redisConfig.password) redisConfig.password = decrypt.decrypt(redisConfig.password)
    redis = new Redis(redisConfig)
    module.exports = redis
} else {
    module.exports = Service
}

//module.exports = Service
//module.exports = redis
