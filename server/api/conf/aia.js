module.exports = {
    ent: "aia",
    dbtype: "mssql",
    xls_idt: 0,
    serial_grant: 0,
    serial_usr_grant: 0,
    test_sms_fpt: 1,
    keyvault_config: {
        uri: "https://aiavndev-einvoice-vault.vault.azure.net/",
        client_id: "147324a3-2a30-4b46-8a67-c0dda1210b63",
        client_key: "-2L-6S4AiKA29wgCC4Gwz2LIG-n5T~ZpEP",
        tenant_id: "2ea32b59-7646-4673-9710-921225fa9c61"
    },
    sms_config:{
        tokenEndpoint: "https://api.uat.aia-apps.biz/sso/authenticate",
        sendSmsEndpoint: "https://apis.uat.aia-apps.com/sms/api/messages/send",
        allowSmsFilter: true,
    },
    ou_grant: 0,
    invoice_seq:1, // có chức năng cấp số hay không
    ldapPrivate: 1,//1-rieng 0-chung 
    captchaType: 2, // 0-ko dung 1-offline 2-online(Google)  // Chinh ca o file ext.js client
    mailbcc: "hunglq19@fpt.com.vn",
    mail: "FPT.einvoice.OnPrem@fpt.com.vn",
    smtpConfig: { host: "mail.fpt.com.vn", port: 587, secure: false, auth: { user: "FPT.einvoice.OnPrem@fpt.com.vn", pass: "5db0fd40a93fd5887aaff5b407bd62c6982cc9989c60ae76f09dfa0882d5cfa6" } }, //465(SSL),587(TLS)
    ldap: "OPENLDAP",
    ldapsConfig: { url: "ldaps://10.15.68.56:636" },
    baseDN: "ou=People,dc=tlg,dc=com",
    adminDN: "cn=ldapadm,dc=tlg,dc=com",
    adminPW: "06788b699916eb0d07226ec0d45d7209e2be2a7515c8a638411f6c0584234665",
    useJobSendmail: 1,
    useRedisQueueSendmail: 1,
    searchFilter: "(&(objectClass=inetOrgPerson)(uid={{username}}))",
    poolConfig: { user: "einv_owner", password: "06788b699916eb0d07226ec0d45d7209e2be2a7515c8a638411f6c0584234665", database: "app_e_invoice", server: "10.15.119.167", parseJSON: false, options: { enableArithAbort: true }, requestTimeout : 600000, connectionTimeout: 600000,pool: {
        max: 10,
        min: 0,
        idleTimeoutMillis: 180000
    } },
    poolIntegrationConfig: { user: "einv_owner", password: "06788b699916eb0d07226ec0d45d7209e2be2a7515c8a638411f6c0584234665", database: "app_e_invoice_job", server: "10.15.119.167", parseJSON: false, options: { enableArithAbort: true }, requestTimeout : 600000, connectionTimeout: 600000,pool: {
        max: 10,
        min: 0,
        idleTimeoutMillis: 180000
    } },
    // redisConfig: { host: "10.15.68.103", port: 6390, password: "redis@6390" }
    redisConfig: { host: "10.15.119.59", port: 7380, password: "ae7ede594c1a9f92244f76131d94abcf239d0d329a31a53e46b5ff2a4c112a60", db: 0 },
    scheduleslist:[
        {schedulename: "Send Approve and Cancelling mail from temporary table", scheduleinterval: '0 * * * * *', schedulemethod:`schedule.sendEmailApprInv()`, schedulestatus: 0},
        {schedulename: "Delete data using to send mail from temporary table", scheduleinterval: '0 22 * * *', schedulemethod:`schedule.delTempDocMail()`, schedulestatus: 0}
    ],
    upload_minute: 1,
    sendmail_pdf_xml: 1,
    sendApprMail: 1,
    sendCancMail: 1,
    genPassFile: 0,
    path_zip  : 'C:/test/',
    config_jsrep: {
        JSREPORT_URL: "http://10.15.68.103:5486/api/report",
        JSREPORT_ACC: "admin",
        JSREPORT_PASS : "8f9d4949a09d66f14f93314dc50855591ed01771cd1d4487382c4dc89bc310a2"
    },
    pathtemplate: `C:/temp/`
}
