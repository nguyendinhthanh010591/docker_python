module.exports = {
    ent: "jpm",
    dbtype: "mysql",
    xls_idt: 0,
    serial_grant: 0,
    serial_usr_grant: 1,
    ou_grant: 0,
    bth_length: 2,
    ldapPrivate: 1,//1-rieng 0-chung 
    captchaType: 0, // 0-ko dung 1-offline 2-online(Google)  // Chinh ca o file ext.js client
    // mail: "JPMC.eInvoice@fpt.com.vn",
    // smtpConfig: { host: "mail.fpt.com.vn", port: 587, secure: false, auth: { user: "JPMC.eInvoice@fpt.com.vn", pass: "bbea71f513c1e5a4647bf48c0b3705c138154223b1cab9a069d9fc2bcbc2c894" } }, //465(SSL),587
    mail: "FPT.einvoice.OnPrem@fpt.com.vn",
    smtpConfig: { host: "mail.fpt.com.vn", port: 587, secure: false, auth: { user: "FPT.einvoice.OnPrem@fpt.com.vn", pass: "37cb8399a86b034dc4ae3b8319bff75a56631a52881d9f3059bbfadc557162e6" } }, //465(SSL),587
    ldap: "AD",
    ldapsConfig: { url: "ldaps://10.100.11.1:389" },
    baseDN: "ou=People,dc=lg,dc=com",
    adminDN: "cn=ldapadm,dc=lg,dc=com",
    adminPW: "56e4a818ec215194bda946aabcec735608aac35e11420a07c76b0444ce8e3b55",
    searchFilter: "(&(objectClass=inetOrgPerson)(uid={{username}}))",
    poolConfig: { user: "appuser", password: "32d67029d6b7e88e3d1870bc1087c87576ed82b4c572bd49b762754a58620f6a", database: "jpmg", host: "10.15.68.212", port: 3306},//Admin123$
    redisConfig: { host: "10.14.122.35", port: 6379, password: "c244a62332ab19ceba468529738bcf3b4f5112cac61f2c8bfb088b2474b544d9"},
    scheduleslist: [       
        { schedulename: "Send to api tvan", scheduleinterval: '*/10 * * * * *', schedulemethod: `schedule.sendToTvan('SENDTVAN', 300)`, schedulestatus: 1 }, //10s 1 l�n
        { schedulename: "Delete expiring running job", scheduleinterval: '0 */12 * * *', schedulemethod: `schedule.DropRunningJobExpire('SENDTVAN')`, schedulestatus: 1 },// b?t job ch?y d? ko b? l?i ko g?i email do t?n t?i key cu d� h?t h?n trong b?ng s_listvalues
        { schedulename: "Send Approve and Cancelling mail from temporary table", scheduleinterval: '0 * * * * *', schedulemethod: `schedule.sendEmailApprInv('SENDEMAILAPPRINV', 300)`, schedulestatus: 1 },
        { schedulename: "Delete expiring running job", scheduleinterval: '0 */5 * * *', schedulemethod: `schedule.DropRunningJobExpire('SENDEMAILAPPRINV')`, schedulestatus: 1 },// b?t job ch?y d? ko b? l?i ko g?i email do t?n
    ],
    upload_minute: 0,
    sendmail_pdf_xml: 1,
    sendmail_pdf_xml_notzip: 0, //Gửi mail không zip file, 1 - Không zip, 0 - Có zip
    sendApprMail: 1,
    sendCancMail: 1,
    genPassFile: 0,
    grant_search: 1, //Tham số tra cứu portal có được phép tra cứu theo MST con hay không
    useJobSendmail: 1,
    useRedisQueueSendmail: 0,
    apprSubjectMail: "J.P. Morgan Việt Nam - Hóa đơn điện tử ký hiệu {{serial}} , số hóa đơn {{seq}}, phát hành ngày {{idt}}",
    canSubjectMail: "J.P. Morgan Việt Nam - Hóa đơn điện tử ký hiệu {{serial}} , số hóa đơn {{seq}}, phát hành ngày {{idt}}",
    days_change_pass: 90, //S? ng�y b?t bu?c ph?i d?i m?t kh?u, s? d?ng cho user local
    days_warning_change_pass: 10, //S? ng�y b?t b?t d?u g?i c?nh b�o m?t kh?u, s? d?ng cho user local
    total_pass_store: 10, //S? l?n luu m?t kh?u d? check tr�ng l?p m?t kh?u cu
    is_use_local_user: 1, //C� d�ng user luu tr? tr�n local DB hay kh�ng
    local_password_expire: 1, //Th?i gian m?t kh?u expire, t�nh theo ng�y
    onelogin: 0, //Tham số có check 1 user login cho 1 thiết bị hay không,
    config_jsrep: {
        JSREPORT_URL: "http://10.15.68.103:5486/api/report",
        JSREPORT_ACC: "admin",
        JSREPORT_PASS : "ab664278f0e5a035326390664ba7247749dcb3c199216ceff462bfba4fa25f16"

    },
  
    dbCache: 1,
    log4jsConfig: {
        appenders: {
            einvoice: { type: 'file', filename: '/u01/apps/LG_NC123/log/einvoice_lg123.log', maxLogSize: 10485760, backups: 60, compress: true }
        },
        categories: {
            default: { appenders: ['einvoice'], level: 'info' }
        },
        pm2: true
    },
    url_tvan: "http://localhost:6799/",
    path_zip: 'C:/test/',
}
