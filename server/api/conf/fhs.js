module.exports = {
    ent: "fhs",
    dbtype: "pgsql",
    xls_idt: 0,
    serial_grant: 1,
	serial_usr_grant: 0,
    ou_grant: 1,
    ldapPrivate: 1,//1-rieng 0-chung 
    captchaType: 2, // 0-ko dung 1-offline 2-online(Google)  // Chinh ca o file ext.js client
    mail: "FPT.einvoice.OnPrem@fpt.com.vn",
    smtpConfig: { host: "mail.fpt.com.vn", port: 587, secure: false, auth: { user: "FPT.einvoice.OnPrem@fpt.com.vn", pass: "ae66d44064b145ea073d9698d5de3f866015840074444fdc32f10953078b4ef3" } }, //465(SSL),587(TLS)
    ldap: "OPENLDAP",
    ldapsConfig: { url: "ldaps://10.15.68.56:636" },
    baseDN: "ou=People,dc=tlg,dc=com",
    adminDN: "cn=ldapadm,dc=tlg,dc=com",
    adminPW: "06788b699916eb0d07226ec0d45d7209e2be2a7515c8a638411f6c0584234665",
    searchFilter: "(&(objectClass=inetOrgPerson)(uid={{username}}))",
    poolConfig: {
        user: "fahasa", password: "8009eb276f6fcc0053dc1d597fe78c9ffa7a8f5f1370a456e2a6e949b80e73c7", database: "invoice", host: "10.15.68.41", port: 5432, 
        max: 32, //maximum connection which postgresql or mysql can intiate
        min: 0, //maximum connection which postgresql or mysql can intiate
        acquire: 20000, // time require to reconnect 
        idle: 20000, // get idle connection
        evict: 10000 // it actualy removes the idle connection
    },//UAT
    redisConfig: { host: "10.15.68.42", port: 6379, password: "ae7ede594c1a9f92244f76131d94abcf239d0d329a31a53e46b5ff2a4c112a60" },
    upload_minute: 1,
    sendmail_pdf_xml: 1,
    sendApprMail: 0,
    sendCancMail: 0,
    genPassFile: 0,
    xls_data: 1,
    chk_mst: 0, //0: khong kiem tra, 1: kiem tra
    config_chietkhau_col:"Y",//tham so khai chiet khau hoa don phan dtl co gia trị là hien thi, ko co là an
    config_jsrep: {
        //JSREPORT_URL: "http://10.15.68.103:5486/api/report",
        JSREPORT_URL: "http://10.15.68.103:5486",
        JSREPORT_ACC: "admin",
        JSREPORT_PASS : "4a3283ac314b967445ec56e77bf723703665aa96ef482947107374faf610d2db",
        jsreportrender: 1
    },
    api:"http://10.15.68.42:8888/api/pdf/"
}

