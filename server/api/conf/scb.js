module.exports = {
  ent: "scb",
  dbtype: "mssql",
  xls_idt: 0,
  serial_grant: 0,
  serial_usr_grant: 0,
  ou_grant: 0,
  invoice_seq: 0, // có chức năng cấp số hay không
  ldapPrivate: 1,//1-rieng 0-chung 
  captchaType: 2, // 0-ko dung 1-offline 2-online(Google)  // Chinh ca o file ext.js client
  mail: "FPT.einvoice.OnPrem@fpt.com.vn",
  smtpConfig: { host: "mail.fpt.com.vn", port: 587, secure: false, auth: { user: "FPT.einvoice.OnPrem@fpt.com.vn", pass: "ae66d44064b145ea073d9698d5de3f866015840074444fdc32f10953078b4ef3" } }, //465(SSL),587(TLS)
  ldap: "OUD",
  ldapsConfig: { url: "ldaps://10.15.68.113:636" },
  //baseDN: "cn=FPT,dc=einvoice,dc=local",
  baseDN: "dc=einvoice,dc=local",
  adminDN: "cn=Directory Manager",
  adminPW: "06788b699916eb0d07226ec0d45d7209e2be2a7515c8a638411f6c0584234665",
  searchFilter: "(&(objectClass=inetOrgPerson)(cn={{username}}))",
  baseDNUsersFilter: "dc=einvoice,dc=local",
  searchUsersFilter: "(&(objectClass=person)(objectClass=inetOrgPerson))",
  ldapattributes: ["cn", "mail", "fullName", "ismemberof"],
  /*
  tlsOptions: {
    ca: [
      fs.readFileSync('/path/to/root_ca_cert.crt')
    ]
  },
  */
  poolConfig: { user: "einv_owner", password: "06788b699916eb0d07226ec0d45d7209e2be2a7515c8a638411f6c0584234665", database: "scb_einvoiceapp", server: "10.15.119.150\\EINVPROD1", parseJSON: false, options: { enableArithAbort: true } },
  dbCache: 1,
  //redisConfig: { host: "10.15.119.59", port: 6383, password: "ae7ede594c1a9f92244f76131d94abcf239d0d329a31a53e46b5ff2a4c112a60" },
  upload_minute: 1,
  sendmail_pdf_xml: 1,
  sendApprMail: 1,
  sendCancMail: 0,
  genPassFile: 1,
  not_sendmail_pass: 1,
  genpassrule: { length: 10, numbers: true, symbols: true, lowercase: true, uppercase: true, strict: true },
  path_zip: 'C:/test/',
  useJobSendmail: 1,
  scheduleslist: [
    { schedulename: "Send Approve and Cancelling mail from temporary table", scheduleinterval: '0 * * * * *', schedulemethod: `schedule.sendEmailApprInv()`, schedulestatus: 0 },
    { schedulename: "Delete data using to send mail from temporary table", scheduleinterval: '0 22 * * *', schedulemethod: `schedule.delTempDocMail()`, schedulestatus: 0 },
    { schedulename: "Send SPLUNK report 01 (AUMSER-ENVOICE-Audit)", scheduleinterval: '0 0 */1 * * *', schedulemethod: `schedule.AUMSER1()`, schedulestatus: 0 }, //Chạy 1h/lần
    { schedulename: "Send SPLUNK report 02 (AUMSER-ENVOIC-ID)", scheduleinterval: '0 0 */1 * * *', schedulemethod: `schedule.AUMSER2()`, schedulestatus: 0 }, //Chạy 1h/lần
    { schedulename: "Send SPLUNK report 03 (AUMSER-ENVOICE-Login-Logout)", scheduleinterval: '0 0 */1 * * *', schedulemethod: `schedule.AUMSER3()`, schedulestatus: 0 }, //Chạy 1h/lần
    { schedulename: "Send SPLUNK report 04 (AUMSER-ENVOICE-Profile)", scheduleinterval: '0 0 */1 * * *', schedulemethod: `schedule.AUMSER4()`, schedulestatus: 0 }, //Chạy 1h/lần
    { schedulename: "Synchronize Users and Group from OUD", scheduleinterval: '0 22 * * *', schedulemethod: `schedule.syncUsersFromOUD()`, schedulestatus: 0 }, //Chạy 1h/lần
    { schedulename: "Delete Historical Data", scheduleinterval: '0 22 * * *', schedulemethod: `schedule.delHistoricalData()`, schedulestatus: 0 }
  ],
  ftpConfig: { host: "10.15.119.131", port: 22, user: "einvoice", password: "06788b699916eb0d07226ec0d45d7209e2be2a7515c8a638411f6c0584234665", SFTP: 1 },
  ftpdestPath: "/u01/apps/splunkscb/",
  splunkReportPath: "/u01/apps/splunkscb/",
  mdis: {
    senderAddress : "no.reply.my@sc.com",
    root_topic: "v1/51499-fpteinvoice-/eml/scbml-3.0/vn/req/out/28509-mdis-/",
    scb_messageSender: "FPTEINVOICE",
    ext1_language: "ENG",
    scb_countryCode: "VN",
    scb_captureSystem: "MDIS",
    scb_payloadVersion: "1.0",
    scb_domainName: "GroupFunctions",
    scb_messageVersion: "1.0",
    scb_subDomainType: "Communication",
    scb_typeName: "Communication",
    scb_subTypeName: "sendEmail",
    scb_possibleDuplicate: "N",
    ext1_enableTemplateFlag: "N",
    ext1_utfIndicator: "Y",
    ext1_attachmentEncryptionPassPhrase: "Y",
    ext1_mulitpleRecepientsIndicator: "Y"
  },
  scb_dept_check: 1,
  test_sms_fpt: 1,
  config_jsrep: {
    //JSREPORT_URL: "http://10.15.68.103:5486/api/report",
    JSREPORT_URL: "http://10.15.68.103:5486",
    JSREPORT_ACC: "admin",
    JSREPORT_PASS: "4a3283ac314b967445ec56e77bf723703665aa96ef482947107374faf610d2db",
    jsreportrender: 1
  },
  check_attach_size_limit: 5, //Tham số theo MB để hạn chế dung lượng gửi mail, không có tham số này thì sẽ là không giới hạn
  array_table_setting: [
    //tablename: Tên bảng cần xóa, conditionfield: tên trường theo điều kiện xóa, numberofdaystokeep: số ngày lưu trữ tính từ thời điểm hiện tại trở về trước và dữ liệu trước đó sẽ xóa hết, extendcondition: Thêm điều kiện nếu cần (ví dụ status = 1), enable: Có thực hiện xóa hay không
    { tablename: "dbo.s_sys_logs", conditionfield: "dt", numberofdaystokeep: 90, extendcondition: "", enable: 1 }, //Bảng lưu log thao tác người dùng
    { tablename: "dbo.s_solace_msg", conditionfield: "dt", numberofdaystokeep: 90, extendcondition: "", enable: 1 }, //Bảng lưu dữ liệu nhận gửi với hệ thống Solace Queue
    { tablename: "dbo.s_trans", conditionfield: "create_date", numberofdaystokeep: 90, extendcondition: "", enable: 0 }, //Bảng lưu giao dịch gom hóa đơn
    { tablename: "dbo.s_inv", conditionfield: "idt", numberofdaystokeep: 90, extendcondition: "", enable: 0 }, //Bảng lưu dữ liệu hóa đơn
    { tablename: "dbo.einv_psgl_reconciliation", conditionfield: "ps_journal_dt", numberofdaystokeep: 90, extendcondition: "", enable: 0 }, //Bảng lưu dữ liệu báo cáo đối chiếu
    { tablename: "scb_integration.dbo.cdl_cards_vn_accontpf", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_cards_vn_bsltrxpf", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_cards_vn_cardsfpf", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_cards_vn_iastrxpf", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_cards_vn_inpeidpf", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_cards_vn_rntavgpf", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_cards_vn_transrpf", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_cards_vn_tratyppf", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_dtp_vn_custrot", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_dtp_vn_dealchg1", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_dtp_vn_dealchg2", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_ebbs_vn_account", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_ebbs_vn_chnl", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_ebbs_vn_chrg", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_ebbs_vn_cnttype", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_ebbs_vn_currrt", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_ebbs_vn_mast", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_ebbs_vn_mastrel", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_ebbs_vn_mstinfo", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_ebbs_vn_rel", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_ebbs_vn_reladdr", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_ebbs_vn_relcont", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_ebbs_vn_trnarc", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_ebbs_vn_trncd", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_ebbs_vn_trntype", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_fapsgl_all_psfn3_gl_je_line", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_fapsgl_all_psfn3_ps_chartfields", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_imxgl_vn_acctjrnstdext", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_imxgl_vn_trdcusstdext", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_mxg_all_cashflow", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_mxg_all_trade", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_opc_vn_trade", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_otp_vn_scbt_t_deal_hist", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_otp_vn_scbt_t_ips_basel_txn", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_otp_vn_scbt_t_ips_glel_cust_static", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_otp_vn_scbt_t_ips_psgl_txn", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_rls_vn_cm500a", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_rls_vn_cm520a", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_secure1_all_interface_posting", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.cdl_secure1_all_invoice_detail", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.einv_c400_transactions", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.einv_cdlsync_daily", conditionfield: "process_date", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.einv_dtp_transactions", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.einv_ebbs_customer", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.einv_ebbs_email", conditionfield: "ods", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.einv_ebbs_transactions", conditionfield: "process_date", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.einv_opic_transactions", conditionfield: "process_date", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.einv_otp_transactions", conditionfield: "process_date", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.einv_process_tail", conditionfield: "process_date", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.einv_psgl_reconciliation", conditionfield: "process_date", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.einv_rls_transactions", conditionfield: "process_date", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.einv_seccure_transactions", conditionfield: "process_date", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.einv_transactions", conditionfield: "process_date", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.einv_transactions_exception", conditionfield: "process_date", numberofdaystokeep: 90, extendcondition: "", enable: 1 },
    { tablename: "scb_integration.dbo.einv_transmission", conditionfield: "process_date", numberofdaystokeep: 90, extendcondition: "", enable: 1 }


  ],
  AdminMail: [{
    idmail: "getUserFromOUD_Err",
    to: "hunglq19@fpt.com.vn",
    cc: "hunglqfpt@gmail.com",
    subject: "Failed to connect AD",
    content: `Dear Admin group members.
    
    Here is the error when connecting to OUD. 
    
    <div>{{err.message}}</div>
    
    Best & Regards.`
  }, {
    idmail: "getUserFromOUD_Succ",
    to: "hunglq19@fpt.com.vn",
    cc: "hunglqfpt@gmail.com",
    subject: "Success to connect AD",
    content: `Dear Admin group members.
    
    Task "Pull data from OUD" complete successfully. 
    
    Best & Regards.`
  }],
  https_server_option: {
    key: 'E:/Temp/ssl/cert.key',
    cert: 'E:/Temp/ssl/cert.crt'

  }
}

