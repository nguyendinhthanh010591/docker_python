module.exports = {
    ent: "cimb",
    dbtype: "mysql",
    xls_idt: 0,
    serial_grant: 0,
    serial_usr_grant: 1,
    ou_grant: 0,
    ldapPrivate: 0, //1-rieng 0-chung 
    captchaType: 0, // 0-ko dung 1-offline 2-online(Google)  // Chinh ca o file ext.js client
    mail: "FPT.einvoice.OnPrem@fpt.com.vn",
    smtpConfig: { host: "mail.fpt.com.vn", port: 587, secure: false, auth: { user: "FPT.einvoice.OnPrem@fpt.com.vn", pass: "28ce1d150b41609d904584d2b4fc51af53a5cef42e4b9fcb0ed0fe159f717954" } }, //465(SSL),587(TLS) //Admin@12345
    ldap: "AD",
    ldapsConfig: { url: "ldaps://10.15.119.161:636" },
    baseDN: "ou=People,dc=lg,dc=com",
    adminDN: "cn=ldapadm,dc=lg,dc=com",
    adminPW: "56e4a818ec215194bda946aabcec735608aac35e11420a07c76b0444ce8e3b55",
    searchFilter: "(&(objectClass=inetOrgPerson)(uid={{username}}))",
    poolConfig: { user: "appuser", password: "34674807579ca5179c2562817624ef4eb420e58c08ba49bf337794838e962919", database: "einvoice", host: "10.14.122.35", port: 3306},//Admin123$
    redisConfig: { host: "10.14.122.35", port: 6379, password: "c244a62332ab19ceba468529738bcf3b4f5112cac61f2c8bfb088b2474b544d9"},
    scheduleslist: [       
        { schedulename: "Send to api tvan", scheduleinterval: '*/10 * * * * *', schedulemethod: `schedule.sendToTvan('SENDTVAN', 300)`, schedulestatus: 1 },//10s 1 lân
        { schedulename: "Delete expiring running job", scheduleinterval: '0 */12 * * *', schedulemethod: `schedule.DropRunningJobExpire('SENDTVAN')`, schedulestatus: 1 },// bật job chạy để ko bị lỗi ko gửi email do tồn tại key cũ đã hết hạn trong bảng s_listvalues
        { schedulename: "Send Approve and Cancelling mail from temporary table", scheduleinterval: '0 * * * * *', schedulemethod: `schedule.sendEmailApprInv('SENDEMAILAPPRINV', 300)`, schedulestatus: 1 },
        { schedulename: "Delete expiring running job", scheduleinterval: '0 */5 * * *', schedulemethod: `schedule.DropRunningJobExpire('SENDEMAILAPPRINV')`, schedulestatus: 1 },// bật job chạy để ko bị lỗi ko gửi email do tồn

    ],
    upload_minute: 0,
    sendmail_pdf_xml: 1,
    sendmail_pdf_xml_notzip: 0, //Gửi mail không zip file, 1 - Không zip, 0 - Có zip
    sendApprMail: 1,
    sendCancMail: 0,
    genPassFile: 1,
    grant_search: 1, //Tham số tra cứu portal có được phép tra cứu theo MST con hay không
    useJobSendmail: 1,
    useRedisQueueSendmail: 0,
    onelogin: 0, //Tham số có check 1 user login cho 1 thiết bị hay không,
    days_change_pass: 90, //Số ngày bắt buộc phải đổi mật khẩu, sử dụng cho user local
    days_warning_change_pass: 10, //Số ngày bắt bắt đầu gửi cảnh báo mật khẩu, sử dụng cho user local
    total_pass_store: 10, //Số lần lưu mật khẩu để check trùng lặp mật khẩu cũ
    is_use_local_user: 1, //Có dùng user lưu trữ trên local DB hay không
    local_password_expire: 90, //Thời gian mật khẩu expire, tính theo ngày
    config_jsrep: {
        JSREPORT_URL: "http://10.14.122.35:5486/api/report",
        JSREPORT_ACC: "admin",
        JSREPORT_PASS : "ce68bb1cf44d8da0afd341cbcf13533f30eb58885f061d495d1fb458277bbc7e" // jsreport@5488
    },
    dbCache: 0,
    log4jsConfig: {
        appenders: {
            einvoice: { type: 'file', filename: '/u01/apps/LG_NC123/log/einvoice_lg123.log', maxLogSize: 10485760, backups: 60, compress: true }
        },
        categories: {
            default: { appenders: ['einvoice'], level: 'info' }
        },
        pm2: true
    },
    url_tvan: "http://127.0.0.1:3333/",

	//vinh mercode cho 123
}