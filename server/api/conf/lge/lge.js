module.exports = {
    ent: "lge",
    dbtype: "mysql",
    xls_idt: 0,
    serial_grant: 1,
    serial_usr_grant: 0,
    ou_grant: 0,
    ldapPrivate: 1,//1-rieng 0-chung 
    captchaType: 0, // 0-ko dung 1-offline 2-online(Google)  // Chinh ca o file ext.js client
    mail: "FPT.einvoice.OnPrem@fpt.com.vn",
    smtpConfig: { host: "mail.fpt.com.vn", port: 587, secure: false, auth: { user: "FPT.einvoice.OnPrem@fpt.com.vn", pass: "37cb8399a86b034dc4ae3b8319bff75a56631a52881d9f3059bbfadc557162e6" } }, //465(SSL),587
    ldap: "OPENLDAP",
    ldapsConfig: { url: "ldaps://10.15.119.161:636" },
    baseDN: "ou=People,dc=lg,dc=com",
    adminDN: "cn=ldapadm,dc=lg,dc=com",
    adminPW: "56e4a818ec215194bda946aabcec735608aac35e11420a07c76b0444ce8e3b55",
    searchFilter: "(&(objectClass=inetOrgPerson)(uid={{username}}))",
    poolConfig: { user: "appuser", password: "4e3e71b6dbe12f644dd1858863212e87a6efb82bf2a88bff1cbc574353cc1f75", database: "einvoice", host: "10.15.119.205", port: 3306},//Admin123$
    redisConfig: { host: "10.15.119.205", port: 6386, password: "e4f82ab59ac7df24ddc55e90123640c9e68f369861159d46fcaed2a5bf9d157e" },
    scheduleslist: [       
        { schedulename: "Send to api tvan", scheduleinterval: '*/10 * * * * *', schedulemethod: `schedule.sendToTvan('SENDTVAN', 300)`, schedulestatus: 1 },//10s 1 lân
        { schedulename: "Delete expiring running job", scheduleinterval: '0 */5 * * *', schedulemethod: `schedule.DropRunningJobExpire('SENDTVAN')`, schedulestatus: 1 },// bật job chạy để ko bị lỗi ko gửi email do tồn tại key cũ đã hết hạn trong bảng s_listvalues
    ],
    upload_minute: 0,
    sendmail_pdf_xml: 1,
    sendmail_pdf_xml_notzip: 1, //Gửi mail không zip file, 1 - Không zip, 0 - Có zip
    sendApprMail: 1,
    sendCancMail: 0,
    genPassFile: 0,
    grant_search: 1, //Tham số tra cứu portal có được phép tra cứu theo MST con hay không
    useJobSendmail: 1,
    useRedisQueueSendmail: 1,
    onelogin: 0, //Tham số có check 1 user login cho 1 thiết bị hay không,
    config_jsrep: {
        JSREPORT_URL: "http://10.15.68.103:5486/api/report",
        JSREPORT_ACC: "admin",
        JSREPORT_PASS : "ce68bb1cf44d8da0afd341cbcf13533f30eb58885f061d495d1fb458277bbc7e"
    },
    dbCache: 0,
    log4jsConfig: {
        appenders: {
            einvoice: { type: 'file', filename: '/u01/apps/LG_NC123/log/einvoice_lg123.log', maxLogSize: 10485760, backups: 60, compress: true }
        },
        categories: {
            default: { appenders: ['einvoice'], level: 'info' }
        },
        pm2: true
    },
    url_tvan: "http://10.15.119.161:3333/",

	//vinh mercode cho 123
}