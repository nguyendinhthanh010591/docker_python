
module.exports = {
    ent: "mzh",
    dbtype: "mssql",
    xls_idt: 0,
    serial_grant: 0,
    serial_usr_grant: 0,
    ou_grant: 0,
    invoice_seq:0, // có chức năng cấp số hay không
    ldapPrivate: 1,//1-rieng 0-chung 
    captchaType:0, // 0-ko dung 1-offline 2-online(Google)  // Chinh ca o file ext.js client
    mail: "einvoice@fpt.com.vn",
    smtpConfig: { host: "mail.fpt.com.vn", port: 587, secure: false, auth: { user: "einvoice@fpt.com.vn", pass: "da3f94647b1b1092180bd7b19a5c2fb2ac7ae2ae8ebf824a70ec205f4c7d2677" } }, //465(SSL),587(TLS)
    ldap: "OPENLDAP",
    ldapsConfig: { url: "ldaps://10.15.68.56:636" },
    baseDN: "ou=People,dc=tlg,dc=com",
    adminDN: "cn=ldapadm,dc=tlg,dc=com",
    adminPW: "8559351e8709d6d47db768ebcc1f7e928958c13e5f746dd1f1bae0d9defdc6eb",
    ftpConfig: { host: "10.15.68.113", port: 21, user: "einv-ftp", password: "99d5dcd8bf43c3d7216cf4e8da8a3c8bfc74e75475e729093f5c7d2800be134c"},
    ftpdestPath: "C:/Einvoice_FTP/einv-ftp/",
    searchFilter: "(&(objectClass=inetOrgPerson)(uid={{username}}))",
    poolConfig: { user: "TuanNA", password: "f4d23f36d8578cf015f0c568d8ef9673b01ef49f11ef9538fe709b7ef26a48a3", database: "Einv_MZH", server: "10.15.68.104", parseJSON: false, options: { enableArithAbort: true }, requestTimeout : 600000, connectionTimeout: 600000,pool: {
      max: 10,
      min: 0,
      idleTimeoutMillis: 180000
  } }, //UAT 
    // poolConfig: { server: "10.15.119.167",
		// 		  port: 1433,
		// 		  database: "mzh_einvoiceapp",
		// 		  driver: "msnodesqlv8",
		// 		  options: {
		// 			trustedConnection: true
		// 		  }},
    poolConfigdds: { user: "TuanNA",
     password: "f4d23f36d8578cf015f0c568d8ef9673b01ef49f11ef9538fe709b7ef26a48a3",
      database: "Einv_MZH_DB2", 
      server: "10.15.68.104", 
      parseJSON: false, options: { enableArithAbort: true }, requestTimeout : 600000, connectionTimeout: 600000,pool: {
        max: 10,
        min: 0,
        idleTimeoutMillis: 180000
    } }, //UAT 
    redisConfig: { host: "10.15.119.125", port: 6381, password: "9e94548f924bcbdad53d225f45662d57ef8d82fbc59879d1453b72b665c4439a" },
    scheduleslist:[
      {schedulename: "Synchronyze Exchange Rate Currency", scheduleinterval: '* 5 * * *', schedulemethod:`schedule.syncExchangeRate()`, schedulestatus: 1}
    ],
    upload_minute: 1,
    sendmail_pdf_xml: 1,
    sendApprMail: 1,
    sendCancMail: 1,
    genPassFile: 1,
    path_zip  : 'C:/MZH/',
    evm:'FPT',
    config_jsrep: {
        JSREPORT_URL: "http://10.15.68.103:5489/api/report",
        JSREPORT_ACC: "admin",
        JSREPORT_PASS : "efcd186df996a88a1f0d2c12037ef23ab42beaca0953339ea39a08059f6ce606"
    }
}

