
module.exports = {
    ent: "ctbc",
    dbtype: "mssql",
    xls_idt: 0,
    serial_grant: 0,
    serial_usr_grant: 0,
    test_sms_fpt: 1,
    keyvault_config: {
        uri: "https://aiavndev-einvoice-vault.vault.azure.net/",
        client_id: "147324a3-2a30-4b46-8a67-c0dda1210b63",
        client_key: "-2L-6S4AiKA29wgCC4Gwz2LIG-n5T~ZpEP",
        tenant_id: "2ea32b59-7646-4673-9710-921225fa9c61"
    },
    sms_config:{
        tokenEndpoint: "https://api.uat.aia-apps.biz/sso/authenticate",
        sendSmsEndpoint: "https://apis.uat.aia-apps.com/sms/api/messages/send",
        allowSmsFilter: true,
    },
    ou_grant: 0,
    invoice_seq:1, // có ch?c nang c?p s? hay không
    ldapPrivate: 0,//1-rieng 0-chung 
    captchaType: 2, // 0-ko dung 1-offline 2-online(Google)  // Chinh ca o file ext.js client
    mail: "FPT.einvoice.OnPrem@fpt.com.vn",
    smtpConfig: { host: "mail.fpt.com.vn", port: 587, secure: false, auth: { user: "FPT.einvoice.OnPrem@fpt.com.vn", pass: "37cb8399a86b034dc4ae3b8319bff75a56631a52881d9f3059bbfadc557162e6" } }, //465(SSL),587(TLS)
    //mail: "truv@fpt.com.vn",
    //smtpConfig: { host: "mail.fpt.com.vn", port: 587, secure: false, UsingCredential: false },
    //ldap: "OPENLDAP",
    //ldapsConfig: { url: "ldaps://10.15.68.56:636" },
    //baseDN: "ou=People,dc=tlg,dc=com",
    //adminDN: "cn=ldapadm,dc=tlg,dc=com",
    //adminPW: "admin@123",
	//searchFilter: "(&(objectClass=inetOrgPerson)(uid={{username}}))",
	ldapsConfig: { url: "ldaps://10.15.68.113:636" }, 
    ldap: "AD",
    baseDN: "DC=einvoice,DC=internal",
    adminDN: "einvoice\\Administrator",
    adminPW: "37cb8399a86b034dc4ae3b8319bff75a56631a52881d9f3059bbfadc557162e6", //Admin@12345
    searchFilter: "(&(objectCategory=person)(objectclass=user)(sAMAccountName={{username}}))",
    poolConfig: { user: "einv_owner", password: "a866cee7baa3921181b65976b4f24b106574d8eb4c27ab9a7675fcf7a39c8d80", database: "ctbc_einvoiceapp", server: "10.15.119.150\\EINVPROD1", parseJSON: false, options: { enableArithAbort: true }, requestTimeout : 600000, connectionTimeout: 600000,pool: {
        max: 10,
        min: 0,
        idleTimeoutMillis: 180000
    } },//admin@123
    poolIntegrationConfig: { user: "einv_owner", password: "a866cee7baa3921181b65976b4f24b106574d8eb4c27ab9a7675fcf7a39c8d80", database: "ctbc_einvoiceapp", server: "10.15.119.150\\EINVPROD1", parseJSON: false, options: { enableArithAbort: true }, requestTimeout : 600000, connectionTimeout: 600000,pool: {
        max: 10,
        min: 0,
        idleTimeoutMillis: 180000
    } }, //admin@123
    redisConfig: { host: "10.15.119.161", port: 6379, password: "ae7ede594c1a9f92244f76131d94abcf239d0d329a31a53e46b5ff2a4c112a60" }, // redis@6379
    scheduleslist:[
        {schedulename: "Send Approve and Cancelling mail from temporary table", scheduleinterval: '0 * * * * *', schedulemethod:`schedule.sendEmailApprInv()`, schedulestatus: 1},
        {schedulename: "Delete data using to send mail from temporary table", scheduleinterval: '0 22 * * *', schedulemethod:`schedule.delTempDocMail()`, schedulestatus: 1},
        {schedulename: "Send SOC report detail", scheduleinterval: '0 * * * * *', schedulemethod:`schedule.socD()`, schedulestatus: 1},
        {schedulename: "Send SOCH report", scheduleinterval: '0 * * * * *', schedulemethod:`schedule.socH()`, schedulestatus: 1},
        {schedulename: "Send SCO2 user", scheduleinterval: '0 * * * * *', schedulemethod:`schedule.SCO2_user()`, schedulestatus: 1},
        {schedulename: "Send SCO2 group", scheduleinterval: '0 * * * * *', schedulemethod:`schedule.SCO2_group()`, schedulestatus: 1}
    ],
    ftpConfigDH: { host: "10.15.119.131", port: 21, user: "einvoice", password: "06788b699916eb0d07226ec0d45d7209e2be2a7515c8a638411f6c0584234665"},
    ftpConfigAG: { host: "10.15.119.131", port: 21, user: "einvoice", password: "06788b699916eb0d07226ec0d45d7209e2be2a7515c8a638411f6c0584234665"},
    ftpConfigXML: { host: "10.15.119.131", port: 21, user: "einvoice", password: "06788b699916eb0d07226ec0d45d7209e2be2a7515c8a638411f6c0584234665"},
    ftpdestPathXML: "/home/einvoice/ftp/einvoiceprem/xml/",
    ftpdestPathDH: "/home/einvoice/ftp/einvoiceprem/dh/",
    ftpdestPathAG: "/home/einvoice/ftp/einvoiceprem/accgr/",
    useFtpAttach: 1,
    upload_minute: 1,
    sendmail_pdf_xml: 1,
    sendApprMail: 1,
    sendCancMail: 1,
    genPassFile: 1,
    path_zip  : 'C:/test/',
    sendApprSMS: 1,
    sendCancSMS: 1,
    useJobSendmail: 1,
    config_jsrep: {
        JSREPORT_URL: "http://10.15.68.103:5486/api/report",
        JSREPORT_ACC: "admin",
        JSREPORT_PASS : "2ea6f6952c60f44a04b5312d1d594a7fccf79b444881665e6eaf179111cd819a"
    }
}

