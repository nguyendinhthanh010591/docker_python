module.exports = {
    ent: "baca",
    dbtype: "orcl",
    xls_data: 1,
    xls_idt: 1,
    xls_ccy: 1,
    config_chietkhau_col: 1,
    serial_grant: 1,
    serial_usr_grant: 0,
    test_sms_fpt: 1,
    sms_config: {
      tokenEndpoint: "https://api.uat.aia-apps.biz/sso/authenticate",
      sendSmsEndpoint: "https://apis.uat.aia-apps.com/sms/api/messages/send",
      allowSmsFilter: true,
    },
    bth_length: 2,
    ou_grant: 0,
    invoice_seq: 0, // cÃ³ chá»©c nÄƒng cáº¥p sá»‘ hay khÃ´ng
    ldapPrivate: 0, //1-rieng 0-chung
    captchaType: 0, // 0-ko dung 1-offline 2-online(Google)  // Chinh ca o file ext.js client
    refno: { mappingfield: "items.c0" }, //Khai bÃ¡o map trÆ°á»ng sá»‘ bÃºt toÃ¡n
    refdate: { mappingfield: "items.c1" }, //Khai bÃ¡o map trÆ°á»ng ngÃ y bÃºt toÃ¡n
  
    mail: "FPT.einvoice.OnPrem@fpt.com.vn",
    smtpConfig: {
      host: "mail.fpt.com.vn",
      port: 587,
      secure: false,
      auth: {
        user: "FPT.einvoice.OnPrem@fpt.com.vn",
        pass: "37cb8399a86b034dc4ae3b8319bff75a56631a52881d9f3059bbfadc557162e6",
      },
    }, //465(SSL),587(TLS)
    ldapsConfig: { url: "ldaps://10.15.68.113:636" },
    ldap: "AD",
  
    baseDN: "DC=einvoice,DC=internal",
    adminDN: "einvoice\\Administrator",
    adminPW: "cc5d6411b49a3bc329d2923fb579ec7f648517bcf12d19a3473a8daa69415e37",
    searchFilter:
      "(&(objectCategory=person)(objectclass=user)(sAMAccountName={{username}}))",
    searchFilter: "(&(objectClass=inetOrgPerson)(uid={{username}}))",
    poolConfig: {
      user: "einvoice",
      password:
        "8009eb276f6fcc0053dc1d597fe78c9ffa7a8f5f1370a456e2a6e949b80e73c7", // admin123
      connectString: "10.15.68.114/baca",
      poolMax: 32,
    },
  
    redisConfig: {
      host: "10.15.119.59",
      port: 6380,
      password:
        "ae7ede594c1a9f92244f76131d94abcf239d0d329a31a53e46b5ff2a4c112a60", // redis@6379
    },
    scheduleslist: [
      {
        schedulename: "Send Approve and Cancelling mail from temporary table",
        scheduleinterval: "0 * * * * *",
        schedulemethod: `schedule.sendEmailApprInv('SENDEMAILAPPRINV', 300)`,
        schedulestatus: 0,
      },
      {
        schedulename: "Send to api tvan",
        scheduleinterval: "*/10 * * * * *",
        schedulemethod: `schedule.sendToTvan('SENDTVAN', 300)`,
        schedulestatus: 1,
      },
      {
        schedulename: "Delete expiring running job",
        scheduleinterval: "0 */5 * * * *",
        schedulemethod: `schedule.DropRunningJobExpire('SENDTVAN')`,
        schedulestatus: 1,
      }, // báº­t job cháº¡y Ä‘á»ƒ ko bá»‹ lá»—i ko gá»­i email do tá»“n táº¡i key cÅ© Ä‘Ã£ háº¿t háº¡n trong báº£ng s_listvalues
      {
        schedulename: "Delete expiring running job",
        scheduleinterval: "0 */5 * * *",
        schedulemethod: `schedule.DropRunningJobExpire('SENDEMAILAPPRINV')`,
        schedulestatus: 0,
      }, // báº­t job cháº¡y Ä‘á»ƒ ko bá»‹ lá»—i ko gá»­i email do tá»“n táº¡i key cÅ© Ä‘Ã£ háº¿t háº¡n trong báº£ng s_listvalues
      {
        schedulename: "Delete expiring running job send solace",
        scheduleinterval: "0 * * * * *",
        schedulemethod: `schedule.DropRunningJobExpire('PUTSOLACEQUEUE')`,
        schedulestatus: 0,
      },
      {
        schedulename: "Delete data using to send mail from temporary table",
        scheduleinterval: "0 22 * * *",
        schedulemethod: `schedule.delTempDocMail()`,
        schedulestatus: 0,
      },
      {
        schedulename: "Send SPLUNK report 01 (AUMSER-ENVOICE-Audit)",
        scheduleinterval: "0 0 */1 * * *",
        schedulemethod: `schedule.AUMSER1()`,
        schedulestatus: 0,
      }, //Cháº¡y 1h/láº§n
      {
        schedulename: "Send SPLUNK report 02 (AUMSER-ENVOIC-ID)",
        scheduleinterval: "0 0 */1 * * *",
        schedulemethod: `schedule.AUMSER2()`,
        schedulestatus: 0,
      }, //Cháº¡y 1h/láº§n
      {
        schedulename: "Send SPLUNK report 03 (AUMSER-ENVOICE-Login-Logout)",
        scheduleinterval: "0 0 */1 * * *",
        schedulemethod: `schedule.AUMSER3()`,
        schedulestatus: 0,
      }, //Cháº¡y 1h/láº§n
      {
        schedulename: "Send SPLUNK report 04 (AUMSER-ENVOICE-Profile)",
        scheduleinterval: "0 0 */1 * * *",
        schedulemethod: `schedule.AUMSER4()`,
        schedulestatus: 0,
      }, //Cháº¡y 1h/láº§n
      {
        schedulename: "Synchronize Users and Group from OUD",
        scheduleinterval: "0 22 * * *",
        schedulemethod: `schedule.syncUsersFromOUD()`,
        schedulestatus: 0,
      }, //Cháº¡y 1h/láº§n
      {
        schedulename: "Delete Historical Data",
        scheduleinterval: "0 22 * * *",
        schedulemethod: `schedule.delHistoricalData()`,
        schedulestatus: 0,
      },
      {
        schedulename: "Create inv from trans",
        scheduleinterval: "*/2 * * * *",
        schedulemethod: `schedule.createInvFromTran()`,
        schedulestatus: 0,
      },
      {
        schedulename: "Send mail for admin when digital signatures is expired",
        scheduleinterval: "0 7 * * *",
        schedulemethod: `schedule.sendEmailCTSExpried()`,
        schedulestatus: 1,
      }, // job nháº¯c cks háº¿t háº¡n cháº¡y vÃ o 7h sÃ¡ng hÃ ng ngÃ y
    ],
    ftpConfig: {
      host: "10.15.119.131",
      port: 21,
      user: "einvoice",
      password:
        "06788b699916eb0d07226ec0d45d7209e2be2a7515c8a638411f6c0584234665",
    },
    ftpdestPath: "/home/einvoice/ftp/einvoiceprem/",
    useFtpAttach: 1,
    upload_minute: 1 /*
      config_7zip: { //Cáº¥u hÃ¬nh liÃªn quan Ä‘áº¿n viá»‡c zip file attach thÃ nh file 7z
          PathSave7zipFile: "C:/Temp/7zip/", //ThÆ° má»¥c lÆ°u file 7z sau khi táº¡o xong
          PathFileToZip: "C:/Temp/7zip/" //ThÆ° má»¥c lÆ°u táº¡m file PDF, XML phá»¥c vá»¥ cho viá»‡c zip thÃ nh file 7z
      },*/,
    sendmail_pdf_xml: 1,
    sendApprMail: 1,
    sendCancMail: 1,
    genPassFile: 0,
    not_sendmail_pass: 0,
    genpassrule: {
      length: 10,
      numbers: true,
      symbols: true,
      lowercase: true,
      uppercase: true,
      strict: true,
    },
    path_zip: "C:/test/",
    sendApprSMS: 1,
    sendCancSMS: 1,
    useJobSendmail: 1,
    useRedisQueueSendmail: 1,
    config_jsrep: {
      JSREPORT_URL: "http://10.15.68.103:5486/api/report",
      JSREPORT_ACC: "admin",
      JSREPORT_PASS:
        "2ea6f6952c60f44a04b5312d1d594a7fccf79b444881665e6eaf179111cd819a",
    } /*,
      https_server_option: {
      key: 'E:/Temp/ssl/cert.key',
      cert: 'E:/Temp/ssl/cert.crt'
  
      }*/,
    apprSubjectMail:
      "HÃ“A ÄÆ N ÄIá»†N Tá»¬ Cá»¦A NGÃ‚N HÃ€NG TMCP Báº¢O VIá»†T (BAOVIET Bank E-invoice)",
    canSubjectMail:
      "HÃ“A ÄÆ N ÄIá»†N Tá»¬ Cá»¦A NGÃ‚N HÃ€NG TMCP Báº¢O VIá»†T (BAOVIET Bank E-invoice)",
    //useJobSendmailAPI : 1,
    //useRedisQueueSendmailAPI: 1,
    days_left_active_cts: 30,
    days_change_pass: 90, //Sá»‘ ngÃ y báº¯t buá»™c pháº£i Ä‘á»•i máº­t kháº©u, sá»­ dá»¥ng cho user local
    days_warning_change_pass: 10, //Sá»‘ ngÃ y báº¯t báº¯t Ä‘áº§u gá»­i cáº£nh bÃ¡o máº­t kháº©u, sá»­ dá»¥ng cho user local
    total_pass_store: 10, //Sá»‘ láº§n lÆ°u máº­t kháº©u Ä‘á»ƒ check trÃ¹ng láº·p máº­t kháº©u cÅ©
    is_use_local_user: 1, //Có dùng user lưu trữ trên local DB hay không
    local_password_expire: 1, //Thá»i gian máº­t kháº©u expire, tÃ­nh theo ngÃ y
    disable_worker: 0, // Táº¯t worker hay khÃ´ng
    dbCache: 0,
    log4jsConfig: {
      appenders: {
        einvoice: {
          type: "console",
          filename: "E:/Temp/einvoice/logs/einvoice.log",
          maxLogSize: 10485760,
          backups: 60,
          compress: true,
        },
      },
      categories: {
        default: { appenders: ["einvoice"], level: "debug" },
      },
      pm2: true,
    },
    url_tvan: "http://10.15.119.161:3313/",
  };
  