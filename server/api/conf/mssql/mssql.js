
module.exports = {
    useAzureAD: false, //Có sử dụng Azure AD hay không, nếu cái này true thì phải set config is_use_local_user: 0
    azureADConfig: {
        strategy: `azuread-openidconnect`,
        // Required
        identityMetadata: 'https://login.microsoftonline.com/fptcloud.onmicrosoft.com/v2.0/.well-known/openid-configuration', 
        //Example: identityMetadata: 'https://login.microsoftonline.com/abc-company.onmicrosoft.com/v2.0/.well-known/openid-configuration', 
        // or equivalently: 'https://login.microsoftonline.com/<tenant_guid>/v2.0/.well-known/openid-configuration'
        //
        // or you can use the common endpoint
        // 'https://login.microsoftonline.com/common/v2.0/.well-known/openid-configuration'
        // To use the common endpoint, you have to either turn `validateIssuer` off, or provide the `issuer` value.
        
        // Required, the client ID of your app in AAD  
        clientID: '17a3a7d9-15d4-423e-a323-c914ed177055',

        // Required if `responseType` is 'code', 'id_token code' or 'code id_token'. 
        // If app key contains '\', replace it with '\\'.
        clientSecret: '8~l8Q~MK9a8Q~IMNoNyKrYSziINZ4FyDeatmWbzg', 

        // Required, must be 'code', 'code id_token', 'id_token code' or 'id_token'
        // If you want to get access_token, you must use 'code', 'code id_token' or 'id_token code' 
        responseType: 'code id_token', 

        // Required
        responseMode: 'form_post', 

        // Required, the reply URL registered in AAD for your app
        redirectUrl: 'http://localhost:3000/api/signin-azure-ad-success', 

        // Required if we use http for redirectUrl
        allowHttpForRedirectUrl: true,

        // Required to set to false if you don't want to validate issuer
        validateIssuer: false,

        // Required if you want to provide the issuer(s) you want to validate instead of using the issuer from metadata
        // issuer could be a string or an array of strings of the following form: 'https://sts.windows.net/<tenant_guid>/v2.0'
        issuer: null,

        // Required to set to true if the `verify` function has 'req' as the first parameter
        passReqToCallback: false,

        // Recommended to set to true. By default we save state in express session, if this option is set to true, then
        // we encrypt state and save it in cookie instead. This option together with { session: false } allows your app
        // to be completely express session free.
        useCookieInsteadOfSession: false,

        // Required if `useCookieInsteadOfSession` is set to true. You can provide multiple set of key/iv pairs for key
        // rollover purpose. We always use the first set of key/iv pair to encrypt cookie, but we will try every set of
        // key/iv pair to decrypt cookie. Key can be any string of length 32, and iv can be any string of length 12.
        cookieEncryptionKeys: [ 
            { 'key': '12345678901234567890123456789012', 'iv': '123456789012' },
            { 'key': 'abcdefghijklmnopqrstuvwxyzabcdef', 'iv': 'abcdefghijkl' }
        ],

        // The additional scopes we want besides 'openid'.
        // 'profile' scope is required, the rest scopes are optional.
        // (1) if you want to receive refresh_token, use 'offline_access' scope
        // (2) if you want to get access_token for graph api, use the graph api url like 'https://graph.microsoft.com/mail.read'
        scope: ['profile', 'offline_access', 'https://graph.microsoft.com/mail.read'],

        // Optional, 'error', 'warn' or 'info'
        loggingLevel: 'info',

        // Optional. The lifetime of nonce in session or cookie, the default value is 3600 (seconds).
        nonceLifetime: null,

        // Optional. The max amount of nonce saved in session or cookie, the default value is 10.
        nonceMaxAmount: 5,

        // Optional. The clock skew allowed in token validation, the default value is 300 seconds.
        clockSkew: null,

        destroySessionUrl: 'https://login.microsoftonline.com/common/oauth2/logout?post_logout_redirect_uri=http://localhost:8080',

        sessionSecretKey: "FRTeInvoice",

        cookieSameSite: false,
    },
    ent: "onprem",
    dbtype: "mssql",
    xls_data: 0,
    xls_idt: 0,
    xls_ccy: 1,
    config_chietkhau_col: 1,
    serial_grant: 0,
    serial_usr_grant: 0,
    test_sms_fpt: 1,
    sms_config: {
        tokenEndpoint: "https://api.uat.aia-apps.biz/sso/authenticate",
        sendSmsEndpoint: "https://apis.uat.aia-apps.com/sms/api/messages/send",
        allowSmsFilter: true,
    },
    bth_length:1000,
    ou_grant: 0,
    invoice_seq: 1, // có chức năng cấp số hay không
    ldapPrivate: 0,//1-rieng 0-chung 
    captchaType: 0, // 0-ko dung 1-offline 2-online(Google)  // Chinh ca o file ext.js client
    mail: "FPT.einvoice.OnPrem@fpt.com.vn",
    smtpConfig: { host: "mail.fpt.com.vn", port: 587, secure: false, auth: { user: "FPT.einvoice.OnPrem@fpt.com.vn", pass: "37cb8399a86b034dc4ae3b8319bff75a56631a52881d9f3059bbfadc557162e6" } }, //465(SSL),587(TLS)
    ldapsConfig: { url: "ldaps://10.14.119.4:636" }, 
    ldap: "AD",
    baseDN: "DC=einvoice,DC=internal",
    adminDN: "einvoice\\Administrator",
    adminPW: "cc5d6411b49a3bc329d2923fb579ec7f648517bcf12d19a3473a8daa69415e37",
    searchFilter: "(&(objectCategory=person)(objectclass=user)(sAMAccountName={{username}}))",
    poolConfig: {
        user: "einv_owner", password: "06788b699916eb0d07226ec0d45d7209e2be2a7515c8a638411f6c0584234665", database: "app_onprem_dev", server: "10.14.120.4\\MSSQL2017", parseJSON: false, options: { enableArithAbort: true }, requestTimeout: 600000, connectionTimeout: 600000, pool: {
            max: 10,
            min: 0,
            idleTimeoutMillis: 180000
        }
    }, //admin@123
    poolIntegrationConfig: {
        user: "einv_owner", password: "bde0f0a3595e73981a31918f264ec07d058a0037b07141bc63b112a42b34960c", database: "kbank_einvoiceapp", server: "10.15.119.167", parseJSON: false, options: { enableArithAbort: true }, requestTimeout: 600000, connectionTimeout: 600000, pool: {
            max: 10,
            min: 0,
            idleTimeoutMillis: 180000
        }
    },
    redisConfig: { host: "10.14.122.40", port: 6387, password: "redis@6379" },

    scheduleslist: [
        { schedulename: "Send Approve and Cancelling mail from temporary table", scheduleinterval: '0 * * * * *', schedulemethod: `schedule.sendEmailApprInv('SENDEMAILAPPRINV', 300)`, schedulestatus: 0 },
        { schedulename: "Send to api tvan", scheduleinterval: '*/10 * * * * *', schedulemethod: `schedule.sendToTvan('SENDTVAN', 300)`, schedulestatus: 0 },
        { schedulename: "Delete expiring running job", scheduleinterval: '0 */5 * * * *', schedulemethod: `schedule.DropRunningJobExpire('SENDTVAN')`, schedulestatus: 0 },// bật job chạy để ko bị lỗi ko gửi email do tồn tại key cũ đã hết hạn trong bảng s_listvalues
        { schedulename: "Delete expiring running job", scheduleinterval: '0 */5 * * *', schedulemethod: `schedule.DropRunningJobExpire('SENDEMAILAPPRINV')`, schedulestatus: 0 },// bật job chạy để ko bị lỗi ko gửi email do tồn tại key cũ đã hết hạn trong bảng s_listvalues
        { schedulename: "Delete expiring running job send solace", scheduleinterval: '0 * * * * *', schedulemethod: `schedule.DropRunningJobExpire('PUTSOLACEQUEUE')`, schedulestatus: 0 },
        { schedulename: "Delete data using to send mail from temporary table", scheduleinterval: '0 22 * * *', schedulemethod: `schedule.delTempDocMail()`, schedulestatus: 0 },
        { schedulename: "Send SPLUNK report 01 (AUMSER-ENVOICE-Audit)", scheduleinterval: '0 0 */1 * * *', schedulemethod: `schedule.AUMSER1()`, schedulestatus: 0 }, //Chạy 1h/lần
        { schedulename: "Send SPLUNK report 02 (AUMSER-ENVOIC-ID)", scheduleinterval: '0 0 */1 * * *', schedulemethod: `schedule.AUMSER2()`, schedulestatus: 0 }, //Chạy 1h/lần
        { schedulename: "Send SPLUNK report 03 (AUMSER-ENVOICE-Login-Logout)", scheduleinterval: '0 0 */1 * * *', schedulemethod: `schedule.AUMSER3()`, schedulestatus: 0 }, //Chạy 1h/lần
        { schedulename: "Send SPLUNK report 04 (AUMSER-ENVOICE-Profile)", scheduleinterval: '0 0 */1 * * *', schedulemethod: `schedule.AUMSER4()`, schedulestatus: 0 }, //Chạy 1h/lần
        { schedulename: "Synchronize Users and Group from OUD", scheduleinterval: '0 22 * * *', schedulemethod: `schedule.syncUsersFromOUD()`, schedulestatus: 0 }, //Chạy 1h/lần
        { schedulename: "Delete Historical Data", scheduleinterval: '0 22 * * *', schedulemethod: `schedule.delHistoricalData()`, schedulestatus: 0 },
        { schedulename: "Create inv from trans", scheduleinterval: '*/2 * * * *', schedulemethod: `schedule.createInvFromTran()`, schedulestatus: 0 },
        { schedulename: "Send mail for admin when digital signatures is expired", scheduleinterval: '0 7 * * *', schedulemethod: `schedule.sendEmailCTSExpried()`, schedulestatus: 0 }, // job nhắc cks hết hạn chạy vào 7h sáng hàng ngày
    ],
    ftpConfig: { host: "10.15.119.131", port: 21, user: "einvoice", password: "06788b699916eb0d07226ec0d45d7209e2be2a7515c8a638411f6c0584234665" },
    ftpdestPath: "/home/einvoice/ftp/einvoiceprem/",
    useFtpAttach: 1,
    upload_minute: 1,/*
    config_7zip: { //Cấu hình liên quan đến việc zip file attach thành file 7z
        PathSave7zipFile: "C:/Temp/7zip/", //Thư mục lưu file 7z sau khi tạo xong
        PathFileToZip: "C:/Temp/7zip/" //Thư mục lưu tạm file PDF, XML phục vụ cho việc zip thành file 7z
    },*/
    passportsaml: {
        strategy: 'saml',
        saml: {
          callbackUrl: 'https://10.15.68.212/api/signinsaml/callback',
          //protocol: "https://",
          //path: config.passport.saml.path,
          entryPoint: 'https://sso.corp.dbs.com:8443/am/SSORedirect/metaAlias/employee/idp',
          issuer: 'https://10.15.68.212',
          cert: "-----BEGIN CERTIFICATE-----MIIGvjCCBKagAwIBAgITHAABbEUaiw0jjIJXlwAAAAFsRTANBgkqhkiG9w0BAQsFADB0MRMwEQYK CZImiZPyLGQBGRYDY29tMRMwEQYKCZImiZPyLGQBGRYDZGJzMRUwEwYKCZImiZPyLGQBGRYFMWJh bmsxFDASBgoJkiaJk/IsZAEZFgRyZWcxMRswGQYDVQQDExJEQlNCYW5rLUVudC1TdWItQ0EwHhcN MjAwNDI5MDczNDU2WhcNMzAwNDI3MDczNDU2WjBoMQswCQYDVQQGEwJTRzEOMAwGA1UEBxMFSW5m cmExFTATBgNVBAoTDERCUyBCYW5rIEx0ZDENMAsGA1UECxMEQ1NFQzEjMCEGA1UEAxMaY3liZXJz ZWN1cmVpZHAudWF0LmRicy5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDcsRHk k9SwUWDTwjLiY2K5trnqYhbjFuXcl6+InVgGY94kYzX0A/dNAJktFuc1taSd3whHJnq30QhX9zHA 5E2PTuV6YeyzstHMSSOE7tG2WCe31Up9MZEMYi63KoYv6RZLvXzErMqVEPxM558cdoarGCWL2XYP TKJVYUsDa6ykr3Pz+1JL90K6/PQglRqIlv+dvmJsXlT9wVTdXNNcfWlqd+TVE+0qea/xv1+P2rGz NRCtS/D26/X2QCLY0ZU7tg2f6jp0XEbetGoeV60Q0eyxxa/OKNXzi95HUC8kng7BCl1LtRHdO9eq Fd8rVDRNAEEMVM494ba+tWnH32Mf3sVJAgMBAAGjggJTMIICTzBjBgNVHREEXDBaghpjeWJlcnNl Y3VyZWlkcC51YXQuZGJzLmNvbYIdeDAxc2NzZWNhcHAzYS52c2kudWF0LmRicy5jb22CHXgwMXNj c2VjYXBwNGEudnNpLnVhdC5kYnMuY29tMB0GA1UdDgQWBBTJg6qPVYyXWrk62bQ8oh7jppXzwDAf BgNVHSMEGDAWgBQZg7IAihLG4MUaAiTwb69wPgR69jBFBgNVHR8EPjA8MDqgOKA2hjRodHRwOi8v ZGJzY3JsLnNncC5kYnMuY29tL2NybC9EQlNCYW5rLUVudC1TdWItQ0EuY3JsMIHJBggrBgEFBQcB AQSBvDCBuTCBtgYIKwYBBQUHMAKGgalsZGFwOi8vL0NOPURCU0JhbmstRW50LVN1Yi1DQSxDTj1B SUEsQ049UHVibGljJTIwS2V5JTIwU2VydmljZXMsQ049U2VydmljZXMsQ049Q29uZmlndXJhdGlv bixEQz0xYmFuayxEQz1kYnMsREM9Y29tP2NBQ2VydGlmaWNhdGU/YmFzZT9vYmplY3RDbGFzcz1j ZXJ0aWZpY2F0aW9uQXV0aG9yaXR5MA4GA1UdDwEB/wQEAwIFoDA9BgkrBgEEAYI3FQcEMDAuBiYr BgEEAYI3FQiIvBOG3IFhh6GLHYTAxDKEj79kgWWH0qIXgt+xFAIBZAIBCDAdBgNVHSUEFjAUBggr BgEFBQcDAgYIKwYBBQUHAwEwJwYJKwYBBAGCNxUKBBowGDAKBggrBgEFBQcDAjAKBggrBgEFBQcD ATANBgkqhkiG9w0BAQsFAAOCAgEAJUYAk99b7mSdQ22Q2YgTMftcozjmfWVVr94c+9e7xKoszZ2C MBUQ6lq/BjUTtCLZ+sgSxb6tCA0gt0MlD8q4MKd6Yhi2tUcREroE0jmt4ywSd9kByCwV7LWxB3TA k5rN9jQvhKEm6NfOMnAAQdZQpMVx8BsWbRXezxKiM6EOCxhByAdroNez3FWBjAiJObwdPjtPi7IM 8JtCcy3VnOKRvMrXEi8vnuWYhkT368VAJ9kDQ4fz0OBfmbPCy14G2kXp/E27StcDDxf5LDimOVyj PQ5TMU3UfFy76EHYRassFg7M/CNWqinYlNBxLUmmbw+OMsU5V3pnQXdP6P5Umjs1MzraPKyagl7v JDCIk+JfKjQykSxeDbrviXewhaJrQIQaqIFVSCWSaX9PYg9+RelVZ2LczUrVVUtryb0o/qorkato bD+O6e9qkzCXqla+omSKQ2hAYjT5I2SI2FwkrJpQe3DvKhbmUiet4d7dKXAl15MOiLmSYrJPJ2Pm Nz0fYHMh6GMShrPEw4cCVnIz77ReUVPJgoKcnCNWLxR/xBLSUuD0CFsTshf5E7MznuiNMWihpTb8 SNFZMFo+rHsqBZD8hMB8RfSgOQps7NYHA+Ml2whaeJooMcqVIwvGoXTs07Wn2wkBbzyzhe4C/YWP VKNXP93bKGl36yWNP+IlYUZMORM=-----END CERTIFICATE-----",
          //authnContext: 'http://schemas.microsoft.com/ws/2008/06/identity/authenticationmethod/windows',
          acceptedClockSkewMs: -1,
          identifierFormat: null
          //disableRequestedAuthnContext:false,
          //authnContext: 'http://schemas.microsoft.com/ws/2008/06/identity/authenticationmethod/password'
        }
      },
    user_adfs: 1,//C� d�ng ADFS hay kh�ng  
    sendmail_pdf_xml: 1,
    Infobip:{
        Key:"0334a4c996f11259e691423736accf98-5020506e-2827-4970-bd8f-ec6ed979816d",
        Url:`https://pwz35m.api.infobip.com/email/3/send`,
    },
    sendApprMail: 1,
    sendCancMail: 1,
    genPassFile: 0,
    not_sendmail_pass: 0,
    genpassrule: { length: 10, numbers: true, symbols: true, lowercase: true, uppercase: true, strict: true },
    path_zip: 'C:/test/',
    sendApprSMS: 1,
    sendCancSMS: 1,
    useJobSendmail: 0,
    useRedisQueueSendmail: 0,
    // config_jsrep: {
    //     JSREPORT_URL: "http://10.14.122.40:5486/api/report",
    //     JSREPORT_ACC: "admin",
    //     JSREPORT_PASS: "2ea6f6952c60f44a04b5312d1d594a7fccf79b444881665e6eaf179111cd819a" //jsreport@5488
    // }
    config_jsrep: {
        JSREPORT_URL: "http://localhost:5489/api/report",
        JSREPORT_ACC: "admin",
        JSREPORT_PASS: "2ea6f6952c60f44a04b5312d1d594a7fccf79b444881665e6eaf179111cd819a" //jsreport@5488
    }
    /*,
    https_server_option: {
    key: 'E:/Temp/ssl/cert.key',
    cert: 'E:/Temp/ssl/cert.crt'

    }*/,
    apprSubjectMail: "HÓA ĐƠN ĐIỆN TỬ CỦA NGÂN HÀNG TMCP BẢO VIỆT (BAOVIET Bank E-invoice)",
    canSubjectMail: "HÓA ĐƠN ĐIỆN TỬ CỦA NGÂN HÀNG TMCP BẢO VIỆT (BAOVIET Bank E-invoice)",
    //useJobSendmailAPI : 1,
    //useRedisQueueSendmailAPI: 1,    
    days_left_active_cts: 30,
    days_change_pass: 90, //Số ngày bắt buộc phải đổi mật khẩu, sử dụng cho user local
    days_warning_change_pass: 10, //Số ngày bắt bắt đầu gửi cảnh báo mật khẩu, sử dụng cho user local
    total_pass_store: 10, //Số lần lưu mật khẩu để check trùng lặp mật khẩu cũ
    is_use_local_user: 1, //Có dùng user lưu trữ trên local DB hay không
    local_password_expire: 1, //Thời gian mật khẩu expire, tính theo ngày
    disable_worker: 0, // Tắt worker hay không
    dbCache: 1,
    log4jsConfig: {
        appenders: {
            einvoice: { type: 'console', filename: 'E:/Temp/einvoice/logs/einvoice.log', maxLogSize: 10485760, backups: 60, compress: true }
        },
        categories: {
            default: { appenders: ['einvoice'], level: 'debug' }
        },
        pm2: true
    },
    url_tvan: "http://localhost:3333/",
   
}

