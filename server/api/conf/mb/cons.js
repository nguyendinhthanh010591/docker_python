module.exports = {
    USER_NO_LDAP: "__",// ký tư để check loai user ko check ldap(trungpq10)
    ORG_EDIT: 0, // THAM SO DUOC PHEP SUA THONG TIN KHACH HANG TU DONG
    MAX_ROW_EXCEL: 50000,
    vrt: 0,
    qname: "mailfptdev",
    recaptchaScore: 0.1,
    recaptchaUrl: "https://www.google.com/recaptcha/api/siteverify",
    recaptchaSecret: "6Le3w78ZAAAAADOP0IurNaWXQH3r6eAmg6-1TOuh",
    port: 3000,
    secret: "admin#0986301253AbC&$(SeCrEt)$$$#@",
    secretdds: "56e4a818ec215194bda946aabcec735608aac35e11420a07c76b0444ce8e3b55",
    expires: 36000,
    limit: 50,
    mfd: "DD/MM/YYYY",
    dtf: "YYYY-MM-DD HH:mm:ss",
    dtfs: "DD/MM/YYYY HH:mm:ss",
    pwd: "56e4a818ec215194bda946aabcec735608aac35e11420a07c76b0444ce8e3b55",
    sessionTimeout: 9000000,
    seasecret: "lifesoshort#like$aCUP%chill*coffeebeans@",
    seaexpires: 3600,
   
    catfromredis: ["cattax", "ikind", "sstatus", "ltype", "sitype", "itype", "paymethod", "funcname_logging", "emailtype", "trstatus", "errtatus", "mailstatus", "smsstatus", "lsd", "lgd", "invtype", "stype", "dtsendtype", "hascodelist", "istatustvan", "regtype", "reg_act", "sendtype", "statement_cqt_status", "stmstatus", "goodstype", "istatusbth", "noti_taxtype_option", "noti_type_combo", "status_cqt", "type_ref_combo", "type_th", "inv_cqt_status", "wrongnotice_cqt_status","inv_cqt_status_search"], //Cau hinh mot so danh muc lay tu redis hoac bang s_listvalues thay vi ngay truoc toan fix vao code
    CARR: ["c0", "c1", "c2", "c3", "c4", "c5", "c6", "c7", "c8", "c9"],
    IKIND: [{ "id": 1, "value": "replace", "status": 1, "status123": 1 }, { "id": 2, "value": "adjust", "status": 1, "status123": 1 }, { "id": 3, "value": "replaced", "status": 1, "status123": 1 }, { "id": 4, "value": "adjusted", "status": 1, "status123": 1 }],
    ITYPE: [{ id: "01GTKT", value: "Hóa đơn giá trị gia tăng", valueclient: "inv_01gtkt", status: 1 }, { id: "02GTTT", value: "Hóa đơn bán hàng", valueclient: "inv_02gttt", status: 1 }, { id: "07KPTQ", value: "Hóa đơn bán hàng khu phi thuế quan", valueclient: "invoice_ntz", status: 1 }, { id: "03XKNB", value: "Phiếu xuất kho kiêm vận chuyển nội bộ", valueclient: "inv_03xknb", status: 1 }, { id: "04HGDL", value: "Phiếu xuất kho gửi bán hàng đại lý", valueclient: "invoice_dnfsa", status: 1 }, { id: "01/TVE", value: "Tem, vé có VAT", valueclient: "invoice_satwv", status: 1 }, { id: "02/TVE", value: "Tem, vé", valueclient: "invoice_sat", status: 1 }, { id: "01BLP", value: "Biên lai không có mệnh giá", valueclient: "invoice_trhnd", status: 1 }, { id: "02BLP", value: "Biên lai có mệnh giá", valueclient: "invoice_dr", status: 1 }],

    SSTATUS: [{ id: 3, value: "serial_status_wappr" }, { id: 1, value: "serial_status_eff" }, { id: 2, value: "serial_status_cancel" }, { id: 4, value: "serial_status_outnum" }],
    LTYPE: [{ id: "Khuyến mãi", value: "inv_kmai" }, { id: "Chiết khấu", value: "inv_chkhau" }, { id: "Mô tả", value: "inv_mota" }],
    SITYPE: [{ id: 1, value: "serial_type_manual" }, { id: 2, value: "serial_type_integ" }, { id: 3, value: "serial_type_man_inte" }],
    EMAILTYPE: [{ id: "-1", value: "emailtype_normal" }, { id: "4", value: "emailtype_cancel" }, { id: "-2", value: "emailtype_pass" }, { id: "1", value: "emailtype_user_create" }, { id: "2", value: "emailtype_cust_resetpass" }, { id: "5", value: "emailtype_user_resetpass" }],
    TRSTATUS: [{ id: "0", value: "scbtrstatus_0" }, { id: "1", value: "scbtrstatus_1" }, { id: "4", value: "scbtrstatus_4" }, { id: "2", value: "scbtrstatus_2" }, { id: "3", value: "scbtrstatus_3" }],
    ERRTATUS: [{ id: "*", value: "all" }, { id: "0", value: "err_custom" }, { id: "1", value: "err_data" }, { id: "2", value: "err_data_custom" }],
    PAYMETHOD: [{id:"CK",value:"CK",valueclient:"paymethod_ck"},{id:"TM",value:"TM",valueclient:"paymethod_tm"},{id:"TM/CK",value:"TM/CK",valueclient:"paymethod_ck_tm"},{id:"DTCN",value:"DTCN",valueclient:"paymethod_dtcn"}],
    MAILSTATUS: [{ id: "*", value: "all" }, { id: "0", value: "mail_notsent_status" }, { id: "1", value: "mail_sent_status" }],
    SMSSTATUS: [{ id: "*", value: "all" }, { id: "0", value: "sms_notsent_status" }, { id: "1", value: "sms_sent_ok_status" }, { id: "-1", value: "sms_sent_notok_status" }],
    LSD: [{ id: "0", value: "trstatus_0" }, { id: "1", value: "trstatus_1" }, { id: "4", value: "trstatus_4" }, { id: "2", value: "trstatus_2" }, { id: "3", value: "trstatus_3" }],
    LGD: [{ id: "0", value: "fee" }, { id: "1", value: "rate" }],
    INVTYPE: [{ id: "9", value: "Khác", valueclient: "other", status: 1 }, { id: "1", value: "Xăng dầu", valueclient: "petrol", status: 1 }, { id: "2", value: "Vận tải hàng không", valueclient: "air_transport", status: 1 }],
    STYPE: [{ id: 1, value: "Xuất hóa đơn chi tiết", valueclient: "sendinvtype_1", status: 1 }, { id: 2, value: "Gom Bảng tổng hợp", valueclient: "sendinvtype_2", status: 1 }],
    DTSENDTYPE: [{ id: "sendinv", value: "Chuyển đầy đủ nội dung từng hóa đơn", valueclient: "sendinv", status1: 1, status2: 1 }, { id: "listinv", value: "Chuyển bảng tổng hợp", valueclient: "listinv", status1: 1, status2: 1 }, { id: "sendinv,listinv", value: "Chuyển đầy đủ nội dung từng hóa đơn, Chuyển bảng tổng hợp", valueclient: "sendinv,listinv", status1: 0, status2: 1 }, { id: "listinv,sendinv", value: "Chuyển đầy đủ nội dung từng hóa đơn, Chuyển bảng tổng hợp", valueclient: "sendinv,listinv", status1: 0, status2: 1 }],
    HASCODELIST: [{ id: "1", value: "Hóa đơn có mã", valueclient: "lblhascode" }, { id: "0", value: "Hóa đơn không mã", valueclient: "lblnoncode" }],
    ISTATUSTVAN: [{ id: "0", value: "Chờ gửi", valueclient: "pendingtvan" }, { id: "1", value: "Đã gửi", valueclient: "senttvan" }, { id: "2", value: "Đã duyệt", valueclient: "approvedtvan" }, { id: "3", value: "Từ chối", valueclient: "rejectedtvan" }],
    REGTYPE: [{ id: "1", value: "Đăng ký mới", valueclient: "regnew" }, { id: "2", value: "Thay đổi thông tin", valueclient: "regchange" }],
    REG_ACT: [{ id: 1, value: "Thêm mới", valueclient: "regactnew" }, { id: 2, value: "Gia hạn", valueclient: "regactextend" }, { id: 3, value: "Ngưng sử dụng", valueclient: "regactcancel" }],
    SENDTYPE: [{ id: "0", value: "Thông qua tổ chức cung cấp dịch vụ hóa đơn điện tử (điểm b2, khoản 3, Điều 22 của Nghị định)" }, { id: "1", value: "Doanh nghiệp nhỏ và vừa, hợp tác xã, hộ, cá nhân kinh doanh tại địa bàn có điều kiện kinh tế xã hội khó khăn, địa bàn có điều kiện kinh tế xã hội đặc biệt khó khăn." }, { id: "2", value: "Doanh nghiệp nhỏ và vừa khác theo đề nghị của Ủy ban nhân dân tỉnh, thành phố trực thuộc trung ương gửi Bộ Tài chính trừ doanh nghiệp hoạt động tại các khu kinh tế, khu công nghiệp, khu công nghệ cao." }],
    STATEMENT_CQT_STATUS: [{ id: "0", value: "Chờ gửi CQT", valueclient: "cqt_wait_send" }, { id: 1, value: "Gửi CQT", valueclient: "cqt_sent" }, { id: 2, value: "Gửi không thành công", valueclient: "cqt_sent_fail" }, { id: 3, value: "CQT tiếp nhận", valueclient: "cqt_received" }, { id: 4, value: "CQT không tiếp nhận", valueclient: "cqt_not_received" }, { id: 5, value: "CQT chấp nhận", valueclient: "cqt_accepted" }, { id: 6, value: "CQT không chấp nhận", valueclient: "cqt_not_accepted" }, { id: 7, value: "Hết hiệu lực", valueclient: "cqt_expired" }],
    INV_CQT_STATUS : [{id: "0", value: "Chờ gửi CQT", valueclient: "cqt_wait_send"}, {id: 1, value: "Gửi CQT", valueclient: "cqt_sent"}, {id: 2, value: "Gửi không thành công", valueclient: "cqt_sent_fail"}, {id: 8, value: "Kiểm tra hợp lệ", valueclient: "inv_valid"}, {id: 9, value: "Kiểm tra không hợp lệ", valueclient: "inv_invalid"}, {id: 10, value: "Đã cấp mã", valueclient: "inv_code_grant"}, {id: 12, value: "wno_valid", valueclient: "wno_valid"}, {id: 13, value: "Dữ liệu không hợp lệ", valueclient: "wno_invalid"}],
    WRONGNOTICE_CQT_STATUS : [{id: "0", value: "Chờ gửi CQT", valueclient: "cqt_wait_send"},{id: 1, value: "Gửi CQT", valueclient: "cqt_sent"},{id: 2, value: "Gửi không thành công", valueclient: "cqt_sent_fail"},{ id:15, value: "Dữ liệu hợp lệ", valueclient: "wno_valid"},{id: 16, value: "Dữ liệu không hợp lệ", valueclient: "wno_invalid"},{id: 17, value: "Kiểm tra hợp lệ", valueclient: "wno_processed"}],
    // MAIL_STATUS : [{id:"0", value:"Chờ gửi mail"},{ id:"1",value: "Gửi mail không thành công"},{id:"2", value: "Gửi mail thành công"},{id: "3", value:"Không gửi mail"}],
    // SMS_STATUS :[{id:"1", value:"Chờ gửi SMS"},{id: "2", value:"Gửi SMS thành công"},{id: "3", value: "Gửi SMS không thành công"},{id:"4", value: "Không gửi SMS"}],
    STMSTATUS: [{ id: "1", value: "stmwaitforapproval", valueclient: "stmwaitforapproval" }, { id: "2", value: "stmapproved", valueclient: "stmapproved" }], //Trang thai dang ky su dung dich vu HDDT
    GOODSTYPE: [{ id: "0", value: "Hóa đơn điện tử", valueclient: "electronic_inv" }, { id: "1", value: "Hóa đơn đặt in", valueclient: "inv_printed" }],
    ISTATUSBTH: [{ id: "*", value: "Tất cả", valueclient: "all" }, { id: "0", value: "Chờ duyệt", valueclient: "pending_bth" }, { id: "1", value: "Đã duyệt", valueclient: "approved_bth" }],
    NOTI_TAXTYPE_OPTION: [{ id: "1", value: "Thông báo hủy/giải trình của NNT", valueclient: "taxpayer_notice_cancel_explan" }, { id: "2", value: "Thông báo hủy/giải trình của NNT theo thông báo của CQT", valueclient: "taxpayer_notice_cancel_explan_CQT" }],
    NOTI_TYPE_COMBO: [{ id: "0", value: "Mới", valueclient: "n_t_c_new" }, { id: "1", value: "Hủy", valueclient: "n_t_c_cancel" }, { id: "2", value: "Điều chỉnh", valueclient: "n_t_c_adj" }, { id: "3", value: "Thay thế", valueclient: "n_t_c_rep" }, { id: "4", value: "Giải trình", valueclient: "n_t_c_explan" }, { id: "5", value: "Sai sót do tổng hợp", valueclient: "n_t_c_mistake" }],
    STATUS_CQT: [{ id: "1", value: "CQT chấp nhận", valueclient: "CQT_SUCCESS" }, { id: "2", value: "CQT từ chối", valueclient: "CQT_FAIL" }],
    TYPE_REF_COMBO: [{ id: "1", value: "Hóa đơn điện tử theo Nghị định 123/2020/NĐ-CP", valueclient: "t_r_c_1" }],
    TYPE_TH: [{ id: "1", value: "Mới", valueclient: "type1" }, { id: "2", value: "Điều chỉnh", valueclient: "type2" }, { id: "3", value: "Thay thế", valueclient: "type3" }, { id: "4", value: "Hủy", valueclient: "type4" }, { id: "5", value: "Giải trình", valueclient: "type5" }, { id: "6", value: "Sai sót do tổng hợp", valueclient: "type6" }],
    CATTAX: [{ id: 10, vatCode: "VAT1000", cusVatCode: "VAT1000", vrt: 10, original_vrt: 10, vrn: "10%", vrngdtlbl: "Thuế suất", vrngdt: "10%", vrnc: "vat_rate_10", vrnVATRep: "Hàng hoá, dịch vụ chịu thuế GTGT (10%)", amt: 0, amtv: 0, vat: 0, vatv: 0, status: 1 }, {id: 8,vatCode: "VAT0800",cusVatCode: "VAT0800",vrt: 8,original_vrt: 8,vrn: "8%",vrngdtlbl: "Thuế suất",vrngdt: "8%",vrnc: "vat_rate_8",vrnVATRep: "Hàng hoá, dịch vụ chịu thuế GTGT (8%)",amt: 0,amtv: 0,vat: 0,vatv: 0,"status": 1},  { "id": 7, "vatCode": "VAT1070", "cusVatCode": "VAT1070", vrt: 7, original_vrt: 10, vrn: "10% x 70%", vrngdtlbl: "Thuế suất", vrngdt: "10% x 70%", vrnc: "vat_rate_1070", vrnVATRep: "Hàng hoá, dịch vụ chịu thuế GTGT (10% x 70%)", amt: 0, amtv: 0, vat: 0, vatv: 0, status: 1 }, { "id": 5.26, "vatCode": "VAT5260", "cusVatCode": "VAT5260", vrt: 5.26, original_vrt: 5.26, vrn: "5.26%", vrngdtlbl: "Thuế suất", vrngdt: "5.26%", vrnc: "vat_rate_526", vrnVATRep: "Hàng hoá, dịch vụ chịu thuế GTGT (5.26%)", amt: 0, amtv: 0, vat: 0, vatv: 0, status: 0 }, { "id": 5, "vatCode": "VAT0500", "cusVatCode": "VAT0500", vrt: 5, original_vrt: 5, vrn: "5%", vrngdtlbl: "Thuế suất", vrngdt: "5%", vrnc: "vat_rate_5", vrnVATRep: "Hàng hoá, dịch vụ chịu thuế GTGT (5%)", amt: 0, amtv: 0, vat: 0, vatv: 0, status: 1 }, { "id": 3.5, "vatCode": "VAT0570", "cusVatCode": "VAT0570", vrt: 3.5, original_vrt: 5, vrn: "5% x 70%", vrngdtlbl: "Thuế suất", vrngdt: "5% x 70%", vrnc: "vat_rate_5035", vrnVATRep: "Hàng hoá, dịch vụ chịu thuế GTGT (5% x 70%)", amt: 0, amtv: 0, vat: 0, vatv: 0, status: 1 }, { "id": 0, "vatCode": "VAT0000", "cusVatCode": "VAT0000", vrt: 0, original_vrt: 0, vrn: "0%", vrngdtlbl: "Thuế suất", vrngdt: "0%", vrnc: "vat_rate_0", vrnVATRep: "Hàng hoá, dịch vụ thuế 0%:", amt: 0, amtv: 0, vat: 0, vatv: 0, status: 1 }, { "id": -1, "vatCode": "VAT-100", "cusVatCode": "VAT-100", vrt: -1, original_vrt: -1, vrn: "Không chịu thuế", vrngdtlbl: "Thuế suất", vrngdt: "Không chịu thuế", vrnVATRep: "Hàng hoá, dịch vụ không chịu thuế GTGT:", vrnc: "vat_rate_1", amt: 0, amtv: 0, vat: 0, vatv: 0, status: 1 }, { "id": -2, "vatCode": "VAT-200", "cusVatCode": "VAT-200", vrt: -2, original_vrt: -2, vrn: "Không kê khai nộp thuế", vrngdtlbl: "Thuế suất", vrngdt: "Không kê khai nộp thuế", vrnc: "vat_rate_2", vrnVATRep: "Hàng hoá, dịch vụ không tính thuế GTGT:", amt: 0, amtv: 0, vat: 0, vatv: 0, status: 1 }],
    INV_CQT_STATUS_SEARCH : [{id: "0", value: "Chờ gửi CQT", valueclient: "cqt_wait_send"}, {id: 1, value: "Gửi CQT", valueclient: "cqt_sent"}, {id: 2, value: "Gửi không thành công", valueclient: "cqt_sent_fail"}, {id: 12, value: "wno_valid", valueclient: "wno_valid"}, {id: 13, value: "Dữ liệu không hợp lệ", valueclient: "wno_invalid"}],
    //1-system  2-danhmục  3-tbph  4-lậpHĐ  5-duyệtHĐ 6-report 7-tracuu 8-dieuchinh 9-thaythe 10-huy 11-duyetTBPH 12-huyTBPH
    // PATH_ROLE: {
    //     1: ["/ous", "/edi", "/ema", "/ete", "/use", "/gro", "/ads", "/col", "/dtl", "/xls"],
    //     2: ["/exc", "/loc", "/gns"],
    //     3: ["/ser", "/tbq"],
    //     4: ["/inv", "/exc", "/gns", "/sea"],
    //     5: ["/seq", "/app", "/sav", "/sig"],
    //     6: ["/rpt"],
    //     7: ["/sea"],
    //     8: ["/adj", "/sea", "/inv", "/gns"],
    //     9: ["/rep", "/sea", "/inv", "/gns"],
    //     10: ["/sea", "/ica", "/ist", "/iwc", "/app", "/ser", "/inv"],
    //     11: ["/ser", "/sap"],
    //     12: ["/ser", "/sca", "/sda"],
    //     13: ["/ser", "/inv", "/sea", "/adj", "/ica", "/rep"],
    //     14: ["/iwc", "/ist", "/app", "/ser", "/inv"]
    // },
    XLSCHK: {
        "GNS": { "$schema": "http://json-schema.org/draft-07/schema#", "type": "object", "properties": { "code": { "type": ["integer", "string"], "minimum": 0, "maxLength": 30 }, "name": { "type": "string", "maxLength": 255 }, "unit": { "type": "string", "maxLength": 30 }, "price": { "type": "number", "minimum": 0 } }, "required": ["code", "name", "unit"] },
        "00ORGS": { "$schema": "http://json-schema.org/draft-07/schema#", "type": "object", "properties": { "name": { "type": "string", "maxLength": 255 }, "addr": { "type": "string", "maxLength": 255 }, "mail": { "type": "string", "maxLength": 255 }, "acc": { "type": "string", "maxLength": 255 }, "bank": { "type": "string", "maxLength": 255 } }, "required": ["code", "name", "addr"] },
        "03XKNB": {
            "$schema": "http://json-schema.org/draft-07/schema#", "type": "object", "definitions": { "item": { "type": "object", "properties": { "line": { "type": "integer", "minimum": 1 }, "type": { "type": "string", "enum": ["", "Khuyến mãi", "Chiết khấu", "Mô tả"] }, "name": { "type": "string", "maxLength": 255 }, "price": { "type": "number", "minimum": 0 }, "quantity": { "type": "number", "minimum": 0 }, "amount": { "type": "number", "minimum": 0 }, "unit": { "type": "string", "maxLength": 255 } }, "required": ["line", "name"] } },
            "properties": { "note": { "type": "string", "maxLength": 255 }, "contr": { "type": "string", "maxLength": 255 }, "vehic": { "type": "string", "maxLength": 255 }, "trans": { "type": "string", "maxLength": 255 }, "recvr": { "type": "string", "maxLength": 255 }, "ordou": { "type": "string", "maxLength": 255 }, "whsfr": { "type": "string", "maxLength": 255 }, "whsto": { "type": "string", "maxLength": 255 }, "curr": { "description": "Loại tiền", "type": "string", "default": "VND", "enum": ["AUD", "CAD", "CHF", "DKK", "EUR", "GBP", "HKD", "INR", "JPY", "KRW", "KWD", "MYR", "NOK", "RUB", "SAR", "SEK", "SGD", "THB", "USD", "VND"] }, "exrt": { "type": "number", "minimum": 1/* , "default": 1 */ }, "sum": { "type": "number", "minimum": 0 }, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 } },
            "required": ["ic", "whsfr", "whsto", "curr", "exrt", "items"]
        },
        "01GTKT": {
            "$schema": "http://json-schema.org/draft-07/schema#", "type": "object", "definitions": {
                "item": { "type": "object", "properties": { "line": { "type": "integer", "minimum": 1 }, "type": { "type": "string", "enum": ["", "Khuyến mãi", "Chiết khấu", "Mô tả"] }, "name": { "type": "string", "maxLength": 255 }, "price": { "type": "number", "minimum": 0 }, "quantity": { "type": "number", "minimum": 0 }, "amount": { "type": "number", "minimum": 0 }, "vat": { "type": "number", "minimum": 0 }, "total": { "type": "number", "minimum": 0 }, "vrt": { "type": "string", "maxLength": 255 }, "unit": { "type": "string", "maxLength": 255 } }, "required": ["line", "name", "vrt", 'vatCode'] }
            },
            "properties": {
                "STR": { "type": "string", "maxLength": 255 }, "buyer": { "type": "string", "maxLength": 255 }, "bname": { "type": "string", "maxLength": 255 }, "baddr": { "type": "string", "maxLength": 255 }, "btel": { "type": "string", "maxLength": 255 }, "bacc": { "type": "string", "maxLength": 255 }, "bbank": { "type": "string", "maxLength": 255 }, "note": { "type": "string", "maxLength": 255 }, "bmail": { "type": "string", "maxLength": 255 }, "paym": { "description": "HTTT", "type": "string", "default": "TM", "enum": ["CK", "TM", "TM/CK", "DTCN"] }, "curr": { "description": "Loại tiền", "type": "string", "default": "VND", "enum": ["AUD", "CAD", "CHF", "DKK", "EUR", "GBP", "HKD", "INR", "JPY", "KRW", "KWD", "MYR", "NOK", "RUB", "SAR", "SEK", "SGD", "THB", "USD", "VND"] }, "exrt": { "type": "number", "minimum": 1/* , "default": 1 */ }, "sum": { "type": "number", "minimum": 0 }, "total": { "type": "number", "minimum": 0 }, "vat": { "type": "number", "minimum": 0 }, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 }
            },
            "required": ["idt", "ic", "bname", "baddr", "paym", "curr", "exrt", "vat", "sum", "total", "items"]
        },
        "02GTTT": {
            "$schema": "http://json-schema.org/draft-07/schema#", "type": "object", "definitions": { "item": { "type": "object", "properties": { "line": { "type": "integer", "minimum": 1 }, "type": { "type": "string", "enum": ["", "Khuyến mãi", "Chiết khấu", "Mô tả"] }, "name": { "type": "string", "maxLength": 255 }, "price": { "type": "number", "minimum": 0 }, "quantity": { "type": "number", "minimum": 0 }, "amount": { "type": "number", "minimum": 0 }, "unit": { "type": "string", "maxLength": 255 } }, "required": ["line", "name"] } },
            "properties": {
                "buyer": { "type": "string", "maxLength": 255 }, "bname": { "type": "string", "maxLength": 255 }, "baddr": { "type": "string", "maxLength": 255 }, "btel": { "type": "string", "maxLength": 255 }, "bacc": { "type": "string", "maxLength": 255 }, "bbank": { "type": "string", "maxLength": 255 }, "note": { "type": "string", "maxLength": 255 }, "bmail": { "type": "string", "maxLength": 255 }, "paym": { "description": "HTTT", "type": "string", "default": "TM", "enum": ["CK", "TM", "TM/CK", "DTCN"] }, "curr": { "description": "Loại tiền", "type": "string", "default": "VND", "enum": ["AUD", "CAD", "CHF", "DKK", "EUR", "GBP", "HKD", "INR", "JPY", "KRW", "KWD", "MYR", "NOK", "RUB", "SAR", "SEK", "SGD", "THB", "USD", "VND"] }, "exrt": { "type": "number", "minimum": 1/* , "default": 1 */ }, "sum": { "type": "number", "minimum": 0 }, "total": { "type": "number", "minimum": 0 }, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 }
            },
            "required": ["ic", "bname", "baddr", "paym", "curr", "exrt", "sum", "total", "items"]
        }
    },
    PATH_ROLE: {
        PERM_FULL_MANAGE: ["/ous", "/edi", "/ema", "/ete", "/use", "/gro", "/ads", "/col", "/dtl", "/tra", "/xls", "/fld", "/dep", "/rol", "/seg", "/rol", "/com","/pou"],
        //phan quyen chi tiet cau hinh he thong
        PERM_SYS_CONFIG: ["/org", "/ous", "/cat", "/fld","/pou"],
        PERM_SYS_CONFIG_CREATE: ["/org", "/ous", "/cat", "/fld"],
        PERM_SYS_CONFIG_DELETE: ["/org", "/ous", "/cat", "/fld"],
        PERM_SYS_CONFIG_UPDATE: ["/org", "/ous", "/cat", "/fld"],
        PERM_SYS_CONFIG_VIEW_CA: ["/org", "/ous", "/cat", "/fld"],

        //phan quyen chi tiet quan ly nguoi dung
        PERM_SYS_USER: ["/ous", "/cat", "/use", "/ser", "/fld", "/ads"],
        PERM_SYS_USER_TAPUPDATE_UPDATE: ["/ous", "/cat", "/use", "/ser", "/fld", "/ads"],
        PERM_SYS_USER_TAPUPDATE_UNSELECT: ["/ous", "/cat", "/use", "/ser", "/fld", "/ads"],
        PERM_SYS_USER_TAPUPDATE_CREATE: ["/ous", "/cat", "/use", "/ser", "/fld", "/ads"],
        PERM_SYS_USER_TAPGROUPGRANT_SAVE: ["/ous", "/cat", "/use", "/ser", "/fld", "/ads"],
        PERM_SYS_USER_TAPDATA_SAVE: ["/ous", "/cat", "/use", "/ser", "/fld", "/ads"],
        PERM_SYS_USER_RESTORE: ["/ous", "/cat", "/use", "/ser", "/fld", "/ads"],
        PERM_SYS_USER_CANCEL: ["/ous", "/cat", "/use", "/ser", "/fld", "/ads"],

        //phan quyen chi tiet quan ly nhom
        PERM_SYS_GROUP: ["/gro", "/fld"],
        PERM_SYS_GROUP_RESTORE: ["/gro", "/fld"],
        PERM_SYS_GROUP_CANCEL: ["/gro", "/fld"],
        PERM_SYS_GROUP_TAPUPDATE_UNSELECT: ["/gro", "/fld"],
        PERM_SYS_GROUP_TAPUPDATE_UPDATE: ["/gro", "/fld"],
        PERM_SYS_GROUP_TAPUPDATE_CREATE: ["/gro", "/fld"],
        PERM_SYS_GROUP_TAPDATA_SAVE: ["/gro", "/fld"],
        PERM_SYS_GROUP_TAPGROUPGRANT_SAVE: ["/gro", "/fld"],
        //phan quyen chi tiet giao dich
        PERM_TRANS: ["/cat", "/sea", "/see", "/ous", "/fld", "/exc", "/tran", "/org", "/seg", "/inv", "/itr"],


        //phan quyen
        PERM_SYS_ROLE: ["/gro", "/fld"],
        PERM_SYS_ROLE_SAVE: ["/gro", "/fld"],

        //phan quyen chi tiet tao mau hoa don
        PERM_SYS_TEMPLATE: ["/org", "/fld", "/edi", "/see"],
        PERM_SYS_TEMPLATE_APPROVE: ["/org", "/fld", "/edi", "/see"],
        PERM_SYS_TEMPLATE_SAVE: ["/org", "/fld", "/edi", "/see"],
        PERM_SYS_TEMPLATE_VIEW: ["/org", "/fld", "/edi", "/see"],
        PERM_SYS_TEMPLATE_DOWLOAD: ["/org", "/fld", "/edi", "/see"],

        //phan quyen chi tiet tao mau mail
        PERM_SYS_EMAIL: ["/ema", "/fld"],
        PERM_SYS_EMAIL_SAVE_UP: ["/ema", "/fld"],

        //phan quyen chi tiet cau hinh hoa don
        PERM_SYS_INV_CONFIG: ["/xls", "/col", "/dtl", "/fld"],
        PERM_SYS_INV_CONFIG_EDIT_DETAIL: ["/xls", "/col", "/dtl", "/fld"],
        PERM_SYS_INV_CONFIG_EDIT_EXCEL: ["/xls", "/col", "/dtl", "/fld"],
        PERM_SYS_INV_CONFIG_EDIT_INFO: ["/xls", "/col", "/dtl", "/fld"],

        //phan quyen chi tiet cau hinh truong
        PERM_SYS_FIELD_CONFIG: ["/fld"],

        //phan quyen chi tiet log he thong
        PERM_LOG_SYS: ["/sys"],

        PERM_CATEGORY_MANAGE: ["/org", "/cat", "/cus", "/fld", "/gns", "/kat", "/loc", "/ven"],
        //quyen chi tiet hang hoa dich vu
        PERM_CATEGORY_ITEMS: ["/gns", "/cat", "/fld"],
        PERM_CATEGORY_ITEMS_DELETE: ["/gns", "/cat", "/fld"],
        PERM_CATEGORY_ITEMS_CREATE: ["/gns", "/cat", "/fld"],
        PERM_CATEGORY_ITEMS_EXCEL: ["/gns", "/cat", "/fld"],
        PERM_CATEGORY_ITEMS_UPDATE: ["/gns", "/cat", "/fld"],
        PERM_CATEGORY_ITEMS_UNCHECK: ["/gns", "/cat", "/fld"],

        //quyen chi tiet ty gia
        PERM_CATEGORY_RATE: ["/cat", "/exc", "/fld"],
        PERM_CATEGORY_RATE_INSERT: ["/cat", "/exc", "/fld"],
        PERM_CATEGORY_RATE_EXCEL: ["/cat", "/exc", "/fld"],
        PERM_CATEGORY_RATE_DELETE: ["/cat", "/exc", "/fld"],
        PERM_CATEGORY_RATE_UPDATE: ["/cat", "/exc", "/fld"],
        //quyen chi tiet khach hang
        PERM_CATEGORY_CUSTOMER: ["/org", "/cat", "/cus", "/fld"],
        PERM_CATEGORY_CUSTOMER_DELETE: ["/org", "/cat", "/cus", "/fld"],
        PERM_CATEGORY_CUSTOMER_CREATE: ["/org", "/cat", "/cus", "/fld"],
        PERM_CATEGORY_CUSTOMER_GETCODE: ["/org", "/cat", "/cus", "/fld"],
        PERM_CATEGORY_CUSTOMER_EXCEL: ["/org", "/cat", "/cus", "/fld"],
        PERM_CATEGORY_CUSTOMER_UPDATE: ["/org", "/cat", "/cus", "/fld"],
        PERM_CATEGORY_CUSTOMER_UNCHECK: ["/org", "/cat", "/cus", "/fld"],
        //quyen chi tiet danh muc khac
        PERM_CATEGORY_ANOTHER_DELETE: ["/kat", "/cat", "/fld"],
        PERM_CATEGORY_ANOTHER: ["/kat", "/cat", "/fld"],
        PERM_CATEGORY_ANOTHER_CREATE: ["/kat", "/cat", "/fld"],
        PERM_CATEGORY_ANOTHER_UPDATE: ["/kat", "/cat", "/fld"],
        //quyen chi tiet dia ban
        PERM_CATEGORY_LOCATION_DELETE: ["/cat", "/loc", "/fld"],
        PERM_CATEGORY_LOCATION: ["/cat", "/loc", "/fld"],
        PERM_CATEGORY_LOCATION_CREATE: ["/cat", "/loc", "/fld"],
        PERM_CATEGORY_LOCATION_UPDATE: ["/cat", "/loc", "/fld"],

        PERM_CREATE_INV_ANOTHER: ["/sea", "/syn"],

        //phan quyen chi tiet TBPH
        PERM_TEMPLATE_REGISTER: ["/ser", "/cat", "/sca", "/sap", "/fld"],
        PERM_TEMPLATE_REGISTER_CREATE: ["/ser", "/cat", "/sca", "/sap", "/fld"],
        PERM_TEMPLATE_REGISTER_APPROVE: ["/ser", "/cat", "/sca", "/sap", "/fld"],
        PERM_TEMPLATE_REGISTER_CANCEL: ["/ser", "/cat", "/sca", "/sap", "/fld"],
        PERM_TEMPLATE_REGISTER_DELETE: ["/ser", "/cat", "/sca", "/sap", "/fld"],

        //phan quyen chi tiet Gan quyen su dung TBPH
        PERM_TEMPLATE_REGISTER_GRANT: ["/ser", "/fld"],
        PERM_TEMPLATE_REGISTER_GRANT_SAVE: ["/ser", "/fld"],
        PERM_TEMPLATE_REGISTER_GRANT_SEARCH: ["/ser", "/fld"],

        //phan quyen chi tiet Gan TBPH theo nguoi dung
        PERM_TEMPLATE_REGISTER_USRGRANT: ["/ser", "/fld"],
        PERM_TEMPLATE_REGISTER_USRGRANT_SAVE: ["/ser", "/fld"],
        PERM_TEMPLATE_REGISTER_USRGRANT_SEARCHNAME: ["/ser", "/fld"],
        PERM_TEMPLATE_REGISTER_USRGRANT_SEARCHSER: ["/ser", "/fld"],

        //quyen chi tiet lap hoa don
        PERM_CREATE_INV_MANAGE: ["/sys", "/kat", "/fld", "/gns", "/cat", "/org", "/sea", "/inv", "/cus", "/dts", "/see", "/ica", "/iwc", "/sav", "/sig", "/rep", "/exc"],
        PERM_CREATE_INV_MANAGE_ISSUE: ["/sys", "/kat", "/fld", "/gns", "/cat", "/org", "/sea", "/inv", "/cus", "/dts", "/see", "/ica", "/iwc", "/sav", "/sig", "/rep", "/exc"],
        PERM_CREATE_INV_MANAGE_SAVE: ["/sys", "/kat", "/fld", "/gns", "/cat", "/org", "/sea", "/inv", "/cus", "/dts", "/see", "/ica", "/iwc", "/sav", "/sig", "/rep", "/exc"],
        PERM_CREATE_INV_MANAGE_VIEW: ["/sys", "/kat", "/fld", "/gns", "/cat", "/org", "/sea", "/inv", "/cus", "/dts", "/see", "/ica", "/iwc", "/sav", "/sig", "/rep", "/exc"],
        //quyen chi tiet lap hoa don tu excel
        PERM_CREATE_INV_EXCEL_READ: ["/sea", "/inv", "/fld"],
        PERM_CREATE_INV_EXCEL_FORM: ["/sea", "/inv", "/fld"],
        PERM_CREATE_INV_EXCEL_SAVE: ["/sea", "/inv", "/fld"],
        PERM_CREATE_INV_EXCEL: ["/sea", "/inv", "/fld"],

        PERM_EDIT_INV_MANAGE: ["/ous", "/inv", "/exc", "/gns", "/sea", "/fld"],
        //phan quyen chi tiet duyet hoa don
        PERM_APPROVE_INV_MANAGE: ["/seq", "/cat", "/app", "/see", "/fld", "/sys", "/inv", "/app", "/sig"],
        PERM_APPROVE_INV_MANAGE_VIEW: ["/seq", "/cat", "/app", "/see", "/fld", "/sys", "/inv", "/app", "/sig"],
        PERM_APPROVE_INV_MANAGE_APPR: ["/seq", "/cat", "/app", "/see", "/fld", "/sys", "/inv", "/app", "/sig"],

        //phan quyen chi tiet cap so
        PERM_CREATE_INV_SEQ: ["/seq", "/cat", "/app", "/see", "/fld", "/sys", "/inv"],
        PERM_CREATE_INV_SEQ_ONE: ["/seq", "/cat", "/app", "/see", "/fld", "/sys", "/inv"],
        PERM_CREATE_INV_SEQ_ALL: ["/seq", "/cat", "/app", "/see", "/fld", "/sys", "/inv"],

        PERM_REDIRECT_IDT: ["/inv"],
        //phan quyen chi tiet bao cao, thong ke
        PERM_REPORT_INV: ["/ous", "/fld", "/sea", "/rpt"],
        PERM_REPORT_INV_EXREP: ["/ous", "/fld", "/sea", "/rpt"],

        //phan quyen chi tiet Tra cuu hoa don
        PERM_SEARCH_INV: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/tra"],
        PERM_SEARCH_INV_SEARCH: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/tra"],
        PERM_SEARCH_INV_EXCEL: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/tra"],
        PERM_SEARCH_INV_VIEW: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/tra"],
        PERM_SEARCH_INV_VIEW_DOWNLOAD_FILE: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/tra"],
        PERM_SEARCH_INV_VIEW_SIGN_PDF: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/tra"],
        PERM_SEARCH_INV_SEARCH_RELATE: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/tra"],
        PERM_SEARCH_INV_VIEWDETAIL_RELATE: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/tra"],
        PERM_SEARCH_INV_DELETE: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/tra"],
        PERM_SEARCH_INV_UPDATE: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/tra"],
        PERM_SEARCH_INV_ADJUST_UPLOAD_FILE: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/tra"],
        PERM_SEARCH_INV_ADJUST_CREATE_MINUTES: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/tra"],
        PERM_SEARCH_INV_ADJUST_SAVE: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/tra"],
        PERM_SEARCH_INV_ADJUST_UNSELECT: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/tra"],
        PERM_SEARCH_INV_ADJUST_ADDADJUST: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/tra"],
        PERM_SEARCH_INV_CANCEL: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/tra"],
        PERM_SEARCH_INV_CANCEL_CREATE_MINUTES: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/tra"],
        PERM_SEARCH_INV_CANCEL_CANCEL: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/tra"],
        PERM_SEARCH_INV_CANCEL_VIEW: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/tra"],
        PERM_SEARCH_INV_REJECT_CANCEL: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/tra"],
        PERM_SEARCH_INV_CONVERT: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/tra"],
        PERM_SEARCH_INV_CONVERT_DOWNLOAD_FILE: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/tra"],
        PERM_SEARCH_INV_CONVERT_PRINT: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/tra"],
        PERM_SEARCH_INV_PRINT: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/tra"],
        PERM_SEARCH_INV_COPPY: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/tra"],
        PERM_SEARCH_INV_DOWNLOAD_MINUTES: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/tra"],
        PERM_SEARCH_INV_VIEW_XML: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/tra"],

        PERM_INVOICE_ADJUST: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/tra", "/adj", "/gns", "/org", "/cus", "/sys", "/adj", "/sig", "/exc"],
        PERM_INVOICE_REPLACE: ["/sea", "/kat", "/see", "/cat", "/inv", "/ous", "/fld", "/iwc", "/gns", "/org", "/dts", "/sav", "/sign", "/rep", "/sys"],
        //phan quyen chi tiet huy hoa don
        PERM_INVOICE_CANCEL: ["/sea", "/cus", "/kat", "/see", "/cat", "/ous", "/inv", "/fld", "/app", "/iwc", "/tra", "/seq", "/ist", "/gns", "/org", "/dts", "/sav", "/sig", "/sys"],
        PERM_INVOICE_CANCEL_VIEW: ["/sea", "/cus", "/kat", "/see", "/cat", "/ous", "/inv", "/fld", "/app", "/iwc", "/tra", "/seq", "/ist", "/gns", "/org", "/dts", "/sav", "/sig", "/sys"],
        PERM_INVOICE_CANCEL_APPR: ["/sea", "/cus", "/kat", "/see", "/cat", "/ous", "/inv", "/fld", "/app", "/iwc", "/tra", "/seq", "/ist", "/gns", "/org", "/dts", "/sav", "/sig", "/sys"],
        PERM_INVOICE_CANCEL_WAIT: ["/sea", "/cus", "/kat", "/see", "/cat", "/ous", "/inv", "/fld", "/app", "/iwc", "/tra", "/seq", "/ist", "/gns", "/org", "/dts", "/sav", "/sig", "/sys"],

        PERM_INVOICE_DELETE: ["/ous", "/sea", "/ica", "/ist", "/iwc", "/app", "/ser", "/inv", "/seg", "/fld"],
        PERM_VOID_WAIT: ["/sea", "/seq", "/cus", "/kat", "/cat", "/app", "/see", "/fld", "/ist"],
        PERM_TEMPLATE_APPROVE: ["/ser", "/sca", "/sap", "/cat", "/fld"],
        PERM_TEMPLATE_VOID: ["/ser", "/sca", "/sap", "/cat", "/fld"],
        PERM_INTEGRATED: ["/ous", "/ser", "/inv", "/sea", "/adj", "/ica", "/rep", "/fld"],
        PERM_CATEGORY_VENDOR: ["/ven", "/fld", "/loc", "/gns", "/fld", "/ven"],
        PERM_CANCEL_ENTRY: ["/ous", "/fld", "/inv", "/ent"],
        PERM_CANCEL_ENTRY_APPR: ["/ous", "/inv", "/fld", "/ent"],
        PERM_MAIL: ["/inv"],
        PERM_MAILS: ["/inv"],
        PERM_SMS: ["/inv"],
        PERM_EXPORT_EXCEL: ["/inv"],
        PERM_INVOICE_STATUS: ["/inv"],
        PERM_INVOICE_DATE: ["/inv"],

        //Hoa don dau vao
        PERM_SEARCH_BINV: ["/inv", "/see", "/cat", "/org"],
        PERM_SEARCH_BINV_READ_XML: ["/inv", "/see", "/cat", "/org"],
        PERM_SEARCH_BINV_LIST_EXP: ["/inv", "/see", "/cat", "/org"],
        PERM_SEARCH_BINV_COPY: ["/inv", "/see", "/cat", "/org"],
        PERM_SEARCH_BINV_XLS: ["/inv", "/see", "/cat", "/org"],
        PERM_SEARCH_BINV_VIEW: ["/inv", "/see", "/cat", "/org"],
        PERM_SEARCH_BINV_MANAGE: ["/inv", "/see", "/cat", "/org"],
        PERM_SEARCH_BINV_DELETE: ["/inv", "/see", "/cat", "/org"],
        PERM_SEARCH_BINV_EDIT: ["/inv", "/see", "/cat", "/org"],

        //Quan ly giao dich
        PERM_TRANS: ["/cat", "/sea", "/see", "/ous", "/fld", "/exc", "/tran", "/org", "/seg", "/inv", "/itr"],
        PERM_TRANS_EXP: ["/cat", "/sea", "/see", "/ous", "/fld", "/exc", "/tran", "/org", "/seg", "/inv", "/itr"],
        PERM_TRANS_JOB_SEARCH: ["/cat", "/sea", "/see", "/ous", "/fld", "/exc", "/tran", "/org", "/seg", "/inv", "/itr"],
        PERM_TRANS_TRANS_ACTIVE: ["/cat", "/sea", "/see", "/ous", "/fld", "/exc", "/tran", "/org", "/seg", "/inv", "/itr"],
        PERM_TRANS_TRANS_INACTIVE: ["/cat", "/sea", "/see", "/ous", "/fld", "/exc", "/tran", "/org", "/seg", "/inv", "/itr"],
        PERM_TRANS_SINGLE_INV: ["/cat", "/sea", "/see", "/ous", "/fld", "/exc", "/tran", "/org", "/seg", "/inv", "/itr"],
        PERM_TRANS_NEW_INV: ["/cat", "/sea", "/see", "/ous", "/fld", "/exc", "/tran", "/org", "/seg", "/inv", "/itr"],
        PERM_TRANS_TRANS_INVS: ["/cat", "/sea", "/see", "/ous", "/fld", "/exc", "/tran", "/org", "/seg", "/inv", "/itr"],
        PERM_TRANS_APPR: ["/cat", "/sea", "/see", "/ous", "/fld", "/exc", "/tran", "/org", "/seg", "/inv", "/itr"],
        PERM_TRANS_APPR_ACCEPT: ["/cat", "/sea", "/see", "/ous", "/fld", "/exc", "/tran", "/org", "/seg", "/inv", "/itr"],
        PERM_TRANS_APPR_REJECT: ["/cat", "/sea", "/see", "/ous", "/fld", "/exc", "/tran", "/org", "/seg", "/inv", "/itr"],

        //phan quyen chi tiet lap hoa don online
        PERM_INV_ONLINE: ["/tra"],
        PERM_INV_ONLINE_INSERT: ["/tra"],

        PERM_ETL: ["/etl"],
        PERM_SYS_SEGMENT: ["/seg"],
        PERM_DEP: ["/dep"],
        PERM_ROLE_DEP: ["/rol"],

        //phan quyen chi tiet ky so tai lieu
        PERM_SIGN: ["/dss"],
        PERM_SIGN_VERIFY: ["/dss"],
        PERM_SIGN_DIGITAL: ["/dss"],
        PERM_SIGN_UPFILE: ["/dss"],
        PERM_SIGN_PDFFILE: ["/dss"],

        //Dang ky su dung dich vu
        PERM_CREATE_STM: ["/stg", "/stm", "/tim"],
        PERM_CREATE_STM_CREATE: ["/stg", "/stm", "/tim"],
        PERM_SEARCH_STM: ["/stg", "/stm", "/tim", "/sta"],
        PERM_SEARCH_STM_SEARCH: ["/stg", "/stm", "/tim", "/sta"],

        //Thong bao sai sot
        PERM_CREATE_WRONGNOTICE: ["/wno", "/tim"],

        //Bang tong hop
        PERM_CREATE_BTH: ["/bth", "/tim"],
        PERM_SEARCH_BTH: ["/bth", "/tim"],
        PERM_APPOVE_BTH: ["/bth", "/tim"],

        //phan quyen chi tiet TBPH ND 123
        PERM_TEMPLATE_REGISTER_123: ["/ser", "/cat", "/sca", "/sap", "/fld", "/tim"],
        PERM_TEMPLATE_REGISTER_CREATE_123: ["/ser", "/cat", "/sca", "/sap", "/fld", "/tim"],
        PERM_TEMPLATE_REGISTER_APPROVE_123: ["/ser", "/cat", "/sca", "/sap", "/fld", "/tim"],
        PERM_TEMPLATE_REGISTER_CANCEL_123: ["/ser", "/cat", "/sca", "/sap", "/fld", "/tim"],
        PERM_TEMPLATE_REGISTER_DELETE_123: ["/ser", "/cat", "/sca", "/sap", "/fld", "/tim"],

        PERM_CREATE_ADJUST_INV: ["/inv"],
        PERM_PRINT_MERGING_INVOICE: ["/inv"],
        REPORT_COMPARE: ["/com"]
    },
    PATH_XML: "//*[local-name(.)='DLHDon']",
    BKHDVAT_EXRA_OPTION: "2", //1 - In mặc định chỉ hóa đơn đã duyệt, 2 - In các hóa đơn hủy, thay thế
    LOGGING_AUDIT_OPT: "1", //1 - Có ghi log audit chức năng, 0 - Không ghi log
    FUNCNAME_LOGGING: [//Danh sách cácc tác vụ ghi log
		{ id: "api", value: "Ứng dụng tích hợp API", logenable:"Y", extfld: [], extfldlbl: []},
																									  
        { id: "user_login", valueclient: "login_sys", value: "Login hệ thống", logenable: "Y", extfld: [], extfldlbl: [] },
        { id: "user_disable", valueclient: "cancel_nsd", value: "Hủy NSD", logenable: "Y", extfld: [], extfldlbl: [] },
        { id: "user_enable", valueclient: "activate_nsd", value: "Kích hoạt NSD", logenable: "Y", extfld: [], extfldlbl: [] },
        //Bổ sung thêm
        { id: "cat_ins", valueclient: "cat_ins", value: "Thêm mới danh mục", logenable: "Y", extfld: [], extfldlbl: [] },
        { id: "cat_upd", valueclient: "cat_upd", value: "Cập nhật danh mục", logenable: "Y", extfld: [], extfldlbl: [] },
        { id: "cat_del", valueclient: "cat_del", value: "Xóa danh mục", logenable: "Y", extfld: [], extfldlbl: [] },
        { id: "exch_post", valueclient: "exch_post", value: "Cập nhật thông tin tỷ giá ngoại tệ", logenable: "Y", extfld: [], extfldlbl: [] },
        { id: "field_post", valueclient: "field_post", value: "Cập nhật thông tin cấu hình thuộc tính các trường trên chức năng", logenable: "Y", extfld: [], extfldlbl: [] },
        { id: "gns_del", valueclient: "gns_del", value: "Xóa danh mục hàng hóa, dịch vụ", logenable: "Y", extfld: [], extfldlbl: [] },
        { id: "gns_upd", valueclient: "gns_upd", value: "Cập nhật danh mục hàng hóa, dịch vụ", logenable: "Y", extfld: [], extfldlbl: [] },
        { id: "gns_ins", valueclient: "gns_ins", value: "Thêm mới danh mục hàng hóa, dịch vụ", logenable: "Y", extfld: [], extfldlbl: [] },
        { id: "loc_post", valueclient: "loc_post", value: "Cập nhật thông tin địa bàn", logenable: "Y", extfld: [], extfldlbl: [] },

        { id: "user_update", valueclient: "upd_info_nsd", value: "Cập nhật thông tin NSD", logenable: "Y", extfld: ["id", "name"], extfldlbl: ["account", "fullname"] },
        { id: "user_insert", valueclient: "add_info_nsd", value: "Thêm thông tin NSD", logenable: "Y", extfld: ["id", "name"], extfldlbl: ["account", "fullname"] },
        { id: "user_get", valueclient: "get_info_nsd", value: "Tra cứu NSD", logenable: "Y", extfld: [], extfldlbl: [] },
        { id: "group_disable", valueclient: "cancel_group", value: "Hủy nhóm NSD", logenable: "Y", extfld: [], extfldlbl: [] },
        { id: "group_enable", valueclient: "activate_group", value: "Kích hoạt nhóm NSD", logenable: "Y", extfld: [], extfldlbl: [] },
        { id: "group_update", valueclient: "update_group", value: "Cập nhật nhóm", logenable: "Y", extfld: ["id", "name"], extfldlbl: ["id", "name"] },
        { id: "group_insert", valueclient: "add_info_group", value: "Thêm thông tin nhóm", logenable: "Y", extfld: ["id", "name"], extfldlbl: ["id", "name"] },
        { id: "group_del_mbr", valueclient: "role_many_groups", value: "Phân quyền nhóm", logenable: "Y", extfld: ["id", "name"], extfldlbl: ["id", "name"] },
        //{ id: "user_dep_mbr", valueclient: "user_dep_mbr", value: "Phân quyền người dùng, phòng ban", logenable: "Y", extfld: ["id", "name"], extfldlbl: ["id", "name"] },
        { id: "group_rolemember", valueclient: "role_group", value: "Phân quyền theo nhóm", logenable: "Y", extfld: ["id"], extfldlbl: ["id", "name"] },
        { id: "group_user_insert", valueclient: "add_info_nsd_2_group", value: "Thêm thông tin NSD vào nhóm", logenable: "Y", extfld: [], extfldlbl: [] },
        { id: "data_user_insert", valueclient: "add_info_nsd_2_data", value: "Thêm thông tin NSD vào dữ liệu", logenable: "Y", extfld: [], extfldlbl: [] },
        { id: "inv_get", valueclient: "invoice_search", value: "Tra cứu hóa đơn", logenable: "Y", extfld: [], extfldlbl: [] },
        { id: "inv_post", valueclient: "invoice_issue", value: "Lập hóa đơn", logenable: "Y", extfld: ["form", "serial", "seq", "c0"], extfldlbl: ["form", "serial", "seq"] },
        { id: "inv_put", valueclient: "edit_invoice", value: "Sửa hóa đơn", logenable: "Y", extfld: ["form", "serial", "seq", "c0"], extfldlbl: ["form", "serial", "seq"] },
        { id: "inv_del", valueclient: "invoice_del_btn", value: "Xóa hóa đơn", logenable: "Y", extfld: ["form", "serial", "seq", "c0"], extfldlbl: ["form", "serial", "seq"] },
        { id: "inv_seqpost", valueclient: "sequence_invoice", value: "Cấp số hóa đơn", logenable: "Y", extfld: ["form", "serial", "seq", "c0"], extfldlbl: ["form", "serial", "seq"] },
        { id: "inv_adj", valueclient: "adj_inv", value: "Điều chỉnh hóa đơn", logenable: "Y", extfld: ["form", "serial", "seq", "c0"], extfldlbl: ["form", "serial", "seq"] },
        { id: "inv_apprall", valueclient: "invoice_approve", value: "Duyệt hóa đơn", logenable: "Y", extfld: ["form", "serial", "seq", "c0"], extfldlbl: ["form", "serial", "seq"] },
        { id: "inv_cancel", valueclient: "invoice_cancel", value: "Hủy hóa đơn", logenable: "Y", extfld: ["form", "serial", "seq", "c0"], extfldlbl: ["form", "serial", "seq"] },
        { id: "inv_replace", valueclient: "rep_inv", value: "Thay thế hóa đơn", logenable: "Y", extfld: ["form", "serial", "seq", "c0"], extfldlbl: ["form", "serial", "seq"] },
        { id: "inv_istatus", valueclient: "convert_status_invoice", value: "Chuyển đổi trạng thái hóa đơn", logenable: "Y", extfld: ["form", "serial", "seq", "c0"], extfldlbl: ["form", "serial", "seq"] },
        { id: "inv_signget", valueclient: "sign_digital_inv", value: "Ký số hóa đơn", logenable: "Y", extfld: ["form", "serial", "seq", "c0"], extfldlbl: ["form", "serial", "seq"] },
        { id: "xls_upl", valueclient: "upload_excel_file_inv", value: "Upload file excel hóa đơn", logenable: "Y", extfld: [], extfldlbl: [] },
        { id: "ser_get", valueclient: "look_up_use_registration", value: "Tra cứu đăng ký sử dụng", logenable: "Y", extfld: [], extfldlbl: [] },
        { id: "ser_ins", valueclient: "create_registration", value: "Tạo đăng ký sử dụng", logenable: "Y", extfld: ["taxc", "form", "serial"], extfldlbl: ["taxcode", "form", "serial"] },
        { id: "ser_upd", valueclient: "fix_usage_registration", value: "Sửa đăng ký sử dụng", logenable: "Y", extfld: ["taxc", "form", "serial"], extfldlbl: ["taxcode", "form", "serial"] },
        { id: "ser_del", valueclient: "delete_registered_use", value: "Xóa đăng ký sử dụng", logenable: "Y", extfld: ["taxc", "form", "serial"], extfldlbl: ["taxcode", "form", "serial"] },
        { id: "ser_apr", valueclient: "browse_registration_for_use", value: "Duyệt đăng ký sử dụng", logenable: "Y", extfld: ["taxc", "form", "serial"], extfldlbl: ["taxcode", "form", "serial"] },
        { id: "ser_cancel", valueclient: "cancel_release_notice", value: "Hủy đăng ký sử dụng", logenable: "Y", extfld: ["taxc", "form", "serial"], extfldlbl: ["taxcode", "form", "serial"] },
        { id: "ser_grant", valueclient: "assign_registration_for_use", value: "Gán đăng ký sử dụng", logenable: "Y", extfld: [], extfldlbl: [] },
        { id: "org_ins", valueclient: "add_info_customer", value: "Thêm mới thông tin khách hàng", logenable: "Y", extfld: ["name"], extfldlbl: ["customer_name"] },
        { id: "org_upd", valueclient: "upd_info_customer", value: "Cập nhật thông tin khách hàng", logenable: "Y", extfld: ["name"], extfldlbl: ["customer_name"] },
        { id: "org_del", valueclient: "del_info_customer", value: "Xóa thông tin khách hàng", logenable: "Y", extfld: ["name"], extfldlbl: ["customer_name"] },
        { id: "ou_ins", valueclient: "add_info_ou", value: "Thêm mới thông tin đơn vị", logenable: "Y", extfld: ["name", "mst"], extfldlbl: ["customer_name", "taxcode"] },
        { id: "ou_upd", valueclient: "upd_info_ou", value: "Cập nhật thông tin đơn vị", logenable: "Y", extfld: ["name", "mst"], extfldlbl: ["customer_name", "taxcode"] },
        { id: "ou_del", valueclient: "del_info_ou", value: "Xóa thông tin đơn vị", logenable: "Y", extfld: ["name", "mst"], extfldlbl: ["customer_name", "taxcode"] },
        //{ id: "inv_date_change", valueclient: "inv_date_change", value: "Sửa ngày hóa đơn", logenable: "Y", extfld: [], extfldlbl: [] },
        //{ id: "inv_status_change", valueclient: "inv_status_change", value: "Sửa trạng thái hóa đơn", logenable: "Y", extfld: [], extfldlbl: [] },
        { id: "wrongnotice_send", valueclient: "wrongnotice_send", value: "Gửi thông báo sai sót", logenable: "Y", extfld: [], extfldlbl: [] },
        { id: "approvalbth", valueclient: "approval_bth", value: "Duyệt bảng tổng hợp", logenable: "Y", extfld: [], extfldlbl: [] },
        { id: "createbth", valueclient: "list_inv_collect", value: "Gom bảng tổng hợp", logenable: "Y", extfld: [], extfldlbl: [] },
        { id: "editbth", valueclient: "edit_bth", value: "Sửa bảng tổng hợp", logenable: "Y", extfld: [], extfldlbl: [] },
        { id: "bth_saveInvAdj", valueclient: "bthwin", value: "Gửi thông tin/ giải trình tổng hợp", logenable: "Y", extfld: [], extfldlbl: [] },
    ],
    SRC_LOGGING: [{ id: "APP", value: "Ứng dụng eInvoice" }, { id: "API", value: "Ứng dụng tích hợp" }], //Tham số các ứng dụng ghi log
    SRC_LOGGING_DEFAULT: "APP", //Ứng dụng ghi log mặc định
    SEQ_LEN: 8, //Độ dài số hóa đơn
    FNC_GEN_SEC: "fpt", //Khai báo tên hàm generate mã bí mật sec, đặt mặc định fpt là gen theo hàm của FPT
    MAX_ROW_EXCEL_EXPORT: 300000, //Số dòng tối đa kết xuất ra excel ở chức năng tra cứu hóa đơn
    MAX_ROW_APPROVE: 5000, //Số hóa đơn tối đa được phép duyệt trên chức năng cho 1 lần duyệt
    SER_USES_APP: "(1,3)", //Kiểu nhập trên dải TBPH được phép sử dụng trên chức năng hóa đơn
    SER_USES_GRANT: "(1,2,3)", //Kiểu nhập trên dải TBPH được phép hiển thị để phân quyền
    MAX_ROW_JOB_GET: 1000, //Số dòng mỗi lần job lấy ra để tạo email và gửi
    MAX_DATE_TO_KEEP_TEMP_ROW: 7, //Số ngày lưu trữ dữ liệu trong bảng tạm phục vụ gửi mail bằng job
    SER_GRANT: "(1,2,3)", //loại ky hieu dc phép grant
    SPECIAL_CHAR: [`\\u0002`],
    MAX_ROW_DELETE_ALL: 100000, //Số lượng bản ghi tối đa được phép xóa trong trường hợp xóa nhiều
    GRIDNFCONF: 4, // Số lượng chữ số thập phân
    DEGREE_CONFIG: "123", //NĐ sử dụng, 119 hoặc 123
    checkCADate: 1, // check ca
    checkStatement: 1 // check statement
}