module.exports = {
  ent: "hlv",
  dbtype: "mysql",
  xls_idt: 1,
  serial_grant: 1,
  serial_usr_grant: 0,					
  ou_grant: 1,
  invoice_seq:1,			  
  ldapPrivate: 0,//1-rieng 0-chung 
  captchaType: 0, // 0-ko dung 1-offline 2-online(Google)  // Chinh ca o file ext.js client
  mail: "FPT.einvoice.OnPrem@fpt.com.vn",
  smtpConfig: { host: "mail.fpt.com.vn", port: 587, secure: false, auth: { user: "FPT.einvoice.OnPrem@fpt.com.vn", pass: "5db0fd40a93fd5887aaff5b407bd62c6982cc9989c60ae76f09dfa0882d5cfa6" } }, //465(SSL),587(TLS)
	ldap: "AD",
  ldapsConfig: { url: "ldaps://10.15.68.113:636" },
  baseDN: "DC=einvoice,DC=internal",
  adminDN: "einvoice\\Administrator",
  adminPW: "04e9d62255f342c16c9978d1c2a9e11a4ae1b792c9f55eb6733f3ebec31cc190",
  searchFilter: "(&(objectCategory=person)(objectclass=user)(sAMAccountName={{username}}))",
  //poolConfig: { user: "appuser", password: "Admin123$", database: "hlv", host: "10.15.119.87", port: 3306},
  poolConfig: { user: "appuser", password: "4e3e71b6dbe12f644dd1858863212e87a6efb82bf2a88bff1cbc574353cc1f75", database: "hlv", host: "10.15.119.87", port: 3306},
  
  redisConfig: { host: "10.15.68.103", port: 6381, password: "70527570f80f4371a82b8b0ac51793813055fbd5858eefde33887ffbb168eba2" },
  useJobSendmail: 1,
  useRedisQueueSendmail: 1,
  upload_minute: 0,
  sendmail_pdf_xml: 0,
  sendApprMail: 1,
  sendCancMail: 1,
  genPassFile: 0,
  config_jsrep: {
      JSREPORT_URL: "http://10.15.68.103:5486/api/report",
      JSREPORT_ACC: "admin",
      JSREPORT_PASS : "8f9d4949a09d66f14f93314dc50855591ed01771cd1d4487382c4dc89bc310a2"
  },
  //api_url:"http://10.15.68.103:3008/api"
  api_url:"http://10.15.68.103:3022/api"
} 
