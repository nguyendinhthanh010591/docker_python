
module.exports = {
    ent: "kbank",
    dbtype: "mssql",
    xls_idt: 0,
    serial_grant: 0,
    serial_usr_grant: 0,
    test_sms_fpt: 1,
    sms_config:{
        tokenEndpoint: "https://api.uat.aia-apps.biz/sso/authenticate",
        sendSmsEndpoint: "https://apis.uat.aia-apps.com/sms/api/messages/send",
        allowSmsFilter: true,
    },
    ou_grant: 0,
    invoice_seq:1, // có chức năng cấp số hay không
    ldapPrivate: 0,//1-rieng 0-chung 
    captchaType: 2, // 0-ko dung 1-offline 2-online(Google)  // Chinh ca o file ext.js client
    mail: "FPT.einvoice.OnPrem@fpt.com.vn",
    smtpConfig: { host: "mail.fpt.com.vn", port: 587, secure: false, auth: { user: "FPT.einvoice.OnPrem@fpt.com.vn", pass: "37cb8399a86b034dc4ae3b8319bff75a56631a52881d9f3059bbfadc557162e6" } }, //465(SSL),587(TLS)
	ldapsConfig: { url: "ldaps://10.15.68.113:636" }, 
    ldap: "AD",
    baseDN: "DC=einvoice,DC=internal",
    adminDN: "einvoice\\Administrator",
    adminPW: "cc5d6411b49a3bc329d2923fb579ec7f648517bcf12d19a3473a8daa69415e37",
    searchFilter: "(&(objectCategory=person)(objectclass=user)(sAMAccountName={{username}}))",
    poolConfig: { user: "einv_owner", password: "06788b699916eb0d07226ec0d45d7209e2be2a7515c8a638411f6c0584234665", database: "kbank_einvoiceapp", server: "10.15.119.150\\EINVPROD1", parseJSON: false, options: { enableArithAbort: true }, requestTimeout : 600000, connectionTimeout: 600000,pool: {
        max: 10,
        min: 0,
        idleTimeoutMillis: 180000
    } }, // admin@123
    poolIntegrationConfig: { user: "einv_owner", password: "bde0f0a3595e73981a31918f264ec07d058a0037b07141bc63b112a42b34960c", database: "kbank_einvoiceapp", server: "10.15.119.150\\EINVPROD1", parseJSON: false, options: { enableArithAbort: true }, requestTimeout : 600000, connectionTimeout: 600000,pool: {
        max: 10,
        min: 0,
        idleTimeoutMillis: 180000
    } },
    redisConfig: { host: "10.15.119.161", port: 6381, password: "ae7ede594c1a9f92244f76131d94abcf239d0d329a31a53e46b5ff2a4c112a60" },
    scheduleslist:[
        {schedulename: "Send Approve and Cancelling mail from temporary table", scheduleinterval: '0 * * * * *', schedulemethod:`schedule.sendEmailApprInv()`, schedulestatus: 1},
        {schedulename: "Delete data using to send mail from temporary table", scheduleinterval: '0 22 * * *', schedulemethod:`schedule.delTempDocMail()`, schedulestatus: 1},
        {schedulename: "Send SOC report detail", scheduleinterval: '0 8 * * * *', schedulemethod:`schedule.socD()`, schedulestatus: 1},
        {schedulename: "Send SOCH report", scheduleinterval: '1 8 * * * *', schedulemethod:`schedule.socH()`, schedulestatus: 1},
        {schedulename: "Send SCO2 user", scheduleinterval: '2 8 * * * *', schedulemethod:`schedule.SCO2_user()`, schedulestatus: 1},
        {schedulename: "Send SCO2 group", scheduleinterval: '3 8 * * * *', schedulemethod:`schedule.SCO2_group()`, schedulestatus: 1},
        { schedulename: "Send to api tvan", scheduleinterval: '*/10 * * * * *', schedulemethod: `schedule.sendToTvan('SENDTVAN', 300)`, schedulestatus: 1 },
        { schedulename: "Delete expiring running job", scheduleinterval: '0 */5 * * * *', schedulemethod: `schedule.DropRunningJobExpire('SENDTVAN')`, schedulestatus: 1 },// bật job chạy để ko bị lỗi ko gửi email do tồn tại key cũ đã hết hạn trong bảng s_listvaluess
    ],
	ftpConfig: { host: "10.15.119.131", port: 21, user: "einvoice", password: "06788b699916eb0d07226ec0d45d7209e2be2a7515c8a638411f6c0584234665"},
    ftpdestPath: "/home/einvoice/ftp/einvoiceprem/",
    useFtpAttach: 1,
    upload_minute: 1,
    sendmail_pdf_xml: 1,
    sendApprMail: 1,
    sendCancMail: 1,
    genPassFile: 0,
    not_sendmail_pass: 1,
    genpassrule: { length: 10, numbers: true, symbols: true, lowercase: true, uppercase: true, strict: true },
    path_zip: 'C:/test/',
    sendApprSMS: 1,
    sendCancSMS: 1,
    useJobSendmail: 1,
    useRedisQueueSendmail: 1,
    config_jsrep: {
        JSREPORT_URL: "http://10.15.68.103:5486",
        JSREPORT_ACC: "admin",
        JSREPORT_PASS : "2ea6f6952c60f44a04b5312d1d594a7fccf79b444881665e6eaf179111cd819a",
        jsreportrender: 1
    },
    log4jsConfig: {
        appenders: {
            einvoice: { type: 'console', filename: 'F:\logeinvoice\einvoice.log', maxLogSize: 10485760, backups: 60, compress: true }
        },
        categories: {
            default: { appenders: ['einvoice'], level: 'debug' }
        },
        pm2: true
    },
    useAzureAD: false,
    url_tvan: "http://localhost:5555/",
}

