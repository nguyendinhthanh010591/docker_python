module.exports = {
    ent: "aia",
    dbtype: "mssql",
    xls_idt: 0,
    serial_grant: 0,
    serial_usr_grant: 0,
    test_sms_fpt: 1,
    keyvault_config: {
        uri: "https://kv-vn01-sea-u-sms01.vault.azure.net",
        client_id: "[AIA will key in]",
        client_key: "[AIA will key in]",
        tenant_id: "[AIA will key in]"
    },
    sms_config:{
        tokenEndpoint: "https://api.uat.aia-apps.biz/sso/authenticate",
        sendSmsEndpoint: "https://apis.uat.aia-apps.com/sms/api/messages/send",
        allowSmsFilter: true,
    },
    ou_grant: 0,
    invoice_seq:0, // có chức năng cấp số hay không
    user_adfs: 0,//C� d�ng ADFS hay kh�ng
    ldapPrivate: 1,//1-rieng 0-chung 
    captchaType: 0, // 0-ko dung 1-offline 2-online(Google)  // Chinh ca o file ext.js client
    tran_url: "https://10.50.139.9:8253/core/v1.1/einvoice/transactions/seqno/{tran_no}?module={module}&trandate={trandate}",
    cif_url: "https://10.50.139.9:8253/core/cif/{cifno}",
    sms_url: "https://10.50.139.9:8253/sms/v11/message/{mobileno}/send",
    sms_auth: "Basic ZWVudm9pY2U6dmliMTIz",
    sms_servicecode: "EB001",
    ecm_config: {
        url: "http://localhost:18080/alfresco/cmisbrowser",
        username: "hunglq",
        password: "hunglq",
    },
	hsm_url: "http://10.15.68.113:8080/fpt-sighub/services/signXML",
    hsm_auth: "c3ByaW5nOnh4IyNAMTIzNDU2Nzg5QDIwMTQwNzI5",
    mailcc: "hunglq19@fpt.com.vn",    
    mailbcc: "hunglq19@fpt.com.vn",
    mail: "FPT.einvoice.OnPrem@fpt.com.vn",
    smtpConfig: { host: "mail.fpt.com.vn", port: 587, secure: false, auth: { user: "FPT.einvoice.OnPrem@fpt.com.vn", pass: "04e9d62255f342c16c9978d1c2a9e11a4ae1b792c9f55eb6733f3ebec31cc190" } }, //465(SSL),587(TLS)
    ldap: "OPENLDAP",
    ldapsConfig: { url: "ldaps://10.15.68.56:636" },
    baseDN: "ou=People,dc=tlg,dc=com",
    adminDN: "cn=ldapadm,dc=tlg,dc=com",
    adminPW: "56e4a818ec215194bda946aabcec735608aac35e11420a07c76b0444ce8e3b55",
    useJobSendmail: 0,
    useRedisQueueSendmail: 0,
    ftpConfig: { host: "10.15.68.113", port: 21, user: "einv-ftp", password: "Admin@123"},
    ftpdestPath: "C:/Einvoice_FTP/einv-ftp/",
    searchFilter: "(&(objectClass=inetOrgPerson)(uid={{username}}))",
    passportsaml: {
      strategy: 'saml',
      saml: {
        callbackUrl: 'https://10.15.68.212/api/signinsaml/callback',
        //protocol: "https://",
        //path: config.passport.saml.path,
        entryPoint: 'https://adfs.einvoice.internal/adfs/ls',
        issuer: 'https://10.15.68.212',
        cert: null,
        //authnContext: 'http://schemas.microsoft.com/ws/2008/06/identity/authenticationmethod/windows',
        acceptedClockSkewMs: -1,
        identifierFormat: null
        //disableRequestedAuthnContext:false,
        //authnContext: 'http://schemas.microsoft.com/ws/2008/06/identity/authenticationmethod/password'
      }
    },
    poolConfig: { user: "einv_owner", password: "56e4a818ec215194bda946aabcec735608aac35e11420a07c76b0444ce8e3b55", database: "aia_einvoiceapp", server: "10.15.119.167", parseJSON: false, options: { enableArithAbort: true }, requestTimeout : 600000, connectionTimeout: 600000,pool: {
        max: 10,
        min: 0,
        idleTimeoutMillis: 180000
    } },
    poolConfigdds: { user: "59f082f5c54840059ab8371a5989b9ab1c01050c548c52f37150db72c8437fe3",
     password: "59f082f5c54840059ab8371a5989b9ab1c01050c548c52f37150db72c8437fe3",
      database: "83344a3ffb9374408bbb70f4ad0e361dbafb796a9e069ac9a16de44499aad25c", 
      server: "7edd3be79e0c22165f154218b1cc018e8264080fda7f9f70cca54cbe5f9c75bf", 
      parseJSON: false, options: { enableArithAbort: true }, requestTimeout : 600000, connectionTimeout: 600000,pool: {
        max: 10,
        min: 0,
        idleTimeoutMillis: 180000
    } }, //UAT 
    // redisConfig: { host: "10.15.68.103", port: 6390, password: "redis@6390" }
    redisConfig: { host: "10.15.119.59", port: 7380, password: "d9e03ab3f0347906ee5723027b8e4651de002c550ccd754a282ab41c4a759e76" },
    upload_minute: 1,
    sendmail_pdf_xml: 1,
    sendApprMail: 1,
    sendCancMail: 1,
    genPassFile: 0,
    sendApprSMS: 0,
    sendCancSMS: 0,
    path_zip  : 'C:/test/',
    xls_data: 1,
    chk_mst: 0, //0: khong kiem tra, 1: kiem tra
    config_chietkhau_col:"Y",//tham so khai chiet khau hoa don phan dtl co gia trị là hien thi, ko co là an
    useFtpAttach: 0,
    To01GTKTApproverMail: "hunglqfpt@gmail.com",
    To03XKNBApproverMail: "hunglq19@fpt.com.vn",
    subjectApproverMail: "The following invoice has been assigned to you.",
    htmlApproverMail: `<!DOCTYPE html>
    <html>
    <head>
    <style>
    table, th, td {
      border: 1px solid white;
      line-height: 2;
    }
    </style>
    </head>
    <body>
    
    <table style="width:100%" border="10px">
      <tr>
        <th colspan="2" style="text-align: left">The following invoice has been assigned to you.</th>
      </tr>
      <tr>
        <td style="width:30%">Invoice Number</td>
        <td>#seq#</td>
      </tr>
      <tr>
        <td>Customer Name</td>
        <td>#bname#</td>
      </tr>
      <tr>
        <td>Total Amout</td>
        <td>#total#</td>
      </tr>
      <tr>
        <td>Assigned By</td>
        <td>#uc#</td>
      </tr>
      <tr>
        <td colspan="2"><b>Link To Invoice Processing</b></td>
      </tr>
      <tr>
        <td colspan="2"><a href="https://einvoice-mhvn.moethennessy.com/">https://einvoice-mhvn.moethennessy.com/</a></td>
      </tr>
    </table>
    
    </body>
    </html>`,
    ToDailyMail: "tuanna2@fpt.com.vn;giangltc@fpt.com.vn;",
    subjectDailyMail: "The following invoice has been assigned to you.",
    htmlDailyMail: `<!DOCTYPE html>
    <html>
    <head>
    <style>
    table, th, td {
      border: 1px solid white;
      line-height: 2;
    }
    </style>
    </head>
    <body>
    
    <table style="width:100%" border="10px">
      <tr>
        <th colspan="2" style="text-align: left">The following invoice has been assigned to you.</th>
      </tr>
      <tr>
        <td style="width:30%">Invoice Number</td>
        <td>#seq#</td>
      </tr>
      <tr>
        <td>Customer Name</td>
        <td>#bname#</td>
      </tr>
      <tr>
        <td>Total Amout</td>
        <td>#total#</td>
      </tr>
      <tr>
        <td>Assigned By</td>
        <td>#uc#</td>
      </tr>
      <tr>
        <td colspan="2"><b>Link To Invoice Processing</b></td>
      </tr>
      <tr>
        <td colspan="2"><a href="https://einvoice-mhvn.moethennessy.com/">https://einvoice-mhvn.moethennessy.com/</a></td>
      </tr>
    </table>
    
    </body>
    </html>`,
    evm:'FPT',
    onelogin: 1,
    config_jsrep: {
        JSREPORT_URL: "http://10.15.68.103:5489/api/report",
        JSREPORT_ACC: "admin",
        JSREPORT_PASS : "8f9d4949a09d66f14f93314dc50855591ed01771cd1d4487382c4dc89bc310a2"
    }
    
}
