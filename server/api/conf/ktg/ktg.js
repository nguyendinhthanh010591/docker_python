
module.exports = {
    ent: "onprem",
    dbtype: "mssql",
   
    xls_idt: 0,
    xls_ccy: 1,
    config_chietkhau_col: 1,
    serial_grant: 0,
    serial_usr_grant: 0,
    test_sms_fpt: 1,
    sms_config: {
        tokenEndpoint: "https://api.uat.aia-apps.biz/sso/authenticate",
        sendSmsEndpoint: "https://apis.uat.aia-apps.com/sms/api/messages/send",
        allowSmsFilter: true,
    },
    bth_length:10000,
    ou_grant: 0,
    invoice_seq: 1, // có chức năng cấp số hay không
    ldapPrivate: 0,//1-rieng 0-chung 
    captchaType: 0, // 0-ko dung 1-offline 2-online(Google)  // Chinh ca o file ext.js client
    mail: "einvoice@npllogistics.com",
    smtpConfig: { host: "mail.npllogistics.com", port: 587, secure: false, auth: { user: "einvoice@npllogistics.com", pass: "59a363ee5316fef586a2cf18be5c852fabe9bc59c0c0c6f1e8a142b6eba290e5" } }, //465(SSL),587(TLS)
    ldapsConfig: { url: "ldaps://10.15.68.113:636" },
    ldap: "AD",
    baseDN: "DC=einvoice,DC=internal",
    adminDN: "einvoice\\Administrator",
    adminPW: "cc5d6411b49a3bc329d2923fb579ec7f648517bcf12d19a3473a8daa69415e37",
    searchFilter: "(&(objectCategory=person)(objectclass=user)(sAMAccountName={{username}}))",
   poolConfig: { user: "einv_owner", password: "06788b699916eb0d07226ec0d45d7209e2be2a7515c8a638411f6c0584234665", database: "ktg_einvoice_uat", server: "10.15.119.150\\MSSQL2017", parseJSON: false, options: { enableArithAbort: true }, requestTimeout : 600000, connectionTimeout: 600000,pool: {
        max: 10,
        min: 0,
        idleTimeoutMillis: 180000
		 
    } }, // admin@123
   
    redisConfig: { host: "10.15.119.131", port: 6380, password: "ae7ede594c1a9f92244f76131d94abcf239d0d329a31a53e46b5ff2a4c112a60" },
    scheduleslist: [
        { schedulename: "Send Approve and Cancelling mail from temporary table", scheduleinterval: '0 * * * * *', schedulemethod: `schedule.sendEmailApprInv('SENDEMAILAPPRINV', 300)`, schedulestatus: 1 },
        { schedulename: "Send to api tvan", scheduleinterval: '*/10 * * * * *', schedulemethod: `schedule.sendToTvan('SENDTVAN', 300)`, schedulestatus: 1 },
        { schedulename: "Delete expiring running job", scheduleinterval: '0 */5 * * * *', schedulemethod: `schedule.DropRunningJobExpire('SENDTVAN')`, schedulestatus: 1 },// bật job chạy để ko bị lỗi ko gửi email do tồn tại key cũ đã hết hạn trong bảng s_listvalues
        { schedulename: "Delete expiring running job", scheduleinterval: '0 */5 * * *', schedulemethod: `schedule.DropRunningJobExpire('SENDEMAILAPPRINV')`, schedulestatus: 1 },// bật job chạy để ko bị lỗi ko gửi email do tồn tại key cũ đã hết hạn trong bảng s_listvalues
       
        {schedulename: "Delete data using to send mail from temporary table", scheduleinterval: '0 22 * * *', schedulemethod:`schedule.delTempDocMail()`, schedulestatus: 0},
        {schedulename: "Send SOC report detail", scheduleinterval: '0 8 * * * *', schedulemethod:`schedule.socD()`, schedulestatus: 0},
        {schedulename: "Send SOCH report", scheduleinterval: '1 8 * * * *', schedulemethod:`schedule.socH()`, schedulestatus: 0},
        {schedulename: "Send SCO2 user", scheduleinterval: '2 8 * * * *', schedulemethod:`schedule.SCO2_user()`, schedulestatus: 0},
        {schedulename: "Send SCO2 group", scheduleinterval: '3 8 * * * *', schedulemethod:`schedule.SCO2_group()`, schedulestatus: 0},
																																													 
        {schedulename: "Remind user to change password", scheduleinterval: '0 * * * * *', schedulemethod:`schedule.pwdExpirationReminder()`, schedulestatus: 0}
    ],
    ftpConfig: { host: "10.15.119.131", port: 21, user: "einvoice", password: "06788b699916eb0d07226ec0d45d7209e2be2a7515c8a638411f6c0584234665" },
    ftpdestPath: "/home/einvoice/ftp/einvoiceprem/",
    useFtpAttach: 1,
    upload_minute: 1,/*
    config_7zip: { //Cấu hình liên quan đến việc zip file attach thành file 7z
        PathSave7zipFile: "C:/Temp/7zip/", //Thư mục lưu file 7z sau khi tạo xong
        PathFileToZip: "C:/Temp/7zip/" //Thư mục lưu tạm file PDF, XML phục vụ cho việc zip thành file 7z
    },*/
    sendmail_pdf_xml: 1,
    sendApprMail: 1,
    sendCancMail: 1,
    genPassFile: 0,
    not_sendmail_pass: 0,
    genpassrule: { length: 10, numbers: true, symbols: true, lowercase: true, uppercase: true, strict: true },
    path_zip: 'C:/test/',
    sendApprSMS: 1,
    sendCancSMS: 1,
    useJobSendmail: 1,
    useRedisQueueSendmail: 0,
    config_jsrep: {
        JSREPORT_URL: "http://10.15.68.103:5486/api/report",
        JSREPORT_ACC: "admin",
        JSREPORT_PASS: "2ea6f6952c60f44a04b5312d1d594a7fccf79b444881665e6eaf179111cd819a"
    }/*,
    https_server_option: {
    key: 'E:/Temp/ssl/cert.key',
    cert: 'E:/Temp/ssl/cert.crt'

    }*/,
    apprSubjectMail: "HÓA ĐƠN ĐIỆN TỬ CỦA {{sname}}",
    canSubjectMail: "HÓA ĐƠN ĐIỆN TỬ CỦA {{sname}}",
    //useJobSendmailAPI : 1,
    //useRedisQueueSendmailAPI: 1,    
    days_left_active_cts: 30,
    days_change_pass: 90, //Số ngày bắt buộc phải đổi mật khẩu, sử dụng cho user local
    days_warning_change_pass: 10, //Số ngày bắt bắt đầu gửi cảnh báo mật khẩu, sử dụng cho user local
    total_pass_store: 10, //Số lần lưu mật khẩu để check trùng lặp mật khẩu cũ
    is_use_local_user: 0, //Có dùng user lưu trữ trên local DB hay không
    local_password_expire: 1, //Thời gian mật khẩu expire, tính theo ngày
    disable_worker: 0, // Tắt worker hay không
    dbCache: 1,
    margin_footer_form: 1, // 1 không margin footer,0 margin theo các mẫu chung là 5mm toàn mẫu các con cũ mặc định là 0
    customForm: 'KIMTIN', // chỉ dung cho kim tin
    log4jsConfig: {
        appenders: {
            einvoice: { type: 'file', filename: '/home/einvoice/appslog/einvoice-mssql.log', maxLogSize: 10485760, backups: 60, compress: true }
        },
        categories: {
            default: { appenders: ['einvoice'], level: 'debug' }
        },
        pm2: true
    },
    url_tvan: "http://10.15.119.131:3312/",
    tvanConfig: {
        headers: {
            'Authorization': 'Basic T05QUkVNOk9ucHJlbTEyMw==',
            'Content-Type': 'application/json'
        },
        urls: [
            {
                //Dang ky su dung dich vu
                msgtype: "REG",
                method: 'post',
                url: 'http://118.71.250.233/ftvan-hddt/dkyhddt/dkysdung'
            },
            {
                //Gui hoa don co ma
                msgtype: "INVHASCODE",
                method: 'post',
                url: 'http://118.71.250.233/ftvan-hddt/hdon/cmahdon'
            },
            {
                //Gui hoa don khong ma
                msgtype: "INVNONECODE",
                method: 'post',
                url: 'http://118.71.250.233/ftvan-hddt/hdon/hdonkma'
            },
            {
                //Gui hoa don khong ma
                msgtype: "INVNONECODE",
                method: 'post',
                url: 'http://118.71.250.233/ftvan-hddt/hdon/hdonkma'
            }
        ]
    }
}

