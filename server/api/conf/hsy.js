module.exports = {
    ent: "hsy",
    dbtype: "mysql",
    xls_idt: 1,
    serial_grant: 0,
    serial_usr_grant: 0,
    ou_grant: 0,
    user_adfs: 0,//C� d�ng ADFS hay kh�ng
    ldapPrivate: 1,//1-rieng 0-chung 
    captchaType: 2, // 0-ko dung 1-offline 2-online(Google)  // Chinh ca o file ext.js client
    mail: "FPT.einvoice.OnPrem@fpt.com.vn",
    smtpConfig: { host: "mail.fpt.com.vn", port: 587, secure: false, auth: { user: "FPT.einvoice.OnPrem@fpt.com.vn", pass: "5db0fd40a93fd5887aaff5b407bd62c6982cc9989c60ae76f09dfa0882d5cfa6" } }, //465(SSL),587(TLS)
    /*
    ldap: "AD", 
    ldapsConfig: { url: "ldaps://10.15.68.58:636" },
    baseDN: "CN=Users,DC=invoice,DC=fpt,DC=com,DC=vn",
    adminDN: "invoice\\Administrator",
    adminPW: "admin@123",
    searchFilter: "(&(objectCategory=person)(objectclass=user)(sAMAccountName={{username}}))",
    */
    ldap: "OPENLDAP",
    ldapsConfig: { url: "ldaps://10.15.68.56:636" },
    baseDN: "ou=People,dc=tlg,dc=com",
    adminDN: "cn=ldapadm,dc=tlg,dc=com",
    adminPW: "56e4a818ec215194bda946aabcec735608aac35e11420a07c76b0444ce8e3b55",
    searchFilter: "(&(objectClass=inetOrgPerson)(uid={{username}}))",
    passportsaml: {
      strategy: 'saml',
      saml: {
        callbackUrl: 'https://10.15.68.212/api/signinsaml/callback',
        //protocol: "https://",
        //path: config.passport.saml.path,
        entryPoint: 'https://adfs.einvoice.internal/adfs/ls',
        issuer: 'https://10.15.68.212',
        cert: null,
        //authnContext: 'http://schemas.microsoft.com/ws/2008/06/identity/authenticationmethod/windows',
        acceptedClockSkewMs: -1,
        identifierFormat: null
        //disableRequestedAuthnContext:false,
        //authnContext: 'http://schemas.microsoft.com/ws/2008/06/identity/authenticationmethod/password'
      }
    },
    poolConfig: { user: "appuser", password: "4e3e71b6dbe12f644dd1858863212e87a6efb82bf2a88bff1cbc574353cc1f75", database: "hennessy", host: "10.15.68.212", port: 3306},
    redisConfig: { host: "10.15.68.212", port: 6381, password: "d9e03ab3f0347906ee5723027b8e4651de002c550ccd754a282ab41c4a759e76" },
    scheduleslist:[
      {schedulename: "Send Approve and Cancelling mail from temporary table", scheduleinterval: '0 * * * * *', schedulemethod:`schedule.sendEmailApprInv()`, schedulestatus: 1},
      {schedulename: "Delete data using to send mail from temporary table", scheduleinterval: '0 22 * * *', schedulemethod:`schedule.delTempDocMail()`, schedulestatus: 1},
      {schedulename: "Mail report daily", scheduleinterval: '0 1 * * *', schedulemethod:`schedule.dailymailhsy()`, schedulestatus: 1}
    ],
    upload_minute: 0,
    sendmail_pdf_xml: 1,
    sendApprMail: 1,
    sendCancMail: 0,
    genPassFile: 0,
    useJobSendmail: 0,
    useFtpAttach: 0,
    To01GTKTApproverMail: "hunglqfpt@gmail.com",
    To03XKNBApproverMail: "hunglq19@fpt.com.vn",
    subjectApproverMail: "The following invoice has been assigned to you.",
    htmlApproverMail: `<!DOCTYPE html>
    <html>
    <head>
    <style>
    table, th, td {
      border: 1px solid white;
      line-height: 2;
    }
    </style>
    </head>
    <body>
    
    <table style="width:100%" border="10px">
      <tr>
        <th colspan="2" style="text-align: left">The following invoice has been assigned to you.</th>
      </tr>
      <tr>
        <td style="width:30%">Invoice Number</td>
        <td>#seq#</td>
      </tr>
      <tr>
        <td>Customer Name</td>
        <td>#bname#</td>
      </tr>
      <tr>
        <td>Total Amout</td>
        <td>#total#</td>
      </tr>
      <tr>
        <td>Assigned By</td>
        <td>#uc#</td>
      </tr>
      <tr>
        <td colspan="2"><b>Link To Invoice Processing</b></td>
      </tr>
      <tr>
        <td colspan="2"><a href="https://einvoice-mhvn.moethennessy.com/">https://einvoice-mhvn.moethennessy.com/</a></td>
      </tr>
    </table>
    
    </body>
    </html>`,
    ToDailyMail: "giangltc@fpt.com.vn;",
    subjectDailyMail: "Daily sale report #dt#",
    htmlDailyMail: `<!DOCTYPE html>
    <html>
    <head>
    <style>
    table, th, td {
      border: 1px solid white;
      line-height: 2;
    }
    </style>
    </head>
    <body>
    
    <table style="width:100%" border="10px">
      <tr>
        <th colspan="2" style="text-align: left">Dear all, <br>Here is the daily sale report of day #dt#.</th>
      </tr>
      <tr>
        <td colspan="2"><a href="https://einvoice-mhvn.moethennessy.com/">https://einvoice-mhvn.moethennessy.com/</a></td>
      </tr>
    </table>
    
    </body>
    </html>`,
    config_jsrep: {
        JSREPORT_URL: "http://10.15.68.103:5489/api/report",
        JSREPORT_ACC: "admin",
        JSREPORT_PASS : "8f9d4949a09d66f14f93314dc50855591ed01771cd1d4487382c4dc89bc310a2"
    }
}