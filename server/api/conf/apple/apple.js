
module.exports = {
    ent: "apple",
    dbtype: "mysql",
    xls_idt: 0,
    serial_grant: 0,
    serial_usr_grant: 0,
    test_sms_fpt: 1,
    sms_config:{
        tokenEndpoint: "https://api.uat.aia-apps.biz/sso/authenticate",
        sendSmsEndpoint: "https://apis.uat.aia-apps.com/sms/api/messages/send",
        allowSmsFilter: true,
    },
    ou_grant: 0,
    invoice_seq:1, // có chức năng cấp số hay không
    ldapPrivate: 0,//1-rieng 0-chung 
    captchaType: 0, // 0-ko dung 1-offline 2-online(Google)  // Chinh ca o file ext.js client
    mail: "FPT.einvoice.OnPrem@fpt.com.vn",
    smtpConfig: { host: "mail.fpt.com.vn", port: 587, secure: false, auth: { user: "FPT.einvoice.OnPrem@fpt.com.vn", pass: "37cb8399a86b034dc4ae3b8319bff75a56631a52881d9f3059bbfadc557162e6" } }, //465(SSL),587(TLS)
	ldapsConfig: { url: "ldaps://10.15.68.113:636" }, 
    ldap: "AD",
    baseDN: "DC=einvoice,DC=internal",
    adminDN: "einvoice\\Administrator",
    adminPW: "cc5d6411b49a3bc329d2923fb579ec7f648517bcf12d19a3473a8daa69415e37",
    searchFilter: "(&(objectCategory=person)(objectclass=user)(sAMAccountName={{username}}))",
    poolConfig: { user: "appuser", password: "32d67029d6b7e88e3d1870bc1087c87576ed82b4c572bd49b762754a58620f6a", database: "apple_einvoice", host: "10.15.68.212", port: 3306},//Admin123$
    //redisConfig: { host: "10.15.119.131", port: 6382, password: "ae7ede594c1a9f92244f76131d94abcf239d0d329a31a53e46b5ff2a4c112a60" },
    scheduleslist:[
        { schedulename: "Send to api tvan", scheduleinterval: '*/10 * * * * *', schedulemethod: `schedule.sendToTvan('SENDTVAN', 300)`, schedulestatus: 1 },//10s 1 lân
        { schedulename: "Delete expiring running job", scheduleinterval: '0 */12 * * *', schedulemethod: `schedule.DropRunningJobExpire('SENDTVAN')`, schedulestatus: 1 },// bật job chạy để ko bị lỗi ko gửi email do tồn tại key cũ đã hết hạn trong bảng s_listvalues
        { schedulename: "Send Approve and Cancelling mail from temporary table", scheduleinterval: '0 * * * * *', schedulemethod: `schedule.sendEmailApprInv('SENDEMAILAPPRINV', 300)`, schedulestatus: 1 },
        { schedulename: "Delete expiring running job", scheduleinterval: '0 * * * * *', schedulemethod: `schedule.DropRunningJobExpire('SENDEMAILAPPRINV')`, schedulestatus: 1 },
        { schedulename: "Delete data using to send mail from temporary table", scheduleinterval: '0 22 * * *', schedulemethod: `schedule.delTempDocMail()`, schedulestatus: 1 },
        {schedulename: "Remind user to change password", scheduleinterval: '0 22 * * *', schedulemethod:`schedule.pwdExpirationReminder()`, schedulestatus: 1}
    ],
	ftpConfig: { host: "10.15.119.131", port: 21, user: "einvoice", password: "06788b699916eb0d07226ec0d45d7209e2be2a7515c8a638411f6c0584234665"},
    ftpdestPath: "/home/einvoice/ftp/einvoiceprem/",
    useFtpAttach: 1,
    upload_minute: 1,/*
    config_7zip: { //Cấu hình liên quan đến việc zip file attach thành file 7z
        PathSave7zipFile: "C:/Temp/7zip/", //Thư mục lưu file 7z sau khi tạo xong
        PathFileToZip: "C:/Temp/7zip/" //Thư mục lưu tạm file PDF, XML phục vụ cho việc zip thành file 7z
    },*/
    sendmail_pdf_xml: 1,
    sendApprMail: 1,
    sendCancMail: 1,
    genPassFile: 0,
    not_sendmail_pass: 0,
    genpassrule: { length: 10, numbers: true, symbols: true, lowercase: true, uppercase: true, strict: true },
    path_zip: 'C:/test/',
    sendApprSMS: 1,
    sendCancSMS: 1,
    useJobSendmail: 1,
    useRedisQueueSendmail: 1,
    config_jsrep: {
        JSREPORT_URL: "http://10.15.68.103:5486/api/report",
        JSREPORT_ACC: "admin",
        JSREPORT_PASS : "2ea6f6952c60f44a04b5312d1d594a7fccf79b444881665e6eaf179111cd819a"
    },/*
    https_server_option: {
    key: 'E:/Temp/ssl/cert.key',
    cert: 'E:/Temp/ssl/cert.crt'

    },*/
    apprSubjectMail: "HÓA ĐƠN ĐIỆN TỬ CỦA NGÂN HÀNG TMCP BẢO VIỆT (BAOVIET Bank E-invoice)",
    canSubjectMail: "HÓA ĐƠN ĐIỆN TỬ CỦA NGÂN HÀNG TMCP BẢO VIỆT (BAOVIET Bank E-invoice)",
    //useJobSendmailAPI : 1,
    //useRedisQueueSendmailAPI: 1,
    range_usage_serial: 0.8,
    days_change_pass: 90, //Số ngày bắt buộc phải đổi mật khẩu, sử dụng cho user local
    days_warning_change_pass: 10, //Số ngày bắt bắt đầu gửi cảnh báo mật khẩu, sử dụng cho user local
    total_pass_store: 10, //Số lần lưu mật khẩu để check trùng lặp mật khẩu cũ
    is_use_local_user: 1, //Có dùng user lưu trữ trên local DB hay không
    local_password_expire: 1, //Thời gian mật khẩu expire, tính theo ngày
    disable_worker: 0, // Tắt worker hay không
    dbCache: 1,
    log4jsConfig: {
        appenders: {
            einvoice: { type: 'file', filename: '/u01/apps/apple/einvoice-app/logs/einvoice-mysql.log', maxLogSize: 10485760, backups: 60, compress: true }
        },
        categories: {
            default: { appenders: ['einvoice'], level: 'debug' }
        },
        pm2: true
    },
    url_tvan: "http://10.15.68.212:3333/",
    tvanConfig: {
        headers: {
            'Authorization': 'Basic T05QUkVNOk9ucHJlbTEyMw==',
            'Content-Type': 'application/json'
        },
        urls: [
            {
                //Dang ky su dung dich vu
                msgtype: "REG",
                method: 'post',
                url: 'http://118.71.250.233/ftvan-hddt/dkyhddt/dkysdung'
            },
            {
                //Gui hoa don co ma
                msgtype: "INVHASCODE",
                method: 'post',
                url: 'http://118.71.250.233/ftvan-hddt/hdon/cmahdon'
            },
            {
                //Gui hoa don khong ma
                msgtype: "INVNONECODE",
                method: 'post',
                url: 'http://118.71.250.233/ftvan-hddt/hdon/hdonkma'
            },
            {
                //Gui hoa don khong ma
                msgtype: "INVNONECODE",
                method: 'post',
                url: 'http://118.71.250.233/ftvan-hddt/hdon/hdonkma'
            }
        ]  
    }
}

