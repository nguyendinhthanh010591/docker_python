module.exports = {
    ent: "ghtk",
    dbtype: "mysql",
    xls_idt: 0,
    xls_data: 1,
    serial_grant: 1,
    serial_usr_grant: 0,
    ou_grant: 0,
    ldapPrivate: 1,//1-rieng 0-chung 
    captchaType: 0, // 0-ko dung 1-offline 2-online(Google)  // Chinh ca o file ext.js client
    mail: "FPT.einvoice.OnPrem@fpt.com.vn",
    smtpConfig: { host: "mail.fpt.com.vn", port: 587, secure: false, auth: { user: "FPT.einvoice.OnPrem@fpt.com.vn", pass: "ae66d44064b145ea073d9698d5de3f866015840074444fdc32f10953078b4ef3" } }, //465(SSL),587
    ldap: "OPENLDAP",
    ldapsConfig: { url: "ldaps://10.15.119.161:636" },
    baseDN: "ou=People,dc=lg,dc=com",
    adminDN: "cn=ldapadm,dc=lg,dc=com",
    adminPW: "56e4a818ec215194bda946aabcec735608aac35e11420a07c76b0444ce8e3b55",
    searchFilter: "(&(objectClass=inetOrgPerson)(uid={{username}}))",
    poolConfig: { user: "appuser", password: "4e3e71b6dbe12f644dd1858863212e87a6efb82bf2a88bff1cbc574353cc1f75", database: "ghtk_einvoice", host: "10.15.68.212", port: 3306},
    redisConfig: { host: "10.15.119.131", port: 6379, password: "d9e03ab3f0347906ee5723027b8e4651de002c550ccd754a282ab41c4a759e76" },
    upload_minute: 0,
    sendmail_pdf_xml: 1,
    sendmail_pdf_xml_notzip: 1, //Gửi mail không zip file, 1 - Không zip, 0 - Có zip
    sendApprMail: 1,
    sendCancMail: 0,
    genPassFile: 0,
    grant_search: 1, //Tham số tra cứu portal có được phép tra cứu theo MST con hay không
    useJobSendmail: 1,
    useRedisQueueSendmail: 1,
    onelogin: 0, //Tham số có check 1 user login cho 1 thiết bị hay không,
    config_jsrep: {
        JSREPORT_URL: "http://10.15.68.103:5486/api/report",
        JSREPORT_ACC: "admin",
        JSREPORT_PASS : "8f9d4949a09d66f14f93314dc50855591ed01771cd1d4487382c4dc89bc310a2"
    }
}