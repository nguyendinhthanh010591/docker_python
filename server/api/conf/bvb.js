
module.exports = {
    ent: "bvb",
    dbtype: "orcl",
    serial_grant: 1,
    serial_usr_grant: 0,
    ou_grant: 1,
    xls_idt: 0,
    ldapPrivate: 0,//1-rieng 0-chung 
    captchaType: 0, // 0-ko dung 1-offline 2-online(Google)  // Chinh ca o file ext.js client
    refno: {mappingfield: "items.c4"}, //Khai báo map trường số bút toán
    sms_config:{
        sendSmsEndpoint: "http://10.15.119.59:8080/einvoice_api_rest/rest/sms/sendms",
        user: "ebsgateway",
        password: "ebsgateway",
        sms_appr: `Hoa don dien tu cua Quy khach phat hanh voi ma tra cuu {{sec}}. Tran trong.`,
        sms_can: `Hoa don dien tu cua Quy khach phat hanh tai voi ma tra cuu {{sec}} da bi huy. Tran trong.`
    },
    hsm_url: "http://10.15.68.113:8080/fpt-sighub/services/signXML",
    hsm_auth: "c3ByaW5nOnh4IyNAMTIzNDU2Nzg5QDIwMTQwNzI5",
    mail: "FPT.einvoice.OnPrem@fpt.com.vn",
    smtpConfig: { host: "mail.fpt.com.vn", port: 587, secure: false, auth: { user: "FPT.einvoice.OnPrem@fpt.com.vn", pass: "ae66d44064b145ea073d9698d5de3f866015840074444fdc32f10953078b4ef3" } }, //465(SSL),587(TLS)
//    ldap: "OPENLDAP",
//    ldapsConfig: { url: "ldaps://10.15.68.56:636" },
//    baseDN: "ou=People,dc=tlg,dc=com",
//    adminDN: "cn=ldapadm,dc=tlg,dc=com",
//    adminPW: "admin@123",
//    searchFilter: "(&(objectClass=inetOrgPerson)(uid={{username}}))", 
ldapsConfig: { url: "ldaps://10.15.68.113:636" }, 
    ldap: "AD",
    baseDN: "DC=einvoice,DC=internal",
    adminDN: "einvoice\\Administrator",
    adminPW: "cc5d6411b49a3bc329d2923fb579ec7f648517bcf12d19a3473a8daa69415e37",
    searchFilter: "(&(objectCategory=person)(objectclass=user)(sAMAccountName={{username}}))",
    poolConfig: { user: "einvoice", password: "8009eb276f6fcc0053dc1d597fe78c9ffa7a8f5f1370a456e2a6e949b80e73c7", connectString: "10.15.119.123/bvb", poolMax: 32 },
    test_sms_fpt: 1,
    redisConfig: { host: "10.15.119.59", port: 6386, password: "d9e03ab3f0347906ee5723027b8e4651de002c550ccd754a282ab41c4a759e76" },
    useJobSendmail: 1,
    useRedisQueueSendmail: 1,    
    upload_minute: 0,
    sendmail_pdf_xml: 1,
    sendcanmail_pdf_xml: 1,
    sendApprMail: 1,
    sendCancMail: 1,
    sendApprSMS: 1,
    sendCancSMS: 1,
    genPassFile: 0,
    config_jsrep: {
        JSREPORT_URL: "http://10.15.68.103:5486/api/report",
        JSREPORT_ACC: "admin",
        JSREPORT_PASS : "8f9d4949a09d66f14f93314dc50855591ed01771cd1d4487382c4dc89bc310a2"
    },
    PROCEDURE:"PK_SP_GET_INVOICE_ONLINE.sp_GET_INVOICE_ONLINE",
    api_url:"http://localhost:3002/api",
    disable_worker: 1
}

