
module.exports = {
    ent: "sgr",
    dbtype: "mssql",
    xls_idt: 1,
    xls_data: 1,
    serial_grant: 0,
    serial_usr_grant: 0,
    test_sms_fpt: 1,
    keyvault_config: {
        uri: "https://aiavndev-einvoice-vault.vault.azure.net/",
        client_id: "147324a3-2a30-4b46-8a67-c0dda1210b63",
        client_key: "-2L-6S4AiKA29wgCC4Gwz2LIG-n5T~ZpEP",
        tenant_id: "2ea32b59-7646-4673-9710-921225fa9c61"
    },
    sms_config:{
        tokenEndpoint: "https://api.uat.aia-apps.biz/sso/authenticate",
        sendSmsEndpoint: "https://apis.uat.aia-apps.com/sms/api/messages/send",
        allowSmsFilter: true,
    },
    ou_grant: 0,
    invoice_seq:1, // có chức năng cấp số hay không
    ldapPrivate: 0,//1-rieng 0-chung 
    captchaType: 0, // 0-ko dung 1-offline 2-online(Google)  // Chinh ca o file ext.js client
    mail: "FPT.einvoice.OnPrem@fpt.com.vn",
    smtpConfig: { host: "mail.fpt.com.vn", port: 587, secure: false, auth: { user: "FPT.einvoice.OnPrem@fpt.com.vn", pass: "066c842c8805fd94bc22fcee64883ad604cc1dd17c046c65b5da4377da9f51b2" } }, //465(SSL),587(TLS)
	  ldapsConfig: { url: "ldaps://10.15.68.113:636" }, 
    ldap: "AD",
    baseDN: "DC=einvoice,DC=internal",
    adminDN: "einvoice\\Administrator",
    adminPW: "b6756d0352246c0f178a62f632cabbd5aa66498d1690356bddeaca43d0665ea6",
    searchFilter: "(&(objectCategory=person)(objectclass=user)(sAMAccountName={{username}}))",
    poolConfig: { user: "einv_owner", password: "b6756d0352246c0f178a62f632cabbd5aa66498d1690356bddeaca43d0665ea6", database: "sgr_einvoice_app", server: "10.15.119.150\\MSSQL2017", abortTransactionOnError : true, parseJSON: false, options: { enableArithAbort: true }, requestTimeout : 600000, connectionTimeout: 600000,pool: {
        max: 10,
        min: 0,
        idleTimeoutMillis: 180000
    } },
    redisConfig: { host: "10.15.119.59", port: 6382, password: "64f8e8d0216dfc72a88f567476242088929aa9f291463723c910a4d47f65d2b8" },
    scheduleslist:[
        {schedulename: "Send Approve and Cancelling mail from temporary table", scheduleinterval: '0 * * * * *', schedulemethod:`schedule.sendEmailApprInv()`, schedulestatus: 0},
        {schedulename: "Delete data using to send mail from temporary table", scheduleinterval: '0 22 * * *', schedulemethod:`schedule.delTempDocMail()`, schedulestatus: 0}
    ],
    upload_minute: 1,
    sendmail_pdf_xml: 1,
    sendApprMail: 1,
    sendCancMail: 1,
    genPassFile: 0,
    path_zip  : 'C:/test/',
    sendApprSMS: 1,
    sendCancSMS: 1,
    useJobSendmail: 1,
    useRedisQueueSendmail: 1,
    config_jsrep: {
        JSREPORT_URL: "http://10.15.68.103:5486/api/report",
        JSREPORT_ACC: "admin",
        JSREPORT_PASS : "0f42716e39d753983ebe785a29d318e21033eebebddcf857cc1436159a56ea7a"
    },
    days_change_pass: 90, //Số ngày bắt buộc phải đổi mật khẩu, sử dụng cho user local
    days_warning_change_pass: 10, //Số ngày bắt bắt đầu gửi cảnh báo mật khẩu, sử dụng cho user local
    total_pass_store: 10, //Số lần lưu mật khẩu để check trùng lặp mật khẩu cũ
    is_use_local_user: 1 
}

