module.exports = {
    ent: "hdb",
    dbtype: "orcl",
    serial_grant: 0,
    serial_usr_grant: 0,
    ou_grant: 1,
    xls_idt: 0,
    ldapPrivate: 1,//1-rieng 0-chung 
    captchaType: 1, // 0-ko dung 1-offline 2-online(Google)  // Chinh ca o file ext.js client
	hsm_url: "http://10.15.68.113:8080/fpt-sighub/services/signXML",
    hsm_auth: "c3ByaW5nOnh4IyNAMTIzNDU2Nzg5QDIwMTQwNzI5",
    hsm_sys_code: "HD",
    mail: "einvoice@fpt.com.vn",
    smtpConfig: { host: "mail.fpt.com.vn", port: 587, secure: false, auth: { user: "einvoice@fpt.com.vn", pass: "ab55ea6f25c41f0d5ff8c44d346f3abadc32330b034617ae54feee4f0e6033d0" } }, //465(SSL),587(TLS)
    ldapsConfig: { url: "ldaps://10.15.68.113:636" }, 
    ldap: "AD",
    baseDN: "DC=einvoice,DC=internal",
    adminDN: "einvoice\\Administrator",
    adminPW: "04e9d62255f342c16c9978d1c2a9e11a4ae1b792c9f55eb6733f3ebec31cc190",
    searchFilter: "(&(objectCategory=person)(objectclass=user)(sAMAccountName={{username}}))",
    poolConfig: { user: "HDB", password: "e1cbd82b39a6e5f4916bfb7818bd2d98abd343d3c8f7b5c4c2e1cbe9adc170b6", connectString: "10.15.119.123/orclcdb", poolMax: 32,idle:1000000 },
    redisConfig: { host: "10.15.68.56", port: 6379, password: "d9e03ab3f0347906ee5723027b8e4651de002c550ccd754a282ab41c4a759e76" },
    scheduleslist: [
        { schedulename: "Delete Invoices that haven't been sequenced", scheduleinterval: '* 5 * * *', schedulemethod: `schedule.delInvNotSeq()` }
    ],
    upload_minute: 0,
    sendmail_pdf_xml: 0,
    sendApprMail: 0,
    sendCancMail: 0,
    genPassFile: 0,
    config_jsrep: {
        JSREPORT_URL: "http://10.15.68.103:5489/api/report",
        JSREPORT_ACC: "admin",
        JSREPORT_PASS : "8f9d4949a09d66f14f93314dc50855591ed01771cd1d4487382c4dc89bc310a2"
    }
}