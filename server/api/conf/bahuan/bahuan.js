module.exports = {
    ent: "bahuan",
    dbtype: "mysql",
    xls_idt: 0,
    xls_data: 0,
    serial_grant: 0,
    serial_usr_grant: 0,
    ou_grant: 0,
    ldapPrivate: 1,//1-rieng 0-chung 
    captchaType: 0, // 0-ko dung 1-offline 2-online(Google)  // Chinh ca o file ext.js client
    mail: "FPT.einvoice.OnPrem@fpt.com.vn",
    smtpConfig: { host: "mail.fpt.com.vn", port: 587, secure: false, auth: { user: "FPT.einvoice.OnPrem@fpt.com.vn", pass: "ae66d44064b145ea073d9698d5de3f866015840074444fdc32f10953078b4ef3" } }, //465(SSL),587
    ldap: "OPENLDAP",
    ldapsConfig: { url: "ldaps://10.15.119.161:636" },
    baseDN: "ou=People,dc=lg,dc=com",
    adminDN: "cn=ldapadm,dc=lg,dc=com",
    adminPW: "56e4a818ec215194bda946aabcec735608aac35e11420a07c76b0444ce8e3b55",
    searchFilter: "(&(objectClass=inetOrgPerson)(uid={{username}}))",
    poolConfig: { user: "appuser", password: "4e3e71b6dbe12f644dd1858863212e87a6efb82bf2a88bff1cbc574353cc1f75", database: "bahuan", host: "10.15.68.212", port: 3306},// Admin123$
    redisConfig: { host: "10.15.68.212", port: 6380, password: "d9e03ab3f0347906ee5723027b8e4651de002c550ccd754a282ab41c4a759e76" }, // redis@6379
    scheduleslist: [       
        { schedulename: "Send to api tvan", scheduleinterval: '*/10 * * * * *', schedulemethod: `schedule.sendToTvan('SENDTVAN', 300)`, schedulestatus: 1 },//10s 1 lân
        { schedulename: "Delete expiring running job", scheduleinterval: '0 */12 * * * *', schedulemethod: `schedule.DropRunningJobExpire('SENDTVAN')`, schedulestatus: 1 },// bật job chạy để ko bị lỗi ko gửi email do tồn tại key cũ đã hết hạn trong bảng s_listvalues
    ],
    upload_minute: 0,
    sendmail_pdf_xml: 1,
    sendmail_pdf_xml_notzip: 1, //Gửi mail không zip file, 1 - Không zip, 0 - Có zip
    sendApprMail: 1,
    sendCancMail: 0,
    genPassFile: 0,
    grant_search: 1, //Tham số tra cứu portal có được phép tra cứu theo MST con hay không
    useJobSendmail: 1,
    useRedisQueueSendmail: 1,
    onelogin: 1, //Tham số có check 1 user login cho 1 thiết bị hay không,
    config_jsrep: {
        JSREPORT_URL: "http://10.15.68.103:5486/api/report",
        JSREPORT_ACC: "admin",
        JSREPORT_PASS: "2ea6f6952c60f44a04b5312d1d594a7fccf79b444881665e6eaf179111cd819a"
    },
    dbCache: 0,
    log4jsConfig: {
        appenders: {
            einvoice: { type: 'file', filename: 'E:\\logeinvoice\\einvoice_lg123.log', maxLogSize: 10485760, backups: 60, compress: true }
        },
        categories: {
            default: { appenders: ['einvoice'], level: 'info' }
        },
        pm2: true
    },
    url_tvan: "http://localhost:3334/"
  } 
