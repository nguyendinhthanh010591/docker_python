module.exports = {
    ent: "onprem",
    dbtype: "pgsql",
    xls_idt: 0,
    xls_data: 0,
    serial_grant: 1,
    serial_usr_grant: 0,
    ou_grant: 1,
    ldapPrivate: 1,//1-rieng 0-chung 
    captchaType: 0, // 0-ko dung 1-offline 2-online(Google)  // Chinh ca o file ext.js client
    mail: "FPT.einvoice.OnPrem@fpt.com.vn",
    smtpConfig: { host: "mail.fpt.com.vn", port: 587, secure: false, auth: { user: "FPT.einvoice.OnPrem@fpt.com.vn", pass: "ae66d44064b145ea073d9698d5de3f866015840074444fdc32f10953078b4ef3" } }, //465(SSL),587(TLS)
    ldap: "OPENLDAP",
    ldapsConfig: { url: "ldaps://10.15.68.42:636" },
    baseDN: "ou=People,dc=fhs,dc=com",
    adminDN: "cn=ldapadm,dc=fhs,dc=com",
    adminPW: "06788b699916eb0d07226ec0d45d7209e2be2a7515c8a638411f6c0584234665",
    searchFilter: "(&(objectClass=inetOrgPerson)(uid={{username}}))",
    scheduleslist:[
        {schedulename: "Send Approve and Cancelling mail from temporary table", scheduleinterval: '0 * * * * *', schedulemethod:`schedule.sendEmailApprInv()`, schedulestatus: 0},
        {schedulename: "Delete data using to send mail from temporary table", scheduleinterval: '0 22 * * *', schedulemethod:`schedule.delTempDocMail()`, schedulestatus: 0},
        {schedulename: "Remind user to change password", scheduleinterval: '0 * * * * *', schedulemethod:`schedule.pwdExpirationReminder()`, schedulestatus: 0}
    ],
    //poolConfig: { user: "postgres", password: "postgres", database: "einvoice", host: "10.15.68.166", port: 5432 },
    poolConfig: { user: "fabico", password: "8009eb276f6fcc0053dc1d597fe78c9ffa7a8f5f1370a456e2a6e949b80e73c7", database: "app_einv_onprem", host: "10.15.68.41", port: 5432, 
        max: 32, //maximum connection which postgresql or mysql can intiate
        min: 0, //maximum connection which postgresql or mysql can intiate
        acquire: 20000, // time require to reconnect 
        idle: 20000, // get idle connection
        evict: 10000 // it actualy removes the idle connection
    },//UAT
    redisConfig: { host: "10.15.119.131", port: 6383, password: "ae7ede594c1a9f92244f76131d94abcf239d0d329a31a53e46b5ff2a4c112a60" },
    useJobSendmail: 1,
    useRedisQueueSendmail: 1,    
    upload_minute: 1,
    sendmail_pdf_xml: 0,
    sendApprMail: 1,
    sendCancMail: 0,
    genPassFile: 0,
    xls_data: 1,
    test_sms_fpt : 1,
    chk_mst: 0, //0: khong kiem tra, 1: kiem tra
    config_chietkhau_col:"Y",//tham so khai chiet khau hoa don phan dtl co gia trị là hien thi, ko co là an
    config_jsrep: {
        JSREPORT_URL: "http://10.15.68.103:5486/api/report",
        JSREPORT_ACC: "admin",
        JSREPORT_PASS : "4a3283ac314b967445ec56e77bf723703665aa96ef482947107374faf610d2db"
    },
    api:"http://10.15.68.42:8888/api/pdf/",/*
    https_server_option: {
    key: 'E:/Temp/ssl/cert.key',
    cert: 'E:/Temp/ssl/cert.crt'

    },*/
    apprSubjectMail: "HÓA ĐƠN ĐIỆN TỬ CỦA NGÂN HÀNG TMCP BẢO VIỆT (BAOVIET Bank E-invoice)",
    canSubjectMail: "HÓA ĐƠN ĐIỆN TỬ CỦA NGÂN HÀNG TMCP BẢO VIỆT (BAOVIET Bank E-invoice)",
    //useJobSendmailAPI : 1,
    //useRedisQueueSendmailAPI: 1,    
    days_change_pass: 90, //Số ngày bắt buộc phải đổi mật khẩu, sử dụng cho user local
    days_warning_change_pass: 10, //Số ngày bắt bắt đầu gửi cảnh báo mật khẩu, sử dụng cho user local
    total_pass_store: 10, //Số lần lưu mật khẩu để check trùng lặp mật khẩu cũ
    is_use_local_user: 1, //Có dùng user lưu trữ trên local DB hay không
    local_password_expire: 2, //Thời gian mật khẩu expire, tính theo ngày
    disable_worker: 0, // Tắt worker hay không
    log4jsConfig: {
        appenders: {
            einvoice: { type: 'console', filename: 'E:/Temp/einvoice/logs/einvoice.log', maxLogSize: 10485760, backups: 60, compress: true }
        },
        categories: {
            default: { appenders: ['einvoice'], level: 'debug' }
        }
    }
}
