
module.exports = {
    useAzureAD: false,
    ent: "shlv",
    dbtype: "orcl",
    xls_data: 0,
    xls_idt: 0,
    xls_ccy: 1,
    config_chietkhau_col: 1,
    serial_grant: 0,
    serial_usr_grant: 0,
    test_sms_fpt: 1,
    bth_length:30,
    ou_grant: 0,
    invoice_seq: 1, // có chức năng cấp số hay không
    ldapPrivate: 0,//1-rieng 0-chung 
    captchaType: 0, // 0-ko dung 1-offline 2-online(Google)  // Chinh ca o file ext.js client
    mail: "FPT.einvoice.OnPrem@fpt.com.vn",
    smtpConfig: { host: "mail.fpt.com.vn", port: 587, secure: false, auth: { user: "FPT.einvoice.OnPrem@fpt.com.vn", pass: "37cb8399a86b034dc4ae3b8319bff75a56631a52881d9f3059bbfadc557162e6" } }, //465(SSL),587(TLS)
    ldapsConfig: { url: "ldaps://10.15.68.113:636" },
    ldap: "AD",
    baseDN: "DC=einvoice,DC=internal",
    adminDN: "einvoice\\Administrator",
    adminPW: "cc5d6411b49a3bc329d2923fb579ec7f648517bcf12d19a3473a8daa69415e37",
    searchFilter: "(&(objectCategory=person)(objectclass=user)(sAMAccountName={{username}}))",

	poolConfig: { user: "shlv_einvoice", password: "cb78f7a66fa2142f806faca6efc4e75b415f5e213d32ec212ff21f76467e10f0", connectString: "10.14.119.11/shlv", poolMax: 32 },

    poolIntegrationConfig: {
        user: "shlv_einvoice", password: "bde0f0a3595e73981a31918f264ec07d058a0037b07141bc63b112a42b34960c", database: "shlv", server: "10.14.119.11", parseJSON: false, options: { enableArithAbort: true }, requestTimeout: 600000, connectionTimeout: 600000, pool: {
            max: 10,
            min: 0,
            idleTimeoutMillis: 180000
        }
    },
	scheduleslist: [
        { schedulename: "Send Approve and Cancelling mail from temporary table", scheduleinterval: '*/20 * * * * *', schedulemethod: `schedule.sendEmailApprInv('SENDEMAILAPPRINV', 300)`, schedulestatus: 1 },
        { schedulename: "Send to api tvan", scheduleinterval: '*/10 * * * * *', schedulemethod: `schedule.sendToTvan('SENDTVAN', 300)`, schedulestatus: 0 },
        { schedulename: "Delete expiring running job", scheduleinterval: '0 */5 * * * *', schedulemethod: `schedule.DropRunningJobExpire('SENDTVAN')`, schedulestatus: 0 },// bật job chạy để ko bị lỗi ko gửi email do tồn tại key cũ đã hết hạn trong bảng s_listvalues
        { schedulename: "Delete expiring running job", scheduleinterval: '0 */5 * * *', schedulemethod: `schedule.DropRunningJobExpire('SENDEMAILAPPRINV')`, schedulestatus: 1 },// bật job chạy để ko bị lỗi ko gửi email do tồn tại key cũ đã hết hạn trong bảng s_listvalues
        { schedulename: "Send mail for admin when digital signatures is expired", scheduleinterval: '0 7 * * *', schedulemethod: `schedule.sendEmailCTSExpried()`, schedulestatus: 0 }, // job nhắc cks hết hạn chạy vào 7h sáng hàng ngày
    ],
    ftpConfig: { host: "10.15.119.131", port: 21, user: "einvoice", password: "06788b699916eb0d07226ec0d45d7209e2be2a7515c8a638411f6c0584234665" },
    ftpdestPath: "/home/einvoice/ftp/einvoiceprem/",
    useFtpAttach: 1,
    upload_minute: 1,
    
    user_adfs: 0,
    sendmail_pdf_xml: 1,
    sendApprMail: 1,
    sendCancMail: 1,
    genPassFile: 0,
    not_sendmail_pass: 0,
    genpassrule: { length: 10, numbers: true, symbols: true, lowercase: true, uppercase: true, strict: true },
    path_zip: 'C:/test/',
    sendApprSMS: 1,
    sendCancSMS: 1,
    useJobSendmail: 1,
    useRedisQueueSendmail: 0,
    config_jsrep: {
        JSREPORT_URL: "http://10.14.122.40:5486/api/report",
        JSREPORT_ACC: "admin",
        JSREPORT_PASS: "2ea6f6952c60f44a04b5312d1d594a7fccf79b444881665e6eaf179111cd819a" //jsreport@5488
    },
    apprSubjectMail: "HÓA ĐƠN ĐIỆN TỬ CỦA NGÂN HÀNG TMCP BẢO VIỆT (BAOVIET Bank E-invoice)",
    canSubjectMail: "HÓA ĐƠN ĐIỆN TỬ CỦA NGÂN HÀNG TMCP BẢO VIỆT (BAOVIET Bank E-invoice)",
    days_left_active_cts: 30,
    days_change_pass: 90, //Số ngày bắt buộc phải đổi mật khẩu, sử dụng cho user local
    days_warning_change_pass: 10, //Số ngày bắt bắt đầu gửi cảnh báo mật khẩu, sử dụng cho user local
    total_pass_store: 10, //Số lần lưu mật khẩu để check trùng lặp mật khẩu cũ
    is_use_local_user: 1, //Có dùng user lưu trữ trên local DB hay không
    local_password_expire: 1, //Thời gian mật khẩu expire, tính theo ngày
    disable_worker: 0, // Tắt worker hay không
    dbCache: 1,
    log4jsConfig: {
        appenders: {
            einvoice: { type: 'console', filename: 'E:/Temp/einvoice/logs/einvoice.log', maxLogSize: 10485760, backups: 60, compress: true }
        },
        categories: {
            default: { appenders: ['einvoice'], level: 'debug' }
        },
        pm2: true
    },
    url_tvan: "http://localhost:3333/",
    infobipConfig: {
        // Key:"0334a4c996f11259e691423736accf98-5020506e-2827-4970-bd8f-ec6ed979816d",
        // Url:`https://pwz35m.api.infobip.com/email/3/send`,
        apiKey: '813607b3e4e9efd12b93b5679984a288-a630b3a2-5a0c-4c40-8811-f311505e96c2',
        apiUrl: 'https://yrme8g.api.infobip.com/email/2/send',
        senderEmail: 'phamquangquang2008@gmail.com'
    },
}

