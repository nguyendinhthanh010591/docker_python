"use strict"
const moment = require("moment")
const config = require("../config")
const logger4app = require("../logger4app")
const dbs = require("./dbs")
const inv = require("./inv")
const util = require("../util")
const email = require("../email")
const ent = config.ent
const HDB = ent === "hdb"
const sql_del_exp = {
    sql_del_exp_mssql: `DELETE FROM s_listvalues WHERE keyname=@1 AND getdate() > TRY_CAST(keyvalue as datetime)`,
    sql_del_exp_mysql: `DELETE FROM s_listvalues WHERE keyname=?`,
    sql_del_exp_orcl: `DELETE FROM s_listvalues WHERE keyname=:1 AND SYSDATE > TO_DATE(keyvalue, 'YYYY-MM-DD HH24:MI:SS')`,
    sql_del_exp_pgsql: `DELETE FROM s_listvalues WHERE keyname=$1`,
}
const sql_del = {
    sql_del_mssql: `DELETE FROM s_listvalues WHERE keyname=@1`,
    sql_del_mysql: `DELETE FROM s_listvalues WHERE keyname=?`,
    sql_del_orcl: `DELETE FROM s_listvalues WHERE keyname=:1`,
    sql_del_pgsql: `DELETE FROM s_listvalues WHERE keyname=$1`,
}
const sql_set_ins = {
    sql_set_ins_mssql: `INSERT INTO s_listvalues (keyname, keyvalue, keytype) VALUES(@1, @2, 'String')`,
    sql_set_ins_mysql: `INSERT INTO s_listvalues (keyname, keyvalue, keytype) VALUES(?, ?, 'String')`,
    sql_set_ins_orcl: `INSERT INTO s_listvalues (keyname, keyvalue, keytype) VALUES(:1, :2, 'String')`,
    sql_set_ins_pgsql: `INSERT INTO s_listvalues (keyname, keyvalue, keytype) VALUES($1, $2, 'String')`,
}
const rbi = (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            const sql = `select doc "doc" from s_inv where id=:id`
            const result = await dbs.query(sql, [id])
            const rows = result.rows
            if (rows.length == 0) reject(new Error(`Không tìm thấy Hóa đơn ${id} \n (Invoice not found ${id})`))
            const doc = JSON.parse(rows[0].doc)
            resolve(doc)
        } catch (err) {
            reject(err)
        }
    })
}
const Service = {
    delInvNotSeq : async (req, res, next) => {
		try {
        let sql, doc, result, binds, where = ` where status=1 and idt < :idt 
        and exists(select 1 from s_serial where a.stax=taxc and a.form=form and a.serial=serial and uses=:uses)`
        let yestday = moment(new Date).subtract(1,"days").endOf("day").toDate()
        //let yestday = new Date()
        binds = [yestday,1]
        sql = `select pid "pid", id "id", a.doc.items "items" from s_inv a ${where}`
        result = await dbs.query(sql, binds)
        if (result.rows.length > 0) {
            for(let it of result.rows) {
                if (it.pid) {
                    doc = await rbi(it.pid)
                    doc.status = 3
                    //Xóa doc.cde , nếu không có cũng ko ảnh hưởng.
                    delete doc["cde"]
                    await dbs.query(`update s_inv set doc=:doc,cid=null,cde=null where id=:pid`, [JSON.stringify(doc), it.pid])
                }
				if (it.items) {
                    if (HDB) {
                        await inv.hdbdel(JSON.parse(it.items), it.id)
                    }
                }
            }
        }
        sql = `delete from s_inv a ${where} `
        result = await dbs.query(sql, binds)
        if(result.rowsAffected) logger4app.debug("Đã xóa", result.rowsAffected)
		} catch (e) {
			logger4app.debug(e)
		}
    },
    // sendEmailApprInv : async (req, res, next) => {
    //     // try {
    //     //     let sql, result, binds
    //     //     sql = `delete from s_inv a where  status=1 and idt < ? 
    //     //     and exists(select 1 from s_serial where a.stax=taxc and a.form=form and a.serial=serial and uses=?)`
    //     //     let yestday = moment(new Date).subtract(1,"days").endOf("day").toDate()
    //     //     binds = [yestday,1]
    //     //     result = await dbs.query(sql, binds)
    //     //     // result = await dbs.query(sql, binds)
    //     //     if(result.affectedRows) logger4app.debug("Đã xóa", result.affectedRows)   
    //     // } catch (err) {
    //     //     next(err)
    //     // }
    // },
    sendEmailApprInv : async (pkey, ptimeout) => {
        //Goi ham check va update trang thai job dang chay hay khong
        if (config.disable_worker) return
        let vcheck = await Service.UpdateRunningJob(pkey, 0, ptimeout)
        logger4app.debug('sendEmailApprInv','vcheck',vcheck)
        //Neu co job dang chay (vcheck = 0) thi thoat ra, khong thuc hien lenh tiep theo
        if (!vcheck) return
        try {
            let sql, result, binds, rows
            sql = `SELECT id "id", invid "invid", doc "doc", dt "dt", status "status", xml "xml" FROM s_invdoctemp WHERE status = 0 order by id, dt OFFSET 0 ROWS FETCH NEXT ${config.MAX_ROW_JOB_GET} ROWS ONLY `
            binds = []
            result = await dbs.query(sql, binds)
            rows = result.rows
            for (const row of rows) {
                //Cập nhật trạng thái bảng tạm s_invdoctemp bang 2 de tranh cac job khac quet gay ra trung lap
                sql = `UPDATE s_invdoctemp SET status=-2 WHERE id=:1`
                binds = [row.id]
                await dbs.query(sql, binds)
            }
            for (const row of rows) {
                try {
                    let doc = JSON.parse(row.doc), xml = row.xml 
                    if (row.invid == 0) {
                        //Voi truong hop invid = 0 thi gui mail luon, noi dung gui mail la truong doc da insert vao
                        await email.sendfromdoctemp(JSON.stringify(doc))
                    } else {//Neu invid khac 0 thi dung ham gui mail hoa don
                        //Check xem co du thong tin hoa don khong, neu khong du thi truy van bang s_inv lay thong tin hoa don
                        const vcheckinvinfo = (doc.stax) && (doc.form) && (doc.serial) && (doc.seq) && (doc.status) && (doc.bmail)
                        if (!vcheckinvinfo) doc = await inv.invdocbid(row.invid)
                        //Quét để FTP hoặc gửi mail
                        if (config.useFtpAttach) {
                            await ftp.uploadattach(doc, xml)
                        }
                        let mailstatus
                        if (!util.isEmpty(doc.bmail)) mailstatus = await email.senddirectmail(doc, xml)

                        //Cập nhật ngày và trạng thái gửi mail
                        if (mailstatus) await inv.updateSendMailStatus(doc)
                    }
                    //Cập nhật trạng thái bảng tạm
                    sql = `UPDATE s_invdoctemp SET status=1, iserror = :1, error_desc = :2 WHERE id=:3 AND status=-2`
                    binds = [0, '', row.id]
                    await dbs.query(sql, binds)
                } catch (err) {
                    let err_mes = `sendEmailApprInv ${row.id}: ${err.message}` 
                    logger4app.debug(err_mes)
                    //Cập nhật trạng thái bảng tạm trường hợp xử lý lỗi
                    sql = `UPDATE s_invdoctemp SET status=1, iserror = :1, error_desc = :2 WHERE id=:3 AND status=-2`
                    binds = [1, err_mes, row.id]
                    await dbs.query(sql, binds)
                }
            }
            
        } catch (err) {
            logger4app.debug(`sendEmailApprInv `,err.message)
        }
        vcheck = await Service.UpdateRunningJob(pkey, 1)
    },
    delTempDocMail : async (req, res, next) => {
        try {
            let sql, result, binds, rows
            sql = `DELETE FROM s_invdoctemp WHERE dt <= sysdate-${config.MAX_DATE_TO_KEEP_TEMP_ROW} and status = 1 and iserror = 0`
            binds = []
            await dbs.query(sql, binds)
            rows = result[0]
        } catch (err) {
            logger4app.debug(`delTempDocMail `,err.message)
        }
    },
    pwdExpirationReminder : async (req, res, next) => {
        if (!config.is_use_local_user) return
        let  binds, sql, data
        let days_valid
        try {
            days_valid = Number(config.days_change_pass) - Number(config.days_warning_change_pass)
        } catch (err) {
            days_valid = 80
        }
        sql = `SELECT id "id", mail "mail", change_pass_date "change_pass_date", create_date "create_date", trunc(sysdate) - trunc(nvl(change_pass_date, create_date)) "day_diff" FROM s_user WHERE uc = 1 AND local = 1 and trunc(sysdate) - trunc(nvl(change_pass_date, create_date)) >= :1`
        binds = [days_valid]
        data = await dbs.query(sql, binds)
        const rows = data[0]
        for (let row of rows) {
            try {
                let uid = row.id
                let mail = row.mail
                let days_left = Number(config.days_change_pass) - Number(row.day_diff)
                //Khóa tài khoản nếu quá hạn
                if (days_left < 0) {
                    await dbs.query(`update s_user set uc=:1 where id=:2`, [2, uid])
                    continue;
                }
                //Gửi mail thông báo nhắc đổi mật khẩu
                if (days_left <= config.days_warning_change_pass) {
                    await ads.change_pass_reminder(uid, mail, days_left)
                }
            } catch (err) {
                logger4app.error(err);
            }
        }
    
    },
    UpdateRunningJob: async (pkey, preset, jobtimeout) => {//Ham thuc hien update job dang chay bang cach insert vao bang s_listvalues theo key. Tham so pkey la key truyen vao tu khai bao job. Tham so preset dung cho truong hop xoa theo key khi da chay xong
        if (!pkey) return 0
        let vjobtimeout = 300 //Mac dinh thoi gian timeout la 5 phut neu khong cau hinh
        if (util.isNumber(jobtimeout)) vjobtimeout = jobtimeout
        if (!preset) {//Thuc hien danh dau job dang chay
            logger4app.debug('UpdateRunningJob','pkey',pkey)
            let vReturn = 1 //Mac dinh tra ra trang thai la job chua chay
            try {
                let vtime = (new Date()).getTime() //Lay gio hien tai
                let expiredate = new Date(vtime + (vjobtimeout * 1000)) //Tinh thoi gian expire de sau nay xoa trong truong hop ung dung bi restart bat ngo. Se co job khac quet de xoa cac key expire nay
                //logger4app.debug('expiredate ',moment(expiredate).format("YYYY-MM-DD HH:mm:ss"))
                await dbs.query(sql_set_ins[`sql_set_ins_${config.dbtype}`], [pkey, moment(expiredate).format("YYYY-MM-DD HH:mm:ss")]) //Insert du lieu vao bang s_listvalues theo key vaf ngay gio qua han
                logger4app.debug('UpdateRunningJob','vReturn',vReturn)
            } catch (err) {
                //Truong hop insert loi tuc la co job khac dang chay, tra ra trang thai la da chay
                logger4app.debug('UpdateRunningJob err','vReturn',vReturn)
                logger4app.debug('UpdateRunningJob err','err',err)
                vReturn = 0
            }
        
            return vReturn
        }
        else {
            //Neu chay xong thi xoa key di
            await dbs.query(sql_del[`sql_del_${config.dbtype}`], [pkey])
            return 1
        }
    },
    DropRunningJobExpire: async (pkey) => {//Ham thuc hien xoa cac key cua cac job bi expire. Tham so pkey la key truyen vao tu khai bao job.
        //Neu chay xong thi xoa key di
        //logger4app.debug('sql_del_exp',sql_del_exp[`sql_del_exp_${config.dbtype}`])
        await dbs.query(sql_del_exp[`sql_del_exp_${config.dbtype}`], [pkey])
        return 1

    }
}

module.exports = Service