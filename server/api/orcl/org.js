"use strict"
const config = require("../config")
const logger4app = require("../logger4app")
const dbs = require("./dbs")
const logging = require("../logging")
const Service = {
    fbi: async (req, res, next) => {
        try {
            const result = await dbs.query(`select id "id",taxc "taxc",name "name",fadd "fadd",mail "mail",tel "tel",acc "acc",bank "bank",code "code" from s_org where id=:1`, [req.params.id])
            res.json(result.rows)
        }
        catch (err) {
            next(err)
        }
    },
    ous: async (req, res, next) => {
        try {
            const query = req.query, filter = query.filter
            let rows
            if (filter && filter.value) {
                const sql = `select SCORE(1),id "id",name "name",taxc "taxc",addr "addr",mail "mail",tel "tel",acc "acc",bank "bank",prov "prov",dist "dist",ward "ward",fadd "fadd",code "code" from s_org where CONTAINS(name,:1,1)>0 ORDER BY SCORE(1) DESC FETCH FIRST ${config.limit} ROWS ONLY`
                const result = await dbs.query(sql, [`{${filter.value}}`])
                rows = result.rows
            }
            res.json(rows)
        }
        catch (err) {
            next(err)
        }
    },
    fbn: async (req, res, next) => {
        try {
            const query = req.query, filter = query.filter
            let rows
            if (filter && filter.value) {
                const sql = `select SCORE(1),taxc "taxc",name "name",fadd "fadd",mail "mail",tel "tel",acc "acc",bank "bank",code "code" from s_org where CONTAINS(name,:1,1)>0 ORDER BY SCORE(1) DESC FETCH FIRST ${config.limit} ROWS ONLY`
                const result = await dbs.query(sql, [`{${filter.value}}`])
                rows = result.rows
            }
            res.json(rows)
        }
        catch (err) {
            next(err)
        }
    },
    get: async (req, res, next) => {
        try {
            const query = req.query, name = query.name, limit = query.count ? parseInt(query.count) : 10, start = query.start ? parseInt(query.start) : 0, sort = query.sort
            let rows, ret, result, sql, order = ""
            if (sort) {
                Object.keys(sort).forEach(key => {
                    if (key == "name" || key == "code") {
                        order = ` ORDER BY NLSSORT(${key}, 'NLS_SORT=generic_m') ${sort[key]} NULLS LAST`
                    } else order = ` ORDER BY ${key} ${sort[key]}`
                })
            }
            if (name) {
                sql = `select SCORE(1),id "id",code "code",taxc "taxc",name "name",name_en "name_en",addr "addr",prov "prov",dist "dist",ward "ward",mail "mail",tel "tel",acc "acc",bank "bank",fadd "fadd",c0 "c0",c1 "c1",c2 "c2",c3 "c3",c4 "c4",c5 "c5",c6 "c6",c7 "c7",c8 "c8",c9 "c9",type "type" from s_org where CONTAINS(name,:1,1)>0 ${order} OFFSET ${start} ROWS FETCH NEXT ${limit} ROWS ONLY` //config.limit -> limit
                result = await dbs.query(sql, [`{${name}}`])
                rows = result.rows
                if (start == 0) {
                    sql = `select count(*) "total" from s_org where CONTAINS(name,:1,1)>0`
                    result = await dbs.query(sql, [`{${name}}`])
                }
            } else {
                sql = `select id "id",code "code",taxc "taxc",name "name",name_en "name_en",addr "addr",prov "prov",dist "dist",ward "ward",mail "mail",tel "tel",acc "acc",bank "bank",fadd "fadd",c0 "c0",c1 "c1",c2 "c2",c3 "c3",c4 "c4",c5 "c5",c6 "c6",c7 "c7",c8 "c8",c9 "c9",type "type" from s_org ${order} OFFSET ${start} ROWS FETCH NEXT ${limit} ROWS ONLY`
                result = await dbs.query(sql)
                rows = result.rows
                if (start == 0) {
                    sql = `select count(*) "total" from s_org`
                    result = await dbs.query(sql)
                }
            }
            ret = { data: rows, pos: start }
            if (start == 0) {
                ret.total_count = result.rows[0].total
            }
            return res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const body = req.body, operation = body.webix_operation
            let binds, sql, result, sSysLogs = { fnc_id: '', src: config.SRC_LOGGING_DEFAULT, dtl: ``}
            if (operation == "update") {
                sql = `update s_org set code=:1,taxc=:2,name=:3,name_en=:4,prov=:5,dist=:6,ward=:7,addr=:8,mail=:9,tel=:10,acc=:11,bank=:12,c0=:c0,c1=:c1,c2=:c2,c3=:c3,c4=:c4,c5=:c5,c6=:c6,c7=:c7,c8=:c8,c9=:c9 where id=:id`
                binds = [body.code, body.taxc, body.name, body.name_en, body.prov, body.dist, body.ward, body.addr, body.mail, body.tel, body.acc, body.bank, body.c0, body.c1, body.c2, body.c3, body.c4, body.c5, body.c6, body.c7, body.c8, body.c9, body.id]
                sSysLogs = { fnc_id: 'org_upd', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật thông tin khách hàng id ${body.id}`, msg_id: body.id, doc: JSON.stringify(body)};
            }
            else if (operation == "insert") {
                sql = `insert into s_org(name,name_en,taxc,code,prov,dist,ward,addr,tel,mail,acc,bank,c0,c1,c2,c3,c4,c5,c6,c7,c8,c9) values (:name,:name_en,:taxc,:code,:prov,:dist,:ward,:addr,:tel,:mail,:acc,:bank,:c0,:c1,:c2,:c3,:c4,:c5,:c6,:c7,:c8,:c9) return id into :id`
                binds = { name: body.name, name_en: body.name_en, taxc: body.taxc, code: body.code, prov: body.prov, dist: body.dist, ward: body.ward, addr: body.addr, tel: body.tel, mail: body.mail, acc: body.acc, bank: body.bank, c0: body.c0, c1: body.c1, c2: body.c2, c3: body.c3, c4: body.c4, c5: body.c5, c6: body.c6, c7: body.c7, c8: body.c8, c9: body.c9, id: { type: 2010, dir: 3003 } }
                sSysLogs = { fnc_id: 'org_ins', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm mới thông tin khách hàng: ${JSON.stringify(body)}`, msg_id: body.id, doc: JSON.stringify(body)};
            }
            else if (operation == "delete") {
                sql = `delete from s_org where id=:id`
                binds = [body.id]
                sSysLogs = { fnc_id: 'org_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa thông tin khách hàng id ${body.id}`, msg_id: body.id, doc: JSON.stringify(body)};
            }
            result = await dbs.query(sql, binds)
            if (operation == "insert"){
                let result_id = await dbs.query(`select id from s_org where code=:1`, [body.code])
                let id_ins, rows_ins = result_id.rows
                if(rows_ins.length >0) id_ins=rows_ins[0].ID
                sSysLogs.msg_id=id_ins
                }
            logging.ins(req,sSysLogs,next) 
            if (operation == "insert") res.json({ id: result.outBinds.id[0] })
            else res.json(result.rowsAffected)
        }
        catch (err) {
            next(err)
        }
    },
    ins: async (req, res, next) => {
        try {
            const body = req.body
            const cols = ['name', 'name_en', 'taxc', 'code', 'addr', 'tel', 'mail', 'acc', 'bank', 'c0', 'c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9']
            let arr = []
            const sql = `insert into s_org(name,name_en,taxc,code,addr,tel,mail,acc,bank,c0,c1,c2,c3,c4,c5,c6,c7,c8,c9) values (:name,:name_en,:taxc,:code,:addr,:tel,:mail,:acc,:bank,:c0,:c1,:c2,:c3,:c4,:c5,:c6,:c7,:c8,:c9)`
            for (const row of body) {
                let id = row.id
                try {
                    for (const col of cols) {
                        if (!row.hasOwnProperty(col)) row[col] = null
                    }
                    delete row["id"]
                    delete row["chk"]
                    delete row["error"]
                    await dbs.query(sql, row)
                    const sSysLogs = { fnc_id: 'org_ins', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm mới thông tin khách hàng: ${JSON.stringify(row)}`, msg_id: id, doc: JSON.stringify(row)};
                    logging.ins(req,sSysLogs,next) 
                    arr.push({ id: id, error: "OK" })
                } catch (err) {
                    if (err.errorNum == 1) err.message = "duplicate"
                    arr.push({ id: id, error: err.message })
                }
            }
            res.json(arr)
        }
        catch (err) {
            next(err)
        }
    },
    obbcode: async (code) => {
        try {
            let rows, ob
            const sql = `select id "id",code "code",taxc "taxc",name "name",name_en "name_en",addr "addr",prov "prov",dist "dist",ward "ward",mail "mail",tel "tel",acc "acc",bank "bank",fadd "fadd",c0 "c0",c1 "c1",c2 "c2",c3 "c3",c4 "c4",c5 "c5",c6 "c6",c7 "c7",c8 "c8",c9 "c9",type "type" from s_org where code = :code`
            const result = await dbs.query(sql, [code])
            rows = result.rows
            ob = (rows && rows.length > 0) ? rows[0] : null
            return ob
        }
        catch (err) {
            next(err)
        }
    }
}
module.exports = Service