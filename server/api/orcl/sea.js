"use strict"
const moment = require("moment")
const bcrypt = require("bcryptjs")
const jwt = require("jsonwebtoken")
const config = require("../config")
const logger4app = require("../logger4app")
const util = require("../util")
const inc = require("../inc")
const path = require("path")
const fs = require("fs")
const xlsxtemp = require("xlsx-template")
const ous = require("./ous")
const rsmq = require("../rsmq")
const sec = require("../sec")
const dbs = require("./dbs")
const inv = require(`./inv`)
const handlebars = require("handlebars")
const ext = require("../ext")
const mfd = config.mfd
const seasecret = config.seasecret
const seaexpires = config.seaexpires
const ENT = config.ent
const rbi = (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            let doc = await inv.invdocbid(id)
            resolve(doc)
        } catch (err) {
            reject(err)
        }
    })
}
const html_vib = `
<HTML>
<HEAD>
    <META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
<TITLE></TITLE>
</HEAD>
<BODY LANG="en-US" LINK="#0000ff" DIR="LTR">
<TABLE WIDTH=793 CELLPADDING=1 CELLSPACING=0 STYLE="page-break-before: always">
    <COL WIDTH=45>
    <COL WIDTH=700>
    <COL WIDTH=40>
    <TR>
        <TD COLSPAN=3 WIDTH=700 VALIGN=TOP STYLE="border-top: 1px solid #00000a; border-bottom: none; border-left: 1px solid #00000a; border-right: 1px solid #00000a; padding-top: 0in; padding-bottom: 0in; padding-left: 0in; padding-right: 0in">
            <P CLASS="western"><IMG SRC="e86c692c6f1f37542d3522c0d3cd10db_html_9187b75d.png" ALIGN=BOTTOM WIDTH=900 HEIGHT=183 BORDER=0></P>
        </TD>
    </TR>
    <TR VALIGN=TOP>
        <TD WIDTH=45 STYLE="border-top: none; border-bottom: none; border-left: 1px solid #00000a; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0in; padding-right: 0in">
            <P CLASS="western" STYLE="margin-top: 0.08in"><BR>
            </P>
        </TD>
        <TD WIDTH=700 STYLE="border: none; padding: 0in">
            <P CLASS="western" STYLE="margin-top: 0.08in"><BR>
            </P>
        </TD>
        <TD WIDTH=40 STYLE="border-top: none; border-bottom: none; border-left: none; border-right: 1px solid #00000a; padding: 0in">
            <P CLASS="western" STYLE="margin-top: 0.08in"><BR>
            </P>
        </TD>
    </TR>
    <TR VALIGN=TOP>
        <TD WIDTH=45 STYLE="border-top: none; border-bottom: 1px solid #00000a; border-left: 1px solid #00000a; border-right: none; padding-top: 0in; padding-bottom: 0in; padding-left: 0in; padding-right: 0in">
            <P CLASS="western" STYLE="margin-top: 0.08in"><BR>
            </P>
        </TD>
        <TD WIDTH=200 STYLE="border-top: none; border-bottom: 1px solid #00000a; border-left: none; border-right: none; padding: 0in">
            <P CLASS="western" ALIGN=JUSTIFY STYLE="margin-top: 0.08in"><FONT COLOR="#000000"><FONT FACE="Myriad Pro, serif"><FONT SIZE=4>Kính</FONT></FONT></FONT><FONT COLOR="#000000"><FONT FACE="Myriad Pro, serif"><FONT SIZE=4><SPAN LANG="vi-VN">
            gửi Quý khách hàng (Dear Customer)</SPAN></FONT></FONT></FONT><FONT COLOR="#000000"><FONT FACE="Myriad Pro, serif"><FONT SIZE=4>,
            </FONT></FONT></FONT>
            </P>
            <P CLASS="western" ALIGN=JUSTIFY STYLE="margin-top: 0.08in;"><BR><BR>
            </P>
            <P CLASS="western" ALIGN=JUSTIFY STYLE="margin-top: 0.08in"><FONT COLOR="#000000"><FONT FACE="Myriad Pro, serif"><FONT SIZE=4>Cảm</FONT></FONT></FONT><FONT COLOR="#000000"><FONT FACE="Myriad Pro, serif"><FONT SIZE=4><SPAN LANG="vi-VN">
            ơn Quý khách hàng đã mua hàng tại Ngân hàng TMCP Quốc
            Tế Việt Nam (Thank you for purchasing product of Vietnam
            International Commercial Joint Stock Bank).</SPAN></FONT></FONT></FONT></P>
            <P CLASS="western" ALIGN=JUSTIFY STYLE="margin-top: 0.08in"><A NAME="_GoBack"></A>
            <FONT COLOR="#000000"><FONT FACE="Myriad Pro, serif"><FONT SIZE=4>Tài
            khoản</FONT></FONT></FONT><FONT COLOR="#000000"><FONT FACE="Myriad Pro, serif"><FONT SIZE=4><SPAN LANG="vi-VN">
            của Quý khách đã được </SPAN></FONT></FONT></FONT><FONT COLOR="#000000"><FONT FACE="Myriad Pro, serif"><FONT SIZE=4>thay
            đổi mật khẩu thành công trên hệ thống Tra cứu hóa
            đơn điện tử của Ngân hàng</FONT></FONT></FONT><FONT COLOR="#000000"><FONT FACE="Myriad Pro, serif"><FONT SIZE=4><SPAN LANG="vi-VN">
            TMCP Quốc Tế Việt Nam (We would like to </SPAN></FONT></FONT></FONT><FONT COLOR="#000000"><FONT FACE="Myriad Pro, serif"><FONT SIZE=4>notice
            that your password has been changed successfully on E–invoice
            Searching System</FONT></FONT></FONT><FONT COLOR="#000000"><FONT FACE="Myriad Pro, serif"><FONT SIZE=4><SPAN LANG="vi-VN">.).</SPAN></FONT></FONT></FONT></P>
            <P CLASS="western" ALIGN=JUSTIFY STYLE="margin-top: 0.08in"><FONT COLOR="#000000"><FONT FACE="Myriad Pro, serif"><FONT SIZE=4><SPAN LANG="vi-VN">Quý
            khách có thể tra cứu và sử dụng hoá đơn điện tử
            tại https://einvoice.vib.com.vn. </SPAN></FONT></FONT></FONT><FONT COLOR="#ff0000"><FONT FACE="Myriad Pro, serif"><FONT SIZE=4><SPAN LANG="vi-VN">(</SPAN></FONT></FONT></FONT><FONT COLOR="#ff0000"><FONT FACE="Myriad Pro, serif"><FONT SIZE=4>Kindly
            search and download e-invoice via the portal </FONT></FONT></FONT><FONT COLOR="#ff0000"><FONT FACE="Myriad Pro, serif"><FONT SIZE=4><SPAN LANG="vi-VN">https://einvoice.vib.com.vn).</SPAN></FONT></FONT></FONT></P>
            <P CLASS="western" ALIGN=JUSTIFY STYLE="margin-top: 0.08in"><FONT COLOR="#000000"><FONT FACE="Myriad Pro, serif"><FONT SIZE=4><SPAN LANG="vi-VN">Thông
            tin chi tiết của tài khoản (Account detail):</SPAN></FONT></FONT></FONT></P>
            <TABLE WIDTH=740 CELLPADDING=7 CELLSPACING=0>
                <COL WIDTH=258>
                <COL WIDTH=455>
                <TR VALIGN=TOP>
                    <TD WIDTH=258 STYLE="border: none; padding: 0in">
                        <UL>
                            <LI><P STYLE="margin-top: 0.08in"><FONT FACE="Myriad Pro, serif"><FONT SIZE=4>Tên
                            đăng nhập </FONT></FONT><FONT FACE="Myriad Pro, serif"><FONT SIZE=4><SPAN LANG="vi-VN">
                            (Username): </SPAN></FONT></FONT>
                            </P>
                            <LI><P STYLE="margin-top: 0.08in"><FONT FACE="Myriad Pro, serif"><FONT SIZE=4>Mật
                            khẩu</FONT></FONT><FONT FACE="Myriad Pro, serif"><FONT SIZE=4><SPAN LANG="vi-VN">
                            (Password): </SPAN></FONT></FONT></P>
                        </UL>
                        <P CLASS="western" STYLE="margin-left: -0.07in; margin-top: 0.08in">
                        <BR>
                        </P>
                    </TD>
                    <TD WIDTH=455 STYLE="border: none; padding: 0in">
                        <P CLASS="western" STYLE="margin-bottom: 0in">
                        <FONT FACE="Myriad Pro, serif" COLOR="#000000"><FONT SIZE=4> #taxc# </FONT></FONT></P>
                        <P CLASS="western"><FONT FACE="Myriad Pro, serif" COLOR="#000000"><FONT SIZE=4>#pwd# </FONT></FONT></P>
                    </TD>
                </TR>
            </TABLE>
            <P LANG="en-AU" CLASS="western" STYLE="margin-left: -0.07in; margin-top: 0.08in; line-height: 120%">
            <BR><BR>
            </P>
            <P CLASS="western" STYLE="margin-right: -0.06in; margin-top: 0.08in; line-height: 120%">
            <FONT FACE="Myriad Pro, serif"><FONT SIZE=4><SPAN LANG="en-AU">Trân
            trọng </SPAN></FONT></FONT><FONT COLOR="#ff0000"><FONT FACE="Myriad Pro, serif"><FONT SIZE=4><SPAN LANG="en-AU">(Sincerely),</SPAN></FONT></FONT></FONT></P>
            <P CLASS="western" STYLE="margin-top: 0.08in; line-height: 120%">
            <FONT FACE="Myriad Pro, serif"><FONT SIZE=4><SPAN LANG="vi-VN">Ngân
            hàng TMCP Quốc Tế Việt Nam (VIB)</SPAN></FONT></FONT></P>
            <P CLASS="western" STYLE="margin-top: 0.08in; line-height: 120%"><BR><BR>
            </P>
        </TD>
        <TD WIDTH=40 STYLE="border-top: none; border-bottom: 1px solid #00000a; border-left: none; border-right: 1px solid #00000a; padding: 0in">
            <P LANG="vi-VN" CLASS="western" STYLE="margin-top: 0.08in; line-height: 120%">
            <BR><BR>
            </P>
        </TD>
    </TR>
</TABLE>
<P CLASS="western" STYLE="margin-bottom: 0in"><BR>
</P>
</BODY>
</HTML>`
const Service = {
    login: async (req, res, next) => {
        try {
            await util.chkCaptcha(req)
            const query = req.query, taxc = query.username
            let sql, result, rows
            if(ENT == "hdb") {
                result = await dbs.query(`select taxc from s_org where taxc=:taxc and rownum < 3`, [taxc])
                rows = result.rows
                if (rows.length > 1) throw new Error(`MST trùng, vui lòng liên hệ với HDBank để được hỗ trợ \n (The tax code is duplicated, please contact HDBank for assistance)`)
            }
            sql = `select pwd "pwd" from s_org where taxc=:taxc and pwd is not null`
            result = await dbs.query(sql, [taxc])
            rows = result.rows
            if (rows.length == 0) throw new Error(`Không tìm thấy tài khoản ${taxc} \n (Cannot find the account ${taxc})`)
            const pwd = rows[0].pwd
            if (!bcrypt.compareSync(query.password, pwd)) throw new Error(`Mật khẩu không đúng \n (The password is incorrect)`)
            const json = { taxc: taxc }
            jwt.sign(json, seasecret, { expiresIn: seaexpires }, (err, token) => {
                if (err) return next(err)
                json.token = token
                json.expires = new Date(Date.now() + seaexpires * 1000) 
                return res.json(json)
            })
        } catch (err) {
            next(err)
        }
    },
    change: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, query = req.query
            let sql, result, rows, pwd
            sql = `select pwd "pwd" from s_org where taxc=:taxc and pwd is not null`
            result = await dbs.query(sql, [taxc])
            rows = result.rows
            if (rows.length == 0) throw new Error(`Không tìm thấy tài khoản ${taxc} \n (Account not found ${taxc})`)
            pwd = rows[0].pwd
            if (!bcrypt.compareSync(query.pwd1, pwd)) throw new Error(`Mật khẩu cũ không đúng \n (The old password is incorrect)`)
            pwd = util.bcryptpwd(query.pwd2)
            sql = `update s_org set pwd=:pwd where taxc=:taxc`
            result = await dbs.query(sql, [pwd, taxc])
            res.json(result.rowsAffected)
        } catch (err) {
            next(err)
        }
    },
    reset: async (req, res, next) => {
        try {
            await util.chkCaptcha(req)
            const query = req.query, taxc = query.taxc.trim(), mail = query.mail.trim()
            let sql, result, rows
            if(ENT == "hdb") {
                result = await dbs.query(`select taxc from s_org where taxc=:taxc and rownum < 3`, [taxc])
                rows = result.rows
                if (rows.length > 1) throw new Error(`MST trùng, vui lòng liên hệ với HDBank để được hỗ trợ \n (The tax code is duplicated, please contact HDBank for assistance)`)
            }
            sql = `select mail "mail" from s_org where taxc=:taxc and lower(mail) like :mail`
            result = await dbs.query(sql, [taxc, `%${mail.toLowerCase()}%`])
            rows = result.rows
            if (rows.length == 0) throw new Error(`Tài khoản ${taxc} Mail ${mail} không tồn tại \n (Account ${taxc} Mail ${mail} does not exist)`)
            const pwd = util.pwd()
            sql = "update s_org set pwd=:pwd where taxc=:taxc"
            result = await dbs.query(sql, [util.bcryptpwd(pwd), taxc])
            const subject = "Mật khẩu đăng nhập hệ thống hóa đơn điện tử"
            let html
            try {
                const source = await util.tempmail(2)
                const template = handlebars.compile(source)
                const obj = { taxc: taxc, pwd: pwd }
                html = template(obj)
            } catch (error) {
                html = `- Tài khoản : ${taxc}<br>- Mật khẩu: ${pwd}`
            }
            /*
            if (ENT == "vib") {
                html = html_vib 
                html = html.replace("#taxc#", `${taxc}`)
                html = html.replace("#pwd#", `${pwd}`)
            }
            */
            const content = { from: config.mail, to: mail, subject: subject, html: html }
            if (config.dbCache)
                await inc.inserttempmail(content)
            else
                await rsmq.sendMessageAsync({ qname: config.qname, message: JSON.stringify(content) })
            res.send(`Mật khẩu của tài khoản ${taxc} đã được gửi đến ${mail} \n (The account's password ${taxc} have been sent ${mail})`)
        } catch (err) {
            next(err)
        }
    },
    xml0: async (req, res, next) => {
        try {
            const token = sec.decode(req)
            const id = req.params.id
            const sql = `select xml "xml" from s_inv where id=:id and btax=:btax and status in (3,4)`
            const result = await dbs.query(sql, [id, token.taxc])
            const rows = result.rows
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn ${id} \n (Invoice not found ${id})`)
            const row = rows[0]
            res.json({ xml: row.xml })
        }
        catch (err) {
            next(err)
        }
    },
    search0: async (req, res, next) => {
        try {
            const token = sec.decode(req)
            const id = req.params.id
            const sql = `select pid "pid",cid "cid",cde "cde",adjtyp "adjtyp",adjdes "adjdes",status "status",doc "doc" from s_inv where id=:id and btax=:btax and status in (3,4)`
            const result = await dbs.query(sql, [id, token.taxc])
            const rows = result.rows
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn ${id} \n (Invoice not found ${id})`)
            const row = rows[0], doc = JSON.parse(row.doc), org = await ous.obt(doc.stax), idx = org.temp, fn = util.fn(doc.form, idx), tmp = await util.template(fn)
            //const htm = await hbs.j2h(doc, idx), pdf = await util.pdf(htm)
            //res.json({ htm: htm, pdf: `data:application/pdf;base64,${pdf.toString("base64")}`, xml: row.xml, adjtyp: row.adjtyp, adjdes: row.adjdes, pid: row.pid, cid: row.cid, cde: row.cde, status: row.status })
            let obj ={ status: doc.status, doc: doc, tmp: tmp }
            let reqjsr = ext.createRequest(obj)
            // res.json({ status: status, doc: doc, tmp: tmp })
             const bodyBuffer = await  util.jsReportRenderAttach(reqjsr)
             let sizeInByte = bodyBuffer.byteLength   
            res.json({  pdf: `data:application/pdf;base64, ${bodyBuffer.toString('base64')}`, sizePDF: sizeInByte, adjtyp: row.adjtyp, adjdes: row.adjdes, pid: row.pid, cid: row.cid, cde: row.cde, status: row.status })
        }
        catch (err) {
            next(err)
        }
    },
    xml1: async (req, res, next) => {
        try {
            if (ENT != "vib" ){
                await util.chkCaptcha(req)
            }
            
            const query = req.query, sec = query.sec, stax = query.stax
            let sql = `select xml "xml" from s_inv where sec=:sec and status in (3,4)`, bind = [sec]
            /*
            Tạm thời nút lại, lúc nào VHC có tên miền thì ợ ra
            if (ENT == "vhc" ) {
                if (stax) {
                    sql = `select xml "xml" from s_inv where sec=:sec and stax=:stax and status in (3,4,6)`
                    bind = [sec, stax]
                } else {
                    sql = `select xml "xml" from s_inv where sec=:sec and stax not in ('0100367812', '0200519868') and status in (3,4,6)`
                    bind = [sec]
                }
            }
            Tạm thời nút lại, lúc nào VHC có tên miền thì ợ ra
            */
            const result = await dbs.query(sql, bind)
            const rows = result.rows
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn mã tra cứu: ${sec} \n (Invoice not found code ${sec})`)
            const row = rows[0]
            res.json({ xml: row.xml })
        }
        catch (err) {
            next(err)
        }
    },
    search1: async (req, res, next) => {
        try {
            await util.chkCaptcha(req)
            const query = req.query, sec = query.sec, stax = query.stax
            let sql = `select id "id",pid "pid",cid "cid",cde "cde",adjtyp "adjtyp",adjdes "adjdes",status "status",doc "doc", stax "stax" from s_inv where sec=:sec and status in (3,4)`, bind = [sec]
            /*
            Tạm thời nút lại, lúc nào VHC có tên miền thì ợ ra
            if (ENT == "vhc" ) {
                if (stax) {
                    sql = `select id "id",pid "pid",cid "cid",cde "cde",adjtyp "adjtyp",adjdes "adjdes",status "status",doc "doc", stax "stax" from s_inv where sec=:sec and stax = :stax and status in (3,4,6)`
                    bind = [sec, stax]
                } else {
                    sql = `select id "id",pid "pid",cid "cid",cde "cde",adjtyp "adjtyp",adjdes "adjdes",status "status",doc "doc", stax "stax" from s_inv where sec=:sec and stax not in ('0100367812', '0200519868') and status in (3,4,6)`
                    bind = [sec]
                }
            }
            Tạm thời nút lại, lúc nào VHC có tên miền thì ợ ra
            */
            const result = await dbs.query(sql, bind)
            const rows = result.rows
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn mã tra cứu: ${sec} \n (Invoice not found code ${sec})`)
            const row = rows[0], doc = await rbi(row.id), org = await ous.obt(doc.stax), idx = org.temp, fn = util.fn(doc.form, idx), tmp = await util.template(fn) 
            let rcid
            //Kiểm tra nếu hóa đơn điều chỉnh chưa duyệt thì xóa cid đi
            if (row.cid) {
                let doccid
                try {
                    doccid = await rbi(row.cid)
                    if (doccid.status == 3) rcid = row.cid
                } catch (error) {
                    logger4app.debug(error)
                }
                
            }
            let obj ={ status: doc.status, doc: doc, tmp: tmp }
            let reqjsr = ext.createRequest(obj)
            // res.json({ status: status, doc: doc, tmp: tmp })
             const bodyBuffer = await  util.jsReportRenderAttach(reqjsr)
             let sizeInByte = bodyBuffer.byteLength   
             //res.contentType("application/pdf")    
             //res.end(bodyBuffer, "binary")
         
           //  res.json({ pdf: `data:application/pdf;base64, ${bodyBuffer.toString('base64')}`, sizePDF: sizeInByte})
            res.json({  pdf: `data:application/pdf;base64, ${bodyBuffer.toString('base64')}`, sizePDF: sizeInByte, adjtyp: row.adjtyp, adjdes: row.adjdes, pid: row.pid, cid: rcid, cde: row.cde, status: row.status })
        }
        catch (err) {
            next(err)
        }
    },
    search2: async (req, res, next) => {
        try {
            const token = sec.decode(req), query = req.query, filter = JSON.parse(query.filter)
            const sort = query.sort, start = query.start ? query.start : 0, count = query.count ? query.count : 10
            let order, val, sql, where, result, binds, i, ret
            let stax = filter.stax
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = `order by ${key} ${sort[key]}`
                })
            }
            else order = " order by id desc"
            
            where = `where idt between :1 and :2 and btax=:3 and status in (3,4)`
            binds = [new Date(filter.fd), new Date(moment(filter.td).endOf("day")), token.taxc]
            i = 3

            /*
            Tạm thời nút lại, lúc nào VHC có tên miền thì ợ ra
            if (ENT == "vhc") {
                if (stax) {
                    where = `where idt between :1 and :2 and btax=:3 and stax = :4 and status in (3,4,6)`
                    binds = [new Date(filter.fd), new Date(moment(filter.td).endOf("day")), token.taxc, stax]
                    i = 4
                } else {
                    where = `where idt between :1 and :2 and btax=:3 and stax not in ('0100367812', '0200519868') and status in (3,4,6)`
                    binds = [new Date(filter.fd), new Date(moment(filter.td).endOf("day")), token.taxc]
                    i = 3
                }

            }
            Tạm thời nút lại, lúc nào VHC có tên miền thì ợ ra
            */

            for (const key of Object.keys(filter)) {
                val = filter[key]
                if (val) {
                    switch (key) {
                        case "fd":
                            break
                        case "td":
                            break
                        case "stax":
                            break
                        default:
                            where += ` and ${key} like :${++i}`
                            binds.push(`%${val}%`)
                    }
                }
            }
            sql = `select sec "sec",pid "pid",cid "cid",id "id",idt "idt",status "status",type "type",form "form",serial "serial",seq "seq",stax "stax",sname "sname",saddr "saddr",stel "stel",smail "smail",sacc "sacc",sbank "sbank",btax "btax",bname "bname",buyer "buyer",baddr "baddr",btel "btel",bmail "bmail",bacc "bacc",bbank "bbank",note "note",sumv "sumv",vatv "vatv",totalv "totalv",adjdes "adjdes",cde "cde" from s_inv ${where} ${order} OFFSET ${start} ROWS FETCH NEXT ${count} ROWS ONLY`
            result = await dbs.query(sql, binds)
            ret = { data: result.rows, pos: start }
            if(result.rows.length>0){
                if (start == 0) {
                    sql = `select count(*) "total" from s_inv ${where}`
                    result = await dbs.query(sql, binds)
                    ret.total_count = result.rows[0].total
                }
                res.json(ret)
            }
            else{
                throw new Error("Không tìm thấy hóa đơn \n (Cannot find Invoice)")
            }
        }
        catch (err) {
            next(err)
        }
    },
    xlssearch: async (req, res, next) => {
        try {
            const token = sec.decode(req), query = req.query, filter = JSON.parse(query.filter)
            let order, val, sql, where, result, binds, i
            order = "order by id", where = "where idt between :1 and :2 and btax=:3 and status in (3,4)"
            binds = [new Date(filter.fd), new Date(moment(filter.td).endOf("day")), token.taxc]
            i = 3
            for (const key of Object.keys(filter)) {
                val = filter[key]
                if (val) {
                    switch (key) {
                        case "fd":
                            break
                        case "td":
                            break
                        default:
                            where += ` and ${key} like :${i++}`
                            binds.push(`%${val}%`)
                    }
                }
            }
            sql = `select sec "sec",pid "pid",cid "cid",id "id",TO_CHAR(idt,'${mfd}') "idt",DECODE(status,3,'Đã duyệt',4,'Đã hủy') "status",type "type",form "form",serial "serial",seq "seq",stax "stax",sname "sname",saddr "saddr",stel "stel",smail "smail",sacc "sacc",sbank "sbank",btax "btax",bname "bname",buyer "buyer",baddr "baddr",btel "btel",bmail "bmail",bacc "bacc",bbank "bbank",note "note",sumv "sumv",vatv "vatv",totalv "totalv",adjdes "adjdes",cde "cde" from s_inv ${where} ${order}`
            result = await dbs.query(sql, binds)
            res.json(result.rows)
        }
        catch (err) {
            next(err)
        }
    },
    rpxlssearch: async (req, res, next) => {
        try {
            const fn = "temp/rpxlsxsearch_vib.xlsx"
            const token = sec.decode(req), query = req.query, filter = JSON.parse(query.filter)
            let order, val, sql, where, result, binds, i, json, rows
            order = "order by id", where = "where idt between :1 and :2 and btax=:3 and status in (3,4)"
            binds = [new Date(filter.fd), new Date(moment(filter.td).endOf("day")), token.taxc]
            i = 3
            for (const key of Object.keys(filter)) {
                val = filter[key]
                if (val) {
                    switch (key) {
                        case "fd":
                            break
                        case "td":
                            break
                        default:
                            where += ` and ${key} like :${i++}`
                            binds.push(`%${val}%`)
                    }
                }
            }
            sql = `select sec "sec",pid "pid",cid "cid",id "id",TO_CHAR(idt,'${mfd}') "idt",DECODE(status,3,'Đã duyệt',4,'Đã hủy') "status",type "type",form "form",serial "serial",seq "seq",stax "stax",sname "sname",saddr "saddr",stel "stel",smail "smail",sacc "sacc",sbank "sbank",btax "btax",bname "bname",buyer "buyer",baddr "baddr",btel "btel",bmail "bmail",bacc "bacc",bbank "bbank",note "note",sumv "sumv",vatv "vatv",totalv "totalv",adjdes "adjdes",cde "cde" from s_inv ${where} ${order}`
            result = await dbs.query(sql, binds)
            rows = result.rows
            json = { table: rows}
            const file = path.join(__dirname, "..", "..", "..", fn)
            const xlsx = fs.readFileSync(file)
            const template = new xlsxtemp(xlsx)
            template.substitute(1, json)
            res.end(template.generate(), "binary")
        }
        catch (err) {
            next(err)
        }
    }
}
module.exports = Service