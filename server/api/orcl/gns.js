"use strict"
const config = require("../config")
const logger4app = require("../logger4app")
const logging = require("../logging")
const util = require("../util")
const dbs = require("./dbs")
const path = require("path")
const fs = require("fs")
const xlsxtemp = require("xlsx-template")
const Service = {
    fbn: async (req, res, next) => {
        try {
            const query = req.query, filter = query.filter
            let sql, result, where = " where 1=1", binds = {}, val
            if (filter) {
                val = `%${filter.value.trim().toUpperCase()}%`
                where += " and (upper(code) like :val or upper(name) like :val)"
                binds.val=val
            }
            sql = `select code "code",name "name",unit "unit",price "price" ${config.ent === 'vib' ? ',vrt "vrt"' : ''} from s_gns ${where} order by code FETCH FIRST ${config.limit} ROWS ONLY`
            result = await dbs.query(sql, binds)
            res.json(result.rows)
        }
        catch (err) {
            next(err)
        }
    },
    fbw: async (req, res, next) => {
        try {
            const query = req.query, filter = query.filter, sort = query.sort
            let sql, result, where = " where 1=1", order = "", binds = [], val
            let i = 1
            if (filter) {
                Object.keys(filter).forEach(key => {
                    val = filter[key]
                    if (val) {
                        if (key == "price") {
                            if (util.isNumber(val)) {
                                where += ` and price=:${i++}`
                                binds.push(val)
                            }
                            else where += ` and price = ${val}`
                        }
                        else {
                            where += ` and upper(${key}) like :${i++}`
                            binds.push(`%${val.toUpperCase()}%`)
                        }

                    }
                })
            }
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = ` order by ${key} ${sort[key]}`
                })
            }
            sql = `select id "id",code "code",name "name",unit "unit",price "price" ${config.ent === 'vib' ? ',vrt "vrt"' : ''} from s_gns ${where} ${order} FETCH FIRST ${config.limit} ROWS ONLY`
            result = await dbs.query(sql, binds)
            res.json(result.rows)
        }
        catch (err) {
            next(err)
        }
    },
    get: async (req, res, next) => {
        try {
            const query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 10, sort = query.sort
            let sql, result, ret, where = " where 1=1", order = "", binds = []
            if (query.filter) {
                let filter = JSON.parse(query.filter), val
                let i = 1
                Object.keys(filter).forEach(key => {
                    val = filter[key]
                    if (val) {
                        if (key == "unit" || key == "vrt" ) {
                            where += ` and ${key}=:${i++}`
                            binds.push(val)
                        }
                        else {
                            where += ` and upper(${key}) like :${i++}`
                            binds.push(`%${val.toUpperCase()}%`)
                        }
                    }
                })
            }
            if (sort) {
                Object.keys(sort).forEach(key => {
                    if (key == "name" || key == "code") {
                        order = ` order by NLSSORT(${key}, 'NLS_SORT=generic_m') ${sort[key]} NULLS LAST`
                    } else order = ` order by ${key} ${sort[key]}`
                })
            }
            sql = `select id "id",code "code",name "name",unit "unit",price "price" ${config.ent === 'vib' ? ',vrt "vrt"' : ''} from s_gns ${where}  ${order} offset ${start} rows fetch next ${count} rows only`
            result = await dbs.query(sql, binds)
            ret = { data: result.rows, pos: start }
            if (start == 0) {
                sql = `select count(*) "total" from s_gns ${where}`
                result = await dbs.query(sql, binds)
                ret.total_count = result.rows[0].total
            }
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const body = req.body, operation = body.webix_operation
            let sql, result, binds, price = body.price ? body.price : null, sSysLogs = { fnc_id: '', src: config.SRC_LOGGING_DEFAULT, dtl: ``}
            switch (operation) {
                case "update":
                    sql = config.ent === 'vib' ? `update s_gns set code=:1,name=:2,price=:3,unit=:4,vrt=:5 where id=:6`
                     :`update s_gns set code=:1,name=:2,price=:3,unit=:4 where id=:5`
                    binds = config.ent === 'vib' ? [body.code, body.name, price, body.unit, body.vrt, body.id]
                    : [body.code, body.name, price, body.unit, body.id]
                    sSysLogs = { fnc_id: 'gns_upd', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật danh mục hàng hóa, dịch vụ ${body.name} (${operation})`, msg_id: body.id, doc: JSON.stringify(body) };
                    break
                case "insert":
                    sql = config.ent === 'vib' ? `insert into s_gns(code, name, unit, price, vrt) values (:code,:name,:unit,:price,:vrt) RETURN id INTO :id`
                    : `insert into s_gns(code, name, unit, price) values (:code,:name,:unit,:price) RETURN id INTO :id`
                    binds = config.ent === 'vib' ? { code: body.code, name: body.name, unit: body.unit, price: price, vrt: body.vrt, id: { type: 2010, dir: 3003 } }
                    : { code: body.code, name: body.name, unit: body.unit, price: price, id: { type: 2010, dir: 3003 } }
                    sSysLogs = { fnc_id: 'gns_ins', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm mới danh mục hàng hóa, dịch vụ ${body.name} (${operation})`, msg_id: body.id, doc: JSON.stringify(body) };
                    break
                case "delete":
                    sql = `delete from s_gns where id=:1`
                    binds = [body.id]
                    sSysLogs = { fnc_id: 'gns_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa danh mục hàng hóa, dịch vụ ${body.name} (${operation})`, msg_id: body.id, doc: JSON.stringify(body) };
                    break
                default:
                    throw new Error(`${operation} là không hợp lệ (${operation} is invalid)`)
            }
            result = await dbs.query(sql, binds)
            if (operation == "insert"){
                let result_id = await dbs.query(`select id from s_gns where code=:1`, [body.code])
                let id_ins, rows_ins = result_id.rows
                if(rows_ins.length >0) id_ins=rows_ins[0].ID
                sSysLogs.msg_id=id_ins
                }
            logging.ins(req, sSysLogs, next)
            if (operation == "insert") res.json({ id: result.outBinds.id[0] })
            else res.json(result.rowsAffected)
        }
        catch (err) {
            next(err)
        }
    },
    ins: async (req, res, next) => {
        try {
            const body = req.body
            const cols = ['code', 'name', 'unit', 'price']
            let arr = []
            const sql = config.ent === 'vib' ? `insert into s_gns (code, name, unit, price, vrt) values (:code,:name,:unit,:price, :vrt)`
            : `insert into s_gns (code, name, unit, price) values (:code,:name,:unit,:price)`
            for (const row of body) {
                let id = row.id
                try {
                    for (const col of cols) {
                        if (!row.hasOwnProperty(col)) row[col] = null
                    }
                    delete row["id"]
                    delete row["chk"]
                    await dbs.query(sql, row)
                    const sSysLogs = { fnc_id: 'gns_ins', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm mới danh mục hàng hóa, dịch vụ ${body.name}`, msg_id: body.id, doc: JSON.stringify(body) };
                    logging.ins(req, sSysLogs, next)
                    arr.push({ id: id, error: "OK" })
                } catch (err) {
                    if(err.errorNum==1) err.message = "duplicate"
                    arr.push({ id: id, error: err.message })
                }
            }
            res.json(arr)
        }
        catch (err) {
            next(err)
        }
    },
    xls: async (req, res, next) => {
        try {
            const query = req.query, sort = query.sort
            let sql, result, where = " where 1=1", order = "", binds = [], rows, json
            if (query.filter) {
                let filter = JSON.parse(query.filter), val
                let i = 1
                Object.keys(filter).forEach(key => {
                    val = filter[key]
                    if (val) {
                        if (key == "unit" || key == "vrt") {
                            where += ` and ${key}=:${i++}`
                            binds.push(val)
                        }
                        else {
                            where += ` and upper(${key}) like :${i++}`
                            binds.push(`%${val.toUpperCase()}%`)
                        }
                    }
                })
            }
            if (sort) {
                Object.keys(sort).forEach(key => {
                    if (key == "name" || key == "code") {
                        order = ` order by NLSSORT(${key}, 'NLS_SORT=generic_m') ${sort[key]} NULLS LAST`
                    } else order = ` order by ${key} ${sort[key]}`
                })
            }
            sql = `select id "id",code "code",name "name",unit "unit",price "price" ${config.ent === 'vib' ? ',vrt "vrt"' : ''} from s_gns ${where}  ${order} `
            result = await dbs.query(sql, binds)
            rows = result.rows
            for(let row of rows){
                switch(row.vrt){
                    case -2 : 
                        row.vrt = "Không kê khai nộp thuế"
                        break
                    case -1 :
                        row.vrt = "Không chịu thuế"
                        break
                    case 0 :
                        row.vrt = "0%"
                        break
                    case 5 :
                        row.vrt= "5%"
                        break
                    case 10:
                        row.vrt= "10%"
                        break
                    default:
                        row.vrt= ""
                        break
                }
            }
            let fn = "temp/VIB_GNS.xlsx"
            json = { table: rows }
            const file = path.join(__dirname, "..", "..", "..", fn)
            const xlsx = fs.readFileSync(file)
            const template = new xlsxtemp(xlsx)
            template.substitute(1, json)
            res.end(template.generate(), "binary")
        } catch (err) {
            next(err)
        }
    }
}
module.exports = Service