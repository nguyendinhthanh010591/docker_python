"use strict"
const Hashids = require("hashids/cjs")
const moment = require("moment")
const Json2csvParser = require("json2csv").Parser;
var uuid = require('uuid');
const oracledb = require("oracledb")
const config = require("../config")
const logger4app = require("../logger4app")
const email = require("../email")
const sign = require("../sign")
const spdf = require("../spdf")
const util = require("../util")
const tvan = require("../tvan")
const redis = require("../redis")
const hbs = require("../hbs")
const fcloud = require("../fcloud")
const hsm = require("../hsm")
const SEC = require("../sec")
const SERIAL = require("./serial")
const dbs = require("./dbs")
const COL = require("./col")
const ous = require("./ous")
const user = require("./user")
const path = require("path")
const sms = require("../sms")
const rsmq = require("../rsmq")
const qname = config.qname
const fs = require("fs")
const xlsxtemp = require("xlsx-template")
const logging = require("../logging")
const opt = { autoCommit: false, batchErrors: true }
const hashids = new Hashids(config.secret, 10, "abcdefghijklmnopqrstuvwxyz1234567890")
const CARR = config.CARR
const grant = config.ou_grant
const mfd = config.mfd
const dtf = config.dtf
const ent = config.ent
const serial_usr_grant = config.serial_usr_grant
const useJobSendmail = config.useJobSendmail
const UPLOAD_MINUTES = config.upload_minute
const HDB = ent === "hdb"

const rbi = (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            const sql = `select doc "doc", uc "uc", ou "ou", c0 "c0", c1 "c1", c2 "c2", c3 "c3", c4 "c4", c5"c5", c6 "c6", c7 "c7", c8 "c8", c9 "c9" from s_inv where id=:id`
            const result = await dbs.query(sql, [id])
            const rows = result.rows
            if (rows.length == 0) reject(new Error(`Không tìm thấy Hóa đơn ${id} \n (Invoice not found ${id})`))
            let doc = JSON.parse(rows[0].doc), uc = rows[0].uc, ou = rows[0].ou, c0 = rows[0].c0, c1 = rows[0].c1, c2 = rows[0].c2, c3 = rows[0].c3,
                c4 = rows[0].c4, c5 = rows[0].c5, c6 = rows[0].c6, c7 = rows[0].c7, c8 = rows[0].c8, c9 = rows[0].c9
            doc.uc = uc
            doc.ou = ou
            doc.c0 = c0
            doc.c1 = c1
            doc.c2 = c2
            doc.c3 = c3
            doc.c4 = c4
            doc.c5 = c5
            doc.c6 = c6
            doc.c7 = c7
            doc.c8 = c8
            doc.c9 = c9
            //Lấy thông tin user của user lập hóa đơn
            let usercreate = await user.obid(uc)
            //Lấy thông tin ou của user lập hóa đơn
            let useroucreate = await ous.obid(ou)
            if (ent == "vib") {
                doc.bname_ = doc.bname
                doc.btax_ = doc.btax
                doc.baddr_ = doc.baddr
                doc.bmail_ = doc.bmail
                doc.btel_ = doc.btel
                doc.buyer_ = doc.buyer
                doc.bacc_ = doc.bacc
            }
            //Gán vào doc và trả ra
            doc.usercreate = usercreate
            doc.useroucreate = useroucreate
            doc.ouname = useroucreate.name
            //Lay thong tin so thu tu template de sau nay dung truy van template hoa don
            const org = await ous.obt(doc.stax), idx = org.temp, fn = util.fn(doc.form, idx)
            doc.tempfn = fn
            resolve(doc)
        } catch (err) {
            reject(err)
        }
    })
}
const oubi = (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            const result = await dbs.query(`select name "name" from s_ou where id=:1`, [id])
            let name
            const rows = result.rows
            if (rows.length == 0)
                name = ""
            else
                name = rows[0].name
            resolve(name)
        } catch (err) {
            reject(err)
        }
    })
}
const chkou = async (id, token) => {
    if (grant) {
        const u = token.u
        if (token.ou !== u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)
        const result = await dbs.query(`select ou "ou" from s_inv where id=:id`, [id])
        const rows = result.rows
        if (rows.length > 0 && rows[0].ou !== u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)
    }
}
//
const rde = (variable) => {
    return new Promise(async (resolve, reject) => {
        try {
            const sql = `select id "id", type "type", doc "doc", uc "uc", ou "ou", c0 "c0", c1 "c1", c2 "c2", c3 "c3", c4 "c4", c5"c5", c6 "c6", c7 "c7", c8 "c8", c9 "c9" from s_inv where c2=:1`
            const result = await dbs.query(sql, [variable])
            const rows = result.rows
            if (rows.length == 0) reject(new Error(`Không tìm thấy Hóa đơn có số bút toán ${variable} \n (Invoice not found ${variable})`))
            let doc = JSON.parse(rows[0].doc), uc = rows[0].uc, ou = rows[0].ou, c0 = rows[0].c0, c1 = rows[0].c1, c2 = rows[0].c2, c3 = rows[0].c3,
                c4 = rows[0].c4, c5 = rows[0].c5, c6 = rows[0].c6, c7 = rows[0].c7, c8 = rows[0].c8, c9 = rows[0].c9, id = rows[0].id, type = rows[0].type
            doc.uc = uc
            doc.ou = ou
            doc.c0 = c0
            doc.c1 = c1
            doc.c2 = c2
            doc.c3 = c3
            doc.c4 = c4
            doc.c5 = c5
            doc.c6 = c6
            doc.c7 = c7
            doc.c8 = c8
            doc.c9 = c9
            doc.id = id
            doc.type = type
            //Lấy thông tin user của user lập hóa đơn
            let usercreate = await user.obid(uc)
            //Lấy thông tin ou của user lập hóa đơn
            let useroucreate = await ous.obid(ou)
            //Gán vào doc và trả ra
            doc.usercreate = usercreate
            doc.useroucreate = useroucreate
            doc.ouname = useroucreate.name
            resolve(doc)
        } catch (err) {
            reject(err)
        }
    })
}

//Hàm lấy ra trạng thái chữ
const exSTATUS = (n) => {
    switch (n) {
        case 1:
            return "Chờ cấp số"
        case 3:
            return "Đã duyệt"
        case 4:
            return "Đã hủy"
        case 5:
            return "Đã gửi"
        case 6:
            return "Chờ hủy"
        default:
            return ""
    }
}

const Service = {
    getcol: async (json) => {
        try {
            let doc = JSON.parse(JSON.stringify(json))
            let configfield = {}
            let configfieldhdr = await COL.cache(doc.type, 'vi')
            let configfielddtl = await COL.cachedtls(doc.type, 'vi')
            configfield.configfieldhdr = configfieldhdr
            configfield.configfielddtl = configfielddtl
            doc.configfield = configfield
            let doctmp = await rbi(json.id)
            doc.org = doctmp.useroucreate
            return doc
        }
        catch (err) {
            throw err
        }
    },
    invdocbid: async (invid) => {
        try {
            let doc = await rbi(invid)
            return doc
        }
        catch (err) {
            logger4app.debug(`invdocbid ${invid}:`, err.message)
        }
    },
    insertDocTempJob: async (invid, doc, xml) => {
        try {
            if (!config.useRedisQueueSendmail) {
                logger4app.debug(`inv.insertDocTempJob s_invdoctemp`)
                await dbs.query(`INSERT INTO s_invdoctemp (invid, doc, status, xml) values (:1,:2,:3,:4)`, [invid, JSON.stringify(doc), 0, xml])
            } else {
                logger4app.debug(`inv.insertDocTempJob ${JSON.stringify({ invid: invid, status: doc.status, subject: doc.subject, bmail: doc.bmail, sendmanual: doc.sendmanual })}`)
                await rsmq.sendMessageAsync({ qname: qname, message: JSON.stringify({ invid: invid, status: doc.status, subject: doc.subject, bmail: doc.bmail, sendmanual: doc.sendmanual }) })
            }
        }
        catch (err) {
            logger4app.debug(`insertDocTempJob ${invid}:`, err.message)
        }
    },
    insertDocTempJobAPI: async (invid, doc, xml) => {
        try {
            if (!config.useRedisQueueSendmailAPI) {
                logger4app.debug(`inv.insertDocTempJob s_invdoctemp`)
                await dbs.query(`INSERT INTO s_invdoctemp (invid, doc, status, xml) values (:1,:2,:3,:4)`, [invid, JSON.stringify(doc), 0, xml])
            } else {
                logger4app.debug(`inv.insertDocTempJob ${JSON.stringify({ invid: invid, status: doc.status, subject: doc.subject, bmail: doc.bmail, sendmanual: doc.sendmanual })}`)
                await rsmq.sendMessageAsync({ qname: qname, message: JSON.stringify({ invid: invid, status: doc.status, subject: doc.subject, bmail: doc.bmail, sendmanual: doc.sendmanual }) })
            }
        }
        catch (err) {
            logger4app.debug(`insertDocTempJob ${invid}:`, err.message)
        }
    },
    updateSendMailStatus: async (doc) => {
        try {
            let sql, binds
            let docupd = await rbi(doc.id)
            //Cập nhật ngày và trạng thái gửi mail, số lần gửi mail
            if (docupd.SendMailNum) {
                try {
                    docupd.SendMailNum = parseInt(docupd.SendMailNum) + 1
                } catch (err) { docupd.SendMailNum = 1 }
            } else {
                docupd.SendMailNum = 1
            }
            docupd.SendMailStatus = 1
            docupd.SendMailDate = moment(new Date()).format(dtf)
            sql = `update s_inv set doc=:1 where id=:2`
            binds = [JSON.stringify(docupd), doc.id]
            await dbs.query(sql, binds)
        }
        catch (err) {
            throw err
        }
    },
    updateSendSMSStatus: async (doc, smsstatus) => {
        try {
            let sql, binds
            let docupd = await rbi(doc.id)
            if ((config.sendApprSMS) || (config.sendCancSMS)) {
                //Cập nhật ngày và trạng thái gửi SMS, số lần gửi SMS
                if (docupd.SendSMSNum) {
                    try {
                        docupd.SendSMSNum = parseInt(docupd.SendSMSNum) + 1
                    } catch (err) { docupd.SendSMSNum = 1 }
                } else {
                    docupd.SendSMSNum = 1
                }
                docupd.SendSMSStatus = smsstatus.errnum == 0 ? 1 : -1
                docupd.SendSMSDate = moment(new Date()).format(dtf)
                docupd.SendSMSDesc = smsstatus.resdesc
                sql = `update s_inv set doc=:1 where id=:2`
                binds = [JSON.stringify(docupd), doc.id]
                await dbs.query(sql, binds)
            }
        }
        catch (err) {
            throw err
        }
    },
    checkrefno: async (doc) => {
        try {
            if (!config.refno) return []
            let str = "", sql, arr = [], binds = [], where = "where 1=1 ", mappingfield = config.refno.mappingfield, rows
            if (mappingfield.split(".")[0] == "doc") {
                rows = [doc]
            } else {
                rows = doc.items
            }
            const crr = eval(`[...new Set(rows.map(x => x.${mappingfield.split(".")[1]}))]`)
            crr.forEach(function (value, i) {
                str += `:rn${i},`
                binds.push(value)
            })
            where += ` and ref_no in (${str.slice(0, -1)}) `
            sql = `select ref_no "ref_no" from s_invd ${where}`
            const result = await dbs.query(sql, binds)
            rows = result.rows
            rows.forEach((v, i) => {
                arr.push(v.ref_no)
            })
            return arr
        } catch (err) {
            throw err
        }
    },
    checkrefdate: async (doc) => {
        try {
            if (!config.refdate) return []
            let arr = [], mappingfield = config.refdate.mappingfield, rows
            const idt = moment(doc.idt, dtf).toDate()
            if (mappingfield.split(".")[0] == "doc") {
                rows = [doc]
            } else {
                rows = doc.items
            }
            const crr = eval(`[...new Set(rows.map(x => x.${mappingfield.split(".")[1]}))]`)
            crr.forEach(function (value, i) {
                let refdate = moment(value, dtf).toDate()
                if (refdate > idt) arr.push(value)
            })

            return arr
        } catch (err) {
            throw err
        }
    },
    saverefno: async (doc) => {
        try {
            const id = doc.id
            if (!config.refno) return 1
            let numc = 0, sql = `INSERT INTO s_invd (ref_no,inv_id) VALUES (:refno,:id) `, mappingfield = config.refno.mappingfield, rows
            if (mappingfield.split(".")[0] == "doc") {
                rows = [doc]
            } else {
                rows = doc.items
            }
            const crr = eval(`[...new Set(rows.map(x => x.${mappingfield.split(".")[1]}))]`)
            for (let i = 0; i < crr.length; i++) {
                let value = crr[i]
                try {
                    if (value && value != '') {
                        await dbs.query(sql, [value, id])
                        numc++
                    }
                } catch (error) {
                    logger4app.error(error)
                }
            }
            return numc
        } catch (err) {
            throw err
        }
    },
    saveTrans: async (trans, inv, seq, ou) => {
        try {
            let conn = await dbs.getConn()
            let binds = []
            let PROCESS_MESS = `Đã phát hành hóa đơn thành công (Mã số thuế:${inv.stax} - số hóa đơn:${seq} - ký hiệu:${inv.serial} - mẫu:${inv.form})`
            for (const item of inv.items) {
                let tran_no = item.c4
                let result = await dbs.query('select PROCESS_STATUS,PROCESS_MESS from BVB_INTEGRATION.BVB_HDDT where TRANS_NO=:1', [tran_no])
                if (result.rows.length > 0 && result.rows[0].PROCESS_STATUS == 1) throw new Error('Giao dịch đã được tạo hóa đơn từ nguồn tự động ' + (result.rows[0].PROCESS_MESS ? result.rows[0].PROCESS_MESS : ''))
            }
            for (const item of inv.items) {
                let tran_no = item.c4
                let result = await dbs.query('select PROCESS_STATUS,PROCESS_MESS from BVB_INTEGRATION.BVB_HDDT where TRANS_NO=:1', [tran_no])

                if (result.rows.length > 0 && result.rows[0].PROCESS_STATUS != 1) {
                    await dbs.query(`update BVB_INTEGRATION.BVB_HDDT set PROCESS_STATUS=1,PROCESS_MESS=:1,process_date=:2,SOURCE ='APP',seq=:3,form=:4,serial=:5,ou=:6,fkey=:7   where TRANS_NO=:8`, [PROCESS_MESS, inv.idt, seq, inv.form, inv.serial, ou, inv.sec, tran_no])

                }
                if (result.rows.length == 0) {
                    for (const tran of trans) {
                        if (tran.TRANS_NO == tran_no) {
                            binds.push([tran.TRANS_NO, tran.MA_GD, tran.PHAN_HE, tran.TRANS_TYPE, tran.LOAI_PHI, new Date(tran.NGAY_GD), new Date(tran.NGAY_THU_PHI), tran.SO_TK_KH, tran.CHI_NHANH_MO_TK
                                , tran.CIF
                                , tran.TEN_KH
                                , tran.MST_KH
                                , tran.DIA_CHI_KH
                                , tran.SDT_KH
                                , tran.EMAIL_KH
                                , tran.SECTOR_KH
                                , tran.LOAI_KH
                                , tran.HINH_THUC_THANH_TOAN
                                , tran.MA_HHDV
                                , tran.LOAI_HHDV
                                , tran.TEN_HHDV
                                , tran.LOAI_TIEN
                                , tran.CURRENCY_MARKET
                                , tran.THUE_SUAT
                                , tran.TEN_THUE_SUAT
                                , tran.TY_GIA
                                , tran.TIEN_SAU_THUE_NGUYEN_TE
                                , tran.TIEN_THUE_NGUYEN_TE
                                , tran.TIEN_SAU_THUE_QUY_DOI
                                , tran.TIEN_THUE_QUY_DOI
                                , tran.TIEN_TRUOC_THUE_NGUYEN_TE
                                , tran.TIEN_TRUOC_THUE_QUY_DOI
                                , tran.TRANG_THAI_PHI_TF
                                , tran.NOI_DUNG
                                , tran.CN_PGD
                                , tran.CHI_NHANH_CAP_1
                                , tran.TEN_CHI_NHANH
                                , tran.DIA_CHI_CN
                                , tran.DIEN_THOAI_CN
                                , tran.MST_CN
                                , tran.INPUTTER
                                , tran.AUTHORISER

                                , tran.REVERSE
                                , tran.TRANS_REF
                                , tran.DON_VI_TINH
                                , tran.SO_LUONG
                                , tran.DON_GIA
                                , tran.DIEU_CHINH
                                , tran.TIEN_THUE_QUY_DOI, 1, seq,
                            inv.form,
                            inv.serial, ou, 'APP', PROCESS_MESS, new Date(inv.idt), inv.sec])
                        }
                    }


                }
            }


            try {
                let sql = `INSERT INTO BVB_INTEGRATION.BVB_HDDT (TRANS_NO,MA_GD,PHAN_HE,TRANS_TYPE,LOAI_PHI,NGAY_GD,NGAY_THU_PHI,SO_TK_KH,CHI_NHANH_MO_TK                    ,CIF
                    ,TEN_KH
                    ,MST_KH
                    ,DIA_CHI_KH
                    ,SDT_KH
                    ,EMAIL_KH
                    ,SECTOR_KH
                    ,LOAI_KH
                    ,HINH_THUC_THANH_TOAN
                   , MA_HHDV
                   , LOAI_HHDV
                    ,TEN_HHDV
                   , LOAI_TIEN
                    ,CURRENCY_MARKET
                    ,THUE_SUAT
                   , TEN_THUE_SUAT
                   , TY_GIA
                   , TIEN_SAU_THUE_NGUYEN_TE
                    ,TIEN_THUE_NGUYEN_TE
                   , TIEN_SAU_THUE_QUY_DOI
                   , TIEN_THUE_QUY_DOI
                   , TIEN_TRUOC_THUE_NGUYEN_TE
                   , TIEN_TRUOC_THUE_QUY_DOI
                   , TRANG_THAI_PHI_TF
                    ,NOI_DUNG
                   , CN_PGD
                   , CHI_NHANH_CAP_1
                   , TEN_CHI_NHANH
                   , DIA_CHI_CN
                   , DIEN_THOAI_CN
                   , MST_CN
                   , INPUTTER
                   , AUTHORISER                  
                   , REVERSE
                   , TRANS_REF
                   , DON_VI_TINH
                   , SO_LUONG
                   , DON_GIA
                   , DIEU_CHINH,
                    TIEN_THUE_QUY_DOI_ITEM,PROCESS_STATUS,SEQ,
                    FORM,
                    SERIAL,OU,SOURCE,PROCESS_MESS,process_date,fkey) VALUES (:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14,:15,:16,:17,:18,:19,:20,:21,:22,:23,:24,:25
                        ,:26,:27,:28,:29,:30,:31,:32,:33,:34,:35,:36,:37,:38,:39,:40,:41,:42,:43,:44,:45,:46,:47,:48,:49,:50,:51,:52,:53,:54,:55,:56,:57,:58) `
                await conn.executeMany(sql, binds, opt)
                await conn.commit()
            } catch (err) {
                await conn.rollback()
                throw err
            }
            finally {
                await dbs.closeConn(conn)
            }


            return 1
        } catch (err) {
            throw err
        }
    },
    delrefno: async (doc) => {
        try {
            if (!config.refno) return 1
            let numc = 0, sql = `DELETE from s_invd where ref_no = :1 `, mappingfield = config.refno.mappingfield, rows
            if (mappingfield.split(".")[0] == "doc") {
                rows = [doc]
            } else {
                rows = doc.items
            }
            const crr = eval(`[...new Set(rows.map(x => x.${mappingfield.split(".")[1]}))]`)
            for (let i = 0; i < crr.length; i++) {
                let value = crr[i]
                try {
                    await dbs.query(sql, [value])
                    numc++
                } catch (error) {
                    logger4app.error(error)
                }
            }
            return numc
        } catch (err) {
            throw err
        }
    },
    count12: async (req, res, next) => {
        try {
            const token = SEC.decode(req), query = req.query
            const sql = `select count(*) "rc" from s_inv where stax=:1 and form=:2 and serial=:3 and status in (1, 2)`
            const result = await dbs.query(sql, [token.taxc, query.form, query.serial])
            const rows = result.rows
            res.json(rows[0])
        }
        catch (err) {
            next(err)
        }
    },
    getetl: async (req, res, next) => {
        try {
            const query = req.query, filter = JSON.parse(query.filter)
            let td = filter.td
            // td =`${moment(td).format('YYYY-MM-DD')} 00:00:00`
            let fd = `${moment(td).format('YYYY-MM-DD')} 23:59:59`
            let where = " where LOG_DATE BETWEEN TO_DATE(:td,'yyyy-mm-dd hh24:mi:ss') AND TO_DATE(:fd,'yyyy-mm-dd hh24:mi:ss') "
            let binds = [td, fd]
            let sql = `SELECT
            sid "sid",
            PROCESS_STATUS "prs",
            PROCESS_MESS "prd",
            to_char(LOG_DATE,'DD/MM/YYYY') "lg" 
            FROM 	EINV_OWNER.STAGING_INVOICE_LOG ${where}`
            let result = await dbs.query(sql, binds)
            let ret = { data: result.rows }
            res.json(ret)
        } catch (error) {
            next(error)
        }
    },
    etlpost: async (req, res, next) => {
        try {
            let body = req.body
            let rows = body.ids
            let sql, sql2
            let where = `WHERE sid= :id`
            sql = `UPDATE EINV_OWNER.STAGING_INVOICE
            SET PROCESS_STATUS = '0',
	            STATUS ='0' ${where}`
            sql2 = `DELETE
            FROM
            EINV_OWNER.STAGING_INVOICE_LOG ${where}`
            let count = 0
            for (const id of rows) {
                await dbs.query(sql, [id])
                await dbs.query(sql2, [id])
                count++
            }
            res.json(count)
        }
        catch (err) {
            next(err)
        }
    },
    seqget: async (req, res, next) => {
        try {
            const token = SEC.decode(req), query = req.query, filter = JSON.parse(query.filter), start = query.start ? query.start : 0, count = query.count ? query.count : 10
            let td = moment(filter.td).endOf("day").format("YYYY-MM-DD HH:mm:ss")
            let where = " where stax=:stax and form=:form and serial=:serial and idt<=to_date(:td,'yyyy-mm-dd hh24:mi:ss') and status=1"
            let binds = [token.taxc, filter.form, filter.serial, td]
            if (HDB) {
                where += " and ou = :ou"
                binds.push(token.ou)
            }
            let sql = `select id "id",idt "idt",status "status",ou "ou",type "type",form "form",serial "serial",seq "seq",btax "btax",bname "bname",buyer "buyer",baddr "baddr",sumv "sumv",vatv "vatv",totalv "totalv",sum "sum",vat "vat",total "total",adjdes "adjdes",uc "uc",ic "ic",note "note" from s_inv ${where} order by idt,id OFFSET ${start} ROWS FETCH NEXT ${count} ROWS ONLY`
            let result = await dbs.query(sql, binds)
            let ret = { data: result.rows, pos: start }
            if (start == 0) {
                sql = `select count(*) "total" from s_inv ${where}`
                result = await dbs.query(sql, binds)
                ret.total_count = result.rows[0].total
            }
            res.json(ret)
        } catch (error) {
            next(error)
        }
    },
    seqpost: async (req, res, next) => {
        let key
        try {
            const token = SEC.decode(req)
            const wait = 30, taxc = token.taxc, body = req.body, form = body.form, serial = body.serial, idt = new Date(body.idt), seqList = body.seqList
            key = `APPR.${taxc}.${form}.${serial}`
            const val = await redis.get(key)
            if (val === "1") throw new Error(`Đang cấp số cho HĐ MST=${taxc} mẫu số=${form} ký hiệu=${serial}. Bạn hãy đợi ${wait}s ! \n Sequencing for invoice Tax.No=${taxc} Form=${form} Serial=${serial}. Should be waiting for ${wait}s !`)
            await redis.set(key, "1", "EX", wait)
            let sql, binds
            if (seqList.length > 0) {
                // cấp 1 hoặc lỗ chỗ trong 1 ngày
                binds = [taxc, form, serial]
                let str = "", i = 0
                for (const row of seqList) {
                    str += `:${i++},`
                    binds.push(row)
                }
                sql = `select id "id",pid "pid",ou "ou",adjtyp "adjtyp",idt "idt",sec "sec" from s_inv where stax=:stax and form=:form and serial=:serial and status=1 and id IN (${str.slice(0, -1)}) order by idt,id`
            } else {
                // cấp theo dải bé hơn ngày hiện tại
                sql = `select id "id",pid "pid",ou "ou",adjtyp "adjtyp",idt "idt",sec "sec" from s_inv where idt<=:idt and stax=:stax and form=:form and serial=:serial and status=1 order by idt,id`
                binds = [idt, taxc, form, serial]
            }
            const result = await dbs.query(sql, binds)
            const rows = result.rows
            let count = 0
            const sup = `update s_inv set doc=:doc where id=:id and stax=:stax and status=1`, pup = `update s_inv set cde=:cde,doc=:doc where id=:pid`
            for (const row of rows) {
                let seq, s7, rs, rc, id, pid, doc
                try {
                    id = row.id
                    doc = await rbi(id)
                    doc.status = 2
                    seq = await SERIAL.sequence(taxc, form, serial, row.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    doc.seq = s7
                    rs = await dbs.query(sup, [JSON.stringify(doc), id, taxc])
                    rc = rs.rowsAffected
                    if (rc == 1) {
                        if (config.ent == "vib") {
                            let sqlidt = `update s_inv set idt=:idt where id=:id and stax=:stax`
                            let now = new Date()
                            await dbs.query(sqlidt, [now, id, taxc])
                        }
                        count++
                        pid = row.pid
                        if (!util.isEmpty(pid)) {
                            try {
                                const cde = `${(row.adjtyp == 1 ? "Bị thay thế" : "Bị điều chỉnh")} bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${moment(row.idt).format(config.mfd)} mã ${row.sec}`
                                let pdoc = await rbi(pid)
                                pdoc.cde = cde
                                await dbs.query(pup, [cde, JSON.stringify(pdoc), pid])
                            } catch (err) {
                                logger4app.error(err)
                            }
                        }
                    }
                    else throw new Error(`Không cấp số cho hóa đơn ${id} \n (Do not issue number to the invoice: ${id})`)
                    const rtolog = await rbi(id)
                    const sSysLogs = { fnc_id: 'inv_seqpost', src: config.SRC_LOGGING_DEFAULT, dtl: `Cấp số hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(rtolog) };
                    logging.ins(req, sSysLogs, next)
                } catch (err) {
                    if (!((err.errorNum == 1) && (String(err.message).toUpperCase().indexOf("S_INV_UID_01") >= 0))) await SERIAL.err(taxc, form, serial, seq)
                    throw err
                }
            }
            res.json(count)
        }
        catch (err) {
            next(err)
        }
        finally {
            if (!util.isEmpty(key)) await redis.del(key)
        }
    },
    apprall: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, org = await ous.obt(taxc), signtype = org.sign
            if (signtype !== 2) throw new Error("Chỉ hỗ trợ kiểu ký bằng file \n (Only support sign by file type)")
            const iwh = await Service.iwh(req, true)
            const sql = `select id "id",doc "doc",bmail "bmail",btel "btel" from s_inv ${iwh.where}`
            const result = await dbs.query(sql, iwh.binds)
            const rows = result.rows
            if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
            if (rows.length > config.MAX_ROW_APPROVE) throw new Error(`Dữ liệu ký duyệt vượt quá ${config.MAX_ROW_APPROVE} cho phép \n (Signing data exceeds ${config.MAX_ROW_APPROVE} characters for allowed)`)
            let pwd, ca
            //hunglq chinh sua thong bao loi
            try {
                pwd = util.decrypt(org.pwd)
                ca = await sign.ca(taxc, pwd)
            } catch (err) {
                throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)')
            }
            let count = 0
            const sql2 = `update s_inv set doc=:doc,xml=:xml,dt=:dt where id=:id and status=2`
            for (const row of rows) {
                let doc = JSON.parse(row.doc)
                doc.status = 3
                const id = row.id, xml = util.signature(sign.sign(ca, util.j2x((await Service.getcol(doc)), id), id), doc.idt), result2 = await dbs.query(sql2, [JSON.stringify(doc), xml, new Date(), id])
                if (result2.rowsAffected) {
                    if (useJobSendmail) {
                        await Service.insertDocTempJob(id, doc, xml)
                    } else {
                        if (!util.isEmpty(doc.bmail)) {
                            let mailstatus = await email.rsmqmail(doc)
                            if (mailstatus) await Service.updateSendMailStatus(doc)
                        }
                        if (!util.isEmpty(doc.btel)) {
                            let smsstatus = await sms.smssend(doc)
                            if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                        }
                    }
                    count++
                }
                const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                logging.ins(req, sSysLogs, next)
            }
            res.json(count)
        }
        catch (err) {
            next(err)
        }
    },
    apprpost: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const taxc = token.taxc, body = req.body, ids = body.ids
            let i = 0, str = "", binds = []
            for (const row of ids) {
                str += `:${i++},`
                binds.push(row)
            }
            binds.push(taxc)
            let sql, result
            sql = `select id "id",doc "doc",bmail "bmail",btel "btel" from s_inv where id in (${str.slice(0, -1)}) and stax=:stax and status=2`
            result = await dbs.query(sql, binds)
            const rows = result.rows
            if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
            if (rows.length > config.MAX_ROW_APPROVE) throw new Error(`Dữ liệu ký duyệt vượt quá ${config.MAX_ROW_APPROVE} cho phép \n (Signing data exceeds ${config.MAX_ROW_APPROVE} characters for allowed)`)
            const org = await ous.obt(taxc), signtype = org.sign
            if (signtype == 1) {
                let arr = []
                for (const row of rows) {
                    const doc = JSON.parse(row.doc), id = row.id, xml = util.j2x((await Service.getcol(doc)), id)
                    // push {inc: id,...} thay vi {id: id,...} vì con ký anh Đại đang để là inc thay vì id CLOUD
                    arr.push({ inc: id, xml: Buffer.from(xml).toString("base64") })
                }
                res.json({ mst: taxc, date: new Date(), ref: config.PATH_XML, arr: arr })
            }
            else {
                let count = 0
                let subquery = `select pid "pid" from s_inv where id=:id and pid is not null`
                sql = `update s_inv set doc=:doc,xml=:xml,dt=:dt where id=:id and stax=:stax and status=2`
                if (signtype == 2) {
                    //hunglq chinh sua thong bao loi
                    let ca, pwd
                    try {
                        pwd = util.decrypt(org.pwd)
                        ca = await sign.ca(taxc, pwd)
                    } catch (err) {
                        throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)')
                    }

                    for (const row of rows) {
                        const doc = JSON.parse(row.doc), id = row.id
                        if(config.DEGREE_CONFIG=='123'){
                            doc.msgType = 'INV'
                            let stt_cqt = tvan.sendMsgToTvanQueue(doc)
                            if(stt_cqt == 1){
                                doc.statustaxo = 3
                                doc.status = 3
                            }else {
                                throw new Error('Lỗi không thể duyệt hoá đơn \n (Error cannot browse invoices)')
                            }
                            await dbs.query(sql, [JSON.stringify(doc), xml, new Date(), id, taxc])
                        
                        }else if(config.DEGREE_CONFIG=='119'){
                            const  xml = util.signature(sign.sign(ca, util.j2x((await Service.getcol(doc)), id), id), doc.idt)
                            const subResult = await dbs.query(subquery, [id])
                            let subRows = subResult.rows
                            if (subRows.length > 0 && ((ent == "baca" || ent == "vib") && doc.adj.typ == 1)) {
                                let sqlSub2 = `select doc "doc" from s_inv where id='${subRows[0].pid}'`
                                const docRS = await dbs.query(sqlSub2);
                                const rsDOC = docRS.rows
                                const doc2 = JSON.parse(rsDOC[0].doc)
                                doc2.status = 4
                                let subQu = `update s_inv set doc=:doc where id='${subRows[0].pid}'`
                                await dbs.query(subQu, [JSON.stringify(doc2)])
                            }
    
                            doc.status = 3
                            await dbs.query(sql, [JSON.stringify(doc), xml, new Date(), id, taxc])
                            if (useJobSendmail) {
                                await Service.insertDocTempJob(id, doc, xml)
                            } else {
                                if (!util.isEmpty(doc.bmail)) {
                                    let mailstatus = await email.rsmqmail(doc)
                                    if (mailstatus) await Service.updateSendMailStatus(doc)
                                }
                                if (!util.isEmpty(doc.btel)) {
                                    let smsstatus = await sms.smssend(doc)
                                    if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                                }
                            }
                        }
                        const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                        logging.ins(req, sSysLogs, next)
                        count++
                    }
                }
                else {
                    //hunglq chinh sua thong bao loi
                    let kqs, usr, pwd
                    try {
                        if (signtype == 3) {
                            usr = org.usr
                            pwd = util.decrypt(org.pwd)
                            kqs = await fcloud.xml(usr, pwd, taxc, rows)
                        }
                        else kqs = await hsm.xml(rows)
                    } catch (err) {
                        throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)')
                    }

                    for (const row of kqs) {
                        const doc = JSON.parse(row.doc), id = row.id, xml = util.signature(row.xml, doc.idt)
                        const subResult = await dbs.query(subquery, [id])
                        let subRows = subResult.rows
                        //trạng thái vib
                        if (subRows.length > 0 && (ent == "vib" || ent == "baca") && (doc.adj.typ == 1 || doc.adjtyp == 1)) {
                            let sqlSub2 = `select doc "doc" from s_inv where id='${subRows[0].pid}'`
                            const docRS = await dbs.query(sqlSub2);
                            const rsDOC = docRS.rows
                            const doc2 = JSON.parse(rsDOC[0].doc)
                            doc2.status = 4
                            let subQu = `update s_inv set doc=:doc where id='${subRows[0].pid}'`
                            await dbs.query(subQu, [JSON.stringify(doc2)])
                        }
                        doc.status = 3
                        await dbs.query(sql, [JSON.stringify(doc), xml, new Date(), id, taxc])
                        if (useJobSendmail) {
                            await Service.insertDocTempJob(id, doc, xml)
                        } else {
                            if (!util.isEmpty(doc.bmail)) {
                                let mailstatus = await email.rsmqmail(doc)
                                if (mailstatus) await Service.updateSendMailStatus(doc)
                            }
                            if (!util.isEmpty(doc.btel)) {
                                let smsstatus = await sms.smssend(doc)
                                if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                            }
                        }
                        const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                        logging.ins(req, sSysLogs, next)
                        count++
                    }
                }
                if(config.DEGREE_CONFIG=='123'){
                    res.json({count:count, nd123:'nd123'})
                }else res.json(count)
            }
        }
        catch (err) {
            next(err)
        }
    },
    apprpostou: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const taxc = token.taxc, body = req.body, ids = body.ids
            let i = 0, str = "", result, sql
            for (const id of ids) {
                sql = `select id "id",doc "doc",bmail "bmail",btel "btel" from s_inv where id  =:1 and stax=:stax and status=2 and edit=1`
                result = await dbs.query(sql, [id, taxc])

                let doc = result.rows[0].doc
                doc = JSON.parse(doc)

                doc.bname = doc.bname_
                doc.btax = doc.btax_
                doc.baddr = doc.baddr_
                doc.bmail = doc.bmail_
                doc.btel = doc.btel_
                doc.buyer = doc.buyer_
                doc.bacc = doc.bacc_

                doc.edit = 2
                await dbs.query(`update s_inv set doc=:doc where id=:id`, [JSON.stringify(doc), id])

                const sSysLogs = { fnc_id: 'inv_put', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt thông tin khách hàng id ${id}`, msg_id: id, doc: JSON.stringify(body) };
                logging.ins(req, sSysLogs, next)

            }
            res.json(1)

        }
        catch (err) {
            next(err)
        }
    },
    apprput: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const taxc = token.taxc, body = req.body, incs = body.incs, signs = body.signs
            let i = 0, str = "", binds = []
            for (const row of incs) {
                str += `:${i++},`
                binds.push(row)
            }
            binds.push(taxc)
            let sql, result, rows, count = 0
            sql = `select id "id",doc "doc",bmail "bmail",btel "btel" from s_inv where id in (${str.slice(0, -1)}) and stax=:stax and status=2`
            result = await dbs.query(sql, binds)
            rows = result.rows
            if (rows.length > 0) {
                sql = `update s_inv set doc=:doc,xml=:xml,dt=:dt where id=:id and stax=:stax and status=2`
                for (const row of rows) {
                    const id = row.id, sign = signs.find(item => item.inc === id)
                    if (sign && sign.xml) {
                        const doc = JSON.parse(row.doc), xml = util.signature(Buffer.from(sign.xml, "base64").toString(), doc.idt)
                        doc.status = 3
                        await dbs.query(sql, [JSON.stringify(doc), xml, new Date(), id, taxc])
                        if (useJobSendmail) {
                            await Service.insertDocTempJob(id, doc, xml)
                        } else {
                            if (!util.isEmpty(doc.bmail)) {
                                let mailstatus = await email.rsmqmail(doc)
                                if (mailstatus) await Service.updateSendMailStatus(doc)
                            }
                            if (!util.isEmpty(doc.btel)) {
                                let smsstatus = await sms.smssend(doc)
                                if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                            }
                        }
                        const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                        logging.ins(req, sSysLogs, next)
                        count++
                    }
                }
            }
            res.json(count)
        }
        catch (err) {
            next(err)
        }
    },
    iwh: async (req, all, decodereq) => {
        const token = (!decodereq) ? SEC.decode(req) : req.token, query = req.query, filter = JSON.parse(query.filter), sort = query.sort
        let keys = Object.keys(filter), kar = ["ou", "type", "form", "serial", "status", "paym", "curr", "cvt"]
        let i = 0, val, ext = "", extxls = "", cols, where = `where idt between :fd and :td and stax=:stax`, binds = [new Date(filter.fd), new Date(moment(filter.td).endOf("day")), token.taxc], order = " order by id desc"
        if (config.ent == "bvb") {
            i = 5
        }
        if (serial_usr_grant) {
            where = `, (select taxc, type types, form forms, serial serials from s_serial ss, s_seusr ss2 where ss.id = ss2.se and ss2.usrid = :usrid) ss where idt between :fd and :td and stax=:stax`
            binds = [token.uid, new Date(filter.fd), new Date(moment(filter.td).endOf("day")), token.taxc]
        }
        if (config.serial_grant == 1) {
            where += ` AND exists  (select 1 from s_serial a,s_seou b  where a.id=b.se and b.ou=:ou and a.taxc = :stax and a.serial=s_inv.serial) `
            binds.push(token.ou, token.taxc)
        }
        if (config.ou_grant && ["bvb", "vib"].includes(config.ent)) {
            where += all ? ` AND exists (SELECT 1 FROM s_ou o where o.id = ou START WITH o.id = :ou CONNECT BY PRIOR o.id = o.pid)` : ` and ou=:ou`
            binds.push(token.u)
        }
        else if (config.ou_grant && token.ou == token.u) {
            where += all ? ` AND exists (SELECT 1 FROM s_ou o where o.id = ou START WITH o.id = :ou CONNECT BY PRIOR o.id = o.pid)` : ` and ou=:ou`
            binds.push(token.u)
        }
        if (keys.includes("type")) {
            cols = await COL.cache(filter["type"])
            if (cols.length > 0) {
                let exts = []
                let extsxls = []
                for (const col of cols) {
                    let cid = col.id
                    exts.push(`${cid} "${String(cid).toLowerCase()}"`)
                    //Xu ly rieng cho excel neu la truong date thi format lai
                    if (col.view == "datepicker")
                        extsxls.push(`TO_CHAR(TO_DATE(SUBSTR(${cid},1,10),'YYYY-MM-DD'),'${mfd}') "${String(cid).toLowerCase()}"`)
                    else
                        extsxls.push(`${cid} "${String(cid).toLowerCase()}"`)
                }
                ext = `,${exts.join()}`
                extxls = `,${extsxls.join()}`
            }
        }
        for (const key of kar) {
            if (config.ent == "vcm") {//tra cuu voi mutiselect
                if (keys.includes(key) && key.includes("serial")) {

                    val = filter[key]
                    let str = "", arr = val.split(",")
                    for (const row of arr) {
                        str += `'${row}',`
                    }
                    where += ` and ${key} in (${str.slice(0, -1)})`
                }
                else if (keys.includes(key) && key.includes("ou")) {

                    val = filter[key]
                    let str = "", arr = val.split(",")
                    for (const row of arr) {
                        let d = row.split("__")
                        str += `${d[0]},`
                    }
                    where += ` and ${key} in (${str.slice(0, -1)})`

                    //binds.push(filter[key])
                } else {
                    if (keys.includes(key)) {
                        where += ` and ${key}=:${i++}`
                        binds.push(filter[key])
                    }
                }
            } else {
                if (keys.includes(key)) {
                    if (config.ent == "vib") {
                        if (key == "status") {
                            var n = filter[key]
                            logger4app.debug(n);
                            switch (n) {
                                case "4":
                                    where += ` and status=4 and xml is not null`
                                    break;
                                case "7":
                                    where += ` and status=4 and xml is null`
                                    break;
                                default:
                                    where += ` and ${key}=:${i++}`
                                    binds.push(filter[key])
                            }
                        } else {
                            where += ` and ${key}=:${i++}`
                            binds.push(filter[key])
                        }
                    }
                    else {
                        where += ` and ${key}=:${i++}`
                        binds.push(filter[key])
                    }
                }
            }

        }
        kar.push("fd", "td")
        for (const key of kar) {
            delete keys[key]
        }
        for (const key of keys) {
            val = filter[key]
            if (val && !kar.includes(key)) {
                switch (key) {
                    case "seq":
                        where += ` and seq like :${i++}`
                        binds.push(`%${val}`)
                        break
                    case "btax":
                        where += ` and btax like :${i++}`
                        binds.push(`%${val}%`)
                        break
                    case "bacc":
                        where += ` and bacc like :${i++}`
                        binds.push(`%${val}%`)
                        break
                    case "user":
                        where += ` and uc like :${i++}`
                        binds.push(`%${val}%`)
                        break
                    case "vat":
                        let catobj = JSON.parse(JSON.stringify(config.CATTAX))
                        let objtax = catobj.find(x => x.vatCode == val)
                        where += ` and instr(',' || replace(replace(replace(json_query(doc, '$.tax.vrt' with wrapper), '"', null), '[', null), ']', null), :${i++}) > 0`
                        binds.push(`,${String(objtax.vrt)}`)
                        break
                    case "adjtyp":
                        switch (val) {
                            case "3":
                                where += ` and cid is not null and cde like :${i++}`
                                binds.push("Bị thay thế%")
                                break
                            case "4":
                                where += ` and cid is not null and cde like :${i++}`
                                binds.push("Bị điều chỉnh%")
                                break
                            default:
                                where += ` and adjtyp=:${i++}`
                                binds.push(val)
                                break
                        }
                        break
                    case "sendmailstatus":
                        switch (val) {
                            case "0":
                                where += ` and sendmailstatus = :${i++}`
                                binds.push(val)
                                break
                            case "1":
                                where += ` and sendmailstatus = :${i++}`
                                binds.push(val)
                                break
                            default:
                                break
                        }
                        break
                    case "sendsmsstatus":
                        switch (val) {
                            case "0":
                                where += ` and sendsmsstatus = :${i++}`
                                binds.push(val)
                                break
                            case "1":
                                where += ` and sendsmsstatus = :${i++}`
                                binds.push(val)
                                break
                            case "-1":
                                where += ` and sendsmsstatus = :${i++}`
                                binds.push(val)
                                break
                            default:
                                break
                        }
                        break
                    case "tran":
                        where += ` and (instr(json_query(doc, '$.items.c1' with wrapper),:${i++}) > 0 or instr(json_query(doc, '$.items.c1' with wrapper),:${i++}) > 0)`
                        binds.push(`${val}`)
                        binds.push(`${val}`)
                        break
                    case "customercode":
                        where += ` and JSON_VALUE (doc,'$.bcode') = :${i++}`
                        binds.push(`${val}`)
                        break
                    case "mail":
                        break
                    default:
                        let b = true
                        if (CARR.includes(key) && cols) {
                            let obj = cols.find(x => x.id == key)
                            if (typeof obj !== "undefined") {
                                const view = obj.view
                                if (view == "datepicker") {
                                    b = false
                                    where += ` and TRUNC(TO_DATE(SUBSTR(${key},1,10),'YYYY-MM-DD'))=:${i++}`
                                    binds.push(new Date(val))
                                }
                                else if (view == "multicombo") {
                                    b = false
                                    let str = "", arr = val.split(",")
                                    for (const row of arr) {
                                        str += `:${i++},`
                                        binds.push(row)
                                    }
                                    where += ` and ${key} in (${str.slice(0, -1)})`

                                }
                                else if (view == "combo") {
                                    b = false
                                    where += ` and ${key}=:${i++}`
                                    binds.push(val)
                                }
                                else if (view == "text") {
                                    if (obj.type == "number") {
                                        b = false
                                        where += ` and ${key} like :${i++}`
                                        binds.push(`%${val}%`)
                                    }
                                }
                            }
                        }
                        if (b) {
                            where += ` and upper(${key}) like :${i++}`
                            binds.push(`%${val.toString().toUpperCase()}%`)
                        }
                        break
                }
            }
        }
        if (serial_usr_grant) {
            where += ` and stax = ss.taxc and type = ss.types and form = ss.forms and serial = ss.serials`
        }
        if (sort) {
            Object.keys(sort).forEach(key => {
                order = `order by ${key} ${sort[key]}`
            })
        }
        else order = "order by idt desc,ABS(id) desc, type, form, serial, seq"
        return { where: where, binds: binds, ext: ext, order: order, extxls: extxls }
    },
    xls: async (req, res, next) => {
        try {
            const query = req.query, filter = JSON.parse(query.filter)
            let fn, json, rows
            if (ent != "vib") {
                fn = "temp/KQTCHD.xlsx"
            } else {
                fn = "temp/KQTCHD_VIB.xlsx"
            }
            const iwh = await Service.iwh(req, true)
            let cols = await COL.cacheLang(filter["type"], req.query.lang)
            let colhds = req.query.colhd.split(",")
            let extsh = [], sqlfields = ""
            if (ent == "vib") {
                for (const col of colhds) {
                    let colhidhd = col.split("|"), vcolid = colhidhd[0]
                    switch (colhidhd[0]) {
                        case "idt":
                            vcolid = `to_char(idt,'dd/mm/yyyy')`
                            break
                        case "status":
                            vcolid = `DECODE(status,1,'Chờ cấp số',2,'Chờ duyệt',3,'Đã duyệt',4,'Đã hủy',5,'Đã gửi',6,'Chờ hủy')`
                            break
                        case "code":
                            vcolid = `replace(replace(replace(json_query(doc, '$.items.code' with conditional wrapper),'"', null),'[', null),']', null)`
                            break
                        case "name":
                            vcolid = `replace(replace(replace(replace(json_query(doc, '$.items.name' with wrapper),'"', null),'[', null),']', null),',','-')`
                            break
                        //Thay the ky tu , thanh -
                        case "ou":
                            vcolid = `replace(pkg_vat_report.fnc_getouname(ou),',','-')`
                            break
                        case "bname":
                            vcolid = `replace(bname,',','-')`
                            break
                        case "buyer":
                            vcolid = `replace(buyer,',','-')`
                            break
                        case "note":
                            vcolid = `replace(note,',','-')`
                            break
                        case "adjdes":
                            vcolid = `replace(adjdes,',','-')`
                            break
                        case "bmail":
                            vcolid = `replace(bmail,',','-')`
                            break
                        case "baddr":
                            vcolid = `replace(baddr,',','-')`
                            break
                        case "cde":
                            vcolid = `replace(cde,',','-')`
                            break
                        default:
                            break
                    }
                    sqlfields += `${vcolid} "${colhidhd[1]}",`
                }
            }
            for (const col of cols) {
                if (ent != "vib") {
                    let clbl = col.label
                    extsh.push(clbl)
                } else {
                    let vcolid = col.id
                    if (col.view == 'datepicker') vcolid = `TO_CHAR(TO_DATE(SUBSTR(${col.id},1,10),'YYYY-MM-DD'),'DD/MM/YYYY')`
                    sqlfields += `${vcolid} "${col.label}",`
                    let clbl = col.label
                    extsh.push(clbl)
                }
            }
            sqlfields = sqlfields.substr(0, sqlfields.length - 1)
            let sql
            if (ent == "vib") {
                sql = `select count(*) "total" from s_inv ${iwh.where}`
                const result = await dbs.query(sql, iwh.binds)
                if (result.rows && result.rows.length > 0 && result.rows[0].total > config.MAX_ROW_EXCEL_EXPORT) {
                    res.json({ err: `Số lượng bản ghi vượt quá số lượng cho phép (${config.MAX_ROW_EXCEL_EXPORT} dòng) \n (The number of records exceeds the allowed number (${config.MAX_ROW_EXCEL_EXPORT} lines))` })
                }
                //sql = `select id "id",sec "sec",ic "ic",to_char(idt,'dd/mm/yyyy') "idt",DECODE(status,1,'Chờ cấp số',2,'Chờ duyệt',3,'Đã duyệt',4,'Đã hủy',5,'Đã gửi',6,'Chờ hủy') "status",type "type",form "form",serial "serial",seq "seq",btax "btax",bname "bname",buyer "buyer",baddr "baddr",note "note",sumv "sumv",vatv "vatv",totalv "totalv",sum "sum",vat "vat",total "total",adjdes "adjdes",cde "cde", bmail "bmail", curr "curr",pkg_vat_report.fnc_getouname(ou) "ou"${iwh.extxls}, replace(replace(replace(json_query(doc, '$.items.code' with conditional wrapper),'"', null),'[', null),']', null) "itemcodes", replace(replace(replace(json_query(doc, '$.items.name' with wrapper),'"', null),'[', null),']', null) "itemnames", bacc "bacc",sendmailstatus "sendmailstt", sendmaildate "sendmaildt", sendmailnum "sendmailnum", sendsmsstatus "sendsmsstt", sendsmsdate "sendsmsdt", sendsmsnum "sendsmsnum" from s_inv ${iwh.where} ${/*iwh.order*/''} OFFSET 0 ROWS FETCH NEXT ${config.MAX_ROW_EXCEL_EXPORT} ROWS ONLY`
                sql = `select /*+ parallel(s_inv,auto) */ ${sqlfields} from s_inv ${iwh.where} ${/*iwh.order*/''} OFFSET 0 ROWS FETCH NEXT ${config.MAX_ROW_EXCEL_EXPORT} ROWS ONLY`
            } else {
                if (ent == "bvb") {
                    sql = `select 0 "chk",id "id", stel "stel",JSON_VALUE (doc, '$.bcode') "bcode", bacc "bacc", JSON_VALUE (doc, '$.items.c1') "magd",sec "sec",ic "ic",uc "uc",curr "curr" ,pid "pid",cid "cid",idt "idt",${config.ent == "vib" ? "CASE WHEN STATUS = 4 AND XML IS NULL THEN 7 ELSE STATUS END" : "status"} "status", ${config.ent == "vib" ? `id_file "id_file",` : ''} ou "ou",type "type",form "form",serial "serial",seq "seq",btax "btax",bname "bname",buyer "buyer",baddr "baddr",note "note",sumv "sumv",vatv "vatv",totalv "totalv",sum "sum",vat "vat",total "total",adjdes "adjdes",adjtyp "adjtyp",cde "cde", cvt "cvt", bmail "bmail"${iwh.ext}${['vib', 'aia'].includes(ent) ? `,sendmailnum "sendmailnum", sendmailstatus "sendmailstatus", sendmaildate "sendmaildate", sendsmsnum "sendsmsnum", sendsmsstatus "sendsmsstatus", sendsmsdate "sendsmsdate"` : ``} ${config.ent == "vib" ? `,bacc "bacc", replace(replace(replace(json_query(doc, '$.items.code' with conditional wrapper),'"', null),'[', null),']', null) "itemcodes", replace(replace(replace(json_query(doc, '$.items.name' with wrapper),'"', null),'[', null),']', null) "itemnames"` : ''} from s_inv ${iwh.where} ${iwh.order} OFFSET 0 ROWS FETCH NEXT ${config.MAX_ROW_EXCEL_EXPORT} ROWS ONLY`
                }
                else {
                    sql = `select id "id",sec "sec",ic "ic",to_char(idt,'dd/mm/yyyy') "idt",DECODE(status,1,'Chờ cấp số',2,'Chờ duyệt',3,'Đã duyệt',4,'Đã hủy',5,'Đã gửi',6,'Chờ hủy') "status",type "type",form "form",serial "serial",seq "seq",btax "btax",bname "bname",buyer "buyer",baddr "baddr",note "note",sumv "sumv",vatv "vatv",totalv "totalv",sum "sum",vat "vat",total "total",adjdes "adjdes",cde "cde", bmail "bmail", curr "curr",uc "uc", ou${iwh.extxls} from s_inv ${iwh.where} ${iwh.order} OFFSET 0 ROWS FETCH NEXT ${config.MAX_ROW_EXCEL_EXPORT} ROWS ONLY`
                    sql = `select a.*, b.name "ou" from (${sql}) a inner join s_ou b on a.OU=b.id`
                }
            }

            const result = await dbs.query(sql, iwh.binds)
            rows = result.rows
            //Them ham tra gia tri theo mang de hien thi tương ứng voi tung cot
            if (ent != "vib") {
                for (const row of rows) {
                    if (ent == "vib") {
                        row.sumv = row.sumv
                        row.totalv = row.totalv
                    } else {
                        row.sumv = parseInt(row.sumv)
                        //row.vatv = parseInt(row.vatv)
                        row.totalv = parseInt(row.totalv)
                    }
                    try {
                        row.ou = await oubi(row.ou)
                    }
                    catch (err) {

                    }
                    let extsval = []
                    for (const col of cols) {
                        extsval.push(row[col.id])
                    }
                    row.extsval = extsval
                }
                json = { table: rows, extsh: extsh, colhds: colhds }
                const file = path.join(__dirname, "..", "..", "..", fn)
                const xlsx = fs.readFileSync(file)
                const template = new xlsxtemp(xlsx)
                template.substitute(1, json)
                res.end(template.generate(), "binary")
            } else {
                if (rows.length <= 0) {
                    res.json({ data: '' })
                    return
                }
                const json2csvParser = new Json2csvParser({ header: true, default: '', quote: '', delimiter: ',', encoding: 'unicode', withBOM: true });
                const csv = json2csvParser.parse(rows);
                res.json({ data: csv })
            }
        }
        catch (err) {
            next(err)
        }
    },
    appget: async (req, res, next) => {
        try {
            const token = SEC.decode(req), query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 10
            let sql, result, ret, iwh = await Service.iwh(req, false)
            if (HDB) {
                iwh.where += " and ou = :ou"
                iwh.binds.push(token.ou)
            }
            sql = `select 0 "chk",id "id",sec "sec",ic "ic",uc "uc",curr "curr" ,pid "pid",cid "cid",idt "idt",status "status",ou "ou",type "type",form "form",serial "serial",seq "seq",btax "btax",bname "bname",buyer "buyer",baddr "baddr",note "note",sumv "sumv",vatv "vatv",totalv "totalv",sum "sum",vat "vat",total "total",adjdes "adjdes",adjtyp "adjtyp",cde "cde", cvt "cvt", bmail "bmail"${iwh.ext} from s_inv ${iwh.where} ${iwh.order} OFFSET ${start} ROWS FETCH NEXT ${count} ROWS ONLY`
            result = await dbs.query(sql, iwh.binds)
            ret = { data: result.rows, pos: start }
            if (start == 0) {
                sql = `select count(*) "total" from s_inv ${iwh.where}`
                result = await dbs.query(sql, iwh.binds)
                ret.total_count = result.rows[0].total
            }
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    appgetou: async (req, res, next) => {
        try {
            const token = SEC.decode(req), query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 10
            let sql, result, ret, iwh = await Service.iwh(req, false)
            if (HDB) {
                iwh.where += " and ou = :ou"
                iwh.binds.push(token.ou)
            }
            sql = `select 0 "chk",id "id",sec "sec",ic "ic",uc "uc",curr "curr" ,pid "pid",cid "cid",idt "idt",status "status",ou "ou",type "type",form "form",serial "serial",seq "seq",btax_ "btax",bname_ "bname",buyer_ "buyer",baddr_ "baddr",note "note",sumv "sumv",vatv "vatv",totalv "totalv",sum "sum",vat "vat",total "total",adjdes "adjdes",adjtyp "adjtyp",cde "cde", cvt "cvt", bmail "bmail"${iwh.ext} from s_inv ${iwh.where} ${iwh.order} OFFSET ${start} ROWS FETCH NEXT ${count} ROWS ONLY`
            result = await dbs.query(sql, iwh.binds)
            ret = { data: result.rows, pos: start }
            if (start == 0) {
                sql = `select count(*) "total" from s_inv ${iwh.where}`
                result = await dbs.query(sql, iwh.binds)
                ret.total_count = result.rows[0].total
            }
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    get: async (req, res, next) => {
        try {
            const query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 10, iwh = await Service.iwh(req, true)
            let sql, result, ret
            if (ent == "bvb") {
                sql = `select 0 "chk",id "id", btel "btel",JSON_VALUE (doc, '$.bcode') "bcode", bacc "bacc", JSON_VALUE (doc, '$.items.c1') "magd",sec "sec",ic "ic",uc "uc",curr "curr" ,pid "pid",cid "cid",idt "idt",${config.ent == "vib" ? "CASE WHEN STATUS = 4 AND XML IS NULL THEN 7 ELSE STATUS END" : "status"} "status", ${config.ent == "vib" ? `id_file "id_file",` : ''} ou "ou",type "type",form "form",serial "serial",seq "seq",btax "btax",bname "bname",buyer "buyer",baddr "baddr",note "note",sumv "sumv",vatv "vatv",totalv "totalv",sum "sum",vat "vat",total "total",adjdes "adjdes",adjtyp "adjtyp",cde "cde", cvt "cvt", bmail "bmail"${iwh.ext}${['vib', 'aia'].includes(ent) ? `,sendmailnum "sendmailnum", sendmailstatus "sendmailstatus", sendmaildate "sendmaildate", sendsmsnum "sendsmsnum", sendsmsstatus "sendsmsstatus", sendsmsdate "sendsmsdate"` : ``} ${config.ent == "vib" ? `,bacc "bacc", replace(replace(replace(json_query(doc, '$.items.code' with conditional wrapper),'"', null),'[', null),']', null) "itemcodes", replace(replace(replace(json_query(doc, '$.items.name' with wrapper),'"', null),'[', null),']', null) "itemnames"` : ''}, error_msg_van "error_msg_van" from s_inv ${iwh.where} ${iwh.order} OFFSET ${start} ROWS FETCH NEXT ${count} ROWS ONLY`
            }
            else {
                sql = `select 0 "chk",id "id",sec "sec",ic "ic",uc "uc",curr "curr" ,pid "pid",cid "cid",idt "idt",${config.ent == "vib" ? "CASE WHEN STATUS = 4 AND XML IS NULL THEN 7 ELSE STATUS END" : "status"} "status", ${config.ent == "vib" ? `id_file "id_file",` : ''} ou "ou",type "type",form "form",serial "serial",seq "seq",btax "btax",bname "bname",buyer "buyer",baddr "baddr",note "note",sumv "sumv",vatv "vatv",totalv "totalv",sum "sum",vat "vat",total "total",adjdes "adjdes",adjtyp "adjtyp",cde "cde", cvt "cvt", bmail "bmail"${iwh.ext}${['vib', 'aia'].includes(ent) ? `,sendmailnum "sendmailnum", sendmailstatus "sendmailstatus", sendmaildate "sendmaildate", sendsmsnum "sendsmsnum", sendsmsstatus "sendsmsstatus", sendsmsdate "sendsmsdate"` : ``} ${config.ent == "vib" ? `,bacc "bacc", replace(replace(replace(json_query(doc, '$.items.code' with conditional wrapper),'"', null),'[', null),']', null) "itemcodes", replace(replace(replace(json_query(doc, '$.items.name' with wrapper),'"', null),'[', null),']', null) "itemnames"` : ''}, error_msg_van "error_msg_van" from s_inv ${iwh.where} ${iwh.order} OFFSET ${start} ROWS FETCH NEXT ${count} ROWS ONLY`
            }
            result = await dbs.query(sql, iwh.binds)
            ret = { data: result.rows, pos: start }
            if (start == 0) {
                sql = `select count(*) "total" from s_inv ${iwh.where}`
                result = await dbs.query(sql, iwh.binds)
                ret.total_count = result.rows[0].total
            }
            const sSysLogs = { fnc_id: 'inv_get', src: config.SRC_LOGGING_DEFAULT, dtl: `Tra cứu hóa đơn ${query.filter}`, msg_id: "", doc: query.filter };
            logging.ins(req, sSysLogs, next)
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    signget: async (req, res, next) => {
        try {
            const token = SEC.decode(req), u = token.u
            if (grant && token.ou !== u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const id = req.query.id, taxc = token.taxc
            let sql, result, binds = [id, taxc]
            sql = `select id "id",doc "doc",bmail "bmail",btel "btel" from s_inv where id=:id and stax=:stax and status=2`
            if (grant) {
                sql += ' and ou=:ou'
                binds.push(u)
            }
            result = await dbs.query(sql, binds)
            const rows = result.rows
            if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
            const org = await ous.obt(taxc), signtype = org.sign, row = rows[0], doc = JSON.parse(row.doc)
            let xml
            if (signtype == 1) {
                const usr = org.usr, pwd = util.decrypt(org.pwd)
                xml = util.j2x((await Service.getcol(doc)), id)
                //const arr = [{ id: id, xml: Buffer.from(xml).toString("base64") }]
                const arr = [{ inc: id, xml: Buffer.from(xml).toString("base64") }]
                res.json({ lib: usr, sid: 0, pin: pwd, mst: taxc, date: new Date(), ref: config.PATH_XML, arr: arr })
            }
            else {
                if (signtype == 2) {
                    const pwd = util.decrypt(org.pwd)
                    const ca = await sign.ca(taxc, pwd)
                    xml = util.signature(sign.sign(ca, util.j2x((await Service.getcol(doc)), id), id), doc.idt)
                }
                else if (signtype == 3) {
                    const usr = org.usr, pwd = util.decrypt(org.pwd)
                    const kqs = await fcloud.xml(usr, pwd, taxc, rows), kq = kqs[0]
                    xml = kq.xml
                }
                else {
                    const kqs = await hsm.xml(rows), kq = kqs[0]
                    xml = util.signature(kq.xml, doc.idt)
                }
                doc.status = 3
                await dbs.query(`update s_inv set doc=:doc,xml=:xml,dt=:dt where id=:id and stax=:stax and status=2`, [JSON.stringify(doc), xml, new Date(), id, taxc])
                if (useJobSendmail) {
                    await Service.insertDocTempJob(id, doc, xml)
                } else {
                    if (!util.isEmpty(doc.bmail)) {
                        let mailstatus = await email.rsmqmail(doc)
                        if (mailstatus) await Service.updateSendMailStatus(doc)
                    }
                    if (!util.isEmpty(doc.btel)) {
                        let smsstatus = await sms.smssend(doc)
                        if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                    }
                }
                const sSysLogs = { fnc_id: 'inv_signget', src: config.SRC_LOGGING_DEFAULT, dtl: `Ký số hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                logging.ins(req, sSysLogs, next)
                res.json(1)
            }
        }
        catch (err) {
            next(err)
        }
    },
    signput: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, body = req.body, signs = body.signs
            let sign = signs[0], id = sign.inc, xml = Buffer.from(sign.xml, "base64").toString()
            let result
            result = await dbs.query(`select doc "doc",bmail "bmail",btel "btel" from s_inv where id=:1 and stax=:2 and status=2`, [id, taxc])
            const rows = result.rows
            if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
            const row = rows[0], doc = JSON.parse(row.doc)
            xml = util.signature(xml, doc.idt)
            doc.status = 3
            await dbs.query(`update s_inv set doc=:doc,xml=:xml,dt=:dt where id=:1 and stax=:2 and status=2`, [JSON.stringify(doc), xml, new Date(), id, taxc])
            if (useJobSendmail) {
                await Service.insertDocTempJob(id, doc, xml)
            } else {
                if (!util.isEmpty(doc.bmail)) {
                    let mailstatus = await email.rsmqmail(doc)
                    if (mailstatus) await Service.updateSendMailStatus(doc)
                }
                if (!util.isEmpty(doc.btel)) {
                    let smsstatus = await sms.smssend(doc)
                    if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                }
            }
            const sSysLogs = { fnc_id: 'inv_signget', src: config.SRC_LOGGING_DEFAULT, dtl: `Ký số hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
            logging.ins(req, sSysLogs, next)
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },
    put: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, id = req.params.id
            const dt = moment(body.idt), eod = dt.endOf('day').toDate(), dtsc = dt.format('YYYYMMDD'), idtc = (moment(new Date())).format('YYYYMMDD'), taxc = token.taxc
            if (dtsc != idtc) {
                let result = await dbs.query(`select count(*) "rc" from s_inv where idt>:eod and type=:type and form=:form and serial=:serial and stax=:stax and (status in (2,3,5,6) or (status = 4 and xml is not null)) and status<>4 and rownum <= 100`, [eod, body.type, body.form, body.serial, taxc])
                let row = result.rows[0]
                if (row.rc > 0) throw new Error(`Tồn tại HĐ có ngày lớn hơn ${moment(body.idt).format('DD/MM/YYYY')} và đã cấp số \n (Invoice has a larger date  ${moment(body.idt).format('DD/MM/YYYY')} and issued a number)`)
            }
            await chkou(id, token)
            // let orgin = await rbi(id)
            await Service.delrefno(body)
            const hdb = await Service.checkrefno(body)
            if (hdb.length > 0) throw new Error(`Số bút toán đã tồn tại: ${hdb.join()} \n(Entry number already exists: ${hdb.join()})`)

            const checkrefdate = await Service.checkrefdate(body)
            if (checkrefdate.length > 0) throw new Error(`Ngày bút toán lớn hơn ngày hóa đơn: ${checkrefdate.join(',')} \n(Ref Date is greater than Invoice Date: ${checkrefdate.join(',')})`)

            await dbs.query(`update s_inv set doc=:doc,idt=:idt where id=:id and status=:status`, [JSON.stringify(body), new Date(body.idt), id, body.status])

            await Service.saverefno(body)

            const sSysLogs = { fnc_id: 'inv_put', src: config.SRC_LOGGING_DEFAULT, dtl: `Sửa hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(body) };
            logging.ins(req, sSysLogs, next)
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },
    ins: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const taxc = token.taxc, uid = token.uid, ou = token.ou, org = await ous.obt(taxc), AUTO = (org.seq === 1), body = req.body
            let invs = body.invs, sql = `insert into s_inv(id, pid, sec, ic, idt, ou, uc, doc) values (:id, :pid, :sec, :ic, :idt, :ou, :uc, :doc)`
            //Thêm đoạn sort lại danh sách hóa đơn trước khi cấp 
            if (AUTO) {
                //Ghep dữ liệu cần sort
                for (let inv of invs) {
                    inv.sortprop = `#${(moment(inv.idt, dtf).format('YYYYMMDD'))}#${inv.ic}`
                }
                invs = util.sortobj(invs, 'sortprop', 0)
            }
            let count = 1
            for (let inv of invs) {
                let ic, id, sec, form, serial, type, pid, idt, seq, s7, rs, rc
                try {
                    form = inv.form
                    serial = inv.serial
                    type = form.substr(0, 6)
                    ic = inv.ic
                    inv.sid = ic
                    pid = inv.pid
                    idt = moment(inv.idt, dtf)
                    id = await redis.incr("INC")
                    sec = util.generateSecCode(config.FNC_GEN_SEC, id)
                    inv.id = id
                    inv.sec = sec
                    inv.type = type
                    inv.name = util.tenhd(type)
                    inv.stax = taxc
                    inv.sname = org.name
                    inv.saddr = org.addr
                    inv.smail = org.mail
                    inv.stel = org.tel
                    inv.taxo = org.taxo
                    inv.sacc = org.acc
                    inv.sbank = org.bank
                    const ouob = await ous.obid(ou)
                    inv.systemCode = ouob.code
                    if (AUTO) {
                        const sqlcheck = `select count(*) "rcount" from s_inv where stax=:1 and (status in (2,3,5,6) or (status = 4 and xml is not null)) and form=:2 and serial=:3 and idt>:4 and rownum <= 100`
                        const resultcheck = await dbs.query(sqlcheck, [taxc, form, serial, (moment(idt.toDate()).endOf("day")).toDate()])
                        const rowscheck = resultcheck.rows
                        if (rowscheck[0].rcount > 0) throw new Error("Đã tồn tại hóa đơn được cấp số có ngày hóa đơn lớn hơn hóa đơn hiện tại \n (A numbered invoice already exists that has a larger invoice date than the current invoice)")
                        seq = await SERIAL.sequence(taxc, form, serial, inv.idt)
                        s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                        inv.seq = s7
                        inv.status = 2
                    }
                    else {
                        s7 = "......."
                        inv.status = 1
                    }
                    rs = await dbs.query(sql, [id, pid, sec, ic, idt.toDate(), ou, uid, JSON.stringify(inv)])
                    rc = rs.rowsAffected
                    if (rc == 1) {
                        if (pid) {
                            try {
                                const cde = `Bị thay thế bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${idt.format(mfd)} mã ${sec}`
                                let doc = await rbi(pid)
                                doc.status = 4
                                doc.cde = cde
                                doc.cdt = moment(idt.toDate()).format(dtf)
                                await dbs.query(`update s_inv set doc=:doc,cid=:cid,cde=:cde where id=:id and status=3`, [JSON.stringify(doc), id, cde, pid])
                            } catch (err) {
                                logger4app.error(err)
                            }
                        }
                    }
                    else throw new Error(`Không ghi được hóa đơn ${id} \n (The invoice ${id} cannot be recorded)`)
                    const sSysLogs = { fnc_id: 'inv_post', src: config.SRC_LOGGING_DEFAULT, dtl: 'Lập hóa đơn từ excel ', msg_id: id, doc: JSON.stringify(inv) };
                    logging.ins(req, sSysLogs, next)
                } catch (e) {
                    if (AUTO) {
                        if (!((e.errorNum == 1) && (String(e.message).toUpperCase().indexOf("S_INV_UID_01") >= 0))) await SERIAL.err(taxc, form, serial, seq)
                    }
                    throw e
                }
            }
            res.json(invs.length)
        }
        catch (err) {
            next(err)
        }
    },
    upds: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const body = req.body, invs = body.invs
            for (let inv of invs) {
                let pid, rs
                try {
                    pid = inv.pid
                    delete inv["pid"]
                    let doc = await rbi(pid)
                    let idt = doc.idt
                    for (let i in inv) {
                        doc[i] = inv[i]
                    }
                    doc.idt = idt
                    await dbs.query(`update s_inv set doc=:doc where id=:id and status=2`, [JSON.stringify(doc), pid])
                    const sSysLogs = { fnc_id: 'inv_adj', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật hóa đơn id ${pid}`, msg_id: pid, doc: JSON.stringify(doc) };
                    logging.ins(req, sSysLogs, next)
                } catch (e) {
                    throw e
                }
            }
            res.json(invs.length)
        }
        catch (err) {
            next(err)
        }
    },
    wcan: async (req, res, next) => {
        const token = SEC.decode(req), taxc = token.taxc, body = req.body, invs = body.invs, TLG = (config.ent === "tlg")
        let sql = `update s_inv set doc=:doc where id=:id and stax=:taxc and status`
        if (TLG) sql += '=3'
        else sql += ' in (2,3)'
        for (const item of invs) {
            try {
                let doc = await rbi(item.id)
                doc.status = 6
                doc.cancel = { typ: 3, ref: item.ref, rdt: item.rdt, rea: item.rea }
                const result = await dbs.query(sql, [JSON.stringify(doc), item.id, taxc])
                if (result.rowsAffected > 0) {
                    const pid = item.pid
                    if (pid) await dbs.query(`update s_inv set cid=null,cde=null where id=?`, [pid])
                    item.error = 'OK'
                }
                else item.error = 'NOT OK'
            }
            catch (e) {
                item.error = e.message
            }
        }
        res.json({ save: true, data: invs })
    },
    sav: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body
            if (grant && token.ou !== token.u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)
            const hdb = await Service.checkrefno(body)
            if (hdb.length > 0) throw new Error(`Số bút toán đã tồn tại: ${hdb.join()} \n(Entry number already exists: ${hdb.join()})`)

            const checkrefdate = await Service.checkrefdate(body)
            if (checkrefdate.length > 0) throw new Error(`Ngày bút toán lớn hơn ngày hóa đơn: ${checkrefdate.join(',')} \n(Ref Date is greater than Invoice Date: ${checkrefdate.join(',')})`)

            const taxc = token.taxc, uid = token.uid, ou = token.ou, org = await ous.obt(taxc), type = body.type, form = body.form, serial = body.serial, id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC, id)
            let seq, s7, result, row
            const dt = !['vib'].includes(config.ent) ? moment(body.idt) : moment(new Date()), eod = dt.endOf('day').toDate(), dts = dt.format(mfd), dtsc = dt.format('YYYYMMDD'), idtc = (moment(new Date())).format('YYYYMMDD')
            try {
                if (dtsc != idtc) {
                    result = await dbs.query(`select count(*) rc from s_inv where idt>:eod and type=:type and form=:form and serial=:serial and (status in (2,3,5,6) or (status = 4 and xml is not null)) and rownum <= 100`, [eod, type, form, serial])
                    row = result.rows[0]
                    if (row.rc > 0) throw new Error(`Tồn tại HĐ có ngày lớn hơn ${moment(body.idt).format('DD/MM/YYYY')} và đã cấp số \n (Invoice has a larger date  ${moment(body.idt).format('DD/MM/YYYY')} and issued a number)`)
                }
                body.id = id
                body.sec = sec
                body.stax = org.taxc
                body.sname = org.name
                body.saddr = org.addr
                body.smail = org.mail
                body.stel = org.tel
                body.taxo = org.taxo
                body.sacc = org.acc
                body.sbank = org.bank
                const ouob = await ous.obid(ou)
                body.systemCode = ouob.code
                seq = await SERIAL.sequence(taxc, form, serial, body.idt)
                s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                body.seq = s7
                if (config.ent == 'onprem') {
                    body.status = 3
                } else body.status = 2
                await dbs.query(`insert into s_inv (id,sec,idt,doc,ou,uc) values (:id,:sec,:idt,:doc,:ou,:uc)`, { id: id, sec: sec, idt: new Date(body.idt), doc: JSON.stringify(body), ou: ou, uc: uid })

                await Service.saverefno(body)

                const sSysLogs = { fnc_id: 'inv_post', src: config.SRC_LOGGING_DEFAULT, dtl: `Lập hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(body) };
                logging.ins(req, sSysLogs, next)
                res.json(id)
            }
            catch (err) {
                if (!((err.errorNum == 1) && (String(err.message).toUpperCase().indexOf("S_INV_UID_01") >= 0))) await SERIAL.err(taxc, form, serial, seq)
                throw err
            }
        } catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body
            if (grant && token.ou !== token.u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)
            const hdb = await Service.checkrefno(body)
            if (hdb.length > 0) throw new Error(`Số bút toán đã tồn tại: ${hdb.join()} \n(Entry number already exists: ${hdb.join()})`)
            const checkrefdate = await Service.checkrefdate(body)
            if (checkrefdate.length > 0) throw new Error(`Ngày bút toán lớn hơn ngày hóa đơn: ${checkrefdate.join(',')} \n(Ref Date is greater than Invoice Date: ${checkrefdate.join(',')})`)
            const taxc = token.taxc, uid = token.uid, ou = token.ou, org = await ous.obt(taxc), AUTO = (org.seq === 1 || body.sourceinv == "COREAPI"), type = body.type, form = body.form, serial = body.serial, id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC, id)
            let seq, s7, result, row
            const dt = moment(body.idt), eod = dt.endOf('day').toDate(), dts = dt.format(mfd), dtsc = dt.format('YYYYMMDD'), idtc = (moment(new Date())).format('YYYYMMDD')
            try {
                if (dtsc != idtc) {
                    result = await dbs.query(`select count(*) "rc" from s_inv where idt>:eod and type=:type and form=:form and serial=:serial and stax=:stax and (status in (2,3,5,6) or (status = 4 and xml is not null)) and status<>4 and rownum <= 100`, [eod, type, form, serial, taxc])
                    row = result.rows[0]
                    if (row.rc > 0) throw new Error(`Tồn tại HĐ có ngày lớn hơn ${moment(body.idt).format('DD/MM/YYYY')} và đã cấp số \n (Invoice has a larger date  ${moment(body.idt).format('DD/MM/YYYY')} and issued a number)`)
                }
                result = await dbs.query(`select count(*) "rc" from s_serial where fd>:eod and form=:form and serial=:serial and status=1 and taxc=:taxc `, [eod, form, serial, taxc])
                row = result.rows[0]
                if (row.rc > 0) throw new Error(`Ngày hiệu lực TBPH phải nhỏ hơn hoặc bằng ngày hóa đơn ${moment(body.idt).format('DD/MM/YYYY')}  \n (The serial effective date must be less than or equal to the invoice date ${moment(row.fd).format('DD/MM/YYYY')})`)

                body.id = id
                body.sec = sec
                body.stax = org.taxc
                body.sname = org.name
                body.saddr = org.addr
                body.smail = org.mail
                body.stel = org.tel
                body.taxo = org.taxo
                body.sacc = org.acc
                body.sbank = org.bank
                const ouob = await ous.obid(ou)
                body.systemCode = ouob.code

                if (body.sourceinv != "COREAPI" && config.ent == "vib") {
                    let result_vib, row_vib
                    result_vib = await dbs.query(`SELECT uses "uses" FROM s_serial WHERE taxc = :taxc AND form = :form AND serial = :serial`, [taxc, form, serial])
                    row_vib = result_vib.rows[0]
                    if (row_vib.uses != 1) throw new Error(`Dải số hoá đơn không hợp lệ! \n (Invoice Release Infomation is invalid)`)
                }

                if (AUTO) {
                    if (config.ent == "vib") {
                        body.idt = (moment(new Date())).format(dtf)
                    }
                    seq = await SERIAL.sequence(taxc, form, serial, body.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    body.seq = s7
                    body.status = 2
                }
                else body.status = 1
                if (ent != 'vib') {
                    await dbs.query(`insert into s_inv (id,sec,idt,doc,ou,uc) values (:id,:sec,:idt,:doc,:ou,:uc)`, { id: id, sec: sec, idt: new Date(body.idt), doc: JSON.stringify(body), ou: ou, uc: uid })
                } else {
                    let id_file = ''
                    if (body.id_file) id_file = '[' + body.id_file + ']'
                    await dbs.query(`insert into s_inv (id,sec,idt,doc,ou,uc,id_file) values (:id,:sec,:idt,:doc,:ou,:uc,:id_file)`, { id: id, sec: sec, idt: new Date(body.idt), doc: JSON.stringify(body), ou: ou, uc: uid, id_file: id_file })
                }

                await Service.saverefno(body)
                const sSysLogs = { fnc_id: 'inv_post', src: config.SRC_LOGGING_DEFAULT, dtl: `Lập hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(body) };
                logging.ins(req, sSysLogs, next)
                res.json(id)
            }
            catch (err) {
                if (AUTO) {
                    if (!((err.errorNum == 1) && (String(err.message).toUpperCase().indexOf("S_INV_UID_01") >= 0))) await SERIAL.err(taxc, form, serial, seq)
                }
                throw err
            }
        } catch (err) {
            next(err)
        }
    },
    adj: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, pid = body.pid
            await chkou(pid, token)
            const uid = token.uid, ou = token.ou, taxc = body.stax, form = body.form, serial = body.serial, org = await ous.obt(taxc), AUTO = (org.seq === 1), id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC, id), idt = new Date(body.idt)
            let conn, seq, s7, result, insid
            try {
                conn = await dbs.getConn()
                body.id = id
                body.sec = sec
                const ouob = await ous.obid(ou)
                body.systemCode = ouob.code
                if (AUTO) {
                    seq = await SERIAL.sequence(taxc, form, serial, body.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    body.seq = s7
                    body.status = 2
                }
                else {
                    s7 = "......."
                    body.status = 1
                }
                if (ent == 'vib') {
                    let id_file = ''
                    if (body.id_file) id_file = '[' + body.id_file + ']'
                    result = await conn.execute(`insert into s_inv (id,pid,sec,idt,doc,ou,uc,id_file) values (:id,:pid,:sec,:idt,:doc,:ou,:uc,:id_file) RETURNING ID INTO :insid`, { id: id, pid: pid, sec: sec, idt: idt, doc: JSON.stringify(body), ou: ou, uc: uid, id_file: id_file, insid: { type: oracledb.STRING, dir: oracledb.BIND_OUT } }, opt)
                } else {
                    result = await conn.execute(`insert into s_inv (id,pid,sec,idt,doc,ou,uc) values (:id,:pid,:sec,:idt,:doc,:ou,:uc) RETURNING ID INTO :insid`, { id: id, pid: pid, sec: sec, idt: idt, doc: JSON.stringify(body), ou: ou, uc: uid, insid: { type: oracledb.STRING, dir: oracledb.BIND_OUT } }, opt)
                }

                insid = result.outBinds.insid[0]
                const cde = `Bị điều chỉnh bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${moment(idt).format(config.mfd)} mã ${sec}`
                const pdoc = await rbi(pid)
                pdoc.cde = cde
                pdoc.cdt = moment(new Date()).format(dtf)
                await conn.execute(`update s_inv set cid=:1,cde=:2,doc=:3 where id=:4`, [id, cde, JSON.stringify(pdoc), pid], opt)
                await conn.commit()
                const sSysLogs = { fnc_id: 'inv_adj', src: config.SRC_LOGGING_DEFAULT, dtl: `Điều chỉnh hóa đơn id ${pid}`, msg_id: pid, doc: JSON.stringify(pdoc) };
                logging.ins(req, sSysLogs, next)
                res.json(insid)
            }
            catch (err) {
                if (AUTO) {
                    if (!((err.errorNum == 1) && (String(err.message).toUpperCase().indexOf("S_INV_UID_01") >= 0))) await SERIAL.err(taxc, form, serial, seq)
                }
                await conn.rollback()
                throw err
            }
            finally {
                await dbs.closeConn(conn)
            }
        }
        catch (err) {
            next(err)
        }
    },
    adjniss: async (req, res, next) => {
    },
    rep: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            let body = req.body, pid = body.pid
            await chkou(pid, token)

            const hdb = await Service.checkrefno(body)
            if (hdb.length > 0) throw new Error(`Số bút toán đã tồn tại: ${hdb.join()} \n (Entry number already exists: ${hdb.join()})`)
            const checkrefdate = await Service.checkrefdate(body)
            if (checkrefdate.length > 0) throw new Error(`Ngày bút toán lớn hơn ngày hóa đơn: ${checkrefdate.join(',')} \n(Ref Date is greater than Invoice Date: ${checkrefdate.join(',')})`)
            const uid = token.uid, ou = token.ou, taxc = body.stax, form = body.form, serial = body.serial, org = await ous.obt(taxc), AUTO = (org.seq === 1), id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC, id), idt = new Date(body.idt)
            let conn, seq, s7, result, insid
            try {
                conn = await dbs.getConn()
                body.id = id
                body.sec = sec
                const ouob = await ous.obid(ou)
                body.systemCode = ouob.code
                // AutoSeq exists nghĩa là PHAT HANH ở THAY THE
                if (AUTO || body.AutoSeq) {
                    if (body.AutoSeq) delete body["AutoSeq"]
                    seq = await SERIAL.sequence(taxc, form, serial, body.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    body.seq = s7
                    body.status = 2
                }
                else {
                    s7 = "......."
                    body.status = 1
                }
                if (ent == 'vib') {
                    let id_file = ''
                    if (body.id_file) id_file = '[' + body.id_file + ']'
                    result = await conn.execute(`insert into s_inv (id,pid,sec,idt,doc,ou,uc,id_file) values (:id,:pid,:sec,:idt,:doc,:ou,:uc,:id_file) RETURNING ID INTO :insid`, { id: id, pid: pid, sec: sec, idt: idt, doc: JSON.stringify(body), ou: ou, uc: uid, id_file: id_file, insid: { type: oracledb.STRING, dir: oracledb.BIND_OUT } }, opt)
                } else {
                    result = await conn.execute(`insert into s_inv (id,pid,sec,idt,doc,ou,uc) values (:id,:pid,:sec,:idt,:doc,:ou,:uc) RETURNING ID INTO :insid`, { id: id, pid: pid, sec: sec, idt: idt, doc: JSON.stringify(body), ou: ou, uc: uid, insid: { type: oracledb.STRING, dir: oracledb.BIND_OUT } }, opt)
                }


                await Service.saverefno(body)

                insid = result.outBinds.insid[0]
                const cde = `Bị thay thế bởi HĐ số ${s7} ký hiệu ${body.serial} mẫu số ${body.form} ngày ${moment(idt).format(config.mfd)} mã ${sec}`
                const doc = await rbi(pid)
                if (ent != "vib") {
                    doc.status = ent == "baca" ? 6 : 4
                }
                doc.cde = cde
                doc.cdt = moment(new Date()).format(dtf)
                await conn.execute(`update s_inv set doc=:1,cid=:2,cde=:3 where id=:5`, [JSON.stringify(doc), id, cde, pid], opt)
                await conn.commit()
                const sSysLogs = { fnc_id: 'inv_replace', src: config.SRC_LOGGING_DEFAULT, dtl: `Thay thế hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                logging.ins(req, sSysLogs, next)
                res.json(insid)
            }
            catch (err) {
                if (AUTO) {
                    if (!((err.errorNum == 1) && (String(err.message).toUpperCase().indexOf("S_INV_UID_01") >= 0))) await SERIAL.err(taxc, form, serial, seq)
                }
                await conn.rollback()
                throw err
            }
            finally {
                await dbs.closeConn(conn)
            }
        }
        catch (err) {
            next(err)
        }
    },
    go24: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, body = req.body, form = body.form, serial = body.serial, seq = body.seq, id = body.id, d = body.d, direct = d > 0 ? "tiến" : "lùi"
            await chkou(id, token)
            const idt = new Date(body.idt)
            idt.setDate(idt.getDate() + d)
            const dt = moment(idt), sod = dt.startOf('day').toDate(), eod = dt.endOf('day').toDate(), dts = dt.format(mfd)
            let result, row
            if (d > 0) {
                const today = moment().endOf('day').toDate()
                if (eod > today) throw new Error(`Ngày tiến hóa đơn ${dts} lớn hơn ngày hiện tại \n (The forward date of the invoice ${dts} is greater than the current date)`)
            }
            else {
                result = await dbs.query(`select count(*) "rc" from s_serial where taxc=:taxc and form=:form and serial=:serial and fd>:eod`, [taxc, form, serial, eod])
                row = result.rows[0]
                if (row.rc > 0) throw new Error(`Ngày lùi hóa đơn ${dts} không có TBPH \n (The back date of the invoice ${dts} doesn't have released information)`)
            }
            if (seq) {
                const iseq = parseInt(seq)
                result = await dbs.query(`select count(*) "rc" from s_inv where idt<:sod and form=:form and serial=:serial and (status=1 or ((status in (2,3,5,6) or (status = 4 and xml is not null)) and TO_NUMBER(seq)>:iseq))`, [sod, form, serial, iseq])
                row = result.rows[0]
                if (row.rc > 0) throw new Error(`Hóa đơn không ${direct} được: tồn tại HĐ có ngày nhỏ hơn ${dts} chưa cấp số hoặc có số lớn hơn ${seq} \n (Invoice can not ${direct}: Existing invoices with dates less than ${dts} and not issued a number or number greater than ${seq})`)
                result = await dbs.query(`select count(*) "rc" from s_inv where idt>:eod and form=:form and serial=:serial and (status in (2,3,5,6) or (status = 4 and xml is not null)) and TO_NUMBER(seq)<:iseq`, [eod, form, serial, iseq])
                row = result.rows[0]
                if (row.rc > 0) throw new Error(`Hóa đơn không ${direct} được: tồn tại HĐ có ngày lớn hơn ${dts} và số lớn hơn ${seq} \n (Invoice can not ${direct}: Existing invoices with dates greater than ${dts} and number greater than ${seq})`)
            }
            else {
                result = await dbs.query(`select count(*) "rc" from s_inv where idt>:eod and form=:form and serial=:serial and (status in (2,3,5,6) or (status = 4 and xml is not null))`, [eod, form, serial])
                row = result.rows[0]
                if (row.rc > 0) throw new Error(`Hóa đơn không ${direct} được : tồn tại HĐ có ngày lớn hơn ${dts} và đã cấp số \n (Invoice can not ${direct}: Existing invoices with dates greater than ${dts} and issued a number`)
            }
            result = await dbs.query(`select doc "doc" from s_inv where id=:1 and status in (1,2)`, [id])
            row = result.rows[0]
            const doc = JSON.parse(row.doc)
            doc.idt = moment(sod).format(dtf)
            await dbs.query(`update s_inv set idt=:sod,doc=:doc where id=:id and status in (1, 2)`, [sod, JSON.stringify(doc), id])
            res.json(1)
        } catch (err) {
            next(err)
        }
    },
    status: async (req, res, next) => {
        try {
            const token = SEC.decode(req), id = req.params.id
            let status = req.params.status
            await chkou(id, token)
            const doc = await rbi(id)
            doc.status = status
            let oldstatus = doc.status
            let sSysLogs
            if (status == 3 || status == 2) {
                delete doc["cancel"]
                const r = await dbs.query(`select 1 from s_inv where id=:id and xml is not null AND rownum <= 1`, [id])
                if (r.rows.length > 0) status = 3
                else status = 2
                //Xóa biên bản
                if (UPLOAD_MINUTES) await dbs.query(`DELETE FROM s_inv_file WHERE id =:id`, [id])
                sSysLogs = { fnc_id: 'inv_istatus', src: config.SRC_LOGGING_DEFAULT, dtl: `Chuyển đổi trạng thái hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
            }
            else if (status == 4) {
                if (doc.pid && doc.adj.typ == 2) await dbs.query(`update s_inv set cid=null,cde=null where id=:id`, [doc.pid])
                if (doc.pid && doc.adj.typ == 1 && ent == "baca") {
                    let sqlSub2 = `select doc "doc" from s_inv where id='${doc.pid}'`
                    const docRS = await dbs.query(sqlSub2);
                    const rsDOC = docRS.rows
                    const doc2 = JSON.parse(rsDOC[0].doc)
                    doc2.status = 3
                    await dbs.query(`update s_inv set doc=:doc,cid=null,cde=null where id=:id`, [JSON.stringify(doc2), doc.pid])
                }
                doc.cdt = moment(new Date()).format(dtf)
                doc.cancel = { typ: 3, ref: id, rdt: moment().format(dtf) }
                sSysLogs = { fnc_id: 'inv_cancel', src: config.SRC_LOGGING_DEFAULT, dtl: `Hủy hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
            }
            let result = await dbs.query(`update s_inv set doc=:doc where id=:id and stax=:stax`, [JSON.stringify(doc), id, token.taxc])
            if (result.rowsAffected && (oldstatus == 3 || oldstatus == 6)) {
                if ((status == 4) && !util.isEmpty(doc.bmail)) {
                    doc.status = 4
                    let mailstatus = await email.rsmqmail(doc)
                    if (mailstatus) await Service.updateSendMailStatus(doc)
                }
                if ((status == 4) && !util.isEmpty(doc.btel)) {
                    let smsstatus = await sms.smssend(doc)
                    if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                }
                logging.ins(req, sSysLogs, next)
            }
            res.json(1)
        } catch (err) {
            next(err)
        }
    },
    cancel: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, id = req.params.id
            await chkou(id, token)
            const doc = await rbi(id)
            if (doc.pid && doc.adj.typ == 2) await dbs.query(`update s_inv set cid=null,cde=null where id=:id`, [doc.pid])
            doc.cdt = moment(new Date()).format(dtf)
            let oldstatus = doc.status
            doc.status = 4
            doc.cancel = { typ: body.typ, ref: body.ref, rdt: body.rdt, rea: body.rea }
            let result = await dbs.query(`update s_inv set doc=:doc where id=:id and stax=:stax`, [JSON.stringify(doc), id, token.taxc])
            if (result.rowsAffected && (oldstatus == 3 || oldstatus == 6)) {
                if (useJobSendmail) {
                    await Service.insertDocTempJob(id, doc, ``)
                } else {
                    if (!util.isEmpty(doc.bmail)) {
                        let mailstatus = await email.rsmqmail(doc)
                        if (mailstatus) await Service.updateSendMailStatus(doc)
                    }
                    if (!util.isEmpty(doc.btel)) {
                        let smsstatus = await sms.smssend(doc)
                        if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                    }
                }
            }
            const sSysLogs = { fnc_id: 'inv_cancel', src: config.SRC_LOGGING_DEFAULT, dtl: `Hủy hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
            logging.ins(req, sSysLogs, next)
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },
    trash: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, body = req.body, id = req.params.id
            await chkou(id, token)
            const doc = await rbi(id)
            doc.status = 6
            doc.cancel = { typ: body.typ, ref: body.ref, rdt: body.rdt, rea: body.rea }
            const result = await dbs.query(`update s_inv set doc=:doc where id=:id and stax=:staxc and status in (2,3)`, [JSON.stringify(doc), id, taxc])
            res.json(result.rowsAffected)
            const sSysLogs = { fnc_id: 'inv_adj', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
            logging.ins(req, sSysLogs, next)
        }
        catch (err) {
            next(err)
        }
    },
    istatus: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, body = req.body, ids = body.ids, os = body.os, now = moment().format(dtf)
            let rc = 0, ns = body.ns
            for (let id of ids) {
                try {
                    const doc = await rbi(id)
                    let doclog, sSysLogs
                    let oldstatus = doc.status
                    if (ns == 4) {
                        if (doc.pid && doc.adj.typ == 2) await dbs.query(`update s_inv set cid=null,cde=null where id=:id`, [doc.pid])
                        if (doc.pid && doc.adj.typ == 1 && ent == "baca") {
                            let sqlSub2 = `select doc "doc" from s_inv where id='${doc.pid}'`
                            const docRS = await dbs.query(sqlSub2);
                            const rsDOC = docRS.rows
                            const doc2 = JSON.parse(rsDOC[0].doc)
                            doc2.status = 3
                            await dbs.query(`update s_inv set doc=:doc,cid=null,cde=null where id=:id`, [JSON.stringify(doc2), doc.pid])
                        }
                        //Lay du lieu doc truoc khi xoa de luu log
                        doclog = await rbi(id)
                        doc.cdt = moment(new Date()).format(dtf)
                        if (!doc.cancel) doc.cancel = { typ: 3, ref: id, rdt: now }
                        sSysLogs = { fnc_id: 'inv_cancel', src: config.SRC_LOGGING_DEFAULT, dtl: `Hủy hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doclog) };
                    } else {
                        //Lay du lieu doc truoc khi xoa de luu log
                        doclog = await rbi(id)
                        if (ns == 2 || ns == 3) {
                            const r = await dbs.query(`select 1 from s_inv where id=:id and xml is not null AND rownum <= 1`, [id])
                            if (r.rows.length > 0) ns = 3
                            else ns = 2
                        }
                        sSysLogs = { fnc_id: 'inv_istatus', src: config.SRC_LOGGING_DEFAULT, dtl: `Chuyển đổi trạng thái hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doclog) };
                    }
                    doc.status = ns
                    const result = await dbs.query(`update s_inv set doc=:doc where id=:id and stax=:staxc and status=:status`, [JSON.stringify(doc), id, taxc, os])
                    if (result.rowsAffected && (oldstatus == 3 || oldstatus == 6)) {
                        if (useJobSendmail) {
                            if (((ns == 4) && !util.isEmpty(doc.bmail)) || ((ns == 4) && !util.isEmpty(doc.btel))) {
                                doc.status = 4
                                await Service.insertDocTempJob(doc.id, doc, ``)
                            }
                        } else {
                            if ((ns == 4) && !util.isEmpty(doc.bmail)) {
                                doc.status = 4
                                if (!util.isEmpty(doc.bmail)) {
                                    let mailstatus = await email.rsmqmail(doc)
                                    if (mailstatus) await Service.updateSendMailStatus(doc)
                                }
                            }
                            if ((ns == 4) && !util.isEmpty(doc.btel)) {
                                doc.status = 4
                                if (!util.isEmpty(doc.btel)) {
                                    let smsstatus = await sms.smssend(doc)
                                    if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                                }
                            }
                        }
                        rc++
                    }

                    logging.ins(req, sSysLogs, next)
                } catch (e) {
                    logger4app.error(e)
                }
            }
            res.json(rc)
        }
        catch (err) {
            next(err)
        }
    },
    del: async (req, res, next) => {
        let conn
        try {
            const token = SEC.decode(req), id = req.params.id, pid = req.params.pid
            await chkou(id, token)
            //Lay du lieu doc truoc khi xoa de luu log
            let doclog = await rbi(id)
            conn = await dbs.getConn()
            let body = await rbi(id)

            await Service.delrefno(doclog)

            const result = await conn.execute(`delete from s_inv where id=:id and stax=:stax and status=:status`, [id, token.taxc, 1], opt)
            if (result.rowsAffected) {
                let doc
                if (pid > 0) {
                    doc = await rbi(pid)
                    doc.status = 3
                    //Xóa doc.cde , nếu không có cũng ko ảnh hưởng.
                    delete doc["cde"]
                    delete doc["cdt"]
                    await conn.execute(`update s_inv set doc=:doc,cid=null,cde=null where id=:pid`, [JSON.stringify(doc), pid], opt)
                    //Xóa biên bản
                    await conn.execute(`DELETE FROM s_inv_file WHERE id =:pid`, [pid], opt)
                }
            }
            await conn.commit()
            const sSysLogs = { fnc_id: 'inv_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doclog) };
            logging.ins(req, sSysLogs, next)
            res.json(1)
        }
        catch (err) {
            await conn.rollback()
            next(err)
        }
        finally {
            await dbs.closeConn(conn)
        }
    },
    relate: async (req, res, next) => {
        try {
            const token = SEC.decode(req), id = req.params.id, doc = await rbi(id), sec = doc.sec, root = doc.root, rsec = root ? root.sec : null
            let sql, result, binds = { stax: token.taxc, sec: sec }
            sql = `select id "id",idt "idt",type "type",form "form",serial "serial",seq "seq",sec "sec",status "status",adjdes "adjdes",cde "cde" from s_inv a where a.stax=:stax and a.sec<>'{'||:sec||'}' and (json_value(a.doc format json , '$.root.sec' returning varchar2(10) null on error) = :sec`
            if (rsec) {
                sql += " or a.sec=:rsec or json_value(a.doc format json , '$.root.sec' returning varchar2(10) null on error) = :rsec"
                binds.rsec = rsec
            }
            sql += ") order by id"
            result = await dbs.query(sql, binds)
            res.json(result.rows)
        }
        catch (err) {
            next(err)
        }
    },
    relateadjs: async (req, res, next) => {
        try {
            const token = SEC.decode(req), id = req.params.id
            let sql, result
            sql = `select id "id",idt "idt",type "type",form "form",serial "serial",seq "seq",sec "sec",status "status",adjdes "adjdes",cde "cde" from s_inv a where id in (${id}) order by id`

            result = await dbs.query(sql, [id])
            res.json(result.rows)
        }
        catch (err) {
            next(err)
        }
    },
    deleteinv: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const taxc = token.taxc, uid = token.uid, ou = token.ou, org = await ous.obt(taxc), AUTO = (org.seq === 1), body = req.body, invs = body.invs
            let count = 0
            let conn = await dbs.getConn()
            let status, message
            try {
                const token = SEC.decode(req)

                conn = await dbs.getConn()
                for (let inv of invs) {
                    await chkou(inv.id, token)
                    //Lay du lieu doc truoc khi xoa de luu log
                    let doclog = await rbi(inv.id)
                    let iv = await rbi(inv.id)

                    // if(ent == "baca" ){
                    //     if(doclog.pid && (doclog.adj.typ == 1 || doclog.adj.typ == 2) && (doclog.status=="2" || doclog.status=="1")){
                    //         let sqlSub2=`select doc "doc" from s_inv where id='${doclog.pid}'`
                    //         const docRS= await dbs.query(sqlSub2);
                    //         const rsDOC=docRS.rows
                    //         const doc2=JSON.parse(rsDOC[0].doc)
                    //         doc2.status=3
                    //         await dbs.query(`update s_inv set doc=:doc,cid=null,cde=null where id=:id`,[JSON.stringify(doc2), doclog.pid])
                    //     }
                    // }

                    if (doclog.pid && (doclog.adj.typ == 1 || doclog.adj.typ == 2) && (doclog.status == "2" || doclog.status == "1")) {
                        let sqlSub2 = `select doc "doc" from s_inv where id='${doclog.pid}'`
                        const docRS = await dbs.query(sqlSub2);
                        const rsDOC = docRS.rows
                        const doc2 = JSON.parse(rsDOC[0].doc)
                        doc2.status = 3
                        await dbs.query(`update s_inv set doc=:doc,cid=null,cde=null where id=:id`, [JSON.stringify(doc2), doclog.pid])
                    }
                    await Service.delrefno(doclog)

                    await conn.execute(`delete from s_inv where id=:id and status = 1`, [inv.id], opt)
                    count++
                    const sSysLogs = { fnc_id: 'inv_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa hóa đơn id ${inv.id}`, msg_id: inv.id, doc: JSON.stringify(doclog) };
                    logging.ins(req, sSysLogs, next)
                }
                await conn.commit()
                status = 1
                message = 'Success'
            } catch (e) {
                status = 2
                throw e
            }

            res.json({ message: message, status: status, countseq: count })
        }
        catch (err) {
            next(err)
        }
    },
    mailinvs: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, invs = body.invs
            let status, message

            try {
                for (let inv of invs) {
                    let docmail = await rbi(inv.id)
                    let doc = JSON.parse(JSON.stringify(docmail)), xml = ''
                    if (config.ent == 'vcm') {
                        let taxc_root = await ous.pidtaxc(doc.ou)
                        doc.taxc_root = taxc_root
                    }
                    //logger4app.debug(`begin send mail : ${doc.stax} - ${doc.form} - ${doc.serial} - ${doc.seq}`)
                    if (useJobSendmail) {
                        await Service.insertDocTempJob(doc.id, doc, xml)
                    } else {
                        if (!util.isEmpty(doc.bmail)) {
                            let mailstatus = await email.senddirectmail(doc, xml)
                            if (mailstatus) await Service.updateSendMailStatus(doc)
                        }
                        if (!util.isEmpty(doc.btel)) {
                            let smsstatus = await sms.smssend(doc)
                            if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                        }
                    }
                }
                status = 1
                message = 'Success'
            } catch (e) {
                status = 2
                throw e
            }

            res.json({ message: message, status: status })
        }
        catch (err) {
            next(err)
        }
    },
    sendmaildoc: async (req, res, next) => {
        try {
            let doc = req.body, xml = doc.xml
            //logger4app.debug(`begin send mail : ${doc.stax} - ${doc.form} - ${doc.serial} - ${doc.seq}`)
            if (config.useJobSendmailAPI) {
                await Service.insertDocTempJobAPI(doc.id, doc, xml)
            } else {
                if (!util.isEmpty(doc.bmail)) {
                    let mailstatus = await email.senddirectmail(doc, xml)
                    if (mailstatus) await Service.updateSendMailStatus(doc)
                }
                if (!util.isEmpty(doc.btel)) {
                    let smsstatus = await sms.smssend(doc)
                    if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                }
            }
            //logger4app.debug(`end send mail : ${doc.stax} - ${doc.form} - ${doc.serial} - ${doc.seq}`)
            res.json({ result: "1" })
        }
        catch (err) {
            throw err
        }

    },
    signdoc: async (req, res, next) => {
        try {
            let doc = req.body
            const org = await ous.gettosign(doc.stax)
            //logger4app.debug(org)
            let signtype = org.sign
            let xml, binds, result
            let sql

            if (signtype == 1) {
                const usr = org.usr, pwd = util.decrypt(org.pwd)
                xml = util.j2x((await Service.getcol(doc)), doc.id)
                const arr = [{ inc: id, xml: Buffer.from(xml).toString("base64") }]
                res.json({ lib: usr, sid: 0, pin: pwd, mst: doc.stax, date: new Date(), ref: config.PATH_XML, arr: arr })
            }
            else {
                if (signtype == 2) {
                    const pwd = util.decrypt(org.pwd)
                    const ca = await sign.ca(doc.stax, pwd)
                    xml = util.signature(sign.sign(ca, util.j2x((await Service.getcol(doc)), doc.id), doc.id), doc.idt)
                }
                else if (signtype == 3) {
                    sql = `select id,doc,bmail,pid from s_inv where id=:1 and stax=:2 and status=2`
                    binds = [doc.id, doc.stax]
                    result = await dbs.query(sql, binds)
                    const rows = result.rows
                    if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
                    const usr = org.usr, pwd = util.decrypt(org.pwd)
                    const kqs = await fcloud.xml(usr, pwd, doc.stax, rows), kq = kqs[0]
                    xml = kq.xml
                }
                else {
                    xml = await hsm.signxml(doc.stax, util.j2x((await Service.getcol(doc)), doc.id))
                    xml = util.signature(xml, doc.idt)
                }
            }
            res.json({ xml: xml })
        }
        catch (err) {
            next(err)
        }

    },
    signxml: async (req, res, next) => {
        try {
            const xml = req.body.xml, stax = req.body.stax, id = req.body.id, idt = req.body.idt
            const org = await ous.gettosign(stax)
            //logger4app.debug(org)
            let signtype = org.sign
            let binds, result, xmlresult
            let sql

            if (signtype == 1) {
                const usr = org.usr, pwd = util.decrypt(org.pwd)
                xmlresult = util.j2x((await Service.getcol(doc)), doc.id)
                const arr = [{ inc: id, xml: Buffer.from(xml).toString("base64") }]
                res.json({ lib: usr, sid: 0, pin: pwd, mst: stax, date: new Date(), ref: config.PATH_XML, arr: arr })
            }
            else {
                if (signtype == 2) {
                    const pwd = util.decrypt(org.pwd)
                    const ca = await sign.ca(stax, pwd)
                    xmlresult = util.signature(sign.sign(ca, xml, id), idt)
                }
                else if (signtype == 3) {
                    sql = `select id,doc,bmail,pid from s_inv where id=:1 and stax=:2 and status=2`
                    binds = [id, stax]
                    result = await dbs.query(sql, binds)
                    const rows = result.rows
                    if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
                    const usr = org.usr, pwd = util.decrypt(org.pwd)
                    const kqs = await fcloud.xml(usr, pwd, stax, rows), kq = kqs[0]
                    xmlresult = kq.xml
                }
                else {
                    xmlresult = await hsm.signxml(stax, xml)
                    xmlresult = util.signature(xmlresult, idt)
                }
            }
            res.json({ xml: xmlresult })
        }
        catch (err) {
            next(err)
        }

    },
    syncRedisDb: async (req, res, next) => {
        try {
            var sqlinv = 'select max(id) id from s_inv'
            logger4app.debug('sqlinv : ', sqlinv)
            var resultinv = await dbs.query(sqlinv, [])
            var maxid = resultinv.rows[0].id
            logger4app.debug('resultinv.recordset[0] : ', resultinv.rows[0])
            logger4app.debug('maxid : ', maxid)
            var keyidinv = `INC`, curid = await redis.get(keyidinv)
            if (curid < maxid) {
                await redis.set(keyidinv, maxid)
            }
            res.json({ result: "1" })
        } catch (err) {
            next(err)
        }
    },
    print: async (req, res, next) => {
        try {
            const iwh = await Service.iwh(req, true)
            const result = await dbs.query(`select doc "doc" from s_inv ${iwh.where} order by serial,seq`, iwh.binds)
            const rows = result.rows
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn \n No invoice be found`)
            const token = SEC.decode(req), org = await ous.org(token), idx = org.temp, filter = JSON.parse(req.query.filter), fn = util.fn(filter.form, idx), tmp = await util.template(fn)
            // const token = SEC.decode(req), mst = token.mst, org = await ous.org(token), qrc = org.qrc == 1, filter = JSON.parse(req.query.filter)
            // for (const row of rows) {
            //     const doc = row.doc
            //     util.viewadj(doc)
            //if (qrc) row.qrcode = await util.qrcode(row.doc)
            // }
            for (const row of rows) {
                let doc = JSON.parse(row.doc)
                doc.tempfn = fn
                row.doc = doc
            }
            res.json({ docs: rows, tmp: tmp })
        } catch (err) {
            next(err)
        }
    },
    printmerge: async (req, res, next) => {
        try {
            const iwh = await Service.iwh(req, true)
            const result = await dbs.query(`select doc "doc" from s_inv ${iwh.where} order by serial,seq`, iwh.binds)
            const rows = result.rows
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn \n No invoice be found`)
            const token = SEC.decode(req), org = await ous.org(token), idx = org.temp, filter = JSON.parse(req.query.filter), fn = util.fn(filter.form, idx), tmp = await util.template(fn)
            // const token = SEC.decode(req), mst = token.mst, org = await ous.org(token), qrc = org.qrc == 1, filter = JSON.parse(req.query.filter)
            // for (const row of rows) {
            //     const doc = row.doc
            //     util.viewadj(doc)
            //if (qrc) row.qrcode = await util.qrcode(row.doc)
            // }
            let jsons = []
            for (const row of rows) {
                let doc = JSON.parse(row.doc)
                doc.tempfn = fn
                row.doc = doc
                jsons.push({ doc: row.doc })
            }
            let respdf = await util.jsReportRenderAttachPdfs(jsons)
            res.json(respdf)
        } catch (err) {
            next(err)
        }
    },
    vib_cancel: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, tran_no = body.tran_no
            // await chkou(id, token)
            const doc = await rde(tran_no)
            if (doc.pid && doc.adj.typ == 2) await dbs.query(`update s_inv set cid=null,cde=null where id=:id`, [doc.pid])
            doc.cdt = moment(new Date()).format(dtf)
            let oldstatus = doc.status
            doc.status = 4
            doc.cancel = { typ: body.typ, ref: body.ref, rdt: body.rdt, rea: body.rea }
            let result = await dbs.query(`update s_inv set doc=:doc where id=:id and stax=:stax`, [JSON.stringify(doc), doc.id, token.taxc])
            if (result.rowsAffected && oldstatus == 3) {
                if (!util.isEmpty(doc.bmail)) {
                    doc.status = 4
                    let mailstatus = await email.rsmqmail(doc)
                    if (mailstatus) await Service.updateSendMailStatus(doc)
                }
                if (!util.isEmpty(doc.btel)) {
                    let smsstatus = await sms.smssend(doc)
                    if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                }
            }
            const sSysLogs = { fnc_id: 'inv_cancel', src: config.SRC_LOGGING_DEFAULT, dtl: `Hủy hóa đơn id ${doc.id}`, msg_id: doc.id, doc: JSON.stringify(doc) };
            logging.ins(req, sSysLogs, next)
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },
    upload: async (req, res, next) => {
        let ok = false, err
        try {
            const token = SEC.decode(req), body = req.body, tran_no = body.tranno
            // await chkou(id, token)
            const doc = await rde(tran_no)
            const taxc = token.taxc
            const file = req.file, type = doc.type, id = doc.id
            let typemin = ""
            if (type) {
                if (type == 1) {
                    typemin = "rep"
                } else if (type == 2) {
                    typemin = "adj"
                }
            }
            let content = file.buffer, contentb64 = content.toString('base64')
            let sql, result, binds
            sql = `select 1 from s_inv_file where id=:1 and type=:2`
            binds = [id, typemin]
            result = await dbs.query(sql, binds)
            if (result.rows.length) {
                sql = "update s_inv_file set id=:1, type=:2, content=:3 where id=:4 and type=:5"
                binds = [id, type, contentb64, id, typemin]
            } else {
                sql = "insert into s_inv_file (id, type, content) VALUES (:1,:2,:3)"
                binds = [id, type, contentb64]
            }
            result = await dbs.query(sql, binds)
            if (result.rowsAffected) ok = true
        } catch (error) {
            err = error.message
        }
        res.json({ ok, err })
    },
    note: async (req, res, next) => {
        try {
            let result, doc, sql
            const query = req.body
            if (Array.isArray(query.id)) {
                for (let id of query.id) {
                    doc = await rbi(id)
                    doc.note = doc.note + query.note_no + " (" + query.idt + ")  "
                    sql = "update s_inv set doc=:1 where id = :2"
                    result = await dbs.query(sql, [JSON.stringify(doc), id])
                }

            } else {
                doc = await rbi(query.id)
                doc.note = doc.note + query.note_no + " (" + query.idt + ")  "
                sql = "update s_inv set doc=:1 where id = :2"
                result = await dbs.query(sql, [JSON.stringify(doc), query.id])
            }
            res.json(result)
        } catch (error) {
            next(error)
        }
    },
    noteseq: async (req, res, next) => {
        try {
            let result, rows, note = ""
            const query = req.body
            let sql = "select note from s_inv where id = :1"
            result = await dbs.query(sql, [query.id])
            rows = result.rows
            for (let row of rows) {
                if (row.NOTE != null) {
                    note += row.NOTE
                }
            }
            res.json(note)
        } catch (error) {
            next(error)
        }

    },
    getIDFile: async (req, res, next) => {
        try {
            const id = req.params.id
            let sql, result, ret, count
            sql = `Select id_file from s_inv where id = ${id}`
            result = await dbs.query(sql)
            count = JSON.parse(result.rows[0].ID_FILE)
            ret = { data: count }
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    delFile: async (req, res, next) => {
        try {
            const body = req.body, arrID = body.arrID, id = body.id
            let sql, result, ret, count, maSQL, countL = 0, newFILE
            sql = `Select id_file from s_inv where id = '${id}'`
            result = await dbs.query(sql)
            count = JSON.parse(result.rows[0].ID_FILE)
            for (let i = 0; i < count.length; i++) {
                for (let j = 0; j < arrID.length; j++) {
                    if ((count[i].id).localeCompare(arrID[j]) == 0 && count[i].id) {
                        count.splice(i, 1);
                        i = i - 1;
                        break;
                    }
                }
            }
            ret = count.length
            if (count.length == 0) {
                count = ''
            } else {
                count = JSON.stringify(count)
            }
            maSQL = `update s_inv set ID_FILE = '${count}' where id = '${id}'`
            await dbs.query(maSQL)
            res.json(ret)
        }
        catch (err) {
            next(err)
        }

    },
    changeDate: async (req, res, next) => {
        try {
            const body = req.body
            let id = body.idEnv, dateChange = moment(body.dateC, dtf), result, sqlDate, resultD, row
            let doc = await rbi(id)
            //Lấy ngày phát hành
            sqlDate = `Select fd "fd",td "td" from s_serial where form = :1 and serial = :2 and taxc = :3`
            resultD = await dbs.query(sqlDate, [doc.form, doc.serial, doc.stax])
            let fd = resultD.rows[0].fd, td = resultD.rows[0].td
            const dt = moment(body.dateC), eod = dt.endOf('day').toDate(), dtsc = dt.format('YYYYMMDD'), idtc = (moment(new Date())).format('YYYYMMDD')
            //Check ngay nho hon so lon hon hoac ngay lon hon so nho hon
            result = await dbs.query(`select count(*) "rc" from s_inv where ((trunc(idt)>to_date('${moment(dt).format('DD/MM/YYYY')}','dd/mm/yyyy') and to_number(seq)<to_number('${doc.seq}')) or (trunc(idt)<to_date('${moment(dt).format('DD/MM/YYYY')}','dd/mm/yyyy') and to_number(seq)>to_number('${doc.seq}'))) and type=:type and form=:form and serial=:serial and stax=:stax and status>1 and status<>4 and id<>${id} and rownum <= 100`, [doc.type, doc.form, doc.serial, doc.stax])
            row = result.rows[0]
            if (row.rc > 0) throw new Error(`Tồn tại HĐ có ngày lớn hơn, số nhỏ hơn hoặc ngày nhỏ hơn, số lớn hơn \n (Invoice has a larger date, smaller invoice number or smaller date, larger date)`)

            if (dateChange.toDate() < fd) {
                fd = moment(fd).format('DD-MM-YYYY HH:mm:ss')
                res.json({ fd: fd, stt: 0 })
            } else if (td && dateChange.toDate() > td) {
                td = moment(td).format('DD-MM-YYYY HH:mm:ss')
                res.json({ td: td, stt: 1 })
            } else {
                //Update ngày hóa đơn
                doc.idt = moment(dateChange).format('YYYY-MM-DD HH:mm:ss')
                let sql = `Update s_inv set idt = :1, doc = :2 where id =:3`
                result = await dbs.query(sql, [dateChange.toDate(), JSON.stringify(doc), id])
                const sSysLogs = { fnc_id: 'inv_date_change', src: config.SRC_LOGGING_DEFAULT, dtl: `Sửa ngày hóa đơn của hóa đơn ${id} từ ${doc.idt} sang ${dateChange}`, msg_id: id, doc: JSON.stringify(body) };
                await logging.ins(req, sSysLogs, next)
                return res.json(1)
            }
        } catch (err) {
            next(err)
        }
    },
    changeStatus: async (req, res, next) => {
        try {
            const body = req.body
            let id = body.id, result, oldST
            let doc = await rbi(id)
            oldST = doc.status
            doc.status = 2
            let sql = `Update s_inv set doc = :1 where id =:2`
            result = await dbs.query(sql, [JSON.stringify(doc), id])
            const sSysLogs = { fnc_id: 'inv_status_change', src: config.SRC_LOGGING_DEFAULT, dtl: `Sửa trạng thái hóa đơn của hóa đơn ${id} từ ${exSTATUS(oldST)} sang ${exSTATUS(doc.status)}`, msg_id: id, doc: JSON.stringify(doc) };
            await logging.ins(req, sSysLogs, next)
            return res.json(1)
        } catch (err) {
            next(err)
        }
    },
    upNumberTran: async (req, res, next) => {
        try {
            const body = req.body
            let id = body.id, result, c2 = body.number_tran, c0 = body.module, ic = c0 + c2
            let doc = await rbi(id)
            doc.c2 = c2
            doc.c0 = c0
            let sql = `Update s_inv set doc = :1, ic = :2 where id =:3`
            result = await dbs.query(sql, [JSON.stringify(doc), ic, id])
            const sSysLogs = { fnc_id: 'inv_numbertran_update', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật số bút toán cho hóa đơn ${id}`, msg_id: id, doc: JSON.stringify(doc) };
            await logging.ins(req, sSysLogs, next)
            return res.json(1)
        } catch (err) {
            next(err)
        }
    },
    delAll: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body
            let sql, result, binds, rows, sqlinv, resultinv, query = { filter: JSON.stringify(body) }, reqtmp = { query: query, token: token }, iwh = await Service.iwh(reqtmp, false, true), countseq = 0
            //Chi cho phép xóa các hóa đơn với trạng thai nhất định
            if (!['1'].includes(body.status)) throw new Error(`Chỉ được xóa các hóa đơn với các trạng thái được phép xóa \n (Only invoices with statuses allowed to be deleted can be deleted)`)
            //Check so ban ghi truoc khi thuc hien xoa
            const sqlcount = `select count(*) total from s_inv ${iwh.where}`
            const resultcount = await dbs.query(sqlcount, iwh.binds)
            if (resultcount.rows[0].total > config.MAX_ROW_DELETE_ALL) throw new Error(`Số lượng bản ghi cần xóa vượt quá giới hạn cho phép \n (The number of records to be deleted exceeds the allowed limit)`)
            //Lấy các hóa đơn cần xóa
            sqlinv = `select pid "pid", id "id" from s_inv ${iwh.where}`
            resultinv = await dbs.query(sqlinv, iwh.binds)
            rows = resultinv.rows
            for (let inv of rows) {
                await chkou(inv.id, token)
                //Lay du lieu doc truoc khi xoa de luu log
                let doclog = await rbi(inv.id)
                if (config.ent == "scb") {
                    for (const item of doclog.items) {
                        if (item.tranID) {
                            await dbs.query(`update s_trans set status = :1, inv_id = null, inv_date = null where tranID = :2`, [item.statusGD, item.tranID])
                        }
                    }
                }
                await dbs.query(`delete from s_inv where id=:1`, [inv.id])
                await Service.delrefno(doclog)
                await dbs.query(`update  s_inv set cid=null,cde=null where id=:1 and cid=:2`, [inv.pid, inv.id])
                countseq++
                if (config.ent == 'mzh') {
                    await dbs.query(`update  s_trans set status=stattus_temp,inv_id=inv_id_tmp,exrt=null,inv_date=null,ma_nv=null,trancol=null where inv_id=:1`, [inv.id])
                }
                const sSysLogs = { fnc_id: 'inv_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa hóa đơn id ${inv.id}`, msg_id: inv.id, doc: JSON.stringify(doclog) };
                logging.ins(req, sSysLogs, next)
            }

            res.json({ countseq: countseq })
        } catch (err) {
            next(err)
        }
    }
}
module.exports = Service