"use strict"
const path = require("path")
const fs = require("fs")
const docxt = require("docxtemplater")
const xlsxtemp = require("xlsx-template")
const jszip = require("jszip")
const moment = require("moment")
const util = require("../util")
const config = require("../config")
const logger4app = require("../logger4app")
const redis = require("../redis")
const sec = require("../sec")
const dbs = require("./dbs")


const Service = {
    
    get: async (req, res, next) => {
        try {
            const query = req.query, filter = JSON.parse(query.filter)
            const sort = query.sort, start = query.start ? query.start : 0, count = query.count ? query.count : 10
            let where = " where dt between :fd and :td", order, sql, result, rows, ret, val
            let binds = [new Date(filter.fd), new Date(moment(filter.td).endOf("day"))]
            let i = 1
            Object.keys(filter).forEach((key) => {
                val = filter[key]
                if (val) {
                    switch (key) {
                        case "fd":
                            break
                        case "td":
                            break
                        case "srclog":
                            where += ` and SRC=:${i++}`
                            binds.push(val)
                            break
                        case "type":
                            where += ` and FNC_ID=:${i++}`
                            binds.push(val)
                            break
                        case "user":
                            where += ` and USER_ID like :${i++}`
                            binds.push('%'+val+'%')
                            break
                        case "msg_id":
                            where += ` and MSG_ID like :${i++}`
                            binds.push('%'+val+'%')
                            break
                        case "msg_status":
                            where += ` and MSG_STATUS = :${i++}`
                            binds.push(val)
                            break
                        default:
                            where += ` and ${key} like :${i++}`
                            binds.push('%'+val+'%')
                            break
                    }
                }
            })
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = ` order by ${key} ${sort[key]}`
                })
            }
            else order = " order by dt desc, id"
            sql = `select TO_CHAR(ID) "id",TO_CHAR(DT,'dd/MM/yyyy HH24:mi:ss') "dt",FNC_ID "fnc_id",FNC_NAME "fnc_name",FNC_URL "fnc_url",ACTION "action",USER_ID "user_id",USER_NAME "user_name",SRC "src",DTL "dtl",MSG_ID "msg_id",CASE WHEN msg_status=0 THEN 'Lỗi' WHEN msg_status=1 THEN 'Thành công' ELSE '' END "msg_status",r1 "r1",r2 "r2",r3 "r3",r4 "r4",doc "doc" from s_sys_logs ${where} ${order}  offset ${start} rows fetch next ${count} rows only`
            result = await dbs.query(sql, binds)
            
            ret = { data: result.rows, pos: start }
            if (start == 0) {
                sql = `select count(*) "total" from s_sys_logs ${where}`
                result = await dbs.query(sql, binds)
                rows = result.rows
                ret.total_count = rows[0].total
            }
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    xls: async (req, res, next) => {
        try {
            const query = req.query, filter = JSON.parse(query.filter)
            const sort = query.sort
            let where = " where dt between :fd and :td", order, sql, result, rows, val, json
            let binds = [new Date(filter.fd), new Date(moment(filter.td).endOf("day"))]
            let i = 1
            Object.keys(filter).forEach((key) => {
                val = filter[key]
                logger4app.debug(val)
                if (val) {
                    switch (key) {
                        case "fd":
                            break
                        case "td":
                            break
                        case "srclog":
                            where += ` and SRC=:${i++}`
                            binds.push(val)
                            break
                        case "type":
                            where += ` and FNC_ID=:${i++}`
                            binds.push(val)
                            break
                        case "user":
                            where += ` and USER_ID like :${i++}`
                            binds.push('%'+val+'%')
                            break
                        case "msg_id":
                            where += ` and MSG_ID like :${i++}`
                            binds.push('%'+val+'%')
                            break
                        case "msg_status":
                            where += ` and MSG_STATUS = :${i++}`
                            binds.push(val)
                            break
                        default:
                            where += ` and ${key} like :${i++}`
                            binds.push('%'+val+'%')
                            break
                    }
                }
            })
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = ` order by ${key} ${sort[key]}`
                })
            }
            else order = " order by dt desc, id"
            sql = `select TO_CHAR(ID) "id",TO_CHAR(DT,'dd/MM/yyyy HH24:mi:ss') "dt",FNC_ID "fnc_id",FNC_NAME "fnc_name",FNC_URL "fnc_url",ACTION "action",USER_ID "user_id",USER_NAME "user_name",SRC "src",DTL "dtl",MSG_ID "msg_id",CASE WHEN msg_status=0 THEN 'Lỗi' WHEN msg_status=1 THEN 'Thành công' ELSE '' END "msg_status",r1 "r1",r2 "r2",r3 "r3",r4 "r4",doc "doc" from s_sys_logs ${where} ${order}  offset 0 rows fetch next ${config.MAX_ROW_EXCEL_EXPORT} rows only`
            result = await dbs.query(sql, binds)
            
            rows = result.rows
            const fn = "temp/SYSLOG.xlsx"
            json = { table: rows }
            const file = path.join(__dirname, "..", "..", "..", fn)
            const xlsx = fs.readFileSync(file)
            const template = new xlsxtemp(xlsx)
            template.substitute(1, json)
            res.end(template.generate(), "binary")
        }
        catch (err) {
            logger4app.debug("excel" + err)

            next(err)


        }
    }
}
module.exports = Service   