"use strict"
const excel = require("exceljs")
const numeral = require("numeral")
const path = require("path")
const fs = require("fs")
const Json2csvParser = require("json2csv").Parser;
var uuid = require('uuid');
const moment = require("moment")
const xlsxtemp = require("xlsx-template")
const dbs = require("./dbs")
const config = require("../config")
const logger4app = require("../logger4app")
const ENT = config.ent
const sec = require("../sec")
const util = require("../util")
const logging = require("../logging")
const ous = require("./ous")
const mfd = config.mfd
const grant = config.ou_grant
const KCT = "Không chịu thuế"
const KKK = "Không kê khai nộp thuế"
const CK = "Chiết khấu"
const max_row_report_tct = config.max_row_report_tct ? config.max_row_report_tct : 500000
const charList = (a, z, d = 1) => (a=a.charCodeAt(),z=z.charCodeAt(),[...Array(Math.floor((z-a)/d)+1)].map((_,i)=>String.fromCharCode(a+i*d)))
const tenhd = (type) => {
    let result = config.ITYPE.find(item => item.id === type)
    return result.value
}
const parseNumber = (value, curr) => {
    if (curr == "VND") {
        return parseInt(value)
    } else {
        return parseFloat(value)
    }
}
const checkrp = (data, row) => {
    if (util.isEmpty(data)) {
        delete row.cancel
    }
    else {
        let arr = data.sort((a, b) => a - b), len = arr.length, str
        if (len === 1) {
            str = arr[0].toString().padStart(7, "0")
        }
        else if (len === 2) {
            const cs = arr[0] + 1 === arr[1] ? "-" : ";"
            for (let i = 0; i < len; i++) {
                arr[i] = arr[i].toString().padStart(7, "0")
            }
            str = arr.join(cs)
        }
        else {
            let del = []
            for (let i = 1; i < len; i++) {
               
                if (Number(arr[i - 1]) + 2 === Number(arr[i + 1])) del.push(i)
            }
            for (let i = 0; i < len; i++) {
                if (del.includes(i)) arr[i] = null
                else arr[i] = arr[i].toString().padStart(7, "0")
            }
            str = arr.join(";").replace(/\;\;+/g, '-')
        }
        return str
    }
}
const rbi = (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            const sql = `select doc "doc" from s_inv where id=:id`
            const result = await dbs.query(sql, [id])
            const rows = result.rows
            if (rows.length == 0) reject(new Error(`Không tìm thấy Hóa đơn ${id} \n (Invoice not found ${id})`))
            let doc = JSON.parse(rows[0].doc)

            resolve(doc)
        } catch (err) {
            reject(err)
        }
    })
}
const sqlcountvatvib = `SELECT /*+ parallel(s_inv,auto) */
SUM ("total")     AS "total"
FROM (SELECT /*+ PARALLEL_INDEX(s_inv, S_INV_IDT_IDX, 10) */
        COUNT (*)     AS "total"
   FROM s_inv  a,
        JSON_TABLE (a.doc.items,
                    '$[*]'
                    COLUMNS (ROW_NUMBER FOR ORDINALITY,
                             vrn VARCHAR2 (50) PATH '$.vrn',
                             vrt NUMBER PATH '$.vrt',
                             code VARCHAR2 (50) PATH '$.code',
                             name VARCHAR2 (255) PATH '$.name',
                             amount NUMBER PATH '$.amount',
                             vat NUMBER PATH '$.vat',
                             itemtype VARCHAR2 (100) PATH '$.type'))
        AS it
  WHERE     a.idt BETWEEN :1
                      AND :2
        AND a.stax = :3
        AND a.TYPE = '01GTKT'
        AND a.status IN (3, 4)
        AND EXISTS
                (    SELECT 1
                       FROM s_ou o
                      WHERE o.id = a.ou
                 START WITH o.id = :4
                 CONNECT BY PRIOR o.id = o.pid)
 UNION ALL
 SELECT /*+ PARALLEL_INDEX(s_inv, S_INV_CDT_IDX, 10) */
               COUNT (*)     AS "total"
          FROM s_inv  a,
               JSON_TABLE (a.doc.items,
                           '$[*]'
                           COLUMNS (ROW_NUMBER FOR ORDINALITY,
                                    vrn VARCHAR2 (50) PATH '$.vrn',
                                    vrt NUMBER PATH '$.vrt',
                                    code VARCHAR2 (50) PATH '$.code',
                                    name VARCHAR2 (255) PATH '$.name',
                                    amount NUMBER PATH '$.amount',
                                    vat NUMBER PATH '$.vat',
                                    itemtype VARCHAR2 (100) PATH '$.type'))
               AS it
  WHERE     a.cdt BETWEEN :1
                      AND :2
        AND a.stax = :3
        AND a.TYPE = '01GTKT'
        AND a.status = 4
        AND a.xml IS NOT NULL
        AND EXISTS
                (    SELECT 1
                       FROM s_ou o
                      WHERE o.id = a.ou
                 START WITH o.id = :4
                 CONNECT BY PRIOR o.id = o.pid))`
const sqlsumvatvib = `DECLARE
-- Declarations
l_P_FD     DATE;
l_P_TD     DATE;
l_P_STAX   VARCHAR2 (32767);
l_P_OU     NUMBER;
BEGIN
-- Initialization
l_P_FD := :1;
l_P_TD := :2;
l_P_STAX := :3;
l_P_OU := :4;

-- Call
pkg_vat_report.prc_vat_rep (P_FD     => l_P_FD,
                            P_TD     => l_P_TD,
                            P_STAX   => l_P_STAX,
                            P_OU     => l_P_OU);
-- Transaction Control
COMMIT;
END;`
const sqlfetchvatvib = `SELECT /*+ PARALLEL(auto) */
ROW_NUMBER ()
    OVER (PARTITION BY it.vrt
          ORDER BY
              r."idt",
              r."form",
              r."serial",
              r."seq")
    "STT",
--r."id",
REPLACE (pkg_vat_report.fnc_getouname (r."ou"), ',', '-')
    "Branch",
r."form"
    "Ký hiệu mẫu hóa đơn",
r."serial"
    "Ký hiệu hóa đơn",
r."seq"
    "Số hóa đơn",
r."idt"
    "Ngày-tháng-năm lập hóa đơn",
r."btax"
    "Mã số thuế người mua",
REPLACE (r."bname", ',', '-')
    "Tên đơn vị mua",
it.code
    "Mã hàng",
REPLACE (it.name, ',', '-')
    "Mặt hàng",
  it.amount
* pkg_vat_report.fnc_get_factor_adj (r."dif", r."adj", r."adjtyp")
* r."factor"
    "Doanh thu chưa có thuế GTGT",
it.vrn
    "Thuế suất",
  it.vat
* pkg_vat_report.fnc_get_factor_adj (r."dif", r."adj", r."adjtyp")
* r."factor"
    "Thuế GTGT",
--r."buyer",
--r."status",
--r."cstatus",
--r."vibstatus",
--r."adjdes",
--r."cde",
--r."adjtyp",
--r."dif",
--r."root",
--r."adj",
--r."taxs",
REPLACE (
    CASE
        WHEN r."adjdes" IS NOT NULL
        THEN
            r."adjdes"
        ELSE
            CASE
                WHEN r."cde" IS NOT NULL THEN r."cde"
                ELSE r."note"
            END
    END,
    ',',
    '-')
    "Ghi chú",
--r."factor",
r."refno"
    "Số bút toán hạch toán doanh thu",
r."glacc"
    "GL doanh thu" /*,
r."ou",
DECODE (it.vrt,  -2, 1,  -1, 2,  0, 3,  5, 4,  5)
|| LPAD (ROW_NUMBER ()
        OVER (PARTITION BY it.vrt
              ORDER BY
                  r."idt",
                  r."form",
                  r."serial",
                  r."seq"),
    9,
    '0')                            "orderno"
--it.vrt                                   "vrt"
*/
FROM s_rep_vat_tmp  r,
JSON_TABLE ("taxs",
            '$[*]'
            COLUMNS (ROW_NUMBER FOR ORDINALITY,
                     vrn VARCHAR2 (50) PATH '$.vrn',
                     vrt NUMBER PATH '$.vrt',
                     code VARCHAR2 (50) PATH '$.code',
                     name VARCHAR2 (255) PATH '$.name',
                     amount NUMBER PATH '$.amount',
                     vat NUMBER PATH '$.vat',
                     itemtype VARCHAR2 (100) PATH '$.type')) AS it
WHERE NVL (it.itemtype, '@@hunglq@@') <> 'Chiết khấu'`
const sqlcountbcbhvib = `SELECT /*+ INDEX(S_INV_IDT_IDX) parallel(s_inv,auto) */
COUNT (*) as "total" 
FROM s_inv  a,
JSON_TABLE (a.doc.items,
            '$[*]'
            COLUMNS (ROW_NUMBER FOR ORDINALITY,
                     vrn VARCHAR2 (50) PATH '$.vrn',
                     vrt NUMBER PATH '$.vrt',
                     code VARCHAR2 (50) PATH '$.code',
                     name VARCHAR2 (255) PATH '$.name',
                     amount NUMBER PATH '$.amount',
                     vat NUMBER PATH '$.vat',
                     itemtype VARCHAR2 (100) PATH '$.type')) AS it
WHERE     idt BETWEEN :1 AND :2
AND stax = :3
AND ("TYPE" = :4 OR :4 = '*')
AND EXISTS
        (    SELECT 1
               FROM s_ou o
              WHERE o.id = ou
         START WITH o.id = :5
         CONNECT BY PRIOR o.id = o.pid)`
const sqlsumbcbhvib = `DECLARE
-- Declarations
l_P_FD     DATE;
l_P_TD     DATE;
l_P_STAX   VARCHAR2 (32767);
l_P_OU     NUMBER;
l_P_TYPE   VARCHAR2 (32767);
BEGIN
-- Initialization
l_P_FD := :1;
l_P_TD := :2;
l_P_STAX := :3;
l_P_OU := :4;
l_P_TYPE := :5;

-- Call
PKG_VAT_REPORT.PRC_BCBH_REP (P_FD     => l_P_FD,
                             P_TD     => l_P_TD,
                             P_STAX   => l_P_STAX,
                             P_OU     => l_P_OU,
                             P_TYPE   => l_P_TYPE);

-- Transaction Control
COMMIT;
END;`
const sqlfetchbcbhvib = `    SELECT /*+ parallel(s_rep_vat_tmp,auto) */
ROW_NUMBER ()
    OVER (ORDER BY
              r."idt",
              r."form",
              r."serial",
              r."seq")
    "STT",
r."btax"
    "MST người mua",
REPLACE (r."bname", ',', '-')
    "Tên đơn vị",
REPLACE (r."buyer", ',', '-')
    "Tên người mua",
CASE
    WHEN r."vibstatus" = 4 THEN 'Hủy HĐ đã ký số'
    WHEN r."vibstatus" = 7 THEN 'Hủy HĐ đã ký số'
    ELSE r."status"
END
    "Trạng thái Hóa đơn",
r."form"
    "Mẫu số",
r."serial"
    "Ký hiệu",
r."seq"
    "Số hóa đơn",
r."idt"
    "Ngày lập HĐ",
REPLACE (it.name, ',', '-')
    "Mặt hàng",
--r."cstatus",
--r."vibstatus",
CASE WHEN r."vibstatus" IN (4, 7) THEN NULL ELSE it.amount END
    "Doanh số bán chưa thuế",
CASE WHEN r."vibstatus" IN (4, 7) THEN NULL ELSE it.vat END
    "Thuế GTGT",
REPLACE (r."note", ',', '-')
    "Ghi chú"
FROM s_rep_vat_tmp r,
JSON_TABLE ("taxs",
            '$[*]'
            COLUMNS (ROW_NUMBER FOR ORDINALITY,
                     vrn VARCHAR2 (50) PATH '$.vrn',
                     vrt NUMBER PATH '$.vrt',
                     code VARCHAR2 (50) PATH '$.code',
                     name VARCHAR2 (255) PATH '$.name',
                     amount NUMBER PATH '$.amount',
                     vat NUMBER PATH '$.vat',
                     itemtype VARCHAR2 (100) PATH '$.type')) AS it
WHERE NVL (it.itemtype, '@@hunglq@@') <> 'Chiết khấu'
ORDER BY r."idt",
r."form",
r."serial",
r."seq"`

const Service = {
    get: async (req, res, next) => {
        try {
            const token = sec.decode(req), query = req.body, taxc = token.taxc, reportext = query.reportext
            const now = new Date(), ndate = now.getDate(), nmonth = now.getMonth() + 1, nyear = now.getFullYear()
            let fn, json
            const firstlist = (query.firstlist && query.firstlist == "1") ? "X" : "", updatelist = query.updatelist
            let ress = await dbs.query(`select fadd "fadd" from s_ou where id=:id and rownum=1`, [token.ou])
            let sfaddr, ress2, taxc2
            if (query.ou == '*') {
                ress2 = await dbs.query(`select stax from s_inv where rownum=1`)
            } else {
                ress2 = await dbs.query(`select stax from s_inv where ou=:1 and rownum=1`, [query.ou])
            }
            
            //const sSysLogs = { fnc_id: 'user_update', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật thông tin Tài khoản ${body.id}`};
            //const abc = logging.infoMSG()
            if (ress2.rows.length > 0) taxc2 = ress2.rows[0].STAX
            if (ress.rows.length > 0) sfaddr = ress.rows[0].fadd
            if (!reportext) {
                const tn = query.tn,form =query.form,serial = query.serial, type = query.type, report = query.report, ou = ((typeof query.ou == "undefined" || query.ou == '') ? "*" : query.ou)
                const fd = new Date(query.fd),td = new Date(moment(query.td).endOf("day"))
                const fr = moment(query.fd).format(mfd), to = moment(query.td).format(mfd), rt = moment().format(mfd)
                let sql3,sql2, sql, result, where, binds, rows, i, where_ft = "", extcol = ""
                if (report == 1) {
                    // fn = "temp/08BKHDVAT.xlsx"
                    if (config.ent == "vib") fn = "temp/08BKHDVAT_VIB.xlsx"
                    let workbook = new excel.Workbook()
                    let worksheet = workbook.addWorksheet("sheet 1")

                    numeral.locale("vi")
                    const borderStyles = {
                        top: { style: "thin" },
                        left: { style: "thin" },
                        bottom: { style: "thin" },
                        right: { style: "thin" }
                    };

                    worksheet.columns = [
                        { key: "index", width: 6, style: { alignment: { vertical: 'middle', horizontal: 'center' } } },
                        { key: "form", width: 15 },
                        { key: "serial", width: 20 },
                        { key: "seq", width: 20 },
                        { key: "idt", width: 25 },
                        { key: "bname", width: 40 },
                        { key: "btax", width: 25 },
                        // { key: "sumv", width: 25 },
                        // { key: "vatv", width: 25 },
                        { key: "curr", width: 25 },
                        { key: "exrt", width: 25 },
                        { key: "sum", width: 25 },
                        { key: "vat", width: 25 },
                        { key: "note", width: 60 },
                    ];

                    worksheet.mergeCells('A1:J1');
                    worksheet.getCell('A1').value = 'CỘNG HOÀ XÃ HỘI CHỦ NGHĨA VIỆT NAM'
                    worksheet.getCell('A1').font = { name: 'Times New Roman', bold: true }
                    worksheet.getCell('A1').alignment = { horizontal: 'center' }

                    worksheet.mergeCells('A2:J2');
                    worksheet.getCell('A2').value = 'ĐỘC LẬP TỰ DO HẠNH PHÚC'
                    worksheet.getCell('A2').font = { name: 'Times New Roman', bold: true }
                    worksheet.getCell('A2').alignment = { horizontal: 'center' }

                    worksheet.mergeCells('A6:J6');
                    worksheet.getCell('A6').value = 'BẢNG KÊ HOÁ ĐƠN ĐÃ SỬ DỤNG THEO NGƯỜI BÁN'
                    worksheet.getCell('A6').font = { name: 'Times New Roman', bold: true }
                    worksheet.getCell('A6').alignment = { horizontal: 'center' }

                    worksheet.mergeCells('A7:J7')
                    worksheet.getCell('A7').value = `Từ ngày ${moment(fd).format('DD/MM/YYYY')} đến ngày ${moment(td).format('DD/MM/YYYY')}`
                    worksheet.getCell('A7').font = { name: 'Times New Roman', italic: true }
                    worksheet.getCell('A7').alignment = { horizontal: 'center' }

                    worksheet.mergeCells('I3:J3');
                    worksheet.getCell('I3').value = ' Mẫu số: 08/BK-HĐXT'
                    worksheet.getCell('I3').border = borderStyles
                    worksheet.getCell('I3').font = { name: 'Times New Roman', bold: true }
                    worksheet.getCell('I3').alignment = { horizontal: 'center' }

                    worksheet.mergeCells('I4:J4');
                    worksheet.getCell('I4').value = 'Ban hành kèm theo Quyết định số 1209/QĐ-BTC ngày 23/06/2015 của Bộ Tài chính Mẫu biểu báo cáo theo nghị định 119/2018'
                    worksheet.getCell('I4').border = borderStyles
                    worksheet.getCell('I4').font = { name: 'Times New Roman'}
                    worksheet.getCell('I4').alignment = { horizontal: 'center' }

                    let sname= token.on 
                    worksheet.mergeCells('A8:F8')
                    worksheet.getCell('A8').value = `Tên người bán: ${sname}`
                    worksheet.getCell('A8').font = { name: 'Times New Roman' }
                    worksheet.getCell('A8').alignment = { horizontal: 'left' }

                    worksheet.mergeCells('G8:J8')
                    worksheet.getCell('G8').value = `Đơn vị báo cáo: ${sname}`
                    worksheet.getCell('G8').font = { name: 'Times New Roman' }
                    worksheet.getCell('G8').alignment = { horizontal: 'left' }

                    let stax= taxc
                    worksheet.mergeCells('A9:F9')
                    worksheet.getCell('A9').value = `MST người bán: ${stax}`
                    worksheet.getCell('A9').font = { name: 'Times New Roman' }
                    worksheet.getCell('A9').alignment = { horizontal: 'left' }

                    worksheet.mergeCells('A10:A11')
                    worksheet.getCell('A10').value = 'STT'
                    worksheet.getCell('A10').border = borderStyles
                    worksheet.getCell('A10').font = { name: 'Times New Roman', bold: true }
                    worksheet.getCell('A10').alignment = { horizontal: 'center' }

                    worksheet.mergeCells('B10:E10')
                    worksheet.getCell('B10').value = 'Hóa đơn, chứng từ bán ra'
                    worksheet.getCell('B10').border = borderStyles
                    worksheet.getCell('B10').font = { name: 'Times New Roman', bold: true }
                    worksheet.getCell('B10').alignment = { horizontal: 'center' }

                    worksheet.mergeCells('F10:F11')
                    worksheet.getCell('F10').value = 'Tên đơn vị mua'
                    worksheet.getCell('F10').border = borderStyles
                    worksheet.getCell('F10').font = { name: 'Times New Roman', bold: true }
                    worksheet.getCell('F10').alignment = { horizontal: 'center' }

                    worksheet.mergeCells('G10:G11')
                    worksheet.getCell('G10').value = 'Mã số thuế người mua'
                    worksheet.getCell('G10').border = borderStyles
                    worksheet.getCell('G10').font = { name: 'Times New Roman', bold: true }
                    worksheet.getCell('G10').alignment = { horizontal: 'center' }

                    worksheet.mergeCells('H10:H11')
                    worksheet.getCell('H10').value = 'Loại Tiền'
                    worksheet.getCell('H10').border = borderStyles
                    worksheet.getCell('H10').font = { name: 'Times New Roman', bold: true }
                    worksheet.getCell('H10').alignment = { horizontal: 'center' }

                    worksheet.mergeCells('I10:I11')
                    worksheet.getCell('I10').value = 'Tỷ Giá'
                    worksheet.getCell('I10').border = borderStyles
                    worksheet.getCell('I10').font = { name: 'Times New Roman', bold: true }
                    worksheet.getCell('I10').alignment = { horizontal: 'center' }

                    worksheet.mergeCells('J10:J11')
                    worksheet.getCell('J10').value = 'Doanh thu chưa có thuế GTGT'
                    worksheet.getCell('J10').border = borderStyles
                    worksheet.getCell('J10').font = { name: 'Times New Roman', bold: true }
                    worksheet.getCell('J10').alignment = { horizontal: 'center' }

                    worksheet.mergeCells('K10:K11')
                    worksheet.getCell('K10').value = 'Thuế GTGT'
                    worksheet.getCell('K10').border = borderStyles
                    worksheet.getCell('K10').font = { name: 'Times New Roman', bold: true }
                    worksheet.getCell('K10').alignment = { horizontal: 'center' }

                    worksheet.mergeCells('L10:L11')
                    worksheet.getCell('L10').value = 'Ghi chú'
                    worksheet.getCell('L10').border = borderStyles
                    worksheet.getCell('L10').font = { name: 'Times New Roman', bold: true }
                    worksheet.getCell('L10').alignment = { horizontal: 'center' }

                    worksheet.mergeCells('B11:D11')
                    worksheet.getCell('B11').value = 'Ký hiệu mẫu hóa đơn, ký hiệu hóa đơn, số hóa đơn'
                    worksheet.getCell('B11').border = borderStyles
                    worksheet.getCell('B11').font = { name: 'Times New Roman', bold: true }
                    worksheet.getCell('B11').alignment = { horizontal: 'center' }

                    worksheet.getCell('E11').value = 'Ngày, tháng, năm lập hóa đơn'
                    worksheet.getCell('E11').border = borderStyles
                    worksheet.getCell('E11').font = { name: 'Times New Roman', bold: true }
                    worksheet.getCell('E11').alignment = { horizontal: 'center' }

                    worksheet.getCell('A12').value = '(1)'
                    worksheet.getCell('A12').border = borderStyles
                    worksheet.getCell('A12').font = { name: 'Times New Roman' }
                    worksheet.getCell('A12').alignment = { horizontal: 'center' }

                    worksheet.getCell('B12').value = '(2)'
                    worksheet.getCell('B12').border = borderStyles
                    worksheet.getCell('B12').font = { name: 'Times New Roman' }
                    worksheet.getCell('B12').alignment = { horizontal: 'center' }

                    worksheet.getCell('C12').value = '(3)'
                    worksheet.getCell('C12').border = borderStyles
                    worksheet.getCell('C12').font = { name: 'Times New Roman' }
                    worksheet.getCell('C12').alignment = { horizontal: 'center' }

                    worksheet.getCell('D12').value = '(4)'
                    worksheet.getCell('D12').border = borderStyles
                    worksheet.getCell('D12').font = { name: 'Times New Roman' }
                    worksheet.getCell('D12').alignment = { horizontal: 'center' }

                    worksheet.getCell('E12').value = '(5)'
                    worksheet.getCell('E12').border = borderStyles
                    worksheet.getCell('E12').font = { name: 'Times New Roman' }
                    worksheet.getCell('E12').alignment = { horizontal: 'center' }

                    worksheet.getCell('F12').value = '(6)'
                    worksheet.getCell('F12').border = borderStyles
                    worksheet.getCell('F12').font = { name: 'Times New Roman' }
                    worksheet.getCell('F12').alignment = { horizontal: 'center' }

                    worksheet.getCell('G12').value = '(7)'
                    worksheet.getCell('G12').border = borderStyles
                    worksheet.getCell('G12').font = { name: 'Times New Roman' }
                    worksheet.getCell('G12').alignment = { horizontal: 'center' }

                    worksheet.getCell('H12').value = '(8)'
                    worksheet.getCell('H12').border = borderStyles
                    worksheet.getCell('H12').font = { name: 'Times New Roman' }
                    worksheet.getCell('H12').alignment = { horizontal: 'center' }

                    worksheet.getCell('I12').value = '(9)'
                    worksheet.getCell('I12').border = borderStyles
                    worksheet.getCell('I12').font = { name: 'Times New Roman' }
                    worksheet.getCell('I12').alignment = { horizontal: 'center' }

                    worksheet.getCell('J12').value = '(10)'
                    worksheet.getCell('J12').border = borderStyles
                    worksheet.getCell('J12').font = { name: 'Times New Roman' }
                    worksheet.getCell('J12').alignment = { horizontal: 'center' }

                    worksheet.getCell('K12').value = '(11)'
                    worksheet.getCell('K12').border = borderStyles
                    worksheet.getCell('K12').font = { name: 'Times New Roman' }
                    worksheet.getCell('K12').alignment = { horizontal: 'center' }
    
                    worksheet.getCell('L12').value = '(12)'
                    worksheet.getCell('L12').border = borderStyles
                    worksheet.getCell('L12').font = { name: 'Times New Roman' }
                    worksheet.getCell('L12').alignment = { horizontal: 'center' }
    
                    let row_stt = 15
                    let dataRange = charList('A', 'L')
                    //Lấy các hóa đơn đã duyệt, thêm trường factor đánh dấu hệ số dương
                    where = `where a.idt between :1 and :2 and a.stax=:3 and a.type='01GTKT' and a.status=3 `// and a.cid is null `
                    binds = [fd, td, taxc]

                    i = 4
                    /* Hỏi lại Duy xem sao lại có đoạn này
                    if (grant && token.ou == token.u) {
                        where += ` and ou in (select cid from s_ou_tree where pid=:${i++})` 
                       
                        binds.push(token.u)
                    }               
                    */
                    if (!ou.includes("*")) {
                        where += ` and a.ou=:${i++}`
                        binds.push(ou)
                    }
                    if (config.ou_grant && token.ou == token.u) {
                        // where += `and ou in (select id from s_ou start with id=:ou connect by prior id=pid) `
                        where += ` AND exists (SELECT 1 FROM s_ou o where o.id = a.ou START WITH o.id = :ou CONNECT BY PRIOR o.id = o.pid) `
                        binds.push(token.u)
                    }
                    let taxsql = /*`case when a.adjtyp=2 then a.doc.tax else a.doc.tax end`*/`a.doc.tax`
                    //Nếu là VIB thì không lấy tax nữa mà lấy items nên tạm thời gán tax = null
                    if (config.ent == "vib") {
                        extcol = `,c2 "refno", c1 "glacc", ou "ou"`
                        taxsql = `NULL`
                    }

                    sql = `select id "id", TO_CHAR(a.idt,'${mfd}') "idt",a.form "form",a.serial "serial",a.seq "seq",a.btax "btax",a.bname "bname",a.curr "curr",a.exrt "exrt",a.adjdes "adjdes",a.cde "cde",a.adjtyp "adjtyp",a.doc.dif "dif",a.doc.root "root",a.doc.adj "adj",${taxsql} "taxs", N'' || note "note", 1 as "factor"${extcol} from s_inv a ${where}`
                    if (config.BKHDVAT_EXRA_OPTION == '2') {
                        let where1 = `where a.cdt between :5 and :6 and a.stax=:7 and a.type='01GTKT' and a.status=4 and a.xml is not null`
                        
                        binds.push(fd)
                        binds.push(td)
                        binds.push(taxc)
                        if (!ou.includes("*")) {
                            i = 8
                            where1 += ` and a.ou=:${i++}`
                            binds.push(ou)
                        }
                        if (config.ou_grant && token.ou == token.u) {
                            // sql += `and ou in (select id from s_ou start with id=:ou connect by prior id=pid) `
                            where1 += ` AND exists (SELECT 1 FROM s_ou o where o.id = a.ou START WITH o.id = :ou CONNECT BY PRIOR o.id = o.pid) `
                            binds.push(token.u)
                        }
                        //Lấy hóa đơn hủy thay thế => status = 4, ngày lấy theo dt, factor đánh dấu hệ số âm
                        sql += ` UNION ALL select id "id", TO_CHAR(a.cdt,'${mfd}') "idt",a.form "form",a.serial "serial",a.seq "seq",a.btax "btax",a.bname "bname",a.curr "curr",a.exrt "exrt",a.adjdes "adjdes",a.cde "cde",a.adjtyp "adjtyp",a.doc.dif "dif",a.doc.root "root",a.doc.adj "adj",${taxsql} "taxs", CASE WHEN JSON_VALUE (doc, '$.cancel.rea') IS NOT NULL THEN N'' || JSON_VALUE (doc, '$.cancel.rea') ELSE cde END "note", -1 as "factor"${extcol} from s_inv a ${where1} `

                        let where2 = `where a.idt between :9 and :10 and a.stax=:11 and a.type='01GTKT' and a.status=4 and a.xml is not null`

                        binds.push(fd)
                        binds.push(td)
                        binds.push(taxc)
                        if (!ou.includes("*")) {
                            i = 8
                            where2 += ` and a.ou=:${i++}`
                            binds.push(ou)
                        }
                        if (config.ou_grant && token.ou == token.u) {
                            // sql += `and ou in (select id from s_ou start with id=:ou connect by prior id=pid) `
                            where2 += ` AND exists (SELECT 1 FROM s_ou o where o.id = a.ou START WITH o.id = :ou CONNECT BY PRIOR o.id = o.pid) `
                            binds.push(token.u)
                        }

                        //Hóa đơn bị hủy, thay thế => status = 4, ngày lấy theo idt, factor đánh dấu hệ số dương
                        sql += ` UNION ALL select id "id", TO_CHAR(a.idt,'${mfd}') "idt",a.form "form",a.serial "serial",a.seq "seq",a.btax "btax",a.bname "bname",a.curr "curr",a.exrt "exrt",a.adjdes "adjdes",a.cde "cde",a.adjtyp "adjtyp",a.doc.dif "dif",a.doc.root "root",a.doc.adj "adj",${taxsql} "taxs",N'' || note "note", 1 as "factor"${extcol} from s_inv a ${where2} `
                    }
                    sql = 'Select * from (' + sql + ') order by "idt","form","serial","seq"'
                    let vuuid = uuid.v4()
                    logger4app.debug(`Chay thu tuc tong hop du lieu bao cao VAT VIB ${token.uid} - ${token.u} - ${token.taxc} - ${ndate} - ${vuuid}`)
                    console.time(`Chay thu tuc tong hop du lieu bao cao VAT VIB ${token.uid} - ${token.u} - ${token.taxc} - ${ndate} - ${vuuid}`)
                    
                    if (!['vib'].includes(config.ent)) result = await dbs.query(sql, binds)
                    else {
                        binds = [fd, td, taxc, token.u]
                        result = await dbs.query(sqlcountvatvib, binds)
                        rows = result.rows
                        if (rows && rows.length > 0 && rows[0].total > max_row_report_tct) {
                            res.json({ err: `Số lượng bản ghi vượt quá số lượng cho phép (${max_row_report_tct} dòng) \n (The number of records exceeds the allowed number (${max_row_report_tct} lines))` })
                        }
                        binds = [fd, td, taxc, token.u]
                        result = await dbs.executereportvat(sqlsumvatvib, sqlfetchvatvib, binds)
                    }
                    console.timeEnd(`Chay thu tuc tong hop du lieu bao cao VAT VIB ${token.uid} - ${token.u} - ${token.taxc} - ${ndate} - ${vuuid}`)
                    rows = result.rows
                    //Begin HUNGLQ them doan lay du lieu dong theo danh muc loai thue
                    //Lay ra danh muc loai thue suat truoc
                    let arr = JSON.parse(JSON.stringify(config.CATTAX))
                    //Sap xep theo ty le thue vrt truoc, phai nhan voi 100 khong thi vrt 7 lai lon hon 10
                    for (let itemArr of arr) {
                        itemArr.sortprop = String(itemArr.vrt * 100).padStart(7, "0")
                    }
                    arr = util.sortobj(arr, 'sortprop', 1)
                    let objRep = {}
                    //Lap danh muc loai thue suat de tao key object truoc
                    for (let vVat of arr) {
                        //Dat key la ky tu dau va vrt nhan voi 100 de co so nguyen (de phong truong hop vrt = 5.26, 3.5)
                        let vatKey = `VAT${vVat.vrt * 100}`
                        //Neu dinh thay bang key la orriginal vrt thi bo comment doan duoi nay
                        //let vatKey = `VAT${vVat.original_vrt * 100}`
                        objRep[vatKey] = {
                            totalSumByVrt: 0, //Tong cua du lieu sum theo tung vrt, gan truoc bang 0
                            totalVatByVrt: 0, //Tong cua du lieu vat theo tung vrt, gan truoc bang 0
                            index: 0, //gan so thu tu, gan truoc bang 0
                            key_vrn: vVat.vrnVATRep, // danh dáu key len dong dien dai
                            dataVat: [] //Du lieu bao cao, gan bang mang rong truoc, ti nua push du lieu sau
                        }
                    }
                    //End HUNGLQ them doan lay du lieu dong theo danh muc loai thue
                    if (config.ent == "vib") {
                        //const jsonData = JSON.parse(JSON.stringify(rows));
                        if (rows.length <= 0) {
                            res.json({ data: '' })
                            return
                        }
                        logger4app.debug(`Chay ket xuat CSV bao cao VAT VIB ${token.uid} - ${token.u} - ${token.taxc} - ${ndate} - ${vuuid}`)
                        console.time(`Chay ket xuat CSV bao cao VAT VIB ${token.uid} - ${token.u} - ${token.taxc} - ${ndate} - ${vuuid}`)
                        const json2csvParser = new Json2csvParser({ header: true, default: '', quote: '', delimiter: ',', encoding: 'unicode', withBOM: true });
                        const csv = json2csvParser.parse(rows);
                        console.timeEnd(`Chay ket xuat CSV bao cao VAT VIB ${token.uid} - ${token.u} - ${token.taxc} - ${ndate} - ${vuuid}`)
                        res.json({ data: csv })
                        return
                    }
                    console.time(`Chay xu ly cac hoa don bao cao VAT ${token.uid} - ${token.u} - ${token.taxc} - ${ndate}`)
                    for (const row of rows) {
                        console.time(`Chay xu ly hoa don bao cao VAT ${token.uid} - ${token.u} - ${token.taxc} - ${ndate} - ${row.id}`)
                        let idt, form, serial, seq, btax, bname, taxs = (config.ent != "vib") ? JSON.parse(row.taxs) : [], ouname

                        if (config.ent == 'vib') {
                            //let doc = await rbi(row.id)
                            //taxs = doc.items
                            taxs = (row.taxs && row.taxs != '') ? JSON.parse(row.taxs) : []

                            let ouobj = await ous.obid(row.ou)
                            ouname = ouobj.name

                        }
                        if (row.adjtyp == 2) {
                            let hd 
                            if (row.adj) {
                                hd = JSON.parse(row.adj)
                            }
                            row.note += `Điều chỉnh tăng/ giảm thông tin cho HĐ ${hd.seq}`
                        }
                        if (row.adjtyp == 1) {
                            let hdtt
                            if (row.adj) {
                                hdtt = JSON.parse(row.adj)
                            }
                            row.note += `Thay thế cho hóa đơn ${hdtt.seq}`
                        }
                        if(row.cde && row.cde.includes('điều chỉnh')){
                            let result_seq = await dbs.query('select seq from s_inv where pid=:1 and form=:2 and serial=:3', [row.id,row.form,row.serial])
                            let row_seq = result_seq.rows
                            let seq_new
                            if(row_seq.length>0) seq_new = row_seq[0].seq
                            row.note += `${row.note ? '\n':''}Bị điều chỉnh cho HĐ ${row.form}-${row.serial}-${seq_new}`
                        }
                        if(row.cde && row.cde.includes('thay thế')){
                            let result_seq = await dbs.query('select seq from s_inv where pid=:1 and form=:2 and serial=:3', [row.id,row.form,row.serial])
                            let row_seq = result_seq.rows
                            let seq_new
                            if(row_seq.length>0) seq_new = row_seq[0].seq
                            row.note += `${row.note ? '\n':''}Bị thay thế cho HĐ ${row.form}-${row.serial}-${seq_new}`
                        }
                            // note cu
                            // if (row.adjdes != null) {
                                // let root = (row.root != null) ? row.root : {}
                                // idt = moment(root.idt).format(config.mfd)
                                // let rootadj = row.adj
                                // if (rootadj.seq) {
                                //     let arrseq = String(rootadj.seq).split("-")
                                //     if (arrseq.length >= 3) {
                                //         form = arrseq[0]
                                //         serial = arrseq[1]
                                //         seq = arrseq[2]
                                //     }
                                // }
                                // row.note = row.adjdes
                                //row.note = `Bị điều chỉnh bởi HĐ ${row.form}-${row.serial}-${row.seq}`
                                // btax = root.btax
                                // bname = root.bname
                            // }
                            // else {
                                // if (row.cde != null) {
                                    // row.note = row.cde
                                // }
                                // idt = row.idt
                                // form = row.form
                                // serial = row.serial
                                // seq = row.seq
                                // btax = row.btax
                                // bname = row.bname
                            // }
                        //Dữ liệu của hóa đơn nào lấy của hóa đơn đấy
                        idt = row.idt
                        form = row.form
                        serial = row.serial
                        seq = row.seq
                        btax = row.btax
                        bname = row.bname
                        let vbname = {bname : bname}
                        
                        for (const c of config.SPECIAL_CHAR) {
                            vbname = JSON.parse(JSON.stringify(vbname).replace(c, ""))
                            bname = vbname.bname
                        }
                        if (row.adjtyp == 2) {
                            //Nếu chênh lệch dương thì gán số ấm vì là điều chỉnh giảm
                            if (row.dif) {
                                let dif = JSON.parse(row.dif)
                                if (dif.total > 0) row.factor = -1 * row.factor
                            }
                            else {
                                if (row.adj) {
                                    let adj = JSON.parse(row.adj)
                                    if (adj.adjType && adj.adjType == '3') row.factor = -1 * row.factor
                                }
                            }
                        }
                        console.time(`Chay xu ly item cac hoa don bao cao VAT ${token.uid} - ${token.u} - ${token.taxc} - ${ndate} - ${row.id}`)
                        for (const tax of taxs) {
                            console.time(`Chay xu ly item hoa don bao cao VAT ${token.uid} - ${token.u} - ${token.taxc} - ${ndate} - ${row.id}`)
                            if (config.ent == 'vib') {
                                if (tax.type == 'Chiết khấu') continue
                                if (tax.vrt == -2) {
                                    tax.vrn = 'Không kê khai nộp thuế'
                                }
                                if (tax.vrt == -1) {
                                    tax.vrn = 'Không chịu thuế'
                                }
                            }

                            if (config.ent == 'vib') {
                                tax.amtv = tax.amount
                                tax.vatv = tax.vat
                            }
                            let vrt = Number(tax.vrt), s = util.isNumber(tax.amtv) ? tax.amtv * row.factor : 0, v = util.isNumber(tax.vatv) ? (tax.vatv * row.factor) : 0
                            if(row.curr=="VND") {
                                s = util.isNumber(tax.amtv) ? Math.abs(tax.amtv) * row.factor : 0
                                v = util.isNumber(tax.vatv) ? (Math.abs(tax.vatv) * row.factor) : 0
                            } else {
                                s = util.isNumber(tax.amt) ? Math.abs(tax.amt) * row.factor : 0
                                v = util.isNumber(tax.vat) ? (Math.abs(tax.vat) * row.factor) : 0
                            } 
                            let r = { idt: idt, form: form, serial: serial, seq: seq, btax: btax, bname, sum: s, vat: v, note: row.note, curr: row.curr, exrt: row.exrt }
                            if (config.ent == 'vib') {
                                r.ouname = ouname
                                r.refno = row.refno
                                r.glacc = row.glacc
                                r.itemname = tax.name
                                r.itemcode = tax.code
                                r.vrn = tax.vrn
                            }
                            //Begin HUNGLQ them doan lay du lieu dong theo danh muc loai thue
                            //Bat dau day du lieu vao objRep, neu doan nay on bo cai switch o duoi di
                            const vatKey = `VAT${vrt * 100}`
                            objRep[vatKey].dataVat.push(r)
                            objRep[vatKey].totalSumByVrt += s
                            objRep[vatKey].totalVatByVrt += v
                            //End HUNGLQ them doan lay du lieu dong theo danh muc loai thue
                            console.timeEnd(`Chay xu ly item hoa don bao cao VAT ${token.uid} - ${token.u} - ${token.taxc} - ${ndate} - ${row.id}`)
                        }
                        console.timeEnd(`Chay xu ly item cac hoa don bao cao VAT ${token.uid} - ${token.u} - ${token.taxc} - ${ndate} - ${row.id}`)
                        console.timeEnd(`Chay xu ly hoa don bao cao VAT ${token.uid} - ${token.u} - ${token.taxc} - ${ndate} - ${row.id}`)
                    }
                    console.timeEnd(`Chay xu ly cac hoa don bao cao VAT ${token.uid} - ${token.u} - ${token.taxc} - ${ndate}`)
                    // json = { stax: taxc, sname: token.on, fd: fr, td: to, rt: rt, a0: a0, s0: s0,v0:v0, a5: a5, s5: s5, v5: v5, a10: a10, s10: s10, v10: v10, a1: a1, s1: s1,v1:v1, a2: a2, s2: s2,v2:v2, s: (s0 + s5 + s10 + s1 + s2), v: (v0+ v1 + v2 +v5 + v10) }
                    //Begin HUNGLQ them doan lay du lieu dong theo danh muc loai thue
                    //Lay du lieu va day ra bien lu danh sach bao cao 
                    //Khai bao bien day ra bao cao
                    let s=0,v=0
                    for (var key in objRep) {
                        //Neu mang co du lieu thi moi xu
                        if (objRep[key].dataVat.length <= 0) continue
                        //push cai dong dien giai truoc
                        let rowname
                        rowname = worksheet.addRow({ index: objRep[key].key_vrn , form: "", serial: "", seq: "", idt: "",bname: "",btax: "",sum: "",vat: "", note: "", curr: "", exrt: ""})
                        worksheet.mergeCells(row_stt, 1, row_stt, 12)
                        rowname.getCell('A').border = borderStyles
                        rowname.alignment = { horizontal: 'left' }
                        rowname.font = { name: 'Times New Roman', bold: true }
                        row_stt = row_stt + 1
                        //Push tiep du lieu bao cao
                        for (const rep of objRep[key].dataVat) {
                            rep.index = ++objRep[key].index
                            //danh dau de format font chữ checksum
                            // rep.checksum = 0
                            // arrRep.push(rep)
                            let rowdata = worksheet.addRow(rep)
                            rowdata.font = { name: 'Times New Roman' }
                            //set border for range of data
                            dataRange.map(x => {
                                rowdata.getCell(x).border = borderStyles
                                if(x=='L') rowdata.getCell(x).alignment = { wrapText:true }
                            })
                            row_stt = row_stt + 1
                        }
                        // arrRep.push({index:`TỔNG`,sumv:objRep[key].totalSumByVrt,vatv:objRep[key].totalVatByVrt,checksum:1})
                        rowname = worksheet.addRow({ index: `TỔNG` , form: "", serial: "", seq: "", idt: "",bname: "",btax: "",sum:objRep[key].totalSumByVrt,vat: objRep[key].totalVatByVrt, note: ""})
                        worksheet.mergeCells(row_stt, 1, row_stt, 9)
                        worksheet.getCell(`H${row_stt}`).border = borderStyles
                        worksheet.getCell(`I${row_stt}`).border = borderStyles
                        worksheet.getCell(`J${row_stt}`).border = borderStyles
                        rowname.getCell('A').border = borderStyles
                        worksheet.getCell(`A${row_stt}`).alignment = { horizontal: 'left' }
                        // rowname.alignment = { horizontal: 'left' }
                        rowname.font = { name: 'Times New Roman', bold: true }
                        row_stt = row_stt + 1
                        s+=objRep[key].totalSumByVrt
                        v+=objRep[key].totalVatByVrt
                        //Push cai dong tong
                    }
                    //End HUNGLQ them doan lay du lieu dong theo danh muc loai thue 
                    // json = { stax: taxc, sname: token.on, fd: fr, td: to, rt: rt, arrRep: arrRep,s:s,v:v }

                    // return json
                    worksheet.mergeCells(row_stt, 1, row_stt, 9)
                    worksheet.getCell(`A${row_stt}`).value = `${rt}`
                    worksheet.getCell(`A${row_stt}`).border = borderStyles
                    worksheet.getCell(`A${row_stt}`).font = { name: 'Times New Roman' }
                    worksheet.getCell(`A${row_stt}`).alignment = { horizontal: 'left' }

                    worksheet.getCell(`J${row_stt}`).value = `${s}`
                    worksheet.getCell(`J${row_stt}`).border = borderStyles
                    worksheet.getCell(`J${row_stt}`).font = { name: 'Times New Roman', bold: true }
                    worksheet.getCell(`J${row_stt}`).alignment = { horizontal: 'right' }


                    worksheet.getCell(`K${row_stt}`).value = `${v}`
                    worksheet.getCell(`K${row_stt}`).border = borderStyles
                    worksheet.getCell(`K${row_stt}`).font = { name: 'Times New Roman', bold: true }
                    worksheet.getCell(`K${row_stt}`).alignment = { horizontal: 'right' }


                    const buffer = await workbook.xlsx.writeBuffer()
                    res.end(buffer, "binary")
                    return
                }
                else if (report == 2) {
                    fn = "temp/08BKHDXT.xlsx"
                    if (config.ent == "vib") fn = "temp/08BKHDXT_VIB.xlsx"
                    where = "where idt between :1 and :2 and stax=:3"
                    binds = [fd, td, taxc]
                  
                    if (type !== "*") {
                        where += ` and type=:type`
                        binds.push(type)
                    }
                              
                    if (!ou.includes("*")) {
                        where += ` and ou=:ou`
                        binds.push(ou)
                    }
                    if (config.ou_grant && token.ou == token.u) {
                        // where += `and ou in (select id from s_ou start with id=:ou connect by prior id=pid) `
                        where += ` AND exists (SELECT 1 FROM s_ou o where o.id = ou START WITH o.id = :ou CONNECT BY PRIOR o.id = o.pid) `
                        binds.push(token.u)
                    }
                    //${config.ent == "vib" ? "CASE WHEN STATUS = 4 AND XML IS NULL THEN 7 ELSE STATUS END" : "status"}
                    if (config.ent == "vib") {
                        extcol = `,CASE WHEN STATUS = 4 AND XML IS NULL THEN 7 ELSE STATUS END "vibstatus"`
                    }
                    sql = `select id "id", TO_CHAR(idt,'${mfd}') "idt",form "form",serial "serial",seq "seq",btax "btax",bname "bname",buyer "buyer",CASE WHEN status=1 THEN 'Chờ cấp số' WHEN status=2 THEN 'Chờ duyệt' WHEN status=3 THEN 'Đã duyệt' WHEN status=4 THEN 'Đã hủy' WHEN status=6 THEN 'Chờ hủy' ELSE TO_CHAR(status) END "status",status "cstatus", sumv "sumv",vatv "vatv",note || '-' || cde || '-' || adjdes "note"${extcol} from s_inv ${where} order by idt,form,serial,seq`
                    let vuuid = uuid.v4()
                    if (!['vib'].includes(config.ent)) result = await dbs.query(sql, binds)
                    else {
                        binds = [fd, td, taxc, type, type, token.u]
                        result = await dbs.query(sqlcountbcbhvib, binds)
                        rows = result.rows
                        if (rows && rows.length > 0 && rows[0].total > max_row_report_tct) {
                            res.json({ err: `Số lượng bản ghi vượt quá số lượng cho phép (${max_row_report_tct} dòng) \n (The number of records exceeds the allowed number (${max_row_report_tct} lines))` })
                        }
                        logger4app.debug(`Chay thu tuc tong hop du lieu bao cao ban hang VIB ${token.uid} - ${token.u} - ${token.taxc} - ${ndate} - ${vuuid}`)
                        console.time(`Chay thu tuc tong hop du lieu bao cao ban hang VIB ${token.uid} - ${token.u} - ${token.taxc} - ${ndate} - ${vuuid}`)
                        
                        binds = [fd, td, taxc, token.u, type]
                        result = await dbs.executereportvat(sqlsumbcbhvib, sqlfetchbcbhvib, binds)
                        console.timeEnd(`Chay thu tuc tong hop du lieu bao cao ban hang VIB ${token.uid} - ${token.u} - ${token.taxc} - ${ndate} - ${vuuid}`)
                    }
                    rows = result.rows
                    if (config.ent == "vib") {
                        //const jsonData = JSON.parse(JSON.stringify(rows));
                        if (rows.length <= 0) {
                            res.json({ data: '' })
                            return
                        }
                        logger4app.debug(`Chay ket xuat CSV bao cao ban hang VIB ${token.uid} - ${token.u} - ${token.taxc} - ${ndate} - ${vuuid}`)
                        console.time(`Chay ket xuat CSV bao cao ban hang VIB ${token.uid} - ${token.u} - ${token.taxc} - ${ndate} - ${vuuid}`)
                        const json2csvParser = new Json2csvParser({ header: true, default: '', quote: '', delimiter: ',', encoding: 'unicode', withBOM: true });
                        const csv = json2csvParser.parse(rows);
                        console.timeEnd(`Chay ket xuat CSV bao cao ban hang VIB ${token.uid} - ${token.u} - ${token.taxc} - ${ndate} - ${vuuid}`)
                        res.json({ data: csv })
                        return
                    }
                    let i = 0
                    let rowbc = []
                    for (const row of rows) {
                        if (ENT == "bvb" && (row.note).startsWith('--Điều chỉnh')) {
                            const data = await dbs.query(`select JSON_QUERY (doc, '$.dif') "dif" from s_inv where id=${row.id}`)
                            let rs = data.rows
                            if (!(rs[0].dif)) {
                                row.sumv = ""
                                row.vatv = ""
                            }
                        }
                        row.index = ++i
                        if (row.cstatus == 4) {
                            delete row["sumv"]
                            delete row["vatv"]
                        }
                        if (config.ent != "vib") {
                            rowbc.push(row)
                        }
                        else {

                            let doc = await rbi(row.id)
                            let items = doc.items
                            for (const item of items) {
                                if (items.type === 'Chiết khấu') continue
                                let rowitem = { ...row }
                                rowitem.sumv = item.amount
                                rowitem.vatv = item.vat
                                rowitem.itemname = item.name
                                if ([4,7].includes(rowitem.vibstatus)) {
                                    delete rowitem["sumv"]
                                    delete rowitem["vatv"]
                                }
                                if (rowitem.vibstatus == 4) {
                                    rowitem.status = "Hủy HĐ đã ký số"
                                } else if (rowitem.vibstatus == 7) {
                                    rowitem.status = "Hủy HĐ chưa ký số"
                                }
                                else {
                                    rowitem.status = rowitem.status
                                }
                                rowbc.push(rowitem)
                            }
                        }
                    }
                    json = { stax: taxc, sname: token.on, fd: fr, td: to, rt: rt, tn: tn, table: rowbc }
                }
                else if (report == 3 || report == 4) {
                    let status_del = '(2,4)', where_cancel = 'sum(CASE WHEN status=4 THEN 1  WHEN status=2 THEN 1 ELSE 0 END)'
                    if (config.ent == 'vhc') {
                        status_del = '(4)'
                        where_cancel = 'sum(CASE WHEN status=4 THEN 1  ELSE 0 END)'
                    } 
                    fn = "temp/05BCHD.xlsx"
                    if (report == 4 && config.ent == 'baca') {
                        fn = "temp/05BCHD_SL.xlsx"
                    }
                    let data = [], total0 = 0, total1 = 0, total2 = 0, total3 = 0, total4 = 0, total5 = 0, total6 = 0, total7 = 0
                    let where, binds, type_name
                    if (type == "*") {
                        where = ""
                        binds = [ fd, taxc, fd, td, taxc, fd, td, taxc]
                    }
                    else {
                        where = " and type=:type"
                        binds = [ fd, taxc, type, fd, td, taxc, type, fd, td, taxc, type]
                        type_name = tenhd(type)
                    }
                   
                   
					  // sua dap ung nhieu dai so khac nhau chung ky hieu
                      sql =  `select form "form",serial "serial",0 "min1",0 "max1",min "min0",max "max0",0 "maxu0",0 "minu",0 "maxu", 0 "minc",0 "maxc",0 "cancel",null "clist" from s_serial where taxc=:1 and fd<:2  and (status not in (2,3,4) or (td>=:fd and status=2 ) or (td>=:fd and status=4 )) ${where} order by form,serial,min`
                      if (type == "*")   result = await dbs.query(sql, [taxc, fd,fd,fd])
                      else result = await dbs.query(sql, [taxc, fd,fd,fd,type])
                      rows = result.rows
                      let rows_serial = [],formc = "",serialc = "",bindsc = [],rows_serialarr = []
                      for (const row of rows)
                      {
                          if (formc==row.form && serialc==row.serial)
                          {
                              for (let rowc of rows_serial){
                                  if(rowc.form==row.form && rowc.serial==row.serial) {
                                      rowc.max0 = row.max0
                                  }
                              
                              }
                          }else{
                              rows_serial.push(row)
                          }
                          formc=row.form
                          serialc=row.serial
                      }
                      let c_id =moment(new Date()).format("YYYYMMDDHHHHmmssSSS")
                      for (let rowc of rows_serial){
                         
                          rowc.c_id=(c_id)
                          rowc=Object.values(rowc)
                          rows_serialarr.push(rowc)
                      }
                     
                      if(rows_serialarr.length>0){
                          await dbs.executeMany("insert into s_report_tmp(form,serial,min1, max1, min0, max0, maxu0,minu, maxu,minc,maxc, cancel,clist,c_id) values (:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14)", rows_serialarr)
                      }
                      

                      sql =  `select form "form",serial "serial",min "min1",max "max1",0 "min0",0 "max0",0 "maxu0",0 "minu",0 "maxu",0 "minc",0 "maxc",0 "cancel",null "clist" from s_serial where taxc=:1 and fd between :2 and :3 and status !=3 ${where} order by form,serial,min`
                      if (type == "*")  result = await dbs.query(sql, [taxc, fd, td])
                      else result = await dbs.query(sql, [taxc, fd, td,type])
                      rows = result.rows
                       rows_serial=[],formc="",serialc="",bindsc=[],rows_serialarr=[]
                      for (const row of rows)
                      {
                          if (formc==row.form && serialc==row.serial)
                          {
                              for (let rowc of rows_serial){
                                  if(rowc.form==row.form && rowc.serial==row.serial) {
                                      rowc.max1 = row.max1
                                  }
                              
                              }
                          }else{
                              rows_serial.push(row)
                          }
                          formc=row.form
                          serialc=row.serial
                      }
                      let c_id2 = moment(new Date()).format("YYYYMMDDHHHHmmssSSS")
                      for (let rowc of rows_serial){
                         
                          rowc.c_id=(c_id2)
                          rowc=Object.values(rowc)
                          rows_serialarr.push(rowc)
                      }
                     
                      if(rows_serialarr.length > 0){
                          await dbs.executeMany("insert into s_report_tmp(form,serial,min1, max1, min0, max0, maxu0,minu, maxu,minc,maxc,  cancel,clist,c_id)  values (:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14)", rows_serialarr)
                      }

                      sql =  `select form "form",serial "serial",0 "min1",0 "max1",0 "min0",0 "max0",0 "maxu0",0 "minu",0 "maxu", cur "minc",max "maxc",0 "cancel",null "clist" from s_serial where taxc=:1 and fd <:2 and td between :3 and :4  and status =2 ${where} order by form,serial,min`
                      if (type == "*") result = await dbs.query(sql, [taxc,td,fd,td])
                      else result = await dbs.query(sql, [taxc,td,fd,td,type])
                      rows = result.rows
                       rows_serial=[],formc="",serialc="",bindsc=[],rows_serialarr=[]
                      for (const row of rows)
                      {
                          if (formc==row.form && serialc==row.serial)
                          {
                              for (let rowc of rows_serial){
                                  if(rowc.form==row.form && rowc.serial==row.serial) {
                                      rowc.maxc = row.maxc
                                  }
                              
                              }
                          }else{
                              rows_serial.push(row)
                          }
                          formc=row.form
                          serialc=row.serial
                      }
                      let c_id3 =moment(new Date()).format("YYYYMMDDHHHHmmssSSS")
                      for (let rowc of rows_serial){
                         
                          rowc.c_id=(c_id3)
                          rowc=Object.values(rowc)
                          rows_serialarr.push(rowc)
                      }
                     
                      if(rows_serialarr.length > 0){
                          await dbs.executeMany("insert into s_report_tmp(form,serial,min1, max1, min0, max0, maxu0,minu, maxu,minc,maxc, cancel,clist,c_id) values (:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14)", rows_serialarr)
                      }

                    sql = `select t1.form "form",t1.serial "serial",t1.min1 "min1",t1.max1 "max1",t1.min0 "min0",t1.max0 "max0",t1.maxu0 "maxu0",t1.minu "minu", t1.maxu "maxu",t1.minc "minc",t1.maxc "maxc",t1.cancel "cancel",t2.clist "clist" from 
                    (select a.form ,a.serial ,sum(a.min1)  min1,sum(a.max1) max1 ,sum(a.min0) min0,sum(a.max0) max0,sum(a.maxu0) maxu0,sum(a.minu) minu,sum(a.maxu) maxu,sum(a.minc) minc,sum(a.maxc) maxc,sum(a.cancel) cancel,max(a.clist) clist from (
                    select form,serial,min1,max1,min0,max0,maxu0,minu,maxu,minc,maxc,cancel,null clist from s_report_tmp where c_id= ${c_id}
                    union
                    select form,serial,min1,max1,min0,max0,maxu0,minu,maxu,minc,maxc,cancel,null clist from s_report_tmp where c_id= ${c_id2}
                    union
                    select form,serial,min1,max1,min0,max0,maxu0,minu,maxu,minc,maxc,cancel,null clist from s_report_tmp where c_id= ${c_id3}
                    union
                    select form,serial,0 min1,0 max1,0 min0,0 max0,max(TO_NUMBER(seq)) maxu0,0 minu,0 maxu,0 minc,0 maxc,0 cancel,null clist from s_inv where idt<:fd and stax=:taxc and status>1 ${where} group by form,serial
                    union
                    select form,serial,0 min1,0 max1,0 min0,0 max0,0 maxu0,min(TO_NUMBER(seq)) minu,max(TO_NUMBER(seq)) maxu,0 minc,0 maxc,${where_cancel} cancel,null clist from s_inv where idt between :fd and :td and stax=:taxc and status>1 ${where} group by form,serial
                    union
                    select form,serial,0 min1,0 max1,0 min0,0 max0,0 maxu0,0 minu,0 maxu,0 minc,0 maxc,0 cancel,null clist from s_inv where idt between :fd and :td and stax=:taxc and status in ${status_del} ${where} group by form,serial
                    ) a group by a.form,a.serial) t1 left join 
                    ( select form,serial,JSON_ARRAYAGG(TO_NUMBER(seq)  RETURNING clob) clist from s_inv where idt between :fd and :td and stax=:taxc and status in ${status_del} ${where} group by form,serial) t2 on t1.form=t2.form and t1.serial=t2.serial`
                    result = await dbs.query(sql, binds)
                    rows = result.rows
                    await dbs.query(`Delete from s_report_tmp where c_id=${c_id}`)
                    await dbs.query(`Delete from s_report_tmp where c_id=${c_id2}`)
                    await dbs.query(`Delete from s_report_tmp where c_id=${c_id3}`)
                    let i = 0
                    for (let row of rows) {
                        row.index = ++i
                        row.type_name = type == "*" ? tenhd(row.form.substr(0, 6)) : type_name
                        const clist = JSON.parse(row.clist)
                        if (util.isEmpty(clist)) {
                            delete row.cancel
                        }
                        else {
                            let arr = clist.sort((a, b) => a - b), len = arr.length, str
                            if (len === 1) {
                                str = arr[0].toString().padStart(7, "0")
                            }
                            else if (len === 2) {
                                const cs = arr[0] + 1 === arr[1] ? "-" : ";"
                                for (let i = 0; i < len; i++) {
                                    arr[i] = arr[i].toString().padStart(7, "0")
                                }
                                str = arr.join(cs)
                            }
                            else {
                                let del = []
                                for (let i = 1; i < len; i++) {
                                   
                                    if (Number(arr[i - 1]) + 2 === Number(arr[i + 1])) del.push(i)
                                }
                                for (let i = 0; i < len; i++) {
                                    if (del.includes(i)) arr[i] = null
                                    else arr[i] = arr[i].toString().padStart(7, "0")
                                }
                                str = arr.join(";").replace(/\;\;+/g, '-')
                            }
                            row.clist = str
                        }
                            let max0 = parseInt(row.max0), min0 = parseInt(row.min0), max1 = parseInt(row.max1), min1 = parseInt(row.min1), maxu0 = parseInt(row.maxu0), minu = parseInt(row.minu), maxu = parseInt(row.maxu), cancel = parseInt(row.cancel),minc=parseInt(row.minc),maxc=parseInt(row.maxc)
                        //use
                        let su = 0, s0 = 0, s1 = 0, s11 = 0
                        if (maxu > 0) {
                            su = maxu - minu + 1
                            row.su = su
                            row.fu = minu.toString().padStart(7, '0')
                            row.tu = maxu.toString().padStart(7, '0')
                            row.used = cancel > 0 ? (su - cancel) : su
                        }
                        //tondau
                        if (max1 > 0) {
                            if(max0 > 0){
                                s0 = max1 - min0 + 1
                                row.s0 = s0
                               
                            }else{
                                s0 = max1 - min1 + 1
                                row.s0 = s0
                               
                            }
                            row.f1 = min1.toString().padStart(7, '0')
                            row.t1 = max1.toString().padStart(7, '0')
                        }
                         if (max0 > 0) {
                            if (maxu0 > 0) {
                                if (max0 > maxu0) {
                                    if(max1 > 0){
                                        s0 = max1 - maxu0
                                    }else{
                                        s0 = max0 - maxu0
                                    }
                                   
                                    row.s0 = s0
                                    row.f0 = (maxu0 + 1).toString().padStart(7, '0')
                                    row.t0 = max0.toString().padStart(7, '0')
                                }
                                if (max0 == maxu0 && max1>maxu0) {
                                    s0 = max1 - maxu0
                                    row.s0 = s0
                                   
                                }
                            }
                            else {
                                if(max1 > 0){
                                    s0 = max1 - min0 + 1
                                }else{
                                    s0 = max0 - min0 + 1
                                }
                               // s0 = max0 - min0 + 1
                                row.s0 = s0
                                row.f0 = min0.toString().padStart(7, '0')
                                row.t0 = max0.toString().padStart(7, '0')
                            }
                        }
                        //toncuoi
                        if (maxu > 0) {
                            if (s0 > su) {
                                row.s2 = s0 - su
                                row.f2 = (maxu + 1).toString().padStart(7, '0')
                                if (max1 > 0) row.t2 = row.t1
                                else if (max0 > 0) row.t2 = row.t0
                            }
                        }
                        else {
                            if (max1 > 0) {
                                if(min0>0){
                                    row.s2=  max1 - (maxu0==0?min0:maxu0+1) + 1
                                    row.f2 = (maxu0==0?min0:maxu0+1).toString().padStart(7, '0')
                                    row.t2 = row.t1
                                }else{
                                    row.s2=  max1 - min1 + 1
                                    row.f2 = min1.toString().padStart(7, '0')
                                    row.t2 = row.t1
                                }
                              
                            }
                            else if (max0 > 0) {
                                row.s2 = s0
                                row.f2 = row.f0
                                row.t2 = row.t0
                            }
                        }
                         //huy ph
                         if(maxc>0){
                               
                               
                            minc= (minc==0?(maxu0==0?(min0>0?min0-1:(maxu>0?(min1+maxu-1):min1-1)):maxu0):minc)+1
                            minc= (minc==0?1:minc)
                            row.countc = maxc - (minc)+1
                            row.listc = `${minc.toString().padStart(7, '0')} - ${maxc.toString().padStart(7, '0')}`

                            row.s2=  0
                            row.f2 = ""
                            row.t2 = ""
                        }else{
                           
                        }

                        //Tổng số sử dụng, xóa bỏ, mất, hủy
                        row.type = "E"
                        row.lost = 0
                        if (maxc > 0) {
                            su = maxc - (minu == 0 ? minc : minu)
                            row.su = su + 1
                            row.fu = (minu == 0 ? minc.toString().padStart(7, '0') : minu.toString().padStart(7, '0'))
                            row.tu = maxc.toString().padStart(7, '0')
                        }
                        else {
                            if (maxu > 0) {
                                row.tu = maxu.toString().padStart(7, '0')
                            }
                        }
                        
                        if (typeof row.countc == "undefined") row.countc = 0
                        if (typeof row.s0 == "undefined") row.s0 = 0
                        if (typeof row.su == "undefined") row.su = 0
                        if (typeof row.used == "undefined") row.used = 0
                        if (typeof row.cancel == "undefined") row.cancel = 0
                        if (typeof row.s2 == "undefined") row.s2 = 0
                        if (typeof row.s1 == "undefined") row.s1 = 0
                        if (typeof row.s11 == "undefined") row.s11 = 0
                        row.note = row.serial
                        if ((row.countc == 0) && (row.s0 == 0) && (row.su == 0) && (row.used == 0) && (row.cancel == 0) && (row.s2 == 0)) {

                        }else{
                        data.push(row)
                        }
                        if (max1 > 0) {
                            s1 = max1 - min1 + 1
                            row.s1 = s1
                        }
                        if (max0 > 0) {
                            s11 = max0 - min0 + 1 - maxu0;
                            row.s11 = s11
                        }
                        total0 += row.s11
                        total1 += row.s1
                        total2 += row.used
                        total3 += row.cancel
                        total4 += row.lost
                        total5 += row.countc
                        total6 += row.su
                        total7 += row.s2
                    }
                    json = { stax: taxc, sfaddr: sfaddr, now, ndate, nmonth, nyear, sname: token.on, period: `Kỳ báo cáo ${query.period}`, fd: fr, td: to, rt: rt, tn: tn, table: data, total0: total0, total1: total1, total2: total2, total3: total3, total4: total4, total5: total5, total6: total6, total7: total7 }
                }
                else if (report == 11) {
                    fn = "temp/BKHDDTCQT_BACA.xlsx"
                    where = "where idt between :1 and :2 and stax=:3"
                    binds = [fd, td, taxc]
                    let ij = 4
                    if (type !== "*") {
                        where += ` and type=:${ij++}`
                        binds.push(type)
                    }

                    where += where_ft
                    where += ` and status = 3`

                    sql = `select a.id "id",a.form "form",a.serial "serial",a.seq "seq",TO_CHAR(a.idt,'${mfd}') "idt",a.btax "btax",a.bname "bname",a.buyer "buyer", a.note "note" from s_inv a ${where} order by idt,form,serial,seq`
                    result = await dbs.query(sql, binds)
                    rows = result.rows

                    let rowbc = []
                    let i = 0
                    for (const row of rows) {
                        let doc=await rbi(row.id)

                        let items = [ ... doc.items]

                        //Lấy danh sách các item hóa đơn
                        for (const item of items) {
                            i++
                            let rowdc = item
                            rowdc.formserial = `${row.form} - ${row.serial}`
                            rowdc.seq = row.seq
                            rowdc.idt = row.idt
                            rowdc.bname = (row.bname != null && row.bname != "") ? row.bname : row.buyer
                            rowdc.btax = row.btax
                            rowdc.itemname = item.name
                            rowdc.itemunit = item.unit
                            rowdc.itemquantity = item.quantity
                            rowdc.itemamount = item.amount
                            rowdc.itemvrn = item.vrn
                            rowdc.itemvat = item.vat
                            rowdc.itemtotal = item.total
                            rowdc.itemnote = row.note

                            rowdc.index = i
                            rowbc.push(rowdc)
                        }

                    }

                    json = { stax: taxc, sname: token.on, firstlist: firstlist, updatelist: updatelist, sfaddr: sfaddr, fd: fr, td: to, table: rowbc }
                }
                else if (report == 16) {
                    fn = "temp/01_BK_BC26_AC_2020.xlsx"
                    const token = sec.decode(req), query = req.query
                    let where = " where ((:fd BETWEEN fd AND nvl(td, to_date('31/12/3000','DD/MM/YYYY'))) OR (:td BETWEEN fd AND nvl(td, to_date('31/12/3000','DD/MM/YYYY')))) ", order, sql, result, rows, ret, val, str, arr = []
                    let binds = [fd, td]
                    order = " order by id"

                    sql = `select id "id",taxc "taxc",type "type",form "form",serial "serial",min "min",max "max",cur "cur",status "status",fd "fd",td "td", uses "uses", TO_NUMBER(SUBSTR(form, -3)) "idx",des "des",priority "priority" from s_serial ${where} ${order}`
                    result = await dbs.query(sql, binds)
                    rows = result.rows
                    let rowbc = []
                    let i = 0

                    for (const row of rows) {
                        i++
                        row.typename =  tenhd(row.type)
                        row.cur = row.cur + 1
                        row.amount = row.max - row.cur + 1
                        row.index = i
                        rowbc.push(row)

                    }
                    let d = new Date()
                    json = { table: rowbc, stax: token.taxc, hour: d.getHours(), minute: d.getMinutes(), dt: moment(d).format("DD/MM/YYYY") }
                }
                else if (report == 17) {
                    let status_del = '(2,4)', where_cancel = 'sum(CASE WHEN status=4 THEN 1  WHEN status=2 THEN 1 ELSE 0 END)'
                    fn = "temp/BACA_INTERNAL.xlsx"
                    let data = []
                    let where, binds, type_name, whereoutbph = "", subresult, whereouinv = "", wherename = ""
                    if (type == "*") {
                        where = ""
                        binds = [ fd, fd, td, fd, td, fd, td, fd, td, fd, td, fd, td, fd, td]
                    }
                    else {
                        where = "and type=:type"
                        binds = [ fd, type, fd, td, type, fd, td,fd, td,fd, td, fd, td, fd, td, fd, td, type]
                        type_name = tenhd(type)
                    }
                    if (!ou.includes("*")) {

                        subresult = await dbs.query(`select mst "mst" from s_ou where id=:id`, [ou])
                        whereoutbph += ` and taxc='${subresult.rows[0].mst}'`
                        whereouinv += ` and stax='${subresult.rows[0].mst}'`
                        wherename += `mst='${subresult.rows[0].mst}'`
                    }
					  // sua dap ung nhieu dai so khac nhau chung ky hieu
                    sql =  `select taxc "stax",form "form",serial "serial",0 "min1",0 "max1",min "min0",max "max0",0 "maxu0",0 "minu",0 "maxu", 0 "minc",0 "maxc",0 "cancel" from s_serial where fd<:1  and (status not in (2,3,4) or (td>=:fd and status=2 ) or (td>=:fd and status=4 )) ${where} ${whereoutbph} order by taxc,form,serial,min`
                    if (type == "*")   result = await dbs.query(sql, [fd,fd,fd])
                      else result = await dbs.query(sql, [fd,fd,fd,type])
                      rows = result.rows
                      let rows_serial = [],formc = "",serialc = "",bindsc = [],rows_serialarr = []
                      for (const row of rows)
                      {
                          if (formc==row.form && serialc==row.serial)
                          {
                              for (let rowc of rows_serial){
                                  if(rowc.form==row.form && rowc.serial==row.serial) {
                                      rowc.max0 = row.max0
                                  }
                              
                              }
                          }else{
                              rows_serial.push(row)
                          }
                          formc=row.form
                          serialc=row.serial
                      }
                      let c_id =moment(new Date()).format("YYYYMMDDHHHHmmssSSS")
                      for (let rowc of rows_serial){
                         
                          rowc.c_id=(c_id)
                          rowc=Object.values(rowc)
                          rows_serialarr.push(rowc)
                      }
                     
                      if(rows_serialarr.length>0){
                          await dbs.executeMany("insert into s_report_tmp(stax,form,serial,min1, max1, min0, max0, maxu0,minu, maxu,minc,maxc, cancel,c_id) values (:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14)", rows_serialarr)
                      }
                      
                      sql =  `select taxc "stax",form "form",serial "serial",min "min1",max "max1",0 "min0",0 "max0",0 "maxu0",0 "minu",0 "maxu",0 "minc",0 "maxc",0 "cancel" from s_serial where fd between :1 and :2 and status !=3 ${where} ${whereoutbph} order by taxc,form,serial,min`
                      if (type == "*")  result = await dbs.query(sql, [fd, td])
                      else result = await dbs.query(sql, [fd, td,type])
                      rows = result.rows
                       rows_serial=[],formc="",serialc="",bindsc=[],rows_serialarr=[]
                      for (const row of rows)
                      {
                          if (formc==row.form && serialc==row.serial)
                          {
                              for (let rowc of rows_serial){
                                  if(rowc.form==row.form && rowc.serial==row.serial) {
                                      rowc.max1 = row.max1
                                  }
                              
                              }
                          }else{
                              rows_serial.push(row)
                          }
                          formc=row.form
                          serialc=row.serial
                      }
                      let c_id2 = moment(new Date()).format("YYYYMMDDHHHHmmssSSS")
                      for (let rowc of rows_serial){
                         
                          rowc.c_id=(c_id2)
                          rowc=Object.values(rowc)
                          rows_serialarr.push(rowc)
                      }
                     
                      if(rows_serialarr.length > 0){
                          await dbs.executeMany("insert into s_report_tmp(stax,form,serial,min1, max1, min0, max0, maxu0,minu, maxu,minc,maxc, cancel,c_id)  values (:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14)", rows_serialarr)
                      }

                      sql = `select taxc "taxc",form "form",serial "serial",0 "min1",0 "max1",0 "min0",0 "max0",0 "maxu0",0 "minu",0 "maxu", cur "minc",max "maxc",0 "cancel" from s_serial where fd <:1 and td between :2 and :3  and status =2 ${where} ${whereoutbph} order by taxc,form,serial,min`
                      if (type == "*") result = await dbs.query(sql, [td,fd,td])
                      else result = await dbs.query(sql, [td,fd,td,type])
                      rows = result.rows
                       rows_serial=[],formc="",serialc="",bindsc=[],rows_serialarr=[]
                      for (const row of rows)
                      {
                          if (formc==row.form && serialc==row.serial)
                          {
                              for (let rowc of rows_serial){
                                  if(rowc.form==row.form && rowc.serial==row.serial) {
                                      rowc.maxc = row.maxc
                                  }
                              
                              }
                          }else{
                              rows_serial.push(row)
                          }
                          formc=row.form
                          serialc=row.serial
                      }
                      let c_id3 =moment(new Date()).format("YYYYMMDDHHHHmmssSSS")
                      for (let rowc of rows_serial){
                         
                          rowc.c_id=(c_id3)
                          rowc=Object.values(rowc)
                          rows_serialarr.push(rowc)
                      }
                     
                      if(rows_serialarr.length > 0){
                          await dbs.executeMany("insert into s_report_tmp(stax,form,serial,min1, max1, min0, max0, maxu0,minu, maxu,minc,maxc, cancel,c_id) values (:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14)", rows_serialarr)
                      }
                      let subdata, subrows, scode, sname, smst
                      if (query.ou == "*" ) {
                          subdata = await dbs.query(`select mst "mst", code "code", name "name" from s_ou where name=:name and rownum=1`, [token.on])
                      }
                      else {
                          subdata = await dbs.query(`select mst "mst", code "code", name "name" from s_ou where id=:id and rownum=1`, [query.ou])
                      }
                      
                      if (subdata.rows.length > 0) {
                          subrows = subdata.rows
                          scode = subrows[0].code
                          sname = subrows[0].name
                          smst = subrows[0].mst

                      } else {
                          subrows = ""
                          scode = "000"
                          sname = token.on
                          smst = taxc
                      }

                    sql = `select t1.stax "stax",t1.form "form",t1.serial "serial",t1.min1 "min1", t1.max1 "max1", t1.min0 "min0",t1.max0 "max0",t1.maxu0 "maxu0",t1.minu "minu", t1.maxu "maxu",t1.minc "minc",t1.maxc "maxc",t1.cancel "cancel",twait "twait", statewait "statewait", cancelwait "cancelwait", subcancel "subcancel", aprove "aprove"  from 
                    (select a.stax,a.form ,a.serial ,sum(a.min1)  min1,sum(a.max1) max1 ,sum(a.min0) min0,sum(a.max0) max0,sum(a.maxu0) maxu0,sum(a.minu) minu,sum(a.maxu) maxu,sum(a.minc) minc,sum(a.maxc) maxc,sum(a.cancel) cancel, sum(a.twait) twait, sum(a.statewait) statewait, sum(a.cancelwait) cancelwait, sum(a.subcancel) subcancel, sum(a.aprove) aprove from (
                    select stax,form,serial,min1,max1,min0,max0,maxu0,minu,maxu,minc,maxc,cancel, 0 twait, 0 statewait, 0 cancelwait, 0 subcancel, 0 aprove from s_report_tmp where c_id= ${c_id} 
                    union
                    select stax,form,serial,min1,max1,min0,max0,maxu0,minu,maxu,minc,maxc,cancel, 0 twait, 0 statewait, 0 cancelwait, 0 subcancel, 0 aprove from s_report_tmp where c_id= ${c_id2}
                    union
                    select stax,form,serial,min1,max1,min0,max0,maxu0,minu,maxu,minc,maxc,cancel, 0 twait, 0 statewait, 0 cancelwait, 0 subcancel, 0 aprove from s_report_tmp where c_id= ${c_id3}
                    union
                    select stax,form,serial,0 min1,0 max1,0 min0,0 max0,max(TO_NUMBER(seq)) maxu0,0 minu,0 maxu,0 minc,0 maxc,0 cancel, 0 twait, 0 statewait, 0 cancelwait, 0 subcancel, 0 aprove from s_inv where idt<:fd and status>1 ${where} ${whereouinv} group by stax,form,serial
                    union
                    select stax,form,serial,0 min1,0 max1,0 min0,0 max0,0 maxu0,min(TO_NUMBER(seq)) minu,max(TO_NUMBER(seq)) maxu,0 minc,0 maxc,${where_cancel} cancel, 0 twait, 0 statewait, 0 cancelwait, 0 subcancel, 0 aprove from s_inv where idt between :fd and :td and status>1 ${where} ${whereouinv} group by stax,form,serial
                    union
                    select stax,form,serial,0 min1,0 max1,0 min0,0 max0,0 maxu0,0 minu,0 maxu,0 minc,0 maxc,0 cancel, count(*) twait, 0 statewait, 0 cancelwait, 0 subcancel, 0 aprove from s_inv where idt between :fd and :td and status = 1 ${whereouinv} group by stax,form,serial
                    union
                    select stax,form,serial,0 min1,0 max1,0 min0,0 max0,0 maxu0,0 minu,0 maxu,0 minc,0 maxc,0 cancel, 0 twait, count(*) statewait, 0 cancelwait, 0 subcancel, 0 aprove from s_inv where idt between :fd and :td and status = 2 ${whereouinv} group by stax,form,serial
                    union
                    select stax,form,serial,0 min1,0 max1,0 min0,0 max0,0 maxu0,0 minu,0 maxu,0 minc,0 maxc,0 cancel, 0 twait, 0 statewait, count(*) cancelwait, 0 subcancel, 0 aprove from s_inv where idt between :fd and :td and status = 6 ${whereouinv} group by stax,form,serial
                    union
                    select stax,form,serial,0 min1,0 max1,0 min0,0 max0,0 maxu0,0 minu,0 maxu,0 minc,0 maxc,0 cancel, 0 twait, 0 statewait, 0 cancelwait, count(*) subcancel, 0 aprove from s_inv where idt between :fd and :td and status = 4 ${whereouinv} group by stax,form,serial
                    union
                    select stax,form,serial,0 min1,0 max1,0 min0,0 max0,0 maxu0,0 minu,0 maxu,0 minc,0 maxc,0 cancel, 0 twait, 0 statewait, 0 cancelwait, 0 subcancel, count(*) aprove from s_inv where idt between :fd and :td and status = 3 ${whereouinv} group by stax,form,serial 
                    union
                    select stax,form,serial,0 min1,0 max1,0 min0,0 max0,0 maxu0,0 minu,0 maxu,0 minc,0 maxc,0 cancel, 0 twait, 0 statewait, 0 cancelwait, 0 subcancel, 0 aprove from s_inv where idt between :fd and :td ${whereouinv} and status in ${status_del} ${where} group by stax,form,serial
                    ) a group by a.stax,a.form,a.serial order by a.stax) t1`
                    
                    result = await dbs.query(sql, binds)
                    rows = result.rows
                    await dbs.query(`Delete from s_report_tmp where c_id=${c_id}`)
                    await dbs.query(`Delete from s_report_tmp where c_id=${c_id2}`)
                    await dbs.query(`Delete from s_report_tmp where c_id=${c_id3}`)
                    
                    let i = 0
                    for (let row of rows) {
                       
                        let subname
                        if (!ou.includes("*") ) {
                            subname = await dbs.query(`select name "name", mst "mst", code "code" from s_ou where id=:id`, [query.ou])
                        }
                        else {
                            subname = await dbs.query(`select mst "mst", code "code", name "name" from s_ou where name=:name`, [token.on])
                        }
                        let res = subname.rows
                        row.submst = res[0].mst
                        row.subcode = res[0].code
                        row.subname = res[0].name
                        row.index = ++i
                        row.type_name = type == "*" ? tenhd(row.form.substr(0, 6)) : type_name
                        let max0 = parseInt(row.max0), min0 = parseInt(row.min0), max1 = parseInt(row.max1), min1 = parseInt(row.min1), maxu0 = parseInt(row.maxu0), minu = parseInt(row.minu), maxu = parseInt(row.maxu), subcancel = parseInt(row.subcancel), minc=parseInt(row.minc),maxc=parseInt(row.maxc)
                        
                        let su = 0, s0 = 0, s1 = 0, s11 = 0, subsu = 0
                        su = row.subcancel + row.cancelwait + row.statewait + row.aprove
                        row.su = su
                        //use
                        if (maxu > 0) {
                            subsu = maxu - minu + 1
                            row.fu = minu.toString().padStart(7, '0')
                            row.tu = maxu.toString().padStart(7, '0')
                            row.used = subcancel > 0 ? (subsu - subcancel) : subsu
                        }
                        //tondau
                        if (max1 > 0) {
                            if(max0 > 0){
                                s0 = max1 - min0 + 1
                                row.s0 = s0
                               
                            }else{
                                s0 = max1 - min1 + 1
                                row.s0 = s0
                               
                            }
                            row.f1 = min1.toString().padStart(7, '0')
                            row.t1 = max1.toString().padStart(7, '0')
                        }
                         if (max0 > 0) {
                            if (maxu0 > 0) {
                                if (max0 > maxu0) {
                                    if(max1 > 0){
                                        s0 = max1 - maxu0
                                    }else{
                                        s0 = max0 - maxu0
                                    }
                                   
                                    row.s0 = s0
                                    row.f0 = (maxu0 + 1).toString().padStart(7, '0')
                                    row.t0 = max0.toString().padStart(7, '0')
                                }
                                if (max0 == maxu0 && max1>maxu0) {
                                    s0 = max1 - maxu0
                                    row.s0 = s0
                                }
                            }
                            else {
                                if(max1 > 0){
                                    s0 = max1 - min0 + 1
                                }else{
                                    s0 = max0 - min0 + 1
                                }
                               // s0 = max0 - min0 + 1
                                row.s0 = s0
                                row.f0 = min0.toString().padStart(7, '0')
                                row.t0 = max0.toString().padStart(7, '0')
                            }
                        }
                        //toncuoi
                        if (maxu > 0) {
                            if (s0 > subsu) {
                                row.s2 = s0 - subsu
                                row.f2 = (maxu + 1).toString().padStart(7, '0')
                                if (max1 > 0) row.t2 = row.t1
                                else if (max0 > 0) row.t2 = row.t0
                            }
                        }
                        else {
                            if (max1 > 0) {
                                if(min0>0){
                                    row.s2=  max1 - (maxu0==0?min0:maxu0+1) + 1
                                    row.f2 = (maxu0==0?min0:maxu0+1).toString().padStart(7, '0')
                                    row.t2 = row.t1
                                }else{
                                    row.s2=  max1 - min1 + 1
                                    row.f2 = min1.toString().padStart(7, '0')
                                    row.t2 = row.t1
                                }
                              
                            }
                            else if (max0 > 0) {
                                row.s2 = s0
                                row.f2 = row.f0
                                row.t2 = row.t0
                            }
                        }
                         //huy ph
                         if(maxc>0){
                               
                            minc= (minc==0?(maxu0==0?(min0>0?min0-1:(maxu>0?(min1+maxu-1):min1-1)):maxu0):minc)+1
                            minc= (minc==0?1:minc)
                            row.countc = maxc - (minc)+1
                            row.listc = `${minc.toString().padStart(7, '0')} - ${maxc.toString().padStart(7, '0')}`

                            row.s2=  0
                            row.f2 = ""
                            row.t2 = ""
                        }else{
                           
                        }
                        //Tổng số sử dụng, xóa bỏ, mất, hủy
                        row.type = "E"
                        row.lost = 0
                        if (maxc > 0) {
                            // su = maxc - (minu == 0 ? minc : minu)
                            // row.su = su + 1
                            row.fu = (minu == 0 ? minc.toString().padStart(7, '0') : minu.toString().padStart(7, '0'))
                            row.tu = maxc.toString().padStart(7, '0')
                        }
                        else {
                            if (maxu > 0) {
                                row.tu = maxu.toString().padStart(7, '0')
                            }
                        }
                        if (typeof row.countc == "undefined") row.countc = 0
                        if (typeof row.s0 == "undefined") row.s0 = 0
                        if (typeof row.su == "undefined") row.su = 0
                        if (typeof row.used == "undefined") row.used = 0
                        if (typeof row.cancel == "undefined") row.cancel = 0
                        if (typeof row.s2 == "undefined") row.s2 = 0
                        if (typeof row.s1 == "undefined") row.s1 = 0
                        if (typeof row.s11 == "undefined") row.s11 = 0
                        row.note = row.serial
                        if ((row.countc == 0) && (row.s0 == 0) && (row.su == 0) && (row.used == 0) && (row.cancel == 0) && (row.s2 == 0)) {

                        }else{
                        data.push(row)
                        }
                        if (max1 > 0) {
                            s1 = max1 - min1 + 1
                            row.s1 = s1
                        }
                        if (max0 > 0) {
                            s11 = max0 - min0 + 1 - maxu0;
                            row.s11 = s11
                        }
                    }

                    json = { stax: smst, now, ndate, nmonth, nyear, sname: sname, fd: fr, td: to, rt: rt, tn: tn, table: data }
                }
                else if (report == 18) {
                    fn = "temp/BACA_PERIOD.xlsx"
                    const token = sec.decode(req), query = req.query
                    let status = query.status
                    let binds, where, data

                    let subdata, subrows, scode, sname, smst
                      if (query.ou == "*" ) {
                          subdata = await dbs.query(`select mst "mst", code "code", name "name" from s_ou where name=:name `, [token.on])
                      }
                      else {
                          subdata = await dbs.query(`select mst "mst", code "code", name "name" from s_ou where id=:id`, [query.ou])
                      }
                      
                      if (subdata.rows.length > 0) {
                          subrows = subdata.rows
                          scode = subrows[0].code
                          sname = subrows[0].name
                          smst = subrows[0].mst

                      } else {
                          subrows = ""
                          scode = "000"
                          sname = token.on
                          smst = taxc
                      }
                
                    if (query.ou == "*") { 
                        if (status == "*") {

                            where = "and STATUS IN (2,3,4,6)"
                            status = "Tất cả"
                            binds = [fd, td]
                        }
                        else {
                            where = "and status=:status"
                            binds = [fd, td, status]
                        }
                        sql = `select stax,  form, serial, status, note, seq, idt from s_inv where id > 0 and idt between :fd and :td ${where} order by stax,form,serial`
                    }
                    else {
                        if (status == "*") {

                            where = "and STATUS IN (2,3,4,6)"
                            status = "Tất cả"
                            binds = [fd,td,smst]
                            sql = `select stax, form, serial, status, note, seq, idt from s_inv where id > 0 and idt between :fd and :td and stax=:smst ${where} order by stax,form,serial`
                        }
                        else {
                            where = "and status=:status"
                            binds = [ fd,td,taxc,status]
                            sql = `select stax, form, serial, status, note, seq, idt from s_inv where id > 0 and idt between :fd and :td and stax=:taxc and ou=${query.ou} ${where}  order by stax,form,serial`
                        }
                    }
                    data = await dbs.query(sql,binds)
                    let rows = data.rows
                 
                    let i = 0
                    for (let row of rows) {
                        if (query.ou == "*" ) {
                            subdata = await dbs.query(`select mst "mst", code "code", name "name" from s_ou where taxc = '${row.STAX}' `)
                        }
                        else {
                            subdata = await dbs.query(`select mst "mst", code "code", name "name" from s_ou where id=:id`, [query.ou])
                        }
                       let res = subdata.rows
                        switch (row.STATUS) {
                            case 2:
                                row.STATUS = "Chờ duyệt"
                                break
                            case 3:
                                row.STATUS = "Đã duyệt"
                                break
                            case 4:
                                row.STATUS = "Đã hủy"
                                break
                            case 6:
                                row.STATUS = "Chờ hủy"
                                break
                        }
                        row.submst = res[0].STAX
                        row.subcode = res[0].code
                        row.subname = res[0].name
                        row.index = ++i
                    }
                    switch (status) {
                        case '2':
                            status = "Chờ duyệt"
                            break
                        case '3':
                            status = "Đã duyệt"
                            break
                        case '4':
                            status = "Đã hủy"
                            break
                        case '6':
                            status = "Chờ hủy"
                            break
                    }
                    json = { table: rows, stax: smst, ndate : ndate , nmonth : nmonth, nyear : nyear, sname : sname, tstatus : status,fd: fr, td: to }
                } else if (report == 8) {
                    fn = "temp/BChuyhoadon_BVB.xlsx"
                    where = "where idt between :1 and :2 and stax=:3"
                    binds = [fd, td, taxc]
                    let ij = 4
                    if (type !== "*") {
                        where += ` and type=:${ij++}`
                        binds.push(type)
                    }
    
                    where += where_ft
                    where += ` and status = 4`
                    let taxsql = `case when a.adjtyp in(1,2) then a.adjdes else a.doc.cancel.rea end`
                    sql = `select TO_CHAR(idt,'${mfd}') "idt",form "form",serial "serial",seq "seq",btax "btax",bname "bname",buyer "buyer",a.doc.canceller canceller,TO_CHAR(cdt,'${mfd}') "cdt",ou "ou",stax "stax",cde "cde", ${taxsql} "reason", uc "uc" from s_inv a ${where} order by idt,form,serial,seq`
                    
                    result = await dbs.query(sql, binds)
                    rows = result.rows
                    let i = 0
                    for (const row of rows) {
                        row.index = ++i
                        try {
                            let invou = await ous.obid(row.ou)
                            row.ou = invou.name
                        }
                        catch (err) {
                            row.ou = ``
                        }
                        row.bname = (row.bname != null && row.bname != "") ? row.bname : row.buyer
                    }
    
                    json = { stax: taxc, sname: token.on, sfaddr: sfaddr, fd: fr, td: to, totalinv: i, table: rows }
                }
                else if (report == 9) {
                    fn = "temp/BCDCHD_BVB.xlsx"
                    where = "where idt between :1 and :2 and stax=:3"
                    binds = [fd, td, taxc]
                    let ij = 4
                    if (type !== "*") {
                        where += ` and type=:${ij++}`
                        binds.push(type)
                    }
    
                    where += where_ft
                    where += ` and adjtyp = 2 and status = 3`
    
                    sql = `select pid "pid", TO_CHAR(idt,'${mfd}') "idt",form "form",serial "serial",seq "seq",btax "btax",bname "bname",buyer "buyer",a.doc.canceller canceller,TO_CHAR(dt,'${mfd}') "cdt",ou "ou",stax "stax",sumv "sumv",vatv "vatv",sec "sec",baddr "baddr",uc "uc", case when adjtyp in (1,2) then adjdes else note end "adjdes", TO_CHAR(dt,'${mfd}') "dt" from s_inv a ${where} order by idt,form,serial,seq`
                    result = await dbs.query(sql, binds)
                    rows = result.rows
                    let rowbc = []
                    let i = 0
    
                    //Lấy danh sách các hóa đơn điều chỉnh
                    for (const row of rows) {
                        try {
                            let invou = await ous.obid(row.ou)
                            row.ou = invou.name
                        }
                        catch (err) {
                            row.ou = ``
                        }
    
                        let sqlu = `SELECT name "name" FROM s_user where id = :1`  
                        
                        let resultu = await dbs.query(sqlu, [row.uc])
                        let rowsu = resultu.rows
                        if (rowsu.length != 0) {
                            row.uc = rowsu[0].name
                        }
    
                        row.bname = (row.bname != null && row.bname != "") ? row.bname : row.buyer
                        let vpid = String(row.pid).split(",")
                        //Tìm hóa đơn gốc bị điều chỉnh
                        for (const pid of vpid) {
                            let sqlroot = `select TO_CHAR(idt,'${mfd}') "idt",form "form",serial "serial",seq "seq",sumv "sumv",vatv "vatv" from s_inv where id = :1`
                            let resultroot = await dbs.query(sqlroot, [pid])
                            let rowsroot = resultroot.rows
                            if (rowsroot.length > 0) {
                                i++
                                let rowdc = { pid: row.pid, idt: row.idt, form: row.form, serial: row.serial, seq: row.seq, btax: row.btax, bname: row.bname, buyer: row.buyer, canceller: row.canceller, cdt: row.cdt, ou: row.ou, stax: row.stax, sumv: row.sumv, vatv: row.vatv, sec: row.sec, baddr: row.baddr, uc: row.uc, adjdes: row.adjdes, dt: row.dt }
    
                                rowdc.rootform = rowsroot[0].form
                                rowdc.rootserial = rowsroot[0].serial
                                rowdc.rootseq = rowsroot[0].seq
                                rowdc.rootidt = rowsroot[0].idt
                                rowdc.rootsumv = rowsroot[0].sumv
                                rowdc.rootvatv = rowsroot[0].vatv
    
                                if (String(rowdc.adjdes).includes("giảm")) 
                                {
                                    rowdc.sumv = (util.isNumber(rowdc.sumv) ? ((-1)*rowdc.sumv) : 0)
                                    rowdc.vatv = (util.isNumber(rowdc.vatv) ? ((-1)*rowdc.vatv) : 0)
                                } else if (String(rowdc.adjdes).includes("tăng")) 
                                {
                                    rowdc.sumv = (util.isNumber(rowdc.sumv) ? (rowdc.sumv) : 0)
                                    rowdc.vatv = (util.isNumber(rowdc.vatv) ? (rowdc.vatv) : 0)
                                }
    
                                rowdc.index = i
                                rowbc.push(rowdc)
                            }
    
                        }
                    }
    
                    json = { stax: taxc, sname: token.on, sfaddr: sfaddr, fd: fr, td: to, totalinv: i, table: rowbc }
                } else if (report == 10) {
                    fn = "temp/BCHDthaythe_BVB.xlsx"
                    where = "where idt between :1 and :2 and stax=:3"
                    binds = [fd, td, taxc]
                    let ij = 4
                    if (type !== "*") {
                        where += ` and type=:${ij++}`
                        binds.push(type)
                    }
    
                    where += where_ft
                    where += ` and adjtyp = 1 and status = 3`
    
                    sql = `select pid "pid", TO_CHAR(idt,'${mfd}') "idt",form "form",serial "serial",seq "seq",btax "btax",bname "bname",buyer "buyer",a.doc.canceller canceller,TO_CHAR(dt,'${mfd}') "cdt",ou "ou",stax "stax",sumv "sumv",vatv "vatv",sec "sec",baddr "baddr",uc "uc" from s_inv a ${where} order by idt,form,serial,seq`
                    result = await dbs.query(sql, binds)
                    rows = result.rows
                    let i = 0
                    for (const row of rows) {
                        row.index = ++i
                        try {
                            let invou = await ous.obid(row.ou)
                            row.ou = invou.name
                        }
                        catch (err) {
                            row.ou = ``
                        }
    
                        let sqlu = `SELECT name "name" FROM s_user where id = :1`  
                        
                        let resultu = await dbs.query(sqlu, [row.uc])
                        let rowsu = resultu.rows
                        if (rowsu.length != 0) {
                            row.uc = rowsu[0].name
                        }
    
                        row.bname = (row.bname != null && row.bname != "") ? row.bname : row.buyer
                        let sqlroot = `select TO_CHAR(idt,'${mfd}') "idt",form "form",serial "serial",seq "seq",sumv "sumv",vatv "vatv" from s_inv where id = :1`
                        let resultroot = await dbs.query(sqlroot, [row.pid])
                        let rowsroot = resultroot.rows
                        if (rowsroot.length > 0) {
                            row.rootform = rowsroot[0].form
                            row.rootserial = rowsroot[0].serial
                            row.rootseq = rowsroot[0].seq
                            row.rootidt = rowsroot[0].idt
                            row.rootsumv = rowsroot[0].sumv
                            row.rootvatv = rowsroot[0].vatv
                        }
                    }
    
                    json = { stax: taxc, sname: token.on, sfaddr: sfaddr, fd: fr, td: to, totalinv: i, table: rows }
                } 
                else if (report == 20) {
                    let filter = req.body, keys = Object.keys(filter), kar = ["serial", "form", "type", "ou"], val,name,fadd,taxc,whereHDDT,bindHDDT,where2
                    let sname = await dbs.query('select name, fadd,taxc from s_ou where id=:1', [filter.ou])
                    if (sname.rows.length > 0) name = sname.rows[0].NAME,fadd=sname.rows[0].FADD,taxc=sname.rows[0].TAXC
                    fn = "temp/08BKHDXTNEW.xlsx"
                    where = "where idt between :1 and :2 and stax=:3 "
                    whereHDDT = "where NGAY_THU_PHI between :1 and :2 and MST=:3"
                    binds = [fd, td, taxc]
                    bindHDDT = [fd, td, taxc]
                    for (const key of kar) {
                        if (keys.includes(key) && key.includes("serial")) {
                            val = filter[key]
                            if (val != " ") {
                                where += ` and serial = :serial `
                                whereHDDT +=` and SERIAL = :serial`
                                binds.push(serial)
                                bindHDDT.push(serial)
                            }
                        }
                        else if (keys.includes(key) && key.includes("form")) {
                            val = filter[key]
                            if (val && val != " ") {
                                where += ` and form = :form `
                                whereHDDT +=` and FORM = :form`
                                binds.push(form)
                                bindHDDT.push(form)
                            }
                        } else if (keys.includes(key) && key.includes("type")) {
                            val = filter[key]
                            if (val !== "*") {
                                where += ` and type=:type`
                                binds.push(type)
                            }
                        } else if (keys.includes(key) && key.includes("ou")) {
                            val = filter[key]
                            if (val!=("*")) {
                                where += ` and ou=:ou`
                                whereHDDT +=` and OU = :ou`
                                bindHDDT.push(Number(ou))
                                binds.push(Number(ou))
                            }
                        }
                    }
                    where2 = where + ` AND a.STATUS IN(4)`
                    where += ` AND a.STATUS NOT IN(4)`
                    sql3 = `SELECT
                    TRANS_NO "c4",
                    MA_GD "c1",
                    NGAY_GD "c0",
                    CHI_NHANH_CAP_1 "code2",
                    CN_PGD "code",
                    SO_HOA_DON "seq",
                    TO_CHAR(NGAY_THU_PHI, ' DD / MM / YYYY') "IDT",
                    FORM "form",
                    SERIAL "serial",
                    SEQ "seq",
                    TIEN_TRUOC_THUE_QUY_DOI "sumv",
                    THUE_SUAT "vat",
                    TIEN_THUE_QUY_DOI "vatv",
                    CIF "bcode",
                    SO_TK_KH "bacc",
                    TEN_KH "bname",
                    DIA_CHI_KH "baddr",
                    MST_KH "btax",
                    NOI_DUNG "itemname",
                    LOAI_PHI "c6",
                    PROCESS_STATUS "process_status"
                    FROM
                        BVB_INTEGRATION.BVB_HDDT
                    ${whereHDDT}
                    ORDER BY
                        NGAY_THU_PHI,
                        FORM,
                        SERIAL`
                    sql2 = `SELECT code from s_ou where id=:1`
                    sql = `SELECT * 
                    FROM (
                        SELECT
                            a.id "id",
                            TO_CHAR(a.idt, ' DD / MM / YYYY') "IDT",
                            a.form "FORM",
                            a.baddr "baddr",
                            a.serial "SERIAL",
                            a.seq "SEQ",
                            a.sec "sec",
                            b.pid "pid",
                            b.code "code",
                            a.btax "btax",
                            a.bacc "bacc",
                            a.bname "bname",
                            a.buyer "buyer",
                            a.sumv "sumv",
                            a.vatv "vatv",
                            a.STATUS "status",
                            a.note || '-' || a.cde || '-' || a.adjdes "note",
                            1 AS "factor"
                        FROM
                            s_inv a
                        INNER JOIN s_ou b ON
                            a.OU = b.ID
                        ${where}
                    UNION ALL
                        SELECT
                            a.id "id",
                            TO_CHAR(a.idt, ' DD / MM / YYYY') "IDT",
                            a.form "FORM",
                            a.baddr "baddr",
                            a.serial "SERIAL",
                            a.seq "SEQ",
                            a.sec "sec",
                            b.pid "pid",
                            b.code "code",
                            a.btax "btax",
                            a.bacc "bacc",
                            a.bname "bname",
                            a.buyer "buyer",
                            a.sumv "sumv",
                            a.vatv "vatv",
                            a.STATUS "status",
                            a.note || '-' || a.cde || '-' || a.adjdes "note",
                            1 AS "factor"
                        FROM
                            s_inv a
                        INNER JOIN s_ou b ON
                            a.OU = b.ID
                        ${where2}
                    UNION ALL
                        SELECT
                            a.id "id",
                            TO_CHAR(a.idt, ' DD / MM / YYYY') "IDT",
                            a.form "FORM",
                            a.baddr "baddr",
                            a.serial "SERIAL",
                            a.seq "SEQ",
                            a.sec "sec",
                            b.pid "pid",
                            b.code "code",
                            a.btax "btax",
                            a.bacc "bacc",
                            a.bname "bname",
                            a.buyer "buyer",
                            a.sumv "sumv",
                            a.vatv "vatv",
                            a.STATUS "status",
                            a.note || '-' || a.cde || '-' || a.adjdes "note",
                            -1 AS "factor"
                        FROM
                            s_inv a
                        INNER JOIN s_ou b ON
                            a.OU = b.ID
                        ${where2}) t
                    ORDER BY
                        t.idt,
                        t.form,
                        t.serial,
                        t.seq`
                    result = await dbs.query(sql, binds)
                    rows = result.rows
                    let result3 = await dbs.query(sql3, bindHDDT)
                    let rows3 = result3.rows
                    let i = 0, a
                    let rowbc = []
                    for (const row of rows) {
                        let binds2 = [row.pid]
                        let result2 = await dbs.query(sql2, binds2)
                        let rows2 = result2.rows
                        
                        let doc = await rbi(row.id)
                        let items = doc.items
                        for (const item of items) {
                            let rowitem = { ...row }, vrt
                            rowitem.index = ++i
                            rowitem.sumv = item.amount * row.factor
                            rowitem.vatv = row.vatv * row.factor
                            if (item.vrt == -1) {
                                vrt = `Không chịu thuế`
                            } else if (item.vrt == -2) {
                                vrt = `Không kê khai nộp thuế`
                            } else vrt = item.vrt
                            rowitem.vat = vrt
                            rowitem.itemname = item.name
                            if (item.c0) rowitem.c0 = moment(item.c0).format('DD / MM / YYYY')
                            rowitem.c4 = item.c4
                            rowitem.c6 = item.c6
                            rowitem.bcode = doc.bcode
                            rowitem.c1 = item.c1
                            if (row.pid & row.pid != 1) {
                                rowitem.code2 = row.code
                                for (const row2 of rows2) {
                                    rowitem.code = row2.CODE
                                }
                            }
                            rowbc.push(rowitem)
                        }
                        // }
                    }
                    a = i
                    for (const row of rows3) {
                        row.index = ++a
                        if(row.c0) row.c0 = moment(row.c0).format('DD / MM / YYYY')
                        rowbc.push(row)
                    }
                    json = { saddr: fadd, stax: taxc, sname: name, fd: fr, td: to, rt: rt, tn: tn, table: rowbc }
                }
                else if (report == 21) {
                    let filter = req.body, keys = Object.keys(filter), kar = ["serial", "form", "type", "ou"], val,whereh,sql2
                    fn = "temp/08BKHDKH_BVB.xlsx"
                    where = "where idt between :1 and :2 and stax=:3"
                    binds = [fd, td, taxc2]
                    for (const key of kar) {
                        if (keys.includes(key) && key.includes("serial")) {
                            val = filter[key]
                            if (val != " ") {
                                where += ` and serial = :serial `
                                binds.push(serial)
                            }
                        }
                        else if (keys.includes(key) && key.includes("form")) {
                            val = filter[key]
                            if (val && val != " ") {
                                where += ` and form = :form `
                                binds.push(form)
                            }
                        } else if (keys.includes(key) && key.includes("type")) {
                            val = filter[key]
                            if (val !== "*") {
                                where += ` and type=:type`
                                binds.push(type)
                            }
                        } else if (keys.includes(key) && key.includes("ou")) {
                            val = filter[key]
                            if (val!=("*")) {
                                where += ` and ou=:ou`
                                binds.push(Number(ou))
                            }
                        }
                    }
                    whereh = where + ` and a.STATUS in (4)`
                    where +=` and a.STATUS in (3)`
                    sql = `SELECT
                    a.id "id",
                    TO_CHAR(a.idt, ' DD / MM / YYYY') "idt",
                    a.form "form",
                    a.baddr "baddr",
                    a.serial "serial",
                    a.seq "seq",
                    b.pid "pid",
                    b.code "code",
                    a.btax "btax",
                    a.btax || '.0' "btaxgroup",
                    a.bname "bname",
                    a.STATUS "status",
                    a.buyer "buyer",
                    a.sumv "sumv",
                    a.vatv "vatv",
                    a.vat "vat",
                    a.note || ' ' || a.cde || ' ' || a.adjdes "note",
                    1 AS "factor",
                    NULL "sumsv",
                    NULL "sumvt",
                    NULL "sumva"
                FROM
                    s_inv a
                INNER JOIN s_ou b ON
                    a.OU = b.ID
                ${where}
                UNION ALL
                SELECT
                    a.id "id",
                    TO_CHAR(a.cdt, ' DD / MM / YYYY') "idt",
                    a.form "form",
                    a.baddr "baddr",
                    a.serial "serial",
                    a.seq "seq",
                    b.pid "pid",
                    b.code "code",
                    a.btax "btax",
                    a.btax || '.0' "btaxgroup",
                    a.bname "bname",
                    a.STATUS "status",
                    a.buyer "buyer",
                    a.sumv "sumv",
                    a.vatv "vatv",
                    a.vat "vat",
                    a.note || ' ' || a.cde || ' ' || a.adjdes "note",
                    1 AS "factor",
                    NULL "sumsv",
                    NULL "sumvt",
                    NULL "sumva"
                FROM
                    s_inv a
                INNER JOIN s_ou b ON
                    a.OU = b.ID
                ${whereh}
                UNION ALL
                SELECT
                    a.id "id",
                    TO_CHAR(a.cdt, ' DD / MM / YYYY') "idt",
                    a.form "form",
                    a.baddr "baddr",
                    a.serial "serial",
                    a.seq "seq",
                    b.pid "pid",
                    b.code "code",
                    a.btax "btax",
                    a.btax || '.0' "btaxgroup",
                    a.bname "bname",
                    a.STATUS "status",
                    a.buyer "buyer",
                    a.sumv "sumv",
                    a.vatv "vatv",
                    a.vat "vat",
                    a.note || ' ' || a.cde || ' ' || a.adjdes "note",
                    -1 AS "factor",
                    NULL "sumsv",
                    NULL "sumvt",
                    NULL "sumva"
                FROM
                    s_inv a
                INNER JOIN s_ou b ON
                    a.OU = b.ID
                ${whereh}
                UNION ALL
                SELECT
                    NULL "id",
                    NULL "idt",
                    NULL "form",
                    NULL "baddr",
                    NULL "serial",
                    NULL "seq",
                    NULL "pid",
                    NULL "code",
                    t.btax "btax",
                    btax || '.1' "btaxgroup",
                    NULL "bname",
                    NULL "status",
                    NULL "buyer",
                    NULL "sumv",
                    NULL "vatv",
                    NULL "vat",
                    NULL "note",
                    NULL "factor",
                    SUM(t.sumv) "sumsv",
                    SUM(t.vatv) "sumvt",
                    SUM(t.vat) "sumva"
                FROM
                    (
                    SELECT
                        a.id "id",
                        TO_CHAR(a.idt, ' DD / MM / YYYY') "idt",
                        a.form "form",
                        a.baddr "baddr",
                        a.serial "serial",
                        a.seq "seq",
                        b.pid "pid",
                        b.code "code",
                        a.btax "BTAX",
                        a.btax || '.0' "btaxgroup",
                        a.bname "bname",
                        a.STATUS "status",
                        a.buyer "buyer",
                        a.sumv "SUMV",
                        a.vatv "VATV",
                        a.vat "VAT",
                        a.note || ' ' || a.cde || ' ' || a.adjdes "note",
                        1 AS "factor",
                        NULL "sumsv",
                        NULL "sumvt",
                        NULL "sumva"
                    FROM
                        s_inv a
                    INNER JOIN s_ou b ON
                        a.OU = b.ID
                    ${where}
                UNION ALL
                    SELECT
                        a.id "id",
                        TO_CHAR(a.cdt, ' DD / MM / YYYY') "idt",
                        a.form "form",
                        a.baddr "baddr",
                        a.serial "serial",
                        a.seq "seq",
                        b.pid "pid",
                        b.code "code",
                        a.btax "BTAX",
                        a.btax || '.0' "btaxgroup",
                        a.bname "bname",
                        a.STATUS "status",
                        a.buyer "buyer",
                        a.sumv "SUMV",
                        a.vatv "VATV",
                        a.vat "VAT",
                        a.note || ' ' || a.cde || ' ' || a.adjdes "note",
                        1 AS "factor",
                        NULL "sumsv",
                        NULL "sumvt",
                        NULL "sumva"
                    FROM
                        s_inv a
                    INNER JOIN s_ou b ON
                        a.OU = b.ID
                    ${whereh}
                UNION ALL
                    SELECT
                        a.id "id",
                        TO_CHAR(a.cdt, ' DD / MM / YYYY') "idt",
                        a.form "form",
                        a.baddr "baddr",
                        a.serial "serial",
                        a.seq "seq",
                        b.pid "pid",
                        b.code "code",
                        a.btax "BTAX",
                        a.btax || '.0' "btaxgroup",
                        a.bname "bname",
                        a.STATUS "status",
                        a.buyer "buyer",
                        a.sumv "SUMV",
                        a.vatv "VATV",
                        a.vat "VAT",
                        a.note || ' ' || a.cde || ' ' || a.adjdes "note",
                        -1 AS "factor",
                        NULL "sumsv",
                        NULL "sumvt",
                        NULL "sumva"
                    FROM
                        s_inv a
                    INNER JOIN s_ou b ON
                        a.OU = b.ID
                    ${whereh}) t
                GROUP BY
                    t.btax
                ORDER BY
                    10`
                    result = await dbs.query(sql, binds)
                    rows = result.rows
                    let i = 0, bnamea
                    let rowbc = []
                    let name, sumvat = 0, sumvatv = 0, sumsumv = 0
                    sql2 = ` SELECT code "code" from s_ou where id=:1 `
                    for (const row of rows) {
                        let result2 = await dbs.query(sql2, [row.pid])
                        let rows2 = result2.rows
                        row.index = ++i
                        bnamea = row.bname
                        if (row.id) {
                            let doc = await rbi(row.id)
                            row.bcode = doc.bcode
                        }
                        if(row.status == 3){
                            row.status =`Đã Duyệt`
                        }else if(row.status == 4){
                            row.status =`Đã Huỷ`
                        }
                        if (rows2.pid & rows2.pid != 1) {
                            row.code2 = row.code
                            for (const row2 of rows2) {
                                row.code = row2.CODE
                            }
                        }
                        row.sumv = row.sumv * row.factor
                        row.vatv = row.vatv * row.factor
                        row.vat = row.vat * row.factor
                        if (row.btaxgroup == '.1' || row.btaxgroup == `${row.btax}.1`) {
                            row.sumv = row.sumsv
                            row.vatv = row.sumvt
                            row.vat = row.sumva
                            sumvat += row.sumva
                            sumsumv += row.sumsv
                            sumvatv += row.sumvt
                            row.bname = `Tổng CIF ${name}`
                            row.index = ''
                            row.btax = ''
                            i = 0
                        } else name = row.bname

                        rowbc.push(row)
                    }
                    json = { sumsumv: sumsumv, sumvatv: sumvatv, sumvat: sumvat, saddr: sfaddr, sname: token.on, stax: taxc, fd: fr, td: to, rt: rt, tn: tn, table: rowbc }
                }
            } else {
                //Search rieng reportext  (report extension) is exist
                if (config.ent == "hdb") {
                    let ou = query.ou, vrt = query.vrt, prof = query.prof, cuscode = query.cuscode, cusacc = query.cusacc, idt = query.idt, mst = query.mst, report = query.report
                    let sql, result, binds = [], where = "", order
                    if (["1", "2"].includes(report)) {
                        order = ` order by ${report == 1 ? 'vi.doc.branchCode' : 'esf.BRANCH_CODE'},  ${report == 1 ? 'vi.doc.serial,vi.doc.seq,' : ''}${report == 1 ? 'vi.doc.items.c0,' : 'esf.FKEY,'}${report == 1 ? 'vi.idt' : 'esf.process_date'}`
                        let prof_map = [
                            "Thu lãi",
                            "Thu dịch vụ",
                            "Bán ngoại tệ",
                            "Thu từ KD ngoại tệ",
                            "Thu từ KD chứng khoán",
                            "Thu khác",
                            "Hàng khuyến mãi",
                            "Thu nhập từ góp vốn mua cổ phần",
                        ]
                        let frequency_map = [
                            "Ngày",
                            "1 lần/tháng",
                            "2 lần/tháng",
                            "3 lần/tháng"
                        ]
                        const iStatus = [
                            "Chờ cấp số",
                            "Chờ duyệt",
                            "Đã duyệt",
                            "Đã hủy",
                            "Đã gửi",
                            "Chờ hủy",
                        ]
                        // Dieu kien where goc
                        if (report == 1) {
                            where = " WHERE esf.FORM = vi.FORM AND esf.SERIAL = vi.SERIAL AND esf.INV_NO = vi.SEQ AND esf.TAX_CODE = vi.STAX AND vi.STATUS not in (1, 2) " // AND vi.STATUS in (3, 4) 
                        } else if (report == 2) {
                            let seaDate = moment(idt,config.dtf), eoy = seaDate.endOf("year").toDate(), soy = seaDate.startOf("year").toDate()
                            binds.push(eoy, soy)
                            where = `
                             WHERE (not exists (select 1 FROM HDB_INTEGRATION.EINV_SOURCE_FILE esf, s_inv vi
                                WHERE esf.FORM = vi.FORM AND esf.SERIAL = vi.SERIAL AND esf.INV_NO = vi.SEQ AND esf.TAX_CODE = vi.STAX
                                and vi.IDT between :eoy and :soy
                                AND ROWNUM = 1 AND vi.STATUS IN (1, 2))) `
                        }
                        // where += `and esf.tax_code in (select distinct mst from s_ou start with id=:id connect by prior id=pid) `
                        if (report == 1) {
                            where += ` AND exists (SELECT 1 FROM s_ou o where o.mst = esf.tax_code START WITH o.id = :ou CONNECT BY PRIOR o.id = o.pid) `
                            binds.push(token.ou)
                        }

                        //Them dieu kien where tuong ung tuy bao cao, 1 la bao cao 1, 2 la bao cao 2

                        //1: 2:esf.BRANCH_CODE
                        if (ou) {
                            let c = ou.split(","), str = ""
                            for (let i of c) {
                                str += `:c${i},`
                                binds.push(i)
                            }
                            where += ` and ${report == 1 ? "JSON_VALUE(vi.doc,'$.branchCode')" : "esf.BRANCH_CODE"} in (${str.slice(0, -1)}) `
                        }
                        //1:HDB_INTEGRATION.EINV_SOURCE_FILE.CUST_CODE  2:HDB_INTEGRATION.EINV_SOURCE_FILE.CUST_CODE
                        if (cuscode) {
                            where += ` and esf.CUST_CODE like :cuscode `
                            binds.push(`%${cuscode}%`)
                        }
                        //1:vi.bacc  2:HDB_INTEGRATION.EINV_SOURCE_FILE.CUST_CODE
                        if (cusacc) {
                            where += ` and ${report == 1 ? "vi.bacc" : "JSON_VALUE(esf.FILE_CONTENT,'$.bacc')"} like :cusacc `
                            binds.push(`%${cusacc}%`)
                        }
                        //1:s_inv.DOC.c0 2:HDB_INTEGRATION.EINV_SOURCE_FILE.FILE_CONTENT.acctype
                        if (prof) {
                            where += ` and ${report == 1 ? "JSON_VALUE(vi.doc,'$.c0')" : "JSON_VALUE(esf.FILE_CONTENT,'$.acctype')"}=:prof `
                            binds.push(prof)
                        }
                        //1:s_inv.DOC   2:HDB_INTEGRATION.EINV_SOURCE_FILE.FILE_CONTENT.items.vatr
                        if (vrt) {
                            where += ` and ${report == 1 ? "JSON_VALUE(vi.doc,'$.items.vrn')" : "JSON_VALUE(esf.FILE_CONTENT,'$.items.vatr')"}=:vrt `
                            binds.push(report == 1 ? vrt : vrt.replace("%", ""))
                        }
                        //1:s_inv
                        if (mst) {
                            let a = mst.split(","), str = "", x = 1
                            a = Array.from(new Set(a))
                            for (let i of a) {
                                str += `:a${x++},`
                                binds.push(`${i}`)
                            }
                            where += ` and esf.tax_code in (${str.slice(0, -1)}) `
                        }
                        if (idt) {
                            where += ` and ${report == 1 ? 'trunc(vi.idt)' : 'trunc(esf.process_date)'}=trunc(:idt) `
                            binds.push(new Date(idt))
                        }

                        // set gia tri lay ten file  va cau lenh sql 
                        if (report == 1) {
                            fn = "temp/hdb/invdetail.xlsx"
                            sql = `select distinct 
                            vi.doc.branchCode as "branchcode", vi.doc.serial "serial", vi.doc.seq "seq",
                            vi.idt "idt", vi.doc.items.c0 "itc0", vi.doc.items.c1 "itc1", 
                            vi.doc.items.amount "amount", vi.doc.items.vrn "vrn", vi.doc.items.vat "itc2", vi.doc.items.total "itc3", vi.curr "curr", 
                            vi.doc.c0 "dc0", esf.CUST_CODE "cuscode", vi.bacc "bacc", vi.bname "bname", 
                            CASE WHEN esf.cust_code is not null THEN (select c0 from s_org g where g.code=esf.cust_code) ELSE '' END "frequency", 
                            vi.status "status" 
                            from HDB_INTEGRATION.EINV_SOURCE_FILE esf, s_inv vi 
                            ${where}
                            `
                        } else if (report == 2) {
                            fn = "temp/hdb/transwait.xlsx"
                            sql = `select esf.FILE_CONTENT "doc", esf.BRANCH_CODE as "branchcode",esf.FKEY "fkey", esf.process_date "processdate", JSON_VALUE(esf.FILE_CONTENT, '$.items.amount') "amount", JSON_VALUE(esf.FILE_CONTENT, '$.items.vatr') "vrn", 
                            JSON_VALUE(esf.FILE_CONTENT, '$.items.vata') "itc2", JSON_VALUE(esf.FILE_CONTENT, '$.items.total') "itc3", JSON_VALUE(esf.FILE_CONTENT, '$.curr') "curr", JSON_VALUE(esf.FILE_CONTENT, '$.acctype') "dc0", 
                            esf.CUST_CODE "cuscode", JSON_VALUE(esf.FILE_CONTENT, '$.bacc') "bacc",
                            CASE WHEN esf.cust_code is not null THEN g.name ELSE JSON_VALUE(esf.FILE_CONTENT, '$.bname') END "bname", g.C0 "frequency"
                            FROM HDB_INTEGRATION.EINV_SOURCE_FILE esf
                            LEFT JOIN S_ORG g on esf.cust_code = g.code
                            LEFT JOIN HDB_INTEGRATION.EINV_SOURCE_FILE_CONTENT esfc on esf.FILE_CONTENT_ID = esfc.ID
                            ${where} 
                            `
                        }
                        sql += order
                        result = await dbs.query(sql, binds)
                        let rows = result.rows, arr = []
                        let aCurr = {}

                        // phan tich du lieu, các dữ liệu này là cứng bên  hdb nên mình phải phân tích
                        for (let i of rows) {
                            // i.vrn = [...new Set(i.vrn.replace(/\[|\]|"/g,"").split(","))]                            
                            // if(vrt && !i.vrn.includes(vrt)) continue
                            // i.vrn = i.vrn.join(",")
                            if (!aCurr[i.curr]) aCurr[i.curr] = { sAmt: 0, sc2: 0, sc3: 0 }
                            i.dc0 = prof_map[parseInt(i.dc0) - 1]
                            i.frequency = i.frequency ? frequency_map[parseInt(i.frequency)] : ''
                            i.status = iStatus[i.status - 1]
                            i.vrn = i.vrn && i.vrn[0] != "[" ? i.vrn == "-1" ? "Không chịu thuế GTGT" : i.vrn.includes("%") ? i.vrn : i.vrn + "%" : i.vrn
                            if (i.processdate) i.processdate = moment(i.processdate).format(mfd)
                            if (i.idt) i.idt = moment(i.idt).format(mfd)
                            if (i.itc0 && i.itc0[0] == "[") {
                                //is Array
                                let aitc0 = JSON.parse(i.itc0)
                                let aitc1 = JSON.parse(i.itc1)
                                let aitc2 = JSON.parse(i.itc2)
                                let aitc3 = JSON.parse(i.itc3)
                                let aamount = JSON.parse(i.amount)
                                let avrn = i.vrn ? JSON.parse(i.vrn) : ""
                                for (let j = 0; j < aitc0.length; j++) {
                                    let itc2=aitc2?parseNumber(aitc2[j], i.curr):0 , itc3= aitc3?parseNumber(aitc3[j], i.curr):0, amount=aamount?parseNumber(aamount[j],i.curr):0
                                    aCurr[i.curr].sAmt += amount
                                    aCurr[i.curr].sc2 += itc2
                                    aCurr[i.curr].sc3 += itc3
                                    // scurr+=parseInt(i.curr)
                                    let obj = { branchcode: i.branchcode, serial: i.serial, seq: i.seq, idt: i.idt, itc0: aitc0[j], itc1: aitc1[j], itc2: itc2, itc3: itc3, amount: amount, vrn: avrn[j], curr: i.curr, dc0: i.dc0, cuscode: i.cuscode, bacc: i.bacc, bname: i.bname, frequency: i.frequency, status: i.status }
                                    arr.push(obj)
                                }
                            } else if (i.doc) {
                                let doc=JSON.parse(i.doc), items = doc.items
                                if(items.length>0 ) {
                                    for(let e of items) {
                                        e.vatr = e.vatr ? (e.vatr == "-1" ? "Không chịu thuế GTGT" : (e.vatr+'').includes("%")?e.vatr:e.vatr+"%") : ""
                                        e.amount = e.amount ? parseNumber(e.amount, i.curr) : 0
                                        e.vata = e.vata ? parseNumber(e.vata, i.curr) : 0
                                        e.total = e.total ? parseNumber(e.total, i.curr) : 0
                                        aCurr[i.curr].sAmt += parseNumber(e.amount, i.curr)
                                        aCurr[i.curr].sc2 += parseNumber(e.vata, i.curr)
                                        aCurr[i.curr].sc3 += parseNumber(e.total, i.curr)
                                        let obj = { branchcode: i.branchcode, fkey: i.fkey, processdate:i.processdate, amount: e.amount, vrn:e.vatr, itc2:e.vata, itc3:e.total, curr: i.curr, dc0: i.dc0, cuscode: i.cuscode, bacc: i.bacc, bname: i.bname, frequency: i.frequency }
                                        arr.push(obj)
                                    }
                                }
                            } else {
                                if (i.itc1) i.itc1 = moment(i.itc1).format(mfd)
                                i.amount = i.amount ? parseNumber(i.amount, i.curr) : 0
                                i.itc2 = i.itc2 ? parseNumber(i.itc2, i.curr) : 0
                                i.itc3 = i.itc3 ? parseNumber(i.itc3, i.curr) : 0
                                aCurr[i.curr].sAmt += parseNumber(i.amount, i.curr)
                                aCurr[i.curr].sc2 += parseNumber(i.itc2, i.curr)
                                aCurr[i.curr].sc3 += parseNumber(i.itc3 ,i.curr)
                                arr.push(i)
                            }
                        }
                        let aSum = []
                        for (let i in aCurr) {
                            aCurr[i].curr = i
                            // aCurr[i].sAmt = numeral(aCurr[i].sAmt).format('0,0.[00]')
                            // aCurr[i].sc2 = numeral(aCurr[i].sc2).format('0,0.[00]')
                            // aCurr[i].sc3 = numeral(aCurr[i].sc3).format('0,0.[00]')
                            aSum.push(aCurr[i])
                        }
                        if (aSum.length == 0) aSum.push({ sAmt: 0, sc2: 0, sc3: 0, curr: "VND" })
                        json = { date: moment(idt).format(mfd), sum: aSum, table: arr }
                    } else {
                        // TH các report 3,4,5 (3:cancel_report, 4:BC26_SoLuong, 5:BC26_SoLuong_tranform)
                        if (report == 3) {
                            fn = "temp/hdb/invdetail.xlsx"
                        } else if (report == 4) {
                            fn = "temp/hdb/bc26sl.xlsx"
                        } else if (report == 5) {
                            fn = "temp/hdb/bc26sl_transform.xlsx"
                        } else {
                            throw new Error("Loại báo cáo không tồn tại \n (Report Type is null)")
                        }
                        json = {}
                    }
                }
            }
            console.time(`Chay ket xuat excel bao cao ${token.uid} - ${token.u} - ${token.taxc} - ${ndate}`)
            const file = path.join(__dirname, "..", "..", "..", fn)
            const xlsx = fs.readFileSync(file)
            const template = new xlsxtemp(xlsx)
            template.substitute(1, json)
            console.timeEnd(`Chay ket xuat excel bao cao ${token.uid} - ${token.u} - ${token.taxc} - ${ndate}`)
            res.end(template.generate(), "binary")
        } catch (err) {
            next(err)
        }
    }
}


module.exports = Service;
