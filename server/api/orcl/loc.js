"use strict"
const util = require('../util')
const dbs = require("./dbs")
const config = require("../config")
const logger4app = require("../logger4app")
const logging = require("../logging")
const Service = {
    get: async (req, res, next) => {
        try {
            const sql = `select id "id",name "name",pid "pid" from s_loc where id like :1 order by id`
            const result = await dbs.query(sql, [`${req.params.pid}%`])
            //const rows= JSON.parse(JSON.stringify(result[0])) 
            const rows = result.rows
            let arr = [], obj
            for (const row of rows) {
                if (row.id.length <= 5) row.data = []
                if (!row.pid) arr.push(row)
                else {
                    obj = util.findDFS(arr, row.pid)
                    if (obj) obj.data.push(row)
                }
            }
            res.json(arr)
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            let body = req.body, binds, sql, result, operation = body.webix_operation, id
            switch (operation) {
                case "update":
                    sql = "update s_loc set name=:1, pid=:2 where id=:3"
                    binds = [body.name, body.parent, body.id]
                    break
                case "insert":
                    const pid = body.p.id, count = body.p.$count
                    id = `${pid}${(2 * count + 1)}`
                    sql = "insert into s_loc (id, name, pid) values (:id,:name,:pid) RETURN id INTO :id2"
                    binds = { id: id, name: body.name, pid: pid, id2: { type: 2010, dir: 3003 } }
                    break
                case "delete":
                    sql = "delete s_loc where id=:1"
                    binds = [body.id]
                    break
                default:
                    throw new Error(`${operation} là hoạt động không hợp lệ \n (${operation} is invalid operation`)
            }
            result = await dbs.query(sql, binds)
            const sSysLogs = { fnc_id: 'loc_post', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật thông tin địa bàn, ${body.name} (${operation})`, msg_id: body.id, doc: JSON.stringify(body) };
            logging.ins(req, sSysLogs, next)
            if (operation == "insert") res.json({ id: result.outBinds.id2[0] })
            else res.json(result.rowsAffected)
        }
        catch (err) {
            next(err)
        }
    }

}
module.exports = Service