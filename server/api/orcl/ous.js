"use strict"
const util = require("../util")
const redis = require("../redis")
const sec = require("../sec")
const dbs = require("./dbs")
const config = require("../config")
const logger4app = require("../logger4app")
const logging = require("../logging")
const Service = {
    taxc: async (req, res, next) => {
        try {
            const sql = `select taxc "id",taxc "value" from s_ou where taxc is not null order by taxc`
            const result = await dbs.query(sql)
            res.json(result.rows)
        }
        catch (err) {
            next(err)
        }
    },
    taxn: async (req, res, next) => {
        try {
            const sql = `select taxc "id",taxc||' | '||code||' | '||name "value" from s_ou where taxc is not null order by taxc`
            const result = await dbs.query(sql)
            res.json(result.rows)
        }
        catch (err) {
            next(err)
        }
    },
    taxt: async (req, res, next) => {
        try {
            const sql = `select taxc "id",taxc||' | '||code||' | '||name "value" from s_ou where taxc is not null order by taxc`
            const result = await dbs.query(sql)
            res.json(result.rows)
        }
        catch (err) {
            next(err)
        }
    },
    seq: async (req, res, next) => {
        try {
            const mst = req.params.mst
            const sql = `select count(*) "rc" from s_inv where stax=:1 and status=:2`
            const result = await dbs.query(sql, [mst, 1])
            const row = result.rows[0]
            res.json(row)
        }
        catch (err) {
            next(err)
        }
    },
    get: async (req, res, next) => {
        try {
            const sql = `select id "id",code "code",taxc "taxc",mst "mst",name "name",pid "pid",addr "addr",fadd "fadd",prov "prov",dist "dist",ward "ward",mail "mail",tel "tel",acc "acc",bank "bank",paxo "paxo",taxo "taxo",sign "sign",seq "seq",status "status",level "level",usr "usr",temp "temp",receivermail "receivermail",dsignissuer "dsignissuer",dsignto "dsignto",dsignfrom "dsignfrom",dsignsubject "dsignsubject",dsignserial "dsignserial" from s_ou start with pid is null connect by PRIOR id=pid`
            const result = await dbs.query(sql)
            const rows = result.rows
            let arr = [], obj
            for (const row of rows) {
                if (!row.pid) arr.push(row)
                else {
                    obj = util.findDFS(arr, row.pid)
                    if (obj) {
                        if (!obj.hasOwnProperty('data')) obj.data = []
                        obj.data.push(row)
                    }
                }
            }
            res.json(arr)
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const body = req.body, operation = body.webix_operation
            let binds, sql, result, taxc = body.taxc ? body.taxc : null, sSysLogs = { fnc_id: '', src: config.SRC_LOGGING_DEFAULT, dtl: ``}, dsignfrom = new Date(body.dsignfrom), dsignto = new Date(body.dsignto)
            if (taxc) await redis.del(`OUS.${taxc}`)
            switch (operation) {
                case "update":
                    const seq = body.seq ? body.seq : 1, sign = body.sign ? body.sign : 1, pwd = body.pwd
                    sql = "update s_ou set name=:1,addr=:3,taxc=:4,prov=:5,dist=:6,ward=:7,mail=:8,tel=:9,acc=:10,bank=:11,paxo=:12,taxo=:13,seq=:14,sign=:15,usr=:16,mst=:17,status=:18,code=:19,temp=:20,receivermail=:21,dsignserial=:22,dsignsubject=:23,dsignissuer=:24,dsignfrom=:25,dsignto=:26"
                    binds = [body.name, body.addr, taxc, body.prov, body.dist, body.ward, body.mail, body.tel, body.acc, body.bank, body.paxo, body.taxo, seq, sign, body.usr, body.mst, body.status, body.code, body.temp, body.receivermail, body.dsignserial, body.dsignsubject, body.dsignissuer, dsignfrom,dsignto]
                    if (pwd) {
                        sql += ",pwd=:pwd"
                        binds.push(util.encrypt(pwd))
                    }
                    binds.push(body.id)
                    sql += " where id=:id"
                    sSysLogs = { fnc_id: 'ou_upd', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật thông tin đơn vị id ${body.id}`, msg_id: body.id, doc: JSON.stringify(body)};
                    break
                case "insert":
                    sql = "insert into s_ou (name,pid,mst,code,temp) values(:name,:pid,:mst,:code,:temp) RETURN id INTO :id"
                    binds = { name: body.name, pid: body.parent, mst: body.mst, code: body.code, temp: body.temp, id: { type: 2010, dir: 3003 } }
                    sSysLogs = { fnc_id: 'ou_ins', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm mới thông tin đơn vị: ${JSON.stringify(body)}`, msg_id: body.id, doc: JSON.stringify(body)};
                    break
                case "delete":
                    sql = "delete from s_ou where id=:id"
                    binds = [body.id]
                    sSysLogs = { fnc_id: 'ou_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa thông tin đơn vị id ${body.id}`, msg_id: body.id, doc: JSON.stringify(body)};
                    break
                default:
                    throw new Error(`${operation} là hoạt động không hợp lệ \n (${operation} is invalid operation`)
            }
            result = await dbs.query(sql, binds)
            logging.ins(req,sSysLogs,next) 
            if (operation == "insert") res.json({ id: result.outBinds.id[0] })
            else res.json(result.rowsAffected)
        }
        catch (err) {
            next(err)
        }
    },
    bytaxc: async (req, res, next) => {
        try {
            const taxc = req.params.taxc
            //const sql = `select id "id",lpad('_',3*(level - 1),'_')||name "value" from s_ou where taxc=:1 start with pid is null connect by prior id=pid`
            const sql = `select id "id",name "value" from s_ou where mst=:1 start with pid is null connect by prior id=pid`
            const result = await dbs.query(sql, [taxc])
            res.json(result.rows)
        }
        catch (err) {
            next(err)
        }
    },
    byid: async (req, res, next) => {
        try {
            const id = req.params.id
            const sql = `select id "id",name "value" from s_ou start with id=:1 connect by prior id=pid`
            const result = await dbs.query(sql, [id])
            res.json(result.rows)
        }
        catch (err) {
            next(err)
        }
    },
    bytoken: async (req, res, next) => {
        try {
            const token = sec.decode(req)
            const sql = `select id "id",mst "mst",code "code", name "value" from s_ou start with id=:1 connect by prior id=pid order by code`
            const result = await dbs.query(sql, [token.ou])
            res.json(result.rows)
        }
        catch (err) {
            next(err)
        }
    },
    bytokenou: async (req, res, next) => {
        try {
            const token = sec.decode(req)
            const sql = `select * from (select id "id",mst "mst",taxc "taxc",code "code", name "value" from s_ou start with id=:1 connect by prior id=pid order by code ) aa  where aa."mst"  in (select b.taxc "taxc" from s_manager a,s_ou b where a.taxc=b.taxc and a.user_id=:2)`
            const result = await dbs.query(sql, [token.ou,token.uid])
            res.json(result.rows)
        }
        catch (err) {
            next(err)
        }
    },
    bytokenoubaca: async (req, res, next) => {
        try {
            const sql = `select id "id", taxc "taxc", name "value", pid from s_ou where pid is null or pid = 1`
            const result = await dbs.query(sql)
            res.json(result.rows)
        }
        catch (err) {
            next(err)
        }
    },
    getou: async (req, res, next) => {
        try {
            const token = sec.decode(req)
            let outoken = await Service.obid(token.ou), sql   = `select id "id",mst "mst",code "code", name "value" from s_ou  where pid in (select id from s_ou where pid is null)`, binds = []

            if (outoken.pid) {
                sql = `select id "id",mst "mst",code "code", name "value" from s_ou  where id = $1`
                binds = [token.ou]
            }
            const result = await dbs.query(sql, binds)
            res.json(result.rows)
        }
        catch (err) {
            next(err)
        }
    },
    obt: async (taxc) => {
        let row
        const sql = `select so.id "id",so.taxc "taxc",so.paxo "paxo",st2.name "paxoname",so.taxo "taxo",st.name "taxoname",so.name "name",so.fadd "addr",so.mail "mail",so.tel "tel",so.acc "acc",so.bank "bank",so.seq "seq",so.sign "sign",so.usr "usr",so.pwd "pwd",so.sc "sc",so.sct "sct",so.bcc "bcc",so.qrc "qrc",so.na "na",so.np "np",so.nq "nq",so.itype "itype",so.ts "ts",so.degree_config "degree_config" from s_ou so left join s_taxo st on st.id=so.taxo left join s_taxo st2 on st2.id=so.paxo where taxc=:1`
        const result = await dbs.query(sql, [taxc])
        row = result.rows[0]
        return row
    },
    gettosign: async (taxc) => {
        const sql = `select id "id",taxc "taxc",taxo "taxo",name "name",fadd "addr",mail "mail",tel "tel",acc "acc",bank "bank",seq "seq",sign "sign",usr "usr",pwd "pwd",temp "temp" from s_ou where taxc=:1`
        const result = await dbs.query(sql, [taxc])
        let row = result.rows[0]
        return row
    },
    org: async (token) => {
        return await Service.obt(token.taxc)
    },
    obid: async (id) => {
        const sql = `select so.id "id",so.taxc "taxc",so.paxo "paxo",st2.name "paxoname",so.taxo "taxo",st.name "taxoname",so.name "name",so.fadd "addr",so.mail "mail",so.tel "tel",so.acc "acc",so.bank "bank",so.seq "seq",so.sign "sign",so.usr "usr",so.pwd "pwd",so.sc "sc",so.sct "sct",so.bcc "bcc",so.qrc "qrc",so.na "na",so.np "np",so.nq "nq",so.itype "itype",so.ts "ts",so.degree_config "degree_config" from s_ou so left join s_taxo st on st.id=so.taxo left join s_taxo st2 on st2.id=so.paxo where so.id=:1`
        const result = await dbs.query(sql, [id])
        let row = result.rows[0]
        return row
    },
    oball: async () => {
        const sql = `select id "id",taxc "taxc",taxo "taxo",name "name",fadd "addr",mail "mail",tel "tel",acc "acc",bank "bank",seq "seq",sign "sign",usr "usr",pwd "pwd",temp "temp" from s_ou`
        const result = await dbs.query(sql, [])
        let rows = result.rowss
        return rows
    }
}
module.exports = Service