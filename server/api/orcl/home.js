"use strict"
const moment = require('moment')
const sec = require("../sec")
const dbs = require("./dbs")
const config = require("../config")
const logger4app = require("../logger4app")
const serial_usr_grant = config.serial_usr_grant
const ENT = config.ent
const Service = {
    get: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, query = req.query
            let form_combo = query.form_combo, serial_combo = query.serial_combo
            if(!form_combo) form_combo='*'
            if(!serial_combo) serial_combo='*'            
            const fd = new Date(moment(query.fd).format("YYYY-MM-DD 00:00:00")), td = new Date(moment(query.td).format("YYYY-MM-DD 23:59:59"))
            const tg = query.tg, dvt = query.dvt
            if (!['1','1000','1000000'].includes(dvt))  throw new Error("Tham số ĐVT không hợp lệ \n ((Invalid DVT parameter)")
            let tgc
            if (tg == 1) tgc = `to_char(idt,'DD/MM/YYYY')`
            else if (tg == 2) tgc = `to_char(idt,'WW/YY')`
            else tgc = `to_char(idt,'MM/YY')`
            let sql, rs, rows, kq = {}, thsd, invs = 0, totalv = 0, vatv = 0
            //1-thsd
            let where1 = `where stax=:1 and idt between :2 and :3 and (:4 ='*' or form=:5) and (:6='*' or serial = :7)`
            let bind1 = [taxc, fd, td,form_combo,form_combo,serial_combo,serial_combo]
            if (serial_usr_grant) {
                where1 = `, (select taxc, type types, form forms, serial serials from s_serial ss, s_seusr ss2 where ss.id = ss2.se and ss2.usrid = :1) ss where stax=:2 and stax = ss.taxc and type = ss.types and form = ss.forms and serial = ss.serials`
                bind1 = [token.uid, taxc]
            }
            sql = `select 
            nvl(sum(1),0) "i0", 
            nvl(sum(CASE status WHEN 1 THEN 1 ELSE 0 end),0) "i1",
            nvl(sum(CASE status WHEN 2 THEN 1 ELSE 0 end),0) "i2",
            nvl(sum(CASE status WHEN 3 THEN 1 ELSE 0 end),0) "i3",
            nvl(sum(CASE status WHEN 4 THEN 1 ELSE 0 end),0) "i4" 
            from s_inv ${where1}`
            if (["vib","baca"].includes(ENT)) sql = `select 
            nvl(sum(i0),0) "i0", 
            nvl(sum(i1),0) "i1",
            nvl(sum(i2),0) "i2",
            nvl(sum(i3),0) "i3",
            nvl(sum(i4),0) "i4"
            from s_inv_sum ${where1}`
            rs = await dbs.query(sql, bind1)
            thsd = rs.rows[0]
            kq.thsd = thsd
            //2-hd
            let where2 = `where idt BETWEEN :1 and :2 and stax=:3`
            let bind2 = [fd, td, taxc/*,form_combo,form_combo,serial_combo,serial_combo*/]
            if (serial_usr_grant) {
                where2 = `, (select taxc, type types, form forms, serial serials from s_serial ss, s_seusr ss2 where ss.id = ss2.se and ss2.usrid = :1) ss where idt BETWEEN :2 and :3 and stax=:4 and stax = ss.taxc and type = ss.types and form = ss.forms and serial = ss.serials`
                bind2 = [token.uid, fd, td, taxc]
            }
            sql = `select ${tgc} "tg",
            sum(1) "i0",
            sum(CASE status WHEN 1 THEN 1 ELSE 0 end) "i1",
            sum(CASE status WHEN 2 THEN 1 ELSE 0 end) "i2",
            sum(CASE status WHEN 3 THEN 1 ELSE 0 end) "i3",
            sum(CASE status WHEN 4 THEN 1 ELSE 0 end) "i4" 
            from s_inv ${where2}
            group by ${tgc} order by ${tgc}`
            if (["vib","baca"].includes(ENT)) sql = `select ${tgc} "tg",
            sum(i0) "i0",
            sum(i1) "i1",
            sum(i2) "i2",
            sum(i3) "i3",
            sum(i4) "i4" 
            from s_inv_sum ${where2}
            group by ${tgc} order by ${tgc}`
            rs = await dbs.query(sql,bind2)
            rows = rs.rows
            kq.invoice = rows

            //Chinh lai doan lay hoa don
            where2 = `where status in (1,2,3,4) and idt BETWEEN :1 and :2 and stax=:3 and (:4 ='*' or form=:5) and (:6='*' or serial = :7)`
            bind2 = [fd, td, taxc,form_combo,form_combo,serial_combo,serial_combo]
            if (serial_usr_grant) {
                where2 = `, (select taxc, type types, form forms, serial serials from s_serial ss, s_seusr ss2 where ss.id = ss2.se and ss2.usrid = :1) ss where status in (1,2,3,4) and idt BETWEEN :2 and :3 and stax=:4 and (:5 ='*' or form=:6) and (:7='*' or serial = :8) and stax = ss.taxc and type = ss.types and form = ss.forms and serial = ss.serials`
                bind2 = [token.uid, fd, td, taxc,form_combo,form_combo,serial_combo,serial_combo]
            }
            sql = `select count(*) "i0" from s_inv ${where2}`
            if (["vib","baca"].includes(ENT)) {
                where2 = `where idt BETWEEN :1 and :2 and stax=:3 and (:4 ='*' or form=:5) and (:6='*' or serial = :7)`
                sql = `select count(*) "i0" from s_inv_sum ${where2}`
            }
            rs = await dbs.query(sql, bind2)
            rows = rs.rows
            invs = rows[0].i0

            //3-totalv
            let where3 = `where idt BETWEEN :1 and :2 and stax=:3 and status=3 and type in ('01GTKT','02GTTT') and cid is null 
            and (:4 ='*' or form=:5) and (:6='*' or serial = :7) and 1 = 0`
            let bind3 = [fd, td, taxc,form_combo,form_combo,serial_combo,serial_combo]
            if (serial_usr_grant) {
                where3 = `, (select taxc, type types, form forms, serial serials from s_serial ss, s_seusr ss2 where ss.id = ss2.se and ss2.usrid = :1) ss where idt BETWEEN :2 and :3 and stax=:4 and status=3 and type in ('01GTKT','02GTTT') and cid is null 
                and (:5 ='*' or form=:6) and (:7='*' or serial = :8) and stax = ss.taxc and type = ss.types and form = ss.forms and serial = ss.serials`
                bind3 = [token.uid, fd, td, taxc,form_combo,form_combo,serial_combo,serial_combo]
            }
            sql = `select ${tgc} "tg", 
            sum(NVL(CASE WHEN adjtyp=2 THEN TO_NUMBER(a.doc.totalv) ELSE totalv END,0))/${dvt} "totalv",
            sum(NVL(CASE WHEN adjtyp=2 THEN TO_NUMBER(a.doc.vatv)   ELSE vatv   END,0))/${dvt} "vatv" 
            from s_inv a ${where3}
            group by ${tgc} order by ${tgc}`
            rs = await dbs.query(sql, bind3)
            rows = rs.rows
            kq.revenue = rows
            for (const row of rows) {
                totalv += parseFloat(row.totalv)
                vatv += parseFloat(row.vatv)
            }
            kq.total = { invs: invs, totalv: totalv, vatv: vatv }
            //4-serial
            let where4 = `where taxc=:1`
            let bind4 = [taxc/*,form_combo,form_combo,serial_combo,serial_combo*/]
            if (serial_usr_grant) {
                where4 = `, s_seusr ss where id = ss.se and ss.usrid = :1 and taxc=:2`
                bind4 = [token.uid, taxc/*, form_combo,form_combo,serial_combo,serial_combo*/]
            }
            sql = `select form||'-'||serial "serial",max "max",cur "cur",CONCAT(ROUND(((max - cur)/max) *100,8),'%') "rate",(max-cur) "kd" from s_serial ${where4} and status=1 order by form,serial`
            rs = await dbs.query(sql, bind4)
            kq.serial = rs.rows
            res.json(kq)
        } catch (err) {
            next(err)
        }
    }
}
module.exports = Service