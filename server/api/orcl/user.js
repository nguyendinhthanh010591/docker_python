"use strict"
const util = require("../util")
const sec = require("../sec")
const dbs = require("./dbs")
const logging = require("../logging")
const config = require("../config")
const logger4app = require("../logger4app")
const opt = { autoCommit: false, batchErrors: true }
const Service = {
  member: async (req, res, next) => {
    let conn
    try {
      conn = await dbs.getConn()
      const body = req.body, uid = body.uid, arr = body.arr
      let sql, result
      sql = `delete from s_group_user where user_id=:1`
      result = await conn.execute(sql, [uid], opt)
      if (arr.length > 0) {
        let binds = []
        for (const rid of arr) {
          binds.push([uid, rid])
        }
        sql = `insert into s_group_user(user_id,group_id) values (:1,:2)`
        result = await conn.executeMany(sql, binds, opt)
      }
      await conn.commit()
      const sSysLogs = { fnc_id: 'group_user_insert', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm thông tin Tài khoản ${uid} vào nhóm`};
      logging.ins(req,sSysLogs,next)  
      res.json(result)
    }
    catch (err) {
      await conn.rollback()
      next(err)
    }
    finally {
      await dbs.closeConn(conn)
    }
  },
  manager: async (req, res, next) => {
    let conn
    try {
      conn = await dbs.getConn()
      const body = req.body, uid = body.uid, arr = body.arr
      let sql, result
      sql = `delete from s_manager where user_id=:1`
      result = await conn.execute(sql, [uid], opt)
      if (arr.length > 0) {
        let binds = []
        for (const oid of arr) {
          binds.push([uid, oid])
        }
        sql = `insert into s_manager values (:1,:2)`
        result = await conn.executeMany(sql, binds, opt)
      }
      await conn.commit()
      const sSysLogs = { fnc_id: 'data_user_insert', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm thông tin Tài khoản ${uid} vào dữ liệu`};
      logging.ins(req,sSysLogs,next)
      res.json(result)
    }
    catch (err) {
      await conn.rollback()
      next(err)
    }
    finally {
      await dbs.closeConn(conn)
    }
  },
  role: async (req, res, next) => {
    try {
      const uid = req.params.uid
     // const sql = `select x.id "id",x.name "name",x.sel "sel" from (select a.id,a.name,0 sel from s_role a left join s_member b on a.id = b.role_id and b.user_id=:user_id where b.role_id is null union select a.id,a.name,1 sel from s_role a inner join s_member b on a.id = b.role_id and b.user_id=:user_id) x order by x.id`
     const sql = `select x.id "id",x.name "name",x.sel "sel" from (select a.id,a.name,0 sel from s_group a left join s_group_user b on a.id = b.group_id and b.user_id=:user_id where b.group_id is null union select a.id,a.name,1 sel from s_group a inner join s_group_user b on a.id = b.group_id and b.user_id=:user_id) x order by x.id`
      const result = await dbs.query(sql, [uid])
      res.json(result.rows)
    }
    catch (err) {
      next(err)
    }
  },
  ou: async (req, res, next) => {
    try {
      const token = sec.decode(req), ou = token.ou, uid = req.params.uid
      const sou = "(select id,pid,taxc,name from s_ou where taxc is not null start with id=:ou connect by prior id=pid)"
      const sql = `select x.id "id",x.pid "pid",x.taxc "taxc",x.name "name",x.sel "checked" from (select a.id,a.pid,a.taxc,a.name,0 sel from ${sou} a left join s_manager b on a.taxc=b.taxc and b.user_id=:user_id where b.taxc is null union select a.id,a.pid,a.taxc,a.name,1 sel from ${sou} a inner join s_manager b on a.taxc=b.taxc and b.user_id=:user_id) x start with x.id=:ou connect by prior x.id=x.pid`
      const result = await dbs.query(sql, [ou, uid])
      const rows = result.rows
      let arr = [], obj
      for (const row of rows) {
        if (row.id == ou) {
          row.open = true
          arr.push(row)
        }
        else {
          obj = util.findDFS(arr, row.pid)
          if (obj) {
            if (!obj.hasOwnProperty('data')) obj.data = []
            obj.data.push(row)
          }
        }
      }
      res.json(arr)
    }
    catch (err) {
      next(err)
    }
  },
  get: async (req, res, next) => {
    try {
      const token = sec.decode(req), query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 10, sort = query.sort
      let where = " where ou in (select id from s_ou start with id=:ou connect by prior id=pid) ", order = "", sql, result, ret, binds = [token.ou]
      if (query.filter) {
        let filter = JSON.parse(query.filter), val
        let i = 1
        Object.keys(filter).forEach((key) => {
          val = filter[key]
          if (val) {
            if (key == "id" || key == "mail" || key == "name" || key == "pos" || key == "code") {
              where += ` and upper(${key}) like :${i++}`
              binds.push(`%${val.toUpperCase()}%`)
            }
            else {
              where += ` and ${key}=:${i++}`
              binds.push(val)
            }
          }
        })
      }
      if (sort) {
        Object.keys(sort).forEach((key) => {
          order = `order by ${key}  ${sort[key]}`
        })
      }
      sql = `select id "id",mail "mail",ou "ou",uc "uc",name "name",pos "pos",code "code"${(config.is_use_local_user) ? `,local "local"` : ``} from s_user ${where} ${order} offset ${start} rows fetch next ${count} rows only`
      result = await dbs.query(sql, binds)
      ret = { data: result.rows, pos: start }
      if (start == 0) {
        sql = `select count(*) "total" from s_user ${where}`
        result = await dbs.query(sql, binds)
        ret.total_count = result.rows[0].total
      }
      const sSysLogs = { fnc_id: 'user_get', src: config.SRC_LOGGING_DEFAULT, dtl: `Tra cứu thông tin NSD`};
      logging.ins(req,sSysLogs,next)
      return res.json(ret)
    }
    catch (err) {
      next(err)
    }
  },
  post: async (req, res, next) => {
    try {
      const body = req.body, operation = body.webix_operation
      let sql, result, binds
      if (operation == "update") {
        sql = `update s_user set ou=:ou,name=:name,pos=:pos,code=:code,mail=:mail where id=:id`
        binds = [body.ou, body.name, body.pos, body.code, body.mail, body.id]
      }
      result = await dbs.query(sql, binds)
      res.json(result)
      const sSysLogs = { fnc_id: 'user_update', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật thông tin Tài khoản ${body.id}`, msg_id: body.id, doc: JSON.stringify(body)};
      logging.ins(req,sSysLogs,next)
    }
    catch (err) {
      next(err)
    }
  },
  ubr: async (req, res, next) => {
    try {
      const token = sec.decode(req), rid = req.params.rid
      const where = "and a.ou in (select id from s_ou start with id=:ou connect by prior id=pid)"
      const sql = `select x.id "acc",x.name "name",x.sel "sel",x.mail "mail",x.ou "ou",x.uc "uc",x.pos "pos" from (select a.id,a.name,0 sel,a.mail,a.ou,a.uc,a.pos from s_user a left join s_member b on a.id = b.user_id and b.role_id=:role_id where b.user_id is null ${where} union select a.id,a.name,1 sel,a.mail,a.ou,a.uc,a.pos from s_user a inner join s_member b on a.id = b.user_id and b.role_id=:role_id ${where}) x order by x.id`
      const result = await dbs.query(sql, [rid, token.ou])
      res.json(result.rows)
    } catch (err) {
      next(err)
    }
  },
  mbr: async (req, res, next) => {
    let conn
    try {
      conn = await dbs.getConn()
      const body = req.body, rid = body.rid, users = body.users
      let sql, result
      sql = `delete from s_member where role_id=:1`
      result = await conn.execute(sql, [rid], opt)
      if (users.length > 0) {
        let binds = []
        for (const uid of users) {
          binds.push([rid, uid])
        }
        sql = `insert into s_member (role_id, user_id) values (:1,:2)`
        result = await conn.executeMany(sql, binds, opt)
      }
      await conn.commit()
      res.json("ok")
    } catch (err) {
      await conn.rollback()
      next(err)
    }
    finally {
      await dbs.closeConn(conn)
    }
  },
  disable: async (req, res, next) => {
    try {
      const uid = req.params.uid
      await dbs.query(`update s_user set uc=:uc where id=:id`, [2, uid])
      const sSysLogs = { fnc_id: 'user_disable', src: config.SRC_LOGGING_DEFAULT, dtl: `Hủy bỏ Tài khoản ${uid}`, msg_id: uid};
      logging.ins(req,sSysLogs,next)
      res.send(`Tài khoản ${uid} đã bị hủy bỏ \n (The account ${uid} has been canceled)`)
    } catch (err) {
      next(err)
    }
  },
  enable: async (req, res, next) => {
    try {
      const uid = req.params.uid
      await dbs.query(`update s_user set uc=:uc where id=:id`, [1, uid])
      const sSysLogs = { fnc_id: 'user_enable', src: config.SRC_LOGGING_DEFAULT, dtl: `Kích hoạt Tài khoản ${uid}`, msg_id: uid};
      logging.ins(req,sSysLogs,next)
      res.send(`Tài khoản ${uid} đã được kích hoạt \n (The account ${uid} has been activated)`)
    } catch (err) {
      next(err)
    }
  },
  ins: async (req, obj) => {
    try {
      let ou = obj.ou
      if (!ou) {
        const token = sec.decode(req)
        ou = token.ou
      }
      const result = await dbs.query(`insert into s_user(id,code,name,ou,mail,pos${(config.is_use_local_user) ? `,local,pass` : ``}) values (:id,:code,:name,:ou,:mail,:pos${(config.is_use_local_user) ? `,:local,:pass` : ``})`, (config.is_use_local_user) ? { id: obj.id, code: (obj.code) ? obj.code : null, name: obj.name, ou: ou, mail: obj.mail, pos: obj.pos, local: obj.local, pass: obj.pass } : { id: obj.id, code: (obj.code) ? obj.code : null, name: obj.name, ou: ou, mail: obj.mail, pos: obj.pos })
      const sSysLogs = { fnc_id: 'user_insert', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm thông tin Tài khoản ${obj.id}`, msg_id: obj.id, doc: JSON.stringify(obj)};
      logging.ins(req,sSysLogs)
      return result.rowsAffected
    } catch (err) {
      throw err
    }
  },
  obid: async (id) => {
      const sql = `select id "id",mail "mail",ou "ou",uc "uc",name "name",pos "pos",code "code"${(config.is_use_local_user) ? `,local "local",change_pass_date "change_pass_date",login_number "login_number"` : ``} from s_user where id=:1`
      const result = await dbs.query(sql, [id])
      let row = result.rows[0]
      return row
  },
  upduserst: async (id) => {
    const sql = `update s_user set last_login = SYSDATE where id=:1`
    await dbs.query(sql, [id])
  },
  updatelocaluserpass: async (id, oldpass, newpass) => {
    let sql,result,binds,rows

    //Check xem old pass co dung hay khong
    sql = `select count(*) "countret" from s_user where pass = :1 and id = :2`
    binds = [oldpass, id]
    result = await dbs.query(sql, binds)
    if (result.rows[0].countret <= 0) {
      throw new Error(`Mật khẩu cũ không đúng \n Old password is incorrect`)
    }

    //Check xem new pass co trung voi cac mat khau cu hay khong
    sql = `SELECT "pass" "pass", "change_date" "change_date" FROM (  
    select pass "pass", change_pass_date "change_date" from s_user where id = :1 
    union all 
    select pass "pass", change_date "change_date" from s_user_pass where user_id = :2 ) 
    order by "change_date" desc 
    OFFSET 0 ROWS FETCH NEXT :3 ROWS ONLY`
    binds = [id, id, config.total_pass_store - 1]
    result = await dbs.query(sql, binds)
    rows =  result.rows
    for (const row of rows) {
      if (row.pass == newpass) {
        throw new Error(`Mật khẩu không được trùng lặp với ${config.total_pass_store} mật khẩu cũ \n Password cannot be the same as ${config.total_pass_store} old passwords`)
      }
    }
    
    //Insert mat khau cu vao bang s_user_pass
    await dbs.query(`insert into s_user_pass(user_id,pass,change_date) values (:1,:2,sysdate)`, [id, oldpass])

    //Update mat khau moi
    sql = `UPDATE s_user
              SET pass = :1
              ,change_pass_date = sysdate
              ,change_pass = 1
            WHERE id = :2`
    result = await dbs.query(sql, [newpass, id])
  },
  resetlocaluserpass: async (id, mail, newpass) => {
    let sql,result,binds,rows

    //Check xem old pass co dung hay khong
    sql = `select count(*) "countret" from s_user where mail = :1 and id = :2`
    binds = [mail, id]
    result = await dbs.query(sql, binds)
    if (result.rows[0].countret <= 0) {
      throw Error(`Tài khoản ${id} Mail ${mail} không tồn tại \n (Account ${id} Mail ${mail} does not exist)`)
    }

    //Update mat khau moi
    sql = `UPDATE s_user
              SET pass = :1
              ,change_pass_date = sysdate
              ,change_pass = 0
            WHERE id = :2`
    result = await dbs.query(sql, [newpass, id])
  }
}
module.exports = Service 