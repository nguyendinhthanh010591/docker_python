"use strict"
const Hashids = require("hashids/cjs")
const moment = require("moment")
const config = require("../config")
const logger4app = require("../logger4app")
const email = require("../email")
const sign = require("../sign")
const spdf = require("../spdf")
const util = require("../util")
const redis = require("../redis")
const hbs = require("../hbs")
const fcloud = require("../fcloud")
const hsm = require("../hsm")
const SEC = require("../sec")
const SERIAL = require("./serial")
const dbs = require("./dbs")
const COL = require("./col")
const ous = require("./ous")
const path = require("path")
const fs = require("fs")
const n2w = require("../n2w")
const xlsxtemp = require("xlsx-template")
const logging = require("../logging")
const { error } = require("console")
const invobj = require(`./inv`)
const sms = require("../sms")
let axios = require('axios')
const objectMapper = require("object-mapper")                
const hashids = new Hashids(config.secret, 10, "abcdefghijklmnopqrstuvwxyz1234567890")
const CARR = config.CARR
const grant = config.ou_grant
const serial_usr_grant = config.serial_usr_grant
const mfd = config.mfd
const dtf = config.dtf
const ENT = config.ent
const invoice_seq = config.invoice_seq
const UPLOAD_MINUTES = config.upload_minute
const useJobSendmail = config.useJobSendmail
const useRedisQueueSendmail = config.useRedisQueueSendmail
const KM = "Khuyến mãi"
const CK = "Chiết khấu"
const MT = "Mô tả"
const KCT = "Không chịu thuế"
const KKK = "Không kê khai nộp thuế"
const KMMT = [KM, MT]
const rbi = (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            const result = await dbs.query("select doc from s_inv where id=@1", [id])
            const rows = result.recordset
            if (rows.length == 0) reject(new Error(`Không tìm thấy Hóa đơn ${id} \n (Invoice not found ${id})`))
            const doc = JSON.parse(rows[0].doc)
            resolve(doc)
        } catch (err) {
            reject(err)
        }
    })
}
const tax = async (rows, exrt) => {
    let catobj = JSON.parse(JSON.stringify(config.CATTAX))
    let arr = catobj
    for (let row of rows) {
        let amt = Number(row.amount)
        //if (!util.isNumber(amt)) continue
        let vat = Number(row.vat)
       // if (!util.isNumber(vat)) continue
        const vrt = Number(row.vrt)
        let obj = arr.find(x => x.vrt == vrt)
        if (typeof obj == "undefined") continue
        if (row.type == CK) amt = -amt
        obj.amt += amt
        if (amt == 0) obj.isZero = true
        if (obj.hasOwnProperty("vat")) obj.vat +=  vat 
    }
    let tar = arr.filter(obj => { return obj.amt !== 0 || (obj.hasOwnProperty("isZero") && delete obj.isZero) })
    for (let row of tar) {
        row.amtv = Math.round(Number(row.amt) * Number(exrt))
        if (row.hasOwnProperty("vat")) row.vatv = Math.round(Number(row.vat) * Number(exrt))
    }
    return tar
}
const oubi = (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            const result = await dbs.query("select name name from s_ou where id=@1", [id])
            let name
            const rows = result.recordset
            if (rows.length == 0) 
                name = ""
            else 
                name = rows[0].name
            resolve(name)
        } catch (err) {
            reject(err)
        }
    })
}
const chkou = async (id, token) => {
    if (grant) {
        if (token.ou !== token.u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)
        const result = await dbs.query(`select ou from s_inv where id=@1`, [id])
        const rows = result.recordset
        if (rows.length > 0 && rows[0].ou !== token.u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)
    }
}
const Service = {
    count12: async (req, res, next) => {
        try {
            const token = SEC.decode(req), query = req.query
            const result = await dbs.query(`select count(*) rc from s_inv where stax=@1 and form=@2 and serial=@3 and status in (1, 2)`, [token.taxc, query.form, query.serial])
            res.json(result.recordset[0])
        }
        catch (err) {
            next(err)
        }
    },
    seqget: async (req, res, next) => {
        try {
            // const token = SEC.decode(req), query = req.query, filter = JSON.parse(query.filter), start = query.start ? query.start : 0, count = query.count ? query.count : 10
            // let td = filter.td
            // let where = " where stax=? and form=? and serial=? and dt<=? and status=1"
            // let binds = [token.taxc, filter.form, filter.serial,td]
            // let sql = `select id id,idt idt,status status,ou ou,type type,form form,serial serial,seq seq,btax btax,bname bname,buyer buyer,baddr baddr,sumv sumv,vatv vatv,totalv totalv,sum "sum",vat "vat",total "total",adjdes adjdes,uc uc,ic ic from s_inv ${where} order by idt,id LIMIT ${count} OFFSET ${start}`
            // let result = await dbs.query(sql, binds)
            // let ret = { data: result[0], pos: start }
            // if (start == 0) {
            //     sql = `select count(*) total from s_inv ${where}`
            //     result = await dbs.query(sql, binds)
            //     ret.total_count = result[0][0].total
            // }
            // res.json(ret)
            const token = SEC.decode(req), query = req.query, filter = JSON.parse(query.filter), start = query.start ? query.start : 0, count = query.count ? query.count : 10
            let td = moment(filter.td).endOf("day").format("YYYY-MM-DD HH:mm:ss")
            let where = "where stax=@1 and form=@2 and serial=@3 and idt<=@4 and status=1"
            let binds = [token.taxc, filter.form, filter.serial, td]
            let sql = `select id,idt,status,ou,type,form,serial,seq,btax,bname,buyer,baddr,sumv,vatv,totalv,adjdes,uc,ic from s_inv ${where} order by idt,id  OFFSET ${start} ROWS FETCH NEXT ${count} ROWS ONLY`
            let result = await dbs.query(sql, binds)
            let ret = { data: result.recordset, pos: start }
            if (start == 0) {
                sql = `select count(*) total from s_inv ${where}`
                result = await dbs.query(sql, binds)
                ret.total_count = result.recordset[0].total
            }
            res.json(ret)
        } catch (error) {
            next(error)
        }
    },
    seqpost: async (req, res, next) => {
        let key
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const wait = 60, taxc = token.taxc, body = req.body, form = body.form, serial = body.serial, idt = new Date(body.idt), seqList = body.seqList
            key = `APPR.${taxc}.${form}.${serial}`
            const val = await redis.get(key)
            if (val == "1") throw new Error(`Đang cấp số cho HĐ MST=${taxc} mẫu số=${form} ký hiệu=${serial}. Bạn hãy đợi ${wait}s !  \n (Sequencing for invoice Tax.No=${taxc} Form=${form} Serial=${serial}. Should be waiting for ${wait}s !)`)
            await redis.set(key, "1", "EX", wait)
            let sql, binds
            if (seqList.length > 0) {
                // cấp 1 hoặc lỗ chỗ trong 1 ngày
                binds = [taxc, form, serial]
                let str = "", i = 0
                for (const row of seqList) {
                    str += `@${i++},`
                    binds.push(row)
                }
                sql = `select id,pid,ou,adjtyp,idt,sec from s_inv where stax=@1 and form=@2 and serial=@3 and status=1 and id IN (${str.slice(0, -1)}) order by idt,id`
            } else {
                // cấp theo dải bé hơn ngày hiện tại
                sql = `select id,pid,ou,adjtyp,idt,sec from s_inv where idt<=@1 and stax=@2 and form=@3 and serial=@4 and status=1 order by idt,id`
                binds = [idt, taxc, form, serial]
            }
            const result = await dbs.query(sql, binds)
            const rows = result.recordset
            const sup = `update s_inv set doc=JSON_MODIFY(JSON_MODIFY(doc,'$.seq',@1),'$.status',2) where id=@2 and stax=@3 and status=1`, pup = `update s_inv set cde=@1,doc=@2 where id=@3`
            let count = 0
            for (const row of rows) {
                let seq, s7, rs, id, pid
                try {
                    id = row.id
                    seq = await SERIAL.sequence(taxc, form, serial,row.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    rs = await dbs.query(sup, [s7, id, taxc])
                    if (rs.rowsAffected[0] > 0) {
                        count++
                        pid = row.pid
                        if (!util.isEmpty(pid)) {
                            try {
                                const cde = `${(row.adjtyp == 1 ? "Bị thay thế" : "Bị điều chỉnh")} bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${moment(row.idt).format(mfd)} mã ${row.sec}`
                                let pdoc = await rbi(pid)
                                pdoc.cde = cde
                                await dbs.query(pup, [cde, JSON.stringify(pdoc), pid])
                            } catch (err) { logger4app.error(err) }
                        }
                    }
                    else throw new Error(`Không cấp số cho hóa đơn ${id} \n (Do not issue number to the invoice: ${id})`)
                    const sSysLogs = { fnc_id: 'inv_seqpost', src: config.SRC_LOGGING_DEFAULT, dtl: `Cấp số hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(body) };
                    logging.ins(req, sSysLogs, next)
                } catch (err) {
                    await SERIAL.err(taxc, form, serial, seq)
                    throw err
                }
            }
            res.json(count)
        }
        catch (err) {
            next(err)
        }
        finally {
            if (!util.isEmpty(key)) await redis.del(key)
        }
    },
    apprseqpost: async (req, res, next) => {
        let key
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const wait = 60,taxc = token.taxc, body = req.body, ids = body.ids,form = body.form, serial = body.serial, idt = new Date(body.idt)
            let i = 1, str = "", binds = []
            binds = [idt, taxc, form, serial]
            let sql, result
            sql = `select id,pid,ou,adjtyp,idt,sec,doc,bmail from s_inv where idt<=@1 and stax=@2 and form=@3 and serial=@4 and status=1 order by idt,id`
           
            result = await dbs.query(sql, binds)
            const rows = result.recordset
            if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
            if (rows.length > config.MAX_ROW_APPROVE) throw new Error(`Dữ liệu ký duyệt vượt quá ${config.MAX_ROW_APPROVE} cho phép \n (Signing data exceeds ${config.MAX_ROW_APPROVE} characters for allowed)`)
            key = `APPR.${taxc}.${form}.${serial}`
            const val = await redis.get(key)
            if (val == "1") throw new Error(`Đang duyệt cấp số cho HĐ MST=${taxc} mẫu số=${form} ký hiệu=${serial}. Bạn hãy đợi ${wait}s ! \n Sequencing for invoice Tax.No=${taxc} Form=${form} Serial=${serial}. Should be waiting for ${wait}s !`)
            await redis.set(key, "1", "EX", wait)

            const org = await ous.obt(taxc), signtype = org.sign
            if (signtype == 1) {
                let arr = []
                for (const row of rows) {
                    const doc = JSON.parse(row.doc), id = row.id, xml = util.j2x(doc, id)
                    // push {inc: id,...} thay vi {id: id,...} vì con ký anh Đại đang để là inc thay vì id CLOUD
                    arr.push({ inc: id, xml: Buffer.from(xml).toString("base64") })
                }
                res.json({ mst: taxc, date: new Date(), ref: config.PATH_XML, arr: arr })
            }
            else {
                let count = 0,seq,s7
                sql = `update s_inv set doc=JSON_MODIFY(JSON_MODIFY(doc,'$.seq',@1),'$.status',3),xml=@2,dt=@3 where id=@4 and stax=@5 and status=1`
            
                if (signtype == 2) {
                    //hunglq chinh sua thong bao loi
                    let ca, pwd
                    try {
                        pwd = util.decrypt(org.pwd)
                        ca = await sign.ca(taxc, pwd)
                    } catch (err) {
                        throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)')
                    }
                    for (const row of rows) {
                        let doc = JSON.parse(row.doc)
                        const id = row.id, xml = sign.sign(ca, util.j2x(doc, id), id)
                      
                            seq = await SERIAL.sequence(taxc, form, serial,row.idt)
                            s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                           await dbs.query(sql, [s7,xml, moment(new Date()).format(dtf), id, taxc])         
                           if(doc.adj) 
                           {
                            let cdt = moment(new Date()).format(dtf)
                            if (!util.isEmpty(row.pid)) {
                                try {
                                    // let cde = `${(row.adjtyp == 1 ? "Bị thay thế" : "Bị điều chỉnh")} bởi HĐ số ${s7} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)} mã ${doc.sec}`
                                    // let pdoc = await rbi(row.pid)

                                    let cde=''
                                    result = await dbs.query("select doc,cid,cde from s_inv where id=@1", [row.pid])
                                    const rows = result.recordset
                                    let cid,pdoc=JSON.parse(rows[0].doc)
                                    if(row.adjtyp == 2){
                                        if(rows[0].cde) cde =rows[0].cde+ `_/_`
                                        else  cde = `Bị điều chỉnh`
                                    }
                                    
                                    if(row.adjtyp == 1){
                                        cde = `Bị thay thế bởi HĐ số ${s7} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)}`
                                    }
                                    if(rows[0].cid) cid=rows[0].cid +"," + row.id
                                    else cid = row.id
                                   
                                     //HungLQ them lan dieu chinh cho VCM, neu on on thi sua chung
                                    if (config.ent == "vcm" && row.adjtyp==2) {
                                        if (pdoc.adjnum) {
                                            pdoc.adjnum = parseInt(pdoc.adjnum) + 1
                                        } else {
                                            pdoc.adjnum = 1
                                        }
                                     //   cde = `Lần điều chỉnh thứ ${pdoc.adjnum}.` + cde
                                    }
                                    if (config.ent == "vcm" && row.adjtyp==1) {
                                        pdoc.cdetemp=null
                                        pdoc.cdt=cdt
                                        pdoc.canceller=token.uid
                                        pdoc.status=4
                                        
                                    }
                                    pdoc.cde = cde
                                    await dbs.query( `update s_inv set cde=@1,doc=@2,cid=@3 where id=@4`, [cde, JSON.stringify(pdoc),cid, row.pid])
                                } catch (err) { logger4app.error(err) }
                            }
                          
                           }
                        doc.status = 3
                        if (!util.isEmpty(row.bmail)) await email.rsmqmail(doc)
                        const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                        logging.ins(req, sSysLogs, next)
                        count++
                    }
                }
                else {
                   
                    let kqs, usr, pwd
                    try {
                        if (signtype == 3) {
                            usr = org.usr
                            pwd = util.decrypt(org.pwd)
                            kqs = await fcloud.xml(usr, pwd, taxc, rows)
                        }
                        else kqs = await hsm.xml(rows)
                    } catch (err) {
                        throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)')
                    }
                    for (const row of kqs) {
                        let doc = JSON.parse(row.doc)
                        const id = row.id, xml = row.xml
                            seq = await SERIAL.sequence(taxc, form, serial,row.idt)
                            s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                            await dbs.query(sql, [s7,xml, id, taxc])
                            if(doc.adj) 
                            {
                             let cdt = moment(new Date()).format(dtf)
                             if (!util.isEmpty(row.pid)) {
                                 try {
                                     let cde = `${(row.adjtyp == 1 ? "Bị thay thế" : "Bị điều chỉnh")} bởi HĐ số ${s7} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)} mã ${doc.sec}`
                                     //let cde = row.adjtyp == 1?` Bị thay thế bởi HĐ số ${s7} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)} mã ${doc.sec}`:``
                                     let pdoc = await rbi(row.pid)
                                      //HungLQ them lan dieu chinh cho VCM, neu on on thi sua chung
                                     if (config.ent == "vcm" && row.adjtyp==2) {
                                         if (pdoc.adjnum) {
                                             pdoc.adjnum = parseInt(pdoc.adjnum) + 1
                                         } else {
                                             pdoc.adjnum = 1
                                         }
                                         //cde= `Lần điều chỉnh thứ ${pdoc.adjnum}.` + cde
                                     }
                                     if (config.ent == "vcm" && row.adjtyp==1) {
                                         pdoc.cdetemp=null
                                         pdoc.cdt=cdt
                                         pdoc.canceller=token.uid
                                         pdoc.status=4
 
                                     }
                                     pdoc.cde = cde
                                     await dbs.query( `update s_inv set cde=@1,doc=@2 where id=@3`, [cde, JSON.stringify(pdoc), row.pid])
                                 } catch (err) { logger4app.error(err) }
                             }
                           
                            }
                           
                        doc.status = 3
                        if (!util.isEmpty(row.bmail)) await email.rsmqmail(doc)
                        const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc)  };
                        logging.ins(req, sSysLogs, next)
                        count++
                    }
                }


                res.json(count)
            }
        }
        catch (err) {
            next(err)
        }
        finally {
            if (!util.isEmpty(key)) await redis.del(key)
        }
    },
    apprall: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, org = await ous.obt(taxc), signtype = org.sign
            if (signtype !== 2) throw new Error("Chỉ hỗ trợ kiểu ký bằng file \n (Only support sign by file type)")
            const iwh = await Service.iwh(req, true)
            const sql = `select id,doc,bmail from s_inv ${iwh.where}`
            const result = await dbs.query(sql, iwh.binds)
            const rows = result.recordset
            if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
            if (rows.length > config.MAX_ROW_APPROVE) throw new Error(`Dữ liệu ký duyệt vượt quá ${config.MAX_ROW_APPROVE} cho phép \n (Signing data exceeds ${config.MAX_ROW_APPROVE} characters for allowed)`)
            let pwd, ca
            //hunglq chinh sua thong bao loi
            try {
                pwd = util.decrypt(org.pwd)
                ca = await sign.ca(taxc, pwd)
            } catch (err) {
                throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)')
            }
            let count = 0
            const sql2 = `update s_inv set doc=JSON_MODIFY(doc,'$.status',3),xml=@1,dt=@2 where id=@3 and status=2`
            for (const row of rows) {
                let doc = JSON.parse(row.doc)
                const id = row.id, xml = sign.sign(ca, util.j2x(doc, id), id), result2 = await dbs.query(sql2, [xml, moment(new Date()).format(dtf), id])
                if (result2.rowsAffected[0] > 0) {
                    doc.status = 3
                    if (!util.isEmpty(row.bmail)) await email.rsmqmail(doc)
                    count++
                }
                const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                logging.ins(req, sSysLogs, next)
            }
            res.json(count)
        }
        catch (err) {
            next(err)
        }
    },
    apprpost: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const taxc = token.taxc, body = req.body, ids = body.ids
            let i = 1, str = "", binds = []
            for (const row of ids) {
                str += `@${i++},`
                binds.push(row)
            }
            binds.push(taxc)
            let sql, result
            sql = `select id,doc,bmail,pid from s_inv where id in (${str.slice(0, -1)}) and stax=@${i++} and status=2`            
            result = await dbs.query(sql, binds)
            const rows = result.recordset
            if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
            if (rows.length > config.MAX_ROW_APPROVE) throw new Error(`Dữ liệu ký duyệt vượt quá ${config.MAX_ROW_APPROVE} cho phép \n (Signing data exceeds ${config.MAX_ROW_APPROVE} characters for allowed)`)
            const org = await ous.obt(taxc), signtype = org.sign
            if (signtype == 1) {
                let arr = []
                for (const row of rows) {
                    const doc = JSON.parse(row.doc), id = row.id, xml = util.j2x(doc, id)
                    // push {inc: id,...} thay vi {id: id,...} vì con ký anh Đại đang để là inc thay vì id CLOUD
                    arr.push({ inc: id, xml: Buffer.from(xml).toString("base64") })
                }
                res.json({ mst: taxc, date: new Date(), ref: config.PATH_XML, arr: arr })
            }
            else {
                let count = 0,seq,s7
                sql = `update s_inv set doc=JSON_MODIFY(doc,'$.status',3),xml=@1,dt=@2 where id=@3 and stax=@4 and status=2`
               
                
                if (signtype == 2) {
                    //hunglq chinh sua thong bao loi
                    let ca, pwd
                    try {
                        pwd = util.decrypt(org.pwd)
                        ca = await sign.ca(taxc, pwd)
                    } catch (err) {
                        throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)')
                    }
                    for (const row of rows) {
                        let doc = JSON.parse(row.doc)
                        const id = row.id, xml = sign.sign(ca, util.j2x(doc, id), id)
                        await dbs.query(sql, [xml, moment(new Date()).format(dtf), id, taxc])
                        doc.status = 3
                        if (!util.isEmpty(row.bmail)) await email.rsmqmail(doc)
                        const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                        logging.ins(req, sSysLogs, next)
                        count++
                    }
                }
                else {
                    //hunglq chinh sua thong bao loi
                    let kqs, usr, pwd
                    try {
                        if (signtype == 3) {
                            usr = org.usr
                            pwd = util.decrypt(org.pwd)
                            kqs = await fcloud.xml(usr, pwd, taxc, rows)
                        }
                        else kqs = await hsm.xml(rows)
                    } catch (err) {
                        throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)')
                    }
                    for (const row of kqs) {
                        let doc = JSON.parse(row.doc)
                        const id = row.id, xml = row.xml
                        await dbs.query(sql, [xml, moment(new Date()).format(dtf), id, taxc])
                        doc.status = 3
                        if (!util.isEmpty(row.bmail)) await email.rsmqmail(doc)
                        const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc)  };
                        logging.ins(req, sSysLogs, next)
                        count++
                    }
                }
                res.json(count)
            }
        }
        catch (err) {
            next(err)
        }
    },
    apprput: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, body = req.body, incs = body.incs, signs = body.signs
            let i = 1, str = "", binds = []
            for (const row of incs) {
                str += `@${i++},`
                binds.push(row)
            }
            binds.push(taxc)
            let sql, result, rows, count = 0
            sql = `select id,doc,bmail from s_inv where id in (${str.slice(0, -1)}) and stax=@${i++} and status=2 `
            result = await dbs.query(sql, binds)
            rows = result.recordset
            if (rows.length > 0) {
                sql = `update s_inv set doc=JSON_MODIFY(doc,'$.status',3),xml=@1,dt=@2 where id=@3 and stax=@4 and status=2`
                for (const row of rows) {
                    let doc = JSON.parse(row.doc)
                    const id = row.id, sign = signs.find(item => item.inc === id)
                    if (sign && sign.xml) {
                        const xml = Buffer.from(sign.xml, "base64").toString()
                        await dbs.query(sql, [xml, moment(new Date()).format(dtf), id, taxc])
                        doc.status = 3
                        if (!util.isEmpty(row.bmail)) await email.rsmqmail(doc)
                        const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                        logging.ins(req, sSysLogs, next)
                        count++
                    }
                }
            }
            res.json(count)
        }
        catch (err) {
            next(err)
        }
    },
    iwh: async (req, all) => {
        const token = SEC.decode(req), u = token.u, query = req.query, filter = JSON.parse(query.filter), sort = query.sort
        let keys = Object.keys(filter), kar = [], i = 3
        let  val,  cols, where = `where valueDate between @1 and @2 `, binds = [moment(new Date(filter.fd)).format(dtf), moment(moment(filter.td).endOf("day")).format(dtf)], order
       
        kar.push("fd", "td")
        for (const key of kar) {
            delete keys[key]
        }
        for (const key of keys) {
            val = filter[key]
            if (val ) {
                switch (key) {
                    case "status":
                        where += ` and status in (${val})`
                      
                        break
                    case "trantype":
                        where += ` and trantype = @${i++}`
                        binds.push(`%${val}%`)
                        break
                    case "customerAcc":
                        where += ` and customerAcc like @${i++}`
                        binds.push(`%${val}%`)
                        break
                    case "curr":
                        where += ` and curr = @${i++}`
                        binds.push(`${val}`)
                        break
                    case "segment":
                        where += ` and segment = @${i++}`
                        binds.push(`${val}`)
                        break
                    case "inv_id":
                            where += ` and inv_id = @${i++}`
                            binds.push(`${val}`)
                            break
                    case "inv_date":
                        where += ` and inv_date = @${i++}`
                        binds.push(moment(new Date(`${val}`)).format(dtf))
                        break
                    case "refNo":
                            where += ` and refNo like @${i++}`
                            binds.push(`%${val}%`)
                        break
                    case "content":
                            where += ` and upper(vcontent) like @${i++}`
                            binds.push(`%${val.toString().toUpperCase()}%`)
                        break
                    case "vat":
                        where += ` and vrt in (${val})`
                      
                        break
                
                }
            }
        }
      
        if (sort) {
            Object.keys(sort).forEach(key => {
                order = `order by ${key} ${sort[key]}`
            })
        }
        else order = "order by valueDate desc"
        return { where: where, binds: binds,  order: order }
    },
    xls: async (req, res, next) => {
        try {
            const fn = "temp/KQTCHD.xlsx", query = req.query, filter = JSON.parse(query.filter)
            let json, rows
            const iwh = await Service.iwh(req, true)
            let cols = await COL.cacheLang(filter["type"], req.query.lang)
            let colhds = req.query.colhd.split(",")
            let extsh = []
            for (const col of cols) {
                let clbl = col.label
                extsh.push(clbl)
            }
         
            const sql = `select id,sec,ic,uc,FORMAT(idt,'dd/MM/yyyy') idt,case when status=1 then N'Chờ cấp số' when status=2 then N'Chờ duyệt' when status=3 then N'Đã duyệt' when status=4 then N'Đã hủy' when status=5 then N'Đã gửi' when status=6 then N'Chờ hủy' end status,ou,type,form,serial,seq,btax,bname,buyer,baddr,note,sumv,vatv,totalv,sum,vat,total,adjdes,cde,bmail,curr,sname sname${iwh.extxls} from s_inv ${iwh.where} ${iwh.order} `
            const result = await dbs.query(sql, iwh.binds)
            rows = result.recordset
            //Them ham tra gia tri theo mang de hien thi tương ứng voi tung cot
            for (const row of rows) {
                row.sumv = parseInt(row.sumv)
                //row.vatv = parseInt(row.vatv)
                row.totalv = parseInt(row.totalv)
                
                try {
                    row.ou = await oubi(row.ou)
                }
                catch (err) {

                }
                let extsval = []
                for (const col of cols) {
                    extsval.push(row[col.id])
                }
                row.extsval = extsval
            }
            json = { table: rows, extsh:extsh, colhds: colhds }
            const file = path.join(__dirname, "..", "..", "..", fn)
            const xlsx = fs.readFileSync(file)
            const template = new xlsxtemp(xlsx)
            template.substitute(1, json)
            res.end(template.generate(), "binary")
        }
        catch (err) {
            next(err)
        }
    },
    appget: async (req, res, next) => {
        try {
            const query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 10, iwh = await Service.iwh(req, false)
            let sql, result, ret
            sql = `select 0 chk,id,sec,ic,uc,curr,pid,cid,idt,status,ou,type,form,serial,seq,btax,bname,buyer,baddr,note,sumv,vatv,totalv,sum,vat,total,adjdes,adjtyp,cde,cvt,bmail${iwh.ext} from s_inv ${iwh.where} ${iwh.order} OFFSET ${start} ROWS FETCH NEXT ${count} ROWS ONLY`
            result = await dbs.query(sql, iwh.binds)
            ret = { data: result.recordset, pos: start }
            if (start == 0) {
                sql = `select count(*) total from s_inv ${iwh.where}`
                result = await dbs.query(sql, iwh.binds)
                ret.total_count = result.recordset[0].total
            }
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    get: async (req, res, next) => {
        try {
            //console.time("timeget")
            const query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 10, iwh = await Service.iwh(req, true)
            let sql, result, ret
            sql = `select 0 chk,[ID]
            ,[refNo]
            ,[valueDate]
            ,[customerID]
            ,[customerName]
            ,[taxCode]
            ,[customerAddr]
            ,[isSpecial]
            ,[curr]
            ,[exrt]
            ,[vrt]
            ,[chargeAmount]
            ,[vcontent]
            ,[amount]
            ,[vat]
            ,status
            ,[total]
            ,FORMAT (create_date, 'dd/MM/yyyy, HH:mm:ss') create_date
            , FORMAT (last_update, 'dd/MM/yyyy, HH:mm:ss') last_update
            ,[ma_nv]
            ,[ma_ks]
            
            ,[tranID] from s_trans ${iwh.where} ${iwh.order} OFFSET ${start} ROWS FETCH NEXT ${count} ROWS ONLY`
            result = await dbs.query(sql, iwh.binds)
            ret = { data: result.recordset, pos: start }
            if (start == 0) {
                sql = `select count(*) total from s_trans ${iwh.where}`
                result = await dbs.query(sql, iwh.binds)
                ret.total_count = result.recordset[0].total
            }
            //console.timeEnd("timeget")
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    signget: async (req, res, next) => {
        try {
            const token = SEC.decode(req), u = token.u
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const id = req.query.id, taxc = token.taxc
            let sql, result, binds = [id, taxc]
            if(ENT=='vcm'){//ko co cho cap so
                sql = `select id,doc,bmail,pid from s_inv where id=@1 and stax=@2 and status=1 `
            }else{
                sql = `select id,doc,bmail,pid from s_inv where id=@1 and stax=@2 and status=2 `
            }
           
            if (grant) {
                sql += ' and ou=@3'
                binds.push(u)
            }
            result = await dbs.query(sql, binds)
            const rows = result.recordset
            if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
            const org = await ous.obt(taxc), signtype = org.sign, row = rows[0]
            let doc = JSON.parse(row.doc)
            let xml
            if (signtype == 1) {
                const usr = org.usr, pwd = util.decrypt(org.pwd)
                xml = util.j2x(doc, id)
                const arr = [{ id: id, xml: Buffer.from(xml).toString("base64") }]
                res.json({ lib: usr, sid: 0, pin: pwd, mst: taxc, date: new Date(), ref: config.PATH_XML, arr: arr })
            }
            else {
                try{
                        if (signtype == 2) {
                            const pwd = util.decrypt(org.pwd)
                            const ca = await sign.ca(taxc, pwd)
                            xml = sign.sign(ca, util.j2x(doc, id), id)
                        }
                        else if (signtype == 3) {
                        

                                const usr = org.usr, pwd = util.decrypt(org.pwd)
                                const kqs = await fcloud.xml(usr, pwd, taxc, rows), kq = kqs[0]
                                xml = kq.xml
                            
                        
                        }
                        else {
                            const kqs = await hsm.xml(rows), kq = kqs[0]
                            xml = kq.xml
                        }               
                    } catch (err) {
                        logger4app.debug("signtype :"+ signtype +"_" + err.message)
                        throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)')
                    }
                if(ENT=='vcm'){//ko co cho cap so
                   let seq = await SERIAL.sequence(taxc, doc.form, doc.serial,doc.idt)
                   let s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    doc.seq = s7
                    doc.status = 3
                    await dbs.query(`update s_inv set doc=@1,xml=@2,dt=@3 where id=@4 and stax=@5 and status=1`, [JSON.stringify(doc),xml, moment(new Date()).format(dtf), id, taxc])
                    if(doc.adj && doc.adj.typ==1) 
                            {
                            let cdt = moment(new Date()).format(dtf)
                            if (!util.isEmpty(row.pid)) {
                                try {
                                    // let cde = `${(row.adjtyp == 1 ? "Bị thay thế" : "Bị điều chỉnh")} bởi HĐ số ${s7} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)} mã ${doc.sec}`
                                    // let pdoc = await rbi(row.pid)

                                    let cde=''
                                    result = await dbs.query("select doc,cid,cde from s_inv where id=@1", [row.pid])
                                    const rows2 = result.recordset
                                    let cid,pdoc=JSON.parse(rows2[0].doc)
                                    cde = `Bị thay thế bởi HĐ số ${doc.seq} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)}`
                                    
                                    cid = doc.id
                                    
                                    
                                    pdoc.cdetemp=null
                                    pdoc.cdt=cdt
                                    pdoc.canceller=token.uid
                                    pdoc.status=4
                                    pdoc.cde = cde
                                    await dbs.query( `update s_inv set cde=@1,doc=@2,cid=@3 where id=@4`, [cde, JSON.stringify(pdoc),cid, row.pid])
                                } catch (err) { logger4app.error(err) }
                            }
                        
                            }
                    
                }else{
                    await dbs.query(`update s_inv set doc=JSON_MODIFY(doc,'$.status',3),xml=@1,dt=@2 where id=@3 and stax=@4 and status=2`, [xml, moment(new Date()).format(dtf), id, taxc])
                }
               
                doc.status = 3
                if (!util.isEmpty(row.bmail)) await email.rsmqmail(doc)
                const sSysLogs = { fnc_id: 'inv_signget', src: config.SRC_LOGGING_DEFAULT, dtl: `Ký số hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                logging.ins(req, sSysLogs, next)
                res.json(1)
            }

        }
        catch (err) {
            next(err)
        }
    },
  
    signput: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, body = req.body, signs = body.signs
            const sign = signs[0], id = sign.id, xml = Buffer.from(sign.xml, "base64").toString()
            let result 
            if(ENT=='vcm'){//ko co cho cap so
                result = await dbs.query(`select doc,bmail,pid from s_inv where id=@1 and stax=@2 and status=1`, [id, taxc])
            }else{
                result = await dbs.query(`select doc,bmail,pid from s_inv where id=@1 and stax=@2 and status=2`, [id, taxc])
            }
            const rows = result.recordset
            if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
            const row = rows[0]
            let doc = JSON.parse(row.doc)
            if(ENT=='vcm'){//ko co cho cap so
                await dbs.query(`update s_inv set doc=JSON_MODIFY(doc,'$.status',3),xml=@1,dt=@2 where id=@3 and stax=@4 and status=1`, [xml, moment(new Date()).format(dtf), id, taxc])
                
                if(doc.adj && doc.adj.typ==1) 
                {
                 let cdt = moment(new Date()).format(dtf)
                 if (!util.isEmpty(row.pid)) {
                     try {
                         // let cde = `${(row.adjtyp == 1 ? "Bị thay thế" : "Bị điều chỉnh")} bởi HĐ số ${s7} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)} mã ${doc.sec}`
                         // let pdoc = await rbi(row.pid)

                         let cde=''
                         result = await dbs.query("select doc,cid,cde from s_inv where id=@1", [row.pid])
                         const rows2 = result.recordset
                         let cid,pdoc=JSON.parse(rows2[0].doc)
                         cde = `Bị thay thế bởi HĐ số ${doc.seq} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)}`
                        
                         cid = doc.id
                        
                         
                         pdoc.cdetemp=null
                         pdoc.cdt=cdt
                         pdoc.canceller=token.uid
                         pdoc.status=4
                         pdoc.cde = cde
                         await dbs.query( `update s_inv set cde=@1,doc=@2,cid=@3 where id=@4`, [cde, JSON.stringify(pdoc),cid, row.pid])
                     } catch (err) { logger4app.error(err) }
                 }
               
                }
            }else{
                await dbs.query(`update s_inv set doc=JSON_MODIFY(doc,'$.status',3),xml=@1,dt=@2 where id=@3 and stax=@4 and status=2`, [xml, moment(new Date()).format(dtf), id, taxc])
            }
          
            
            doc.status = 3
            if (!util.isEmpty(row.bmail)) await email.rsmqmail(doc)
            const sSysLogs = { fnc_id: 'inv_signget', src: config.SRC_LOGGING_DEFAULT, dtl: `Ký số hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
            logging.ins(req, sSysLogs, next)
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },
    put: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, id = req.params.id
            await chkou(id, token)
            await dbs.query(`update s_inv set doc=@1 where id=@2 and status=@3`, [JSON.stringify(body), id, body.status])
            const sSysLogs = { fnc_id: 'inv_put', src: config.SRC_LOGGING_DEFAULT, dtl: `Sửa hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(body) };
            logging.ins(req, sSysLogs, next)
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },
    ins: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const taxc = token.taxc, uid = token.uid, ou = token.ou, org = await ous.obt(taxc), AUTO = (org.seq === 1), body = req.body, invs = body.invs
            const sql = `insert into s_inv(id,pid,sec,ic,idt,ou,uc,doc) values (@1,@2,@3,@4,@5,@6,@7,@8)`
            for (let inv of invs) {
                let ic, id, sec, form, serial, type, pid, idt, seq, s7, rs
                try {
                    form = inv.form
                    serial = inv.serial
                    type = form.substr(0, 6)
                    ic = inv.ic
                    pid = inv.pid
                    idt = moment(inv.idt, dtf)
                    id = await redis.incr("INC")
                    sec = util.generateSecCode(config.FNC_GEN_SEC,id)
                    inv.id = id
                    inv.sec = sec
                    inv.type = type
                    inv.name = util.tenhd(type)
                    inv.stax = taxc
                    inv.sname = org.name
                    inv.saddr = org.addr
                    inv.smail = org.mail
                    inv.stel = org.tel
                    inv.taxo = org.taxo
                    inv.sacc = org.acc
                    inv.sbank = org.bank
                    if (ENT=='vcm'){
                        inv.c6 = sec
                    }
                    if (AUTO) {
                        seq = await SERIAL.sequence(taxc, form, serial,inv.idt)
                        s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                        inv.seq = s7
                        inv.status = 2
                    }
                    else {
                        s7 = "......."
                        inv.status = 1
                    }
                    rs = await dbs.query(sql, [id, pid, sec, ic, moment(idt.toDate()).format(dtf), ou, uid, JSON.stringify(inv)])
                    if (rs.rowsAffected[0] > 0) {
                        if (pid) {
                            try {
                                const cde = `Bị thay thế bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${idt.format(mfd)} mã ${sec}`
                                let cdt = moment(idt.toDate()).format(dtf)
                                await dbs.query(`update s_inv set doc=JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(doc,'$.cdt',@1),'$.status',4),'$.cde',@2),'$.canceller',@3),cid=@4,cde=@5 where id=@6 and status=3`, [cdt, cde, token.uid, id, cde, pid])
                            } catch (error) {
                                logger4app.error(error)
                            }
                        }
                    }
                    else throw new Error(`Không cấp số cho hóa đơn ${id} \n (Do not issue number to the invoice: ${id})`)
                    const sSysLogs = { fnc_id: 'inv_post', src: config.SRC_LOGGING_DEFAULT, dtl: `Lập hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(inv) };
                    logging.ins(req, sSysLogs, next)
                } catch (e) {
                    if (AUTO) await SERIAL.err(taxc, form, serial, seq)
                    throw e
                }
            }
            res.json(invs.length)
        }
        catch (err) {
            next(err)
        }
    },
    upds: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const body = req.body, invs = body.invs
            for (let inv of invs) {
                try {
                    const pid = inv.pid, doc = await rbi(pid), idt = doc.idt
                    delete inv["pid"]
                    for (let i in inv) {
                        doc[i] = inv[i]
                    }
                    doc.idt = idt
                    await dbs.query(`update s_inv set doc=@1 where id=@2 and status=2`, [JSON.stringify(doc), pid])
                    const sSysLogs = { fnc_id: 'inv_adj', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật hóa đơn id ${pid}`, msg_id: pid, doc: JSON.stringify(doc)  };
                    logging.ins(req, sSysLogs, next)
                } catch (e) {
                    throw e
                }
            }
            res.json(invs.length)
        }
        catch (err) {
            next(err)
        }
    },
    wcan: async (req, res, next) => {
        const token = SEC.decode(req), taxc = token.taxc, body = req.body, invs = body.invs
        const sql = `update s_inv set doc=JSON_MODIFY(JSON_MODIFY(doc,'$.status',6),'$.cancel',JSON_QUERY(@1)) where id=@2 and stax=@3 and status in (2,3)`
        for (const item of invs) {
            try {
                const cancel = { typ: 3, ref: item.ref, rdt: item.rdt, rea: item.rea }
                const result = await dbs.query(sql, [JSON.stringify(cancel), item.id, taxc])
                if (result.rowsAffected[0] > 0) {
                    if (item.pid) await dbs.query(`update s_inv set cid=null,cde=null where id=@1`, [item.pid])
                    item.error = 'OK'
                }
                else item.error = 'NOT OK'
            }
            catch (e) {
                item.error = e.message
            }
        }
        res.json({ save: true, data: invs })
    },
    sav: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body
            if (grant && token.ou !== token.u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)
            const taxc = token.taxc, uid = token.uid, ou = token.ou, org = await ous.obt(taxc), form = body.form, serial = body.serial, id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC,id)
            let seq, s7,result,row
            const dt = moment(body.idt),  eod = dt.endOf('day').toDate(), dts = dt.format(mfd),dtsc = dt.format('YYYYMMDD'),idtc=moment(new Date()).format('YYYYMMDD')
            try {
                if (dtsc!= idtc){
                    result = await dbs.query(`select count(*) rc from s_inv where idt>@1 and form=@2 and serial=@3 and status>1 and status<>4`, [eod, form, serial])
                    row = result.recordset[0]
                    if (row.rc > 0) throw new Error(`Tồn tại HĐ có ngày lớn hơn ${moment(body.idt).format('DD/MM/YYYY')} và đã cấp số \n (Invoice has a larger date  ${moment(body.idt).format('DD/MM/YYYY')} and issued a number)`)
                }

                body.id = id
                body.sec = sec
                body.stax = org.taxc
                body.sname = org.name
                body.saddr = org.addr
                body.smail = org.mail
                body.stel = org.tel
                body.taxo = org.taxo
                body.sacc = org.acc
                body.sbank = org.bank
                if(ENT!='vcm'){
                    seq = await SERIAL.sequence(taxc, form, serial,body.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    body.seq = s7
                    body.status = 2
                   
                }else{
                    body.status = 1
                    body.c6 = sec
                   
                }
                await dbs.query(`insert into s_inv(id,sec,idt,ou,uc,doc) values (@1,@2,@3,@4,@5,@6)`, [id, sec, moment(body.idt).format("YYYY-MM-DD HH:mm:ss"), ou, uid, JSON.stringify(body)])
               
                const sSysLogs = { fnc_id: 'inv_post', src: config.SRC_LOGGING_DEFAULT, dtl: `Lập hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(body)  };
                logging.ins(req, sSysLogs, next)
                res.json(id)
            }
            catch (err) {
                await SERIAL.err(taxc, form, serial, seq)
                throw err
            }
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body
            if (grant && token.ou !== token.u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)
            const taxc = token.taxc, uid = token.uid, ou = token.ou, org = await ous.obt(taxc), AUTO = (org.seq === 1), form = body.form, serial = body.serial, id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC,id)
            let seq, s7,result,row
            const dt = moment(body.idt),  eod = dt.endOf('day').toDate(), dts = dt.format(mfd),dtsc = dt.format('YYYYMMDD'),idtc=(moment(new Date())).format('YYYYMMDD')
            try {
                if (dtsc!= idtc){
                    result = await dbs.query(`select count(*) rc from s_inv where idt>@1 and form=@2 and serial=@3 and status>1 and status<>4`, [eod, form, serial])
                    row = result.recordset[0]
                    if (row.rc > 0) throw new Error(`Tồn tại HĐ có ngày lớn hơn ${moment(body.idt).format('DD/MM/YYYY')} và đã cấp số \n (Invoice has a larger date  ${moment(body.idt).format('DD/MM/YYYY')} and issued a number)`)
                }
                
                body.id = id
                body.sec = sec
                body.stax = org.taxc
                body.sname = org.name
                body.saddr = org.addr
                body.smail = org.mail
                body.stel = org.tel
                body.taxo = org.taxo
                body.sacc = org.acc
                body.sbank = org.bank
                if (AUTO) {
                    seq = await SERIAL.sequence(taxc, form, serial,body.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    body.seq = s7
                    body.status = 2
                }
                else body.status = 1
                if (ENT=='vcm'){
                    body.c6=sec
                }
                await dbs.query(`insert into s_inv(id,sec,idt,ou,uc,doc) values (@1,@2,@3,@4,@5,@6)`, [id, sec, moment(body.idt).format("YYYY-MM-DD HH:mm:ss"), ou, uid, JSON.stringify(body)])
                const sSysLogs = { fnc_id: 'inv_post', src: config.SRC_LOGGING_DEFAULT, dtl: `Lập hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(body) };
                logging.ins(req, sSysLogs, next)
                res.json(id)
            }
            catch (err) {
                if (AUTO) await SERIAL.err(taxc, form, serial, seq)
                throw err
            }
        }
        catch (err) {
            next(err)
        }
    },
    adj: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, pid = body.pid,sumv=body.sumv
            await chkou(pid, token)
            const uid = token.uid, ou = token.ou, taxc = body.stax, form = body.form, serial = body.serial, org = await ous.obt(taxc), AUTO = (org.seq === 1), id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC,id), idt = new Date(body.idt)
            let seq, s7, result, insid
            result = await dbs.query("select doc,cid,pid,adjtyp from s_inv where id=@1", [pid])
            const rows = result.recordset
            if(!util.isEmpty(rows[0].pid) && body.sumv!=0 && rows[0].adjtyp!=1) throw new Error(`Chỉ được phép điều chỉnh thông tin cho hóa đơn điều chỉnh ${pid} \n (Only adjust information for adjustment invoices ${pid})`)
            if(ENT=="vcm" ){//check tong tien nhieu hoa don dieu chinh cho 1 hoa don
                if( body.dif && body.dif.sumv>0){
                    
                    if (rows.length == 0) throw (new Error(`Không tìm thấy Hóa đơn gốc ${pid} \n (No original invoice found ${pid}`))
                    const cid = rows[0].cid,sumg=JSON.parse(rows[0].doc).sumv
                    if(cid){
                         result = await dbs.query(`select doc from s_inv where id in (${cid})`, [])
                        const  rowsi = result.recordset
                        if (rowsi.length == 0) throw(new Error(`Không tìm thấy Hóa đơn điều chỉnh của hóa đơn gốc ${cid} \n (The Adjustment Invoice of original invoice was not found ${cid})`))
                        let suml=0,sumi=0
                        for (let row of rowsi) {
                          let   doc = JSON.parse(row.doc)
                            if (doc.dif && doc.dif.sumv>0) suml+=doc.sumv
                            if (doc.dif && doc.dif.sumv<0) sumi+=doc.sumv
                        }
                        if (suml+sumv>sumg+sumi) throw(new Error(`Tổng thành tiền của các hóa đơn điều chỉnh giảm cho cùng một hóa đơn gốc phải <= Thành tiền của các hóa đơn điều chỉnh tăng cho cùng hóa đơn gốc + Thành tiền chưa thuế trên hóa đơn gốc ${pid} \n (The total amount of invoice reduction adjustments for the same original invoice must <= the total amount of invoice increasing adjustments for the same original invoice + the total amount without tax of the original invoice ${pid})`))
                    }
                }
                body.c6 = sec
                
            }
            try {
                body.id = id
                body.sec = sec
                if (AUTO) {
                    seq = await SERIAL.sequence(taxc, form, serial,body.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    body.seq = s7
                    body.status = 2
                }
                else {
                    s7 = "......."
                    body.status = 1
                }
                result = await dbs.query(`insert into s_inv (id,pid,sec,idt,ou,uc,doc) OUTPUT inserted.id values (@1,@2,@3,@4,@5,@6,@7)`, [id, pid, sec, idt, ou, uid, JSON.stringify(body)])
                if (result.rowsAffected[0]) insid = result.recordset[0].id
                let cde=''
                result = await dbs.query("select doc,cid,cde from s_inv where id=@1", [pid])
                const rows = result.recordset
                let cid,pdoc=JSON.parse(rows[0].doc)
                if(ENT=="vcm" ){//nhieu điều chỉnh cho 1
                     if(rows[0].cde) cde =rows[0].cde+ `_/_`
                     else  cde = `Bị điều chỉnh`

                    if(rows[0].cid) cid =rows[0].cid+"," + id
                    else cid=id
                }else{
                    cde = `Bị điều chỉnh bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${moment(idt).format(mfd)} mã ${sec}.`
                    cid=id
                }
               
                pdoc.cde = cde
               
                let cdt = moment(new Date()).format(dtf)
                pdoc.cdt = cdt
                pdoc.canceller=token.uid
                await dbs.query(`update s_inv set cid=@1,cde=@2,doc=@3 where id=@4`, [cid, cde, JSON.stringify(pdoc), pid])
                const sSysLogs = { fnc_id: 'inv_adj', src: config.SRC_LOGGING_DEFAULT, dtl: `Điều chỉnh hóa đơn id ${pid}`, msg_id: pid, doc: JSON.stringify(pdoc) };
                logging.ins(req, sSysLogs, next)
                res.json(insid)
            }
            catch (err) {
                if (AUTO) await SERIAL.err(taxc, form, serial, seq)
                throw err
            }
        }
        catch (err) {
            next(err)
        }
    },
    adjniss: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, pid = body.pid,sumv=body.sumv

            await chkou(pid, token)
            const uid = token.uid, ou = token.ou,u = token.u, taxc = body.stax, form = body.form, serial = body.serial, org = await ous.obt(taxc), AUTO = (org.seq === 1), id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC,id), idt = new Date(body.idt)
            let seq, s7, result, insid,chk=0
            result = await dbs.query("select doc,cid,pid,adjtyp from s_inv where id=@1", [pid])
            let rows = result.recordset
            if(!util.isEmpty(rows[0].pid) && body.sumv!=0 && rows[0].adjtyp!=1) throw new Error(`Chỉ được phép điều chỉnh thông tin cho hóa đơn điều chỉnh ${pid} \n (Only adjust information for adjustment invoices ${pid})`)
            if(ENT=="vcm" ){//check tong tien nhieu hoa don dieu chinh cho 1 hoa don
                if(body.dif && body.dif.sumv>0){
                   
                    if (rows.length == 0) throw(new Error(`Không tìm thấy Hóa đơn gốc ${pid} \n (No original invoice found ${pid}`))
                    const cid = rows[0].cid,sumg=JSON.parse(rows[0].doc).sumv
                    if(cid){
                         result = await dbs.query(`select doc from s_inv where id in (${cid})`, [])
                         rows = result.recordset
                        if (rows.length == 0) throw(new Error(`Không tìm thấy Hóa đơn điều chỉnh của hóa đơn gốc ${cid} \n (The Adjustment Invoice of original invoice was not found ${cid})`))
                        let suml=0,sumi=0
                        for (let row of rows) {
                          let   doc = JSON.parse(row.doc)
                            if (doc.dif.sumv>0) suml+=doc.sumv
                            if (doc.dif.sumv<0) sumi+=doc.sumv
                        }
                        if (suml+sumv>sumg+sumi) throw(new Error(`Tổng thành tiền của các hóa đơn điều chỉnh giảm cho cùng một hóa đơn gốc phải <= Thành tiền của các hóa đơn điều chỉnh tăng cho cùng hóa đơn gốc + Thành tiền chưa thuế trên hóa đơn gốc ${pid} \n (The total amount of invoice reduction adjustments for the same original invoice must <= the total amount of invoice increasing adjustments for the same original invoice + the total amount without tax of the original invoice ${pid})`))
                    }
                }
                body.c6 = sec
                
            }
            try {
                body.id = id
                body.sec = sec
               
                
                body.status = 1
              
                result = await dbs.query(`insert into s_inv (id,pid,sec,idt,ou,uc,doc) OUTPUT inserted.id values (@1,@2,@3,@4,@5,@6,@7)`, [id, pid, sec, idt, ou, uid, JSON.stringify(body)])
                chk = 1
                if (result.rowsAffected[0]) insid = result.recordset[0].id
                //const cde = `Bị điều chỉnh bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${moment(idt).format(mfd)} mã ${sec}.`
                //const pdoc = await rbi(pid)

                let cde=''
                result = await dbs.query("select doc,cid,cde from s_inv where id=@1", [pid])
                rows = result.recordset
                let cid,pdoc= JSON.parse(rows[0].doc)
                if(ENT=="vcm" ){//nhieu điều chỉnh cho 1
                    if(rows[0].cde) cde =rows[0].cde+ `_/_`
                    else  cde = `Bị điều chỉnh`

                    if(rows[0].cid) cid=rows[0].cid +"," + id
                    else cid=id
                }else{
                    cde = `Bị điều chỉnh bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${moment(idt).format(mfd)} mã ${sec}.`
                    cid=id
                }
                pdoc.cde = cde
                //HungLQ them lan dieu chinh cho VCM, neu on on thi sua chung
                if (config.ent == "vcm") {
                    if (pdoc.adjnum) {
                        pdoc.adjnum = parseInt(pdoc.adjnum) + 1
                    } else {
                        pdoc.adjnum = 1
                    }
                   // pdoc.cde = `Lần điều chỉnh thứ ${pdoc.adjnum}.` + cde
                }
                let cdt = moment(new Date()).format(dtf)
                pdoc.cdt = cdt
                pdoc.canceller=token.uid
                await dbs.query(`update s_inv set cid=@1,cde=@2,doc=@3 where id=@4`, [cid, cde, JSON.stringify(pdoc), pid])

             // ky xml
                let sql, binds = [id, taxc]
                sql = `select id,doc,bmail from s_inv where id=@1 and stax=@2 and status=1 `
                if (grant) {
                    sql += ' and ou=@3'
                    binds.push(u)
                }
                result = await dbs.query(sql, binds)
                 rows = result.recordset
                if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
                const org = await ous.obt(taxc), signtype = org.sign, row = rows[0]
                let doc = JSON.parse(row.doc)
                let xml
                if (signtype == 1) {
                    const usr = org.usr, pwd = util.decrypt(org.pwd)
                    xml = util.j2x(doc, id)
                    const arr = [{ id: id, xml: Buffer.from(xml).toString("base64") }]
                    res.json({ lib: usr, sid: 0, pin: pwd, mst: taxc, date: new Date(), ref: config.PATH_XML, arr: arr })
                }
                else {
                try {
                        if (signtype == 2) {
                            const pwd = util.decrypt(org.pwd)
                            const ca = await sign.ca(taxc, pwd)
                            xml = sign.sign(ca, util.j2x(doc, id), id)
                        }
                        else if (signtype == 3) {
                            const usr = org.usr, pwd = util.decrypt(org.pwd)
                            const kqs = await fcloud.xml(usr, pwd, taxc, rows), kq = kqs[0]
                            xml = kq.xml
                        }
                        else {
                            const kqs = await hsm.xml(rows), kq = kqs[0]
                            xml = kq.xml
                        }
                    }catch (err) {
                        
                       // if(chk==1) err.message=`Đã lưu hóa đơn ${insid} .`+ err.message
                        throw new Error(`Đã lưu tạm hóa đơn ${insid}. Cấu hình chữ ký số không hợp lệ !  \n (The invoice has been temporarily saved ${insid} . Invalid digital signature configuration!`)
                    }
                    seq = await SERIAL.sequence(taxc, form, serial,body.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    body.seq = s7
                    body.status = 3
                    await dbs.query(`update s_inv set doc=@1,xml=@2,dt=@3 where id=@4 and stax=@5 and status=1`, [JSON.stringify(body),xml, moment(new Date()).format(dtf), id, taxc])
                    doc.status = 3
                    if (!util.isEmpty(row.bmail)) await email.rsmqmail(doc)

                const sSysLogs = { fnc_id: 'inv_adj', src: config.SRC_LOGGING_DEFAULT, dtl: `Điều chỉnh hóa đơn id ${pid}`, msg_id: pid, doc: JSON.stringify(pdoc) };
                logging.ins(req, sSysLogs, next)
                res.json(insid)
            }}
            catch (err) {
                if (AUTO) await SERIAL.err(taxc, form, serial, seq)
               // if(chk==1) err.message=`Đã lưu hóa đơn ${insid} .`+ err.message
                throw err
            }
        }
        catch (err) {
           
            next(err)
        }
    },
    rep: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            let body = req.body, pid = body.pid
            await chkou(pid, token)
            const uid = token.uid, ou = token.ou, taxc = body.stax, form = body.form, serial = body.serial, org = await ous.obt(taxc), AUTO = (org.seq === 1), id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC,id), idt = new Date(body.idt)
            let seq, s7, result, insid
            try {
                body.id = id
                body.sec = sec
                // AutoSeq exists nghĩa là PHAT HANH ở THAY THE, voi vcm ky mới cấp số
                if ((AUTO || body.AutoSeq) && ENT!='vcm')  {
                    if (body.AutoSeq) delete body["AutoSeq"]
                    seq = await SERIAL.sequence(taxc, form, serial,body.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    body.seq = s7
                    body.status = 2
                }
                else {
                    s7 = "......."
                    body.status = 1
                }
                if (ENT=='vcm') body.c6 = sec
                result = await dbs.query(`insert into s_inv (id,pid,sec,idt,ou,uc,doc) OUTPUT inserted.id values (@1,@2,@3,@4,@5,@6,@7)`, [id, pid, sec, idt, ou, uid, JSON.stringify(body)])
                if (result.rowsAffected[0]) insid = result.recordset[0].id
                const cde = `Bị thay thế bởi HĐ số ${s7} ký hiệu ${body.serial} mẫu số ${body.form} ngày ${moment(idt).format(mfd)} mã ${sec}`
                let cdt = moment(new Date()).format(dtf)
                if (invoice_seq==0){
                    await dbs.query(`update s_inv set doc=JSON_MODIFY(doc,'$.cdetemp',@1) where id=@2`, [cde,pid])
                }else{
                    await dbs.query(`update s_inv set doc=JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(doc,'$.cdt',@1),'$.status',4),'$.cde',@2),'$.canceller',@3),cid=@4,cde=@5 where id=@6`, [cdt, cde, token.uid, id, cde, pid])
                }
                
                const sSysLogs = { fnc_id: 'inv_replace', src: config.SRC_LOGGING_DEFAULT, dtl: `Thay thế hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(body) };
                logging.ins(req, sSysLogs, next)
                res.json(insid)
            }
            catch (err) {
                if (AUTO) await SERIAL.err(taxc, form, serial, seq)
                throw err
            }
        }
        catch (err) {
            next(err)
        }
    },
    go24: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, body = req.body, form = body.form, serial = body.serial, seq = body.seq, id = body.id, d = body.d, direct = d > 0 ? "tiến" : "lùi"
            await chkou(id, token)
            const idt = new Date(body.idt)
            idt.setDate(idt.getDate() + d)
            const dt = moment(idt), sod = dt.startOf('day').toDate(), eod = dt.endOf('day').toDate(), dts = dt.format(mfd)
            let result, row
            if (d > 0) {
                const today = moment().endOf('day').toDate()
                if (eod > today) throw new Error(`Ngày tiến hóa đơn ${dts} lớn hơn ngày hiện tại \n (The forward date of the invoice ${dts} is greater than the current date)`)
            }
            else {
                result = await dbs.query(`select count(*) rc from s_serial where taxc=@1 and form=@2 and serial=@3 and fd>@4`, [taxc, form, serial, eod])
                row = result.recordset[0]
                if (row.rc > 0) throw new Error(`Ngày lùi hóa đơn ${dts} không có TBPH \n (The back date of the invoice ${dts} doesn't have Released information)`)
            }
            if (seq) {
                const iseq = parseInt(seq)
                result = await dbs.query(`select count(*) rc from s_inv where idt<@1 and form=@2 and serial=@3 and (status=1 or (status>1 and cast(seq as int)>@4))`, [sod, form, serial, iseq])
                row = result.recordset[0]
                if (row.rc > 0) throw new Error(`Hóa đơn không ${direct} được: tồn tại HĐ có ngày nhỏ hơn ${dts} chưa cấp số hoặc có số lớn hơn ${seq} \n (Invoice can not ${direct}: Existing invoices with dates less than ${dts} and not issued a number or number greater than ${seq})`)
                result = await dbs.query(`select count(*) rc from s_inv where idt>@1 and form=@2 and serial=@3 and status>1 and cast(seq as int)<@4`, [eod, form, serial, iseq])
                row = result.recordset[0]
                if (row.rc > 0) throw new Error(`Hóa đơn không ${direct} được: tồn tại HĐ có ngày lớn hơn ${dts} và số lớn hơn ${seq} \n (Invoice can not ${direct}: Existing invoices with dates greater than ${dts} and number greater than ${seq})`)
            }
            else {
                result = await dbs.query(`select count(*) rc from s_inv where idt>@1 and form=@2 and serial=@3 and status>1`, [eod, form, serial])
                row = result.recordset[0]
                if (row.rc > 0) throw new Error(`Hóa đơn không ${direct} được : tồn tại HĐ có ngày lớn hơn ${dts} và đã cấp số \n (Invoice can not ${direct}: Existing invoices with dates greater than ${dts} and issued a number`)
            }
            await dbs.query(`update s_inv set idt=@1,doc=JSON_MODIFY(doc,'$.idt',@2) where id=@3 and status in (1, 2)`, [sod, moment(sod).format(dtf), id])
            res.json(1)
        } catch (err) {
            next(err)
        }
    },
    status: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, id = req.params.id
            let doc, status = req.params.status
            await chkou(id, token)
            if (status == 3 || status == 2) {
                await dbs.query(`update s_inv set doc=JSON_MODIFY(doc,'$.cancel',NULL) where id=@1`, [id])
                const r = await dbs.query(`select top 1 1 from s_inv where id=@1 and xml is not null`, [id])
                if (r.recordset.length > 0) status = 3
                else status = 2
                //Xóa biên bản
                if (UPLOAD_MINUTES) await dbs.query(`DELETE FROM s_inv_file where id=@1`, [id])
            }
            else if (status == 4) {
                doc = await rbi(id)
                const cancel = { typ: 3, ref: id, rdt: moment().format(dtf) }
                if (doc.pid && doc.adj.typ == 2) await dbs.query(`update s_inv set cid=null,cde=null where id=@1`, [doc.pid])
                let cdt = moment(new Date()).format(dtf)
                await dbs.query(`update s_inv set doc=JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(doc,'$.cdt',@1),'$.canceller',@2),'$.cancel',JSON_QUERY(@3)) where id=@4 and stax=@5`, [cdt, token.uid, JSON.stringify(cancel), id, taxc])
            }
            let result = await dbs.query(`update s_inv set doc=JSON_MODIFY(doc,'$.status',@1) where id=@2 and stax=@3`, [status, id, taxc])
            if (result.rowsAffected[0] > 0) {
                if ((status == 4) && !util.isEmpty(doc.bmail)) {
                    doc.status = 4
                    await email.rsmqmail(doc)
                }
                const sSysLogs = { fnc_id: 'inv_istatus', src: config.SRC_LOGGING_DEFAULT, dtl: `Chuyển đổi trạng thái hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                logging.ins(req, sSysLogs, next)
            }
            res.json(1)
        } catch (err) {
            next(err)
        }
    },
    cancel: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, id = req.params.id
            await chkou(id, token)
            const doc = await rbi(id), cancel = { typ: body.typ, ref: body.ref, rdt: body.rdt, rea: body.rea }
            let result,rows
            if (doc.pid && doc.adj.typ == 2) 
            {
                result = await dbs.query("select cid,cde from s_inv where id=@1", [doc.pid])
                rows = result.recordset
                let cid = rows[0].cid
                if (cid && cid.split(',').length>1){
                   let cids=cid.split(','),cidst=''
                   for(let c of cids){
                       if(c!=id ) cidst=cidst+','+c 
                   }
                   cidst=cidst.slice(1)
                   await dbs.query(`update s_inv set cid=@1 where id=@2`, [cidst,doc.pid])
                }else{
                    await dbs.query(`update s_inv set cid=null,cde=null where id=@1`, [doc.pid])
                }
               
            }
            let cdt = moment(new Date()).format(dtf)
             result = await dbs.query(`update s_inv set doc=JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(doc,'$.cdt',@1),'$.canceller',@2),'$.status',4),'$.cancel',JSON_QUERY(@3)) where id=@4 and stax=@5`, [cdt, token.uid, JSON.stringify(cancel), id, token.taxc])
            if (result.rowsAffected[0]) {
                if (!util.isEmpty(doc.bmail)) {
                    doc.status = 4
                    await email.rsmqmail(doc)
                }
            }
            const sSysLogs = { fnc_id: 'inv_cancel', src: config.SRC_LOGGING_DEFAULT, dtl: `Hủy hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc)  };
            logging.ins(req, sSysLogs, next)
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },
    trash: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, id = req.params.id
            await chkou(id, token)
            //Lay du lieu doc truoc khi xoa de luu log
            let doclog = await rbi(id)
            const cancel = { typ: body.typ, ref: body.ref, rdt: body.rdt, rea: body.rea }
            await dbs.query(`update s_inv set doc=JSON_MODIFY(JSON_MODIFY(doc,'$.status',6),'$.cancel',JSON_QUERY(@1)) where id=@2 and stax=@3 and status in (2,3)`, [JSON.stringify(cancel), id, token.taxc])
            res.json(1)
            const sSysLogs = { fnc_id: 'inv_adj', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doclog) };
            logging.ins(req, sSysLogs, next)
        }
        catch (err) {
            next(err)
        }
    },
    istatus: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, body = req.body, ids = body.ids, os = parseInt(body.os), now = moment().format(dtf)
            let rc = 0, ns = body.ns
            for (let id of ids) {
                try {
                    let doc, sql, binds, doclog, sSysLogs
                    if (ns == 4) {
                        const re = await dbs.query("select doc,cdt from s_inv where id=@1", [id])
                        const rws = re.recordset
                        if (rws.length == 0) throw new Error(`Không tìm thấy Hóa đơn ${id} \n Invoice not found ${id}`)
                        doc = JSON.parse(rws[0].doc)
                        //Lay du lieu doc truoc khi xoa de luu log
                        doclog = await rbi(id)
						let cdt = rws[0].cdt
						if (cdt) {
							sql = `update s_inv set doc=JSON_MODIFY(doc,'$.status',@1) where id=@2 and stax=@3 and status=@4`
							binds = [ns, id, taxc, os]
						} else {
							cdt = moment(new Date()).format(dtf)
							sql = `update s_inv set doc=JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(doc,'$.cdt',@1),'$.status',@2),'$.canceller',@3) where id=@4 and stax=@5 and status=@6`
							binds = [cdt, ns, token.uid, id, taxc, os]				
						}
                        if (doc.pid && doc.adj.typ == 2) await dbs.query(`update s_inv set cid=null,cde=null where id=@1`, [doc.pid])
                        if (!doc.cancel) await dbs.query(`update s_inv set doc=JSON_MODIFY(doc,'$.cancel',JSON_QUERY(@1)) where id=@2 and stax=@3 and status=@4`, [JSON.stringify({ typ: 3, ref: id, rdt: now }), id, taxc, os])
                        sSysLogs = { fnc_id: 'inv_cancel', src: config.SRC_LOGGING_DEFAULT, dtl: `Hủy hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doclog)};
                    } else {
                        //Lay du lieu doc truoc khi xoa de luu log
                        doclog = await rbi(id)
                        if (ns == 2 || ns == 3) {
                            const r = await dbs.query(`select top 1 1 from s_inv where id=@1 and xml is not null`, [id])
                            if (r.recordset.length > 0) ns = 3
                            else ns = 2
                        }
                        sql = `update s_inv set doc=JSON_MODIFY(doc,'$.status',@1) where id=@2 and stax=@3 and status=@4`
                        binds = [ns, id, taxc, os]
                        sSysLogs = { fnc_id: 'inv_istatus', src: config.SRC_LOGGING_DEFAULT, dtl: `Chuyển đổi trạng thái hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doclog)};
                    }
                    const result = await dbs.query(sql, binds)
                    if (result.rowsAffected[0] > 0) {
                        if ((ns == 4) && !util.isEmpty(doc.bmail)) {
                            doc.status = 4
                            await email.rsmqmail(doc)
                        }
                        rc++
                    }
                    
                    logging.ins(req, sSysLogs, next)
                } catch (e) {
                    logger4app.error(e)
                }
            }
            res.json(rc)
        }
        catch (err) {
            next(err)
        }
    },
    del: async (req, res, next) => {
        try {
            const token = SEC.decode(req), id = req.params.id, pid = req.params.pid
            await chkou(id, token)
            //Lay du lieu doc truoc khi xoa de luu log
            let doclog = await rbi(id)
            const result = await dbs.query(`delete from s_inv where id=@1 and stax=@2 and status=@3`, [id, token.taxc, 1])
            if (result.rowsAffected[0] > 0 && pid > 0) {
                let doc = await rbi(pid)
                doc.status = 3
                //Xóa doc.cde , nếu không có cũng ko ảnh hưởng.
                delete doc["cde"]
                delete doc["cdt"]
                delete doc["canceller"]
                // await dbs.query(`update s_inv set doc=JSON_MODIFY(doc,'$.status',3),cid=null,cde=null where id=@1`, [pid])
                await dbs.query(`update s_inv set doc=@1,cid=null,cde=null where id=@2`, [JSON.stringify(doc), pid])
                //Xóa biên bản
                await dbs.query(`DELETE FROM s_inv_file where id=@1`, [pid])
            }
            const sSysLogs = { fnc_id: 'inv_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doclog) };
            logging.ins(req, sSysLogs, next)
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },
    relate: async (req, res, next) => {
        try {
            const token = SEC.decode(req), id = req.params.id
            let sql, result
            result = await dbs.query("select doc,pid from s_inv where id=@1", [id])
            const rows = result.recordset
            if (rows.length == 0) reject(new Error(`Không tìm thấy Hóa đơn ${id} \n Invoice not found ${id}`))
            const doc = JSON.parse(rows[0].doc),sec = doc.sec, root = doc.root, rsec = root ? root.sec : null,pid=rows[0].pid
            let  binds = [token.taxc, sec]
            sql = `select id,idt,type,form,serial,seq,sec,status,adjdes,cde from s_inv where stax=@1 and sec<>@2 and (JSON_VALUE(doc,'$.root.sec')=@2`
            if (rsec) {
                if (pid){
                    sql += ` or sec=@3 or JSON_VALUE(doc,'$.root.sec')=@3 or id in (${pid})`
                }else{
                    sql += ` or sec=@3 or JSON_VALUE(doc,'$.root.sec')=@3 `
                }
                
                binds.push(rsec)
            }
            
            sql += ") order by id"
            result = await dbs.query(sql, binds)
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    relateadjs: async (req, res, next) => {
        try {
            const token = SEC.decode(req), id = req.params.id
            let sql, result
            sql = `select id id,idt idt,type type,form form,serial serial,seq seq,sec sec,status status,adjdes adjdes,cde cde from s_inv a where id in (${id}) order by id`
          
            result = await dbs.query(sql, [])
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    deleteinv: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const taxc = token.taxc, uid = token.uid, ou = token.ou,
                org = await ous.obt(taxc), AUTO = (org.seq === 1), body = req.body,
                invs = body.invs
            let count = 1
            let status, message

                try {
                    const token = SEC.decode(req)
                 
                    for (let inv of invs) {
                        await chkou(inv.id, token)
                        //Lay du lieu doc truoc khi xoa de luu log
                        let doclog = await rbi(inv.id)
                        await dbs.query(`delete from s_inv where id=@1 and status = 1`, [inv.id])
                        await dbs.query(`update  s_inv set cid=null,cde=null where id=@1 and cid=@2`, [inv.pid,inv.id])
                        const sSysLogs = { fnc_id: 'inv_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa hóa đơn id ${inv.id}`, msg_id: inv.id, doc: JSON.stringify(doclog)};
                        logging.ins(req,sSysLogs,next)
                    }
                    status = 1
                    message ='Success'
                } catch (e) {
                    status = 2
                    throw e

      
               }

            res.json({ message: message, status: status })
        }
        catch (err) {
            next(err)
        }
    },
    signdoc: async (req, res, next) => {
        try {
            let doc = req.body
            const org = await ous.gettosign(doc.stax)
            logger4app.debug(org)
            let signtype = org.sign
            let xml, binds, result
            let sql          
            
            if (signtype == 1) {
                const usr = org.usr, pwd = util.decrypt(org.pwd)
                xml = util.j2x(doc, doc.id)
                const arr = [{ inc: id, xml: Buffer.from(xml).toString("base64") }]
                res.json({ lib: usr, sid: 0, pin: pwd, mst: doc.stax, date: new Date(), ref: config.PATH_XML, arr: arr })
            }
            else {
                if (signtype == 2) {
                    const pwd = util.decrypt(org.pwd)
                    const ca = await sign.ca(doc.stax, pwd)
                    xml = sign.sign(ca, util.j2x(doc, doc.id), doc.id)
                }
                else if (signtype == 3) {
                    sql = `select id,doc,bmail,pid from s_inv where id=@1 and stax=@2 and status=2`
                    binds = [doc.id, doc.stax]
                    result = await dbs.query(sql, binds)
                    const rows = result.recordset
                    if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
                    const usr = org.usr, pwd = util.decrypt(org.pwd)
                    const kqs = await fcloud.xml(usr, pwd, doc.stax, rows), kq = kqs[0]
                    xml = kq.xml
                }
                else {
                    xml = await hsm.signxml(doc.stax, util.j2x(doc, doc.id))
                }
            }
            if (!util.isEmpty(doc.bmail)) {
                doc.status = 3
                await email.rsmqmail(doc)
            }
            res.json({ xml: xml})
        }
        catch (err) {
            next(err)
        }

    },
    signxml: async (req, res, next) => {
        try {
            const xml = req.body.xml, stax = req.body.stax, id = req.body.id
            const org = await ous.gettosign(stax)
            logger4app.debug(org)
            let signtype = org.sign
            let binds, result, xmlresult
            let sql

            if (signtype == 1) {
                const usr = org.usr, pwd = util.decrypt(org.pwd)
                xmlresult = util.j2x(doc, doc.id)
                const arr = [{ inc: id, xml: Buffer.from(xml).toString("base64") }]
                res.json({ lib: usr, sid: 0, pin: pwd, mst: stax, date: new Date(), ref: config.PATH_XML, arr: arr })
            }
            else {
                if (signtype == 2) {
                    const pwd = util.decrypt(org.pwd)
                    const ca = await sign.ca(stax, pwd)
                    xmlresult = sign.sign(ca, xml, id)
                }
                else if (signtype == 3) {
                    sql = `select id,doc,bmail,pid from s_inv where id=@1 and stax=@2 and status=2`
                    binds = [id, stax]
                    result = await dbs.query(sql, binds)
                    const rows = result.recordset
                    if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
                    const usr = org.usr, pwd = util.decrypt(org.pwd)
                    const kqs = await fcloud.xml(usr, pwd, stax, rows), kq = kqs[0]
                    xmlresult = kq.xml
                }
                else {
                    xmlresult = await hsm.signxml(stax, xml)
                }
            }
            res.json({ xml: xmlresult})
        }
        catch (err) {
            next(err)
        }

    },
    syncRedisDb: async (req, res, next) => {
        try {
            var sqlinv = 'select max(id) id from s_inv'
            logger4app.debug('sqlinv : ', sqlinv)
            var resultinv = await dbs.query(sqlinv, [])
            var maxid = resultinv.recordset[0].id
            logger4app.debug('resultinv.recordset[0] : ', resultinv.recordset[0])
            logger4app.debug('maxid : ', maxid)
            var keyidinv = `INC`, curid = await redis.get(keyidinv)
            if (curid < maxid) {
                await redis.set(keyidinv, maxid)
            }
            res.json({ result: "1"})
        } catch (err) {
            next(err)
        }
    },
    getol: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            let  query = req.query,ma_gd = query.ma_gd,rows
            const taxc = token.taxc, uid = token.uid,ou = token.ou, name=token.uid, org = await ous.obt(taxc),ous1 =await ous.obid(token.ou)
            let Obj = {
                "DIA_CHI_KH": "baddr","LOAI_KH":"c3","SDT_KH":"btel","EMAIL_KH": "bmail","HINH_THUC_THANH_TOAN":"paym","SO_TK_KH":"bacc"
                , "TEN_KH": "bname", "MST_KH": "btax", "LOAI_TIEN": "curr", "TY_GIA": "exrt", 
                 "TIEN_TRUOC_THUE_NGUYEN_TE": "items.amount", "NOI_DUNG": "items.name",  "THUE_SUAT": "items.vrt","NGAY_GD": "items.c0","MA_GD": "items.c1","TRANS_NO": "items.c4","LOAI_PHI": "items.c6","TIEN_THUE_NGUYEN_TE": "items.vat","TIEN_SAU_THUE_NGUYEN_TE": "items.total","TIEN_SAU_THUE_QUY_DOI": "items.totalv","TIEN_THUE_QUY_DOI": "items.vatv","TIEN_TRUOC_THUE_QUY_DOI": "items.amountv"
               
                };    
                let options = {
                    method: 'post',
                    url: `${config.api_url}/syn/invol`,
                    headers: { 
                      'Authorization': token,
                      'content-type': 'application/json' 
                    },
                    data : {ma_gd:ma_gd}                    
                  };
                  
                await axios(options)
                    .then(function (response) {                     
                         rows = response.data                       
                    })
                    .catch(function (error) {
                        logger4app.debug(error)
                        throw new Error(error)
                    })     
                 
           // let result = await dbs.queryprocedure_ol(ma_gd),ret,note = '',checkerr='',arr_ref=[]
             let result =rows,ret,note = '',checkerr='',arr_ref=[]
            for (let r of result) {
             let   invd = objectMapper(r, Obj),note=''
             let item = invd.items
            if(ous1.code != r.CN_PGD)  throw new Error(`Không có quyền lập dữ liệu của đơn vị khác \n (Denied access for create data of another branches)`)
             if (!item.amount) {
                note += `. Thiếu thành tiền.`
            }
            if(!item.c1) note += `. Thiếu Mã giao dịch.`
            //check so but toan
            if (!invd.bname) note += `. Thiếu Đơn vị mua hàng.`
            if (!invd.baddr) note += `. Thiếu Đơn vị mua hàng.`
            if (!invd.curr) note += `. Thiếu loại tiền.`
            if (!invd.exrt) note += `. Thiếu tỷ giá.`
            //if (!item.vatv) note += `. Thiếu VAT VND.`
            //if (!item.vat) note += `. Thiếu Tiền thuế.`
            if (!item.amount) note += `. Thiếu TIEN_TRUOC_THUE_NGUYEN_TE.`
            if (!item.amountv) note += `.Thiếu TIEN_TRUOC_THUE_QUY_DOI.`
            if (!item.total) note += `. Thiếu TIEN_SAU_THUE_NGUYEN_TE.`
            if (!item.totalv) note += `. Thiếu TIEN_SAU_THUE_QUY_DOI.`
            if (!item.vrt) note += `Thiếu Thue_suat.`
            if(!["0","5","10","-2","-1"].includes(item.vrt)) note += `. Thue_suat sai định dạng.`
            if(["5","10"].includes(item.vrt) && !item.vat) note += `. Thiếu Tiền thuế.`
            if(["5","10"].includes(item.vrt) && !item.vatv) note += `. Thiếu VAT VND.`
            let  items=[]
            items.push(item)
            invd.items=items
            for (let r of arr_ref) {
               if(item.c4==r) note += `. Trùng số bút toán.`
            }
            arr_ref.push(item.c4)
           
            const refno = await invobj.checkrefno(invd)      
            if (refno.length > 0) {
                note +=`. Số bút toán đã tồn tại: ${refno.join()}`
              
            }
            if (invd.bmail && !util.checkemail(invd.bmail)) note += ". Email không hợp lệ. "
            if (invd.curr == 'VND' && invd.exrt != 1) note += ". VND tỉ giá phải bằng 1 "
            if(invd.btax && !util.checkmst(invd.btax.toString())) note += ". MST không hợp lệ . "
            
             r.note = note
             if(note) checkerr=1
            }
            const result1 =result,result2=[]
            
            let invd,invc,items = [],count = 0,rssr02,rssr01,check01=0,check02=0,rsc
            for (let r of result) {
                if(r.LOAI_HD==1) check02 = 1
                else check01 = 1
                if (r.REVERSE =='REVERSE') throw new Error("Không lập được hóa đơn do có giao dịch reverse")
            }
            if(check02){
                rssr02 =  await dbs.query(`select distinct form "form",serial "serial",type "type",priority "priority" from s_serial where taxc=:1 and form='01GTKT0/002'  and fd<=:2 and status=1 and uses=1 order by priority`, [taxc, new Date()])
                if (rssr02.rows.length <1) throw new Error("Không tìm thấy dải ký hiệu đang hiệu lực tích hơp cho form 01GTKT0/002")
                rsc = await dbs.query(`select count(*) rc from s_inv where idt>:1 and status > 1 and status<>4 and form = '01GTKT0/002' and serial=:2 `, [moment(new Date()).endOf('day').toDate(),rssr02.rows[0].serial])
          
                if (rsc.rows[0].rc > 0) note= `Tồn tại HĐ của dải ${rssr02.rows[0].form} - ${rssr02.rows[0].serial}  có ngày lớn hơn ${moment(new Date()).format('DD/MM/YYYY')} và đã cấp số \n (Invoice has a date larger than ${moment(new Date()).format('DD/MM/YYYY')} and issued a number)`
            }
            if(check01){
                rssr01 =  await dbs.query(`select distinct form "form",serial "serial",type "type",priority "priority"  from s_serial where taxc=:1 and form='01GTKT0/001'  and fd<=:2 and status=1 and uses=1 order by priority`, [taxc, new Date()])
                if (rssr01.rows.length <1) throw new Error("Không tìm thấy dải ký hiệu đang hiệu lực tích hơp cho form 01GTKT0/001")
                rsc = await dbs.query(`select count(*) rc from s_inv where idt>:1 and status > 1 and status<>4 and form = '01GTKT0/001' and serial=:2 `, [moment(new Date()).endOf('day').toDate(),rssr01.rows[0].serial])
          
                if (rsc.rows[0].rc > 0) note += `Tồn tại HĐ của dải ${rssr01.rows[0].form} - ${rssr01.rows[0].serial}  có ngày lớn hơn ${moment(new Date()).format('DD/MM/YYYY')} và đã cấp số `
            }
           
            // let line =1,sum=0,sumv=0,vat=0,vatv=0,total=0,totalv=0,key='',length = result.length,STT=0
            // for (let r of result) {
            //     count++
            //     invd = objectMapper(r, Obj)
            //     let item = invd.items
            //     let keyc =(r.LOAI_HD?r.LOAI_HD:'') + '_' + r.MA_GD + '_' + r.THUE_SUAT +'_'+ r.TY_GIA 

            //     if (item.vrt >= 0) item.vrn = `${item.vrt}%`
            //     else item.vrn = "\\"                
            //     item.line = line++
                
               
            //     if(key == keyc){
            //         sum += Number(item.amount)
            //         sumv += Number(item.amountv)
            //         vat += Number(item.vat?item.vat:0)
            //         vatv += Number(item.vatv?item.vatv:0)
            //         total += Number(item.total)
            //         totalv += Number(item.totalv)
            //         items.push(item)
                   
                   
            //     }else{
                   
            //         if(count == 1){  
            //             invc = invd       
            //             invc.form = r.LOAI_GD == 1 ?'01GTKT0/002':'01GTKT0/001'    
            //             invc.serial = r.LOAI_GD == 1?rssr02.rows[0].serial:rssr01.rows[0].serial         
            //             sum += Number(item.amount)
            //             sumv += Number(item.amountv)
            //             vat += Number(item.vat?item.vat:0)
            //             vatv += Number(item.vatv?item.vatv:0)
            //             total += Number(item.total)
            //             totalv += Number(item.totalv)
            //             items.push(item)
            //         }else{
            //             //let id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC,id)
            //             STT++
            //             invc.sum = sum
            //             invc.sumv = sumv
            //             invc.vat = vat
            //             invc.vatv = vatv
            //             invc.total = total
            //             invc.totalv = totalv
            //             invc.stax = org.taxc
            //             invc.sname = org.name
            //             invc.saddr = org.addr
            //             invc.smail = org.mail
            //             invc.stel = org.tel
            //             invc.taxo = org.taxo
            //             invc.sacc = org.acc
            //             invc.sbank = org.bank      
            //            // invc.form = r.LOAI_GD == 1 ?'01GTKT0/002':'01GTKT0/001'
            //             //invc.serial = r.LOAI_GD == 1?rssr02.rows[0].serial:rssr01.rows[0].serial
            //             invc.type = '01GTKT'                     
            //             invc.stax = taxc
            //             invc.ou = ou
            //             invc.uc = name                           
            //           //  invd.sec = sec
            //           invc.status = 1                         
            //           invc.idt = moment(new Date()).format("YYYY-MM-DD HH:mm:ss")     
            //           invc.stt=   STT                 
            //            // invd.id = id  
            //            invc.items = items
            //            invc.word = n2w.d2wx(Math.round(invc.totalv * 100)/100, "VND") 
            //            invc.tax = await tax(items, Number(invc.exrt))
            //             // const refno = await invobj.checkrefno(invc)      
            //             // if (refno.length > 0) {
            //             //     note +=`Số bút toán đã tồn tại: ${refno.join()} \n(Entry number already exists: ${refno.join()})`
            //             //     invc.note = `Số bút toán đã tồn tại: ${refno.join()} \n(Entry number already exists: ${refno.join()})`
            //             // }
            //              //if (invc.bmail && !util.checkemail(invc.bmail)) note += "Email không hợp lệ (Invalid email). "
            //             // if (invc.curr == 'VND' && invc.exrt !== 1) note += "VND tỉ giá phải bằng 1 \n (VND exchange rate must be 1)"
            //             // if(invc.btax && !util.checkmst(invc.btax.toString())) note += "MST không hợp lệ (Invalid tax code). "
                         
            //             result2.push(invc)
            //             invc = invd
            //             invc.form = r.LOAI_GD == 1 ?'01GTKT0/002':'01GTKT0/001'
            //             invc.serial = r.LOAI_GD == 1?rssr02.rows[0].serial:rssr01.rows[0].serial
            //             //khoi tao cho thang tiep theo
            //             items =[]
            //             note =''
            //             sum = 0
            //             sumv = 0
            //             vat = 0 
            //             vatv = 0
            //             total = 0
            //             totalv = 0
            //             line =0 //reset line
            //             sum += Number(item.amount)
            //             sumv += Number(item.amountv)
            //             vat += Number(item.vat?item.vat:0)
            //             vatv += Number(item.vatv?item.vatv:0)
            //             total += Number(item.total)
            //             totalv += Number(item.totalv)
            //             items.push(item)
            //         }

            //         key = keyc
            //     }
            //     //dong cuoi
            //     if (length==count){
            //         STT++ 
            //        // let id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC,id)
            //        invc.sum = sum
            //        invc.sumv = sumv
            //        invc.vat = vat
            //        invc.vatv = vatv
            //        invc.total = total
            //        invc.totalv = totalv
            //        invc.stax = org.taxc
            //        invc.sname = org.name
            //        invc.saddr = org.addr
            //        invc.smail = org.mail
            //        invc.stel = org.tel
            //        invc.taxo = org.taxo
            //        invc.sacc = org.acc
            //        invc.sbank = org.bank      
            //        invc.form = r.LOAI_GD == 1 ?'01GTKT0/002':'01GTKT0/001'
            //        invc.serial = r.LOAI_GD == 1?rssr02.rows[0].serial:rssr01.rows[0].serial
            //        invc.type = '01GTKT'                     
            //        invc.stax = taxc
            //        invc.tax = await tax(items, Number(invc.exrt))
            //        invc.ou = ou
            //        invc.uc = name                           
            //        // invd.sec = sec
            //        invc.status = 1                         
            //        invc.idt = moment(new Date()).format("YYYY-MM-DD HH:mm:ss")                          
            //        // invd.id = id  
            //        invc.stt=   STT 
            //        invc.items = items
            //        invc.word = n2w.d2wx(Math.round(invc.totalv * 100)/100, "VND") 
            //         const refno = await invobj.checkrefno(invc)      
            //         // if (refno.length > 0) {
            //         //     note +=`Số bút toán đã tồn tại: ${refno.join()} \n(Entry number already exists: ${refno.join()})`
            //         //     invc.note = `Số bút toán đã tồn tại: ${refno.join()} \n(Entry number already exists: ${refno.join()})`
            //         // }
            //         //invc = invd
            //         result2.push(invc)
            //         invc ={}
            //     }
            //     }
                if(note) note='Tồn tại giao dịch lỗi'
                ret = { data: result1,  note:checkerr }
            res.json(ret)
        } catch (err) {
            next(err)
        }
       },
       getinv: async (req, res, next) => {
        try {
            const token = SEC.decode(req),body = req.body,trans=body.trans
          
            const taxc = token.taxc,ou = token.ou, name=token.uid, org = await ous.obt(taxc)
          
            if (grant && token.ou !== token.u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)
            let Obj = {
                "DIA_CHI_KH": "baddr","LOAI_KH":"c3","SDT_KH":"btel","EMAIL_KH": "bmail","HINH_THUC_THANH_TOAN":"paym","SO_TK_KH":"bacc"
                , "TEN_KH": "bname", "MST_KH": "btax", "LOAI_TIEN": "curr", "TY_GIA": "exrt", 
                 "TIEN_TRUOC_THUE_NGUYEN_TE": "items.amount", "NOI_DUNG": "items.name",  "THUE_SUAT": "items.vrt","NGAY_GD": "items.c0","MA_GD": "items.c1","TRANS_NO": "items.c4","LOAI_PHI": "items.c6","TIEN_THUE_NGUYEN_TE": "items.vat","TIEN_SAU_THUE_NGUYEN_TE": "items.total","TIEN_SAU_THUE_QUY_DOI": "items.totalv","TIEN_THUE_QUY_DOI": "items.vatv","TIEN_TRUOC_THUE_QUY_DOI": "items.amountv"
               
                };    
            let   rssr01 =  await dbs.query(`select distinct form "form",serial "serial",type "type",priority "priority"  from s_serial where taxc=:1 and form='01GTKT0/001'  and fd<=:2 and status=1 and uses=1 order by priority`, [taxc, new Date()])
            let    rssr02 =  await dbs.query(`select distinct form "form",serial "serial",type "type",priority "priority" from s_serial where taxc=:1 and form='01GTKT0/002'  and fd<=:2 and status=1 and uses=1 order by priority`, [taxc, new Date()])
            let line =1,sum=0,sumv=0,vat=0,vatv=0,total=0,totalv=0,key='',length = trans.length,STT=0,invs=[],count=0,invd={},invc={},items=[]
            for (let r of trans) {
                count++
                invd = objectMapper(r, Obj)
                let item = invd.items
                let keyc =(r.LOAI_HD?r.LOAI_HD:'') + '_' + r.MA_GD + '_' + r.THUE_SUAT +'_'+ r.TY_GIA 

                if (item.vrt >= 0) item.vrn = `${item.vrt}%`
                else item.vrn = "\\"                
                item.line = line++
                
               
                if(key == keyc){
                    sum += Number(item.amount)
                    sumv += Number(item.amountv)
                    vat += Number(item.vat?item.vat:0)
                    vatv += Number(item.vatv?item.vatv:0)
                    total += Number(item.total)
                    totalv += Number(item.totalv)
                    items.push(item)
                   
                   
                }else{
                   
                    if(count == 1){  
                        invc = invd       
                        invc.form = r.LOAI_HD == 1 ?'01GTKT0/002':'01GTKT0/001'    
                        invc.serial = r.LOAI_HD == 1?rssr02.rows[0].serial:rssr01.rows[0].serial         
                        sum += Number(item.amount)
                        sumv += Number(item.amountv)
                        vat += Number(item.vat?item.vat:0)
                        vatv += Number(item.vatv?item.vatv:0)
                        total += Number(item.total)
                        totalv += Number(item.totalv)
                        items.push(item)
                    }else{
                        //let id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC,id)
                        STT++
                        invc.sum = sum
                        invc.sumv = sumv
                        invc.vat = vat
                        invc.vatv = vatv
                        invc.total = total
                        invc.totalv = totalv
                        invc.stax = org.taxc
                        invc.sname = org.name
                        invc.saddr = org.addr
                        invc.smail = org.mail
                        invc.stel = org.tel
                        invc.taxo = org.taxo
                        invc.sacc = org.acc
                        invc.sbank = org.bank      
                     
                        invc.type = '01GTKT'                     
                        invc.stax = taxc
                        invc.ou = ou
                        invc.uc = name                           
                    
                        invc.status = 1                         
                        invc.idt = moment(new Date()).format("YYYY-MM-DD HH:mm:ss")     
                        invc.stt=   STT                 
                      
                        invc.items = items
                        invc.word = n2w.d2wx(Math.round(invc.totalv * 100)/100, "VND") 
                        invc.tax = await tax(items, Number(invc.exrt))
                       
                         
                        invs.push(invc)
                        invc = invd
                        invc.form = r.LOAI_HD == 1 ?'01GTKT0/002':'01GTKT0/001'
                        invc.serial = r.LOAI_HD == 1?rssr02.rows[0].serial:rssr01.rows[0].serial
                        //khoi tao cho thang tiep theo
                        items =[]
                      
                        sum = 0
                        sumv = 0
                        vat = 0 
                        vatv = 0
                        total = 0
                        totalv = 0
                        line =0 //reset line
                        sum += Number(item.amount)
                        sumv += Number(item.amountv)
                        vat += Number(item.vat?item.vat:0)
                        vatv += Number(item.vatv?item.vatv:0)
                        total += Number(item.total)
                        totalv += Number(item.totalv)
                        items.push(item)
                    }

                    key = keyc
                }
                //dong cuoi
                if (length==count){
                    STT++ 
                
                   invc.sum = sum
                   invc.sumv = sumv
                   invc.vat = vat
                   invc.vatv = vatv
                   invc.total = total
                   invc.totalv = totalv
                   invc.stax = org.taxc
                   invc.sname = org.name
                   invc.saddr = org.addr
                   invc.smail = org.mail
                   invc.stel = org.tel
                   invc.taxo = org.taxo
                   invc.sacc = org.acc
                   invc.sbank = org.bank      
                   invc.form = r.LOAI_HD == 1 ?'01GTKT0/002':'01GTKT0/001'
                   invc.serial = r.LOAI_HD == 1?rssr02.rows[0].serial:rssr01.rows[0].serial
                   invc.type = '01GTKT'                     
                   invc.stax = taxc
                   invc.tax = await tax(items, Number(invc.exrt))
                   invc.ou = ou
                   invc.uc = name                           
                   // invd.sec = sec
                    invc.status = 1                         
                    invc.idt = moment(new Date()).format("YYYY-MM-DD HH:mm:ss")                          
                   // invd.id = id  
                    invc.stt=   STT 
                    invc.items = items
                    invc.word = n2w.d2wx(Math.round(invc.totalv * 100)/100, "VND") 
                    invs.push(invc)
                    invc ={}
                 }
                }
              
               
            res.json({ data: invs })
        } catch (err) {
            next(err)
        }
       },
       sav_ol: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, invs = body.invs,trans=body.trans
            const taxc = token.taxc, uid = token.uid,ou = token.ou
            const org = await ous.obt(taxc), signtype = org.sign
            if (grant && token.ou !== token.u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)
            for (let inv of invs) {
                const hdb = await invobj.checkrefno(inv)
                if (hdb.length > 0) throw new Error(`Số bút toán đã tồn tại: ${hdb.join()} \n(Entry number already exists: ${hdb.join()})`)   
                let   rsc = await dbs.query(`select count(*) rc from s_inv where idt>:1 and status > 1 and status<>4 and form = :2 and serial=:3 `, [moment(new Date()).endOf('day').toDate(),inv.form,inv.serial])
          
                if (rsc.rows[0].rc > 0) throw new Error (`Tồn tại HĐ của dải ${inv.form} - inv.serial}  có ngày lớn hơn ${moment(new Date()).format('DD/MM/YYYY')} và đã cấp số \n (Invoice has a date larger than ${moment(new Date()).format('DD/MM/YYYY')} and issued a number)`)          
            }
            let count=0      
            for (let inv of invs) {       
                count++
                const  id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC,id)
                let seq, s7,result,row                   
               
                inv.id = id
                inv.sec = sec               
               
                seq = await SERIAL.sequence(inv.stax, inv.form, inv.serial,inv.idt)
                s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                inv.seq = s7
                inv.status = 3               
                inv.ou= ou
              
                if(signtype != 2)  throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)')
                if (signtype == 2) {
                    //hunglq chinh sua thong bao loi
                    let ca, pwd
                    try {
                        pwd = util.decrypt(org.pwd)
                        ca = await sign.ca(taxc, pwd)
                    } catch (err) {
                        throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)')
                    }
                    const doc = inv, xml = sign.sign(ca, util.j2x(inv, id), id)
                    await dbs.query(`insert into s_inv (id,sec,idt,doc,ou,uc,xml) values (:id,:sec,:idt,:doc,:ou,:uc,:xml)`, { id: id, sec: sec, idt: new Date(inv.idt), doc: JSON.stringify(inv), ou: ou, uc: uid,xml })
                    await invobj.saveTrans(trans,inv,s7,ou) //chi insert 1 lần
                    
                    await invobj.saverefno(inv)
                  
                    if (useJobSendmail) {
                        await invobj.insertDocTempJob(id, inv, xml)
                    } else if (useRedisQueueSendmail) {
                        if (!util.isEmpty(inv.bmail)) {
                            let mailstatus = await email.rsmqmail(inv)
                            if (mailstatus) await invobj.updateSendMailStatus(inv)
                        }
                        if (!util.isEmpty(inv.btel)) {
                            let smsstatus = await sms.smssend(inv)
                            if (smsstatus) await invobj.updateSendSMSStatus(inv, smsstatus)
                        }
                    }
                    const sSysLogs = { fnc_id: 'inv_post', src: config.SRC_LOGGING_DEFAULT, dtl: `Lập hóa đơn online id ${id}`, msg_id: id, doc: JSON.stringify(inv)  };
                    logging.ins(req, sSysLogs, next)
                   
                }
               
              
            }
            res.json(1)
               
           
        } catch (err) {
            next(err)
        }
    },
    getTemp: async (req, res, next) => {
        try {
            const query = req.query, id = query.id, token = SEC.decode(req)
            let sql, result, rows
            sql=`select * from bvb_online where ma_gd = ${id}`
            result = await dbs.query(sql,[])
            rows = result.rows[0]
            let doc = {}
            doc.ic = ""
            doc.idt = rows.NGAY_GD
            doc.buyer = rows.TEN_KH
            doc.bname = ""
            doc.btax = rows.MST_KH
            doc.baddr = rows.DIA_CHI_KH
            doc.btel = rows.SDT_KH
            doc.bmail = rows.EMAIL_KH
            doc.paym = rows.HINH_THUC_THANH_TOAN
            doc.curr = rows.LOAI_TIEN
            doc.exrt = Number(rows.TY_GIA)
            doc.sum = Number(rows.TIEN_TRUOC_THUE_NGUYEN_TE)
            doc.sumv = Number(rows.TIEN_TRUOC_THUE_QUY_DOI)
            doc.vat = Number(rows.TIEN_THUE_NGUYEN_TE)
            doc.vatv = Number(rows.TIEN_THUE_QUY_DOI)
            doc.total = Number(rows.TIEN_SAU_THUE_NGUYEN_TE)
            doc.totalv = Number(rows.TIEN_SAU_THUE_QUY_DOI)
            doc.type = rows.LOAI_HD
            let item = [], items = {}, tax = [], taxs = {}
            //Tax
            taxs.vrt = rows.THUE_SUAT
            taxs.vrn = rows.THUE_SUAT+'%'
            tax.push(taxs)
            doc.tax = tax
            //item
            items.name = rows.CN_PGD
            items.unit = rows.DON_VI_TINH
            items.quantity = Number(rows.SO_LUONG)
            items.price = Number(rows.DON_GIA)
            items.vrt = rows.THUE_SUAT
            item.push(items)
            doc.items = item
            doc.stax = ""
            doc.form = "01GTKT0/001"
            doc.serial  = ""
            doc.name = ""
            doc.sname = "NGÂN HÀNG THƯƠNG MẠI CỔ PHẦN BẢO VIỆT"
            doc.saddr = "Tầng 1 và tầng 5, tòa nhà Corner Stone, Số 16 đường Phan Chu - Phường Phan Chu Trinh - Quận Hoàn Kiếm - Hà Nội, Phường Phan Chu Trinh, Quận Hoàn Kiếm, Hà Nội"
            doc.smail = ""
            doc.stell = "024-37551188"
            doc.taxo = ""
            doc.sacc = ""
            doc.sbank = ""
            logger4app.debug(doc);
            const org = await ous.org(token), idx = org.temp, fn = util.fn(doc.form, idx), tmp = await util.template(fn)
            res.json({ status: '2', doc: doc, tmp: tmp })
        } catch (err) {
            next(err)
        }
    }
}
module.exports = Service