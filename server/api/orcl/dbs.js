'use strict'
const oracledb = require('oracledb')
const config = require("../config")
const logger4app = require("../logger4app")
const decrypt = require('../encrypt')
oracledb.autoCommit = true
oracledb.fetchAsString = [oracledb.CLOB]
//oracledb.DATE
const Service = {
    initialize: async () => {
        let poolConfig = JSON.parse(JSON.stringify(config.poolConfig))
        poolConfig.password = decrypt.decrypt(poolConfig.password)
        await oracledb.createPool(poolConfig)
        logger4app.debug('Connection pool started')
    },
    close: async () => {
        await oracledb.getPool().close()
        logger4app.debug('Pool closed')
    },
    getConn: (autoCommitx) => {
        return new Promise(async (resolve, reject) => {
            let conn
            try {
                //console.time('getConn')
                try {
                    if(autoCommitx == false) {
                        oracledb.autoCommit = false
                    } else {
                        oracledb.autoCommit = true
                    }
                    conn = await oracledb.getConnection()
                } catch (e) {
                    // logger4app.debug(`dbs.getConn`, e)
                    if (String(e.message).toUpperCase().indexOf("NJS-047") >= 0) {
                        await Service.initialize()
                        conn = await oracledb.getConnection()
                    } else throw e
                }
                resolve(conn)
            }
            catch (err) {
                reject(err)
            }
        })
    },
    closeConn: async conn => {
        if (conn) {
            try {
                //console.time('closeConn')
                await conn.close()
                //console.timeEnd('closeConn')
            }
            catch (err) {
                logger4app.debug(err)
            }
        }
    },
    execute: (sql, binds = [], opts = {}) => {
        return new Promise(async (resolve, reject) => {
            let conn
            opts.outFormat = oracledb.OUT_FORMAT_OBJECT
            try {
                conn = await Service.getConn()
                const result = await conn.execute(sql, binds, opts)
                resolve(result)
            }
            catch (err) {
                reject(err)
            }
            finally {
                Service.closeConn(conn)
            }
        })
    },
    executereportvat: (sqlsum, sqlfetch, binds = [], opts = {}) => {
        return new Promise(async (resolve, reject) => {
            let conn
            opts.outFormat = oracledb.OUT_FORMAT_OBJECT
            try {
                conn = await Service.getConn()
                conn.autoCommit = false
                await conn.execute(sqlsum, binds, opts)
                const result = await conn.execute(sqlfetch, [], opts)
                resolve(result)
            }
            catch (err) {
                reject(err)
            }
            finally {
                Service.closeConn(conn)
            }
        })
    },
    query: (sql, binds = [], opts = {}) => {
        return new Promise(async (resolve, reject) => {
            let conn
            opts.outFormat = oracledb.OUT_FORMAT_OBJECT
            try {
               
                conn = await Service.getConn()
              
                const result = await conn.execute(sql, binds, opts)
               
                resolve(result)
            }
            catch (err) {
                reject(err)
            }
            finally {
                Service.closeConn(conn)
            }
        })
    },
    queryConn: (sql, binds = [], opts = {}, conn) => {
        return new Promise(async (resolve, reject) => {
            let conn
            opts.outFormat = oracledb.OUT_FORMAT_OBJECT
            try {
               
                const result = await conn.execute(sql, binds, opts)
               
                resolve(result)
            }
            catch (err) {
                reject(err)
            }
            finally {
                Service.closeConn(conn)
            }
        })
    },
    queryprocedure_ol: (tran_no) => {
        return new Promise(async (resolve, reject) => {
            let conn
          
            try {
               
                conn = await Service.getConn()
              
                const result = await conn.execute( `BEGIN ${config.PROCEDURE}(:PI_TRANS_ID, :PO_RESULT,:PO_RES_CODE,:PO_RES_DES); END;`,
                {  // bind variables
                    PI_TRANS_ID:   tran_no,
                    PO_RESULT: { dir: oracledb.BIND_OUT, type: oracledb.CURSOR},
                    PO_RES_CODE: { dir: oracledb.BIND_OUT, type: oracledb.STRING},
                    PO_RES_DES: { dir: oracledb.BIND_OUT, type: oracledb.STRING},
                }, { outFormat: oracledb.OUT_FORMAT_OBJECT })
                const cursor = result.outBinds.PO_RESULT;
                const queryStream = cursor.toQueryStream();
                let row,rows=[];
                const consumeStream = new Promise((resolve, reject) => {
                queryStream.on('data', function(row) {
                   // logger4app.debug(row);
                    rows.push(row)
                });
                queryStream.on('error', reject);
                queryStream.on('close', resolve);
                });

                await consumeStream;
                resolve(rows)
               
            }
            catch (err) {
                reject(err)
            }
            finally {
                Service.closeConn(conn)
            }
        })
    },
    executeMany: (sql, binds = [], opts = {}) => {
        return new Promise(async (resolve, reject) => {
            let conn
            try {
                conn = await Service.getConn()
                const result = await conn.executeMany(sql, binds, opts)
                resolve(result)
            }
            catch (err) {
                reject(err)
            }
            finally {
                Service.closeConn(conn)
            }
        })
    },
    xstream: (sql, binds = []) => {
        return new Promise(async (resolve, reject) => {
            try {
                let conn, stream, arr = []
                conn = await Service.getConn()
                stream = await conn.queryStream(sql, binds, { outFormat: oracledb.OUT_FORMAT_OBJECT })
                stream.on("error", async (err) => {
                    await Service.closeConn(conn)
                    reject(err)
                })
                stream.on("data", row => {
                    arr.push(row)
                })
                stream.on("close", async () => {
                    await Service.closeConn(conn)
                    resolve(arr)
                })
            }
            catch (err) {
                reject(err)
            }
        })
    }

}
module.exports = Service