
"use strict"
const moment = require("moment")
const sec = require("../sec")
const dbs = require("./dbs")
const util = require("../util")
const config = require("../config")
const logger4app = require("../logger4app")
let axios = require('axios')
const UPLOAD_MINUTES = config.upload_minute
const mfd = config.mfd
const ENT = config.ent
const formatItems = (rows) => {
    for (const row of rows) {
		if (!row.unit) row.unit = ""
        row.price = util.fn2(row.price)
        row.amount = util.fn2(row.amount)
        if (!row.quantity) row.quantity = ""
        row.name=String(row.name).replace("<br>","")
    }
    return rows
}
const Service = {
    minutesReplace: async (req, res, next) => {
        try {
            const id = req.body.id, adj = req.body.inv.adj
            const result = await dbs.query(`select doc "doc" from s_inv where id=:1`, [id])
            const doc = JSON.parse(result.rows[0].doc)
            doc.idt = moment(doc.idt).format(mfd)
            doc.items = formatItems(doc.items)
            doc.vatv = util.fn2(doc.vatv)
            doc.sumv = util.fn2(doc.sumv)
            doc.totalv = util.fn2(doc.totalv)
            doc.ref = adj.ref
            doc.rea = adj.rea
            doc.stel = (doc.stel) ? doc.stel : ''
            doc.saddr = (doc.saddr) ? doc.saddr : ''
            doc.smail = (doc.smail) ? doc.smail : ''
            doc.btel= (doc.btel) ? doc.btel : ''
            doc.bmail = (doc.bmail) ? doc.bmail : ''
            doc.btax= (doc.btax) ? doc.btax : ''
            doc.rdt = moment(adj.rdt).format(mfd)
            doc.isvat = doc.type == "01GTKT" ? true : false
            let ndocx
            if (ENT=="bvb") {
                ndocx = "mrep_bvb.docx"
            } else {
                ndocx = "mrep.docx"
            } 
            const out = util.docxrender(doc, doc.type.toLowerCase() + ndocx)
            res.end(out, "binary")
        } catch (err) {
            next(err)
        }
    },
    minutesCancel: async (req, res, next) => {
        try {
            const doc = req.body, adj = doc.adj
            doc.idt = moment(doc.idt).format(mfd)
            doc.items = formatItems(doc.items)
            doc.vatv = util.fn2(doc.vatv)
            doc.sumv = util.fn2(doc.sumv)
            doc.totalv = util.fn2(doc.totalv)
            doc.ref = adj.ref
            doc.rea = adj.rea
            doc.btel= (doc.btel) ? doc.btel : ''
            doc.bmail = (doc.bmail) ? doc.bmail : ''
            doc.btax= (doc.btax) ? doc.btax : ''
            doc.rdt = moment(adj.rdt).format(mfd)
            doc.isvat = doc.type == "01GTKT" ? true : false
            const out = util.docxrender(doc, doc.type.toLowerCase() + "mcan.docx")
            res.end(out, "binary")
        } catch (err) {
            next(err)
        }
    },
    minutesCreate: async (req, res, next) => {
        try {
            const doc = req.body, adj = doc.adj
            doc.idt = moment(doc.idt).format(mfd)
            doc.ref = adj.ref
            doc.rea = adj.rea
            doc.des = adj.des
            doc.stel= (doc.stel) ? doc.stel : ''
            doc.smail = (doc.smail) ? doc.smail : ''
			doc.btel= (doc.btel) ? doc.btel : ''
            doc.bmail = (doc.bmail) ? doc.bmail : ''
            doc.btax= (doc.btax) ? doc.btax : ''
            doc.rdt = moment(adj.rdt).format(mfd)
            doc.items0 = formatItems(doc.items0)
            doc.items1 = formatItems(doc.items1)
            if (["vib"].includes(config.ent)) doc.items1 = formatItems(doc.items)
            doc.isvat = doc.type == "01GTKT" ? true : false
            if (!doc.seq) doc.seq = "............"
            const out = util.docxrender(doc, doc.type.toLowerCase() + "madj.docx")
            res.end(out, "binary")
        } catch (err) {
            next(err)
        }
    },
    minutesCustom: async (req, res, next) => {
        try {
            const token = sec.decode(req), id = req.params.id, download = req.query.download
            let result = await dbs.query(`select pid "pid",type "type",status "status",adjtyp "adjtyp",doc "doc" from s_inv where id=:1`, [id])
            let row = result.rows[0], type = row.type, status = row.status, doc = JSON.parse(row.doc), fn, pid = row.pid
            if (UPLOAD_MINUTES && download) {
                if(["vib"].includes(config.ent))
                {
                    result = await dbs.query(`select file_name "file_name",id_file "id_file" from s_inv where id=:1`, [id])
                    row = result.rows[0]
                    let file_name = row.file_name,ids=row.id_file

                    if(!ids || ids=='') throw new Error(`Hóa đơn không có biên bản.\n (Invocies don't have Minutes)`)
                    let options = {
                        method: 'post',
                        url: `${config.api_cmis}/getcmis`,
                        headers: { 
                          'Authorization': token, 
                          "content-type": "text/plain",
                          'Cookie': 'render-complete=true'
                        },
                        data : ids                    
                      };
                      
                    await axios(options)
                        .then(function (response) {
                            logger4app.debug(response.data);
                            if(response.data.status == 1){
                                let file = Buffer.from(response.data.data,"base64")
                                res.end(file, "binary")
                            }else throw new Error(`Không tìm thấy biên bản trên hệ thống ECM.\n (No records found on ECM system)`)
                            
                          
                        })
                        .catch(function (error) {
                            logger4app.debug(error)
                            throw new Error(error)
                        })
                }else{
                    let typ, iid = pid
                    if (status == 4) {
                        typ = "can"
                        iid = id
                    } else {
                        let adjtyp = row.adjtyp
                        if (adjtyp == 1) {
                            typ = "rep"
                        } else if (adjtyp == 2) {
                            typ = "adj"
                        }
                    }
                    if (!typ) {
                        iid = id
                        typ = "normal"
                    }
                    let minutes = await dbs.query(`select id "id", type "type", content "content" from s_inv_file where id=:1 and type=:2`, [iid, typ])
                    if (!minutes.rows.length) throw new Error("Không tìm thấy biên bản! \n (Cannot find the minute)")
                    let file = Buffer.from(minutes.rows[0].content,"base64")
                    res.end(file, "binary")
                }
                
            } else {
                if (status == 4) {
                    let cancel = doc.cancel
                    if (!cancel) throw new Error(`Hóa đơn không có biên bản.\n (Invocies don't have Minutes)`)
                    doc.idt = moment(doc.idt).format(mfd)
                    doc.items = formatItems(doc.items)
                    doc.vatv = util.fn2(doc.vatv)
                    doc.sumv = util.fn2(doc.sumv)
                    doc.totalv = util.fn2(doc.totalv)
                    doc.ref = cancel.ref
                    doc.rea = cancel.rea
					doc.btel= (doc.btel) ? doc.btel : ''
					doc.bmail = (doc.bmail) ? doc.bmail : ''
					doc.btax= (doc.btax) ? doc.btax : ''
                    doc.rdt = moment(cancel.rdt).format(mfd)
                    doc.isvat = type == "01GTKT" ? true : false
                    fn = doc.type.toLowerCase() + "mcan.docx"
                }
                else {
                    let adjtyp = row.adjtyp
                    if (adjtyp && pid) {
                        let adj = doc.adj
                        result = await dbs.query(`select doc "doc" from s_inv where id=:1`, [pid])
                        row = result.rows[0]
                        let pdoc = JSON.parse(row.doc)
                        if (adjtyp == 1) {
                            doc = pdoc
                            doc.idt = moment(doc.idt).format(mfd)
                            doc.items = formatItems(doc.items)
                            doc.vatv = util.fn2(doc.vatv)
                            doc.sumv = util.fn2(doc.sumv)
                            doc.totalv = util.fn2(doc.totalv)
                            doc.ref = adj.ref
                            doc.rea = adj.rea
							doc.btel= (doc.btel) ? doc.btel : ''
							doc.bmail = (doc.bmail) ? doc.bmail : ''
							doc.btax= (doc.btax) ? doc.btax : ''
                            doc.rdt = moment(adj.rdt).format(mfd)
                            doc.isvat = type == "01GTKT" ? true : false
                            fn = doc.type.toLowerCase() + "mrep.docx"
                        }
                        else if (adjtyp == 2) {
                            let items1 = doc.items, items0 = pdoc.items
                            doc.idt = moment(doc.idt).format(mfd)
                            doc.ref = adj.ref
                            doc.rea = adj.rea
                            doc.des = adj.des
							doc.btel= (doc.btel) ? doc.btel : ''
							doc.bmail = (doc.bmail) ? doc.bmail : ''
							doc.btax= (doc.btax) ? doc.btax : ''
                            doc.rdt = moment(adj.rdt).format(mfd)
                            doc.items0 = formatItems(items0)
                            doc.items1 = formatItems(items1)
                            doc.isvat = type == "01GTKT" ? true : false
                            if (!doc.seq) doc.seq = "............"
                            fn = doc.type.toLowerCase() + "madj.docx"
                        }
                    }
                }
                if (fn) {
                    for (let i in doc) if (util.isEmpty(doc[i])) doc[i] = ""
                    for (let row of doc.items) 
                    {
                        for (let i in row)
                        {
                            if (util.isEmpty(row[i])) row[i] = ""
                        }
                       
                        if (typeof  row.unit=="undefined") 
                        {
                            row.unit="";
                        }
                    }
                    const out = util.docxrender(doc, fn)
                    res.end(out, "binary")
                }
                else {
                    throw new Error(`Hóa đơn không có biên bản.\n (Invocies don't have Minutes)`)
                }
            }
        } catch (err) {
            next(err)
        }
    },
    upload: async (req, res, next) => {
        let ok = false,err,id_f
        try {
            const token = sec.decode(req), taxc = token.taxc
            const file = req.file, body = req.body, type = body.type.toString().toLowerCase(), id = body.id
            let content = file.buffer, contentb64 = content.toString('base64')
            let sql, result, binds
            if(config.ent=='vib'){//day sang ecm
                let name = moment(new Date()).format('YYYYMMDDHHmmss')+'_'+ file.originalname,id_file
                let flag = body.flag
                let options = {
                    method: 'post',
                    url: `${config.api_cmis}/createcmis`,
                    headers: { 
                      'Authorization': token, 
                      'Content-Type': 'application/json', 
                      'Cookie': 'render-complete=true'
                    },
                    data : {data:contentb64,name:name},
                    json: true    
                  };
                  if( (typeof id == "undefined") || id=="undefined" || !id ) {//them moi
                    await axios(options)
                    .then(function (response) {
                        logger4app.debug(response.data);
                     
                        ok = true
                        id_file = response.data
                        id_f = {id:id_file,name:name}
                        id_f =  JSON.stringify(id_f)
                    })
                    .catch(function (error) {
                        logger4app.debug(error)
                        err = "file đính kèm không thành công. Có lỗi xảy ra khi đẩy file sang ECM :"+ error
                    })
                  }else{
                   
                    await axios(options)
                      .then(function (response) {
                          logger4app.debug(response.data);
                          ok = true
                          id_file = response.data
                       
                      })
                      .catch(function (error) {
                          logger4app.debug(error)
                          err = "file đính kèm không thành công. Có lỗi xảy ra khi đẩy file sang ECM :"+ error
                      })
                      if(id_file){
                        let ids=[]
                        sql = `select id_file "id_file" from  s_inv  where id=:1 `
                        binds = [id]
                        result = await dbs.query(sql, binds)
                        let id_files = result.rows[0].id_file
                        if(id_files)ids =JSON.parse(id_files)
                         
                          ids.push({id:id_file,name:name})
                          sql = `update s_inv set id_file=:1 where id=:2 `
                          binds = [JSON.stringify(ids), id]
                          result =  dbs.query(sql, binds)
                          ok = true
                         // id_f = id_file
                      }
                  }
                  
                   
                
            }else{
                sql = `select 1 from s_inv_file where id=:1 and type=:2`
                binds = [id, type]
                result = await dbs.query(sql, binds)
                if (result.rows.length) {
                    sql = "update s_inv_file set id=:1, type=:2, content=:3 where id=:4 and type=:5"
                    binds = [id, type, contentb64, id, type]
                } else {
                    sql = "insert into s_inv_file (id, type, content) VALUES (:1,:2,:3)"
                    binds = [id, type, contentb64]
                }
                result = await dbs.query(sql, binds)
                if (result.rowsAffected) ok=true
               
            }
            
        } catch (error) {
            err = error.message
        }
        res.json({ok, err,id_f})
    }

}
module.exports = Service 