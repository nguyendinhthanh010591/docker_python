"use strict"
const moment = require("moment")
const config = require("./config")
const logger4app = require("./logger4app")
const util = require(`./util`)
const fcloud = require("./fcloud")
const spdf = require("./spdf")
const hbs = require("./hbs")
const redis = require("./redis")
const mfd = config.mfd
const dbtype = config.dbtype
const dbs = require(`./${dbtype}/dbs`)
const ext = require("./ext")
const execsqlselect = (sql, binds) => {
    return new Promise(async (resolve, reject) => {
        try {
            let sqltmp = `${sql} `, arrtobind = sqltmp.split("?"), sqlrun = ``
            if (arrtobind.length == 1) {
                sqlrun = sqltmp
            } else {
                for (let i = 1; i < arrtobind.length; i++) {
                    let vbind
                    switch (dbtype) {
                        case "mssql":
                            vbind = `@${i}`
                            break
                        case "mysql":
                            vbind = `?`
                            break
                        case "orcl":
                            vbind = `:${i}`
                            break
                        case "pgsql":
                            vbind = `$${i}`
                            break
                    }
                    sqlrun += (arrtobind[i - 1] + vbind)
                }
                sqlrun += arrtobind[arrtobind.length - 1]
            }
            const result = await dbs.query(sqlrun, binds)
            let rows
            switch (dbtype) {
                case "mssql":
                    rows = result.recordset
                    break
                case "mysql":
                    rows = result[0]
                    break
                case "orcl":
                    rows = result.rows
                    break
                case "pgsql":
                    rows = result.rows
                    break
            }
            resolve(rows)
        } catch (err) {
            console.trace(err)
            //throw new Error(err)
            reject(err)
        }
    })
}
const execsqlinsupddel = (sql, binds, conn) => {
    return new Promise(async (resolve, reject) => {
        try {
            let sqltmp = `${sql} `, arrtobind = sqltmp.split("?"), sqlrun = ``
            if (arrtobind.length == 1) {
                sqlrun = sqltmp
            } else {
                for (let i = 1; i < arrtobind.length; i++) {
                    let vbind
                    switch (dbtype) {
                        case "mssql":
                            vbind = `@${i}`
                            break
                        case "mysql":
                            vbind = `?`
                            break
                        case "orcl":
                            vbind = `:${i}`
                            break
                        case "pgsql":
                            vbind = `$${i}`
                            break
                    }
                    sqlrun += (arrtobind[i - 1] + vbind)
                }
                sqlrun += arrtobind[arrtobind.length - 1]
            }
            if (!conn) await dbs.query(sqlrun, binds)
            else {
                switch (dbtype) {
                    case "mssql":
                        await dbs.queryConn(sqlrun, binds, conn)
                        break
                    case "mysql":
                        await conn.query(sqlrun, binds)
                        break
                    case "orcl":
                        await dbs.queryConn(sqlrun, binds, {}, conn)
                        break
                    case "pgsql":
                        await conn.query(sqlrun, binds)
                        break
                }
            }
            resolve(1)
        } catch (err) {
            reject(err)
        }
    })
}
const execsqlReturnIdAfterInsert = (sql, binds, conn) => {
    return new Promise(async (resolve, reject) => {
        try {
            let sqltmp = `${sql} `, arrtobind = sqltmp.split("?"), sqlrun = ``, result, id
            if (arrtobind.length == 1) {
                sqlrun = sqltmp
            } else {
                for (let i = 1; i < arrtobind.length; i++) {
                    let vbind
                    switch (dbtype) {
                        case "mssql":
                            vbind = `@${i}`
                            break
                        case "mysql":
                            vbind = `?`
                            break
                        case "orcl":
                            vbind = `:${i}`
                            break
                        case "pgsql":
                            vbind = `$${i}`
                            break
                    }
                    sqlrun += (arrtobind[i - 1] + vbind)
                }
                sqlrun += arrtobind[arrtobind.length - 1]
            }
            switch (dbtype) {
                case "mssql":
                    sqlrun += `;select scope_identity() as id`
                    break
                case "mysql":
                    sqlrun += ``
                    break
                case "orcl":
                    sqlrun += ` RETURN id INTO :id`
                    binds.push({ type: 2010, dir: 3003 })
                    break
                case "pgsql":
                    sqlrun += ` RETURNING id`
                    break
            }
            if (!conn) result = await dbs.query(sqlrun, binds)
            else {
                switch (dbtype) {
                    case "mssql":
                        result = await dbs.queryConn(sqlrun, binds, conn)
                        break
                    case "mysql":
                        result = await conn.query(sqlrun, binds)
                        break
                    case "orcl":
                        result = await dbs.queryConn(sqlrun, binds, {}, conn)
                        break
                    case "pgsql":
                        result = await conn.query(sqlrun, binds)
                        break
                }
            }
            switch (dbtype) {
                case "mssql":
                    if (result.rowsAffected[0])
                        id = result.recordset[0].id
                    break
                case "mysql":
                    if (result[0].affectedRows > 0)
                        id = result[0].insertId
                    break
                case "orcl":
                    if (result.rowsAffected)
                        id = result.outBinds.id[0]
                    break
                case "pgsql":
                    if (result.rowsAffected)
                        id = result.rows[0].id
                    break
            }
            resolve(id)
        } catch (err) {
            reject(err)
        }
    })
}
const sql_xml = {
    sql_xml_mssql: `select case when hascode = 1 and status_received = 10 then xml_received else xml end xml from s_inv where id =@1 and status in (3,4)`,
    sql_xml_mysql: `select case when hascode = 1 and status_received = 10 then xml_received else xml end xml from s_inv where id =? and status in (3,4)`,
    sql_xml_orcl: `select case when hascode = 1 and status_received = 10 then xml_received else xml end "xml" from s_inv where id =:id and status in (3,4)`,
    sql_xml_pgsql: `select case when hascode = 1 and status_received = 10 then xml_received else xml end xml from s_inv where id =$1 and status in (3,4)`
}
const sql_xml_list_inv = {
    sql_xml_mssql: `select xml from s_list_inv where id =@1`,
    sql_xml_mysql: `select xml from s_list_inv where id =?`,
    sql_xml_orcl: `select xml "xml" from s_list_inv where id =:id`,
    sql_xml_pgsql: `select xml from s_list_inv where id =$1`
}
const sql_xml_stm = {
    sql_xml_mssql: `select xml_accepted from s_statement where id=@1 `,
    sql_xml_mysql: `select xml_accepted from s_statement where id=?`,
    sql_xml_orcl: `select xml_accepted "xml_accepted" from s_statement where id=:id`,
    sql_xml_pgsql: `select xml_accepted from s_statement where id=$1 `
}
const sql_cvt = {
    sql_cvt_mssql: `update s_inv set cvt=1,doc=@1 where id=@2`,
    sql_cvt_mysql: `update s_inv set cvt=1,doc=? where id=?`,
    sql_cvt_orcl: `update s_inv set cvt=1,doc=:doc where id=:id`,
    sql_cvt_pgsql: `update s_inv set cvt=1,doc=$1 where id=$2`
}
const sql_doctemp = {
    sql_doctemp_mssql: `INSERT INTO s_invdoctemp (invid, doc, status) values (0,@1,0)`,
    sql_doctemp_mysql: `INSERT INTO s_invdoctemp (invid, doc, status) values (0,?,0)`,
    sql_doctemp_orcl: `INSERT INTO s_invdoctemp (invid, doc, status) values (0,:1,0)`,
    sql_doctemp_pgsql: `INSERT INTO s_invdoctemp (invid, doc, status) values (0,$1,0)`
}
const rbi = (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            const inv = require(`./inv`)
            let doc = await inv.invdocbid(id)
            resolve(doc)
        } catch (err) {
            reject(err)
        }
    })
}
const Service = {
    execsqlselect: async (sql, binds) => {
        try {
            let rows = await execsqlselect(sql, binds) 
            return rows
        } catch (err) {
            throw err
        }
    },
    execsqlinsupddel: async (sql, binds, conn) => {
        try {
            let vrturn = await execsqlinsupddel(sql, binds, conn) 
            return vrturn
        } catch (err) {
            throw err
        }
    },
    execsqlReturnIdAfterInsert: async (sql, binds, conn) => {
        try {
            let vrturn = await execsqlReturnIdAfterInsert(sql, binds, conn) 
            return vrturn
        } catch (err) {
            throw err
        }
    },
    getsqlpager: (start, count) => {
        let vsqlpager
        try {
            switch (dbtype) {
                case "mssql":
                    vsqlpager = `OFFSET ${start} ROWS FETCH NEXT ${count} ROWS ONLY`
                    break
                case "orcl":
                    vsqlpager = `OFFSET ${start} ROWS FETCH NEXT ${count} ROWS ONLY`
                    break
                case "mysql":
                    vsqlpager = `LIMIT ${count} OFFSET ${start}`
                    break
                case "pgsql":
                    vsqlpager = `limit ${count} offset ${start}`
                    break
                default:
                    vsqlpager = ``
                    break
            } 
            return vsqlpager
        } catch (err) {
            throw err
        }
    },
    getsqlpagerandbind: (start, count) => {
        let vsqlpager, vsqlpagerstr = ``, vsqlpagerbind = [] , vstart = Number(start), vcount = Number(count)
        try {
            switch (dbtype) {
                case "mssql":
                    vsqlpagerstr = `OFFSET ? ROWS FETCH NEXT ? ROWS ONLY`
                    vsqlpagerbind = [vstart, vcount]
                    break
                case "orcl":
                    vsqlpagerstr = `OFFSET ? ROWS FETCH NEXT ? ROWS ONLY`
                    vsqlpagerbind = [vstart, vcount]
                    break
                case "mysql":
                    vsqlpagerstr = `LIMIT ? OFFSET ?`
                    vsqlpagerbind = [vcount, vstart]
                    break
                case "pgsql":
                    vsqlpagerstr = `limit ? offset ?`
                    vsqlpagerbind = [vcount, vstart]
                    break
                default:
                    break
            } 
            vsqlpager = {vsqlpagerstr : vsqlpagerstr, vsqlpagerbind : vsqlpagerbind}
            return vsqlpager
        } catch (err) {
            throw err
        }
    },
    getConnection: async () => {
        let conn
        try {
            switch (dbtype) {
                case "mssql":
                    conn = await dbs.getConnection()
                    break
                case "orcl":
                    conn = await dbs.getConnection()
                    break
                case "mysql":
                    conn = await dbs.getConnection()
                    break
                case "pgsql":
                    conn = dbs
                    break
                default:
                    break
            } 
            
            return conn
        } catch (err) {
            throw err
        }
    },
    load: async (req, res, next) => {
        try {
            const id = req.params.id, doc = await rbi(id)
            if (doc["SendMailNum"]) delete doc["SendMailNum"]
            if (doc["SendMailStatus"]) delete doc["SendMailStatus"]
            if (doc["SendMailDate"]) delete doc["SendMailDate"]
            if (doc["SendSMSNum"]) delete doc["SendSMSNum"]
            if (doc["SendSMSStatus"]) delete doc["SendSMSStatus"]
            if (doc["SendSMSDate"]) delete doc["SendSMSDate"]
            if (doc["ic"]) delete doc["ic"]
            if (doc["cde"]) delete doc["cde"]
            if (doc["cdt"]) delete doc["cdt"]
            if (doc["cancel"]) delete doc["cancel"]
            if (doc["convertd"]) delete doc["convertd"]
            if (doc["convertu"]) delete doc["convertu"]
            if (doc["root"] && !doc["adjdif"]) delete doc["root"]
            if (doc["dif"]) delete doc["dif"]
            doc.idt_=doc.idt
            res.json(doc)
        } catch (err) {
            next(err)
        }
    },
    view: async (req, res, next) => {
        try {
            const ous = require(`./ous`)
            const sec = require("./sec")
            const token = sec.decode(req), taxc = token.taxc, body = req.body, org = await ous.obt(taxc), idx = org.temp
            if (!body.stax) {
                body.stax = org.taxc
                body.sname = org.name
                body.saddr = org.addr
                body.smail = org.mail
                body.stel = org.tel
                body.taxo = org.taxo
                body.sacc = org.acc
                body.sbank = org.bank
                body.status = 1
            }
            const status = body.status
            if (!([3, 4].includes(status))) body.sec = ""
            const doc = body, form = doc.form, type = doc.type, serial = doc.serial
            const fn = form.length > 1 ? util.fn(form, idx) : `${type}.${form}.${serial}.${idx}`
            const tmp = await util.template(fn)
            let obj = { status: status, doc: doc, tmp: tmp }
            let reqjsr = ext.createRequest(obj)
            const bodyBuffer = await util.jsReportRenderAttach(reqjsr)
            let sizeInByte = bodyBuffer.byteLength
            res.json({ pdf: `data:application/pdf;base64, ${bodyBuffer.toString('base64')}`, sizePDF: sizeInByte })
            //  res.json({ doc: doc, tmp: tmp })
        } catch (err) {
            next(err)
        }
    },
    htm: async (req, res, next) => {
        try {
            const doc1 = req.params
            const ous = require(`./ous`)
            const id = doc1.id, doc = await rbi(id), status = doc.status, org = await ous.obt(doc.stax), idx = org.temp
            // const id = req.params.id, doc = await rbi(id), status = doc.status, org = await ous.obt(doc.stax), idx = org.temp
            if (!([3, 4].includes(status))) doc.sec = ""
            const form = doc.form, type = doc.type, serial = doc.serial
            const fn = form.length > 1 ? util.fn(form, idx) : `${type}.${form}.${serial}.${idx}`
            const tmp = await util.template(fn)
            let obj = { status: status, doc: doc, tmp: tmp }
            let reqjsr = ext.createRequest(obj)
            // res.json({ status: status, doc: doc, tmp: tmp })
            const bodyBuffer = await util.jsReportRenderAttach(reqjsr)
            let sizeInByte = bodyBuffer.byteLength
            //res.contentType("application/pdf")    
            //res.end(bodyBuffer, "binary")

            res.json({ pdf: `data:application/pdf;base64, ${bodyBuffer.toString('base64')}`, sizePDF: sizeInByte, status: status })

            //  res.json({ status: status, doc: doc, tmp: tmp })
        } catch (err) {
            next(err)
        }
    },
    htmn: async (req, res, next) => {
        try {

            const doc1 = req.query
            const doc = JSON.parse(doc1.inv)

            // const doc = JSON.stringify(doc1)
            // logger4app.debug('doc: '+doc)
            const ous = require(`./ous`)
            const org = await ous.obt(doc.stax)
            const idx = org.temp, status = 1

            if (!([3, 4].includes(status))) doc.sec = ""
            const fn = util.fn(doc.form, idx), tmp = await util.template(fn)
            let obj = { status: status, doc: doc, tmp: tmp }
            let reqjsr = ext.createRequest(obj)
            // res.json({ status: status, doc: doc, tmp: tmp })
            const bodyBuffer = await util.jsReportRenderAttach(reqjsr)
            let sizeInByte = bodyBuffer.byteLength
            //res.contentType("application/pdf")    
            //res.end(bodyBuffer, "binary")

            res.json({ pdf: `data:application/pdf;base64, ${bodyBuffer.toString('base64')}`, sizePDF: sizeInByte })
            //res.json({ status: status, doc: doc, tmp: tmp })
        } catch (err) {
            next(err)
        }
    },
    conf: async (req, res, next) => {
        try {
            const ous = require(`./ous`)
            const sec = require("./sec")
            const token = sec.decode(req), taxc = token.taxc, org = await ous.obt(taxc), sign = org.sign
            if (sign == 1) res.json({ sign: sign, mst: taxc, date: new Date() })
            else res.json({ sign: sign })
        } catch (err) {
            next(err)
        }
    },
    pdf: async (req, res, next) => {
        try {
            const ous = require(`./ous`)
            const sec = require("./sec")
            const token = sec.decode(req), taxc = token.taxc, org = await ous.obt(taxc)
            let b64 = req.body.b64
            if (org.sign == 2) {
                const pwd = util.decrypt(org.pwd), buf = Buffer.from(b64.substring(28), 'base64')
                b64 = await spdf.signpdf(taxc, pwd, buf)
            }
            else if (org.sign == 3) {
                const pwd = util.decrypt(org.pwd), buf = Buffer.from(b64.substring(28), 'base64')
                b64 = await fcloud.pdf(org.usr, pwd, taxc, buf)
            }
            res.json({ pdf: `data:application/pdf;base64,${b64}` })
        } catch (err) {
            next(err)
        }
    },
    mail: async (req, res, next) => {
        try {
            const id = req.params.id, doc = await rbi(id)
            const subject = await util.getSubjectMail(doc, '')
            const html = await hbs.j2m(doc)
            res.json(util.encodehtml({ to: doc.bmail, subject: subject, html: html, id: id, status: doc.status }))
        }
        catch (err) {
            next(err)
        }
    },
    sms: async (req, res, next) => {
        try {
            const id = req.params.id, doc = await rbi(id)
            res.json({ btel: doc.btel, id: id })
        }
        catch (err) {
            next(err)
        }
    },
    xml: async (req, res, next) => {
        try {
            const id = req.params.id
            let xml, result, rows
            result = await dbs.query(sql_xml[`sql_xml_${dbtype}`], [id])
            switch (dbtype) {
                case "mssql":
                    rows = result.recordset
                    break
                case "mysql":
                    rows = result[0]
                    break
                case "orcl":
                    rows = result.rows
                    break
                case "pgsql":
                    rows = result.rows
                    break
            }
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn ${id} \n (Invoice not found ${id})`)
            xml = rows[0].xml
            res.json({ xml: xml })
        }
        catch (err) {
            next(err)
        }
    },
    xmlListInv: async (req, res, next) => {
        try {
            const id = req.params.id
            let xml, result, rows
            result = await dbs.query(sql_xml_list_inv[`sql_xml_${dbtype}`], [id])
            switch (dbtype) {
                case "mssql":
                    rows = result.recordset
                    break
                case "mysql":
                    rows = result[0]
                    break
                case "orcl":
                    rows = result.rows
                    break
                case "pgsql":
                    rows = result.rows
                    break
            }
            if (rows.length == 0) throw new Error(`Không tìm thấy bảng tổng hợp ${id} \n (List einvoice not found ${id})`)
            xml = rows[0].xml
            res.json({ xml: xml })
        }
        catch (err) {
            next(err)
        }
    },
    downloadpdf: async (req, res, next) => {
        const ous = require(`./ous`)
        try {
            const query = req.query, sec = query.sec
         
            const result = await execsqlselect(`select id "id",pid "pid",cid "cid",cde "cde",adjtyp "adjtyp",adjdes "adjdes",status "status",doc "doc", stax "stax" from s_inv where sec=? and status in (3,4,6)`, [sec])
           
            let rows = result
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn mã tra cứu: ${sec} \n (Invoice not found for code: ${sec})`)
            const row = rows[0], doc = await rbi(row.id)
            const org = await ous.obt(doc.stax)
            const idx = org.temp,fn = doc.form.length > 1 ? util.fn(doc.form, idx) : `${doc.type}.${doc.form}.${doc.serial}.${idx}`,tmp = await util.template(fn)
          //  const idx = org.temp, fn = util.fn(doc.form, idx), tmp = await util.template(fn)
           
            let obj ={ status: doc.status, doc: doc, tmp: tmp }
            let reqjsr = ext.createRequest(obj)
            const bodyBuffer = await  util.jsReportRenderAttach(reqjsr)

            res.setHeader('Content-Disposition', 'attachment; filename=invoice.pdf');
            res.setHeader('Content-Transfer-Encoding', 'binary');
            res.setHeader('Content-Type', 'application/octet-stream');
            res.send(new Buffer(bodyBuffer, 'binary'))
        }
        catch (err) {
            next(err)
        }
    },
    downloadxml: async (req, res, next) => {
            const ous = require(`./ous`)
        try {
            const query = req.query, sec = query.sec
            let result = await execsqlselect(`select xml from s_inv where sec=? and status in (3,4)`, [sec])
            let rows = result
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn mã tra cứu: ${sec} \n (Could not find invoice for code: ${sec})`)
            let xml = rows[0].xml
            res.setHeader('Content-Disposition', 'attachment; filename=invoice.xml');
            res.setHeader('Content-Type', 'application/xml');
            res.send(new Buffer(xml))
        }
        catch (err) {
            next(err)
        }
    },
    xml_stm: async (req, res, next) => {
        try {
            const id = req.params.id
            let xml, result, rows
            result = await dbs.query(sql_xml_stm[`sql_xml_${dbtype}`], [id])
            switch (dbtype) {
                case "mssql":
                    rows = result.recordset
                    break
                case "mysql":
                    rows = result[0]
                    break
                case "orcl":
                    rows = result.rows
                    break
                case "pgsql":
                    rows = result.rows
                    break
            }
            if (rows.length == 0) throw new Error(`Không tìm thấy tờ khai ${id} \n (No declaration found ${id})`)
            xml = rows[0].xml_accepted
            res.json({ xml: xml })
        }
        catch (err) {
            next(err)
        }
    },
    cvt: async (req, res, next) => {
        try {
            const ous = require(`./ous`)
            const sec = require("./sec")
            const id = req.params.id, doc = await rbi(id), org = await ous.obt(doc.stax), idx = org.temp, token = sec.decode(req)
            const form = doc.form, type = doc.type, serial = doc.serial
            const fn = form.length > 1 ? util.fn(form, idx) : `${type}.${form}.${serial}.${idx}`
            const tmp = await util.template(fn)
            if (!doc.convertd || !doc.convertu) {
                doc.convertd = moment(new Date()).format('DD/MM/YYYY')
                doc.convertu = token.fn
                await dbs.query(sql_cvt[`sql_cvt_${dbtype}`], [JSON.stringify(doc), id])
            }
            doc.convert = 1
            // res.json({ doc: doc, tmp: tmp })
            let reqjsr = ext.createRequest({ doc: doc, tmp: tmp })
            // res.json({ status: status, doc: doc, tmp: tmp })
            const bodyBuffer = await util.jsReportRenderAttach(reqjsr)
            let sizeInByte = bodyBuffer.byteLength
            //res.contentType("application/pdf")    
            //res.end(bodyBuffer, "binary")

            res.json({ pdf: `data:application/pdf;base64, ${bodyBuffer.toString('base64')}`, sizePDF: sizeInByte })
        } catch (err) {
            next(err)
        }
    },
    refresh: async (req, res, next) => {
        try {
            let keys = ["CAT.PROV", "CAT.ROLE", "CAT.UNIT", "CAT.CURRENCY", "CAT.ITYPE", "CAT.ISTATUS", "CAT.TAXO"]
            await redis.del(keys)
            keys = ["LOCAL.*", "TAXO.*", "COL.*", "XLS.*", "USB.*", "FCLOUD.*", "DTL.*", "ORC.*", "OUS.*"]
            for (let i = 0; i < keys.length; i++) {
                let rows = await redis.keys(keys[i])
                if (rows && rows.length > 0) await redis.del(rows)
            }
            res.send("Đã đồng bộ cache (Cache synced)")
        }
        catch (err) {
            next(err)
        }
    },
    seq: async (req, res, next) => {
        try {
            let key = req.params.key, id = await redis.incr(key)
            res.json({ id: id })
        }
        catch (err) {
            next(err)
        }
    },
    // get suggesst of currentcy
    kache: async (req, res, next) => {
        try {
            const cat = require(`./cat`)
            let type = req.params.type.toUpperCase(), key = `CAT.${type}`, data
            //data = await redis.get(key)
            //if (data) return res.json(JSON.parse(data))
            data = await cat.cache(type, key)
            res.json(data)
        }
        catch (err) {
            next(err)
        }
    },
    segment: async (req, res, next) => {
        try {
            const cat = require(`./cat`)
            let type = req.params.type.toUpperCase(), key = `CAT.${type}`, data
            data = await cat.cache(type, key)
            res.json(data)
        }
        catch (err) {
            next(err)
        }
    },
    getserial: async (req, res, next) => {
        try {
            let type = req.params.type
            let data
            try {
                if (!type) {
                    type = req.query.type
                }
            } catch (error) {

            }

            if (type == 'form') {
                switch (dbtype) {
                    case "mssql":
                        data = await dbs.query(`select form "id", form "value" from s_serial group by form`)
                        if (data) res.json(data.recordset)
                        else res.json({})
                        break
                    case "mysql":
                        data = await dbs.query(`select form "id", form "value" from s_serial group by form`)
                        if (data) res.json(data[0])
                        else res.json({})
                        break
                    case "orcl":
                        data = await dbs.query(`select form "id", form "value" from s_serial group by form`)
                        if (data) res.json(data.rows)
                        else res.json({})
                        break
                    case "pgsql":
                        data = await dbs.query(`select form "id", form "value" from s_serial group by form`)
                        if (data) res.json(data.rows)
                        else res.json({})
                        break
                }
            } else {
                switch (dbtype) {
                    case "mssql":
                        data = await dbs.query(`select serial "id", serial "value" from s_serial where form=@1 group by serial`, [type])
                        if (data) res.json(data.recordset)
                        else res.json({})
                        break
                    case "mysql":
                        data = await dbs.query(`select serial "id", serial "value" from s_serial where form=? group by serial`, [type])
                        if (data) res.json(data[0])
                        else res.json({})
                        break
                    case "orcl":
                        data = await dbs.query(`select serial "id", serial "value" from s_serial where form=:type group by serial`, [type])
                        if (data) res.json(data.rows)
                        else res.json({})
                        break
                    case "pgsql":
                        data = await dbs.query(`select serial "id", serial "value" from s_serial where form=$1 group by serial`, [type])
                        if (data) res.json(data.rows)
                        else res.json({})
                        break
                }
            }

        }
        catch (err) {
            next(err)
        }
    },
    inserttempmail: async (content) => {//Ham nay dung de insert vao bang tam trong truong hop khong dung rsmq
        try {
            //Insert vao bang tam s_invdoctemp voi invid (id hoa don) bang 0 de phan biet voi gui mail hoa don, doc la noi dung mail gui di
            await dbs.query(sql_doctemp[`sql_doctemp_${dbtype}`], [JSON.stringify(content)])
        } catch (err) {
            throw err
        }
    },
    getstatementinfo: async (mst) => {//Ham nay dung de lay thong tin dang ky dich vu su dung hoa don dien tu tra ra
        try {
            let ret = { hascode: 0, invtype: `01GTKT,02GTTT,03XKNB,07KPTQ,04HGDL,01/TVE,02/TVE`, dtsendtype : `sendinv,listinv` }
            let rows = await execsqlselect(`select hascode "hascode", invtype "invtype",dtsendtype "dtsendtype" from s_statement where cqtstatus=5 and stax=? order by createdt desc`, [mst])
            if (rows && rows.length > 0) {
                let row = rows[0]
                ret.hascode = row.hascode
                ret.invtype = row.invtype
                ret.dtsendtype = row.dtsendtype
            }
            return ret
        } catch (err) {
            throw err
        }
    },
    xmlListInv: async (req, res, next) => {
        try {
            const id = req.params.id
            let xml, result, rows
            result = await dbs.query(sql_xml_list_inv[`sql_xml_${dbtype}`], [id])
            switch (dbtype) {
                case "mssql":
                    rows = result.recordset
                    break
                case "mysql":
                    rows = result[0]
                    break
                case "orcl":
                    rows = result.rows
                    break
                case "pgsql":
                    rows = result.rows
                    break
            }
            if (rows.length == 0) throw new Error(`Không tìm thấy bảng tổng hợp ${id} \n (List einvoice not found ${id})`)
            xml = rows[0].xml
            res.json({ xml: xml })
        }
        catch (err) {
            next(err)
        }
    },
    checkrevoke: async (req, res, next) => {
        try {
            const sec=require('./sec')
            const token = sec.decode(req), stax = token.taxc, list_seri_in_statement=[]
            // check thông tin cấu hình hệ thống
            let ouInfo = await Service.execsqlselect(`select chk_revoke "chk_revoke" from s_ou where taxc=? `, [token.taxc])
            ouInfo=ouInfo[0]
            if(ouInfo.chk_revoke!=1) return
            let result = await Service.execsqlselect(`select doc "doc" from s_statement where stax=? and cqtstatus=5 order by createdt desc `, [stax])
            if (result.length == 0) throw new Error(`Không tìm thấy Tờ khai ${stax}`)
            let row = result[0]
            let doc = util.parseJson(row.doc)
            //  lấy items
            for(let item of doc.items) {
                if(new Date(item.sign_from) > new Date(item.sign_to) ) {
                    next(new Error(`Ngày hiệu lực của CA không hợp lệ`))
                }
                if(new Date(item.sign_from) > new Date() || new Date(item.sign_to) < new Date() ) {
                    continue
                }
                if(new Date(item.sign_from) <= new Date() && new Date(item.sign_to) >= new Date() ) {
                    list_seri_in_statement.push(item.sign_seri)
                }
            }
            // compare seri với seri trong bảng revoke
            result = await Service.execsqlselect(`select taxc "taxc", serial_number "seri", valid_to "valid_to", valid_from "valid_from", status "status", check_date "check_date", description "description" from s_revoce_ca where taxc=? order by check_date desc`, [stax])
            let rowCheckRevoke= result[0], seri = ""
            if(!rowCheckRevoke || !rowCheckRevoke.check_date || !moment(rowCheckRevoke.check_date).isValid() || moment(rowCheckRevoke.check_date)<moment().subtract(1, 'days')) throw new Error("Chữ ký số chưa được check revoke\nSignature hasn't been check revoke")
            if(rowCheckRevoke.status==0 || rowCheckRevoke.status==2) throw new Error(rowCheckRevoke.description)
            seri = rowCheckRevoke.seri
            if(list_seri_in_statement.length<1) throw new Error(`Chữ ký số trên tờ khai không hợp lệ`)
            if(seri && !list_seri_in_statement.includes(seri)) {
                throw new Error(`Chữ ký số đã bị thu hồi`)
            }
            if(!seri) {
                throw new Error(`Service Check revoke có lỗi`)
            }
            
            // trả về kết quả
        } catch(err) {
            console.log("Errror check revoke:", err)
            throw err
        }
    },
    checkrevokeAPI: async (stax) => {
        try {
            // check thông tin cấu hình hệ thống
            let ouInfo = await Service.execsqlselect(`select chk_revoke "chk_revoke" from s_ou where taxc=? `, [stax])
            ouInfo=ouInfo[0]
            if(ouInfo.chk_revoke!=1) return
            const list_seri_in_statement=[]
            let result = await Service.execsqlselect(`select doc "doc" from s_statement where stax=? and cqtstatus=5 order by createdt desc `, [stax])
            if (result.length == 0) throw new Error(`Không tìm thấy Tờ khai ${stax}`)
            let row = result[0]
            let doc = util.parseJson(row.doc)
            //  lấy items
            for(let item of doc.items) {
                if(new Date(item.sign_from) > new Date(item.sign_to) ) {
                    next(new Error(`Ngày hiệu lực của CA không hợp lệ`))
                }
                if(new Date(item.sign_from) > new Date() || new Date(item.sign_to) < new Date() ) {
                    continue
                }
                if(new Date(item.sign_from) <= new Date() && new Date(item.sign_to) >= new Date() ) {
                    list_seri_in_statement.push(item.sign_seri)
                }
            }
            // compare seri với seri trong bảng revoke
            result = await Service.execsqlselect(`select taxc "taxc", serial_number "seri", valid_to "valid_to", valid_from "valid_from", status "status", check_date "check_date", description "description" from s_revoce_ca where taxc=? order by check_date desc`, [stax])
            let rowCheckRevoke= result[0], seri = ""
            if(!rowCheckRevoke || !rowCheckRevoke.check_date || !moment(rowCheckRevoke.check_date).isValid() || moment(rowCheckRevoke.check_date)<moment().subtract(1, 'days')) throw new Error("Chữ ký số chưa được check revoke\nSignature hasn't been check revoke")
            if(rowCheckRevoke.status==0 || rowCheckRevoke.status==2) throw new Error(rowCheckRevoke.description)
            seri = rowCheckRevoke.seri
            if(list_seri_in_statement.length<1) throw new Error(`Chữ ký số trên tờ khai không hợp lệ`)
            if(seri && !list_seri_in_statement.includes(seri)) {
                throw new Error(`Chữ ký số chưa được check revoke\nSignature hasn't been check revoke`)
            }
            if(!seri) {
                throw new Error(`Service Check revoke có lỗi`)
            }
            
            // trả về kết quả
        } catch(err) {
            console.log("Errror check revoke:", err)
            throw err
        }
    },
    checkrevokeStatement: async (org) => {
        try {
            if(org.chk_revoke!=1) return
            const sign = require('./sign')
            const signtype = org.sign, taxc= org.taxc
            if (signtype != 2) {
                next()
                return
            }
            let ca, pwd, result
            try {
                pwd = util.decrypt(org.pwd)
                ca = await sign.ca(taxc, pwd)
                
            } catch (err) {
                throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)')
            }
            // compare seri với seri trong bảng revoke
            result = await Service.execsqlselect(`select taxc "taxc", serial_number "seri", valid_to "valid_to", valid_from "valid_from", status "status", check_date "check_date", description "description" from s_revoce_ca where taxc=? order by check_date desc`, [taxc])
            let rowCheckRevoke= result[0], seri = ""
            seri = rowCheckRevoke.seri
            if(!rowCheckRevoke || ca.serialNumber!=seri) throw new Error(`CKS chưa được check revoke\n eSignature has not been checked revoke`)
            if(!rowCheckRevoke || !rowCheckRevoke.check_date || !moment(rowCheckRevoke.check_date).isValid() || moment(rowCheckRevoke.check_date)<moment().subtract(1, 'days')) throw new Error("Chữ ký số chưa được check revoke\nSignature hasn't been checkded revoke")
            if(rowCheckRevoke.status==0 || rowCheckRevoke.status==2) throw new Error(rowCheckRevoke.description)
            return
        } catch(err) {
            console.log("Errror check revoke:", err)
            throw err
        }
    },
}
module.exports = Service