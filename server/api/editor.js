"use strict"
const path = require("path")
const fs = require("fs")
const moment = require("moment")
const cheerio = require("cheerio")
const config = require("./config")
const logger4app = require("./logger4app")
const sec = require("./sec")
const util = require("./util")
const n2w = require("./n2w")
const redis = require("./redis")
const ous = require(`./${config.dbtype}/ous`)
const ext = require(`./ext`)
const pathtemplate = config.pathtemplate

//const hbs = require("./hbs")
// const PDF = require("./pdf")
const getRandomInt = (min, max) => {
    min = Math.ceil(min)
    max = Math.floor(max)
    return Math.floor(Math.random() * (max - min + 1)) + min
}
const units = ["bao", "bình", "cái", "chai", "chiếc", "cm", "cm2", "cm3", "dịch vụ", "dm", "dm2", "dm3", "g", "giao dịch", "giờ", "gói", "in", "inch", "kg", "km", "kw", "lần", "lạng", "lít", "lọ", "m", "m2", "m3", "ml", "mm", "mm2", "N/A", "ounce", "pound", "tạ", "tấn", "yến"]
const jsonex = (b, type,org, form, serial) => {
    
    let json
    if (b == 0) {
        if (type == "03XKNB") {
            json = {
                "curr": "VND",
                "exrt": 1,
                "sec": "43a3a255xk",
                "type": type,
                "name": util.tenhd(type),
                "form": form,
                "serial": serial,
                "seq": "0000001",
                "note": "Hóa đơn mẫu",
                "status": 2,
                "recvr": "Nguyễn Văn A",
                "ordno": "FIS.19.000001",
                "ordou": "Công ty Hệ thống Thông tin FPT",
                "ordre": "Chuyển hàng",
                "whsfr": "Tầng 21 Tòa nhà Keangnam Landmark 72, E6 Phạm Hùng, Nam Từ Liêm, Hà Nội",
                "whsto": "Tầng 13 Tòa nhà FPT Cầu Giấy, số 17 phố Duy Tân, Dịch Vọng Hậu, Cầu Giấy, Hà Nội",
                "trans": "VNPOST",
                "vehic": "",
                "contr": ""
            }

        }
        else {
            json = {
                "paym": "TM",
                "curr": "VND",
                "exrt": 1,
                "sec": "43a3a255ca",
                "type": type,
                "name": util.tenhd(type),
                "form": form,
                "serial": serial,
                "seq": "0000001",
                "note": "Hóa đơn mẫu",
                "status": 2,
                "discount": "",
                "bacc": "",
                "bbank": "",
                "bmail": "",
                "btel": "",
                "btax": "2222222222",
                "bname": "Công ty XYZ",
                "buyer": "Nguyễn Văn A",
                "baddr": "Xã X, Huyện H, Tỉnh T",
                "c4":  moment().format("YYYY-MM-DD HH:MM")
            }
        }

        let arr = [], sum = 0
        const len = units.length
        for (let i = 0; i < 20; i++) {
            const uid = getRandomInt(0, len - 1)
            const price = getRandomInt((i + 1) * 100000, 100 * 100000)
            const quantity = getRandomInt(1, i + 1)
            const amount = price * quantity
            sum += amount
            const itype = (i % 10) ? "" : "MT"
            let item = { line: i + 1, name: `Hàng hóa dịch vụ ${i + 1}`, type: itype, unit: units[uid], price: price, quantity: quantity, amount: amount }
            if (type == "01GTKT") {
                item.vrt = "10"
                item.vrn = "10%"
            }
            arr.push(item)
        }
        let total
        if (type == "01GTKT") {
            const vat = sum / 10
            total = sum + vat
            json.vat = vat
            json.vatv = vat
            json.tax = [{ vrt: "10", vrn: "10%", amt: sum, amtv: sum, vat: vat, vatv: vat }]
        }
        else total = sum
        json.idt = moment().format("YYYY-MM-DD HH:MM")
        json.stax = org.taxc
        json.sname = org.name
        json.saddr = org.addr
        json.smail = org.mail
        json.stel = org.tel
        json.taxo = org.taxo
        json.sacc = org.acc
        json.sbank = org.bank
        json.sum = sum
        json.sumv = sum
        json.total = total
        json.totalv = total
        json.items = arr
        json.ent = config.ent
        json.word = n2w.n2w(total, "VND")
    } else {
        json = {
            "paym": "",
            "curr": "VND",
           
            "sec": "43a3a255ca",
            "type": '',
            "name": util.tenhd(type),
            "form": '',
            "serial": serial,
            "seq": "",
            "note": "",
            "status": 2,
            "discount": "",
            "bacc": "",
        }
        let arr = [], sum = 0
        const len = units.length
        for (let i = 0; i < 5; i++) {
           
            let item = { line: '', name: '', type: '', unit: '', price: '', quantity: '', amount: '' }
           
            arr.push(item)
        }
        let total
        if (type == "01GTKT") {
           
            json.tax = [{ vrt: "", vrn: "", amt: '', amtv: '', vat: '', vatv: '' }]
        }
        else total = sum
        json.idt = ''
        json.stax = org.taxc
        json.sname = org.name
        json.saddr = org.addr
        json.smail = org.mail
        json.stel = org.tel
        json.taxo = org.taxo
        json.sacc = org.acc
        json.sbank = org.bank
        json.items = arr
        json.ent = config.ent
        json.total = 0

    }
    return json
}

const Service = {
    htm: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, org = await ous.obt(taxc), form = req.query.form, idx = req.query.idx, type = req.query.type, b = req.query.flag, serial = req.query.serial
            const fn = form.length > 1 ? util.fn(form, idx) : `${type}.${form}.${serial}.${idx}`
            const tmp = await util.template(fn)
            const json = jsonex(b,type,org, form, serial)
            /*
            const htm = await hbs.j2h(json, idx)
            const pdf = await util.pdf(htm)
            res.json({ htm: htm, pdf: `data:application/pdf;base64,${pdf.toString("base64")}` })
            */

            //const pdf = await PDF.pdf1(json, form, idx)
            //res.json({ htm: null, pdf: `data:application/pdf;base64,${pdf.toString("base64")}` })
           // res.json({ doc: json, tmp: tmp })
           let obj = { doc: json, tmp: tmp }
           let reqjsr = ext.createRequest(obj)
            const pdf = await util.jsReportRenderAttach(reqjsr)
            res.json({ pdf: `data:application/pdf;base64,${pdf.toString("base64")}` })
        } catch (err) {
            next(err)
        }
    },
    app: async (req, res, next) => {
        try {
            const type = req.query.type, form = req.query.form, serial = req.query.serial, idx = req.query.idx
            const fn = form.length > 1 ? util.fn(form, idx) : `${type}.${form}.${serial}.${idx}`
            await redis.set(`APPROVED.${fn}`, "1")
            res.json(`Đã duyệt mẫu hóa đơn ${fn} (Approved the invoice form ${fn})`)
        }
        catch (err) {
            next(err)
        }
    },
    get: async (req, res, next) => {
        try {
            const type = req.query.type, form = req.query.form, serial = req.query.serial, idx = req.query.idx
            const fn = form.length > 1 ? util.fn(form, idx) : `${type}.${form}.${serial}.${idx}`
            const tmp = await util.template(fn)
            const content = `<!DOCTYPE html><html><head> <meta content="text/html; charset=utf-8" http-equiv="Content-Type"><style>${tmp.sty}</style></head><body><div id="inv">${tmp.inv}</div><div id="bgi" style="display: none;">${tmp.bgi}</div></body></html>`
            const status = await redis.exists(`APPROVED.${fn}`)
            res.json({ content: content, status: status })
        }
        catch (err) {
            next(err)
        }
    },
    tmp: async (req, res, next) => {
        try {
            const type = req.body.type, form = req.body.form, serial = req.body.serial, temp = req.body.temp
            var fn = form.length > 1 ? util.fn(form, temp) : `${type}.${form}.${serial}.${temp}`
            const tmp = await util.template(fn)
                
            let tmpr = ext.createTemplateIf(tmp, config.ent ,type)
            res.json({ tmp : tmpr })
        }
        catch (err) {
            next(err)
        }
    },
    tmpApi: async (req, res, next) => {
        try {
            const type = req.body.type, form = req.body.form, serial = req.body.serial, temp = req.body.temp
            var fn = form.length > 1 ? util.fn(form, temp) : `${type}.${form}.${serial}.${temp}`
            const tmp = await util.template(fn)
            res.json({ tmp : tmp })
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
      
        try {
            const rbody = req.body, form = rbody.form, idx = rbody.idx, content = rbody.content, type = rbody.type, serial = rbody.serial
            const fn = form.length > 1 ? util.fn(form, idx) : `${type}.${form}.${serial}.${idx}`
            const $ = cheerio.load(content, { decodeEntities: false })
            const sty = $('style').html(), inv = $('#inv').html(), bgi = $('#bgi').html(), ftr = $('#ftr').html(), hdr = $("#hdr").html(), dtl = $("#dtl").html(), body = $("body").html(), title = $("#hdr").attr("title"),rft= $("#rft").html()
            //, hdr=$('#hdr').html(), dtl=$('#dtl').html()
            const tmp = { sty, inv, bgi, ftr,rft, hdr, dtl, body, title }
           
            await redis.set(`TMP.${fn}`, JSON.stringify(tmp))
            let FILE
            if (pathtemplate) {
                FILE = path.join(pathtemplate, `${fn}`)
            } else
                FILE = path.join(__dirname, "..", "..", `temp/${fn}`)
            fs.writeFileSync(FILE, content)
            res.json(`Đã lưu thành công mẫu hóa đơn ${fn} (Invoice form has been successfully saved ${fn})`)
        }
        catch (err) {
            next(err)
        }
    }
}
module.exports = Service