"use strict"

const config = require("./config")
const margin_footer_form = config.margin_footer_form
const customForm = config.customForm

const helpers = `
	 const hbs = require("handlebars"), helpers = require("handlebars-helpers")({handlebars: hbs}), hbh = require("hbh")
	 hbh.register(hbs)
	 function getPageNumber(idx){
		if (idx==null) return ''
		const page = idx + 1
		if (page>1) return 'Tiếp trang trước - Trang ' + page
		else        return 'Trang ' + page
	}
	function getPageNumber0(idx){
		if (idx==null) return ''
		const page = idx + 1
		if (page>1) return ' Trang ' + page
		else        return 'Trang ' + page
	}
	function getPageNumbervcm(idx){
		if (idx==null) return ''
		const page = idx + 1
		if (page>1) return 'tiep theo trang truoc - trang ' + page
		else        return ''
	}
	function getTotalPages(pages){
		if (!pages) return ''
		return pages.length
	}
	function lastPage(idx, pages, opts) {
		if (idx === pages-1)  return opts.fn(this)
		return ''
	}
	`
const chrome_bgi = { preferCSSPageSize: true, /* marginTop: "2mm", marginBottom: "2mm", marginRight: "2mm", marginLeft: "2mm" */}
const Service = {
    createRequest : (json) => {
		let margin=margin_footer_form?0:5
        const tmp = json.tmp, sty = tmp.sty,rft = tmp.rft ? tmp.rft : ""
        // , inv = `<style>${sty}</style>${tmp.inv}`
        const ftr = tmp.ftr
        const footer = ftr ? `{{#lastPage @index ../$pdf.pages.length}}${ftr}{{/lastPage}}` : ``
        let tmp_ftr = `
		<style>
			@page {size: A4;}
			* { box-sizing: border-box;}
			html, body { margin: 0px; padding: 0px;}
			.main { display: flex; flex-direction: column; justify-content: space-between; width: 100%; height: 100%;}
			.footer { width: 100%; padding-bottom: 4mm; font-size: 9pt;text-align: center; }
		</style>
		{{#each $pdf.pages}}
			{{#if @index}}<div style="page-break-before: always;"></div>{{/if}}
			<main class="main">
				<header><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></header> 
				
				<footer class="footer">${rft}${footer}<div style='font-weight: bold; text-align: right;'>{{getPageNumber @index}}/{{getTotalPages ../$pdf.pages}}</div></footer>
			</main>
		{{/each}}
	`
		if(customForm=='KIMTIN')
		tmp_ftr = `
		<style>
			@page {size: A4;}
			* { box-sizing: border-box;}
			html, body { margin: 0px; padding: 0px;}
			.main { display: flex; flex-direction: column; justify-content: space-between; width: 100%; height: 100%;}
			.footer { width: 100%; padding-bottom: 4mm; font-size: 9pt;text-align: center; }
		</style>
		{{#each $pdf.pages}}
			{{#if @index}}<div style="page-break-before: always;"></div>{{/if}}
			<main class="main">
				<header><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></header> 
				{{#if @index}}<footer class="footer"><div style='font-weight: bold; text-align: right;padding-right: 5mm; margin-bottom: -40px;'>{{getPageNumber0 @index}}/{{getTotalPages ../$pdf.pages}}</div>${rft}${footer}</footer>{{else}}<footer class="footer"><div style='font-weight: bold; text-align: right;padding-right: 5mm; margin-bottom: -30px;'>{{getPageNumber0 @index}}/{{getTotalPages ../$pdf.pages}}</div>${rft}${footer}</footer>{{/if}}
				
			</main>
		{{/each}}
	`
	    const chrome_ftr = { preferCSSPageSize: true, marginRight: `${margin}mm`, marginLeft: `${margin}mm` }
        const opt_ftr = { type: "merge", mergeWholeDocument: true, mergeToFront: true, template: { content: tmp_ftr, engine: "handlebars", recipe: "chrome-pdf", helpers: helpers,chrome: chrome_ftr } }
        let opts = [opt_ftr], body, chrome_inv
        if (tmp.bgi) {
            const tmp_bgi = `<header><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></header><style> @page {size: A4;} .bgi{position:fixed; top:0; left:0; width:100%; height:100%; ${tmp.bgi}}</style><div class="bgi"></div>`
            const opt_bgi = { type: "merge", template: { content: tmp_bgi, engine: "none", recipe: "chrome-pdf", chrome: chrome_bgi } }
            opts.push(opt_bgi)
        }
        if (tmp.hdr && tmp.dtl) {
            const hdr = `<header><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></header><style>${sty}</style>${tmp.hdr}`
            const chrome_hdr = { preferCSSPageSize: true, marginTop: "7mm", marginBottom: "15mm", marginRight: "7mm", marginLeft: "7mm" }
            const opt_hdr = { type: "merge", mergeToFront: true, template: { content: hdr, engine: "handlebars", recipe: "chrome-pdf",helpers: helpers,  chrome: chrome_hdr } }
            opts.push(opt_hdr)
            body = `<header><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></header><style>${sty}</style>${tmp.dtl}`
            const title = tmp.title ? tmp.title : "10cm"
            chrome_inv = { preferCSSPageSize: true, marginTop: title, marginBottom: "15mm", marginRight: "7mm", marginLeft: "7mm" }
        }
        else {
            body = `<header><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></header><style>${sty}</style>${tmp.body}`
            chrome_inv = { preferCSSPageSize: true, marginTop: "7mm", marginBottom: "15mm", marginRight: "7mm", marginLeft: "7mm" }
        }
        const req = { data: json.doc, template: { content: body, engine: "handlebars", recipe: "chrome-pdf", helpers: helpers, chrome: chrome_inv, pdfOperations: opts } }
        return req
	},
	createRequestAPI : (json) => {
        const tmp = json.tmp, sty = tmp.sty
        // , inv = `<style>${sty}</style>${tmp.inv}`
        const ftr = tmp.ftr, rft = tmp.rft ? tmp.rft : ""
        const footer = ftr ? `{{#lastPage @index ../$pdf.pages.length}}${ftr}{{/lastPage}}` : ``
        const tmp_ftr = `
		<style>
			@page {size: A4;}
			* { box-sizing: border-box;}
			html, body { margin: 0px; padding: 0px;}
			.main { display: flex; flex-direction: column; justify-content: space-between; width: 100%; height: 100%;}
			.footer { width: 100%; padding-bottom: 4mm; padding-right: 5mm; font-size: 9pt;text-align: center;height:200px }
		</style>
		{{#each $pdf.pages}}
			{{#if @index}}<div style="page-break-before: always;"></div>{{/if}}
			<main class="main">
				<header><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></header> 
				<footer class="footer">${rft}${footer}<div style='font-weight: bold; text-align: right;'>{{getPageNumber @index}}/{{getTotalPages ../$pdf.pages}}</div></footer>
			</main>
		{{/each}}
	`
        const opt_ftr = { type: "merge", mergeWholeDocument: true, mergeToFront: true, template: { content: tmp_ftr, engine: "handlebars", recipe: "chrome-pdf", helpers: helpers } }
        let opts = [opt_ftr], body, chrome_inv
        if (tmp.bgi) {
            const tmp_bgi = `<header><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></header><style> @page {size: A4;} .bgi{position:fixed; top:0; left:0; width:100%; height:100%; ${tmp.bgi}}</style><div class="bgi"></div>`
            const opt_bgi = { type: "merge", template: { content: tmp_bgi, engine: "none", recipe: "chrome-pdf", chrome: chrome_bgi } }
            opts.push(opt_bgi)
        }
        if (tmp.hdr && tmp.dtl) {
            const hdr = `<header><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></header><style>${sty}</style>${tmp.hdr}`
            const chrome_hdr = { preferCSSPageSize: true, marginTop: "7mm", marginBottom: "15mm", marginRight: "7mm", marginLeft: "7mm" }
            const opt_hdr = { type: "merge", mergeToFront: true, template: { content: hdr, engine: "handlebars", recipe: "chrome-pdf", chrome: chrome_hdr } }
            opts.push(opt_hdr)
            body = `<header><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></header><style>${sty}</style>${tmp.dtl}`
            const title = tmp.title ? tmp.title : "10cm"
            chrome_inv = { preferCSSPageSize: true, marginTop: title, marginBottom: "15mm", marginRight: "7mm", marginLeft: "7mm" }
        }
        else {
            body = `<header><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></header><style>${sty}</style>${tmp.body}`
            chrome_inv = { preferCSSPageSize: true, marginTop: "7mm", marginBottom: "15mm", marginRight: "7mm", marginLeft: "7mm" }
        }
        const req = { content: body, engine: "handlebars", recipe: "chrome-pdf", helpers: helpers, chrome: chrome_inv, pdfOperations: opts  }
        return req
	},
	 createTemplatePage01GTGT: (tmp) => {
		const sty = tmp.sty
		// , inv = `<style>${sty}</style>${tmp.inv}`
		const ftr = tmp.ftr, rft = tmp.rft ? tmp.rft : ""
		const footer = ftr ? `{{#lastPage @index ../$pdf.pages.length}}${ftr}{{/lastPage}}` : ``
		const tmp_ftr = `
			<style>
				@page {size: A4;}
				* { box-sizing: border-box;}
				html, body { margin: 0px; padding: 0px;}
				.main { display: flex; flex-direction: column; justify-content: space-between; width: 100%; height: 100%;}
				.footer { width: 100%; padding-bottom: 4mm; padding-right: 5mm; font-size: 9pt;text-align: center; }
				.header { width: 100%; padding-top: 40mm; font-size: 9pt;text-align: center; margin-left: 7mm}
			</style>
			{{#each $pdf.pages}}
				
				{{#if @index}}<div style="page-break-before: always;"></div>{{/if}}
				<main class="main">
					<header class="header">
					<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
					<div style='text-align: center;'>{{#compare @index ">" 0}}{{getPageNumbervcm @index}}/{{getTotalPages ../$pdf.pages}}{{/compare}}</div>
					</header> 
					<footer class="footer">${rft}${footer}<div style='font-weight: bold; text-align: right;'></div></footer>
				</main>
			{{/each}}
		`
		const opt_ftr = { type: "merge", mergeWholeDocument: true, mergeToFront: true, template: { content: tmp_ftr, engine: "handlebars", recipe: "chrome-pdf", helpers: helpers } }
		let opts = [opt_ftr], body, chrome_inv
		if (tmp.bgi) {
			const tmp_bgi = `<header><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></header><style> @page {size: A4;} .bgi{position:fixed; top:0; left:0; width:100%; height:100%; ${tmp.bgi}}</style><div class="bgi"></div>`
			const opt_bgi = { type: "merge", template: { content: tmp_bgi, engine: "none", recipe: "chrome-pdf", chrome: chrome_bgi } }
			opts.push(opt_bgi)
		}
		if (tmp.hdr && tmp.dtl) {
			const hdr = `<header><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></header><style>${sty}</style>${tmp.hdr}`
			const chrome_hdr = { preferCSSPageSize: true, marginTop: "7mm", marginBottom: "15mm", marginRight: "7mm", marginLeft: "7mm" }
			const opt_hdr = { type: "merge", mergeToFront: true, template: { content: hdr, engine: "handlebars", recipe: "chrome-pdf", chrome: chrome_hdr, helpers: helpers } }
			opts.push(opt_hdr)
			body = `<header><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></header><style>${sty}</style>${tmp.dtl}`
			const title = tmp.title ? tmp.title : "10cm"
			chrome_inv = { preferCSSPageSize: true, marginTop: title, marginBottom: "15mm", marginRight: "7mm", marginLeft: "7mm" }
		}
		else {
			body = `<header><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></header><style>${sty}</style>${tmp.body}`
			chrome_inv = { preferCSSPageSize: true, marginTop: "7mm", marginBottom: "15mm", marginRight: "7mm", marginLeft: "7mm" }
		}
		const req = { content: body, engine: "handlebars", recipe: "chrome-pdf", helpers: helpers, chrome: chrome_inv, pdfOperations: opts }
		return req
	},
	 createTemplatePagePXK: (tmp) => {
		const sty = tmp.sty
		// , inv = `<style>${sty}</style>${tmp.inv}`
		const ftr = tmp.ftr, rft = tmp.rft ? tmp.rft : ""
		const footer = ftr ? `{{#lastPage @index ../$pdf.pages.length}}${ftr}{{/lastPage}}` : ``
		const tmp_ftr = `
			<style>
				@page {size: A4;}
				* { box-sizing: border-box;}
				html, body { margin: 0px; padding: 0px;}
				.main { display: flex; flex-direction: column; justify-content: space-between; width: 100%; height: 100%;}
				.footer { width: 100%; padding-bottom: 4mm; padding-right: 5mm; font-size: 9pt;text-align: center; }
				.header { width: 100%; padding-top: 57mm; font-size: 9pt;text-align: center; margin-left: 0mm}
			</style>
			{{#each $pdf.pages}}
				
				{{#if @index}}<div style="page-break-before: always;"></div>{{/if}}
				<main class="main">
					<header class="header">
					<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
					<div style='text-align: center;'>{{#compare @index ">" 0}}{{getPageNumbervcm @index}}/{{getTotalPages ../$pdf.pages}}{{/compare}}</div>
					</header> 
					<footer class="footer">${rft}${footer}<div style='font-weight: bold; text-align: right;'></div></footer>
				</main>
			{{/each}}
		`
		const opt_ftr = { type: "merge", mergeWholeDocument: true, mergeToFront: true, template: { content: tmp_ftr, engine: "handlebars", recipe: "chrome-pdf", helpers: helpers } }
		let opts = [opt_ftr], body, chrome_inv
		if (tmp.bgi) {
			const tmp_bgi = `<header><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></header><style> @page {size: A4;} .bgi{position:fixed; top:0; left:0; width:100%; height:100%; ${tmp.bgi}}</style><div class="bgi"></div>`
			const opt_bgi = { type: "merge", template: { content: tmp_bgi, engine: "none", recipe: "chrome-pdf", chrome: chrome_bgi } }
			opts.push(opt_bgi)
		}
		if (tmp.hdr && tmp.dtl) {
			const hdr = `<header><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></header><style>${sty}</style>${tmp.hdr}`
			const chrome_hdr = { preferCSSPageSize: true, marginTop: "7mm", marginBottom: "15mm", marginRight: "7mm", marginLeft: "7mm" }
			const opt_hdr = { type: "merge", mergeToFront: true, template: { content: hdr, engine: "handlebars", recipe: "chrome-pdf", chrome: chrome_hdr, helpers: helpers } }
			opts.push(opt_hdr)
			body = `<header><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></header><style>${sty}</style>${tmp.dtl}`
			const title = tmp.title ? tmp.title : "10cm"
			chrome_inv = { preferCSSPageSize: true, marginTop: title, marginBottom: "15mm", marginRight: "7mm", marginLeft: "7mm" }
		}
		else {
			body = `<header><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></header><style>${sty}</style>${tmp.body || tmp.inv}`
			chrome_inv = { preferCSSPageSize: true, marginTop: "7mm", marginBottom: "15mm", marginRight: "7mm", marginLeft: "7mm" }
		}
		const req = { content: body, engine: "handlebars", recipe: "chrome-pdf", helpers: helpers, chrome: chrome_inv, pdfOperations: opts }
		return req
	},
//    createRequest: (json) => {
// 		if(json.doc.ent=='vcm'){
// 			if(json.doc.type == '03XKNB')
// 			{
// 				const tmp =  Service.createTemplatePagePXK(json.tmp)
// 				return { data: json.doc, template: tmp }
// 			}
// 			else return { data: json.doc, template: Service.createTemplatePage01GTGT(json.tmp) }
// 		}else{
// 			return { data: json.doc, template: createTemplate(json.tmp) }
// 		}
		
// 	},
	 createTemplateIf: (tmp,ent,type) => {
		if(ent=='vcm'){
			if(type == '03XKNB')
			return Service.createTemplatePagePXK(tmp)
			else return  Service.createTemplatePage01GTGT(tmp)
		}else{
			let data={}
			data.tmp = tmp
			return Service.createRequestAPI(data)
		}
		
	}
}
module.exports = Service