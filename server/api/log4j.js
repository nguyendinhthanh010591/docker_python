const log4js = require("log4js")
const config = require("./config") 
//log4js.configure(config.log4jconf)
log4js.configure({
    "appenders": {
        "default": {
            "type": "dateFile",
            "filename": "E:/Temp/einvoice/log/einvoice.log",
            "pattern": "-yyyy-MM-dd",
            "maxLogSize": 10485760,
            "numBackups": 3,
            "category": "default"
        }
    },
    "categories": {
        "default": { "appenders": ["default"], "level": "DEBUG" }
    }
})
const logger = log4js.getLogger()
module.exports = logger