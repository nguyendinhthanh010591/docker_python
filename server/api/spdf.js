
"use strict"
const pdfkitReferenceMock = require("node-signpdf/dist/helpers/pdfkitReferenceMock")
const abstract_reference = require("node-signpdf/dist/helpers/pdfkit/abstract_reference")
const forge = require("node-forge")
const pki = forge.pki
const redis = require("./redis")
const sign = require("./sign")

const DEFAULT_SIGNATURE_LENGTH = 8192
const DEFAULT_BYTE_RANGE_PLACEHOLDER = '**********'

const getIndexFromRef = (refTable, ref) => {
    let [index] = ref.split(' ')
    index = parseInt(index)
    if (!refTable.offsets.has(index)) throw new Error(`Không xác định được đối tượng "${ref}" (Failed to locate object "${ref}")`)
    return index
}

const findObject = (pdf, refTable, ref) => {
    const index = getIndexFromRef(refTable, ref)
    const offset = refTable.offsets.get(index)
    let slice = pdf.slice(offset)
    slice = slice.slice(0, slice.indexOf('endobj'))
    slice = slice.slice(slice.indexOf('<<') + 2)
    slice = slice.slice(0, slice.lastIndexOf('>>'))
    return slice
}

const getPagesDictionaryRef = (info) => {
    const pagesRefRegex = new RegExp('\\/Type\\s*\\/Catalog\\s*\\/Pages\\s+(\\d+\\s\\d+\\sR)', 'g')
    const match = pagesRefRegex.exec(info.root)
    if (match === null) throw new Error('Không tìm thấy trình mô tả trang. Đây có thể là sự cố trong node-signpdf. \n (Failed to find the pages descriptor. This is probably a problem in node-signpdf.)')
    return match[1]
}

const getPageRef = (pdfBuffer, info) => {
    const pagesRef = getPagesDictionaryRef(info)
    const pagesDictionary = findObject(pdfBuffer, info.xref, pagesRef)
    const kidsPosition = pagesDictionary.indexOf('/Kids')
    const kidsStart = pagesDictionary.indexOf('[', kidsPosition) + 1
    const kidsEnd = pagesDictionary.indexOf(']', kidsPosition)
    const pages = pagesDictionary.slice(kidsStart, kidsEnd).toString()
    const split = pages.trim().split(' ', 3)
    return `${split[0]} ${split[1]} ${split[2]}`
}

const readRefTable = (pdfBuffer, position) => {
    const offsetsMap = new Map()
    let refTable = pdfBuffer.slice(position)
    if (refTable.indexOf('xref') !== 0) throw new Error('Định dạng bảng tham chiếu chéo không mong muốn. \n (Unexpected cross-reference table format.)')
    refTable = refTable.slice(4)
    refTable = refTable.slice(refTable.indexOf('\n') + 1)
    let nextNewLine = refTable.indexOf('\n')
    let line = refTable.slice(0, nextNewLine)
    refTable = refTable.slice(nextNewLine + 1)
    let [startingIndex, length] = line.toString().split(' ')
    startingIndex = parseInt(startingIndex)
    length = parseInt(length)
    const tableRows = []
    let maxOffset = 0
    let maxIndex = 0
    for (let i = startingIndex; i < startingIndex + length; i += 1) {
        nextNewLine = refTable.indexOf('\n')
        line = refTable.slice(0, nextNewLine).toString()
        refTable = refTable.slice(nextNewLine + 1)
        tableRows.push(line)
        let [offset] = line.split(' ')
        offset = parseInt(offset)
        maxOffset = Math.max(maxOffset, offset)
        maxIndex = Math.max(maxIndex, i)
        offsetsMap.set(i, offset)
    }
    return { tableOffset: position, tableRows, maxOffset, startingIndex, maxIndex, offsets: offsetsMap }
}

const readPdf = (pdfBuffer) => {
    const trailerStart = pdfBuffer.lastIndexOf('trailer')
    const trailer = pdfBuffer.slice(trailerStart, pdfBuffer.length - 6)
    if (trailer.lastIndexOf('/Prev') !== -1) throw new Error('Các tệp PDF được cập nhật ngày càng tăng vẫn chưa được hỗ trợ. \n(Incrementally updated PDFs are not yet supported.)')
    let rootSlice = trailer.slice(trailer.indexOf('/Root'))
    rootSlice = rootSlice.slice(0, rootSlice.indexOf('/', 1))
    const rootRef = rootSlice.slice(6).toString().trim()
    let xRefPosition = trailer.slice(trailer.lastIndexOf('startxref') + 10).toString()
    xRefPosition = parseInt(xRefPosition)
    const refTable = readRefTable(pdfBuffer, xRefPosition)
    const root = findObject(pdfBuffer, refTable, rootRef).toString()
    if (root.indexOf('AcroForm') !== -1) throw new Error('Tài liệu đã chứa một biểu mẫu. Điều này chưa được hỗ trợ \n (The document already contains a form. This is not yet supported.)')
    return { xref: refTable, rootRef, root, trailerStart }
}

const createBufferRootWithAcroform = (info, form) => {
    const rootIndex = getIndexFromRef(info.xref, info.rootRef)
    return Buffer.concat([Buffer.from(`${rootIndex} 0 obj\n`), Buffer.from('<<\n'), Buffer.from(`${info.root}\n`), Buffer.from(`/AcroForm ${form}`), Buffer.from('\n>>\nendobj\n')])
}

const createBufferPageWithAnnotation = (pdf, info, pagesRef, widget) => {
    const pagesDictionary = findObject(pdf, info.xref, pagesRef).toString()
    if (pagesDictionary.indexOf('/Annots') !== -1) throw new Error('Đã có / Các thông báo được mô tả. Điều này chưa được hỗ trợ \n (There already are /Annots described. This is not yet supported)')
    const pagesDictionaryIndex = getIndexFromRef(info.xref, pagesRef)
    return Buffer.concat([Buffer.from(`${pagesDictionaryIndex} 0 obj\n`), Buffer.from('<<\n'), Buffer.from(`${pagesDictionary}\n`), Buffer.from(`/Annots [${widget}]`), Buffer.from('\n>>\nendobj\n')])
}

const createBufferTrailer = (pdf, info, addedReferences) => {
    const rows = info.xref.tableRows
    addedReferences.forEach((offset, index) => {
        const paddedOffset = `0000000000${offset}`.slice(-10)
        rows[index] = `${paddedOffset} 00000 n `
    })
    return Buffer.concat([Buffer.from('xref\n'), Buffer.from(`${info.xref.startingIndex} ${rows.length}\n`), Buffer.from(rows.join('\n')), Buffer.from('\ntrailer\n'), Buffer.from('<<\n'), Buffer.from(`/Size ${rows.length}\n`), Buffer.from(`/Prev ${info.xref.tableOffset}\n`), Buffer.from(`/Root ${info.rootRef}\n`), Buffer.from('>>\n'), Buffer.from('startxref\n'), Buffer.from(`${pdf.length}\n`), Buffer.from('%%EOF')])

}


const pad = (str, length) => (Array(length + 1).join('0') + str).slice(-length)
const escapableRe = /[\n\r\t\b\f\(\)\\]/g
const escapable = { '\n': '\\n', '\r': '\\r', '\t': '\\t', '\b': '\\b', '\f': '\\f', '\\': '\\\\', '(': '\\(', ')': '\\)' }
const swapBytes = buff => {
    const l = buff.length
    if (l & 0x01) {
        throw new Error('Chiều dài bộ đệm phải bằng \n(Buffer length must be even)')
    } else {
        for (let i = 0, end = l - 1; i < end; i += 2) {
            const a = buff[i]
            buff[i] = buff[i + 1]
            buff[i + 1] = a
        }
    }
    return buff
}

const convert = object => {
    if (typeof object === 'string') return `/${object}`
    if (object instanceof String) {
        let string = object, isUnicode = false
        for (let i = 0, end = string.length; i < end; i += 1) {
            if (string.charCodeAt(i) > 0x7f) {
                isUnicode = true
                break
            }
        }
        let stringBuffer
        if (isUnicode) stringBuffer = swapBytes(Buffer.from(`\ufeff${string}`, 'utf16le'))
        else stringBuffer = Buffer.from(string, 'ascii')
        string = stringBuffer.toString('binary')
        string = string.replace(escapableRe, c => escapable[c])
        return `(${string})`
    }
    if (Buffer.isBuffer(object)) return `<${object.toString('hex')}>`
    if (object instanceof abstract_reference.default) return object.toString()
    if (object instanceof Date) {
        let string = `D:${pad(object.getUTCFullYear(), 4)}${pad(object.getUTCMonth() + 1, 2)}${pad(object.getUTCDate(), 2)}${pad(object.getUTCHours(), 2)}${pad(object.getUTCMinutes(), 2)}${pad(object.getUTCSeconds(), 2)}Z`
        return `(${string})`
    }
    if (Array.isArray(object)) {
        const items = object.map(e => convert(e)).join(' ')
        return `[${items}]`
    }
    if ({}.toString.call(object) === '[object Object]') {
        const out = ['<<']
        for (const key in object) {
            const val = object[key]
            out.push(`/${key} ${convert(val)}`)
        }
        out.push('>>')
        return out.join('\n')
    }
    if (typeof object === 'number') return Math.round(object * 1e6) / 1e6
    return `${object}`
}

const pdfkitAddPlaceholder = (pdf, reason) => {
    const signature = pdf.ref({ Type: 'Sig', Filter: 'Adobe.PPKLite', SubFilter: 'adbe.pkcs7.detached', ByteRange: [0, DEFAULT_BYTE_RANGE_PLACEHOLDER, DEFAULT_BYTE_RANGE_PLACEHOLDER, DEFAULT_BYTE_RANGE_PLACEHOLDER], Contents: Buffer.from(String.fromCharCode(0).repeat(DEFAULT_SIGNATURE_LENGTH)), Reason: new String(reason), M: new Date() })
    const widget = pdf.ref({ Type: 'Annot', Subtype: 'Widget', FT: 'Sig', Rect: [0, 0, 0, 0], V: signature, T: new String('Signature1'), F: 4, P: pdf.page.dictionary })
    pdf.page.dictionary.data.Annots = [widget]
    const form = pdf.ref({ Type: 'AcroForm', SigFlags: 3, Fields: [widget] })
    pdf._root.data.AcroForm = form
    return { signature, form, widget }
}
const plainAddPlaceholder = (pdf, reason) => {
    const info = readPdf(pdf), pageRef = getPageRef(pdf, info), pageIndex = getIndexFromRef(info.xref, pageRef)
    const addedReferences = new Map()
    const pdfKitMock = {
        ref: input => {
            info.xref.maxIndex += 1
            addedReferences.set(info.xref.maxIndex, pdf.length + 1)
            pdf = Buffer.concat([pdf, Buffer.from('\n'), Buffer.from(`${info.xref.maxIndex} 0 obj\n`), Buffer.from(convert(input)), Buffer.from('\nendobj\n')])
            return new pdfkitReferenceMock.default(info.xref.maxIndex)
        },
        page: {
            dictionary: new pdfkitReferenceMock.default(pageIndex, { data: { Annots: [] } })
        },
        _root: { data: {} }
    }
    const { form, widget } = pdfkitAddPlaceholder(pdfKitMock, reason)
    const rootIndex = getIndexFromRef(info.xref, info.rootRef)
    addedReferences.set(rootIndex, pdf.length + 1)
    pdf = Buffer.concat([pdf, Buffer.from('\n'), createBufferRootWithAcroform(info, form)])
    addedReferences.set(pageIndex, pdf.length + 1)
    pdf = Buffer.concat([pdf, Buffer.from('\n'), createBufferPageWithAnnotation(pdf, info, pageRef, widget)])
    pdf = Buffer.concat([pdf, Buffer.from('\n'), createBufferTrailer(pdf, info, addedReferences)])
    return pdf
}
const signpdf = async (mst, pwd, pdf) => {
    try {
        const b64 = await redis.get(`CA.${mst}`)
        if (!b64) throw new Error(`Khóa CA.${mst} không được tìm thấy \n(Key CA.${mst} is not found)`)
        const der = forge.util.decode64(b64), asn1 = forge.asn1.fromDer(der), p12 = forge.pkcs12.pkcs12FromAsn1(asn1, false, pwd)
        let bags, bag
        bags = p12.getBags({ bagType: pki.oids.certBag })
        bag = bags[pki.oids.certBag][0]
        const cert = bag.cert
        //REM 02row for test
        //const valid = sign.valid(cert, mst)
        //if (!valid) throw new Error("Chứng thư số không hợp lệ")
        bags = p12.getBags({ bagType: pki.oids.pkcs8ShroudedKeyBag })
        bag = bags[pki.oids.pkcs8ShroudedKeyBag][0]
        const privateKey = bag.key
        pdf = plainAddPlaceholder(pdf, 'FPTeinvoice')
        const byteRangePlaceholder = [0, `/${DEFAULT_BYTE_RANGE_PLACEHOLDER}`, `/${DEFAULT_BYTE_RANGE_PLACEHOLDER}`, `/${DEFAULT_BYTE_RANGE_PLACEHOLDER}`]
        const byteRangeString = `/ByteRange [${byteRangePlaceholder.join(' ')}]`
        const byteRangePos = pdf.indexOf(byteRangeString)
        const byteRangeEnd = byteRangePos + byteRangeString.length
        const contentsTagPos = pdf.indexOf('/Contents ', byteRangeEnd)
        const placeholderPos = pdf.indexOf('<', contentsTagPos)
        const placeholderEnd = pdf.indexOf('>', placeholderPos)
        const placeholderLengthWithBrackets = placeholderEnd + 1 - placeholderPos
        const placeholderLength = placeholderLengthWithBrackets - 2
        const byteRange = [0, 0, 0, 0]
        byteRange[1] = placeholderPos
        byteRange[2] = byteRange[1] + placeholderLengthWithBrackets
        byteRange[3] = pdf.length - byteRange[2]
        let actualByteRange = `/ByteRange [${byteRange.join(' ')}]`
        actualByteRange += ' '.repeat(byteRangeString.length - actualByteRange.length)
        pdf = Buffer.concat([pdf.slice(0, byteRangePos), Buffer.from(actualByteRange), pdf.slice(byteRangeEnd)])
        pdf = Buffer.concat([pdf.slice(0, byteRange[1]), pdf.slice(byteRange[2], byteRange[2] + byteRange[3])])
        const aa = [{ type: pki.oids.contentType, value: pki.oids.data }, { type: forge.pki.oids.messageDigest }, { type: forge.pki.oids.signingTime, value: new Date() }]
        const p7 = forge.pkcs7.createSignedData()
        p7.content = forge.util.createBuffer(pdf.toString('binary'))
        p7.addCertificate(cert)
        p7.addSigner({ key: privateKey, certificate: cert, digestAlgorithm: pki.oids.sha256, authenticatedAttributes: aa })
        p7.sign({ detached: true })
        //logger4app.debug(p7.toAsn1())
        const raw = forge.asn1.toDer(p7.toAsn1()).getBytes()
        if (raw.length * 2 > placeholderLength) throw new Error(`Chữ ký vượt quá độ dài trình giữ chỗ :  ${raw.length * 2} > ${placeholderLength} \n (Signature exceeds placeholder length: ${raw.length * 2} > ${placeholderLength})`)
        const signature = Buffer.from(raw, 'binary').toString('hex') + Buffer.from(String.fromCharCode(0).repeat(placeholderLength / 2 - raw.length)).toString('hex')
        pdf = Buffer.concat([pdf.slice(0, byteRange[1]), Buffer.from(`<${signature}>`), pdf.slice(byteRange[1])])
        return pdf.toString("base64")
    } catch (err) {
        throw err
    }
}
exports.signpdf = signpdf





