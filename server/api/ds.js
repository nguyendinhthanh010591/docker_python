"use strict"
const fs = require("fs")
const FormData = require("form-data")
const axios = require("axios")
const sign = require("./sign")
const util = require("./util")
const sec = require("./sec")
const fcloud = require("./fcloud")
const config = require("./config")
const logger4app = require("./logger4app")
const ous = require(`./${config.dbtype}/ous`)

const Service = {
    conf: async (req, res, next) => {
        try {
            const token = sec.decode(req), org = await ous.org(token), sign = org.sign
            if (sign == 1) res.json({ sign: sign, mst: token.mst })
            else res.json({ sign: sign })
        } catch (err) {
            next(err)
        }
    },
    verifyxml: async (req, res, next) => {
        try {
            const file = req.file, xml = file.buffer.toString()
            const result = await sign.verify(xml)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    verifypdf: async (req, res, next) => {
        try {
            const form = new FormData(), file = req.file
            form.append("file", file.buffer, { filename: file.originalname, contentType: file.mimetype })
            axios.defaults.headers.post["Authorization"] = req.headers.authorization
            axios.post(`${config.api}verify`, form, { headers: form.getHeaders(), maxContentLength: Infinity, maxBodyLength: Infinity }).then(response => {
                res.json(response.data)
            }).catch(err => next(err))
        } catch (err) {
            next(err)
        }
    },
    signxml: async (req, res, next) => {
        try {
            const file = req.file, token = sec.decode(req), mst = token.mst, org = await ous.org(token), body = req.body
            const buf = file.buffer, pwd = util.decrypt(org.pwd)
            const str = buf.toString(), ref = body.ref
            const xml = org.sign == 3 ? await fcloud.xml1(util.decrypt(org.usr), pwd, mst, str, ref) : sign.fsign(await sign.ca(mst, pwd), str, ref)
            res.json({ xml: xml })
        }
        catch (err) {
            res.json({ err: err.message })
        }
    },
    signpdf: async (req, res, next) => {
        try {
            const file = req.file, token = sec.decode(req), mst = token.taxc, org = await ous.org(token), body = req.body
            const buf = file.buffer, pwd = util.decrypt(org.pwd), param = JSON.parse(body.param)
            param.pin = pwd
            param.mst = mst
            res.setHeader("Content-Type", "application/pdf")
            res.setHeader("Content-Disposition", "attachment; filename=download.pdf")
            if(org.sign!=2)  throw new Error(`Chưa hỗ trợ kiểu ký số`)
            if (org.sign == 3) {
                param.usr = util.decrypt(org.usr)
                const b64 = await fcloud.pdf(buf, param)
                res.send(Buffer.from(b64, "base64"))
            }
            else {
                param.cn = org.name
                const formData = new FormData()
                formData.append("file", buf, { filename: file.originalname, contentType: file.mimetype })
                formData.append("param", JSON.stringify(param))
                axios.defaults.headers.post["Authorization"] = req.headers.authorization
                axios.post(`${config.api}signp12`, formData, { headers: formData.getHeaders(), maxContentLength: Infinity, maxBodyLength: Infinity, responseType: "stream" }).then(response => { response.data.pipe(res) }).catch(err => next(err))
            }
        }
        catch (err) {
            next(err)
        }
    }
}
module.exports = Service           