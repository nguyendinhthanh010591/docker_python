const log4js = require("log4js")
const config = require("./config")
const logger4app = require("./logger4app")
log4js.configure(config.log4jsConfig)
const logger = log4js.getLogger("einvoice")
module.exports = logger 