"use strict"
const path = require("path")
const fs = require("fs")
const docxt = require("docxtemplater")
const jszip = require("jszip")
const moment = require("moment")
const util = require("./util")
const config = require("./config")
const logger4app = require("./logger4app")
let redis = require("./redis")
const sec = require("./sec")
const inc = require("./inc")
const ous = require("./ous")
const logging = require("./logging")
const { disconnect } = require("process")
const ENT = config.ent
const dbCache = config.dbCache
const GRANT = config.serial_grant
const GRANT_USR = config.serial_usr_grant
const where_get = {
    where_get_mssql: ` where fd between ? and ?`,
    where_get_orcl: ` WHERE NOT ((? > nvl(trunc(td), to_date('31/12/3000','DD/MM/YYYY'))) OR (? < nvl(trunc(fd), to_date('31/12/3000','DD/MM/YYYY')))) `,
    where_get_mysql: ` where fd between ? and ?`,
    where_get_pgsql: ` where fd between ? and ?`
}
const sql_get = {
    sql_get_mssql: `id,taxc,type,form,serial,min,max,cur,status,fd,td,uses,CAST(RIGHT(form,3) as int) idx,des,priority,dt,sendtype,invtype,degree_config`,
    sql_get_orcl: `id "id",taxc "taxc",type "type",form "form",serial "serial",min "min",max "max",cur "cur",status "status",fd "fd",td "td", uses "uses", TO_NUMBER(SUBSTR(form, -3)) "idx",des "des",priority "priority", dt "dt", sendtype "sendtype", invtype "invtype", degree_config "degree_config"`,
    sql_get_mysql: `id id,taxc taxc,type type,form form,serial serial,min min,max max, cur,status status,fd fd,td td,uses uses,des des,SUBSTRING(form,-1,3) idx,priority,dt,sendtype,invtype,degree_config`,
    sql_get_pgsql: `id,taxc,type,form,serial,min,max,cur,status,fd,td,uses,CAST(RIGHT(form,3) as INTEGER) idx,des,priority,dt,sendtype,invtype,degree_config`
}
const sql_post = {
    sql_post_mssql: `select COUNT(*) 'count' from s_statement where stax = ? and JSON_VALUE(json_accepted,'$.noti_taxdt')=(select MAX(JSON_VALUE(json_accepted,'$.noti_taxdt')) 'noti_taxdt' from s_statement where stax =? ) and invtype like ? `,
    sql_post_mysql: `select COUNT(*) 'count' from s_statement where stax = ? and json_accepted > '' and json_accepted ->> '$.noti_taxdt'=(select MAX(json_accepted ->> '$.noti_taxdt') 'noti_taxdt' from s_statement where stax =? and json_accepted > '') and invtype like ? `,
    sql_post_orcl: `select COUNT(*) "count" from s_statement where stax = ? and JSON_VALUE(json_accepted,'$.noti_taxdt')=(select MAX(JSON_VALUE(json_accepted,'$.noti_taxdt')) "noti_taxdt" from s_statement where stax =? ) and invtype like ? `,
    sql_post_pgsql: `select COUNT(*) 'count' from s_statement where stax = ? and (json_accepted -> 'noti_taxdt')=(select MAX(json_accepted -> 'noti_taxdt') 'noti_taxdt' from s_statement where stax =? ) and invtype like ? `
}
const sql_post2 = {
    sql_post2_mssql: `select MAX(JSON_VALUE(json_accepted,'$.noti_taxdt')) 'noti_taxdt' from s_statement where stax =?`,
    sql_post2_mysql: `select MAX(json_accepted ->> '$.noti_taxdt') 'noti_taxdt' from s_statement where json_accepted > '' and stax =?`,
    sql_post2_orcl: `select MAX(JSON_VALUE(json_accepted,'$.noti_taxdt')) "noti_taxdt" from s_statement where stax =?`,sql_post2_pgsql: `select MAX(json_accepted -> 'noti_taxdt') 'noti_taxdt' from s_statement where stax =?`
}
const getSerial = {
    getSerial_mssql: `select id "id",taxc "taxc",type "type",form "form",serial "serial",min "min",max "max",cur "cur",priority "priority",fd "fd" from s_serial where LEN(form) = 1 and (serial like ? or serial like ?) and status=1`,
    getSerial_mysql: `select taxc "taxc",type "type",form "form",serial "serial",min "min",max "max",cur "cur",priority "priority",fd "fd" from s_serial where length(form) = 1 and (serial like 'K22%' or serial like 'C22%') and status=1`,
    getSerial_orcl: `select id "id",taxc "taxc",type "type",form "form",serial "serial",min "min",max "max",cur "cur",priority "priority",fd "fd" from s_serial where length(form) = 1 and (serial like ? or serial like ?)and status=1`,
    getSerial_pgsql: `select taxc "taxc",type "type",form "form",serial "serial",min "min",max "max",cur "cur",priority "priority",fd "fd" from s_serial where length(form) = 1 and (serial like 'K22%' or serial like 'C22%') and status=1`
}
const taxon = async (id) => {
    const i3 = id.substr(0, 3), key = `TAXO.${i3}00`
    let rows = await redis.get(key)
    if (rows) rows = JSON.parse(rows)
    else {
        const result = await inc.execsqlselect(`select id "id",name "value" from s_taxo where id like ?`, [`${i3}%`])
        rows = result
        await redis.set(key, JSON.stringify(rows))
    }
    let obj = rows.find(x => x.id === id)
    if (typeof obj == "undefined") return ""
    else return obj.value
}

const Service = {
    getFormByType: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, id = req.query.id, now = new Date()
            let sql, result, binds, uses = 2
			if (ENT == "dtt") uses = -1
            if (GRANT) {
                sql = `select distinct a.form "id",a.form "value" from s_serial a,s_seou b where a.id=b.se and b.ou=? and a.type=? and a.status=1 and a.uses<>? and a.fd<=? order by form`
                binds = [token.ou, id, uses, now]
            }
            else {
                sql = `select distinct form "id",form "value" from s_serial where taxc=? and type=? and status=1 and uses<>? and fd<=? order by form`
                binds = [taxc, id, uses, now]
            }
            result = await inc.execsqlselect(sql, binds)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    getFormByTypeInv: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, id = req.query.id, now = new Date(), seruseapp = config.SER_USES_APP,ou = req.query.id
            let sql, result, binds, uses = 2, where, where1
            if(`${config.dbtype}` == "orcl"){
                where = "trunc(a.fd)"
                where1 = "trunc(fd)"
            } 
            else {
                where = "a.fd"
                where1 = "fd"
            }
            if (ENT == "dtt") uses = -1
            if (ENT == "hsy") uses = -1
            if (GRANT_USR) {
                sql = `select distinct a.form "id",a.form "value" from s_serial a,s_seusr b where a.id=b.se and b.usrid=? and a.type=? and a.status=1 and a.uses<>? and ${where}<=? and a.uses in ${seruseapp} order by form`
                binds = [token.uid, id, uses, now]
            } else {
                if (GRANT) {
                    sql = `select distinct a.form "id",a.form "value" from s_serial a,s_seou b where a.id=b.se and b.ou=? and a.type=? and a.status=1 and a.uses<>? and ${where}<=? and a.uses in ${seruseapp} order by form`
                    binds = [token.ou, id, uses, now]
                }
                else {
                    sql = `select distinct form "id",form "value" from s_serial where taxc=? and type=? and status=1 and uses<>? and ${where1}<=? and uses in ${seruseapp} order by form`
                    binds = [taxc, id, uses, now]
                }
            }
            result = await inc.execsqlselect(sql, binds)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    getFormByTypeInvEditor: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, id = req.query.id
            let sql, result, binds, uses = 2, where, where1
            if(`${config.dbtype}` == "orcl"){
                where = "trunc(a.fd)"
                where1 = "trunc(fd)"
            } 
            else {
                where = "a.fd"
                where1 = "fd"
            }
			if (ENT == "dtt") uses = -1
            if (GRANT_USR) {
                sql = `select distinct a.form "id",a.form "value" from s_serial a,s_seusr b where a.id=b.se and b.usrid=? and a.type=? order by form`
                binds = [token.uid, id]
            } else {
                if (GRANT) {
                    sql = `select distinct a.form "id",a.form "value" from s_serial a,s_seou b where a.id=b.se and b.ou=? and a.type=? order by form`
                    binds = [token.ou, id]
                }
                else {
                    sql = `select distinct form "id",form "value" from s_serial where taxc=? and type=?  order by form`
                    binds = [taxc, id]
                }
            }
            result = await inc.execsqlselect(sql, binds)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    getFormByTypeInvAndOu: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, id = req.query.id, now = new Date(), seruseapp = config.SER_USES_APP,ou = req.query.ou
            let sql, result, binds, uses = 2
            sql = `select distinct s.form "id",s.form "value" from s_serial s,s_ou ou where s.taxc=ou.mst and ou.id=? and s.type=? and s.status=1 and s.uses<>? and s.fd<=? and s.uses in ${seruseapp}`
            binds = [ou, id, uses, now]
            result = await inc.execsqlselect(sql, binds)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    getAllFormByType: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, id = req.query.id, now = new Date()
            let sql, result, binds
            if (GRANT) {
                sql = `select distinct a.form "id",a.form "value" from s_serial a,s_seou b where a.id=b.se and b.ou=? and a.taxc=? and a.type=? `
                binds = [token.ou, taxc, id]
            }
            else {
                sql = `select distinct form "id",form "value" from s_serial where taxc=? and type=? `
                binds = [taxc, id]
            }
            result = await inc.execsqlselect(sql, binds)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    getAllFormByTypeInv: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, id = req.query.id, now = new Date()
            let sql, result, binds
            if (GRANT_USR) {
                sql = `select distinct a.form "id",a.form "value" from s_serial a,s_seusr b where a.id=b.se and b.usrid=? and a.taxc=? and a.type=? and a.fd<=?`
                binds = [token.uid, taxc, id, now]
            } else {
                if (GRANT) {
                   // sql = `select distinct a.form "id",a.form "value" from s_serial a,s_seusr b where a.id=b.se and b.usrid=? and a.taxc=? and a.type=? and a.fd<=?`
                   // binds = [token.uid, taxc, id, now]
                    sql = `select distinct a.form id,a.form value from s_serial a,s_seou b where a.id=b.se and b.ou=? and a.taxc=? and a.type=? and a.fd<=?`
                    binds = [token.ou, taxc, id, now]
                }
                else {
                    sql = `select distinct form "id",form "value" from s_serial where taxc=? and type=? and fd<=? and status !=3`
                    binds = [taxc, id, now]
                }
            }
            result = await inc.execsqlselect(sql, binds)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    getAllSerialByFormInvIn123: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, id = req.query.id, now = moment().format(config.dtf), sendtype = req.query.sendtype
            let sql, result, binds
            if (GRANT_USR) {
                sql = `select distinct a.form "id",a.form "value" from s_serial a,s_seusr b where a.id=b.se and b.usrid=? and a.taxc=? and a.type=? and a.fd<=? and sendtype =?`
                binds = [token.uid, taxc, id, now, sendtype]
            } else {
                if (GRANT) {
                    sql = `select distinct a.form "id",a.form "value" from s_serial a,s_seusr b where a.id=b.se and b.usrid=? and a.taxc=? and a.type=? and a.fd<=? and sendtype =?`
                    binds = [token.uid, taxc, id, now, sendtype]
                }
                else {
                    sql = `select distinct serial "id",serial "value" from s_serial where taxc=? and form=? and fd<=? and status !=3 and sendtype =?`
                    binds = [taxc, id, now, sendtype]
                }
            }
            result = await inc.execsqlselect(sql, binds)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    getSerialByForm: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, id = req.query.id, now = new Date()
            let sql, result, binds, uses = 2
			if(ENT == "dtt") uses = -1
            if (GRANT) {
                sql = `select distinct a.serial "id",a.serial "value" from s_serial a,s_seou b where a.id=b.se and b.ou=? and a.taxc=? and a.form=? and a.status=1 and a.uses<>? and a.fd<=?`
                binds = [token.ou, taxc, id, uses, now]
            }
            else {
                sql = `select distinct serial "id",serial "value" from s_serial where taxc=? and form=? and status=1 and uses<>? and fd<=?`
                binds = [taxc, id, uses, now]
            }
            result = await inc.execsqlselect(sql, binds)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    getSerialByFormInv: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, id = req.query.id, now = new Date(), seruseapp = config.SER_USES_APP
            let sql, result, binds, uses = 2, where, where1
            if(`${config.dbtype}` == "orcl"){
                where = "trunc(a.fd)"
                where1 = "trunc(fd)"
            } 
            else {
                where = "a.fd"
                where1 = "fd"
            }
			if(ENT == "dtt") uses = -1
            if (GRANT_USR) {
                sql = `select distinct a.serial "id",a.serial "value" from s_serial a,s_seusr b where a.id=b.se and b.usrid=? and a.taxc=? and a.form=? and a.status=1 and a.uses<>? and ${where}<=? and a.uses in ${seruseapp} order by serial`
                binds = [token.uid, taxc, id, uses, now]
            } else {
                if (GRANT) {
                    sql = `select distinct a.serial "id",a.serial "value" from s_serial a,s_seou b where a.id=b.se and b.ou=? and a.taxc=? and a.form=? and a.status=1 and a.uses<>? and ${where}<=? and a.uses in ${seruseapp} order by serial`
                    binds = [token.ou, taxc, id, uses, now]
                }
                else {
                    sql = `select distinct serial "id",serial "value" from s_serial where taxc=? and form=? and status=1 and uses<>? and ${where1}<=? and uses in ${seruseapp} order by serial`
                    binds = [taxc, id, uses, now]
                }
            }
            result = await inc.execsqlselect(sql, binds)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    getSerialByFormEditor: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, id = req.query.id, now = new Date(), seruseapp = config.SER_USES_APP
            let sql, result, binds, uses = 2, where, where1
            if(`${config.dbtype}` == "orcl"){
                where = "trunc(a.fd)"
                where1 = "trunc(fd)"
            } 
            else {
                where = "a.fd"
                where1 = "fd"
            }
			if(ENT == "dtt") uses = -1
            if (GRANT_USR) {
                sql = `select distinct a.serial "id",a.serial "value" from s_serial a,s_seusr b where a.id=b.se and b.usrid=? and a.taxc=? and a.form=? order by serial`
                binds = [token.uid, taxc, id]
            } else {
                if (GRANT) {
                    sql = `select distinct a.serial "id",a.serial "value" from s_serial a,s_seou b where a.id=b.se and b.ou=? and a.taxc=? and a.form=? order by serial`
                    binds = [token.ou, taxc, id]
                }
                else {
                    sql = `select distinct serial "id",serial "value" from s_serial where taxc=? and form=? order by serial`
                    binds = [taxc, id]
                }
            }
            result = await inc.execsqlselect(sql, binds)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    getSerialByFormInvAndOu: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, id = req.query.id,ou = req.query.ou, now = new Date(), seruseapp = config.SER_USES_APP
            let sql, result, binds, uses = 2
			sql = `select distinct s.serial "id",s.serial "value" from s_serial s,s_ou ou where s.taxc=ou.mst and ou.id=? and s.form=? and s.status=1 and s.uses<>? and s.fd<=? and s.uses in ${seruseapp}`
            binds = [ou, id, uses, now]
            result = await inc.execsqlselect(sql, binds)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    getAllSerial: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, now = new Date()
            let sql, result, binds, uses = 2
			// if(ENT == "dtt") uses = -1
            // if (GRANT) {
            //     sql = `select distinct a.serial id,a.serial value from s_serial a,s_seou b where a.id=b.se and b.ou=@1 and a.taxc=@2   order by a.serial`
            //     binds = [token.ou, taxc]
            // }
            // else {
            //     sql = `select distinct serial id,serial value from s_serial where taxc=@1  order by serial`
            //     binds = [taxc]
            // }
            sql = `select distinct serial "id",serial "value" from s_serial where serial is not null order by serial`
            result = await inc.execsqlselect(sql, [])
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    getAllSerialByForm1WithListinv: async (req, res, next) => {
        try {
            const token = sec.decode(req),taxc = token.taxc, now = new Date(), id = req.query.id
            let sql, binds, result, form
            if(id == "01GTKT") form = 1
            else if(id == "02GTTT" || id == "07KPTQ") form = 2
            else if(id == "03XKNB" || id == "04HGDL")  form = 6
            else if(id == "01/TVE" || id == "02/TVE")  form = 5
            if (GRANT) {
                sql = `select distinct a.serial "id",a.serial "value" from s_serial a, s_seou b where a.status in (1,2,4) and a.id=b.se and b.ou=? and a.taxc=? and a.fd<=? and a.degree_config=123 and sendtype=2`
                binds = [token.ou, token.mst, now]
                if(id && id!='*') {
                    sql+="and a.invtype=? "
                    binds.push(id)
                }
            }
            else {
                sql = `select distinct serial "id",serial "value" from s_serial where taxc=? and type =? and form=? and fd<=? and degree_config=123 and status in (1,2,4) and sendtype=2 order by serial`
                binds = [taxc, id, form, now]
            }
            result = await inc.execsqlselect(sql, binds)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    getAllSerialByForm1NoListinv: async (req, res, next) => {
        try {
            const token = sec.decode(req),taxc = token.taxc, now = new Date()
            let sql, binds, result, type = req.query.type, form = req.query.form
            if (GRANT) {
                sql = `select distinct a.serial "id",a.serial "value" from s_serial a, s_seou b where a.status in (1,2,4) and a.id=b.se and b.ou=? and a.taxc=? and a.invtype=? and a.fd<=?  and sendtype <> 2`
                binds = [token.ou, token.mst, req.query.id, now]
            }
            else {
                sql = `select distinct serial "id",serial "value" from s_serial where taxc=? and type = ? and form=? and fd<=? and status in (1,2,4) and (sendtype = 1 or sendtype is null) order by serial`
                binds = [taxc, type, form, now]
            }
            result = await inc.execsqlselect(sql, binds)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    getAllSerialByForm: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, id = req.query.id, now = new Date()
            let sql, result, binds
            if (GRANT) {
                sql = `select distinct a.serial "id",a.serial "value" from s_serial a,s_seou b where a.id=b.se and b.ou=? and a.taxc=? and a.form=? and a.fd<=? order by serial`
                binds = [token.ou, taxc, id, now]
            }
            else {
                sql = `select distinct serial "id",serial "value" from s_serial where taxc=? and form=? and fd<=? order by serial`
                binds = [taxc, id, now]
            }
            result = await inc.execsqlselect(sql, binds)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    getAllSerialByFormInv: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, id = req.query.id, now = new Date()
            let sql, result, binds, where, where1
            if(`${config.dbtype}` == "orcl"){
                where = "trunc(a.fd)"
                where1 = "trunc(fd)"
            } 
            else {
                where = "a.fd"
                where1 = "fd"
            }
            if (GRANT_USR) {
                sql = `select distinct a.serial "id",a.serial "value" from s_serial a,s_seusr b where a.id=b.se and b.usrid=? and a.taxc=? and a.form=? and ${where}<=? order by serial`
                binds = [token.uid, taxc, id, now]
            } else {
                if (GRANT) {
                    sql = `select distinct a.serial "id",a.serial "value" from s_serial a,s_seou b where a.id=b.se and b.ou=? and a.taxc=? and a.form=? and ${where}<=? and status !=3 order by serial`
                    binds = [token.ou, taxc, id, now]
                }
                else {
                    sql = `select distinct serial "id",serial "value" from s_serial where taxc=? and form=? and ${where1}<=? order by serial`
                    binds = [taxc, id, now]
                }
            }
            result = await inc.execsqlselect(sql, binds)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    getAllSerialManualByFormInv: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, id = req.query.id, now = new Date()
            let sql, result, binds, where, where1
            if(`${config.dbtype}` == "orcl"){
                where = "trunc(a.fd)"
                where1 = "trunc(fd)"
            } 
            else {
                where = "a.fd"
                where1 = "fd"
            }
            if (GRANT_USR) {
                sql = `select distinct a.serial "id",a.serial "value" from s_serial a,s_seusr b where a.id=b.se and b.usrid=? and a.taxc=? and a.form=? and ${where}<=? and a.uses =1 order by serial`
                binds = [token.uid, taxc, id, now]
            } else {
                if (GRANT) {
                    sql = `select distinct a.serial "id",a.serial "value" from s_serial a,s_seou b where a.id=b.se and b.ou=? and a.taxc=? and a.form=? and ${where}<=? and status !=3 and a.uses =1  order by serial`
                    binds = [token.ou, taxc, id, now]
                }
                else {
                    sql = `select distinct serial "id",serial "value" from s_serial where taxc=? and form=? and ${where1}<=? and uses =1  order by serial`
                    binds = [taxc, id, now]
                }
            }
            result = await inc.execsqlselect(sql, binds)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    err: async (taxc, form, serial, seq) => {
        if (seq) await redis.lpush(`SERIAL.${taxc}.${form}.${serial}.err`, seq)
    },
    sequence: (taxc, form, serial,idt) => {
        return new Promise(async (resolve, reject) => {
            try {
                const uk = `${taxc}.${form}.${serial}`, msg = `Ký hiệu (Serial) ${uk}`, key = `SERIAL.${uk}`, kax = `${key}.max`, ker = `${key}.err`,kreq=`${key}.req`
                const max = await redis.get(kax)
                if (!max) throw new Error(`${msg} đã hủy hoặc đã hết số \n (${msg} was cancelled or out of numbers)`)
                let val = await redis.rpop(ker)
                if (!val) val = await redis.incr(key)//val = await redis.get(key)
                val = Number(val)
               // val = await redis.incr(key)
                let reqc=await redis.get(kreq)
                if (reqc){
                    let reqcs=reqc.split("__")
                    for (const row of reqcs) {
                        let k=row.split(".")
                        if(k.length>1){
                            if ( val==Number(k[1])){
                                await inc.execsqlinsupddel(`update s_serial set status=?,cur=?,td=? where taxc=? and form=? and serial=? and priority=?`, [4, Number(k[1]), new Date(), taxc, form, serial,Number(k[3])])
                            }
                            //check hieu luc dai so
                            if (val>=Number(k[0]) && val<=Number(k[1])){
                                let result = await inc.execsqlinsupddel(`update s_serial set cur=? where taxc=? and form=? and serial=? and min=? and max=? and status=1`, [val, taxc, form, serial, Number(k[0]), Number(k[1])])
                                let dt= moment(idt).format("YYYYMMDD")
                                if (Number(dt) < Number(k[2])) 
                                {
                                    await redis.lpush(ker, val)
                                    throw new Error(`${msg} đã hết số hoặc hết hiệu lực \n ${msg} out of number or out of date`)
                                }
                            }
                         }
                    }
                }
               
                //val = await redis.incr(key)
                if (val >= Number(max)) {
                    await inc.execsqlinsupddel(`update s_serial set status=?,cur=?,td=? where taxc=? and form=? and serial=? and (? between min and max)`, [4, max, new Date(), taxc, form, serial, max])
                    await redis.del([key, kax, ker,kreq])
                    if (val > Number(max)) throw new Error(`${msg} đã hết số \n (${msg} was out of numbers)`)
                    else resolve(val)
                }
                else resolve(val)
            }
            catch (err) {
                reject(err)
            }
        })
    },
    approve: async (req, res, next) => {
        try {
            let id = req.params.id, result, rows, row,sql,error="", arr = []
            result = await inc.execsqlselect( `select taxc "taxc",type "type",form "form",serial "serial",min "min",max "max",cur "cur",priority "priority",fd "fd" from s_serial where id=? and status=3 and max>cur`, [id])
            rows = result
            if (rows.length == 0) throw new Error(`Không tìm thấy ${id} để duyệt phát hành \n (Cannot found ${id} to approval for releasing)`)
            row = rows[0]
            let seal = row.serial.slice(3,6)
            let year_seal = parseInt(row.serial.slice(1,3))
            let year_seal_back = year_seal - 1
            let seal_chk = 'K' + year_seal_back.toString() + '%'
            let seal_chk_c = 'C' + year_seal_back.toString() + '%'
             // check dai so chua duyet
             sql = `select min(priority) "priority" from s_serial where taxc=? and form=? and serial=? and status=3`
             result = await inc.execsqlselect(sql, [row.taxc,row.form,row.serial])
             let   rowss = result
             if (result.length > 0 && rowss.priority!=null) {
               
                 if (row.priority > (rowss.priority)) {
                        error+= ' Bạn cần duyệt dải có số thứ tự ưu tiên thấp đến cao, dải độ ưu tiên  '+(rowss.priority) +' chưa duyệt' + '(You need to approve the range with low to high precedence, the precedence range '+(rowss.priority) +' unapproved)';
                       
                 }
             }
             //end check
            // check dai so da duyet
            sql = `select max(max) "max" from s_serial where taxc=? and form=? and serial=? and status=1`
            result = await inc.execsqlselect(sql, [row.taxc,row.form,row.serial])
               rowss = result
            if (result.length > 0 && rowss.max!=null) {
              
                if (row.min != (Number(rowss.max) + 1)) {
                       error+= ' Dải số phải liên tiếp với các dải đã duyệt, Từ số phải bắt đầu từ '+(Number(rowss.max) + 1)+'(The range of numbers must be consecutive with the approved ranges, The word number must start from '+(Number(rowss.max) + 1)+')';
                      
                }
            }
            if (!util.isEmpty(error)) throw new Error(error)
            //end check
            const uk = `${row.taxc}.${row.form}.${row.serial}`, key = `SERIAL.${uk}`, kax = `${key}.max`,kreq=`${key}.req`
            let cur = row.cur, min = row.min, max = row.max, val = min-1,condition = `${row.min}.${row.max}.${moment(row.fd).format('YYYYMMDD')}.${row.priority}__`

            sql = `select * from s_serial where taxc=? and form=? and serial=? and status=1`
            result = await inc.execsqlselect(sql, [row.taxc,row.form,row.serial])
            rowss = result

            for (const rowc of rowss) {
                condition += `${rowc.min}.${rowc.max}.${moment(rowc.fd).format('YYYYMMDD')}.${rowc.priority}__`
            }
            const valc = await redis.get(key)
            //Sửa lại hàm set multi thành nhiều hàm set do DB không có hàm này (áp dụng cho SCB)
            if (dbCache) {
                if (valc != null) {
                    redis.set(kax, max)
                    redis.set(kreq, condition)
                    result = await inc.execsqlinsupddel(`update s_serial set status=?,td=? where id=? and status=3 and max>cur`, [1, null, id])
                    res.json(result)
                    const sSysLogs = { fnc_id: 'ser_apr', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt đăng ký sử dụng: ${id}`, msg_id: id, doc: JSON.stringify(row) };
                    logging.ins(req, sSysLogs, next)

                } else {

                    redis.set(key, val)
                    redis.set(kax, max)
                    redis.set(kreq, condition)
                    result = await inc.execsqlinsupddel(`update s_serial set status=?,td=? where id=? and status=3 and max>cur`, [1, null, id])
                    res.json(result)
                    const sSysLogs = { fnc_id: 'ser_apr', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt đăng ký sử dụng: ${id}`, msg_id: id, doc: JSON.stringify(row) };
                    logging.ins(req, sSysLogs, next)


                }
            } else {
                if (valc != null) {
                    redis.multi().set(kax, max).set(kreq, condition).exec(async (err, results) => {
                        if (err) throw new Error(err)
                        result = await inc.execsqlinsupddel(`update s_serial set status=?,td=? where id=? and status=3 and max>cur`, [1, null, id])
                        res.json(result)
                        const sSysLogs = { fnc_id: 'ser_apr', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt đăng ký sử dụng: ${id}`, msg_id: id, doc: JSON.stringify(row) };
                        logging.ins(req, sSysLogs, next)
                    })
                } else {

                    redis.multi().set(key, val).set(kax, max).set(kreq, condition).exec(async (err, results) => {
                        if (err) throw new Error(err)
                        result = await inc.execsqlinsupddel(`update s_serial set status=?,td=? where id=? and status=3 and max>cur`, [1, null, id])
                        res.json(result)
                        const sSysLogs = { fnc_id: 'ser_apr', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt đăng ký sử dụng: ${id}`, msg_id: id, doc: JSON.stringify(row) };
                        logging.ins(req, sSysLogs, next)
                    })

                }
            }
           
            let result_ser = await inc.execsqlselect(`${getSerial[`getSerial_${config.dbtype}`]}`, [seal_chk,seal_chk_c])
              
            for (let row of result_ser) {
            if(row.serial.slice(3,6) == `${seal}`)
            {
                if (!config.dbCache) {
                    let keys = await redis.keys(`TMP.${row.type}.${row.form}.${row.serial}.*`)
                    for (let key of keys) {
                        let fn = key.split(".")
                        let tmp = await redis.get(key)
                        if (fn[3] && fn[3].includes(`${year_seal_back}`) && tmp) {
                            // console.log(fn[3])
                            fn[3] = fn[3].replace(`${year_seal_back}`, `${year_seal}`)
                            // console.log(fn[3])
                            // console.log(`${fn[0]}.${fn[1]}.${fn[2]}.${fn[3]}.${fn[4]}`)
                            await redis.set(`${fn[0]}.${fn[1]}.${fn[2]}.${fn[3]}.${fn[4]}`, tmp)
                            
                        }                        
                    }                  
                }
                
                else {
                    let temps = await inc.execsqlselect(`select keyname "keyname", keyvalue "keyvalue" from s_listvalues where keyname like 'TMP.${row.type}.${row.form}.${row.serial}.%'`, [])
                    //console.log(temps)
                    for (let temp of temps) {
                        let keyname = temp.keyname
                        let fn = keyname.split(".")
                        let keyvalue = temp.keyvalue
                        if (fn[3] && fn[3].includes(`${year_seal_back}`) && keyvalue) {
                            //console.log(fn[3])
                            fn[3] = fn[3].replace(`${year_seal_back}`, `${year_seal}`)
                            // console.log(fn[3])
                            // console.log(`${fn[0]}.${fn[1]}.${fn[2]}.${fn[3]}.${fn[4]}`)
                            await inc.execsqlinsupddel(`update s_listvalues set keyvalue = ? where keyname = ?`, [keyvalue,`${fn[0]}.${fn[1]}.${fn[2]}.${fn[3]}.${fn[4]}`])
                            
                        }
                    }                    
                }
                break
            }
            else continue
        }
        //chuyển phân quyền 
        for (let row of result_ser) {
            if(row.serial.slice(3,6) == `${seal}`)
            {
                sql = `select ou from s_seou where se=?`
                result = await inc.execsqlselect(sql, [row.id])
                for(let row of result){
                    arr.push(row.ou)
                }
                if (arr.length > 0) {
                    for (const ou of arr) {
                        await inc.execsqlinsupddel(`insert into s_seou (se,ou) values (?,?)`, [id, ou])
                    }                  
                }
                break
            }
            else continue
        }
        }
        catch (err) {
            next(err)
        }
    },
    cancel: async (req, res, next) => {
        try {
            let id = req.params.id, result, rows, row
            result = await inc.execsqlselect(`select taxc "taxc",form "form",serial "serial",min "min" from s_serial where id=? and status=1`, [id])
            rows = result
            if (rows.length == 0) throw new Error(`Không tìm thấy ${id} để hủy phát hành \n (Cannot found ${id} to cancel release)`)
            row = rows[0]
            const uk = `${row.taxc}.${row.form}.${row.serial}`, key = `SERIAL.${uk}`, kax = `${key}.max`, ker = `${key}.err`,kreq=`${key}.req`
            let min = row.min, cur = await redis.get(key)
           // if (!cur || cur < min) cur = min
            //Sửa lại hàm set multi thành nhiều hàm set do DB không có hàm này (áp dụng cho SCB)
            if (dbCache) {
                redis.del(key)
                redis.del(kax)
                redis.del(ker)
                redis.del(kreq)
                result = await inc.execsqlinsupddel(`update s_serial set status=?,td=? where taxc=? and form=? and serial=? and status=1`, [2, moment(new Date()).format('YYYY-MM-DD HH:mm:ss'), row.taxc, row.form, row.serial])

                res.json(result)
                result = await inc.execsqlinsupddel(`update s_serial set cur=? where taxc=? and form=? and serial=? and status=1 and min<=? and max>?`, [cur, row.taxc, row.form, row.serial, cur, cur])
                const sSysLogs = { fnc_id: 'ser_cancel', src: config.SRC_LOGGING_DEFAULT, dtl: `Hủy đăng ký sử dụng: ${id}`, msg_id: id, doc: JSON.stringify(row) };
                logging.ins(req, sSysLogs, next)
            }
            else {
                redis.multi().del(key).del(kax).del(ker).del(kreq).exec(async (err, results) => {
                    if (err) throw new Error(err)
                    result = await inc.execsqlinsupddel(`update s_serial set status=?,td=? where taxc=? and form=? and serial=? and status=1`, [2, util.convertDate(moment(new Date()).format('YYYY-MM-DD HH:mm:ss')), row.taxc, row.form, row.serial])

                    res.json(result)
                    result = await inc.execsqlinsupddel(`update s_serial set cur=? where taxc=? and form=? and serial=? and status=1 and min<=@1 and max>@1`, [cur, row.taxc, row.form, row.serial, cur, cur])
                    const sSysLogs = { fnc_id: 'ser_cancel', src: config.SRC_LOGGING_DEFAULT, dtl: `Hủy đăng ký sử dụng: ${id}`, msg_id: id, doc: JSON.stringify(row) };
                    logging.ins(req, sSysLogs, next)
                })
            }
        }
        catch (err) {
            next(err)
        }
    },
    mulapprove: async (req, res, next) => {
        try {
            let ids=req.body.id
            let sql, result, rows, row, error="", arr=[]
            for(const serial of ids) {
                const {id} = serial
                sql = `select taxc "taxc",type "type",form "form",serial "serial",min "min",max "max",cur "cur",priority "priority",fd "fd" from s_serial where id=? and status=3 and max>cur`
                result = await inc.execsqlselect(sql, [id])
                rows = result
                if (rows.length == 0) {
                    throw new Error(`Không tìm thấy ${id} để duyệt phát hành \n (Cannot found ${id} to approval for releasing)`)
                    
                }
                row = rows[0]
            let seal = row.serial.slice(3,6)
            let year_seal = parseInt(row.serial.slice(1,3))
            let year_seal_back = year_seal - 1
            let seal_chk = 'K' + year_seal_back.toString() + '%'
            let seal_chk_c = 'C' + year_seal_back.toString() + '%'
             // check dai so chua duyet
             sql = `select min(priority) "priority" from s_serial where taxc=? and form=? and serial=? and status=3`
             result = await inc.execsqlselect(sql, [row.taxc,row.form,row.serial])
             let   rowss = result
             if (result.length > 0 && rowss.priority!=null) {
               
                 if (row.priority > (rowss.priority)) {
                        error+= ' Bạn cần duyệt dải có số thứ tự ưu tiên thấp đến cao, dải độ ưu tiên  '+(rowss.priority) +' chưa duyệt' + '(You need to approve the range with low to high precedence, the precedence range '+(rowss.priority) +' unapproved)';
                       
                 }
             }
             //end check
            // check dai so da duyet
            sql = `select max(max) "max" from s_serial where taxc=? and form=? and serial=? and status=1`
            result = await inc.execsqlselect(sql, [row.taxc,row.form,row.serial])
               rowss = result
            if (result.length > 0 && rowss.max!=null) {
              
                if (row.min != (Number(rowss.max) + 1)) {
                       error+= ' Dải số phải liên tiếp với các dải đã duyệt, Từ số phải bắt đầu từ '+(Number(rowss.max) + 1)+'(The range of numbers must be consecutive with the approved ranges, The word number must start from '+(Number(rowss.max) + 1)+')';
                      
                }
            }
            if (!util.isEmpty(error)) throw new Error(error)
            //end check
            const uk = `${row.taxc}.${row.form}.${row.serial}`, key = `SERIAL.${uk}`, kax = `${key}.max`,kreq=`${key}.req`
            let cur = row.cur, min = row.min, max = row.max, val = min-1,condition = `${row.min}.${row.max}.${moment(row.fd).format('YYYYMMDD')}.${row.priority}__`

            sql = `select * from s_serial where taxc=? and form=? and serial=? and status=1`
            result = await inc.execsqlselect(sql, [row.taxc,row.form,row.serial])
            rowss = result

            for (const rowc of rowss) {
                condition += `${rowc.min}.${rowc.max}.${moment(rowc.fd).format('YYYYMMDD')}.${rowc.priority}__`
            }
            const valc = await redis.get(key)
            //Sửa lại hàm set multi thành nhiều hàm set do DB không có hàm này (áp dụng cho SCB)
            if (dbCache) {
                if (valc != null) {
                    redis.set(kax, max)
                    redis.set(kreq, condition)
                    result = await inc.execsqlinsupddel(`update s_serial set status=?,td=? where id=? and status=3 and max>cur`, [1, null, id])
                   // res.json(result)
                    const sSysLogs = { fnc_id: 'ser_apr', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt đăng ký sử dụng: ${id}`, msg_id: id, doc: JSON.stringify(row) };
                    logging.ins(req, sSysLogs, next)

                } else {

                    redis.set(key, val)
                    redis.set(kax, max)
                    redis.set(kreq, condition)
                    result = await inc.execsqlinsupddel(`update s_serial set status=?,td=? where id=? and status=3 and max>cur`, [1, null, id])
                    //res.json(result)
                    const sSysLogs = { fnc_id: 'ser_apr', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt đăng ký sử dụng: ${id}`, msg_id: id, doc: JSON.stringify(row) };
                    logging.ins(req, sSysLogs, next)


                }
            } else {
                if (valc != null) {
                    redis.multi().set(kax, max).set(kreq, condition).exec(async (err, results) => {
                        if (err) throw new Error(err)
                        result = await inc.execsqlinsupddel(`update s_serial set status=?,td=? where id=? and status=3 and max>cur`, [1, null, id])
                        //res.json(result)
                        const sSysLogs = { fnc_id: 'ser_apr', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt đăng ký sử dụng: ${id}`, msg_id: id, doc: JSON.stringify(row) };
                        logging.ins(req, sSysLogs, next)
                    })
                } else {

                    redis.multi().set(key, val).set(kax, max).set(kreq, condition).exec(async (err, results) => {
                        if (err) throw new Error(err)
                        result = await inc.execsqlinsupddel(`update s_serial set status=?,td=? where id=? and status=3 and max>cur`, [1, null, id])
                        //res.json(result)
                        const sSysLogs = { fnc_id: 'ser_apr', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt đăng ký sử dụng: ${id}`, msg_id: id, doc: JSON.stringify(row) };
                        logging.ins(req, sSysLogs, next)
                    })

                }
            }
           
            let result_ser = await inc.execsqlselect(`${getSerial[`getSerial_${config.dbtype}`]}`, [seal_chk,seal_chk_c])
              
            
            for (let row of result_ser) {
            if(row.serial.slice(3,6) == `${seal}`)
            {
                if (!config.dbCache) {
                    let keys = await redis.keys(`TMP.${row.type}.${row.form}.${row.serial}.*`)
                    for (let key of keys) {
                        let fn = key.split(".")
                        let tmp = await redis.get(key)
                        if (fn[3] && fn[3].includes(`${year_seal_back}`) && tmp) {
                            // console.log(fn[3])
                            fn[3] = fn[3].replace(`${year_seal_back}`, `${year_seal}`)
                            // console.log(fn[3])
                            // console.log(`${fn[0]}.${fn[1]}.${fn[2]}.${fn[3]}.${fn[4]}`)
                            await redis.set(`${fn[0]}.${fn[1]}.${fn[2]}.${fn[3]}.${fn[4]}`, tmp)
                            
                        }                        
                    }                  
                }
                
                else {
                    let temps = await inc.execsqlselect(`select keyname "keyname", keyvalue "keyvalue" from s_listvalues where keyname like 'TMP.${row.type}.${row.form}.${row.serial}.%'`, [])
                    //console.log(temps)
                    for (let temp of temps) {
                        let keyname = temp.keyname
                        let fn = keyname.split(".")
                        let keyvalue = temp.keyvalue
                        if (fn[3] && fn[3].includes(`${year_seal_back}`) && keyvalue) {
                            //console.log(fn[3])
                            fn[3] = fn[3].replace(`${year_seal_back}`, `${year_seal}`)
                            // console.log(fn[3])
                            // console.log(`${fn[0]}.${fn[1]}.${fn[2]}.${fn[3]}.${fn[4]}`)
                            await inc.execsqlinsupddel(`update s_listvalues set keyvalue = ? where keyname = ?`, [keyvalue,`${fn[0]}.${fn[1]}.${fn[2]}.${fn[3]}.${fn[4]}`])
                            
                        }
                    }                    
                }
                break
            }
            else continue
        }
        //chuyển phân quyền 
        for (let row of result_ser) {
            if(row.serial.slice(3,6) == `${seal}`)
            {
                sql = `select ou from s_seou where se=?`
                result = await inc.execsqlselect(sql, [row.id])
                for(let row of result){
                    arr.push(row.ou)
                }
                if (arr.length > 0) {
                    for (const ou of arr) {
                        await inc.execsqlinsupddel(`insert into s_seou (se,ou) values (?,?)`, [id, ou])
                    }                  
                }
                break
            }
            else continue
        }
        arr = []
        }
        res.json(1)
    }
        catch (err) {
            next(err)
        }
    },
    mulcancel: async (req, res, next) => {
        try {
            let ids=req.body.id
            let sql, result, rows, row,  success=[]
            for(const serial of ids) {
                const {id}= serial
                result = await inc.execsqlselect(`select taxc "taxc",form "form",serial "serial",min "min" from s_serial where id=? and status=1`, [id])
                rows = result
                if (rows.length == 0) {
                    throw new Error(`Không tìm thấy ${id} để hủy phát hành \n (Cannot found ${id} to cancel release)`)
                }
                const uk = `${row.taxc}.${row.form}.${row.serial}`, key = `SERIAL.${uk}`, kax = `${key}.max`, ker = `${key}.err`,kreq=`${key}.req`
            let min = row.min, cur = await redis.get(key)
           // if (!cur || cur < min) cur = min
            //Sửa lại hàm set multi thành nhiều hàm set do DB không có hàm này (áp dụng cho SCB)
            if (dbCache) {
                redis.del(key)
                redis.del(kax)
                redis.del(ker)
                redis.del(kreq)
                result = await inc.execsqlinsupddel(`update s_serial set status=?,td=? where taxc=? and form=? and serial=? and status=1`, [2, moment(new Date()).format('YYYY-MM-DD HH:mm:ss'), row.taxc, row.form, row.serial])

                res.json(result)
                result = await inc.execsqlinsupddel(`update s_serial set cur=? where taxc=? and form=? and serial=? and status=1 and min<=? and max>?`, [cur, row.taxc, row.form, row.serial, cur, cur])
                const sSysLogs = { fnc_id: 'ser_cancel', src: config.SRC_LOGGING_DEFAULT, dtl: `Hủy đăng ký sử dụng: ${id}`, msg_id: id, doc: JSON.stringify(row) };
                logging.ins(req, sSysLogs, next)
            }
            else {
                redis.multi().del(key).del(kax).del(ker).del(kreq).exec(async (err, results) => {
                    if (err) throw new Error(err)
                    result = await inc.execsqlinsupddel(`update s_serial set status=?,td=? where taxc=? and form=? and serial=? and status=1`, [2, util.convertDate(moment(new Date()).format('YYYY-MM-DD HH:mm:ss')), row.taxc, row.form, row.serial])

                    res.json(result)
                    result = await inc.execsqlinsupddel(`update s_serial set cur=? where taxc=? and form=? and serial=? and status=1 and min<=@1 and max>@1`, [cur, row.taxc, row.form, row.serial, cur, cur])
                    const sSysLogs = { fnc_id: 'ser_cancel', src: config.SRC_LOGGING_DEFAULT, dtl: `Hủy đăng ký sử dụng: ${id}`, msg_id: id, doc: JSON.stringify(row) };
                    logging.ins(req, sSysLogs, next)
                })
            }
        }}
        catch (err) {
            next(err)
        }
    },
    extend: async (req, res, next) => {
        try{
            let result,getdate,id,serial,yearnew,today,year,rows,seal,serialNew,bind,getdatenew,serial_chk,serial_first,serial_stt,count = 0
            let year_clone = parseInt(req.body.year)
            today = new Date()
            getdate = today.getFullYear()
            getdatenew = year_clone +1
            year = year_clone - 2000
            yearnew = year + 1
            result =  await inc.execsqlselect(`select * from s_serial where serial like 'k${year}%' or serial like 'c${year}%'`)
            rows = result
            let fdnew = new Date(`${getdatenew}-01-01 00:00:00.000`)
            if(result.length>0){
                for(let row of rows){
                    id = row.id
                    serial = await inc.execsqlselect(`select serial "serial", status "status" from s_serial where id = ${id}`)
                    serial_first = (serial[0].serial).slice(0,1)
                    serial_stt = serial[0].status
                    if(serial_stt == 1 || serial_stt == 4){
                    count++
                    if(serial_first == 'K'){
                        seal = (serial[0].serial).slice(3,6)
                        serialNew = 'K'+yearnew + seal
                        serial_chk = await inc.execsqlselect(`select * from s_serial where serial like 'K${yearnew}${seal}'`)
                        if(serial_chk.length > 0){
                            continue
                        }else{
                            bind = [row.taxc,row.type,row.form,serialNew,row.min,row.max,0,3,fdnew,null,today,row.uc,row.uses,row.degree_config,row.sendtype?row.sendtype:null,row.invtype?row.invtype:null,row.priority?row.priority:null,row.des?row.des:null]//trạng thái đã duyệt là status = 1 và td về null nên insert ở trạng thái đã duyệt luôn
                            await inc.execsqlinsupddel(`insert into s_serial(taxc,type,form,serial,min,max,cur,status,fd,td,dt,uc,uses,degree_config,sendtype,invtype,priority,des) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,bind)
                        } 
                    }else{
                        seal = (serial[0].SERIAL).slice(3,6)
                        serialNew = 'C'+yearnew + seal
                        serial_chk = await inc.execsqlselect(`select * from s_serial where serial like 'C${yearnew}${seal}'`)
                        if(serial_chk.length > 0){
                            continue
                        }else{
                            bind = [row.taxc,row.type,row.form,serialNew,row.min,row.max,0,3,fdnew,null,today,row.uc,row.uses,row.degree_config,row.sendtype?row.sendtype:null,row.invtype?row.invtype:null,row.priority?row.priority:null,row.des?row.des:null]//trạng thái đã duyệt là status = 1 và td về null nên insert ở trạng thái đã duyệt luôn
                            await inc.execsqlinsupddel(`insert into s_serial(taxc,type,form,serial,min,max,cur,status,fd,td,dt,uc,uses,degree_config,sendtype,invtype,priority,des) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,bind)
                        } 
                    }                                 
                }                          
                }
                if(count == 0){
                    res.json({ resultnew: 3 })
                }else{
                    res.json({ resultnew: 1 }) 
                }                                                   
            }
            res.json({ resultnew: 3 })
        }
        catch (err) {
            res.json({ resultnew: 2 })
         }
    },
    getsexou: async (req, res, next) => {
        try {
            const params = req.params, mst = params.mst, se = params.se
            const sql = `select x.id "id",x.name "name",x.sel "sel" from (select a.id,a.name,0 sel from s_ou a where a.mst= ? and not exists (select 1 from s_seou where se= ? and ou=a.id) union select a.id,a.name,1 sel from s_ou a,s_seou b where a.mst=? and b.se=? and b.ou=a.id) x order by x.id`
            const result = await inc.execsqlselect(sql, [mst, se, mst, se]) //VinhHQ12 gom DB
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    getseou: async (req, res, next) => {
        try {
            const token = sec.decode(req), query = req.query, sort = query.sort, filter = query.filter
            let order, where = "where status=1 and taxc=?", sql, result
            let binds = [token.taxc]
            if (filter) {
                let val, i = 2
                Object.keys(filter).forEach(key => {
                    val = filter[key]
                    if (val && val !== "null") {
                        if (key == "fd") {
                            where += ` and ${key}>=?`
                            binds.push(new Date(val))
                        }
                        else {
                            where += ` and ${key}=?`
                            binds.push(val)
                        }
                    }
                })
            }
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = `order by ${key} ${sort[key]}`
                })
            }
            else order = "order by id"
            sql = `select id "id",taxc "taxc",type "type",form "form",serial "serial",min "min",max "max",cur "cur",fd "fd",uses "uses" from s_serial ${where} ${order}`
            result = await inc.execsqlselect(sql, binds)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    postseou: async (req, res, next) => {
        try {
            const token = sec.decode(req), uid = token.uid, body = req.body, se = body.se, arr = body.arr
            await inc.execsqlinsupddel(`delete from s_seou where se=?`, [se])
            let binds
            if (arr.length > 0) {
                binds = []
                for (const ou of arr) {
                    binds.push([se, ou])
                    await inc.execsqlinsupddel(`insert into s_seou (se,ou) values (?,?)`, [se, ou]) // VinhHQ12 gom DB
                }
            }
            const sSysLogs = { fnc_id: 'ser_grant', src: config.SRC_LOGGING_DEFAULT, dtl: `Gán đăng ký sử dụng`, doc: JSON.stringify(binds)};
            logging.ins(req,sSysLogs,next) 
            res.json(1)
        } catch (err) {
            next(err)
        }
    },
    //HungLQ them phan quyen serial theo user
    getsexusr: async (req, res, next) => {
        try {
            const token = sec.decode(req), params = req.params, query = req.query, sort = query.sort, filter = query.filter, usid = params.usid, seruseapp = config.SER_USES_GRANT
            let order, where = `where status in (1,2,4) and uses in ${seruseapp} and taxc=?`, sql, result, sqlexists, sqlnotexists
            let binds = [token.taxc], i = 1
            if (ENT == "vcm") {
                where = `where status in (1,2,4) and uses in ${seruseapp}`
                binds = []
                i = 0
            }
            if (filter) {
                let val
                Object.keys(filter).forEach(key => {
                    val = filter[key]
                    if (val && val !== "null") {
                        if (key == "fd") {
                            where += ` and ${key}>=?`
                            binds.push(new Date(val))
                        }
                        else {
                            where += ` and ${key}=?`
                            binds.push(val)
                        }
                    }
                })
            }
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = `order by ${key} ${sort[key]}`
                })
            }
            else order = "order by taxc"
            i++

            sqlnotexists = `select 0 "sel",id "id",taxc "taxc",type "type",form "form",serial "serial",min "min",max "max",cur "cur",fd "fd",uses "uses",status "status" from s_serial a ${where} and not exists (select 1 from s_seusr where usrid=? and se=a.id)`
            
            sqlexists = `select 1 "sel",id "id",taxc "taxc",type "type",form "form",serial "serial",min "min",max "max",cur "cur",fd "fd",uses "uses",status "status" from s_serial a, s_seusr b ${where} and usrid=? and b.se=a.id`
            
            sql = `select * from (${sqlnotexists} union ${sqlexists}) x ${order}`
            // gán quyền TBPH cho người dùng truyền thiếu taxc vào binding
            binds.push(usid,token.taxc, usid)
            result = await inc.execsqlselect(sql, binds)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    getseusr: async (req, res, next) => {
        try {
            const token = sec.decode(req)
            const params = req.params, se = params.se
            //let sql = `select id "id",name "name" from s_user where (ou=? or id='__admin') order by id`, binds = [token.ou]
            let sql = `select id "id",name "name" from s_user where id in (select user_id from s_manager where taxc = ?) order by id`, binds = [token.taxc]
            if (ENT == "vcm") {
                sql = `select id "id",name "name" from s_user order by id`
                binds = []
            }
            const result = await inc.execsqlselect(sql, binds)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
        
    },
    postseusr: async (req, res, next) => {
        try {
            const token = sec.decode(req), uid = token.uid, body = req.body, usid = body.usid, arr = body.arr
            await inc.execsqlinsupddel(`delete from s_seusr where usrid=?`, [usid])
            let binds = []
            if (arr.length > 0) {
                for (const se of arr) {
                  //  binds.push()
					await inc.execsqlinsupddel(`insert into s_seusr (se,usrid) values (?,?)`, [se, usid])
                }
                
            }
            const sSysLogs = { fnc_id: 'ser_grant', src: config.SRC_LOGGING_DEFAULT, dtl: `Gán đăng ký sử dụng cho NSD`, doc: JSON.stringify(binds)};
            logging.ins(req,sSysLogs,next) 
            res.json(1)
        } catch (err) {
            next(err)
        }
    }, //HungLQ them phan quyen serial theo user    
    get: async (req, res, next) => {
        try {
            const query = req.query, filter = JSON.parse(query.filter)
            const sort = query.sort, start = query.start ? query.start : 0, count = query.count ? query.count : 10
            let where = `${where_get[`where_get_${config.dbtype}`]}`, order, sql, result, ret, val, i = 3
            let binds = [new Date(filter.fd), new Date(moment(filter.td).endOf("day"))], str, arr = [], pagerobj = inc.getsqlpagerandbind(start, count)
            Object.keys(filter).forEach((key) => {
                val = filter[key]
                if (val) {
                    switch (key) {
                        case "fd":
                            break
                        case "td":
                            break
                        case ("serial") :
                        case ("taxc") :
                                let str = "", arr = String(val).split(",")
                                for (const row of arr) {
                                    let d = row.split("|")
                                    str += `'${d[0]}',`
                                }
                                where += ` and ${key} in (${str.slice(0, -1)})`
                                break
                        default:
                            where += ` and ${key}=?`
                            binds.push(val)
                            break
                    }
                }
            })
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = `order by ${key} ${sort[key]}`
                })
            }
            else order = "order by id"
            sql = `select ${sql_get[`sql_get_${config.dbtype}`]} from s_serial ${where} ${order} ${pagerobj.vsqlpagerstr}`
            for (let t of pagerobj.vsqlpagerbind) binds.push(t)
            result = await inc.execsqlselect(sql, binds)
            ret = { data: result, pos: start }
            if (start == 0) {
                sql = `select count(*) "total" from s_serial ${where}`
                let bindscount = []
                for (let i = 0; i < binds.length - 2;i++) bindscount.push(binds[i])
                result = await inc.execsqlselect(sql, bindscount)
                ret.total_count = result[0].total
            }
            const sSysLogs = { fnc_id: 'ser_get', src: config.SRC_LOGGING_DEFAULT, dtl: `Tra cứu đăng ký sử dụng:  ${query.filter}`, msg_id: "", doc: query.filter};
            logging.ins(req,sSysLogs,next)
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const token = sec.decode(req), body = req.body, operation = body.webix_operation, org = await ous.org(token), degree_config = config.DEGREE_CONFIG
            let binds, sql, result
            if (operation == "update") {
                sql = `update s_serial set type=?,form=?,serial=?,min=?,max=?,cur=?,fd=?,uc=?,uses=?,taxc=?,des=?,priority=?,sendtype=?,invtype=?,degree_config=? where id=?`
                const uid = token.uid, type = body.type, idx = body.idx
                const form = degree_config == "119" ? `${type}0/${idx.padStart(3, "0")}` : body.form, seri = body.serial, min = body.min, max = body.max, arr = body.taxc.split(","), fd = new Date(body.fd), uses = body.uses, id =body.id,des=body.des,priority=body.priority, sendtype = body.sendtype, invtype = body.invtype
                body.form = form
                binds = []
                // let row = { type: type, form: form, serial: seri, min: min, max: max, cur: min, fd: fd, uc: uid, uses: uses, id: id }
                for (const taxc of arr) {
                    // row.taxc = taxc
                    binds =[type, form, seri, min, max, min, fd, uid, uses, taxc,des,priority,sendtype,invtype,degree_config,id]
                    await inc.execsqlinsupddel(sql, binds)
                }
                const sSysLogs = { fnc_id: 'ser_upd', src: config.SRC_LOGGING_DEFAULT, dtl: `Sửa đăng ký sử dụng:  ${id}`, msg_id: id, doc: JSON.stringify(binds)};
                logging.ins(req,sSysLogs,next)
                //update
            } else {
                if (operation == "insert") {
                    const uid = token.uid, type = body.type, idx = body.idx
                    const form = degree_config == "119" ? `${type}0/${idx.padStart(3, "0")}` : body.form, seri = body.serial, min = body.min, max = body.max, arr = body.taxc.split(","), fd = new Date(body.fd), uses = body.uses,des=body.des,priority=body.priority, sendtype = body.sendtype, invtype = body.invtype
                    body.form = form
                    binds = []
                    let  error = ""
                    for (const taxc of arr) {
                        let bindroot = [type, form, seri, min, max, 0, util.convertDate(moment(fd).format('YYYY-MM-DD')), uid, uses, taxc,des,priority]                        
                        if (degree_config == "123") {
                            bindroot.push(sendtype)
                            bindroot.push(invtype!=''?invtype:null)
                            bindroot.push(degree_config)
                        } else {
                            bindroot.push(null)
                            bindroot.push(null)
                            bindroot.push(degree_config)
                        }
                        binds.push(bindroot)
                        //check ban ghi ton tại
                        sql = `select *  from s_serial where taxc=? and form=? and serial=?`
                        result = await inc.execsqlselect(sql, [taxc,form,seri])
                        let   rows = result
                        if (rows.length > 0) {
                           for(const row of rows) {
                            if (priority == row.priority) {
                                error+= ' Bản ghi đã tồn tại' + '(The record already exits)'
                                throw new Error(error)
                                  }
                           }
                            
                            
                        }
                        sql = `select max(max) "max",max(fd) "fd" from s_serial where taxc=? and form=? and serial=?`
                        result = await inc.execsqlselect(sql, [taxc,form,seri])
                        rows = result
                        if (rows.length > 0 && rows[0].max!=null) {
                            if(rows[0].max == 99999999) error+= 'Dải số hóa đơn NSD kê khai đã được đăng ký đến số HĐ tối đa, '
                            else if (min != (rows[0].max + 1)) {
                                   error+= ' Dải số phải liên tiếp với các dải đã khai, Từ số phải bắt đầu từ '+(rows[0].max + 1)+'. (The range of numbers must be consecutive with the declared ranges, The word number must start from '+ (rows[0].max + 1) 
                                   throw new Error(error)
                            }
                            if (moment(fd).format("YYYYMMDD") < moment(rows[0].fd).format("YYYYMMDD")) {
                                error+= ' Ngày hiệu lực phải lớn hơn hoặc bằng các dải đã khai '+'(The effective date must be greater than or equal to the declared ranges)'
                                throw new Error(error)
                         }
                            
                        }
                        sql = `${sql_post[`sql_post_${config.dbtype}`]}`
                        result = await inc.execsqlselect(sql, [taxc,taxc,`%${type}%`])
                        rows = result
                        if (rows.length > 0) {
                            if (rows[0].count == 0) {
                                error+= ` Loại hoá đơn chưa được đăng ký với mã số thuế ${taxc}`+`(Chemical types have not been registered with the tax code  ${taxc})`
                                throw new Error(error)
                         }
                        }

                        sql = `${sql_post2[`sql_post2_${config.dbtype}`]}`
                        result = await inc.execsqlselect(sql, [taxc])
                        rows = result
                        if (rows.length > 0) {
                            if (moment(fd).format("YYYYMMDD") < moment(rows[0].noti_taxdt).format("YYYYMMDD")) {
                                error+= ' Ngày hiệu lực phải >=ngày CQT chấp nhận tờ khai đăng ký '+'( Right validity date> = Date of CQT accepts the registration declaration )'
                                throw new Error(error)
                         }
                        }
																																				    
                        sql = `select max(priority) "max" from s_serial where taxc=? and form=? and serial=?`
                        result = await inc.execsqlselect(sql, [taxc,form,seri])
                        let   rowss = result
                        if (rowss.length > 0 && rowss[0].max!=null) {
                          
                            if (priority != (rowss[0].max + 1)) {
                                   error+= ' Độ ưu tiên phải liên tiếp với các dải đã khai, Độ ưu tiên mới phải là '+(rowss[0].max + 1)+'.(Priority must be consecutive with declared ranges, New precedence must be '+ (rowss[0].max + 1)+'.)'
                                   throw new Error(error)
                            }
                            
                        }
                        let statement = await inc.getstatementinfo(taxc),_serial1char = seri[0]
                        if (statement.hascode == 1) {
                            if (_serial1char != "C") {
                                error+= `MST ${taxc} có mã ký tự đầu tiên phải là C. ( MST ${taxc} Having the first character code must be c ) `
                                throw new Error(error)                            
                            }
                            if(sendtype == 'listinv'){
                                error+= `MST ${taxc} Có mã chỉ được chọn phương thức 'Chuyển đầy đủ nội dung từng hóa đơn'. ( MST ${taxc} has a code that only selects the method 'fully converts each invoice' ) `
                                throw new Error(error)   
                            }

                        } else if (statement.hascode == 0)  {
                            if (_serial1char != "K"){
                                error+= `MST ${taxc} không mã ký tự đầu tiên phải là K. ( MST ${taxc} Not the first character code must be k ) `
                                throw new Error(error)                            
                            }
                        }   
                                             //end check
                    }
                    if (util.isEmpty(error)){
                        for (const bind of binds) {
                            result = await inc.execsqlinsupddel(`insert into s_serial (type,form,serial,min,max,cur,fd,uc,uses,taxc,des,priority,sendtype,invtype,degree_config) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`, bind)
                        }
                        for (const bind of binds) {
                            let result_id = await inc.execsqlselect(`select *  from s_serial where taxc=? and form=? and serial=?`, [bind[9], bind[1], bind[2]])
                            let id, rows = result_id
                            if (rows.length > 0) id = rows[0].id
                            const sSysLogs = { fnc_id: 'ser_ins', src: config.SRC_LOGGING_DEFAULT, dtl: `Tạo đăng ký sử dụng: ${id}`, msg_id: id, doc: JSON.stringify(body) };
                            logging.ins(req, sSysLogs, next)
                        }      
                    }else{
                        throw new Error(error)
                       
                    }
                  
                }
                else if (operation == "delete") {
                    sql = `delete from s_serial where id=? and status=3 `
                    binds = [body.id]
                    result = await inc.execsqlinsupddel(sql, binds)
                    const sSysLogs = { fnc_id: 'ser_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa đăng ký sử dụng: ${body.id}`, msg_id: body.id, doc: JSON.stringify(body)};
                    logging.ins(req,sSysLogs,next)
                }
            }
            res.json({ arr: 1})
        }
        catch (err) {
            next(err)
        }
    },
    syncs: (taxc) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result, rows, binds,str = "",where=""
                let arr = taxc.split(",")
                for (const row of arr) {
                    str += `'${row}',`
                }
                where += `  taxc in (${str.slice(0, -1)})`
                result = await inc.execsqlselect(`select id "id",form "form",serial "serial",cur "cur",min "min",max "max",taxc "taxc" from s_serial where ${where} and status=1`, [])
                rows = result
                if (rows.length > 0) {
                    binds = []
                    for (const row of rows) {
                        const key = `SERIAL.${row.taxc}.${row.form}.${row.serial}`
                        const cur = await redis.get(key)
                        if (cur && cur > row.cur && cur >= row.min && cur<=row.max) {
                            result = await inc.execsqlinsupddel(`update s_serial set cur=? where id=? and status=1`, [cur, row.id])
                        }
                    }
                }
                result = await inc.execsqlselect(`select id "id",form "form",serial "serial",taxc "taxc" from s_serial where ${where} and status=1 and cur=max`, [])
                rows = result
                if (rows.length > 0) {
                    const dt = new Date()
                    binds = []
                    for (const row of rows) {
                        const key = `SERIAL.${row.taxc}.${row.form}.${row.serial}`, kax = `${key}.max`,kreq=`${key}.req`
                        let val = await redis.get(kax)
                        if (val == row.max)
                        {
                            await redis.del([key, kax,kreq])
                            binds.push([dt, row.id])
                        }
                    }
                    if (binds.length > 0) {
                        result = await inc.execsqlinsupddel(`update s_serial set status=4,td=? where id=? and status=1`, binds)
                    }
                }

                resolve()
            }
            catch (err) {
                reject(err)
            }
        })
    },
    sync: async (req, res, next) => {
        try {
            const taxc = req.params.taxc
            await Service.syncs(taxc)
            res.json("Đã đồng bộ số hiện tại /n (Current number synchronized)")
        }
        catch (err) {
            next(err)
        }
    },
    apiseq: async (req, res, next) => {
        try {
            let json = req.body, idt = (moment(json.idt, 'YYYY-MM-DD')).startOf("day").format("YYYY-MM-DD HH:mm:ss"), taxc = json.taxc, form = json.form, serial = json.serial
            logger4app.debug(`apiseq : idt - ${idt}, taxc - ${taxc}, form - ${form}, serial - ${serial}`)
            const seq = await Service.sequence(taxc, form, serial, idt)
            res.json({ result: seq})
        }
        catch (err) {
            logger4app.debug(`apiseq error : `,err)
            next(err)
        }
    },
    syncredisdb: async (req, res, next) => {
        try {
            let id = req.params.id, result, rows, row,sql,error=""
            //result = await dbs.query(`delete from s_serial where priority>1 `, [])
            result = await inc.execsqlselect(`select taxc "taxc",type "type",form "form",serial "serial",min "min",max "max",cur "cur",priority "priority",fd "fd",status "status" from s_serial where status=1 ${(!id) ? 'and uses = 2' : ''} order by taxc,form,serial desc`, [])
            rows = result
            if (rows.length == 0) logger4app.debug( result.toString('khong tin thay dai so trong DB'));
         
            for (let row of rows) {
                let uk = `${row.taxc}.${row.form}.${row.serial}`, key = `SERIAL.${uk}`, kax = `${key}.max`,kreq=`${key}.req`,ker = `${key}.err`
                let cur = row.cur, min = row.min, max = row.max, val = min-1 , condition = `${row.min}.${row.max}.${moment(row.fd).format('YYYYMMDD')}.${row.priority}__`
               if(row.status == 1){
                //await redis.del([key, kax, ker,kreq])
                   //Sửa lại hàm set multi thành nhiều hàm set do DB không có hàm này (áp dụng cho SCB)
                   
                  let curred = await redis.get(key)
                  if(!curred || cur > curred){
                        if (dbCache) {
                            redis.set(key, cur)
                            redis.set(kax, max)
                            redis.set(kreq, condition)
                        } else {
                            redis.multi().set(key, cur).set(kax, max).set(kreq, condition).exec(async (err, results) => {
                                if (err) throw new Error(err)
                                logger4app.debug('Dong bo dai: ' + key)
    
                            })
                        }
                  }else{
                      if(curred > cur) await inc.execsqlinsupddel(`update s_serial set cur = ? where id = ? and status = 1 and min <= ? and max >= ?`, [curred, row.id, curred, curred])
                      if(curred == max) await inc.execsqlinsupddel(`update s_serial set cur = ?,status = 4 where id = ? and status = 1 and min <= ? and max >= ?`, [curred, row.id, curred, curred])
                    
                 }
                   
               }
               error = error+ ' Dong bo dai: ' + key
            }
            res.json({ result: "1"})
        }
        catch (err) {
            res.json({ result: "0"})
        }
    },
    syncredisdbsgr: async (taxc, form, serial) => {
        try {
            let result, rows
            //result = await dbs.query(`delete from s_serial where priority>1 `, [])
 
            result = await inc.execsqlselect(`select max(CAST(seq AS int)) "seq" from s_inv where stax = ? and form = ? and serial = ?`, [taxc, form, serial])
            rows = result
            let vseq = rows[0].seq
            await inc.execsqlinsupddel(`update s_serial set cur = ? where taxc = ? and form = ? and serial = ?`, [vseq, taxc, form, serial])
            
            const uk = `${taxc}.${form}.${serial}`, key = `SERIAL.${uk}`
            await redis.set(key, vseq)
        }
        catch (err) {
            throw err
        }
    },
    docx: async (req, res, next) => {
        try {
            const token = sec.decode(req)
            const type = req.params.type, taxc = req.params.taxc, org = await ous.obt(taxc)
            const fn = path.join(__dirname, "..", "..", "..", `temp/${type}.docx`), file = fs.readFileSync(fn)
            const time = moment().format(config.mfd).split("/"), srdt = `ngày ${time[0]} tháng ${time[1]} năm ${time[2]}`
            let rs, rows, arr = [], i = 1, data
            for (let i in org) if (!org[i]) org[i] = ""
            if (type == "sqdsd") {
                rs = await inc.execsqlselect(`select type "type",form "form",serial "serial" from s_serial where taxc=? and status=?`, [taxc])
                rows = rs
                for (const row of rows) {
                    arr.push({ sindex: i++, stype: util.tenhd(row.type), sform: row.form, sserial: row.serial })
                }
                const staxo = await taxon(org.taxo)
                data = { stax: taxc, sname: org.name.toUpperCase(), saddress: org.addr, stel: org.tel, staxo: staxo, srdt: srdt, arrSer: arr }
            }
            else if (type == "stbph") {
                rs = await inc.execsqlselect(`select type "type",form "form",serial "serial",min "min",max "max",fd "fd" from s_serial where taxc=? and status=?`, [taxc, 3])
                rows = rs
                for (const row of rows) {
                    arr.push({ id: i++, serName: util.tenhd(row.type), serForm: row.form, serSerial: row.serial, serSum: row.max, fromNum: row.min.toString().padStart(config.SEQ_LEN, '0'), toNum: row.max.toString().padStart(config.SEQ_LEN, '0'), fd: moment(row.fd).format(config.mfd) })
                }
                const staxo = await taxon(org.taxo)
                data = { stax: taxc, sname: org.name, saddress: org.addr, stel: org.tel, staxo: staxo, srdt: srdt, arrSerial: arr }
            }
            else if (type == "sdktd") {
                // rs = await dbs.query("select * from s_ca where taxc=?", [token.taxc])
                // rows = rs.rows
                // for (const row of rows) {
                //     //sIssuer: (row.issuer).split('=')[1].split(',')[0],
                //     arr.push({ id: i++, sSerial: row.serial, sSubject: row.subject, sIssuer: row.issuer, fd: moment(row.fd).format(config.mfd), td: moment(row.td).format(config.mfd) })
                // }
                arr.push({id:"", sIssuer:"", sSerial:"", fd:"", td:""})
                data = { stax: token.taxc, sname: token.on, saddress: org.addr, stel: org.tel, smail: org.mail, srdt: srdt, ser: token.fn, arrCa: arr }
            }
            const zip = new jszip(file)
            const doc = new docxt()
            doc.loadZip(zip)
            doc.setData(data)
            doc.render()
            const out = doc.getZip().generate({ type: "nodebuffer" })
            res.end(out, "binary")
        } catch (err) {
            next(err)
        }
    },
    xmlall: async (req, res, next) => {
        try {
            const token = sec.decode(req)
            const fullname = token.fn
            logger4app.debug(req.params.taxc)
            let str = "",taxc=req.params.taxc, arr = taxc.split(","),where=""
            for (const row of arr) {
                str += `'${row}',`
            }
            where = `in (${str.slice(0, -1)})`
            let rs, rows ,rs2, rows2
            var Parser = require("fast-xml-parser").j2xParser;
            if(`${config.dbtype}` == "mssql") {
                rs = await inc.execsqlselect(`select rownum as STT,A.ID,A.TAXC,A.TYPE,A.FORM,A.SERIAL,A.MIN,A.MAX,A.CUR,A.STATUS,to_char(A.FD,'yyyy-mm-dd') as FD,A.TD,A.DT,A.UC,A.USES,B.TAXO,B.TEL,B.MAIL,B.NAME,
                (select name from  S_loc where id =B.PROV and rownum=1) as PROV,
                (select name from  S_loc where id =B.DIST and rownum=1) as DIST,
                (select name from  S_loc where id =B.WARD and rownum=1) as WARD,
                B.ADDR,B.FADD,B.NAME,B.PROV as PROV_ID,B.DIST as DIST_ID,B.WARD as WARD_ID,
                (select NAME from s_ou where ID = (select PID from s_ou where Taxc=B.TAXC and rownum=1) and rownum=1) as NAMECHUQUAN,
                (select TAXC from s_ou where ID = (select PID from s_ou where Taxc=B.TAXC and rownum=1) and rownum=1) as TAXCCHUQUAN,
                (select NAME from S_TAXO where ID = B.TAXO ) as COQUANTIEPNHAN
                from s_serial A 
                inner join s_ou B on (A.TAXC=B.TAXC) 
                left join s_taxo C on (B.taxo=C.ID) where A.status=3 and A.TAXC ${where}`, []) 
            }
            if(`${config.dbtype}` == "orcl") {
                rs = await dbs.query(`select rownum as STT,A.ID,A.TAXC,A.TYPE,A.FORM,A.SERIAL,A.MIN,A.MAX,A.CUR,A.STATUS,to_char(A.FD,'yyyy-mm-dd') as FD,A.TD,A.DT,A.UC,A.USES,B.TAXO,B.TEL,B.MAIL,B.NAME,
                (select name from  S_loc where id =B.PROV and rownum=1) as PROV,
                (select name from  S_loc where id =B.DIST and rownum=1) as DIST,
                (select name from  S_loc where id =B.WARD and rownum=1) as WARD,
                B.ADDR,B.FADD,B.NAME,B.PROV as PROV_ID,B.DIST as DIST_ID,B.WARD as WARD_ID,
                (select NAME from s_ou where ID = (select PID from s_ou where Taxc=B.TAXC and rownum=1) and rownum=1) as NAMECHUQUAN,
                (select TAXC from s_ou where ID = (select PID from s_ou where Taxc=B.TAXC and rownum=1) and rownum=1) as TAXCCHUQUAN,
                (select NAME from S_TAXO where ID = B.TAXO ) as COQUANTIEPNHAN
                from s_serial A 
                inner join s_ou B on (A.TAXC=B.TAXC) 
                left join s_taxo C on (B.taxo=C.ID) where A.status=3 and A.TAXC = ?`, [req.params.taxc])
            }
            if(`${config.dbtype}` == "mysql") {
                rs = await dbs.query(`select  @rownum := @rownum + 1 AS STT,A.ID,A.TAXC,A.TYPE,A.FORM,A.SERIAL,A.MIN,A.MAX,A.CUR,A.STATUS,date_format(A.FD,'%Y%m%d') as FD,A.TD,A.DT,A.UC,A.USES,B.TAXO,B.TEL,B.MAIL,B.NAME,
                (select name from  s_loc where id =B.PROV limit 1) as PROV,
                (select name from  s_loc where id =B.DIST limit 1) as DIST,
                (select name from  s_loc where id =B.WARD limit 1) as WARD,
                B.ADDR,B.FADD,B.NAME,B.PROV as PROV_ID,B.DIST as DIST_ID,B.WARD as WARD_ID,
                (select NAME from s_ou where ID = (select PID from s_ou where Taxc=B.TAXC  limit 1)  limit 1) as NAMECHUQUAN,
                (select TAXC from s_ou where ID = (select PID from s_ou where Taxc=B.TAXC  limit 1)  limit 1) as TAXCCHUQUAN,
                (select NAME from s_taxo where ID = B.TAXO ) as COQUANTIEPNHAN
                from s_serial A 
                inner join s_ou B on (A.TAXC=B.TAXC) 
                left join s_taxo C on (B.taxo=C.ID)
                ,(SELECT @rownum := 0) r
                where A.status=3 and A.TAXC = ?`, [req.params.taxc])
            }
            if(`${config.dbtype}` == "pgsql") {
                rs = await dbs.query(`select row_number() over() as STT,A.ID,A.TAXC,A.TYPE,A.FORM,A.SERIAL,A.MIN,A.MAX,A.CUR,A.STATUS,to_char(A.FD,'yyyy-mm-dd') as FD,A.TD,A.DT,A.UC,A.USES,B.TAXO,B.TEL,B.MAIL,B.NAME,
                (select name from  S_loc where id =B.PROV limit 1) as PROV,
                (select name from  S_loc where id =B.DIST limit 1) as DIST,
                (select name from  S_loc where id =B.WARD limit 1) as WARD,
                B.ADDR,B.FADD,B.NAME,B.PROV as PROV_ID,B.DIST as DIST_ID,B.WARD as WARD_ID,
                (select NAME from s_ou where ID = (select PID from s_ou where Taxc=B.TAXC limit 1) limit 1) as NAMECHUQUAN,
                (select TAXC from s_ou where ID = (select PID from s_ou where Taxc=B.TAXC limit 1) limit 1) as TAXCCHUQUAN,
                (select NAME from S_TAXO where ID = B.TAXO ) as COQUANTIEPNHAN
                from s_serial A 
                inner join s_ou B on (A.TAXC=B.TAXC) 
                left join s_taxo C on (B.taxo=C.ID) where A.status=3 and A.TAXC = ?`, [req.params.taxc])
            }
            rows = rs
            var jsonchitiet =[]
            rows.forEach(row => {
                var chitiet = {
                        "tenLoaiHDon": config.ITYPE.find(item => item.id === row.TYPE).value,
                        "mauSo": row.FORM === null?"":row.FORM,
                        "kyHieu": row.SERIAL === null?"":row.SERIAL,
                        "soLuong": Number(row.MAX)-Number(row.MIN)+1,
                        "tuSo": row.MIN,
                        "denSo": row.MAX,
                        "ngayBDauSDung": row.FD === null?"":row.FD,
                        "DoanhNghiepIn": {
                            "ten":"",
                            "mst": ""
                        },
                        "HopDongDatIn": {
                            "so":"",
                            "ngay": ""
                        }
                }
                jsonchitiet.push(chitiet)
            });
            var json = {}
            if(rows.length==0){
                rs2 = await inc.execsqlselect(`select rownum as STT,B.TAXC,B.TAXO,B.TEL,B.MAIL,B.NAME,
                (select name from  S_loc where id =B.PROV and rownum=1) as PROV,
                (select name from  S_loc where id =B.DIST and rownum=1) as DIST,
                (select name from  S_loc where id =B.WARD and rownum=1) as WARD,
                B.ADDR,B.FADD,B.NAME,B.PROV as PROV_ID,B.DIST as DIST_ID,B.WARD as WARD_ID,
                (select NAME from s_ou where ID = (select PID from s_ou where Taxc=B.TAXC and rownum=1) and rownum=1) as NAMECHUQUAN,
                (select TAXC from s_ou where ID = (select PID from s_ou where Taxc=B.TAXC and rownum=1) and rownum=1) as TAXCCHUQUAN,
                (select NAME from S_TAXO where ID = B.TAXO ) as COQUANTIEPNHAN
                from s_ou B  
                left join s_taxo C on (B.taxo=C.ID) where B.TAXC ${where}`, [])
                rows2 = rs2
                json = {
                    "HSoThueDTu": {
                        "HSoKhaiThue": {
                            "TTinChung": {
                                "TTinDVu": {
                                "maDVu": "ETAX",
                                "tenDVu": "ETAX 1.0",
                                "pbanDVu": "1.0",
                                "ttinNhaCCapDVu": "ETAX_TCT"
                                },
                                "TTinTKhaiThue": {
                                "TKhaiThue": {
                                "maTKhai": "106",
                                "tenTKhai": "Thông báo phát hành hóa đơn",
                                "moTaBMau":"",
                                "pbanTKhaiXML": "2.1.2",
                                "loaiTKhai": "C",
                                "soLan": "0",
                                "KyKKhaiThue": {
                                    "kieuKy": "D",
                                    "kyKKhai": moment().format('DD/MM/YYYY'),
                                    "kyKKhaiTuNgay": moment().format('DD/MM/YYYY'),
                                    "kyKKhaiDenNgay": moment().format('DD/MM/YYYY'),
                                    "kyKKhaiTuThang": "",
                                    "kyKKhaiDenThang": ""
                                },
                                "maCQTNoiNop": rows2[0].TAXO === null?"":rows2[0].TAXO,
                                "tenCQTNoiNop": rows2[0].NAME === null?"":rows2[0].NAME,
                                "ngayLapTKhai": moment().format('DD/MM/YYYY'),
                                "nguoiKy": fullname === null?"":fullname,
                                "ngayKy": moment().format('DD/MM/YYYY'),
                                "nganhNgheKD":""
                                },
                                "NNT": {
                                "mst": rows2[0].TAXC === null?"":rows2[0].TAXC,
                                "tenNNT": rows2[0].NAME === null?"":rows2[0].NAME,
                                "dchiNNT": rows2[0].ADDR === null?"":rows2[0].ADDR,
                                "phuongXa": rows2[0].WARD === null?"":rows2[0].WARD,
                                "maHuyenNNT":rows2[0].DIST_ID === null?"":rows2[0].DIST_ID,
                                "tenHuyenNNT": rows2[0].DIST === null?"":rows2[0].DIST,
                                "maTinhNNT":rows2[0].PROV_ID === null?"":rows2[0].PROV_ID,
                                "tenTinhNNT": rows2[0].PROV === null?"":rows2[0].PROV,
                                "dthoaiNNT": rows2[0].TEL === null?"":rows2[0].TEL,
                                "faxNNT": "",
                                "emailNNT": rows2[0].MAIL === null?"":rows2[0].MAIL
                                }
                                }
                            },
                            "CTieuTKhaiChinh": {
                                "HoaDon": {
                                    "ChiTiet":{}
                                },
                                "DonViChuQuan": {
                                    "ten": rows2[0].NAMECHUQUAN === null?"":rows2[0].NAMECHUQUAN,
                                    "mst": rows2[0].TAXCCHUQUAN === null?"":rows2[0].TAXCCHUQUAN
                                },
                                "tenCQTTiepNhan": rows2[0].COQUANTIEPNHAN === null?"":rows2[0].COQUANTIEPNHAN,
                                "nguoiDaiDien": fullname === null?"":fullname,
                                "ngayBCao": moment().format('DD/MM/YYYY')
                            }
                        },
                        "CKyDTu":""
                    }
                }
            }else{
                json = {
                    "HSoThueDTu": {
                        "HSoKhaiThue": {
                            "TTinChung": {
                                "TTinDVu": {
                                "maDVu": "ETAX",
                                "tenDVu": "ETAX 1.0",
                                "pbanDVu": "1.0",
                                "ttinNhaCCapDVu": "ETAX_TCT"
                                },
                                "TTinTKhaiThue": {
                                "TKhaiThue": {
                                "maTKhai": "106",
                                "tenTKhai": "Thông báo phát hành hóa đơn",
                                "moTaBMau":"",
                                "pbanTKhaiXML": "2.1.2",
                                "loaiTKhai": "C",
                                "soLan": "0",
                                "KyKKhaiThue": {
                                    "kieuKy": "D",
                                    "kyKKhai": moment().format('DD/MM/YYYY'),
                                    "kyKKhaiTuNgay": moment().format('DD/MM/YYYY'),
                                    "kyKKhaiDenNgay": moment().format('DD/MM/YYYY'),
                                    "kyKKhaiTuThang": "",
                                    "kyKKhaiDenThang": ""
                                },
                                "maCQTNoiNop": rows[0].TAXO === null?"":rows[0].TAXO,
                                "tenCQTNoiNop": rows[0].NAME === null?"":rows[0].NAME,
                                "ngayLapTKhai": moment().format('DD/MM/YYYY'),
                                "nguoiKy": fullname === null?"":fullname,
                                "ngayKy": moment().format('DD/MM/YYYY'),
                                "nganhNgheKD":""
                                },
                                "NNT": {
                                "mst": rows[0].TAXC === null?"":rows[0].TAXC,
                                "tenNNT": rows[0].NAME === null?"":rows[0].NAME,
                                "dchiNNT": rows[0].ADDR === null?"":rows[0].ADDR,
                                "phuongXa": rows[0].WARD === null?"":rows[0].WARD,
                                "maHuyenNNT":rows[0].DIST_ID === null?"":rows[0].DIST_ID,
                                "tenHuyenNNT": rows[0].DIST === null?"":rows[0].DIST,
                                "maTinhNNT":rows[0].PROV_ID === null?"":rows[0].PROV_ID,
                                "tenTinhNNT": rows[0].PROV === null?"":rows[0].PROV,
                                "dthoaiNNT": rows[0].TEL === null?"":rows[0].TEL,
                                "faxNNT": "",
                                "emailNNT": rows[0].MAIL === null?"":rows[0].MAIL
                                }
                                }
                            },
                            "CTieuTKhaiChinh": {
                                "HoaDon": {
                                    ChiTiet:jsonchitiet
                                },
                                "DonViChuQuan": {
                                    "ten": rows[0].NAMECHUQUAN === null?"":rows[0].NAMECHUQUAN,
                                    "mst": rows[0].TAXCCHUQUAN === null?"":rows[0].TAXCCHUQUAN
                                },
                                "tenCQTTiepNhan": rows[0].COQUANTIEPNHAN === null?"":rows[0].COQUANTIEPNHAN,
                                "nguoiDaiDien": fullname === null?"":fullname,
                                "ngayBCao": moment().format('DD/MM/YYYY')
                            }
                        },
                        "CKyDTu":""
                    }
                }
            }
            var parser = new Parser();
            
            var xml = parser.parse(json);
            xml = xml.replace(`<HSoThueDTu>`, `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>\n<HSoThueDTu xmlns="http://kekhaithue.gdt.gov.vn/TKhaiThue" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">`)
            xml= xml.replace(`<mst/>`,`<mst xsi:nil="true"/>`)
            xml= xml.replace(`<ngay/>`,`<ngay xsi:nil="true"/>`)
            rows.forEach(row => {
                var bienid=`<ChiTiet ID="${row.STT}">`
                xml= xml.replace(`<ChiTiet>`,bienid)
            });
            res.end(xml)
        } catch (err) {
            next(err)
        }
    },
    getAllTypeByTax: async (req, res, next) => {
        try {
            const token = sec.decode(req)
            const result = await inc.execsqlselect(`select distinct type as "id", type as "value" from s_serial where taxc=? and degree_config = '123'`, [token.taxc])
            if(result.length > 0) {
                for(let row of result) {
                    for(let i of config.ITYPE){
                        if(row.id == i.id){
                            row.value = i.value
                        }
                    }
                }
            }
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    getSendType: async (req, res, next) => {
        try {
            const token = sec.decode(req)
            let query = req.query, now = new Date()
            const result = await inc.execsqlselect(`select sendtype "sendtype" from s_serial where taxc=? and type=? and serial=? and fd<=?`, [token.taxc, query.type, query.serial, now])
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    } 
}
module.exports = Service   