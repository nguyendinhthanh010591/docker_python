"use strict"
const config = require("../config")
const logger4app = require("../logger4app")
const util = require("../util")
const inc = require("./inc")
const logging = require("../logging")
const { bind } = require("file-loader")

const Service= {
    get: async(req,res,next)=>{
        try{
            let binds=[], where="where 1=1", order
            const query= req.query, sort=query.sort
            if(query.filter){
                let filter = JSON.parse(query.filter), val, i = 1
                Object.keys(filter).forEach(key => {
                    val = filter[key]
                    if(val){
                        where += ` and upper(${key}) like ?`
                        binds.push(`%${val.toUpperCase()}%`)
                    }
                }
                )
            }
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = ` order by ${key} ${sort[key]}`
                })
            } else order = ` order by deptid`
            const sql=`select deptid "deptid",deptname "deptname" from s_dept ${where} ${order}`
            const result= await inc.execsqlselect(sql,binds)
            res.json(result)
        }
        catch(err){
            next(err)
        }
    },
    post: async(req,res,next)=>{
        try{
            let body=req.body,binds, result, operation = body.webix_operation,sql
            switch(operation){
                case "insert":
                    sql="insert into s_dept (deptname,deptid) values (?,?)"
                    binds=[body.deptname,body.deptid]
                    break
                case "delete":
                    sql="delete from s_dept where deptid=?"
                    binds=[body.deptid]
                    break
                case "update":
                    sql="update s_dept set deptname=? where deptid=?"
                    binds=[body.deptname,body.deptid]
                    break
                default:
                    throw new Error(`${operation} là không hợp lệ \n (${operation} is invalid)`)
            }
            result = await inc.execsqlinsupddel(sql, binds)
            if (operation == "insert") res.json(result)
            else res.json(result)
        }
        catch(err){
            next(err)
        }
    },
    roledep: async (req, res, next) => {
        try {
          const uid = req.params.uid
          const sql=`select id "id",
          mail "mail",
          ou "ou",
          uc "uc",
          name "name",
          pos "pos",
          code "code"
          from s_user  order by id`
          const result = await inc.execsqlselect(sql, [])
          res.json(result)
        }
        catch (err) {
          next(err)
        }
      },
      gir: async (req, res, next) => {
        try {
        let rid = req.params.rid
          const sql = `select
          x.deptid "id",
          x.deptname "name",
          x.sel "sel"
      from
          (
          select
              a.deptid,
              a.deptname,
              0 sel
          from
              s_dept a
          left join s_deptusr b on
              a.deptid = b.deptid
              and b.usrid = ?
          where
              b.usrid is null
      union
          select
              a.deptid,
              a.deptname,
              1 sel
          from
              s_dept a
          inner join s_deptusr b on
              a.deptid = b.deptid
              and b.usrid = ? ) x
      order by
          x.deptid`
          const result = await inc.execsqlselect(sql, [rid, rid])
          res.json(result)
        } catch (err) {
          next(err)
        }
      },
    mbr: async (req, res, next) => {
    try {
      const body = req.body, usid = body.usid, arr = body.arr
      let sql, result
      sql = `	delete from s_deptusr where usrid=?`
      result = await inc.execsqlinsupddel(sql, [usid])
      if (arr.length > 0) {
        let binds = []
        for (const uid of arr) {
          binds.push([usid, uid])
        }
        sql = `insert into s_deptusr (usrid, deptid) values (?,?)`
        result = await inc.execsqlinsupddel(sql, binds)
      }
      const sSysLogs = { fnc_id: 'user_dep_mbr', src: config.SRC_LOGGING_DEFAULT, dtl: 'Phân quyền người dùng, phòng ban', msg_id: "", doc: JSON.stringify(body) };
      logging.ins(req, sSysLogs, next)
      res.json(1)
    } catch (err) {

      next(err)
    }

  }
}


module.exports=Service