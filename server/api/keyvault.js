"use strict"
const config = require("./config")
const logger4app = require("./logger4app")
const client_id = config.keyvault_config ? config.keyvault_config.client_id : ""
const client_key = config.keyvault_config ? config.keyvault_config.client_key : ""
const tenant_id = config.keyvault_config ? config.keyvault_config.tenant_id : ""

const { ClientSecretCredential  } = require("@azure/identity")
const { SecretClient } = require("@azure/keyvault-secrets")

const credential = new ClientSecretCredential(tenant_id, client_id, client_key)

const url = config.keyvault_config ? config.keyvault_config.uri : ""
 
const client = new SecretClient(url, credential);
 
const Service = {
    getSecretByKey: async (KeyIn) => {
        try {
            
            let ret = await client.getSecret(KeyIn)
            return ret.value
        }
        catch (err) {
            throw err
        }
    },
    setSecretByKey: async (KeyIn, Value)  => {
        try {
            let secretName = KeyIn, value = Value, optionsopt = {}
            client.setSecret(url, secretName, value, optionsopt).then((results) => {
                logger4app.debug(results);
            })
        }
        catch (err) {
            throw err
        }
    }
}
module.exports = Service            