"use strict"
const moment = require("moment")
const config = require("./config")
const dbtype = config.dbtype
const dbs = require(`./${dbtype}/dbs`)
const sign = require("./sign")
const util = require("./util")
const redis = require("./redis")
const fcloud = require("./fcloud")
const inc = require("./inc")
const logging = require("./logging")
const ous = require(`./${dbtype}/ous`)
const ext = require("./ext")
const dtf = config.dtf
const sec = require("./sec")
const tvan = require("./tvan")
const xlsxtemp = require("xlsx-template")
const path = require("path")
const fs = require("fs")
const hsm = require('./hsm')
const uuid = require('uuid');
const opt = { autoCommit: false, batchErrors: true }
const TYPE_TH ="01/TH-HĐĐT"

const key = "LISTINV.ID"

const sql_search = {
    sql_search_mssql: `select CASE WHEN dt.status = 4 THEN 4 WHEN dt.status = 3 and dt.adjtyp is null THEN 1 WHEN dt.status = 3 and dt.adjtyp =1 THEN 3 WHEN dt.status = 3 and dt.adjtyp =2 THEN 2 END kindinv, JSON_QUERY([doc],'$.items') items, dt.sname, JSON_VALUE([doc],'$.type_ref') typeref0,dt.stax,dt.saddr,dt.smail,dt.sacc,dt.buyer,dt.bname,dt.bcode,dt.btax,dt.baddr,dt.bmail,dt.btel,dt.bacc,dt.note,dt.sum,dt.sumv,dt.vat,dt.vatv,dt.total,dt.totalv,dt.sec,JSON_VALUE([doc],'$.adj.typ') adjtyp,JSON_VALUE([doc],'$.adj.ref') adjref,JSON_VALUE([doc],'$.adj.rea') adjrea,JSON_VALUE([doc],'$.adj.des') adjdes,JSON_VALUE([doc],'$.cancel.typ') cantyp, JSON_VALUE([doc],'$.adj.form') adjform, JSON_VALUE([doc],'$.adj.serial') adjserial, JSON_VALUE([doc],'$.adj.seq') adjseq, JSON_VALUE([doc],'$.adj.periodtype') periodtype, JSON_VALUE([doc],'$.adj.period') period, dt.id, FORMAT(dt.idt,'dd/MM/yyyy') idt, JSON_VALUE(JSON_QUERY (doc, '$.tax[0]'),'$.vrn') vrn, dt.type, dt.form, dt.serial, dt.seq, dt.status, dt.buyer, lt.sendtype from s_inv as dt left join s_serial as lt on dt.form = lt.form and dt.serial=lt.serial and dt.stax = lt.taxc `,
    sql_search_mysql: `select CASE
        WHEN dt.status = 4 THEN 4
        WHEN dt.status = 3 and dt.adjtyp is null THEN 1
        WHEN dt.status = 3 and dt.adjtyp =1 THEN 3
        WHEN dt.status = 3 and dt.adjtyp =2 THEN 2
        ELSE ""
        END kindinv,        
        dt.doc->>'$.items' items, dt.sname,dt.doc->>'$.type_ref' typeref0,doc->>'$.tax[0].vrn' vrn,dt.stax,dt.saddr,dt.smail,dt.sacc,dt.buyer,dt.bname,dt.bcode,dt.btax,dt.baddr,dt.bmail,dt.btel,dt.bacc,dt.note,dt.sum,dt.sumv,dt.vat,dt.vatv,dt.total,dt.totalv,dt.sec,dt.adjtyp,dt.adjref,dt.adjrea,dt.adjdes,dt.cantyp,dt.doc->>'$.adj.form' adjform,dt.doc->>'$.adj.serial' adjserial,dt.doc->>'$.adj.seq' adjseq,dt.doc->>'$.adj.periodtype' periodtype,dt.doc->>'$.adj.period' period, dt.id, DATE_FORMAT(dt.idt,'%d/%m/%Y') idt, dt.type, dt.form, dt.serial, dt.seq, dt.status, dt.buyer, lt.sendtype from s_inv as dt left join s_serial as lt on dt.form = lt.form and dt.serial=lt.serial and dt.stax = lt.taxc `,
    sql_search_orcl: `select CASE WHEN dt.status = 4 THEN 4 WHEN dt.status = 3 and dt.adjtyp is null THEN 1 WHEN dt.status = 3 and dt.adjtyp =1 THEN 3 WHEN dt.status = 3 and dt.adjtyp =2 THEN 2 ELSE "" END "kindinv", JSON_QUERY(dt.doc,'$.items') "items", dt.sname, JSON_VALUE(dt.doc,'$.type_ref') "typeref0",dt.stax "stax",dt.saddr "saddr",dt.smail "smail",dt.sacc "sacc",dt.buyer "buyer",dt.bcode "bcode",dt.baddr "baddr",dt.bmail "bamil",dt.btel "btel",dt.bacc "bacc",dt.note "note",dt.sumv "sumv",dt.vat "vat",dt.vatv "vatv",dt.totalv "totalv",dt.sec "sec",JSON_VALUE(doc,'$.adj.typ') "adjtyp",JSON_VALUE(doc,'$.adj.ref') "adjref",JSON_VALUE(doc,'$.adj.rea') "adjrea",JSON_VALUE(doc,'$.adj.des') "adjdes",JSON_VALUE(doc,'$.cancel.typ') "cantyp", JSON_VALUE(dt.doc,'$.adj.form') "adjform", JSON_VALUE(dt.doc,'$.adj.serial') "adjserial", JSON_VALUE(dt.doc,'$.adj.seq') "adjseq", JSON_VALUE(dt.doc,'$.adj.periodtype') "periodtype", JSON_VALUE(dt.doc,'$.adj.period') "period", JSON_VALUE(JSON_QUERY (doc, '$.tax[0]'),'$.vrn') "vrn", dt.id "id", to_char(dt.idt,'dd/MM/yyyy') "idt", dt.type "type", dt.form "form", dt.serial "serial", dt.seq "seq", dt.status "status", dt.buyer "buyer", lt.sendtype "sendtype" from s_inv as dt left join s_serial as lt on dt.form = lt.form and dt.serial=lt.serial and dt.stax = lt.taxc `,
    sql_search_pgsql: `select CASE WHEN dt.status = 4 THEN 4 WHEN dt.status = 3 and dt.adjtyp is null THEN 1 WHEN dt.status = 3 and dt.adjtyp =1 THEN 3 WHEN dt.status = 3 and dt.adjtyp =2 THEN 2 ELSE "" END kindinv, dt.doc->'items' items, dt.sname,dt.doc->'type_ref' typeref0,dt.doc->tax->0->'vrn' vrn,dt.stax,dt.saddr,dt.smail,dt.sacc,dt.buyer,dt.bname,dt.bcode,dt.btax,dt.baddr,dt.bmail,dt.btel,dt.bacc,dt.note,dt.sum,dt.sumv,dt.vat,dt.vatv,dt.total,dt.totalv,dt.sec,dt.adjtyp,dt.adjref,dt.adjrea,dt.adjdes,dt.cantyp,dt.doc->'adj'->'form' adjform,dt.doc->'adj'->'serial' adjserial,dt.doc->'adj'->'seq' adjseq,dt.doc->'adj'->'periodtype' periodtype,dt.doc->'adj'->'period' period, dt.id,dt.id, DATE_FORMAT(dt.idt,'%d/%m/%Y') idt, dt.type, dt.form, dt.serial, dt.seq, dt.status, dt.sum, dt.total, dt.bname, dt.buyer, dt.btax, lt.sendtype from s_inv as dt left join s_serial as lt on dt.form = lt.form and dt.serial=lt.serial and dt.stax = lt.taxc `
}

const sql_searchBth = {
    sql_searchBth_mssql: `select id as idc,id, listinv_code, FORMAT(created_date,'dd/MM/yyyy') created_date, created_by, approve_date, approve_by, s_name, s_tax, item_type, listinvoice_num, period_type, period,curr,times f_times, times,status,doc,status_received,error_msg_van from s_list_inv `,
    sql_searchBth_mysql: `select id as idc,id, listinv_code, DATE_FORMAT(created_date,'%d/%m/%Y')created_date, created_by, approve_date, approve_by, s_name, s_tax, item_type, listinvoice_num, period_type, period,curr,times f_times,times,status,doc,status_received,error_msg_van from s_list_inv `,
    sql_searchBth_orcl: `select id "idc",id "id", listinv_code "listinv_code", to_char(created_date,'dd/MM/yyyy') "created_date", created_by "created_by", curr "curr",approve_date "approve_date", approve_by "approve_by", s_name "s_name", s_tax "s_tax", item_type "item_type", listinvoice_num "listinvoice_num", period_type "period_type", period "period", times "f_times",times "times",status "status",doc "doc",status_received "status_received",error_msg_van "error_msg_van" from s_list_inv `,
    sql_searchBth_pgsql: `select id as idc,id, listinv_code, to_char(created_date,'DD/MM/YYYY') created_date, created_by, approve_date, approve_by, s_name, s_tax, item_type, listinvoice_num, period_type, period,curr,times f_times,times,status,doc,status_received,error_msg_van from s_list_inv `
}

const sql_searchBthApproved = {
    sql_searchBthApproved_mssql: `select id as idc,id, listinv_code, FORMAT(created_date,'dd-MM-yyyy hh:mm:ss')created_date, created_by, approve_date, approve_by, s_name, s_tax, item_type, listinvoice_num, period_type, period,times f_times,status,doc from s_list_inv `,
    sql_searchBthApproved_mysql: `select id as idc,id, listinv_code, DATE_FORMAT(created_date,'%d-%m-%Y %T')created_date, created_by, approve_date, approve_by, s_name, s_tax, item_type, listinvoice_num, period_type, period,times f_times,status,doc from s_list_inv `,
    sql_searchBthApproved_orcl: `select id as "idc",id "id", listinv_code "listinv_code", TO_CHAR(created_date, 'DD-MM-YYYY HH24:MI:SS') "created_date", created_by "created_by", approve_date "approve_date", approve_by "approve_by", s_name "s_name", s_tax "s_tax", item_type "item_type", listinvoice_num "listinvoice_num", period_type "period_type", period "period", times "f_times",status "status",doc "doc" from s_list_inv `,
    sql_searchBthApproved_pgsql: `select id as idc,id, listinv_code, to_char(created_date,'dd-MM-yyyy hh:mm:ss')created_date, created_by, approve_date, approve_by, s_name, s_tax, item_type, listinvoice_num, period_type, period,times f_times,status,doc from s_list_inv `
}

const sql_history = {
    sql_history_mssql:   `select htg.id, FORMAT(htg.datetime_trans,'dd/MM/yyyy HH:mm:ss') datetime_trans,htg.status,htg.description, htg.r_xml "r_xml"
                        from van_history htg join s_list_inv sli on htg.id_ref=sli.id 
                        where htg.type_invoice=1 and htg.id_ref=? order by htg.datetime_trans`
    ,
    sql_history_mysql:  `select htg.id,DATE_FORMAT(htg.datetime_trans,'%d/%m/%Y %T') datetime_trans,htg.status,htg.description, htg.r_xml "r_xml"
                        from van_history htg join s_list_inv sli on htg.id_ref=sli.id 
                        where htg.type_invoice=1 and htg.id_ref=? order by htg.datetime_trans`
    ,
    sql_history_orcl:  `select htg.id "id",to_char(htg.datetime_trans,'DD/MM/YYYY HH24:MI:SS') "datetime_trans",htg.status "status",htg.description "description", htg.r_xml "r_xml"
                        from van_history htg join s_list_inv sli on htg.id_ref=sli.id 
                        where htg.type_invoice=1 and htg.id_ref=? order by htg.datetime_trans`
    ,
    sql_history_pgsql:  `select htg.id,to_char(htg.datetime_trans,'DD/MM/YYYY HH:MI:SS') datetime_trans,htg.status,htg.description, htg.r_xml "r_xml"
                        from van_history htg join s_list_inv sli on htg.id_ref=sli.id 
                        where htg.type_invoice=1 and htg.id_ref=? order by htg.datetime_trans`
}

const saveth = async (data,req,form__, next) => {
    const token = sec.decode(req)
    let sql,  binds
    let form = req.body
    for(const item of data.data){                      
        // if (form.invtype != "2") {
        //     item.idt = moment(new Date(item.idt)).format(dtf)
        // }
        item.id = item.idinv
        delete item.idinv
       // delete item.line
        if(item.adj){
            item.adj = {
                form: item.adj.form,
                period: item.adj.period,
                periodtype: item.adj.periodtype,
                seq: item.adj.seq,
                serial: item.adj.serial,
                type_ref: item.adj.type_ref,
                note: item.adj.note,
                inform_num: item.adj.inform_num,
                inform_dt: item.adj.inform_dt
            }
        }
        
        if (JSON.stringify(item.adj) == "{}") delete item.adj
        delete item.adjform,
        delete item.adjperiod,
        delete item.adjperiodtype,
        delete item.adjseq,
        delete item.adjserial
    }
    try { 
        let fd = new Date(Date.parse(form.fd)), d = fd.getDate(), m = fd.getMonth()+1, y =fd.getFullYear(),period_type,period,tdf=util.convertDate(moment(form.td).endOf("day").format(dtf)),fdf = util.convertDate(moment(form.fd).startOf("day").format(dtf))
        if (form.period == "d") {
            period_type="N"
            //period.setValue(moment(form.fd).format("DD/MM/YYYY"))
            period = d.toString().padStart(2, "0") +"/"+m.toString().padStart(2, "0")+"/"+y
        }
        else if (["q1", "q2", "q3", "q4"].includes(form.period)) {
            period_type="Q"
            period = form.period.replace("q", "0") + "/" + y
        }
        else {
            period_type="T"
            period=m.toString().padStart(2, "0")+"/"+y
        }
    sql = `insert into s_list_inv (id,listinv_code,ou, created_date,s_name, s_tax,item_type, period_type, period,times, created_by, doc,listinvoice_num,curr)   values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)`               
    let id = await redis.incr("INCLIST")
    let doc = {
        ou:token.ou,
        stax:token.taxc,
        sname:token.on,
        name_bth:"Bảng tổng hợp dữ liệu hóa đơn điện tử",
        period_type:period_type,
        period:period,
        idt: moment(new Date()).format(dtf),
        times:(form.f_times == 1)?0:form__.times,
        listinv_code:TYPE_TH,
        created_by:token.uid,
        // listinvids:data.listinvids,
        items: data.data,
        item_type: form.invtype,
        listinvoice_num: (form.listinvoice_num? form.listinvoice_num.toString().padStart(5, "0") :null),
        curr: data.curr,
    }
    binds = [
      id,
      TYPE_TH,
      token.ou,
      util.convertDate(moment(new Date()).format(dtf)),
      token.on,
      token.taxc,
      form.invtype,
      period_type,
      period,
      form.f_times == 1 ? 0 : form.times,

      token.uid,
      JSON.stringify(doc),
      form.listinvoice_num
        ? form.listinvoice_num.toString().padStart(5, "0")
        : null,
      data.curr,
    ];
           
           // let   result = await inc.execsqlinsupddel(sql, binds)    
            let loopUpdateIds = data.listinvids
            let loopUpdateIds_bx = data.listinvids_bx
            let binds2=[]
   
            for(let i of loopUpdateIds){
                binds2.push([id, i,""])
            }
            if(loopUpdateIds_bx.length>0){
                for(let i of loopUpdateIds_bx){
                    binds2.push([id, i.invid,"BX"])
                }
            }
            let conn
          //  if (data.listinvids) loopUpdateIds = data.listinvids
        switch (dbtype) {
            case "mssql":
                await  inc.execsqlinsupddel(sql, binds)
                await dbs.queries(`insert into  s_list_inv_id  values (@1,@2,@3)`, binds2)
                
                await inc.execsqlinsupddel(`update s_inv set list_invid=?,period_type=?,period=? where id in (select inv_id from s_list_inv_id where id=? and (loai is null or loai <>'BX'))`, [id,period_type,period,id])
                //await inc.execsqlinsupddel(`update s_inv set list_invid_bx=? where id in (select inv_id from s_list_inv_id where id=? and loai ='BX')`, [id,id])
                await inc.execsqlinsupddel(`update s_list_inv_cancel set listinv_id=? where inv_id  in (select inv_id from s_list_inv_id where id=?) and cdt between ? and ?`, [id,id,fdf,tdf])
                
                if(loopUpdateIds_bx.length>0){
                    for(let i of loopUpdateIds_bx){
                        await inc.execsqlinsupddel(`update s_inv_adj set listinv_id=?, statuscqt = 0 where id = ? and createdate between ? and ?`, [id,i.invadjid,fdf,tdf])
                        await inc.execsqlinsupddel(`update s_inv_adj set listinv_id = -1 where inv_id = ? and listinv_id is null`, [i.invid])
                        await inc.execsqlinsupddel(`update s_inv set list_invid_bx=? where id in (select inv_id from s_list_inv_id where id=? and loai ='BX')`, [id,id])
                    }
                }
               
                break
            case "mysql":
              
                try {
                conn = await dbs.getConnection()
                await conn.beginTransaction()
                await  conn.query(`insert into  s_list_inv_id values ?`, [binds2])
                await  conn.query(sql, binds)
                await conn.query(`update s_inv set list_invid=?,period_type=?,period=? where id in (select inv_id from s_list_inv_id where id=? and (loai is null or loai <>'BX'))`, [id,period_type,period,id])
                await conn.query(`update s_inv set list_invid_bx=? where id in (select inv_id from s_list_inv_id where id=? and loai ='BX')`, [id,id])

                await conn.query(`update s_list_inv_cancel set listinv_id=? where inv_id in (select inv_id from s_list_inv_id where id=? ) and cdt between ? and ?`, [id,id,fdf,tdf])
    
                await conn.query(`update s_inv_adj set listinv_id=?, statuscqt = 0 where inv_id in  (select inv_id from s_list_inv_id where id=?) and createdate between ? and ?`, [id,id,fdf,tdf])
    
                await conn.commit()
                }catch (err) {
                    await conn.rollback()
                    throw new Error(err)
                }
                finally {
                await conn.release()
                }
                break
            case "orcl":
         
                try {
                conn = await dbs.getConn(false)           
                await  conn.executeMany(`insert into  S_LIST_INV_ID (id,inv_id, loai) values (:1,:2,:3)`, binds2,opt)
                //console.log(binds)
                await  conn.execute(`insert into s_list_inv (id,listinv_code,ou, created_date,s_name, s_tax,item_type, period_type, period,times, created_by, doc,listinvoice_num, curr)   values (:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,:14)`, binds,opt)
                await  conn.execute(`update s_inv set list_invid=:1,period_type=:2,period=:3 where id in (select inv_id from S_LIST_INV_ID where id=:4 and (loai is null or loai <>'BX'))`, [id,period_type,period,id],opt)
                //await  conn.execute(`update s_inv set list_invid_bx=:1 where id in (select inv_id from S_LIST_INV_ID where id=:2 and loai ='BX')`, [id,id],opt)

                await  conn.execute(`update s_list_inv_cancel set listinv_id=:1 where inv_id in (select inv_id from S_LIST_INV_ID where id=:2) and cdt between :3 and :4`, [id,id,fdf,tdf],opt)
                if(loopUpdateIds_bx.length>0){
                    for(let i of loopUpdateIds_bx){
                        await  conn.execute(`update s_inv_adj set listinv_id=:1, statuscqt = 0 where id =:2 and createdate between :3 and :4`, [id,i.invadjid,fdf,tdf],opt)
                        await  conn.execute(`update s_inv_adj set listinv_id = -1 where inv_id = :1 and listinv_id is null`, [i.invid],opt)
                        await  conn.execute(`update s_inv set list_invid_bx=:1 where id in (select inv_id from S_LIST_INV_ID where id=:2 and loai ='BX')`, [id,id],opt)
                    }
                }
                await  conn.commit()
                }catch (err) {
                    await conn.rollback()
                    throw new Error(err)
                }
                finally {
                    await dbs.closeConn(conn)
                }
                break
            case "pgsql":
            //chau sửa cho pgr phan bth
               await  inc.execsqlinsupddel(sql, binds,opt)   
              await inc.execsqlinsupddel(`insert into  S_LIST_INV_ID values (:1,:2,:3)`,binds2, opt)
              await inc.execsqlinsupddel(`update s_inv set list_invid=?,period_type=?,period=? where id in (${loopUpdateIds.toString()})`, [id,period_type,period])
              await inc.execsqlinsupddel(`update s_inv set list_invid=? where id in (${loopUpdateIds.toString()})`, [id])

              await inc.execsqlinsupddel(`update s_list_inv_cancel set listinv_id=? where inv_id in (${loopUpdateIds.toString()})`, [id])

              await inc.execsqlinsupddel(`update s_inv_adj set listinv_id=?, statuscqt = 0 where inv_id in (${loopUpdateIds.toString()})`, [id])
              break
            }
            return id  
                     
    }
    catch (err) {
        console.trace(err)
        throw new Error(err)
    }
  
}

const rbi = (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            const result = await inc.execsqlselect(`select doc "doc", status "status", approve_date "adt" from s_list_inv where id=?`, [id])
            const rows = result
            if (rows.length == 0) reject(new Error(`Không tìm thấy bảng tổng hợp ${id}`))
            let doc = util.parseJson(rows[0].doc)
            doc.type = TYPE_TH
            doc.status = rows[0].status
            doc.adt= rows[0].adt
            resolve(doc)
        } catch (err) {
            reject(err)
        }
    })
}

const Service = {
    search: async (data, req) => {
        const token = sec.decode(req)
        let sql, result
        let keySearchBth = uuid.v1()
        let where = ` where dt.idt between ? and ? and dt.stax=? `,binds = [util.convertDate(data.fd), util.convertDate(moment(data.td).endOf("day").format(dtf)), token.taxc]

        sql =`insert into s_tmp_list_inv (keyname,id,kindinv,curr,idt,type,form,serial,seq) select '${keySearchBth}' "keyname", dt.id "id", CASE WHEN dt.status = 4 THEN 0 WHEN dt.status = 3 and dt.adjtyp is null THEN 0 WHEN dt.status = 3 and dt.adjtyp =1 THEN 3 WHEN dt.status = 3 and dt.adjtyp =2 THEN 2  ELSE 0 END "kindinv", curr "curr", dt.idt "idt", dt.type "type", dt.form "form", dt.serial "serial", dt.seq "seq" from s_inv dt left join s_serial lt on dt.form = lt.form and dt.serial=lt.serial and dt.stax = lt.taxc ` 
        if (data.listinvoice_num) {
            let form = req.body
            let fd = new Date(Date.parse(form.fd)), d = fd.getDate(), m = fd.getMonth()+1, y =fd.getFullYear(),period_type,period
            if (form.period == "d") {
                period_type="N"
                //period.setValue(moment(form.fd).format("DD/MM/YYYY"))
                period = d.toString().padStart(2, "0") +"/"+m.toString().padStart(2, "0")+"/"+y
            }
            else if (["q1", "q2", "q3", "q4"].includes(form.period)) {
                period_type="Q"
                period = form.period.replace("q", "0") + "/" + y
            }
            else {
                period_type="T"
                period=m.toString().padStart(2, "0")+"/"+y
            }                    
            result = await inc.execsqlselect('select max(times) "times" from s_list_inv where s_tax=? and period=? and listinvoice_num=? and times=0 and status_received=12', [token.taxc,period,data.listinvoice_num.toString().padStart(5, "0") ])
            if(result[0].times===null ) throw new Error(`Không tồn tại số bảng tổng hợp ${data.listinvoice_num} cho kỳ dữ liệu ${period}  \n (Not found list invoice number ${data.listinvoice_num} with period ${period})`)
            result = await inc.execsqlselect('select max(times) "times" from s_list_inv where s_tax=? and period=? and listinvoice_num=?  and status_received=12', [token.taxc,period,data.listinvoice_num.toString().padStart(5, "0") ])
            if(result[0].times===null ) throw new Error(`Không tồn tại số bảng tổng hợp ${data.listinvoice_num} cho kỳ dữ liệu ${period}  \n (Not found list invoice number ${data.listinvoice_num} with period ${period})`)
            else {data.times=Number(result[0].times)+1}
        } 
        try {
            let vtime = (new Date()).getTime() //Lay gio hien tai
            let expiredate = new Date(vtime + (1800 * 1000)) //Tinh thoi gian expire de sau nay xoa trong truong hop ung dung bi restart bat ngo. Se co job khac quet de xoa cac key expire nay
            //logger4app.debug('expiredate ',moment(expiredate).format("YYYY-MM-DD HH:mm:ss"))

            await inc.execsqlinsupddel(`INSERT INTO s_listvalues (keyname, keyvalue, keytype) VALUES(?, ?, 'String')`, [`BTH_${token.ou}`, moment(expiredate).format("YYYY-MM-DD HH:mm:ss")]) //Insert du lieu vao bang s_listvalues theo key vaf ngay gio qua han
           
        } catch (err) {

            throw new Error(`LOCK_WAIT_CREATE`)
            
        }				   
        delete data.status
        Object.keys(data).forEach(key => {
            let val = data[key]
            if (val && val != "*") {
                if (["invtype"].includes(key)) {
                    where+=`and lt.invtype = ? `
                    binds.push(val)
                }
                else if (["type"].includes(key)) {
                    where += `and dt.type = ? and lt.type = ? `
                    binds.push(val)
                    binds.push(val)
                }
                else if (["form"].includes(key)) {
                    where += `and dt.form = ? `
                    binds.push(val)

                }
                else if (["serial"].includes(key)) {
                    where += `and dt.serial = ? and lt.serial = ? `
                    binds.push(val)
                    binds.push(val)
                }
                else if (["ou"].includes(key)) {
                    where += `and dt.ou = ? `
                    binds.push(val)
                }
            }
        })
        sql += where

        sql += `and list_invid IS NULL and lt.sendtype = 2 and dt.status in (3,4) `
        // sql += ` order by ABS(dt.id), ABS(dt.pid) `
        // //if (!data.operation) sql += limit
        await inc.execsqlselect(sql, binds)

        // let sql_auto = sql + `and list_invid IS NULL and lt.sendtype = 2 and dt.status in (3,4) and dt.id<0 order by ABS(dt.id), ABS(dt.pid) `
        // let sql_manual = sql + `and list_invid IS NULL and lt.sendtype = 2 and dt.status in (3,4) and dt.id>0 order by ABS(dt.id), ABS(dt.pid) `
        // sql += ` order by idt,dt,type,form,serial,seq `
        // result = await inc.execsqlselect(sql_auto, binds)
        // let result2 = await inc.execsqlselect(sql_manual, binds)
        // result = result.concat(result2)

        let sqlcc
        binds = [util.convertDate(data.fd), util.convertDate(moment(data.td).endOf("day").format(dtf)), token.taxc]
        where= "where cdt between ? and ? and stax=? and listinv_id is null "
        Object.keys(data).forEach(key => {
            let val = data[key]

            if (val && val != "*") {
                if (["type"].includes(key)) {
                    where += `and type = ? `
                    binds.push(val)
                }
                else if (["form"].includes(key)) {
                    where += `and form = ? `
                    binds.push(val)

                }
                else if (["serial"].includes(key)) {
                    where += `and serial = ? `
                    binds.push(val)

                }
                else if (["ou"].includes(key)) {
                    where += `and ou = ? `
                    binds.push(val)

                }
                else if (["invtype"].includes(key)) {
                    where += `and invtype = ? `
                    binds.push(val)

                }
            }
        })
        sqlcc = `insert into s_tmp_list_inv (keyname,id,kindinv,curr,idt,type,form,serial,seq) select '${keySearchBth}' "keyname", inv_id "id",1 "kindinv", curr "curr",cdt "idt", type "type", 0 "form", serial "serial", 0 "seq" from s_list_inv_cancel `
        sqlcc += where
        await inc.execsqlselect(sqlcc,binds)
        //if (resultListCancel.length) result.push(...(resultListCancel.map(e=>{ e.doc = util.parseJson(e.doc); e.doc.id = e.id; e.doc.kindinv = 1; return e.doc;})))

        // if (resultListCancel.length) result.push(...(resultListCancel.map(e=>{ return {id: e.id, kindinv: e.kindinv};})))

        let sqladj = `insert into s_tmp_list_inv (keyname,id,kindinv,curr)  select '${keySearchBth}' "keyname", "id", "kindinv", "curr" from (select MAX(id) "id", type "kindinv", inv_id, curr "curr" from s_inv_adj `
        binds = [util.convertDate(data.fd), util.convertDate(moment(data.td).endOf("day").format(dtf)), token.taxc]
        where = "where createdate between ? and ? and stax=? and listinv_id is null "
        Object.keys(data).forEach(key => {
            let val = data[key]
            if (val && val != "*") {
                if (["form"].includes(key)) {
                    where += `and form = ? `
                    binds.push(val)
                }
                else if (["serial"].includes(key)) {
                    where += `and serial = ? `
                    binds.push(val)
                }
            }
        })
        sqladj += where
        sqladj += ' group by inv_id,type,curr) tmp'
        await inc.execsqlselect(sqladj,binds)
        // if (resultInvAdj.length) result.push(...(resultInvAdj.map(e=>{ return {id: e.id, kindinv: e.kindinv, curr: e.curr};})))
        // Tách mảng có thể có nhiều loại tiền thành 1 object có từng loại tiền
        // select bảng tạm và sort theo loại tiền, id giảm dần
        let tmpSql = `select id "id", kindinv "kindinv", curr "curr" from s_tmp_list_inv where keyname=? order by curr,kindinv,idt,type,form,serial,seq`
        result = await inc.execsqlselect(tmpSql,[keySearchBth])
        //tmpSql = `select id "id", kindinv "kindinv", curr "curr" from s_tmp_list_inv where keyname=? and kindinv = 1 order by curr,kindinv,idt,type,form,serial,seq`
        //let resultCancel = await inc.execsqlselect(tmpSql,[keySearchBth])
        //result.push(...resultCancel)
        await inc.execsqlinsupddel(`delete from s_tmp_list_inv where keyname = ?`,[keySearchBth])
        if (result.length==0 ) throw new Error(`Không có bản ghi nào \n (Not found)`)
        return result
    },
    collistinv: async (result,req,form__) => {
        let count_loop = 0// tinh luong hoa don
        let count_length = 0// xác định hết mảng
        let arr = []
        let arr_id = []
        let listinvids=[]
        let listinvids_bx=[]
        let form = req.body
        let td = new Date(util.convertDate(moment(form.td).endOf("month").format(dtf)))
        for (let item of result) {
            count_length++           
            let resultadj
            let resultdoc
            if (item.kindinv == 4 || item.kindinv == 5) {
                resultadj = await inc.execsqlselect(`select note "note", so_tb "so_tb", ngay_tb "ngay_tb", th_type "th_type", inv_id "inv_id" from s_inv_adj where id = ?`, [item.id])
                if (resultadj && resultadj.length > 0) { 
                    resultdoc = await inc.execsqlselect('select doc "doc", list_invid "list_invid",cde "cde" from s_inv where id=?',[resultadj[0].inv_id])
                }
                else {continue}
            }
            else {
                resultdoc = await inc.execsqlselect('select doc "doc", list_invid "list_invid",cde "cde" from s_inv where id=?',[item.id])
            }
            if(resultdoc.length==0){               
                continue
            } 
            let doc = util.parseJson(resultdoc[0].doc),cde=resultdoc[0].cde
            item.invid = doc.id
            let adj ={}
            if (doc.btax && !util.checkmst(doc.btax.toString())){               
                //objMapInv.MKHang = objMapInv.MSTNMua
                doc.btax = ''
            }
            if (resultadj && resultadj.length > 0) {
                if (resultadj[0].th_type == 2) {
                    adj.inform_num = resultadj[0].so_tb
                    adj.inform_dt = resultadj[0].ngay_tb 
                }
                adj.note = resultadj[0].note
            }
            if(doc.type =='01GTKT') {
                let itemLoop = doc.tax
                if (doc.adj && doc.adj.typ == 2) {
                    if (doc.items.length>0 && (doc.items[0].type && doc.items[0].type == "Mô tả")) {
                        doc.items[0].vatv = 0
                        doc.items[0].amtv = 0
                        doc.items[0].vat = 0
                        doc.items[0].amt = 0
                        itemLoop = doc.items
                    }
                }
                // if (item.adjtyp == 2) {
                //     let adjitemLoop = item.dif
                //     itemLoop = adjitemLoop.tax
                // }
                var taxl = [-1, -2, 0, 5, 8, 10];
                
                if(itemLoop.length==0){
                    let items={
                            vat:0,
                            vrn:"0%",
                            amtv:0,
                            vatv:0 ,
                            amt:0,
                            vat:0 ,
                            vrt :0             
                    }
                    itemLoop.push(items)
                }
                // if(item.kindinv == 1){
                //     let items={
                //             vat:0,
                //             vrn:"0%",
                //             amtv:0,
                //             vatv:0 ,
                //             amt:0,
                //             vat:0 ,
                //             vrt :0             
                //     }
                //     itemLoop=[items]
                // }
                // delete resultdoc.vat
                // delete resultdoc.vatv
                // kindinv 4 giải trình tổng hợp
                // kindinv 1 tạo mới trong bảng s_list_inv_cancel
                // kindinv 2 tạo mới điều chỉnh
                // kindinv 3 tạo mới thay thế
                let a
                for (let tax of itemLoop) {
                    count_loop++
                    let itemtax = doc.items
                    //giai trinh sua lại duong tien 2023
                    // if (item.kindinv == 4){
                    //     if(tax.vat) tax.vat = -tax.vat
                    //     tax.amt = -tax.amt                   
                    // }
                    //if (tax.vrn.includes("%"))tax.vrn1 = tax.vrn
                    if (tax.vrt == -1){
                        tax.vrn = "KCT"
                        //tax.vrn1 = "Không chịu thuế"
                    }
                    else if (tax.vrt == -2){
                        tax.vrn = "KKKNT"
                        //tax.vrn1 = "Không kê khai nộp thuế"
                    }
                        else if (!taxl.includes(Number(tax.vrt))){
                        tax.vrn = `KHAC:${(tax.vrt)}%`
                        //tax.vrn1 = `KHAC:${Number(tax.vrt).toFixed(2)}%`
                    }
                    a = {
                        inc: item.invid,
                        // amountv: tax.amtv||0,
                        // vatv: (tax.vrt <0?0: (tax.vatv||0)),
                        amount: util.roundNumberInBth(tax.amt||0, doc.curr),
                        vat: util.roundNumberInBth(tax.vrt <0?0: (tax.vat||0), doc.curr),
                        vrn: tax.vrn,
                        bname: doc.bname,
                        bcode: doc.bcode,
                        btax: doc.btax,
                        form: doc.form,
                        idt: doc.idt,
                        note: doc.note,
                        serial: doc.serial,
                        seq: doc.seq,
                        //id: item.id,
                        idinv: doc.id,
                        totalv: parseFloat((tax.amtv||0)+(tax.vrt <0?0: (tax.vatv||0))),
                        total: util.roundNumberInBth((tax.amt||0)+(tax.vrt <0?0: (tax.vat||0)), doc.curr),
                        //totalv: item.totalv,
                        kindinv: item.kindinv,             
                        type: doc.type,
                        exrt: doc.exrt
                        //status: item.status
                        // items: item.items
                    }
                    if (item.kindinv == 0 && doc.status ==4 &&  doc.adj && doc.adj.typ==1){//huy thay the
                        a.kindinv=3
                    }
                    if (item.kindinv == 0 && doc.status ==4 &&  doc.adj && doc.adj.typ==2){//huy thay the
                        a.kindinv=2
                    }
                    // if (doc.kindinv ==4){
                    //   if(tax.vat)  a.vat = -tax.vat
                    //     a.amount = -tax.amt
                    //     a.total = -parseFloat(tax.amt+tax.vat)  
                    // }
                    if (item.kindinv == 2 || item.kindinv ==3){//dc thay the
                        //a.typeref0 = item.typeref0
                        if(doc.adj && doc.adj.serial && !doc.adj.serial.includes('/'))
                        a.typeref0 = 1
                        else a.typeref0 = 3
                        if(doc.adj && !doc.adj.serial && doc.adj.seq) {
                            let serial= doc.adj.seq.split('-')[1]
                            if(serial.includes('/'))
                            a.typeref0 = 3
                            else a.typeref0 = 1
                        }
                    }
                    if (item.kindinv ==0  && doc.status ==4 && doc.adj){//thay the thay the
                        //a.typeref0 = item.typeref0
                        if(doc.adj && doc.adj.serial && !doc.adj.serial.includes('/'))
                        a.typeref0 = 1
                        else a.typeref0 = 3
                        if(doc.adj && !doc.adj.serial && doc.adj.seq) {
                            let serial= doc.adj.seq.split('-')[1]
                            if(serial.includes('/'))
                            a.typeref0 = 3
                            else a.typeref0 = 1
                        }
                    }
                    if (item.kindinv == 1 && doc.status ==4 ){
                        if(doc.cancel) a.note = doc.cancel.rea
                      }
                    
                    // if (item.kindinv == 0 && doc.status ==4 && cde){
                    //     a.kindinv=0
                    // }
                    if (resultadj && resultadj.length > 0) a.adj = adj
                    if (doc.adj && !resultdoc[0].list_invid && item.kindinv != 4  &&  item.kindinv != 5){//dc thay thê
                        adj.form = doc.adj.form
                        adj.serial = doc.adj.serial
                        adj.seq = doc.adj.seq
                        if (adj.seq) {
                            if(!adj.form) adj.form= adj.seq.split('-')[0]
                            if(!adj.serial) adj.serial= adj.seq.split('-')[1]
                            if(adj.seq.includes('-')) adj.seq=adj.seq.split('-')[2]
                        }
                        adj.period = doc.adj.period
                        adj.periodtype = doc.adj.periodtype
                        adj.type_ref =  a.typeref0 
                        adj.note = doc.adj.rea
                        
                    } 
                    if (resultadj && resultadj.length > 0) {
                        if (resultadj[0].th_type == 2) {
                            adj.inform_num = resultadj[0].so_tb
                            adj.inform_dt = resultadj[0].ngay_tb 
                        }
                        adj.note = resultadj[0].note
                        a.note=resultadj[0].note
                    }
                   // if (adj.hasOwnProperty('seq')) a.adj = adj
                     if (adj) a.adj = adj
                    // if (doc.status == 4){
                    //     a.vatv = -Math.abs(tax.vatv)
                    //     a.amountv = -Math.abs(tax.amtv)
                    //     a.totalv = -Math.abs(parseFloat(tax.amtv+tax.vatv))
                    // }
                    if (doc.dif) {
                        if ((doc.dif.sum && doc.dif.sum > 0)|| (doc.dif.total && doc.dif.total > 0) || doc.adjType==3) {
                            // a.vatv = -Math.abs(tax.vatv)
                            // a.amountv = -Math.abs(tax.amtv)
                            // a.totalv = -Math.abs(parseFloat(tax.amtv+tax.vatv))
                            a.vat = util.roundNumberInBth(-1*Math.abs(tax.vat), doc.curr)
                            a.amount = util.roundNumberInBth(-1*Math.abs(tax.amt), doc.curr)
                            a.total =util.roundNumberInBth(-1*Math.abs(parseFloat(tax.amt+tax.vat)), doc.curr)
                        }
                    }
                    if ( doc.adjType==3) {
                        // a.vatv = -Math.abs(tax.vatv)
                        // a.amountv = -Math.abs(tax.amtv)
                        // a.totalv = -Math.abs(parseFloat(tax.amtv+tax.vatv))
                        a.vat = util.roundNumberInBth(-1*Math.abs(tax.vat), doc.curr)
                        a.amount = -Math.abs(tax.amt)
                        a.total = util.roundNumberInBth(-1*Math.abs(parseFloat(tax.amt+tax.vat)), doc.curr)
                    }
                    if(item.kindinv == 1 || item.kindinv == 4 || item.kindinv == 5){//huy ,giai trình,sai sot do tong hop
                        listinvids_bx.push({invid: item.invid, invadjid: item.id})
                    }else{
                        listinvids.push(item.invid)
                    }
                   
                   
                    a.line = count_loop 
                    if(a.kindinv == 0 ) delete a.note
                    arr.push(a)
                   
                }
                let cdt=''
                if (item.kindinv == 0 && doc.status ==4 && !cde ){
                    cdt =new Date(Date.parse(doc.cdt))
                    if (!cdt) cdt =new Date(Date.parse(doc.idt))
                }
                if (item.kindinv == 0 && doc.status ==4 && !cde && new Date(Date.parse(cdt))<=td){//huy
                    let a_2={...a}
                    // a_2.amountv= doc.sumv
                    // a_2.vatv=doc.vatv
                    // a_2.totalv=doc.totalv
                    a_2.amount= util.roundNumberInBth(doc.sum, doc.curr)
                    a_2.vat=util.roundNumberInBth(doc.vat, doc.curr)
                    a_2.total=util.roundNumberInBth(doc.total, doc.curr)

                    count_loop++
                    a_2.line = count_loop
                    a_2.kindinv=1
                    if(doc.cancel) a_2.note = doc.cancel.rea
                    delete a_2.adj
                    arr.push(a_2)
                }
               
            } else {
                count_loop++
                let a = {
                    inc: item.invid,
                    amountv: doc.sumv,
                    vatv: 0,
                    amount: util.roundNumberInBth(doc.sum, doc.curr),
                    vat: 0,
                    vrn: "0%",
                    bname: doc.bname,
                    bcode: doc.bcode,
                    btax: doc.btax,
                    form: doc.form,
                    idt: doc.idt,
                    note: doc.note,
                    serial: doc.serial,
                    seq: doc.seq,
                    //id: item.id,
                    idinv: doc.id,
                    // totalv: doc.totalv,
                    total: util.roundNumberInBth(doc.total, doc.curr),
                    //totalv: item.totalv,
                    kindinv: item.kindinv,             
                    type: doc.type,
                    exrt: doc.exrt
                    //status: item.status
                    // items: item.items
                }
                // if (doc.kindinv ==4){
                //   if(tax.vat)  a.vat = -tax.vat
                //     a.amount = -tax.amt
                //     a.total = -parseFloat(tax.amt+tax.vat)  
                // }
                if (item.kindinv == 2 || item.kindinv ==3){//dc thay the
                    //a.typeref0 = item.typeref0
                    if(doc.adj && doc.adj.serial && !doc.adj.serial.includes('/'))
                    a.typeref0 = 1
                    else a.typeref0 = 3
                    if(doc.adj && !doc.adj.serial && doc.adj.seq) {
                        let serial= doc.adj.seq.split('-')[1]
                        if(serial.includes('/'))
                        a.typeref0 = 3
                        else a.typeref0 = 1
                    }
                }
                if (item.kindinv ==0  && doc.status ==4 && doc.adj){//thay the thay the
                    //a.typeref0 = item.typeref0
                    if(doc.adj && doc.adj.serial && !doc.adj.serial.includes('/'))
                    a.typeref0 = 1
                    else a.typeref0 = 3
                    if(doc.adj && !doc.adj.serial && doc.adj.seq) {
                        let serial= doc.adj.seq.split('-')[1]
                        if(serial.includes('/'))
                        a.typeref0 = 3
                        else a.typeref0 = 1
                    }
                }
                // if (item.kindinv == 0 && doc.status ==4 && cde){
                //     a.kindinv=3
                // }
                if (item.kindinv == 1 && doc.status ==4 ){
                    if(doc.cancel) a.note = doc.cancel.rea
                  }
                if (item.kindinv == 0 && doc.status ==4 &&  doc.adj && doc.adj.typ==1){//huy thay the
                    a.kindinv=3
                }
                if (item.kindinv == 0 && doc.status ==4 &&  doc.adj && doc.adj.typ==2){//huy thay the
                    a.kindinv=2
                }
                if (resultadj && resultadj.length > 0) a.adj = adj
                if (doc.adj && !resultdoc[0].list_invid && item.kindinv != 4  &&  item.kindinv != 5){
                    adj.form = doc.adj.form
                    adj.serial = doc.adj.serial
                    adj.seq = doc.adj.seq
                    if (adj.seq) {
                        if(!adj.form) adj.form= adj.seq.split('-')[0]
                        if(!adj.serial) adj.serial= adj.seq.split('-')[1]
                        if(adj.seq.includes('-')) adj.seq=adj.seq.split('-')[2]
                    }
                    adj.period = doc.adj.period
                    adj.periodtype = doc.adj.periodtype
                    adj.type_ref = a.typeref0
                    adj.note = doc.adj.rea
                   
                } 
                if (resultadj && resultadj.length > 0) {
                    if (resultadj[0].th_type == 2) {
                        adj.inform_num = resultadj[0].so_tb
                        adj.inform_dt = resultadj[0].ngay_tb 
                    }
                    adj.note = resultadj[0].note
                    a.note=resultadj[0].note
                }
                //if (adj.hasOwnProperty('seq')) a.adj = adj
                if (adj) a.adj = adj
                // if (doc.status == 4){
                //     a.vatv = -Math.abs(0)
                //     a.amountv = -Math.abs(doc.sumv)
                //     a.totalv = -Math.abs(parseFloat(doc.sumv))
                // }
                if (doc.dif) {
                    if ((doc.dif.sum && doc.dif.sum > 0)|| (doc.dif.total && doc.dif.total > 0) || doc.adjType==3) {
                        // a.vatv = -Math.abs(0)
                        // a.amountv = -Math.abs(doc.sumv)
                        // a.totalv = -Math.abs(parseFloat(doc.sumv))
                        a.vat = 0
                        a.amount = util.roundNumberInBth(-1*Math.abs(doc.sum), doc.curr)
                        a.total = util.roundNumberInBth(-1*Math.abs(parseFloat(doc.sum)), doc.curr)
                    }
                }
                if ( doc.adjType==3) {
                    // a.vatv = -Math.abs(0)
                    // a.amountv = -Math.abs(doc.sumv)
                    // a.totalv = -Math.abs(parseFloat(doc.sumv))
                    a.vat = 0
                    a.amount = util.roundNumberInBth(-1*Math.abs(doc.sum), doc.curr)
                    a.total = util.roundNumberInBth(-1*Math.abs(parseFloat(doc.sum)), doc.curr)
                }
                if(item.kindinv == 1 || item.kindinv == 4 || item.kindinv == 5){//huy ,giai trình,sai sot do tong hop
                    listinvids_bx.push({invid: item.invid, invadjid: item.id})
                }else{
                    listinvids.push(item.invid)
                }
               
               // listinvids.push(item.id)
                a.line = count_loop
                if(a.kindinv == 0 ) delete a.note
                arr.push(a)
                let cdt=''
                if (item.kindinv == 0 && doc.status ==4 && !cde ){
                    cdt =new Date(Date.parse(doc.cdt))
                    if (!cdt) cdt =new Date(Date.parse(doc.idt))
                }
                if (item.kindinv == 0 && doc.status ==4 && !cde  && new Date(Date.parse(cdt))<=td){//huy
                    let a_2={...a}
                    count_loop++
                    a_2.line = count_loop
                    a_2.kindinv=1
                    if(doc.cancel) a_2.note = doc.cancel.rea
                    delete a_2.adj
                    arr.push(a_2)
                }
            }
            if(count_loop >= config.bth_length || count_length==result.length || (result[count_length].curr!=item.curr) || (result[count_length].kindinv!=item.kindinv && item.kindinv ==0) ){
                let dataloop=  {data:arr, listinvids: listinvids,listinvids_bx:listinvids_bx, curr: item.curr}
                let idth =   await saveth(dataloop,req,form__)
                arr_id.push(idth)
                count_loop = 0
                arr = []
                listinvids=[]
                listinvids_bx=[]
            }
        }
        
        return arr_id
    },
    getListByInvTypeOil: async (result, req, next) => {
        try {
          
            let listinvids=[],sumObject = {},dataReturn=[]
      
            if (result.length) {
                for (let i of result) {
                    let resultdoc = await inc.execsqlselect('select doc "doc" from s_inv where id=?',[i.id])
                    let doc = util.parseJson(resultdoc[0].doc)
                    let itemsLoop = doc.items, isAdjust = doc.adj && doc.adj.typ==2, isCancel = doc.status == 4
                    listinvids.push(i.id)
                    // if (isAdjust) {
                    //     itemsLoop = doc.dif.items
                    // }
                    for (let item of itemsLoop) {
                        let icode = item.code
                        // dataReturn.push(obj)
                        if (!sumObject[icode]) {
                            sumObject[icode] = {
                                "code": item.code,
                                "name": item.name,
                                "unit": item.unit,
                                "quantity": 0,
                                "amount": 0,
                                "vat": 0,
                                "total": 0,
                                "vrn": item.vrn,
                                "note": "",
                                "kindinv": "0",
                                "vatv": 0,
                                "amountv": 0,
                                "totalv": 0
                            }
                            if (!doc.adj){
                                delete doc.type_ref
                            }
                        }
                        // if (isAdjust) {
                        //     if (item.status == 0) {
                        //         item.amount = - Math.abs(item.amount)
                        //         item.vat = - Math.abs(item.vat)
                        //         item.total = - Math.abs(item.total)
                        //         item.quantity = - Math.abs(item.quantity)
                        //     }
                        // }
                        if (isCancel) {
                            item.amount = - Math.abs(item.amount)
                            item.vat = - Math.abs(item.vat)
                            item.total = - Math.abs(item.total)
                            item.quantity = - Math.abs(item.quantity)
                            item.vatv = - Math.abs(item.vatv)
                            item.amountv = - Math.abs(item.amountv)
                            item.totalv = - Math.abs(item.totalv)
                        }
                        let obj = sumObject[icode]
                        obj.amount += Number(item.amount)
                        obj.vat += Number(item.vat)
                        obj.total += Number(item.total)
                        obj.quantity += Number(item.quantity)
                        obj.vatv += Number(item.vatv)
                        obj.amountv += Number(item.amountv)
                        obj.totalv += Number(item.totalv)
                    }
                }
            }
            for (let prop in sumObject) {
                dataReturn.push(sumObject[prop])
            }
          
            return {data:dataReturn, listinvids: listinvids}
           // res.json({dalengthaReturn, listinvids: listinvids})
        } catch (err) {
            next(err)
        }
    },
    save: async (data,req, next) => {        
        const token = sec.decode(req)
        let sql,  binds
        let form = req.body
        for(const item of data.data){                      
            // if (form.invtype != "2") {
            //     item.idt = moment(new Date(item.idt)).format(dtf)
            // }
            item.id = item.idinv
            delete item.idinv
            delete item.line
            if(item.adj){
                item.adj = {
                    form: item.adj.form,
                    period: item.adj.period,
                    periodtype: item.adj.periodtype,
                    seq: item.adj.seq,
                    serial: item.adj.serial,
                    type_ref: item.adj.type_ref,
                    note: item.adj.note
                }
            }
            
            if (JSON.stringify(item.adj) == "{}") delete item.adj
            delete item.adjform,
            delete item.adjperiod,
            delete item.adjperiodtype,
            delete item.adjseq,
            delete item.adjserial
        }
        try { 
            let fd = new Date(Date.parse(form.fd)), d = fd.getDate(), m = fd.getMonth()+1, y =fd.getFullYear(),period_type,period
            if (form.period == "d") {
                period_type="N"
                //period.setValue(moment(form.fd).format("DD/MM/YYYY"))
                period = d.toString().padStart(2, "0") +"/"+m.toString().padStart(2, "0")+"/"+y
            }
            else if (["q1", "q2", "q3", "q4"].includes(form.period)) {
                period_type="Q"
                period = form.period.replace('q', '') + "/" + y
            }
            else {
                period_type="T"
                period=m.toString().padStart(2, "0")+"/"+y
            }
        sql = `insert into s_list_inv (id,listinv_code,ou, created_date,s_name, s_tax,item_type, period_type, period,times, created_by, doc, curr) values (?,?,?,?,?,?,?,?,?,?,?,?,?)`               
        let id = await redis.incr("INCLIST")
        let doc = {
            ou:token.ou,
            stax:token.taxc,
            sname:token.on,
            name_bth:"Bảng tổng hợp dữ liệu hóa đơn điện tử",
            period_type:period_type,
            period:period,
            idt: moment(new Date()).format(dtf),
            times:(form.f_times == 1)?0:form.times,
            listinv_code:TYPE_TH,
            created_by:token.uid,
            listinvids:data.listinvids,
            items:data.data,
            item_type: form.invtype,
            curr: data.curr
        }
        binds = [ id,TYPE_TH,token.ou,
                   moment(new Date()).format(dtf),
                  token.on,
                   token.taxc,
                   form.invtype,               
                   period_type,
                   period,
                    (form.f_times == 1)?0:form.times,
                   // listinvoice_num: listinvoice_num,
                  token.uid,
                  JSON.stringify(doc),
                  data.curr
        ]
               
                let   result = await inc.execsqlinsupddel(sql, binds)    
                let loopUpdateIds = data.listinvids
              //  if (data.listinvids) loopUpdateIds = data.listinvids
                await inc.execsqlinsupddel(`update s_inv set list_invid=?,period_type=?,period=? where id in (${loopUpdateIds.toString()})`, [id,period_type,period])

                await inc.execsqlinsupddel(`update s_list_inv_cancel set listinv_id=? where inv_id in (${loopUpdateIds.toString()})`, [id])

                await inc.execsqlinsupddel(`update s_inv_adj set listinv_id=?, statuscqt = 0 where inv_id in (${loopUpdateIds.toString()})`, [id])

                return id  
                         
        }
        catch (err) {
            throw new Error(err)
        }
      
    },
    searchData: async (req, res, next) => {
        const form = req.body
        const token = sec.decode(req)
        try {
            let result = await Service.search(form, req)
            if(form.invtype !=1) result = await Service.collistinv(result,req,form)
            await inc.execsqlinsupddel(`delete from s_listvalues where keyname=?`, [`BTH_${token.ou}`]) 
            //  else result = await Service.getListByInvTypeOil(result, req,next) //xăng dầu tạm rào lại
            //  result = await Service.save(result,req,next)
            return res.json(result.toString())
        }
        catch (err) {
			 if(err.message=='LOCK_WAIT_CREATE')  err= new Error(`Tiến trình tạo bảng tổng hợp cho chi nhánh chưa kết thúc`)
             else await inc.execsqlinsupddel(`delete from s_listvalues where keyname=?`, [`BTH_${token.ou}`]) 
            next(err)
        }
    },
    
    searchBth: async (data, req) => {
        const token = sec.decode(req),mst = token.taxc
        const mailTimeCol = "created_date"
        let sql, result, binds = []
        let start = data.start ? data.start : 0, count = data.count ? data.count : 10
        delete data.start
        delete data.count
        delete data.continue
		let where = ` where 1=1 and s_tax= '${mst}'`, pagerobj = inc.getsqlpagerandbind(start, count) //VinhHQ12 gom DB
        sql = `${sql_searchBth[`sql_searchBth_${config.dbtype}`]}`
        delete data.status
        Object.keys(data).forEach(key => {
            let val = data[key]
            if (val) {
                if (["fd"].includes(key)) {
                    where += `and ${mailTimeCol} >= ? `
                    binds.push(util.convertDate(moment(val).startOf("day").format(dtf)))
                }
                else if (["td"].includes(key)) {
                    where += `and ${mailTimeCol} <= ? `
                    binds.push(util.convertDate(moment(val).endOf("day").format(dtf)))

                }
                else if (["statusBth"].includes(key)) {
                    where += `and status = ? `
                    binds.push(val)
                }
                else if (["listinvoice_num"].includes(key)) {
                    where += `and listinvoice_num = ? `
                    binds.push(val)
                }
                else if (["id"].includes(key)) {
                    where += `and id = ? `
                    binds.push(val)
                }
                else if (key != "operation") {
                    where += `and ${key}=? `
                    binds.push(val)
                }
                else if(["curr"].includes(key))
                {
                    where += `and curr = ? `
                    binds.push(val)
                }
                else if(["period"].includes(key))
                {
                    where += `and period = ? `
                    binds.push(val)
                }
            }
        })
        sql += where
        sql += ` order by id desc `
        if (!data.operation) {
            sql += pagerobj.vsqlpagerstr
		    for (let t of pagerobj.vsqlpagerbind) binds.push(t)
        }
        result = await inc.execsqlselect(sql, binds)
        let databth = { data: result, pos: start }
        if (start == 0) {
            let binds1 = binds
            if(!data.operation) binds1=binds1.slice(0, binds1.length-2)
            // for (let i = 0; i < binds.length - 2;i++) binds1.push(binds[i])
            sql = `select count(id) "total" from s_list_inv ${where} `
            result = await inc.execsqlselect(sql, binds1)
            databth.total_count = result.length > 0 ? result[0].total : 0
        }
        return databth
    },
    searchBthApproved: async (req, res, next) => {
        try {
            const token = sec.decode(req), data = req.query,mst = token.taxc
            const mailTimeCol = "created_date"
            let sql, result, binds = []
            let start = data.start ? data.start : 0, count = data.count ? data.count : 10
            delete data.start
            delete data.count
            delete data.continue
            let where = ` where 1=1 and status=0 and s_tax= '${mst}' `, pagerobj = inc.getsqlpagerandbind(start, count)
            sql = `${sql_searchBthApproved[`sql_searchBthApproved_${config.dbtype}`]}`
          
            Object.keys(data).forEach(key => {
                let val = data[key]           
                if (val) {
                    if (["fd"].includes(key)) {
                        where += `and ${mailTimeCol} >= ? `
                        binds.push(util.convertDate(moment(val).startOf("day").format(dtf)))
                    }
    
                    else if (["td"].includes(key)) {
                        where += `and ${mailTimeCol} <= ? `
                        binds.push(util.convertDate(moment(val).endOf("day").format(dtf)))
    
                    }
                    // else if (["statusBth"].includes(key)) {
                    //     where += `and status = ? `
                    //     binds.push(val)
    
                    // }
                    // else if (["listinvoice_num"].includes(key)) {
                    //     where += `and listinvoice_num = ? `
                    //     binds.push(val)
    
                    // } 
                    // else if(key != "operation") {
                    //     where += `and ${key}=? `
                    //     binds.push(val)
                    // }
    
                }
            })
            sql += where
            sql += ` order by id `
            sql += pagerobj.vsqlpagerstr
            for (let t of pagerobj.vsqlpagerbind) binds.push(t)
            result = await inc.execsqlselect(sql, binds)
            let databth = { data: result, pos: start }
            if (start == 0) {
                let binds1 = []
                for (let i = 0; i < binds.length - 2;i++) binds1.push(binds[i])
                sql = `select count(id) "total" from s_list_inv ${where} `
                result = await inc.execsqlselect(sql, binds1)
                databth.total_count = result.length > 0 ? result[0].total : 0
            }
    
            return res.json(databth)

        }
        catch (err) {
            next(err)
        }
       
    },
    approvalbth_post: async (req, res, next) => {
        let token = sec.decode(req)
        try {
               const listID = req.body.listC,org = await ous.org(token),mst = token.taxc
               let sql, result,rows,   _affectedRows = 0       ,listinvoice_num=1
                sql = `select id "id",id "inc",doc "doc",listinvoice_num "listinvoice_num" from s_list_inv  where id in (${listID.toString()}) and s_tax=?  and status=0 order by id `          
                result = await inc.execsqlselect(sql, [mst])
                rows = result
               if (rows.length == 0) throw new Error("Không có dữ liệu để ký")
               await inc.checkrevoke(req, res, next)
               if (org.sign == 2) {
                    for (let row of rows) {
                        let doc = util.parseJson(row.doc),id=row.id
                        result = await inc.execsqlselect('select max(listinvoice_num) "seq" from s_list_inv where s_tax=? and period=?', [mst,doc.period])
                        if (result[0].seq) listinvoice_num = Number(result[0].seq) +1
                        if(listinvoice_num>99999)listinvoice_num=1
                        listinvoice_num = listinvoice_num.toString().padStart(5, "0") 
                       
                        
                        doc.type = TYPE_TH
                        if(row.listinvoice_num)doc.listinvoice_num=row.listinvoice_num
                        else   doc.listinvoice_num=listinvoice_num
                      
                        const ca = await sign.ca(mst, util.decrypt(org.pwd))                  
                         let  xml = await util.j2x(doc, row.id)
                        , PATH_XML = await util.getPathXml(TYPE_TH)
                        , signSample = await util.getSignSample(TYPE_TH, doc.idt, row.id)
                        xml = sign.sign123(ca, xml, row.id, PATH_XML, signSample)
                        // result=  await inc.execsqlinsupddel(`update s_list_inv set doc=?, status = 1,approve_date = ?, approve_by = ?,xml=?,listinvoice_num=? where id = ?`, [JSON.stringify(doc),moment().format(config.dtf),token.fn,xml,listinvoice_num, id])
                        // _affectedRows += result
                        // const syslog = { func_id: "approvalbth", action: "Duyệt bảng tổng hơp", dtl: JSON.stringify({id:row.id, listinv: doc}) }JSON
                        // await Log.insLog(req, syslog, next)
                        let paramsDataTVan = { 
                            ids: [row.id],
                            type: "01/TH-HĐĐT"
                        }
                       
                        result=  await inc.execsqlinsupddel(`update s_list_inv set xml=?, status_received = 0 where id = ?`, [xml, id])
                      //  await tvan.dataTvanApi(token, paramsDataTVan, req)
                        result=  await inc.execsqlinsupddel(`update s_list_inv set doc=?, status = 1,approve_date = ?, approve_by = ?,xml=?,listinvoice_num=? where id = ?`, [JSON.stringify(doc),util.convertDate(moment().format(config.dtf)),token.fn,xml,row.listinvoice_num?row.listinvoice_num:listinvoice_num, id])
                        _affectedRows += result
                        await inc.execsqlinsupddel(`INSERT INTO van_einv_listid_msg (pid, type, status_scan) values (?,?,?)`, [id, 'BTH', 0])
                        let sSysLogs = { fnc_id: 'approvalbth', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt bảng tổng hợp: ${row.id}`, msg_id: row.id, doc: JSON.stringify(doc) }
                        logging.ins(req, sSysLogs, next)
                    }
                    return  res.json(_affectedRows)
                }else if (org.sign == 4) {                   
                  
                    let kqs, usr, pwd
                    for (let row of rows) {
                        let doc = JSON.parse(row.doc),id=row.id
                        if(listinvoice_num==1)
                        {
                            result = await inc.execsqlselect('select max(listinvoice_num) "seq" from s_list_inv where s_tax=? and period=?', [mst,doc.period])
                            if (result[0].seq) listinvoice_num = Number(result[0].seq) +1
                        }
                        doc.type = TYPE_TH
                        if(listinvoice_num>99999)listinvoice_num=1
                       // doc.listinvoice_num=listinvoice_num.toString().padStart(5, "0")
                        if(row.listinvoice_num)doc.listinvoice_num=row.listinvoice_num
                        else   doc.listinvoice_num=listinvoice_num.toString().padStart(5, "0")
                        listinvoice_num++
                        if(listinvoice_num>99999)listinvoice_num=1
                        row.doc = doc
                    }     
                    kqs = await hsm.xml(rows, "BTH") 
                    for (let row of kqs) {
                        let doc = row.doc,id=row.id
                        doc.type = TYPE_TH
                       // doc.listinvoice_num=listinvoice_num
                        let xmls = row.xml
                        xmls= xmls.replace('<?xml version="1.0" encoding="UTF-8" standalone="no"?>', '')
                       
                       // result=  await dbs.query(`update s_list_inv set xml=:1 where id = :2`, [xmls, row.id])
                       
                        //await tvan.dataTvanApi(token, paramsDataTVan, req)
                        result=  await inc.execsqlinsupddel(`update s_list_inv set doc=?, status = 1,approve_date = ?, approve_by = ?,xml=?,listinvoice_num=? where id = ?`, [JSON.stringify(doc),new Date(),token.fn,xmls,row.listinvoice_num?row.listinvoice_num:doc.listinvoice_num, id])
                        await inc.execsqlinsupddel(`INSERT INTO van_einv_listid_msg (pid, type, status_scan) values (?,?,?)`, [row.id, 'BTH', 0])
                        _affectedRows += result.rows
                        let sSysLogs = { fnc_id: 'approvalbth', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt bảng tổng hợp: ${row.id}`, msg_id: row.id, doc: JSON.stringify(doc) }
                        logging.ins(req, sSysLogs, next)
                    }
                    return  res.json(listID.toString())
                } else {
                    let arr = []
                    for (const row of rows) {
                        let doc = row.doc,id=row.id
                        doc.type = TYPE_TH
                        result = await inc.execsqlselect('select max(listinvoice_num) "seq" from s_list_inv where s_tax=? and period=?', [mst,doc.period])
                     
                        if (result[0].seq) listinvoice_num = Number(result[0].seq) +1
                        if(listinvoice_num>99999)listinvoice_num=1
                        //doc.listinvoice_num=listinvoice_num.toString().padStart(5, "0")
                        if(row.listinvoice_num)doc.listinvoice_num=row.listinvoice_num
                        else   doc.listinvoice_num=listinvoice_num.toString().padStart(5, "0")
                        listinvoice_num++
                        if(listinvoice_num>99999)listinvoice_num=1
                        const  xml = await util.j2x(doc, id)
                        const PATH_XML = await util.getPathXml(doc.type)
                        const signSample = await util.getSignSample(doc.type, doc.idt, id)
                        // const signSample = await util.getSignSample(doc.type, doc.idt, inc)
                        let  str = xml ,xml_sign_time = signSample.signingTime
                        , xml_end_tag_replace = signSample.xmlEndTagReplace
                        , object_signing_time = `${xml_sign_time}${xml_end_tag_replace}`
                        // Add signature that have signing time to xml
                        str = str.replace(xml_end_tag_replace, object_signing_time)
                        str = str.replace(`<DLBTHop>`,`<DLBTHop Id="${id}">`)
                        // arr.push({
                        //     inc: id,
                        //     idt: doc.idt,
                        //     xml: Buffer.from(xml).toString("base64"),
                        //     ref: PATH_XML,
                        //     signSample
                        // })
                        arr.push({
                            id :id,
                            xml: Buffer.from(str).toString("base64"),
                            reference1:id,                            
                            reference2 :`SigningTime-${signSample.tagBegin}-${id}`,
                            tagSign:"BTHDLieu",
                            tagEnd:"</BTHDLieu>"
                        })
                    }
                    res.json({ mst: mst, date: new Date(), ref: config.PATH_XML, arr: arr }) 
                   
               }
          
        }
        catch (err) {
            console.log("err: ", err)
            next(err)
        }
        
       
    },
    approvalbth_put: async (req, res, next) => {
        try {
            const token = sec.decode(req), mst = token.taxc, body = req.body, incs = body.incs, signs = body.signs,  org = await ous.org(token)
            let sql, result, rows, count = 0
            sql = `select id "id",doc "doc" from s_list_inv  where id in (?) and s_tax=? and status=0`
            result = await inc.execsqlselect(sql, [incs,mst])
            rows = result
            if (rows.length > 0) {
                sql = `update s_list_inv set status=1,xml=? where id=? and status=0`
                for (const row of rows) {
                    const id = row.id
                        , sign = signs.find(item => item.incs === id)
                    if (sign && sign.xml) {
                        const sml = Buffer.from(sign.xml, "base64").toString(), xml = sml 
                        await inc.execsqlinsupddel(sql, [ xml, id])                      
                        count++
                        // const syslog = { func_id: "approvalbth", action: "Duyệt bảng tổng hơp", dtl: JSON.stringify({id:id, listinv: row.doc}) }
                        // await Log.insLog(req, syslog, next)
                        let paramsDataTVan = { 
                            ids: [id],
                            type: "01/TH-HĐĐT"
                        } 
                        await inc.execsqlinsupddel(`INSERT INTO van_einv_listid_msg (pid, type, status_scan) values (?,?,?)`, [id, 'BTH', 0])
                        // await tvan.dataTvanApi(token, paramsDataTVan, req)
                        let sSysLogs = { fnc_id: 'approvalbth', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt bảng tổng hợp: ${id}`, msg_id: id, doc: JSON.stringify(row.doc) }
                        logging.ins(req, sSysLogs, next)
                    }
                }
               
            }
            res.json(count)
        }
        catch (err) {
            next(err)
        }
    },
    SendbackToVanBTH: async (req, res) => {
         try {
            const id = req.params.id
            await inc.execsqlinsupddel(`INSERT INTO van_einv_listid_msg (pid, type, status_scan) values (?,?,?)`, [id, 'BTH', 0])
            res.json({ result: 1 })
        }
        catch (err) {
           console.trace(err)
           res.json({ result: 0 })
        }

    },
    searchDataBth: async (req, res, next) => {
        try {
            const data = req.query
            let result = await Service.searchBth(data, req)
            return res.json(result)

        }
        catch (err) {
            next(err)
        }
    },
    dellbth: async (req, res, next) => {
        try {
            const  id = req.body.id
            let sql,binds,result,sql2, sql3, sql4
            sql = `delete from s_list_inv where id=? and status=0`
            binds = [id]
            sql2 = `update s_list_inv_cancel set listinv_id = null where listinv_id =?`
            sql3 = `update s_inv_adj set listinv_id = null, statuscqt = null where listinv_id =?`
            sql4 = `update s_inv set list_invid = null,error_msg_van=null where id in (select inv_id from s_list_inv_id where id=? and (loai is null or loai <>'BX'))`
            result = await inc.execsqlinsupddel(sql2, binds)
            result = await inc.execsqlinsupddel(sql3, binds)
            result = await inc.execsqlinsupddel(sql4, binds)
            result = await inc.execsqlinsupddel(`update s_inv set list_invid_bx = null where id in (select inv_id from s_list_inv_id where id=? and loai = 'BX')`, binds)
            result = await inc.execsqlinsupddel('update s_list_inv set status=4 where id=? and status=1', binds)

            result = await inc.execsqlinsupddel('delete from s_list_inv_id where id in (select id from s_list_inv where id=? and status=0)', binds)
            result = await inc.execsqlinsupddel(sql, binds)

            res.json(result)
            let sSysLogs = { fnc_id: 'delbth', src: config.SRC_LOGGING_DEFAULT, dtl: `Xoá bảng tổng hợp ${id}`, msg_id: id };
            logging.ins(req, sSysLogs, next)
        }
        catch (err) {
            
            console.log("err: ", err)
            next(err)
        }
    },
    dellbths: async (req, res, next) => {
        try {
            const ids = req.body.id
            for (const id of ids) {
                let sql,binds,result,sql2, sql3, sql4
                sql = `delete from s_list_inv where id=? and status=0`
                binds = [id]
                sql2 = `update s_list_inv_cancel set listinv_id = null where listinv_id =?`
                sql3 = `update s_inv_adj set listinv_id = null, statuscqt = null where listinv_id =?`
                sql4 = `update s_inv set list_invid = null,error_msg_van=null where id in (select inv_id from s_list_inv_id where id=? and (loai is null or loai <>'BX'))`
                result = await inc.execsqlinsupddel(sql2, binds)
                result = await inc.execsqlinsupddel(sql3, binds)
                result = await inc.execsqlinsupddel(sql4, binds)
                result = await inc.execsqlinsupddel(`update s_inv set list_invid_bx = null where id in (select inv_id from s_list_inv_id where id=? and loai = 'BX')`, binds)
                result = await inc.execsqlinsupddel('update s_list_inv set status=4 where id=? and status=1', binds)

                result = await inc.execsqlinsupddel('delete from s_list_inv_id where id in (select id from s_list_inv where id=? and status=0)', binds)
                result = await inc.execsqlinsupddel(sql, binds)

                
                let sSysLogs = { fnc_id: 'delbth', src: config.SRC_LOGGING_DEFAULT, dtl: `Xoá bảng tổng hợp ${id}`, msg_id: id };
                logging.ins(req, sSysLogs, next)
            }
            res.json("Success")
        }
        catch (err) {
            res.json("fail")
            console.log("err: ", err)
            next(err)
        }
    },
    htm: async (req, res, next) => {
        try {
            const token = sec.decode(req), id = req.params.id, body = await rbi(id),
            doc = body
            const words = doc.period.split('/');
            if (doc.period_type == "N") {
                doc.pd = `Ngày ${words[words.length - 3]} tháng ${words[words.length - 2]} năm ${words[words.length - 1]}.`
            }
            else if (doc.period_type == "T") {
                doc.pd = `Tháng ${words[words.length - 2]} năm ${words[words.length - 1]}.`
            }
            else {
                doc.pd = `Quý ${words[words.length - 2]} năm ${words[words.length - 1]}.`
            }
            if (doc.times == "0") {
                doc.times1 = `x`
            } else {
                doc.times2 = doc.times
            }
            var mst = doc.stax
            doc.obj = {}
            mst.split("").map((e, i) => {
                doc.obj[`mst${i}`] = e
            })
            let arr = []
            let _line = 1
            for (let item of doc.items) {
                if ( item.kindinv == 0) item.status = "Mới"
                if ( item.kindinv == 2) item.status = "Điều chỉnh"
                if ( item.kindinv == 3) item.status = "Thay thế"
                if ( item.kindinv == 1) item.status = "Hủy"
                if ( item.kindinv == 4) item.status = "Giải trình"
                if ( item.kindinv == 5) item.status = "Sai sót do tổng hợp"  
                if ( item.kindinv == 3 && !item.adj) item.status = "Mới"
                item.idt = moment(item.idt).format("DD/MM/YYYY")   
                let a = {
                    id: item.id,
                    form: item.form,
                    serial: item.serial,
                    seq: item.seq,
                    idt: item.idt,
                    bname: item.bname,
                    bcode: item.btax,
                    status: item.status,
                    //adjseq:item.adjseq,
                    note: item.note,
                    // name: item.name,
                    // quantity: item.quantity,
                    // amount: item.amountv,
                    vrt: item.vrn,
                    // vat: item.vatv,
                    // total: parseFloat(item.amountv+item.vatv),
                    amount: doc.curr ? (item.amount||0):(item.amountv||0),
                    vat: doc.curr ? (item.vat||0): (item.vatv),
                    total: doc.curr ? parseFloat(item.amount+item.vat):parseFloat(item.amountv+item.vatv),
                    exrt: item.exrt,
                    curr: doc.curr,
                    line:_line++,
                    
                }
                if (item.adj){
                    if (item.adj.seq) a.adjseq = item.adj.seq
                    if (item.adj.note) a.note = item.adj.note
                }    
                arr.push(a)
                doc.listinv = arr
                delete doc.items
            }
            doc.crdt = `Ngày ${new Date(doc.idt).getDate()} tháng ${new Date(doc.idt).getMonth()+1} năm ${new Date(doc.idt).getFullYear()}.`

            const tmp = await util.templatebth(doc)
            let obj = { doc: doc, tmp: tmp }
            let reqjsr = ext.createRequest(obj)
            // res.json({ status: status, doc: doc, tmp: tmp })
            const bodyBuffer = await util.jsReportRenderAttach(reqjsr)
            let sizeInByte = bodyBuffer.byteLength
            res.json({ pdf: `data:application/pdf;base64, ${bodyBuffer.toString('base64')}`, sizePDF: sizeInByte, vdt: body.vdt, status: body.status })
        } catch (err) {
            next(err)
        }
    },
    htmnew: async (req, res, next) => {
        try {
            let doc = req.body
            const words = doc.period.split('/');
            //doc.items = JSON.parse(doc.items)
            if (doc.period_type == "N") {
                doc.pd = `Ngày ${words[words.length - 3]} tháng ${words[words.length - 2]} năm ${words[words.length - 1]}.`
            }
            else if (doc.period_type == "T") {
                doc.pd = `Tháng ${words[words.length - 2]} năm ${words[words.length - 1]}.`
            }
            else {
                doc.pd = `Quý ${words[words.length - 2]} năm ${words[words.length - 1]}.`
            }
            if (doc.f_times == "1") {
                doc.times1 = `x`
            } else {
                doc.times2 = doc.times
            }
            //doc.listinv_num = "Mẫu"
            var mst = doc.stax
            doc.obj = {}
            mst.split("").map((e, i) => {
                doc.obj[`mst${i}`] = e
            })
            let arr = []
            let _line = 1
            for (let item of doc.items) {
                if ( item.kindinv == 0) item.status = "Mới"
                if ( item.kindinv == 1) item.status = "Điều chỉnh"
                if ( item.kindinv == 2) item.status = "Thay thế"
                if ( item.kindinv == 3) item.status = "Hủy"
                if ( item.kindinv == 4) item.status = "Giải trình"
                if ( item.kindinv == 5) item.status = "Sai sót do tổng hợp"      
                if ( item.kindinv == 3 && !item.adj) item.status = "Mới"
                let a = {
                    id: item.id,
                    form: item.form,
                    serial: item.serial,
                    seq: item.seq,
                    idt: item.idt,
                    bname: item.bname,
                    bcode: item.btax,
                    status: item.status,
                    adjseq:item.adjseq,
                    note: item.note,
                    name: item.name,
                    quantity: item.quantity,
                    amount: item.sum,
                    vrt: item.vrn,
                    vat: item.vat,
                    total: item.total,
                    line:_line++,
                    
                }
                arr.push(a)
                doc.listinv = arr
                delete doc.items
            }
            const tmp = await util.templatebth(doc)
            let obj = { doc: doc, tmp: tmp }
            let reqjsr = ext.createRequest(obj)
            // res.json({ status: status, doc: doc, tmp: tmp })
            const bodyBuffer = await util.jsReportRenderAttach(reqjsr)
            let sizeInByte = bodyBuffer.byteLength
            res.json({ pdf: `data:application/pdf;base64, ${bodyBuffer.toString('base64')}`, sizePDF: sizeInByte})
        } catch (err) {
            next(err)
        }
    },
    saveInvAdj: async (req, res, next) => {
        try {
            const body = req.body
            let sql = `insert into s_inv_adj(inv_id,form,serial,stax,type,note,so_tb,ngay_tb,createdate,th_type,seq, curr) values (?,?,?,?,?,?,?,?,?,?,?,?)`
            let binds
            for (let item of body.items) {
                binds = [item.inv_id, item.form, item.serial, item.stax, item.noti_type, item.note, item.so_tb||"", item.ngay_tb? util.convertDate(item.ngay_tb): null,new Date(), item.th_type, item.seq, item.curr]
                //if (!item.insertnew) {
                    //await inc.execsqlinsupddel(`delete from s_inv_adj where inv_id = ? and listinv_id is null`, [item.id])
                //}
                let id = await inc.execsqlReturnIdAfterInsert(sql, binds)
                //await inc.execsqlinsupddel(`update s_inv set inv_adj = ?, th_type = ? where id = ?`, [id,item.th_type,item.inv_id])
                let sSysLogs = { fnc_id: 'bth_saveInvAdj', src: config.SRC_LOGGING_DEFAULT, dtl: `Gửi thông thông tin/ giải trình tổng hợp id: ${id}`, msg_id: id, doc: JSON.stringify(item) };
                logging.ins(req, sSysLogs, next)
            }
            res.json({status: 1})
        }
        catch (err) {
            next(err)
        }
    },
    searchInvAdj: async (req, res, next) => {
        try {
            let id = req.params.id
            let result = await inc.execsqlselect(`select id "id", inv_id "inv_id", form "form", serial "serial", seq "seq", stax "stax", th_type "th_type", type "inv_adj_type", note "note", so_tb "so_tb", ngay_tb "ngay_tb", statuscqt "statuscqt", msgcqt "msgcqt", listinv_id "listinv_id" from s_inv_adj where inv_id = ? order by createdate desc`, [id])
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    hscqt: async (req, res, next) => {
        try {
            const token = sec.decode(req), schema = token.schema, params = req.params, id = params.id
            // let sql = `select htg.datetime_trans,htg.status,htg.description from van_history htg
            //     join s_list_inv sli on htg.id_ref=sli.id 
            //     where htg.type_invoice=1 and htg.id_ref=? order by htg.datetime_trans`
            let sql = sql_history[`sql_history_${config.dbtype}`]
            let binds = [id]
            let result = await inc.execsqlselect(sql, binds)
            res.json(result)
        } catch (err) {
            next(err)
        }
    },
    hscqtXml: async (req, res, next) => {
        try {
            const token = sec.decode(req), schema = token.schema, params = req.params, id = params.id
            let sql =  `select vh.r_xml "r_xml" from van_history vh where id = ?`
            let binds = [id]
            let result = await inc.execsqlselect(sql, binds)
            res.json(result)
        } catch (err) {
            next(err)
        }
    },
    minutesCustom: async (req, res, next) => {
        try {
            const id = req.params.id
            let result = await inc.execsqlselect(`select doc "doc" from s_list_inv where id=?`, [id])
            let row = result[0], doc = JSON.parse(row.doc)
            let header = []
            const words = doc.period.split('/');
            if (doc.period_type == "N") {
                header.push(`Ngày ${words[words.length - 3]} tháng ${words[words.length - 2]} năm ${words[words.length - 1]}.`)
            }
            else if (doc.period_type == "T") {
                header.push(`Tháng ${words[words.length - 2]} năm ${words[words.length - 1]}.`)
            }
            else {
                header.push(`Quý ${words[words.length - 2]} năm ${words[words.length - 1]}.`)
            }
            
            if (doc.times == "0") {
                header.push(`x`)
                header.push(``)
            } else {
                header.push(``)
                header.push(doc.times)
            }
            header.push(doc.sname)
            header.push(doc.stax)
            header.push(doc.listinvoice_num)
            let arr = []
            //console.log(doc.items , "doc.items")
            for (let item of doc.items) {
                if ( item.kindinv == 0) item.status = "Mới"
                if ( item.kindinv == 2) item.status = "Điều chỉnh"
                if ( item.kindinv == 3 && item.adj) item.status = "Thay thế"
                if ( item.kindinv == 1) item.status = "Hủy"
                if ( item.kindinv == 4) item.status = "Giải trình"
                if ( item.kindinv == 5) item.status = "Sai sót do tổng hợp"
                if ( item.kindinv == 3 && !item.adj) item.status = "Mới"
                item.idt = moment(item.idt).format("DD/MM/YYYY")   
                let a = {
                    id: item.id,
                    idbth:id,
                    form: item.form,
                    serial: item.serial,
                    seq: item.seq,
                    idt: item.idt,
                    bname: item.bname,
                    btax: item.btax,
                    bcode: item.bcode,
                    status: item.status,
                    //adjseq:item.adjseq,
                    note: item.note,
                    // name: item.name,
                    // quantity: item.quantity,
                    // amount: item.amountv,
                    vrt: item.vrn,
                    // vat: item.vatv,
                    // total: parseFloat(item.amountv+item.vatv),
                    amount: doc.curr ? (item.amount||0):(item.amountv||0),
                    vat: doc.curr ? (item.vat||0): (item.vatv||0),
                    total: doc.curr ? parseFloat(item.amount+item.vat):parseFloat(item.amountv+item.vatv),
                    line:item.line,
                    formserial: item.form+item.serial,
                    curr: doc.curr,
                    exrt: item.exrt
                }
                if (item.adj){
                    if (item.adj.seq) a.adjseq =item.adj.seq
                    if (item.adj.form && item.adj.serial) a.adjformseial =item.adj.form+item.adj.serial
                    if (item.adj.note) a.note = item.adj.note
                }   
                arr.push(a)
                doc.listinv = arr
                delete doc.items
            }
            let json = {table:doc.listinv, extsh: header, colhds: header}
            const file = path.join(__dirname, "..", "..", "temp/KetXuatExcel_BTH.xlsx")
            const xlsx = fs.readFileSync(file)
            const template = new xlsxtemp(xlsx)
            template.substitute(1, json)
            res.end(template.generate(), "binary")
        } catch (err) {
            next(err)
        }
    },
    delinvadj: async (req, res, next) => {
        try {
            let id = req.body.id, invid = req.body.invid
            await inc.execsqlinsupddel('delete from s_inv_adj where id = ?', [id])
            res.json({status: 1})
            let sSysLogs = { fnc_id: 'bth_delInvAdj', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa điều chỉnh thông tin/ giải trình tổng hợp id: ${id} cho hóa đơn id: ${invid}`, msg_id: invid };
            logging.ins(req, sSysLogs, next)
        } catch (err) {
            next(err)
        }
    }
}

module.exports = Service