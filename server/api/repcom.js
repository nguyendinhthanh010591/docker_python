"use strict"
const path = require("path")
const moment = require("moment")
const fs = require("fs")
const xlsxtemp = require("xlsx-template")
const jwt = require("jsonwebtoken")
const dbs = require("./dbs")
const SEC = require("../sec")
const config = require("../config")
const logger4app = require("../logger4app")



const Service = {

    decode: (req) => {
        let header = req.headers && req.headers.authorization, matches = header ? /^Bearer (\S+)$/.exec(header) : null, token = matches && matches[1]
        return jwt.decode(token)
    },
    getsys: async (req, res, next) => {
        try {
            // const token = SEC.decode(req), uid = token.uid
            const sql = `SELECT name "id", name "value"
          FROM s_cat where type = 'SYSTEM_SCB'`
            const result = await dbs.query(sql)
            res.json(result.recordset)
        }
        catch (err) {
            next(err)
        }
    },
    get: async (req, res, next) => {
        try {
            const query = req.query
            let td = query.td, fd = query.fd, system = query.SYSTEM, fn, binds, sql, result, rows, json
            const fm = moment(query.fd), tm = moment(query.td)
            const fr = fm.format(config.mfd), to = tm.format(config.mfd)
            let systemtow, rowbc = []


            if (system == "*" || system == "EBBS" || system == "C400" || system == "OAF" || system == "SECURE" || system == "OPICs") {
                let whereinv, wherepsgl
                if (system == "*") {
                    whereinv = `and st.trantype in ('EBBS','C400','OAF','SECURE','OPICs')`
                    wherepsgl = `and pr.je_source_id in ('VTNAM-BBS','VTNAM-CRD','VTNAM-OAF','VTNAM-SEC','VTNAM-SEC')`// chờ sys bên khách hàng OPICs 
                } else {
                    whereinv = `and st.trantype = @3`
                    wherepsgl = `and pr.je_source_id = @4`
                }
                if (system == "EBBS") systemtow = "VTNAM-BBS"
                if (system == "C400") systemtow = "VTNAM-CRD"
                if (system == "OAF") systemtow = "VTNAM-OAF"
                if (system == "SECURE") systemtow = "VTNAM-SEC"
                if (system == "OPICs") systemtow = "VTNAM-BBS"// chờ sys bên khách hàng 

                fn = "temp/SCB_SRS.xlsx"
                binds = [fd, td, system, systemtow]
                sql = `SELECT
                x.system "system",
                x.Date "Date",
                x.PSGL_Account "PSGL_Account",
                x.Currency "Currency",
                x.OU "OU",
                Sum(x.Amount_per_Inv) "Amount_per_Inv",
                SUM(x.Amount_per_PSGL) "Amount_per_PSGL"
            FROM
                (
                SELECT
                st.trantype "system",
                FORMAT (st.valueDate,'dd/MM/yyyy') "Date",
                st.acclasscode "PSGL_Account",
                    st.curr "Currency",
                    st.branchcode "OU",
                    SUM(st.amount) "Amount_per_Inv",
                    0 "Amount_per_PSGL"
                FROM
                    s_trans st
                where
                    st.valueDate BETWEEN @1 AND @2
                    ${whereinv}
                    and st.acclasscode like '5%'
                    or st.valueDate BETWEEN @1 AND @2
                    ${whereinv}
                    and st.acclasscode like '3%'
                    or st.valueDate BETWEEN @1 AND @2
                    ${whereinv}
                    and st.acclasscode = '287954'
                    
                group by
                    st.trantype,
                    st.valueDate,
                    st.acclasscode,
                    st.curr,
                    st.branchcode
            UNION ALL
                SELECT
                CASE pr.je_source_id WHEN 'VTNAM-BBS' THEN 'EBBS'
                WHEN 'VTNAM-CRD' THEN 'C400'
                WHEN 'VTNAM-OAF' THEN 'OAF'
                WHEN 'VTNAM-SEC' THEN 'SECURE'
                WHEN 'VTNAM-SEC' THEN 'OPICs' END "system",
                FORMAT (pr.ps_journal_dt,'dd/MM/yyyy') "Date",
                    pr.ps_account_id "PSGL_Account",
                    pr.currency_cd "Currency",
                    pr.ps_operating_unit_id "OU",
                    0 "Amount_per_Inv",
                    SUM(pr.je_line_cr_trans_amt)+ SUM(pr.je_line_dr_trans_amt) "Amount_per_PSGL"
                FROM
                    einv_psgl_reconciliation pr
                where
                    pr.ps_journal_dt BETWEEN @1 AND @2
                    ${wherepsgl}
                    and pr.ps_account_id like '5%'
                    or pr.ps_journal_dt BETWEEN @1 AND @2
                    ${wherepsgl}
                    and pr.ps_account_id like '3%'
                    or pr.ps_journal_dt BETWEEN @1 AND @2
                    ${wherepsgl}
                    and pr.ps_account_id = '287954'
                
                group by
                    pr.je_source_id,
                    pr.ps_journal_dt,
                    pr.ps_account_id,
                    pr.currency_cd,
                    pr.ps_operating_unit_id) x
            group by
                x.system,
                x.Date,
                x.PSGL_Account,
                x.Currency,
                x.OU`
                result = await dbs.query(sql, binds)
                rows = result.recordset
                for (const row of rows) {
                    row.diff = row.Amount_per_Inv + row.Amount_per_PSGL
                    rowbc.push(row)
                }
                // json = { fd: fr, td: to, table: rowbc }
            }
            if (system == "*" || system == "IMEX") {
                let systemimex
                systemimex = "IMEX"
                systemtow = "VTNAM-IMX"
                fn = "temp/SCB_SRS.xlsx"
                binds = [fd, td, systemimex, systemtow]
                sql = `SELECT
                x.system "system",
                x.Date "Date",
                x.PSGL_Account "PSGL_Account",
                x.Currency "Currency",
                x.OU "OU",
                Sum(x.Amount_per_Inv) "Amount_per_Inv",
                Sum(x.exception) "Exception",
                SUM(x.Amount_per_PSGL) "Amount_per_PSGL"
            FROM
                (
                SELECT
                    st.trantype "system",
                    FORMAT (st.valueDate,
                    'dd/MM/yyyy') "Date",
                    st.acclasscode "PSGL_Account",
                    st.curr "Currency",
                    st.branchcode "OU",
                    SUM(st.amount) "Amount_per_Inv",
                    0 "Exception",
                    0 "Amount_per_PSGL"
                FROM
                    s_trans st
                where
                    st.valueDate BETWEEN @1 AND @2
                    and st.trantype = @3
                    and st.acclasscode like '5%'
                    or st.valueDate BETWEEN @1 AND @2
                    and st.trantype = @3
                    and st.acclasscode like '3%'
                    or st.valueDate BETWEEN @1 AND @2
                    and st.trantype = @3
                    and st.acclasscode = '287954'
                    or st.valueDate BETWEEN @1 AND @2
                    and st.trantype = @3
                    and st.acclasscode = '170204'
                    or st.valueDate BETWEEN @1 AND @2
                    and st.trantype = @3
                    and st.acclasscode = '268502'
                group by
                    st.trantype,
                    st.valueDate,
                    st.acclasscode,
                    st.curr,
                    st.branchcode
            UNION ALL
                SELECT
                    CASE pr.je_source_id WHEN 'VTNAM-IMX' THEN 'IMEX' END "system",
                    FORMAT (pr.ps_journal_dt, 'dd/MM/yyyy') "Date",
                    pr.ps_account_id "PSGL_Account",
                    pr.currency_cd "Currency",
                    pr.ps_operating_unit_id "OU",
                    0 "Amount_per_Inv",
                    0 "Exception",
                    SUM(pr.je_line_cr_trans_amt)+ SUM(pr.je_line_dr_trans_amt) "Amount_per_PSGL"
                FROM
                    einv_psgl_reconciliation pr
                where
                    pr.ps_journal_dt BETWEEN @1 AND @2
                    and pr.je_source_id = @4
                    and pr.ps_account_id like '5%' 
                    OR pr.ps_journal_dt BETWEEN @1 AND @2
                    and pr.je_source_id = @4
                    and pr.ps_account_id like '3%'
                    or pr.ps_journal_dt BETWEEN @1 AND @2
                    and pr.je_source_id = @4
                    and pr.ps_account_id = '287954'
                    or pr.ps_journal_dt BETWEEN @1 AND @2
                    and pr.je_source_id = @4
                    and pr.ps_account_id = '170204'
                    or pr.ps_journal_dt BETWEEN @1 AND @2
                    and pr.je_source_id = @4
                    and pr.ps_account_id = '268502'
                group by
                    pr.je_source_id,
                    pr.ps_journal_dt,
                    pr.ps_account_id,
                    pr.currency_cd,
                    pr.ps_operating_unit_id
            UNION ALL
                SELECT
                    st.trantype "system",
                    FORMAT (st.valueDate,
                    'dd/MM/yyyy') "Date",
                    st.acclasscode "PSGL_Account",
                    st.curr "Currency",
                    st.branchcode "OU",
                    0 "Amount_per_Inv",
                    SUM(st.amount) "Exception",
                    0 "Amount_per_PSGL"
                FROM
                    s_trans_exc st
                where
                st.valueDate BETWEEN @1 AND @2
                and st.trantype = @3 
                and st.acclasscode like '5%'
                or st.valueDate BETWEEN @1 AND @2
                and st.trantype = @3 
                and st.acclasscode like '3%'
                or st.valueDate BETWEEN @1 AND @2
                and st.trantype = @3 
                and st.acclasscode = '287954'
                or st.valueDate BETWEEN @1 AND @2
                and st.trantype = @3 
                and st.acclasscode = '170204'
                or st.valueDate BETWEEN @1 AND @2
                and st.trantype = @3 
                and st.acclasscode = '268502'
                group by
                    st.trantype,
                    st.valueDate,
                    st.acclasscode,
                    st.curr,
                    st.branchcode ) x
            group by
                x.system,
                x.Date,
                x.PSGL_Account,
                x.Currency,
                x.OU`
                result = await dbs.query(sql, binds)
                rows = result.recordset
                for (const row of rows) {
                    row.diff = row.Amount_per_Inv + row.Exception + row.Amount_per_PSGL
                    rowbc.push(row)

                }
                // json = { fd: fr, td: to, table: rowbc }
            }
            if (system == "*" || system == "RLS") {
                let  systemrls 
                systemrls = "RLS"
                systemtow = "VTNAM-RLS"
                let sql2, result2, rows2
                fn = "temp/SCB_SRS.xlsx"
                binds = [fd, td, systemrls, systemtow]
                sql = `SELECT
                st.trantype "system",
                FORMAT (st.valueDate,'dd/MM/yyyy') "Date",
                ISNULL(st.acclasscode, 'RLS')  "PSGL_Account",
                st.curr "Currency",
                st.branchcode "OU",
                SUM(st.amount) "Amount_per_Inv",
                0 "Exception",
                0 "Amount_per_PSGL"
            FROM
                s_trans st
            where
                st.valueDate BETWEEN @1 AND @2
                and st.trantype = @3
            group by
                st.trantype,
                st.valueDate,
                st.acclasscode,
                st.curr,
                st.branchcode`
                sql2 = `SELECT
                CASE pr.je_source_id WHEN 'VTNAM-RLS' THEN 'RLS' END "system",
                FORMAT (pr.ps_journal_dt,'dd/MM/yyyy') "Date",
                pr.ps_account_id "PSGL_Account",
                pr.currency_cd "Currency",
                pr.ps_operating_unit_id "OU",
                0 "Amount_per_Inv",
                0 "Exception",
                SUM(pr.je_line_cr_trans_amt)+ SUM(pr.je_line_dr_trans_amt) "Amount_per_PSGL"
            FROM
                einv_psgl_reconciliation pr
            where
                pr.ps_journal_dt BETWEEN @1 AND @2
                and pr.je_source_id = @4
                and pr.ps_account_id like '5%' 
                OR pr.ps_journal_dt BETWEEN @1 AND @2
                and pr.je_source_id = @4
                and pr.ps_account_id like '3%'
                or pr.ps_journal_dt BETWEEN @1 AND @2
                and pr.je_source_id = @4
                and pr.ps_account_id = '287954'
            group by
                pr.je_source_id,
                pr.ps_journal_dt,
                pr.ps_account_id,
                pr.currency_cd,
                pr.ps_operating_unit_id`
                result = await dbs.query(sql, binds)
                rows = result.recordset
                result2 = await dbs.query(sql2, binds)
                rows2 = result2.recordset
                for (const row of rows) {
                    rowbc.push(row)
                }
                for (const row of rows2) {
                    rowbc.push(row)
                }
                // json = { system: system, fd: fr, td: to, table: rowbc }
            }
            json = {fd: fr, td: to, table: rowbc }
            const file = path.join(__dirname, "..", "..", "..", fn)
            const xlsx = fs.readFileSync(file)
            const template = new xlsxtemp(xlsx)
            template.substitute(1, json)
            res.end(template.generate(), "binary")
        }
        catch (err) {
            next(err)
        }
    }


}

module.exports = Service