"use strict"
const moment = require("moment")
const inc = require("./inc")
const config = require("./config")
const logger4app = require("./logger4app")
const logging = require("./logging")
const Service = {
    get: async (req, res, next) => {
        try {
            const query = req.query, filter = JSON.parse(query.filter)
            const sort = query.sort, start = query.start ? query.start : 0, count = query.count ? query.count : 10
            let where = ` where dt between ? and ?`, order, sql, result, ret, i = 2, pagerobj = inc.getsqlpagerandbind(start, count)
            let binds = [new Date(filter.fd), new Date(moment(filter.td).endOf("day"))]
            Object.keys(filter).forEach(key => {
                if (key == "curr") {
                    where += ` and cur=?`
                    binds.push(filter[key])
                    i = 3
                }
            })
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = ` order by ${key} ${sort[key]}`
                })
            }
            else order = " order by dt desc"
            sql = `select id "id",cur "cur",val "val",dt "dt" from s_ex ${where} ${order} ${pagerobj.vsqlpagerstr}`
            for (let t of pagerobj.vsqlpagerbind) binds.push(t)
            result = await inc.execsqlselect(sql, binds)
            ret = { data: result, pos: start }
            if (start == 0) {
                sql = `select count(*) "total" from s_ex ${where}`
                let bindscount = []
                for (let i = 0; i < binds.length - 2; i++) bindscount.push(binds[i])
                result = await inc.execsqlselect(sql, bindscount)
                ret.total_count = result[0].total
            }
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const body = req.body, operation = body.webix_operation
            let binds, sql
            switch (operation) {
                case "update":
                    sql = `update s_ex set val=? where id=?`
                    binds = [body.val, body.id]
                    break
                case "insert":
                    sql = `insert into s_ex (cur,val,dt) values (?,?,?)`
                    binds = [body.cur, body.val, new Date(body.dt)]
                    break
                case "delete":
                    sql = `delete from s_ex where id=?`
                    binds = [body.id]
                    break
                default:
                    throw new Error(`${operation} là hoạt động không hợp lệ \n (${operation} is invalid operation)`)
            }
            const result = await inc.execsqlinsupddel(sql, binds)
            const sSysLogs = { fnc_id: 'exch_post', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật thông tin tỷ giá ngoại tệ ${body.cur} (${operation})`, msg_id: body.id, doc: JSON.stringify(body) };
            logging.ins(req, sSysLogs, next)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    rate: async (req, res, next) => {
        try {
            const query = req.query, cur = query.cur, dt = new Date()
            const sql = `select val "val" from s_ex where cur=? and dt=(select max(dt) from s_ex where cur=? and dt<=?)`
            const result = await inc.execsqlselect(sql, [cur, cur, dt])
            const rows = result
            const val = rows.length>0 ? rows[0].val : 0
            res.json(val)
        }
        catch (err) {
            next(err)
        }
    },
    sync: async (rows,date) => {
        try {
            let sql, binds
            sql = `delete from s_ex where CONVERT(DATETIME, CONVERT(DATE, dt)) = ?`

            binds = [date]
            await dbs.query(sql, binds)
            
            for (let row of rows) {
                sql = `insert into s_ex (cur,val,dt) values (?,?,?)`
                logger4app.debug('ty gia dong :'+ row.CCY + '_'+row.TTM)
                binds = [row.CCY, row.TTM, date]
                await inc.execsqlinsupddel(sql, binds)
            }
        }
        catch (err) {
            logger4app.debug(err.message)
           // next(err)
        }
    }
}
module.exports = Service   