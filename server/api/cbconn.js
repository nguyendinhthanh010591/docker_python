"use strict"
process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
const objectMapper = require("object-mapper")
const moment = require("moment")
const axios = require("axios")
const config = require("./config")
const logger4app = require("./logger4app")
const util = require("./util")
const SEC = require("./sec")
const dbtype = config.dbtype
const dbs = require(`./${dbtype}/dbs`)
const n2w = require("./n2w")
const dtf = config.dtf
const KM = "Khuyến mãi"
const CK = "Chiết khấu"
const MT = "Mô tả"
const KMMT = [KM, MT]
const test_sms_fpt = config.test_sms_fpt
//axios.defaults.headers.common["Authorization"] = config.sms_auth
axios.defaults.headers.get["Content-Type"] = "*/*" //"application/json"

const map_trans_inv = {
    "SID": "ic",
    "AUN": "aun",
    "TYPE": "type",
    "FORM": "form",
    "SERIAL": "serial",
    "SEQ": "seq",
    "SEC": "sec",
    "PAYM": "paym",
    "CURR": "curr",
    "EXRT": "exrt",
    "NOTE": "note",
    "IDT": "idt",
    "STAX": "stax",
    "SUM": "sum",
    "HVATX": "vat",
    "HTOTAL": "total",
    "SUMV": "sumv",
    "VATV": "vatv",
    "TOTALV": "totalv",
    "WORDV": "word",
    "LINE": "items.line",
    "CODE": "items.code",
    "NAME": "items.name",
    "TRANSDATE": "items.transdate",
    "TRANNO": "items.tranno",
    "VRT": "items.vrt",
    "UNIT": "items.unit",
    "QUANTITY": "items.quantity",
    "PRICE": "items.price",
    "AMOUNT": "items.amount",
    "VAT": "items.vat",
    "TOTAL": "items.total",
    "SYSTEMCODE": "systemcode",
    "STATUS": "status",
    "BCODE": "bcode",
    "BNAME": "bname",
    "BUYER": "buyer",
    "BTAX": "btax",
    "BADDR": "baddr",
    "BTEL": "btel",
    "BMAIL": "bmail",
    "BACC": "bacc",
    "BBANK": "bbank",
    "DC0": "c0",
    "DC1": "c1",
    "DC3": "dc3",
    "TRANNO": "c2",
    "TRANSDATE": "c3"
}

const map_trans_org = {
    "name_en": "name_en",
    "TAXFILENO": "taxc",
    "prov": "prov",
    "dist": "dist",
    "ward": "ward",
    "ADDRESS": "addr",
    "MOBI": "tel",
    "EMAIL": "mail",
    "acc": "acc",
    "bank": "bank",
    "status": "status",
    "c0": "c0",
    "c1": "c1",
    "c2": "c2",
    "c3": "c3",
    "c4": "c4",
    "c5": "c5",
    "c6": "c6",
    "c7": "c7",
    "c8": "c8",
    "c9": "c9",
    "CIF": "code",
    "pwd": "pwd",
    "ADDRESS": "fadd",
    "type": "type",
    "fn": "fn",
    "id": "id",
    "CLIENTNAME": "name"
}

const _gettrans = async (tranreq) => {
    return new Promise(async (resolve, reject) => {
        try {
            let result = await dbs.query(`select tran_no "tran_no", module "module", trandate "trandate",result "result" from testtrans where tran_no = :1 and module = :2 and trandate = :3`, [tranreq.tran_no, tranreq.module, tranreq.trandate])
            if (result.rows.length == 0) reject(new Error("Không tìm thấy giao dịch tương ứng \n (Cannot find matching transaction)"))
            let DATA = JSON.parse(result.rows[0].result).DATA

            resolve(DATA)
        } catch (err) {
            reject(err)
        }
    })
}

const _getorg = async (orgreq) => {
    return new Promise(async (resolve, reject) => {
        try {
            const sql = `select id "id",code "CIF",taxc "TAXFILENO",name "CLIENTNAME",name_en "name_en",addr "ADDRESS",prov "prov",dist "dist",ward "ward",mail "EMAIL",tel "MOBI",acc "acc",bank "bank",fadd "fadd",c0 "c0",c1 "c1",c2 "c2",c3 "c3",c4 "c4",c5 "c5",c6 "c6",c7 "c7",c8 "c8",c9 "c9",type "type" from s_org where code = :code`
            let result = await dbs.query(sql, [orgreq.cif_no])
            let DATA = result.rows[0]

            resolve(DATA)
        } catch (err) {
            reject(err)
        }
    })
}

const _getcal = async (tranreq) => {
        try {
            let result = await dbs.query(`UPDATE S_TRANS_CAN
            SET STATUS='1',MA_KS=:1
            WHERE TRAN_NO=:2`, [tranreq.uid,tranreq.tran_no])

        } catch (err) {
            logger4app.debug(err)
        }
}


const gettrans = async (json) => {
    try {
        return { errnum: 0, resdesc: "0904127341:000000;" }
    } catch (err) {
        logger4app.debug(err)
    }
}

const getcif = async (json) => {
    try {
        return { errnum: 0, resdesc: "0904127341:000000;" }
    } catch (err) {
        logger4app.debug(err)
    }
}

const getcal = async (json) => {
    try {
        return { errnum: 0, resdesc: "0904127341:000000;" }
    } catch (err) {
        logger4app.debug(err)
    }
}

const gettrans_vib = async (json) => {
    try {
        // //Gọi hàm lấy giao dịch của VIB
        // let tran_url = String(config.tran_url).replace("{tran_no}", json.tran_no).replace("{module}", json.module).replace("{trandate}", json.trandate)
        // axios.defaults.baseURL = tran_url
        // //Kết thúc gọi hàm lấy giao dịch của VIB
        try {
            // //Gọi hàm lấy giao dịch của VIB
            // const { data } = await axios({ method: "GET" })
            // const [arr] = data.Result.DATA
            // //Kết thúc gọi hàm lấy giao dịch của VIB
            let now = new Date(), NOW = moment(now).format(dtf)
            let result = [], ic = "^_^", obj, items, arr
            if (test_sms_fpt) {
                arr = await _gettrans(json)
            }
            else {
                //Gọi hàm lấy giao dịch của VIB
                let tran_url = String(config.tran_url).replace("{tran_no}", json.tran_no).replace("{module}", json.module).replace("{trandate}", json.trandate)
                axios.defaults.baseURL = tran_url
				axios.defaults.headers.common["Authorization"] = config.tran_auth
                //Kết thúc gọi hàm lấy giao dịch của VIB
                //Gọi hàm lấy giao dịch của VIB
                const { data } = await axios({ method: "GET" })
                arr = data.Result.DATA
                //Kết thúc gọi hàm lấy giao dịch của VIB
            }
            for (const r of [arr]) {
                const row = objectMapper(r, map_trans_inv)
                if (!row.ic) throw new Error("Thiếu mã hóa đơn \n (Missing invoice code)")
                if (ic == row.ic) {
                    items.push(row.items)
                }
                else {
                    if (obj) {
                        obj.items = items
                        result.push(obj)
                    }
                    obj = row
                    items = [row.items]
                    ic = row.ic
                }
                
            }
            if (obj) {
                
                obj.items = items
                  //Chinh: Map lại dữ liệu serial theo des cho vib
                  if (obj.serial) {
                    let sql, data, rows
                    sql = `select serial "serial" from s_serial where des=:1 and status = 1 `
                    data = await dbs.query(sql, [obj.serial])
                    rows = data.rows
                    if (rows.length > 0) {
                        obj.serial = rows[0].serial
                    } else {
                        throw new Error("Dữ liệu serial không tồn tại (Serial data does not exist)")
                    }

                }
                if (obj.idt) {
                    const idt = moment(obj.idt, "YYYY-MM-DD")
                    if (idt.isValid()) {
                        obj.idt = idt.format(dtf)
                        if (idt.toDate() > now) throw new Error("Ngày hđ lớn hơn ngày hiện tại (The invoice date is greater than the current date). ")

                    }
                    else throw new Error("Ngày hđ sai định dạng (Invoice date in wrong format). ")
                } else {
                    obj.idt = NOW
                }

                //Kiểm tra format tran date
                if (obj.c3) {
                    const c3 = moment(obj.c3, "YYYY-MM-DD")
                    if (c3.isValid()) {
                        obj.c3 = c3.format(dtf)
                    }
                    else throw new Error("Ngày giao dịch sai định dạng (Incorrect transaction date). ")
                }

                let curr = obj.curr, exrt = obj.exrt
                if (!util.isNumber(exrt)) {
                    exrt = 1
                    obj.exrt = 1
                }
                if (curr == 'VND' && exrt !== 1) throw new Error("VND tỉ giá phải bằng 1 \n (VND exchange rate must be 1)")
                if (obj.btax && !util.checkmst(obj.btax.toString())) throw new Error("MST không hợp lệ (Invalid tax code). ")
                if (obj.bmail && !util.checkemail(obj.bmail)) throw new Error("Email không hợp lệ (Invalid email). ")
                let i = 1, vat = 0, sum = 0, total = 0, totalv, itemsinv = obj.items
                for (const item of itemsinv) {
                    let erri = ""
                    item.line = i++
                    item.type = item.type ? item.type.toString().toUpperCase() : ""
                    switch (item.type) {
                        case "KM":
                            item.type = KM
                            break
                        case "CK":
                            item.type = CK
                            break
                        case "MT":
                            item.type = MT
                            break
                        default:
                            item.type = ""
                            break
                    }
                    try {
                        item.vrt = Number(item.vrt)
                        if (!util.isNumber(item.vrt)) throw new Error(`Thiếu thuế suất. `)
                    } catch (exp) {

                    }

                    
                    let p = item.price, q = item.quantity, amt = 0, vmt = 0, vrt = 0
  
                    if (KMMT.includes(item.type)) {
                        item.amount = 0
                    }
                    else {
                        try {
                            item.amount = Number(item.amount)
                        } catch (exp) {
    
                        }
                        if (util.isNumber(item.amount)) {
                            amt = item.amount
                            if (item.type == CK) amt = -amt
                            sum += amt

                            vrt = Number(item.vrt)
                            if (vrt > 0) {
                                vmt = amt * vrt / 100
                                vat += vmt
                            }

                        }
                        //else erri += `Thiếu thành tiền.`+` (Missing total amount)`
                    }
                    item.total = amt + vmt

                    if (vrt >= 0) item.vrn = `${vrt}%`
                    else item.vrn = "\\"
                    item.vat = vmt
                    item.vrt = `${vrt}`

                }
                // Duy fix lam tron
                vat = parseFloat(vat.toFixed(2))
                total = sum + vat
                obj.sum = sum
                obj.total = total
                obj.vat=vat
                obj.vatv = Math.round(vat * exrt)
                obj.sumv = Math.round(sum * exrt)
                totalv = Math.round(total * exrt)
                obj.totalv = totalv
                obj.word = n2w.n2w(totalv, curr)
                obj.sourceinv = "COREAPI"
                result.push(obj)
            }
            return result[0]
        } catch (error) {
            throw error
        }
        

    } catch (err) {
        logger4app.error(err)
        throw new Error(`Không tìm thấy dữ liệu giao dịch do điều kiện tìm kiếm không đúng \n (No transaction data found due to incorrect search conditions)`)
    }
}

const getcif_vib = async (json) => {
    try {
        // //Gọi hàm lấy khách hàng của VIB
        // let cif_url = String(config.cif_url).replace("{cifno}", json.cif_no)
        // axios.defaults.baseURL = cif_url
        // //Kết thúc gọi hàm lấy giao dịch của VIB
        try {
            // //Gọi hàm lấy giao dịch của VIB
            // const { data } = await axios({ method: "GET" })
            // const [arr] = data.Result.DATA
            // //Kết thúc gọi hàm lấy khách hàng của VIB
            let result = [], ic = "^_^", obj, items, arr
            if (test_sms_fpt) {
                arr = await _getorg(json)
            } else {
                //Gọi hàm lấy khách hàng của VIB
                let cif_url = String(config.cif_url).replace("{cifno}", json.cif_no)
                axios.defaults.baseURL = cif_url
				axios.defaults.headers.common["Authorization"] = config.cif_auth
                //Kết thúc gọi hàm lấy khách hàng của VIB
                //Gọi hàm lấy khách hàng của VIB
                const { data } = await axios({ method: "GET" })
                arr = data.Result.DATA
                //Kết thúc gọi hàm lấy khách hàng của VIB
            }

            for (const r of [arr]) {
                const row = objectMapper(r, map_trans_org)

                result.push(row)
            }
            return result[0]
        } catch (error) {
            logger4app.debug(error)
        }


    } catch (err) {
        logger4app.debug(err)
    }
}

//cancel

const cancel_vib = async (json) => {
    try {
        const sql = `select id, doc "doc" from s_inv where c2=:1`
        const result = await dbs.query(sql, [json.tran_no])
        const rows = result.rows
        let id = rows[0].id
        let doc = JSON.parse(rows[0].doc)
        doc.cdt = moment(new Date()).format(dtf)
        doc.status = 4
        doc.cancel = { typ: 3, ref: id, rdt: moment(new Date()).format(dtf), rea: 'Huy theo chuc nang huy giao dich GL/FT' }
        let result2 = await dbs.query(`update s_inv set doc=:doc where c2=:1`, [JSON.stringify(doc), json.tran_no])
    }
    catch (err) {
        logger4app.debug(err)
    }
}
//get
const getcal_vib = async(json)=>{
    try {
        // //Gọi hàm lấy khách hàng của VIB
        // let cif_url = String(config.cif_url).replace("{cifno}", json.cif_no)
        // axios.defaults.baseURL = cif_url
        // //Kết thúc gọi hàm lấy giao dịch của VIB
        try {
            // //Gọi hàm lấy giao dịch của VIB
            // const { data } = await axios({ method: "GET" })
            // const [arr] = data.Result.DATA
            // //Kết thúc gọi hàm lấy khách hàng của VIB
            let resdesc = "", errnum = 0, jsonsub
            let api_get = await eval(`gettrans_vib(json)`)
            jsonsub = JSON.stringify(api_get)
            jsonsub = JSON.parse(jsonsub)
            //return -1 là đánh dấu giao dịch hủy thành công
            if (test_sms_fpt) {
                const sql = `select count(*) "count" from s_inv where c2=:1`
                const result = await dbs.query(sql, [json.tran_no])
                const rows = result.rows
                if (rows[0].count != 1) {
                    await _getcal(json)
                    let sql_cancel = 'Delete from TESTTRANS where TRAN_NO =:1'
                    await dbs.query(sql_cancel, [json.tran_no])
                    return -1
                } else {
                    const sql2 = `select status "status" from s_inv where c2=:1`
                    const result2 = await dbs.query(sql2, [json.tran_no])
                    const rows2 = result2.rows
                    logger4app.debug(rows2[0].status)
                    if (rows2[0].status != "3") {
                        await cancel_vib(json)
                        await _getcal(json)
                        return -1
                    } else {
                        await cancel_vib(json)
                        await _getcal(json)
                        return 0
                    }
                }
            } else {
                const sql = `select count(*) "count" from s_inv where c2=:1`
                const result = await dbs.query(sql, [json.tran_no])
                const rows = result.rows
                if (json.module == "GL") {
                    //Gọi hàm lấy khách hàng của VIB
                    let cal_url = String(config.ent_canGL).replace("{branch_no}", jsonsub.dc3)
                    axios.defaults.baseURL = cal_url
					axios.defaults.headers.common["Authorization"] = config.tran_auth
                    //Lấy ở jsonsub kết quả lấy từ api
                    let item = jsonsub.items
                    let arr = []
                    for (let i = 0; i < item.length; i++) {
                        let object = {}
                        object["branch_no"] = jsonsub.dc3
                        object["gl_code"] = jsonsub.c1
                        object["profit_centre"] = jsonsub.systemcode
                        object["seq_no"] = jsonsub.seq
                        object["ccy"] = jsonsub.curr
                        object["amount"] = item[i].total

                        arr.push(object)
                    }
                    logger4app.debug(arr)
                    const dat = {
                        "numberofitem": `${item.length}`,
                        "batchdetailinf": arr
                    }
                    logger4app.debug(JSON.stringify(dat))
                    //Kết thúc gọi hàm lấy khách hàng của VIB
                    //Gọi hàm lấy khách hàng của VIB
                    const { data } = await axios({ method: "POST", data: dat })
                    logger4app.debug(JSON.stringify(data))
                    const statuscode = data.Result.STATUSCODE
                    //Nếu status code khác 000000 thì không thành công => trả về 1, còn lại trả về 0
                    if (rows[0].count >= 1) {
                        errnum = -1
                    } else {
                        const sql2 = `select status "status" from s_inv where c2=:1`
                        const result2 = await dbs.query(sql2, [json.tran_no])
                        const rows2 = result2.rows
                        if (rows2[0].status != "3") {
                            await cancel_vib(json)
                            errnum = -1
                        } else {
                            await cancel_vib(json)
                            errnum = statuscode !== "000000" ? 1 : 0
                        }
                    }

                }
                else if (json.module == "FL") {
                    let cal_url = String(config.ent_canFT).replace("{seqno}", jsonsub.seq)
                    axios.defaults.baseURL = cal_url
					axios.defaults.headers.common["Authorization"] = config.tran_auth

                    let transdate = moment(jsonsub.c3).format("DD/MM/YYYY")
                    const dat = {
                        "module": "FT",
                        "trandate": transdate
                    }
                    logger4app.debug(JSON.stringify(dat))
                    //Kết thúc gọi hàm lấy khách hàng của VIB
                    //Gọi hàm lấy khách hàng của VIB
                    const { data } = await axios({ method: "POST", data: dat })
                    logger4app.debug(JSON.stringify(data))
                    const statuscode = data.Result.STATUSCODE
                    if (rows[0].count != 1) {
                        errnum = -1
                    } else {
                        const sql2 = `select status "status" from s_inv where c2=:1`
                        const result2 = await dbs.query(sql2, [json.tran_no])
                        const rows2 = result.rows
                        if (rows2[0].status != "3") {
                            await cancel_vib(json)
                            errnum = -1
                        } else {
                            await cancel_vib(json)
                            errnum = statuscode !== "000000" ? 1 : 0
                        }
                    }
                }
                return errnum
            }

        } catch (error) {
            logger4app.debug(error)
        }


    } catch (err) {
        logger4app.debug(err)
    }
}

//hienthi
const getcancel = async(json) =>{
    try{
        let result2=[], obj, r
        let result = await dbs.query(`SELECT TRAN_NO "c2", NOTE "note", TOTAL "total", SID "ic", SYSTEMCODE "systemcode", BCODE "bcode", C4 "c4", BRANCHCODE "branchCode", STT, CURR "curr", UC "uc", MA_KS "ma_ks" , TRANSDATE "c3" from S_TRANS_CAN where tran_no = :1 and module = :2 and transdate = :3 and STATUS='0'`, [json.tran_no, json.module, json.trandate])
        
        r=JSON.stringify(result.rows[0])
        r=JSON.parse(r)
        result2.push(r)
        return result2[0]
    }
    catch(err){
        logger4app.debug(err)
    }

}


const Service = {
    gettrans: async (req, res, next) => {
        try {
            let returnmmsg
            const /*token = SEC.decode(req), */query = req.body
            const json = { tran_no: query.tran_no, module: query.module, trandate: query.trandate }
            try {
                returnmmsg = await eval(`gettrans_${config.ent}(json)`)
            } catch (err) {
                throw err
                //returnmmsg = await eval(`gettrans(json)`)
            }
            res.json(returnmmsg)
        } catch (err) {
            next(err)
        }
    },
    getcif: async (req, res, next) => {
        try {
            let returnmmsg
            const /*token = SEC.decode(req), */query = req.body
            const json = { cif_no: query.cif_no }
            try {
                returnmmsg = await eval(`getcif_${config.ent}(json)`)
            } catch (err) {
                returnmmsg = await eval(`getcif(json)`)
            }
            res.json(returnmmsg)
        } catch (err) {
            next(err)
        }
    },
    getcel: async(req,res, next) =>{
        try {
            let returnmmsg
            const /*token = SEC.decode(req), */query = req.body
            const json = { tran_no: query.tran_no, module: query.module, trandate: query.trandate }
            logger4app.debug(query)
            try {
                returnmmsg = await eval(`getcancel(json)`)
            } catch (err) {
                next(err)
            }
            res.json(returnmmsg)
        } catch (err) {
            next(err)
        }
    },
    getcal: async(req, res, next) =>{
        try {
            let returnmmsg
            const token = SEC.decode(req), query = req.body, uid = token.uid
            const json = { tran_no: query.tran_no, module: query.module, trandate: query.trandate, uid:uid }
            try {
                returnmmsg = await eval(`getcal_${config.ent}(json)`)
            } catch (err) {
                returnmmsg = await eval(`getcal(json)`)
            }
            res.json(returnmmsg)
        } catch (err) {
            next(err)
        }
    }
}
module.exports = Service