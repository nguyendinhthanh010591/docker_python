"use strict"
const dom = require('xmldom').DOMParser
const config = require("./config")
const logger4app = require("./logger4app")
const dbtype = config.dbtype
const moment = require('moment')
const xpath = require('xpath')
const objectMapper = require("object-mapper")
const SEC = require("./sec")
const sign = require("./sign")

const util = require(`./util`)

// const cons = require('./conf/cons')

// const fcloud = require("./fcloud")
// const spdf = require("./spdf")
// const hbs = require("./hbs")
// const redis = require("./redis")
const dtf = config.dtf
const mfd = config.mfd
const dbs = require(`./${dbtype}/dbs`)
const ous = require(`./${dbtype}/ous`)
const ext = require("./ext")
const path = require('path')
// const user = require(`./${dbtype}/user`)
// const inv = require(`./${dbtype}/inv`)
// const cat = require(`./${dbtype}/cat`)

const sql_rbi = {
    sql_rbi_mssql: `select doc "doc" from b_inv where id=@1`,
    sql_rbi_mysql: `select doc "doc" from b_inv where id=?`,
    sql_rbi_orcl: `select doc "doc" from b_inv where id=:id`,
    sql_rbi_pgsql: `select doc "doc" from b_inv where id=$id`
}
const sql_view = {
    sql_view_mssql: `select doc "doc" from b_inv where id=@1`,
    sql_view_mysql: `select doc "doc" from b_inv where id=?`,
    sql_view_orcl: `select doc "doc" from b_inv where id=:id`,
    sql_view_pgsql: `select doc "doc" from b_inv where id=$id`
}

const sql_upload = {
    sql_upload_mssql: `insert into b_inv (doc, xml) values (@1,@2)`,
    sql_upload_mysql: `insert into b_inv (doc, xml) values (?,?)`,
    sql_upload_orcl: `insert into b_inv (doc, xml) values (:1,:2)`,
    sql_upload_pgsql: `insert into b_inv (doc, xml) values ($1,$2)`
}

const sql_upload_xml = {
    sql_upload_mssql: `insert into b_inv (doc, xml) values (@1,@2)`,
    sql_upload_mysql: `insert into b_inv (doc, xml) values (?,?)`,
    sql_upload_orcl: `insert into b_inv (doc, xml) values (:1,:2)`,
    sql_upload_pgsql: `insert into b_inv (doc, xml) values ($1,$2)`
}

const sql_put = {
    sql_put_mssql: `update b_inv set doc=@1 where id=@2`,
    sql_put_mysql: `update b_inv set doc=? where id=?`,
    sql_put_orcl: `update b_inv set doc=:1 where id=:2`,
    sql_put_pgsql: `update b_inv set doc=$1 where id=$2`
}

const sql_post = {
    sql_post_mssql: `insert into b_inv (doc) values (@1)`,
    sql_post_mysql: `insert into b_inv (doc) values (?)`,
    sql_post_orcl: `insert into b_inv (doc) values (:1)`,
    sql_post_pgsql: `insert into b_inv (doc) values ($1)`
}

const execsqlselect = (sql, binds) => {
    return new Promise(async (resolve, reject) => {
        try {
            const result = await dbs.query(sql, binds)
            let rows
            switch (dbtype) {
                case "mssql":
                    rows = result.recordset
                    break
                case "mysql":
                    rows = result[0]
                    break
                case "orcl":
                    rows = result.rows
                    break
                case "pgsql":
                    rows = result.rows
                    break
            }
            resolve(rows)
        } catch (err) {
            reject(err)
        }
    })
}

const ONPREM = {
    "TTChung.SHDon": "seq",
    "TTChung.TDLap": "idt",
    "TTChung.DVTTe": "curr",
    "TTChung.TTKhac.TTin": "ttkhac[]",
    // "TTChung.TTKhac.sec": "sec",
    // "TTChung.TTKhac.exrt": "exrt",
    // "TTChung.TTKhac.note": "note",
    // "TTChung.TTKhac.paym": "paym",
    // "TTChung.TTKhac.type": "type",
    // // "TTChung.TTKhac.form": "form",
    // "TTChung.TTKhac.serial": "serial",
    // "TTChung.TTKhac.adj.des": "adj.des",
    // "TTChung.TTKhac.adj.idt": "adj.idt",
    // "TTChung.TTKhac.adj.rdt": "adj.rdt",
    // "TTChung.TTKhac.adj.rea": "adj.rea",
    // "TTChung.TTKhac.adj.ref": "adj.ref",
    // "TTChung.TTKhac.adj.seq": "adj.seq",
    // "TTChung.TTKhac.adj.typ": "adj.typ",
    // "TTChung.TTKhac.tax[].amt": "tax[].amt",
    // "TTChung.TTKhac.tax[].amtv": "tax[].amtv",
    // "TTChung.TTKhac.tax[].vrn": "tax[].vrn",
    // "TTChung.TTKhac.tax[].vrt": "tax[].vrt",
    // "TTChung.TTKhac.sec": "sec",
    "TTChung.KHMSHDon": "form",
    "TTChung.KHHDon": "serial",
    "TTChung.THDon": "name",
    "TTChung.TGia": "exrt",
    "TTChung.MTCuu": "sec",
    "NDHDon.NBan.Ten": "sname",
    "NDHDon.NBan.MST": "stax",
    "NDHDon.NBan.DChi": "saddr",
    //"NDHDon.NBan.TTKhac.Tel": "stel",
    //"NDHDon.NBan.TTKhac.Mail": "smail",
    //"NDHDon.NBan.TTKhac.Acc": "sacc",
    //"NDHDon.NBan.TTKhac.Bank": "sbank",
    "NDHDon.NBan.HDKTSo": "cenumber",
    "NDHDon.NBan.HDKTNgay": "cedate",
    "NDHDon.NBan.LDDNBo": "ordno",
    "NDHDon.NBan.SDThoai": "stel",
    "NDHDon.NBan.DCTDTu": "smail",
    "NDHDon.NBan.STKNHang": "sacc",
    "NDHDon.NBan.TNHang": "sbank",
    "NDHDon.NBan.DCKXHang": "whsfr",
    "NDHDon.NBan.HDSo": "contr",
    "NDHDon.NBan.TNVChuyen": "trans",
    "NDHDon.NBan.PTVChuyen": "vehic",
    "NDHDon.NMua.Ten": "bname",
    "NDHDon.NMua.MST": "btax",
    "NDHDon.NMua.DChi": "baddr",
    "NDHDon.NMua.MKHang": "bcode",
    "NDHDon.NMua.SDThoai": "btel",
    "NDHDon.NMua.DCTDTu": "bmail",
    "NDHDon.NMua.HVTNMHang": "buyer",
    "NDHDon.NMua.STKNHang": "bacc",
    "NDHDon.NMua.TNHang": "bbank",
    // "NDHDon.NMua.TTKhac.Tel": "btel",
    // "NDHDon.NMua.TTKhac.Mail": "bmail",
    // "NDHDon.NMua.TTKhac.Acc": "bacc",
    // "NDHDon.NMua.TTKhac.Bank": "bbank",
    // "NDHDon.NMua.TTKhac.Buyer": "buyer",
    "NDHDon.TToan.TgTCThue": "sum",
    "NDHDon.TToan.TgTThue": "vat",
    "NDHDon.TToan.TgTTTBSo": "total",
    "NDHDon.TToan.TTCKTMai": "discountv",
    "NDHDon.TToan.TgTTTBChu": "word",
    "NDHDon.TToan.THTTLTSuat.LTSuat[].TCDChinh": "status",
    "NDHDon.TToan.THTTLTSuat.LTSuat[].TSuat": "vrt",
    "NDHDon.TToan.THTTLTSuat.LTSuat[].ThTien": "amt",
    "NDHDon.TToan.THTTLTSuat.LTSuat[].TThue": "vat",
    "NDHDon.DSHHDVu.HHDVu[].STT": "items[].line",
    "NDHDon.DSHHDVu.HHDVu[].TChat": "items[].type",
    "NDHDon.DSHHDVu.HHDVu[].TCDChinh": "items[].status",
    "NDHDon.DSHHDVu.HHDVu[].TLCKhau": "items[].perdiscount",
    "NDHDon.DSHHDVu.HHDVu[].STCKhau": "items[].amtdiscount",
    "NDHDon.DSHHDVu.HHDVu[].TSuat": "items[].vrn",
    "NDHDon.DSHHDVu.HHDVu[].MHHDVu": "items[].code",
    "NDHDon.DSHHDVu.HHDVu[].THHDVu": "items[].name",
    "NDHDon.DSHHDVu.HHDVu[].DVTinh": "items[].unit",
    "NDHDon.DSHHDVu.HHDVu[].DGia": "items[].price",
    "NDHDon.DSHHDVu.HHDVu[].SLuong": "items[].quantity",
    "NDHDon.DSHHDVu.HHDVu[].ThTien": "items[].amount",
    "NDHDon.DSHHDVu.HHDVu[].TTKhac.TTin": "items[].ttkhac[]",
    // "NDHDon.DSHHDVu.HHDVu[].SoDu": "items[].c2",
    // "NDHDon.DSHHDVu.HHDVu[].IDT": "items[].c0",
    // "NDHDon.DSHHDVu.HHDVu[].VAT": "items[].vat",
    "NDHDon.DSHHDVu.HHDVu[].TOTAL": "items[].total"
}
function round(v) {
    return (v >= 0 || -1) * Math.round(Math.abs(v));
}
const mapvrt = (vrt) => {
    if (vrt == "0%") return 0
    else if (vrt == "5%") return 5
    else if (vrt == "8%") return 8
    else if (vrt == "10%") return 10
    else if (vrt == "KCT") return -1
    else if (vrt == "KKKNT") return -2
    else return vrt
}

const mapvrt2 = (vrt) => {
    if (vrt == "0%") return "0"
    else if (vrt == "5%") return "5"
    else if (vrt == "8%") return "8"
    else if (vrt == "10%") return "10"
    else if (vrt == "KCT") return "-1"
    else if (vrt == "KKKNT") return "-2"
    else return "" + vrt + ""
}

const mapvrn = (vrt) => {
    if (vrt == "0%" || vrt == "5%" || vrt == "8%" || vrt == "10%" || vrt == "KHAC") return vrt
    else return vrt + "%"
}

const mapvrn2 = (vrt) => {
    if (vrt == "0" || vrt == "5%" || vrt == "8%" || vrt == "10%" || vrt == "KHAC") return vrt
    else if (vrt == "KCT") return "Không chịu thuế"
    else if (vrt == "KKKNT") return "Không kê khai nộp thuế"
    else return vrt + "%"
}
const MAP_HDR = {
    "TTChung.TTKhac.taxo": "taxo" ,
    "TTChung.TTKhac.cancel": "cancel",
    "TTChung.TTKhac.adj": "adj",
    "TTChung.TTKhac.dif": "dif",
    "TTChung.TTKhac.tax": "tax",
    "TTChung.TTKhac.inc": "inc",
    "TTChung.TTKhac.id": "id",
    "TTChung.TTKhac.pid": "pid",
    "TTChung.TTKhac.sec":"sec" ,
    "TTChung.TTKhac.exrt": "exrt",
    "TTChung.TTKhac.note": "note",
    "TTChung.TTKhac.paym": "paym",
    "TTChung.TTKhac.type": "type",
    "TTChung.TTKhac.form":"form"  ,
    "TTChung.TTKhac.serial": "serial",
    "TTChung.TTKhac.ou":  "ou",
    "TTChung.TTKhac.uc": "uc",
    "TTChung.THDon": "name",
    "TTChung.So": "seq",
    "TTChung.TDLap": "idt",
    "TTChung.DVTTe": "curr",
    //"TTChung.MTCuu": "sec",
    "NDHDon.NBan.Ten": "sname",
    "NDHDon.NBan.MST": "stax",
    "NDHDon.NBan.DChi": "saddr",
    "NDHDon.NBan.TTKhac.Tel": "stel",
    "NDHDon.NBan.TTKhac.Mail": "smail",
    "NDHDon.NBan.TTKhac.Acc": "sacc",
    "NDHDon.NBan.TTKhac.Bank": "sbank",
    "NDHDon.NMua.Ten": "bname",
    "NDHDon.NMua.MST": "btax",
    "NDHDon.NMua.DChi": "baddr",
    "NDHDon.NMua.TTKhac.Tel": "btel",
    "NDHDon.NMua.TTKhac.Mail": "bmail",
    "NDHDon.NMua.TTKhac.Acc": "bacc" ,
    "NDHDon.NMua.TTKhac.Bank": "bbank",
    "NDHDon.NMua.TTKhac.Buyer": "buyer",
    "NDHDon.TToan.TTKhac.discount": "discount",
    "NDHDon.TToan.TTKhac.sum": "sum",
    "NDHDon.TToan.TgTCThue": "sumv" ,
    "NDHDon.TToan.TTKhac.vat":"vat" ,
    "NDHDon.TToan.TgTThue": "vatv",
    "NDHDon.TToan.TTKhac.total": "total",
    "NDHDon.TToan.TgTTTBSo":  "totalv",
    "NDHDon.TToan.TgTTTBChu":"word",
    "NDHDon.DSHHDVu.HHDVu[].STT":"items[].line",
    "NDHDon.DSHHDVu.HHDVu[].LHHDVu":"items[].type",
    "NDHDon.DSHHDVu.HHDVu[].Ten":"items[].name",
    "NDHDon.DSHHDVu.HHDVu[].DVTinh":"items[].unit",
    "NDHDon.DSHHDVu.HHDVu[].DGia":"items[].price",
    "NDHDon.DSHHDVu.HHDVu[].SLuong":"items[].quantity",
    "NDHDon.DSHHDVu.HHDVu[].TSuat":"items[].vrt",
    "NDHDon.DSHHDVu.HHDVu[].ThTien":"items[].amount"
}
const FPT = {
    "TTChung.SHDon": "seq",
    "TTChung.TDLap": "idt",
    "TTChung.DVTTe": "curr",
    "TTChung.TTKhac.TTin": "ttkhac[]",
    // "TTChung.TTKhac.sec": "sec",
    // "TTChung.TTKhac.exrt": "exrt",
    // "TTChung.TTKhac.note": "note",
    // "TTChung.TTKhac.paym": "paym",
    // "TTChung.TTKhac.type": "type",
    // // "TTChung.TTKhac.form": "form",
    // "TTChung.TTKhac.serial": "serial",
    // "TTChung.TTKhac.adj.des": "adj.des",
    // "TTChung.TTKhac.adj.idt": "adj.idt",
    // "TTChung.TTKhac.adj.rdt": "adj.rdt",
    // "TTChung.TTKhac.adj.rea": "adj.rea",
    // "TTChung.TTKhac.adj.ref": "adj.ref",
    // "TTChung.TTKhac.adj.seq": "adj.seq",
    // "TTChung.TTKhac.adj.typ": "adj.typ",
    // "TTChung.TTKhac.tax[].amt": "tax[].amt",
    // "TTChung.TTKhac.tax[].amtv": "tax[].amtv",
    // "TTChung.TTKhac.tax[].vrn": "tax[].vrn",
    // "TTChung.TTKhac.tax[].vrt": "tax[].vrt",
    // "TTChung.TTKhac.sec": "sec",
    "TTChung.KHMSHDon": "form",
    "TTChung.KHHDon": "serial",
    "TTChung.THDon": "name",
    "TTChung.TGia": "exrt",
    "NDHDon.NBan.Ten": "sname",
    "NDHDon.NBan.MST": "stax",
    "NDHDon.NBan.DChi": "saddr",
    //"NDHDon.NBan.TTKhac.Tel": "stel",
    //"NDHDon.NBan.TTKhac.Mail": "smail",
    //"NDHDon.NBan.TTKhac.Acc": "sacc",
    //"NDHDon.NBan.TTKhac.Bank": "sbank",
    "NDHDon.NBan.HDKTSo": "cenumber",
    "NDHDon.NBan.HDKTNgay": "cedate",
    "NDHDon.NBan.LDDNBo": "ordno",
    "NDHDon.NBan.SDThoai": "stel",
    "NDHDon.NBan.DCTDTu": "smail",
    "NDHDon.NBan.STKNHang": "sacc",
    "NDHDon.NBan.TNHang": "sbank",
    "NDHDon.NBan.DCKXHang": "whsfr",
    "NDHDon.NBan.HDSo": "contr",
    "NDHDon.NBan.TNVChuyen": "trans",
    "NDHDon.NBan.PTVChuyen": "vehic",
    "NDHDon.NMua.Ten": "bname",
    "NDHDon.NMua.MST": "btax",
    "NDHDon.NMua.DChi": "baddr",
    "NDHDon.NMua.MKHang": "bcode",
    "NDHDon.NMua.SDThoai": "btel",
    "NDHDon.NMua.DCTDTu": "bmail",
    "NDHDon.NMua.HVTNMHang": "buyer",
    "NDHDon.NMua.STKNHang": "bacc",
    "NDHDon.NMua.TNHang": "bbank",
    // "NDHDon.NMua.TTKhac.Tel": "btel",
    // "NDHDon.NMua.TTKhac.Mail": "bmail",
    // "NDHDon.NMua.TTKhac.Acc": "bacc",
    // "NDHDon.NMua.TTKhac.Bank": "bbank",
    // "NDHDon.NMua.TTKhac.Buyer": "buyer",
    "NDHDon.TToan.TgTCThue": "sum",
    "NDHDon.TToan.TgTThue": "vat",
    "NDHDon.TToan.TgTTTBSo": "total",
    "NDHDon.TToan.TTCKTMai": "discountv",
    "NDHDon.TToan.TgTTTBChu": "word",
    "NDHDon.TToan.THTTLTSuat.LTSuat[].TCDChinh": "status",
    "NDHDon.TToan.THTTLTSuat.LTSuat[].TSuat": "vrt",
    "NDHDon.TToan.THTTLTSuat.LTSuat[].ThTien": "amt",
    "NDHDon.TToan.THTTLTSuat.LTSuat[].TThue": "vat",
    "NDHDon.DSHHDVu.HHDVu[].STT": "items[].line",
    "NDHDon.DSHHDVu.HHDVu[].TChat": "items[].type",
    "NDHDon.DSHHDVu.HHDVu[].TCDChinh": "items[].status",
    "NDHDon.DSHHDVu.HHDVu[].TLCKhau": "items[].perdiscount",
    "NDHDon.DSHHDVu.HHDVu[].STCKhau": "items[].amtdiscount",
    "NDHDon.DSHHDVu.HHDVu[].TSuat": "items[].vrn",
    "NDHDon.DSHHDVu.HHDVu[].MHHDVu": "items[].code",
    "NDHDon.DSHHDVu.HHDVu[].THHDVu": "items[].name",
    "NDHDon.DSHHDVu.HHDVu[].DVTinh": "items[].unit",
    "NDHDon.DSHHDVu.HHDVu[].DGia": "items[].price",
    "NDHDon.DSHHDVu.HHDVu[].SLuong": "items[].quantity",
    "NDHDon.DSHHDVu.HHDVu[].ThTien": "items[].amount",
    "NDHDon.DSHHDVu.HHDVu[].TTKhac.TTin": "items[].ttkhac[]",
    // "NDHDon.DSHHDVu.HHDVu[].SoDu": "items[].c2",
    // "NDHDon.DSHHDVu.HHDVu[].IDT": "items[].c0",
    // "NDHDon.DSHHDVu.HHDVu[].VAT": "items[].vat",
    "NDHDon.DSHHDVu.HHDVu[].TOTAL": "items[].total"

}
const MINV = {
    "TenHoaDon": "name",
    "MauSo": "form",
    "Kyhieu": "serial",
    "SoHoaDon": "seq",
    "NgayHoaDon": "idt",
    "HinhThucThanhToan": "paym",
    "MaTienTe": "curr",
    "TyGia": "exrt",
    "TenNguoiBan": "sname",
    "MaSoThueNguoiBan": "stax",
    "DiaChiNguoiBan": "saddr",
    "DienThoaiNguoiBan": "stel",
    "TaiKhoanBenBan": "sacc",
    "NganHangBenBan": "sbank",
    "TenNguoiMua": "buyer",
    "TenDoanhNghiepMua": "bname",
    "MaSoThueNguoiMua": "btax",
    "DiaChiNguoiMua": "baddr",
    "TaiKhoanBenMua": "bacc",
    "NganHangBenMua": "bbank",
    "TongTienTruocThue": "sumv",
    "TongTienChietKhau": "discountv",
    "TongTienThue": "vatv",
    "TongTien": "totalv",
    "ThanhTienBangChu": "word",
    "ChiTiet[].stt": "items[].line",
    "ChiTiet[].TenHang": "items[].name",
    "ChiTiet[].TenDvt": "items[].unit",
    "ChiTiet[].SoLuong": "items[].quantity",
    "ChiTiet[].DonGia": "items[].price",
    "ChiTiet[].TienTruocThue": "items[].amount",
    "ChiTiet[].PhanTramThue": "items[].vrt",
    "ChiTiet[].TienThue": "items[].vat",
    "ChiTiet[].ThanhTien": "items[].total"
}
const NEWI = {
    "InvoiceTypeName": "name",
    "InvoiceForm": "form",
    "InvoiceSerial": "serial",
    "InvoiceNo": "seq",
    "InvoiceDate": { "key": "idt", "transform": (val) => { return moment(val.substring(0, 10), mfd).format(dtf) } },
    "CurrencyID": "curr",
    "ExchangeRate": "exrt",
    "SumItemAmount": "sumv",
    "SumTaxAmount": "vatv",
    "SumDiscountAmount": "discountv",
    "SumPaymentAmount": "totalv",
    "Note": "note",
    "BuyerName": "buyer",
    "BuyerTaxCode": "btax",
    "BuyerUnitName": "bname",
    "BuyerAddress": "baddr",
    "BuyerBankAccount": "bacc",
    "PayMethodName": "paym",
    "SellerName": "sname",
    "SellerTaxCode": "stax",
    "SellerAddress": "saddr",
    "SellerMobile": "stel",
    "SellerBankAccount": "sacc",
    "SellerBankName": "sbank",
    "InvoiceDetails.InvoiceDetails[].ItemName": "items[].name",
    "InvoiceDetails.InvoiceDetails[].UnitName": "items[].unit",
    "InvoiceDetails.InvoiceDetails[].Qty": "items[].quantity",
    "InvoiceDetails.InvoiceDetails[].Price": "items[].price",
    "InvoiceDetails.InvoiceDetails[].Amount": "items[].amount",
    "InvoiceDetails.InvoiceDetails[].TaxAmount": "items[].vat"
}

const FAST = {
    "InvoiceDate": { "key": "idt", "transform": (val) => { return moment(val, mfd).format(dtf) } },
    "InvoiceName": "name",
    "InvoiceType": "type",
    "InvoicePattern": "form",
    "InvoiceSerial": "serial",
    "InvoiceNumber": "seq",
    "PaymentMethod": "paym",
    "CompanyName": "sname",
    "CompanyTaxCode": "stax",
    "CompanyAddress": "saddr",
    "CompanyPhone": "stel",
    "CompanyEmail": "smail",
    "CompanyBankAccount": "sacc",
    "CompanyBankName": "sbank",
    "Buyer": "buyer",
    "CustomerCode": "bcode",
    "CustomerTaxCode": "btax",
    "CustomerName": "bname",
    "CustomerAddress": "baddr",
    "CustomerPhone": "btel",
    "EmailDeliver": "bmail",
    "CustomerBankAccount": "bacc",
    "CustomerBankName": "bbank",
    "CurrencyUnit": "curr",
    "ExchangRate": "exrt",
    "Amount": "sumv",
    "TaxAmount": "vatv",
    "TotalAmount": "totalv",
    "AmountInWords": "word",
    "DiscountAmount": "discountv",
    "PromotionAmount": "promotionv",
    "Note": "note",
    "Products.Product[].LineNumber": "items[].line",
    "Products.Product[].ItemName": "items[].name",
    "Products.Product[].UnitOfMeasure": "items[].unit",
    "Products.Product[].Quantity": "items[].quantity",
    "Products.Product[].Price": "items[].price",
    "Products.Product[].Amount": "items[].amount",
    "Products.Product[].TaxRate": "items[].vrt",
    "Products.Product[].TaxAmount": "items[].vat",
}


const VNPT = {
    "ArisingDate": { "key": "idt", "transform": (val) => { return moment(val, mfd).format(dtf) } },
    "InvoiceName": "name",
    "InvoicePattern": "form",
    "SerialNo": "serial",
    "InvoiceNo": { "key": "seq", "transform": (val) => { return val.toString().padStart(config.SEQ_LEN, "0") } },
    "Kind_of_Payment": "paym",
    "PaymentMethod": "paym",
    "ComName": "sname",
    "ComTaxCode": "stax",
    "ComAddress": "saddr",
    "ComPhone": "stel",
    "ComEmail": "smail",
    "ComBankNo": "sacc",
    "ComBankName": "sbank",
    "Buyer": "buyer",
    "CusCode": "bcode",
    "CusName": "bname",
    "CusTaxCode": "btax",
    "CusAddress": "baddr",
    "CusPhone": "btel",
    "CusEmail": "bmail",
    "CusEmails": "bmail",
    "CusBankNo": "bacc",
    "CusBankName": "bbank",
    "CurrencyUnit": "curr",
    "ExchangeRate": "exrt",
    "Note": "note",
    "Total": "sumv",
    "VAT_Amount": "vatv",
    "VATAmount": "vatv",
    "Amount": "totalv",
    "Amount_words": "word",
    "AmountInWords": "word",
    "VAT_Rate": "vrt",
    "VATRate": "vrt",
    "Discount_Amount": "discountv",
    "DiscountAmount": "discountv",
    "Products.Product[].Pos": "items[].line",
    "Products.Product[].ProdName": "items[].name",
    "Products.Product[].ProdUnit": "items[].unit",
    "Products.Product[].ProdQuantity": "items[].quantity",
    "Products.Product[].ProdPrice": "items[].price",
    "Products.Product[].Amount": "items[].amount",
    "Products.Product[].VATRate": "items[].vrt",
    "Products.Product[].VATAmount": "items[].vat",
    "Products.Product[].Total": "items[].total",
    //"Products.Product[].IsSum": "items[].issum",
    //"Products.Product[].Code": "items[].code",
    //"Products.Product[].Remark": "items[].remark",
}

const BKAV = {
    "invoiceType": "type",
    "templateCode": "form",
    "invoiceSeries": "serial",
    "invoiceNumber": "seq",
    "invoiceName": "name",
    "invoiceIssuedDate": { "key": "idt", "transform": (val) => { return moment(val).format(dtf) } },
    "paymentMethodName": "paym",
    "payments.payment.paymentMethodName": "paym",
    "currencyCode": "curr",
    "exchangeRate": "exrt",
    "invoiceNote": "note",
    "adjustmentType": "adjustmenttype",
    "originalInvoice": "originalInvoice",
    "sellerLegalName": "sname",
    "sellerTaxCode": "stax",
    "sellerAddressLine": "saddr",
    "sellerPhoneNumber": "stel",
    "sellerEmail": "smail",
    "sellerBankName": "sbank",
    "sellerBankAccount": "sacc",
    "buyerCode": "bcode",
    "buyerDisplayName": "buyer",
    "buyerLegalName": "bname",
    "buyerTaxCode": "btax",
    "buyerAddressLine": "baddr",
    "buyerPhoneNumber": "btel",
    "buyerEmail": "bmail",
    "buyerBankName": "bbank",
    "buyerBankAccount": "bacc",
    "seller.sellerLegalName": "sname",
    "seller.sellerTaxCode": "stax",
    "seller.sellerAddressLine": "sadd",
    "seller.sellerPhoneNumber": "stel",
    "seller.sellerEmail": "smail",
    "seller.sellerBankName": "sbank",
    "seller.sellerBankAccount": "sacc",
    "buyer.buyerCode": "bcode",
    "buyer.buyerDisplayName": "buyer",
    "buyer.buyerLegalName": "bname",
    "buyer.buyerTaxCode": "btax",
    "buyer.buyerAddressLine": "baddr",
    "buyer.buyerPhoneNumber": "btel",
    "buyer.buyerEmail": "bmail",
    "buyer.buyerBankName": "bbank",
    "buyer.buyerBankAccount": "bacc",
    "items.item[].lineNumber": "items[].line",
    "items.item[].itemCode": "items[].code",
    "items.item[].itemName": "items[].name",
    "items.item[].unitName": "items[].unit",
    "items.item[].unitPrice": "items[].price",
    "items.item[].quantity": "items[].quantity",
    "items.item[].vatPercentage": "items[].vrt",
    "items.item[].vatAmount": "items[].vat",
    "items.item[].itemTotalAmountWithoutVat": "items[].amount",
    "items.item[].itemTotalAmountWithVat": "items[].total",
    //"vatPercentageBill": "vrt",
    //"sumOfTotalLineAmountWithoutVAT": "sum",
    "totalAmountWithoutVAT": "sumv",
    "totalVATAmount": "vatv",
    "totalAmountWithVAT": "totalv",
    "totalAmountWithVATFrn": "total",
    "discountAmount": "discountv",
    "totalAmountWithVATInWords": "word"
}

const VHOADON = {
    "invoiceType": "type",
    "invoiceSeries": "serial",
    "invoiceTemplate": "form",
    "invoiceNo": "seq",
    "invoiceName": "name",
    "invoiceDate": { "key": "idt", "transform": (val) => { return moment(val, mfd).format(dtf) } },
    "currencyCode": "curr",
    "exchangeRate": "exrt",
    "notes": "note",
    "paymentMethodName": "paym",
    "seller.sellerLegalName": "sname",
    "seller.sellerTaxCode": "stax",
    "seller.sellerAddressLine": "sadd",
    "seller.sellerEmail": "smail",
    "buyer.buyerCode": "bcode",
    "buyer.buyerName": "buyer",
    "buyer.buyerLegalName": "bname",
    "buyer.buyerTaxCode": "btax",
    "buyer.buyerAddressLine": "baddr",
    "buyer.buyerEmail": "bmail",
    "items.item[].lineNumber": "items[].line",
    "items.item[].itemName": "items[].name",
    "items.item[].unitName": "items[].unit",
    "items.item[].unitPrice": "items[].price",
    "items.item[].quantity": "items[].quantity",
    "items.item[].vatRate": "items[].vrt",
    "items.item[].vatAmount": "items[].vat",
    "items.item[].finamount": "items[].amount",
    "items.item[].grossamount": "items[].total",
    "finAmount": "sumv",
    "vatAmount": "vatv",
    "grossAmount": "totalv",
    "disAmount": "discountv",
    "amountInWords": "word"
}

const AZ = {
    "paymentType": "paym",
    "createdDate": { "key": "idt", "transform": (val) => { return moment(val).format(dtf) } },
    "company.name": "sname",
    "company.taxCode": "stax",
    "company.email": "smail",
    "company.phones": "stel",
    "company.bankAccount": "sacc",
    "company.bankInfo": "sbank",
    "company.address": "saddr",
    "customer.code": "bcode",
    "customer.name": "bname",
    "customer.taxCode": "btax",
    "customer.email": "bmail",
    "customer.address": "baddr",
    "billNumber": { "key": "seq", "transform": (val) => { return val.toString().padStart(config.SEQ_LEN, "0") } },
    "billTemplate.code": "serial",
    "billPattern.billType": "type",
    "billPattern.billTypeName": "name",
    "billPattern.billPatternCode": "form",
    "products.product[].name": "items[].name",
    "products.product[].unit": "items[].unit",
    "products.product[].unitPrice": "items[].price",
    "products.product[].quantity": "items[].quantity",
    "products.product[].taxRate.taxValue": { "key": "items[].vrt", "transform": (val) => { return 100 * val } },
    "totalAmount": "totalv",
    "billCurrency": "curr",
}

const CKS = {
    "NBan.Signature.Object.SignatureProperties.SignatureProperty.SigningTime": "adt"
}

const htm = `<!DOCTYPE html>
<html>
<head>
<style>
	table {width: 100%; border-collapse: collapse; margin-bottom: 10px;}
	table td {padding: 3px;}
	#detail { border: 1px solid;}
	#detail th { border: 1px solid;}
	#detail td { border: 1px dotted;}
    #detail tr {page-break-inside: avoid;}
    table.xwrap td {display: inline-block;}
</style>
</head>
<body>
<table>
<tbody>
<tr>
<td style="text-align: center;"><strong>{{uppercase name}}</strong></td>
<td style="text-align: right; width: 15%;"><strong>Mẫu số:</strong></td>
<td style="width: 15%;">{{form}}</td>
</tr>
<tr>
<td style="text-align: center;"><em>Ngày lập: {{fd idt}}</em></td>
<td style="text-align: right;"><strong>Ký hiệu:</strong></td>
<td>{{serial}}</td>
</tr>
<tr>
<td style="text-align: center;">{{adj.des}}</td>
<td style="text-align: right;"><strong>Số:</strong></td>
<td><span style="color: #ff0000;">{{seq}}</td>
</tr>
</tbody>
</table>
<table class="xwrap">
<tbody>
<tr><td>Đơn vị bán:</td><td colspan="3"><strong>{{sname}}</strong></td></tr>
<tr><td>Địa chỉ:</td><td colspan="3">{{saddr}}</td></tr>
<tr><td><strong>Mã số thuế:</strong></td><td colspan="3"><strong>{{stax}}</strong></td></tr>
<tr><td>Điện thoại:</td><td>{{stel}}</td><td>Mail:</td><td>{{smail}}</td></tr>
{{#if sacc}}<tr><td>Tài khoản:</td><td colspan="3">{{sacc}} {{sbank}}</td></tr>{{/if}}
{{#if buyer}}<tr><td>Người mua:</td><td colspan="3">{{buyer}}</td></tr>{{/if}}
<tr><td>Đơn vị mua:</td><td colspan="3"><strong>{{bname}}</strong></td></tr>
<tr><td>Địa chỉ:</td><td colspan="3">{{baddr}}</td></tr>
<tr><td><strong>Mã số thuế:</strong></td><td colspan="3"><strong>{{btax}}</strong></td></tr>
<tr><td style="width:15%;">Điện thoại:</td><td>{{btel}}</td><td style="width: 15%;">Mail:</td><td>{{bmail}}</td></tr>
{{#if bacc}}<tr><td>Tài khoản:</td><td colspan="3">{{bacc}} {{bbank}}</td></tr>{{/if}}
<tr><td>Hình thức TT:</td><td>{{paym}}</td><td colspan="2">{{#unlessEq curr 'VND'}} Loại tiền : {{curr}} Tỷ giá: {{fn exrt}} {{/unlessEq}}</td></tr>
</tbody>
</table>
<table id="detail">
<thead><tr><th>STT</th><th>Hàng hóa</th><th>ĐVT</th><th>Thuế suất</th><th>Số lượng</th><th>Đơn giá</th><th>Thành tiền</th></tr></thead>
<tbody>
<tr style="display: none;"><td>{{#each items}}</td></tr>
<tr>
	<td style="text-align: center;">{{line}}</td>
	<td>{{name}}</td>
	<td>{{unit}}</td>
	<td style="text-align: right;">{{#if vrt}}{{vrt}}{{/if}}</td>
	<td style="text-align: right;">{{#if quantity}}{{fn2 quantity}}{{/if}}</td>
	<td style="text-align: right;">{{#if price}}{{fn2 price}}{{/if}}</td>
	<td style="text-align: right;">{{fn2 amount}}</td>
</tr>
<tr style="display: none;"><td>{{/each}}</td></tr>
</tbody>
</table>
<table>
<tbody>
    <tr>
        <td style="width: 40%;"></td>
        <td style="width: 40%; text-align: right;"><strong>Cộng tiền hàng:</strong></td>
        <td style="width: 20%; text-align: right;">{{fn sumv}}</td>
    </tr>
    <tr>
        <td><strong>{{#if vrt}}Thuế suất: {{vrt}}%{{/if}}</strong></td>
        <td style="text-align: right;"><strong>Cộng tiền thuế GTGT:</strong></td>
        <td style="text-align: right;">{{fn vatv}}</td>
    </tr>
    <tr>
        <td>{{#if note}}Ghi chú: {{note}}{{/if}}</td>
        <td style="text-align: right;"><strong>Tổng cộng tiền thanh toán:</strong></td>
        <td style="text-align: right;">{{fn totalv}}</td>
    </tr>
    {{#if word}}<tr><td colspan="3"><strong>Bằng chữ: {{word}}</strong></td></tr>{{/if}}
</tbody>
</table>
</div>
</body>
</html>`

const PERIODS = [
    "Tháng 01",
    "Tháng 02",
    "Tháng 03",
    "Tháng 04",
    "Tháng 05",
    "Tháng 06",
    "Tháng 07",
    "Tháng 08",
    "Tháng 09",
    "Tháng 10",
    "Tháng 11",
    "Tháng 12"
]

const CATEGORIES = [
    "Hàng hóa, dịch vụ dùng riêng cho SXKD chịu thuế GTGT",
    "Hàng hóa, dịch vụ dùng riêng cho SXKD không chịu thuế GTGT",
    "Hàng hóa, dịch vụ dùng chung cho SXKD chịu thuế GTGT và không chịu thuế GTGT",
    "Hàng hóa, dịch vụ dùng cho dự án đầu tư"
]

const convertDataBInv = (data) => {
    for ( let b_inv of data) {
        b_inv.month = PERIODS[parseInt(b_inv.month) - 1]
        b_inv.category = CATEGORIES[parseInt(b_inv.category) - 1]
    }
    return data
}


const tmp = { body: htm }
const taxbreak = (tax) => {
    let tmp1 = {}, tmp2 = {}, obj = []
    tax.forEach(d => {
        let vrt = d.vrt
        if (vrt) {
            let vatv = d.vatv, amtv = d.amtv
            if (vatv && amtv) {
                if (tmp1.hasOwnProperty(vrt)) tmp1[vrt] = tmp1[vrt] + vatv
                else tmp1[vrt] = vatv
                if (tmp2.hasOwnProperty(vrt)) tmp2[vrt] = tmp2[vrt] + amtv
                else tmp2[vrt] = amtv
            }
        }
    })
    for (let prop in tmp1) {
        obj.push({ vrt: prop, vrn: `${prop}%`, vatv: tmp1[prop], amtv: tmp2[prop] })
    }
    return obj
}

const IWH = (req) => {
    const token = SEC.decode(req), schema = token.schema, query = req.query, filter = JSON.parse(query.filter), sort = query.sort, dbtypepre = (dbtype == "mysql") ? "" : (dbtype == "mssql") ? "@" : (dbtype == "orcl") ? `:` : `$")`
    let order, where = ` where ${(dbtype == "mysql") ? "date(idt)" : (dbtype == "mssql") ? "CONVERT(datetime, idt)" : (dbtype == "orcl") ? `to_date(idt, "YYYY-MM-DD")` : `to_date(idt, "YYYY-MM-DD")`} between ${(dbtype == "mysql") ? "?" : (dbtypepre + "1")} and ${(dbtype == "mysql") ? "?" : (dbtypepre + "2")} and (ou = ${(dbtype == "mysql") ? "?" : (dbtypepre + "3")} or 1=1)`, binds = [moment(new Date(filter.fd)).format(dtf), moment(moment(filter.td).endOf("day")).format(dtf), token.ou]
    let i = 3
    for (const key of Object.keys(filter)) {
        let val = filter[key]
        if (val) {
            switch (key) {
                case "fd":
                    break
                case "td":
                    break
                case "paid":
                    where += ` and paid=${(dbtype == "mysql") ? "?" : (dbtypepre + (++i))}`
                    binds.push(val)
                    break
                case "type":
                    where += ` and type=${(dbtype == "mysql") ? "?" : (dbtypepre + (++i))}`
                    binds.push(val)
                    break
                case "paym":
                    where += ` and upper(paym) like ${(dbtype == "mysql") ? "?" : (dbtypepre + (++i))}`
                    binds.push(`%${val.toUpperCase()}%`)
                    break
                case "curr":
                    where += ` and curr=${(dbtype == "mysql") ? "?" : (dbtypepre + (++i))}`
                    binds.push(val)
                    break
                case "stax":
                    where += ` and stax=${(dbtype == "mysql") ? "?" : (dbtypepre + (++i))}`
                    binds.push(val)
                    break
                default:
                    where += ` and upper(${key}) like ${(dbtype == "mysql") ? "?" : (dbtypepre + (++i))}`
                    binds.push(`%${val.toUpperCase()}%`)
                    break

            }
        }
    }
    if (sort) {
        Object.keys(sort).forEach(key => {
            order = ` order by ${key} ${sort[key]}`
        })
    }
    else order = " order by id desc"
    return { where: where, binds: binds, order: order, schema: schema }
}

const extractdynamicfield = (ttkhacjson) => {
    let returnarr = []
    if(!ttkhacjson) return returnarr
    let ttkhac = JSON.parse(JSON.stringify(ttkhacjson))
    if (ttkhac && ttkhac.length > 0) {
        //Lặp và bóc tách dữ liệu thẻ TTin.TTruong
        for (let ttin of ttkhac) {
            let ttruong = ttin.TTruong
            //Lấy tên trường
            const vprefname = String(ttruong).split("(")
            let fname = String(vprefname[vprefname.length - 1]).split(")")[0]
            //Lấy giá trị
            let fvalue = ttin.DLieu
            returnarr.push({ fname: fname, fvalue: fvalue })
        }
    }
    return returnarr
}

const TAGS = ["DLHDon", "Content", "invoiceData", "HoaDon", "InvoiceDataXML", "HDonCQT"]
const MAPS = [FPT, VNPT, BKAV, MINV, NEWI, AZ]
function unescape (string) {
    return string.toString().replace(/&apos;/g, "'")
    .replace(/&quot;/g, '"')
    .replace(/&gt;/g, '>')
    .replace(/&lt;/g, '<')
    .replace(/&amp;/g, '&')
}
const Service = {
    x2d: (str) => {
        str = str.replace(/<(\/?)([^:>\s]*:)?([^>]+)>/g, "<$1$3>")
        str = str.replace(/\<br\/>/g, " ")
        const doc = new dom().parseFromString(str, "text/xml")
        return doc
    },
    x2jsub: (doc) => {
        try {
            let inv
            for (let i = 0; i < TAGS.length; i++) {
                const tag = TAGS[i]
                const nodes = xpath.select(`//${tag}`, doc)
                if (nodes.length > 0) {
                    const xml = nodes[0].toString()
                    let json = util.x2j(xml)[tag]
                    //cho riêng yusen
                    json = JSON.stringify(json)
                    json = json.replace(/&amp;/g, "&")
                    json = json.replace(/&lt;/g, "<")
                    json = JSON.parse(json)
                    logger4app.debug(json);
                    let map = MAPS[i]
                    map = {...map, "TTChung.MTCuu": "sec"}
                    if (i == 0) {
                        const TTChung = json.TTChung
                        const mauso = TTChung.hasOwnProperty("MauSo")
                        if (mauso) map = MAP_HDR
                        else {
                            const TTKhac = TTChung.TTKhac
                            if (TTKhac && TTKhac.hasOwnProperty("serial")) map = MAP_HDR
                        }
                        inv = objectMapper(json, map)
                        inv.status = 3
                        inv.btax = inv.btax || ""
                        inv.seq = inv.seq || ""
                        inv.stax = inv.stax || ""
                        let seqL = ((inv.seq).toString()).length, btaxL = ((inv.btax).toString()).length, staxL = ((inv.stax).toString()).length
                        if (btaxL == 9 || btaxL == 12) inv.btax = "0" + inv.btax
                        if (staxL == 9 || staxL == 12) inv.stax = "0" + inv.stax
                        if (seqL < 7) {
                            while (seqL < 7) {
                                inv.seq = "0" + inv.seq
                                seqL++
                            }
                        }
                        if (!mauso) {
                            // if (inv.tax) {
                            //     for (const tax of inv.tax) {
                            //         inv.vat = tax.vat
                            //         tax.vatv = inv.vatv
                            //         tax.amt = inv.sum
                            //         tax.amtv = inv.sumv
                            //         let vrt = 0
                            //         if(inv.vat != 0){
                            //             vrt = inv.sum/inv.vat
                            //         }
                            //         logger4app.debug(vrt);
                            //         tax.vrn = mapvrn2(vrt)
                            //         tax.vrt = mapvrt2(vrt)
                            //     }
                            // }
                            for (const item of inv.items) {
                                const vrt = item.vrt
                                item.vat = 0
                                if (!item.vat && item.vrt != 0 && item.vrt > 0) item.vat = item.amount / vrt
                                if (!item.total) item.total = item.vat + item.amount
                                if (item.vrt < 0){
                                    item.vat = ""
                                    item.vrn ="\\"
                                }else{
                                    item.vrn = mapvrn(vrt)
                                    item.vrt = mapvrt(vrt)
                                }
                            }
                        }
                        //Thẻ động c0 - c9 header
                        //Extract thẻ thông tin khác để đưa vào inv
                        let ttkhachdr = extractdynamicfield(inv.ttkhac)
                        for (let dfield of ttkhachdr) inv[dfield.fname] = dfield.fvalue

                        //Thẻ động c0 - c9 detail
                        //Extract thẻ thông tin khác để đưa vào inv item
                        let items = []
                        for (const item of inv.items) {
                            let ttkhacdtl = extractdynamicfield(item.ttkhac)
                            for (let dfield of ttkhacdtl) item[dfield.fname] = dfield.fvalue
                            items.push(item)
                        }
                        inv.items = items
                        logger4app.debug(inv);
                    }
                    else if (i == 1) {
                        if (json.hasOwnProperty("CompanyTaxCode")) map = FAST
                        inv = objectMapper(json, map)
                    }
                    else if (i == 2) {
                        if (json.hasOwnProperty("grossAmount")) map = VHOADON
                        inv = objectMapper(json, map)
                    }
                    else if (i == 3) {
                        const hd = json.ThongTinHoaDon
                        hd.ChiTiet = json.ChiTietHoaDon.ChiTiet
                        inv = objectMapper(hd, map)
                    }
                    else if (i == 4) {
                        const hd = json.InvoiceData
                        hd.InvoiceDetails = json.InvoiceDetails
                        inv = objectMapper(hd, map)
                    }
                    else inv = objectMapper(json, map)
                    break
                }
            }
            if (!inv) throw new Error("Định dạng file xml chưa được FPTeinvoice hỗ trợ")
            let form = inv.form
            if (!inv.type && form) {
                const f6 = form.substring(0, 6)
                if (["01GTKT", "02GTTT", "07KPTQ"].includes(f6)) inv.type = f6
            }
            if (!inv.name && inv.type) inv.name = util.tenhd(inv.type)
            if (inv.curr == "VND") {
                if (util.isNumber(inv.sumv)) inv.sum = Number(inv.sumv)
                if (util.isNumber(inv.totalv)) inv.total = Number(inv.totalv)
                if (inv.vatv && util.isNumber(inv.vatv)) inv.vat = Number(inv.vatv)
            }
            else {
                let exrt = Number(inv.exrt)
                if (exrt) {
                    if (util.isNumber(inv.sumv) && !(inv.sum)) inv.sum = Number(inv.sumv) / exrt
                    if (util.isNumber(inv.totalv) && !(inv.total)) inv.total = Number(inv.totalv) / exrt
                    if (inv.vatv && util.isNumber(inv.vatv) && !(inv.vat)) inv.vat = Number(inv.vatv) / exrt
                }
            }
            return inv
        } catch (e) {
            throw e
        }
    },    x2jsubTT78: (doc) => {
        try {
            let inv, adt
            for (let i = 0; i < TAGS.length; i++) {
                const tag = TAGS[i]
                const nodes = xpath.select(`//${tag}`, doc)
                if (i==0) {
                    const nodes = xpath.select(`//DSCKS`, doc)
                    if (nodes.length > 0) {
                        const xml = nodes[0].toString()
                        let json = util.x2j(xml)["DSCKS"]
                        json = JSON.stringify(json)
                        json = json.replace(/&amp;/g, "&")
                        json = json.replace(/&lt;/g, "<")
                        json = JSON.parse(json)
                        adt = objectMapper(json, {"NBan.Signature.Object.SignatureProperties.SignatureProperty.SigningTime":"adt"})
                    }
                }
                if (nodes.length > 0) {
                    const xml = nodes[0].toString()
                    let json = util.x2j(xml)[tag]
                    json = JSON.stringify(json)
                    // json = json.replace(/&amp;/g, "&")
                    json = unescape(json)
                    json = json.replace(/&lt;/g, "<")
                    json = JSON.parse(json)
                    let map = MAPS[i]
					map = {...map, "NDHDon.TToan.THTTLTSuat.LTSuat[].TSuat": "tax[].vrt", "NDHDon.TToan.THTTLTSuat.LTSuat[].ThTien": "tax[].amt", "NDHDon.TToan.THTTLTSuat.LTSuat[].TThue": "tax[].vat", "TTChung.NLap":"idt"}

                    if (i == 0) {
                        const TTChung = json.TTChung
                        const mauso = TTChung.hasOwnProperty("MauSo")
                        if (mauso) map = FPT
                        else {
                            const TTKhac = TTChung.TTKhac
                            if (TTKhac && TTKhac.hasOwnProperty("serial")) map = ONPREM
                        }
                        inv = objectMapper(json, map)
                        inv.status = 3
                        inv.btax = inv.btax || ""
                        inv.stax = inv.stax || ""
                        inv.seq = inv.seq || ""
                        let seqL = ((inv.seq).toString()).length, btaxL = ((inv.btax).toString()).length, staxL = ((inv.stax).toString()).length
                        if (btaxL == 9 || btaxL == 12) inv.btax = "0" + inv.btax
                        if (staxL == 9 || staxL == 12) inv.stax = "0" + inv.stax
                        if (seqL < 8) {
                            while (seqL < 8) {
                                inv.seq = "0" + inv.seq
                                seqL++
                            }
                        }
                        // if (!mauso) {
                            for (const item of inv.items) {
                                let vrt = item.vrt, vrn = item.vrn
                                if(!vrt && vrn) {
                                    vrt = mapvrt(vrn)
                                }
                                item.vat = 0
                                if (!item.vat && vrt != 0 && vrt > 0) item.vat = item.amount * vrt/100
                                if (!item.total) item.total = item.vat + item.amount
                                if (item.vrt < 0){
                                    item.vat = ""
                                    item.vrn ="\\"
                                }else if(Number(vrt)>-1){
                                    item.vrn = mapvrn(vrt)
                                    item.vrt = mapvrt(vrt)
                                }
                            }
                        // }
                        //Thẻ động c0 - c9 header
                        //Extract thẻ thông tin khác để đưa vào inv
                        let ttkhachdr = extractdynamicfield(inv.ttkhac)
                        for (let dfield of ttkhachdr) inv[dfield.fname] = dfield.fvalue

                        //Thẻ động c0 - c9 detail
                        //Extract thẻ thông tin khác để đưa vào inv item
                        let items = []
                        for (const item of inv.items) {
                            let ttkhacdtl = extractdynamicfield(item.ttkhac)
                            for (let dfield of ttkhacdtl) item[dfield.fname] = dfield.fvalue
                            items.push(item)
                        }
                        inv.items = items
                    }
                    else if (i == 1) {
                        if (json.hasOwnProperty("CompanyTaxCode")) map = FAST
                        inv = objectMapper(json, map)
                    }
                    else if (i == 2) {
                        if (json.hasOwnProperty("grossAmount")) map = VHOADON
                        inv = objectMapper(json, map)
                    }
                    else if (i == 3) {
                        const hd = json.ThongTinHoaDon
                        hd.ChiTiet = json.ChiTietHoaDon.ChiTiet
                        inv = objectMapper(hd, map)
                    }
                    else if (i == 4) {
                        const hd = json.InvoiceData
                        hd.InvoiceDetails = json.InvoiceDetails
                        inv = objectMapper(hd, map)
                    }
                    else inv = objectMapper(json, map)
                    inv={...inv,...adt}
                    break
                }
                
            }
            
            if (!inv) throw new Error("Định dạng file xml chưa được FPTeinvoice hỗ trợ")
            let form = inv.form
            if (!inv.type && form) {
                let f6 = ''
                switch (form) {
                    case 1:
                        f6 = "01GTKT"
                        break;
                    case 2:
                        f6 = "02GTTT"
                        break;
                    case 7:
                        f6 = "07KPTQ"
                        break;
                    default:
                        break;
                }
                if (["01GTKT", "02GTTT", "07KPTQ"].includes(f6)) inv.type = f6
            }
            if (!inv.name && inv.type) inv.name = util.tenhd(inv.type)
            if (inv.curr == "VND") {
                if (util.isNumber(inv.sumv)) inv.sum = Number(inv.sumv)
                if (util.isNumber(inv.totalv)) inv.total = Number(inv.totalv)
                if (inv.vatv && util.isNumber(inv.vatv)) inv.vat = Number(inv.vatv)
            }
            else {
                let exrt = Number(inv.exrt)
                if (exrt) {
                    if (util.isNumber(inv.sumv) && !(inv.sum)) inv.sum = Number(inv.sumv) / exrt
                    if (util.isNumber(inv.totalv) && !(inv.total)) inv.total = Number(inv.totalv) / exrt
                    if (inv.vatv && util.isNumber(inv.vatv) && !(inv.vat)) inv.vat = Number(inv.vatv) / exrt
                }
            }
            // format vrt in tax
            inv.tax=inv.tax.map(e=>{
                e.vrt=mapvrt(e.vrt)
                if(inv.curr != "VND") {
                    e.vatv=round(e.vat*e.exrt)
                    e.amtv=round(e.amt*e.exrt)
                } else {
                    e.vatv=e.vat
                    e.amtv=e.amt*e.exrt
                }
                return e
            })
            if(inv.curr != "VND") {
                inv.sumv=round(inv.sum*inv.exrt)
                inv.totalv=round(inv.total*inv.exrt)
                inv.vatv=round(inv.vat*inv.exrt)
            } else {
                inv.sumv=inv.sum
                inv.totalv=inv.total
                inv.vatv=inv.vat
            }
            return inv
        } catch (e) {
            throw e
        }
    },
    x2j: (str) => {
        try {
            let inv
            str = str.replace(/<(\/?)([^:>\s]*:)?([^>]+)>/g, "<$1$3>")
            const doc = new dom().parseFromString(str, "text/xml")
            for (let i = 0; i < TAGS.length; i++) {
                const tag = TAGS[i]
                const nodes = xpath.select(`//${tag}`, doc)
                if (nodes.length > 0) {
                    const xml = nodes[0].toString()
                    const json = util.x2jb(xml)[tag]
                    let map = MAPS[i]
                    if (i == 1) {
                        if (json.hasOwnProperty("CompanyTaxCode")) map = FAST
                        inv = objectMapper(json, map)
                    }
                    else if (i == 2) {
                        if (json.hasOwnProperty("grossAmount")) map = VHOADON
                        inv = objectMapper(json, map)
                    }
                    else if (i == 3) {
                        const hd = json.ThongTinHoaDon
                        hd.ChiTiet = json.ChiTietHoaDon.ChiTiet
                        inv = objectMapper(hd, map)
                    }
                    else if (i == 4) {
                        const hd = json.InvoiceData
                        hd.InvoiceDetails = json.InvoiceDetails
                        inv = objectMapper(hd, map)
                    }
                    else inv = objectMapper(json, map)
                    break
                }
            }
            if (!inv) throw new Error("Định dạng file xml chưa được FPTeinvoice hỗ trợ")
            let form = inv.form
            if (!inv.type && form) {
                const f6 = form.substring(0, 6)
                if (["01GTKT", "02GTTT", "07KPTQ"].includes(f6)) inv.type = f6
            }
            if (!inv.name && inv.type) inv.name = util.tenhd(inv.type)
            if (inv.curr == "VND") {
                if (util.isNumber(inv.sumv)) inv.sum = Number(inv.sumv)
                if (util.isNumber(inv.totalv)) inv.total = Number(inv.totalv)
                if (inv.vatv && util.isNumber(inv.vatv)) inv.vat = Number(inv.vatv)
            }
            else {
                let exrt = Number(inv.exrt)
                if (exrt) {
                    if (util.isNumber(inv.sumv)) inv.sum = Number(inv.sumv) / exrt
                    if (util.isNumber(inv.totalv)) inv.total = Number(inv.totalv) / exrt
                    if (inv.vatv && util.isNumber(inv.vatv)) inv.vat = Number(inv.vatv) / exrt
                }
            }
            inv.paid = 0
            return inv
        } catch (e) {
            throw e
        }
    },
    x2j78: (doc) => {
        try {
            doc=Service.x2d(doc)
            let inv, adt
            for (let i = 0; i < TAGS.length; i++) {
                const tag = TAGS[i]
                const nodes = xpath.select(`//${tag}`, doc)
                if (i==0) {
                    const nodes = xpath.select(`//DSCKS`, doc)
                    if (nodes.length > 0) {
                        const xml = nodes[0].toString()
                        let json = util.x2j(xml)["DSCKS"]
                        json = JSON.stringify(json)
                        json = json.replace(/&amp;/g, "&")
                        json = json.replace(/&lt;/g, "<")
                        json = JSON.parse(json)
                        adt = objectMapper(json, {"NBan.Signature.Object.SignatureProperties.SignatureProperty.SigningTime":"adt"})
                    }
                }
                if (nodes.length > 0) {
                    const xml = nodes[0].toString()
                    let json = util.x2j(xml)[tag]
                    json = JSON.stringify(json)
                    // json = json.replace(/&amp;/g, "&")
                    json = unescape(json)
                    json = json.replace(/&lt;/g, "<")
                    json = JSON.parse(json)
                    let map = MAPS[i]
					map = {...map, "NDHDon.TToan.THTTLTSuat.LTSuat[].TSuat": "tax[].vrt", "NDHDon.TToan.THTTLTSuat.LTSuat[].ThTien": "tax[].amt", "NDHDon.TToan.THTTLTSuat.LTSuat[].TThue": "tax[].vat", "TTChung.NLap":"idt"}

                    if (i == 0) {
                        const TTChung = json.TTChung
                        const mauso = TTChung.hasOwnProperty("MauSo")
                        if (mauso) map = FPT
                        else {
                            const TTKhac = TTChung.TTKhac
                            if (TTKhac && TTKhac.hasOwnProperty("serial")) map = ONPREM
                        }
                        inv = objectMapper(json, map)
                        inv.status = 3
                        inv.btax = inv.btax || ""
                        inv.stax = inv.stax || ""
                        inv.seq = inv.seq || ""
                        let seqL = ((inv.seq).toString()).length, btaxL = ((inv.btax).toString()).length, staxL = ((inv.stax).toString()).length
                        if (btaxL == 9 || btaxL == 12) inv.btax = "0" + inv.btax
                        if (staxL == 9 || staxL == 12) inv.stax = "0" + inv.stax
                        if (seqL < 8) {
                            while (seqL < 8) {
                                inv.seq = "0" + inv.seq
                                seqL++
                            }
                        }
                        // if (!mauso) {
                            for (const item of inv.items) {
                                let vrt = item.vrt, vrn = item.vrn
                                if(!vrt && vrn) {
                                    vrt = mapvrt(vrn)
                                }
                                item.vat = 0
                                if (!item.vat && vrt != 0 && vrt > 0) item.vat = item.amount * vrt/100
                                if (!item.total) item.total = item.vat + item.amount
                                if (item.vrt < 0){
                                    item.vat = ""
                                    item.vrn ="\\"
                                }else if(Number(vrt)>-1){
                                    item.vrn = mapvrn(vrt)
                                    item.vrt = mapvrt(vrt)
                                }
                            }
                        // }
                        //Thẻ động c0 - c9 header
                        //Extract thẻ thông tin khác để đưa vào inv
                        let ttkhachdr = extractdynamicfield(inv.ttkhac)
                        for (let dfield of ttkhachdr) inv[dfield.fname] = dfield.fvalue

                        //Thẻ động c0 - c9 detail
                        //Extract thẻ thông tin khác để đưa vào inv item
                        let items = []
                        for (const item of inv.items) {
                            let ttkhacdtl = extractdynamicfield(item.ttkhac)
                            for (let dfield of ttkhacdtl) item[dfield.fname] = dfield.fvalue
                            items.push(item)
                        }
                        inv.items = items
                    }
                    else if (i == 1) {
                        if (json.hasOwnProperty("CompanyTaxCode")) map = FAST
                        inv = objectMapper(json, map)
                    }
                    else if (i == 2) {
                        if (json.hasOwnProperty("grossAmount")) map = VHOADON
                        inv = objectMapper(json, map)
                    }
                    else if (i == 3) {
                        const hd = json.ThongTinHoaDon
                        hd.ChiTiet = json.ChiTietHoaDon.ChiTiet
                        inv = objectMapper(hd, map)
                    }
                    else if (i == 4) {
                        const hd = json.InvoiceData
                        hd.InvoiceDetails = json.InvoiceDetails
                        inv = objectMapper(hd, map)
                    }
                    else inv = objectMapper(json, map)
                    inv={...inv,...adt}
                    break
                }
                
            }
            
            if (!inv) throw new Error("Định dạng file xml chưa được FPTeinvoice hỗ trợ")
            let form = inv.form
            if (!inv.type && form) {
                let f6 = ''
                switch (form) {
                    case 1:
                        f6 = "01GTKT"
                        break;
                    case 2:
                        f6 = "02GTTT"
                        break;
                    case 7:
                        f6 = "07KPTQ"
                        break;
                    default:
                        break;
                }
                if (["01GTKT", "02GTTT", "07KPTQ"].includes(f6)) inv.type = f6
            }
            if (!inv.name && inv.type) inv.name = util.tenhd(inv.type)
            if (inv.curr == "VND") {
                if (util.isNumber(inv.sumv)) inv.sum = Number(inv.sumv)
                if (util.isNumber(inv.totalv)) inv.total = Number(inv.totalv)
                if (inv.vatv && util.isNumber(inv.vatv)) inv.vat = Number(inv.vatv)
            }
            else {
                let exrt = Number(inv.exrt)
                if (exrt) {
                    if (util.isNumber(inv.sumv) && !(inv.sum)) inv.sum = Number(inv.sumv) / exrt
                    if (util.isNumber(inv.totalv) && !(inv.total)) inv.total = Number(inv.totalv) / exrt
                    if (inv.vatv && util.isNumber(inv.vatv) && !(inv.vat)) inv.vat = Number(inv.vatv) / exrt
                }
            }
            // format vrt in tax
            inv.tax=inv.tax.map(e=>{
                e.vrt=mapvrt(e.vrt)
                if(inv.curr != "VND") {
                    e.vatv=round(e.vat*e.exrt)
                    e.amtv=round(e.amt*e.exrt)
                } else {
                    e.vatv=e.vat
                    e.amtv=e.amt*e.exrt
                }
                return e
            })
            if(inv.curr != "VND") {
                inv.sumv=round(inv.sum*inv.exrt)
                inv.totalv=round(inv.total*inv.exrt)
                inv.vatv=round(inv.vat*inv.exrt)
            } else {
                inv.sumv=inv.sum
                inv.totalv=inv.total
                inv.vatv=inv.vat
            }
            inv.paid = 0
            logger4app.debug("inv hd dau vao", inv)
            return inv
        } catch (e) {
            throw e
        }
    },
    rbi: async (req, res, next) => {
        try {
            const token = SEC.decode(req), schema = token.schema, id = req.params.id
            const rows = await execsqlselect(sql_rbi[`sql_rbi_${dbtype}`], [id])

            if (rows.length == 0) throw new Error(`Không tìm thấy Hóa đơn ${id}`)
            let doc = JSON.parse(rows[0].doc)
            res.json(doc)
        } catch (err) {
            next(err)
        }
    },
    xview: async (req, res, next) => {
        try {
            const body = req.body
            await util.chkCaptcha(body.token, body.action, req.connection.remoteAddress)
            const file = req.file
            if (file) {
                const xml = file.buffer.toString(), fpt = body.fpt
                const result = sign.verify(xml)
                if (fpt == "0") {
                    result.tmp = tmp
                    result.doc = Service.x2j(xml)
                }
                res.json(result)
            }
            else res.json({ error: "Không có file" })
        }
        catch (err) {
            res.json({ error: err.message })
        }
    },
    xview2: async (req, res, next) => {
        try {
            const body = req.body
            await util.chkCaptcha(body.token, body.action, req.connection.remoteAddress)
            const file = req.file
            if (file) {
                const xml = file.buffer.toString(), fpt = body.fpt,json={}, loaiTTu = body.loaiTTu
                const result = sign.verify(xml)
                // const result = sign.verify2(xml, null)
                // if (fpt == "0") {
                    let subxml = Service.x2d(xml)
                    if (loaiTTu==32) {
                        result.doc = Service.x2jsub(subxml)
                    } else {
                        result.doc = Service.x2jsubTT78(subxml)
                    }
                    
                    json.doc = result.doc
                    const {stax, form,serial, type} = result.doc
                    if (stax) {
                        const org = await ous.obt(stax)
                        if(!org)throw new Error("Khong tim thay chi nhanh lap hd")
                        const idx = org.temp
                        const fn = form.length > 1 ? util.fn(form, idx) : `${type}.${form}.${serial}.${idx}`
                        const tmp2 = await util.template(fn)
                        result.tmp = tmp2
                    } else {
                        result.tmp = tmp
                    }
                    json.tmp = result.tmp
                // }
                let reqjsr = ext.createRequest(json)
                const bodyBuffer = await util.jsReportRenderAttach(reqjsr)
                let sizeInByte = bodyBuffer.byteLength
                res.json({ pdf: `data:application/pdf;base64, ${bodyBuffer.toString('base64')}`, sizePDF: sizeInByte})
            }
            else res.json({ error: "Không có file" })
        }
        catch (err) {
            res.json({ error: err.message })
        }
    },
    view: async (req, res, next) => {
        try {
            const token = SEC.decode(req), schema = token.schema, id = req.params.id
            const rows = await execsqlselect(sql_rbi[`sql_rbi_${dbtype}`], [id])
            if (rows.length == 0) throw new Error(`Không tìm thấy Hóa đơn ${id}`)
            const doc = rows[0].doc
            // res.json({ doc: doc, tmp: tmp })
            let reqjsr = ext.createRequest({ doc: doc, tmp: tmp })
            const bodyBuffer = await util.jsReportRenderAttach(reqjsr)
            let sizeInByte = bodyBuffer.byteLength
            res.json({ pdf: `data:application/pdf;base64, ${bodyBuffer.toString('base64')}`, sizePDF: sizeInByte })
        } catch (err) {
            next(err)
        }
    },
    xls: async (req, res, next) => {
        try {
            const iwh = IWH(req)
            const sql = `select id,${(dbtype == "mysql") ? "DATE_FORMAT(idt,'%d/%m/%Y')" : (dbtype == "mssql") ? "FORMAT(idt,'dd/MM/yyyy')" : (dbtype == "orcl") ? `TO_CHAR(a.idt,'${mfd}')` : `TO_CHAR(a.idt,'${mfd}')`} idt,type,form,serial,seq,stax,sname,smail,saddr,paid,note,sumv,vatv,totalv from b_inv ${iwh.where} ${iwh.order}`
            let result = await execsqlselect(sql, iwh.binds)
            result = convertDataBInv(result)
            res.json(result[0])
        } catch (err) {
            next(err)
        }
    },
    get: async (req, res, next) => {
        try {
            const query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 10, iwh = IWH(req)
            let sql, result, ret, voffset
            switch (dbtype) {
                case "mssql":
                    voffset = ` OFFSET ${start} ROWS FETCH NEXT ${count} ROWS ONLY`
                    break
                case "mysql":
                    voffset = ` limit ${count} offset ${start}`
                    break
                case "orcl":
                    voffset = ` OFFSET ${start} ROWS FETCH NEXT ${count} ROWS ONLY`
                    break
                case "pgsql":
                    voffset = ` limit ${count} offset ${start}`
                    break
            }
            sql = `select id "id",idt "idt",type "type",form "form",serial "serial",seq "seq",stax "stax",sname "sname",smail "smail",saddr "saddr",paid "paid",note "note",sumv "sumv",vatv "vatv",totalv "totalv",btax "btax",bname "bname",baddr "baddr" from b_inv ${iwh.where} ${iwh.order} ${voffset}`
            result = await execsqlselect(sql, iwh.binds)
            result = convertDataBInv(result)
            ret = { data: result, pos: start }
            if (start == 0) {
                sql = `select count(*) total from b_inv ${iwh.where}`
                result = await execsqlselect(sql, iwh.binds)
                ret.total_count = result[0].total
            }
            res.json(ret)
        } catch (err) {
            next(err)
        }
    },
    del: async (req, res, next) => {
        try {
            const token = SEC.decode(req), schema = token.schema, id = req.params.id, dbtypepre = (dbtype == "mysql") ? "?" : (dbtype == "mssql") ? "@1" : (dbtype == "orcl") ? `:1` : `$1")`
            await dbs.query(`delete from b_inv where id=${dbtypepre}`, [id])
            res.json(1)
        } catch (err) {
            next(err)
        }
    },
    upload: async (req, res, next) => {
        const param = req.body
        const file = req.file
        try {
            const token = SEC.decode(req), taxc = token.taxc, xml = file.buffer.toString(), doc = Service.x2j78(xml)
            /*
            if (btax == taxc) {
                doc.ou = token.ou
                doc.uc = token.uid
                const result = await dbs.query(`insert into ${token.schema}.b_inv set ?`, { xml: xml, doc: JSON.stringify(doc) })
                res.json({ status: "server", message: result[0] })
            }
            else throw new Error(`MST của HĐ ${btax} khác với MST đăng nhập ${taxc}`)
            */
            // doc.ou = token.ou
            // doc.uc = token.uid
            await dbs.query(sql_upload_xml[`sql_upload_${dbtype}`], [JSON.stringify(doc), xml])
            res.json({ status: "server", message: 1 })
        }
        catch (err) {
            const msg = (err.code == "ER_DUP_ENTRY") ? "Bản ghi đã tồn tại" : err.message
            res.json({ status: "error", message: `${file.originalname}: ${msg}` })
        }
    },
    put: async (req, res, next) => {
        try {
            const token = SEC.decode(req), schema = token.schema, body = req.body, id = req.params.id
            await dbs.query(sql_put[`sql_put_${dbtype}`], [JSON.stringify(body), id])

            // Update month, year, category
            // let sql = `SELECT MAX(id) FROM b_inv`
            // let maxId = await dbs.query(sql)
            // maxId = Object.values(maxId.recordset[0])[0]
            // sql = `UPDATE b_inv SET month = ${body.month}, year = ${body.year}, category = ${body.category} where id = ${maxId}`
            // await dbs.query(sql)
            res.json(1);
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try 
        {
            const token = SEC.decode(req), schema = token.schema, uid = token.uid, ou = token.ou, org = await ous.org(token), body = req.body
            body.adt = body.idt
            body.btax = token.taxc
            body.bname = org.name
            body.baddr = org.addr
            body.bmail = org.mail
            body.btel = org.tel
            body.bacc = org.acc
            body.bbank = org.bank
            body.ou = ou
            body.uc = uid
            await dbs.query(sql_post[`sql_post_${dbtype}`], [JSON.stringify(body)]);

            // Update month, year, category
            // let sql = `SELECT MAX(id) FROM b_inv`
            // let maxId = await dbs.query(sql)
            // maxId = Object.values(maxId.recordset[0])[0]
            // sql = `UPDATE b_inv SET month = ${body.month}, year = ${body.year}, category = ${body.category} where id = ${maxId}`
            // await dbs.query(sql)
            res.json({result : 1});
        }
        catch (err) {
            next(err)
        }
    },
    detailS: async (req, res, next) => {
        try {
            let fn,doc,body=req.body,where1,where2,where3,sql1,sql2,sql3,result1,result2,result3,rows1,rows2,rows3
            let month = body.month, year = body.year
            fn = "MZH_BINDetailS.docx"

            where1 = `where MONTH(idt)=${month} and YEAR(idt)=${year} and category = 1` //Hàng hóa, dịch vụ dùng riêng cho SXKD chịu thuế GTGT
            where2 = `where MONTH(idt)=${month} and YEAR(idt)=${year} and category = 3` //Hàng hóa, dịch vụ dùng chung cho SXKD chịu thuế GTGT và không chịu thuế GTGT
            where3 = `where MONTH(idt)=${month} and YEAR(idt)=${year} and category = 4` //Hàng hóa, dịch vụ dùng cho dự án đầu tư
			
            let selectItemVrt = `JSON_VALUE(JSON_QUERY (doc, '$.items[0]'),'$.vrt')`
            let selectItemQuantity = `JSON_VALUE(JSON_QUERY (doc, '$.items[0]'),'$.quantity')`
            let selectItemId = `JSON_VALUE(JSON_QUERY (doc, '$.items[0]'),'$.id')`
            let selectItemLine = `JSON_VALUE(JSON_QUERY (doc, '$.items[0]'),'$.line')`
            let selectItemVat = `JSON_VALUE(JSON_QUERY (doc, '$.items[0]'),'$.vat')`
            let selectItemTotal = `JSON_VALUE(JSON_QUERY (doc, '$.items[0]'),'$.total')`
            let selectItemName = `JSON_VALUE(JSON_QUERY (doc, '$.items[0]'),'$.name')`
            let selectItemUnit = `JSON_VALUE(JSON_QUERY (doc, '$.items[0]'),'$.unit')`
            let selectItemPrice = `JSON_VALUE(JSON_QUERY (doc, '$.items[0]'),'$.price')`
            let selectItemAmount = `JSON_VALUE(JSON_QUERY (doc, '$.items[0]'),'$.amount')`
            let selectItemVrn = `JSON_VALUE(JSON_QUERY (doc, '$.items[0]'),'$.vrn')`
            let selectName = `JSON_VALUE(doc,'$.name')`
            let selectBname = `JSON_VALUE(doc,'$.bname')`
            let selectBaddr = `JSON_VALUE(doc,'$.baddr')`
            let selectBmail = `JSON_VALUE(doc,'$.bmail')`
            let selectBtel = `JSON_VALUE(doc,'$.btel')`
            let selectBacc = `JSON_VALUE(doc,'$.bacc')`
            let selectBbank = `JSON_VALUE(doc,'$.bbank')`
            let selectOu = `JSON_VALUE(doc,'$.ou')`
            let selectUc = `JSON_VALUE(doc,'$.uc')`
            let selectType = `JSON_VALUE(doc,'$.type')`
            let selectIdt = `JSON_VALUE(doc,'$.idt')`
            let selectForm = `JSON_VALUE(doc,'$.form')`
            let selectSerial = `JSON_VALUE(doc,'$.serial')`
            let selectSeq = `JSON_VALUE(doc,'$.seq')`
            let selectSname = `JSON_VALUE(doc,'$.sname')`
            let selectStax = `JSON_VALUE(doc,'$.stax')`
            let selectBuyer = `JSON_VALUE(doc,'$.buyer')`
            let selectSaddr = `JSON_VALUE(doc,'$.saddr')`
            let selectStel = `JSON_VALUE(doc,'$.stel')`
            let selectSmail = `JSON_VALUE(doc,'$.smail')`
            let selectCurr = `JSON_VALUE(doc,'$.curr')`
            let selectExrt = `JSON_VALUE(doc,'$.exrt')`
            let selectPaym = `JSON_VALUE(doc,'$.paym')`
            let selectSacc = `JSON_VALUE(doc,'$.sacc')`
            let selectSbank = `JSON_VALUE(doc,'$.sbank')`
            let selectNote = `JSON_VALUE(doc,'$.note')`
            let selectSumv = `JSON_VALUE(doc,'$.sumv')`
            let selectSum = `JSON_VALUE(doc,'$.sum')`
            let selectVatv = `JSON_VALUE(doc,'$.vatv')`
            let selectVat = `JSON_VALUE(doc,'$.vat')`
            let selectWord = `JSON_VALUE(doc,'$.word')`
            let selectTotalv = `JSON_VALUE(doc,'$.totalv')`
            let selectTotal = `JSON_VALUE(doc,'$.total')`
            let selectPaid = `JSON_VALUE(doc,'$.paid')`
            
            let select = `month, year, category,`
            select += `
                ${selectItemVrt} iVrt,
                ${selectItemQuantity} iQuantity,
                ${selectItemId} iId,
                ${selectItemLine} iLine,
                ${selectItemVat} iVat,
                ${selectItemTotal} iTotal,
                ${selectItemName} iName,
                ${selectItemUnit} iUnit,
                ${selectItemPrice} iPrice,
                ${selectItemAmount} iAmount,
                ${selectItemVrn} iVrn,
                ${selectName} name,
                ${selectBname} bname,
                ${selectBaddr} baddr,
                ${selectBmail} bmail,
                ${selectBtel} btel,
                ${selectBacc} bacc,
                ${selectBbank} bbank,
                ${selectOu} ou,
                ${selectUc} uc,
                ${selectType} type,
                ${selectIdt} idt,
                ${selectForm} form,
                ${selectSerial} serial,
                ${selectSeq} seq,
                ${selectSname} sname,
                ${selectStax} stax,
                ${selectBuyer} buyer,
                ${selectSaddr} saddr,
                ${selectStel} stel,
                ${selectSmail} smail,
                ${selectCurr} curr,
                ${selectExrt} exrt,
                ${selectPaym} paym,
                ${selectSacc} sacc,
                ${selectSbank} sbank,
                ${selectNote} note,
                ${selectSumv} sumv,
                ${selectSum} sum,
                ${selectVatv} vatv,
                ${selectVat} vat,
                ${selectWord} word,
                ${selectTotalv} totalv,
                ${selectTotal} total,
                ${selectPaid} paid`

            sql1=`select ${select} from b_inv ${where1}`
            sql2=`select ${select} from b_inv ${where2}`
            sql3=`select ${select} from b_inv ${where3}`
            result1 = await dbs.query(sql1)
            result2 = await dbs.query(sql2)
            result3 = await dbs.query(sql3)
            rows1 = result1.recordset
            rows2 = result2.recordset
            rows3 = result3.recordset
            // logger4app.debug([...rows1, ...rows2, ...rows3])

            let a1 = [], a2 = [], a3 = []
            let i1 = 0, i2 = 0, i3 = 0
            let sumSumv1 = 0, sumSumv2 = 0, sumSumv3 = 0, sumVat1 = 0, sumVat2 = 0, sumVat3 = 0, finalSumv = 0, finalSumVat = 0
            let seq, idt, sname, stax, sumv, vat, note

            for(const row of rows1){
                let r = new Object()
                sumv = parseInt(row.sumv)
                vat = parseInt(row.vat)
                r.seq = row.seq
                r.idt = moment(row.idt).format("DD/MM/YYYY")
                r.sname = row.sname
                r.stax = row.stax
                r.sumv = sumv.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                r.vat = vat.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                r.note = row.note
                r.index = ++i1
                a1.push(r)

                sumSumv1 += sumv
                sumVat1 += vat
            }

            for(const row of rows2){
                let r = new Object()
                sumv = parseInt(row.sumv)
                vat = parseInt(row.vat)
                r.seq = row.seq
                r.idt = moment(row.idt).format("DD/MM/YYYY")
                r.sname = row.sname
                r.stax = row.stax
                r.sumv = sumv.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                r.vat = vat.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                r.note = row.note
                r.index = ++i2
                a2.push(r)

                sumSumv2 += sumv
                sumVat2 += vat
            }

            for(const row of rows3){
                let r = new Object()
                sumv = parseInt(row.sumv)
                vat = parseInt(row.vat)
                r.seq = row.seq
                r.idt = moment(row.idt).format("DD/MM/YYYY")
                r.sname = row.sname
                r.stax = row.stax
                r.sumv = sumv.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                r.vat = vat.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                r.note = row.note
                r.index = ++i3
                a3.push(r)

                sumSumv3 += sumv
                sumVat3 += vat
            }
            finalSumv = sumSumv1 + sumSumv2 + sumSumv3
            finalSumVat = sumVat1 + sumVat2 + sumVat3
            sumSumv1 = sumSumv1.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            sumSumv2 = sumSumv2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            sumSumv3 = sumSumv3.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            sumVat1 = sumVat1.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            sumVat2 = sumVat2.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            sumVat3 = sumVat3.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            finalSumv = finalSumv.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            finalSumVat = finalSumVat.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")

            year = year.toString()
            doc={
                month:month,
                year:year,
                a1:a1,
                a2:a2,
                a3:a3,
                sumSumv1:sumSumv1,
                sumSumv2:sumSumv2,
                sumSumv3:sumSumv3,
                sumVat1:sumVat1,
                sumVat2:sumVat2,
                sumVat3:sumVat3,
                finalSumv:finalSumv,
                finalSumVat:finalSumVat
            }
            
            const out = util.docxrender(doc, fn)
            res.end(out, "binary")		 
        } catch (err) {
            next(err)
        }
    }
}
module.exports = Service