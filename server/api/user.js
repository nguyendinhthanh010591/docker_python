"use strict"
const util = require("./util")
const sec = require("./sec")
const inc = require("./inc")
const config = require("./config")
const dbs = require(`./${config.dbtype}/dbs`)
const logging = require("./logging")
const logger4app = require("./logger4app")
const moment = require("moment")
const path = require("path")
const fs = require("fs")
const xlsxtemp = require("xlsx-template")
const Json2csvParser = require("json2csv").Parser;
const sql_ou = {
  sql_ou_mssql: `with tree as (
    select id,name,taxc,pid,0 level,'('+CONVERT(VARCHAR(max),id)+'-0)' idx from s_ou where id=?
    union all
    select c.id,c.name,c.taxc,c.pid,p.level+1 level,p.idx+'*('+CONVERT(VARCHAR(max),c.id)+'-'+CONVERT(VARCHAR(max),p.level)+')' idx  from s_ou c,tree p where p.id=c.pid
    ) select x.id,x.pid,x.taxc,x.name,x.checked from (
    select a.id,a.pid,a.taxc,a.name,a.idx,0 checked from tree a left  join s_manager b on a.taxc=b.taxc and b.user_id=? where b.taxc is null 
    union 
    select a.id,a.pid,a.taxc,a.name,a.idx,1 checked from tree a inner join s_manager b on a.taxc=b.taxc and b.user_id=?) 
    x where x.taxc is not null order by idx`,
    sql_ou_orcl: `select x.id "id",x.pid "pid",x.taxc "taxc",x.name "name",x.sel "checked" from (select a.id,a.pid,a.taxc,a.name,0 sel from (select id,pid,taxc,name from s_ou where taxc is not null start with id=:1 connect by prior id=pid) a left join s_manager b on a.taxc=b.taxc and b.user_id=:2 where b.taxc is null union select a.id,a.pid,a.taxc,a.name,1 sel from (select id,pid,taxc,name from s_ou where taxc is not null start with id=:3 connect by prior id=pid) a inner join s_manager b on a.taxc=b.taxc and b.user_id=:4) x start with x.id=:5 connect by prior x.id=x.pid`,
    sql_ou_mysql:`select x.id id,x.pid pid,x.taxc taxc,x.name name,x.sel checked from (select a.id,a.pid,a.taxc,a.name,0 sel from (select o.id,o.pid,o.taxc,o.name from s_ou o where o.id in (select cid from s_ou_tree where pid=?)) a left join s_manager b on a.taxc=b.taxc and b.user_id=? where b.taxc is null union select a.id,a.pid,a.taxc,a.name,1 sel from (select o.id,o.pid,o.taxc,o.name from s_ou o where o.id in (select cid from s_ou_tree where pid=?)) a inner join s_manager b on a.taxc=b.taxc and b.user_id=?) x join s_ou_tree y on y.cid = x.id where x.taxc is not null and y.pid=1 order by y.path`,
  sql_ou_pgsql:`with recursive tree as (select id,name,taxc,pid,0 as level,'('||id||'-0)' idx from s_ou where id=?
    union all
    select c.id,c.name,c.taxc,c.pid,p.level+1,p.idx||'*('||c.id||'-'||p.level||')' idx from s_ou c,tree p where p.id=c.pid
    ) select x.id,x.pid,x.taxc,x.name,x.checked from (
    select a.id,a.pid,a.taxc,a.name,a.idx,0 checked from tree a left  join s_manager b on a.taxc=b.taxc and b.user_id=? where b.taxc is null 
    union 
    select a.id,a.pid,a.taxc,a.name,a.idx,1 checked from tree a inner join s_manager b on a.taxc=b.taxc and b.user_id=?) 
    x where x.taxc is not null order by idx`
}
const sql_ubr = {
  sql_ubr_sql: `with tree as (select ? id union all select c.id from s_ou c,tree p where p.id=c.pid),suse as (select u.* from s_user u,tree t where u.ou=t.id)
    select x.id acc,x.name,x.sel,x.mail,x.ou,x.uc,x.pos from (
    select a.id,a.name,0 sel,a.mail,a.ou,a.uc,a.pos from suse a left  join s_member b on a.id=b.user_id and b.role_id=? where b.user_id is null 
    union 
    select a.id,a.name,1 sel,a.mail,a.ou,a.uc,a.pos from suse a inner join s_member b on a.id=b.user_id and b.role_id=?) 
    x order by x.id`,
  sql_ubr_orcl: `select x.id "acc",x.name "name",x.sel "sel",x.mail "mail",x.ou "ou",x.uc "uc",x.pos "pos" from (select a.id,a.name,0 sel,a.mail,a.ou,a.uc,a.pos from s_user a left join s_member b on a.id = b.user_id and b.role_id=? where b.user_id is null and a.ou in (select id from s_ou start with id=? connect by prior id=pid) union select a.id,a.name,1 sel,a.mail,a.ou,a.uc,a.pos from s_user a inner join s_member b on a.id = b.user_id and b.role_id=? and a.ou in (select id from s_ou start with id=? connect by prior id=pid)) x order by x.id`,
  sql_ubr_mysql: `select x.id acc,x.name name,x.sel sel,x.mail mail,x.ou ou,x.uc uc,x.pos pos from (select a.id,a.name,0 sel,a.mail,a.ou,a.uc,a.pos from s_user a left join s_member b on a.id = b.user_id and b.role_id=? where b.user_id is null and a.ou in (select b.id from s_ou_tree a join s_ou b on b.id = a.cid where a.pid=? order by a.path) union select a.id,a.name,1 sel,a.mail,a.ou,a.uc,a.pos from s_user a inner join s_member b on a.id = b.user_id and b.role_id=? and a.ou in (select b.id from s_ou_tree a join s_ou b on b.id = a.cid where a.pid=? order by a.path)) x order by x.id`,
  sql_ubr_pgsql: `with recursive tree as (select id from s_ou where id=? union all select c.id from s_ou c,tree p where p.id=c.pid),suse as (select u.* from s_user u,tree t where u.ou=t.id)
  select x.id acc,x.name,x.sel,x.mail,x.ou,x.uc,x.pos from (
  select a.id,a.name,0 sel,a.mail,a.ou,a.uc,a.pos from suse a left  join s_member b on a.id=b.user_id and b.role_id=? where b.user_id is null 
  union 
  select a.id,a.name,1 sel,a.mail,a.ou,a.uc,a.pos from suse a inner join s_member b on a.id=b.user_id and b.role_id=?) 
  x order by x.id`
}
const sql_upduserst = {
  sql_upduserst_mssql: `update s_user set last_login = GETDATE(),login_number = login_number+1 where id=?`,
  sql_upduserst_orcl: `update s_user set last_login = SYSDATE where id=?`,
  sql_upduserst_mysql: `update s_user set last_login = CURRENT_TIMESTAMP where id=?`,
  sql_upduserst_pgsql: `update s_user set last_login = NOW() where id=?`
}
const sql_get_01 = {
  sql_get_01_mssql: `with tree as (select ? id union all select c.id from s_ou c,tree p where p.id=c.pid)  select a.id,a.mail,a.ou,a.uc,a.name,a.pos,a.code${(config.is_use_local_user) ? `,a.local` : ``} from s_user a,tree b`,
  sql_get_01_orcl: `select id "id",mail "mail",ou "ou",uc "uc",name "name",pos "pos",code "code"${(config.is_use_local_user) ? `,local "local"` : ``} from s_user a`,
  sql_get_01_mysql: `select id id,mail mail,ou ou,uc uc,name name,pos pos,code code${(config.is_use_local_user) ? `,local local` : ``} from s_user a`,
  sql_get_01_pgsql: `with recursive tree as (select id from s_ou where id=? union all select c.id from s_ou c,tree p where p.id=c.pid) select a.id,a.mail,a.ou,a.uc,a.name,a.pos,a.code${(config.is_use_local_user) ? `,a.local` : ``} from s_user a,tree b`
}
const sql_get_02 = {
  sql_get_02_mssql: ` where a.ou=b.id`,
  sql_get_02_orcl: ` where ou in (select id from s_ou start with id=? connect by prior id=pid) `,
  sql_get_02_mysql: ` where ou in (select b.id from s_ou_tree a join s_ou b on b.id = a.cid where a.pid=? order by a.path) `,
  sql_get_02_pgsql: ` where a.ou=b.id`
}
const sql_get_03 = {
  sql_get_03_mssql: `with tree as (select ? id union all select c.id from s_ou c,tree p where p.id=c.pid) select count(*) "total" from s_user a,tree b`,
  sql_get_03_orcl: `select count(*) "total" from s_user`,
  sql_get_03_mysql: `select count(*) "total" from s_user`,
  sql_get_03_pgsql: `with recursive tree as (select id from s_ou where id=? union all select c.id from s_ou c,tree p where p.id=c.pid) select count(*) "total" from s_user a,tree b`
}
const Service = {
  member: async (req, res, next) => {
    try {
      const body = req.body, uid = body.uid, arr = body.arr
     // await dbs.query(`delete from s_member where user_id=@1`, [uid])
     await inc.execsqlinsupddel(`delete from s_group_user where user_id=?`, [uid])
      if (arr.length > 0) {
        let binds = []
        for (const rid of arr) {
          binds.push([uid, rid])
          await inc.execsqlinsupddel(`insert into s_group_user(user_id,group_id) values (?,?)`, [uid, rid])
        }
       // await dbs.queries(`insert into s_member(user_id,role_id) values (@1,@2)`, binds)
       
       const sSysLogs = { fnc_id: 'group_user_insert', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm thông tin Tài khoản ${uid} vào nhóm`, doc: JSON.stringify(binds)};
      logging.ins(req,sSysLogs,next)  
      }
          
      res.json(1)
    }
    catch (err) {
      next(err)
    }
  },
  system: async (req, res, next) => {
    try {
      const body = req.body, uid = body.uid, arr = body.arr
     await inc.execsqlinsupddel(`delete from s_user_sys where USERID=?`, [uid])
      if (arr.length > 0) {
        let binds = []
        for (const rid of arr) {
          binds.push([uid, rid])
        }
       await inc.execsqlinsupddel(`insert into s_user_sys(USERID,[SYSTEM]) values (?,?)`, binds)
       const sSysLogs = { fnc_id: 'data_user_insert', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm thông tin Tài khoản ${uid} vào dữ liệu`, doc: JSON.stringify(binds)};
      logging.ins(req,sSysLogs,next)  
      }
          
      res.json(1)
    }
    catch (err) {
      next(err)
    }
  },
  //dung cho scb
  getsystem: async (req, res, next) => {
    try {
      const token = sec.decode(req),uid = token.uid
      let result= await inc.execsqlselect(`select SYSTEM "id",SYSTEM "value" from s_user_sys where USERID=?`, [uid])  
          
      res.json(result)
    }
    catch (err) {
      next(err)
    }
  },
  manager: async (req, res, next) => {
    try {
      const body = req.body, uid = body.uid, arr = body.arr
      await inc.execsqlinsupddel(`delete from s_manager where user_id=?`, [uid])
      if (arr.length > 0) {
        for (const oid of arr) {
          await inc.execsqlinsupddel(`insert into s_manager(user_id, taxc) values (?,?)`, [uid, oid])
        }
      const sSysLogs = { fnc_id: 'data_user_insert', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm thông tin Tài khoản ${uid} vào dữ liệu`};
      logging.ins(req,sSysLogs,next)
      }
      res.json(1)
    }
    catch (err) {
      next(err)
    }
  },
  role: async (req, res, next) => {
    try {
      const uid = req.params.uid
     // const sql = `select x.id,x.name,x.sel from (select a.id,a.name,0 sel from s_role a left join s_member b on a.id = b.role_id and b.user_id=@1 where b.role_id is null union select a.id,a.name,1 sel from s_role a inner join s_member b on a.id = b.role_id and b.user_id=@1) x order by x.id`
      const sql = `select x.id "id",x.name "name",x.sel "sel" from (select a.id,a.name,0 sel from s_group a left join s_group_user b on a.id = b.group_id and b.user_id=? where b.group_id is null union select a.id,a.name,1 sel from s_group a inner join s_group_user b on a.id = b.group_id and b.user_id=?) x order by x.id`
      const result = await inc.execsqlselect(sql, [uid, uid])
      res.json(result)
    }
    catch (err) {
      next(err)
    }
  },
 roledata: async (req, res, next) => {
    try {
      const uid = req.params.uid
      const sql = `select
      x.id "id",
      x.name "name",
      x.sel "sel"
    from
      (
      select
        a.id,
        a.name,
        0 sel
      from
        s_cat a
      left join s_user_sys b on
         a.name = b.[SYSTEM]
        and b.USERID =?
      where
        b.USERID is null and a.[type] = 'SYSTEM_SCB'
    union
      select
        a.id,
        a.name,
        1 sel
      from
        s_cat a
      inner join s_user_sys b on
        a.name = b.[SYSTEM]
        and b.USERID =? and a.[type] = 'SYSTEM_SCB' ) x
    order by
      x.id`
      const result = await inc.execsqlselect(sql, [uid, uid])
      res.json(result)
    }
    catch (err) {
      next(err)
    }
  },									   
  ou: async (req, res, next) => {
    try {
      const token = sec.decode(req), ou = token.ou, uid = req.params.uid
      // const sou = "(select id,pid,taxc,name from s_ou where taxc is not null start with id=? connect by prior id=pid)"
      // const sql = `select x.id "id",x.pid "pid",x.taxc "taxc",x.name "name",x.sel "checked" from (select a.id,a.pid,a.taxc,a.name,0 sel from ${sou} a left join s_manager b on a.taxc=b.taxc and b.user_id=? where b.taxc is null union select a.id,a.pid,a.taxc,a.name,1 sel from ${sou} a inner join s_manager b on a.taxc=b.taxc and b.user_id=?) x start with x.id=? connect by prior x.id=x.pid`
      const sql = `${sql_ou[`sql_ou_${config.dbtype}`]}`
      let binds = (config.dbtype == "mssql") ? [ou,uid,uid] : [ou,uid,ou,uid] // VinhHQ12 sửa gộp DB
      switch (config.dbtype) {
        case "mssql":
          binds= [ou,uid,uid]
          break
        case "orcl":
          binds= [ou,uid,ou,uid, ou]
          break
        case "mysql":
          binds= [ou,uid,ou,uid]
          break
        case "pgsql":
          binds= [ou, uid]
          break
        default:
          binds= []
          break
      } 
      const result = await inc.execsqlselect(sql, binds)
      const rows = result
      let arr = [], obj
      for (const row of rows) {
        if (row.id == ou) {
          row.open = true
          arr.push(row)
        }
        else {
          obj = util.findDFS(arr, row.pid)
          if (obj) {
            if (!obj.hasOwnProperty('data')) obj.data = []
            obj.data.push(row)
          }
        }
      }
      res.json(arr)
    }
    catch (err) {
      next(err)
    }
  },
  get: async (req, res, next) => {
    try {
      const token = sec.decode(req), query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 10, sort = query.sort
      let where = sql_get_02[`sql_get_02_${config.dbtype}`], order = "", sql, result, ret, binds = [token.ou], pagerobj = inc.getsqlpagerandbind(start, count)
      if (query.filter) {
        let filter = JSON.parse(query.filter), val
        let i = 2
        Object.keys(filter).forEach((key) => {
          val = filter[key]
          if (val) {
            if (key == "id" || key == "mail" || key == "name" || key == "pos" || key == "code") {
              switch (config.dbtype) {
                case "mssql":
                  where += ` and upper(a.${key}) like ?`
                  break
                case "orcl":
                  where += ` and upper(${key}) like ?`
                  break
                case "mysql":
                  where += ` and upper(${key}) like ?`
                  break
                case "pgsql":
                  where += ` and upper(a.${key}) like ?`
                  break
                default:
                  binds= []
                  break
              }
              binds.push(`%${val.toUpperCase()}%`)
            } else if (key == "ou") {
              if (config.ent != "vcm") {//tra cuu voi mutiselect
                switch (config.dbtype) {
                  case "mssql":
                    where += ` and a.${key}=?`
                    break
                  case "orcl":
                    where += ` and ${key}=?`
                    break
                  case "mysql":
                    where += ` and ${key}=?`
                    break
                  case "pgsql":
                    where += ` and a.${key}=?`
                    break
                  default:
                    binds= []
                    break
                }
                binds.push(val)
              } else {
                let str = "", arr = String(val).split(",")
                for (const row of arr) {
                  let d = row.split("__")
                  str += `${d[0]},`
                }
                where += ` and a.${key} in (${str.slice(0, -1)})`
              }
            }
            else {
              switch (config.dbtype) {
                case "mssql":
                  where += ` and a.${key}=?`
                  break
                case "orcl":
                  where += ` and ${key}=?`
                  break
                case "mysql":
                  where += ` and ${key}=?`
                  break
                case "pgsql":
                  where += ` and a.${key}=?`
                  break
                default:
                  binds= []
                  break
              }
              binds.push(val)
            }
          }
        })
      }
      if (sort) {
        Object.keys(sort).forEach((key) => {
          order = `order by a.${key}  ${sort[key]}`
        })
      } else order = `order by a.id`
      //const tree = sql_get_01[`sql_get_01_${config.dbtype}`] //`with tree as (select ? id union all select c.id from s_ou c,tree p where p.id=c.pid)`
      sql = `${sql_get_01[`sql_get_01_${config.dbtype}`]} ${where} ${order} ${pagerobj.vsqlpagerstr}`
      for (let t of pagerobj.vsqlpagerbind) binds.push(t)
      result = await inc.execsqlselect(sql, binds)
      ret = { data: result, pos: start }
      if (start == 0) {
        sql = `${sql_get_03[`sql_get_03_${config.dbtype}`]} ${where}`
        let bindscount = []
        for (let i = 0; i < binds.length - 2; i++) bindscount.push(binds[i])
        result = await inc.execsqlselect(sql, bindscount)
        ret.total_count = result[0].total
      }
      const sSysLogs = { fnc_id: 'user_get', src: config.SRC_LOGGING_DEFAULT, dtl: `Tra cứu thông tin NSD`, doc: JSON.stringify(binds)};
      logging.ins(req,sSysLogs,next)
      return res.json(ret)
    }
    catch (err) {
      next(err)
    }
  },
  post: async (req, res, next) => {
    try {
      const body = req.body, operation = body.webix_operation
      let sql, result, binds, code = body.code ? body.code : null
      if (operation == "update") {
        sql = `update s_user set ou=?,name=?,pos=?,code=?,mail=? where id=?`
        binds = [body.ou, body.name, body.pos, code, body.mail, body.id]
      }
      result = await inc.execsqlinsupddel(sql, binds)
      res.json(result)
      const sSysLogs = { fnc_id: 'user_update', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật thông tin Tài khoản ${body.id}`, msg_id: body.id, doc: JSON.stringify(body)};
      logging.ins(req,sSysLogs,next)
    }
    catch (err) {
      next(err)
    }
  },
  ubr: async (req, res, next) => {
    try {
      const token = sec.decode(req), rid = req.params.rid
      // const where = "and a.ou in (select id from s_ou start with id=? connect by prior id=pid)"
      // const sql = `select x.id "acc",x.name "name",x.sel "sel",x.mail "mail",x.ou "ou",x.uc "uc",x.pos "pos" from (select a.id,a.name,0 sel,a.mail,a.ou,a.uc,a.pos from s_user a left join s_member b on a.id = b.user_id and b.role_id=? where b.user_id is null ${where} union select a.id,a.name,1 sel,a.mail,a.ou,a.uc,a.pos from s_user a inner join s_member b on a.id = b.user_id and b.role_id=? ${where}) x order by x.id`
      const sql = `${sql_ubr[`sql_ubr_${config.dbtype}`]}`
      let binds
      if(`${config.dbtype}` == "mssql") binds = [token.ou, rid, rid]
      if(`${config.dbtype}` == "orcl") binds = [rid, token.ou, rid, token.ou]
      if(`${config.dbtype}` == "mysql") binds = [rid, token.ou, rid, token.ou]
      if(`${config.dbtype}` == "pgsql") binds = [token.ou, rid, rid]
      const result = await inc.execsqlselect(sql, binds)
      res.json(result)
    } catch (err) {
      next(err)
    }
  },
  mbr: async (req, res, next) => {
    try {
      const body = req.body, rid = body.rid, users = body.users
      await inc.execsqlinsupddel(`delete from s_member where role_id=?`, [rid])
      if (users.length > 0) {
        let binds = []
        for (const uid of users) {
          binds.push([rid, uid])
        }
        await inc.execsqlinsupddel(`insert into s_member(role_id,user_id) values (?,?)`, binds)
      }
      res.json(1)
    } catch (err) {
      next(err)
    }
  },
  disable: async (req, res, next) => {
    try {
      const uid = req.params.uid
      await inc.execsqlinsupddel(`update s_user set uc=? where id=?`, [2, uid])
      let row = await Service.obid(uid)
      const sSysLogs = { fnc_id: 'user_disable', src: config.SRC_LOGGING_DEFAULT, dtl: `Hủy bỏ Tài khoản ${uid}`, msg_id: uid, doc: JSON.stringify(row)};
      logging.ins(req,sSysLogs,next)
      res.send(`Tài khoản ${uid} đã bị hủy bỏ \n (The account ${uid} has been canceled)`)
    } catch (err) {
      next(err)
    }
  },
  enable: async (req, res, next) => {
    try {
      const uid = req.params.uid
      await inc.execsqlinsupddel(`update s_user set uc=? where id=?`, [1, uid])
      let row = await Service.obid(uid)
      const sSysLogs = { fnc_id: 'user_enable', src: config.SRC_LOGGING_DEFAULT, dtl: `Kích hoạt Tài khoản ${uid}`, msg_id: uid, doc: JSON.stringify(row)};
      logging.ins(req,sSysLogs,next)
      res.send(`Tài khoản ${uid} đã được kích hoạt \n (The account ${uid} has been activated)`)
    } catch (err) {
      next(err)
    }
  },
  ins: async (req, obj) => {
    try {
      let ou = obj.ou
      if (!ou) {
        const token = sec.decode(req)
        ou = token.ou
      }
      const result = await inc.execsqlinsupddel(`insert into s_user(id,code,name,ou,mail,pos${(config.is_use_local_user) ? `,local,pass` : ``}) values (?,?,?,?,?,?${(config.is_use_local_user) ? `,?,?` : ``})`, (config.is_use_local_user) ? [obj.id, (obj.code) ? obj.code : null, obj.name, ou, obj.mail, obj.pos, obj.local,obj.pass] : [obj.id, (obj.code) ? obj.code : null, obj.name, ou, obj.mail, obj.pos])
      const sSysLogs = { fnc_id: 'user_insert', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm thông tin Tài khoản ${obj.id}`, msg_id: obj.id, doc: JSON.stringify(obj)};
      logging.ins(req,sSysLogs)
      return result
    } catch (err) {
      throw err
    }
  },
  obid: async (id) => {
    const sql = `select id "id",mail "mail",ou "ou",uc "uc",name "name",pos "pos",code "code"${(config.is_use_local_user) ? `,local "local",change_pass_date "change_pass_date",login_number "login_number"` : ``} from s_user where id=?`
    const result = await inc.execsqlselect(sql, [id])
    let row = result
    return row
  },
  upduserst: async (id) => {
    const sql = sql_upduserst[`sql_upduserst_${config.dbtype}`]
    await inc.execsqlinsupddel(sql, [id])
  },
  xls: async (req, res, next) => {
    try {
      const token = sec.decode(req), query = req.query, sort = query.sort
      let where = " where a.ou=b.id", order = "", sql, result, ret, binds = [token.ou], rows, json
      if (query.filter) {
        let filter = JSON.parse(query.filter), val
        let i = 2
        Object.keys(filter).forEach((key) => {
          val = filter[key]
          if (val) {
            if (key == "id" || key == "mail" || key == "name" || key == "pos" || key == "code") {
              where += ` and upper(a.${key}) like ?`
              binds.push(`%${val.toUpperCase()}%`)
            } else if (key == "ou") {
              if (config.ent != "vcm") {//tra cuu voi mutiselect
                where += ` and a.${key}=?`
                binds.push(val)
              } else {
                let str = "", arr = String(val).split(",")
                for (const row of arr) {
                  let d = row.split("__")
                  str += `${d[0]},`
                }
                where += ` and a.${key} in (${str.slice(0, -1)})`
              }
            }
            else {
              where += ` and a.${key}=?`
              binds.push(val)
            }
          }
        })
      }
      if (sort) {
        Object.keys(sort).forEach((key) => {
          order = `order by a.${key}  ${sort[key]}`
        })
      } else order = `order by a.id`
      const tree = `with tree as (select ? "id" union all select c.id from s_ou c,tree p where p.id=c.pid)`
      sql = `${tree} select a.id "id",a.mail "mail",a.ou "ou",a.uc "uc",a.name "name",a.pos "pos",a.code "code",FORMAT(a.last_login,'dd/MM/yyyy') "last_login",FORMAT(a.create_date,'dd/MM/yyyy') "create_date",FORMAT(a.update_date,'dd/MM/yyyy') "update_date",(select name "name"  from s_ou where id= a.ou ) ou_name   from s_user a,tree b ${where} ${order} `
      result = await inc.execsqlselect(sql, binds)
      rows = result
      for (const row of rows) {
        let rowgrs
        row.appname = "eInvoice"
        sql = `select name "name", des "des"  from s_group where id in (select group_id from s_group_user s where user_id= ?) `
        result = await inc.execsqlselect(sql, [row.id])
        rowgrs = result
        row.gr_name = ''
        row.gr_detail = ''
        for(const rowgr of rowgrs){
          row.gr_name+= rowgr.name +'\t\n'
          row.gr_detail+=rowgr.des+ '\t\n'
        }
        row.type = "USER"
        row.priviliged_id = "NO"
        row.active = "TRUE"

      }
      let fn = "temp/USER_LIST_SCB.xlsx"

      json = { table: rows }
      const file = path.join(__dirname, "..", "..", "..", fn)
      const xlsx = fs.readFileSync(file)
      const template = new xlsxtemp(xlsx)
      template.substitute(1, json)
      res.end(template.generate(), "binary")
    }
    catch (err) {
      logger4app.debug("excel" + err)

      next(err)


    }
  },
  getUserFromOUD: async (arruser) => {
    let sql, result
    if (!(arruser && arruser.length > 0)) return
    //Disable hết các user hiện có
    await inc.execsqlinsupddel(`update s_user set uc=2, update_date = GETDATE() where CHARINDEX('__', id) <= 0`, [])
    //Delete toàn bộ bảng user group để insert lại
    await inc.execsqlinsupddel(`delete from s_group_user where CHARINDEX('__', user_id) <= 0`, [])
    //Delete toàn bộ bảng user dept để insert lại
    //await dbs.query(`delete from s_deptusr where CHARINDEX('__', usrid) <= 0 and deptid = '0'`, [])
    //Lặp danh sách user truyền vào 
    for (let u of arruser) {
      try {
        //Update dữ liệu user trước
        sql = `UPDATE [dbo].[s_user]
                  SET [code] = ?
                    ,[mail] = ?
                    ,[uc] = 1
                    ,[name] = ?
                    ,[pos] = ?
                    ,[update_date] = GETDATE()
                WHERE [id] = ?`
        result = await inc.execsqlinsupddel(sql, [(u.code) ? u.code : null, u.mail, u.name, u.pos, u.id])
        //Nếu không có thì insert user và insert bảng user dept
        if (result <= 0) {
          result = await inc.execsqlinsupddel(`insert into s_user(id,code,name,ou,mail,pos) values (?,?,?,?,?,?)`, [u.id, (u.code) ? u.code : null, u.name, u.ou, u.mail, u.pos])
        }
        //await dbs.query(`insert into s_deptusr(deptid,usrid) values (@1,@2)`, ['0', u.id])
        //Insert group
        let arrgr = u.usergroup
        if (arrgr.length > 0) {
          let binds = []
          for (const rname of arrgr) {
            sql = `select id "id" from s_group where name = ?`
            result = await inc.execsqlselect(sql, [rname])
            if (result.recordset && result.length > 0) {
              binds.push([u.id, result.id])
            }

          }

          await inc.execsqlinsupddel(`insert into s_group_user(user_id,group_id) values (?,?)`, binds)
          
        }
      } catch (ex) {
        logger4app.error(`Update user ${u.id}: ` + ex)
      }

    }

  },
  groupAll: async () => {
    const sql = `select name "name" from s_group order by name`
    const result = await inc.execsqlselect(sql, [])
    let rows = result
    return rows

  },
  soc: async (req, res, next) => {
    try {
      const query = req.query
      let binds, result, records
      //chỉ lấy những trạng thái là hiệu lực
      let where = `where uc = 1`
      binds = []
      let i = 1
      if (query.id) {
        where += ` and s.id like ?`
        binds.push(`%${query.id}%`)
      }
      if (query.name) {
        where += ` and s.name like ?`
        binds.push(`%${query.name}%`)
      }
      if (query.pos) {
        where += ` and s.pos like ?`
        binds.push(`%${query.pos}%`)
      }
      if (query.mail) {
        where += ` and s.mail = ?`
        binds.push(query.mail)
      }
      if (query.ou != "*") {
        where += ` and s.ou=?`
        binds.push(query.ou)
      }
     
      let sql = `select 'VN-EVAT' System_code, s.name "Name", s.id "Login_Account_Nbr", gr.NAME "Name Gruop", format(last_login, 'yyyyMMdd') "Last_Login_Date" from s_user s inner join s_group_user h on s.id = h.user_id inner join s_group gr on group_id = gr.id ${where} group by code, s.id, group_id, last_login, gr.name, s.name order by s.id`
      result = await inc.execsqlselect(sql, binds);
      records = result
      const jsonData = JSON.parse(JSON.stringify(records));
      const json2csvParser = new Json2csvParser({ header: false, default: '', quote: '', encoding: 'utf8' });
      const csv = json2csvParser.parse(jsonData);
      res.end(csv, 'utf8')
    } catch (err) {
      logger4app.debug(err);
    }
  },
  updatelocaluserpass: async (id, oldpass, newpass) => {
    let sql,result,binds,rows

    //Check xem old pass co dung hay khong
    sql = `select count(*) "countret" from s_user where pass = ? and id = ?`
    binds = [oldpass, id]
    result = await inc.execsqlselect(sql, binds)
    if (result.countret <= 0) {
      throw new Error(`Mật khẩu cũ không đúng \n Old password is incorrect`)
    }

    //Check xem new pass co trung voi cac mat khau cu hay khong
    sql = `SELECT "pass" "pass", "change_date" "change_date" FROM (  
      select pass "pass", change_pass_date "change_date" from s_user where id = ? 
      union all 
      select pass "pass", change_date "change_date" from s_user_pass where user_id = ? ) 
      order by "change_date" desc 
      OFFSET 0 ROWS FETCH NEXT ? ROWS ONLY`
    binds = [id, config.total_pass_store - 1]
    result = await inc.execsqlselect(sql, binds)
    rows =  result
    for (const row of rows) {
      if (row.pass == newpass) {
        throw new Error(`Mật khẩu không được trùng lặp với ${config.total_pass_store} mật khẩu cũ \n Password cannot be the same as ${config.total_pass_store} old passwords`)
      }
    }
    
    //Insert mat khau cu vao bang s_user_pass
    await dbs.query(`insert into s_user_pass(user_id,pass,change_date) values (@1,@2,getdate())`, [id, oldpass])

    //Update mat khau moi
    sql = `UPDATE s_user
              SET [pass] = ?
              ,[change_pass_date] = getdate()
              ,[change_pass] = 1
            WHERE [id] = ?`
    result = await inc.execsqlinsupddel(sql, [newpass, id])
  },
  resetlocaluserpass: async (id, mail, newpass) => {
    let sql,result,binds,rows

    //Check xem old pass co dung hay khong
    sql = `select count(*) "countret" from s_user where mail = ? and id = ?`
    binds = [mail, id]
    result = await inc.execsqlinsupddel(sql, binds)
    if (result.countret <= 0) {
      throw Error(`Tài khoản ${id} Mail ${mail} không tồn tại \n (Account ${id} Mail ${mail} does not exist)`)
    }

    //Update mat khau moi
    sql = `UPDATE s_user
              SET [pass] = ?
              ,[change_pass_date] = getdate()
              ,[change_pass] = 0
            WHERE [id] = ?`
    result = await inc.execsqlinsupddel(sql, [newpass, id])
  }
}
module.exports = Service 