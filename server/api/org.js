"use strict"
const config = require("./config")
const logger4app = require("./logger4app")
const inc = require("./inc")
const logging = require("./logging")
const ENT = config.ent
const where_get = {
    where_get_mssql: ` where contains(*,?)`,
    where_get_orcl: ` where CONTAINS(name,?,1)>0`,
    where_get_mysql: ` where MATCH(taxc, code, name, name_en, fadd, tel, mail, acc, bank, c0, c1, c2, c3, c4, c5, c6) AGAINST (? IN NATURAL LANGUAGE MODE)`,
    where_get_pgsql: ``
}
const Service = {
    fbi: async (req, res, next) => {
        try {
            const result = await inc.execsqlselect(`select id "id",taxc "taxc",name "name",fadd "fadd",mail "mail",tel "tel",acc "acc",bank "bank",code "code" from s_org where id=?`, [req.params.id])
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    ous: async (req, res, next) => {
        try {
            const query = req.query, filter = query.filter
            let rows
            if (filter && filter.value) {
                    const sql = `select id "id",name "name",taxc "taxc",addr "addr",mail "mail",tel "tel",acc "acc",bank "bank",prov "prov",dist "dist",ward "ward",fadd "fadd",code "code" from s_org where contains(*,?) order by taxc ${inc.getsqlpager(0, config.limit)}`
                    // OFFSET 0 ROWS fetch first ${config.limit} rows only
                    const result = await inc.execsqlselect(sql, [ `"${filter.value}"`])
                    rows = result
            }
            res.json(rows)
        }
        catch (err) {
            next(err)
        }
    },
    fbn: async (req, res, next) => {
        try {
            const query = req.query, filter = query.filter
            let rows, sql, binds
            
            if (filter && filter.value)  {
                switch (config.dbtype) {
                    case "mssql":
                        sql = sql = `select taxc "taxc",name "name",fadd "fadd",mail "mail",tel "tel",acc "acc",bank "bank",code "code", c2 "c2" from s_org where name like '%${filter.value}%' or code like '%${filter.value}%' or vtaxc like '%${filter.value}%' order by taxc ${inc.getsqlpager(0,config.limit )}`
                        binds = [`"${filter.value}"`]
                        break
                    case "orcl":
                        sql = `select SCORE(1),taxc "taxc",name "name",fadd "fadd",mail "mail",tel "tel",acc "acc",bank "bank",code "code" from s_org where CONTAINS(name,?,1)>0 ORDER BY SCORE(1) ${inc.getsqlpager(0,config.limit )}`
                        binds = [`{${filter.value}}`]
                        break
                    case "mysql":
                        sql = `select taxc "taxc",name "name",fadd "fadd",mail "mail",tel "tel",acc "acc",bank "bank",code "code" from s_org where MATCH(taxc, code, name, name_en, fadd, tel, mail, acc, bank, c0, c1, c2, c3, c4, c5, c6) AGAINST (? IN NATURAL LANGUAGE MODE) ORDER BY id DESC ${inc.getsqlpager(0,config.limit )}`
                        binds = [`${filter.value}`]
                        break
                    case "pgsql":
                        let val, phrase
                        val = filter.value.trim()
                        if (val.indexOf(' ') == -1) {
                            val = `${val}:*`
                            phrase = ""
                        }
                        else phrase = "phrase"
                        sql = `select taxc,name,fadd,mail,tel,acc,bank,ts_rank(fts,tsq) rank,code from s_org,${phrase}to_tsquery($1) tsq where fts @@ tsq order by rank desc ${inc.getsqlpager(0,config.limit )}`
                        binds = [val]
                        break
                    default:
                        sql = ``
                        binds = []
                        break
                }
                const result = await inc.execsqlselect(sql)
                rows = result
            }
            res.json(rows)
        }
        catch (err) {
            next(err)
        }
    },
    get: async (req, res, next) => {
        try {
            const query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 10,sort = query.sort
            let code= query.code, name = query.name, taxc = query.taxc
            let rows, where1='',sql,result, order = "", pagerobj = inc.getsqlpagerandbind(start, count), where = ''

            
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = ` order by ${key} ${sort[key]}`
                })
            } else order = ` order by taxc `

            if (code || name || taxc) {
                if(`${config.dbtype}` == "pgsql") {
                    name = name.trim()
                    let phrase
                    if (name.indexOf(' ') == -1) {
                        name = `${name}:*`
                        phrase = ""
                    }
                    else phrase = "phrase"
                    where_get[where_get_pgsql] = `,${phrase}to_tsquery($1) tsq`
                }
                // where = `${where_get[`where_get_${config.dbtype}`]}`

                if(code != '') where1 += `code like N'${code}%'`
                if(name != '') where1 += ` and name like N'${name}%'`
                if(taxc != '') where1 += ` and taxc like N'${taxc}%'`
                
                if(where1.startsWith(' and')) where = where1.slice(5, where1.length)
                else where = where1
                where = 'where ' + where
                console.log(where)
                let binds = []
                // binds.push(name)
                for (let t of pagerobj.vsqlpagerbind) binds.push(t)
                if(ENT == 'dbs'){
                    sql = `select id "id",code "code",taxc "taxc",name "name",name_en "name_en",addr "addr",prov "prov",dist "dist",ward "ward",mail "mail",tel "tel",acc "acc",bank "bank",fadd "fadd",c0 "c0",c1 "c1",case when c2 = 0 then 'Daily' when c2 = 1 then 'MonthLy' else '' end "c2",c3 "c3",c4 "c4",c5 "c5",c6 "c6",c7 "c7",c8 "c8",c9 "c9",type "type" from s_org ${where} ${order} ${pagerobj.vsqlpagerstr}`
                }else{
                    sql = `select id "id",code "code",taxc "taxc",name "name",name_en "name_en",addr "addr",prov "prov",dist "dist",ward "ward",mail "mail",tel "tel",acc "acc",bank "bank",fadd "fadd",c0 "c0",c1 "c1",c2 "c2",c3 "c3",c4 "c4",c5 "c5",c6 "c6",c7 "c7",c8 "c8",c9 "c9",type "type" from s_org ${where} ${order} ${pagerobj.vsqlpagerstr}`
                }
                result = await inc.execsqlselect(sql, binds)
               
                rows = result
            } else {
                if( ENT == 'dbs'){
                    sql = `select id "id",code "code",taxc "taxc",name "name",name_en "name_en",addr "addr",prov "prov",dist "dist",ward "ward",mail "mail",tel "tel",acc "acc",bank "bank",fadd "fadd",c0 "c0",c1 "c1",case when c2 = 0 then 'Daily' when c2 = 1 then 'MonthLy' else '' end "c2",c3 "c3",c4 "c4",c5 "c5",c6 "c6",c7 "c7",c8 "c8",c9 "c9",type "type" from s_org ${order} ${pagerobj.vsqlpagerstr}`
                }else{
                    sql = `select id "id",code "code",taxc "taxc",name "name",name_en "name_en",addr "addr",prov "prov",dist "dist",ward "ward",mail "mail",tel "tel",acc "acc",bank "bank",fadd "fadd",c0 "c0",c1 "c1",c2 "c2",c3 "c3",c4 "c4",c5 "c5",c6 "c6",c7 "c7",c8 "c8",c9 "c9",type "type" from s_org ${order} ${pagerobj.vsqlpagerstr}`
                }
                result = await inc.execsqlselect(sql, pagerobj.vsqlpagerbind)
               
                rows = result
            }
            let ret = { data: rows, pos: start }
            if (start == 0) {
                if (name || code || taxc){
                    sql = `select count(*) "total" from s_org ${where}`
                    result = await inc.execsqlselect(sql, [])
                }else  {
                    sql = `select count(*) "total" from s_org`
                    result = await inc.execsqlselect(sql, [])
                }
               
                ret.total_count = result[0].total
            }
            return res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const body = req.body, operation = body.webix_operation
            let binds, sql, result, sSysLogs = { fnc_id: '', src: config.SRC_LOGGING_DEFAULT, dtl: ``}
            let email = ''
            email = body.mail
            //mails ngăn cách với nhau bằng dấu ; với ctbc
            if( ENT == 'ctbc') email = email.replace(/,/g, ';')
            const taxc = body.taxc ? body.taxc : null, code = body.code ? body.code : null
            if (operation == "update") {
                sql = `update s_org set code=?,taxc=?,name=?,name_en=?,prov=?,dist=?,ward=?,addr=?,mail=?,tel=?,acc=?,bank=?,c0=?,c1=?,c2=?,c3=?,c4=?,c5=?,c6=?,c7=?,c8=?,c9=? where id=?`
                binds = [code, taxc, body.name, body.name_en, body.prov, body.dist, body.ward, body.addr, email, body.tel, body.acc, body.bank, body.c0, body.c1, body.c2, body.c3, body.c4, body.c5, body.c6, body.c7, body.c8, body.c9, body.id]
                sSysLogs = { fnc_id: 'org_upd', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật thông tin tổ chức id ${body.id}`, msg_id: body.id, doc: JSON.stringify(body) };
            }
            else if (operation == "insert") {
                sql = `insert into s_org(code,taxc,name,name_en,prov,dist,ward,addr,mail,tel,acc,bank,c0,c1,c2,c3,c4,c5,c6,c7,c8,c9) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`
                binds = [code, taxc, body.name, body.name_en, body.prov, body.dist, body.ward, body.addr, email, body.tel, body.acc, body.bank, body.c0, body.c1, body.c2, body.c3, body.c4, body.c5, body.c6, body.c7, body.c8, body.c9]
                sSysLogs = { fnc_id: 'org_ins', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm mới thông tin tổ chức: ${JSON.stringify(body)}`, msg_id: body.id, doc: JSON.stringify(body)};
            }
            else if (operation == "delete") {
                sql = `delete from s_org where id=?`
                binds = [body.id]
                sSysLogs = { fnc_id: 'org_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa thông tin tổ chức id ${body.id}`, msg_id: body.id, doc: JSON.stringify(body)};
            }
            result = await inc.execsqlinsupddel(sql, binds)
            if (operation == "insert") {
                let result_id = await inc.execsqlselect(`select id "id" from s_org where code=?`, [body.code])
                let id_ins, rows_ins = result_id
                if (rows_ins.length > 0) id_ins = rows_ins[0].id
                sSysLogs.msg_id = id_ins
            }
            logging.ins(req,sSysLogs,next) 
            if (operation == "insert") res.json(result)
            else res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    ins: async (req, res, next) => {
        try {
            const body = req.body
            const cols = ['name', 'name_en', 'taxc', 'code', 'addr', 'tel', 'mail', 'acc', 'bank', 'c0', 'c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9']
            let arr = []
            const sql = `insert into s_org(name,name_en,taxc,code,addr,tel,mail,acc,bank,c0,c1,c2,c3,c4,c5,c6,c7,c8,c9) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`
            for (const row of body) {
                let id = row.id
                try {
                    for (const col of cols) {
                        if (!row.hasOwnProperty(col)) row[col] = null
                    }
                    delete row["id"]
                    delete row["chk"]
                    //await dbs.query(sql, row)
                    await inc.execsqlinsupddel(sql, [row.name,row.name_en,row.taxc,row.code,row.addr,row.tel,row.mail,row.acc,row.bank,row.c0,row.c1,row.c2,row.c3,row.c4,row.c5,row.c6,row.c7,row.c8,row.c9])
                    const sSysLogs = { fnc_id: 'org_ins', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm mới thông tin tổ chức: ${JSON.stringify(row)}`, msg_id: id, doc: JSON.stringify(row)};
                    logging.ins(req,sSysLogs,next) 
                    arr.push({ id: id, error: "OK" })
                } catch (err) {
                    let msg
                    switch (err.number) {
                        case 2627:
                            msg = "Bản ghi đã tồn tại/The record already exists"
                            break
                        case 2601:
                            msg = "Bản ghi đã tồn tại/The record already exists"
                            break
                        case 547:
                            msg = "Không xóa được bản ghi cha/Cannot delete parent record"
                            break
                        default:
                            msg = err.message
                    }
                    arr.push({ id: id, error: msg })
                }
            }
            res.json(arr)
        }
        catch (err) {
            next(err)
        }
    },
    obbcode: async (code) => {
        try {
            let rows, ob
            const sql = `select id "id",code "code",taxc "taxc",name "name",name_en "name_en",addr "addr",prov "prov",dist "dist",ward "ward",mail "mail",tel "tel",acc "acc",bank "bank",fadd "fadd",c0 "c0",c1 "c1",c2 "c2",c3 "c3",c4 "c4",c5 "c5",c6 "c6",c7 "c7",c8 "c8",c9 "c9",type "type" from s_org where code = ?`
            const result = await inc.execsqlselect(sql, [code])
            rows = result
            ob = (rows && rows.length > 0) ? rows[0] : null
            return ob
        }
        catch (err) {
            next(err)
        }
    }
}
module.exports = Service