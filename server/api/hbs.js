"use strict"
const handlebars = require("handlebars")
const moment = require("moment")
const numeral = require("numeral")
const config = require("./config")
const logger4app = require("./logger4app")
const n2w = require("./n2w")
const n2wen = require("./n2wen")
const util = require("./util")
const mfd = config.mfd

const isNAN = (val) => {
    if (val == null || (typeof val === "number" && isNaN(val))) return true
    if (typeof val === "string") {
        const str = val.replace(/[^\w]/gi, '')
        if (str === "" || isNaN(str)) return true
    }
    return false
}

const operators = {
    '==': function (l, r) { return l == r },
    '===': function (l, r) { return l === r },
    '!=': function (l, r) { return l != r },
    '!==': function (l, r) { return l !== r },
    '&lt;': function (l, r) { return l < r },
    '&gt;': function (l, r) { return l > r },
    '&lt;=': function (l, r) { return l <= r },
    '&gt;=': function (l, r) { return l >= r },
    'typeof': function (l, r) { return typeof l == r }
}

numeral.register("locale", "vi", {
    delimiters: { thousands: ".", decimal: "," },
    abbreviations: { thousand: " nghìn", million: " triệu", billion: " tỷ", trillion: " nghìn tỷ" },
    ordinal: function () { return "." },
    currency: { symbol: "₫" }
})

numeral.locale("vi")

handlebars.registerHelper("tygia", (curr, exrt) => {
    if (curr !== "VND") return `Loại tiền: ${curr} - Tỷ giá: ${numeral(exrt).format('0,0.[00]')}`
    else return ""
})

handlebars.registerHelper("if_eq", function () {
    const args = Array.prototype.slice.call(arguments, 0, -1)
    const options = arguments[arguments.length - 1]
    const allEqual = args.every(function (expression) {
        return args[0] === expression
    })
    return allEqual ? options.fn(this) : options.inverse(this)
})


handlebars.registerHelper("n2wen", (num, curr) => {
    return n2wen.n2w(num, curr)
})

handlebars.registerHelper("n2w", (num, curr) => {
    return n2w.n2w(num, curr)
})
handlebars.registerHelper("countstr", str => {

    return "trung"
})
handlebars.registerHelper("dif", num => {
    if (!num) return ""
    if (num > 0) return " giảm"
    else return " tăng"
})

handlebars.registerHelper("fd", date => {
    if (date) return moment(date).format(mfd)
    else return ""
})

handlebars.registerHelper("now", () => {
    return moment().format(mfd)
})

handlebars.registerHelper("fn", num => {
    numeral.locale("vi")
    if (isNAN(num)) return null
    return numeral(num).format()
})

handlebars.registerHelper("fne", num => {
    numeral.locale("en")
    if (isNAN(num)) return null
    return numeral(num).format()
})

handlebars.registerHelper("fn2", num => {
    numeral.locale("vi")
    if (isNAN(num)) return null
    return numeral(num).format('0,0.[00]')
})

handlebars.registerHelper("fn3", num => {
    numeral.locale("vi")
    if (isNAN(num)) return null
    return numeral(num).format('0,0.[000]')
})

handlebars.registerHelper("fn4", num => {
    numeral.locale("vi")
    if (isNAN(num)) return null
    return numeral(num).format('0,0.[0000]')
})

handlebars.registerHelper("ff2", num => {
    numeral.locale("vi")
    if (isNAN(num)) return null
    return numeral(num).format('0,0.00')
})

handlebars.registerHelper("ff3", num => {
    numeral.locale("vi")
    if (isNAN(num)) return null
    return numeral(num).format('0,0.000')
})

handlebars.registerHelper("fne2", num => {
    numeral.locale("en")
    if (isNAN(num)) return null
    return numeral(num).format('0,0.[00]')
})

handlebars.registerHelper("fne3", num => {
    numeral.locale("en")
    if (isNAN(num)) return null
    return numeral(num).format('0,0.[000]')
})

handlebars.registerHelper("fne4", num => {
    numeral.locale("en")
    if (isNAN(num)) return null
    return numeral(num).format('0,0.[0000]')
})

handlebars.registerHelper("ffe2", num => {
    numeral.locale("en")
    if (isNAN(num)) return null
    return numeral(num).format('0,0.00')
})

handlebars.registerHelper("ffe3", num => {
    numeral.locale("en")
    if (isNAN(num)) return null
    return numeral(num).format('0,0.000')
})

handlebars.registerHelper("substr", function (str, start, len, opts) {
    if (str) return str.substr(start, len)
    else return ""
})

handlebars.registerHelper("compare", function (lvalue, operator, rvalue, opts) {
    if (arguments.length < 4) throw new Error("Handlerbars compare needs 3 parameters");
    if (!operators[operator]) throw new Error("Handlerbars compare doesn't know the operator " + operator)
    let result = operators[operator](lvalue, rvalue)
    if (result) return opts.fn(this)
    else return opts.inverse(this)
})

handlebars.registerHelper("comp", function (v1, operator, v2, options) {
    switch (operator) {
        case '==':
            return (v1 == v2) ? options.fn(this) : options.inverse(this)
        case '===':
            return (v1 === v2) ? options.fn(this) : options.inverse(this)
        case '!=':
            return (v1 != v2) ? options.fn(this) : options.inverse(this)
        case '!==':
            return (v1 !== v2) ? options.fn(this) : options.inverse(this)
        case '<':
            return (v1 < v2) ? options.fn(this) : options.inverse(this)
        case '<=':
            return (v1 <= v2) ? options.fn(this) : options.inverse(this)
        case '>':
            return (v1 > v2) ? options.fn(this) : options.inverse(this)
        case '>=':
            return (v1 >= v2) ? options.fn(this) : options.inverse(this)
        case '&&':
            return (v1 && v2) ? options.fn(this) : options.inverse(this)
        case '||':
            return (v1 || v2) ? options.fn(this) : options.inverse(this)
        default:
            return options.inverse(this)
    }
})


handlebars.registerHelper("math", function (lvalue, operator, rvalue, opts) {
    lvalue = parseFloat(lvalue)
    rvalue = parseFloat(rvalue)
    return { "+": lvalue + rvalue, "-": lvalue - rvalue, "*": lvalue * rvalue, "/": lvalue / rvalue, "%": lvalue % rvalue }[operator]
})


handlebars.registerHelper("eachsort", function (arr, key, opts) {
    arr = arr.sort((a, b) => {
        let ak = a[key], bk = b[key]
        return ak - bk
        /*
        if (ak > bk) return 1
        else if (ak == bk) return 0
        else return -1
        */
    })
    let result = ''
    for (var i = 0; i < arr.length; i++) {
        result += opts.fn(arr[i])
    }
    return result
})

handlebars.registerHelper("eachwhen", function (arr, k, v, opts) {
    let result = ''
    for (var i = 0; i < arr.length; ++i) {
        const val = arr[i]
        if (val[k] == v) result += opts.fn(val)
    }
    return result
})
handlebars.registerHelper('encodeMyString',function(inputData){
    return encodeURIComponent(inputData);
     })

handlebars.registerHelper("taxone", function (arr, opts) {
    let result = '', b
    for (var i = 0; i < arr.length; ++i) {
        if (arr[i].hasOwnProperty("vat")) {
            b = true
            break
        }
    }
    if (b) {
        for (var i = 0; i < arr.length; ++i) {
            if (arr[i].hasOwnProperty("vat")) result += opts.fn(arr[i])
        }
    }
    else {
        for (var i = 0; i < arr.length; ++i) {
            result += opts.fn(arr[i])
        }
    }
    return result
})
exports.j2h = async (doc, idx) => {
    const fn = util.fn(doc.form, idx)
    const source = await util.template(fn)
    const template = handlebars.compile(source)
    const htm = template(doc)
    return htm
}
exports.j2m = async (doc) => {
    const source = await util.tempmail(doc.status)
    const template = handlebars.compile(source)
    const htm = template(doc)
    return htm
} 
exports.j2sms = async (doc, smscontent) => {
    const source = smscontent
    const template = handlebars.compile(source)
    const htm = template(doc)
    return htm
} 