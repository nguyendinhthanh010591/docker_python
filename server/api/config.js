const file = "mssql"

const cons = require(`./conf/${file}/cons`)
const conf = require(`./conf/${file}/${file}`)
module.exports = { ...conf, ...cons,file }
