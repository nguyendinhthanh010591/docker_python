"use strict"
const inc = require("./inc")
const util = require("./util")
const redis = require("./redis")
const sec = require("./sec")
const config = require("./config")
const logger4app = require("./logger4app")
const logging = require("./logging")
const ENT = config.ent
const moment = require("moment")
const sql_taxn = {
    sql_taxn_mssql: `taxc id,name value`,
    sql_taxn_orcl: `taxc "id",taxc||' | '||code||' | '||name "value"`,
    sql_taxn_mysql: `taxc "id",CONCAT_WS(' | ',taxc,name) "value"`,
    sql_taxn_pgsql: `taxc id,name as value`
}
const sql_taxt = {
    sql_taxt_mssql: `taxc id,CONCAT(taxc,'-',name) value`,
    sql_taxt_orcl: `taxc "id",taxc||' | '||code||' | '||name "value"`,
    sql_taxt_mysql: `taxc "id",CONCAT(taxc,'-',name) "value"`,
    sql_taxt_pgsql: `taxc id,CONCAT(taxc,'-',name) as value`
}
const sql_get = {
    sql_get_mssql: `with tree as (
        select id,taxc,mst,status,code,name,pid,addr,fadd,prov,dist,ward,mail,tel,acc,bank,paxo,taxo,sign,seq,0 level,'('+CONVERT(VARCHAR(max),id)+'-0)' idx, temp temp,c0 c0,receivermail,dsignissuer,dsignto,dsignfrom,dsignsubject,dsignserial,place,chk_revoke from s_ou where pid is null
        union all
        select c.id,c.taxc,c.mst,c.status,c.code,c.name,c.pid,c.addr,c.fadd,c.prov,c.dist,c.ward,c.mail,c.tel,c.acc,c.bank,c.paxo,c.taxo,c.sign,c.seq,p.level+1 level,p.idx+'*('+CONVERT(VARCHAR(max),c.id)+'-'+CONVERT(VARCHAR(max),p.level)+')' idx, c.temp temp,c.c0 c0,c.receivermail,c.dsignissuer,c.dsignto,c.dsignfrom,c.dsignsubject,c.dsignserial,c.place, c.chk_revoke from s_ou c,tree p where p.id=c.pid) 
        select * from tree order by idx`,
    sql_get_orcl: `select id "id",code "code",taxc "taxc",mst "mst",name "name",pid "pid",addr "addr",fadd "fadd",prov "prov",dist "dist",ward "ward",mail "mail",tel "tel",acc "acc",bank "bank",paxo "paxo",taxo "taxo",sign "sign",seq "seq",status "status",level "level",usr "usr",temp "temp",receivermail "receivermail",dsignissuer "dsignissuer",dsignto "dsignto",dsignfrom "dsignfrom",dsignsubject "dsignsubject",dsignserial "dsignserial",place "palce" from s_ou start with pid is null connect by PRIOR id=pid`,
    sql_get_mysql: `select b.id id,b.taxc taxc,b.mst mst,b.status status,b.code code,b.name name,b.pid pid,b.addr addr,b.fadd fadd,b.prov prov,b.dist dist,b.ward ward,b.mail mail,b.tel tel,b.acc acc,b.bank bank,b.paxo paxo,b.taxo taxo,b.sign sign,b.seq seq,a.depth level, b.temp temp,b.receivermail receivermail,b.dsignissuer dsignissuer,b.dsignto dsignto,b.dsignfrom dsignfrom,b.dsignsubject dsignsubject,b.dsignserial dsignserial, b.place place from s_ou_tree a join s_ou b on b.id = a.cid where a.pid=1 order by a.path`,
    sql_get_pgsql: `with recursive tree as (
        select id,taxc,mst,status,code,name,pid,addr,fadd,prov,dist,ward,mail,tel,acc,bank,paxo,taxo,sign,seq,0 as level,'('||id||'-0)' idx, temp, receivermail, dsignissuer, dsignto, dsignfrom, dsignsubject, dsignserial, place  from s_ou where pid is null
        union all
        select c.id,c.taxc,c.mst,c.status,c.code,c.name,c.pid,c.addr,c.fadd,c.prov,c.dist,c.ward,c.mail,c.tel,c.acc,c.bank,c.paxo,c.taxo,c.sign,c.seq,p.level+1,p.idx||'*('||c.id||'-'||p.level||')' idx, c.temp,c.receivermail,c.dsignissuer,c.dsignto,c.dsignfrom,c.dsignsubject,c.dsignserial, c.place  from s_ou c,tree p where p.id=c.pid) 
        select * from tree order by idx`
}
const sql_bytaxc = {
    sql_bytaxc_mssql: `with tree as (
        select id,name value,0 level,'('+CONVERT(VARCHAR(max),id)+'-0)' idx from s_ou where mst=?
        union all
        select c.id,c.name value,p.level+1 level,p.idx+'*('+CONVERT(VARCHAR(max),c.id)+'-'+CONVERT(VARCHAR(max),p.level)+')' idx  from s_ou c,tree p where p.id=c.pid
        ) select id,value from tree order by idx`,
    sql_bytaxc_orcl: `select id "id",name "value" from s_ou where mst=? start with pid is null connect by prior id=pid`,
    sql_bytaxc_mysql: `select id "id",name "value" from s_ou where mst=? start with pid is null connect by prior id=pid`,
    sql_bytaxc_pgsql: `with recursive tree as (
        select id,name,0 as level,'('||id||'-0)' idx from s_ou where mst=?
        union all
        select c.id,c.name,p.level+1,p.idx||'*('||c.id||'-'||p.level||')' idx  from s_ou c,tree p where p.id=c.pid
        ) select id,name as value from tree order by idx`
}
const sql_byid = {
    sql_byid_mssql: `with tree as (
        select id,name value,0 level,'('+CONVERT(VARCHAR(max),id)+'-0)' idx from s_ou where id=?
        union all
        select c.id,c.name value,p.level+1 level,p.idx+'*('+CONVERT(VARCHAR(max),c.id)+'-'+CONVERT(VARCHAR(max),p.level)+')' idx  from s_ou c,tree p where p.id=c.pid
        ) select id,value from tree order by idx`,
    sql_byid_orcl: `select id "id",name "value" from s_ou start with id=? connect by prior id=pid`,
    sql_byid_mysql: `select b.id "id",b.name "value" from s_ou_tree a join s_ou b on b.id = a.cid where a.pid=? order by a.path`,
    sql_byid_pgsql: `with recursive tree as (
        select id,name,0 as level,'('||id||'-0)' idx from s_ou where id=?
        union all
        select c.id,c.name,p.level+1,p.idx||'*('||c.id||'-'||p.level||')' idx  from s_ou c,tree p where p.id=c.pid
        ) select id,name as value from tree order by idx`
}
const sql_bytoken = {
    sql_bytoken_mssql: `with tree as (
        select id,mst,code,name value,0 level,'('+CONVERT(VARCHAR(max),id)+'-0)' idx from s_ou where id=?
        union all
        select c.id,c.mst,c.code,c.name value,p.level+1 level,p.idx+'*('+CONVERT(VARCHAR(max),c.id)+'-'+CONVERT(VARCHAR(max),p.level)+')' idx  from s_ou c,tree p where p.id=c.pid
        ) select id,mst,code,value from tree order by idx`,
    sql_bytoken_orcl: `select id "id",mst "mst",code "code", name "value" from s_ou start with id=? connect by prior id=pid order by code`,
    sql_bytoken_mysql: `select b.id "id",mst "mst",code "code", b.name "value" from s_ou_tree a join s_ou b on b.id = a.cid where a.pid=? order by a.path`,
    sql_bytoken_pgsql: `with recursive tree as (
        select id,mst,code,name,0 as level,'('||id||'-0)' idx from s_ou where id=?
        union all
        select c.id,c.mst,c.code,c.name,p.level+1,p.idx||'*('||c.id||'-'||p.level||')' idx  from s_ou c,tree p where p.id=c.pid
        ) select id,mst,code,name as value from tree order by idx`
}
const sql_bytokenou = {
    sql_bytokenou_mssql: `with tree as (
        select id,mst,taxc,code,name value,0 level,'('+CONVERT(VARCHAR(max),id)+'-0)' idx from s_ou where id=?
        union all
        select c.id,c.mst,c.taxc,c.code,c.name value,p.level+1 level,p.idx+'*('+CONVERT(VARCHAR(max),c.id)+'-'+CONVERT(VARCHAR(max),p.level)+')' idx  from s_ou c,tree p where p.id=c.pid
        ) select id,mst,taxc,code,value from tree where mst in (select b.taxc from s_ou b,s_manager a where b.taxc=a.taxc and a.user_id=?) order by idx`,
    sql_bytokenou_orcl: `select * from (select id "id",mst "mst",taxc "taxc",code "code", name "value" from s_ou start with id=? connect by prior id=pid order by code ) aa  where aa."mst"  in (select b.taxc "taxc" from s_manager a,s_ou b where a.taxc=b.taxc and a.user_id=?)`,
    sql_bytokenou_mysql: `select b.id "id",mst "mst",code "code", b.name "value" from s_ou_tree a join s_ou b on b.id = a.cid where a.pid=? and b.mst in (select d.taxc from s_manager c,s_ou d where c.taxc=d.taxc and c.user_id=? ) order by a.path`,
    sql_bytokenou_pgsql: `with recursive tree as (
        select id,mst,taxc,code,name,0 as level,'('||id||'-0)' idx from s_ou where id=?
        union all
        select c.id,c.mst,c.taxc,c.code,c.name,p.level+1,p.idx||'*('||c.id||'-'||p.level||')' idx  from s_ou c,tree p where p.id=c.pid
        ) select id,mst,taxc,code,name as value from tree where mst in (select b.taxc from s_ou b,s_manager a where b.taxc=a.taxc and a.user_id=?) order by idx`
}
const Service = {
    taxc: async (req, res, next) => {
        try {
            const sql = `select taxc "id",taxc "value" from s_ou where taxc is not null order by taxc`
            const result = await inc.execsqlselect(sql, [])
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    taxn: async (req, res, next) => {
        try {
            const result = awaitinc.execsqlselect( `select ${sql_taxn[`sql_taxn_${config.dbtype}`]} from s_ou where taxc is not null order by taxc`, [])
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    taxt: async (req, res, next) => {
        try {
            const result = await inc.execsqlselect(`select ${sql_taxt[`sql_taxt_${config.dbtype}`]} from s_ou where taxc is not null order by taxc`, [])
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    seq: async (req, res, next) => {
        try {
            const mst = req.params.mst
            const result = await inc.execsqlselect(`select count(*) "rc" from s_inv where stax=? and status=?`, [mst, 1])
            const row = result
            res.json(row)
        }
        catch (err) {
            next(err)
        }
    },
    get: async (req, res, next) => {
      
        try {
            const sql = `${sql_get[`sql_get_${config.dbtype}`]}`
            const result = await inc.execsqlselect(sql, [])
            const rows = result
            let arr = [], obj
            for (const row of rows) {
                if (!row.pid) arr.push(row)
                else {
                    obj = util.findDFS(arr, row.pid)
                    if (obj) {
                        if (!obj.hasOwnProperty('data')) obj.data = []
                        obj.data.push(row)
                    }
                }
            }
            res.json(arr)
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const body = req.body, operation = body.webix_operation
            let binds, sql, result, taxc = body.taxc ? body.taxc : null, sSysLogs = { fnc_id: '', src: config.SRC_LOGGING_DEFAULT, dtl: ``}
            if (taxc) await redis.del(`OUS.${taxc}`)
            switch (operation) {
                case "update":
                    const seq = body.seq ? body.seq : 1, sign = body.sign ? body.sign : 1, pwd = body.pwd
                    body.code = body.code ? body.code : null
                    sql = "update s_ou set name=?,addr=?,taxc=?,prov=?,dist=?,ward=?,mail=?,tel=?,acc=?,bank=?,paxo=?,taxo=?,seq=?,sign=?,usr=?,mst=?,status=?,code=?,temp=?,c0=?,receivermail=?,dsignissuer=?,dsignserial=?,dsignsubject=?,dsignfrom=?,dsignto=?,place=?,chk_revoke=?"
                    binds = [body.name, body.addr, taxc, body.prov, body.dist, body.ward, body.mail, body.tel, body.acc, body.bank, body.paxo, body.taxo, seq, sign, body.usr, body.mst, body.status, body.code, body.temp,body.c0,body.receivermail,body.dsignissuer,body.dsignserial,body.dsignsubject,util.convertDate(moment(new Date(body.dsignfrom)).startOf('day').format(config.dtf)),util.convertDate(moment(new Date(body.dsignto)).startOf('day').format(config.dtf)),body.place,body.chk_revoke]
                    let i = 27
                    if (pwd) {
                        sql += `,pwd=?`
                        binds.push(util.encrypt(pwd))
                    }
                    sql += ` where id=?`
                    binds.push(body.id)
                    sSysLogs = { fnc_id: 'ou_upd', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật thông tin đơn vị id ${body.id}`, msg_id: body.id, doc: JSON.stringify(body)};
                    break
                case "insert":
                    sql = `insert into s_ou (name,pid,mst,code,temp) values (?,?,?,?,?)`
                    binds = [body.name, body.parent, body.mst, body.code, body.temp]
                    sSysLogs = { fnc_id: 'ou_ins', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm mới thông tin đơn vị: ${JSON.stringify(body)}`, msg_id: body.id, doc: JSON.stringify(body)};
                    break
                case "delete":
                    sql = "delete from s_ou where id=?"
                    binds = [body.id]
                    sSysLogs = { fnc_id: 'ou_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa thông tin đơn vị id ${body.id}`, msg_id: body.id, doc: JSON.stringify(body)};
                    break
                default:
                    throw new Error(`${operation} hoạt động không hợp lệ \n (${operation} is invalid operation)`)
            }
            result = await inc.execsqlinsupddel(sql, binds)
            logging.ins(req,sSysLogs,next) 
            if (operation == "insert") {
                result = await inc.execsqlselect('select Max(id) "id" FROM s_ou')
                res.json(result[0])
            }
            else res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    bytaxc: async (req, res, next) => {
        try {
            const taxc = req.params.taxc
            const sql = `${sql_bytaxc[`sql_bytaxc_${config.dbtype}`]}`
            const result = await inc.execsqlselect(sql, [taxc])
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    byid: async (req, res, next) => {
        try {
            const id = req.params.id
            const sql = `${sql_byid[`sql_byid_${config.dbtype}`]}`
            const result = await inc.execsqlselect(sql, [id])
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    bytoken: async (req, res, next) => {
        try {
            const token = sec.decode(req)
            const sql = `${sql_bytoken[`sql_bytoken_${config.dbtype}`]}`
            const result = await inc.execsqlselect(sql, [token.ou])
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
//theo chi nhanh sgr
    byousgr: async (req, res, next) => {
        try {
            const token = sec.decode(req)
            let sql = ` select id "id",taxc "taxc",concat(taxc,'-',name) "value" from s_ou where id=? or pid = ?`
            const result = await inc.execsqlselect(sql, [token.ou])
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    getou: async (req, res, next) => {
        try {
            const token = sec.decode(req)
            let outoken = await Service.obid(token.ou), sql = `select id "id",mst "mst",code "code", name "value" from s_ou  where pid in (select id from s_ou where pid is null)`, binds = []

            if (outoken.pid) {
                sql = `select id "id",mst "mst",code "code", name "value" from s_ou  where id = ?`
                binds = [token.ou]
            }
            const result = await inc.execsqlselect(sql, binds)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    //theo chi nhanh dc chon
    bytokenou: async (req, res, next) => {
        try {
            const token = sec.decode(req)
            const sql = `${sql_bytokenou[`sql_bytokenou_${config.dbtype}`]}`
            const result = await inc.execsqlselect(sql, [token.ou,token.uid])
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    bytokenoubaca: async (req, res, next) => {
        try {
            const sql = `select id "id", taxc "taxc", name "value", pid from s_ou where pid is null or pid = 1`
            const result = await inc.execsqlselect(sql, [])
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    obt: async (taxc) => {
        let row
        const sql = `select so.id "id",so.taxc "taxc",so.paxo "paxo",st2.name "paxoname",so.taxo "taxo",st.name "taxoname",so.name "name",so.fadd "addr",so.mail "mail",so.tel "tel",so.acc "acc",so.bank "bank",so.seq "seq",so.sign "sign",so.usr "usr",so.pwd "pwd",so.sc "sc",so.sct "sct",so.bcc "bcc",so.qrc "qrc",so.na "na",so.np "np",so.nq "nq",so.itype "itype",so.ts "ts",so.degree_config "degree_config",temp "temp" ,so.place "place",so.chk_revoke "chk_revoke" from s_ou so left join s_taxo st on st.id=so.taxo left join s_taxo st2 on st2.id=so.paxo where taxc=?`
        const result = await inc.execsqlselect(sql, [taxc])
        row = result[0]
        return row
    },
    obt2: async (taxc) => {
        const inc = require("./inc")
        let row
        const sql = `select so.id "id",so.taxc "taxc",so.paxo "paxo",st2.name "paxoname",so.taxo "taxo",st.name "taxoname",so.name "name",so.fadd "addr",so.mail "mail",so.tel "tel",so.acc "acc",so.bank "bank",so.seq "seq",so.sign "sign",so.usr "usr",so.pwd "pwd",so.sc "sc",so.sct "sct",so.bcc "bcc",so.qrc "qrc",so.na "na",so.np "np",so.nq "nq",so.itype "itype",so.ts "ts",so.degree_config "degree_config",temp "temp",so.chk_revoke "chk_revoke"  from s_ou so left join s_taxo st on st.id=so.taxo left join s_taxo st2 on st2.id=so.paxo where taxc=?`
        const result = await inc.execsqlselect(sql, [taxc])
        row = result[0]
        return row
    },
    ousebytaxc: async ( taxc, serial, form, type) => {
        let row
        const sql = `select max "max", cur "cur", (select receivermail from s_ou where taxc =?) "receivermail" from s_serial where serial=? and form=? and type=?`
        const result = await inc.execsqlselect(sql, [taxc, serial, form, type])
        row = result[0]
        return row
    },
    gettosign: async (taxc) => {
        const sql = `select id "id",taxc "taxc",taxo "taxo",name "name",fadd "addr",mail "mail",tel "tel",acc "acc",bank "bank",seq "seq",sign "sign",usr "usr",pwd "pwd",temp "temp" from s_ou where taxc=?`
        const result = await inc.execsqlselect(sql, [taxc])
        let row = result[0]
        return row
    },
    org: async (token) => {
        return await Service.obt(token.taxc)
    },
    obid: async (id) => {
        const sql = `select so.id "id",so.taxc "taxc",so.paxo "paxo",st2.name "paxoname",so.taxo "taxo",st.name "taxoname",so.name "name",so.fadd "addr",so.mail "mail",so.tel "tel",so.acc "acc",so.bank "bank",so.seq "seq",so.sign "sign",so.usr "usr",so.pwd "pwd",so.sc "sc",so.sct "sct",so.bcc "bcc",so.qrc "qrc",so.na "na",so.np "np",so.nq "nq",so.itype "itype",so.ts "ts",so.degree_config "degree_config" from s_ou so left join s_taxo st on st.id=so.taxo left join s_taxo st2 on st2.id=so.paxo where so.id=?`
        const result = await inc.execsqlselect(sql, [id])
        let row = result[0]
        return row
    },
    pidtaxc: async (id) => {
        const sql = `select top 1 taxc from s_ou where id in (select pid from s_ou where id=?) or (id = ? and taxc in ('0104918404','0106827752'))`
        const result = await inc.execsqlselect(sql, [id, id])
        let row = result[0]
        return row.taxc
    },
    oball: async () => {
        const sql = `select id "id",taxc "taxc",taxo "taxo",name "name",fadd "addr",mail "mail",tel "tel",acc "acc",bank "bank",seq "seq",sign "sign",usr "usr",pwd "pwd",temp "temp" from s_ou order by id`
        const result = await inc.execsqlselect(sql, [])
        let rows = result
        return rows
    },
    getTemp: async (req,res,next) =>{
        const token = sec.decode(req)
        let result
        const sql = `select temp "temp" from s_ou where id = ?`
        result = await inc.execsqlselect(sql, [token.ou])
        res.json(result[0].temp)
    }
}
module.exports = Service