﻿"use strict"
const moment = require("moment")
const forge = require("node-forge")
const pki = forge.pki
const xmlcrypto = require("xml-crypto")
const select = xmlcrypto.xpath
const dom = require("xmldom").DOMParser
const config = require("./config")
const logger4app = require("./logger4app")
const util = require("./util")
const redis = require("./redis")
const SEC = require("./sec")
const dbtype = config.dbtype
const ous = require(`./${dbtype}/ous`)

//const ous = require(`./ous`)

const keyvault = require("./keyvault")
const captchaType = config.captchaType
//Canonicalization and Transformation Algorithms
//const enveloped_signature = "http://www.w3.org/2000/09/xmldsig#enveloped-signature"
const fields = ["O", "CN"]
// const getdtl = (sub) => {
//   let subjectFields
//   if (sub) {
//     subjectFields = fields.reduce((subjects, fieldName) => {
//       let certAttr = sub.getField(fieldName)
//       if (certAttr) subjects.push(`${fieldName}=${forge.util.decodeUtf8(certAttr.value)}`)
//       return subjects
//     }, [])
//   }
//   return Array.isArray(subjectFields) ? subjectFields.join(",") : ""
// }
const getdtl = (obj) => {
  return obj.attributes.map(attr => {
    let sn = attr.shortName
    let val = attr.value 

    if(val.includes("MST")) sn="UID"
    try {
      val =forge.util.decodeUtf8(val)
    } catch (e) { }
    // const sn = attr.shortName, val = decodeURIComponent(encodeURI(attr.value))
    return sn ? [sn, val].join('=') : val
  }).join(', ')
}
const getmst = (sub) => {
  for (const obj of sub.attributes) {
    const type = obj.type
    if (type === "0.9.2342.19200300.100.1.1" || type === "2.5.4.10" || type === "2.5.4.3" || type === "2.5.4.8") {
      const arr = obj.value.split(":"), mst = arr[arr.length - 1]
      if (util.checkmst(mst)) return mst
    }
  }
  return ""
}

const b642pem = (b64) => {
  const pem = b64.match(/.{1,64}/g).join("\n")
  return `-----BEGIN CERTIFICATE-----\n${pem}\n-----END CERTIFICATE-----`
}

function caKeyInfo(ca) {
  this.getKeyInfo = () => {
    // return `<X509Data><X509IssuerSerial><X509IssuerName>${ca.issuer}</X509IssuerName><X509SerialNumber>${ca.serialNumber}</X509SerialNumber></X509IssuerSerial><X509SubjectName>${ca.subject}</X509SubjectName><X509Certificate>${ca.c64}</X509Certificate></X509Data>`
    return `<X509Data><X509SubjectName>${ca.subject}</X509SubjectName><X509Certificate>${ca.cert}</X509Certificate></X509Data>`
  }
  // this.getKeyInfo = () => {
  //   //return `<X509Data><X509IssuerSerial><X509IssuerName>${ca.issuer}</X509IssuerName><X509SerialNumber>${ca.serialNumber}</X509SerialNumber></X509IssuerSerial><X509SubjectName>${ca.subject}</X509SubjectName><X509Certificate>${ca.c64}</X509Certificate></X509Data>`
  //   return `<X509Data><X509Certificate>${ca.c64}</X509Certificate></X509Data>`
  // }
  this.getKey = () => {
    return b642pem(ca.cert)
  }
}

function myKeyInfo(x509cert) {
  this.getKeyInfo = () => {
    return `<X509Data><X509Certificate>${x509cert}</X509Certificate></X509Data>`
  }
  this.getKey = () => {
    return b642pem(x509cert)
  }
}

const createSig = (id) => {
  const sig = new xmlcrypto.SignedXml()
  sig.id = id
  sig.signatureAlgorithm = "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"
  sig.canonicalizationAlgorithm = "http://www.w3.org/2001/10/xml-exc-c14n#"
  sig.addReference(config.PATH_XML, null, "http://www.w3.org/2001/04/xmlenc#sha256")
  //sig.addReference(config.PATH_XML)
  return sig
}

const Service = {
  getFileSignInfo: async (mst, pwd) => {
    try {
      let b64
      if (['aia'].includes(config.ent)) {
        if (!config.test_sms_fpt) {
          b64 = await keyvault.getSecretByKey(`CA-${mst}`)
        }
        else b64 = await redis.get(`CA.${mst}`)
      }
      else {
        b64 = await redis.get(`CA.${mst}`)
      }

      //   if (!b64) throw new Error(`Key CA.${mst} is notfound`)
      if (!b64) {
        logger4app.error(`Key CA.${mst} is notfound`)
        throw new Error(`Không tìm thấy chữ ký số \n (CA is not found)`)
      }
      const der = forge.util.decode64(b64), asn1 = forge.asn1.fromDer(der), p12 = forge.pkcs12.pkcs12FromAsn1(asn1, false, pwd)
      let bags, bag
      bags = p12.getBags({ bagType: pki.oids.certBag })
      bag = bags[pki.oids.certBag][0]
      const cert = bag.cert
      const validity = cert.validity, subject = cert.subject, taxc = getmst(subject)
      return { mst: taxc, serial: cert.serialNumber, fd: moment(validity.notBefore).startOf('day').format(config.dtf), td: moment(validity.notAfter).startOf('day').format(config.dtf), subject: getdtl(subject), issuer: forge.util.decodeUtf8(cert.issuer.getField("CN").value) }
    } catch (err) {
      throw err
    }
  },
  cakeyvault: async () => {
    try {
      let rows = await ous.oball()
      for (const row of rows) {
        try {
          if (row.taxc && row.taxc != "") {
            const key = `CA.${row.taxc}`
            let ca = await redis.get(`CA.${row.taxc}`)
            //let ca = keyvault.getSecretByKey(key)
            await redis.set(key, ca)
          }
        } catch (err) {
          logger4app.error(`cakeyvault - row.taxc ${err}`)
        }
      }
    } catch (err) {
      logger4app.error(`cakeyvault ${err}`)
    }
  },
  ca: async (mst, pwd) => {
    try {
      let b64
      if (['aia'].includes(config.ent)) {
        if (!config.test_sms_fpt) {
          b64 = await keyvault.getSecretByKey(`CA-${mst}`)
        }
        else b64 = await redis.get(`CA.${mst}`)
      }
      else {
        b64 = await redis.get(`CA.${mst}`)
      }

      //   if (!b64) throw new Error(`Key CA.${mst} is notfound`)
      if (!b64) {
        logger4app.error(`Key CA.${mst} is notfound`)
        throw new Error(`Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)`)
      }
      const der = forge.util.decode64(b64), asn1 = forge.asn1.fromDer(der), p12 = forge.pkcs12.pkcs12FromAsn1(asn1, false, pwd)
      let bags, bag
      bags = p12.getBags({ bagType: pki.oids.certBag })
      bag = bags[pki.oids.certBag][0]
      const cert = bag.cert
      //REM 02row for test
      //const valid = Service.valid(cert, mst)
      //if (!valid)  throw new Error("Chứng thư số không hợp lệ")
      const pem = pki.certificateToPem(cert), certb64 = forge.util.encode64(forge.pem.decode(pem)[0].body)
      bags = p12.getBags({ bagType: pki.oids.pkcs8ShroudedKeyBag })
      bag = bags[pki.oids.pkcs8ShroudedKeyBag][0]
      const key = pki.privateKeyToPem(bag.key), validity = cert.validity
      var now = new Date();

      if (    (validity.notBefore).getTime() < now.getTime() 
          &&  now.getTime() < (validity.notAfter).getTime()) 
      {
        // Certificate is withing its validity days
      } else {
        throw new Error(`Chứng chỉ chưa hợp lệ hoặc đã hết hạn\n(Certificate is either not yet valid or has already expired.)`)
        // Certificate is either not yet valid or has already expired.
      }
      return { key: key, cert: certb64, c64: certb64, mst: mst, fd: validity.notBefore, td: validity.notAfter, subject: getdtl(cert.subject), issuer: getdtl(cert.issuer), serialNumber: cert.serialNumber }
    } catch (err) {
      logger4app.error(err)
      throw new Error(`Lỗi cấu hình chữ ký số không hợp lệ \n(Error of invalid digital signature configuration)`)
    }
  },
  sign: (ca, xml, id) => {
    const sig = createSig(id)
    sig.signingKey = ca.key
    sig.keyInfoProvider = new myKeyInfo(ca.cert)
    sig.computeSignature(xml)
    return sig.getSignedXml()
  },
  verify: (xml) => {
    try {
      const doc = new dom().parseFromString(xml)
      const signature = select(doc, "/*/*[local-name(.)='Signature' and namespace-uri(.)='http://www.w3.org/2000/09/xmldsig#']")[0]
      const x509cert = select(signature, "//*[local-name(.)='X509Certificate']")[0].firstChild.data
      const sig = new xmlcrypto.SignedXml()
      sig.keyInfoProvider = new myKeyInfo(x509cert)
      sig.loadSignature(signature)
      const ret = sig.checkSignature(xml)
      if (!ret) throw new Error("Chữ ký số không hợp lệ \n(Invalid digital signature)")
      const cert = pki.certificateFromPem(b642pem(x509cert))
      const validity = cert.validity, subject = cert.subject, mst = getmst(subject)
      const info = { mst: mst, serial: cert.serialNumber, fd: validity.notBefore, td: validity.notAfter, subject: getdtl(subject), issuer: getdtl(cert.issuer) }
      return { info: info }
    }
    catch (err) {
      return { error: err.message }
    }
  },
  verify2: async (xml, idt) => {
    try {
      xml = xml.replace(/\r\n?/g, '\n')
      const doc = new dom().parseFromString(xml)
      const signature = select(doc, "//*[local-name(.)='Signature' and namespace-uri(.)='http://www.w3.org/2000/09/xmldsig#']")[0]
      const x509cert = select(signature, "//*[local-name(.)='X509Certificate']")[0].firstChild.data
      const sig = new xmlcrypto.SignedXml()
      sig.keyInfoProvider = new myKeyInfo(x509cert)
      sig.loadSignature(signature)
      const ret = sig.checkSignature(xml)
      //if (!ret) logger4app.debug(sig.validationErrors)
      const cert = pki.certificateFromPem(b642pem(x509cert))
      const validity = cert.validity, subject = cert.subject, fd = moment(validity.notBefore), td = moment(validity.notAfter), serial = cert.serialNumber
      const result = { mst: getmst(subject), serial: serial, fd: fd.format(config.mfd), td: td.format(config.mfd), subject: getdtl(subject), issuer: getdtl(cert.issuer), signature: ret }
      const signing_time = select(doc, "//*[local-name(.)='SigningTime']")[0]
      let checked, btn
      const mdt = moment(idt), sdt = signing_time ? moment(signing_time.firstChild.data) : mdt
      result.signing_time = sdt.format(config.mfd)
      if (/*sdt.isSameOrAfter(mdt) && */sdt.isBetween(fd, td)) result.validity = true
      else result.validity = false
      //2-check crl
      const crl = await redis.get(`CRL.${serial}`)
      if (crl) {
        const rdt = moment(new Date(crl))
        result.revocation_time = rdt.format(config.mfd)
        result.revoked = rdt.isSameOrBefore(sdt)
        btn = true
      }
      else {
        result.revocation_time = ""
        result.revoked = false
        checked = await redis.exists(`CHECKED.${serial}`)
        btn = checked ? false : true
      }
      result.checked = checked
      result.btn = btn
      if (btn) result.b64 = x509cert
      return { info: result }
    }
    catch (err) {
      return { error: err.message }
    }
  },
  upload: async (req, res, next) => {
    try {
      const body = req.body
      if (config.ent != 'vib') {
        if (captchaType == 0) {
          //Validated
        } else if (captchaType == 1) {
          const data = await util.chkcaptoff(JSON.parse(body.token))
          if (!data) throw new Error("Nhập sai mã Captcha \n(Enter the wrong Captcha code)")
        } else if (captchaType == 2) {
          const data = await util.reCaptcha(body.token, req.connection.remoteAddress)
          if (!data.success || data.score < config.recaptchaScore || data.action !== body.action) throw new Error(`Lỗi Google reCAPTCHA không hợp lệ : ${JSON.stringify(data)} \n(Error Google reCAPTCHA is not valid : ${JSON.stringify(data)})`)
        } else {
          throw new Error("Không thể nhận dạng Captcha \n(Captcha cannot be identified)")
        }
      }

      const file = req.file
      if (file) {
        const xml = file.buffer.toString()
        const verify = await Service.verify2(xml, null)
        res.json(verify)
      }
      else res.json({ error: "Không có file (File does not exist)" })
    }
    catch (err) {
      res.json({ error: err.message })
    }
  },
  info: (x509cert) => {
    const cert = pki.certificateFromPem(b642pem(x509cert)), validity = cert.validity, subject = cert.subject, mst = getmst(subject)
    return { mst: mst, serial: cert.serialNumber, fd: validity.notBefore, td: validity.notAfter, subject: getdtl(subject), issuer: getdtl(cert.issuer) }
  },
  infob64: async (req, res, next) => {
    const ous = require(`./ous`)
    try {
      let pwd  
      let body = req.body, mst = body.mst
      const org = await ous.obt2(mst)
      if(!org) throw new Error('Đơn vị chưa được cấu hình chữ ký số')
      if(!org.pwd) throw new Error('Bạn chưa lưu mật khẩu')
      pwd = util.decrypt(org.pwd)
      if(!pwd) throw new Error('Bạn chưa lưu mật khẩu')
      let certobj = await Service.getFileSignInfo(mst, pwd)
      res.json(certobj)
    } catch (err) {
      logger4app.error(err)
      if(err.message.includes("Invalid password"))
        err.message = `Sai mật khẩu (Wrong password)`
      next(err)
    }
  },
  upb64: async (req, res, next) => {
    const ous = require(`./ous`)
    try {
      let body = req.body, mst = body.mst, pwd = body.pwd
      if(!pwd) throw new Error('Bạn chưa nhập mật khẩu')
      let certobj = await Service.getFileSignInfo(mst, pwd)
      res.json(certobj)
    } catch (err) {
      logger4app.error(err)
      if(err.message.includes("Invalid password"))
        err.message = `Sai mật khẩu (Wrong password)`
      next(err)
    }
  },
  checkca: async (req, res, next) => {
    const query = req.query
    let taxc = query.taxc
    let retresult
    const pwd = query.pwd
    try {
      const ca = await Service.ca(taxc, pwd)
      if (ca) retresult = 1;
      else retresult = 0;
    } catch (err) {
      retresult = 0;
      logger4app.debug(err)
    }
    res.json({ ret: retresult })
  },
  valid: (cert, mst) => {
    try {
      const mstInCert = getmst(cert.subject)
      if (mst !== mstInCert) throw new Error("MST của chứng thư số khác MST người dùng \n (The tax code of digital certificate different from the tax code of customer)")
      const validity = cert.validity, sysdate = new Date()
      if (sysdate > new Date(validity.notAfter) || sysdate < new Date(validity.notBefore)) throw new Error("Thời hạn chứng thư số không hợp lệ \n (Invalid digital certificate period)")
      return true
    } catch (err) {
      throw err
    }
  },
  uploadCA: async (req, res, next) => {
    try {
      const file = req.file, body = req.body, taxc = body.taxc
      if (file && taxc) {
        const key = `CA.${taxc}`
        await redis.set(key, file.buffer.toString("base64"))
        res.json(file)
      }
      else res.json("Không có file hoặc MST (File or tax code does not exist)")
    } catch (err) {
      next(err)
    }
  },
  sign123: (ca, xml, inc, PATH_XML, signSample) => {
    const sig = new xmlcrypto.SignedXml()
    let xml_sign_time = signSample.signingTime
      , tag_begin = signSample.tagBegin
      , xml_end_tag_replace = signSample.xmlEndTagReplace
      , object_signing_time = `${xml_sign_time}${xml_end_tag_replace}`
      , new_xml_dest
    // Add signature that have signing time to xml
    xml = xml.replace(xml_end_tag_replace, object_signing_time)
    sig.id = inc
    sig.signatureAlgorithm = "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"
    sig.canonicalizationAlgorithm = "http://www.w3.org/TR/2001/REC-xml-c14n-20010315"
    sig.addReference(
      PATH_XML,
      null,
      "http://www.w3.org/2001/04/xmlenc#sha256",
      `#Id-${inc}`
    )
    sig.addReference(
      "//*[local-name(.)='Object']",
      null,
      "http://www.w3.org/2001/04/xmlenc#sha256",
      `#SigningTime-${tag_begin}-${inc}`
    )
    sig.signingKey = ca.key
    sig.keyInfoProvider = new caKeyInfo(ca)
    sig.computeSignature(xml)
    let tag_end_substring = '</KeyInfo>'
      // new signature
      , new_xml = sig.getSignedXml()
      , new_sign_xml = sig.signatureXml
      // convert new signature
      , new_sign_xml_info = new_sign_xml.substring(new_sign_xml.indexOf('<SignedInfo>'), new_sign_xml.indexOf(tag_end_substring) + tag_end_substring.length)
    // delete original signature
    new_xml = new_xml.replace(new_sign_xml, "")
    // add convert new signature to old signature
    new_xml_dest = new_xml.replace(`<Signature xmlns="http://www.w3.org/2000/09/xmldsig#">`, `<Signature xmlns="http://www.w3.org/2000/09/xmldsig#">${new_sign_xml_info}`)
    return new_xml_dest
  },
  checkCABeforeSign: async (ca, dt) => {
    try {
      if(config.checkCADate) {
        if(dt< ca.fd || dt > ca.td) {
          throw new Error(`Dữ liệu ngày ký không thuộc khoảng ngày hiệu lực của chữ ký số \n (Sign date data error does not belong to the valid date range of the digital signature)`)
        }
      }
      if(config.checkStatement) {
        const inc = require("./inc")
        const result = await inc.execsqlselect(`select doc "doc" from s_statement where stax=? and cqtstatus=5 order by createdt desc `, [ca.mst])
        if (result.length == 0) throw new Error(`Không tìm thấy Tờ khai ${ca.mst}`)
        let row = result[0], check = false
        let doc = JSON.parse(row.doc)
        for(let item of doc.items) {
          if(item.sign_seri == ca.serialNumber) {
            check = true
            break
          }
        }
        if(!check) {
          throw new Error(`Chữ ký số chưa được đăng ký với CQT \n (Error digital signature is not registered with CQT)`)
        }
      }
    }
    catch(err) {
      throw(err)
    }
  }
}
module.exports = Service