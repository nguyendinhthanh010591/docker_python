"use strict"
const axios = require("axios")
const moment = require("moment")
var uuid = require('uuid');
const config = require("./config")
const logger4app = require("./logger4app")
const keyvault = require("./keyvault")
const hbs = require("./hbs")
const org = require(`./${config.dbtype}/org`)
const util = require("./util")
const test_sms_fpt = config.test_sms_fpt
const fxp = require("fast-xml-parser")
const Parser = fxp.j2xParser
const checkbeforesend = async (json) => {
    let vcheck = true
    if (json.sendmanual) return vcheck
    if (config.ent == 'aia') {
        if (json.c5 == 'Y' && (!util.isEmpty(json.btel))) {
            if (!util.isEmpty(json.bmail)) vcheck = false
        }
        else vcheck = false
    }
    if (config.ent == 'vib') {
        if (json.bcode && (!util.isEmpty(json.btel)))
        {
            let orgob = await org.obbcode(json.bcode)
            if (orgob && orgob.c1 == '1') vcheck = true
            else vcheck = false
        }
        else vcheck = false
    }
    if (config.ent == 'bvb') {
        logger4app.debug(`json.btel : ${json.btel}`)
        logger4app.debug(`json.bcode : ${json.bcode}`)
        if (json.bcode && (!util.isEmpty(json.btel)))
        {
            let orgob = await org.obbcode(json.bcode)
            logger4app.debug(`orgob : ${JSON.stringify(orgob)}`)
            if (orgob && orgob.c2 == '1') vcheck = true
            else vcheck = false
        }
        else vcheck = false
    }
    if (config.ent == 'baca') {
        if (json.bcode && (!util.isEmpty(json.btel)))
        {
            let orgob = await org.obbcode(json.bcode)
            if (orgob && orgob.c1 == '1') vcheck = true
            else vcheck = false
        }
        else vcheck = false
    }
    return vcheck
}

const sendsms = async (json) => {
    try {
        return { errnum: 0, resdesc: "0904127341:000000;" }
    } catch (err) {
        logger4app.debug(err)
    }
}

const buildsms = async (json) => {
    let reqobj = {}
    try {
        reqobj.mti = '0200'
        reqobj.processingCode = '100000'
        reqobj.transTime = `${moment(new Date()).format('YYYYMMDD')}${moment(new Date()).format('HHmmssSSS')}`
        reqobj.auditNo = uuid.v4()
        reqobj.systemId = 'eInvoice'
        reqobj.user = config.sms_config.user
        reqobj.password = config.sms_config.password
        //cif = json.bcode
        //account = json.bacc
        reqobj.Content = json.content
        reqobj.Mobile = json.btel
        reqobj.signature = ``
        const parser = new Parser()
        let xmlsms = parser.parse(reqobj, {
            ignoreAttributes: false, attrValueProcessor: a => he.decode(a, { isAttributeValue: true }),
            tagValueProcessor: a => he.decode(a)
        }).replace(/&/g, "&amp;")
        xmlsms = `<?xml version="1.0" encoding="UTF-8"?><bvmsg>${xmlsms}</bvmsg>`
        return xmlsms
    } catch (err) {
        logger4app.debug(err)
    }
}

const sendsms_vib = async (json) => {
    try {
        if (test_sms_fpt) return { errnum: 0, resdesc: "0904127341:000000;" }
        const arr = json.btel.trim().split(/[ ,;]+/).map(e => e.trim())
        let resdesc = "", errnum = 0
        axios.defaults.headers.post["Authorization"] = config.sms_auth
        axios.defaults.headers.post["Content-Type"] = "application/json"
        for (let phonenum of arr) {
            let sms_url = String(config.sms_url).replace("{mobileno}", phonenum)
            axios.defaults.baseURL = sms_url
            const dat = { servicecode: config.sms_servicecode, message: `VIB thông báo: hóa đơn của bạn có mã tra cứu là: ${json.sec}. Bạn hãy vào trang einvoice.vib.com.vn để tra cứu chi tiết và download file hóa đơn.` }
            if ((config.sendApprSMS && json.status == 3) || (config.sendCancSMS && json.status == 4)) {
                try {
                    resdesc += phonenum + ":"
                    const { data } = await axios({ method: "POST", data: dat })
                    const statuscode = data.Result.STATUSCODE
                    resdesc += statuscode + ";"
                    if ((!statuscode) || statuscode !== "000000") errnum++
                } catch (error) {
                    resdesc += error.message + ";"
                    errnum++
                }
                return { errnum: errnum, resdesc: resdesc }
            }
        }
    } catch (err) {
        logger4app.debug(err)
    }
}

const sendsms_baca = async (json) => {
    try {
        if (test_sms_fpt) return { errnum: 0, resdesc: "0904127341:000000;" }
        const arr = json.btel.trim().split(/[ ,;]+/).map(e => e.trim())
        let resdesc = "", errnum = 0
        axios.defaults.headers.post["Authorization"] = config.sms_auth
        axios.defaults.headers.post["Content-Type"] = "application/json"
        for (let phonenum of arr) {
            let sms_url = String(config.sms_url).replace("{mobileno}", phonenum)
            axios.defaults.baseURL = sms_url
            const dat = { servicecode: config.sms_servicecode, message: `VIB thông báo: hóa đơn của bạn có mã tra cứu là: ${json.sec}. Bạn hãy vào trang einvoice.vib.com.vn để tra cứu chi tiết và download file hóa đơn.` }
            if ((config.sendApprSMS && json.status == 3) || (config.sendCancSMS && json.status == 4)) {
                try {
                    resdesc += phonenum + ":"
                    const { data } = await axios({ method: "POST", data: dat })
                    const statuscode = data.Result.STATUSCODE
                    resdesc += statuscode + ";"
                    if ((!statuscode) || statuscode !== "000000") errnum++
                } catch (error) {
                    resdesc += error.message + ";"
                    errnum++
                }
                return { errnum: errnum, resdesc: resdesc }
            }
        }
    } catch (err) {
        logger4app.debug(err)
    }
}

const sendsms_aia = async (json) => {
    try {
        if (test_sms_fpt) return { errnum: 0, resdesc: "0904127341:000000;" }
        const sms_appr = `Hoa don dien tu cua Quy khach phat hanh tai https://www.aia-apps.com/e-invoice/search.html voi ma tra cuu ${json.sec}. Tran trong.`;
        const sms_can = `Hoa don dien tu cua Quy khach phat hanh tai https://www.aia-apps.com/e-invoice/search.html voi ma tra cuu ${json.sec} da bi huy. Tran trong.`;
        //Get token endpoint
        //let tokenEndpoint = config.sms_config.tokenEndpoint
        logger4app.debug(`SMS Begin, init parameter`);
        let smsusername = await keyvault.getSecretByKey('einvoice-sms-username');
        let smspassword = await keyvault.getSecretByKey('einvoice-sms-password');
        //Gửi SMS
        const sms_endpoint = config.sms_config.sendSmsEndpoint;
        const sms_token = Buffer.from(`${smsusername}:${smspassword}`, 'utf8').toString('base64');
        const arr = json.btel.trim().split(/[ ,;]+/).map(e => e.trim());
        let resdesc = "", errnum = 0;
        for (let phonenum of arr) {
            if ((config.sendApprSMS && json.status == 3) || (config.sendCancSMS && json.status == 4)) {
                try {
                    resdesc += phonenum + ":"
                    let msgcontent = ""
                    switch (json.status) {
                        case 3:
                            msgcontent = sms_appr
                            break
                        case 4:
                            msgcontent = sms_can
                            break
                        default:
                            break
                    }
                    let sms_contents = { "callback": "true", "phoneNo": phonenum, "content": msgcontent, "source": "EINVOICE-API" };
                    const data = await axios.post(sms_endpoint, sms_contents, {
                        headers: {
                            "Content-Type": "application/json",
                            "User-Agent": "AIA/2020",
                            "Authorization": `Basic ${sms_token}`
                        }
                    })
                    logger4app.debug(`Get result ${data.status}`)
                    const statuscode = data.status
                    resdesc += statuscode + ";"
                    if (statuscode != 200) errnum++
                } catch (error) {
                    logger4app.debug(`Send SMS error `, error)
                    resdesc += error.message + ";"
                    errnum++
                }
                return { errnum: errnum, resdesc: resdesc }
            }
        }
    } catch (err) {
        logger4app.debug(err)
    }
}

const sendsms_bvb = async (json) => {
    try {
        const jsonsms = JSON.parse(JSON.stringify(json))
        const sms_appr = await hbs.j2sms(json, config.sms_config.sms_appr)//`Hoa don dien tu cua Quy khach phat hanh voi ma tra cuu ${jsonsms.sec}. Tran trong.`;
        const sms_can = await hbs.j2sms(json, config.sms_config.sms_appr)//`Hoa don dien tu cua Quy khach phat hanh tai voi ma tra cuu ${jsonsms.sec} da bi huy. Tran trong.`;
        //Get token endpoint
        //let tokenEndpoint = config.sms_config.tokenEndpoint
        logger4app.debug(`SMS BVB Begin, init parameter`);
        //Gửi SMS
        const sms_endpoint = config.sms_config.sendSmsEndpoint;
        const arr = jsonsms.btel.trim().split(/[ ,;]+/).map(e => e.trim());
        logger4app.debug(`sms_endpoint : ${sms_endpoint}`)
        logger4app.debug(`arr tel : ${jsonsms.btel}`)
        let resdesc = "", errnum = 0;
        logger4app.debug(`config.sendApprSMS : ${config.sendApprSMS}`)
        logger4app.debug(`config.sendCancSMS : ${config.sendCancSMS}`)
        if ((config.sendApprSMS && json.status == 3) || (config.sendCancSMS && json.status == 4)) {
            for (let phonenum of arr) {
                try {
                    resdesc += phonenum + ":"
                    jsonsms.btel = phonenum
                    logger4app.debug(`Preparing send sms to : ${phonenum}`)
                    switch (json.status) {
                        case 3:
                            jsonsms.content = sms_appr
                            break
                        case 4:
                            jsonsms.content = sms_can
                            break
                        default:
                            jsonsms.content = ``
                            break
                    }
                    let msgreq = await buildsms(jsonsms)
                    let msgcontent = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ser="http://services.bvbebankservices.baovietbank.vn/"><soapenv:Header/><soapenv:Body><ser:execute><xmlRequest><![CDATA[${msgreq}]]></xmlRequest></ser:execute></soapenv:Body></soapenv:Envelope>`
                    logger4app.debug(`msgcontent : ${msgcontent}`)
                    let data
                    if (test_sms_fpt) return { errnum: 0, resdesc: "0904127341:000000;" }
                    data = await axios.post(sms_endpoint, msgcontent, {
                        headers: {
                            "Content-Type": "text/plain; charset=utf-8"
                        }
                    })
                    logger4app.debug(`Get result : ${data}`)
                    if(data.includes("<responseCode>00</responseCode>")){
                        const statuscode = '00'
                        resdesc += statuscode + ";"
                        if (statuscode != '00') errnum++
                    }else{
                        errnum++
                        throw new Error(`${String(data).substring(0,99)}`)
                    }
                } catch (error) {
                    logger4app.debug(`Send SMS error `, error)
                    resdesc += error.message + ";"
                    errnum++
                }
            }
            return { errnum: errnum, resdesc: resdesc }
        }
        
    } catch (err) {
        logger4app.debug(err)
    }
}

const Service = {
    smssend : 
    async (doc) => {
        let returnmmsg, vcheck = await checkbeforesend(doc)
        if (!vcheck) return
        try {
            returnmmsg = await eval(`sendsms_${config.ent}(doc)`)
        } catch (err) {
            returnmmsg = await eval(`sendsms(doc)`)
        }
        return returnmmsg
    },
    send: async (req, res, next) => {
        try {
            let id = req.body.id, btel = req.body.btel
            const invobj = require(`./inv`)
            let doc = await invobj.invdocbid(id)
            let doctmp = JSON.parse(JSON.stringify(doc))
            doctmp.btel = btel
            doctmp.sendmanual = 1
            await Service.smssend(doctmp)
            res.json("ok")
        }
        catch (err) {
            next(err)
        }
    }
}
module.exports = Service