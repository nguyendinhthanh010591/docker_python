"use strict"
const ONE_THOUSAND = 1000
const ONE_MILLION = 1000000
const ONE_BILLION = 1000000000           //         1.000.000.000 (9)
const ONE_TRILLION = 1000000000000       //     1.000.000.000.000 (12)
const ONE_QUADRILLION = 1000000000000000 // 1.000.000.000.000.000 (15)
const MAX = 9007199254740992             // 9.007.199.254.740.992 (15)
const ChuSo = [" không", " một", " hai", " ba", " bốn", " năm", " sáu", " bảy", " tám", " chín"]
const CURR={
    'VND':'đồng',
    'AUD':'Đô la Úc',
    'BRL':'Rin Brazin',
    'CAD':'Đô la Canada',
    'CHF':'Phơ răng Thụy Sĩ',
    'CNY':'Nhân dân tệ Trung Quốc',
    'DKK':'Curon Đan Mạch',
    'EUR':'Euro',
    'GBP':'Bảng Anh',
    'HKD':'Đô la Hồng Công',
    'IDR':'Rupiah Inđônêsia',
    'INR':'Rupee Ấn độ',
    'JPY':'Yên Nhật',
    'KHR':'Riêl Cămpuchia',
    'LAK':'Kíp Lào',
    'MOP':'Pataca Macao',
    'MYR':'Ringít Malaysia',
    'NOK':'Curon Nauy',
    'NZD':'Đô la Newzealand',
    'PLN':'Đồng Zloty Ba Lan',
    'RUB':'Rúp Nga',
    'SEK':'Curon Thụy Điển',
    'SGD':'Đô la Singapore',
    'THB':'Bath Thái',
    'TRY':'Lira Thổ Nhĩ Kỳ',
    'TWD':'Đô la Đài Loan',
    'USD':'Đô la Mỹ',
    'WON':'Won Hàn Quốc',
	'SAR':'Riyal Ả Rập Saudi',
	'KWD':'Dinar Kuwait',
	'KRW':'Won Hàn Quốc',
	'KPW':'Won Cộng hòa Dân chủ Nhân dân Triều Tiên',
	'BDT':'Taka Bangladesh',
	'AED':'UAE DIRHAM'
}
const isSafeNumber = (value) => {
    return typeof value === 'number' && Math.abs(value) < MAX
}
const DocSo3ChuSo = (so) => {
    if (so == 0) return ""
    let tram, chuc, donvi, kq
    tram = parseInt(so / 100)
    chuc = parseInt((so % 100) / 10)
    donvi = so % 10
    kq = ChuSo[tram] + " trăm"
    if (chuc == 0 && donvi != 0) kq += " linh"
    if (chuc == 1) kq += " mười"
    else if (chuc != 0) kq += ChuSo[chuc] + " mươi"
    switch (donvi) {
        case 1:
            if (chuc != 0 && chuc != 1) kq += " mốt"
            else kq += ChuSo[donvi]
            break
        case 4:
            if (chuc != 0 && chuc != 1) kq += " tư"
            else kq += ChuSo[donvi]
            break
        case 5:
            if (chuc == 0) kq += ChuSo[donvi]
            else kq += " lăm"
            break
        default:
            if (donvi != 0) kq += ChuSo[donvi]
            break
    }
    return kq
}
const DocSoLeTienDo = (so) => {
    if (so == 0) return ""
	if (so.toString().length > 2) {
		so = so.toString().substr(0,2) + '.' + so.toString().substr(2)
		if (so[0]==0){
			so = '0' + Math.round(so)
		} else {
			so = Math.round(so)
		}
	}
	if (so.toString().length == 1) {
		so = so.toString() + '0'
		so = parseInt (so)
	}
	let chuc, donvi, kq
    chuc = parseInt(so / 10)
    donvi = so % 10
    kq = ""
	if (chuc == 0 && donvi != 0) kq += ""
	//chuc
    if (chuc == 1) kq += " mười"
    else if (chuc != 0) kq += ChuSo[chuc] + " mươi"
    //donvi
    switch (donvi) {
        case 1:
            if (chuc != 0 && chuc != 1) kq += " mốt"
            else kq += ChuSo[donvi]
            break
        case 4:
            if (chuc != 0 && chuc != 1) kq += " tư"
            else kq += ChuSo[donvi]
            break
        case 5:
            if (chuc == 0) kq += ChuSo[donvi]
            else kq += " lăm"
            break
        default:
            if (donvi != 0) kq += ChuSo[donvi]
            break
    }
    return kq
}
const n2w = (number, words) => {
    let remainder, word
    if (!number) return !words ? 'không' : words.join('').replace(/,$/, '')
    if (!words) words = []
    if (number < ONE_THOUSAND) {
        remainder = 0
        word = DocSo3ChuSo(number)
    } else if (number < ONE_MILLION) {
        remainder = number % ONE_THOUSAND
        word = DocSo3ChuSo(Math.floor(number / ONE_THOUSAND)) + ' nghìn'
    } else if (number < ONE_BILLION) {
        remainder = number % ONE_MILLION
        word = DocSo3ChuSo(Math.floor(number / ONE_MILLION)) + ' triệu'
    } else if (number < ONE_TRILLION) {
        remainder = number % ONE_BILLION
        word = DocSo3ChuSo(Math.floor(number / ONE_BILLION)) + ' tỷ'
    } else if (number < ONE_QUADRILLION) {
        remainder = number % ONE_TRILLION
        word = DocSo3ChuSo(Math.floor(number / ONE_TRILLION)) + ' nghìn tỷ'
    } else if (number <= MAX) {
        remainder = number % ONE_QUADRILLION
        word = DocSo3ChuSo(Math.floor(number / ONE_QUADRILLION)) + ' triệu tỷ'
    }
    words.push(word)
    return n2w(remainder, words)
}
exports.d2w = (n, c) => {
    let num = parseInt(n, 10)
    if (!isFinite(num) || !isSafeNumber(num)) throw new Error("Số tiền của hóa đơn quá lớn")
    let words = n2w(num).trim()
    if (words.startsWith("không trăm")) words = words.slice(10).trim()
    if (words.startsWith("linh")) words = words.slice(4).trim()
	if (c == 'USD' || c == 'EUR' || c == 'BDT') {
		words += " "
		words += CURR[c]
		if (n % 1 != 0) {
			words += " và"
			let str = String(n)
			let dot = str.split('.')
			let dec = dot[dot.length - 1]
			words += DocSoLeTienDo(dec)
			words += " xu"
		}
		return `${words.charAt(0).toUpperCase()}${words.slice(1)}`
	} else {
		if (n % 1 != 0) {
			words += " phẩy"
			let str = String(n)
			let dot = str.split('.')
			let dec = dot[dot.length - 1]
			let arr = dec.split('')
			for (var i = 0; i < arr.length; i++) {
				words += ChuSo[arr[i]]
			}
		}
		return `${words.charAt(0).toUpperCase()}${words.slice(1)} ${CURR[c]}`
	}
}
exports.n2w = (n, c) => {
    let num = parseInt(n, 10)
    if (!isFinite(num) || !isSafeNumber(num)) throw new Error("Số tiền của hóa đơn quá lớn")
    let words = n2w(num).trim()
    if (words.startsWith("không trăm")) words = words.slice(10).trim()
    if (words.startsWith("linh")) words = words.slice(4).trim()
    //return words.charAt(0).toUpperCase() + words.slice(1) + " " + ((c == "VND") ? "đồng" : c)
    return `${words.charAt(0).toUpperCase()}${words.slice(1)} ${CURR[c]}`
}