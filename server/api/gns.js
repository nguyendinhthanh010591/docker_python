"use strict"
const config = require("./config")
const logger4app = require("./logger4app")
const logging = require("./logging")
const util = require("./util")
const inc = require("./inc")
const sql_fbw_01 = {
    sql_fbw_01_mssql: `OFFSET 0 ROWS FETCH FIRST ${config.limit} ROWS ONLY`,
    sql_fbw_01_mysql: `LIMIT ${config.limit}`,
    sql_fbw_01_orcl: `FETCH FIRST ${config.limit} ROWS ONLY`,
    sql_fbw_01_pgsql: `limit ${config.limit}`
}
const Service = {
    fbn: async (req, res, next) => {
        try {
            const query = req.query, filter = query.filter
            let sql, result, where = " where 1=1", binds = [], val
            if (filter) {
                val = `%${filter.value.trim().toUpperCase()}%`
                where += " and (upper(code) like ? or upper(name) like ?)"
                binds.push(val, val)
            }
            sql = `select code "code",name "name",unit "unit",price "price" from s_gns ${where} order by code ${sql_fbw_01['sql_fbw_01_'+config.dbtype]}`
            result = await inc.execsqlselect(sql, binds)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    fbw: async (req, res, next) => {
        try {
            const query = req.query, filter = query.filter, sort = query.sort
            let sql, result, where = " where 1=1", order, binds = [], val
            if (filter) {
                let i = 1
                Object.keys(filter).forEach(key => {
                    val = filter[key]
                    if (val) {
                        if (key == "price") {
                            if (util.isNumber(val)) {
                                where += ` and price=?`
                                binds.push(val)
                            }
                            else where += ` and price = ${val}`
                        }
                        else {
                            where += ` and upper(${key}) like ?`
                            binds.push(`%${val.toUpperCase()}%`)
                        }
                    }
                })
            }
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = ` order by ${key} ${sort[key]}`
                })
            }
            else order = ` order by code`
            sql = `select id "id",code "code",name "name",unit "unit",price "price" from s_gns ${where} ${order} ${sql_fbw_01[`sql_fbw_01_${config.dbtype}`]}`
            result = await inc.execsqlselect(sql, binds)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    get: async (req, res, next) => {
        try {
            const query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 10, sort = query.sort
            let sql, result, ret, where = " where 1=1", order, binds = [], i = 1, pagerobj = inc.getsqlpagerandbind(start, count)
            if (query.filter) {
                let filter = JSON.parse(query.filter), val
                Object.keys(filter).forEach(key => {
                    val = filter[key]
                    if (val) {
                        if (key == "unit") {
                            where += ` and ${key}=?`
                            binds.push(val)
                        }
                        else {
                            where += ` and upper(${key}) like ?`
                            binds.push(`%${val.toUpperCase()}%`)
                        }
                    }
                })
            }
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = ` order by ${key} ${sort[key]}`
                })
            } else order = ` order by code`
            sql = `select id "id",code "code",name "name",unit "unit",price "price" from s_gns ${where} ${order} ${pagerobj.vsqlpagerstr}`
            for (let t of pagerobj.vsqlpagerbind) binds.push(t)
            result = await inc.execsqlselect(sql, binds)
            ret = { data: result, pos: start }
            if (start == 0) {
                sql = `select count(*) "total" from s_gns ${where}`
                let bindscount = []
                for (let i = 0; i < binds.length - 2; i++) bindscount.push(binds[i])
                result = await inc.execsqlselect(sql, bindscount)
                ret.total_count = result[0].total
            }
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const body = req.body, operation = body.webix_operation
            let sql, result, binds, price = body.price ? body.price : null, sSysLogs = { fnc_id: '', src: config.SRC_LOGGING_DEFAULT, dtl: ``}
            switch (operation) {
                case "update":
                    sql = `update s_gns set code=?,name=?,price=?,unit=? where id=?`
                    binds = [body.code, body.name, price, body.unit, body.id]
                    sSysLogs = { fnc_id: 'gns_upd', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật danh mục hàng hóa, dịch vụ ${body.name} (${operation})`, msg_id: body.id, doc: JSON.stringify(body) };
                    break
                case "insert":
                    sql = `insert into s_gns(code,name,price,unit) values(?,?,?,?);`
                    binds = [body.code, body.name, price, body.unit]
                    sSysLogs = { fnc_id: 'gns_ins', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm mới danh mục hàng hóa, dịch vụ ${body.name} (${operation})`, msg_id: body.id, doc: JSON.stringify(body) };
                    break
                case "delete":
                    sql = `delete from s_gns where id=?`
                    binds = [body.id]
                    sSysLogs = { fnc_id: 'gns_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa danh mục hàng hóa, dịch vụ ${body.name} (${operation})`, msg_id: body.id, doc: JSON.stringify(body) };
                    break
                default:
                    throw new Error(`${operation} là không hợp lệ \n (${operation} is invalid)`)
            }
            result = await inc.execsqlinsupddel(sql, binds)
            if (operation == "insert"){
                let result_id = await inc.execsqlselect(`select id from s_gns where code=?`, [body.code])
                let id_ins, rows_ins = result_id
                if(rows_ins.length >0) id_ins=rows_ins[0].id
                sSysLogs.msg_id=id_ins
            }
            logging.ins(req, sSysLogs, next)
            if (operation == "insert") {
                result = await inc.execsqlselect('select Max(id) as id FROM s_gns')
                res.json(result[0])
            }
            else res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    ins: async (req, res, next) => {
        try {
            const body = req.body, cols = ['code', 'name', 'unit', 'price']
            const sql = `insert into s_gns (code, name, unit, price) values (?,?,?,?)`
            let arr = []
            for (const row of body) {
                let id = row.id
                try {
                    for (const col of cols) {
                        if (!row.hasOwnProperty(col)) row[col] = null
                    }
                    delete row["id"]
                    delete row["chk"]
                    await inc.execsqlinsupddel(sql, Object.values(row))
                    if(config.dbtype=="mssql") await inc.execsqlinsupddel('select scope_identity() as id', [])
                    const sSysLogs = { fnc_id: 'gns_ins', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm mới danh mục hàng hóa, dịch vụ ${body.name}`, msg_id: body.id, doc: JSON.stringify(body) };
                    logging.ins(req, sSysLogs, next)
                    arr.push({ id: id, error: "OK" })
                } catch (err) {
                    const msg = (err.code == "EREQUEST") ? "Bản ghi đã tồn tại \n The record has existed" : err.message
                    arr.push({ id: id, error: msg })
                }
            }
            res.json(arr)
        }
        catch (err) {
            next(err)
        }
    }
}
module.exports = Service