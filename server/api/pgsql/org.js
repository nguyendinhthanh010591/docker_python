"use strict"
const config = require("../config")
const logger4app = require("../logger4app")
const dbs = require("./dbs")
const logging = require("../logging")
const ENT = config.ent
const Service = {
    fbi: async (req, res, next) => {
        try {
            const result = await dbs.query(`select id,taxc,name,fadd,mail,tel,acc,bank,code from s_org where id=$1`, [req.params.id])
            res.json(result.rows)
        }
        catch (err) {
            next(err)
        }
    },
    ous: async (req, res, next) => {
        try {
            const query = req.query, filter = query.filter
            let rows
            if (filter && filter.value) {
                let val, phrase
                val = filter.value.trim()
                if (val.indexOf(' ') == -1) {
                    val = `${val}:*`
                    phrase = ""
                }
                else phrase = "phrase"
                const sql = `select id,name,taxc,addr,mail,tel,acc,bank,prov,dist,ward,fadd,ts_rank(fts,tsq) rank,code from s_org,${phrase}to_tsquery($1) tsq where fts @@ tsq order by rank desc limit ${config.limit}`
                const result = await dbs.query(sql, [val])
                rows = result.rows
            }
            res.json(rows)
        }
        catch (err) {
            next(err)
        }
    },
    fbn: async (req, res, next) => {
        try {
            const query = req.query, filter = query.filter
            let rows
            if (filter && filter.value) {
                let val, phrase
                val = filter.value.trim()
                if (val.indexOf(' ') == -1) {
                    val = `${val}:*`
                    phrase = ""
                }
                else phrase = "phrase"
                const sql = `select taxc,name,fadd,mail,tel,acc,bank,ts_rank(fts,tsq) rank,code from s_org,${phrase}to_tsquery($1) tsq where fts @@ tsq order by rank desc limit ${config.limit}`
                const result = await dbs.query(sql, [val])
                rows = result.rows
            }
            res.json(rows)
        }
        catch (err) {
            next(err)
        }
    },
    get: async (req, res, next) => {
        try {
            let val = req.query.name, start = req.query.start, sort = req.query.sort, order = ""
            let rows
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = ` order by ${key} ${sort[key]}`
                })
            } else order = ` order by rank desc `

            if (val) {
                val = val.trim()
                let phrase
                if (val.indexOf(' ') == -1) {
                    val = `${val}:*`
                    phrase = ""
                }
                else phrase = "phrase"
                const sql = `select id,code,taxc,name,name_en,addr,prov,dist,ward,mail,tel,acc,bank,fadd,c0,c1,c2,c3,c4,c5,c6,c7,c8,c9,ts_rank(fts,tsq) rank,type from s_org,${phrase}to_tsquery($1) tsq where fts @@ tsq ${order} limit ${config.limit}`
                const result = await dbs.query(sql, [val])
                rows = result.rows
            } else {
                val = ''
                let phrase = "phrase"
                const sql = `select id,code,taxc,name,name_en,addr,prov,dist,ward,mail,tel,acc,bank,fadd,c0,c1,c2,c3,c4,c5,c6,c7,c8,c9,ts_rank(fts,tsq) rank,type from s_org,${phrase}to_tsquery($1) tsq ${order} limit ${config.limit}`
                const result = await dbs.query(sql, [val])
                rows = result.rows
            }
            let ret = { data: rows, pos: start }
            if (start == 0) {
                ret.total_count = rows.length
            }
            return res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const body = req.body, operation = body.webix_operation
            let binds, sql, result, sSysLogs = { fnc_id: '', src: config.SRC_LOGGING_DEFAULT, dtl: ``}
            const taxc = body.taxc ? body.taxc : null, code = body.code ? body.code : null
            if (operation == "update") {
                sql = `update s_org set code=$1,taxc=$2,name=$3,name_en=$4,prov=$5,dist=$6,ward=$7,addr=$8,mail=$9,tel=$10,acc=$11,bank=$12,c0=$13,c1=$14,c2=$15,c3=$16,c4=$17,c5=$18,c6=$19,c7=$20,c8=$21,c9=$22 where id=$23`
                binds = [code, taxc, body.name, body.name_en, body.prov, body.dist, body.ward, body.addr, body.mail, body.tel, body.acc, body.bank, body.c0, body.c1, body.c2, body.c3, body.c4, body.c5, body.c6, body.c7, body.c8, body.c9, body.id]
                sSysLogs = { fnc_id: 'org_upd', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật thông tin tổ chức id ${body.id}`, msg_id: body.id, doc: JSON.stringify(body)};
            }
            else if (operation == "insert") {
                sql = `insert into s_org(code,taxc,name,name_en,prov,dist,ward,addr,mail,tel,acc,bank,c0,c1,c2,c3,c4,c5,c6,c7,c8,c9) values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22) RETURNING id`
                binds = [code, taxc, body.name, body.name_en, body.prov, body.dist, body.ward, body.addr, body.mail, body.tel, body.acc, body.bank, body.c0, body.c1, body.c2, body.c3, body.c4, body.c5, body.c6, body.c7, body.c8, body.c9]
                sSysLogs = { fnc_id: 'org_ins', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm mới thông tin tổ chức: ${JSON.stringify(body)}`, msg_id: body.id, doc: JSON.stringify(body)};
            }
            else if (operation == "delete") {
                sql = `delete from s_org where id=$1`
                binds = [body.id]
                sSysLogs = { fnc_id: 'org_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa thông tin tổ chức id ${body.id}`, msg_id: body.id, doc: JSON.stringify(body)};
            }
            result = await dbs.query(sql, binds)
            logging.ins(req,sSysLogs,next) 
            if (operation == "insert") res.json(result.rows[0])
            else res.json(result.rowCount)
        }
        catch (err) {
            next(err)
        }
    },
    ins: async (req, res, next) => {
        try {
            const body = req.body
            const cols = ['name', 'name_en', 'taxc', 'code', 'addr', 'tel', 'mail', 'acc', 'bank', 'c0', 'c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9']
            let arr = []
            const sql = `insert into s_org(name,name_en,taxc,code,addr,tel,mail,acc,bank,c0,c1,c2,c3,c4,c5,c6,c7,c8,c9) values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19)`
            for (const row of body) {
                let id = row.id
                try {
                    for (const col of cols) {
                        if (!row.hasOwnProperty(col)) row[col] = null
                    }
                    delete row["id"]
                    delete row["chk"]
                    delete row["error"]
                    await dbs.query(sql, [row.name,row.name_en,row.taxc,row.code,row.addr,row.tel,row.mail,row.acc,row.bank,row.c0,row.c1,row.c2,row.c3,row.c4,row.c5,row.c6,row.c7,row.c8,row.c9])
                    const sSysLogs = { fnc_id: 'org_ins', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm mới thông tin tổ chức: ${JSON.stringify(row)}`, msg_id: id, doc: JSON.stringify(row)};
                    logging.ins(req,sSysLogs,next) 
                    arr.push({ id: id, error: "OK" })
                } catch (err) {
                    if (["fhs", "fbc"].includes(config.ent)) {
                        if (err.message == `duplicate key value violates unique constraint "s_org_code_idx"`) {
                            let sqlupd = `update
                                        s_org
                                    set
                                        name = $1,
                                        name_en = $2,
                                        taxc = $3,
                                        code = $4,
                                        addr = $5,
                                        tel = $6,
                                        mail = $7,
                                        acc = $8,
                                        bank = $9,
                                        c0 = $10,
                                        c1 = $11,
                                        c2 = $12,
                                        c3 = $13,
                                        c4 = $14,
                                        c5 = $15,
                                        c6 = $16,
                                        c7 = $17,
                                        c8 = $18,
                                        c9 = $19
                                    where
                                        code = $4`
                            await dbs.query(sqlupd, [row.name, row.name_en, row.taxc, row.code, row.addr, row.tel, row.mail, row.acc, row.bank, row.c0, row.c1, row.c2, row.c3, row.c4, row.c5, row.c6, row.c7, row.c8, row.c9])   
                            arr.push({ id: id, error: "OK" })
                        } else arr.push({ id: id, error: err.message })
                    }
                    else arr.push({ id: id, error: err.message })
                }
            }
            res.json(arr)
        }
        catch (err) {
            next(err)
        }
    },
    obbcode: async (code) => {
        try {
            let rows, ob
            const sql = `select id "id",code "code",taxc "taxc",name "name",name_en "name_en",addr "addr",prov "prov",dist "dist",ward "ward",mail "mail",tel "tel",acc "acc",bank "bank",fadd "fadd",c0 "c0",c1 "c1",c2 "c2",c3 "c3",c4 "c4",c5 "c5",c6 "c6",c7 "c7",c8 "c8",c9 "c9",type "type" from s_org where code = :code`
            const result = await dbs.query(sql, [code])
            rows = result.rows
            ob = (rows && rows.length > 0) ? rows[0] : null
            return ob
        }
        catch (err) {
            next(err)
        }
    }
}
module.exports = Service