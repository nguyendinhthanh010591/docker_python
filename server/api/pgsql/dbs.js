'use strict'
const { Pool } = require("pg")
const config = require("../config")
const logger4app = require("../logger4app")
const decrypt = require('../encrypt')
let poolConfig = JSON.parse(JSON.stringify(config.poolConfig))
poolConfig.password = decrypt.decrypt(poolConfig.password)
const pool = new Pool(poolConfig)
pool.on('error', (err) => {
    logger4app.error('Pool error', err)
    process.exit(-1)
})
module.exports = pool