"use strict"
const util = require('../util')
const dbs = require("./dbs")
const config = require("../config")
const logger4app = require("../logger4app")
const logging = require("../logging")
const Service = {
    get: async (req, res, next) => {
        try {
            const pid = req.params.pid
            const result = await dbs.query(`select id,name,pid from s_loc where id like $1 order by id`, [`${pid}%`])
            const rows = result.rows
            let arr = [], obj
            for (const row of rows) {
                if (row.id.length <= 5) row.data = []
                if (!row.pid) arr.push(row)
                else {
                    obj = util.findDFS(arr, row.pid)
                    if (obj) obj.data.push(row)
                }
            }
            res.json(arr)
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            let body = req.body, binds, sql, result, operation = body.webix_operation, id
            switch (operation) {
                case "update":
                    sql = "update s_loc set name=$1,pid=$2 where id=$3"
                    binds = [body.name, body.parent, body.id]
                    break
                case "insert":
                    const pid = body.p.id, count = body.p.$count
                    id = `${pid}${(2 * count + 1)}`
                    sql = "insert into s_loc(id,name,pid) values ($1,$2,$3) RETURNING id"
                    binds = [id, body.name, pid]
                    break
                case "delete":
                    sql = "delete from s_loc where id=$1"
                    binds = [body.id]
                    break
                default:
                    throw new Error(operation + "(là hoạt động không hợp lệ) is invalid operation")
            }
            result = await dbs.query(sql, binds)
            const sSysLogs = { fnc_id: 'loc_post', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật thông tin địa bàn, ${body.name} (${operation})`, msg_id: body.id, doc: JSON.stringify(body) };
            logging.ins(req, sSysLogs, next)
            if (operation == "insert") res.json(result.rows[0])
            else res.json(result.rowCount)
        }
        catch (err) {
            next(err)
        }
    }

}
module.exports = Service