"use strict"
const Hashids = require("hashids/cjs")
const moment = require("moment")
const config = require("../config")
const logger4app = require("../logger4app")
const email = require("../email")
const sign = require("../sign")
const spdf = require("../spdf")
const util = require("../util")
const redis = require("../redis")
const hbs = require("../hbs")
const fcloud = require("../fcloud")
const hsm = require("../hsm")
const SEC = require("../sec")
const SERIAL = require("./serial")
const dbs = require("./dbs")
const COL = require("./col")
const user = require("./user")
const ous = require("./ous")
const sms = require("../sms")
const hashids = new Hashids(config.secret, 10, "abcdefghijklmnopqrstuvwxyz1234567890")
const logging = require("../logging")
const path = require("path")
const fs = require("fs")
const xlsxtemp = require("xlsx-template")
const rsmq = require("../rsmq")
const qname = config.qname
const CARR = config.CARR
const ENT = config.ent
const grant = config.ou_grant
const mfd = config.mfd
const dtf = config.dtf
const serial_usr_grant = config.serial_usr_grant
const UPLOAD_MINUTES = config.upload_minute
const useJobSendmail = config.useJobSendmail
const useRedisQueueSendmail = config.useRedisQueueSendmail
const rbi = (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            const result = await dbs.query("select doc doc, uc uc, ou ou, c0 c0, c1 c1 , c2 c2 ,c3 c3 ,c4 c4, c5 c5, c6 c6, c7 c7,c8 c8,c9 c9 from s_inv where id=$1", [id])
            const rows = result.rows
            if (rows.length == 0) reject(new Error(`Không tìm thấy Hóa đơn ${id} \n Invoice not found ${id}`))
            let doc = rows[0].doc, uc = rows[0].uc, ou = rows[0].ou, c0 = rows[0].c0, c1 = rows[0].c1, c2 = rows[0].c2, c3 = rows[0].c3,
                c4 = rows[0].c4, c5 = rows[0].c5, c6 = rows[0].c6, c7 = rows[0].c7, c8 = rows[0].c8, c9 = rows[0].c9
            doc.uc = uc
            doc.ou = ou
            doc.c0 = c0
            doc.c1 = c1
            doc.c2 = c2
            doc.c3 = c3
            doc.c4 = c4
            doc.c5 = c5
            doc.c6 = c6
            doc.c7 = c7
            doc.c8 = c8
            doc.c9 = c9
            //Lấy thông tin user của user lập hóa đơn
            let usercreate = await user.obid(uc)
            //Lấy thông tin ou của user lập hóa đơn
            let useroucreate = await ous.obid(ou)
            //Gán vào doc và trả ra
            doc.usercreate = usercreate
            doc.useroucreate = useroucreate
            doc.ouname = useroucreate.name
            //Lay thong tin so thu tu template de sau nay dung truy van template hoa don
            const org = await ous.obt(doc.stax), idx = org.temp, fn = util.fn(doc.form, idx)
            doc.tempfn = fn
            resolve(doc)
        } catch (err) {
            reject(err)
        }
    })
}
const oubi = (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            const result = await dbs.query("select name name from s_ou where id=$1", [id])
            let name
            const rows = result.rows
            if (rows.length == 0)
                name = ""
            else
                name = rows[0].name
            resolve(name)
        } catch (err) {
            reject(err)
        }
    })
}
const chkou = async (id, token) => {
    if (grant) {
        if (token.ou !== token.u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches`)
        const result = await dbs.query(`select ou from s_inv where id=$1`, [id])
        const rows = result.rows
        if (rows.length > 0 && rows[0].ou !== token.u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches) `)
    }
}
const Service = {
    getcol: async (json) => {
        try {
            let doc = JSON.parse(JSON.stringify(json))
            let configfield = {}
            let configfieldhdr = await COL.cache(doc.type, 'vi')
            let configfielddtl = await COL.cachedtls(doc.type, 'vi')
            configfield.configfieldhdr = configfieldhdr
            configfield.configfielddtl = configfielddtl
            doc.configfield = configfield
            let doctmp = await rbi(json.id)
            doc.org = doctmp.useroucreate
            
            return doc
        }
        catch (err) {
            throw err
        }
    },
    invdocbid: async (invid) => {
        try {
            let doc = await rbi(invid)
            return doc
        }
        catch (err) {
            logger4app.debug(`invdocbid ${invid}:`, err.message)
        }
    },
    insertDocTempJob: async (invid, doc, xml) => {
        try {
            if (!useRedisQueueSendmail) {
                await dbs.query(`INSERT INTO s_invdoctemp (invid, doc, status, xml) values ($1,$2,$3,$4)`, [invid, JSON.stringify(doc), 0, xml])
            } else {
                await rsmq.sendMessageAsync({ qname: qname, message: JSON.stringify({ invid: invid, status: doc.status, subject: doc.subject, bmail: doc.bmail, sendmanual: doc.sendmanual }) })
            }
        }
        catch (err) {
            logger4app.debug(`insertDocTempJob ${invid}:`, err.message)
        }
    },
    insertDocTempJobAPI: async (invid, doc, xml) => {
        try {
            if (!config.useRedisQueueSendmailAPI) {
                await dbs.query(`INSERT INTO s_invdoctemp (invid, doc, status, xml) values ($1,$2,$3,$4)`, [invid, JSON.stringify(doc), 0, xml])
            } else {
                await rsmq.sendMessageAsync({ qname: qname, message: JSON.stringify({ invid: invid, status: doc.status, subject: doc.subject, bmail: doc.bmail, sendmanual: doc.sendmanual }) })
            }
        }
        catch (err) {
            logger4app.debug(`insertDocTempJob ${invid}:`, err.message)
        }
    },
    updateSendMailStatus: async (doc) => {
        try {
            let sql, binds
            let docupd = await rbi(doc.id)
            //Cập nhật ngày và trạng thái gửi mail, số lần gửi mail
            if (docupd.SendMailNum) {
                try {
                    docupd.SendMailNum = parseInt(docupd.SendMailNum) + 1
                } catch (err) { docupd.SendMailNum = 1 }
            } else {
                docupd.SendMailNum = 1
            }
            docupd.SendMailStatus = 1
            docupd.SendMailDate = moment(new Date()).format(dtf)
            sql = `update s_inv set doc=$1 where id=$2`
            binds = [JSON.stringify(docupd), docupd.id]
            await dbs.query(sql, binds)
        }
        catch (err) {
            throw err
        }
    },
    checkrefno: async (doc) => {
        try {
            if (!config.refno) return []
            let str = "", sql, arr = [], binds = [], where = "where 1=1 ", mappingfield = config.refno.mappingfield, rows
            if (mappingfield.split(".")[0] == "doc") {
                rows = [doc]
            } else {
                rows = doc.items
            }
            const crr = eval(`[...new Set(rows.map(x => x.${mappingfield.split(".")[1]}))]`)
            crr.forEach(function (value, i) {
                str += `$rn${i},`
                binds.push(value)
            })
            where += ` and ref_no in (${str.slice(0, -1)}) `
            sql = `select ref_no "ref_no" from s_invd ${where}`
            const result = await dbs.query(sql, binds)
            rows = result.rows
            rows.forEach((v, i) => {
                arr.push(v.ref_no)
            })
            return arr
        } catch (err) {
            throw err
        }
    },
    checkrefdate: async (doc) => {
        try {
            if (!config.refdate) return []
            let arr = [], mappingfield = config.refdate.mappingfield, rows
            const idt = moment(doc.idt, dtf).toDate()
            if (mappingfield.split(".")[0] == "doc") {
                rows = [doc]
            } else {
                rows = doc.items
            }
            const crr = eval(`[...new Set(rows.map(x => x.${mappingfield.split(".")[1]}))]`)
            crr.forEach(function (value, i) {
                let refdate = moment(value, dtf).toDate()
                if (refdate > idt) arr.push(value)
            })

            return arr
        } catch (err) {
            throw err
        }
    },
    saverefno: async (doc) => {
        try {
            const id = doc.id
            if (!config.refno) return 1
            let numc = 0, sql = `INSERT INTO s_invd (ref_no,inv_id) VALUES ($refno,$id) `, mappingfield = config.refno.mappingfield, rows
            if (mappingfield.split(".")[0] == "doc") {
                rows = [doc]
            } else {
                rows = doc.items
            }
            const crr = eval(`[...new Set(rows.map(x => x.${mappingfield.split(".")[1]}))]`)
            for (let i = 0; i < crr.length; i++) {
                let value = crr[i]
                try {
                    await dbs.query(sql, [value, id])
                    numc++
                } catch (error) {
                    logger4app.error(error)
                }
            }
            return numc
        } catch (err) {
            throw err
        }
    },
    delrefno: async (doc) => {
        try {
            if (!config.refno) return 1
            let numc = 0, sql = `DELETE from s_invd where ref_no = $1 `, mappingfield = config.refno.mappingfield, rows
            if (mappingfield.split(".")[0] == "doc") {
                rows = [doc]
            } else {
                rows = doc.items
            }
            const crr = eval(`[...new Set(rows.map(x => x.${mappingfield.split(".")[1]}))]`)
            for (let i = 0; i < crr.length; i++) {
                let value = crr[i]
                try {
                    await dbs.query(sql, [value])
                    numc++
                } catch (error) {
                    logger4app.error(error)
                }
            }
            return numc
        } catch (err) {
            throw err
        }
    },
    count12: async (req, res, next) => {
        try {
            const token = SEC.decode(req), query = req.query
            const result = await dbs.query(`select count(*) rc from s_inv where stax=$1 and form=$2 and serial=$3 and status in (1, 2)`, [token.taxc, query.form, query.serial])
            let result0 = result.rows[0]
            result0.rc = parseInt(result0.rc)
            res.json(result0)
        }
        catch (err) {
            next(err)
        }
    },
    seqget: async (req, res, next) => {
        try {

            // const token = SEC.decode(req), query = req.query, filter = JSON.parse(query.filter), start = query.start ? query.start : 0, count = query.count ? query.count : 10
            // let td = filter.td
            // let where = " where stax=? and form=? and serial=? and dt<=? and status=1"
            // let binds = [token.taxc, filter.form, filter.serial,td]
            // let sql = `select id id,idt idt,status status,ou ou,type type,form form,serial serial,seq seq,btax btax,bname bname,buyer buyer,baddr baddr,sumv sumv,vatv vatv,totalv totalv,sum "sum",vat "vat",total "total",adjdes adjdes,uc uc,ic ic from s_inv ${where} order by idt,id LIMIT ${count} OFFSET ${start}`
            // let result = await dbs.query(sql, binds)
            // let ret = { data: result[0], pos: start }
            // if (start == 0) {
            //     sql = `select count(*) total from s_inv ${where}`
            //     result = await dbs.query(sql, binds)
            //     ret.total_count = result[0][0].total
            // }
            // res.json(ret)
            const token = SEC.decode(req), query = req.query, filter = JSON.parse(query.filter), start = query.start ? query.start : 0, count = query.count ? query.count : 10
            let td = new Date(moment(filter.td).endOf("day"))
            let where = "where stax=$1 and form=$2 and serial=$3 and idt<=$4 and status=1"
            let binds = [token.taxc, filter.form, filter.serial, td]
            let sql = `select id,idt,status,ou,type,form,serial,seq,btax,bname,buyer,baddr,sumv,vatv,totalv,adjdes,uc,ic from s_inv ${where} order by idt,id limit ${count} offset ${start}`
            let result = await dbs.query(sql, binds)
            let ret = { data: result.rows, pos: start }
            if (start == 0) {
                sql = `select count(*) total from s_inv ${where}`
                result = await dbs.query(sql, binds)
                ret.total_count = result.rows[0].total
            }
            res.json(ret)
        } catch (error) {
            next(error)
        }
    },
    seqpost: async (req, res, next) => {
        let key
        try {
            const token = SEC.decode(req)
            const wait = 60, taxc = token.taxc, body = req.body, form = body.form, serial = body.serial, idt = new Date(body.idt), seqList = body.seqList
            key = `APPR.${taxc}.${form}.${serial}`
            const val = await redis.get(key)
            if (val == "1") throw new Error(`Đang cấp số cho HĐ MST=${taxc} mẫu số=${form} ký hiệu=${serial}. Bạn hãy đợi ${wait}s ! \n Issuing number for MST =${taxc} denominator =${form} symbol=${serial}. Please wait ${wait}s ! `)
            await redis.set(key, "1", "EX", wait)
            let sql, binds
            if (seqList.length > 0) {
                // cấp 1 hoặc lỗ chỗ trong 1 ngày
                binds = [taxc, form, serial]
                let str = "", i = 0
                for (const row of seqList) {
                    str += `$${i++},`
                    binds.push(row)
                }
                sql = `select id,pid,ou,adjtyp,idt,sec from s_inv where stax=$1 and form=$2 and serial=$3 and status=1 and id IN (${str.slice(0, -1)}) order by idt,id`
            } else {
                // cấp theo dải bé hơn ngày hiện tại
                sql = `select id,pid,ou,adjtyp,idt,sec from s_inv where idt<=$1 and stax=$2 and form=$3 and serial=$4 and status=1 order by idt,id`
                binds = [idt, taxc, form, serial]
            }
            const result = await dbs.query(sql, binds)
            const rows = result.rows
            let count = 0
            for (const row of rows) {
                let seq, s7, rs, rc, id, pid
                try {
                    id = row.id
                    seq = await SERIAL.sequence(taxc, form, serial, row.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    rs = await dbs.query(`update s_inv set doc=doc||$1 where id=$2 and stax=$3 and status=1`, [JSON.stringify({ status: 2, seq: s7 }), id, taxc])
                    rc = rs.rowCount
                    if (rc > 0) {
                        count++
                        pid = row.pid
                        if (!util.isEmpty(pid)) {
                            try {
                                const cde = `${(row.adjtyp == 1 ? "Bị thay thế" : "Bị điều chỉnh")} bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${moment(row.idt).format(mfd)} mã ${row.sec}`
                                let pdoc = await rbi(pid)
                                pdoc.cde = cde
                                await dbs.query(`update s_inv set cde=$1,doc=$2 where id=$3`, [cde, JSON.stringify(pdoc), pid])
                            } catch (err) { logger4app.error(err) }
                        }
                    }
                    else throw new Error(`Không cấp số cho hóa đơn ${id}  \n (Do not issue number to the invoice: ${id})`)
                    const rtolog = await rbi(id)
                    const sSysLogs = {
                        fnc_id: 'inv_seqpost', src: config.SRC_LOGGING_DEFAULT, dtl: `Cấp số hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(rtolog
                        )
                    };
                    logging.ins(req, sSysLogs, next)
                } catch (err) {
                    await SERIAL.err(taxc, form, serial, seq)
                    throw err
                }
            }
            res.json(count)
        }
        catch (err) {
            next(err)
        }
        finally {
            if (!util.isEmpty(key)) await redis.del(key)
        }
    },
    apprall: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, org = await ous.obt(taxc), signtype = org.sign
            if (signtype !== 2) throw new Error("Chỉ hỗ trợ kiểu ký bằng file \n Only support signed by file ")
            const iwh = await Service.iwh(req, true)
            const sql = `select id,doc,bmail from s_inv ${iwh.where}`
            const result = await dbs.query(sql, iwh.binds)
            const rows = result.rows
            if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n There is no data to sign ")
            if (rows.length > config.MAX_ROW_APPROVE) throw new Error(`Dữ liệu ký duyệt vượt quá ${config.MAX_ROW_APPROVE} cho phép \n Signed data exceeded ${config.MAX_ROW_APPROVE} allow`)
            let pwd, ca
            //hunglq chinh sua thong bao loi
            try {
                pwd = util.decrypt(org.pwd)
                ca = await sign.ca(taxc, pwd)
            } catch (err) {
                throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n Error configuring invalid digital signature ')
            }
            let count = 0, status = JSON.stringify({ status: 3 })
            for (const row of rows) {
                let doc = row.doc
                const id = row.id, xml = util.signature(sign.sign(ca, util.j2x((await Service.getcol(doc)), id), id), doc.idt)
                const rs = await dbs.query(`update s_inv set doc=doc||$1,xml=$2,dt=$3 where id=$4 and status=2`, [status, xml, new Date(), id])
                if (rs.rowCount > 0) {
                    doc.status = 3
                    if (!util.isEmpty(doc.bmail)) {
                        let mailstatus = await email.rsmqmail(doc)
                        if (mailstatus) await Service.updateSendMailStatus(doc)
                    }
                    count++
                }
                const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                logging.ins(req, sSysLogs, next)
            }
            res.json(count)
        }
        catch (err) {
            next(err)
        }
    },
    apprpost: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const taxc = token.taxc, body = req.body, ids = body.ids
            let i = 1, str = "", binds = []
            for (const row of ids) {
                str += `$${i++},`
                binds.push(row)
            }
            binds.push(taxc)
            str = str.slice(0, -1)
            let sql, result
            sql = `select id,doc,bmail from s_inv where id in (${str}) and stax=$${i} and status=2`
            result = await dbs.query(sql, binds)
            const rows = result.rows
            if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n There is no data to sign")
            if (rows.length > config.MAX_ROW_APPROVE) throw new Error(`Dữ liệu ký duyệt vượt quá ${config.MAX_ROW_APPROVE} cho phép \n Signed data exceeded ${config.MAX_ROW_APPROVE} allow`)
            const org = await ous.obt(taxc), signtype = org.sign
            if (signtype == 1) {
                let arr = []
                for (const row of rows) {
                    const doc = row.doc, id = row.id, xml = util.j2x((await Service.getcol(doc)), id)
                    // push {inc: id,...} thay vi {id: id,...} vì con ký anh Đại đang để là inc thay vì id CLOUD
                    arr.push({ inc: id, xml: Buffer.from(xml).toString("base64") })
                }
                res.json({ mst: taxc, date: new Date(), ref: config.PATH_XML, arr: arr })
            }
            else {
                let count = 0, status = JSON.stringify({ status: 3 })
                sql = `update s_inv set doc=doc||$1,xml=$2,dt=$3 where id=$4 and stax=$5 and status=2`
                if (signtype == 2) {
                    //hunglq chinh sua thong bao loi
                    let ca, pwd
                    try {
                        pwd = util.decrypt(org.pwd)
                        ca = await sign.ca(taxc, pwd)
                    } catch (err) {
                        throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n Error configuring invalid digital signature')
                    }
                    for (const row of rows) {
                        let doc = row.doc
                        const id = row.id, xml = util.signature(sign.sign(ca, util.j2x((await Service.getcol(doc)), id), id), doc.idt)
                        await dbs.query(sql, [status, xml, new Date(), id, taxc])
                        doc.status = 3
                        if (useJobSendmail) {
                            await Service.insertDocTempJob(id, doc, xml)
                        } else {
                            if (!util.isEmpty(doc.bmail)) {
                                let mailstatus = await email.rsmqmail(doc)
                                if (mailstatus) await Service.updateSendMailStatus(doc)
                            }
                            if (!util.isEmpty(doc.btel)) {
                                let smsstatus = await sms.smssend(doc)
                                if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                            }
                        }

                        const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                        logging.ins(req, sSysLogs, next)
                        count++
                    }
                }
                else {
                    //hunglq chinh sua thong bao loi
                    let kqs, usr, pwd
                    try {
                        if (signtype == 3) {
                            usr = org.usr
                            pwd = util.decrypt(org.pwd)
                            kqs = await fcloud.xml(usr, pwd, taxc, rows)
                        }
                        else kqs = await hsm.xml(rows)
                    } catch (err) {
                        throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n Error configuring invalid digital signature')
                    }
                    for (const row of kqs) {
                        let doc = JSON.parse(row.doc)
                        const id = row.id, xml = util.signature(row.xml, doc.idt)
                        await dbs.query(sql, [status, xml, new Date(), id, taxc])
                        doc.status = 3
                        if (useJobSendmail) {
                            await Service.insertDocTempJob(id, doc, xml)
                        } else {
                            if (!util.isEmpty(doc.bmail)) {
                                let mailstatus = await email.rsmqmail(doc)
                                if (mailstatus) await Service.updateSendMailStatus(doc)
                            }
                            if (!util.isEmpty(doc.btel)) {
                                let smsstatus = await sms.smssend(doc)
                                if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                            }
                        }
                        const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                        logging.ins(req, sSysLogs, next)
                        count++
                    }
                }
                res.json(count)
            }
        }
        catch (err) {
            next(err)
        }
    },
    apprput: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, body = req.body, incs = body.incs, signs = body.signs
            let i = 1, str = "", binds = []
            for (const row of incs) {
                str += `$${i++},`
                binds.push(row)
            }
            str = str.slice(0, -1)
            binds.push(taxc)
            let sql, result, rows, count = 0, status = JSON.stringify({ status: 3 })
            sql = `select id,doc,bmail from s_inv where id in (${str}) and stax=$${i} and status=2 `
            result = await dbs.query(sql, binds)
            rows = result.rows
            for (let row of rows) {
                let doc = row.doc
                const id = row.id, sign = signs.find(item => item.inc === id)
                if (sign && sign.xml) {
                    const xml = util.signature(Buffer.from(sign.xml, "base64").toString(), doc.idt)
                    await dbs.query(`update s_inv set doc=doc||$1,xml=$2,dt=$3 where id=$4 and stax=$5 and status=2`, [status, xml, new Date(), id, taxc])
                    doc.status = 3
                    if (useJobSendmail) {
                        await Service.insertDocTempJob(id, doc, xml)
                    } else {
                        if (!util.isEmpty(doc.bmail)) {
                            let mailstatus = await email.rsmqmail(doc)
                            if (mailstatus) await Service.updateSendMailStatus(doc)
                        }
                        if (!util.isEmpty(doc.btel)) {
                            let smsstatus = await sms.smssend(doc)
                            if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                        }
                    }
                    const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                    logging.ins(req, sSysLogs, next)
                    count++
                }
            }
            res.json(count)
        }
        catch (err) {
            next(err)
        }
    },
    iwh: async (req, all, decodereq) => {
        const token = (!decodereq) ? SEC.decode(req) : req.token, u = token.u, query = req.query, filter = JSON.parse(query.filter), sort = query.sort
        let keys = Object.keys(filter), kar = ["ou", "status", "type", "form", "serial", "paym", "curr", "cvt"], i = 4, rows
        let val, tree, ext = "", extxls = "", cols, where = `where idt between $1 and $2 and stax=$3`, binds = [new Date(filter.fd), new Date(moment(filter.td).endOf("day")), token.taxc], order
        if (serial_usr_grant) {
            where = `, (select taxc, type "types", form forms, serial serials from s_serial ss, s_seusr ss2 where ss.id = ss2.se and ss2.usrid = $1) ss where idt between $2 and $3 and stax=$4`
            binds = [token.uid, new Date(filter.fd), new Date(moment(filter.td).endOf("day")), token.taxc]
            i = 5
        }
        if (grant && token.ou == u) {
            //TODO
            if (all) {
                const sql = `with recursive tree as (select ${u} id union all select child.id from s_ou child,tree parent where parent.id=child.pid) select id from tree `
                const result = await dbs.query(sql)
                where += `and ou in(`
                rows = result.rows
                for (var j = 0; j < rows.length; j++) {
                    if (j == rows.length - 1) {
                        where += `${rows[j].id} `
                        break
                    }
                    where += `${rows[j].id}, `
                }
                where += `) `
            }
            else {
                where += ` and ou=$${i++}`
                binds.push(u)
            }
        }
        if (keys.includes("type")) {
            cols = await COL.cache(filter["type"])
            if (cols.length > 0) {
                let exts = []
                let extsxls = []
                for (const col of cols) {
                    let cid = col.id
                    exts.push(`${cid} ${cid}`)
                    //Xu ly rieng cho excel neu la truong date thi format lai
                    if (col.view == "datepicker")
                        extsxls.push(`to_char(${cid},'DD/MM/YYYY') ${cid}`)
                    else
                        extsxls.push(`${cid} ${cid}`)
                }
                ext = `,${exts.join()}`
                extxls = `,${extsxls.join()}`
            }
        }
        for (const key of kar) {
            if (keys.includes(key)) {
                where += ` and ${key}=$${i++}`
                binds.push(filter[key])
            }
        }
        kar.push("fd", "td")
        for (const key of kar) {
            delete keys[key]
        }
        for (const key of keys) {
            val = filter[key]
            if (val && !kar.includes(key)) {
                switch (key) {
                    case "seq":
                        where += ` and seq like $${i++}`
                        binds.push(`%${val}`)
                        break
                    case "btax":
                        where += ` and btax like $${i++}`
                        binds.push(`%${val}%`)
                        break
                    case "bacc":
                        where += ` and bacc like $${i++}`
                        binds.push(`%${val}%`)
                        break
                    case "user":
                        where += ` and uc like $${i++}`
                        binds.push(`%${val}%`)
                        break
                    case "vat":
                        let catobj = JSON.parse(JSON.stringify(config.CATTAX))
                        let objtax = catobj.find(x => x.vatCode == val)
                        where += ` and (doc->'tax') @> '[{"vrt":${String(objtax.vrt)}}]'` //` and doc->'$.tax[*].vrt' ? (@==${val})`
                        //binds.push(`"${val}"`)
                        break
                    case "adjtyp":
                        switch (val) {
                            case "3":
                                where += ` and cid is not null and cde like $${i++}`
                                binds.push("Bị thay thế%")
                                break
                            case "4":
                                where += ` and cid is not null and cde like $${i++}`
                                binds.push("Bị điều chỉnh%")
                                break
                            default:
                                where += ` and adjtyp=$${i++}`
                                binds.push(val)
                                break
                        }
                        break
                    default:
                        let b = true
                        if (CARR.includes(key) && cols) {
                            let obj = cols.find(x => x.id == key)
                            if (typeof obj !== "undefined") {
                                const view = obj.view
                                if (view == "datepicker") {
                                    b = false
                                    where += ` and date(${key})=$${i++}`
                                    binds.push(new Date(val))
                                }
                                else if (view == "multicombo") {
                                    b = false
                                    where += ` and ${key} in (${val.split(",")})`
                                }
                                else if (view == "combo") {
                                    b = false
                                    where += ` and ${key}=$${i++}`
                                    binds.push(val)
                                }
                                else if (view == "text") {
                                    if (obj.type == "number") {
                                        b = false
                                        where += ` and ${key} like $${i++}`
                                        binds.push(`%${val}%`)
                                    }
                                }
                            }
                        }
                        if (b) {
                            where += ` and upper(${key}) like $${i++}`
                            binds.push(`%${val.toString().toUpperCase()}%`)
                        }
                        break
                }
            }
        }
        if (serial_usr_grant) {
            where += ` and stax = ss.taxc and type = ss.types and form = ss.forms and serial = ss.serials`
        }
        if (sort) {
            Object.keys(sort).forEach(key => {
                order = `order by ${key} ${sort[key]}`
            })
        }
        else order = "order by idt desc,ABS(id) desc, type, form, serial, seq"
        return { where: where, binds: binds, ext: ext, order: order, extxls: extxls }
    },
    xls: async (req, res, next) => {
        try {
            const fn = "temp/KQTCHD.xlsx", query = req.query, filter = JSON.parse(query.filter)
            let json, rows
            const iwh = await Service.iwh(req, true)
            let cols = await COL.cacheLang(filter["type"], req.query.lang)
            let colhds = req.query.colhd.split(",")
            let extsh = []
            for (const col of cols) {
                let clbl = col.label
                extsh.push(clbl)
            }
            const sql = `select id,sec,ic,uc,to_char(idt,'DD/MM/YYYY') idt,case when status=1 then 'Chờ cấp số' when status=2 then 'Chờ duyệt' when status=3 then 'Đã duyệt' when status=4 then 'Đã hủy' when status=5 then 'Đã gửi' when status=6 then 'Chờ hủy' end status,ou,type,form,serial,seq,btax,bname,buyer,baddr,note,sumv,vatv,totalv,sum,vat,total,adjdes,cde,bmail,curr,sname sname${iwh.extxls} from s_inv ${iwh.where} ${iwh.order} limit ${config.MAX_ROW_EXCEL_EXPORT} offset 0`
            const result = await dbs.query(sql, iwh.binds)
            rows = result.rows
            //Them ham tra gia tri theo mang de hien thi tương ứng voi tung cot
            for (const row of rows) {
                if (ENT != "fhs") {
                    row.sumv = parseInt(row.sumv)
                    //row.vatv = parseInt(row.vatv)
                    row.totalv = parseInt(row.totalv)
                }
                try {
                    row.ou = await oubi(row.ou)
                }
                catch (err) {

                }
                let extsval = []
                for (const col of cols) {
                    extsval.push(row[col.id])
                }
                row.extsval = extsval
            }
            json = { table: rows, extsh: extsh, colhds: colhds }
            const file = path.join(__dirname, "..", "..", "..", fn)
            const xlsx = fs.readFileSync(file)
            const template = new xlsxtemp(xlsx)
            template.substitute(1, json)
            res.end(template.generate(), "binary")
        }
        catch (err) {
            next(err)
        }
    },
    appget: async (req, res, next) => {
        try {
            const query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 10, iwh = await Service.iwh(req, true)
            let sql, result, ret
            sql = `select 0 chk,id,sec,ic,uc,curr,pid,cid,idt,status,ou,type,form,serial,seq,btax,bname,buyer,baddr,note,sumv,vatv,totalv,sum,vat,total,adjdes,adjtyp,cde,cvt,bmail${iwh.ext} from s_inv ${iwh.where} ${iwh.order} limit ${count} offset ${start}`
            result = await dbs.query(sql, iwh.binds)
            ret = { data: result.rows, pos: start }
            if (start == 0) {
                sql = `select count(*) total from s_inv ${iwh.where}`
                result = await dbs.query(sql, iwh.binds)
                ret.total_count = result.rows[0].total
            }
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    get: async (req, res, next) => {
        try {
            //console.time("timeget")
            const query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 10, iwh = await Service.iwh(req, true)
            let sql, result, ret, extcol_fhs = ["fhs", "fbc"].includes(config.ent) ? `, (
                select
                    sum(c5*quantity)
                from
                    (
                    select
                        ('0' || c5)::numeric(17, 2) c5,
                        ('0' || quantity)::numeric(17, 2) quantity
                    from
                        (
                        select
                            s.id,
                            s.doc,
                            (json_array_elements(doc::json->'items') ->> 'c5'::text)::text c5,
                            (json_array_elements(doc::json->'items') ->> 'quantity'::text)::text quantity
                        from
                            s_inv s
                        where
                            s.id = s_inv.id) s
                    group by
                        s.c5,
                        s.quantity ) as s) coverprice` : ""
            sql = `select 0 chk,id,sec,ic,uc,curr,pid,cid,idt,status,ou,type,form,serial,seq,btax,bname,buyer,baddr,note,sumv,vatv,totalv,sum,vat,total,adjdes,adjtyp,cde,cvt,bmail${iwh.ext}${extcol_fhs}, error_msg_van "error_msg_van" from s_inv ${iwh.where} ${iwh.order}  limit ${count} offset ${start}`
            result = await dbs.query(sql, iwh.binds)
            ret = { data: result.rows, pos: start }
            if (start == 0) {
                sql = `select count(*) total from s_inv ${iwh.where}`
                result = await dbs.query(sql, iwh.binds)
                ret.total_count = result.rows[0].total
            }
            //console.timeEnd("timeget")
            const sSysLogs = { fnc_id: 'inv_get', src: config.SRC_LOGGING_DEFAULT, dtl: `Tra cứu hóa đơn ${query.filter}`, msg_id: "", doc: query.filter };
            logging.ins(req, sSysLogs, next)
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    signget: async (req, res, next) => {
        try {
            const token = SEC.decode(req), u = token.u
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const id = req.query.id, taxc = token.taxc
            let sql, result, binds = [id, taxc]
            sql = `select id,doc,bmail from s_inv where id=$1 and stax=$2 and status=2 `
            if (grant) {
                sql += ' and ou=$3'
                binds.push(u)
            }
            result = await dbs.query(sql, binds)
            const rows = result.rows
            if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n There is no data to sign")
            const org = await ous.obt(taxc), signtype = org.sign, row = rows[0]
            let doc = row.doc
            let xml
            if (signtype == 1) {
                const usr = org.usr, pwd = util.decrypt(org.pwd)
                xml = util.j2x((await Service.getcol(doc)), id)
                //const arr = [{ id: id, xml: Buffer.from(xml).toString("base64") }]
                const arr = [{ inc: id, xml: Buffer.from(xml).toString("base64") }]
                res.json({ lib: usr, sid: 0, pin: pwd, mst: taxc, date: new Date(), ref: config.PATH_XML, arr: arr })
            }
            else {
                if (signtype == 2) {
                    const pwd = util.decrypt(org.pwd)
                    const ca = await sign.ca(taxc, pwd)
                    xml = util.signature(sign.sign(ca, util.j2x((await Service.getcol(doc)), id), id), doc.idt)
                }
                else if (signtype == 3) {
                    const usr = org.usr, pwd = util.decrypt(org.pwd)
                    const kqs = await fcloud.xml(usr, pwd, taxc, rows), kq = kqs[0]
                    xml = kq.xml
                }
                else {
                    const kqs = await hsm.xml(rows), kq = kqs[0]
                    xml = util.signature(kq.xml, doc.idt)
                }
                await dbs.query(`update s_inv set doc=doc||$1,xml=$2,dt=$3 where id=$4 and stax=$5 and status=2`, [JSON.stringify({ status: 3 }), xml, new Date(), id, taxc])
                doc.status = 3
                if (useJobSendmail) {
                    await Service.insertDocTempJob(doc.id, doc, xml)
                } else {
                    if (!util.isEmpty(doc.bmail)) {
                        let mailstatus = await email.rsmqmail(doc)
                        if (mailstatus) await Service.updateSendMailStatus(doc)
                    }
                    if (!util.isEmpty(doc.btel)) {
                        let smsstatus = await sms.smssend(doc)
                        if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                    }
                }
                const sSysLogs = { fnc_id: 'inv_signget', src: config.SRC_LOGGING_DEFAULT, dtl: `Ký số hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                logging.ins(req, sSysLogs, next)
                res.json(1)
            }

        }
        catch (err) {
            next(err)
        }
    },
    signput: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, body = req.body, signs = body.signs
            let sign = signs[0], id = sign.inc, xml = Buffer.from(sign.xml, "base64").toString()
            const result = await dbs.query(`select doc,bmail from s_inv where id=$1 and stax=$2 and status=2`, [id, taxc])
            const rows = result.rows
            if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n There is no data to sign")
            let row = rows[0]
            let doc = JSON.parse(row.doc)
            xml = util.signature(xml, doc.idt)
            await dbs.query(`update s_inv set doc=doc||$1,xml=$2,dt=$3 where id=$4 and stax=$5 and status=2`, [JSON.stringify({ status: 3 }), xml, new Date(), id, taxc])
            doc.status = 3
            if (useJobSendmail) {
                await Service.insertDocTempJob(doc.id, doc, xml)
            } else {
                if (!util.isEmpty(doc.bmail)) {
                    let mailstatus = await email.rsmqmail(doc)
                    if (mailstatus) await Service.updateSendMailStatus(doc)
                }
                if (!util.isEmpty(doc.btel)) {
                    let smsstatus = await sms.smssend(doc)
                    if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                }
            }
            const sSysLogs = { fnc_id: 'inv_signget', src: config.SRC_LOGGING_DEFAULT, dtl: `Ký số hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
            logging.ins(req, sSysLogs, next)
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },
    put: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, id = req.params.id
            await chkou(id, token)
            await Service.delrefno(body)
            const hdb = await Service.checkrefno(body)
            if (hdb.length > 0) throw new Error(`Số bút toán đã tồn tại: ${hdb.join()} \n(Entry number already exists: ${hdb.join()})`)

            const checkrefdate = await Service.checkrefdate(body)
            if (checkrefdate.length > 0) throw new Error(`Ngày bút toán lớn hơn ngày hóa đơn: ${checkrefdate.join(',')} \n(Ref Date is greater than Invoice Date: ${checkrefdate.join(',')})`)

            await dbs.query(`update s_inv set doc=$1,idt=$2 where id=$3 and status=$4`, [JSON.stringify(body), new Date(body.idt), id, body.status])
            await Service.saverefno(body)
            const sSysLogs = { fnc_id: 'inv_put', src: config.SRC_LOGGING_DEFAULT, dtl: `Sửa hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(body) };
            logging.ins(req, sSysLogs, next)
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },
    ins: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const taxc = token.taxc, uid = token.uid, userou = await user.obid(uid), ou = ["fhs", "fbc"].includes(config.ent) ? userou.ou : token.ou, org = await ous.obt(taxc), AUTO = (org.seq === 1), body = req.body
            let invs = body.invs
            const sql = `insert into s_inv(id,pid,sec,ic,idt,ou,uc,doc) values ($1,$2,$3,$4,$5,$6,$7,$8)`
            //Thêm đoạn sort lại danh sách hóa đơn trước khi cấp 
            if (AUTO) {
                //Ghep dữ liệu cần sort
                for (let inv of invs) {
                    inv.sortprop = `#${(moment(inv.idt, dtf).format('YYYYMMDD'))}#${inv.ic}`
                }
                invs = util.sortobj(invs, 'sortprop', 0)
            }
            for (let inv of invs) {
                let ic, id, sec, form, serial, type, pid, idt, seq, s7, rs
                try {
                    form = inv.form
                    serial = inv.serial
                    type = form.substr(0, 6)
                    ic = inv.ic
                    inv.sid = ic
                    pid = inv.pid
                    idt = moment(inv.idt, dtf)
                    id = await redis.incr("INC")
                    sec = util.generateSecCode(config.FNC_GEN_SEC, id)
                    inv.id = id
                    inv.sec = sec
                    inv.ou = ou
                    inv.type = type
                    inv.name = util.tenhd(type)
                    inv.stax = taxc
                    inv.sname = org.name
                    inv.saddr = org.addr
                    inv.smail = org.mail
                    inv.stel = org.tel
                    inv.taxo = org.taxo
                    inv.sacc = org.acc
                    inv.sbank = org.bank
                    const ouob = await ous.obid(ou)
                    inv.systemCode = ouob.code
                    if ((["fhs", "fbc"].includes(config.ent))) {
                        let useroucreate = await ous.obid(ou)
                        inv.c7 = useroucreate.name
                        inv.c8 = useroucreate.addr
                        inv.c9 = useroucreate.tel
                    }
                    if (AUTO) {
                        const sqlcheck = `select count(*) rcount from s_inv where stax=$1 and (status in (2,3,5,6) or (status = 4 and xml is not null)) and form=$2 and serial=$3 and idt>$4`
                        const resultcheck = await dbs.query(sqlcheck, [taxc, form, serial, (moment(idt.toDate()).endOf("day")).toDate()])
                        const rowscheck = resultcheck.rows
                        if (rowscheck[0].rcount > 0) throw new Error("Đã tồn tại hóa đơn được cấp số có ngày hóa đơn lớn hơn hóa đơn hiện tại \n (A numbered invoice already exists that has a larger invoice date than the current invoice)")
                        seq = await SERIAL.sequence(taxc, form, serial, inv.idt)
                        s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                        inv.seq = s7
                        inv.status = 2
                    }
                    else {
                        s7 = "......."
                        inv.status = 1
                    }
                    rs = await dbs.query(sql, [id, pid, sec, ic, idt.toDate(), ou, uid, JSON.stringify(inv)])
                    if (rs.rowCount > 0) {
                        if (pid) {
                            try {
                                const cde = `Bị thay thế bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${idt.format(mfd)} mã ${sec}`
                                let cdt = moment(idt.toDate()).format(mfd)
                                await dbs.query(`update s_inv set doc=doc||$1,cid=$2,cde=$3,cdt=$4 where id=$5 and status=3`, [JSON.stringify({ status: 4, cde: cde, cdt: cdt }), id, cde, idt.toDate(), pid])
                            } catch (error) {
                                logger4app.error(error)
                            }
                        }
                    }
                    else throw new Error(`Không cấp số cho hóa đơn ${id}  \n (Do not issue number to the invoice: ${id})`)
                    const sSysLogs = { fnc_id: 'xls_upl', src: config.SRC_LOGGING_DEFAULT, dtl: 'Đọc hóa đơn từ excel', msg_id: id, doc: JSON.stringify(inv) };
                    logging.ins(req, sSysLogs, next)
                } catch (e) {
                    if (AUTO) await SERIAL.err(taxc, form, serial, seq)
                    throw e
                }
            }
            res.json(invs.length)
        }
        catch (err) {
            next(err)
        }
    },
    upds: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const body = req.body, invs = body.invs
            for (let inv of invs) {
                try {
                    const pid = inv.pid, doc = await rbi(pid), idt = doc.idt
                    delete inv["pid"]
                    for (let i in inv) {
                        doc[i] = inv[i]
                    }
                    doc.idt = idt
                    await dbs.query(`update s_inv set doc=$1 where id=$2 and status=2`, [JSON.stringify(doc), pid])
                    const sSysLogs = { fnc_id: 'inv_adj', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật hóa đơn id ${pid}`, msg_id: pid, doc: JSON.stringify(doc) };
                    logging.ins(req, sSysLogs, next)
                } catch (e) {
                    throw e
                }
            }
            res.json(invs.length)
        }
        catch (err) {
            next(err)
        }
    },
    wcan: async (req, res, next) => {
        const token = SEC.decode(req), taxc = token.taxc, body = req.body, invs = body.invs
        for (const item of invs) {
            try {
                const cancel = { status: 6, cancel: { typ: 3, ref: item.ref, rdt: item.rdt, rea: item.rea } }
                const result = await dbs.query(`update s_inv set doc=doc||$1 where id=$2 and stax=$3 and status in (2,3)`, [JSON.stringify(cancel), item.id, taxc])
                if (result.rowCount > 0) {
                    if (item.pid) await dbs.query(`update s_inv set cid=null,cde=null where id=$1`, [item.pid])
                    item.error = 'OK'
                }
                else item.error = 'NOT OK'
            }
            catch (e) {
                item.error = e.message
            }
        }
        res.json({ save: true, data: invs })
    },
    sav: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body
            if (grant && token.ou !== token.u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)
            const hdb = await Service.checkrefno(body)
            if (hdb.length > 0) throw new Error(`Số bút toán đã tồn tại: ${hdb.join()} \n(Entry number already exists: ${hdb.join()})`)

            const checkrefdate = await Service.checkrefdate(body)
            if (checkrefdate.length > 0) throw new Error(`Ngày bút toán lớn hơn ngày hóa đơn: ${checkrefdate.join(',')} \n(Ref Date is greater than Invoice Date: ${checkrefdate.join(',')})`)

            const taxc = token.taxc, uid = token.uid, ou = token.ou, org = await ous.obt(taxc), form = body.form, serial = body.serial, id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC, id)
            let seq, s7, result, row
            const dt = moment(body.idt), eod = dt.endOf('day').toDate(), dts = dt.format(mfd), dtsc = dt.format('YYYYMMDD'), idtc = (moment(new Date())).format('YYYYMMDD')
            try {
                if (dtsc != idtc) {
                    result = await dbs.query(`select count(*) rc from s_inv where idt>$1 and form=$2 and serial=$3 and (status in (2,3,5,6) or (status = 4 and xml is not null))`, [eod, form, serial])
                    row = result.rows[0]
                    if (row.rc > 0) throw new Error(`Tồn tại HĐ có ngày lớn hơn ${moment(body.idt).format('DD/MM/YYYY')} và đã cấp số \n (Invoice has a larger date ${moment(body.idt).format('DD/MM/YYYY')} and issued a number)`)
                }

                body.id = id
                body.sec = sec
                body.stax = org.taxc
                body.sname = org.name
                body.saddr = org.addr
                body.smail = org.mail
                body.stel = org.tel
                body.taxo = org.taxo
                body.sacc = org.acc
                body.sbank = org.bank
                const ouob = await ous.obid(ou)
                body.systemCode = ouob.code
                if ((["fhs", "fbc"].includes(config.ent))) {
                    let useroucreate = await ous.obid(ou)
                    body.c7 = useroucreate.name
                    body.c8 = useroucreate.addr
                    body.c9 = useroucreate.tel
                }
                seq = await SERIAL.sequence(taxc, form, serial, body.idt)
                s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                body.seq = s7
                body.status = 2
                await dbs.query(`insert into s_inv(id,sec,idt,ou,uc,doc) values ($1,$2,$3,$4,$5,$6)`, [id, sec, new Date(body.idt), ou, uid, JSON.stringify(body)])
                await Service.saverefno(body)
                const sSysLogs = { fnc_id: 'inv_post', src: config.SRC_LOGGING_DEFAULT, dtl: `Lập hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(body) };
                logging.ins(req, sSysLogs, next)
                res.json(id)
            }
            catch (err) {
                await SERIAL.err(taxc, form, serial, seq)
                throw err
            }
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body
            if (grant && token.ou !== token.u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)
            const hdb = await Service.checkrefno(body)
            if (hdb.length > 0) throw new Error(`Số bút toán đã tồn tại: ${hdb.join()} \n(Entry number already exists: ${hdb.join()})`)
            const checkrefdate = await Service.checkrefdate(body)
            if (checkrefdate.length > 0) throw new Error(`Ngày bút toán lớn hơn ngày hóa đơn: ${checkrefdate.join(',')} \n(Ref Date is greater than Invoice Date: ${checkrefdate.join(',')})`)

            const taxc = token.taxc, uid = token.uid, ou = token.ou, org = await ous.obt(taxc), AUTO = (org.seq === 1), form = body.form, serial = body.serial, id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC, id)
            let seq, s7, result, row
            const dt = moment(body.idt), eod = dt.endOf('day').toDate(), dts = dt.format(mfd), dtsc = dt.format('YYYYMMDD'), idtc = (moment(new Date())).format('YYYYMMDD')
            try {
                if (dtsc != idtc) {
                    result = await dbs.query(`select count(*) rc from s_inv where idt>$1 and form=$2 and serial=$3 and stax=$4 and (status in (2,3,5,6) or (status = 4 and xml is not null))`, [eod, form, serial, taxc])
                    row = result.rows[0]
                    if (row.rc > 0) throw new Error(`Tồn tại HĐ có ngày lớn hơn ${moment(body.idt).format('DD/MM/YYYY')} và đã cấp số \n (Invoice has a larger date ${moment(body.idt).format('DD/MM/YYYY')} and issued a number)`)
                }

                result = await dbs.query(`select count(*) rc from s_serial where fd>$1 and form=$2 and serial=$3 and status=1 and taxc=$4 `, [eod, form, serial, taxc])
                row = result.rows[0]
                if (row.rc > 0) throw new Error(`Ngày hiệu lực TBPH phải nhỏ hơn hoặc bằng ngày hóa đơn ${moment(body.idt).format('DD/MM/YYYY')}  \n (The serial effective date must be less than or equal to the invoice date ${moment(row.fd).format('DD/MM/YYYY')})`)

                body.id = id
                body.sec = sec
                body.ou = ou
                body.stax = org.taxc
                body.sname = org.name
                body.saddr = org.addr
                body.smail = org.mail
                body.stel = org.tel
                body.taxo = org.taxo
                body.sacc = org.acc
                body.sbank = org.bank
                const ouob = await ous.obid(ou)
                body.systemCode = ouob.code
                if ((["fhs", "fbc"].includes(config.ent))) {
                    let useroucreate = await ous.obid(ou)
                    body.c7 = useroucreate.name
                    body.c8 = useroucreate.addr
                    body.c9 = useroucreate.tel
                }
                if (AUTO) {
                    seq = await SERIAL.sequence(taxc, form, serial, body.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    body.seq = s7
                    body.status = 2
                }
                else body.status = 1
                await dbs.query(`insert into s_inv(id,sec,idt,ou,uc,doc) values ($1,$2,$3,$4,$5,$6)`, [id, sec, new Date(body.idt), ou, uid, JSON.stringify(body)])
                await Service.saverefno(body)
                const sSysLogs = { fnc_id: 'inv_post', src: config.SRC_LOGGING_DEFAULT, dtl: `Lập hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(body) };
                logging.ins(req, sSysLogs, next)
                res.json(id)
            }
            catch (err) {
                if (AUTO) await SERIAL.err(taxc, form, serial, seq)
                throw err
            }
        }
        catch (err) {
            next(err)
        }
    },
    adj: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, pid = body.pid
            await chkou(pid, token)
            const uid = token.uid, ou = token.ou, taxc = body.stax, form = body.form, serial = body.serial, org = await ous.obt(taxc), AUTO = (org.seq === 1), id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC, id), idt = new Date(body.idt)
            let seq, s7, result, insid
            try {
                body.id = id
                body.sec = sec
                if (AUTO) {
                    seq = await SERIAL.sequence(taxc, form, serial, body.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    body.seq = s7
                    body.status = 2
                }
                else {
                    s7 = "......."
                    body.status = 1
                }
                const ouob = await ous.obid(ou)
                body.systemCode = ouob.code
                if ((["fhs", "fbc"].includes(config.ent))) {
                    let useroucreate = await ous.obid(ou)
                    body.c7 = useroucreate.name
                    body.c8 = useroucreate.addr
                    body.c9 = useroucreate.tel
                }
                result = await dbs.query(`insert into s_inv (id,pid,sec,idt,ou,uc,doc) values ($1,$2,$3,$4,$5,$6,$7) RETURNING id`, [id, pid, sec, idt, ou, uid, JSON.stringify(body)])
                if (result.rowCount) insid = result.rows[0].id
                const cde = `Bị điều chỉnh bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${moment(idt).format(mfd)} mã ${sec}`
                const pdoc = await rbi(pid)
                pdoc.cde = cde
                let cdt = moment(new Date()).format(dtf)
                pdoc.cdt = cdt
                await dbs.query(`update s_inv set cid=$1,cde=$2,doc=$3 where id=$4`, [id, cde, JSON.stringify(pdoc), pid])
                const sSysLogs = { fnc_id: 'inv_adj', src: config.SRC_LOGGING_DEFAULT, dtl: `Điều chỉnh hóa đơn id ${pid}`, msg_id: pid, doc: JSON.stringify(pdoc) };
                logging.ins(req, sSysLogs, next)
                res.json(insid)
            }
            catch (err) {
                if (AUTO) await SERIAL.err(taxc, form, serial, seq)
                throw err
            }
        }
        catch (err) {
            next(err)
        }
    },
    adjniss: async (req, res, next) => {
    },
    rep: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            let body = req.body, pid = body.pid
            await chkou(pid, token)

            const hdb = await Service.checkrefno(body)
            if (hdb.length > 0) throw new Error(`Số bút toán đã tồn tại: ${hdb.join()} \n (Entry number already exists: ${hdb.join()})`)
            const checkrefdate = await Service.checkrefdate(body)
            if (checkrefdate.length > 0) throw new Error(`Ngày bút toán lớn hơn ngày hóa đơn: ${checkrefdate.join(',')} \n(Ref Date is greater than Invoice Date: ${checkrefdate.join(',')})`)


            const uid = token.uid, ou = token.ou, taxc = body.stax, form = body.form, serial = body.serial, org = await ous.obt(taxc), AUTO = (org.seq === 1), id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC, id), idt = new Date(body.idt)
            let seq, s7, result, insid
            try {
                body.id = id
                body.sec = sec
                // AutoSeq exists nghĩa là PHAT HANH ở THAY THE
                if (AUTO || body.AutoSeq) {
                    if (body.AutoSeq) delete body["AutoSeq"]
                    seq = await SERIAL.sequence(taxc, form, serial, body.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    body.seq = s7
                    body.status = 2
                }
                else {
                    s7 = "......."
                    body.status = 1
                }
                const ouob = await ous.obid(ou)
                body.systemCode = ouob.code
                if ((["fhs", "fbc"].includes(config.ent))) {
                    let useroucreate = await ous.obid(ou)
                    body.c7 = useroucreate.name
                    body.c8 = useroucreate.addr
                    body.c9 = useroucreate.tel
                }
                result = await dbs.query(`insert into s_inv (id,pid,sec,idt,ou,uc,doc) values ($1,$2,$3,$4,$5,$6,$7) RETURNING id`, [id, pid, sec, idt, ou, uid, JSON.stringify(body)])
                await Service.saverefno(body)
                if (result.rowCount) insid = result.rows[0].id
                const cde = `Bị thay thế bởi HĐ số ${s7} ký hiệu ${body.serial} mẫu số ${body.form} ngày ${moment(idt).format(mfd)} mã ${sec}`
                let cdt = moment(new Date()).format(dtf)
                await dbs.query(`update s_inv set doc=doc||$1,cid=$2,cde=$3,cdt=$4 where id=$5`, [JSON.stringify({ status: 4, cde: cde, cdt: cdt, canceller: token.uid }), id, cde, new Date(), pid])
                const sSysLogs = { fnc_id: 'inv_replace', src: config.SRC_LOGGING_DEFAULT, dtl: `Thay thế hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(body) };
                logging.ins(req, sSysLogs, next)
                res.json(insid)
            }
            catch (err) {
                if (AUTO) await SERIAL.err(taxc, form, serial, seq)
                throw err
            }
        }
        catch (err) {
            next(err)
        }
    },
    go24: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, body = req.body, form = body.form, serial = body.serial, seq = body.seq, id = body.id, d = body.d, direct = d > 0 ? "tiến" : "lùi"
            await chkou(id, token)
            const idt = new Date(body.idt)
            idt.setDate(idt.getDate() + d)
            const dt = moment(idt), sod = dt.startOf('day').toDate(), eod = dt.endOf('day').toDate(), dts = dt.format(mfd)
            let result, row
            if (d > 0) {
                const today = moment().endOf('day').toDate()
                if (eod > today) throw new Error(`Ngày tiến hóa đơn ${dts} lớn hơn ngày hiện tại \n (The forward date of the invoice ${dts} is greater than the current date) `)
            }
            else {
                result = await dbs.query(`select count(*) rc from s_serial where taxc=$1 and form=$2 and serial=$3 and fd>$4`, [taxc, form, serial, eod])
                row = result.rows[0]
                if (row.rc > 0) throw new Error(`Ngày lùi hóa đơn ${dts} không có TBPH \n (The back date of the invoice ${dts} doesn't have released information)`)
            }
            if (seq) {
                const iseq = parseInt(seq)
                result = await dbs.query(`select count(*) rc from s_inv where idt<$1 and form=$2 and serial=$3 and (status=1 or ((status in (2,3,5,6) or (status = 4 and xml is not null)) and cast(seq as int)>$4))`, [sod, form, serial, iseq])
                row = result.rows[0]
                if (row.rc > 0) throw new Error(`Hóa đơn không ${direct} được: tồn tại HĐ có ngày nhỏ hơn ${dts} chưa cấp số hoặc có số lớn hơn ${seq} \n (Invoice can not ${direct}: Existing invoices with dates less than ${dts} and not issued a number or number greater than ${seq}`)
                result = await dbs.query(`select count(*) rc from s_inv where idt>$1 and form=$2 and serial=$3 and (status in (2,3,5,6) or (status = 4 and xml is not null)) and cast(seq as int)<$4`, [eod, form, serial, iseq])
                row = result.rows[0]
                if (row.rc > 0) throw new Error(`Hóa đơn không ${direct} được: tồn tại HĐ có ngày lớn hơn ${dts} và số lớn hơn ${seq} \n (Invoice can not ${direct} : Existing invoices with dates greater than ${dts} and a greater number ${seq}`)
            }
            else {
                result = await dbs.query(`select count(*) rc from s_inv where idt>$1 and form=$2 and serial=$3 and (status in (2,3,5,6) or (status = 4 and xml is not null))`, [eod, form, serial])
                row = result.rows[0]
                if (row.rc > 0) throw new Error(`Hóa đơn không ${direct} được : tồn tại HĐ có ngày lớn hơn ${dts} và đã cấp số \n (Invoice can not ${direct} : Existing invoices with dates greater than ${dts} and isused a number ${seq}`)
            }
            await dbs.query(`update s_inv set doc=doc||$1,idt=$2 where id=$3 and status in (1,2)`, [JSON.stringify({ idt: moment(sod).format(dtf) }), sod, id])
            res.json(1)
        } catch (err) {
            next(err)
        }
    },
    status: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, id = req.params.id
            let doc, status = req.params.status
            doc = await rbi(id)
            let oldstatus = doc.status
            await chkou(id, token)
            if (status == 3 || status == 2) {
                await dbs.query(`update s_inv set doc=doc-'cancel' where id=$1`, [id])
                const r = await dbs.query(`select 1 from s_inv where id=$1 and xml is not null LIMIT 1`, [id])
                if (r.rowCount > 0) status = 3
                else status = 2
                //Xóa biên bản
                if (UPLOAD_MINUTES) await dbs.query(`DELETE FROM s_inv_file where id=$1`, [id])
            }
            else if (status == 4) {
                doc = await rbi(id)
                if (doc.pid && doc.adj.typ == 2) await dbs.query(`update s_inv set cid=null,cde=null where id=$1`, [doc.pid])
                let cdt = moment(new Date()).format(dtf)
                await dbs.query(`update s_inv set doc=doc||$1, cdt=$2 where id=$3 and stax=$4`, [JSON.stringify({ cdt: cdt, cancel: { typ: 3, ref: id, rdt: moment().format(dtf) }, canceller: token.uid }), new Date(), id, taxc])
            }
            let result = await dbs.query(`update s_inv set doc=doc||$1 where id=$2 and stax=$3`, [JSON.stringify({ status: status }), id, taxc])
            if (result.rowCount && (oldstatus == 3 || oldstatus == 6)) {
                if (useJobSendmail) {
                    if (((status == 4) && !util.isEmpty(doc.bmail)) || ((status == 4) && !util.isEmpty(doc.btel))) {
                        doc.status = 4
                        await Service.insertDocTempJob(doc.id, doc, ``)
                    }
                } else {
                    if ((status == 4) && !util.isEmpty(doc.bmail)) {
                        doc.status = 4
                        if (!util.isEmpty(doc.bmail)) {
                            let mailstatus = await email.rsmqmail(doc)
                            if (mailstatus) await Service.updateSendMailStatus(doc)
                        }
                    }
                    if ((status == 4) && !util.isEmpty(doc.btel)) {
                        doc.status = 4
                        if (!util.isEmpty(doc.btel)) {
                            let smsstatus = await sms.smssend(doc)
                            if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                        }
                    }
                }
                const sSysLogs = { fnc_id: 'inv_istatus', src: config.SRC_LOGGING_DEFAULT, dtl: `Chuyển đổi trạng thái hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                logging.ins(req, sSysLogs, next)
            }
            res.json(1)
        } catch (err) {
            next(err)
        }
    },
    cancel: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, id = req.params.id
            await chkou(id, token)
            let doc = await rbi(id), cancel = { typ: body.typ, ref: body.ref, rdt: body.rdt, rea: body.rea }
            if (doc.pid && doc.adj.typ == 2) await dbs.query(`update s_inv set cid=null,cde=null where id=$1`, [doc.pid])
            let cdt = moment(new Date()).format(dtf)
            doc = await rbi(id)
            doc.cdt = cdt
            doc.canceller = token.uid
            let oldstatus = doc.status
            doc.status = 4
            doc.cancel = cancel
            let result = await dbs.query(`update s_inv set doc=$1 where id=$2 and stax=$3`, [JSON.stringify(doc), id, token.taxc])
            if (result.rowCount && (oldstatus == 3 || oldstatus == 6)) {
                if (useJobSendmail) {
                    if ((!util.isEmpty(doc.bmail)) || (!util.isEmpty(doc.btel))) {
                        doc.status = 4
                        await Service.insertDocTempJob(doc.id, doc, ``)
                    }
                } else {
                    if (!util.isEmpty(doc.bmail)) {
                        doc.status = 4
                        if (!util.isEmpty(doc.bmail)) {
                            let mailstatus = await email.rsmqmail(doc)
                            if (mailstatus) await Service.updateSendMailStatus(doc)
                        }
                    }
                    if (!util.isEmpty(doc.btel)) {
                        doc.status = 4
                        if (!util.isEmpty(doc.btel)) {
                            let smsstatus = await sms.smssend(doc)
                            if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                        }
                    }
                }
            }
            const sSysLogs = { fnc_id: 'inv_cancel', src: config.SRC_LOGGING_DEFAULT, dtl: `Hủy hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
            logging.ins(req, sSysLogs, next)
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },
    trash: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, id = req.params.id
            await chkou(id, token)
            //Lay du lieu doc truoc khi xoa de luu log
            let doclog = await rbi(id)
            await dbs.query(`update s_inv set doc=doc||$1 where id=$2 and stax=$3 and status in (2,3)`, [JSON.stringify({ status: 6, cancel: { typ: body.typ, ref: body.ref, rdt: body.rdt, rea: body.rea } }), id, token.taxc])
            res.json(1)
            const sSysLogs = { fnc_id: 'inv_adj', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doclog) };
            logging.ins(req, sSysLogs, next)
        }
        catch (err) {
            next(err)
        }
    },
    istatus: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, body = req.body, ids = body.ids, os = parseInt(body.os), now = moment().format(dtf)
            let rc = 0, ns = body.ns
            for (let id of ids) {
                try {
                    let doc, sql, binds
                    let doclog, sSysLogs
                    doc = await rbi(id)
                    let oldstatus = doc.status
                    if (ns == 4) {
                        let cdt = moment(new Date()).format(dtf)
                        sql = `update s_inv set doc=doc||$1, cdt=$2 where id=$3 and stax=$4 and status=$5`
                        binds = [JSON.stringify({ status: ns, cdt: cdt }), new Date(), id, taxc, os]
                        doc = await rbi(id)
                        //Lay du lieu doc truoc khi xoa de luu log
                        doclog = await rbi(id)
                        if (doc.pid && doc.adj.typ == 2) await dbs.query(`update s_inv set cid=null,cde=null where id=$1`, [doc.pid])
                        if (!doc.cancel) await dbs.query(`update s_inv set doc=doc||$1, cdt=$2 where id=$3 and stax=$4 and status=$5`, [JSON.stringify({ cancel: { typ: 3, ref: id, rdt: now } }), new Date(), id, taxc, os])
                        sSysLogs = { fnc_id: 'inv_cancel', src: config.SRC_LOGGING_DEFAULT, dtl: `Hủy hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doclog) };
                    } else {
                        //Lay du lieu doc truoc khi xoa de luu log
                        doclog = await rbi(id)
                        if (ns == 2 || ns == 3) {
                            const r = await dbs.query(`select 1 from s_inv where id=$1 and xml is not null LIMIT 1`, [id])
                            if (r.rowCount > 0) ns = 3
                            else ns = 2
                        }
                        sql = `update s_inv set doc=doc||$1 where id=$2 and stax=$3 and status=$4`
                        binds = [JSON.stringify({ status: ns }), id, taxc, os]
                        sSysLogs = { fnc_id: 'inv_istatus', src: config.SRC_LOGGING_DEFAULT, dtl: `Chuyển đổi trạng thái hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doclog) };
                    }
                    const result = await dbs.query(sql, binds)
                    if (result.rowCount && (oldstatus == 3 || oldstatus == 6 || oldstatus == 2)) {
                        if (useJobSendmail) {
                            if (((ns == 4) && !util.isEmpty(doc.bmail)) || ((ns == 4) && !util.isEmpty(doc.btel))) {
                                doc.status = 4
                                await Service.insertDocTempJob(doc.id, doc, ``)
                            }
                        } else {
                            if ((ns == 4) && !util.isEmpty(doc.bmail)) {
                                doc.status = 4
                                if (!util.isEmpty(doc.bmail)) {
                                    let mailstatus = await email.rsmqmail(doc)
                                    if (mailstatus) await Service.updateSendMailStatus(doc)
                                }
                            }
                            if ((ns == 4) && !util.isEmpty(doc.btel)) {
                                doc.status = 4
                                if (!util.isEmpty(doc.btel)) {
                                    let smsstatus = await sms.smssend(doc)
                                    if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                                }
                            }
                        }
                        rc++
                    }

                    logging.ins(req, sSysLogs, next)
                } catch (e) {
                    logger4app.error(e)
                }
            }
            res.json(rc)
        }
        catch (err) {
            next(err)
        }
    },
    del: async (req, res, next) => {
        try {
            const token = SEC.decode(req), id = req.params.id, pid = req.params.pid
            await chkou(id, token)
            //Lay du lieu doc truoc khi xoa de luu log
            let doclog = await rbi(id)
            const result = await dbs.query(`delete from s_inv where id=$1 and stax=$2 and status=$3`, [id, token.taxc, 1])
            if (result.rowCount > 0 && pid > 0) {
                let doc = await rbi(pid)
                doc.status = 3
                //Xóa doc.cde , nếu không có cũng ko ảnh hưởng.
                delete doc["cde"]
                delete doc["cdt"]
                // await dbs.query(`update s_inv set doc=doc||$1,cid=null,cde=null where id=$2`, [JSON.stringify({ status: 3 }), pid])
                await dbs.query(`update s_inv set doc=$1,cid=null,cde=null where id=$2`, [JSON.stringify(doc), pid])
                //Xóa biên bản
                await dbs.query(`DELETE FROM s_inv_file where id=$1`, [pid])
            }
            const sSysLogs = { fnc_id: 'inv_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doclog) };
            logging.ins(req, sSysLogs, next)
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },
    relate: async (req, res, next) => {
        try {
            const token = SEC.decode(req), id = req.params.id, doc = await rbi(id), sec = doc.sec, root = doc.root, rsec = root ? root.sec : null
            let sql, result, binds = [token.taxc, sec]
            sql = `select id,idt,type,form,serial,seq,sec,status,adjdes,cde from s_inv where stax=$1 and sec<>$2 and (doc->>'root.sec'=$2`
            if (rsec) {
                sql += ` or sec=$3 or doc->>'root.sec'=$3`
                binds.push(rsec)
            }
            sql += ") order by id"
            result = await dbs.query(sql, binds)
            res.json(result.rows)
        }
        catch (err) {
            next(err)
        }
    },
    relateadjs: async (req, res, next) => {
        try {
            const token = SEC.decode(req), id = req.params.id
            let sql, result
            sql = `select id,idt,type,form,serial,seq,sec,status,adjdes,cde from s_inv a where id in (${id}) order by id`

            result = await dbs.query(sql, [id])
            res.json(result.rows)
        }
        catch (err) {
            next(err)
        }
    },
    deleteinv: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const taxc = token.taxc, uid = token.uid, ou = token.ou,
                org = await ous.obt(taxc), AUTO = (org.seq === 1), body = req.body, invs = body.invs
            let count = 0
            let status, message
            try {
                const token = SEC.decode(req)


                for (let inv of invs) {
                    await chkou(inv.id, token)
                    //Lay du lieu doc truoc khi xoa de luu log
                    let doclog = await rbi(inv.id)
                    await dbs.query(`delete from s_inv where id=$1 and status = 1`, [inv.id])
                    await Service.delrefno(doclog)
                    await dbs.query(`update  s_inv set cid=null,cde=null where id=$1 and cid=$2`, [inv.pid, inv.id])
                    count++
                    const sSysLogs = { fnc_id: 'inv_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa hóa đơn id ${inv.id}`, msg_id: inv.id, doc: JSON.stringify(doclog) };
                    logging.ins(req, sSysLogs, next)
                }
                status = 1
                message = 'Success'
            } catch (e) {
                status = 2
                throw e
            }

            res.json({ message: message, status: status, countseq: count })
        }
        catch (err) {
            next(err)
        }
    },
    mailinvs: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, invs = body.invs
            let status, message

            try {
                for (let inv of invs) {
                    let docmail = await rbi(inv.id)
                    let doc = JSON.parse(JSON.stringify(docmail)), xml = ''
                    if (config.ent == 'vcm') {
                        let taxc_root = await ous.pidtaxc(doc.ou)
                        doc.taxc_root = taxc_root
                    }
                    //logger4app.debug(`begin send mail : ${doc.stax} - ${doc.form} - ${doc.serial} - ${doc.seq}`)
                    if (useJobSendmail) {
                        await Service.insertDocTempJob(doc.id, doc, xml)
                    } else {
                        if (!util.isEmpty(doc.bmail)) {
                            let mailstatus = await email.senddirectmail(doc, xml)
                            if (mailstatus) await Service.updateSendMailStatus(doc)
                        }
                        if (!util.isEmpty(doc.btel)) {
                            let smsstatus = await sms.smssend(doc)
                            if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                        }
                    }
                }
                status = 1
                message = 'Success'
            } catch (e) {
                status = 2
                throw e
            }

            res.json({ message: message, status: status })
        }
        catch (err) {
            next(err)
        }
    },
    sendmaildoc: async (req, res, next) => {
        try {
            let doc = req.body, xml = doc.xml
            logger4app.debug(`begin send mail : ${doc.stax} - ${doc.form} - ${doc.serial} - ${doc.seq}`)
            if (config.useJobSendmailAPI) {
                await Service.insertDocTempJob(doc.id, doc, xml)
            } else {
                if (!util.isEmpty(doc.bmail)) {
                    let mailstatus = await email.senddirectmail(doc, xml)
                    if (mailstatus) await Service.updateSendMailStatus(doc)
                }
            }
            logger4app.debug(`end send mail : ${doc.stax} - ${doc.form} - ${doc.serial} - ${doc.seq}`)
            res.json({ result: "1" })
        }
        catch (err) {
            throw err
        }

    },
    signdoc: async (req, res, next) => {
        try {
            let doc = req.body
            const org = await ous.gettosign(doc.stax)
            logger4app.debug(org)
            let signtype = org.sign
            let xml, binds, result
            let sql

            if (signtype == 1) {
                const usr = org.usr, pwd = util.decrypt(org.pwd)
                xml = util.j2x((await Service.getcol(doc)), doc.id)
                const arr = [{ inc: id, xml: Buffer.from(xml).toString("base64") }]
                res.json({ lib: usr, sid: 0, pin: pwd, mst: doc.stax, date: new Date(), ref: config.PATH_XML, arr: arr })
            }
            else {
                if (signtype == 2) {
                    const pwd = util.decrypt(org.pwd)
                    const ca = await sign.ca(doc.stax, pwd)
                    xml = util.signature(sign.sign(ca, util.j2x((await Service.getcol(doc)), doc.id), doc.id), doc.idt)
                }
                else if (signtype == 3) {
                    sql = `select id,doc,bmail,pid from s_inv where id=$1 and stax=$2 and status=2`
                    binds = [doc.id, doc.stax]
                    result = await dbs.query(sql, binds)
                    const rows = result.rows
                    if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n There is no data to sign")
                    const usr = org.usr, pwd = util.decrypt(org.pwd)
                    const kqs = await fcloud.xml(usr, pwd, doc.stax, rows), kq = kqs[0]
                    xml = kq.xml
                }
                else {
                    xml = await hsm.signxml(doc.stax, util.j2x((await Service.getcol(doc)), doc.id))
                    xml = util.signature(xml, doc.idt)
                }
            }
            res.json({ xml: xml })
        }
        catch (err) {
            next(err)
        }

    },
    signxml: async (req, res, next) => {
        try {
            const xml = req.body.xml, stax = req.body.stax, id = req.body.id, idt = req.body.idt
            const org = await ous.gettosign(stax)
            logger4app.debug(org)
            let signtype = org.sign
            let binds, result, xmlresult
            let sql

            if (signtype == 1) {
                const usr = org.usr, pwd = util.decrypt(org.pwd)
                xmlresult = util.j2x((await Service.getcol(doc)), doc.id)
                const arr = [{ inc: id, xml: Buffer.from(xml).toString("base64") }]
                res.json({ lib: usr, sid: 0, pin: pwd, mst: stax, date: new Date(), ref: config.PATH_XML, arr: arr })
            }
            else {
                if (signtype == 2) {
                    const pwd = util.decrypt(org.pwd)
                    const ca = await sign.ca(stax, pwd)
                    xmlresult = util.signature(sign.sign(ca, xml, id), idt)
                }
                else if (signtype == 3) {
                    sql = `select id,doc,bmail,pid from s_inv where id=$1 and stax=$2 and status=2`
                    binds = [id, stax]
                    result = await dbs.query(sql, binds)
                    const rows = result.rows
                    if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n There is no data to sign")
                    const usr = org.usr, pwd = util.decrypt(org.pwd)
                    const kqs = await fcloud.xml(usr, pwd, stax, rows), kq = kqs[0]
                    xmlresult = kq.xml
                }
                else {
                    xmlresult = await hsm.signxml(stax, xml)
                }
            }
            res.json({ xml: xmlresult })
        }
        catch (err) {
            next(err)
        }

    },
    syncRedisDb: async (req, res, next) => {
        try {
            var sqlinv = 'select max(id) id from s_inv'
            logger4app.debug('sqlinv : ', sqlinv)
            var resultinv = await dbs.query(sqlinv, [])
            var maxid = resultinv.recordset[0].id
            logger4app.debug('resultinv.recordset[0] : ', resultinv.recordset[0])
            logger4app.debug('maxid : ', maxid)
            var keyidinv = `INC`, curid = await redis.get(keyidinv)
            if (curid < maxid) {
                await redis.set(keyidinv, maxid)
            }
            res.json({ result: "1" })
        } catch (err) {
            next(err)
        }
    },
    print: async (req, res, next) => {
        try {
            const iwh = await Service.iwh(req, true)
            const result = await dbs.query(`select doc from s_inv ${iwh.where} order by serial,seq`, iwh.binds)
            const rows = result.rows
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn \n No invoice be found`)
            const token = SEC.decode(req), org = await ous.org(token), idx = org.temp, filter = JSON.parse(req.query.filter), fn = util.fn(filter.form, idx), tmp = await util.template(fn)
            // const token = SEC.decode(req), mst = token.mst, org = await ous.org(token), qrc = org.qrc == 1, filter = JSON.parse(req.query.filter)
            // for (const row of rows) {
            //     const doc = row.doc
            //     util.viewadj(doc)
            //if (qrc) row.qrcode = await util.qrcode(row.doc)
            // }
            for (const row of rows) {
                row.doc.tempfn = fn
            }
            res.json({ docs: rows, tmp: tmp })
        } catch (err) {
            next(err)
        }
    },
    printmerge: async (req, res, next) => {
        try {
            const iwh = await Service.iwh(req, true)
            const result = await dbs.query(`select doc from s_inv ${iwh.where} order by serial,seq`, iwh.binds)
            const rows = result.rows
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn \n No invoice be found`)
            const token = SEC.decode(req), org = await ous.org(token), idx = org.temp, filter = JSON.parse(req.query.filter), fn = util.fn(filter.form, idx), tmp = await util.template(fn)
            // const token = SEC.decode(req), mst = token.mst, org = await ous.org(token), qrc = org.qrc == 1, filter = JSON.parse(req.query.filter)
            // for (const row of rows) {
            //     const doc = row.doc
            //     util.viewadj(doc)
            //if (qrc) row.qrcode = await util.qrcode(row.doc)
            // }
            let jsons = []
            for (const row of rows) {
                row.doc.tempfn = fn
                jsons.push({ doc: row.doc })
            }
            let respdf = await util.jsReportRenderAttachPdfs(jsons)
            res.json(respdf)
        } catch (err) {
            next(err)
        }
    },
    delAll: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body
            let sql, result, binds, rows, sqlinv, resultinv, query = { filter: JSON.stringify(body) }, reqtmp = { query: query, token: token }, iwh = await Service.iwh(reqtmp, false, true), countseq = 0
            //Chi cho phép xóa các hóa đơn với trạng thai nhất định
            if (!['1'].includes(body.status)) throw new Error(`Chỉ được xóa các hóa đơn với các trạng thái được phép xóa \n (Only invoices with statuses allowed to be deleted can be deleted)`)
            //Check so ban ghi truoc khi thuc hien xoa
            const sqlcount = `select count(*) total from s_inv ${iwh.where}`
            const resultcount = await dbs.query(sqlcount, iwh.binds)
            if (resultcount.recordset[0].total > config.MAX_ROW_DELETE_ALL) throw new Error(`Số lượng bản ghi cần xóa vượt quá giới hạn cho phép \n (The number of records to be deleted exceeds the allowed limit)`)
            //Lấy các hóa đơn cần xóa
            sqlinv = `select pid id from s_inv ${iwh.where}`
            resultinv = await dbs.query(sqlinv, iwh.binds)
            rows = resultinv.recordset
            for (let inv of rows) {
                await chkou(inv.id, token)
                //Lay du lieu doc truoc khi xoa de luu log
                let doclog = await rbi(inv.id)
                if (config.ent == "scb") {
                    for (const item of doclog.items) {
                        if (item.tranID) {
                            await dbs.query(`update s_trans set status = @1, inv_id = null, inv_date = null where tranID = @2`, [item.statusGD, item.tranID])
                        }
                    }
                }
                await dbs.query(`delete from s_inv where id=@1`, [inv.id])
                await Service.delrefno(doclog)
                await dbs.query(`update  s_inv set cid=null,cde=null where id=@1 and cid=@2`, [inv.pid, inv.id])
                countseq++
                if (config.ent == 'mzh') {
                    await dbs.query(`update  s_trans set status=stattus_temp,inv_id=inv_id_tmp,exrt=null,inv_date=null,ma_nv=null,trancol=null where inv_id=@1`, [inv.id])
                }
                const sSysLogs = { fnc_id: 'inv_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa hóa đơn id ${inv.id}`, msg_id: inv.id, doc: JSON.stringify(doclog) };
                logging.ins(req, sSysLogs, next)
            }

            res.json({ countseq: countseq })
        } catch (err) {
            next(err)
        }
    }
}
module.exports = Service