"use strict"
const moment = require("moment")
const bcrypt = require("bcryptjs")
const jwt = require("jsonwebtoken")
const config = require("../config")
const logger4app = require("../logger4app")
const util = require("../util")
const inc = require("../inc")
const ous = require("./ous")
const rsmq = require("../rsmq")
const sec = require("../sec")
const dbs = require("./dbs")
const user = require("./user")
const inv = require(`./inv`)
const handlebars = require("handlebars")
const ext = require("../ext")
const seasecret = config.seasecret
const seaexpires = config.seaexpires
const rbi = (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            let doc = await inv.invdocbid(id)
            resolve(doc)
        } catch (err) {
            reject(err)
        }
    })
}
const Service = {
    login: async (req, res, next) => {
        try {
            await util.chkCaptcha(req)
            const query = req.query, taxc = query.username
            const result = await dbs.query(`select pwd from s_org where taxc=$1 and pwd is not null`, [taxc])
            const rows = result.rows
            if (rows.length == 0) throw new Error(`Không tìm thấy tài khoản ${taxc} \n Account not found ${taxc}`)
            
            const pwd = rows[0].pwd
            if (!bcrypt.compareSync(query.password, pwd)) throw new Error(`Mật khẩu không đúng \n Incorrect password`)
            const json = { taxc: taxc }
            jwt.sign(json, seasecret, { expiresIn: seaexpires }, (err, token) => {
                if (err) return next(err)
                json.token = token
                json.expires = new Date(Date.now() + seaexpires * 1000)
                return res.json(json)
            })
        } catch (err) {
            next(err)
        }
    },
    change: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, query = req.query
            let result, rows, pwd
            result = await dbs.query(`select pwd from s_org where taxc=$1 and pwd is not null`, [taxc])
            rows = result.rows
            if (rows.length == 0) throw new Error(`Không tìm thấy tài khoản ${taxc} \n Account not found ${taxc}`)
            pwd = rows[0].pwd
            if (!bcrypt.compareSync(query.pwd1, pwd)) throw new Error(`Mật khẩu cũ không đúng \n Incorrect password`)
            pwd = util.bcryptpwd(query.pwd2)
            result = await dbs.query(`update s_org set pwd=$1 where taxc=$2`, [pwd, taxc])
            res.json(result.rowCount)
        } catch (err) {
            next(err)
        }
    },
    reset: async (req, res, next) => {
        try {
            await util.chkCaptcha(req)
            const query = req.query, taxc = query.taxc.trim(), mail = query.mail.trim()
            let result, rows
            result = await dbs.query("select mail from s_org where taxc=$1 and lower(mail) like $2", [taxc, `%${mail.toLowerCase()}%`])
            rows = result.rows
            if (rows.length == 0) throw new Error(`Tài khoản ${taxc} Mail ${mail} không tồn tại \n Account ${taxc} Mail ${mail} does not exist`)
            const pwd = util.pwd()
            await dbs.query("update s_org set pwd=$1 where taxc=$2", [util.bcryptpwd(pwd), taxc])
            const subject = "Mật khẩu đăng nhập hệ thống hóa đơn điện tử"
            let html
            try {
                const source = await util.tempmail(2)
                const template = handlebars.compile(source)
                const obj = { taxc: taxc, pwd: pwd }
                html = template(obj)
            } catch (error) {
                html = `- Tài khoản : ${taxc}<br>- Mật khẩu: ${pwd}`
            }
            //const html = `- Tài khoản : ${taxc}<br>- Mật khẩu: ${pwd}`
            const content = { from: config.mail, to: mail, subject: subject, html: html }
            if (config.dbCache)
                await inc.inserttempmail(content)
            else
                await rsmq.sendMessageAsync({ qname: config.qname, message: JSON.stringify(content) })
            res.send(`Mật khẩu của tài khoản ${taxc} đã được gửi đến ${mail} \n (The account's password ${taxc} have been sent ${mail})`)
        } catch (err) {
            next(err)
        }
    },
    xml0: async (req, res, next) => {
        try {
            const token = sec.decode(req), id = req.params.id
            const result = await dbs.query(`select xml from s_inv where id=$1 and btax=$2 and status in (3,4)`, [id, token.taxc])
            const rows = result.rows
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn ${id} \n No invoice found ${id}`)
            res.json({ xml: rows[0].xml })
        }
        catch (err) {
            next(err)
        }
    },
    search0: async (req, res, next) => {
        try {
            const token = sec.decode(req), id = req.params.id
            const result = await dbs.query(`select pid,cid,cde,adjtyp,adjdes,status,doc,xml from s_inv where id=$1 and btax=$2 and status in (3,4)`, [id, token.taxc])
            const rows = result.rows
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn ${id} \n No invoice found ${id}`)
            const row = rows[0], doc = row.doc, org = await ous.obt(doc.stax), idx = org.temp, fn = util.fn(doc.form, idx), tmp = await util.template(fn)
            let obj ={ status: doc.status, doc: doc, tmp: tmp }
            let reqjsr = ext.createRequest(obj)
            // res.json({ status: status, doc: doc, tmp: tmp })
             const bodyBuffer = await  util.jsReportRenderAttach(reqjsr)
             let sizeInByte = bodyBuffer.byteLength   
            res.json({ pdf: `data:application/pdf;base64, ${bodyBuffer.toString('base64')}`, sizePDF: sizeInByte, adjtyp: row.adjtyp, adjdes: row.adjdes, pid: row.pid, cid: row.cid, cde: row.cde, status: row.status })
        }
        catch (err) {
            next(err)
        }
    },
    xml1: async (req, res, next) => {
        try {
            await util.chkCaptcha(req)
            const query = req.query, sec = query.sec
            const result = await dbs.query(`select xml from s_inv where sec=$1 and status in (3,4)`, [sec])
            const rows = result.rows
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn mã tra cứu: ${sec} \n Could not find invoice for code: ${sec}`)
            res.json({ xml: rows[0].xml })
        }
        catch (err) {
            next(err)
        }
    },
    search1: async (req, res, next) => {
        try {
           
            await util.chkCaptcha(req)
            const query = req.query, sec = query.sec
            const result = await dbs.query(`select id,pid,cid,cde,adjtyp,adjdes,status,doc from s_inv where sec=$1 and status in (3,4,6)`, [sec])
            const rows = result.rows
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn mã tra cứu: ${sec} \n Could not find invoice for code: ${sec}`)
            const row = rows[0], doc = await rbi(row.id)
            const org = await ous.obt(doc.stax)
            const idx = org.temp, fn = util.fn(doc.form, idx), tmp = await util.template(fn)
            let obj ={ status: doc.status, doc: doc, tmp: tmp }
            let reqjsr = ext.createRequest(obj)
            // res.json({ status: status, doc: doc, tmp: tmp })
             const bodyBuffer = await  util.jsReportRenderAttach(reqjsr)
             let sizeInByte = bodyBuffer.byteLength   
           
            res.json({ pdf: `data:application/pdf;base64, ${bodyBuffer.toString('base64')}`, sizePDF: sizeInByte, adjtyp: row.adjtyp, adjdes: row.adjdes, pid: row.pid, cid: row.cid, cde: row.cde, status: row.status })
        }
        catch (err) {
            next(err)
        }
    },
    search2: async (req, res, next) => {
        try {
            const token = sec.decode(req), query = req.query, filter = JSON.parse(query.filter)
            const sort = query.sort, start = query.start ? query.start : 0, count = query.count ? query.count : 10
            let order, val, sql, where, result, binds, ret, i = 4
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = `order by ${key} ${sort[key]}`
                })
            }
            else order = "order by id desc"
            where = "where idt between $1 and $2 and btax=$3 and status in (3,4,6)"
            binds = [new Date(filter.fd), new Date(moment(filter.td).endOf("day")), token.taxc]
            for (const key of Object.keys(filter)) {
                val = filter[key]
                if (val) {
                    switch (key) {
                        case "fd":
                            break
                        case "td":
                            break
                        default:
                            where += ` and ${key} like $${i++}`
                            binds.push(`%${val}%`)
                    }
                }
            }
            sql = `select sec,pid,cid,id,idt,status,type,form,serial,seq,stax,sname,saddr,stel,smail,sacc,sbank,btax,bname,buyer,baddr,btel,bmail,bacc,bbank,note,sumv,vatv,totalv,adjdes,cde from s_inv ${where} ${order} limit ${count} offset ${start}`
            result = await dbs.query(sql, binds)
            ret = { data: result.rows, pos: start }
            if (result.rows.length > 0) {
                if (start == 0) {
                    sql = `select count(*) total from s_inv ${where}`
                    result = await dbs.query(sql, binds)
                    ret.total_count = result.rows[0].total
                }
                res.json(ret)
            }else{
                throw new Error("Không tìm thấy hóa đơn \n (Cannot find Invoice)")
            }
        }
        catch (err) {
            next(err)
        }
    },
    xlssearch: async (req, res, next) => {
        try {
            const token = sec.decode(req), query = req.query, filter = JSON.parse(query.filter)
            let order, val, sql, where, result, binds, i = 4
            order = "order by id"
            where = "where idt between $1 and $2 and btax=$3 and status in (3,4)"
            binds = [new Date(filter.fd), new Date(moment(filter.td).endOf("day")), token.taxc]
            for (const key of Object.keys(filter)) {
                val = filter[key]
                if (val) {
                    switch (key) {
                        case "fd":
                            break
                        case "td":
                            break
                        default:
                            where += ` and ${key} like $${i++}`
                            binds.push(`%${val}%`)
                    }
                }
            }
            sql = `select sec,pid,cid,id,to_char(idt,'DD/MM/YYYY') idt,case when status = 3 then 'Đã duyệt' when status = 4 then 'Đã hủy' end status,type,form ,serial,seq,stax,sname,saddr,stel,smail,sacc,sbank,btax,bname,buyer,baddr,btel,bmail,bacc,bbank,note,sumv,vatv,totalv,adjdes,cde from s_inv ${where} ${order}`
            result = await dbs.query(sql, binds)
            res.json(result.rows)
        }
        catch (err) {
            next(err)
        }
    },
}
module.exports = Service