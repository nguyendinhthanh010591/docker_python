"use strict"
const util = require("../util")
const sec = require("../sec")
const dbs = require("./dbs")
const config = require("../config")
const logger4app = require("../logger4app")
const logging = require("../logging")

const Service = {
  add: async (req, res, next) => {
    
    try {
     
      const body = req.body, name = body.name, des = body.des,status=body.status
      let sql, result,binds=[]
     
      sql = `insert into s_group(name,des,status) values ($1,$2,$3)`
      result = await dbs.query(sql, [name,des,status])
      const sSysLogs = { fnc_id: 'group_insert', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm thông tin nhóm`, msg_id: "",doc: JSON.stringify(body) };
      logging.ins(req, sSysLogs, next)
      res.send(`Nhóm thêm thành công \n (Successfully added group)`)
    }
    catch (err) {
     
      next(err)
    }
    
},
update: async (req, res, next) => {
  
  try {
  
    const body = req.body,id = body.id, name = body.name, des = body.des,status=body.status
    let sql, result,binds=[]
   
    sql = `update s_group set name=$1,des=$2,status=$3 where id=$4`
    result = await dbs.query(sql, [name,des,status,id,])
    const sSysLogs = { fnc_id: 'group_update', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật thông tin nhóm`, msg_id: "", doc: JSON.stringify(body) };
    logging.ins(req, sSysLogs, next)
    res.send(`Cập nhật thành công \n (Successfully updated)`)
  }
  catch (err) {
    
    next(err)
  }
 
},
  member: async (req, res, next) => {
    
    try {
     
      const body = req.body, uid = body.uid, arr = body.arr
      let sql, result
      sql = `delete from s_group_role where group_id=$1`
      result = await dbs.query(sql, [uid])
      sql = `insert into s_group_role(group_id,role_id) values ($1,$2)`
      if (arr.length > 0) {
        let binds = []
        for (const rid of arr) {
         
          result = await dbs.query(sql, [uid, rid])
        }
       
      
      }
      const sSysLogs = { fnc_id: 'group_rolemember', src: config.SRC_LOGGING_DEFAULT, dtl: `Phân quyền theo nhóm`, msg_id: "", doc: JSON.stringify(body) };
      logging.ins(req, sSysLogs, next)
      res.json(result)
    }
    catch (err) {
     
      next(err)
    }
    
  },
  
  role: async (req, res, next) => {
    try {
      const uid = req.params.uid
      const sql = `select x.id "id",x.name "name",x.pid "pid",x.sel "sel" from (select a.id,a.name,a.pid,0 sel from s_role a left join s_group_role b on a.id = b.role_id and b.group_id=$1 where b.role_id is null and a.active = 1 union select a.id,a.name,a.pid,1 sel from s_role a inner join s_group_role b on a.id = b.role_id and b.group_id=$1 and a.active = 1) x order by x.id`
      const result = await dbs.query(sql, [uid])
      const rows = result.rows
      let arr = [], obj
      for (const row of rows) {
        if (!row.pid) {
          row.open = true
          arr.push(row)
        }
        else {
          obj = util.findDFS(arr, row.pid)
          if (obj) {
            if (!obj.hasOwnProperty('data')) obj.data = []
            obj.data.push(row)
          }
        }
      }
      res.json(arr)
      // res.json(result.rows)
    }
    catch (err) {
      next(err)
    }
  },
  roleAll: async (req, res, next) => {
    try {
      const uid = req.params.uid
      const sql = `select id "id",pid "pid",name "name" from s_role where active = 1 order by id`
      const result = await dbs.query(sql, [])
      const rows = result.rows
      let arr = [], obj
      for (const row of rows) {
        if (!row.pid) {
          row.open = true
          arr.push(row)
        }
        else {
          obj = util.findDFS(arr, row.pid)
          if (obj) {
            if (!obj.hasOwnProperty('data')) obj.data = []
            obj.data.push(row)
          }
        }
      }
      res.json(arr)
      // res.json(result.rows)
    }
    catch (err) {
      next(err)
    }
  },
 
  get: async (req, res, next) => {
    try {
      const token = sec.decode(req), query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 10, sort = query.sort
      let where = "where 1=$1 ", order = "", sql, result, ret, binds =[1]
      if (query.filter) {
        let filter = JSON.parse(query.filter), val
        let i = 1
        Object.keys(filter).forEach((key) => {
          val = filter[key]
          if (val) {
            if (key == "name") {
              where += ` and upper(${key}) like $${++i}`
              binds.push(`%${val.toUpperCase()}%`)
            }
            else {
              where += ` and ${key}=$${++i}`
              binds.push(val)
            }
          }
        })
      }
      if (sort) {
        Object.keys(sort).forEach((key) => {
          order = `order by ${key}  ${sort[key]}`
        })
      }
      sql = `select id "id",name "name",status "status",des "des" from s_group ${where} ${order} limit ${count} offset ${start}`
      result = await dbs.query(sql, binds)
      ret = { data: result.rows, pos: start }
      if (start == 0) {
        sql = `select count(*) "total" from s_group ${where}`
        result = await dbs.query(sql, binds)
        ret.total_count = result.rows[0].total
      }
      return res.json(ret)
    }
    catch (err) {
      next(err)
    }
  },
    
  gir: async (req, res, next) => {
    try {
      const token = sec.decode(req), rid = req.params.rid
      
      const sql = `select x.id "id", x.name "name",x.sel "sel",x.des "des",x.status "status" from (select a.id,a.name,a.des,a.status,0 sel from s_group a left join s_group_role b on a.id = b.group_id and b.role_id=$1 where b.group_id is null  union select a.id,a.name,a.des,a.status,1 sel from s_group a inner join s_group_role b on a.id = b.group_id and b.role_id=$2 ) x order by x.id`
      const result = await dbs.query(sql, [rid, rid])
      res.json(result.rows)
    } catch (err) {
      next(err)
    }
  },
  mbr: async (req, res, next) => {
    
    try {
     
      const body = req.body, rid = body.rid, groups = body.groups
      let sql, result
      sql = `delete from s_group_role where role_id=$1`
      result = await dbs.query(sql, [rid])
      if (groups.length > 0) {
        let binds = []
        for (const uid of groups) {
        
          result = await dbs.query(`insert into s_group_role (role_id, group_id) values ($1,$2)`, [rid, uid])
        }
       
      }
      const sSysLogs = { fnc_id: 'group_del_mbr', src: config.SRC_LOGGING_DEFAULT, dtl: 'Phân quyền nhóm', msg_id: "", doc: JSON.stringify(body) };
      logging.ins(req, sSysLogs, next)
      res.json("ok")
    } catch (err) {
     
      next(err)
    }
    
  },
  disable: async (req, res, next) => {
    try {
      const uid = req.params.uid
      await dbs.query(`update s_group set status=$1 where id=$2`, [2, uid])
      const sSysLogs = { fnc_id: 'group_disable', src: config.SRC_LOGGING_DEFAULT, dtl: 'Hủy nhóm NSD', msg_id: uid, doc: JSON.stringify({id: uid, status: 2})};
      logging.ins(req, sSysLogs, next)
      res.send(`Đã hủy bỏ nhóm \n Group is cancelled`)
    } catch (err) {
      next(err)
    }
  },
  enable: async (req, res, next) => {
    try {
      const uid = req.params.uid
      await dbs.query(`update s_group set status=$1 where id=$2`, [1, uid])
      const sSysLogs = { fnc_id: 'group_enable', src: config.SRC_LOGGING_DEFAULT, dtl: 'Kích hoạt nhóm NSD', msg_id: uid, doc: JSON.stringify({id: uid, status: 1})};
      logging.ins(req, sSysLogs, next)
      res.send(`Nhóm đã được kích hoạt \n Group is active`)
    } catch (err) {
      next(err)
    }
  }
  
}
module.exports = Service 