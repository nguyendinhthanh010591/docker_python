"use strict"
const moment = require('moment')
const sec = require("../sec")
const dbs = require("./dbs")
const config = require("../config")
const logger4app = require("../logger4app")
const ENT = config.ent
const serial_usr_grant = config.serial_usr_grant
const Service = {
    get: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, query = req.query, ou= token.ou
            let form_combo = query.form_combo, serial_combo = query.serial_combo
            if(!form_combo) form_combo='*'
            if(!serial_combo) serial_combo='*'            
            const fd = moment(query.fd).format("YYYY-MM-DD 00:00:00"), td = moment(query.td).format("YYYY-MM-DD 23:59:59")
            const tg = query.tg, dvt = query.dvt
            if (!['1','1000','1000000'].includes(dvt))  throw new Error("Tham số ĐVT không hợp lệ \n (Invalid DVT parameter)")
            let tgc
            if (tg == 1) tgc = `to_char(idt,'DD/MM/YYYY')`
            else if (tg == 2) tgc = `to_char(idt,'WW/YY')`
            else tgc = `to_char(idt,'MM/YY')`
            let sql, rs, rows, kq = {}, thsd, invs = 0, totalv = 0, vatv = 0, where_ou=""
            //1-thsd
            let where1 = `where stax=$1 and ($2 ='*' or form=$3) and ($4='*' or serial = $5) and idt BETWEEN $6 and $7`
            let bind1 = [taxc,form_combo,form_combo,serial_combo,serial_combo,fd,td]
            if (serial_usr_grant) {
                where1 = `, (select taxc, type types, form forms, serial serials from s_serial ss, s_seusr ss2 where ss.id = ss2.se and ss2.usrid = $1) ss where stax=$2 and ($3 ='*' or form=$4) and ($5='*' or serial = $6) and stax = ss.taxc and type = ss.types and form = ss.forms and serial = ss.serials`
                bind1 = [token.uid, taxc,form_combo,form_combo,serial_combo,serial_combo]
            }
            sql = `select 
            coalesce(sum(1),0) i0, 
            coalesce(sum(CASE status WHEN 1 THEN 1 ELSE 0 end),0) i1,
            coalesce(sum(CASE status WHEN 2 THEN 1 ELSE 0 end),0) i2,
            coalesce(sum(CASE status WHEN 3 THEN 1 ELSE 0 end),0) i3,
            coalesce(sum(CASE status WHEN 4 THEN 1 ELSE 0 end),0) i4 
            from s_inv ${where1}`
            rs = await dbs.query(sql, bind1)
            thsd = rs.rows[0]
            kq.thsd = thsd
            //2-hd
            let where2 = `where idt BETWEEN $1 and $2 and stax=$3 and ($4 ='*' or form=$5) and ($6='*' or serial = $7)`
            let bind2 = [fd, td, taxc,form_combo,form_combo,serial_combo,serial_combo]
            if (serial_usr_grant) {
                where2 = `, (select taxc, type types, form forms, serial serials from s_serial ss, s_seusr ss2 where ss.id = ss2.se and ss2.usrid = $1) ss where idt BETWEEN $2 and $3 and stax=$4 and ($5 ='*' or form=$6) and ($7='*' or serial = $8) and stax = ss.taxc and type = ss.types and form = ss.forms and serial = ss.serials`
                bind2 = [token.uid, fd, td, taxc,form_combo,form_combo,serial_combo,serial_combo]
            }
            sql = `select ${tgc} tg,
            sum(1) i0,
            sum(CASE status WHEN 1 THEN 1 ELSE 0 end) i1,
            sum(CASE status WHEN 2 THEN 1 ELSE 0 end) i2,
            sum(CASE status WHEN 3 THEN 1 ELSE 0 end) i3,
            sum(CASE status WHEN 4 THEN 1 ELSE 0 end) i4 
            from s_inv ${where2}
            group by ${tgc} order by ${tgc}`
            rs = await dbs.query(sql, bind2)
            rows = rs.rows
            kq.invoice = rows

            //Chinh lai doan lay hoa don
            where2 = `where status in (1,2,3,4) and idt BETWEEN $1 and $2 and stax=$3 and ($4 ='*' or form=$5) and ($6='*' or serial = $7)`
            bind2 = [fd, td, taxc,form_combo,form_combo,serial_combo,serial_combo]
            if (serial_usr_grant) {
                where2 = `, (select taxc, type types, form forms, serial serials from s_serial ss, s_seusr ss2 where ss.id = ss2.se and ss2.usrid = $1) ss where status in (1,2,3,4) and idt BETWEEN $2 and $3 and stax=:4 and ($5 ='*' or form=$6) and ($7='*' or serial = $8) and stax = ss.taxc and type = ss.types and form = ss.forms and serial = ss.serials`
                bind2 = [token.uid, fd, td, taxc,form_combo,form_combo,serial_combo,serial_combo]
            }
            sql = `select count(*) i0 from s_inv ${where2}`
            rs = await dbs.query(sql, bind2)
            rows = rs.rows
            invs = rows[0].i0

            //3-totalv
            sql = `with recursive tree as (select ${ou} id union all select child.id from s_ou child,tree parent where parent.id=child.pid) select id from tree `
            rs = await dbs.query(sql)
            let str = ""
            for (const row of rs.rows) {
                str += `${row.id},`
            }
            where_ou += ` and ou in (${str.slice(0, -1)})`

            let where3 = `where idt BETWEEN $1 and $2 and stax=$3 and status=3 and type in ('01GTKT','02GTTT') and cid is null 
            and ($4 ='*' or form=$5) and ($6='*' or serial = $7)`
            let bind3 = [fd, td, taxc,form_combo,form_combo,serial_combo,serial_combo]
            if (serial_usr_grant) {
                where3 = `, (select taxc, type types, form forms, serial serials from s_serial ss, s_seusr ss2 where ss.id = ss2.se and ss2.usrid = $1) ss where idt BETWEEN $2 and $3 and stax=$4 and status=3 and type in ('01GTKT','02GTTT') and cid is null 
                and ($5 ='*' or form=$6) and ($7='*' or serial = $8) and stax = ss.taxc and type = ss.types and form = ss.forms and serial = ss.serials`
                bind3 = [token.uid, fd, td, taxc,form_combo,form_combo,serial_combo,serial_combo]
            }

            sql = `select ${tgc} tg, 
            sum(coalesce(CASE WHEN adjtyp=2 THEN CAST(doc->>'totalv' as bigint) ELSE totalv END,0))/${dvt} totalv,
            sum(coalesce(CASE WHEN adjtyp=2 THEN CAST(doc->>'vatv' as bigint)   ELSE vatv   END,0))/${dvt} vatv 
            from s_inv ${where3} ${["fhs", "fbc"].includes(ENT) ? where_ou : ``} 
            group by ${tgc} order by ${tgc}`
            rs = await dbs.query(sql, [fd, td, taxc,form_combo,form_combo,serial_combo,serial_combo])
            rows = rs.rows
            kq.revenue = rows
            for (const row of rows) {
                totalv += parseFloat(row.totalv)
                vatv += parseFloat(row.vatv)
            }
            if(["fhs", "fbc"].includes(ENT)){
                vatv = Math.round(parseFloat(vatv))
            }
            kq.total = { invs: invs, totalv: totalv, vatv: vatv }
            //4-serial
            let where4 = `where taxc=$1  and ($2 ='*' or form=$3) and ($4='*' or serial = $5)`
            let bind4 = [taxc,form_combo,form_combo,serial_combo,serial_combo]
            if (serial_usr_grant) {
                where4 = `, s_seusr ss where id = ss.se and ss.usrid = $1 and taxc=$2 and ($3 ='*' or form=$4) and ($5='*' or serial = $6)`
                bind4 = [token.uid, taxc, form_combo,form_combo,serial_combo,serial_combo]
            }
             sql = `select CONCAT(form,'-',serial) serial,max,cur,CONCAT(ROUND(((max - cur)/max) *100,8),'%') rate,(max-cur) kd from s_serial ${where4} and status=1 
             order by form,serial`
            rs = await dbs.query(sql, bind4)
            kq.serial = rs.rows
            res.json(kq)
        } catch (err) {
            next(err)
        }
    }
}
module.exports = Service