"use strict"
const redis = require("../redis")
const dbs = require("./dbs")
const config = require("../config")
const logger4app = require("../logger4app")
const logging = require("../logging")
const Service = {
    get: async (req, res, next) => {
        try {
            const type = req.params.type.toUpperCase()
            const sql = `select id,name,des from s_cat where type=$1`
            const result = await dbs.query(sql, [type])
            res.json(result.rows)
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            let body = req.body, binds, sql, result, operation = body.webix_operation, sSysLogs = { fnc_id: '', src: config.SRC_LOGGING_DEFAULT, dtl: ``}
            switch (operation) {
                case "update":
                    sql = "update s_cat set name=$1,des=$2 where id=$3"
                    binds = [body.name,body.des, body.id]
                    sSysLogs = { fnc_id: 'cat_upd', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật danh mục ${body.type} (${operation})`, msg_id: body.id, doc: JSON.stringify(body) };
                    break
                case "insert":
                    sql = "insert into s_cat(name,type) values ($1,$2) RETURNING id"
                    binds = [ body.name.toString() , body.type.toUpperCase() ]
                    sSysLogs = { fnc_id: 'cat_ins', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm mới danh mục ${body.type} (${operation})`, msg_id: body.id, doc: JSON.stringify(body) };
                    break
                case "delete":
                    sql = "delete from s_cat where id=$1"
                    binds = [body.id]
                    sSysLogs = { fnc_id: 'cat_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa danh mục ${body.type} (${operation})`, msg_id: body.id, doc: JSON.stringify(body) };
                    break
                default:
                    throw new Error(operation + "(là hoạt động không hợp lệ) is invalid operation")
            }
            result = await dbs.query(sql, binds)
            logging.ins(req, sSysLogs, next)
            if (operation == "insert")  res.json(result.rows[0])
            else res.json(result.rowCount)
        }
        catch (err) {
            next(err)
        }
    },
    cache: (type, key) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result, json, sql = `select name id,name as value from s_cat where type=$1 order by name`, binds = []
                switch (type) {
                    case "PROV":
                        sql = `select id id,name as value from s_loc where pid is null order by name`
                        break
                    case "ROLE":
                        sql = `select id id,name as value from s_role where active = 1 order by id`
                        break
                    case "TAXO":
                        sql = `select id id,name as value from s_taxo where id like $1 order by id`
                        binds.push("%00")
                        break
                    default:
                        binds.push(type)
                }
                result = await dbs.query(sql, binds)
                json = JSON.stringify(result.rows)
                await redis.set(key, json)
                resolve(result.rows)
            }
            catch (err) {
                reject(err)
            }
        })
    },
    kache: async (req, res, next) => {
        try {
            let type = req.params.type.toUpperCase(), id = req.params.id, key = `${type}.${id}`, data
            //data = await redis.get(key)
            //if (data) return res.json(JSON.parse(data))
            let sql, result
            switch (type) {
                case "LOCAL":
                    sql = `select id id,name as value from s_loc where pid=$1`
                    result = await dbs.query(sql, [id])
                    break
                case "TAXO":
                    sql = `select id id,name as value from s_taxo where id like $1`
                    result = await dbs.query(sql, [`${id.substr(0, 3)}%`])
                    break
                default:
                    throw new Error(type + "(là khóa không hợp lệ) is invalid key")
            }
            const rows = result.rows
            //await redis.set(key, JSON.stringify(rows))
            res.json(rows)
        }
        catch (err) {
            next(err)
        }
    },
    nokache: async (req, res, next) => {
        try {
            const type = req.params.type.toUpperCase()
            let  result
            switch (type) {
                case "GNS":
                    result = await dbs.query(`select id,name as value,unit unit,price price from s_gns order by name`)
                    break
                case "OU":
                    result = await dbs.query(`select id,name as value from s_ou order by name`)
                    break
                default:
                    throw new Error(type + "(là khóa không hợp lệ) is invalid key")
            }
            res.json(result.rows)
        }
        catch (err) {
            next(err)
        }
    }
}
module.exports = Service   