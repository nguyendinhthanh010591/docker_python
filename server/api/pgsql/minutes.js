
"use strict"
const moment = require("moment")
const sec = require("../sec")
const dbs = require("./dbs")
const util = require("../util")
const config = require("../config")
const logger4app = require("../logger4app")
const user = require("./user")
const ous = require("./ous")
const UPLOAD_MINUTES = config.upload_minute
const mfd = config.mfd
const formatItems = (rows) => {
    for (const row of rows) {
		if (!row.unit) row.unit = ""
        row.price = util.fn2(row.price)
        row.amount = util.fn2(row.amount)
        row.vat = util.fn2(row.vat)
        if (!row.quantity) row.quantity = ""
        if (!row.discountdtl) row.discountdtl = 0
        if (!row.vat) row.vat = ""
        else row.vat = util.fn2(row.vat)
        row.name=String(row.name).replace("<br>","")
    }
    return rows
}
const Service = {
    setsnamefhs: async (paramdoc) => { 
        let doc = paramdoc
        if (["fhs", "fbc"].includes(config.ent)){
            const result_ou = await dbs.query(`WITH RECURSIVE parents AS (
                SELECT id, pid, name, 0 as depth
                FROM s_ou
                WHERE id = $1
            UNION
                SELECT op.id, op.pid, op.name, depth - 1
                FROM s_ou op
                JOIN parents p ON op.id = p.pid
            )
            SELECT *
            FROM parents`, [doc.ou])
            let row_ou= result_ou.rows
            doc.sname=""
            let pre = '<w:p><w:r><w:t>';
            let post = '</w:t></w:r></w:p>';
            //let lineBreak = '<w:br/>';
            let arrsname = []
            for(var j=row_ou.length-1 ; j>=0 ; j--){
                doc.sname+= row_ou[j].name// + " \n " + lineBreak;
                arrsname.push({sname: pre + String(row_ou[j].name).replace("&", "&amp;") + post})
            }
            //doc.sname = pre + doc.sname + post
            doc.sname = arrsname
        }
        return doc.sname
    },
    setsaddrfhs: async (paramdoc, token) => { 
        let uid = token.uid, ou
        if (paramdoc.id) {
            const result = await dbs.query("select uc uc, ou ou from s_inv where id=$1", [paramdoc.id])
            const rows = result.rows
            uid = rows[0].uc
            ou = rows[0].ou
        }
        //Lấy thông tin user của user lập hóa đơn
        let usercreate = await user.obid(uid)
        //Lấy thông tin ou của user lập hóa đơn
        let useroucreate = await ous.obid(ou)
        const saddr = (["fhs", "fbc"].includes(config.ent)) ? useroucreate.addr : paramdoc.saddr
        return saddr
    },
    minutesReplace: async (req, res, next) => {
        try {
            const token = sec.decode(req), id = req.body.id, adj = req.body.inv.adj
            const result = await dbs.query(`select doc from s_inv where id=$1`, [id])
            const doc = result.rows[0].doc
            doc.sname = await Service.setsnamefhs(doc)
            doc.saddr = await Service.setsaddrfhs(doc, token)
            doc.idt = moment(doc.idt).format(mfd)
            doc.items = formatItems(doc.items)
            doc.vatv = util.fn2(doc.vatv)
            doc.sumv = util.fn2(doc.sumv)
            doc.totalv = util.fn2(doc.totalv)
            doc.ref = adj.ref
            doc.rea = adj.rea
            doc.btel= (doc.btel) ? doc.btel : ''
            doc.bmail = (doc.bmail) ? doc.bmail : ''
            doc.btax= (doc.btax) ? doc.btax : ''
            doc.rdt = moment(adj.rdt).format(mfd)
            doc.isvat = doc.type == "01GTKT" ? true : false
            const out = util.docxrender(doc, doc.type.toLowerCase() +  "mrep.docx")
            res.end(out, "binary")
        } catch (err) {
            next(err)
        }
    },
    minutesCancel: async (req, res, next) => {
        try {
            const doc = req.body, adj = doc.adj
            doc.idt = moment(doc.idt).format(mfd)
            doc.items = formatItems(doc.items)
            doc.vatv = util.fn2(doc.vatv)
            doc.sumv = util.fn2(doc.sumv)
            doc.totalv = util.fn2(doc.totalv)
            doc.ref = adj.ref
            doc.rea = adj.rea
            doc.btel= (doc.btel) ? doc.btel : ''
            doc.bmail = (doc.bmail) ? doc.bmail : ''
            doc.btax= (doc.btax) ? doc.btax : ''
            doc.rdt = moment(adj.rdt).format(mfd)
            doc.isvat = doc.type == "01GTKT" ? true : false
            const out = util.docxrender(doc, doc.type.toLowerCase() +  "mcan.docx")
            res.end(out, "binary")
        } catch (err) {
            next(err)
        }
    },
    minutesCreate: async (req, res, next) => {
        try {
            const doc = req.body, adj = doc.adj
            doc.idt = moment(doc.idt).format(mfd)
            doc.ref = adj.ref
            doc.rea = adj.rea
            doc.des = adj.des
			doc.btel= (doc.btel) ? doc.btel : ''
            doc.bmail = (doc.bmail) ? doc.bmail : ''
            doc.btax= (doc.btax) ? doc.btax : ''
            doc.rdt = moment(adj.rdt).format(mfd)
            doc.items0 = formatItems(doc.items0)
            doc.items1 = formatItems(doc.items1)
            doc.isvat = doc.type == "01GTKT" ? true : false
            doc.sname = await Service.setsnamefhs(doc)
            doc.saddr = await Service.setsaddrfhs(doc)
            if (!doc.seq) doc.seq = "............"
            const out = util.docxrender(doc, doc.type.toLowerCase() +  "madj.docx")
            res.end(out, "binary")
        } catch (err) {
            next(err)
        }
    },
    minutesCustom: async (req, res, next) => {
        try {
            const token = sec.decode(req), id = req.params.id, download = req.query.download
            let result = await dbs.query(`select pid,type,status,adjtyp,doc from s_inv where id=$1`, [id])
            let row = result.rows[0], type = row.type, status = row.status, doc = row.doc, fn, pid = row.pid

            doc.sname = await Service.setsnamefhs(doc)
            doc.saddr = await Service.setsaddrfhs(doc, token)
            if (UPLOAD_MINUTES && download) {
                let typ, iid = pid
                if (status == 4) {
                    typ = "can"
                    iid = id
                } else {
                    let adjtyp = row.adjtyp
                    if (adjtyp == 1) {
                        typ = "rep"
                    } else if (adjtyp == 2) {
                        typ = "adj"
                    }
                }
                if (!typ) {
                    iid = id
                    typ = "normal"
                }
                let minutes = await dbs.query(`select id, type, content from s_inv_file where id=$1 and type=$2`, [iid, typ])
                if (!minutes.rows.length) throw new Error("Không tìm thấy biên bản! \n Canot find the Minutes!")
                let file = Buffer.from(minutes.rows[0].content,"base64")
                res.end(file, "binary")
            } else {
                if (status == 4) {
                    let cancel = doc.cancel
                    if (!cancel) throw new Error(`Hóa đơn không có biên bản.\n (Invocies don't have Minutes)`)
                    doc.idt = moment(doc.idt).format(mfd)
                    doc.items = formatItems(doc.items)
                    doc.vatv = util.fn2(doc.vatv)
                    doc.sumv = util.fn2(doc.sumv)
                    doc.totalv = util.fn2(doc.totalv)
                    doc.ref = cancel.ref
                    doc.rea = cancel.rea
					doc.btel= (doc.btel) ? doc.btel : ''
					doc.bmail = (doc.bmail) ? doc.bmail : ''
					doc.btax= (doc.btax) ? doc.btax : ''
                    doc.rdt = moment(cancel.rdt).format(mfd)
                    doc.isvat = type == "01GTKT" ? true : false
                    fn = doc.type.toLowerCase() + "mcan.docx"
                }
                else {
                    let adjtyp = row.adjtyp
                    if (adjtyp && pid) {
                        let adj = doc.adj
                        result = await dbs.query(`select doc from s_inv where id=$1`, [pid])
                        row = result.rows[0]
                        let pdoc = row.doc
                        if (adjtyp == 1) {
                            if (!["fhs", "fbc"].includes(config.ent)) {
                                doc = pdoc
                            }
                            doc.idt = moment(doc.idt).format(mfd)
                            doc.items = formatItems(doc.items)
                            doc.vatv = util.fn2(doc.vatv)
                            doc.sumv = util.fn2(doc.sumv)
                            doc.totalv = util.fn2(doc.totalv)
                            doc.ref = adj.ref
                            doc.rea = adj.rea
							doc.btel= (doc.btel) ? doc.btel : ''
							doc.bmail = (doc.bmail) ? doc.bmail : ''
							doc.btax= (doc.btax) ? doc.btax : ''
                            doc.rdt = moment(adj.rdt).format(mfd)
                            doc.isvat = type == "01GTKT" ? true : false
                            if (["fhs", "fbc"].includes(config.ent)) {
                                doc.roottype = pdoc.type
                                doc.rootidt = moment(pdoc.idt).format(mfd)
                                doc.rootform = pdoc.form
                                doc.rootserial = pdoc.serial
                                doc.rootseq = pdoc.seq
                                doc.rootadjdes = doc.adj.rea
                            }
                            fn = doc.type.toLowerCase() + "mrep.docx"
                        }
                        else if (adjtyp == 2) {
                            let items1 = doc.items , items0 = pdoc.items
                            doc.idt = moment(doc.idt).format(mfd)
                            doc.ref = adj.ref
                            doc.rea = adj.rea
                            doc.des = adj.des
							doc.btel= (doc.btel) ? doc.btel : ''
							doc.bmail = (doc.bmail) ? doc.bmail : ''
							doc.btax= (doc.btax) ? doc.btax : ''
                            doc.rdt = moment(adj.rdt).format(mfd)
                            doc.items0 = formatItems(items0)
                            doc.items1 = formatItems(items1)
                            doc.isvat = type == "01GTKT" ? true : false
                            if (!doc.seq) doc.seq = "............"
                            fn = doc.type.toLowerCase() + "madj.docx"
                        }
                    }
                }
                if (fn) {
                    for (let i in doc) if (util.isEmpty(doc[i])) doc[i] = ""
                    for (let row of doc.items) 
                    {
                        for (let i in row)
                        {
                            if (util.isEmpty(row[i])) row[i] = ""
                        }
                       
                        if (typeof  row.unit=="undefined") 
                        {
                            row.unit="";
                        }
                    }
                    const out = util.docxrender(doc, fn)
                    res.end(out, "binary")
                }
                else {
                    throw new Error(`Hóa đơn không có biên bản. \n (Invocies don't have Minutes)`)
                }
            }
        } catch (err) {
            next(err)
        }
    },
    upload: async (req, res, next) => {
        let ok = false,err
        try {
            const token = sec.decode(req), taxc = token.taxc
            const file = req.file, body = req.body, type = body.type.toString().toLowerCase(), id = body.id
            let content = file.buffer, contentb64 = content.toString('base64')
            let sql, result, binds
            sql = `INSERT INTO s_inv_file (id, type, content) 
            VALUES ($1,$2,$3)
            ON CONFLICT (id,type) DO UPDATE 
              SET content = excluded.content`
            binds = [id, type, contentb64]
            result = await dbs.query(sql, binds)
            if (result.rowCount) ok=true
        } catch (error) {
            err = error.message
        }
        res.json({ok, err})
    }
}
module.exports = Service 