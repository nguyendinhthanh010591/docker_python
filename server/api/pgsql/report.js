"use strict"
const excel = require("exceljs")
const numeral = require("numeral")
const path = require("path")
const fs = require("fs")
const moment = require("moment")
const xlsxtemp = require("xlsx-template")
const dbs = require("./dbs")
const config = require("../config")
const logger4app = require("../logger4app")
const sec = require("../sec")
const util = require("../util")

const cons = require(`../conf/${config.file}/cons`)


const ous = require("./ous")
const fxp = require("fast-xml-parser")
const Parser = fxp.j2xParser
const grant = config.ou_grant
const ENT = config.ent
const tkhaithue = {
    "HSoKhaiThue": {
        "TTinChung": {
            "TTinDVu": {
                "maDVu": "HTKK",
                "tenDVu": "HỖ TRỢ KÊ KHAI THUẾ",
                "pbanDVu": "4.3.7",
                "ttinNhaCCapDVu": "73787C2C7CE307BD38F43577DCACB758"
            },
            "TTinTKhaiThue": {
                "TKhaiThue": {
                    "maTKhai": "53",
                    "tenTKhai": "TỜ KHAI THUẾ GIÁ TRỊ GIA TĂNG (Mẫu số 05/GTGT)",
                    "moTaBMau": "(Ban hành kèm theo Thông tư số 156/2013/TT-BTC ngày 06/11/2013 của Bộ Tài chính)",
                    "pbanTKhaiXML": "2.0.0",
                    "loaiTKhai": "C",
                    "soLan": "0",
                    "KyKKhaiThue": {
                        "kieuKy": "M",
                        "kyKKhai": "",
                        "kyKKhaiTuNgay": "", //Lây dữ liệu
                        "kyKKhaiDenNgay": "" //Lây dữ liệu
                    },
                    "maCQTNoiNop": "70303", //Lây dữ liệu
                    "tenCQTNoiNop": "Chi cục Thuế Thành phố Bảo Lộc", //Lây dữ liệu
                    "ngayLapTKhai": "2020-05-05", //Lây dữ liệu
                    "GiaHan": {

                    },
                    "nguoiKy": "",
                    "ngayKy": "" //Lây dữ liệu
                },
                "NNT": {
                    "mst": "0304132047-033",  //Lây dữ liệu
                    "tenNNT": "CN Công ty CP Phát hành sách TP. HCM - Nhà sách Fahasa Bảo Lộc",  //Lây dữ liệu
                    "dchiNNT": "Lầu 1- Co.op Mart - Tháp nước, Đường Trần Phú, Phường 2, Tp.Bảo Lộc",  //Lây dữ liệu
                    "tenHuyenNNT": "Tp.Bảo Lộc",  //Lây dữ liệu
                    "tenTinhNNT": "Lâm Đồng" //Lây dữ liệu
                }
            }
        },
        "CTieuTKhaiChinh": {
            "ct21": "0", //Lây dữ liệu
            "ct22": "0", //Lây dữ liệu
            "ct23": "1",
            "ct24": "2",
            "ct25": "0", //Lây dữ liệu
            "ct26": "0", //Lây dữ liệu
            "ct27": "0" //Lây dữ liệu
        }
    }
}
const charList = (a, z, d = 1) => (a=a.charCodeAt(),z=z.charCodeAt(),[...Array(Math.floor((z-a)/d)+1)].map((_,i)=>String.fromCharCode(a+i*d)))
const bc26 = {
    "HSoKhaiThue": {
        "TTinChung": {
            "TTinDVu": {
                "maDVu": "ETAX",
                "tenDVu": "ETAX 1.0",
                "pbanDVu": "1.0",
                "ttinNhaCCapDVu": "ETAX_TCT"
            },
            "TTinTKhaiThue": {
                "TKhaiThue": {
                    "maTKhai": "102",
                    "tenTKhai": "Báo cáo tình hình sử dụng hóa đơn",
					"moTaBMau": "(Ban hành kèm theo Thông tư số 39/2014/TT-BTC  ngày 31/3/2014 của Bộ Tài chính)",
                    "pbanTKhaiXML": "2.0.8",
                    "loaiTKhai": "C",
                    "soLan": "0",
                    "KyKKhaiThue": {
                        "kieuKy": "Q",
                        "kyKKhai": "4/2019", //Lấy dữ liệu từ ứng dụng
                        "kyKKhaiTuNgay": "01/10/2019", //Lấy dữ liệu từ ứng dụng
                        "kyKKhaiDenNgay": "31/12/2019", //Lấy dữ liệu từ ứng dụng
						"kyKKhaiTuThang": "",
						"kyKKhaiDenThang": ""
					},
                    "maCQTNoiNop": "50100", //Lấy dữ liệu từ ứng dụng
                    "tenCQTNoiNop": "Cục Thuế TP Đà Nẵng", //Lấy dữ liệu từ ứng dụng
                    "ngayLapTKhai": "13/01/2020", //Lấy dữ liệu từ ứng dụng
                    "GiaHan": {
                        "maLyDoGiaHan": "",
                        "lyDoGiaHan": ""
					},
					"nguoiKy": "",
                    "ngayKy": "",
					"nganhNgheKD": ""//Lấy dữ liệu từ ứng dụng
                },
                "NNT": {
                    "mst": "0100231226-999", //Lấy dữ liệu từ ứng dụng
                    "tenNNT": "T?ng c?c thu? test", //Lấy dữ liệu từ ứng dụng
                    "dchiNNT": "123 L� d�c", //Lấy dữ liệu từ ứng dụng
                    "phuongXa": "",
					"maHuyenNNT": "",
					"tenHuyenNNT": "Quận Hai Bà Trưng", //Lấy dữ liệu từ ứng dụng
                    "maTinhNNT": "",
					"tenTinhNNT": "Hà Nội", //Lấy dữ liệu từ ứng dụng
                    "dthoaiNNT": "", //Lấy dữ liệu từ ứng dụng
                    "faxNNT": "",
                    "emailNNT": "" //Lấy dữ liệu từ ứng dụng
                }
            }
        },
        "CTieuTKhaiChinh": {
            "kyBCaoCuoi": "1",
            "chuyenDiaDiem": "0",
            "ngayDauKyBC": "2019-10-01", //Lấy dữ liệu từ ứng dụng
            "ngayCuoiKyBC": "2019-12-31", //Lấy dữ liệu từ ứng dụng
            "HoaDon": {
                "ChiTiet": {
                    "id": "0", //Lấy dữ liệu từ ứng dụng
                    "maHoaDon": "01GTKT", //Lấy dữ liệu từ ứng dụng
                    "tenHDon": "Hóa đơn giá trị gia tăng", //Lấy dữ liệu từ ứng dụng
                    "kHieuMauHDon": "01GTKT2/267", //Lấy dữ liệu từ ứng dụng
                    "kHieuHDon": "AA/11T", //Lấy dữ liệu từ ứng dụng
                    "soTonMuaTrKy_tongSo": "7", //Lấy dữ liệu từ ứng dụng
                    "soTonDauKy_tuSo": "0000001", //Lấy dữ liệu từ ứng dụng
                    "soTonDauKy_denSo": "0000004", //Lấy dữ liệu từ ứng dụng
                    "muaTrongKy_tuSo": "0000005", //Lấy dữ liệu từ ứng dụng
                    "muaTrongKy_denSo": "0000007", //Lấy dữ liệu từ ứng dụng
                    "tongSoSuDung_tuSo": "0", //Lấy dữ liệu từ ứng dụng
                    "tongSoSuDung_denSo": "0",
					"tongSoSuDung_cong": "0",
					"soDaSDung": "0", //Lấy dữ liệu từ ứng dụng
                    "xoaBo_soLuong": "0", //Lấy dữ liệu từ ứng dụng
                    "xoaBo_so": "0",
					"mat_soLuong": "0", //Lấy dữ liệu từ ứng dụng
                    "mat_so": "0",
					"huy_soLuong": "0", //Lấy dữ liệu từ ứng dụng
                    "huy_so": "0",
					"tonCuoiKy_tuSo": "0000001", //Lấy dữ liệu từ ứng dụng
                    "tonCuoiKy_denSo": "0000007", //Lấy dữ liệu từ ứng dụng
                    "tonCuoiKy_soLuong": "7" //Lấy dữ liệu từ ứng dụng
                }
            },
            "tongCongSoTonDKy": "7", //Lấy dữ liệu từ ứng dụng
            "tongCongSDung": "0", //Lấy dữ liệu từ ứng dụng
            "tongCongSoTonCKy": "7", //Lấy dữ liệu từ ứng dụng
            "nguoiLapBieu": "",
            "nguoiDaiDien": "",
            "ngayBCao": "2020-01-13" //Lấy dữ liệu từ ứng dụng
        }
    }
}
const tenhd = (type) => {
    let result = config.ITYPE.find(item => item.id === type)
    return result.value
}
const splitbc26rowbyclist = (row) => {
    let rowbc26 = JSON.parse(JSON.stringify(row))
    const clistlengthmax = config.CLIST_LENGTH_MAX
    let clist = rowbc26.clist
    if (!clist || (clist && clist.length <= clistlengthmax)) return [row]
    let arrfilnal = []
    let arrclist, tsnext, vmin0 = (Number(rowbc26.min0) != 0) ? Number(rowbc26.min0) : 1, vmin1 = (Number(rowbc26.min1) != 0) ? Number(rowbc26.min1) : 1, vmax0 = (Number(rowbc26.max0) != 0) ? Number(rowbc26.max0) : 1, vmax1 = (Number(rowbc26.max1) != 0) ? Number(rowbc26.max1) : 1
    //Khoi tao bien de set cho tu so tiep theo
    tsnext = (vmin0 <= vmin1) ? vmin0 : vmin1
    do {
        //Chuyen danh sach hoa don xoa bo thanh mang
        arrclist = clist.split(';')
        let strtmp = '', vcancel = 0
        //Lap trong danh sach so xoa bo
        for (let i = 0; i < arrclist.length; i++) {
            //Lap va gan cac so xoa bo thanh 1 chuoi
            if (strtmp.length <= clistlengthmax) {
                //Khoi tao bien luu tinh tam truoc khi ngat chuoi
                let strprecalc = strtmp + arrclist[i] + ';'
                //Neu bien tinh tam van nho hon max can tinh thi noi chuoi tiep
                if (strprecalc.length < clistlengthmax) {
                    strtmp += (arrclist[i] + ';')
                    let varrcancel = arrclist[i].split("-")
                    if (varrcancel.length > 1)
                        vcancel += (varrcancel[1] - varrcancel[0] + 1)
                    else
                        vcancel++
                }
                else {
                    //Neu bien tinh tam hon so max thi khoi tao 1 dong bc26 va dua cac gia tri vao 
                    //Tu so bang den so gan nhat
                    let ts = Number((tsnext) ? tsnext : (arrclist[0].split("-"))[0])
                    let arrds = (arrclist[i - 1] ? arrclist[i - 1] : arrclist[arrclist.length - 1]).split("-")
                    let ds = Number((arrds.length > 1) ? arrds[arrds.length - 1] : arrds[0])
                    //Tinh tu so cho dong tiep theo
                    tsnext = Number(ds) + 1
                    //Khoi tao bien luu du lieu dong bc26 theo so huy bi ngat ra
                    let rowbc26tmp = JSON.parse(JSON.stringify(rowbc26))
                    //So ton dau ky
                    if (Number(rowbc26tmp.min0) <= ts && Number(rowbc26tmp.max0) >= ds) {
                        rowbc26tmp.min0 = ts
                        rowbc26tmp.max0 = ds
                        rowbc26tmp.f0 = ts
                        rowbc26tmp.t0 = ds
                    }
                    //So trong ky
                    if (Number(rowbc26tmp.min1) <= ts && Number(rowbc26tmp.max1) >= ds) {
                        rowbc26tmp.min1 = ts
                        rowbc26tmp.max1 = ds
                        rowbc26tmp.f1 = ts
                        rowbc26tmp.t1 = ds
                    }
                    //So nam giua ca dau ky va trong ky
                    if ((Number(rowbc26tmp.min0) <= ts && Number(rowbc26tmp.max0) >= ts) && (Number(rowbc26tmp.min1) <= ds && Number(rowbc26tmp.max1) >= ds)) {
                        rowbc26tmp.min0 = ts
                        rowbc26tmp.f0 = ts
                        rowbc26tmp.f1 = ts
                        rowbc26tmp.t1 = ds
                    }
                    rowbc26tmp.s0 = ds - ts + 1
                    //So su dung
                    rowbc26tmp.fu = ts
                    rowbc26tmp.tu = ds
                    rowbc26tmp.minu = ts
                    rowbc26tmp.maxu = ds
                    rowbc26tmp.su = ds - ts + 1
                    rowbc26tmp.used = ds - ts + 1
                    //So xoa bo
                    rowbc26tmp.cancel = vcancel
                    rowbc26tmp.clist = strtmp.substr(0, strtmp.length - 1)
                    rowbc26tmp.countc = 0
                    rowbc26tmp.listc = ''
                    rowbc26tmp.used = vcancel > 0 ? (rowbc26tmp.su - vcancel) : rowbc26tmp.su
                    if (rowbc26tmp.used > 0) rowbc26tmp.used = rowbc26tmp.used - rowbc26tmp.countc
                    //So cuoi ky
                    rowbc26tmp.f2 = ''
                    rowbc26tmp.t2 = ''
                    rowbc26tmp.s2 = 0
                    arrfilnal.push(rowbc26tmp)
                    arrclist = arrclist.slice(i)
                    break
                }
            }
        }
        clist = arrclist.join(";")
    } while (clist.length > clistlengthmax)

    arrclist = clist.split(';')
    let vcancel = 0
    for (let i = 0; i < arrclist.length; i++) {

        let varrcancel = arrclist[i].split("-")
        if (varrcancel.length > 1)
            vcancel += (varrcancel[1] - varrcancel[0] + 1)
        else
            vcancel++

    }
    let ts = Number((tsnext) ? tsnext : (arrclist[0].split("-"))[0])
    let arrds = (arrclist[arrclist.length - 1]).split("-")
    let ds = Number((arrds.length > 1) ? arrds[arrds.length - 1] : arrds[0])
    //Khoi tao bien luu du lieu dong bc26 theo so huy bi ngat ra
    let rowbc26tmp = JSON.parse(JSON.stringify(rowbc26))
    //So ton dau ky
    if (Number(rowbc26tmp.min0) <= ts && Number(rowbc26tmp.max0) >= ds) {
        rowbc26tmp.min0 = ts
        //rowbc26tmp.max0 = ds
        rowbc26tmp.f0 = ts
        //rowbc26tmp.t0 = ds
        rowbc26tmp.s0 = rowbc26tmp.t0 - ts + 1
    }
    //So trong ky
    if (Number(rowbc26tmp.min1) <= ts && Number(rowbc26tmp.max1) >= ds) {
        rowbc26tmp.min1 = ts
        //rowbc26tmp.max1 = ds
        rowbc26tmp.f1 = ts
        //rowbc26tmp.t1 = ds
        rowbc26tmp.s0 = rowbc26tmp.t1 - ts + 1
    }
    
    //So su dung
    rowbc26tmp.fu = ts
    //rowbc26tmp.tu = ds
    rowbc26tmp.minu = ts
    //rowbc26tmp.maxu = ds
    rowbc26tmp.su = Number(rowbc26tmp.tu) - ts + 1
    //So xoa bo
    rowbc26tmp.cancel = vcancel
    rowbc26tmp.clist = clist
    rowbc26tmp.used = vcancel > 0 ? (rowbc26tmp.su - vcancel) : rowbc26tmp.su
    if (rowbc26tmp.used > 0) rowbc26tmp.used = rowbc26tmp.used - rowbc26tmp.countc
    arrfilnal.push(rowbc26tmp)
    return arrfilnal
}
const rbi = (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            const sql = `select doc "doc" from s_inv where id= $1`
            const result = await dbs.query(sql, [id])
            const rows = result.rows
            if (rows.length == 0) reject(new Error(`Không tìm thấy Hóa đơn ${id} \n (Invoice not found ${id})`))
            let doc = rows[0].doc

            resolve(doc)
        } catch (err) {
            reject(err)
        }
    })
}
const Service = {
    getou: async (req, res, next) => {
        let query = req.query
        let sql = `with recursive tree as (
            select
                id,
                mst,
                taxc,
                code,
                name,
                0 as level,
                '(' || id || '-0)' idx,
                pid
            from
                s_ou
            where
                id = $1
            union all
            select
                c.id,
                c.mst,
                c.taxc,
                c.code,
                c.name,
                p.level + 1,
                p.idx || '*(' || c.id || '-' || p.level || ')' idx,
                c.pid
            from
                s_ou c,
                tree p
            where
                p.id = c.pid )
            select
                id,
                level as lv,
                pid
            from
                tree
            where
                id = $2 or (pid = $2 and $1 != $2)
                or $2 = -1
            order by
                idx`
        const result = await dbs.query(sql, [query.ou2 == "" ? query.ou : query.ou2, query.ou == "*" ? -1 : query.ou])
        let val = query.ou
        let str = ""
        for (const row of result.rows) {
            str += `${row.id},`
        }
        let where_ft = ""
        where_ft += ` and ou in (${str.slice(0, -1)})`
        let where_ou_bc26 = ` and ss.ou in (${str.slice(0, -1)}))`
        // let where_branch = `where id in (${str.slice(0, -1)})`
        // let sql_s = `select ou from s_user where id=$1`
        // let data = await dbs.query(sql_s, [token.uid])
        let where_branch = `where id = ${query.ou == "*" ? query.ou2 : query.ou}`
        let branches = await dbs.query(`select name from s_ou ${where_branch}`)
        let branchesStr = branches.rows.map(branch => branch.name).join(", ")
        return {where_ou_bc26: where_ou_bc26, where_ft : where_ft,branchesStr:branchesStr, rowsou: result.rows }
    },
    get: async (req, res, next) => {
        try {
            const token = sec.decode(req), query = req.body, taxc = token.taxc
            const chktkgtgt_fhs = Number(query.chktkgtgt)
            const tn = query.tn, type = query.type, report = query.report, ou = (typeof query.ou == "undefined" ? "*" : query.ou)
            const fm = moment(query.fd), tm = moment(query.td), period_id = query.period_id
            const fd = fm.startOf("day").toDate(), td = tm.endOf("day").toDate()
            const fr = fm.format(config.mfd), to = tm.format(config.mfd), rt = moment().format(config.mfd)
            const now = new Date(), ndate = now.getDate(), nmonth = now.getMonth() + 1, nyear = now.getFullYear()
            const bcthtype = query.bcthtype
            let sql, result, where, binds, json, fn, rows,where_ft = "",val, where1, where2, branches, branchesStr

            result = await dbs.query("select fadd from s_ou where id=$1 ", [token.ou])
            //if(query.ou2 =="*"){
            //    query.ou2 = token.ou
                // const getou = await Service.getou(req, res, next);
                // where_ft = getou.where_ft
                // branchesStr= getou.branchesStr
            //}
            /*
            if (query.ou != "*" && (typeof query.ou != "undefined")) {

                val = query.ou
                let str = "", arr = val.split(",__")
                for (const row of arr) {
                    let d = row.split("__")
                    str += `${d[0]},`
                }
                where_ft += ` and ou in (${str.slice(0, -1)})`
                // //LMTruong: hien thi ten chi nhanh trong hoa don
                let where_branch = `where id = ${arr[0]}`
                branches = await dbs.query(`select name from s_ou ${where_branch}`)
                branchesStr = branches.rows.map(branch => branch.name).join(", ")

            } 
            else {
                const getou = await Service.getou(query,token,val,where_ft,branches,branchesStr);
                where_ft = getou.where_ft
                branchesStr= getou.branchesStr
            }
            */
            let sfaddr
            if (result.rows.length > 0) sfaddr = result.rows[0].fadd
            if (report == 1) {
                if (!(["fhs", "fbc"].includes(config.ent))) {
                    fn = "temp/08BKHDVAT.xlsx"
                } else {
                    fn = "temp/BKHDVAT_FHS.xlsx"
                }

                if (!(["fhs", "fbc"].includes(config.ent))) {
                    //Lấy các hóa đơn đã duyệt, thêm trường factor đánh dấu hệ số dương
                    where = `where idt between $1 and $2 and stax=$3 and status=3 and type='01GTKT'`
                } else {
                    //Lấy các hóa đơn đã duyệt, thêm trường factor đánh dấu hệ số dương
                    where = `where idt between $1 and $2 and stax=$3 and status=3 and type='01GTKT' and c0='0'`
                }
                let workbook = new excel.Workbook()
                let worksheet = workbook.addWorksheet("sheet 1")

                numeral.locale("vi")
                const borderStyles = {
                    top: { style: "thin" },
                    left: { style: "thin" },
                    bottom: { style: "thin" },
                    right: { style: "thin" }
                };

                worksheet.columns = [
                    { key: "index", width: 6, style: { alignment: { vertical: 'middle', horizontal: 'center' } } },
                    { key: "form", width: 15 },
                    { key: "serial", width: 20 },
                    { key: "seq", width: 20 },
                    { key: "idt", width: 25 },
                    { key: "bname", width: 40 },
                    { key: "btax", width: 25 },
                    // { key: "sumv", width: 25 },
                    // { key: "vatv", width: 25 },
                    { key: "curr", width: 25 },
                    { key: "exrt", width: 25 },
                    { key: "sum", width: 25 },
                    { key: "vat", width: 25 },
                    { key: "note", width: 60 },
                ];

                worksheet.mergeCells('A1:J1');
                worksheet.getCell('A1').value = 'CỘNG HOÀ XÃ HỘI CHỦ NGHĨA VIỆT NAM'
                worksheet.getCell('A1').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('A1').alignment = { horizontal: 'center' }

                worksheet.mergeCells('A2:J2');
                worksheet.getCell('A2').value = 'ĐỘC LẬP TỰ DO HẠNH PHÚC'
                worksheet.getCell('A2').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('A2').alignment = { horizontal: 'center' }

                worksheet.mergeCells('A6:J6');
                worksheet.getCell('A6').value = 'BẢNG KÊ HOÁ ĐƠN ĐÃ SỬ DỤNG THEO NGƯỜI BÁN'
                worksheet.getCell('A6').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('A6').alignment = { horizontal: 'center' }

                worksheet.mergeCells('A7:J7')
                worksheet.getCell('A7').value = `Từ ngày ${moment(fd).format('DD/MM/YYYY')} đến ngày ${moment(td).format('DD/MM/YYYY')}`
                worksheet.getCell('A7').font = { name: 'Times New Roman', italic: true }
                worksheet.getCell('A7').alignment = { horizontal: 'center' }

                worksheet.mergeCells('I3:J3');
                worksheet.getCell('I3').value = ' Mẫu số: 08/BK-HĐXT'
                worksheet.getCell('I3').border = borderStyles
                worksheet.getCell('I3').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('I3').alignment = { horizontal: 'center' }

                worksheet.mergeCells('I4:J4');
                worksheet.getCell('I4').value = 'Ban hành kèm theo Quyết định số 1209/QĐ-BTC ngày 23/06/2015 của Bộ Tài chính Mẫu biểu báo cáo theo nghị định 119/2018'
                worksheet.getCell('I4').border = borderStyles
                worksheet.getCell('I4').font = { name: 'Times New Roman'}
                worksheet.getCell('I4').alignment = { horizontal: 'center' }

                let sname= token.on 
                worksheet.mergeCells('A8:F8')
                worksheet.getCell('A8').value = `Tên người bán: ${sname}`
                worksheet.getCell('A8').font = { name: 'Times New Roman' }
                worksheet.getCell('A8').alignment = { horizontal: 'left' }

                worksheet.mergeCells('G8:J8')
                worksheet.getCell('G8').value = `Đơn vị báo cáo: ${sname}`
                worksheet.getCell('G8').font = { name: 'Times New Roman' }
                worksheet.getCell('G8').alignment = { horizontal: 'left' }

                let stax= taxc
                worksheet.mergeCells('A9:F9')
                worksheet.getCell('A9').value = `MST người bán: ${stax}`
                worksheet.getCell('A9').font = { name: 'Times New Roman' }
                worksheet.getCell('A9').alignment = { horizontal: 'left' }

                worksheet.mergeCells('A10:A11')
                worksheet.getCell('A10').value = 'STT'
                worksheet.getCell('A10').border = borderStyles
                worksheet.getCell('A10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('A10').alignment = { horizontal: 'center' }

                worksheet.mergeCells('B10:E10')
                worksheet.getCell('B10').value = 'Hóa đơn, chứng từ bán ra'
                worksheet.getCell('B10').border = borderStyles
                worksheet.getCell('B10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('B10').alignment = { horizontal: 'center' }

                worksheet.mergeCells('F10:F11')
                worksheet.getCell('F10').value = 'Tên đơn vị mua'
                worksheet.getCell('F10').border = borderStyles
                worksheet.getCell('F10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('F10').alignment = { horizontal: 'center' }

                worksheet.mergeCells('G10:G11')
                worksheet.getCell('G10').value = 'Mã số thuế người mua'
                worksheet.getCell('G10').border = borderStyles
                worksheet.getCell('G10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('G10').alignment = { horizontal: 'center' }

                worksheet.mergeCells('H10:H11')
                worksheet.getCell('H10').value = 'Loại Tiền'
                worksheet.getCell('H10').border = borderStyles
                worksheet.getCell('H10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('H10').alignment = { horizontal: 'center' }

                worksheet.mergeCells('I10:I11')
                worksheet.getCell('I10').value = 'Tỷ Giá'
                worksheet.getCell('I10').border = borderStyles
                worksheet.getCell('I10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('I10').alignment = { horizontal: 'center' }

                worksheet.mergeCells('J10:J11')
                worksheet.getCell('J10').value = 'Doanh thu chưa có thuế GTGT'
                worksheet.getCell('J10').border = borderStyles
                worksheet.getCell('J10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('J10').alignment = { horizontal: 'center' }

                worksheet.mergeCells('K10:K11')
                worksheet.getCell('K10').value = 'Thuế GTGT'
                worksheet.getCell('K10').border = borderStyles
                worksheet.getCell('K10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('K10').alignment = { horizontal: 'center' }

                worksheet.mergeCells('L10:L11')
                worksheet.getCell('L10').value = 'Ghi chú'
                worksheet.getCell('L10').border = borderStyles
                worksheet.getCell('L10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('L10').alignment = { horizontal: 'center' }

                worksheet.mergeCells('B11:D11')
                worksheet.getCell('B11').value = 'Ký hiệu mẫu hóa đơn, ký hiệu hóa đơn, số hóa đơn'
                worksheet.getCell('B11').border = borderStyles
                worksheet.getCell('B11').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('B11').alignment = { horizontal: 'center' }

                worksheet.getCell('E11').value = 'Ngày, tháng, năm lập hóa đơn'
                worksheet.getCell('E11').border = borderStyles
                worksheet.getCell('E11').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('E11').alignment = { horizontal: 'center' }

                worksheet.getCell('A12').value = '(1)'
                worksheet.getCell('A12').border = borderStyles
                worksheet.getCell('A12').font = { name: 'Times New Roman' }
                worksheet.getCell('A12').alignment = { horizontal: 'center' }

                worksheet.getCell('B12').value = '(2)'
                worksheet.getCell('B12').border = borderStyles
                worksheet.getCell('B12').font = { name: 'Times New Roman' }
                worksheet.getCell('B12').alignment = { horizontal: 'center' }

                worksheet.getCell('C12').value = '(3)'
                worksheet.getCell('C12').border = borderStyles
                worksheet.getCell('C12').font = { name: 'Times New Roman' }
                worksheet.getCell('C12').alignment = { horizontal: 'center' }

                worksheet.getCell('D12').value = '(4)'
                worksheet.getCell('D12').border = borderStyles
                worksheet.getCell('D12').font = { name: 'Times New Roman' }
                worksheet.getCell('D12').alignment = { horizontal: 'center' }

                worksheet.getCell('E12').value = '(5)'
                worksheet.getCell('E12').border = borderStyles
                worksheet.getCell('E12').font = { name: 'Times New Roman' }
                worksheet.getCell('E12').alignment = { horizontal: 'center' }

                worksheet.getCell('F12').value = '(6)'
                worksheet.getCell('F12').border = borderStyles
                worksheet.getCell('F12').font = { name: 'Times New Roman' }
                worksheet.getCell('F12').alignment = { horizontal: 'center' }

                worksheet.getCell('G12').value = '(7)'
                worksheet.getCell('G12').border = borderStyles
                worksheet.getCell('G12').font = { name: 'Times New Roman' }
                worksheet.getCell('G12').alignment = { horizontal: 'center' }

                worksheet.getCell('H12').value = '(8)'
                worksheet.getCell('H12').border = borderStyles
                worksheet.getCell('H12').font = { name: 'Times New Roman' }
                worksheet.getCell('H12').alignment = { horizontal: 'center' }

                worksheet.getCell('I12').value = '(9)'
                worksheet.getCell('I12').border = borderStyles
                worksheet.getCell('I12').font = { name: 'Times New Roman' }
                worksheet.getCell('I12').alignment = { horizontal: 'center' }

                worksheet.getCell('J12').value = '(10)'
                worksheet.getCell('J12').border = borderStyles
                worksheet.getCell('J12').font = { name: 'Times New Roman' }
                worksheet.getCell('J12').alignment = { horizontal: 'center' }

                worksheet.getCell('K12').value = '(11)'
                worksheet.getCell('K12').border = borderStyles
                worksheet.getCell('K12').font = { name: 'Times New Roman' }
                worksheet.getCell('K12').alignment = { horizontal: 'center' }

                worksheet.getCell('L12').value = '(12)'
                worksheet.getCell('L12').border = borderStyles
                worksheet.getCell('L12').font = { name: 'Times New Roman' }
                worksheet.getCell('L12').alignment = { horizontal: 'center' }

                let row_stt = 15
                let dataRange = charList('A', 'L')
                binds = [fd, td, taxc]
               let ij=4
                        
                if ((["fhs", "fbc"].includes(config.ent))) {
                    where += where_ft
                } else {
                    if (!ou.includes("*")) {

                        where += ` and ou=$${ij++}`
                        binds.push(ou)

                    }
                }
                let taxsql = /*`case when adjtyp=2 then doc->'tax' else doc->'tax' end`*/`doc->'tax'`
                
                
                sql = `select id, to_char(idt,'DD/MM/YYYY') idt,form,serial,seq,btax,bname,curr,exrt,adjdes,cde,adjtyp,doc->'dif' dif,doc->'root' root,doc->'adj' adj,${taxsql} taxs,note, 1 as factor, 3 statusbc  from s_inv a ${where} `
                if (config.BKHDVAT_EXRA_OPTION == '2') {
                    where1 = `where ${!(["fhs", "fbc"].includes(config.ent)) ? `a.cdt` : `a.cdt`}  between $1 and $2 and a.stax=$3 and a.status=4 and to_char(a.idt,'YYYY-MM')!= to_char(a.cdt,'YYYY-MM') and a.type='01GTKT' and NOT(a.idt BETWEEN $1 and $2 and a.cdt BETWEEN $1 and $2) and c0='0' `
                    where1 += where_ft
                    sql += ` union all select a.id id, to_char(${!(["fhs", "fbc"].includes(config.ent)) ? `cdt` : `idt`},'DD/MM/YYYY') idt,a.form form,a.serial serial,a.seq seq,a.btax btax,a.bname bname,a.curr curr,a.exrt exrt,a.adjdes adjdes,a.cde cde,a.adjtyp adjtyp,doc->'dif' dif,a.doc->'root' root,doc->'adj' adj,${taxsql} taxs,CASE WHEN a.doc->>'cancel.rea' IS NOT NULL THEN a.doc->>'cancel.rea' ELSE cde END note, -1 factor, 4 statusbc from s_inv a ${where1} `
                    
                    where2 = `where a.idt between $1 and $2 and a.stax=$3 and a.type='01GTKT' and a.status=4 and NOT(a.idt BETWEEN $1 and $2 and a.cdt BETWEEN $1 and $2) and c0='0'  `
                    where2 += where_ft
                    sql += ` UNION ALL select a.id id, to_char(idt,'DD/MM/YYYY') idt,a.form form,a.serial serial,a.seq seq,a.btax btax,a.bname bname,a.curr curr,a.exrt exrt,a.adjdes adjdes,a.cde cde,a.adjtyp adjtyp,doc->'dif' dif,a.doc->'root' root,doc->'adj' adj,${taxsql} taxs,note note, 1 factor, 3 statusbc from s_inv a ${where2} `
                }
                sql = 'Select * from (' + sql + ') t order by idt,form,serial,seq'
                result = await dbs.query(sql, binds)
                rows = result.rows
                //Begin HUNGLQ them doan lay du lieu dong theo danh muc loai thue
                    //Lay ra danh muc loai thue suat truoc
                    let arr = JSON.parse(JSON.stringify(config.CATTAX))
                    //Sap xep theo ty le thue vrt truoc, phai nhan voi 100 khong thi vrt 7 lai lon hon 10
                    for (let itemArr of arr) {
                        itemArr.sortprop = String(itemArr.vrt * 100).padStart(7, "0")
                    }
                    arr = util.sortobj(arr, 'sortprop', 1)
                    let objRep = {}
                    //Lap danh muc loai thue suat de tao key object truoc
                    for (let vVat of arr) {
                        //Dat key la ky tu dau va vrt nhan voi 100 de co so nguyen (de phong truong hop vrt = 5.26, 3.5)
                        let vatKey = `VAT${vVat.vrt * 100}`
                        //Neu dinh thay bang key la orriginal vrt thi bo comment doan duoi nay
                        //let vatKey = `VAT${vVat.original_vrt * 100}`
                        objRep[vatKey] = {
                            totalSumByVrt: 0, //Tong cua du lieu sum theo tung vrt, gan truoc bang 0
                            totalVatByVrt: 0, //Tong cua du lieu vat theo tung vrt, gan truoc bang 0
                            index: 0, //gan so thu tu, gan truoc bang 0
                            key_vrn: vVat.vrnVATRep, // danh dáu key len dong dien dai
                            dataVat: [] //Du lieu bao cao, gan bang mang rong truoc, ti nua push du lieu sau
                        }
                    }
                    //End HUNGLQ them doan lay du lieu dong theo danh muc loai thue
                for (const row of rows) {
                    let idt, form, serial, seq, btax, bname, taxs = (row.taxs != null) ? row.taxs : []
                    if (row.adjtyp == 2) {
                        let hd
                        if (row.adj) {
                            hd = JSON.parse(row.adj)
                        }
                        row.note += `Điều chỉnh tăng/ giảm thông tin cho HĐ ${hd.seq}`
                    }
                    if (row.adjtyp == 1) {
                        let hdtt
                        if (row.adj) {
                            hdtt = JSON.parse(row.adj)
                        }
                        row.note += `Thay thế cho hóa đơn ${hdtt.seq}`
                    }
                    if (row.cde && row.cde.includes('điều chỉnh')) {
                        let result_seq = await dbs.query('select seq from s_inv where pid=$1 and form=$2 and serial=$3', [row.id, row.form, row.serial])
                        let row_seq = result_seq.rows
                        let seq_new
                        if (row_seq.length > 0) seq_new = row_seq[0].seq
                        row.note += `${row.note ? '\n' : ''}Bị điều chỉnh cho HĐ ${row.form}-${row.serial}-${seq_new}`
                    }
                    if (row.cde && row.cde.includes('thay thế')) {
                        let result_seq = await dbs.query('select seq from s_inv where pid=$1 and form=$2 and serial=$3', [row.id, row.form, row.serial])
                        let row_seq = result_seq.rows
                        let seq_new
                        if (row_seq.length > 0) seq_new = row_seq[0].seq
                        row.note += `${row.note ? '\n' : ''}Bị thay thế cho HĐ ${row.form}-${row.serial}-${seq_new}`
                    }
                    //note cu
                    // if (row.adjdes != null) {

                    //     row.note = row.adjdes

                    // }
                    // else {
                    //     if (row.cde != null) {
                    //         row.note = row.cde
                    //     }

                    // }
                    //Dữ liệu của hóa đơn nào lấy của hóa đơn đấy
                    idt = row.idt
                    form = row.form
                    serial = row.serial
                    seq = row.seq
                    btax = row.btax
                    bname = row.bname 
                    //Nếu là điều chỉnh xem thẻ dif
                    if (row.adjtyp == 2) {
                        //Nếu chênh lệch dương thì gán số ấm vì là điều chỉnh giảm
                        if (row.dif && row.dif.total > 0) row.factor = -1 * row.factor
                        else {
                            if (row.adj.adjType && row.adj.adjType == '3') row.factor = -1 * row.factor
                        }
                    }
                    let doc = await rbi(row.id)
                    //Them status bc giành riêng cho so huy
                    if (doc.c1 == "Điều chỉnh giảm") {
                        //if (row.statusbc == 3) {
                            row.factor = row.factor * -1
                        //} else {
                        //    row.factor = row.factor
                        //}
                    }
                    let vbname = {bname : bname}
                        
                        for (const c of config.SPECIAL_CHAR) {
                            vbname = JSON.parse(JSON.stringify(vbname).replace(c, ""))
                            bname = vbname.bname
                        }
                    for (const tax of taxs) {
                        
                        let vrt = Number(tax.vrt), s = util.isNumber(tax.amtv) ? tax.amtv * row.factor : 0, v = util.isNumber(tax.vatv) ? (tax.vatv * row.factor) : 0 
                        if(row.curr=="VND") {
                            s = util.isNumber(tax.amtv) ? Math.abs(tax.amtv) * row.factor : 0
                            v = util.isNumber(tax.vatv) ? (Math.abs(tax.vatv) * row.factor) : 0
                        } else {
                            s = util.isNumber(tax.amt) ? Math.abs(tax.amt) * row.factor : 0
                            v = util.isNumber(tax.vat) ? (Math.abs(tax.vat) * row.factor) : 0
                        }
                        let r = { idt: idt, form: form, serial: serial, seq: seq, btax: btax, bname, sumv: s, vatv: v, note: row.note, curr: row.curr, exrt: row.exrt }
                        //Begin HUNGLQ them doan lay du lieu dong theo danh muc loai thue
                            //Bat dau day du lieu vao objRep, neu doan nay on bo cai switch o duoi di
                            const vatKey = `VAT${vrt * 100}`
                            objRep[vatKey].dataVat.push(r)
                            objRep[vatKey].totalSumByVrt += s
                            objRep[vatKey].totalVatByVrt += v
                        //End HUNGLQ them doan lay du lieu dong theo danh muc loai thue
                        // switch (vrt) {
                        //     case 0:
                        //         s0 = Math.round(s0 * 100) / 100
                        //         s0 += s
                        //         r.index = ++i0
                        //         r.indexfhs = 2
                        //         a0.push(r)
                        //         break
                        //     case 5:
                        //         s5 = Math.round(s5 * 100) / 100
                        //         v5 = Math.round(v5 * 100) / 100
                        //         s5 += s
                        //         v5 += v
                        //         r.index = ++i5
                        //         r.indexfhs = 3
                        //         a5.push(r)
                        //         break
                        //     case 10:
                        //         s10 = Math.round(s10 * 100) / 100
                        //         v10 = Math.round(v10 * 100) / 100
                        //         s10 += s
                        //         v10 += v
                        //         r.index = ++i10
                        //         r.indexfhs = 4
                        //         a10.push(r)
                        //         break
                        //     case -1:
                        //         s1 = Math.round(s1 * 100) / 100
                        //         s1 += s
                        //         r.index = ++i1
                        //         a1.push(r)
                        //         r.indexfhs = 1
                        //         break
                        //     case -2:
                        //         s2 = Math.round(s2 * 100) / 100
                        //         s2 += s
                        //         r.index = ++i2
                        //         a2.push(r)
                        //         break
                        //     default:
                        //         break
                        // }
                    }
                }
                // if (["fhs", "fbc"].includes(config.ent)) {
                //     a0 = a0.length <= 0 ? [{ indexfhs: 2 }] : a0
                //     a5 = a5.length <= 0 ? [{ indexfhs: 3 }] : a5
                //     a10 = a10.length <= 0 ? [{ indexfhs: 4 }] : a10
                //     a1 = a1.length <= 0 ? [{ indexfhs: 1 }] : a1
                // }
                // json = { stax: taxc, sname: token.on, fd: fr, td: to, rt: rt, a0: a0, s0: s0, a5: a5, s5: s5, v5: v5, a10: a10, s10: s10, v10: v10, a1: a1, s1: s1, a2: a2, s2: s2, s: ["fhs", "fbc"].includes(config.ent) ? (s0 + s5 + s10) : (s0 + s5 + s10 + s1 + s2) , v: (v5 + v10) }
                // json = { stax: taxc, sname: branchesStr, fd: fr, td: to, rt: rt, a0: a0, s0: s0, a5: a5, s5: s5, v5: v5, a10: a10, s10: s10, v10: v10, a1: a1, s1: s1, a2: a2, s2: s2, s: ["fhs", "fbc"].includes(config.ent) ? (s0 + s5 + s10) : (s0 + s5 + s10 + s1 + s2) , v: (v5 + v10), ss1: s1 + s0 + s5 + s10 }
                //Begin HUNGLQ them doan lay du lieu dong theo danh muc loai thue
                    //Lay du lieu va day ra bien lu danh sach bao cao 
                    //Khai bao bien day ra bao cao
                    let s=0,v=0
                    for (var key in objRep) {
                        //Neu mang co du lieu thi moi xu
                        if (objRep[key].dataVat.length <= 0) continue
                        //push cai dong dien giai truoc
                        let rowname
                        rowname = worksheet.addRow({ index: objRep[key].key_vrn , form: "", serial: "", seq: "", idt: "",bname: "",btax: "",sum: "",vat: "", note: "", curr: "", exrt: ""})
                        worksheet.mergeCells(row_stt, 1, row_stt, 12)
                        rowname.getCell('A').border = borderStyles
                        rowname.alignment = { horizontal: 'left' }
                        rowname.font = { name: 'Times New Roman', bold: true }
                        row_stt = row_stt + 1
                        //Push tiep du lieu bao cao
                        for (const rep of objRep[key].dataVat) {
                            rep.index = ++objRep[key].index
                            //danh dau de format font chữ checksum
                            // rep.checksum = 0
                            // arrRep.push(rep)
                            let rowdata = worksheet.addRow(rep)
                            rowdata.font = { name: 'Times New Roman' }
                            //set border for range of data
                            dataRange.map(x => {
                                rowdata.getCell(x).border = borderStyles
                                if(x=='J') rowdata.getCell(x).alignment = { wrapText:true }
                            })
                            row_stt = row_stt + 1
                        }
                        // arrRep.push({index:`TỔNG`,sumv:objRep[key].totalSumByVrt,vatv:objRep[key].totalVatByVrt,checksum:1})
                        rowname = worksheet.addRow({ index: `TỔNG` , form: "", serial: "", seq: "", idt: "",bname: "",btax: "",sum:objRep[key].totalSumByVrt,vat: objRep[key].totalVatByVrt, note: ""})
                        worksheet.mergeCells(row_stt, 1, row_stt, 9)
                        worksheet.getCell(`H${row_stt}`).border = borderStyles
                        worksheet.getCell(`I${row_stt}`).border = borderStyles
                        worksheet.getCell(`J${row_stt}`).border = borderStyles
                        rowname.getCell('A').border = borderStyles
                        worksheet.getCell(`A${row_stt}`).alignment = { horizontal: 'left' }
                        // rowname.alignment = { horizontal: 'left' }
                        rowname.font = { name: 'Times New Roman', bold: true }
                        row_stt = row_stt + 1
                        s+=objRep[key].totalSumByVrt
                        v+=objRep[key].totalVatByVrt
                        //Push cai dong tong
                    }
                    //End HUNGLQ them doan lay du lieu dong theo danh muc loai thue 
                    // json = { stax: taxc, sname: token.on, fd: fr, td: to, rt: rt, arrRep: arrRep,s:s,v:v }

                    // return json
                    worksheet.mergeCells(row_stt, 1, row_stt, 9)
                    worksheet.getCell(`A${row_stt}`).value = `${rt}`
                    worksheet.getCell(`A${row_stt}`).border = borderStyles
                    worksheet.getCell(`A${row_stt}`).font = { name: 'Times New Roman' }
                    worksheet.getCell(`A${row_stt}`).alignment = { horizontal: 'left' }

                    worksheet.getCell(`J${row_stt}`).value = `${s}`
                    worksheet.getCell(`J${row_stt}`).border = borderStyles
                    worksheet.getCell(`J${row_stt}`).font = { name: 'Times New Roman', bold: true }
                    worksheet.getCell(`J${row_stt}`).alignment = { horizontal: 'right' }


                    worksheet.getCell(`K${row_stt}`).value = `${v}`
                    worksheet.getCell(`K${row_stt}`).border = borderStyles
                    worksheet.getCell(`K${row_stt}`).font = { name: 'Times New Roman', bold: true }
                    worksheet.getCell(`K${row_stt}`).alignment = { horizontal: 'right' }


                    const buffer = await workbook.xlsx.writeBuffer()
                    res.end(buffer, "binary")
                    return
            }
            else if (report == 2) {
                if (!(["fhs", "fbc"].includes(config.ent))) {
                    fn = "temp/08BKHDXT.xlsx"
                    where = "where idt between $1 and $2 and stax=$3"
                    binds = [fd, td, taxc]
                    let ij = 4
                    if (type !== "*") {
                        where += ` and type=$${ij++}`
                        binds.push(type)
                    }


                    if ((["fhs", "fbc"].includes(config.ent))) {
                        where += where_ft
                    } else {
                        if (!ou.includes("*")) {
    
                            where += ` and ou=$${ij++}`
                            binds.push(ou)
    
                        }
                    }

                    sql = `select to_char(idt,'DD/MM/YYYY') idt,form,serial,seq,btax,bname,buyer,CASE WHEN status=1 THEN 'Chờ cấp số' WHEN status=2 THEN 'Chờ duyệt' WHEN status=3 THEN 'Đã duyệt' WHEN status=4 THEN 'Đã hủy' WHEN status=6 THEN 'Chờ hủy' ELSE CAST(status as varchar(22)) END status,status cstatus,sumv,vatv,CONCAT(note,'-',cde,'-',adjdes) note from s_inv ${where} order by idt,form,serial,seq`
                    result = await dbs.query(sql, binds)
                    rows = result.rows
                    let i = 0
                    for (const row of rows) {
                        row.index = ++i
                        if (row.cstatus == 4) {
                            delete row["sumv"]
                            delete row["vatv"]
                        }
                    }
                    json = { stax: taxc, sname: branchesStr, fd: fr, td: to, rt: rt, tn: tn, table: rows }
                } else {
                    //Bảng kê mua vào làm riêng cho Fahasha
                    fn = "temp/08BKHDXT_FHS.xlsx"
                    //Lấy các hóa đơn đã duyệt, thêm trường factor đánh dấu hệ số dương
                    where = `where idt between $1 and $2 and stax=$3 and status=3 and type='01GTKT' and c0='1'`

                    binds = [fd, td, taxc]
                    let ij = 4

                    if ((["fhs", "fbc"].includes(config.ent))) {
                        where += where_ft
                    } else {
                        if (!ou.includes("*")) {
    
                            where += ` and ou=$${ij++}`
                            binds.push(ou)
    
                        }
                    }
                    let taxsql = /*`case when adjtyp=2 then doc->'tax' else doc->'tax' end`*/`doc->'tax'`


                    sql = `select id,to_char(idt,'DD/MM/YYYY') idt,form,serial,seq,btax,bname,adjdes,cde,adjtyp,doc->'dif' dif,doc->'root' root,doc->'adj' adj,${taxsql} taxs,note, -1 as factor from s_inv a ${where} `
                    if (config.BKHDVAT_EXRA_OPTION == '2') {
                        where1 = `where ${!(["fhs", "fbc"].includes(config.ent)) ? `a.cdt` : `a.cdt`}  between $1 and $2 and a.stax=$3 and a.status=4 and a.type='01GTKT' and to_char(a.idt,'YYYY-MM')!= to_char(a.cdt,'YYYY-MM') and c0='1' `
                        where1 += where_ft
                        sql += ` union all select id,to_char(${!(["fhs", "fbc"].includes(config.ent)) ? `cdt` : `idt`},'DD/MM/YYYY') idt,a.form form,a.serial serial,a.seq seq,a.btax btax,a.bname bname,a.adjdes adjdes,a.cde cde,a.adjtyp adjtyp,doc->'dif' dif,a.doc->'root' root,doc->'adj' adj,${taxsql} taxs,CASE WHEN a.doc->>'cancel.rea' IS NOT NULL THEN a.doc->>'cancel.rea' ELSE cde END note, 1 factor from s_inv a ${where1} `

                        where2 = `where a.idt between $1 and $2 and a.stax=$3 and a.type='01GTKT' and a.status=4 and to_char(a.idt,'YYYY-MM')!= to_char(a.cdt,'YYYY-MM') and c0='1'  `
                        where2 += where_ft
                        sql += ` UNION ALL select id,to_char(idt,'DD/MM/YYYY') idt,a.form form,a.serial serial,a.seq seq,a.btax btax,a.bname bname,a.adjdes adjdes,a.cde cde,a.adjtyp adjtyp,doc->'dif' dif,a.doc->'root' root,doc->'adj' adj,${taxsql} taxs,note note, -1 factor from s_inv a ${where2} `

                    }
                    sql = 'Select * from (' + sql + ') t order by idt,form,serial,seq'
                    result = await dbs.query(sql, binds)
                    rows = result.rows
                    let a1 = [], a2 = [], a3 = []
                    let i1 = 0, i2 = 0, i3 = 0
                    let s1 = 0, s2 = 0, s3 = 0
                    let v1 = 0
                    for (const row of rows) {
                        let idt, form, serial, seq, btax, bname, taxs = (row.taxs != null) ? row.taxs : []
                        if (row.adjdes != null) {

                            row.note = row.adjdes

                        }
                        else{
                            if(row.cde != null){
                                row.note = row.cde
                            }

                        }
                        //Dữ liệu của hóa đơn nào lấy của hóa đơn đấy
                        idt = row.idt
                        form = row.form
                        serial = row.serial
                        seq = row.seq
                        btax = row.btax
                        bname = row.bname 
                        let vbname = {bname : bname}
                        
                        for (const c of config.SPECIAL_CHAR) {
                            vbname = JSON.parse(JSON.stringify(vbname).replace(c, ""))
                            bname = vbname.bname
                        }
                        for (const tax of taxs) {
                            //Nếu là điều chỉnh xem thẻ dif
                            if (row.adjtyp == 2) {
                                //Nếu chênh lệch dương thì gán số ấm vì là điều chỉnh giảm
                                row.factor = -1
                                if (row.dif && row.dif.total > 0) row.factor = 1
                                else {
                                    if (row.adj.adjType && row.adj.adjType == '3') row.factor = 1
                                }

                            }
                            let doc = await rbi(row.id)
                            //Them status bc giành riêng cho so huy
                            if (doc.c1 == "Điều chỉnh giảm") {
                                //if (row.statusbc == 3) {
                                row.factor = row.factor * -1
                                //} else {
                                //    row.factor = row.factor
                                //}
                            }
                            let vrt = Number(tax.vrt), s = tax.amtv * row.factor, v = util.isNumber(tax.vatv) ? (tax.vatv * row.factor) : 0, r = { idt: idt, form: form, serial: serial, seq: seq, btax: btax, bname, sumv: s, vatv: v, note: row.note, rlic: "", c0: row.c0 == '1' ? "Hóa đơn GTGT bán hàng" : "Hóa đơn GTGT xuất hoàn trả" }
                            
                            //Nếu là điều chỉnh xem thẻ dif
                            switch (vrt) {
                                case 0:
                                    s1 = Math.round(s1 * 100) / 100
                                    v1 = Math.round(v1 * 100) / 100
                                    s1 += s
                                    v1 += v
                                    r.index = ++i1
                                    a1.push(r)
                                    break
                                case 5:
                                    s1 = Math.round(s1 * 100) / 100
                                    v1 = Math.round(v1 * 100) / 100
                                    s1 += s
                                    v1 += v
                                    r.index = ++i1
                                    a1.push(r)
                                    break
                                case 10:
                                    s1 = Math.round(s1 * 100) / 100
                                    v1 = Math.round(v1 * 100) / 100
                                    s1 += s
                                    v1 += v
                                    r.index = ++i1
                                    a1.push(r)
                                    break
                                case -1:
                                    s1 = Math.round(s1 * 100) / 100
                                    v1 = Math.round(v1 * 100) / 100
                                    s1 += s
                                    v1 += v
                                    r.index = ++i1
                                    a1.push(r)
                                    break
                                case -2:
                                    s1 = Math.round(s1 * 100) / 100
                                    v1 = Math.round(v1 * 100) / 100
                                    s1 += s
                                    v1 += v
                                    r.index = ++i1
                                    a1.push(r)
                                    break
                                default:
                                    break
                            }
                        }
                    }
                    json = { stax: taxc, sname: branchesStr, fd: fr, td: to, rt: rt, a1: a1, a2: a2, a3: a3, s1: s1, s2: s2, s3: s3, v1: v1, s: s1 + s2, v: v1 }
                }
            }
            else if (report == 3) {
                fn = "temp/05BCHD.xlsx"
                if (["fhs", "fbc"].includes(config.ent))
                    fn = "temp/BCHD_FHS.xlsx"
                let data = []
                let where = "", binds = [taxc, fd, td], type_name, where_iv,binds1= [taxc, fd, fd]
                let ij = 4
                if (type !== "*") {
                    where = ` and type=$${ij++}`
                    binds.push(type)
                    binds1.push(type)
                    type_name = tenhd(type)
                }
                //{trung sua them chi nhanh in bao cao
                where_iv = where

                if ((["fhs", "fbc"].includes(config.ent))) {
                    where_iv += where_ft
                } else {
                    if (!ou.includes("*")) {

                        where_iv += ` and ou=$${ij++}`
                        binds.push(ou)
                        binds1.push(ou)

                    }
                }
                //}
                //HungLQ thêm where theo phân quyền dải TBPH cho FHS
                if (["fhs", "fbc"].includes(config.ent)) {
                    /*
                    //Đoạn này lấy theo từng ou, tạm thời rem lại
                    let cparam = ij++;
                    where += ` and exists (select 1 from s_seou ss where ss.se = s_serial.id and ss.ou = $${cparam})`
                    where_iv += ` and exists (select 1 from s_seou ss, s_serial se where ss.se = se.id and s_inv.form = se.form and s_inv."serial" = se."serial" and ss.ou = $${cparam})`
                    binds.push(query.ou2)
                    binds1.push(query.ou2)
                    */
                    //Đoạn này lấy theo danh sách ou cha và con
                    where += ` and exists (select 1 from s_seou ss where ss.se = s_serial.id ${getou.where_ou_bc26}`
                    where_iv += ` and exists (select 1 from s_seou ss, s_serial se where ss.se = se.id and s_inv.form = se.form and s_inv."serial" = se."serial" ${getou.where_ou_bc26}`
                }

                // sua dap ung nhieu dai so khac nhau chung ky hieu
                sql = `select form,serial,0 min1,0 max1,CAST (min AS INTEGER) min0,CAST (max AS INTEGER) max0,0 maxu0,0 minu,0 maxu, 0 minc,0 maxc,0 cancel,null clist from s_serial where taxc=$1 and fd<$2  and  (status not in (2,3,4) or (td>=$2 and status=2 ) or (td>=$3 and status=4 ))  ${where} order by form,serial,min`
                result = await dbs.query(sql, binds1)
                rows = result.rows
                let rows_serial = [], formc = "", serialc = "", bindsc = [], rows_serialarr = []
                for (const row of rows) {
                    if (formc == row.form && serialc == row.serial) {
                        for (let rowc of rows_serial) {
                            if (rowc.form == row.form && rowc.serial == row.serial) {
                                rowc.max0 = row.max0
                            }

                        }
                    } else {
                        rows_serial.push(row)
                    }
                    formc = row.form
                    serialc = row.serial
                }
                let c_id = moment(new Date()).format("YYYYMMDDHHHHmmssSSS")
                for (let rowc of rows_serial) {

                    rowc.c_id = Number(c_id)
                    rowc = Object.values(rowc)
                    rows_serialarr.push(rowc)
                }
                /*
                 if(rows_serialarr.length>0){
                     await dbs.query("insert into s_report_tmp(form,serial,min1, max1, min0, max0, maxu0,minu, maxu,minc,maxc, cancel,clist,c_id) values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14)", [rows_serialarr])
                 }
                 */
                for (let row_serial of rows_serial) {
                    await dbs.query("insert into s_report_tmp(form,serial,min1, max1, min0, max0, maxu0,minu, maxu,minc,maxc, cancel,clist,c_id) values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14)", Object.values(row_serial))
                }


                sql = `select form,serial,CAST (min AS INTEGER)  min1,CAST (max AS INTEGER) max1,0 min0,0 max0,0 maxu0,0 minu,0 maxu,0 minc,0 maxc,0 cancel,null clist from s_serial where taxc=$1 and fd between $2 and $3 and status !=3 ${where} order by form,serial,min`
                result = await dbs.query(sql, binds)
                rows = result.rows
                rows_serial = [], formc = "", serialc = "", bindsc = [], rows_serialarr = []
                for (const row of rows) {
                    if (formc == row.form && serialc == row.serial) {
                        for (let rowc of rows_serial) {
                            if (rowc.form == row.form && rowc.serial == row.serial) {
                                rowc.max1 = row.max1
                            }

                        }
                    } else {
                        rows_serial.push(row)
                    }
                    formc = row.form
                    serialc = row.serial
                }
                let c_id2 = moment(new Date()).format("YYYYMMDDHHHHmmssSSS")
                for (let rowc of rows_serial) {

                    rowc.c_id = Number(c_id2)
                    rowc = Object.values(rowc)
                    rows_serialarr.push(rowc)
                }
                /*
                 if(rows_serialarr.length > 0){
                     await dbs.query("insert into s_report_tmp(form,serial,min1, max1, min0, max0, maxu0,minu, maxu,minc,maxc,  cancel,clist,c_id) values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14)", [rows_serialarr])
                 }*/
                for (let row_serial of rows_serialarr) {
                    await dbs.query("insert into s_report_tmp(form,serial,min1, max1, min0, max0, maxu0,minu, maxu,minc,maxc, cancel,clist,c_id) values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14)", Object.values(row_serial))
                }

                sql = `select form,serial,0 min1,0 max1,0 min0,0 max0,0 maxu0,0 minu,0 maxu,CAST (cur AS INTEGER)  minc,CAST (max AS INTEGER) maxc,0 cancel,null clist from s_serial where taxc=$1 and fd < $3 and td between $2  and $3  and status =2 ${where} order by form,serial,min`
                result = await dbs.query(sql, binds)
                rows = result.rows
                rows_serial = [], formc = "", serialc = "", bindsc = [], rows_serialarr = []
                for (const row of rows) {
                    if (formc == row.form && serialc == row.serial) {
                        for (let rowc of rows_serial) {
                            if (rowc.form == row.form && rowc.serial == row.serial) {
                                rowc.maxc = row.maxc
                            }

                        }
                    } else {
                        rows_serial.push(row)
                    }
                    formc = row.form
                    serialc = row.serial
                }
                let c_id3 = moment(new Date()).format("YYYYMMDDHHHHmmssSSS")
                for (let rowc of rows_serial) {

                    rowc.c_id = Number(c_id3)
                    rowc = Object.values(rowc)
                    rows_serialarr.push(rowc)
                }
                /*
                 if(rows_serialarr.length > 0){
                     await dbs.query("insert into s_report_tmp(form,serial,min1, max1, min0, max0, maxu0,minu, maxu,minc,maxc, cancel,clist,c_id) values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14)", [rows_serialarr])
                 }*/
                for (let row_serial of rows_serialarr) {
                    await dbs.query("insert into s_report_tmp(form,serial,min1, max1, min0, max0, maxu0,minu, maxu,minc,maxc, cancel,clist,c_id) values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14)", Object.values(row_serial))
                }
                sql = `select a.form form,a.serial serial,sum(a.min1) min1,sum(a.max1) max1,sum(a.min0) min0,sum(a.max0) max0,sum(a.maxu0) maxu0,sum(a.minu) minu,sum(a.maxu) maxu,sum(a.minc) minc,sum(a.maxc) maxc,sum(a.cancel) cancel,max(a.clist) clist from (
                select form,serial,min1,max1,min0,max0,maxu0,minu,maxu,minc,maxc,cancel,null clist from s_report_tmp where c_id= ${Number(c_id)}
                union
                select form,serial,min1,max1,min0,max0,maxu0,minu,maxu,minc,maxc,cancel,null clist from s_report_tmp where c_id= ${Number(c_id2)}
                union
                select form,serial,min1,max1,min0,max0,maxu0,minu,maxu,minc,maxc,cancel,null clist from s_report_tmp where c_id= ${Number(c_id3)}
                union
                select form,serial,0 min1,0 max1,0 min0,0 max0,max(CAST(seq as int)) maxu0,0 minu,0 maxu,0 minc,0 maxc,0 cancel,null clist from s_inv where idt<$2 and stax=$1 and status>2 ${where_iv} group by form,serial
                union
                select form,serial,0 min1,0 max1,0 min0,0 max0,0 maxu0,min(CAST(seq as int)) minu,max(CAST(seq as int)) maxu,0 minc,0 maxc,sum(CASE WHEN status=4 THEN 1 ELSE 0 END) cancel,null clist from s_inv where idt between $2 and $3 and stax=$1 and status>2 ${where_iv} group by form,serial
                union 
                select form,serial,0 min1,0 max1,0 min0,0 max0,0 maxu0,0 minu,0 maxu,0 minc,0 maxc,0 cancel,json_agg(CAST(seq as int))::text clist from s_inv where idt between $2 and $3 and stax=$1 and status=4 ${where_iv} group by form,serial
                ) a group by a.form,a.serial`

                result = await dbs.query(sql, binds)
                rows = result.rows
                await dbs.query(`Delete from s_report_tmp where c_id=${Number(c_id)}`)
                await dbs.query(`Delete from s_report_tmp where c_id=${Number(c_id2)}`)
                await dbs.query(`Delete from s_report_tmp where c_id=${Number(c_id3)}`)
                let i = 0

                for (let row of rows) {
                    row.type_name = type == "*" ? tenhd(row.form.substr(0, 6)) : type_name
                    row.type = row.form.substr(0, 6)
                    const clist = JSON.parse(row.clist)
                    if (util.isEmpty(clist)) {
                        delete row.cancel
                    }
                    else {
                        let arr = clist.sort((a, b) => a - b), len = arr.length, str
                        if (len === 1) {
                            str = (!(["fhs", "fbc"].includes(config.ent))) ? arr[0].toString().padStart(config.SEQ_LEN, "0") : arr[0].toString()
                        }
                        else if (len === 2) {
                            const cs = arr[0] + 1 === arr[1] ? "-" : ";"
                            for (let i = 0; i < len; i++) {
                                arr[i] = (!(["fhs", "fbc"].includes(config.ent))) ? arr[i].toString().padStart(config.SEQ_LEN, "0") : arr[i].toString()
                            }
                            str = arr.join(cs)
                        }
                        else {
                            let del = []
                            for (let i = 1; i < len; i++) {
                                
                                if (Number(arr[i - 1]) + 2 === Number(arr[i + 1])) del.push(i)
                            }
                            for (let i = 0; i < len; i++) {
                                if (del.includes(i)) arr[i] = null
                                else arr[i] = (!(["fhs", "fbc"].includes(config.ent))) ? arr[i].toString().padStart(config.SEQ_LEN, "0") : arr[i].toString()
                            }
                            str = arr.join(";").replace(/\;\;+/g, '-')
                        }
                        row.clist = str
                    }
                    let max0 = parseInt(row.max0), min0 = parseInt(row.min0), max1 = parseInt(row.max1), min1 = parseInt(row.min1), maxu0 = parseInt(row.maxu0), minu = parseInt(row.minu), maxu = parseInt(row.maxu), cancel = parseInt(row.cancel), minc = parseInt(row.minc), maxc = parseInt(row.maxc)
                    //use
                    let su = 0, s0 = 0
                    if (maxu > 0) {
                        su = maxu - minu + 1
                        row.su = su
                        row.fu = (!(["fhs", "fbc"].includes(config.ent))) ? minu.toString().padStart(config.SEQ_LEN, '0') : minu.toString()
                        row.tu = (!(["fhs", "fbc"].includes(config.ent))) ? maxu.toString().padStart(config.SEQ_LEN, '0') : maxu.toString()
                        //row.used = cancel > 0 ? (su - cancel) : su
                    }
                    //tondau
                    if (max1 > 0) {
                        if (max0 > 0) {
                            s0 = max1 - min0 + 1
                            row.s0 = s0

                        } else {
                            s0 = max1 - min1 + 1
                            row.s0 = s0

                        }
                        row.f1 = (!(["fhs", "fbc"].includes(config.ent))) ? min1.toString().padStart(config.SEQ_LEN, '0') : min1.toString()
                        row.t1 = (!(["fhs", "fbc"].includes(config.ent))) ? max1.toString().padStart(config.SEQ_LEN, '0') : max1.toString()
                    }
                    if (max0 > 0) {
                        if (maxu0 > 0) {
                            if (max0 >= maxu0) {
                                if (max1 > 0) {
                                    s0 = max1 - maxu0
                                } else {
                                    s0 = max0 - maxu0
                                }

                                row.s0 = s0
                                row.f0 = (!(["fhs", "fbc"].includes(config.ent))) ? (maxu0 + 1).toString().padStart(config.SEQ_LEN, '0') : (maxu0 + 1).toString()
                                row.t0 = (!(["fhs", "fbc"].includes(config.ent))) ? max0.toString().padStart(config.SEQ_LEN, '0') : max0.toString()
                            } else /*if (maxu0 <= max0)*/ {
                                s0 = max0 - min0 + 1
                                row.s0 = s0
                                row.f0 = (!("fhs".includes(ENT))) ? min0.toString().padStart(config.SEQ_LEN, '0') : min0.toString()
                                row.t0 = (!("fhs".includes(ENT))) ? max0.toString().padStart(config.SEQ_LEN, '0') : max0.toString()
                            }
                            if (max0 == maxu0 && max1 > maxu0) {
                                s0 = max1 - maxu0
                                row.s0 = s0

                            }
                        }
                        else {
                            if (max1 > 0) {
                                s0 = max1 - min0 + 1
                            } else {
                                s0 = max0 - min0 + 1
                            }
                            // s0 = max0 - min0 + 1
                            row.s0 = s0
                            row.f0 = (!(["fhs", "fbc"].includes(config.ent))) ? min0.toString().padStart(config.SEQ_LEN, '0') : min0.toString()
                            row.t0 = (!(["fhs", "fbc"].includes(config.ent))) ? max0.toString().padStart(config.SEQ_LEN, '0') : max0.toString()
                        }
                    }

                    //toncuoi
                    if (maxu > 0) {
                        if (s0 > su) {
                            row.s2 = s0 - su
                            row.f2 = (!(["fhs", "fbc"].includes(config.ent))) ? (maxu + 1).toString().padStart(config.SEQ_LEN, '0') : (maxu + 1).toString()
                            if (max1 > 0) row.t2 = row.t1
                            else if (max0 > 0) row.t2 = row.t0
                        }
                    }
                    else {
                        if (max1 > 0) {
                            if (min0 > 0) {
                                row.s2 = max1 - (maxu0 == 0 ? min0 : maxu0 + 1) + 1
                                row.f2 = (!(["fhs", "fbc"].includes(config.ent))) ? (maxu0 == 0 ? min0 : maxu0 + 1).toString().padStart(config.SEQ_LEN, '0') : (maxu0 == 0 ? min0 : maxu0 + 1).toString()
                                row.t2 = row.t1
                            } else {
                                row.s2 = max1 - min1 + 1
                                row.f2 = (!(["fhs", "fbc"].includes(config.ent))) ? min1.toString().padStart(config.SEQ_LEN, '0') : min1.toString()
                                row.t2 = row.t1
                            }

                        }
                        else if (max0 > 0) {
                            row.s2 = s0
                            row.f2 = row.f0
                            row.t2 = row.t0
                        }
                    }
                    //huy ph
                    if (maxc > 0) {


                        //minc = (minc == 0 ? (maxu0 == 0 ? (min0 > 0 ? min0 - 1 : (maxu > 0 ? (min1 + maxu - 1) : min1 - 1)) : maxu0) : minc) + 1
                        minc = (minc == 0 ? (maxu0 == 0 ? (min0 > 0 ? min0 - 1 : (maxu > 0 ? (min1 + maxu - 1) : min1 - 1)) : maxu0) : minc)
                        if (minc == 0) minc++
                        minc = (minc == 0 ? 1 : minc)
                        row.countc = maxc - (minc) + 1
                        row.listc = `${(!(["fhs", "fbc"].includes(config.ent))) ? minc.toString().padStart(config.SEQ_LEN, '0') : minc.toString()} - ${(!(["fhs", "fbc"].includes(config.ent))) ? maxc.toString().padStart(config.SEQ_LEN, '0') : maxc.toString()}`
                        if (Number(maxc) >= Number(row.t2)) {
                            row.s2 = 0
                            row.f2 = ""
                            row.t2 = ""
                        }

                        //if (row.used > 0) row.used = row.used - row.countc

                    } else {

                    }
                    //Tổng số sử dụng, xóa bỏ, mất, hủy

                    if (maxc > 0) {
                        if (maxc > maxu) {
                            su = maxc - (minu == 0 ? minc : minu)
                            row.su = su + 1
                            row.fu = (minu == 0 ? ((!(["fhs", "fbc"].includes(config.ent))) ? minc.toString().padStart(config.SEQ_LEN, '0') : minc.toString()) : ((!(["fhs", "fbc"].includes(config.ent))) ? minu.toString().padStart(config.SEQ_LEN, '0') : minu.toString()))
                            row.tu = (!(["fhs", "fbc"].includes(config.ent))) ? maxc.toString().padStart(config.SEQ_LEN, '0') : maxc.toString()
                        } else {
                            // su = maxc - (minu==0?minc:minu) 
                            // row.su = su+1
                            //   row.fu =(minu==0? minc.toString().padStart(config.SEQ_LEN, '0'):minu.toString().padStart(config.SEQ_LEN, '0'))
                            // row.tu = maxc.toString().padStart(config.SEQ_LEN, '0')
                        }

                    }
                    else {
                        if (maxu > 0) {
                            row.tu = (!(["fhs", "fbc"].includes(config.ent))) ? maxu.toString().padStart(config.SEQ_LEN, '0') : maxu.toString()
                        }
                    }

                    if (typeof row.countc == "undefined") row.countc = 0
                    if (typeof row.s0 == "undefined") row.s0 = 0
                    if (typeof row.su == "undefined") row.su = 0
                    if (typeof row.used == "undefined") row.used = 0
                    if (typeof row.cancel == "undefined") row.cancel = 0
                    if (typeof row.s2 == "undefined") row.s2 = 0

                    row.used = Number(row.cancel) > 0 ? (Number(row.su) - Number(row.cancel)) : Number(row.su)
                    if (row.used > 0) row.used = row.used - Number(row.countc)

                    if ((row.countc == 0) && (row.s0 == 0) && (row.su == 0) && (row.used == 0) && (row.cancel == 0) && (row.s2 == 0)) {

                    } else {
                        row.index = ++i
                        if (config.SPLIT_ROW_BC26_BY_CANCEL_LIST) {
                            let arrcheckmustsplitbyclist = splitbc26rowbyclist(row)
                            for (let rowaftersplit of arrcheckmustsplitbyclist) data.push(rowaftersplit)
                        }
                        else
                            data.push(row)
                    }
                }
                
                let vtongCongSoTonDKy = 0
                let vtongCongSDung = 0
                let vtongCongSoTonCKy = 0
                for (let i = 0; i<= data.length - 1; i++)
                {
                    let rowct = data[i]
                    vtongCongSoTonDKy += rowct.s0
                    vtongCongSDung += rowct.su
                    vtongCongSoTonCKy += rowct.s2
                }
                json = { stax: taxc, sfaddr: sfaddr,  now, ndate, nmonth, nyear, sname: branchesStr, period: `Kỳ báo cáo : ${query.period}`, fd: fr, td: to, rt: rt, tn: tn, table: data, tongCongSoTonDKy : vtongCongSoTonDKy, tongCongSDung : vtongCongSDung, tongCongSoTonCKy : vtongCongSoTonCKy }

                let repChiTiet = []
                if (!chktkgtgt_fhs) {
                //Gán thông tin tờ khai
                if (period_id.includes("q")) {
                    bc26.HSoKhaiThue.TTinChung.TTinTKhaiThue.TKhaiThue.KyKKhaiThue.kieuKy = "Q"
                    bc26.HSoKhaiThue.TTinChung.TTinTKhaiThue.TKhaiThue.KyKKhaiThue.kyKKhai = `${period_id.toUpperCase().substring(1)}${fr.substring(5)}`
                    bc26.HSoKhaiThue.TTinChung.TTinTKhaiThue.TKhaiThue.maTKhai = "102" 
                } else {
                    bc26.HSoKhaiThue.TTinChung.TTinTKhaiThue.TKhaiThue.KyKKhaiThue.kieuKy = "M"
                    bc26.HSoKhaiThue.TTinChung.TTinTKhaiThue.TKhaiThue.KyKKhaiThue.kyKKhai = `${fr.substring(3)}`
                    bc26.HSoKhaiThue.TTinChung.TTinTKhaiThue.TKhaiThue.maTKhai = "131" 
                }

                bc26.HSoKhaiThue.TTinChung.TTinTKhaiThue.TKhaiThue.KyKKhaiThue.kyKKhaiTuNgay = fr
                bc26.HSoKhaiThue.TTinChung.TTinTKhaiThue.TKhaiThue.KyKKhaiThue.kyKKhaiDenNgay = to
                
                sql = `SELECT id, pid, code, "name", taxc, mst, paxo, taxo, prov, dist, ward, addr, tel, mail, acc, bank, status, sign, seq, usr, pwd, "temp", fadd, c0, c1, c2, c3, c4, c5, c6, c7, c8, c9 FROM s_ou where taxc=$1`
                result = await dbs.query(sql, [token.taxc])
                rows = result.rows

                //Lấy thông tin CQT
                let outkhai = rows[0]
                
                bc26.HSoKhaiThue.TTinChung.TTinTKhaiThue.TKhaiThue.maCQTNoiNop = outkhai.taxo
                sql = `select id id,name as name from s_taxo where id = $1 order by id`
                result = await dbs.query(sql, [outkhai.taxo])
                rows = result.rows
                
                bc26.HSoKhaiThue.TTinChung.TTinTKhaiThue.TKhaiThue.tenCQTNoiNop = rows[0].name
                bc26.HSoKhaiThue.TTinChung.TTinTKhaiThue.TKhaiThue.ngayLapTKhai = (moment(new Date())).format('YYYY-MM-DD')

                //Lấy thông tin NNT
                bc26.HSoKhaiThue.TTinChung.TTinTKhaiThue.NNT.mst = token.taxc
                bc26.HSoKhaiThue.TTinChung.TTinTKhaiThue.NNT.tenNNT = outkhai.name
                bc26.HSoKhaiThue.TTinChung.TTinTKhaiThue.NNT.dchiNNT = outkhai.fadd
                bc26.HSoKhaiThue.TTinChung.TTinTKhaiThue.TKhaiThue.ngayKy = (moment(new Date())).format('YYYY-MM-DD')

                result = await dbs.query(`select id,name from s_loc where id = $1`, [outkhai.dist])
                rows = result.rows

                bc26.HSoKhaiThue.TTinChung.TTinTKhaiThue.NNT.tenHuyenNNT = rows[0].name

                result = await dbs.query(`select id,name from s_loc where id = $1`, [outkhai.prov])
                rows = result.rows

                bc26.HSoKhaiThue.TTinChung.TTinTKhaiThue.NNT.tenTinhNNT = rows[0].name
                
                //Chi tiết tờ khai
                bc26.HSoKhaiThue.CTieuTKhaiChinh.ngayDauKyBC = moment(fd).format("YYYY-MM-DD")
                bc26.HSoKhaiThue.CTieuTKhaiChinh.ngayCuoiKyBC = moment(td).format("YYYY-MM-DD")
                
                let vtongCongSoTonDKy = 0
                let vtongCongSDung = 0
                let vtongCongSoTonCKy = 0
                for (let i = 0; i<= data.length - 1; i++)
                {
                    let rowct = data[i]
                    let rowctbc26 = {}
                    rowctbc26.id = rowct.index
                    rowctbc26.maHoaDon = rowct.type
                    rowctbc26.tenHDon = rowct.type_name
                    rowctbc26.kHieuMauHDon = rowct.form
                    rowctbc26.kHieuHDon = rowct.serial
                    rowctbc26.soTonMuaTrKy_tongSo = rowct.s0 ? rowct.s0 : ""
                    rowctbc26.soTonDauKy_tuSo = rowct.f0 ? rowct.f0 : ""
                    rowctbc26.soTonDauKy_denSo = rowct.t0 ? rowct.t0 : ""
                    rowctbc26.muaTrongKy_tuSo = rowct.f1 ? rowct.f1 : ""
                    rowctbc26.muaTrongKy_denSo = rowct.t1 ? rowct.t1 : ""
                    rowctbc26.tongSoSuDung_tuSo = rowct.fu ? rowct.fu : ""
                    rowctbc26.tongSoSuDung_denSo = rowct.tu ? rowct.tu : ""
                    rowctbc26.tongSoSuDung_cong = rowct.su ? rowct.su : 0
                    rowctbc26.soDaSDung = rowct.used ? rowct.used : 0
                    rowctbc26.xoaBo_soLuong = rowct.cancel ? rowct.cancel : 0
                    rowctbc26.xoaBo_so = rowct.clist ? rowct.clist : ""
                    rowctbc26.mat_soLuong = rowct.numlost ? rowct.numlost : 0
                    rowctbc26.mat_so = rowct.seqlost ? rowct.seqlost : ""
                    rowctbc26.huy_soLuong = rowct.countc ? rowct.countc : 0
                    rowctbc26.huy_so = rowct.listc ? rowct.listc : ""
                    rowctbc26.tonCuoiKy_tuSo = rowct.f2 ? rowct.f2 : ""
                    rowctbc26.tonCuoiKy_denSo = rowct.t2 ? rowct.t2 : ""
                    rowctbc26.tonCuoiKy_soLuong = rowct.s2 ? rowct.s2 : 0
                    repChiTiet.push(rowctbc26)
                    vtongCongSoTonDKy += rowct.s0
                    vtongCongSDung += rowct.su
                    vtongCongSoTonCKy += rowct.s2
                }
                bc26.HSoKhaiThue.CTieuTKhaiChinh.HoaDon.ChiTiet = repChiTiet

                bc26.HSoKhaiThue.CTieuTKhaiChinh.tongCongSoTonDKy = vtongCongSoTonDKy
                bc26.HSoKhaiThue.CTieuTKhaiChinh.tongCongSDung = vtongCongSDung
                bc26.HSoKhaiThue.CTieuTKhaiChinh.tongCongSoTonCKy = vtongCongSoTonCKy

                bc26.HSoKhaiThue.CTieuTKhaiChinh.ngayBCao = (moment(new Date())).format('YYYY-MM-DD')
                }
                
                if (chktkgtgt_fhs) {
                    const file = path.join(__dirname, "..", "..", "..", fn)
                    const xlsx = fs.readFileSync(file)
                    const template = new xlsxtemp(xlsx)
                    template.substitute(1, json)
                    res.end(template.generate(), "binary")
                    return
                }
                else {
                    const parser = new Parser()
                    //logger4app.debug(JSON.stringify(bc26)) 
                    let xml = parser.parse(bc26, {ignoreAttributes : false}).replace(/&/g, "&amp;")
                    for (let rbc26 of repChiTiet) {
                        let bienid = `<ChiTiet id="${rbc26.id}">`
                        xml = xml.replace(`<ChiTiet><id>${rbc26.id}</id>`, bienid)
                    }
                    res.end(`<?xml version="1.0" encoding="UTF-8" standalone="no"?><HSoThueDTu xmlns="http://kekhaithue.gdt.gov.vn/TKhaiThue" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">${xml}</HSoThueDTu>`)
                    return
                }
            }else if (report == 6){//bao cao huy fhs

                fn = "temp/BKHHD.xlsx"

                where = `where a.cdt between $1 and $2 and a.stax=$3  and a.status=4`
                binds = [fd, td, taxc]
                let ij=4
                if (type !== "*") {
                    where += ` and type=$${ij++}`
                    binds.push(type)
                }   
                if ((["fhs", "fbc"].includes(config.ent))) {
                    where += where_ft
                } else {
                    if (!ou.includes("*")) {

                        where += ` and ou=$${ij++}`
                        binds.push(ou)
                        binds1.push(ou)

                    }
                }
                sql = `select to_char(idt,'DD/MM/YYYY') idt,form,serial,seq,btax,bname,adjtyp,doc->'cancel' cancel,case when adjtyp=2 then doc->'tax' else doc->'tax' end taxs,note, 1 as factor,cid,cde from s_inv a ${where} `
                
                sql = 'Select * from (' + sql + ') t order by idt,form,serial,seq'
                result = await dbs.query(sql, binds)
                rows = result.rows

                let subSQL,result2,rows2,snameTax
                subSQL=`select name from s_taxo where id=(select taxo from s_ou where id=${token.ou})`
                result2=await dbs.query(subSQL)
                rows2=result2.rows
                for(const row of rows2){
                    snameTax=row.name
                }
               
                let i = 0
                for (const row of rows) {
                    let idt, form, serial, seq, btax, bname, taxs = row.taxs //taxs =  JSON.parse(row.taxs)
                    if (row.cid) {
                        row.note = row.cde 
                    } else if (row.cancel) {
                        row.ref = row.cancel.ref
                        if (row.adjtyp && [1, 2].includes(row.adjtyp))
                            row.note = row.cancel.rea
                        else
                            row.note = row.cancel.rea

                    }
                    row.index = ++i
                    row.type_defaul='Hóa đơn điện tử không có mã của cơ quan thuế'
                }
                json = { stax: taxc, sname: branchesStr, fd: fr, td: to, rt: rt, tn: tn, table: rows,snameTax:snameTax }
           
           
            }else if (report == 7){//bao cao chi tiet huy fhs

                fn = "temp/BCCTH.xlsx"
            
                where = `where a.cdt between $1 and $2 and a.stax=$3  and a.status=4 `

                binds = [fd, td, taxc]

                let ij = 4
                if (type !== "*") {
                    where += ` and type=$${ij++}`
                    binds.push(type)
                }
                if (["fhs", "fbc"].includes(config.ent)) {
                    where += where_ft
                } else {
                    if (!ou.includes("*")) {
                        where += ` and ou=$${ij++}`
                        binds.push(ou)
                    }
                }
                sql = `select to_char(idt,'DD/MM/YYYY') idt,form,serial,seq,btax,bname, adjdes, sname,buyer,adjtyp,doc->'cancel' cancel,cid,cde,ou from s_inv a ${where} `
                
                sql = 'Select * from (' + sql + ') t order by idt,form,serial,seq'
                result = await dbs.query(sql, binds)
                rows = result.rows
            
                let i = 0
                for (const row of rows) {
                    if (row.ou) {
                        let sql = `select name from s_ou where id = ${row.ou}`
                        let data = await dbs.query(sql)
                        let rows = data.rows
                        row.sname = rows[0].name
                    }
                    if (row.cid) {
                        row.rea = row.cde 
                    } else if (row.cancel) {
                        row.ref = row.cancel.ref
                        if (row.adjtyp && [1, 2].includes(row.adjtyp))
                            row.rea = row.adjdes
                        else
                            row.rea = row.cancel.rea

                    }
                    let idt, form, serial, seq, btax, bname, taxs = row.taxs //taxs =  JSON.parse(row.taxs)
                    row.index = ++i
                }
                json = { stax: taxc,now:moment(now).format('DD/MM/YYYY HH:mm:ss'), sname: branchesStr, fd: fr, td: to, rt: rt, tn: tn, table: rows }
       
            }else if (report == 12){
                where = "where idt between $1 and $2 and stax=$3 and c0='0' "
                binds = [fd, td, taxc]
                let ij = 4
                if (type !== "*") {
                    //where += ` and type=$${ij++}`
                    where_ft += ` and type=$${ij++}`
                    binds.push(type)
                }

                //where += where_ft

                let subsql = `select
                            "type" as "type",
                            adjtyp adjtyp,
                            dif dif,
                            adjtypeapi adjtypeapi,
                            factor factor,
                            vrt vrt,
                            vatv * factor vatv,
                            sumv * factor sumv
                        from
                            (
                            select
                                "type" as "type",
                                (json_array_elements(doc::json->'tax') ->> 'vrt'::text)::numeric(17) as vrt,
                                (json_array_elements(doc::json->'tax') ->> 'vatv'::text)::numeric(17,
                                2) as vatv,
                                (json_array_elements(doc::json->'tax') ->> 'amtv'::text)::numeric(17,
                                2) as sumv,
                                adjtyp,
                                ((((((doc)::json ->> 'dif'::text))::json ->> 'total'::text))::int) dif,
                                ((((((doc)::json ->> 'adj'::text))::json ->> 'adjType'::text))::int) adjtypeapi,
                                case
                                    when adjtyp = 2 then
                                    case
                                        when ((((((doc)::json ->> 'dif'::text))::json ->> 'total'::text))::int) > 0 then -1
                                        when ((((((doc)::json ->> 'dif'::text))::json ->> 'total'::text))::int) < 0 then 1
                                        when ((((((doc)::json ->> 'dif'::text))::json ->> 'total'::text))::int) is null then
                                        case
                                            when ((((((doc)::json ->> 'adj'::text))::json ->> 'adjType'::text))::int) = 3 then -1
                                            else 1 end end
                                            else 1 end factor
                                        from
                                            s_inv inv
                                        where
                                            idt between $1 and $2
                                            and stax = $3
                                            and c0 = '0'
                                            and status = 3 ${where_ft}
                                    union all
                                        select
                                            "type" as "type",
                                            (json_array_elements(doc::json->'tax') ->> 'vrt'::text)::numeric(17) as vrt,
                                            (json_array_elements(doc::json->'tax') ->> 'vatv'::text)::numeric(17,
                                            2) as vatv,
                                            (json_array_elements(doc::json->'tax') ->> 'amtv'::text)::numeric(17,
                                            2) as sumv,
                                            null adjtyp,
                                            null dif,
                                            null adjtypeapi,
                                            1 factor
                                        from
                                            s_inv inv
                                        where
                                            idt between $1 and $2
                                            and stax = $3
                                            and c0 = '0'
                                            and status = 4
                                            and to_char(idt, 'YYYY-MM')!= to_char(cdt, 'YYYY-MM') ${where_ft} 
                                    union all
                                        select
                                            "type" as "type",
                                            (json_array_elements(doc::json->'tax') ->> 'vrt'::text)::numeric(17) as vrt,
                                            (json_array_elements(doc::json->'tax') ->> 'vatv'::text)::numeric(17,
                                            2) as vatv,
                                            (json_array_elements(doc::json->'tax') ->> 'amtv'::text)::numeric(17,
                                            2) as sumv,
                                            null adjtyp,
                                            null dif,
                                            null adjtypeapi,
                                            -1 factor
                                        from
                                            s_inv inv
                                        where
                                            cdt between $1 and $2
                                            and stax = $3
                                            and c0 = '0'
                                            and status = 4
                                            and to_char(idt, 'YYYY-MM')!= to_char(cdt, 'YYYY-MM')  ${where_ft} ) inv`

                sql = `select
                inv."type" as "type",
                inv.vrt as vrt,
                case
                    when inv.vrt = 5 then sum(inv.sumv)
                    else 0 end as sum5,
                    case
                        when inv.vrt = 10 then sum(inv.sumv)
                        else 0 end as sum10
                    from
                        (
                         ${subsql} ) inv
                    group by
                        inv."type",
                        inv.vrt`
                result = await dbs.query(sql, binds)
                rows = result.rows
                let doc = {}
                for (const row of rows) {
                    switch (Number(row.vrt)) {
                        case 0:
                            break
                        case 5:
                            tkhaithue.HSoKhaiThue.CTieuTKhaiChinh.ct21 = parseInt(row.sum5)
                            break
                        case 10:
                            tkhaithue.HSoKhaiThue.CTieuTKhaiChinh.ct22 = parseInt(row.sum10)
                            break
                        case -1:
                            break
                        case -2:
                            break
                        default:
                            break
                    }
                }
                doc.ct21 = numeral(tkhaithue.HSoKhaiThue.CTieuTKhaiChinh.ct21).format(`0,0`)
                doc.ct22 = numeral(tkhaithue.HSoKhaiThue.CTieuTKhaiChinh.ct22).format(`0,0`)
                tkhaithue.HSoKhaiThue.CTieuTKhaiChinh.ct25 = parseInt(tkhaithue.HSoKhaiThue.CTieuTKhaiChinh.ct21 * 0.01)
                doc.ct25 = numeral(parseInt(tkhaithue.HSoKhaiThue.CTieuTKhaiChinh.ct21 * 0.01)).format(`0,0`)
                tkhaithue.HSoKhaiThue.CTieuTKhaiChinh.ct26 = parseInt(tkhaithue.HSoKhaiThue.CTieuTKhaiChinh.ct22 * 0.02)
                doc.ct26 = numeral(parseInt(tkhaithue.HSoKhaiThue.CTieuTKhaiChinh.ct22 * 0.02)).format(`0,0`)
                tkhaithue.HSoKhaiThue.CTieuTKhaiChinh.ct27 = tkhaithue.HSoKhaiThue.CTieuTKhaiChinh.ct25 + tkhaithue.HSoKhaiThue.CTieuTKhaiChinh.ct26
                doc.ct27 = numeral(tkhaithue.HSoKhaiThue.CTieuTKhaiChinh.ct25 + tkhaithue.HSoKhaiThue.CTieuTKhaiChinh.ct26).format(`0,0`)
                //Gán thông tin tờ khai
                if (period_id.includes("q")) {
                    tkhaithue.HSoKhaiThue.TTinChung.TTinTKhaiThue.TKhaiThue.KyKKhaiThue.kieuKy = "Q"
                    tkhaithue.HSoKhaiThue.TTinChung.TTinTKhaiThue.TKhaiThue.KyKKhaiThue.kyKKhai = `${period_id.toUpperCase()}${fr.substring(5)}`
                    doc.KyKKhaiThue = `Quý ${period_id.toUpperCase()}${fr.substring(5)}`
                } else {
                    tkhaithue.HSoKhaiThue.TTinChung.TTinTKhaiThue.TKhaiThue.KyKKhaiThue.kieuKy = "M"
                    tkhaithue.HSoKhaiThue.TTinChung.TTinTKhaiThue.TKhaiThue.KyKKhaiThue.kyKKhai = `${fr.substring(3)}`
                    doc.KyKKhaiThue = `Tháng ${fr.substring(3)}`
                }

                tkhaithue.HSoKhaiThue.TTinChung.TTinTKhaiThue.TKhaiThue.KyKKhaiThue.kyKKhaiTuNgay = fr
                tkhaithue.HSoKhaiThue.TTinChung.TTinTKhaiThue.TKhaiThue.KyKKhaiThue.kyKKhaiDenNgay = to
                doc.kyKKhaiTuNgay = fr
                doc.kyKKhaiDenNgay = to
                
                sql = `SELECT taxc, mst FROM s_ou where id=$1`
                result = await dbs.query(sql, [ou])
                rows = result.rows
                let taxctk = (rows && rows.length > 0 && rows[0].taxc) ? rows[0].taxc : rows[0].mst
                
                sql = `SELECT id, pid, code, "name", taxc, mst, paxo, taxo, prov, dist, ward, addr, tel, mail, acc, bank, status, sign, seq, usr, pwd, "temp", fadd, c0, c1, c2, c3, c4, c5, c6, c7, c8, c9 FROM s_ou where taxc=$1`
                result = await dbs.query(sql, [taxctk])
                rows = result.rows

                //Lấy thông tin CQT
                let outkhai = rows[0]
                doc.mail = outkhai.mail
                doc.tel = outkhai.tel
                
                tkhaithue.HSoKhaiThue.TTinChung.TTinTKhaiThue.TKhaiThue.maCQTNoiNop = outkhai.taxo
                doc.maCQTNoiNop = outkhai.taxo
                sql = `select id id,name as name from s_taxo where id = $1 order by id`
                result = await dbs.query(sql, [outkhai.taxo])
                rows = result.rows
                
                tkhaithue.HSoKhaiThue.TTinChung.TTinTKhaiThue.TKhaiThue.tenCQTNoiNop = rows[0].name
                tkhaithue.HSoKhaiThue.TTinChung.TTinTKhaiThue.TKhaiThue.ngayLapTKhai = (moment(new Date())).format('YYYY-MM-DD')
                tkhaithue.HSoKhaiThue.TTinChung.TTinTKhaiThue.TKhaiThue.ngayKy = (moment(new Date())).format('YYYY-MM-DD')

                doc.tenCQTNoiNop = rows[0].name
                let datengayLapTKhai = (moment(new Date()))
                //doc.ngayLapTKhai = `Ngày ${datengayLapTKhai.format(`DD`)} tháng ${datengayLapTKhai.format(`MM`)} năm ${datengayLapTKhai.format(`YYYY`)}`
                
                //Lấy thông tin NNT
                tkhaithue.HSoKhaiThue.TTinChung.TTinTKhaiThue.NNT.mst = taxctk
                tkhaithue.HSoKhaiThue.TTinChung.TTinTKhaiThue.NNT.tenNNT = outkhai.name
                tkhaithue.HSoKhaiThue.TTinChung.TTinTKhaiThue.NNT.dchiNNT = outkhai.addr

                doc.mst = taxctk
                doc.tenNNT = outkhai.name
                doc.dchiNNT = outkhai.fadd

                result = await dbs.query(`select id,name from s_loc where id = $1`, [outkhai.dist])
                rows = result.rows

                tkhaithue.HSoKhaiThue.TTinChung.TTinTKhaiThue.NNT.tenHuyenNNT = (rows && rows.length > 0 && rows[0].name) ? rows[0].name : ""
                doc.tenHuyenNNT = (rows && rows.length > 0 && rows[0].name) ? rows[0].name : ""

                result = await dbs.query(`select id,name from s_loc where id = $1`, [outkhai.prov])
                rows = result.rows

                tkhaithue.HSoKhaiThue.TTinChung.TTinTKhaiThue.NNT.tenTinhNNT = (rows && rows.length > 0 && rows[0].name) ? rows[0].name : ""
                doc.tenTinhNNT = (rows && rows.length > 0 && rows[0].name) ? rows[0].name : ""
                
                const parser = new Parser()
                if (chktkgtgt_fhs) {
                    fn = "tkgtgt_fhs.docx"
                    const out = util.docxrender(doc, fn)
                    
                    res.end(out, "binary")
                    
                }
                else {
                    const xml = parser.parse(tkhaithue, {ignoreAttributes : false}).replace(/&/g, "&amp;")
                    
                    res.end(`<?xml version="1.0" encoding="UTF-8" standalone="no"?><HSoThueDTu xmlns="http://kekhaithue.gdt.gov.vn/TKhaiThue" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">${xml}</HSoThueDTu>`)
                }
                return
            } else if (report == 13) {//Bao cao bang ke tinh thue GTGT
                
                if(["fhs"].includes(config.ent)) fn = `temp/BCTH_OU_FHS.xlsx`
                else if(["fbc"].includes(config.ent)) fn = `temp/BCTH_OU_FBC.xlsx`
                binds = [fd, td, taxc]
                let sqlou, bindou = [token.ou], resultou, rowsou = [], colgrp

                if (bcthtype == "BCTH_OU_FHS") {
                    //Lấy danh sách ou cùng các ou con, cháu
                    // sqlou = `with recursive ou_tree as ( select id as id, name as name, array[]::integer[] as roots from s_ou where pid is null union all select oc.id, oc.name as name, op.roots || oc.pid as roots from ou_tree op, s_ou oc where oc.pid = op.id ) select id as id, name from s_ou where id = $1 union all select id, name from ou_tree where $1 = any(ou_tree.roots)`
                    //sqlou = `select id as id, name as name from s_ou where id = $1 or pid = $1`

                    //resultou = await dbs.query(sqlou, bindou)
                    //rowsou = resultou.rows
                    colgrp = "ou"

                } else {
                    colgrp = "idt"
                }
                let rowbc0 = [], rowbc5 = [], rowbc10 = [], rowbc1 = [], a0 = [], a5 = [], a10 = [], a1 = [], a2 = [], s0 = [], s5 = [], s10 = [], s1 = [], s2 = [], a0510 = [], a05101 = [], s0510 = [], s05101 = [], rowbc0510 = [], rowbc05101 = []
                let amv = [], smv = [], rowbcmv = []
                let rowbctitle1 = [], rowbctitle2s = [], rowbctitle2a = [], rowbctitle2 = []
                let totala0 = 0, totala5 = 0, totala10 = 0, totala1 = 0, totala2 = 0, totala0510 = 0, totala05101 = 0, totalamv = 0
                let totals0 = 0, totals5 = 0, totals10 = 0, totals1 = 0, totals2 = 0, totals0510 = 0, totals05101 = 0, totalsmv = 0

                let subsql = `select
                        "type" as "type",
                        ou ou,
                        idt idt,
                        adjtyp adjtyp,
                        dif dif,
                        adjtypeapi adjtypeapi,
                        factor factor,
                        factordc factordc, 
                        vrt vrt,
                        vatv * factor * factordc vatv,
                        sumv * factor * factordc sumv
                        from
                        (
                        select
                            "c0" as "type",
                            ou ou,
                            date_trunc('month', idt) idt,
                            adjtyp,
                            ((((((doc)::json ->> 'dif'::text))::json ->> 'total'::text))::numeric(17,
                                2)) dif,
                            ((((((doc)::json ->> 'adj'::text))::json ->> 'adjType'::text))::int) adjtypeapi,
                            case
                                when adjtyp = 2 then
                                case
                                    when ((((((doc)::json ->> 'dif'::text))::json ->> 'total'::text))::numeric(17,
                                        2)) > 0 then -1
                                    when ((((((doc)::json ->> 'dif'::text))::json ->> 'total'::text))::numeric(17,
                                        2)) < 0 then 1
                                    when ((((((doc)::json ->> 'dif'::text))::json ->> 'total'::text))::numeric(17,
                                        2)) is null then
                                    case
                                        when ((((((doc)::json ->> 'adj'::text))::json ->> 'adjType'::text))::int) = 3 then -1
                                        else 1 end end
                                        else 1 end factor, case when c1 = 'Điều chỉnh giảm' then -1 else 1 end factordc, 
                                        (json_array_elements(doc::json->'tax') ->> 'vrt'::text)::text as vrt,
                                        (json_array_elements(doc::json->'tax') ->> 'vatv'::text)::numeric(17,
                                        2) as vatv,
                                        (json_array_elements(doc::json->'tax') ->> 'amtv'::text)::numeric(17,
                                        2) as sumv
                                    from
                                        s_inv inv
                                    where
                                        idt between $1 and $2
                                        and stax = $3
                                        and status = 3
                                        and type = '01GTKT' ${where_ft} 
                                union all
                                    select
                                        "c0" as "type",
                                        ou ou,
                                        date_trunc('month', idt) idt,
                                        null as adjtyp,
                                        null as dif,
                                        null as adjtypeapi,
                                        1 as factor, case when c1 is null or c1 = '' then 1 else -1 end factordc, 
                                        (json_array_elements(doc::json->'tax') ->> 'vrt'::text)::text as vrt,
                                        (json_array_elements(doc::json->'tax') ->> 'vatv'::text)::numeric(17,
                                        2) as vatv,
                                        (json_array_elements(doc::json->'tax') ->> 'amtv'::text)::numeric(17,
                                        2) as sumv
                                    from
                                        s_inv inv
                                    where
                                        idt between $1 and $2
                                        and stax = $3
                                        and status = 4
                                        and type = '01GTKT'
                                        and (c0='0' and NOT(idt BETWEEN $1 and $2 and cdt BETWEEN $1 and $2)) ${where_ft} 
                                union all
                                    select
                                        "c0" as "type",
                                        ou ou,
                                        date_trunc('month', idt) idt,
                                        null as adjtyp,
                                        null as dif,
                                        null as adjtypeapi,
                                        -1 as factor, case when c1 is null or c1 = '' then 1 else -1 end factordc, 
                                        (json_array_elements(doc::json->'tax') ->> 'vrt'::text)::text as vrt,
                                        (json_array_elements(doc::json->'tax') ->> 'vatv'::text)::numeric(17,
                                        2) as vatv,
                                        (json_array_elements(doc::json->'tax') ->> 'amtv'::text)::numeric(17,
                                        2) as sumv
                                    from
                                        s_inv inv
                                    where
                                        cdt between $1 and $2
                                        and stax = $3
                                        and status = 4
                                        and type = '01GTKT'
                                        and (c0='0' and to_char(idt,'YYYY-MM')!= to_char(cdt,'YYYY-MM') and NOT(idt BETWEEN $1 and $2 and cdt BETWEEN $1 and $2)) ${where_ft}
                                union all
                                    select
                                        "c0" as "type",
                                        ou ou,
                                        date_trunc('month', idt) idt,
                                        null as adjtyp,
                                        null as dif,
                                        null as adjtypeapi,
                                        -1 as factor, case when c1 is null or c1 = '' then -1 else 1 end factordc, 
                                        (json_array_elements(doc::json->'tax') ->> 'vrt'::text)::text as vrt,
                                        (json_array_elements(doc::json->'tax') ->> 'vatv'::text)::numeric(17,
                                        2) as vatv,
                                        (json_array_elements(doc::json->'tax') ->> 'amtv'::text)::numeric(17,
                                        2) as sumv
                                    from
                                        s_inv inv
                                    where
                                        idt between $1 and $2
                                        and stax = $3
                                        and status = 4
                                        and type = '01GTKT'
                                        and to_char(idt,'YYYY-MM') != to_char(cdt,'YYYY-MM') and c0='1' ${where_ft} 
                                union all
                                    select
                                        "c0" as "type",
                                        ou ou,
                                        date_trunc('month', idt) idt,
                                        null as adjtyp,
                                        null as dif,
                                        null as adjtypeapi,
                                        1 as factor, case when c1 is null or c1 = '' then -1 else 1 end factordc, 
                                        (json_array_elements(doc::json->'tax') ->> 'vrt'::text)::text as vrt,
                                        (json_array_elements(doc::json->'tax') ->> 'vatv'::text)::numeric(17,
                                        2) as vatv,
                                        (json_array_elements(doc::json->'tax') ->> 'amtv'::text)::numeric(17,
                                        2) as sumv
                                    from
                                        s_inv inv
                                    where
                                        cdt between $1 and $2
                                        and stax = $3
                                        and status = 4
                                        and type = '01GTKT'
                                        and to_char(idt,'YYYY-MM')!= to_char(cdt,'YYYY-MM') and c0='1' ${where_ft}) inv`

                //Lấy dữ liệu ou đẩy ra bảng temp
                let conn
                conn = await dbs.connect()
                try {
                    const oulist = getou.rowsou
                    await conn.query(`drop table if exists s_tmp`)
                    await conn.query(`create temp table s_tmp(id int4, lv int4, pid int4)`)
                    for (const row of oulist) {
                        await conn.query(`insert into s_tmp (id, lv, pid) values ($1,$2,$3)`, [row.id, row.lv, row.pid])
                    }

                    sql = `select
                inv.${colgrp} ${colgrp}, ${(bcthtype == "BCTH_OU_FHS") ? "t.pid oupid, t.lv lv, " : ""}
                inv."type" as "type",
                inv.vrt vrt,
                sum(inv.sumv) sumv,
                sum(inv.vatv) vatv
            from
                (
                    ${subsql}
                    ) inv, s_tmp t
                    where inv.ou = t.id
            group by
                inv.${colgrp}, ${(bcthtype == "BCTH_OU_FHS") ? "t.pid, t.lv, " : ""}
                inv."type",
                inv.vrt`

                    //binds.push(token.ou)
                    result = await conn.query(sql, binds)
                    rows = result.rows
                } catch (e) {
                    throw e
                }
                finally {
                    if (conn) conn.release()
                }
                //Chỉnh lại số liệu nếu in theo ou, nếu cấp nhỏ hơn 2 thì cộng vào cấp cha
                if (bcthtype == "BCTH_OU_FHS") {
                    for (const row of rows) {
                        if (row.lv >= 2) row.ou = row.oupid
                    }
                }

                if (bcthtype == "BCTH_OU_FHS") {
                    let i=0, idarr = []
                    for (const oucol of rows) {
                        oucol.id = oucol.ou
                        a0[oucol.id] = 0
                        a5[oucol.id] = 0
                        a10[oucol.id] = 0
                        a1[oucol.id] = 0
                        a2[oucol.id] = 0
                        s0[oucol.id] = 0
                        s5[oucol.id] = 0
                        s10[oucol.id] = 0
                        s1[oucol.id] = 0
                        s2[oucol.id] = 0

                        a0510[oucol.id] = 0
                        a05101[oucol.id] = 0
                        s0510[oucol.id] = 0
                        s05101[oucol.id] = 0

                        amv[oucol.id] = 0
                        smv[oucol.id] = 0
                        
                        sqlou = `select id as id, name as name from s_ou where id = $1`

                        let resultoutmp = await dbs.query(sqlou, [oucol.id])
                        let rowsoutmp = resultoutmp.rows
                        oucol.name = rowsoutmp[0].name
                        if (!idarr.includes(oucol.id)) {
                            idarr.push(oucol.id)
                            rowsou.push(oucol)
                            rowbctitle1.push(oucol.name)
                            rowbctitle2s.push("Doanh thu (chưa có thuế GTGT)")
                            rowbctitle2a.push("Thuế GTGT")
                        }
                        
                    }
                } else {
                    for (let idx = 0; idx <= 11; idx++) {
                        /*
                        let sql,result,finalResult,bindss
                        sql=`select
                        sum(sumv) as sum,
                        sum(vatv) as vat,
                        date_part('month', idt) idt
                    from
                        ( ${subsql}
                        ) as inv where date_part('month', idt)= ${idx + 1 }
                    group by 
                    date_part('month', idt)`
                    
                        bindss=[fd,td,taxc]
                        result= await dbs.query(sql,bindss)
                        finalResult=result.rows
                        */
                        a0[idx] = 0
                        a5[idx] = 0
                        a10[idx] = 0
                        a1[idx] = 0
                        a2[idx] = 0
                        s0[idx] = 0
                        s5[idx] = 0
                        s10[idx] = 0
                        s1[idx] = 0
                        s2[idx] = 0

                        a0510[idx] = 0
                        a05101[idx] = 0
                        s0510[idx] = 0
                        s05101[idx] = 0

                        amv[idx] = 0
                        smv[idx] = 0
                        
                        rowbctitle1.push("Tháng ".concat(String(idx+1).padStart(2, '0')))
                        rowbctitle2s.push("Doanh thu (chưa có thuế GTGT)")
                        rowbctitle2a.push("Thuế GTGT")
                    }
                }
                
                if (bcthtype == "BCTH_OU_FHS") {
                    for (const row of rows) {
                        if (row.type == '0') {
                            try {
                                if (row.vatv)
                                    //row.vatv = parseInt(Math.round(parseFloat(row.vatv)))
                                    row.vatv = parseFloat(row.vatv)
                                else
                                    row.vatv = 0
                            } catch (err) {
                                row.vatv = 0
                            }

                            switch (parseInt(row.vrt)) {
                                case 0:
                                    a0[row.ou] += row.vatv
                                    //s0[row.ou] += parseInt(Math.round(parseFloat(row.sumv)))
                                    s0[row.ou] += parseFloat(row.sumv)

                                    a0510[row.ou] += a0[row.ou]
                                    a05101[row.ou] += a0[row.ou]

                                    s0510[row.ou] += s0[row.ou]
                                    s05101[row.ou] += s0[row.ou]
                                    break
                                case 5:
                                    a5[row.ou] += row.vatv
                                    //s5[row.ou] += parseInt(Math.round(parseFloat(row.sumv)))
                                    s5[row.ou] += parseFloat(row.sumv)

                                    a0510[row.ou] += a5[row.ou]
                                    a05101[row.ou] += a5[row.ou]

                                    s0510[row.ou] += s5[row.ou]
                                    s05101[row.ou] += s5[row.ou]
                                    break
                                case 10:
                                    a10[row.ou] += row.vatv
                                    //s10[row.ou] += parseInt(Math.round(parseFloat(row.sumv)))
                                    s10[row.ou] += parseFloat(row.sumv)

                                    a0510[row.ou] += a10[row.ou]
                                    a05101[row.ou] += a10[row.ou]

                                    s0510[row.ou] += s10[row.ou]
                                    s05101[row.ou] += s10[row.ou]
                                    break
                                case -1:
                                    a1[row.ou] += row.vatv
                                    //s1[row.ou] += parseInt(Math.round(parseFloat(row.sumv)))
                                    s1[row.ou] += parseFloat(row.sumv)

                                    a05101[row.ou] += a1[row.ou]
                                    s05101[row.ou] += s1[row.ou]
                                    break
                                case -2:
                                    a2[row.ou] += row.vatv
                                    //s2[row.ou] += parseInt(Math.round(parseFloat(row.sumv)))
                                    s2[row.ou] += parseFloat(row.sumv)
                                    break
                                default:
                                    break
                            }
                        }

                        if (row.type == '1') {
                            try {
                                if (row.vatv)
                                    //row.vatv = parseInt(Math.round(parseFloat(row.vatv)))
                                    row.vatv = parseFloat(row.vatv)
                                else
                                    row.vatv = 0
                            } catch (err) {
                                row.vatv = 0
                            }
                            amv[row.ou] += row.vatv*(-1)
                            //smv[row.ou] += parseInt(Math.round(parseFloat(row.sumv)))*(-1)
                            smv[row.ou] += parseFloat(row.sumv)*(-1)
                        }

                    }
                } else {
                    for (const row of rows) {
                        let month = row.idt.getMonth() + 1
                        if (row.type == '0') {
                            try {
                                if (row.vatv)
                                    //row.vatv = parseInt(Math.round(parseFloat(row.vatv)))
                                    row.vatv = parseFloat(row.vatv)
                                else
                                    row.vatv = 0
                            } catch (err) {
                                row.vatv = 0
                            }
                            switch (parseInt(row.vrt)) {
                                case 0:
                                    a0[month-1] += row.vatv
                                    //s0[month-1] += parseInt(Math.round(parseFloat(row.sumv)))
                                    s0[month-1] += parseFloat(row.sumv)

                                    a0510[month-1] += a0[month-1]
                                    a05101[month-1] += a0[month-1]

                                    s0510[month-1] += s0[month-1]
                                    s05101[month-1] += s0[month-1]
                                    break
                                case 5:
                                    a5[month-1] += row.vatv
                                    //s5[month-1] += parseInt(Math.round(parseFloat(row.sumv)))
                                    s5[month-1] += parseFloat(row.sumv)

                                    a0510[month-1] += a5[month-1]
                                    a05101[month-1] += a5[month-1]

                                    s0510[month-1] += s5[month-1]
                                    s05101[month-1] += s5[month-1]
                                    break
                                case 10:
                                    a10[month-1] += row.vatv
                                    //s10[month-1] += parseInt(Math.round(parseFloat(row.sumv)))
                                    s10[month-1] += parseFloat(row.sumv)

                                    a0510[month-1] += a10[month-1]
                                    a05101[month-1] += a10[month-1]

                                    s0510[month-1] += s10[month-1]
                                    s05101[month-1] += s10[month-1]
                                    break
                                case -1:
                                    a1[month-1] += row.vatv
                                    //s1[month-1] += parseInt(Math.round(parseFloat(row.sumv)))
                                    s1[month-1] += parseFloat(row.sumv)

                                    a05101[month-1] += a1[month-1]
                                    s05101[month-1] += s1[month-1]
                                    break
                                case -2:
                                    a2[month-1] += row.vatv
                                    //s2[month-1] += parseInt(Math.round(parseFloat(row.sumv)))
                                    s2[month-1] += parseFloat(row.sumv)
                                    break
                                default:
                                    break
                            }
                        }

                        if (row.type == '1') {
                            try {
                                if (row.vatv)
                                    //row.vatv = parseInt(Math.round(parseFloat(row.vatv)))
                                    row.vatv = parseFloat(row.vatv)
                                else
                                    row.vatv = 0
                            } catch (err) {
                                row.vatv = 0
                            }
                            amv[month-1] += row.vatv *(-1)
                            //smv[month-1] += parseInt(Math.round(parseFloat(row.sumv)))*(-1)
                            smv[month-1] += parseFloat(row.sumv)*(-1)
                        }

                    }
                }
                
                if (bcthtype == "BCTH_OU_FHS") {
                    let i=0;
                    for (const oucol of rowsou) {
                        rowbc0.push(s0[oucol.id])
                        rowbc0.push(a0[oucol.id])

                        rowbc5.push(s5[oucol.id])
                        rowbc5.push(a5[oucol.id])

                        rowbc10.push(s10[oucol.id])
                        rowbc10.push(a10[oucol.id])

                        rowbc1.push(s1[oucol.id])
                        rowbc1.push(a1[oucol.id])

                        rowbc0510.push(s0510[oucol.id])
                        rowbc0510.push(a0510[oucol.id])

                        rowbc05101.push(s05101[oucol.id])
                        rowbc05101.push(a05101[oucol.id])

                        rowbcmv.push(smv[oucol.id])
                        rowbcmv.push(amv[oucol.id])

                        rowbctitle2.push(rowbctitle2s[i])
                        rowbctitle2.push(rowbctitle2a[i])

                        totala0 += a0[oucol.id]
                        totala5 += a5[oucol.id]
                        totala10 += a10[oucol.id]
                        totala1 += a1[oucol.id]
                        totala0510 += a0510[oucol.id]
                        totala05101 += a05101[oucol.id]
                        totalamv += Number(amv[oucol.id])

                        totals0 += s0[oucol.id]
                        totals5 += s5[oucol.id]
                        totals10 += s10[oucol.id]
                        totals1 += s1[oucol.id]
                        totals0510 += s0510[oucol.id]
                        totals05101 += s05101[oucol.id]
                        totalsmv += Number(smv[oucol.id])
                        i++
                    }
                } else {
                    for (let idx = 0; idx < a0.length; idx++) {
                        rowbc0.push(s0[idx])
                        rowbc0.push(a0[idx])

                        rowbc5.push(s5[idx])
                        rowbc5.push(a5[idx])

                        rowbc10.push(s10[idx])
                        rowbc10.push(a10[idx])

                        rowbc1.push(s1[idx])
                        rowbc1.push(a1[idx])

                        rowbc0510.push(s0510[idx])
                        rowbc0510.push(a0510[idx])

                        rowbc05101.push(s05101[idx])
                        rowbc05101.push(a05101[idx])

                        rowbcmv.push(smv[idx])
                        rowbcmv.push(amv[idx])
                        rowbctitle2.push(rowbctitle2s[idx])
                        rowbctitle2.push(rowbctitle2a[idx])

                        totala0 += a0[idx]
                        totala5 += a5[idx]
                        totala10 += a10[idx]
                        totala1 += a1[idx]
                        totala0510 += a0510[idx]
                        totala05101 += a05101[idx]
                        totalamv += Number(amv[idx])

                        totals0 += s0[idx]
                        totals5 += s5[idx]
                        totals10 += s10[idx]
                        totals1 += s1[idx]
                        totals0510 += s0510[idx]
                        totals05101 += s05101[idx]
                        totalsmv += Number(smv[idx])
                    }
                }
                rowbc0.push(totals0)
                rowbc0.push(totala0)

                rowbc5.push(totals5)
                rowbc5.push(totala5)

                rowbc10.push(totals10)
                rowbc10.push(totala10)

                rowbc1.push(totals1)
                rowbc1.push(totala1)

                rowbc0510.push(totals0510)
                rowbc0510.push(totala0510)

                rowbc05101.push(totals05101)
                rowbc05101.push(totala05101)

                rowbcmv.push(totalsmv)
                rowbcmv.push(totalamv)

                rowbctitle1.push("Tổng cộng")
                rowbctitle2s.push("Doanh thu (chưa có thuế GTGT)")
                rowbctitle2a.push("Thuế GTGT")

                const lenghts=rowbc0.length
                //Cho cột tổng doanh thu
                rowbctitle2.push(rowbctitle2s[(lenghts/2)-1])
                rowbctitle2.push(rowbctitle2a[(lenghts/2)-1])

                //Chỉnh chỉ hiển thị các cột có giá trị
                let rowbc0s=[],rowbc1s=[],rowbc10s=[],rowbc5s=[],rowbc0510s=[],rowbc05101s=[],rowbcmvs=[],rowbctitle2ss=[],rowbctitle1s=[]
                let k=0, sum=0
                for(let ii=0;ii<lenghts;ii++){
                    if ((ii % 2) == 0) sum = 0
                    
                    sum += Math.abs(rowbc0[ii]) + Math.abs(rowbc1[ii]) + Math.abs(rowbc10[ii]) + Math.abs(rowbc5[ii]) + Math.abs(rowbcmv[ii])
                    if(((ii % 2) == 1) && (sum !=0)){
                        // logger4app.debug(rowbctitle2[ii])
                        rowbc0s.push(rowbc0[ii-1])
                        rowbc1s.push(rowbc1[ii-1])
                        rowbc10s.push(rowbc10[ii-1])
                        rowbc5s.push(rowbc5[ii-1])
                        rowbc0510s.push(rowbc0510[ii-1])
                        rowbc05101s.push(rowbc05101[ii-1])
                        rowbcmvs.push(rowbcmv[ii-1])
                        rowbctitle2ss.push(rowbctitle2[ii-1])

                        rowbc0s.push(rowbc0[ii])
                        rowbc1s.push(rowbc1[ii])
                        rowbc10s.push(rowbc10[ii])
                        rowbc5s.push(rowbc5[ii])
                        rowbc0510s.push(rowbc0510[ii])
                        rowbc05101s.push(rowbc05101[ii])
                        rowbcmvs.push(rowbcmv[ii])
                        rowbctitle2ss.push(rowbctitle2[ii])

                        rowbctitle1s.push(rowbctitle1[(ii-1)/2])
                        /*
                        if ((ii + (ii + 1) % 2 != 0) && ii % 2 == 0) {
                            k = ii / 2
                            rowbctitle1s.push(rowbctitle1[k])
                        }
                        */
                    }
                }
                json = { stax: taxc, sname: branchesStr, fd: fr, td: to, rt: rt, tn: tn, rowbc0: rowbc0s, rowbc5: rowbc5s, rowbc10: rowbc10s, rowbc1: rowbc1s, rowbc0510: rowbc0510s, rowbc05101: rowbc05101s, rowbcmv: rowbcmvs, rowbctitle1: rowbctitle1s, rowbctitle2s: rowbctitle2s, rowbctitle2a: rowbctitle2a, rowbctitle2: rowbctitle2ss }

            } else if (report == 15) {
                
                fn = "temp/08BKHDXT01_FHS.xlsx"
                where = "where idt between $1 and $2 and stax=$3"
                binds = [fd, td, taxc]
                let ij = 4
                if (type !== "*") {
                    where += ` and type=$${ij++}`
                    binds.push(type)
                }


                if ((["fhs", "fbc"].includes(config.ent))) {
                    where += where_ft
                } else {
                    if (!ou.includes("*")) {

                        where += ` and ou=$${ij++}`
                        binds.push(ou)

                    }
                }

                sql = `select to_char(idt,'DD/MM/YYYY') idt,to_char(cdt,'DD/MM/YYYY') cdt,form,serial,seq,btax,bname,buyer,CASE WHEN status=1 THEN 'Chờ cấp số' WHEN status=2 THEN 'Chờ duyệt' WHEN status=3 THEN 'Đã duyệt' WHEN status=4 THEN 'Đã hủy' WHEN status=6 THEN 'Chờ hủy' ELSE CAST(status as varchar(22)) END status,status cstatus,sumv,vatv,CONCAT(note,'-',cde,'-',adjdes) note, c0 from s_inv ${where} order by idt,form,serial,seq`
                result = await dbs.query(sql, binds)
                rows = result.rows
                let i = 0, rowbc = []
                for (const row of rows) {
                    row.index = ++i
                    row.c0 = row.c0 == '0' ? "Hóa đơn GTGT bán hàng" : "Hóa đơn GTGT xuất hoàn trả"
                    let arr = row.note.split("-"), arr1 = []
                    for (let i = 0; i <= arr.length - 1; i++) {
                        if (String(arr[i]).length > 0) arr1.push(arr[i])
                    }
                    row.note = arr1.join("-")
                    row.sumv = 1 * row.sumv
                    row.vatv = 1 * row.vatv
                    rowbc.push(row)
                    if (row.cstatus == 4) {
                        let rowtemp = { ...row }
                        rowtemp.sumv = -1 * rowtemp.sumv
                        rowtemp.vatv = -1 * rowtemp.vatv
                        rowtemp.idt = rowtemp.cdt
                        rowtemp.index = ++i
                        rowbc.push(rowtemp)
                    }
                }
                json = { stax: taxc, sname: branchesStr, fd: fr, td: to, rt: rt, tn: tn, table: rowbc }
                
            }
            const file = path.join(__dirname, "..", "..", "..", fn)
            const xlsx = fs.readFileSync(file)
            const template = new xlsxtemp(xlsx)
            template.substitute(1, json)
            res.end(template.generate(), "binary")
        } catch (err) {
            next(err)
        }
    }
}
module.exports = Service