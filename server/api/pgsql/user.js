"use strict"
const util = require("../util")
const sec = require("../sec")
const dbs = require("./dbs")
const logging = require("../logging")
const config = require("../config")
const logger4app = require("../logger4app")
const Service = {
  member: async (req, res, next) => {
    try {
      const body = req.body, uid = body.uid, arr = body.arr
      //await dbs.query(`delete from s_member where user_id=$1`, [uid])
      await dbs.query(`delete from s_group_user where user_id=$1`, [uid])
      
      if (arr.length > 0) {
        for (const rid of arr) {
            // await dbs.query(`insert into s_member(user_id,role_id) values ($1,$2)`, [uid, rid])
            await dbs.query(`insert into s_group_user(user_id,group_id) values ($1,$2)`, [uid, rid])
            
        }
       
      }
      const sSysLogs = { fnc_id: 'group_user_insert', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm thông tin Tài khoản ${uid} vào nhóm`};
      logging.ins(req,sSysLogs,next)
      res.json(1)
    }
    catch (err) {
      next(err)
    }
  },
  manager: async (req, res, next) => {
    try {
      const body = req.body, uid = body.uid, arr = body.arr
      await dbs.query(`delete from s_manager where user_id=$1`, [uid])
      if (arr.length > 0) {
        for (const oid of arr) {
          await dbs.query(`insert into s_manager(user_id, taxc) values ($1,$2)`, [uid, oid])
        }
      }
      res.json(1)
    }
    catch (err) {
      next(err)
    }
  },
  role: async (req, res, next) => {
    try {
      const uid = req.params.uid
     // const sql = `select x.id,x.name,x.sel from (select a.id,a.name,0 sel from s_role a left join s_member b on a.id = b.role_id and b.user_id=$1 where b.role_id is null union select a.id,a.name,1 sel from s_role a inner join s_member b on a.id = b.role_id and b.user_id=$1) x order by x.id`
      const sql = `select x.id "id",x.name "name",x.sel "sel" from (select a.id,a.name,0 sel from s_group a left join s_group_user b on a.id = b.group_id and b.user_id=$1 where b.group_id is null union select a.id,a.name,1 sel from s_group a inner join s_group_user b on a.id = b.group_id and b.user_id=$1) x order by x.id`
      const result = await dbs.query(sql, [uid])
      res.json(result.rows)
    }
    catch (err) {
      next(err)
    }
  },
  ou: async (req, res, next) => {
    try {
      const token = sec.decode(req), ou = token.ou, uid = req.params.uid
      const sql = `with recursive tree as (select id,name,taxc,pid,0 as level,'('||id||'-0)' idx from s_ou where id=$1
        union all
        select c.id,c.name,c.taxc,c.pid,p.level+1,p.idx||'*('||c.id||'-'||p.level||')' idx from s_ou c,tree p where p.id=c.pid
        ) select x.id,x.pid,x.taxc,x.name,x.checked from (
        select a.id,a.pid,a.taxc,a.name,a.idx,0 checked from tree a left  join s_manager b on a.taxc=b.taxc and b.user_id=$2 where b.taxc is null 
        union 
        select a.id,a.pid,a.taxc,a.name,a.idx,1 checked from tree a inner join s_manager b on a.taxc=b.taxc and b.user_id=$2) 
        x where x.taxc is not null order by idx`
      const result = await dbs.query(sql, [token.u, uid])
      const rows = result.rows
      let arr = [], obj
      for (const row of rows) {
        if (row.id == ou) {
          row.open = true
          arr.push(row)
        }
        else {
          obj = util.findDFS(arr, row.pid)
          if (obj) {
            if (!obj.hasOwnProperty('data')) obj.data = []
            obj.data.push(row)
          }
        }
      }
      res.json(arr)
    }
    catch (err) {
      next(err)
    }
  },
  get: async (req, res, next) => {
    try {
      const token = sec.decode(req), query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 10, sort = query.sort
      let where = " where a.ou=b.id", order = "", sql, result, ret, binds = [token.ou]
      if (query.filter) {
        let filter = JSON.parse(query.filter), val
        let i = 2
        Object.keys(filter).forEach((key) => {
          val = filter[key]
          if (val) {
            if (key == "id" || key == "mail" || key == "name" || key == "pos" || key == "code") {
              where += ` and upper(a.${key}) like $${i++}`
              binds.push(`%${val.toUpperCase()}%`)
            }
            else {
              where += ` and a.${key}=$${i++}`
              binds.push(val)
            }
          }
        })
      }
      if (sort) {
        Object.keys(sort).forEach((key) => {
          order = `order by a.${key}  ${sort[key]}`
        })
      } else order = `order by a.id`
      const tree = `with recursive tree as (select id from s_ou where id=$1 union all select c.id from s_ou c,tree p where p.id=c.pid)`
      sql = `${tree} select a.id,a.mail,a.ou,a.uc,a.name,a.pos,a.code${(config.is_use_local_user) ? `,a.local` : ``} from s_user a,tree b ${where} ${order}  limit ${count} offset ${start}`
      result = await dbs.query(sql, binds)
      ret = { data: result.rows, pos: start }
      if (start == 0) {
        sql = `${tree} select count(*) total from s_user a,tree b ${where}`
        result = await dbs.query(sql, binds)
        ret.total_count = result.rows[0].total
      }
      const sSysLogs = { fnc_id: 'user_get', src: config.SRC_LOGGING_DEFAULT, dtl: `Tra cứu thông tin NSD`};
      logging.ins(req,sSysLogs,next)
      return res.json(ret)
    }
    catch (err) {
      next(err)
    }
  },
  post: async (req, res, next) => {
    try {
      const body = req.body, operation = body.webix_operation
      let sql, result, binds, code = body.code ? body.code : null
      if (operation == "update") {
        sql = `update s_user set ou=$1,name=$2,pos=$3,code=$4,mail=$5 where id=$6`
        binds = [body.ou, body.name, body.pos, code, body.mail, body.id]
      }
      result = await dbs.query(sql, binds)
      res.json(result)
      const sSysLogs = { fnc_id: 'user_update', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật thông tin Tài khoản ${body.id}`, msg_id: body.id, doc: JSON.stringify(body)};
      logging.ins(req,sSysLogs,next)
    }
    catch (err) {
      next(err)
    }
  },
  ubr: async (req, res, next) => {
    try {
      const token = sec.decode(req), rid = req.params.rid
      const sql = `with recursive tree as (select id from s_ou where id=$1 union all select c.id from s_ou c,tree p where p.id=c.pid),suse as (select u.* from s_user u,tree t where u.ou=t.id)
        select x.id acc,x.name,x.sel,x.mail,x.ou,x.uc,x.pos from (
        select a.id,a.name,0 sel,a.mail,a.ou,a.uc,a.pos from suse a left  join s_member b on a.id=b.user_id and b.role_id=$2 where b.user_id is null 
        union 
        select a.id,a.name,1 sel,a.mail,a.ou,a.uc,a.pos from suse a inner join s_member b on a.id=b.user_id and b.role_id=$2) 
        x order by x.id`
      const result = await dbs.query(sql, [token.ou, rid])
      res.json(result.rows)
    } catch (err) {
      next(err)
    }
  },
  mbr: async (req, res, next) => {
    try {
      const body = req.body, rid = body.rid, users = body.users
      await dbs.query(`delete from s_member where role_id=$1`, [rid])
      if (users.length > 0) {
        for (const uid of users) {
          await dbs.query(`insert into s_member(role_id,user_id) values ($1,$2)`, [rid, uid])
        }
      }
      res.json(1)
    } catch (err) {
      next(err)
    }
  },
  disable: async (req, res, next) => {
    try {
      const uid = req.params.uid
      await dbs.query(`update s_user set uc=$1 where id=$2`, [2, uid])
      const sSysLogs = { fnc_id: 'user_disable', src: config.SRC_LOGGING_DEFAULT, dtl: `Hủy bỏ Tài khoản ${uid}`, msg_id: uid};
      logging.ins(req,sSysLogs,next)
      res.send(`Tài khoản ${uid} đã bị hủy bỏ \n (The account ${uid} has been canceled)`)
    } catch (err) {
      next(err)
    }
  },
  enable: async (req, res, next) => {
    try {
      const uid = req.params.uid
      await dbs.query(`update s_user set uc=$1 where id=$2`, [1, uid])
      const sSysLogs = { fnc_id: 'user_enable', src: config.SRC_LOGGING_DEFAULT, dtl: `Kích hoạt Tài khoản ${uid}`, msg_id: uid};
      logging.ins(req,sSysLogs,next)
      res.send(`Tài khoản ${uid} đã được kích hoạt \n (The account ${uid} has been activated)`)
    } catch (err) {
      next(err)
    }
  },
  ins: async (req, obj) => {
    try {
      let ou = obj.ou
      if (!ou) {
        const token = sec.decode(req)
        ou = token.ou
      }
      const result = await dbs.query(`insert into s_user(id,code,name,ou,mail,pos${(config.is_use_local_user) ? `,local,pass` : ``}) values ($1,$2,$3,$4,$5,$6${(config.is_use_local_user) ? `,$7,$8` : ``})`, (config.is_use_local_user) ? [obj.id, (obj.code) ? obj.code : null, obj.name, ou, obj.mail, obj.pos, obj.local,obj.pass] : [obj.id, (obj.code) ? obj.code : null, obj.name, ou, obj.mail, obj.pos])
      const sSysLogs = { fnc_id: 'user_insert', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm thông tin Tài khoản ${obj.id}`, msg_id: obj.id, doc: JSON.stringify(obj)};
      logging.ins(req,sSysLogs)
      return result.rowCount
    } catch (err) {
      throw err
    }
  },
  obid: async (id) => {
      const sql = `select id,mail,ou,uc,name,pos,code${(config.is_use_local_user) ? `,local,change_pass_date,login_number` : ``} from s_user where id=$1`
      const result = await dbs.query(sql, [id])
      let row = result.rows[0]
      return row
  },
  upduserst: async (id) => {
    const sql = `update s_user set last_login = NOW() where id=$1`
    await dbs.query(sql, [id])
  },
  updatelocaluserpass: async (id, oldpass, newpass) => {
    let sql,result,binds,rows

    //Check xem old pass co dung hay khong
    sql = `select count(*) countret from s_user where pass = $1 and id = $2`
    binds = [oldpass, id]
    result = await dbs.query(sql, binds)
    if (result.rows[0].countret <= 0) {
      throw new Error(`Mật khẩu cũ không đúng \n Old password is incorrect`)
    }

    //Check xem new pass co trung voi cac mat khau cu hay khong
    sql = `select pass pass, change_pass_date change_date from s_user where id = $1 
    union all 
    select pass pass, change_date change_date from s_user_pass where user_id = $2 
    order by change_date desc 
    OFFSET 0 ROWS FETCH NEXT $3 ROWS ONLY`
    binds = [id, id, config.total_pass_store - 1]
    result = await dbs.query(sql, binds)
    rows =  result.rows
    for (const row of rows) {
      if (row.pass == newpass) {
        throw new Error(`Mật khẩu không được trùng lặp với ${config.total_pass_store} mật khẩu cũ \n Password cannot be the same as ${config.total_pass_store} old passwords`)
      }
    }
    
    //Insert mat khau cu vao bang s_user_pass
    await dbs.query(`insert into s_user_pass(user_id,pass,change_date) values ($1,$2,NOW())`, [id, oldpass])

    //Update mat khau moi
    sql = `UPDATE s_user
              SET pass = $1
              ,change_pass_date = NOW()
              ,change_pass = 1
            WHERE id = $2`
    result = await dbs.query(sql, [newpass, id])
  },
  resetlocaluserpass: async (id, mail, newpass) => {
    let sql,result,binds,rows

    //Check xem old pass co dung hay khong
    sql = `select count(*) countret from s_user where mail = $1 and id = $2`
    binds = [mail, id]
    result = await dbs.query(sql, binds)
    if (result.rows[0].countret <= 0) {
      throw Error(`Tài khoản ${id} Mail ${mail} không tồn tại \n (Account ${id} Mail ${mail} does not exist)`)
    }

    //Update mat khau moi
    sql = `UPDATE s_user
              SET pass = $1
              ,change_pass_date = NOW()
              ,change_pass = 0
            WHERE id = $2`
    result = await dbs.query(sql, [newpass, id])
  }
}
module.exports = Service 