"use strict"
const moment = require("moment")
const config = require("./config")
const logger4app = require("./logger4app")
const email = require("./email")
const Client = require('ssh2-sftp-client')
const decrypt = require('./encrypt')

const Service = {
    uploadattach:
        async (doc, xml) => {
            let ftpClient = new Client()
            try {
                const arr = doc.bmail.trim().split(/[ ,;]+/).map(e => e.trim())
                let attachments = await email.getOnlyAttachFile(doc, xml, arr)
                if (doc && (doc.status == 3) && attachments[0].filename) {
                    let filepath = config.ftpdestPath + attachments[0].filename
                    let ftpConfig = JSON.parse(JSON.stringify(config.ftpConfig))
                    ftpConfig.password = decrypt.decrypt(ftpConfig.password)
                    ftpClient.connect(ftpConfig)
                        .then(() => {
                            return ftpClient.put(Buffer.from(attachments[0].content), filepath)
                        })
                        .then(() => {
                            return ftpClient.end()
                        })
                        .catch(err => {
                            logger4app.error(err.message)
                        })
                }
            } catch (err) {
                logger4app.debug(`uploadattach file ${doc.id}: `, err.message)
                throw (err)
            } finally {

            }
        },
        uploadmailctbc:
        async (doc, xml) => {
            let ftpClient = new Client()
            let filepath
            try {
                let filename = `VX01-${moment(new Date()).format('YYYYMMDD')}${moment(new Date()).format('HHmmssSSS')}-${doc.bcode}-${moment(new Date()).format('YYYYMMDD')}-${moment(new Date()).format('HHmmssSSS')}`.split("/").join(".")
                
                filepath = config.ftpdestPath + filename + '.xml'
                let ftpConfig = JSON.parse(JSON.stringify(config.ftpConfig))
                ftpConfig.password = decrypt.decrypt(ftpConfig.password)
                ftpClient.connect(ftpConfig)
                    .then(() => {
                        return ftpClient.put(Buffer.from(xml), filepath)
                    })
                    .then(() => {
                        return ftpClient.end()
                    })
                    .catch(err => {
                        logger4app.error(err.message)
                    })

            } catch (err) {
                logger4app.debug(`uploadattach uploadmailctbc file ${filepath}: `, err)
                throw (err)
            } finally {

            }
        },
        uploadsocctbcdetail:
        async (filecontent) => {
            let ftpClient = new Client()
            let filepath
            try {
                let filename = `VNEINV-${moment(new Date()).format('YYYYMMDD')}-${moment(new Date()).format('HHmm')}`.split("/").join(".")
                
                filepath = config.ftpdestPath + filename + '.D'
                let ftpConfig = JSON.parse(JSON.stringify(config.ftpConfig))
                ftpConfig.password = decrypt.decrypt(ftpConfig.password)
                ftpClient.connect(ftpConfig)
                    .then(() => {
                        return ftpClient.put(Buffer.from(filecontent,{ encoding: 'utf8'}), filepath)
                    })
                    .then(() => {
                        return ftpClient.end()
                    })
                    .catch(err => {
                        logger4app.error(err.message)
                    })

            } catch (err) {
                logger4app.debug(`uploadsocctbc file ${filepath}: `, err)
                throw (err)
            } finally {

            }
        },
        uploadsocctbch:
        async (filecontent) => {
            let ftpClient = new Client()
            let filepath
            try {
                let filename = `VNEINV-${moment(new Date()).format('YYYYMMDD')}-${moment(new Date()).format('HHmm')}`.split("/").join(".")
                
                filepath = config.ftpdestPath + filename + '.H'
                let ftpConfig = JSON.parse(JSON.stringify(config.ftpConfig))
                ftpConfig.password = decrypt.decrypt(ftpConfig.password)
                ftpClient.connect(ftpConfig)
                    .then(() => {
                        return ftpClient.put(Buffer.from(filecontent,{ encoding: 'utf8'}), filepath)
                    })
                    .then(() => {
                        return ftpClient.end()
                    })
                    .catch(err => {
                        logger4app.error(err.message)
                    })

            } catch (err) {
                logger4app.debug(`uploadsocctbc file ${filepath}: `, err)
                throw (err)
            } finally {

            }
        },
        uploadsplunkscb:
        async (filecontent,check) => {
            let ftpClient = new Client()
            let filepath ,filename
            try {
                if(check==1){
                    filename = `AUMSER-ENVOICE_Audit_${moment(new Date()).format('DDMMYYYY')}${moment(new Date()).format('HHmm')}`.split("/").join(".")
                }else if(check==2){
                    filename = `AUMSER-ENVOICE_ID_${moment(new Date()).format('DDMMYYYY')}${moment(new Date()).format('HHmm')}`.split("/").join(".")
                }else if(check==3){
                    filename = `AUMSER-ENVOICE_Login_Logout_${moment(new Date()).format('DDMMYYYY')}${moment(new Date()).format('HHmm')}`.split("/").join(".")
                }else {
                    filename = `AUMSER-ENVOICE_Profile_${moment(new Date()).format('DDMMYYYY')}${moment(new Date()).format('HHmm')}`.split("/").join(".")
                }
                filepath = config.ftpdestPath + filename + '.csv'
                let ftpConfig = JSON.parse(JSON.stringify(config.ftpConfig))
                ftpConfig.password = decrypt.decrypt(ftpConfig.password)
                ftpClient.connect(ftpConfig)
                    .then(() => {
                        return ftpClient.put(Buffer.from(filecontent,{ encoding: 'utf8'}), filepath)
                    })
                    .then(() => {
                        return ftpClient.end()
                    })
                    .catch(err => {
                        logger4app.error(err.message)
                    })

            } catch (err) {
                logger4app.debug(`uploadsplunkscb file ${filepath}: `, err)
                throw (err)
            } finally {

            }
        }
}
module.exports = Service