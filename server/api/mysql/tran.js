"use strict"
const Hashids = require("hashids/cjs")
const moment = require("moment")
const config = require("../config")
const logger4app = require("../logger4app")
const email = require("../email")
const sign = require("../sign")
const spdf = require("../spdf")
const util = require("../util")
const redis = require("../redis")
const hbs = require("../hbs")
const fcloud = require("../fcloud")
const hsm = require("../hsm")
const SEC = require("../sec")
const SERIAL = require("./serial")
const dbs = require("./dbs")
const COL = require("./col")
const ous = require("./ous")
const path = require("path")
const fs = require("fs")
const xlsxtemp = require("xlsx-template")
const logging = require("../logging")
const { error } = require("console")
const hashids = new Hashids(config.secret, 10, "abcdefghijklmnopqrstuvwxyz1234567890")
const CARR = config.CARR
const grant = config.ou_grant
const serial_usr_grant = config.serial_usr_grant
const mfd = config.mfd
const dtf = config.dtf
const ENT = config.ent
const invoice_seq = config.invoice_seq
const UPLOAD_MINUTES = config.upload_minute
const rbi = (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            const result = await dbs.query("select doc from s_inv where id=@1", [id])
            const rows = result.recordset
            if (rows.length == 0) reject(new Error(`Không tìm thấy Hóa đơn ${id} \n (Invoice not found ${id})`))
            const doc = JSON.parse(rows[0].doc)
            resolve(doc)
        } catch (err) {
            reject(err)
        }
    })
}
const oubi = (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            const result = await dbs.query("select name name from s_ou where id=@1", [id])
            let name
            const rows = result.recordset
            if (rows.length == 0) 
                name = ""
            else 
                name = rows[0].name
            resolve(name)
        } catch (err) {
            reject(err)
        }
    })
}
const chkou = async (id, token) => {
    if (grant) {
        if (token.ou !== token.u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)
        const result = await dbs.query(`select ou from s_inv where id=@1`, [id])
        const rows = result.recordset
        if (rows.length > 0 && rows[0].ou !== token.u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)
    }
}
const Service = {
    count12: async (req, res, next) => {
        try {
            const token = SEC.decode(req), query = req.query
            const result = await dbs.query(`select count(*) rc from s_inv where stax=@1 and form=@2 and serial=@3 and status in (1, 2)`, [token.taxc, query.form, query.serial])
            res.json(result.recordset[0])
        }
        catch (err) {
            next(err)
        }
    },
    
    
    iwh: async (req, all) => {
        const token = SEC.decode(req), u = token.u, query = req.query, filter = JSON.parse(query.filter), sort = query.sort
        let keys = Object.keys(filter), kar = [], i = 3
        let  val,  cols, where = `where valueDate between @1 and @2 `, binds = [moment(new Date(filter.fd)).format(dtf), moment(moment(filter.td).endOf("day")).format(dtf)], order
       
        kar.push("fd", "td")
        for (const key of kar) {
            delete keys[key]
        }
        for (const key of keys) {
            val = filter[key]
            if (val ) {
                switch (key) {
                    case "status":
                        where += ` and status in (${val})`
                      
                        break
                    case "trantype":
                        where += ` and trantype = @${i++}`
                        binds.push(`%${val}%`)
                        break
                    case "customerAcc":
                        where += ` and customerAcc like @${i++}`
                        binds.push(`%${val}%`)
                        break
                    case "curr":
                        where += ` and curr = @${i++}`
                        binds.push(`${val}`)
                        break
                    case "segment":
                        where += ` and segment = @${i++}`
                        binds.push(`${val}`)
                        break
                    case "inv_id":
                            where += ` and inv_id = @${i++}`
                            binds.push(`${val}`)
                            break
                    case "inv_date":
                        where += ` and inv_date = @${i++}`
                        binds.push(moment(new Date(`${val}`)).format(dtf))
                        break
                    case "refNo":
                            where += ` and refNo like @${i++}`
                            binds.push(`%${val}%`)
                        break
                    case "content":
                            where += ` and upper(vcontent) like @${i++}`
                            binds.push(`%${val.toString().toUpperCase()}%`)
                        break
                    case "vat":
                        where += ` and vrt in (${val})`
                      
                        break
                
                }
            }
        }
      
        if (sort) {
            Object.keys(sort).forEach(key => {
                order = `order by ${key} ${sort[key]}`
            })
        }
        else order = "order by valueDate desc"
        return { where: where, binds: binds,  order: order }
    },
   
   
    get: async (req, res, next) => {
        try {
            //console.time("timeget")
            const query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 10, iwh = await Service.iwh(req, true)
            let sql, result, ret
            sql = `select 0 chk,[ID]
            ,[refNo]
            ,[valueDate]
            ,[customerID]
            ,[customerName]
            ,[taxCode]
            ,[customerAddr]
            ,[isSpecial]
            ,[curr]
            ,[exrt]
            ,[vrt]
            ,[chargeAmount]
            ,[vcontent]
            ,[amount]
            ,[vat]
            ,status
            ,[total]
            ,FORMAT (create_date, 'dd/MM/yyyy, HH:mm:ss') create_date
            , FORMAT (last_update, 'dd/MM/yyyy, HH:mm:ss') last_update
            ,[ma_nv]
            ,[ma_ks]
            
            ,[tranID] from s_trans ${iwh.where} ${iwh.order} OFFSET ${start} ROWS FETCH NEXT ${count} ROWS ONLY`
            result = await dbs.query(sql, iwh.binds)
            ret = { data: result.recordset, pos: start }
            if (start == 0) {
                sql = `select count(*) total from s_trans ${iwh.where}`
                result = await dbs.query(sql, iwh.binds)
                ret.total_count = result.recordset[0].total
            }
            //console.timeEnd("timeget")
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
  
  
    
   
   
   
   
   
    del: async (req, res, next) => {
        try {
            const token = SEC.decode(req), id = req.params.id, pid = req.params.pid
            await chkou(id, token)
            //Lay du lieu doc truoc khi xoa de luu log
            let doclog = await rbi(id)
            const result = await dbs.query(`delete from s_inv where id=@1 and stax=@2 and status=@3`, [id, token.taxc, 1])
            if (result.rowsAffected[0] > 0 && pid > 0) {
                let doc = await rbi(pid)
                doc.status = 3
                //Xóa doc.cde , nếu không có cũng ko ảnh hưởng.
                delete doc["cde"]
                delete doc["cdt"]
                delete doc["canceller"]
                // await dbs.query(`update s_inv set doc=JSON_MODIFY(doc,'$.status',3),cid=null,cde=null where id=@1`, [pid])
                await dbs.query(`update s_inv set doc=@1,cid=null,cde=null where id=@2`, [JSON.stringify(doc), pid])
                //Xóa biên bản
                await dbs.query(`DELETE FROM s_inv_file where id=@1`, [pid])
            }
            const sSysLogs = { fnc_id: 'inv_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doclog) };
            logging.ins(req, sSysLogs, next)
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    }
    
    
}
module.exports = Service