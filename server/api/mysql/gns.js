"use strict"
const config = require("../config")
const logger4app = require("../logger4app")
const logging = require("../logging")
const util = require("../util")
const dbs = require("./dbs")
const Service = {
    fbn: async (req, res, next) => {
        try {
            const query = req.query, filter = query.filter
            let sql, result, where = " where 1=1", binds = [], val
            if (filter) {
                val = `%${filter.value.trim().toUpperCase()}%`
                where += " and (upper(code) like ? or upper(name) like ?)"
                binds.push(val)
                binds.push(val)
            }
            sql = `select code code,name name,unit unit,price price from s_gns ${where} order by code LIMIT ${config.limit}`
            result = await dbs.query(sql, binds)
            res.json(result[0])
        }
        catch (err) {
            next(err)
        }
    },
    fbw: async (req, res, next) => {
        try {
            const query = req.query, filter = query.filter, sort = query.sort
            let sql, result, where = " where 1=1", order = "", binds = [], val
            if (filter) {
                Object.keys(filter).forEach(key => {
                    val = filter[key]
                    if (val) {
                        if (key == "price") {
                            if (util.isNumber(val)) {
                                where += ` and price=?`
                                binds.push(val)
                            }
                            else where += ` and price = ${val}`
                        }
                        else {
                            where += ` and upper(${key}) like ?`
                            binds.push(`%${val.toUpperCase()}%`)
                        }

                    }
                })
            }
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = ` order by ${key} ${sort[key]}`
                })
            }
            sql = `select id "id",code "code",name "name",unit "unit",price "price" from s_gns ${where} ${order} LIMIT ${config.limit}`
            result = await dbs.query(sql, binds)
            res.json(result[0])
        }
        catch (err) {
            next(err)
        }
    },
    get: async (req, res, next) => {
        try {
            const query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 10, sort = query.sort
            let sql, result, ret, where = " where 1=1", order = "", binds = []
            if (query.filter) {
                let filter = JSON.parse(query.filter), val
                Object.keys(filter).forEach(key => {
                    val = filter[key]
                    if (val) {
                        if (key == "unit") {
                            where += ` and ${key}=?`
                            binds.push(val)
                        }
                        else {
                            where += ` and upper(${key}) like ?`
                            binds.push(`%${val.toUpperCase()}%`)
                        }
                    }
                })
            }
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = ` order by ${key} ${sort[key]}`
                })
            }
            sql = `select id "id",code "code",name "name",unit "unit",price "price" from s_gns ${where}  ${order} LIMIT ${count} offset ${start}`
            result = await dbs.query(sql, binds)
            ret = { data: result[0], pos: start }
            if (start == 0) {
                sql = `select count(*) "total" from s_gns ${where}`
                result = await dbs.query(sql, binds)
                ret.total_count = result[0][0].total
            }
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const body = req.body, operation = body.webix_operation
            let sql, result, binds, price = body.price ? body.price : null, sSysLogs = { fnc_id: '', src: config.SRC_LOGGING_DEFAULT, dtl: ``}
            switch (operation) {
                case "update":
                    sql = `update s_gns set code=?,name=?,price=?,unit=? where id=?`
                    binds = [body.code, body.name, price, body.unit, body.id]
                    sSysLogs = { fnc_id: 'gns_upd', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật danh mục hàng hóa, dịch vụ ${body.name} (${operation})`, msg_id: body.id, doc: JSON.stringify(body) };
                    break
                case "insert":
                    sql = `insert into s_gns set ?`
                    binds = { code: body.code, name: body.name, unit: body.unit, price: price }
                    sSysLogs = { fnc_id: 'gns_ins', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm mới danh mục hàng hóa, dịch vụ ${body.name} (${operation})`, msg_id: body.id, doc: JSON.stringify(body) };
                    break
                case "delete":
                    sql = `delete from s_gns where id=?`
                    binds = [body.id]
                    sSysLogs = { fnc_id: 'gns_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa danh mục hàng hóa, dịch vụ ${body.name} (${operation})`, msg_id: body.id, doc: JSON.stringify(body) };
                    break
                default:
                    throw new Error(`${operation} không hợp lệ \n(${operation} is invalid)`)
            }
            result = await dbs.query(sql, binds)
            logging.ins(req, sSysLogs, next)
            if (operation == "insert") res.json({ id: result[0].insertId })
            else res.json(result[0].affectedRows)
        }
        catch (err) {
            next(err)
        }
    },
    ins: async (req, res, next) => {
        try {
            const body = req.body
            const cols = ['code', 'name', 'unit', 'price']
            let arr = []
            const sql = `insert into s_gns set ?`
            for (const row of body) {
                let id = row.id
                try {
                    for (const col of cols) {
                        if (!row.hasOwnProperty(col)) row[col] = null
                    }
                    delete row["id"]
                    delete row["chk"]
                    delete row["error"]
                    await dbs.query(sql, row)
                    const sSysLogs = { fnc_id: 'gns_ins', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm mới danh mục hàng hóa, dịch vụ ${body.name}`, msg_id: body.id, doc: JSON.stringify(body) };
                    logging.ins(req, sSysLogs, next)
                    arr.push({ id: id, error: "OK" })
                } catch (err) {
                    if (err.errno == 1062) err.message = "duplicate"
                    arr.push({ id: id, error: err.message })
                }
            }
            res.json(arr)
        }
        catch (err) {
            next(err)
        }
    }
}
module.exports = Service