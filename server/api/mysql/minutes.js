
"use strict"
const moment = require("moment")
const sec = require("../sec")
const dbs = require("./dbs")
const util = require("../util")
const config = require("../config")
const logger4app = require("../logger4app")
const n2w = require("../n2w")
const ENT = config.ent
const UPLOAD_MINUTES = config.upload_minute
const mfd = config.mfd
const formatItems = (rows) => {
    for (const row of rows) {
        if (!row.unit) row.unit = ""
        if(ENT=='bahuan'){
            if(row.vrt=='-1') row.vrn='\\'
            if(row.vrt=='-2') row.vrn='x'
        }
        row.price = util.fn2(row.price)
        row.amount = util.fn2(row.amount)
        if (!row.quantity) row.quantity = ""
        row.name=String(row.name).replace("<br>","")
    }
    return rows
}
const extCanHLV = (doc) => {
    doc.cbname = doc.bname
    doc.cbaddr = doc.baddr
    doc.cbtax = doc.btax
    doc.cbacc = doc.bacc
    doc.cbbank = doc.bbank ? `tại ${doc.bbank}` : ""
    doc.cbuyer = doc.buyer
    doc.cidt = doc.idt
    doc.cform = doc.form
    doc.cserial = doc.serial
    doc.cseq = doc.seq
    doc.cc0 = doc.c0
    doc.cc1 = doc.c1
    doc.cc2 = doc.c2
    doc.cc3 = doc.c3
    doc.cc4 = doc.c4
    doc.cc5 = doc.c5
    doc.cc6 = doc.c6
    doc.cc7 = doc.c7
    doc.cc8 = doc.c8
    doc.csumv = doc.sumv
    doc.ctotalv = doc.totalv
    doc.cvatv = doc.vatv
    doc.ctotalvString = doc.word
    doc.cvrn = doc.tax.length > 0 ? doc.tax[0].vrn : ""
    JSON.parse(JSON.stringify(doc).replace(/null/g, "\"\""))
    doc.pc1 = doc.c1 ? moment(doc.c1).format(config.mfd) : ""
    doc.pc7 = doc.c2 == 3 ? doc.pc7 : `${doc.pc7} ${doc.pc1}`
    try {
        doc.pc7 = doc.c2 == 3 ? doc.c7 :(doc.c2 == 6 ?`${doc.c7} ${(doc.pc1).substring(3)}`:`${doc.c7} ${doc.pc1}`) 
        } catch (err) {
            
        }
    return doc
}

const extRepHLV = (cdoc, pdoc) => {
    pdoc.cbname = cdoc.bname
    pdoc.cbaddr = cdoc.baddr
    pdoc.cbtax = cdoc.btax
    pdoc.cbacc = cdoc.bacc
    pdoc.cbbank = cdoc.bbank ? `tại ${cdoc.bbank}` : ""
    pdoc.cbuyer = cdoc.buyer
    pdoc.cseq = cdoc.seq
    pdoc.cpaym = cdoc.paym
    for (let row of cdoc.items) 
    {
        for (let i in row)
        {
            if (util.isEmpty(row[i])) row[i] = ""
        }
       
        if (typeof  row.unit=="undefined") 
        {
            row.unit="";
        }
    }
    pdoc.citems = cdoc.items
    pdoc.pc0 = pdoc.c0
    pdoc.pc1 = pdoc.c1
    pdoc.pc2 = pdoc.c2
    pdoc.pc3 = pdoc.c3
    pdoc.pc4 = pdoc.c4
    pdoc.pc5 = pdoc.c5
    pdoc.pc6 = pdoc.c6
    pdoc.pc7 = pdoc.c7
    pdoc.pc8 = pdoc.c8
    pdoc.psumv = util.fn2(pdoc.sumv)
    pdoc.ptotalv = util.fn2(pdoc.totalv)
   
    pdoc.ptotalvString = pdoc.word
    pdoc.pvrn = pdoc.items.length > 0 ? pdoc.items[0].vrn : ""

    pdoc.cidt = moment(cdoc.idt).format(config.mfd)
    pdoc.cform = cdoc.form
    pdoc.cserial = cdoc.serial
    pdoc.cseq = cdoc.seq
    pdoc.cc0 = cdoc.c0
    pdoc.cc1 = cdoc.c1
    pdoc.cc2 = cdoc.c2
    pdoc.cc3 = cdoc.c3
    pdoc.cc4 = cdoc.c4
    pdoc.cc5 = cdoc.c5
    pdoc.cc6 = cdoc.c6
    pdoc.cc7 = cdoc.c7
    pdoc.cc8 = cdoc.c8
    pdoc.csumv = util.fn2(cdoc.sumv)
    pdoc.ctotalv = util.fn2(cdoc.totalv)
    pdoc.ctotalvString = cdoc.word
    pdoc.cvrn = cdoc.items.length > 0 ? cdoc.items[0].vrn : ""
    pdoc.cvatv = util.fn2(cdoc.vatv)

    JSON.parse(JSON.stringify(pdoc).replace(/null/g, "\"\""))
    pdoc.pc1 = pdoc.c1 ? moment(pdoc.c1).format(config.mfd) : ""
    try {
    pdoc.pc7 = pdoc.c2 == 3 ? pdoc.pc7 :(pdoc.c2 == 6 ?`${pdoc.pc7} ${(pdoc.pc1).substring(3)}`:`${pdoc.pc7} ${pdoc.pc1}`) 
    } catch (err) {
        
    }
   
    pdoc.cc1 = cdoc.c1 ? moment(pdoc.cc1).format(config.mfd) : ""
    try {
    pdoc.cc7 = cdoc.c2 == 3 ? pdoc.cc7 :(cdoc.c2 == 6?`${pdoc.cc7} ${(pdoc.cc1).substring(3)}`:`${pdoc.cc7} ${pdoc.cc1}`) 
    } catch (err) {
        
    }
    return pdoc
}
const Service = {
    minutesReplace: async (req, res, next) => {
        try {
            const token = sec.decode(req), id = req.body.id, adj = req.body.inv.adj
            const result = await dbs.query(`select doc from s_inv where id=?`, [id])
            let doc = result[0][0].doc
            doc.idt = moment(doc.idt).format(mfd)
            doc.items = formatItems(doc.items)
            if (doc.curr == "VND") {
                doc.vatv = util.fn2(doc.vatv)
                doc.sumv = util.fn2(doc.sumv)
                doc.totalv = util.fn2(doc.totalv)
            } else {
                doc.vatv = util.fn2(doc.vat)
                doc.sumv = util.fn2(doc.sum)
                doc.totalv = util.fn2(doc.total)
                doc.word = n2w.n2w(doc.total, doc.curr)
            }
            doc.ref = adj.ref
            doc.rea = adj.rea
            doc.btel= (doc.btel) ? doc.btel : ''
            doc.bmail = (doc.bmail) ? doc.bmail : ''
            doc.btax= (doc.btax) ? doc.btax : ''
            doc.rdt = moment(adj.rdt).format(mfd)
            doc.isvat = doc.type == "01GTKT" ? true : false
            for (let row of doc.items) 
            {
                for (let i in row)
                {
                    if (util.isEmpty(row[i])) row[i] = ""
                }
               
                if (typeof  row.unit=="undefined") 
                {
                    row.unit="";
                }
            }
            doc.mType = "thay thế"
            let fn
            if(ENT == "hlv"){
                fn = req.body.inv.form.includes("/001") ? "BBThayThe_HLV001.docx" : "BBThayThe_HLV002.docx"
            }else{
                fn = doc.type.toLowerCase() + "mrep.docx"
            }
           // let fn = ENT == "hlv" ? "BBThayThe_HLV.docx" : "mrep.docx"
            if (ENT == "hlv") {
                let cdoc = req.body.inv
                doc = extRepHLV(cdoc, doc)
                if (req.body.inv.form.includes("/001") && doc.cc2 == 6) {
                    doc.cc3 = ""
                }
                if (req.body.inv.form.includes("/001") && doc.pc2 == 6) {
                    doc.pc3 = ""
                }
            }
            const out = util.docxrender(doc, fn)
            res.end(out, "binary")
        } catch (err) {
            next(err)
        }
    },
    minutesCancel: async (req, res, next) => {
        try {
            let doc = req.body, adj = doc.adj
            doc.idt = moment(doc.idt).format(mfd)
            doc.items = formatItems(doc.items)
            if (doc.curr == "VND") {
                doc.vatv = util.fn2(doc.vatv)
                doc.sumv = util.fn2(doc.sumv)
                doc.totalv = util.fn2(doc.totalv)
            } else {
                doc.vatv = util.fn2(doc.vat)
                doc.sumv = util.fn2(doc.sum)
                doc.totalv = util.fn2(doc.total)
                doc.word = n2w.n2w(doc.total, doc.curr)
            }
            doc.ref = adj.ref
            doc.rea = adj.rea
            doc.btel= (doc.btel) ? doc.btel : ''
            doc.bmail = (doc.bmail) ? doc.bmail : ''
            doc.btax= (doc.btax) ? doc.btax : ''
            doc.rdt = moment(adj.rdt).format(mfd)
            doc.isvat = doc.type == "01GTKT" ? true : false
            doc.mType = "hủy"
            let fn
            if(ENT == "hlv"){
                fn = req.body.inv.form.includes("/001") ? "BBHuy_HLV001.docx" : "BBHuy_HLV002.docx"
            }else{
                fn = doc.type.toLowerCase() + "mcan.docx"
            }
           // let fn = ENT == "hlv" ? "BBHuy_HLV.docx" : "mcan.docx"
            if (ENT == "hlv") {
                doc = extCanHLV(doc)
                if (req.body.inv.form.includes("/001") && doc.cc2 == 6) {
                    doc.cc3 = ""
                }
                if (req.body.inv.form.includes("/001") && doc.pc2 == 6) {
                    doc.pc3 = ""
                }
            }
            const out = util.docxrender(doc, fn)
            res.end(out, "binary")
        } catch (err) {
            next(err)
        }
    },
    minutesCreate: async (req, res, next) => {
        try {
            
            const doc = req.body, adj = doc.adj
            let result = await dbs.query(`select pid,type,status,adjtyp,doc from s_inv where id=?`, [doc.id])
            let row = result[0][0], pdoc = row.doc,fn
            doc.idt = moment(doc.idt).format(mfd)
            doc.ref = adj.ref
            doc.rea = adj.rea
            doc.des = adj.des
			doc.btel= (doc.btel) ? doc.btel : ''
            doc.bmail = (doc.bmail) ? doc.bmail : ''
            doc.btax= (doc.btax) ? doc.btax : ''
            doc.rdt = moment(adj.rdt).format(mfd)
            doc.items0 = formatItems(doc.items0)
            doc.items1 = formatItems(doc.items1)
            doc.isvat = doc.type == "01GTKT" ? true : false
            if (!doc.seq) doc.seq = "............"
            doc.mType = "điều chỉnh"
            doc.bname0 = pdoc.bname
            doc.baddr0 = pdoc.baddr
            doc.btax0 = pdoc.btax
            doc.sumv0 = ["hlv"].includes(ENT) ? (Math.round((pdoc.sumv) * 100) / 100).toLocaleString().replace(/,/gi, ".") : pdoc.sumv
            doc.vatv0 =  ["hlv"].includes(ENT) ? (Math.round((pdoc.vatv) * 100) / 100).toLocaleString().replace(/,/gi, ".") : pdoc.vatv
            doc.c00 = pdoc.c0
            doc.c40 = pdoc.c4
            doc.c710 = pdoc.c7+" "+(pdoc.c1 ? moment(pdoc.c1).format(config.mfd) : "")
            if (ENT == "hlv") {
                if (doc.c2 == 3) {
                    doc.c71 = doc.c7
                } else {
                    doc.c71 = doc.c7 + " " + (doc.c1 ? moment(doc.c1).format(config.mfd) : "")
                }
                
                if (pdoc.c2 == 3) {
                    doc.c710 = pdoc.c7
                } else {
                    doc.c710 = pdoc.c7+" "+(pdoc.c1 ? moment(pdoc.c1).format(config.mfd) : "")
                }
            }    
            doc.c30 = pdoc.c3
            doc.totalv0 = ["hlv"].includes(ENT) ? (Math.round((pdoc.totalv) * 100) / 100).toLocaleString().replace(/,/gi, ".") : pdoc.totalv
            doc.vrn0 =  pdoc.items[0]?pdoc.items[0].vrn:''
            doc.vrn = doc.items[0]?doc.items[0].vrn:''
            doc.word0 = pdoc.word
            if (!doc.seq) doc.seq = "............"
            doc.mType = "điều chỉnh"
            if(ENT == "hlv"){
                doc.idt = moment(pdoc.idt).format(mfd)
              
                doc.form = pdoc.form
                doc.serial = pdoc.serial
                doc.seq = pdoc.seq
               if(doc.totalv==0)   doc.word='Không đồng'
                fn = doc.type.toLowerCase() + "adj_hlv.docx"
            }else{
                fn = doc.type.toLowerCase() + "madj.docx"
            }
          
            const out = util.docxrender(doc, fn)
            res.end(out, "binary")
        } catch (err) {
            next(err)
        }
    },
    minutesCustom: async (req, res, next) => {
        try {
            const token = sec.decode(req), id = req.params.id, download = req.query.download
            let result = await dbs.query(`select pid,type,status,adjtyp,doc,cde from s_inv where id=?`, [id])
            let row = result[0][0], type = row.type, status = row.status, doc = row.doc, fn, pid = row.pid
            let repseq, repform, repidt, repserial
            if (ENT == "lge") {
                repseq = doc.seq,
                repform = doc.form,
                repidt = moment(doc.idt).format(mfd),
                repserial = doc.serial
            }
            if (UPLOAD_MINUTES && download) {
                let typ, iid = pid
                if (status == 4) {
                    typ = "can"
                    iid = id
                } else {
                    let adjtyp = row.adjtyp
                    if (adjtyp == 1) {
                        typ = "rep"
                    } else if (adjtyp == 2) {
                        typ = "adj"
                    }
                }
                if (!typ) {
                    iid = id
                    typ = "normal"
                }
                let minutes = await dbs.query(`select id, type, content from s_inv_file where id=? and type=?`, [iid, typ])
                if (!minutes[0].length) throw new Error("Không tìm thấy biên bản! \n (No Minutes found)")
                let file = Buffer.from(minutes[0][0].content,"base64")
                res.end(file, "binary")
            } else {
                //Bien ban tu tao
                if (status == 4) {
                    let cancel = doc.cancel
                    if (!cancel) throw new Error(`Hóa đơn không có biên bản.\n (Invocies don't have Minutes)`)
                    doc.idt = moment(doc.idt).format(mfd)
                    doc.items = formatItems(doc.items)
                    if (doc.curr == "VND") {
                        doc.vatv = util.fn2(doc.vatv)
                        doc.sumv = util.fn2(doc.sumv)
                        doc.totalv = util.fn2(doc.totalv)
                    } else {
                        doc.vatv = util.fn2(doc.vat)
                        doc.sumv = util.fn2(doc.sum)
                        doc.totalv = util.fn2(doc.total)
                        doc.word = n2w.n2w(doc.total, doc.curr)
                    }
                    doc.ref = cancel.ref
                    doc.rea = cancel.rea
					doc.btel= (doc.btel) ? doc.btel : ''
					doc.bmail = (doc.bmail) ? doc.bmail : ''
					doc.btax= (doc.btax) ? doc.btax : ''
                    doc.rdt = moment(cancel.rdt).format(mfd)
                    doc.isvat = type == "01GTKT" ? true : false
                    doc.mType = "hủy"
                    if(ENT == "hlv"){
                        fn = doc.form.includes("/001") ? "BBHuy_HLV001.docx" : "BBHuy_HLV002.docx"
                    }else{
                        fn = doc.type.toLowerCase() + "mcan.docx"
                    }
                  
                    if (ENT == "hlv") {
                        doc = extCanHLV(doc)
                        if (doc.form.includes("/001") && doc.cc2 == 6) {
                            doc.cc3 = ""
                        }
                        if (doc.form.includes("/001") && doc.pc2 == 6) {
                            doc.pc3 = ""
                        }
                    }
                }
                else {
                    let adjtyp = row.adjtyp
                    if (adjtyp && pid ) {
                        let adj = doc.adj
                        // if (ENT == "lge") {
                        //   let  resultrep = await dbs.query(`select doc from s_inv where id=?`, [id])
                        // }
                        result = await dbs.query(`select doc from s_inv where id=?`, [pid])
                        row = result[0][0]
                        let pdoc = row.doc
                        if (ENT == "lge") {
                            Object.assign(pdoc, {"repseq": repseq, "repidt": repidt, "repform": repform, "repserial": repserial})
                        }
                        if (adjtyp == 1) {
                            pdoc.idt = moment(pdoc.idt).format(mfd)
                            pdoc.items = formatItems(pdoc.items)
                            doc.items = formatItems(doc.items)
                            if (doc.curr == "VND") {
                                pdoc.vatv = util.fn2(pdoc.vatv)
                                pdoc.sumv = util.fn2(pdoc.sumv)
                                pdoc.totalv = util.fn2(pdoc.totalv)
                            } else {
                                pdoc.vatv = util.fn2(pdoc.vat)
                                pdoc.sumv = util.fn2(pdoc.sum)
                                pdoc.totalv = util.fn2(pdoc.total)
                                pdoc.word = n2w.n2w(pdoc.total, pdoc.curr)
                            }
                            pdoc.ref = adj.ref
                            pdoc.rea = adj.rea
							pdoc.btel= (pdoc.btel) ? pdoc.btel : ''
							pdoc.bmail = (pdoc.bmail) ? pdoc.bmail : ''
							pdoc.btax= (pdoc.btax) ? pdoc.btax : ''
                            pdoc.rdt = moment(adj.rdt).format(mfd)
                            pdoc.isvat = type == "01GTKT" ? true : false
                            pdoc.mType = "thay thế"
                            pdoc.time = moment(doc.adj.rdt ? doc.adj.rdt : doc.adj.idt).format(config.mfd)
                            if(ENT == "hlv"){
                                fn = doc.form.includes("/001") ? "BBThayThe_HLV001.docx" : "BBThayThe_HLV002.docx"
                            }else{
                                fn = pdoc.type.toLowerCase() + "mrep.docx"
                            }
                          
                          //  fn = ENT == "hlv" ? "BBThayThe_HLV001.docx" : "mrep.docx"
                            if (ENT == "hlv") {
                                pdoc = extRepHLV(doc, pdoc)
                                if (pdoc.form.includes("/001") && pdoc.cc2 == 6) {
                                    pdoc.cc3 = ""
                                }
                                if (pdoc.form.includes("/001") && pdoc.pc2 == 6) {
                                    pdoc.pc3 = ""
                                }
                            }
                            doc = pdoc // To print
                        }
                        else if (adjtyp == 2) {
                            let items1 = doc.items, items0 = pdoc.items
                            doc.idt = moment(doc.idt).format(mfd)
                            doc.ref = adj.ref
                            doc.rea = adj.rea
                            doc.des = adj.des
							doc.btel = (doc.btel) ? doc.btel : ''
							doc.bmail = (doc.bmail) ? doc.bmail : ''
							doc.btax= (doc.btax) ? doc.btax : ''
                            doc.rdt = moment(adj.rdt).format(mfd)
                            doc.items0 = formatItems(items0)
                            doc.items1 = formatItems(items1)
                            doc.isvat = type == "01GTKT" ? true : false
                            doc.bname0 = pdoc.bname
                            doc.baddr0 = pdoc.baddr
                            doc.btax0 = pdoc.btax
                            doc.sumv0 = pdoc.sumv
                            doc.vatv0 = pdoc.vatv
                            doc.totalv0 = pdoc.totalv
                            doc.vrn0 = pdoc.items[0]?pdoc.items[0].vrn:''
                            doc.vrn = doc.items[0].vrn
                            doc.word0 = pdoc.word
                            if (!doc.seq) doc.seq = "............"
                            doc.mType = "điều chỉnh"
                            if(ENT == "hlv"){
                                doc.c00 = pdoc.c0
                                doc.c40 = pdoc.c4
                                if (doc.c2 == 3) {
                                    doc.c71 = doc.c7
                                } else {
                                    doc.c71 = doc.c7 + " " + (doc.c1 ? moment(doc.c1).format(config.mfd) : "")
                                }
                                
                                if (pdoc.c2 == 3) {
                                    doc.c710 = pdoc.c7
                                } else {
                                    doc.c710 = pdoc.c7+" "+(pdoc.c1 ? moment(pdoc.c1).format(config.mfd) : "")
                                }

                                doc.c30 = pdoc.c3
                                fn = doc.type.toLowerCase() + "adj_hlv.docx"
                            }else{
                                fn = doc.type.toLowerCase() + "madj.docx"
                            }
                            
                        }
                    }
                    if ( ENT == "hlv" && (row.doc.cde).indexOf('Bị điều chỉnh') >=0) {
                        let cdoc = doc.cdoc
                        let pdoc = doc
                        let adj = cdoc.adj
                        let items1 = cdoc.items, items0 = pdoc.items
                            cdoc.idt = moment(cdoc.idt).format(mfd)
                            cdoc.ref = adj.ref
                            cdoc.rea = adj.rea
                            cdoc.des = adj.des
							cdoc.btel = (cdoc.btel) ? cdoc.btel : ''
							cdoc.bmail = (cdoc.bmail) ? cdoc.bmail : ''
							cdoc.btax= (cdoc.btax) ? cdoc.btax : ''
                            cdoc.rdt = moment(adj.rdt).format(mfd)
                            cdoc.items0 = formatItems(items0)
                            cdoc.items1 = formatItems(items1)
                            cdoc.isvat = type == "01GTKT" ? true : false
                            cdoc.bname0 = pdoc.bname
                            cdoc.baddr0 = pdoc.baddr
                            cdoc.btax0 = pdoc.btax
                            cdoc.sumv0 = (Math.round((pdoc.sumv) * 100) / 100).toLocaleString().replace(/,/gi, ".") 
                            cdoc.vatv0 = (Math.round((pdoc.vatv) * 100) / 100).toLocaleString().replace(/,/gi, ".") 
                            cdoc.totalv0 = (Math.round((pdoc.totalv) * 100) / 100).toLocaleString().replace(/,/gi, ".") 
                            cdoc.vrn0 = pdoc.items[0]?pdoc.items[0].vrn:''
                            cdoc.vrn =  cdoc.items[0]?cdoc.items[0].vrn:''
                            cdoc.word0 = pdoc.word
                          
                            cdoc.idt =moment(pdoc.idt).format(mfd) 
                            cdoc.form = pdoc.form
                            cdoc.serial = pdoc.serial
                            cdoc.seq = pdoc.seq
                         
                            if (!cdoc.seq) cdoc.seq = "............"
                            cdoc.mType = "điều chỉnh"
                            cdoc.c00 = pdoc.c0
                            cdoc.c40 = pdoc.c4
                            cdoc.c710 = pdoc.c7+" "+(pdoc.c1 ? moment(pdoc.c1).format(config.mfd) : "")
                          
                            cdoc.c71 = pdoc.c7+" "+ (cdoc.c1 ? moment(cdoc.c1).format(config.mfd) : "")
                            if (ENT == "hlv") {
                                if (cdoc.c2 == 3) {
                                    cdoc.c71 = cdoc.c7
                                } else {
                                    cdoc.c71 = cdoc.c7 + " " + (cdoc.c1 ? moment(cdoc.c1).format(config.mfd) : "")
                                }
                                
                                if (pdoc.c2 == 3) {
                                    cdoc.c710 = pdoc.c7
                                } else {
                                    cdoc.c710 = pdoc.c7+" "+(pdoc.c1 ? moment(pdoc.c1).format(config.mfd) : "")
                                }
                            }   
                            cdoc.c30 = pdoc.c3
                            fn = cdoc.type.toLowerCase() + "adj_hlv.docx"
                            doc = cdoc
                    }
                }
                if (fn) {
                    for (let i in doc) if (util.isEmpty(doc[i])) doc[i] = ""
                    for (let row of doc.items) 
                    {
                        for (let i in row)
                        {
                            if (util.isEmpty(row[i])) row[i] = ""
                        }
                       
                        if (typeof  row.unit=="undefined") 
                        {
                            row.unit="";
                        }
                    }
                    // if (ENT == "leg") {
                    //     doc.push(repseq)
                    // }
                    const out = util.docxrender(doc, fn)
                    res.end(out, "binary")
                }
                else {
                    throw new Error(`Hóa đơn không có biên bản. \n (Invocies don't have Minutes)`)
                }
            }
        } catch (err) {
            next(err)
        }
    },
    upload: async (req, res, next) => {
        let ok = false,err
        try {
            const token = sec.decode(req), taxc = token.taxc
            const file = req.file, body = req.body, type = body.type.toString().toLowerCase(), id = body.id
            let content = file.buffer, contentb64 = content.toString('base64')
            let sql, result, binds
            sql = `REPLACE INTO s_inv_file (id, type, content) VALUES (?, ?, ?)`
            binds = [id, type, contentb64]
            result = await dbs.query(sql, binds)
            if (result[0].affectedRows) ok=true
        } catch (error) {
            err = error.message
        }
        res.json({ok, err})
    }
}
module.exports = Service 