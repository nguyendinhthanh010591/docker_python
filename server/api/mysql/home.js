"use strict"
const moment = require('moment')
const sec = require("../sec")
const dbs = require("./dbs")
const config = require("../config")
const logger4app = require("../logger4app")
const serial_usr_grant = config.serial_usr_grant
const ENT = config.ent

const Service = {
    get: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, query = req.query
            let form_combo = query.form_combo, serial_combo = query.serial_combo
            if(!form_combo) form_combo='*'
            if(!serial_combo) serial_combo='*'            
            const fd = moment(query.fd).format("YYYY-MM-DD 00:00:00"), td = moment(query.td).format("YYYY-MM-DD 23:59:59")
            const tg = query.tg, dvt = query.dvt
            if (!['1','1000','1000000'].includes(dvt))  throw new Error("Tham số ĐVT không hợp lệ \n (Invalid DVT parameter)")
            let tgc
            if (tg == 1) tgc =  `DATE_FORMAT(idt,'%d/%m/%Y')`
            else if (tg == 2) tgc = `DATE_FORMAT(idt,'%v/%x')`
            else tgc = `DATE_FORMAT(idt,'%m/%Y')`
            let sql, rs, rows, kq = {}, thsd, invs = 0, totalv = 0, vatv = 0
            //1-thsd
            let where1 = `where idt BETWEEN ? and ? and stax=? and (? ='*' or form=?) and (?='*' or serial = ?)`
            if (ENT == "hlv") {
                where1 = `where stax=? and ou = ${token.ou}`
            }

            let bind1 = [fd,td,taxc,form_combo,form_combo,serial_combo,serial_combo]
            if (serial_usr_grant) {
                where1 = `, (select taxc, type types, form forms, serial serials from s_serial ss, s_seusr ss2 where ss.id = ss2.se and ss2.usrid = ?) ss where stax=? and stax = ss.taxc and type = ss.types and form = ss.forms and serial = ss.serials`
                bind1 = [token.uid, taxc]
            }
            sql = `select 
            coalesce(sum(1),0) i0, 
            coalesce(sum(CASE status WHEN 1 THEN 1 ELSE 0 end),0) i1,
            coalesce(sum(CASE status WHEN 2 THEN 1 ELSE 0 end),0) i2,
            coalesce(sum(CASE status WHEN 3 THEN 1 ELSE 0 end),0) i3,
            coalesce(sum(CASE status WHEN 4 THEN 1 ELSE 0 end),0) i4 
            from s_inv ${where1}`
            rs = await dbs.query(sql, bind1)
            thsd = rs[0][0]
            kq.thsd = thsd
            //2-hd
            let where2 = `where idt BETWEEN ? and ? and stax=?`
            if (ENT == "hlv") {
                where2 = `where idt BETWEEN ? and ? and stax=? and ou = ${token.ou}`
            }
            let bind2 = [fd, td, taxc/*,form_combo,form_combo,serial_combo,serial_combo*/]
            if (serial_usr_grant) {
                where2 = `, (select taxc, type types, form forms, serial serials from s_serial ss, s_seusr ss2 where ss.id = ss2.se and ss2.usrid = ?) ss where idt BETWEEN ? and ? and stax=? and stax = ss.taxc and type = ss.types and form = ss.forms and serial = ss.serials`
                bind2 = [token.uid, fd, td, taxc]
            }
            sql = `select ${tgc} tg,
            sum(1) i0,
            sum(CASE status WHEN 1 THEN 1 ELSE 0 end) i1,
            sum(CASE status WHEN 2 THEN 1 ELSE 0 end) i2,
            sum(CASE status WHEN 3 THEN 1 ELSE 0 end) i3,
            sum(CASE status WHEN 4 THEN 1 ELSE 0 end) i4 
            from s_inv ${where2}
            group by ${tgc} order by ${tgc}`
            rs = await dbs.query(sql, bind2)
            rows = rs[0]
            kq.invoice = rows
            
            //Chinh lai doan lay hoa don
            where2 = `where status in (1,2,3,4) and idt BETWEEN ? and ? and stax=? and (? ='*' or form=?) and (?='*' or serial = ?)`
            if (ENT == "hlv"){
                where2 = `where status in (1,2,3,4) and idt BETWEEN ? and ? and stax=? and (? ='*' or form=?) and (?='*' or serial = ?) and ou=${token.ou}`
            }
            bind2 = [fd, td, taxc,form_combo,form_combo,serial_combo,serial_combo]
            if (serial_usr_grant) {
                where2 = `, (select taxc, type types, form forms, serial serials from s_serial ss, s_seusr ss2 where ss.id = ss2.se and ss2.usrid = ?) ss where status in (1,2,3,4) and idt BETWEEN ? and ? and stax=? and (? ='*' or form=?) and (?='*' or serial = ?) and stax = ss.taxc and type = ss.types and form = ss.forms and serial = ss.serials`
                bind2 = [token.uid, fd, td, taxc,form_combo,form_combo,serial_combo,serial_combo]
            }
            sql = `select count(*) i0 from s_inv ${where2}`
            rs = await dbs.query(sql, bind2)
            rows = rs[0]
            invs = rows[0].i0

            //3-totalv
            let where3 = `where idt BETWEEN ? and ? and stax=? and status=3 and type in ('01GTKT','02GTTT') ${["hlv"].includes(config.ent) ? '' : 'and cid is null'} 
            and (? ='*' or form=?) and (?='*' or serial = ?)`
            if (ENT == "hlv") {
                where3 = `where idt BETWEEN ? and ? and stax=? and status=3 and type in ('01GTKT','02GTTT') ${["hlv"].includes(config.ent) ? '' : 'and cid is null'} 
                and (? ='*' or form=?) and (?='*' or serial = ?) and ou = ${token.ou}`
            }
            let bind3 = [fd, td, taxc,form_combo,form_combo,serial_combo,serial_combo]
            if (serial_usr_grant) {
                where3 = `, (select taxc, type types, form forms, serial serials from s_serial ss, s_seusr ss2 where ss.id = ss2.se and ss2.usrid = ?) ss where idt BETWEEN ? and ? and stax=? and status=3 and type in ('01GTKT','02GTTT') ${["hlv"].includes(config.ent) ? '' : 'and cid is null'} 
                and (? ='*' or form=?) and (?='*' or serial = ?) and stax = ss.taxc and type = ss.types and form = ss.forms and serial = ss.serials`
                bind3 = [token.uid, fd, td, taxc,form_combo,form_combo,serial_combo,serial_combo]
            }
            sql = `select ${tgc} tg, 
            sum(coalesce(CASE WHEN adjtyp=2 THEN CAST(doc->'$.totalv' as UNSIGNED) ELSE totalv END,0))/${dvt} totalv,
            sum(coalesce(CASE WHEN adjtyp=2 THEN CAST(doc->'$.vatv' as UNSIGNED)   ELSE vatv   END,0))/${dvt} vatv 
            from s_inv ${where3}
            group by ${tgc} order by ${tgc}`
            rs = await dbs.query(sql,bind3)
            rows = rs[0]
            kq.revenue = rows
            for (const row of rows) {
                totalv += parseFloat(row.totalv)
                vatv += parseFloat(row.vatv)
            }
            kq.total = { invs: invs, totalv: totalv, vatv: vatv }
            //4-serial
            let where4 = `where taxc=?`
            let bind4 = [taxc,form_combo,form_combo,serial_combo,serial_combo]
            if (serial_usr_grant) {
                where4 = `, s_seusr ss where id = ss.se and ss.usrid = ? and taxc=?`
                bind4 = [token.uid, taxc, form_combo,form_combo,serial_combo,serial_combo]
            }
            sql = `select CONCAT(form,'-',serial) serial,max max,cur cur,CONCAT(ROUND(((max - cur)/max) *100,8),'%') rate,(max-cur) kd from s_serial ${where4} and status=1
            order by form,serial`
            rs = await dbs.query(sql, bind4)
            kq.serial = rs[0]
            res.json(kq)
        } catch (err) {
            next(err)
        }
    }
}
module.exports = Service