"use strict"
const Hashids = require("hashids/cjs")
const moment = require("moment")
const config = require("../config")
const logger4app = require("../logger4app")
const email = require("../email")
const sign = require("../sign")
const spdf = require("../spdf")
const util = require("../util")
const redis = require("../redis")
const hbs = require("../hbs")
const fcloud = require("../fcloud")
const hsm = require("../hsm")
const SEC = require("../sec")
const SERIAL = require("./serial")
const dbs = require("./dbs")
const COL = require("./col")

const user = require("./user")
const path = require("path")
const fs = require("fs")
const rsmq = require("../rsmq")
const ext = require("../ext")

const ous = require(`./ous`)
const qname=config.qname
const xlsxtemp = require("xlsx-template")
const logging = require("../logging")
const hashids = new Hashids(config.secret, 10, "abcdefghijklmnopqrstuvwxyz1234567890")
const CARR = config.CARR
const grant = config.ou_grant
const mfd = config.mfd
const dtf = config.dtf
const serial_usr_grant = config.serial_usr_grant
const UPLOAD_MINUTES = config.upload_minute
const useJobSendmail = config.useJobSendmail
const useRedisQueueSendmail  = config.useRedisQueueSendmail 
const ENT = config.ent

const renderAsync = async (json) => {
    let items = json.data.items
    for (let i of items) {
        i.price = i.price || ""
        i.quantity = i.quantity || ""
        i.total = i.total || ""
        //i.vat = i.vat || ""
    }
    let bodyBuffer = await util.jsReportRenderAttach(json)
    
    return bodyBuffer
    
}
const rbi = (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            const result = await dbs.query("select doc doc, uc uc, ou ou, c0 c0, c1 c1 , c2 c2 ,c3 c3 ,c4 c4, c5 c5, c6 c6, c7 c7,c8 c8,c9 c9 from s_inv where id=?", [id])
            const rows = result[0]
            if (rows.length == 0) reject(new Error(`Không tìm thấy Hóa đơn ${id} \n (Invoice not found ${id})`))
            let doc = rows[0].doc, uc = rows[0].uc, ou = rows[0].ou,  c0 = rows[0].c0 ,  c1 = rows[0].c1 ,  c2 = rows[0].c2 ,  c3 = rows[0].c3 , 
            c4 = rows[0].c4 ,  c5 = rows[0].c5 ,  c6 = rows[0].c6 ,  c7 = rows[0].c7 ,  c8 = rows[0].c8  ,  c9 = rows[0].c9
            doc.uc = uc
            doc.ou = ou
            doc.c0 = c0
            doc.c1 = c1
            doc.c2 = c2
            doc.c3 = c3
            doc.c4 = c4
            doc.c5 = c5
            doc.c6 = c6
            doc.c7 = c7
            doc.c8 = c8
            doc.c9 = c9
            //Lấy thông tin user của user lập hóa đơn
            let usercreate = await user.obid(uc)
            //Lấy thông tin ou của user lập hóa đơn
            let useroucreate = await ous.obid(ou)
            //Gán vào doc và trả ra
            doc.usercreate = usercreate
            doc.useroucreate = useroucreate
            doc.ouname = useroucreate.name
            //Lay thong tin so thu tu template de sau nay dung truy van template hoa don
            const org = await ous.obt(doc.stax), idx = org.temp, fn = util.fn(doc.form, idx)
            doc.tempfn = fn
            resolve(doc)
        } catch (err) {
            reject(err)
        }
    })
}
const oubi = (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            const result = await dbs.query("select name name from s_ou where id=?", [id])
            let name
            const rows = result[0]
            if (rows.length == 0) 
                name = ""
            else 
                name = rows[0].name
            resolve(name)
        } catch (err) {
            reject(err)
        }
    })
}
const chkou = async (id, token) => {
    if (grant) {
        if (token.ou !== token.u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)
        let result = await dbs.query(`select ou ou from s_inv where id=?`, [id])
        if (result[0].length > 0 && result[0][0].ou != token.u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)
    }
}
const Service = {
    getcol: async (json) => {
        try {
            let doc = JSON.parse(JSON.stringify(json))
            let configfield = {}
            let configfieldhdr = await COL.cache(doc.type,'vi')
            let configfielddtl = await COL.cachedtls(doc.type,'vi')
            configfield.configfieldhdr = configfieldhdr
            configfield.configfielddtl = configfielddtl
            doc.configfield = configfield
            let doctmp = await rbi(json.id)
            doc.org = doctmp.useroucreate
            
            return doc
        }
        catch (err) {
            throw err
        }
    },
    invdocbid: async (invid) => {
        try {
            let doc = await rbi(invid)
            return doc
        }
        catch (err) {
            logger4app.debug(`invdocbid ${invid}:`, err)
        }
    },
    insertDocTempJob: async (invid, doc, xml) => {
        try {
            if (!useRedisQueueSendmail) {
                await dbs.query(`INSERT INTO s_invdoctemp (invid, doc, status, xml) values (?,?,?,?)`, [invid, JSON.stringify(doc), 0, xml])
            } else {
                await rsmq.sendMessageAsync({ qname: qname, message: JSON.stringify({ invid: invid, status: doc.status, subject: doc.subject, bmail: doc.bmail, sendmanual: doc.sendmanual }) })
            }
            console.log(qname)
        }
        catch (err) {
            logger4app.debug(`insertDocTempJob ${invid}:`, err.message)
        }
    },
    insertDocTempJobAPI: async (invid, doc, xml) => {
        try {
            if (!config.useRedisQueueSendmailAPI) {
                await dbs.query(`INSERT INTO s_invdoctemp (invid, doc, status, xml) values (?,?,?,?)`, [invid, JSON.stringify(doc), 0, xml])
            } else {
                await rsmq.sendMessageAsync({ qname: qname, message: JSON.stringify({ invid: invid, status: doc.status, subject: doc.subject, bmail: doc.bmail, sendmanual: doc.sendmanual }) })
            }
        }
        catch (err) {
            logger4app.debug(`insertDocTempJob ${invid}:`, err.message)
        }
    },
    updateSendMailStatus: async (doc) => {
        try {
            let sql, binds
            let docupd = await rbi(doc.id)
            //Cập nhật ngày và trạng thái gửi mail, số lần gửi mail
            if (docupd.SendMailNum) {
                try {
                    docupd.SendMailNum = parseInt(docupd.SendMailNum) + 1
                } catch (err) { docupd.SendMailNum = 1 }
            } else {
                docupd.SendMailNum = 1
            }
            sql = `update s_inv set doc=JSON_SET(doc,'$.SendMailStatus',1,'$.SendMailDate',?,'$.SendMailNum',?) where id=?`
            binds = [moment(new Date()).format(dtf), docupd.SendMailNum, docupd.id]
            await dbs.query(sql, binds)
        }
        catch (err) {
            throw err
        }
    },
    checkrefno: async (doc) => {
        try {
            if (!config.refno) return []
            let str = "", sql, arr = [], binds = [], where = "where 1=1 ", mappingfield = config.refno.mappingfield, rows
            if (mappingfield.split(".")[0] == "doc") {
                rows = [doc]
            } else {
                rows = doc.items
            }
            const crr = eval(`[...new Set(rows.map(x => x.${mappingfield.split(".")[1]}))]`)
            crr.forEach(function (value, i) {
                str += `?,`
                binds.push(value)
            })
            where += ` and ref_no in (${str.slice(0, -1)}) `
            sql = `select ref_no "ref_no" from s_invd ${where}`
            const result = await dbs.query(sql, binds)
            rows = result.rows
            rows.forEach((v, i) => {
                arr.push(v.ref_no)
            })
            return arr
        } catch (err) {
            throw err
        }
    },
    checkrefdate: async (doc) => {
        try {
            if (!config.refdate) return []
            let arr = [], mappingfield = config.refdate.mappingfield, rows
            const idt = moment(doc.idt, dtf).toDate()
            if (mappingfield.split(".")[0] == "doc") {
                rows = [doc]
            } else {
                rows = doc.items
            }
            const crr = eval(`[...new Set(rows.map(x => x.${mappingfield.split(".")[1]}))]`)
            crr.forEach(function (value, i) {
                let refdate = moment(value, dtf).toDate()
                if (refdate > idt) arr.push(value)
            })
            
            return arr
        } catch (err) {
            throw err
        }
    },
    updateSendSMSStatus: async (doc, smsstatus) => {
        try {
            let sql, binds
            if ((config.sendApprSMS) || (config.sendCancSMS)) {
                //Cập nhật ngày và trạng thái gửi SMS, số lần gửi SMS
                if (doc.SendSMSNum) {
                    try {
                        doc.SendSMSNum = parseInt(doc.SendSMSNum) + 1
                    } catch (err) { doc.SendSMSNum = 1 }
                } else {
                    doc.SendSMSNum = 1
                }
                doc.SendSMSStatus = smsstatus.errnum == 0 ? 1 : -1
                doc.SendSMSDate = new Date()
                doc.SendSMSDesc = smsstatus.resdesc
                sql = `update s_inv set doc=? where id=?`
                binds = [JSON.stringify(doc), doc.id]
                await dbs.query(sql, binds)
            }
        }
        catch (err) {
            throw err
        }
    },
    saverefno: async (doc) => {
        try {
            const id =doc.id
            if (!config.refno) return 1
            let numc = 0, sql = `INSERT INTO s_invd (ref_no,inv_id) VALUES (?,?) `, mappingfield = config.refno.mappingfield, rows
            if (mappingfield.split(".")[0] == "doc") {
                rows = [doc]
            } else {
                rows = doc.items
            }
            const crr = eval(`[...new Set(rows.map(x => x.${mappingfield.split(".")[1]}))]`)
            for (let i = 0; i < crr.length; i++) {
                let value = crr[i]
                try {
                    await dbs.query(sql, [value,id])
                    numc++
                } catch (error) {
                    logger4app.error(error)
                }
            }
            return numc
        } catch (err) {
            throw err
        }
    },
    delrefno: async (doc) => {
        try {
            if (!config.refno) return 1
            let numc = 0, sql = `DELETE from s_invd where ref_no = ? `, mappingfield = config.refno.mappingfield, rows
            if (mappingfield.split(".")[0] == "doc") {
                rows = [doc]
            } else {
                rows = doc.items
            }
            const crr = eval(`[...new Set(rows.map(x => x.${mappingfield.split(".")[1]}))]`)
            for (let i = 0; i < crr.length; i++) {
                let value = crr[i]
                try {
                    await dbs.query(sql, [value])
                    numc++
                } catch (error) {
                    logger4app.error(error)
                }
            }
            return numc
        } catch (err) {
            throw err
        }
    },
    sendToApprover: async (doc) => {
        try {
            let doctomail = doc//await rbi(doc.id)
            let mail = ""
            if(doctomail.form.includes("01GTKT"))
                mail = config.To01GTKTApproverMail
            else if(doctomail.form.includes("03XKNB"))
                mail = config.To03XKNBApproverMail
                
            const arr = mail.trim().split(/[ ,;]+/).map(e => e.trim())
            
            let subject = config.subjectApproverMail
            let html = config.htmlApproverMail
            html = html.replace("#seq#", `${doctomail.form} - ${doctomail.serial} - ${doctomail.seq}`)
            html = html.replace("#bname#", `${(doctomail.bname) ? doctomail.bname : doctomail.buyer}`)
            html = html.replace("#total#", `${doctomail.total} ${doctomail.curr}`)
            html = html.replace("#uc#", `${doctomail.uc}`)

            const content = { from: config.mail, to: arr, subject: subject, html: html }
            await rsmq.sendMessageAsync({ qname: config.qname, message: JSON.stringify(content) })
        }
        catch (err) {
            cononsole.log(err)
        }
    },
    count12: async (req, res, next) => {
        try {
            const token = SEC.decode(req), query = req.query
            const sql = `select count(*) rc from s_inv where stax=? and form=? and serial=? and status in (1, 2)`
            const result = await dbs.query(sql, [token.taxc, query.form, query.serial])
            const rows = result[0]
            res.json(rows[0])
        }
        catch (err) {
            next(err)
        }
    },
    seqget: async (req, res, next) => {
        try {
            const token = SEC.decode(req), query = req.query, filter = JSON.parse(query.filter), start = query.start ? query.start : 0, count = query.count ? query.count : 10
            let td = moment(filter.td).endOf("day").format("YYYY-MM-DD HH:mm:ss")
            let where = " where stax=? and form=? and serial=? and idt<=? and status=1"
            let binds = [token.taxc, filter.form, filter.serial,td]
            let sql = `select id id,idt idt,status status,ou ou,type type,form form,serial serial,seq seq,btax btax,bname bname,buyer buyer,baddr baddr,sumv sumv,vatv vatv,totalv totalv,sum "sum",vat "vat",total "total",adjdes adjdes,uc uc,ic ic from s_inv ${where} order by idt,id LIMIT ${count} OFFSET ${start}`
            let result = await dbs.query(sql, binds)
            let ret = { data: result[0], pos: start }
            if (start == 0) {
                sql = `select count(*) total from s_inv ${where}`
                result = await dbs.query(sql, binds)
                ret.total_count = result[0][0].total
            }
            res.json(ret)
        } catch (error) {
            next(error)
        }
    },
    seqpost: async (req, res, next) => {
        let key
        try {
            const token = SEC.decode(req)
            const wait = 60, taxc = token.taxc, body = req.body, form = body.form, serial = body.serial, idt = moment(body.idt).format("YYYY-MM-DD HH:mm:ss"), seqList = body.seqList
            key = `APPR.${taxc}.${form}.${serial}`
            const val = await redis.get(key)
            if (val == "1") throw new Error(`Đang cấp số cho HĐ MST=${taxc} mẫu số=${form} ký hiệu=${serial}. Bạn hãy đợi ${wait}s \n Sequencing for invoice Tax.No=${taxc} Form=${form} Serial=${serial}. Should be waiting for ${wait}s !`)
            await redis.set(key, "1", "EX", wait)
            let sql, binds
            if(seqList.length > 0) {
                // cấp 1 hoặc lỗ chỗ trong 1 ngày
                sql = `select id id,pid pid,ou ou,adjtyp adjtyp,idt idt,sec sec from s_inv where stax=:stax and form=:form and serial=:serial and status=1 and id IN (?) order by idt,id`
                binds = [taxc, form, serial,seqList]
            } else {
                // cấp theo dải bé hơn ngày hiện tại
                sql = `select id id,pid pid,ou ou,adjtyp adjtyp,idt idt,sec sec from s_inv where idt<=? and stax=? and form=? and serial=? and status=1 order by idt,id`
                binds = [idt, taxc, form, serial]
            }
            const result = await dbs.query(sql, binds)
            const rows = result[0]
            const sup = `update s_inv set doc=JSON_SET(doc,'$.status',2,'$.seq',?) where id=? and stax=? and status=1`, pup = `update s_inv set cde=?,doc=JSON_SET(doc,'$.cde',?) where id=?`
            let count = 0
            for (const row of rows) {
                let seq, s7, rs, id, pid
                try {
                    id = row.id
                    seq = await SERIAL.sequence(taxc, form, serial,row.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    rs = await dbs.query(sup, [s7, id, taxc])
                    
                    if (rs[0].affectedRows > 0) {
                        count++
                        pid = row.pid
                        if (!util.isEmpty(pid)) {
                            try {
                                const cde = `${(row.adjtyp == 1 ? "Bị thay thế" : "Bị điều chỉnh")} bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${moment(row.idt).format(mfd)} mã ${row.sec}`
                                await dbs.query(pup, [cde, cde, pid])
                                
                            } catch (err) { }
                        }
                    }
                    else throw new Error(`Không cấp số cho hóa đơn ${id} \n (Do not issue number to the invoice: ${id})`)
                    sql = `select doc doc from s_inv where id=?`
                    const rtolog = await rbi(id)
                    if(config.ent == "hsy") {
                        Service.sendToApprover(rtolog)
                    }
                    const sSysLogs = { fnc_id: 'inv_seqpost', src: config.SRC_LOGGING_DEFAULT, dtl: `Cấp số hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(rtolog)};
                    logging.ins(req,sSysLogs,next) 
                } catch (err) {
                    if (!((err.errno == 1062) && (String(err.message).toUpperCase().indexOf("S_INV_UID_01") >= 0))) await SERIAL.err(taxc, form, serial, seq)
                    throw err
                }
            }
            res.json(count)
        }
        catch (err) {
            next(err)
        }
        finally {
            if (!util.isEmpty(key)) await redis.del(key)
        }
    },
    apprall: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, org = await ous.obt(taxc), signtype = org.sign
            if (signtype !== 2) throw new Error("Chỉ hỗ trợ kiểu ký bằng file \n (Only support sign by file type)")
            const iwh = await Service.iwh(req, true)
            const sql = `select id id,doc doc,bmail bmail from s_inv ${iwh.where}`
            const result = await dbs.query(sql, iwh.binds)
            const rows = result[0]
            if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
            if (rows.length > config.MAX_ROW_APPROVE) throw new Error(`Dữ liệu ký duyệt vượt quá ${config.MAX_ROW_APPROVE} cho phép \n (Signing data exceeds ${config.MAX_ROW_APPROVE} characters for allowed)`)
            let pwd, ca
            //hunglq chinh sua thong bao loi
            try {
                pwd = util.decrypt(org.pwd)
                ca = await sign.ca(taxc, pwd)
            } catch (err) {
                throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)')
            }
            let count = 0
            const sql2 = `update s_inv set doc=JSON_SET(doc,'$.status',3),xml=?,dt=? where id=? and status=2`
            for (const row of rows) {
                let doc = row.doc
                if (config.ent == 'ssi') {
                    doc.totalv = util.isNumber(doc.totalv) ? Math.round(doc.totalv) : doc.totalv
                    doc.sumv = util.isNumber(doc.sumv) ? Math.round(doc.sumv) : doc.sumv
                    doc.vatv = util.isNumber(doc.vatv) ? Math.round(doc.vatv) : doc.vatv
                }
                const id = row.id, xml = util.signature(sign.sign(ca, util.j2x((await Service.getcol(doc)), id), id), doc.idt), result2 = await dbs.query(sql2, [xml, new Date(), id])
                if (result2[0].affectedRows > 0) {
                    doc.status = 3
                    if (useJobSendmail) {
                        await Service.insertDocTempJob(id, doc, xml)
                    } else {
                        if (!util.isEmpty(doc.bmail)) {
                            let mailstatus = await email.rsmqmail(doc)
                            if (mailstatus) await Service.updateSendMailStatus(doc)
                        }
                    }
                    count++
                }
                const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                logging.ins(req,sSysLogs,next) 
            }
            res.json(count)
        }
        catch (err) {
            next(err)
        }
    },
    apprpost: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const taxc = token.taxc, body = req.body, ids = body.ids
            let sql, result
            sql = `select id id,doc doc,bmail bmail from s_inv where id in (?) and stax=? and status=2`
            result = await dbs.query(sql, [ids, taxc])
            const rows = result[0]
            if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
            if (rows.length > config.MAX_ROW_APPROVE) throw new Error(`Dữ liệu ký duyệt vượt quá ${config.MAX_ROW_APPROVE} cho phép \n (Signing data exceeds ${config.MAX_ROW_APPROVE} characters for allowed)`)
            const org = await ous.obt(taxc), signtype = org.sign
            if (signtype == 1) {
                let arr = []
                for (const row of rows) {
                    let doc = row.doc, id = row.id
                    if (config.ent == 'ssi') {
                        doc.totalv = util.isNumber(doc.totalv) ? Math.round(doc.totalv) : doc.totalv
                        doc.sumv = util.isNumber(doc.sumv) ? Math.round(doc.sumv) : doc.sumv
                        doc.vatv = util.isNumber(doc.vatv) ? Math.round(doc.vatv) : doc.vatv
                    }
                    let xml = util.j2x((await Service.getcol(doc)), id)
                    // push {inc: id,...} thay vi {id: id,...} vì con ký anh Đại đang để là inc thay vì id CLOUD
                    arr.push({ inc: id, xml: Buffer.from(xml).toString("base64") })
                }
                res.json({ mst: taxc, date: new Date(), ref: config.PATH_XML, arr: arr })
            }
            else {
                let count = 0
                sql = `update s_inv a set a.doc=JSON_SET(doc,'$.status',3),a.xml=?,a.dt=? where a.id=? and a.stax=? and a.status=2`
                if (signtype == 2) {
                    //hunglq chinh sua thong bao loi
                    let ca, pwd
                    try {
                        pwd = util.decrypt(org.pwd)
                        ca = await sign.ca(taxc, pwd)
                    } catch (err) {
                        throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)')
                    }
                    for (const row of rows) {
                        let doc = row.doc

                        if (config.ent == 'ssi') {
                            doc.totalv = util.isNumber(doc.totalv) ? Math.round(doc.totalv) : doc.totalv
                            doc.sumv = util.isNumber(doc.sumv) ? Math.round(doc.sumv) : doc.sumv
                            doc.vatv = util.isNumber(doc.vatv) ? Math.round(doc.vatv) : doc.vatv
                        }
                        const id = row.id, xml = util.signature(sign.sign(ca, util.j2x((await Service.getcol(doc)), id), id), doc.idt)
                        await dbs.query(sql, [xml, new Date(), id, taxc])
                        doc.status = 3
                        if (useJobSendmail) {
                            await Service.insertDocTempJob(id, doc, xml)
                        } else {
                            if (!util.isEmpty(doc.bmail)) {
                                let mailstatus = await email.rsmqmail(doc)
                                if (mailstatus) await Service.updateSendMailStatus(doc)
                            }
                        }
                        const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                        logging.ins(req,sSysLogs,next) 
                        count++
                    }
                }
                else {
                    //hunglq chinh sua thong bao loi
                    let kqs, usr, pwd
                    try {
                        if (signtype == 3) {
                            usr = org.usr
                            pwd = util.decrypt(org.pwd)
                            kqs = await fcloud.xml(usr, pwd, taxc, rows)
                        }
                        else kqs = await hsm.xml(rows)
                    } catch (err) {
                        throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)')
                    }
                    for (const row of kqs) {
                        const id = row.id, xml = util.signature(row.xml, doc.idt)
                        let doc = row.doc
                        doc.status = 3
                        await dbs.query(sql, [xml, new Date(), id, taxc])
                        if (useJobSendmail) {
                            await Service.insertDocTempJob(id, doc, xml)
                        } else {
                            if (!util.isEmpty(doc.bmail)) {
                                let mailstatus = await email.rsmqmail(doc)
                                if (mailstatus) await Service.updateSendMailStatus(doc)
                            }
                        }
                        const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                        logging.ins(req,sSysLogs,next) 
                        count++
                    }
                }
                res.json(count)
            }
        }
        catch (err) {
            next(err)
        }
    },
    apprput: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, body = req.body, incs = body.incs, signs = body.signs
            let str = "", binds = []
            for (const row of incs) {
                str += `?,`
                binds.push(row)
            }
            binds.push(taxc)
            let sql, result, rows, count = 0
            sql = `select id id,doc doc,bmail bmail from s_inv where id in (${str.slice(0, -1)}) and stax=? and status=2 `
            result = await dbs.query(sql, binds)
            rows = result[0]
            if (rows.length > 0) {
                sql = `update s_inv set doc=JSON_SET(doc,'$.status',3),xml=?,dt=? where id=? and stax=? and status=2`
                for (let row of rows) {
					let doc = row.doc
                    const id = row.id, sign = signs.find(item => item.inc === id)
                    if (sign && sign.xml) {
                        const xml = util.signature(Buffer.from(sign.xml, "base64").toString(), doc.idt)
                        await dbs.query(sql, [xml, new Date(), id, taxc])
                        row.doc.status = 3
                        if (useJobSendmail) {
                            await Service.insertDocTempJob(id, row.doc, xml)
                        } else {
                            if (!util.isEmpty(row.bmail)) { 
                                await email.rsmqmail(row.doc)
                                await Service.updateSendMailStatus(row.doc)
                            }
                        }
                        const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                        logging.ins(req,sSysLogs,next) 
                        count++
                    }
                }
            }
            res.json(count)
        }
        catch (err) {
            next(err)
        }
    },
    iwh: async (req, all, decodereq) => {
        const token = (!decodereq) ? SEC.decode(req) : req.token, query = req.query, filter = JSON.parse(query.filter), sort = query.sort
        let keys = Object.keys(filter), kar = ["ou", "status", "type", "form", "serial", "paym", "curr", "cvt"]
        let val, ext = "", extxls = "", cols, where = `where idt between ? and ? and stax=?`, binds = [new Date(filter.fd), new Date(moment(filter.td).endOf("day")), token.taxc], order = " order by id desc"
        if (serial_usr_grant) {
            where = `, (select taxc, type "types", form "forms", serial "serials" from s_serial ss, s_seusr ss2 where ss.id = ss2.se and ss2.usrid = ?) ss where idt between ? and ? and stax=?`
            binds = [token.uid, new Date(filter.fd), new Date(moment(filter.td).endOf("day")), token.taxc]
        }
        if (grant && token.ou == token.u) {
            where += all ? ` and ou in (select cid from s_ou_tree where pid=?)` : ` and ou=?`
            // AND exists (SELECT 1 FROM s_ou_tree o where o.cid = ou and o.cid=?)
            binds.push(token.u)
        }
        if (keys.includes("type")) {
            cols = await COL.cache(filter["type"])
            if (cols.length > 0) {
                let exts = []
                let extsxls = []
                for (const col of cols) {
                    let cid = col.id
                    exts.push(`${cid} ${cid}`)
                    //Xu ly rieng cho excel neu la truong date thi format lai
                    if (col.view == "datepicker")
                        extsxls.push(`DATE_FORMAT(STR_TO_DATE(${cid}, '%Y-%m-%d'),'%d/%m/%Y') ${cid}`)
                    else
                        extsxls.push(`${cid} ${cid}`)
                }
                ext = `,${exts.join()}`
                extxls = `,${extsxls.join()}`
            }
        }
        for (const key of kar) {
            if (keys.includes(key)) {
                where += ` and ${key}=?`
                binds.push(filter[key])
            }
        }
        kar.push("fd", "td")
        for (const key of kar) {
            delete keys[key]
        }
        for (const key of keys) {
            val = filter[key]
            if (val && !kar.includes(key)) {
                switch (key) {
                    case "seq":
                        where += ` and seq like ?`
                        binds.push(`%${val}`)
                        break
                    case "btax":
                        where += ` and btax like ?`
                        binds.push(`%${val}%`)
                        break
                    case "bacc":
                        where += ` and bacc like ?`
                        binds.push(`%${val}%`)
                        break
                    case "user":
                        where += ` and uc like ?`
                        binds.push(`%${val}%`)
                        break
                    case "vat":
                        let catobj = JSON.parse(JSON.stringify(config.CATTAX))
                        let objtax = catobj.find(x => x.vatCode == val)
                        where += ` and (JSON_CONTAINS(doc->'$.tax[*].vrt',?)=1 or JSON_CONTAINS(doc->'$.tax[*].vrt',?)=1)`
                        binds.push(`"${String(objtax.vrt)}"`)
                        binds.push(`${String(objtax.vrt)}`)
                        break
                    case "adjtyp":
                        switch (val) {
                            case "3":
                                where += ` and cid is not null and cde like ?`
                                binds.push("Bị thay thế%")
                                break
                            case "4":
                                where += ` and cid is not null and cde like ?`
                                binds.push("Bị điều chỉnh%")
                                break
                            default:
                                where += ` and adjtyp=?`
                                binds.push(val)
                                break
                        }
                        break
                    default:
                        let b = true
                        if (CARR.includes(key) && cols) {
                            let obj = cols.find(x => x.id == key)
                            if (typeof obj !== "undefined") {
                                const view = obj.view
                                if (view == "datepicker") {
                                    b = false
                                    where += ` and date(${key})=?`
                                    binds.push(new Date(val))
                                }
                                else if (view == "multicombo") {
                                    b = false
                                    where += ` and ${key} in (?)`
                                    binds.push(val.split(","))
                                }
                                else if (view == "combo") {
                                    b = false
                                    where += ` and ${key}=?`
                                    binds.push(val)
                                }
                                else if (view == "text") {
                                    if (obj.type == "number") {
                                        b = false
                                        where += ` and ${key} like ?`
                                        binds.push(`%${val}%`)
                                    }
                                }
                            }
                        }
                        if (b) {
                            where += ` and upper(${key}) like ?`
                            binds.push(`%${val.toString().toUpperCase()}%`)
                        }
                        break
                }
            }
        }
        if (serial_usr_grant) {
            where += ` and stax = ss.taxc and type = ss.types and form = ss.forms and serial = ss.serials`
        }
        if (sort) {
            Object.keys(sort).forEach(key => {
                order = `order by ${key} ${sort[key]}`
            })
        }
        else order = "order by idt desc,ABS(id) desc, type, form, serial, seq"
        return { where: where, binds: binds, ext: ext, order: order, extxls: extxls }
    },
    xls: async (req, res, next) => {
        try {
            const fn = "temp/KQTCHD.xlsx", query = req.query, filter = JSON.parse(query.filter)
            let json, rows
            const iwh = await Service.iwh(req, true)
            let cols = await COL.cacheLang(filter["type"], req.query.lang)
            let colhds = req.query.colhd.split(",")
            let extsh = []
            for (const col of cols) {
                let clbl = col.label
                extsh.push(clbl)
            }
            
            //ext = `,${exts.join()}`
            const sqlc = `select count(*) count from s_inv ${iwh.where} ${iwh.order} `
            const resultc = await dbs.query(sqlc, iwh.binds)
            rows = resultc[0]
            if(rows[0].count >  config.MAX_ROW_EXCEL_EXPORT){
                throw new Error(`Dữ liệu quá ${config.MAX_ROW_EXCEL_EXPORT} dòng bạn cần thay đổi tham số tìm kiếm để hạn chế dữ liệu \n (Data exceeds ${config.MAX_ROW_EXCEL_EXPORT} lines you need to change the search parameters to restrict data)`)
            }
            const sql = `select id id,sec sec,ic ic,uc uc,DATE_FORMAT(idt,'%d/%m/%Y') idt,case when status = 1 then 'Chờ cấp số'  when status = 2 then 'Chờ duyệt'  when status = 3 then 'Đã duyệt' when status = 4 then 'Đã hủy'  when status = 5 then 'Đã gửi'  when status = 6 then 'Chờ hủy' end status,ou ou,type type,form form,serial serial,seq seq,btax btax,bname bname,buyer buyer,baddr baddr,note note,sumv sumv,vatv vatv,totalv totalv,sum sum,vat vat,total total,adjdes adjdes,cde cde,bmail bmail,curr curr,sname sname${iwh.extxls} from s_inv ${iwh.where} ${iwh.order} `
            const result = await dbs.query(sql, iwh.binds)
            rows = result[0]
            //Them ham tra gia tri theo mang de hien thi tương ứng voi tung cot
            for (const row of rows) {
                row.sumv = parseInt(row.sumv)
                if (ENT == "lge") {row.vatv = parseInt(row.vatv)}
                row.totalv = parseInt(row.totalv)
                try {
                    row.ou = await oubi(row.ou)
                }
                catch (err) {

                }
                let extsval = []
                for (const col of cols) {
                    extsval.push(row[col.id])
                }
                row.extsval = extsval
            }
            json = { table: rows, extsh:extsh, colhds: colhds }
            const file = path.join(__dirname, "..", "..", "..", fn)
            const xlsx = fs.readFileSync(file)
            const template = new xlsxtemp(xlsx)
            template.substitute(1, json)
            res.end(template.generate(), "binary")
        }
        catch (err) {
           logger4app.debug("excel"+ err)
           
            next(err)
          
           
        }
    },
    appget: async (req, res, next) => {
        try {
            const query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 10, iwh = await Service.iwh(req, false)
            let sql, result, ret
            sql = `select 0 chk,id id,sec sec,ic ic,uc uc,curr curr,pid pid,cid cid,idt idt,status status,ou ou,type type,form form,serial serial,seq seq,btax btax,bname bname,buyer buyer,baddr baddr,note note,sumv sumv,vatv vatv,totalv totalv,sum sum,vat vat,total total,adjdes adjdes,adjtyp adjtyp,cde cde,cvt cvt,bmail bmail${iwh.ext} from s_inv ${iwh.where} ${iwh.order} LIMIT ${count} OFFSET ${start}`
            result = await dbs.query(sql, iwh.binds)
            ret = { data: result[0], pos: start }
            if (start == 0) {
                sql = `select count(*) total from s_inv ${iwh.where}`
                result = await dbs.query(sql, iwh.binds)
                ret.total_count = result[0][0].total
            }
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    get: async (req, res, next) => {
        try {
            //console.time("time")
            const query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 10, iwh = await Service.iwh(req, true)
            let sql, result, ret
            sql = `select 0 chk,id id,sec sec,ic ic,uc uc,curr curr,pid pid,cid cid,idt idt,status status,ou ou,type type,form form,serial serial,seq seq,btax btax,bname bname,buyer buyer,baddr baddr,note note,sumv sumv,vatv vatv,totalv totalv,sum sum,vat vat,total total,adjdes adjdes,adjtyp adjtyp,cde cde,cvt cvt,bmail bmail${iwh.ext}, error_msg_van from s_inv ${iwh.where} ${iwh.order} LIMIT ${count} OFFSET ${start}`
            result = await dbs.query(sql, iwh.binds)
            ret = { data: result[0], pos: start }
            if (start == 0) {
                sql = `select count(*) total from s_inv ${iwh.where}`
                result = await dbs.query(sql, iwh.binds)
                ret.total_count = result[0][0].total
            }
            //console.timeEnd("time")
            const sSysLogs = { fnc_id: 'inv_get', src: config.SRC_LOGGING_DEFAULT, dtl: `Tra cứu hóa đơn ${query.filter}`, msg_id: "", doc: query.filter };
            logging.ins(req, sSysLogs, next) 
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    api_search: async (req, res, next) => {
        try {
            //console.time("time")
            const query = req.body
            let c0 = query.policy_number,sql,result
            logger4app.debug('_______________________api_search runing start_________________'+ new Date())
            sql = `select c0,form form,serial serial,seq seq,DATE_FORMAT(idt, "%d-%m-%Y") idt,sec sec,status status,total total,id id from s_inv where c0=? and status in (3,4)`
            result = await dbs.query(sql, [c0])
           
             logger4app.debug('_______________________api_search stop_________________'+ new Date())
            res.json(result[0])
        }
        catch (err) {
            next(err)
        }
    },
    
    api_download: async (req, res, next) => {
        try {
            //console.time("time")
            const query = req.body
            let id = query.id,sql,result,doc
            logger4app.debug('_______________________api_download runing start_________________'+ new Date())
            sql = `select doc doc  from s_inv where id=? and status in (3,4)`
            result = await dbs.query(sql, [Number(id)])
            if (result[0].length > 0) {
                doc = result[0][0].doc
                const status = doc.status,org = await ous.obt(doc.stax), idx = org.temp
                if (!([3, 4].includes(status))) doc.sec = ""
                const fn = util.fn(doc.form, idx), tmp = await util.template(fn)
                let obj = { status: status, doc: doc, tmp: tmp }
                let reqjsr = ext.createRequest(obj)
                let pdf = await renderAsync(reqjsr)
                res.json({status:1,data:pdf.toString("base64")})
            }else{
                res.json('')
            }
            //Create PDF
            res.json(pdf.toString("base64"))
             logger4app.debug('_______________________api_download stop_________________'+ new Date())
            
        }
        catch (err) {
            next(err)
        }
    },
    signget: async (req, res, next) => {
        try {
            const token = SEC.decode(req), u = token.u
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const id = req.query.id, taxc = token.taxc
            let sql, result, binds = [id, taxc]
            sql = `select id id,doc doc,bmail bmail from s_inv where id=? and stax=? and status=2 `
            if (grant) {
                sql += ' and ou=?'
                binds.push(u)
            }
            result = await dbs.query(sql, binds)
            const rows = result[0]
            if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
            const org = await ous.obt(taxc), signtype = org.sign, row = rows[0]
            let xml, doc = row.doc
            if (signtype == 1) {
                const usr = org.usr, pwd = util.decrypt(org.pwd)
                xml = util.j2x((await Service.getcol(doc)), id)
                const arr = [{ inc: id, xml: Buffer.from(xml).toString("base64") }]
                res.json({ lib: usr, sid: 0, pin: pwd, mst: taxc, date: new Date(), ref: config.PATH_XML, arr: arr })
            }
            else {
                if (signtype == 2) {
                    const pwd = util.decrypt(org.pwd)
                    const ca = await sign.ca(taxc, pwd)
                    xml = util.signature(sign.sign(ca, util.j2x((await Service.getcol(doc)), id), id), doc.idt)
                }
                else if (signtype == 3) {
                    const usr = org.usr, pwd = util.decrypt(org.pwd)
                    const kqs = await fcloud.xml(usr, pwd, taxc, rows), kq = kqs[0]
                    xml = kq.xml
                }
                else {
                    const kqs = await hsm.xml(rows), kq = kqs[0]
                    xml = util.signature(kq.xml, doc.idt)
                }
                await dbs.query(`update s_inv set doc=JSON_SET(doc,'$.status',3),xml=?,dt=? where id=? and stax=? and status=2`, [xml, new Date(), id, taxc])
                doc.status = 3
                if (useJobSendmail) {
                    await Service.insertDocTempJob(doc.id, doc, xml)
                } else {
                    if (!util.isEmpty(doc.bmail)) {
                        let mailstatus = await email.rsmqmail(doc)
                        if (mailstatus) await Service.updateSendMailStatus(doc)
                    }
                }
                const sSysLogs = { fnc_id: 'inv_signget', src: config.SRC_LOGGING_DEFAULT, dtl: `Ký số hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                logging.ins(req,sSysLogs,next)
                res.json(1)
            }

        }
        catch (err) {
            next(err)
        }
    },
    signput: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, body = req.body, signs = body.signs
            let sign = signs[0], id = sign.inc, xml = Buffer.from(sign.xml, "base64").toString()
            const result = await dbs.query(`select bmail bmail,doc doc from s_inv where id=? and stax=? and status=2`, [id, taxc])
            const rows = result[0]
            if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
            let row = rows[0]
            let doc = JSON.parse(row.doc)
            xml = util.signature(xml, doc.idt) 
            await dbs.query(`update s_inv set doc=JSON_SET(doc,'$.status',3),xml=?,dt=? where id=? and stax=? and status=2`, [xml, new Date(), id, taxc])
            row.doc.status = 3
            if (useJobSendmail) {
                await Service.insertDocTempJob(id, row.doc, xml)
            } else {
                if (!util.isEmpty(row.bmail)) {
                    await email.rsmqmail(row.doc)
                    await Service.updateSendMailStatus(row.doc)
                }
            }
            const sSysLogs = { fnc_id: 'inv_signget', src: config.SRC_LOGGING_DEFAULT, dtl: `Ký số hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
            logging.ins(req,sSysLogs,next)
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },
    put: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, id = req.params.id
            await chkou(id, token)
            await Service.delrefno(body)
            const hdb = await Service.checkrefno(body)
            if (hdb.length > 0) throw new Error(`Số bút toán đã tồn tại: ${hdb.join()} \n(Entry number already exists: ${hdb.join()})`)
            
            const checkrefdate = await Service.checkrefdate(body)
            if (checkrefdate.length > 0) throw new Error(`Ngày bút toán lớn hơn ngày hóa đơn: ${checkrefdate.join(',')} \n(Ref Date is greater than Invoice Date: ${checkrefdate.join(',')})`)
            
            await dbs.query(`update s_inv set doc=?,idt=? where id=? and status=?`, [JSON.stringify(body),new Date(body.idt), id, body.status])
            await Service.saverefno(body)
            const sSysLogs = { fnc_id: 'inv_put', src: config.SRC_LOGGING_DEFAULT, dtl: `Sửa hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(body) };
            logging.ins(req,sSysLogs,next) 
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },
    ins: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const taxc = token.taxc, uid = token.uid, ou = token.ou, org = await ous.obt(taxc), AUTO = (org.seq === 1), body = req.body
            let invs = body.invs, sql = `insert into s_inv set ?`
            //Thêm đoạn sort lại danh sách hóa đơn trước khi cấp 
            if (AUTO) {
                //Ghep dữ liệu cần sort
                for (let inv of invs) {
                    inv.sortprop = `#${(moment(inv.idt, dtf).format('YYYYMMDD'))}#${inv.ic}`
                }
                invs = util.sortobj(invs, 'sortprop', 0)
            }
            for (let inv of invs) {
                let ic, id, sec, form, serial, type, pid, idt, seq, s7, rs
                try {
                    form = inv.form
                    serial = inv.serial
                    type = form.substr(0, 6)
                    ic = inv.ic
                    inv.sid = ic
                    pid = inv.pid
                    idt = moment(inv.idt, dtf)
                    id = await redis.incr("INC")
                    sec = util.generateSecCode(config.FNC_GEN_SEC,id)
                    inv.id = id
                    inv.sec = sec
                    inv.type = type
                    inv.name = util.tenhd(type)
                    inv.stax = taxc
                    inv.sname = org.name
                    inv.saddr = org.addr
                    inv.smail = org.mail
                    inv.stel = org.tel
                    inv.taxo = org.taxo
                    inv.sacc = org.acc
                    inv.sbank = org.bank
                    const ouob = await ous.obid(ou)
                    inv.systemCode = ouob.code
                    if (AUTO) {
                        const sqlcheck = `select count(*) rcount from s_inv where stax=? and (status in (2,3,5,6) or (status = 4 and xml is not null)) and form=? and serial=? and idt>?`
                        const resultcheck = await dbs.query(sqlcheck, [taxc, form, serial, (moment(idt.toDate()).endOf("day")).toDate()])
                        const rowscheck = resultcheck[0]
                        if (rowscheck[0].rcount > 0) throw new Error("Đã tồn tại hóa đơn được cấp số có ngày hóa đơn lớn hơn hóa đơn hiện tại \n (A numbered invoice already exists that has a larger invoice date than the current invoice)")
                        seq = await SERIAL.sequence(taxc, form, serial,inv.idt)
                        s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                        inv.seq = s7
                        inv.status = 2
                    }
                    else {
                        s7 = "......."
                        inv.status = 1
                    }
                    rs = await dbs.query(sql, { id: id, pid: pid, sec: sec, ic: ic, idt: idt.toDate(), ou: ou, uc: uid, doc: JSON.stringify(inv) })
                    if (rs[0].affectedRows > 0) {
                        if (pid) {
                            try {
                                const cde = `Bị thay thế bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${idt.format(mfd)} mã ${sec}`
                                let cdt = moment(idt.toDate()).format(dtf)
                                await dbs.query(`update s_inv set doc=JSON_SET(doc,'$.status',4,'$.cde',?,'$.cdt',?),cid=?,cde=? where id=? and status=3`, [cde, cdt, id, cde, pid])
                            } catch (error) {
                                logger4app.debug(error)
                            }
                        }
                    }
                    else throw new Error(`Không cấp số cho hóa đơn ${id} \n (Do not issue number to the invoice: ${id}})`)
                    const sSysLogs = { fnc_id: 'xls_upl', src: config.SRC_LOGGING_DEFAULT, dtl: 'Đọc hóa đơn từ excel', msg_id: id,doc: JSON.stringify(inv) };
                    logging.ins(req, sSysLogs, next)
                } catch (e) {
                    if (AUTO) {
                        if (!((e.errno == 1062) && (String(e.message).toUpperCase().indexOf("S_INV_UID_01") >= 0))) await SERIAL.err(taxc, form, serial, seq)
                    }
                    throw e
                }
            }
            res.json(invs.length)
        }
        catch (err) {
            next(err)
        }
    },
    upds: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const body = req.body, invs = body.invs
            for (let inv of invs) {
                let pid, rs
                try {
                    pid = inv.pid
                    delete inv["pid"]
                    let doc = await rbi(pid)
                    let idt = doc.idt
                    for (let i in inv) {
                        doc[i] = inv[i]
                    }
                    doc.idt = idt
                    await dbs.query(`update s_inv set doc=? where id=? and status=2`, [JSON.stringify(doc), pid])
                    const sSysLogs = { fnc_id: 'inv_adj', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật hóa đơn id ${pid}`, msg_id: pid, doc: JSON.stringify(doc) };
                    logging.ins(req,sSysLogs,next)
                } catch (e) {
                    throw e
                }
            }
            res.json(invs.length)
        }
        catch (err) {
            next(err)
        }
    },
    wcan: async (req, res, next) => {
        const token = SEC.decode(req), taxc = token.taxc, body = req.body, invs = body.invs, HLV = (config.ent === "hlv")
        let sql = `update s_inv set doc=JSON_SET(doc,'$.status',6,'$.cancel',JSON_OBJECT('typ',3,'ref',?,'rdt',?,'rea',?)) where id=? and stax=? and status`
        if (HLV) sql += '=3'
        else sql += ' in (2,3)'
        for (const item of invs) {
            try {
                const result = await dbs.query(sql, [item.ref, item.rdt, item.rea, item.id, taxc])
                if (result[0].affectedRows > 0) {
                    const pid = item.pid
                    if (pid) await dbs.query(`update s_inv set cid=null,cde=null where id=?`, [pid])
                    item.error = 'OK'
                }
                else item.error = 'NOT OK'
            }
            catch (e) {
                item.error = e.message
            }
        }
        res.json({ save: true, data: invs })
    },
    sav: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body
            if (grant && token.ou !== token.u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)
            const hdb = await Service.checkrefno(body)
            if (hdb.length > 0) throw new Error(`Số bút toán đã tồn tại: ${hdb.join()} \n(Entry number already exists: ${hdb.join()})`)

            const checkrefdate = await Service.checkrefdate(body)
            if (checkrefdate.length > 0) throw new Error(`Ngày bút toán lớn hơn ngày hóa đơn: ${checkrefdate.join(',')} \n(Ref Date is greater than Invoice Date: ${checkrefdate.join(',')})`)
            
            const taxc = token.taxc, uid = token.uid, ou = token.ou, org = await ous.obt(taxc), form = body.form, serial = body.serial, id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC,id)
            let seq, s7,result,row
            const dt = moment(body.idt),  eod = dt.endOf('day').toDate(), dts = dt.format(mfd),dtsc = dt.format('YYYYMMDD'),idtc=(moment(new Date())).format('YYYYMMDD')
            try {
                if (dtsc!= idtc){
                    result = await dbs.query(`select count(*) rc from s_inv where idt>? and form=? and serial=? and (status in (2,3,5,6) or (status = 4 and xml is not null))`, [eod, form, serial])
                    row = result[0][0]
                    if (row.rc > 0) throw new Error(`Tồn tại HĐ có ngày lớn hơn ${moment(body.idt).format('DD/MM/YYYY')} và đã cấp số \n (Invoice has a larger date  ${moment(body.idt).format('DD/MM/YYYY')} and issued a number)`)
                }
                body.id = id
                body.sec = sec
                body.stax = org.taxc
                body.sname = org.name
                body.saddr = org.addr
                body.smail = org.mail
                body.stel = org.tel
                body.taxo = org.taxo
                body.sacc = org.acc
                body.sbank = org.bank
                const ouob = await ous.obid(ou)
                body.systemCode = ouob.code
                seq = await SERIAL.sequence(taxc, form, serial,body.idt)
                s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                body.seq = s7
                body.status = 2
                await dbs.query(`insert into s_inv (id,sec,idt,doc,ou,uc) values (?,?,?,?,?,?)`, [id, sec, new Date(body.idt), JSON.stringify(body), ou, uid])
                await Service.saverefno(body)
                const sSysLogs = { fnc_id: 'inv_post', src: config.SRC_LOGGING_DEFAULT, dtl: `Lập hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(body) };
                logging.ins(req,sSysLogs,next)
                res.json(id)
            }
            catch (err) {
                if (!((err.errno == 1062) && (String(err.message).toUpperCase().indexOf("S_INV_UID_01") >= 0))) await SERIAL.err(taxc, form, serial, seq)
                throw err
            }
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body
            if (grant && token.ou !== token.u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)
            const hdb = await Service.checkrefno(body)
            if (hdb.length > 0) throw new Error(`Số bút toán đã tồn tại: ${hdb.join()} \n(Entry number already exists: ${hdb.join()})`)
            const checkrefdate = await Service.checkrefdate(body)
            if (checkrefdate.length > 0) throw new Error(`Ngày bút toán lớn hơn ngày hóa đơn: ${checkrefdate.join(',')} \n(Ref Date is greater than Invoice Date: ${checkrefdate.join(',')})`)
 
            const taxc = token.taxc, uid = token.uid, ou = token.ou, org = await ous.obt(taxc), AUTO = (org.seq === 1), form = body.form, serial = body.serial, id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC,id)
            let seq, s7,result,row
            const dt = moment(body.idt),  eod = dt.endOf('day').toDate(), dts = dt.format(mfd),dtsc = dt.format('YYYYMMDD'),idtc=(moment(new Date())).format('YYYYMMDD')
            try {
                if (dtsc!= idtc){
                    result = await dbs.query(`select count(*) rc from s_inv where idt>? and form=? and serial=? and stax= ? and (status in (2,3,5,6) or (status = 4 and xml is not null))`, [eod, form, serial,taxc])
                    row = result[0][0]
                    if (row.rc > 0) throw new Error(`Tồn tại HĐ có ngày lớn hơn ${moment(body.idt).format('DD/MM/YYYY')} và đã cấp số \n (Invoice has a larger date  ${moment(body.idt).format('DD/MM/YYYY')} and issued a number)`)
                }
                
                result = await dbs.query(`select count(*) rc from s_serial where fd>? and form=? and serial=? and status=1 and taxc=? `, [eod, form, serial,taxc])
                row = result[0][0]
                if (row.rc > 0) throw new Error(`Ngày hiệu lực TBPH phải nhỏ hơn hoặc bằng ngày hóa đơn ${moment(body.idt).format('DD/MM/YYYY')}  \n (The serial effective date must be less than or equal to the invoice date ${moment(row.fd).format('DD/MM/YYYY')})`)
                
                body.id = id
                body.sec = sec
                body.stax = org.taxc
                body.sname = org.name
                body.saddr = org.addr
                body.smail = org.mail
                body.stel = org.tel
                body.taxo = org.taxo
                body.sacc = org.acc
                body.sbank = org.bank
                const ouob = await ous.obid(ou)
                body.systemCode = ouob.code
                if (AUTO) {
                    seq = await SERIAL.sequence(taxc, form, serial,body.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    body.seq = s7
                    body.status = 2
                }
                else body.status = 1
                await dbs.query(`insert into s_inv (id,sec,idt,doc,ou,uc) values (?,?,?,?,?,?)`, [id, sec, new Date(body.idt), JSON.stringify(body), ou, uid])
                await Service.saverefno(body)
                const sSysLogs = { fnc_id: 'inv_post', src: config.SRC_LOGGING_DEFAULT, dtl: `Lập hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(body) };
                logging.ins(req,sSysLogs,next) 
                res.json(id)
            }
            catch (err) {
                if (AUTO) {
                    if (!((err.errno == 1062) && (String(err.message).toUpperCase().indexOf("S_INV_UID_01") >= 0))) await SERIAL.err(taxc, form, serial, seq)
                }
                throw err
            }
        }
        catch (err) {
            next(err)
        }
    },
    adj: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, pid = body.pid
            await chkou(pid, token)
            const uid = token.uid, ou = token.ou, taxc = body.stax, form = body.form, serial = body.serial, org = await ous.obt(taxc), AUTO = (org.seq === 1), id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC,id), idt = new Date(body.idt)
            let conn, seq, s7, result, insid
            try {
                conn = await dbs.getConnection()
                await conn.beginTransaction()
                body.id = id
                body.sec = sec
                const ouob = await ous.obid(ou)
                body.systemCode = ouob.code
                if (config.ent == "hlv"){
                    const cde = `Bị điều chỉnh ngày ${moment(idt).format(mfd)}`
                    let cdt = moment(new Date()).format(dtf)
                     let doc = await rbi(pid)
                     doc.cde =cde 
                     doc.cdt= cdt
                     doc.cdoc=body
                    await conn.query(`update s_inv set cde=?,doc=?,cid=? where id=?`, [cde,JSON.stringify(doc),pid, pid])
                    await conn.commit()
                    insid = 0
                }else{
                    if (AUTO) {
                        seq = await SERIAL.sequence(taxc, form, serial,body.idt)
                        s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                        body.seq = s7
                        body.status = 2
                    }
                    else {
                        s7 = "......."
                        body.status = 1
                    }
                    result = await conn.query(`insert into s_inv (id,pid,sec,idt,doc,ou,uc) values (?,?,?,?,?,?,?)`, [id, pid, sec, idt, JSON.stringify(body), ou, uid])
                    insid = result[0].insertId
                    const cde = `Bị điều chỉnh bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${moment(idt).format(mfd)} mã ${sec}`
                    let cdt = moment(new Date()).format(dtf)
                    await conn.query(`update s_inv set cid=?,cde=?,doc=JSON_SET(doc,'$.cde',?,'$.cdt',?) where id=?`, [id, cde, cde, cdt, pid])
                    await conn.commit()
                }
                
               
                const sSysLogs = { fnc_id: 'inv_adj', src: config.SRC_LOGGING_DEFAULT, dtl: `Điều chỉnh hóa đơn id ${pid}`, msg_id: pid, doc: JSON.stringify(body)};
                logging.ins(req,sSysLogs,next) 
                res.json(insid)
            }
            catch (err) {
                if (AUTO) {
                    if (!((err.errno == 1062) && (String(err.message).toUpperCase().indexOf("S_INV_UID_01") >= 0))) await SERIAL.err(taxc, form, serial, seq)
                }
                await conn.rollback()
                throw err
            }
            finally {
                await conn.release()
            }
        }
        catch (err) {
            next(err)
        }
    },
    adjniss: async (req, res, next) => {
    },
    rep: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            let body = req.body, pid = body.pid
            await chkou(pid, token)

            const hdb = await Service.checkrefno(body)
            if (hdb.length > 0) throw new Error(`Số bút toán đã tồn tại: ${hdb.join()} \n (Entry number already exists: ${hdb.join()})`)
            const checkrefdate = await Service.checkrefdate(body)
            if (checkrefdate.length > 0) throw new Error(`Ngày bút toán lớn hơn ngày hóa đơn: ${checkrefdate.join(',')} \n(Ref Date is greater than Invoice Date: ${checkrefdate.join(',')})`)
 
            const uid = token.uid, ou = token.ou, taxc = body.stax, form = body.form, serial = body.serial, org = await ous.obt(taxc), AUTO = (org.seq === 1), id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC,id), idt = new Date(body.idt)
            let conn, seq, s7, result, insid
            try {
                conn = await dbs.getConnection()
                await conn.beginTransaction()
                body.id = id
                body.sec = sec
                const ouob = await ous.obid(ou)
                body.systemCode = ouob.code
                // AutoSeq exists nghĩa là PHAT HANH ở THAY THE
                if (AUTO || body.AutoSeq) {
                    if (body.AutoSeq) delete body["AutoSeq"]
                    seq = await SERIAL.sequence(taxc, form, serial,body.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    body.seq = s7
                    body.status = 2
                }
                else {
                    s7 = "......."
                    body.status = 1
                }
                result = await conn.query(`insert into s_inv (id,pid,sec,idt,doc,ou,uc) values (?,?,?,?,?,?,?)`, [id, pid, sec, idt, JSON.stringify(body), ou, uid])
                await Service.saverefno(body)
                insid = result[0].insertId
                const cde = `Bị thay thế bởi HĐ số ${s7} ký hiệu ${body.serial} mẫu số ${body.form} ngày ${moment(idt).format(mfd)} mã ${sec}`
                let cdt = moment(new Date()).format(dtf)
                await conn.query(`update s_inv set doc=JSON_SET(doc,'$.status',4,'$.cde',?,'$.cdt',?),cid=?,cde=? where id=?`, [cde, cdt, id, cde, pid])
                await conn.commit()
                const sSysLogs = { fnc_id: 'inv_replace', src: config.SRC_LOGGING_DEFAULT, dtl: `Thay thế hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(body)};
                logging.ins(req,sSysLogs,next)
                res.json(insid)
            }
            catch (err) {
                if (AUTO) {
                    if (!((err.errno == 1062) && (String(err.message).toUpperCase().indexOf("S_INV_UID_01") >= 0))) await SERIAL.err(taxc, form, serial, seq)
                }
                await conn.rollback()
                throw err
            }
           
            finally {

                await conn.release()

            }
        }
        catch (err) {
            next(err)
        }
    },
    go24: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, body = req.body, form = body.form, serial = body.serial, seq = body.seq, id = body.id, d = body.d, direct = d > 0 ? "tiến" : "lùi"
            await chkou(id, token)
            const idt = new Date(body.idt)
            idt.setDate(idt.getDate() + d)
            const dt = moment(idt), sod = dt.startOf('day').toDate(), eod = dt.endOf('day').toDate(), dts = dt.format(mfd)
            let result, row
            if (d > 0) {
                const today = moment().endOf('day').toDate()
                if (eod > today) throw new Error(`Ngày tiến hóa đơn ${dts} lớn hơn ngày hiện tại \n (The forward date of the invoice ${dts} is greater than the current date)`)
            }
            else {
                result = await dbs.query(`select count(*) rc from s_serial where taxc=? and form=? and serial=? and fd>?`, [taxc, form, serial, eod])
                row = result[0][0]
                if (row.rc > 0) throw new Error(`Ngày lùi hóa đơn ${dts} không có TBPH \n (The back date of the invoice ${dts} doesn't have released information)`)
            }
            if (seq) {
                const iseq = parseInt(seq)
                result = await dbs.query(`select count(*) rc from s_inv where idt<? and form=? and serial=? and (status=1 or ((status in (2,3,5,6) or (status = 4 and xml is not null)) and cast(seq as unsigned)>?))`, [sod, form, serial, iseq])
                row = result[0][0]
                if (row.rc > 0) throw new Error(`Hóa đơn không ${direct} được: tồn tại HĐ có ngày nhỏ hơn ${dts} chưa cấp số hoặc có số lớn hơn ${seq} \n (Invoice can not ${direct}: Existing invoices with dates less than ${dts} and not issued a number or number greater than ${seq})`)
                result = await dbs.query(`select count(*) rc from s_inv where idt>? and form=? and serial=? and (status in (2,3,5,6) or (status = 4 and xml is not null)) and cast(seq as unsigned)<?`, [eod, form, serial, iseq])
                row = result[0][0]
                if (row.rc > 0) throw new Error(`Hóa đơn không ${direct} được: tồn tại HĐ có ngày lớn hơn ${dts} và số lớn hơn ${seq} \n (Invoice can not ${direct}: Existing invoices with dates greater than ${dts} and number greater than ${seq})`)
            }
            else {
                result = await dbs.query(`select count(*) rc from s_inv where idt>? and form=? and serial=? and (status in (2,3,5,6) or (status = 4 and xml is not null))`, [eod, form, serial])
                row = result[0][0]
                if (row.rc > 0) throw new Error(`Hóa đơn không ${direct} được : tồn tại HĐ có ngày lớn hơn ${dts} và đã cấp số \n (Invoice can not ${direct}: Existing invoices with dates greater than ${dts} and issued a number`)
            }
            await dbs.query(`update s_inv set idt=?,doc=JSON_SET(doc,'$.idt',?) where id=? and status in (1, 2)`, [sod, moment(sod).format(dtf), id])
            res.json(1)
        } catch (err) {
            next(err)
        }
    },
    status: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, id = req.params.id
            let doc, status = req.params.status
            await chkou(id, token)
            doc = await rbi(id)
            let oldstatus = doc.status
            if (status == 3 || status == 2) {
                await dbs.query(`update s_inv set doc=JSON_REMOVE(doc,'$.cancel') where id=? and stax=?`, [id, taxc])
                const r = await dbs.query(`select 1 from s_inv where id=? and xml is not null LIMIT 1`, [id])
                if (r[0].length > 0) status = 3
                else status = 2
                //Xóa biên bản
                if (UPLOAD_MINUTES) await dbs.query(`DELETE FROM s_inv_file where id=?`, [id])
            }
            else if (status == 4) {
                doc = await rbi(id)
                const now = moment().format(dtf)
                if (doc.pid && doc.adj.typ == 2) await dbs.query(`update s_inv set cid=null,cde=null where id=?`, [doc.pid])
                let cdt = moment(new Date()).format(dtf)
                await dbs.query(`update s_inv set doc=JSON_SET(doc,'$.cancel',JSON_OBJECT('typ',3,'ref',?,'rdt',?),'$.cdt',?) where id=? and stax=?`, [id, now, cdt, id, taxc])
            }
            let result = await dbs.query(`update s_inv set doc=JSON_SET(doc,'$.status',?) where id=? and stax=?`, [status, id, taxc])
            if (result[0].affectedRows > 0 && oldstatus==3) {
                if ((status == 4) && !util.isEmpty(doc.bmail)) {
                    doc.status = 4
                    if (useJobSendmail) {
                        await Service.insertDocTempJob(id, doc, ``)
                    } else {
                        if (!util.isEmpty(doc.bmail)) {
                            let mailstatus = await email.rsmqmail(doc)
                            if (mailstatus) await Service.updateSendMailStatus(doc)
                        }
                    }
                }
                const sSysLogs = { fnc_id: 'inv_istatus', src: config.SRC_LOGGING_DEFAULT, dtl: `Chuyển đổi trạng thái hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                logging.ins(req,sSysLogs,next)
            }
            res.json(1)
        } catch (err) {
            next(err)
        }
    },
    cancel: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, id = req.params.id
            await chkou(id, token)
            const doc = await rbi(id)
            let oldstatus = doc.status
            if (doc.pid && doc.adj.typ == 2) await dbs.query(`update s_inv set cid=null,cde=null where id=?`, [doc.pid])
            let cdt = moment(new Date()).format(dtf)
            let result = await dbs.query(`update s_inv set doc=JSON_SET(doc,'$.status',4,'$.cancel',JSON_OBJECT('typ',?,'ref',?,'rdt',?,'rea',?),'$.cdt',?) where id=? and stax=?`, [body.typ, body.ref, body.rdt, body.rea, cdt, id, token.taxc])
            if (result[0].affectedRows && oldstatus==3) {
                if (!util.isEmpty(doc.bmail)) {
                    doc.status = 4
                    if (useJobSendmail) {
                        await Service.insertDocTempJob(id, doc, ``)
                    } else {
                        if (!util.isEmpty(doc.bmail)) {
                            let mailstatus = await email.rsmqmail(doc)
                            if (mailstatus) await Service.updateSendMailStatus(doc)
                        }
                    }
                }
            }
            const sSysLogs = { fnc_id: 'inv_cancel', src: config.SRC_LOGGING_DEFAULT, dtl: `Hủy hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
            logging.ins(req,sSysLogs,next)
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },
    trash: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, id = req.params.id
            await chkou(id, token)
            //Lay du lieu doc truoc khi xoa de luu log
            let doclog = await rbi(id)
            await dbs.query(`update s_inv set doc=JSON_SET(doc,'$.status',6,'$.cancel',JSON_OBJECT('typ',?,'ref',?,'rdt',?,'rea',?)) where id=? and stax=? and status in (2,3)`, [body.typ, body.ref, body.rdt, body.rea, id, token.taxc])
            res.json(1)
            const sSysLogs = { fnc_id: 'inv_adj', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doclog)};
            logging.ins(req,sSysLogs,next)
        }
        catch (err) {
            next(err)
        }
    },
    istatus: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, body = req.body, ids = body.ids, os = parseInt(body.os), now = moment().format(dtf)
            let rc = 0, ns = body.ns
            for (let id of ids) {
                try {
                    let sql, binds = []
                    let doc, doclog, sSysLogs
                    doclog = await rbi(id)
                    let oldstatus = doclog.status
                    if (ns == 4) {
                        let cdt = moment(new Date()).format(dtf)
                        sql = `update s_inv set doc=JSON_SET(doc,'$.status',?,'$.cdt',?) where id=? and stax=? and status=?`
                        //Lay du lieu doc truoc khi xoa de luu log
                        doclog = await rbi(id)
                        binds = [ns, cdt, id, taxc, os]
                        doc = await rbi(id)
                        if (doc.pid && doc.adj.typ == 2) await dbs.query(`update s_inv set cid=null,cde=null where id=?`, [doc.pid])
                        if (!doc.cancel) {
                            binds = [ns, cdt, id, now, id, taxc, os]
                            sql = `update s_inv set doc=JSON_SET(doc,'$.status',?,'$.cdt',?,'$.cancel',JSON_OBJECT('typ',3,'ref',?,'rdt',?)) where id=? and stax=? and status=?`
                        }
                        sSysLogs = { fnc_id: 'inv_cancel', src: config.SRC_LOGGING_DEFAULT, dtl: `Hủy hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doclog)};
                    } else {
                        //Lay du lieu doc truoc khi xoa de luu log
                        doclog = await rbi(id)
                        if (ns == 2 || ns == 3) {
                            const r = await dbs.query(`select 1 from s_inv where id=? and xml is not null LIMIT 1`, [id])
                            if (r[0].length > 0) ns = 3
                            else ns = 2
                        }
                        sql = `update s_inv set doc=JSON_SET(doc,'$.status',?) where id=? and stax=? and status=?`
                        binds = [ns, id, taxc, os]
                        sSysLogs = { fnc_id: 'inv_istatus', src: config.SRC_LOGGING_DEFAULT, dtl: `Chuyển đổi trạng thái hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doclog)};
                    }
                    const result = await dbs.query(sql, binds)
                    if (result[0].affectedRows > 0 && (oldstatus == 3 || oldstatus == 6)) {
                        if ((ns == 4) && !util.isEmpty(doc.bmail)) {
                            doc.status = 4
                            if (useJobSendmail) {
                                await Service.insertDocTempJob(id, doc, ``)
                            } else {
                                if (!util.isEmpty(doc.bmail)) {
                                    let mailstatus = await email.rsmqmail(doc)
                                    if (mailstatus) await Service.updateSendMailStatus(doc)
                                }
                            }
                        }
                        rc++
                    }
                    
                    logging.ins(req,sSysLogs,next)
                } catch (e) {
                    logger4app.error(e)
                }
            }
            res.json(rc)
        }
        catch (err) {
            next(err)
        }
    },
    del: async (req, res, next) => {
        let conn
        try {
            const token = SEC.decode(req), id = req.params.id, pid = req.params.pid
            await chkou(id, token)
            //Lay du lieu doc truoc khi xoa de luu log
            let doclog = await rbi(id)
            conn = await dbs.getConnection()
            await conn.beginTransaction()
            const result = await conn.query(`delete from s_inv where id=? and stax=? and status=?`, [id, token.taxc, 1])
            if (result[0].affectedRows > 0) {
                if (pid > 0) {
                    await conn.query(`update s_inv set doc=JSON_SET(doc,'$.status',3),doc=JSON_REMOVE(doc,'$.cde','$.cdt'),cid=null,cde=null where id=?`, [pid])
                    //Xóa biên bản
                    // await conn.query(`DELETE FROM s_inv_file where id=?`, [pid])
                }
            }
            await conn.commit()
            const sSysLogs = { fnc_id: 'inv_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doclog)};
            logging.ins(req,sSysLogs,next) 
            res.json(1)
        }
        catch (err) {
            await conn.rollback()
            next(err)
        }
        finally {
            await conn.release()
        }
    },
    relate: async (req, res, next) => {
        try {
            const token = SEC.decode(req), id = req.params.id, doc = await rbi(id), sec = doc.sec, root = doc.root, rsec = root ? root.sec : null
            let sql, result, binds = [token.taxc, sec, sec]
            sql = `select id id,idt idt,type type,form form,serial serial,seq seq,sec sec,status status,adjdes adjdes,cde cde from s_inv a where a.stax=? and a.sec<>? and (a.doc->"$.root.sec"=?`
            if (rsec) {
                sql += ` or a.sec=? or a.doc->"$.root.sec"=?`
                binds.push(rsec, rsec)
            }
            sql += ") order by id"
            result = await dbs.query(sql, binds)
            res.json(result[0])
        }
        catch (err) {
            next(err)
        }
    },
    relateadjs: async (req, res, next) => {
        try {
            const token = SEC.decode(req), id = req.params.id
            let sql, result
            sql = `select id id,idt idt,type type,form form,serial serial,seq seq,sec sec,status status,adjdes adjdes,cde cde from s_inv a where id in (${id}) order by id`
          
            result = await dbs.query(sql, [])
            res.json(result[0])
        }
        catch (err) {
            next(err)
        }
    },
    deleteinv: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const taxc = token.taxc, uid = token.uid, ou = token.ou, 
            org = await ous.obt(taxc), AUTO = (org.seq === 1), body = req.body, invs = body.invs
            let count = 0
            let status, message
                try {
                    const token = SEC.decode(req)
                
                    for (let inv of invs) {
                        await chkou(inv.id, token)
                        //Lay du lieu doc truoc khi xoa de luu log
                        let doclog = await rbi(inv.id)
                        await dbs.query(`delete from s_inv where id=? and status = 1`, [inv.id])
                        if(ENT=='hlv' && doclog.ids){
                            await dbs.query(`update s_inv_temp set status = 0 where id=? and status = 1`, [doclog.ids[0].id])
                        }
                        await Service.delrefno(doclog)
                        await dbs.query(`update  s_inv set cid=null,cde=null where id=? and cid=?`, [inv.pid, inv.id])
                        count++
                        const sSysLogs = { fnc_id: 'inv_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa hóa đơn id ${inv.id}`, msg_id: inv.id, doc: JSON.stringify(doclog)};
                        logging.ins(req,sSysLogs,next) 
                    }
                    status = 1
                    message ='Success'
                } catch (e) {
                    status = 2
                    throw e
                }
            
            res.json({message:message,status:status,countseq: count})
        }
        catch (err) {
            next(err)
        }
    },
    deleteinv_gd: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const taxc = token.taxc, uid = token.uid, ou = token.ou, 
            org = await ous.obt(taxc), AUTO = (org.seq === 1), body = req.body, invs = body.invs
            let count = 0
            let status, message
                try {
                    const token = SEC.decode(req)
                
                    for (let inv of invs) {
                        await chkou(inv.id, token)
                        //Lay du lieu doc truoc khi xoa de luu log
                        let doclog = await rbi(inv.id)
                        await dbs.query(`delete from s_inv where id=? and status = 1`, [inv.id])
                        if(ENT=='hlv' && doclog.ids){
                            await dbs.query(`delete from s_inv_temp  where id=? and status = 1`, [doclog.ids[0].id])
                        }
                       
                        await Service.delrefno(doclog)
                        count++
                        const sSysLogs = { fnc_id: 'inv_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa hóa đơn id ${inv.id}`, msg_id: inv.id, doc: JSON.stringify(doclog)};
                        logging.ins(req,sSysLogs,next) 
                    }
                    status = 1
                    message ='Success'
                } catch (e) {
                    status = 2
                    throw e
                }
            
            res.json({message:message,status:status,countseq: count})
        }
        catch (err) {
            next(err)
        }
    },
    mailinvs: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, invs = body.invs
            let status, message

            try {
                for (let inv of invs) {
                    let docmail = await rbi(inv.id)
                    let doc = JSON.parse(JSON.stringify(docmail)), xml = ''
                    if (config.ent == 'vcm') {
                        let taxc_root = await ous.pidtaxc(doc.ou)
                        doc.taxc_root = taxc_root
                    }
                    //logger4app.debug(`begin send mail : ${doc.stax} - ${doc.form} - ${doc.serial} - ${doc.seq}`)
                    if (useJobSendmail) {
                        await Service.insertDocTempJob(doc.id, doc, xml)
                    } else {
                        if (!util.isEmpty(doc.bmail)) {
                            let mailstatus = await email.senddirectmail(doc, xml)
                            if (mailstatus) await Service.updateSendMailStatus(doc)
                        }
                        if (!util.isEmpty(doc.btel)) {
                            let smsstatus = await sms.smssend(doc)
                            if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                        }
                    }
                }
                status = 1
                message = 'Success'
            } catch (e) {
                status = 2
                throw e
            }

            res.json({ message: message, status: status })
        }
        catch (err) {
            next(err)
        }
    },
    sendmaildoc: async (req, res, next) => {
        try {
            let doc = req.body, xml = doc.xml
            logger4app.debug(`begin send mail : ${doc.stax} - ${doc.form} - ${doc.serial} - ${doc.seq} : ${doc.status}`)
            if (config.useJobSendmailAPI) {
                await Service.insertDocTempJob(doc.id, doc, xml)
            } else {
                if (!util.isEmpty(doc.bmail)) {
                    let mailstatus = await email.senddirectmail(doc, xml)
                    if (mailstatus) await Service.updateSendMailStatus(doc)
                }
            }
            
            if (config.ent == "hsy") {
                const rtolog = await rbi(doc.id)
                doc.uc = rtolog.uc
                if (doc.status == 2) await Service.sendToApprover(doc)
            }
            logger4app.debug(`end send mail : ${doc.stax} - ${doc.form} - ${doc.serial} - ${doc.seq} : ${doc.status}`)
            res.json({ result: "1"})
        }
        catch (err) {
            throw err
        }
    },
    signdoc: async (req, res, next) => {
        try {
            let doc = req.body
            const org = await ous.gettosign(doc.stax)
            logger4app.debug(org)
            let signtype = org.sign
            let xml, binds, result
            let sql          
            
            if (signtype == 1) {
                const usr = org.usr, pwd = util.decrypt(org.pwd)
                xml = util.j2x((await Service.getcol(doc)), doc.id)
                const arr = [{ inc: id, xml: Buffer.from(xml).toString("base64") }]
                res.json({ lib: usr, sid: 0, pin: pwd, mst: doc.stax, date: new Date(), ref: config.PATH_XML, arr: arr })
            }
            else {
                if (signtype == 2) {
                    const pwd = util.decrypt(org.pwd)
                    const ca = await sign.ca(doc.stax, pwd)
                    xml = util.signature(sign.sign(ca, util.j2x((await Service.getcol(doc)), doc.id), doc.id), doc.idt)
                }
                else if (signtype == 3) {
                    sql = `select id,doc,bmail,pid from s_inv where id=? and stax=? and status=2`
                    binds = [doc.id, doc.stax]
                    result = await dbs.query(sql, binds)
                    const rows = result[0]
                    if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
                    const usr = org.usr, pwd = util.decrypt(org.pwd)
                    const kqs = await fcloud.xml(usr, pwd, doc.stax, rows), kq = kqs[0]
                    xml = kq.xml
                }
                else {
                    xml = await hsm.signxml(doc.stax, util.j2x((await Service.getcol(doc)), doc.id))
                    xml = util.signature(xml, doc.idt)
                }
            }
            res.json({ xml: xml})
        }
        catch (err) {
            next(err)
        }
    },
    signxml: async (req, res, next) => {
        try {
            const xml = req.body.xml, stax = req.body.stax, id = req.body.id, idt = req.body.idt
            const org = await ous.gettosign(stax)
            logger4app.debug(org)
            let signtype = org.sign
            let binds, result, xmlresult
            let sql

            if (signtype == 1) {
                const usr = org.usr, pwd = util.decrypt(org.pwd)
                xmlresult = util.j2x((await Service.getcol(doc)), doc.id)
                const arr = [{ inc: id, xml: Buffer.from(xml).toString("base64") }]
                res.json({ lib: usr, sid: 0, pin: pwd, mst: stax, date: new Date(), ref: config.PATH_XML, arr: arr })
            }
            else {
                if (signtype == 2) {
                    const pwd = util.decrypt(org.pwd)
                    const ca = await sign.ca(stax, pwd)
                    xmlresult = util.signature(sign.sign(ca, xml, id), idt)
                }
                else if (signtype == 3) {
                    sql = `select id,doc,bmail,pid from s_inv where id=? and stax=? and status=2`
                    binds = [id, stax]
                    result = await dbs.query(sql, binds)
                    const rows = result[0]
                    if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
                    const usr = org.usr, pwd = util.decrypt(org.pwd)
                    const kqs = await fcloud.xml(usr, pwd, stax, rows), kq = kqs[0]
                    xmlresult = kq.xml
                }
                else {
                    xmlresult = await hsm.signxml(stax, xml)
                    xmlresult = util.signature(xmlresult, idt)
                }
            }
            res.json({ xml: xmlresult })
        }
        catch (err) {
            next(err)
        }
    },
    syncRedisDb: async (req, res, next) => {
        try {
            var sqlinv = 'select max(id) id from s_inv'
            logger4app.debug('sqlinv : ', sqlinv)
            var resultinv = await dbs.query(sqlinv, [])
            var maxid = resultinv.recordset[0].id
            logger4app.debug('resultinv.recordset[0] : ', resultinv.recordset[0])
            logger4app.debug('maxid : ', maxid)
            var keyidinv = `INC`, curid = await redis.get(keyidinv)
            if (curid < maxid) {
                await redis.set(keyidinv, maxid)
            }
            res.json({ result: "1"})
        } catch (err) {
            next(err)
        }
    },
    print: async (req, res, next) => {
        try {
            const iwh = await Service.iwh(req, true)
            const result = await dbs.query(`select doc from s_inv ${iwh.where} order by serial,seq`, iwh.binds)
            const rows = result[0]
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn \n No invoice be found`)
            const token = SEC.decode(req), org = await ous.org(token), idx = org.temp, filter = JSON.parse(req.query.filter), fn = util.fn(filter.form, idx), tmp = await util.template(fn)
            // const token = SEC.decode(req), mst = token.mst, org = await ous.org(token), qrc = org.qrc == 1, filter = JSON.parse(req.query.filter)
            // for (const row of rows) {
            //     const doc = row.doc
            //     util.viewadj(doc)
                //if (qrc) row.qrcode = await util.qrcode(row.doc)
            // }
            for (const row of rows) {
                row.doc.tempfn = fn
            }
            res.json({ docs: rows, tmp: tmp })
        } catch (err) {
            next(err)
        }
    },
    printmerge: async (req, res, next) => {
        try {
            const iwh = await Service.iwh(req, true)
            const result = await dbs.query(`select doc from s_inv ${iwh.where} order by serial,seq`, iwh.binds)
            const rows = result[0]
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn \n No invoice be found`)
            const token = SEC.decode(req), org = await ous.org(token), idx = org.temp, filter = JSON.parse(req.query.filter), fn = util.fn(filter.form, idx), tmp = await util.template(fn)
            // const token = SEC.decode(req), mst = token.mst, org = await ous.org(token), qrc = org.qrc == 1, filter = JSON.parse(req.query.filter)
            // for (const row of rows) {
            //     const doc = row.doc
            //     util.viewadj(doc)
                //if (qrc) row.qrcode = await util.qrcode(row.doc)
            // }
            let jsons = []
            for (const row of rows) {
                row.doc.tempfn = fn
                jsons.push({doc : row.doc})
            }
            let respdf = await util.jsReportRenderAttachPdfs(jsons)
            res.json(respdf)
        } catch (err) {
            next(err)
        }
    },
    delAll: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body
            let sql, result, binds, rows, sqlinv, resultinv, query = {filter: JSON.stringify(body)}, reqtmp = {query: query, token: token}, iwh = await Service.iwh(reqtmp, false, true), countseq = 0
            //Chi cho phép xóa các hóa đơn với trạng thai nhất định
            if (!['1'].includes(body.status)) throw new Error(`Chỉ được xóa các hóa đơn với các trạng thái được phép xóa \n (Only invoices with statuses allowed to be deleted can be deleted)`) 
            //Check so ban ghi truoc khi thuc hien xoa
            const sqlcount = `select count(*) total from s_inv ${iwh.where}`
            const resultcount = await dbs.query(sqlcount, iwh.binds)
            if (resultcount.recordset[0].total > config.MAX_ROW_DELETE_ALL) throw new Error(`Số lượng bản ghi cần xóa vượt quá giới hạn cho phép \n (The number of records to be deleted exceeds the allowed limit)`) 
            //Lấy các hóa đơn cần xóa
            sqlinv = `select pid id from s_inv ${iwh.where}`
            resultinv = await dbs.query(sqlinv, iwh.binds)
            rows = resultinv.recordset
            for (let inv of rows) {
                await chkou(inv.id, token)
                //Lay du lieu doc truoc khi xoa de luu log
                let doclog = await rbi(inv.id)
                if (config.ent == "scb") {
                    for (const item of doclog.items) {
                        if (item.tranID) {
                            await dbs.query(`update s_trans set status = @1, inv_id = null, inv_date = null where tranID = @2`, [item.statusGD, item.tranID])
                        }
                    }
                }
                await dbs.query(`delete from s_inv where id=@1`, [inv.id])
                await Service.delrefno(doclog)
                await dbs.query(`update  s_inv set cid=null,cde=null where id=@1 and cid=@2`, [inv.pid, inv.id])
                countseq++
                if (config.ent == 'mzh') {
                    await dbs.query(`update  s_trans set status=stattus_temp,inv_id=inv_id_tmp,exrt=null,inv_date=null,ma_nv=null,trancol=null where inv_id=@1`, [inv.id])
                }
                const sSysLogs = { fnc_id: 'inv_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa hóa đơn id ${inv.id}`, msg_id: inv.id, doc: JSON.stringify(doclog) };
                logging.ins(req, sSysLogs, next)
            }
            
            res.json({ countseq: countseq })
        } catch (err) {
            next(err)
        }
    }
}
module.exports = Service