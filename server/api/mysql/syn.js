"use strict"
const moment = require("moment")
const Hashids = require("hashids/cjs")
const util = require("../util")
const sec = require("../sec")
const dbs = require("./dbs")
const redis = require("../redis")
const ous = require("./ous")
const objectMapper = require("object-mapper")
const config = require("../config")
const logger4app = require("../logger4app")
let axios = require('axios')
const path = require("path")							
const fs = require("fs")
const xlsxtemp = require("xlsx-template")							
						
										 
								
const grant = config.ou_grant
const ent = config.ent
const HLV = ent === "hlv"
const mfd = config.mfd
const dtf = config.dtf
const KM = "Khuyến mãi"
const CK = "Chiết khấu"
const MT = "Mô tả"
const KCT = "Không chịu thuế"
const KKK = "Không kê khai nộp thuế"
const KMMT = [KM, MT]
const GNS = ["code", "name", "unit", "price"]
const ORG = ["name", "name_en", "taxc", "code", "addr", "tel", "mail", "acc", "bank"]
const PXC = ["ic", "ordno", "orddt", "ordou", "ordre", "recvr", "trans", "vehic", "contr", "whsfr", "whsto", "curr", "exrt", "note", "sum", "items.type", "items.name", "items.unit", "items.price", "items.quantity", "items.amount"]
const HDC = ["ic", "form", "serial", "seq", "idt", "buyer", "bname", "btax", "baddr", "btel", "bmail", "bacc", "bbank", "paym", "curr", "exrt", "note", "sum", "vat", "total", "items.type", "items.name", "items.unit", "items.price", "items.quantity", "items.vrt", "items.amount", "rform", "rserial", "rseq", "rref", "rrdt", "rrea"]
const URL = "http://json-schema.org/draft-07/schema#"
const STT = { "type": "integer", "minimum": 1 }
const STR = { "type": "string", "maxLength": 255 }
const NUM = { "type": "number", "minimum": 0 }
const PAYM = { "description": "HTTT", "type": "string", "default": "TM", "enum": ["CK", "TM", "TM/CK", "DTCN"] }
const CURR = { "description": "Loại tiền", "type": "string", "default": "VND", "enum": ["AUD", "CAD", "CHF", "DKK", "EUR", "GBP", "HKD", "INR", "JPY", "KRW", "KWD", "MYR", "NOK", "RUB", "SAR", "SEK", "SGD", "THB", "USD", "VND"] }
const ITEM = { "type": "object", "properties": { "line": STT, "type": { "type": "string", "enum": ["", KM, CK, MT] }, "name": STR, "price": NUM, "quantity": NUM, "amount": NUM }, "required": ["line", "name"] }
const EXRT = { "type": "number", "minimum": 1, "default": 1 }
const n2w = require("../n2w")
const ajv = require("ajv")
const jv = new ajv({ allErrors: true })
const hashids = new Hashids(config.secret, 10, "abcdefghijklmnopqrstuvwxyz1234567890")
const SCHEMAS = {
  "GNS": { "$schema": URL, "type": "object", "properties": { "code": { "type": ["integer", "string"], "minimum": 0, "maxLength": 30 }, "name": STR, "unit": { "type": "string", "maxLength": 30 }, "price": NUM }, "required": ["code", "name", "unit"] },
  "00ORGS": { "$schema": URL, "type": "object", "properties": { "name": STR, "addr": STR, "mail": STR }, "required": ["name", "addr"] },
  "03XKNB": {
    "$schema": URL, "type": "object", "definitions": { "item": ITEM },
    "properties": { "whsfr": STR, "whsto": STR, "curr": CURR, "exrt": EXRT, "sum": NUM, "total": NUM, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 } },
    "required": ["ic", "whsfr", "whsto", "curr", "exrt", "sum", "total", "items"]
  },
  "01GTKT": {
    "$schema": URL, "type": "object", "definitions": {
      "item": { "type": "object", "properties": { "line": STT, "type": { "type": "string", "enum": ["", KM, CK, MT] }, "name": STR, "price": NUM, "quantity": NUM, "amount": NUM, "vrt": { "type": "string", "enum": ["-2", "-1", "0", "5", "10"] } }, "required": ["line", "name", "vrt"] }
    },
    "properties": {
      "buyer": STR, "bname": STR, "baddr": STR, "bmail": STR, "paym": PAYM, "curr": CURR, "exrt": EXRT, "sum": NUM, "total": NUM, "vat": NUM, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 }
    },
    "required": ["idt", "ic", "bname", "baddr", "paym", "curr", "exrt", "vat", "sum", "total", "items"]
  },
  "02GTTT": {
    "$schema": URL, "type": "object", "definitions": { "item": ITEM },
    "properties": {
      "buyer": STR, "bname": STR, "baddr": STR, "bmail": STR, "paym": PAYM, "curr": CURR, "exrt": EXRT, "sum": NUM, "total": NUM, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 }
    },
    "required": ["ic", "bname", "baddr", "paym", "curr", "exrt", "sum", "total", "items"]
  }
}
const tax = async (rows, exrt) => {
  let catobj = JSON.parse(JSON.stringify(config.CATTAX))
  let arr = catobj
  for (let row of rows) {
    let amt = row.amount
    if (!util.isNumber(amt)) continue
    const vrt = row.vrt
    let obj = arr.find(x => x.vrt == vrt)
    if (typeof obj == "undefined") continue
    if (row.type == CK) amt = -amt
    obj.amt += amt
    if (obj.hasOwnProperty("vat")) obj.vat += amt * vrt / 100
  }
  let tar = arr.filter(obj => { return obj.amt !== 0 })
  for (let row of tar) {
    row.amtv = Math.round(row.amt * exrt)
    if (row.hasOwnProperty("vat")) row.vatv = Math.round(row.vat * exrt)
  }
  return tar
}
const Service = {

  get: async (req, res, next) => {
    try {
      const token = sec.decode(req), taxc = token.taxc, query = req.query, filter = JSON.parse(query.filter), start = query.start ? query.start : 0, count = query.count ? query.count : 10, sort = query.sort, now = new Date(), NOW = moment(now).format(dtf)
      const type = filter.type, iform = filter.form, iserial = filter.serial, VAT = (type === "01GTKT"), schema = SCHEMAS[type]
      //let where = " where ou in (select id from s_ou start with id=? connect by prior id=pid) ", order = "", sql, result, ret, binds = [token.ou]

      let where = " where invoice_date between ? and ?", order = "order by id desc", sql, resultq, ret, binds = [new Date(filter.fd), new Date(moment(filter.td).endOf("day"))]
      if (query.filter) {
        let filter = JSON.parse(query.filter), val
        Object.keys(filter).forEach((key) => {
          val = filter[key]
          if (val) {
            if (key == "status") {
              where += ` and status = ?`
              binds.push(val)
            }
            if (key == "type") {
              where += ` and type = ?`
              binds.push(val)
            }
            if (key == "form") {
              where += ` and form = ?`
              binds.push(val)
            }
            if (key == "serial") {
              where += ` and serial = ?`
              binds.push(val)
            }

          }
        })
      }
      
      order = `order by invoice_date,sid`
      sql = `select id,
      sid,
      policy_number,
      DATE_FORMAT(due_date, '%Y-%m-%d') due_date,
      paid_amount,
      pay_mode,
      main_product_name,
      proposer_name,
      life_insured_name,
      address,
      email,
      tax_identification,
      DATE_FORMAT(payment_date, '%Y-%m-%d') payment_date,
      DATE_FORMAT(effective_date, '%Y-%m-%d') effective_date,
      service_code,
      buyer_name,
      tax_code,
      payment_method,
      currency,
      exchange_rate,
      unit,
      price,
      quantity,
      total_vat,
      amount,
      total_amount,
      DATE_FORMAT(invoice_date, '%Y-%m-%d') invoice_date,
      content,
      service_name,
      status,
      error,
      parent_id,
      type,
      form,
      serial
       from s_inv_temp ${where} ${order} LIMIT ${count} offset ${start}`
      resultq = await dbs.query(sql, binds)
      // let Obj = {
      //   "policy_number": "c0", "due_date": "c1", "service_code": "c2", "pay_mode": "c3", "life_insured_name": "c4", "payment_date": "c5", "effective_date": "c6", "content": "c7", "service_name": "c8", "address": "baddr", "email": "bmail","type": "type","form": "form","serial": "serial"
      //   , "proposer_name": "bname", "tax_identification": "btax", "buyer_name": "buyer", "currency": "curr", "exchange_rate": "exrt", "sid": "ic"
      //   , "invoice_date": "idt", "paid_amount": "items.amount", "main_product_name": "items.name","total_vat": "items.vat", "price": "items.price", "quantity": "items.quantity", "unit": "items.unit", "tax_code": "items.vrt", "payment_method": "paym", "status": "statusGD", "error": "error", "id": "ids.id"
      // };
      // let ic = "^_^", obj, items, result = [], ids, dt,retdate,mydate

      // for (const r of resultq[0]) {
      //   const row = objectMapper(r, Obj)
      //   //if (!row.ic) throw new Error("Thiếu mã hóa đơn \n (Missing invoice code)")
      //     retdate = moment(dt).format('YYYY-MM-DD');
      //      mydate = moment(row.idt).format('YYYY-MM-DD');
      //   if (ic == row.ic && retdate == mydate) {
      //     items.push(row.items)
      //     ids.push(row.ids)
      //   }
      //   else {
      //     if (obj) {
      //       obj.items = items
      //       obj.ids = ids
      //       result.push(obj)
      //     }
      //     obj = row
      //     items = [row.items]
      //     ids = [row.ids]
      //     ic = row.ic
      //     dt = row.idt
      //   }
      // }
      // if (obj) {
      //   obj.items = items
      //   obj.ids = ids
      //   result.push(obj)
      // }
      // for (const row of result) {
      //   let curr = row.curr, exrt = row.exrt, error = "", i = 1, vat = 0, sum = 0, total = 0, totalv, items = row.items
      //   row.ic = row.ic.toString()
      //   if (HLV && row.c0) {
      //    // if (!(/^[0-9]{10}$/.test(row.c0))) error = "Số hợp đồng sai định dạng. "
      //   }
      //   if (util.isEmpty(exrt)) {
      //     exrt = 1
      //     row.exrt = 1
      //   }
      // //  if (curr == 'VND' && Number(exrt) !== 1) error += "VND tỉ giá phải bằng 1(VND exchange rate must be 1). "
      //  // if (row.btax && !util.checkmst(row.btax.toString())) error += "MST không hợp lệ(Invalid tax code). "
      //  // if (row.bmail && !util.checkemail(row.bmail)) error += "Email không hợp lệ(Invalid email). "
      //   if (row.idt) {
      //     const idt = moment(row.idt, mfd)
      //     if (idt.isValid()) {
      //       row.idt = idt.format(dtf)
      //   //    if (idt.toDate() > now) error += "Ngày hđ lớn hơn ngày hiện tại(The invoice date is greater than the current date). "
      //     }
      //    // else error += "Ngày hđ sai định dạng (The invoice date is not in the correct format). "
      //   }


      //   for (const item of items) {
      //     let erri = ""
      //     item.line = i++
      //     item.type = item.type ? item.type.toString().toUpperCase() : ""
      //     switch (item.type) {
      //       case "KM":
      //         item.type = KM
      //         break
      //       case "CK":
      //         item.type = CK
      //         break
      //       case "MT":
      //         item.type = MT
      //         break
      //       default:
      //         item.type = ""
      //         break
      //     }
      //     //if (VAT && !util.isNumber(item.vrt)) erri += `thiếu thuế suất. `

      //     let p = item.price, q = item.quantity, amt = 0, vmt = 0, vrt = 0
      //     // if (!util.isEmpty(p) && !util.isEmpty(q) && !(Number(item.amount)<0)) item.amount = Number(p) * Number(q)
      //     // else {
      //     //   if (!p) delete item.price
      //     //   if (!q) delete item.quantity
      //     // }
      //     // if (KMMT.includes(item.type)) {
      //     //   item.amount = 0
      //     // }
      //   // else {
      //       if (!util.isEmpty(item.amount)) {
      //         amt = Number(item.amount)
      //         //if (item.type == CK) amt = -amt
      //         sum += amt
      //        // if (VAT && item.vat) {
      //         if (item.vat) {
      //           vmt = Number(item.vat)
      //           vat += vmt
      //           // if (vrt > 0) {
      //           //   vmt = amt * vrt / 100
      //           //   vat += vmt
      //           // }
      //         }
      //       }
      //       //else erri += `thiếu thành tiền. `
      //    // }
      //     vrt = item.vrt
      //     item.total = amt + vmt
      //     //if (VAT) {
      //        if (vrt >= 0) item.vrn = `${vrt}%`
      //        else item.vrn = "\\"
      //       item.vat = vmt
      //      // item.vrt = `${vrt}`
      //    // }
      //    // if (!util.isEmpty(erri)) error += `Lỗi chi tiết ${i}: ${erri}(Error detail ${i}: ${erri}). `
      //   }
      //   total = sum + vat
      //   row.sum = sum
      //   row.total = total
      //   row.sumv = Math.round(sum * Number(exrt))
      //   totalv = Math.round(total * Number(exrt))
      //   row.totalv = totalv
      //   row.word = n2w.n2w(totalv, curr)
      //   //if (VAT) {
      //     row.vat = vat
      //     row.vatv = Math.round(vat * Number(exrt))
      //     row.tax = await tax(items, Number(exrt))
      //  // }
      //   row.stax = taxc
      // //  if (!row.form) row.form = iform
      // //  if (!row.serial) row.serial = iserial
      //   row.error = util.isEmpty(error) ? row.error : error + " " + row.error
      //   row.statusGD = util.isEmpty(error) ? row.statusGD : (row.statusGD == 0 ? 2 : row.statusGD)
      // }

      ret = { data: resultq[0], pos: start }
      if (start == 0) {
        sql = `select count(DISTINCT sid) "total" from s_inv_temp ${where}`
        resultq = await dbs.query(sql, binds)
        ret.total_count = resultq[0][0].total
      }
      return res.json(ret)
    }
    catch (err) {
      next(err)
    }
  },
	  xls: async (req, res, next) => {
    try {
        const fn = "temp/inv_hlv.xlsx"
        let json, rows
        const token = sec.decode(req), taxc = token.taxc, query = req.query, filter = JSON.parse(query.filter),  now = new Date(), NOW = moment(now).format(dtf)     
  
        let where = " where invoice_date between ? and ?", order = "order by id desc", sql, resultq, ret, binds = [new Date(filter.fd), new Date(moment(filter.td).endOf("day"))]
        if (query.filter) {
          let filter = JSON.parse(query.filter), val
          Object.keys(filter).forEach((key) => {
            val = filter[key]
            if (val) {
              if (key == "status") {
                where += ` and status = ?`
                binds.push(val)
              }
              if (key == "type") {
                where += ` and type = ?`
                binds.push(val)
              }
              if (key == "form") {
                where += ` and form = ?`
                binds.push(val)
              }
              if (key == "serial") {
                where += ` and serial = ?`
                binds.push(val)
              }
  
            }
          })
        }
       
        order = `order by invoice_date,sid`
        sql = `select DATE_FORMAT(due_date, "%d/%m/%Y") due_date,id,
        sid,
        policy_number,
       
        paid_amount,
        pay_mode,
        main_product_name,
        proposer_name,
        life_insured_name,
        address,
        email,
        tax_identification,
        DATE_FORMAT(payment_date, "%d/%m/%Y")  payment_date,
        DATE_FORMAT(effective_date, "%d/%m/%Y") effective_date,
        service_code,
        buyer_name,
        tax_code,
        payment_method,
        currency,
        exchange_rate,
        unit,
        price,
        quantity,
        total_vat,
        amount,
        total_amount,
        DATE_FORMAT(invoice_date, "%d/%m/%Y")  invoice_date,
        content,
        service_name,
        (CASE
    WHEN status =2 THEN "Lỗi"
    WHEN status = 0 THEN "Chờ tạo"
    WHEN status = 1 THEN "Đã tạo"
    ELSE "__"
   END) status,
        error,
        parent_id,
        type,
        form,
        serial
         from s_inv_temp ${where} ${order} `
        resultq = await dbs.query(sql, binds)
       rows = resultq[0]
        for (let row of rows) {
          if(row.paid_amount)row.paid_amount =Number(row.paid_amount)
          if(row.exchange_rate)row.exchange_rate =Number(row.exchange_rate)
          if(row.price) row.price = Number(row.price)
          if(row.amount) row.amount = Number(row.amount)
          if(row.total_vat) row.total_vat = Number(row.total_vat)
          if(row.quantity) row.quantity = Number(row.quantity)
          if(row.total_amount) row.total_amount =Number(row.total_amount)
        } 
        json = { table: rows }
        const file = path.join(__dirname, "..", "..", "..", fn)
        const xlsx = fs.readFileSync(file)
        const template = new xlsxtemp(xlsx)
        template.substitute(1, json)
        res.end(template.generate(), "binary")
    }
    catch (err) {
       logger4app.debug("excel"+ err)
       
        next(err)
      
       
    }
},		
xlss: async (req, res, next) => {
  try {
      const fn = "temp/inv_hlv.xlsx"
      let json, rows
      const token = sec.decode(req), taxc = token.taxc, query = req.query, filter = JSON.parse(query.filter)     

      let options = {
        method: 'post',
        url: `${config.api_url}/syn/xls`,
        headers: { 
          'Authorization': token,
          'content-type': 'application/json' 
        },
        data : {fd:moment(filter.fd).format("YYYY-MM-DD"),td:moment(filter.td).format("YYYY-MM-DD"),verify:filter.verify}                    
      };
      
    await axios(options)
        .then(function (response) {
         
             rows = response.data
           
        })
        .catch(function (error) {
            logger4app.debug(error)
            throw new Error(error)
        })     
     
      json = { table: rows }
      const file = path.join(__dirname, "..", "..", "..", fn)
      const xlsx = fs.readFileSync(file)
      const template = new xlsxtemp(xlsx)
      template.substitute(1, json)
      res.end(template.generate(), "binary")
  }
  catch (err) {
     logger4app.debug("excel"+ err)
     
      next(err)
    
     
  }
},						
  sync: async (req, res, next) => {
    try {
      let reqs = req.body, status , count=0 , sql , result, rows=[], binds =[],  token = sec.decode(req)
    
      let options = {
        method: 'post',
        url: `${config.api_url}/syn/sync`,
        headers: { 
          'Authorization': token,
          'content-type': 'application/json' 
        },
        data : {fd:moment(reqs.fd).format("YYYY-MM-DD"),td:moment(reqs.td).format("YYYY-MM-DD")}                    
      };
      
    await axios(options)
        .then(function (response) {
         
             rows = response.data
            if(rows.length <1) {
              status = 0
              count = 0
            }
        })
        .catch(function (error) {
            logger4app.debug(error)
            throw new Error(error)
        })
        if(rows.length > 0){
          for (const item of rows) {
            sql = `select status from s_inv_temp where parent_id =?`,binds = [item.ID]
            result = await dbs.query(sql, binds)
            if(result[0].length==0){
              count++
              status = 1
              binds =[]
              binds.push([item.SID,
                item.POLICY_NUMBER, 
                item.DUE_DATE,
                 item.PAID_AMOUNT,
                  item.PAY_MODE, 
                  item.MAIN_PRODUCT_NAME,
                   item.PROPOSER_NAME,
                    item.LIFE_INSURED_NAME,
                     item.ADDRESS, 
                     item.EMAIL,
                      item.TAX_IDENTIFICATION,
                      item.PAYMENT_DATE, 
                      item.EFFECTIVE_DATE, 
                      item.SERVICE_CODE,
                       item.BUYER_NAME, 
                       item.TAX_CODE,
                       item.PAYMENT_METHOD, 
                       item.CURRENCY,
                        item.EXCHANGE_RATE, 
                        item.UNIT, item.PRICE, 
                        item.QUANTITY, 
                        item.TOTAL_VAT, 
                        item.AMOUNT, 
                        item.TOTAL_AMOUNT,
                        item.INVOICE_DATE,
                         item.CONTENT, 
                         item.SERVICE_NAME,0,item.ID])
              await dbs.query(`insert into s_inv_temp (SID, POLICY_NUMBER, DUE_DATE,
                                PAID_AMOUNT,
                                PAY_MODE,
                                MAIN_PRODUCT_NAME,
                                PROPOSER_NAME,
                                LIFE_INSURED_NAME,
                                ADDRESS,
                                EMAIL,
                                TAX_IDENTIFICATION,
                                PAYMENT_DATE,
                                EFFECTIVE_DATE,
                                SERVICE_CODE,
                                BUYER_NAME,
                                TAX_CODE,
                                PAYMENT_METHOD,
                                CURRENCY,
                                EXCHANGE_RATE,
                                UNIT,
                                PRICE,
                                QUANTITY,
                                TOTAL_VAT,
                                AMOUNT,
                                TOTAL_AMOUNT,
                                INVOICE_DATE,
                                CONTENT,
                                SERVICE_NAME,STATUS,parent_id) VALUES ?`, [binds])
            }else if(result[0][0].status==0){
              count++
              status = 1
             
              await dbs.query(`update s_inv_temp set SID=?, POLICY_NUMBER=?, DUE_DATE=?,
                PAID_AMOUNT=?,
                PAY_MODE=?,
                MAIN_PRODUCT_NAME=?,
                PROPOSER_NAME=?,
                LIFE_INSURED_NAME=?,
                ADDRESS=?,
                EMAIL=?,
                TAX_IDENTIFICATION=?,
                PAYMENT_DATE=?,
                EFFECTIVE_DATE=?,
                SERVICE_CODE=?,
                BUYER_NAME=?,
                TAX_CODE=?,
                PAYMENT_METHOD=?,
                CURRENCY=?,
                EXCHANGE_RATE=?,
                UNIT=?,
                PRICE=?,
                QUANTITY=?,
                TOTAL_VAT=?,
                AMOUNT=?,
                TOTAL_AMOUNT=?,
                INVOICE_DATE=?,
                CONTENT=?,
                SERVICE_NAME=? where parent_id=?`, [item.SID,
                  item.POLICY_NUMBER, 
                  item.DUE_DATE,
                   item.PAID_AMOUNT,
                    item.PAY_MODE, 
                    item.MAIN_PRODUCT_NAME,
                     item.PROPOSER_NAME,
                      item.LIFE_INSURED_NAME,
                       item.ADDRESS, 
                       item.EMAIL,
                        item.TAX_IDENTIFICATION,
                        item.PAYMENT_DATE, 
                        item.EFFECTIVE_DATE, 
                        item.SERVICE_CODE,
                         item.BUYER_NAME, 
                         item.TAX_CODE,
                         item.PAYMENT_METHOD, 
                         item.CURRENCY,
                          item.EXCHANGE_RATE, 
                          item.UNIT, item.PRICE, 
                          item.QUANTITY, 
                          item.TOTAL_VAT, 
                          item.AMOUNT, 
                          item.TOTAL_AMOUNT,
                          item.INVOICE_DATE, 
                           item.CONTENT, 
                           item.SERVICE_NAME,item.ID])
            }
  
          }
        }
        
      res.json({ status: status, count: count})
  }
  catch (err) {
      logger4app.debug(err)
      next(err)
  }
},
  ins: async (req, res, next) => {
    let conn, count=0,count_e=0,rsout=[]
    try {
      conn = await dbs.getConnection()
      const token = sec.decode(req)
      if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
      const taxc = token.taxc, uid = token.uid, ou = token.ou, body = req.body, org = await ous.obt(taxc),  sql = `insert into s_inv set ?`,now = new Date(), NOW = moment(now).format(dtf)
      let Obj = {
        "policy_number": "c0", "due_date": "c1", "service_code": "c2", "pay_mode": "c3", "life_insured_name": "c4", "payment_date": "c5", "effective_date": "c6", "content": "c7", "service_name": "c8", "address": "baddr", "email": "bmail","type": "type","form": "form","serial": "serial"
        , "proposer_name": "bname", "tax_identification": "btax", "buyer_name": "buyer", "currency": "curr", "exchange_rate": "exrt", "sid": "ic"
        , "invoice_date": "idt", "total_amount": "items.total","amount": "items.amount", "main_product_name": "items.name","total_vat": "items.vat", "price": "items.price", "quantity": "items.quantity", "unit": "items.unit", "tax_code": "items.vrt", "payment_method": "paym", "status": "statusGD", "error": "error", "id": "ids.id"
      };
        
     
      //console.time("chk2")
      let binds = [], rs, ecs, rows,invs_=[],invs = body.invs
      let items = [],ids=[]
      for (let r of invs) {
        items = []
        ids = []
        r = objectMapper(r, Obj)
        items.push(r.items)
        ids.push(r.ids)
        r.ids = ids
        r.items =items
        r.error = ''
        invs_.push(r)
      }
      invs = invs_
     
      for (const row of invs) {
        binds.push([row.ic, row.form, row.serial, moment(row.idt, dtf).endOf("day").subtract(1, "seconds").toDate()])
      }
     
      await conn.query("DROP TEMPORARY TABLE IF EXISTS s_tmp")
      await conn.query("DROP TEMPORARY TABLE IF EXISTS s_imp")
      await conn.query("create TEMPORARY table s_tmp(ic varchar(36),idt datetime,form varchar(11),serial varchar(6),seq varchar(7),unique(ic)) ENGINE=MEMORY")
      await conn.query("insert into s_tmp(ic,form,serial,idt) values ?", [binds])
      rs = await conn.query("select a.ic ic from s_tmp a where not exists (select 1 from s_serial where taxc=? and form=a.form and serial=a.serial and status=1 and fd<=a.idt)", [taxc])
      rows = rs[0]
      if (rows.length > 0) {
        ecs = []
        for (const row of rows) {
          ecs.push(row.ic)
        }
        for (let row of invs) {
           row.error=''
          if (ecs.includes(row.ic)) row.error += "Dải số hđ không hợp lệ(Invalid invoice number range). "
        }
      }
      rs = await conn.query("select a.ic ic from s_tmp a where exists (select 1 from s_inv where ic=a.ic)")
      rows = rs[0]
      if (rows.length > 0) {
        ecs = []
        for (const row of rows) {
          ecs.push(row.ic)
        }
        for (let row of invs) {
          if(!row.error) row.error=''
          if (ecs.includes(row.ic)) row.error += "Mã hđ đã tồn tại(Invoice code already exists). "
        }
      }
      await conn.query("create temporary table s_imp ENGINE=MEMORY as select form,serial,max(idt) idt from s_inv where stax=? and status>1 group by form,serial", [taxc])
      rs = await conn.query("select a.ic ic from s_tmp a where exists (select 1 from s_imp where form=a.form and serial=a.serial and idt>=a.idt)")
      rows = rs[0]
      if (rows.length > 0) {
        ecs = []
        for (const row of rows) {
          ecs.push(row.ic)
        }
        for (let row of invs) {
          if(!row.error) row.error=''
          if (ecs.includes(row.ic)) row.error += "Đã có hđ có ngày lớn hơn đã cấp số(Already have a invoice with a larger date that issued numbers). "
        }
      }//check nv
      for (let row of invs) {
       
        if (!row.ic) row.error += "Thiếu mã hóa đơn \n (Missing invoice code)"
        if (row.curr == 'VND' && Number(row.exrt) !== 1) row.error += "VND tỉ giá phải bằng 1(VND exchange rate must be 1). "
        if (row.btax && !util.checkmst(row.btax.toString())) row.error += "MST không hợp lệ(Invalid tax code). "
        if (row.bmail && !util.checkemail(row.bmail)) row.error += "Email không hợp lệ(Invalid email). "
        if (row.idt) {
          const idt = moment(row.idt, dtf)
          if (idt.isValid()) {
            row.idt = idt.format(dtf)
            if (idt.toDate() > now) row.error += "Ngày hđ lớn hơn ngày hiện tại(The invoice date is greater than the current date). "
          }
          else row.error += "Ngày hđ sai định dạng (The invoice date is not in the correct format). "
        }
        if(![ "TM", "CK","TM/CK","DTCN"].includes(row.paym)) row.error += `hình thức thanh toán thuộc các giá trị : TM; CK; TM/CK;DTCN. `
        for (const item of row.items) {
          if (util.isEmpty(item.amount)) {
            row.error += `thiếu thành tiền. `
          }
         try{
          if(![10,5,0,-1,-2].includes(Number(item.vrt)))  row.error += `tax code không hợp lệ(Invalid tax code) `
          } catch (e) {
            row.error += `tax code không hợp lệ(Invalid tax code) `
          }
        
        }
      }

      for (let inv of invs) {
        let ic, id, sec, form, serial, type, pid, idt, seq, s7, rs, id_tem = inv.id

        try {
          if (util.isEmpty(inv.error) && inv.statusGD == 0) {
            form = inv.form
            serial = inv.serial
            type = form.substr(0, 6)
            ic = inv.ic
            // pid = inv.pid

            idt = moment(inv.idt, dtf)
            id = await redis.incr("INC")
            sec = util.generateSecCode(config.FNC_GEN_SEC,id)
            inv.id = id
            inv.sec = sec
            inv.type = type
            inv.name = util.tenhd(type)
            inv.stax = taxc
            inv.sname = org.name
            inv.saddr = org.addr
            inv.smail = org.mail
            inv.stel = org.tel
            inv.taxo = org.taxo
            inv.sacc = org.acc
            inv.sbank = org.bank
            inv.c5=moment(inv.c5).format('YYYY-MM-DD')
            inv.c6=moment(inv.c6).format('YYYY-MM-DD')
            inv.c1=moment(inv.c1).format('YYYY-MM-DD')
            inv.status = 1
            let p = inv.items[0].price, q = inv.items[0].quantity, amt = 0, vmt = 0, vrt = 0
            if (inv.items[0].vrt >= 0) inv.items[0].vrn = `${vrt}%`
            else inv.items[0].vrn = "\\"
            inv.items[0].vat=  Number((inv.items[0].vat?inv.items[0].vat:0))
            inv.items[0].amount = Number(inv.items[0].amount)
           // inv.items[0].total =  Number(inv.items[0].amount)  + Number((inv.items[0].vat?inv.items[0].vat:0))
            
             inv.sum = Number(inv.items[0].amount)
             inv.total = Number(inv.items[0].total)
             inv.sumv = Math.round(Number(inv.sum)  * Number(inv.exrt))
             let totalv = Math.round(Number(inv.total) * Number(inv.exrt))
             inv.totalv = totalv
             inv.word = n2w.n2w(Number(totalv), inv.curr)
       
             inv.vat = Number(inv.items[0].vat?inv.items[0].vat:0)
             inv.vatv = Math.round(Number(inv.items[0].vat?inv.items[0].vat:0) * Number(inv.exrt))
             inv.tax = await tax(inv.items, Number(inv.exrt))
     
            if (inv.sum < 0) {
              rs = await conn.query("select * from s_inv where c0 =? and status=3 and idt<=? order by idt desc", [inv.c0, inv.idt])
              let sum = 0,vat=0,total=0,tmp_v,tmp_s,tmp_t
              rows = rs[0]
              if (rows.length > 0) {
                ecs = []
                for (const row of rows) {
                  sum = sum + Number(row.sumv)
                  vat = vat + Number(row.vat?row.vat:0)
                  total = total + Number(row.total)
                  ecs.push(row.id)
                  if (sum >= Math.abs(inv.sum)) break;
                }
                if (sum >= Math.abs(inv.sum * -1)) {
                  for (const id of ecs) {
                    //update cho huy
                    await conn.query("update s_inv set doc=JSON_SET(doc,'$.status',6) where id=?", [id])
                  }
                  // tao hoa don moi
                  tmp_s=inv.sum
                  tmp_v = inv.vat
                  tmp_t = inv.total
                  inv.total = total - Math.abs(tmp_t)
                  inv.sumv = sum - Math.abs(tmp_s)
                  inv.sum = sum - Math.abs(tmp_s)
                  inv.totalv= total - Math.abs(tmp_t)
                  inv.vat = vat - Math.abs(tmp_v?tmp_v:0)
                  inv.vatv = vat - Math.abs(tmp_v?tmp_v:0)
                  inv.items[0].total=sum - Math.abs(tmp_s)
                  inv.items[0].amount=sum - Math.abs(tmp_s)
                  inv.items[0].vat = vat - Math.abs(tmp_v?tmp_v:0) 
                  inv.items[0].price =Math.abs(inv.items[0].price)
                  inv.word = n2w.n2w(inv.totalv, inv.curr)
                  inv.tax = await tax(inv.items, Number(inv.exrt))
                  if(inv.sum > 0){
                    await dbs.query(sql, { id: id, sec: sec, ic: ic, idt: idt.toDate(), ou: ou, uc: uid, doc: JSON.stringify(inv) })
                  }
                 
                  count++
                  for (const id of inv.ids) {
                    if(inv.sum > 0 ){
                      await conn.query("update s_inv_temp set status=1,type=?,form=?,serial=?  where id=?", [inv.type,inv.form,inv.serial,id.id])
                    }else{
                      await conn.query("update s_inv_temp set status=1 where id=?", [id.id])
                    }
                  }
                } else {
                  //loi do ko tim du hoa don co so tien lơn hon
                  for (const id of inv.ids) {
                    await conn.query("update s_inv_temp set status=2,error=? where id=?", ["Tổng tiền các hóa đơn có số hợp đồng cần hủy,nhỏ hơn hóa đơn hiện tại", id.id])
                  }
                  count_e++
                }
              }else {
                //loi do ko tim du hoa don co so tien lơn hon
                for (const id of inv.ids) {
                  await conn.query("update s_inv_temp set status=2,error=? where id=?", ["Tổng tiền các hóa đơn có số hợp đồng cần hủy,nhỏ hơn hóa đơn hiện tại", id.id])
                }
                count_e++
              }
            } else {
              rs = await dbs.query(sql, { id: id, sec: sec, ic: ic, idt: idt.toDate(), ou: ou, uc: uid, doc: JSON.stringify(inv) })
              count++
              for (const id of inv.ids) {
                await conn.query("update s_inv_temp set status=1,type=?,form=?,serial=? where id=?", [inv.type,inv.form,inv.serial,id.id])
              }
            }

          } else {
            // update doi voi giao dịch loi
            if (!util.isEmpty(inv.error)) {
              for (const id of inv.ids) {
                await conn.query("update s_inv_temp set status=2,error=? where id=?", [inv.error, id.id])
              }
              count_e++
            }

          }


        } catch (e) {
          throw e
        }

      }
      rsout.push({"count_e":count_e,"count":count})
     
      res.json(rsout)
    }
    catch (err) {
      next(err)
    }
  },


}
module.exports = Service 