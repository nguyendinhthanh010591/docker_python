"use strict"
const redis = require("../redis")
const dbs = require("./dbs")
const config = require("../config")
const logger4app = require("../logger4app")
const logging = require("../logging")
const Service = {
    get: async (req, res, next) => {
        try {
            const type = req.params.type.toUpperCase()
            const sql = `select id "id",name "name", des "des" from s_cat where type=?`
            const result = await dbs.query(sql, [type])
            res.json(result[0])
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            let body = req.body, binds, sql, result, operation = body.webix_operation, sSysLogs = { fnc_id: '', src: config.SRC_LOGGING_DEFAULT, dtl: ``}
            switch (operation) {
                case "update":
                    sql = "update s_cat set name=?,des=? where id=?"
                    binds = [body.name, body.des, body.id]
                    sSysLogs = { fnc_id: 'cat_upd', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật danh mục ${body.type} (${operation})`, msg_id: body.id, doc: JSON.stringify(body) };
                    break
                case "insert":
                    sql = "insert into s_cat(name,type) values (?,?)"
                    binds = [body.name, body.type.toUpperCase()]
                    sSysLogs = { fnc_id: 'cat_ins', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm mới danh mục ${body.type} (${operation})`, msg_id: body.id, doc: JSON.stringify(body) };
                    break
                case "delete":
                    sql = "delete from s_cat where id=?"
                    binds = [body.id]
                    sSysLogs = { fnc_id: 'cat_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa danh mục ${body.type} (${operation})`, msg_id: body.id, doc: JSON.stringify(body) };
                    break
                default:
                    throw new Error(`${operation} là hoạt động không hợp lệ \n (${operation} is invalid operation)`)
            }
            result = await dbs.query(sql, binds)
            logging.ins(req, sSysLogs, next)
            if (operation == "insert") res.json({ id: result[0].insertId })
            else res.json(result[0].affectedRows)
        }
        catch (err) {
            next(err)
        }
    },
    cache: (type, key) => {
        return new Promise(async (resolve, reject) => {
            try {
                let result, json, sql = `select name "id",name "value" from s_cat where type=? order by name`, binds = []
                switch (type) {
                    case "PROV":
                        sql = `select id "id",name "value" from s_loc where pid is null order by name`
                        break
                    case "ROLE":
                        sql = `select id "id",name "value" from s_role where active = 1 order by id`
                        break
                    case "TAXO":
                        sql = `select id "id",name "value" from s_taxo where id like ? order by id`
                        binds.push("%00")
                        break
                    default:
                        binds.push(type)
                }
                result = await dbs.query(sql, binds)
                json = JSON.stringify(result[0])
                await redis.set(key, json)
                resolve(result[0])
            }
            catch (err) {
                reject(err)
            }
        })
    },
    kache: async (req, res, next) => {
        try {
            let type = req.params.type.toUpperCase(), id = req.params.id, key = type + "." + id, data
            //data = await redis.get(key)
            //if (data) return res.json(JSON.parse(data))
            let sql, result
            switch (type) {
                case "LOCAL":
                    sql = `select id "id",name "value" from s_loc where pid=?`
                    result = await dbs.query(sql, [id])
                    break
                case "TAXO":
                    sql = `select id "id",name "value" from s_taxo where id like ?`
                    result = await dbs.query(sql, [`${id.substr(0, 3)}%`])
                    break
                default:
                    throw new Error(`${type} khóa không hợp lệ \n (${type} is invalid key)`)
            }
            const rows = result[0]
            //await redis.set(key, JSON.stringify(rows))
            res.json(rows)
        }
        catch (err) {
            next(err)
        }
    },
    nokache: async (req, res, next) => {
        try {
            const type = req.params.type.toUpperCase()
            let sql, result
            switch (type) {
                case "GNS":
                    sql = `select id "id",name "value",unit "unit",price "price" from s_gns order by name`
                    result = await dbs.query(sql)
                    break
                case "OU":
                    sql = `select id "id",name "value" from s_ou order by name`
                    result = await dbs.query(sql)
                    break
                default:
                    throw new Error(`${type} khóa không hợp lệ \n (${type} is invalid key)`)
            }
            res.json(result[0])
        }
        catch (err) {
            next(err)
        }
    }
}
module.exports = Service   