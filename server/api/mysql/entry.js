"use strict"
const moment = require('moment');
const { bind } = require("file-loader")
const config = require("../config")
const logger4app = require("../logger4app")
const util = require("../util")
const dbs = require("./dbs")
const Service= {
    post: async(req,res,next)=>{
        try{
            let rows=req.body.arr, binds=[], obj=[],sql, i=0,j=1, values=req.body.values
            //Gans gia tri
            for(const row of rows){
                obj[i++]=row.c2
                obj[i++]=null
                obj[i++]=row.total
                obj[i++]=row.ic
                obj[i++]=row.systemcode
                obj[i++]=row.bcode
                obj[i++]=null
                obj[i++]=null
                obj[i++]=j++
                obj[i++]=row.curr
                obj[i++]=null
                obj[i++]=null
                obj[i++]=(moment(row.c3).toDate())
                obj[i++]=values.module
            }
            for(const row of obj){
                logger4app.debug(row)
                binds.push(row)
            }
            //Cau lenh sql
            sql="insert into S_TRANS_CAN(TRAN_NO,NOTE,TOTAL,SID,SYSTEMCODE,BCODE,C4,BRANCHCODE,STT,CURR,UC,MA_KS,TRANSDATE,STATUS,MODULE) values (:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11,:12,:13,'0',:14)"
            await dbs.query(sql,binds)
            res.json(1)
            
        }
        catch(err){
            next(err)
        }
    }
}


module.exports=Service