"use strict"
const moment = require("moment")
const bcrypt = require("bcryptjs")
const jwt = require("jsonwebtoken")
const config = require("../config")
const logger4app = require("../logger4app")
const util = require("../util")
const inc = require("../inc")
const ous = require("./ous")
const rsmq = require("../rsmq")
const sec = require("../sec")
const dbs = require("./dbs")
const inv = require(`./inv`)
const handlebars = require("handlebars")
const ext = require("../ext")
const seasecret = config.seasecret
const seaexpires = config.seaexpires
const rbi = (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            let doc = await inv.invdocbid(id)
            resolve(doc)
        } catch (err) {
            reject(err)
        }
    })
}
const Service = {
    login: async (req, res, next) => {
        try {
            await util.chkCaptcha(req)
            const query = req.query, taxc = query.username,ENT =query.ENT
            const sql = `select pwd "pwd" from s_org where taxc=? and pwd is not null`
            const result = await dbs.query(sql, [taxc])
            const rows = result[0]
            if(ENT=='en'){
                if (rows.length == 0) throw new Error(`Không tìm thấy tài khoản ${taxc}`)
            }else{
                if (rows.length == 0) throw new Error(`Account not found ${taxc}`)
            }
           
            const pwd = rows[0].pwd
            if(ENT=='en'){
                if (!bcrypt.compareSync(query.password, pwd)) throw new Error(`Incorrect password`)
            }else{
                if (!bcrypt.compareSync(query.password, pwd)) throw new Error(`Mật khẩu không đúng`)
            }
           
            const json = { taxc: taxc }
            jwt.sign(json, seasecret, { expiresIn: seaexpires }, (err, token) => {
                if (err) return next(err)
                json.token = token
                json.expires = new Date(Date.now() + seaexpires * 1000)
                return res.json(json)
            })
        } catch (err) {
            next(err)
        }
    },
    change: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, query = req.query,ENT=query.ENT
            let sql, result, rows, pwd
            sql = `select pwd "pwd" from s_org where taxc=? and pwd is not null`
            result = await dbs.query(sql, [taxc])
            rows = result[0]
            if(ENT=='en'){
                if (rows.length == 0) throw new Error(`No account found ${taxc}`)
            }else{
                if (rows.length == 0) throw new Error(`Không tìm thấy tài khoản ${taxc}`)
            }
           
            pwd = rows[0].pwd
            if(ENT=='en'){
                if (!bcrypt.compareSync(query.pwd1, pwd)) throw new Error(`Old password is incorrect`)
            }else{
                if (!bcrypt.compareSync(query.pwd1, pwd)) throw new Error(`Mật khẩu cũ không đúng`)
            }
            
            pwd = util.bcryptpwd(query.pwd2)
            sql = `update s_org set pwd=? where taxc=?`
            result = await dbs.query(sql, [pwd, taxc])
            res.json(result[0].affectedRows)
        } catch (err) {
            next(err)
        }
    },
    reset: async (req, res, next) => {
        try {
            await util.chkCaptcha(req)
            const query = req.query, taxc = query.taxc.trim(), mail = query.mail.trim(),ENT =query.ENT
            let sql, result, rows
            sql = `select mail "mail" from s_org where taxc=? and lower(mail) like ?`
            result = await dbs.query(sql, [taxc, `%${mail.toLowerCase()}%`])
            rows = result[0]
            if(ENT=='en'){
                if (rows.length == 0) throw new Error(`Account ${taxc} Mail ${mail} does not exist`)
            }else{
                if (rows.length == 0) throw new Error(`Tài khoản ${taxc} Mail ${mail} không tồn tại`)
            }
         
            const pwd = util.pwd()
            sql = "update s_org set pwd=? where taxc=?"
            result = await dbs.query(sql, [util.bcryptpwd(pwd), taxc])
            let subject = "Mật khẩu đăng nhập hệ thống hóa đơn điện tử"

            let html
            try {
                const source = await util.tempmail(2)
                const template = handlebars.compile(source)
                const obj = { taxc: taxc, pwd: pwd }
                html = template(obj)
            } catch (error) {
                html = `- Tài khoản : ${taxc}<br>- Mật khẩu: ${pwd}`
            }

            //let html = `- Tài khoản : ${taxc}<br>- Mật khẩu: ${pwd}`
            if (ENT == 'en') {
                subject = "Password to login Invoice system"
                //html = `- Account : ${taxc}<br>- Password: ${pwd}`
            }
          
            const content = { from: config.mail, to: mail, subject: subject, html: html }
            if (config.dbCache)
                await inc.inserttempmail(content)
            else
                await rsmq.sendMessageAsync({ qname: config.qname, message: JSON.stringify(content) })
            if(ENT=='en'){
                res.send(`Account password ${taxc} has been sent to ${mail}`)
            }else{
                res.send(`Mật khẩu của tài khoản ${taxc} đã được gửi đến ${mail}`)
            }
           
        } catch (err) {
            next(err)
        }
    },
    xml0: async (req, res, next) => {
        try {
            const token = sec.decode(req)
            const id = req.params.id,ENT= req.headers.ent
            const sql = `select xml "xml" from s_inv where id=? and btax=? and status in (3,4)`
            const result = await dbs.query(sql, [id, token.taxc])
            const rows = result[0]
            if (ENT=='en'){
                if (rows.length == 0) throw new Error(`No invoice found ${id}`)
            }else{
                if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn ${id}`)
            }
           
            const row = rows[0]
            res.json({ xml: row.xml })
        }
        catch (err) {
            next(err)
        }
    },
    search0: async (req, res, next) => {
        try {
            const token = sec.decode(req)
            const id = req.params.id,ENT= req.headers.ent
            const sql = `select pid "pid",cid "cid",cde "cde",adjtyp "adjtyp",adjdes "adjdes",status "status",doc "doc",xml "xml" from s_inv where id=? and ${(!config.grant_search) ? `btax = ?` : `btax like ?`} and status in (3,4)`
            const result = await dbs.query(sql, [id, (!config.grant_search) ? token.taxc : `${token.taxc}%`])
            const rows = result[0]
            if (ENT=='en'){
                if (rows.length == 0) throw new Error(`No invoice found ${id}`)
            }else{
                if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn ${id}`)
            }
           
            const row = rows[0], doc = row.doc, org = await ous.obt(doc.stax), idx = org.temp, fn = util.fn(doc.form, idx), tmp = await util.template(fn)
            //const htm = await hbs.j2h(doc, idx), pdf = await util.pdf(htm)
            //res.json({ htm: htm, pdf: `data:application/pdf;base64,${pdf.toString("base64")}`, xml: row.xml, adjtyp: row.adjtyp, adjdes: row.adjdes, pid: row.pid, cid: row.cid, cde: row.cde, status: row.status })
            let obj ={ status: doc.status, doc: doc, tmp: tmp }
            let reqjsr = ext.createRequest(obj)
            // res.json({ status: status, doc: doc, tmp: tmp })
             const bodyBuffer = await  util.jsReportRenderAttach(reqjsr)
             let sizeInByte = bodyBuffer.byteLength  
            res.json({pdf: `data:application/pdf;base64, ${bodyBuffer.toString('base64')}`, sizePDF: sizeInByte,adjtyp: row.adjtyp, adjdes: row.adjdes, pid: row.pid, cid: row.cid, cde: row.cde, status: row.status })
        }
        catch (err) {
            next(err)
        }
    },
    xml1: async (req, res, next) => {
        try {
            await util.chkCaptcha(req)
            const query = req.query, sec = query.sec,ENT= req.headers.ent
            const result = await dbs.query(`select xml "xml" from s_inv where sec=? and status in (3,4)`, [sec])
            const rows = result[0]
            if (ENT=='en'){
                if (rows.length == 0) throw new Error(`Could not find invoice for code: ${sec}`)
            }else{
                if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn mã tra cứu: ${sec}`)
            }
          
            const row = rows[0]
            res.json({ xml: row.xml })
        }
        catch (err) {
            next(err)
        }
    },
    search1: async (req, res, next) => {
        try {
            await util.chkCaptcha(req)
            const query = req.query, sec = query.sec,ENT= req.headers.ent
            const result = await dbs.query(`select id "id",pid "pid",cid "cid",cde "cde",adjtyp "adjtyp",adjdes "adjdes",status "status",doc "doc" from s_inv where sec=? and status in (3,4)`, [sec])
            const rows = result[0]
            if(ENT=='en'){
                if (rows.length == 0) throw new Error(`Invoice not found for code: ${sec}`)
            }else{
                if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn mã tra cứu: ${sec}`)
            }
            
            const row = rows[0], doc = await rbi(row.id), org = await ous.obt(doc.stax), idx = org.temp, fn = util.fn(doc.form, idx), tmp = await util.template(fn)
            let obj ={ status: doc.status, doc: doc, tmp: tmp }
            let reqjsr = ext.createRequest(obj)
            // res.json({ status: status, doc: doc, tmp: tmp })
             const bodyBuffer = await  util.jsReportRenderAttach(reqjsr)
             let sizeInByte = bodyBuffer.byteLength   
            
            res.json({  pdf: `data:application/pdf;base64, ${bodyBuffer.toString('base64')}`, sizePDF: sizeInByte, adjtyp: row.adjtyp, adjdes: row.adjdes, pid: row.pid, cid: row.cid, cde: row.cde, status: row.status })
        }
        catch (err) {
            next(err)
        }
    },
    search2: async (req, res, next) => {
        try {
            const token = sec.decode(req), query = req.query, filter = JSON.parse(query.filter)
            const sort = query.sort, start = query.start ? query.start : 0, count = query.count ? query.count : 10
            let order, val, sql, where, result, binds, ret
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = `order by ${key} ${sort[key]}`
                })
            }
            else order = " order by id desc"
            where = `where idt between ? and ? and ${(!config.grant_search) ? `btax = ?` : `btax like ?`} and status in (3,4)`
            if (config.ent == "lge") {
                if (filter.paym == "*") {
                    where += ` and paym in('CK','TM','FOC','TM/CK','DTCN')`
                }else where += ` and paym in('${filter.paym}')`
            }
            binds = [new Date(filter.fd), new Date(moment(filter.td).endOf("day")), (!config.grant_search) ? token.taxc : `${token.taxc}%`]
            for (const key of Object.keys(filter)) {
                val = filter[key]
                if (val) {
                    switch (key) {
                        case "fd":
                            break
                        case "td":
                            break
                        case "paym":
                            break
                        default:
                            where += ` and ${key} like ?`
                            binds.push(`%${val}%`)
                    }
                }
            }
            sql = `select sec "sec",pid "pid",cid "cid",id "id",idt "idt",status "status",type "type",form "form",serial "serial",seq "seq",stax "stax",sname "sname",saddr "saddr",stel "stel",smail "smail",sacc "sacc",sbank "sbank",btax "btax",bname "bname",buyer "buyer",baddr "baddr",btel "btel",bmail "bmail",bacc "bacc",bbank "bbank",note "note",sumv "sumv",vatv "vatv",totalv "totalv",CASE paym WHEN 'FOC' THEN 'Không thu tiền 'ELSE paym END  "paym",adjdes "adjdes",cde "cde" from s_inv ${where} ${order} LIMIT ${count} OFFSET ${start}`
            result = await dbs.query(sql, binds)
            ret = { data: result[0], pos: start }
            if (result[0].length > 0) {
                if (start == 0) {
                    sql = `select count(*) "total" from s_inv ${where}`
                    result = await dbs.query(sql, binds)
                    ret.total_count = result[0][0].total
                }
                res.json(ret)
            }else{
                throw new Error("Không tìm thấy hóa đơn \n (Cannot find Invoice)")
            }
        }
        catch (err) {
            next(err)
        }
    },
    xlssearch: async (req, res, next) => {
        try {
            const token = sec.decode(req), query = req.query, filter = JSON.parse(query.filter),ENT= req.headers.ent
            let order, val, sql, where, result, binds
            order = "order by id", where = "where idt between ? and ? and btax=? and status in (3,4)"
            binds = [new Date(filter.fd), new Date(moment(filter.td).endOf("day")), token.taxc]
            for (const key of Object.keys(filter)) {
                val = filter[key]
                if (val) {
                    switch (key) {
                        case "fd":
                            break
                        case "td":
                            break
                        default:
                            where += ` and ${key} like ?`
                            binds.push(`%${val}%`)
                    }
                }
            }
            if (ENT=='en'){
                sql = `select sec "sec",pid "pid",cid "cid",id "id",DATE_FORMAT(idt,'%d/%m/%Y') "idt",
                case
                    when status = 3 then 'Approved'
                    when status = 4 then 'Cancelled'
                end "status",type "type",form "form",serial "serial",seq "seq",stax "stax",sname "sname",saddr "saddr",stel "stel",smail "smail",sacc "sacc",sbank "sbank",btax "btax",bname "bname",buyer "buyer",baddr "baddr",btel "btel",bmail "bmail",bacc "bacc",bbank "bbank",note "note",sumv "sumv",vatv "vatv",totalv "totalv",adjdes "adjdes",cde "cde" from s_inv ${where} ${order}`
            }else{
                sql = `select sec "sec",pid "pid",cid "cid",id "id",DATE_FORMAT(idt,'%d/%m/%Y') "idt",
                case
                    when status = 3 then 'Đã duyệt'
                    when status = 4 then 'Đã hủy'
                end "status",type "type",form "form",serial "serial",seq "seq",stax "stax",sname "sname",saddr "saddr",stel "stel",smail "smail",sacc "sacc",sbank "sbank",btax "btax",bname "bname",buyer "buyer",baddr "baddr",btel "btel",bmail "bmail",bacc "bacc",bbank "bbank",note "note",sumv "sumv",vatv "vatv",totalv "totalv",adjdes "adjdes",cde "cde" from s_inv ${where} ${order}`
            }
           
            result = await dbs.query(sql, binds)
            res.json(result[0])
        }
        catch (err) {
            next(err)
        }
    }
}
module.exports = Service