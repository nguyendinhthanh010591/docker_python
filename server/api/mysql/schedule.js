"use strict"
const moment = require("moment")
var uuid = require('uuid');
const dbs = require("./dbs")
const exch = require("./exch")
const fxp = require("fast-xml-parser")
const Parser = fxp.j2xParser
const Json2csvParser = require("json2csv").Parser;
const inv = require("./inv")
const util = require("../util")
const ftp = require("../ftp")
const email = require("../email")
const config = require("../config")
const logger4app = require("../logger4app")
const objectMapper = require("object-mapper")
const sms = require("../sms")
const ads = require("../ads")
const report = require("./report")
const groups = require("./groups")
const redis = require("../redis")
const path = require("path")
const fs = require("fs")
const dtf = config.dtf
const xlsxtemp = require("xlsx-template")
const sql_del_exp = {
    sql_del_exp_mssql: `DELETE FROM s_listvalues WHERE keyname=@1 AND getdate() > TRY_CAST(keyvalue as datetime)`,
    sql_del_exp_mysql: `DELETE FROM s_listvalues WHERE keyname=?`,
    sql_del_exp_orcl: `DELETE FROM s_listvalues WHERE keyname=:1 AND SYSDATE > TO_DATE(keyvalue, 'YYYY-MM-DD HH24:MI:SS')`,
    sql_del_exp_pgsql: `DELETE FROM s_listvalues WHERE keyname=$1`,
}
const sql_del = {
    sql_del_mssql: `DELETE FROM s_listvalues WHERE keyname=@1`,
    sql_del_mysql: `DELETE FROM s_listvalues WHERE keyname=?`,
    sql_del_orcl: `DELETE FROM s_listvalues WHERE keyname=:1`,
    sql_del_pgsql: `DELETE FROM s_listvalues WHERE keyname=$1`,
}
const sql_set_ins = {
    sql_set_ins_mssql: `INSERT INTO s_listvalues (keyname, keyvalue, keytype) VALUES(@1, @2, 'String')`,
    sql_set_ins_mysql: `INSERT INTO s_listvalues (keyname, keyvalue, keytype) VALUES(?, ?, 'String')`,
    sql_set_ins_orcl: `INSERT INTO s_listvalues (keyname, keyvalue, keytype) VALUES(:1, :2, 'String')`,
    sql_set_ins_pgsql: `INSERT INTO s_listvalues (keyname, keyvalue, keytype) VALUES($1, $2, 'String')`,
}

const Service = {
    delInvNotSeq : async (req, res, next) => {
        // try {
        //     let sql, result, binds
        //     sql = `delete from s_inv a where  status=1 and idt < ? 
        //     and exists(select 1 from s_serial where a.stax=taxc and a.form=form and a.serial=serial and uses=?)`
        //     let yestday = moment(new Date).subtract(1,"days").endOf("day").toDate()
        //     binds = [yestday,1]
        //     result = await dbs.query(sql, binds)
        //     // result = await dbs.query(sql, binds)
        //     if(result.affectedRows) logger4app.debug("Đã xóa", result.affectedRows)   
        // } catch (err) {
        //     next(err)
        // }
    },
    // sendEmailApprInv : async (req, res, next) => {
    //     // try {
    //     //     let sql, result, binds
    //     //     sql = `delete from s_inv a where  status=1 and idt < ? 
    //     //     and exists(select 1 from s_serial where a.stax=taxc and a.form=form and a.serial=serial and uses=?)`
    //     //     let yestday = moment(new Date).subtract(1,"days").endOf("day").toDate()
    //     //     binds = [yestday,1]
    //     //     result = await dbs.query(sql, binds)
    //     //     // result = await dbs.query(sql, binds)
    //     //     if(result.affectedRows) logger4app.debug("Đã xóa", result.affectedRows)   
    //     // } catch (err) {
    //     //     next(err)
    //     // }
    // },
    sendEmailApprInv : async (pkey, ptimeout) => {
        //Goi ham check va update trang thai job dang chay hay khong
        if (config.disable_worker) return
        let vcheck = await Service.UpdateRunningJob(pkey, 0, ptimeout)
        logger4app.debug('sendEmailApprInv','vcheck',vcheck)
        //Neu co job dang chay (vcheck = 0) thi thoat ra, khong thuc hien lenh tiep theo
        if (!vcheck) return
        try {
            let sql, result, binds, rows
            sql = `SELECT id, invid, doc, dt, status, xml FROM s_invdoctemp WHERE status = 0 order by id, dt LIMIT ${config.MAX_ROW_JOB_GET} OFFSET 0 `
            binds = []
            result = await dbs.query(sql, binds)
            rows = result[0]
            for (const row of rows) {
                try {
                    let doc = row.doc, xml = row.xml 
                    if (row.invid == 0) {
                        //Voi truong hop invid = 0 thi gui mail luon, noi dung gui mail la truong doc da insert vao
                        await email.sendfromdoctemp(JSON.stringify(doc))
                    } else {//Neu invid khac 0 thi dung ham gui mail hoa don
                        //Check xem co du thong tin hoa don khong, neu khong du thi truy van bang s_inv lay thong tin hoa don
                        const vcheckinvinfo = (doc.stax) && (doc.form) && (doc.serial) && (doc.seq) && (doc.status) && (doc.bmail)
                        if (!vcheckinvinfo) doc = await inv.invdocbid(row.invid)
                        //Quét để FTP hoặc gửi mail
                        if (config.useFtpAttach) {
                            await ftp.uploadattach(doc, xml)
                        }
                        if (!util.isEmpty(doc.bmail)) {
                            //Cập nhật ngày và trạng thái gửi mail
                            let mailstatus = await email.senddirectmail(doc, xml)
                            if (mailstatus) await inv.updateSendMailStatus(doc)
                        }
                    }
                    //Cập nhật trạng thái bảng tạm
                    sql = `UPDATE s_invdoctemp SET status=1, iserror = ?, error_desc = ? WHERE id=?`
                    binds = [0, '', row.id]
                    await dbs.query(sql, binds)
                } catch (err) {
                    let err_mes = `sendEmailApprInv ${row.id}: ${err}` 
                    logger4app.debug(err_mes)
                    //Cập nhật trạng thái bảng tạm trường hợp xử lý lỗi
                    sql = `UPDATE s_invdoctemp SET status=1, iserror = ?, error_desc = ? WHERE id=?`
                    binds = [1, err_mes, row.id]
                    await dbs.query(sql, binds)
                }
            }
            
        } catch (err) {
            logger4app.debug(`sendEmailApprInv `,err)
        }
        vcheck = await Service.UpdateRunningJob(pkey, 1)
    },
    delTempDocMail : async (req, res, next) => {
        try {
            let sql, result, binds, rows
            sql = `DELETE FROM s_invdoctemp WHERE dt <= DATE_ADD(current_timestamp, INTERVAL -${config.MAX_DATE_TO_KEEP_TEMP_ROW} day) and status = 1 and iserror = 0`
            binds = []
            await dbs.query(sql, binds)
        } catch (err) {
            logger4app.debug(`delTempDocMail `,err)
        }
    },
    dailymailhsy: async (req, res, next) => {
        try {
            //Lay tham so
            //Lấy ra các ou
            let result, sql, rows
            let date = new Date()
            let vfd = date.setDate((new Date()).getDate() - 1), vtd = date.setDate((new Date()).getDate() - 1) 
            //let vfd = '2010-09-01', vtd = '2020-09-30'
            const fm = moment(vfd), tm = moment(vtd)
            const fd = fm.startOf("day").format("YYYY-MM-DD HH:mm:ss"), td = tm.endOf("day").format("YYYY-MM-DD HH:mm:ss")
            sql = `select taxc from s_ou where taxc is not null`// and taxc = '0305774706'`
            result = await dbs.query(sql, [])
            rows = result[0]

            //Lặp với từng MST
            let attachs = []
            for (let row of rows) {
                req = {}
                let query = {}

                query.stax = row.taxc
                query.fd = fd
                query.td = td
                req.query = query

                //Tao file
                let fileInvoiceRep = await report.getInvoiceRep(req, res, next)
                let fileInvoiceSum = await report.getInvoiceSum(req, res, next)

                let fileInvoiceRepName = `Daily Sale by Invoice-${row.taxc}-${moment(vfd).format("YYYY-MM-DD")}.xlsx`
                let fileInvoiceSumName = `Daily Sale Summary-${row.taxc}-${moment(vfd).format("YYYY-MM-DD")}.xlsx`

                attachs.push({ filename: fileInvoiceRepName, content: fileInvoiceRep, encoding: 'base64' })
                attachs.push({ filename: fileInvoiceSumName, content: fileInvoiceSum, encoding: 'base64' })
            }

            //Gui mail
            let mail = config.ToDailyMail
            const arr = mail.trim().split(/[ ,;]+/).map(e => e.trim())
            let subject = config.subjectDailyMail
            let html = config.htmlDailyMail
            subject = subject.replace("#dt#", `${fm.startOf("day").format("DD/MM/YYYY")}`)
            html = html.replace("#dt#", `${fm.startOf("day").format("DD/MM/YYYY")}`)

            const content = { from: config.mail, to: arr, subject: subject, html: html, attachments: attachs }
            await rsmq.sendMessageAsync({ qname: config.qname, message: JSON.stringify(content) })

        } catch (err) {
            logger4app.debug(`dailymailhsy `, err)
        }
    },
    pwdExpirationReminder : async (req, res, next) => {
        if (!config.is_use_local_user) return
        let  binds, sql, data
        let days_valid
        try {
            days_valid = Number(config.days_change_pass) - Number(config.days_warning_change_pass)
        } catch (err) {
            days_valid = 80
        }
        sql = `SELECT id, mail, change_pass_date, create_date, DATEDIFF(CURRENT_TIMESTAMP, IFNULL(change_pass_date, create_date)) day_diff FROM s_user WHERE uc = 1 AND local = 1 and DATEDIFF(CURRENT_TIMESTAMP, IFNULL(change_pass_date, create_date)) >= ?`
        binds = [days_valid]
        data = await dbs.query(sql, binds)
        const rows = data[0]
        for (let row of rows) {
            try {
                let uid = row.id
                let mail = row.mail
                let days_left = Number(config.days_change_pass) - Number(row.day_diff)
                //Khóa tài khoản nếu quá hạn
                if (days_left < 0) {
                    await dbs.query(`update s_user set uc=? where id=?`, [2, uid])
                    continue;
                }
                //Gửi mail thông báo nhắc đổi mật khẩu
                if (days_left <= config.days_warning_change_pass) {
                    await ads.change_pass_reminder(uid, mail, days_left)
                }
            } catch (err) {
                logger4app.error(err);
            }
        }
    
    },
    UpdateRunningJob: async (pkey, preset, jobtimeout) => {//Ham thuc hien update job dang chay bang cach insert vao bang s_listvalues theo key. Tham so pkey la key truyen vao tu khai bao job. Tham so preset dung cho truong hop xoa theo key khi da chay xong
        if (!pkey) return 0
        let vjobtimeout = 300 //Mac dinh thoi gian timeout la 5 phut neu khong cau hinh
        if (util.isNumber(jobtimeout)) vjobtimeout = jobtimeout
        if (!preset) {//Thuc hien danh dau job dang chay
            logger4app.debug('UpdateRunningJob','pkey',pkey)
            let vReturn = 1 //Mac dinh tra ra trang thai la job chua chay
            try {
                let vtime = (new Date()).getTime() //Lay gio hien tai
                let expiredate = new Date(vtime + (vjobtimeout * 1000)) //Tinh thoi gian expire de sau nay xoa trong truong hop ung dung bi restart bat ngo. Se co job khac quet de xoa cac key expire nay
                //logger4app.debug('expiredate ',moment(expiredate).format("YYYY-MM-DD HH:mm:ss"))
                await dbs.query(sql_set_ins[`sql_set_ins_${config.dbtype}`], [pkey, moment(expiredate).format("YYYY-MM-DD HH:mm:ss")]) //Insert du lieu vao bang s_listvalues theo key vaf ngay gio qua han
                logger4app.debug('UpdateRunningJob','vReturn',vReturn)
            } catch (err) {
                //Truong hop insert loi tuc la co job khac dang chay, tra ra trang thai la da chay
                logger4app.debug('UpdateRunningJob err','vReturn',vReturn)
                logger4app.debug('UpdateRunningJob err','err',err)
                vReturn = 0
            }
        
            return vReturn
        }
        else {
            //Neu chay xong thi xoa key di
            await dbs.query(sql_del[`sql_del_${config.dbtype}`], [pkey])
            return 1
        }
    },
    DropRunningJobExpire: async (pkey) => {//Ham thuc hien xoa cac key cua cac job bi expire. Tham so pkey la key truyen vao tu khai bao job.
        //Neu chay xong thi xoa key di
        //logger4app.debug('sql_del_exp',sql_del_exp[`sql_del_exp_${config.dbtype}`])
        await dbs.query(sql_del_exp[`sql_del_exp_${config.dbtype}`], [pkey])
        return 1

    }
}

module.exports = Service