"use strict"
const xlsx = require("xlsx")
const objectMapper = require("object-mapper")
const moment = require("moment")
const ajv = require("ajv")
const jv = new ajv({ allErrors: true })
const redis = require("../redis")
const util = require("../util")
const n2w = require("../n2w")
const config = require("../config")
const logger4app = require("../logger4app")
const SEC = require("../sec")
const dbs = require("./dbs")
const logging = require("../logging")
const ent = config.ent
const HLV = ent === "hlv"
const xls_idt = config.xls_idt
const xls_data = config.xls_data
const xls_ccy = config.xls_ccy
const mfd = config.mfd
const dtf = config.dtf
const KM = "Khuyến mãi"
const CK = "Chiết khấu"
const MT = "Mô tả"
const KCT = "Không chịu thuế"
const KKK = "Không kê khai nộp thuế"
const KMMT = [KM, MT]
const GNS = ["code", "name", "unit", "price"]
const ORG = ["name", "name_en", "taxc", "code", "addr", "tel", "mail", "acc", "bank"]
const PXC = ["ic", "ordno", "orddt", "ordou", "ordre", "recvr", "trans", "vehic", "contr", "whsfr", "whsto", "curr", "exrt", "note", "sum", "items.type", "items.name", "items.unit", "items.price", "items.quantity", "items.amount"]
const HDC = ["ic", "form", "serial", "seq", "idt", "buyer", "bname", "btax", "baddr", "btel", "bmail", "bacc", "bbank", "paym", "curr", "exrt", "note", "sum", "vat", "total", "items.type", "items.name", "items.unit", "items.price", "items.quantity", "items.vrt", "items.amount", "rform", "rserial", "rseq", "rref", "rrdt", "rrea"]
const URL = "http://json-schema.org/draft-07/schema#"
const STT = { "type": "integer", "minimum": 1 }
const STR = { "type": "string", "maxLength": 255 }
const NUM = { "type": "number", "minimum": 0 }
const PAYM = { "description": "HTTT", "type": "string", "default": "TM", "enum": ["CK", "TM", "TM/CK", "DTCN", "FOC"] }
const CURR = { "description": "Loại tiền", "type": "string"/* , "default": "VND" */, "enum": ["AUD", "CAD", "CHF", "DKK", "EUR", "GBP", "HKD", "INR", "JPY", "KRW", "KWD", "MYR", "NOK", "RUB", "SAR", "SEK", "SGD", "THB", "USD", "VND"] }
const ITEM = { "type": "object", "properties": { "line": STT, "type": { "type": "string", "enum": ["", KM, CK, MT] }, "name": STR, "price": NUM, "quantity": NUM, "amount": NUM, "unit": STR }, "required": ["line", "name"] }
const EXRT = { "type": "number", "minimum": 1, "default": 1 }
const chkschema = {
    "SCHEMAS_GHTK": {
        "GNS": { "$schema": URL, "type": "object", "properties": { "code": { "type": ["integer", "string"], "minimum": 0, "maxLength": 30 }, "name": STR, "unit": { "type": "string", "maxLength": 30 }, "price": NUM }, "required": (!"yusen".includes(config.ent)) ? ["code", "name", "unit"] : ["code", "name"] },
        "00ORGS": { "$schema": URL, "type": "object", "properties": { "name": STR, "addr": STR, "mail": STR }, "required": ["code", "name", "addr"] },
        "03XKNB": {
            "$schema": URL, "type": "object", "definitions": { "item": ITEM },
            "properties": { "whsfr": STR, "whsto": STR, "curr": CURR, "exrt": EXRT, "sum": NUM, "total": NUM, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 } },
            "required": ["ic", "whsfr", "whsto", "curr", "exrt", "sum", "total", "items"]
        },
        "01GTKT": {
            "$schema": URL, "type": "object", "definitions": {
                "item": { "type": "object", "properties": { "line": STT, "type": { "type": "string", "enum": ["", KM, CK, MT] }, "name": STR, "price": NUM, "quantity": NUM, "amount": NUM, "vat": NUM, "total": NUM, "vrt": STR, "unit": STR }, "required": ["line", "name", "vrt", "vatCode"] }
            },
            "properties": {
                "buyer": STR, "bname": STR, "baddr": STR, "bmail": STR, "paym": PAYM, "curr": CURR, "exrt": EXRT, "sum": NUM, "total": NUM, "vat": NUM, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 }
            },
            "required": ["idt", "ic", "paym", "curr", "exrt", "vat", "sum", "total", "items"]
        },
        "02GTTT": {
            "$schema": URL, "type": "object", "definitions": { "item": ITEM },
            "properties": {
                "buyer": STR, "bname": STR, "baddr": STR, "bmail": STR, "paym": PAYM, "curr": CURR, "exrt": EXRT, "sum": NUM, "total": NUM, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 }
            },
            "required": ["ic", "bname", "paym", "curr", "exrt", "sum", "total", "items"]
        }
    },

    "DEFAULT": {
        "GNS": { "$schema": URL, "type": "object", "properties": { "code": { "type": ["integer", "string"], "minimum": 0, "maxLength": 30 }, "name": STR, "unit": { "type": "string", "maxLength": 30 }, "price": NUM }, "required": (!"yusen".includes(config.ent)) ? ["code", "name", "unit"] : ["code", "name"] },
        "00ORGS": { "$schema": URL, "type": "object", "properties": { "name": STR, "addr": STR, "mail": STR, "acc": STR, "bank": STR }, "required": ["name", "addr"] },
        "03XKNB": {
            "$schema": URL, "type": "object", "definitions": { "item": ITEM },
            "properties": { "whsfr": STR, "whsto": STR, "curr": CURR, "exrt": EXRT, "sum": NUM, "total": NUM, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 } },
            "required": ["ic", "whsfr", "whsto", "curr", "exrt", "sum", "total", "items"]
        },
        "01GTKT": {
            "$schema": URL, "type": "object", "definitions": {
                "item": { "type": "object", "properties": { "line": STT, "type": { "type": "string", "enum": ["", KM, CK, MT] }, "name": STR, "price": NUM, "quantity": NUM, "amount": NUM, "vat": NUM, "total": NUM, "vrt": STR }, "required": ["line", "name", "vrt", 'vatCode'] }
            },
            "properties": {
                "buyer": STR, "bname": STR, "baddr": STR, "btel": STR, "bacc": STR, "bbank": STR, "note": STR, "bmail": STR, "paym": PAYM, "curr": CURR, "exrt": EXRT, "sum": NUM, "total": NUM, "vat": NUM, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 }
            },
            "required": ["idt", "ic", "bname", "paym", "curr", "exrt", "vat", "sum", "total", "items"]
        },
        "02GTTT": {
            "$schema": URL, "type": "object", "definitions": { "item": ITEM },
            "properties": {
                "buyer": STR, "bname": STR, "baddr": STR, "btel": STR, "bacc": STR, "bbank": STR, "note": STR, "bmail": STR, "paym": PAYM, "curr": CURR, "exrt": EXRT, "sum": NUM, "total": NUM, "items": { "type": "array", "items": { "$ref": "#/definitions/item" }, "minitems": 1 }
            },
            "required": ["ic", "bname", "paym", "curr", "exrt", "sum", "total", "items"]
        }
    }
}

const gns = {
    "code": "Mã hàng",
    "name": "Tên hàng",
    "unit": "Đơn vị tính",
    "price": "Đơn giá"
}
const gns_en = {
    "code": "Commodity code",
    "name": "Name of goods",
    "unit": "Unit",
    "price": "Unit price"
}
const org = {
    "name": "Tên KH",
    "name_en": "Tên KH tiếng Anh",
    "taxc": "Mã số thuế",
    "code": "Mã KH",
    "addr": "Địa chỉ",
    "tel": "Số điện thoại",
    "mail": "Email",
    "acc": "Tài khoản NH",
    "bank": "Tên NH"
}
const org_en = {
    "name": "Customer name",
    "name_en": "English customer name",
    "taxc": "TIN",
    "code": "KH code",
    "addr": "Address",
    "tel": "Phone number",
    "mail": "Email",
    "acc": "Bank Account",
    "bank": "Bank name"
}
const getSCHEMAS = () => {
    try {
        return chkschema[`SCHEMAS_${config.ent}`.toUpperCase()] ? chkschema[`SCHEMAS_${config.ent}`.toUpperCase()] : chkschema[`DEFAULT`.toUpperCase()]
    } catch (err) {
        return chkschema[`DEFAULT`.toUpperCase()]
    }
}
const SCHEMAS = getSCHEMAS()

const init = async (type) => {
    const sql = "insert into s_xls(itype,jc,xc) values ?"
    let i = 65, binds = []
    switch (type) {
        case "03XKNB":
            for (const rec of PXC) {
                binds.push([type, rec, String.fromCharCode(i++)])
            }
            await dbs.query(sql, [binds])
            break
        case "02GTTT":
            for (const rec of HDC) {
                if (!["items.vrt", "vat"].includes(rec)) {
                    binds.push([type, rec, String.fromCharCode(i++)])
                }
            }
            await dbs.query(sql, [binds])
            break
        case "01GTKT":
            for (const rec of HDC) {
                binds.push([type, rec, i > 90 ? `A${String.fromCharCode(i++ - 26)}` : String.fromCharCode(i++)])
            }
            await dbs.query(sql, [binds])
            break
        case "00ORGS":
            for (const rec of ORG) {
                binds.push([type, rec, String.fromCharCode(i++)])
            }
            await dbs.query(sql, [binds])
            break
        default:
            break
    }
}

const cache = async (type) => {
    let obj, key = `XLS.${type}`
    obj = await redis.get(key)
    if (obj) return JSON.parse(obj)
    const whe = "where itype=? and xc is not null and xc!='' "
    const sql = `select xc xc,CONCAT('c',idx) jc from s_dcm ${whe} and status=1 and dtl=2 union select xc xc,jc jc from s_xls ${whe}`
    let result
    if (type == "00ORGS") result = await dbs.query(sql, [type, type])
    else result = await dbs.query(`select xc xc,CONCAT('items.c',idx) jc from s_dcm ${whe} and status=1 and dtl=1 union ${sql}`, [type, type, type])
    const rows = result[0]
    obj = {}
    for (const row of rows) {
        obj[row.xc] = row.jc
    }
    await redis.set(key, JSON.stringify(obj))
    return obj
}
const get_schema = async (type, dtl) => {
    try {
        const sql = `select CONCAT('c',idx) jc,typ,atr atr from s_dcm where itype=? and xc is not null and xc!='' and status=1 and dtl=? and typ not in ('text','checkbox','popup')`
        const result = await dbs.query(sql, [type, dtl])
        const rows = result[0]
        let obj = {}
        let dts = []
        for (const row of rows) {
            const typ = row.typ, jc = row.jc
            if (typ == "number") obj[jc] = NUM
            else if (typ == "datepicker") dts.push(jc)
            else if (typ == "date") dts.push(jc)
            else {
                const atr = row.atr
                if (atr) {
                    let arr = []
                    if (atr.suggest.data) {
                        for (const item of atr.suggest.data) {
                            arr.push(item.id)
                        }
                        if (arr.length > 0) obj[jc] = { enum: arr }
                    }
                }
            }
        }
        await redis.set(`DTS.${type}.${dtl}`, JSON.stringify(dts))
        return obj
    } catch (err) {
        throw err
    }
}

const cache_dts = async (type, dtl) => {
    const key = `DTS.${type}.${dtl}`
    let obj = await redis.get(key)
    if (obj) return JSON.parse(obj)
    else return []
}

const cache_schema = async (type) => {
    try {
        const key = `CHK.${type}`
        let schema
        schema = await redis.get(key)
        if (schema) return JSON.parse(schema)
        schema = SCHEMAS[type]
        const hdr = await get_schema(type, 2), dtl = await get_schema(type, 1)
        if (!util.isEmpty(hdr)) schema.properties = { ...schema.properties, ...hdr }
        if (!util.isEmpty(dtl)) schema.definitions.item.properties = { ...schema.definitions.item.properties, ...dtl }
        await redis.set(key, JSON.stringify(schema))
        return schema
    } catch (err) {
        throw err
    }
}
const tax = async (rows, exrt) => {
    let catobj = JSON.parse(JSON.stringify(config.CATTAX))
    let arr = catobj
    for (let v of arr) {
        delete v["vrnc"]
        delete v["status"]
    }
    for (let row of rows) {
        let amt = row.amount
        if (!util.isNumber(amt)) continue
        let vat = row.vat
        if (!util.isNumber(vat)) continue
        const vrt = row.vrt
        const vatCode = row.vatCode
        let obj = arr.find(x => x.vatCode == vatCode)
        if (typeof obj == "undefined") continue
        if (row.type == CK) amt = -amt
        obj.amt += amt
        if (amt == 0) obj.isZero = true
        if (obj.hasOwnProperty("vat")) obj.vat += (xls_data) ? vat : amt * vrt / 100
    }
    let tar = arr.filter(obj => { return obj.amt !== 0 || (obj.hasOwnProperty("isZero") && delete obj.isZero) })
    for (let row of tar) {
        row.amtv = Math.round(row.amt * exrt)
        if (row.hasOwnProperty("vat")) row.vatv = Math.round(row.vat * exrt)
    }
    return tar
}

const checkerr = (result) => {
    for (const row of result) {
        if (!util.isEmpty(row.error)) return true
    }
    return false
}

const getKeyByValue = (object, value) => {
    return Object.keys(object).find(key => object[key] === value)
}

const Service = {
    get: async (req, res, next) => {
        try {
            const itype = req.params.itype
            let sql, result, rows
            sql = `select id id,itype itype,jc jc,xc xc from s_xls where itype=? order by LENGTH(xc),xc`
            result = await dbs.query(sql, [itype])
            rows = result[0]
            if (rows.length == 0) {
                await init(itype)
                result = await dbs.query(sql, [itype])
                rows = result[0]
            }
            res.json(rows)
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const body = req.body, itype = body.itype, key = `XLS.${itype}`
            const sql = "update s_xls set xc=? where id=?"
            const result = await dbs.query(sql, [body.xc, body.id])
            await redis.del(key)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    upload: async (req, res, next) => {
        let conn
        try {
            //console.time("read")
            conn = await dbs.getConnection()
            const file = req.file, body = req.body, type = body.type, token = SEC.decode(req), taxc = token.taxc, now = new Date(), NOW = moment(now).format(dtf)
            const wb = xlsx.read(file.buffer, { type: "buffer", cellDates: true, dateNF: mfd, sheetRows: config.MAX_ROW_EXCEL }) //cellText: false
            const sheetName = wb.SheetNames[0], ws = wb.Sheets[sheetName]
            for (let i in ws) {
                const wsi = ws[i]
                if (wsi.t == "d") wsi.v = moment(wsi.v, mfd).add(1, 'minutes').format(mfd)
            }
            const opts = ["01GTKT", "02GTTT", "03XKNB"].includes(type) ? { blankrows: false, header: "A", defval: null } : { blankrows: false, range: 1, header: type == "GNS" ? GNS : "A", defval: null }
            const arr = xlsx.utils.sheet_to_json(ws, opts)
            if (util.isEmpty(arr)) throw new Error("Không đọc được số liệu \n (Data cannot be read)")
            let header
            if (type == "01GTKT") {
                header = arr[0]
                arr.splice(0, 1)
            } else if (type == "02GTTT") {
                header = arr[0]
                arr.splice(0, 1)
            } else if (type == "03XKNB") {
                header = arr[0]
                arr.splice(0, 1)
            }
            //console.timeEnd("read")
            let result = []
            const checkerr = () => {
                for (const row of result) {
                    if (!util.isEmpty(row.error)) return true
                }
                return false
            }
            if (type == "GNS") {
                const schema = SCHEMAS[type]
                for (const row of arr) {
                    const ret = jv.validate(schema, row)
                    row.error = ""
                    if (!ret) {
                        let msgArr = []
                        for (let i of jv.errors) {
                            let errorsText = i, prop
                            if (errorsText) prop = errorsText.dataPath.slice(1)
                            if (!prop) prop = errorsText.params.missingProperty
                            else if (prop.endsWith("[0]")) prop = "_".concat(errorsText.params.missingProperty)
                            if (prop.includes("[")||prop.includes("]")) prop = "_".concat(prop.split(".")[1]) || prop
                            switch (errorsText.keyword) {
                                case "required":
                                    msgArr.push(`Nhập thiếu thông tin ${gns[prop] || prop} `)
                                    msgArr.push(`(Lack of ${gns_en[prop] || prop}).`)
                                    break
                                case "enum":
                                    msgArr.push(`Dữ liệu ${gns[prop] || prop} không hợp lệ `)
                                    msgArr.push(`(Data ${gns_en[prop] || prop} is invalid).`)
                                    break
                                case "maxLength":
                                    msgArr.push(`Dữ liệu ${gns[prop] || prop} vượt quá ký tự cho phép ${errorsText.params.limit} ký tự `)
                                    msgArr.push(`(Data ${gns_en[prop] || prop} exceed allowed characters number ${errorsText.params.limit} characters).`)
                                    break
                                case "type":
                                    msgArr.push(`${gns[prop] || prop} phải là ${errorsText.params.type == "number" ? 'Số' : errorsText.params.type} `)
                                    msgArr.push(`(${gns_en[prop] || prop} should be ${errorsText.params.type == "number" ? 'Number' : errorsText.params.type}).`)
                                    break
                                case "minimum":
                                    msgArr.push(`Dữ liệu ${gns[prop] || prop} phải lớn hơn ${errorsText.params.limit} `)
                                    msgArr.push(`(Data ${gns_en[prop] || prop} should be greater than ${errorsText.params.limit}).`)
                                    break
                                default:
                                    msgArr.push(i.message)
                                    break
                            }
                            row.error += msgArr.join(", ")
                        }
                        
                        
                    }
                    //row.error = ret ? "" : jv.errorsText()
                    result.push(row)
                }
                res.json({ err: checkerr(), data: result })
            }
            else if (type == "00ORGS") {
                const schema = SCHEMAS[type], map = await cache(type)
                for (const r of arr) {
                    const row = objectMapper(r, map)
                    let error = ""
                    if (row.taxc && !util.checkmst(row.taxc.toString())) error += "MST không hợp lệ(Invalid tax code). "
                    else if (row.mail && !util.checkemail(row.mail)) error += "Email không hợp lệ(Invalid email). "
                    else {
                        const ret = jv.validate(schema, row)
                        if (!ret) {
                            let msgArr = []
                            for (let i of jv.errors) {
                                let errorsText = i, prop
                                if (errorsText) prop = errorsText.dataPath.slice(1)
                                if (!prop) prop = errorsText.params.missingProperty
                                else if (prop.endsWith("[0]")) prop = "_".concat(errorsText.params.missingProperty)
                                if (prop.includes("[") || prop.includes("]")) prop = "_".concat(prop.split(".")[1]) || prop
                                switch (errorsText.keyword) {
                                    case "required":
                                        msgArr.push(`Nhập thiếu thông tin ${org[prop] || prop} `)
                                        msgArr.push(`(Lack of ${org_en[prop] || prop}).`)
                                        break
                                    case "enum":
                                        msgArr.push(`Dữ liệu ${org[prop] || prop} không hợp lệ `)
                                        msgArr.push(`(Data ${org_en[prop] || prop} is invalid).`)
                                        break
                                    case "maxLength":
                                        msgArr.push(`Dữ liệu ${org[prop] || prop} vượt quá ký tự cho phép ${errorsText.params.limit} ký tự `)
                                        msgArr.push(`(Data ${org_en[prop] || prop} exceed allowed characters number ${errorsText.params.limit} characters).`)
                                        break
                                    case "type":
                                        msgArr.push(`${org[prop] || prop} phải là ${errorsText.params.type == "number" ? 'Số' : errorsText.params.type} `)
                                        msgArr.push(`(${org_en[prop] || prop} should be ${errorsText.params.type == "number" ? 'Number' : errorsText.params.type}).`)
                                        break
                                    case "minimum":
                                        msgArr.push(`Dữ liệu ${org[prop] || prop} phải lớn hơn ${errorsText.params.limit} `)
                                        msgArr.push(`(Data ${org_en[prop] || prop} should be greater than ${errorsText.params.limit}).`)
                                        break
                                    default:
                                        msgArr.push(i.message)
                                        break
                                }
                                error += msgArr.join(", ")
                            }
                        }
                    }
                    // if(util.isEmpty(row.code)){
                    //     var nowMilliseconds = moment(new Date()).format("YYYYMMDDHHMMssSSS");
                    //     row.code='A'+nowMilliseconds;
                    // }
                    //if (util.isEmpty(row.code)) error += "Mã khách hàng không được để trống (The customer code cannot be left empty)"
                    row.error = error
                    result.push(row)
                }
                res.json({ err: checkerr(), data: result })
            }
            else if (type == "CAN") {
                for (const row of arr) {
                    row.G = row.G ? moment(row.G, mfd).endOf("day").subtract(1, "seconds").format(dtf) : NOW
                    let obj = { form: row.A, serial: row.B, seq: row.C, c0: row.D, rea: row.E ? row.E: "", ref: row.F, rdt: row.G, error: "" }
                    if (!(row.A && row.B && row.C)) obj.error = "Thiếu mẫu số hoặc ký hiệu hoặc số hóa đơn(Missing model number or symbol or invoice number). "
                    result.push(obj)
                }
                let binds = [], i = 1
                for (const row of result) {
                    const ic = i++, seq = row.seq ? row.seq.toString().padStart(config.SEQ_LEN, "0") : "", c0 = util.isEmpty(row.c0) ? "0" : row.c0, rdt = moment(row.rdt, dtf).toDate()
                    row.ic = ic
                    row.seq = seq
                    if (rdt > moment(now,dtf).endOf("day").toDate()) row.error += "Ngày văn bản lớn hơn hiện tại(The date in the text is greater than the current one). "
                    if (!row.error) binds.push([ic, row.form, row.serial, seq, c0, rdt])
                }
                if (binds.length > 0) {
                    await conn.query("DROP TEMPORARY TABLE IF EXISTS s_tmp")
                    await conn.query("create TEMPORARY table s_tmp(ic varchar(36),idt datetime,form varchar(11),serial varchar(6),seq varchar(7),c0 varchar(255),unique(ic)) ENGINE=MEMORY")
                    await conn.query("insert into s_tmp (ic,form,serial,seq,c0,idt) values ?", [binds])
                    let objs = {}, rs
                    rs = await conn.query("select a.ic ic,b.id id,b.pid pid,b.adjtyp adjtyp from s_tmp a,s_inv b where b.stax=? and b.status=3 and a.form=b.form and a.serial=b.serial and a.seq=b.seq and (a.c0='0' or a.c0=b.c0) and a.idt>b.idt", [taxc])
                    for (const row of rs[0]) {
                        objs[row.ic] = [row.id, (row.pid && row.adjtyp == 2) ? row.pid : null]
                    }
                    for (const row of result) {
                        let obj = objs[row.ic]
                        if (obj) {
                            row.id = obj[0]
                            row.pid = obj[1]
                            row.ref = row.ref ? row.ref : row.id
                        }
                        else row.error += HLV ? "Không tìm thấy hóa đơn(Invoice not found)." : "Không tìm thấy hoặc ngày văn bản sai(Not found or wrong text date)."
                    }
                }
                res.json({ err: checkerr(), data: result })
            }
            else {
                //console.time("chk1")
                const map = await cache(type), mapkeys = Object.keys(map), getKeyByValue = (v) => { return mapkeys.find(key => map[key] === v) }
                const iform = body.form, iserial = body.serial, VAT = (type === "01GTKT"), schema = await cache_schema(type)
                const dts2 = await cache_dts(type, 2), dts1 = await cache_dts(type, 1), len2 = dts2.length > 0, len1 = dts1.length > 0
                let ic = "^_^", obj, items
                for (const r of arr) {
                    const row = objectMapper(r, map)
                    if (!row.ic) throw new Error("Thiếu mã hóa đơn \n (Missing invoice code)")
                    if (ic == row.ic) {
                        items.push(row.items)
                    }
                    else {
                        if (obj) {
                            obj.items = items
                            result.push(obj)
                        }
                        obj = row
                        items = [row.items]
                        ic = row.ic
                    }
                }
                if (obj) {
                    obj.items = items
                    result.push(obj)
                }
                let HLV_PAYMENTS_INFO = []
                if (HLV) {
                    let hlvr = await conn.query(`select name id, des from s_cat where type = "PAYMENTS"`)
                    if (hlvr[0].length > 0) {
                        HLV_PAYMENTS_INFO = hlvr[0]
                    }
                    SCHEMAS["01GTKT"].properties.c0 = {type: "string", maxLength: 255}
                    SCHEMAS["01GTKT"].properties.c3 = {type: "string", maxLength: 255}
                    SCHEMAS["01GTKT"].properties.c4 = {type: "string", maxLength: 255}
                    SCHEMAS["01GTKT"].properties.c7 = {type: "string", maxLength: 255}
                    SCHEMAS["01GTKT"].properties.c8 = {type: "string", maxLength: 255}
                }
                for (let row of result) {
                    row.error = ""
                    let curr = row.curr, exrt = row.exrt, error = "", i = 1, vat = 0, sum = 0, total = 0, totalv, items = row.items
                    row.ic = row.ic.toString()
                    if (HLV && row.ic.length > 36) row.error += "SID Vượt quá ký tự cho phép 36 ký tự(Exceeds the allowed character 36 characters). "
                    if (HLV) {
                        // if(row.c0) {
                        //     //if (!(/^[0-9]{10}$/.test(row.c0))) error = "Số hợp đồng sai định dạng. "
                        // }
                        if (row.c0 && row.c0.length < 10) {
                            row.error += "Độ dài số hợp đồng phải lớn hơn 10 ký tự(Contract number length must be greater than 10 characters)"
                        }
                        // c2: serv_code c7:content c8: serv_name
                        if (row.c2) {
                            let hlvdes = HLV_PAYMENTS_INFO.find(e => {return e.id == row.c2})
                            if (hlvdes) {
                                hlvdes = JSON.parse(hlvdes.des)
                                row.c7 = hlvdes.servicecontent
                                row.c8 = hlvdes.servicename
                            }
                        }
                    }
                    if ((!xls_ccy) || (type == "03XKNB")) {
                        if (!util.isNumber(exrt)) {
                            exrt = 1
                            row.exrt = 1
                        }
                        if (!curr) {
                            curr = 'VND'
                            row.curr = 'VND'
                        }
                        if (curr == 'VND' && exrt !== 1) row.error += "VND tỉ giá phải bằng 1 \n (VND exchange rate must be 1)"
                    } else {
                        if (!curr) curr = 'VND'
                        if (!util.isNumber(exrt)) exrt = 1
                        if (row.curr == 'VND' && row.exrt !== 1) row.error += "VND tỉ giá phải bằng 1 \n (VND exchange rate must be 1)"                            
                    }
                    if (row.btax && !util.checkmst(row.btax.toString())) error += "MST không hợp lệ(Invalid tax code). "
                    if (row.bmail && !util.checkemail(row.bmail)) error += "Email không hợp lệ(Invalid email). "
                    if (row.idt) {
                        const idt = moment(row.idt, mfd, true)
                        if (idt.isValid()) {
                            row.idt = idt.format(dtf)
                            if (idt.toDate() > now) error += "Ngày hđ lớn hơn ngày hiện tại(The invoice date is greater than the current date). "
                        }
                        else error += "Ngày hđ sai định dạng(Date of wrong format). "
                    }
                    else {
                        if (xls_idt) error += "Thiếu ngày hóa đơn (Missing invoice date). "
                        else row.idt = NOW
                    }

                    if (row.orddt) {
                        const orddt = moment(row.orddt, mfd, true)
                        if (orddt.isValid()) {
                            row.orddt = orddt.format(dtf)
                        } else error += "Ngày lệnh sai định dạng." + "(Incorrect command date) "
                    }

                    if (len2) {
                        for (const col of dts2) {
                            if (row[col]) {
                                const idt = moment(row[col], mfd, true)
                                if (idt.isValid()) row[col] = idt.format(dtf)
                                else error += `Cột ${ws[`${getKeyByValue(col)}1`].v} sai định dạng ngày(Column ${ws[`${getKeyByValue(col)}1`].v}  wrong of date format). `
                                // else error += `Cột ${getKeyByValue(col)} sai định dạng ngày. `
                            }
                        }
                    }
                    let catobj = JSON.parse(JSON.stringify(config.CATTAX))
                    for (const item of items) {
                        let erri = ""
                        item.line = i++
                        let objtax = catobj.find(x => x.cusVatCode == item.vatCode)
                        if (objtax) {
                            item.vrt = objtax.vrt
                            item.vatCode = objtax.vatCode
                        } else {
                            let objvrttotax = catobj.find(x => x.id == item.vrt)
                            if (objvrttotax) {
                                objtax = objvrttotax
                                item.vatCode = objvrttotax.vatCode
                            } else {
                                erri += "Sai mã loại thuế. (Wrong VAT code)"
                            }
                        }

                        item.type = item.type ? item.type.toString().toUpperCase() : ""
                        switch (item.type) {
                            case "KM":
                                item.type = KM
                                break
                            case "CK":
                                item.type = CK
                                break
                            case "MT":
                                item.type = MT
                                break
                            case "MÔ TẢ":
                                item.type = MT
                                break
                            case "KHUYẾN MÃI":
                                item.type = KM
                                break
                            case "CHIẾT KHẤU":
                                item.type = CK
                                break
                            default:
                                item.type = ""
                                break
                        }
                        if (VAT && !util.isNumber(item.vrt)) erri += `thiếu thuế suất. `
                        if (len1) {
                            for (const col of dts1) {
                                if (item[col]) {
                                    const idt = moment(item[col], mfd)
                                    if (idt.isValid()) item[col] = idt.format(dtf)
                                    else erri += `sai định dạng ngày. `
                                }
                            }
                        }
                        let p = item.price, q = item.quantity, amt = 0, vmt = 0, vrt = 0
                        /*
                        if (util.isNumber(p) && util.isNumber(q)) item.amount = p * q
                        else {
                            if (!p) delete item.price
                            if (!q) delete item.quantity
                        }
                        */
                        if (KMMT.includes(item.type)) {
                            item.amount = 0
                            vrt = Number(item.vrt)
                        }
                        else {
                            if (type == "03XKNB" && !item.amount) {
                                item.amount = 0
                                vrt = Number(item.vrt)
                            }
                            if (util.isNumber(item.amount)) {
                                amt = item.amount
                                if (item.type == CK) amt = -amt
                                sum += amt
                                if (VAT) {
                                    vrt = Number(item.vrt)
                                    if (vrt > 0) {
                                        vmt = (xls_data) ? item.vat : (amt * vrt / 100)
                                        vat += vmt
                                    }
                                }
                            }
                            //else erri += `Thiếu thành tiền.`+` (Missing total amount)`
                        }
                        item.total = (xls_data) ? item.total : amt + vmt
                        if (VAT) {
                            if (objtax) item.vrn = objtax.vrn
                            else item.vrn = "\\"
                            item.vat = (xls_data) ? ((item.vat) ? item.vat : 0) : vmt
                            item.vrt = `${vrt}`
                        }
                        if (!util.isEmpty(erri)) error += `Lỗi chi tiết ${i}: ${erri}(Error detail ${i}: ${erri}). `
                    }
                    // Duy fix lam tron
                    vat = parseFloat(vat.toFixed(2))
                    total = sum + vat
                    //Nếu phiếu xuất kho thì gán mặc định sum, total = 0 khi không có giá trị
                    if (type == "03XKNB") {
                        row.sum = (row.sum) ? row.sum : 0
                        row.total = (row.total) ? row.total : 0
                    }
                    try {
                        row.sum = (xls_data) ? parseFloat(row.sum.toFixed(2)) : parseFloat(sum.toFixed(2))
                        sum = row.sum
                    } catch (ex) {
                        sum = 0
                        error += `Dữ liệu Thành tiền không hợp lệ (Invalid Amount Data). `
                    }
                    try {
                        row.total = (xls_data) ? parseFloat(row.total.toFixed(2)) : parseFloat(total.toFixed(2))
                        total = row.total
                    } catch (ex) {
                        total = 0
                        error += `Dữ liệu Tổng tiền không hợp lệ (Invalid Total Data). `
                    }
                    row.sumv = Math.round(sum * exrt)
                    totalv = Math.round(total * exrt)
                    row.totalv = totalv
                    row.word = n2w.n2w(totalv, curr)
                    if (VAT) {
                        try {
                            row.vat = (xls_data) ? parseFloat(row.vat.toFixed(2)) : vat
                            vat = row.vat
                        } catch (ex) {
                            vat = 0
                            error += `Dữ liệu Thuế suất không hợp lệ (Invalid Tax Data). `
                        }
                        row.vatv = Math.round(vat * exrt)
                        row.tax = await tax(items, exrt)
                    }
                    row.stax = taxc
                    if (!row.form) row.form = iform
                    if (!row.serial) row.serial = iserial
                    row.error += util.isEmpty(error) ? "" : error
                }
                let trans = {
                    ic: "Mã hóa đơn",
                    form: "Mẫu số",
                    serial: "Ký hiệu",
                    seq: "Số HĐ",
                    idt: "Ngày HĐ",
                    buyer: "Người mua",
                    bname: "Đơn vị",
                    btax: "MST",
                    baddr: "Địa chỉ",
                    btel: "Điện thoại",
                    bmail: "Mail",
                    bacc: "Số tài khoản",
                    bbank: "Ngân hàng",
                    paym: "Hình thức thanh toán",
                    recvr: "Người nhận",
                    trans: "Người vận chuyển",
                    vehic: "Phương tiện",
                    contr: "Số hợp đồng",
                    ordno: "Lệnh số",
                    orddt: "Lệnh ngày",
                    ordou: "Đơn vị",
                    ordre: "Lý do",
                    whsfr: "Nơi xuất",
                    whsto: "Nơi nhập",
                    curr: "Loại tiền",
                    exrt: "Tỷ giá",
                    note: "Ghi chú",
                    sum: "Tổng tiền hàng",
                    vat: "Tổng thuế",
                    total: "Tổng tiền thanh toán",
                    name: "Tên khách hàng",
                    name_en: "Tên tiếng Anh",
                    taxc: "MST",
                    code: "Mã",
                    addr: "Địa chỉ",
                    tel: "Điện thoại",
                    mail: "Mail",
                    acc: "Số tài khoản",
                    bank: "Ngân hàng",
                    rform: "Mẫu số cần thay thế",
                    rserial: "Ký hiệu cần thay thế",
                    rseq: "Số hóa đơn cần thay thế",
                    rref: "Số văn bản",
                    rrdt: "Ngày văn bản",
                    rrea: "Lý do",
                    score: "Điểm tiêu",
                    sum2: "Không chịu thuế",
                    vat0: "Thuế 0%",
                    sum0: "Thành tiền chưa thuế 0%",
                    vat5: "Thuế 5%",
                    sum5: "Thành tiền chưa thuế 5%",
                    vat10: "Thuế 10%",
                    sum10: "Thành tiền chưa thuế 10%",
                    _type: "Hình thức",
                    _name: "Hàng hóa, dịch vụ",
                    _unit: "Đơn vị tính",
                    _price: "Đơn giá",
                    _quantity: "Số lượng",
                    _vrt: "Thuế suất",
                    _amount: "Thành tiền",
                    _vat: "Tiền thuế chi tiết",
                    _total: "Thành tiền sau thuế chi tiết",
                    paym: "Hình thức thanh toán",

                    c2: "Mã dịch vụ"
                }
                let trans_en = {
                    ic: "Invoice Code",
                    form: "Form",
                    serial: "Serial",
                    seq: "Invoice No",
                    idt: "Ngày HĐ",
                    buyer: "Buyer",
                    bname: "Buyer Name",
                    btax: "Tax No",
                    baddr: "Buyer Adress",
                    btel: "Buyer Phone",
                    bmail: "Buyer Mail",
                    bacc: "Buyer Account",
                    bbank: "Buyer Bank",
                    paym: "Payment Mode",
                    recvr: "Receiver",
                    trans: "Transporter",
                    vehic: "Vehicle",
                    contr: "Contract No",
                    ordno: "Order No",
                    orddt: "Order Date",
                    ordou: "Organization Unit",
                    ordre: "Reason",
                    whsfr: "Export location",
                    whsto: "Import location",
                    curr: "Currency",
                    exrt: "Exchange Rate",
                    note: "Note",
                    sum: "Summary",
                    vat: "Sum VAT",
                    total: "Total",
                    name: "Customer Name",
                    name_en: "Customer Name (ENG)",
                    taxc: "Tax No",
                    code: "Code",
                    addr: "Adress",
                    tel: "Phone",
                    mail: "Mail",
                    acc: "Account",
                    bank: "Bank",
                    rform: "Replacing form",
                    rserial: "Replacing serial",
                    rseq: "Replacing invoice no",
                    rref: "Document No",
                    rrdt: "Document Date",
                    rrea: "Reason",
                    score: "Score",
                    sum2: "Summary (without VAT)",
                    vat0: "Tax Rate 0%",
                    sum0: "Summary (without VAT) 0%",
                    vat5: "Sum VAT 5%",
                    sum5: "Summary (without VAT) 5%",
                    vat10: "Sum VAT 10%",
                    sum10: "Summary (without VAT) 10%",
                    _type: "Type",
                    _name: "Goods, Services",
                    _unit: "Unit",
                    _price: "Price",
                    _quantity: "Quantity",
                    _vrt: "VAT rate",
                    _amount: "Amount",
                    _vat: "Detailed tax money",
                    _total: "Amount after detailed tax",
                    paym: "Payment Mode",

                    c2: "Service Code",
                }
                if (HLV) {
                    trans.c0 = "Policy number"
                    trans.c3 = "Pay mode"
                    trans.c4 = "Life Insured name"
                    trans.c7 = "Service name"
                    trans.c8 = "Content"
                    trans_en.c0 = "Policy number"
                    trans_en.c3 = "Pay mode"
                    trans_en.c4 = "Life Insured name"
                    trans_en.c7 = "Service name"
                    trans_en.c8 = "Content"
                }

                if (config.ent == "lge") {
                    trans.c0 = "Ký hiệu"
                    trans_en.c0 = "Detail Code"
                }
                for (const row of result) {
                    const ret = jv.validate(schema, row)
                    if (!ret) {
                        let msgArr = []
                        for (let i of jv.errors) {
                            let errorsText = i, prop
                            if (errorsText) prop = errorsText.dataPath.slice(1)
                            for (let i = 0; i < row.items.length; i++) {
                                if (!prop) {
                                    prop = errorsText.params.missingProperty
                                    break
                                }
                                else if (prop.endsWith(`[${i}]`)) {
                                    prop = "_".concat(errorsText.params.missingProperty)
                                    break
                                }
                            }
                            if (prop.includes("[")||prop.includes("]")) prop = "_".concat(prop.split(".")[1]) || prop
                            switch (errorsText.keyword) {
                                case "required":
                                    msgArr.push(`Nhập thiếu thông tin ${trans[prop] || prop} `)
                                    msgArr.push(`(Lack of ${trans_en[prop] || prop}).`)
                                    break
                                case "enum":
                                    msgArr.push(`Dữ liệu ${trans[prop] || prop} không hợp lệ `)
                                    msgArr.push(`(Data ${trans_en[prop] || prop} is invalid).`)
                                    break
                                case "maxLength":
                                    msgArr.push(`Dữ liệu ${trans[prop] || prop} vượt quá ký tự cho phép ${errorsText.params.limit} ký tự `)
                                    msgArr.push(`(Data ${trans_en[prop] || prop} exceed allowed characters number ${errorsText.params.limit} characters).`)
                                    break
                                case "type":
                                    msgArr.push(`${trans[prop] || prop} phải là ${errorsText.params.type == "number" ? 'Số' : errorsText.params.type} `)
                                    msgArr.push(`(${trans_en[prop] || prop} should be ${errorsText.params.type == "number" ? 'Number' : errorsText.params.type}).`)
                                    break
                                case "minimum":
                                    msgArr.push(`Dữ liệu ${trans[prop] || prop} phải lớn hơn ${errorsText.params.limit} `)
                                    msgArr.push(`(Data ${trans_en[prop] || prop} should be greater than ${errorsText.params.limit}).`)
                                    break                                    
                                default:
                                    msgArr.push(i.message)
                                    break
                            }
                        }
                        row.error += msgArr.join(", ")
                        // row.error += jv.errorsText()
                    }
                }
                //console.timeEnd("chk1")
                if (!checkerr()) {
                    //console.time("chk2")
                    let binds = [], rs, ecs, rows
                    for (const row of result) {
                        binds.push([row.ic, row.form, row.serial, moment(row.idt, dtf).endOf("day").subtract(1, "seconds").toDate()])
                    }
                    await conn.query("DROP TEMPORARY TABLE IF EXISTS s_tmp")
                    await conn.query("DROP TEMPORARY TABLE IF EXISTS s_imp")
                    await conn.query("create TEMPORARY table s_tmp(ic varchar(36),idt datetime,form varchar(11),serial varchar(6),seq varchar(7),unique(ic)) ENGINE=MEMORY")
                    await conn.query("insert into s_tmp(ic,form,serial,idt) values ?", [binds])
                    rs = await conn.query("select a.ic ic from s_tmp a where not exists (select 1 from s_serial where taxc=? and form=a.form and serial=a.serial and status=1 and fd<=a.idt)", [taxc])
                    rows = rs[0]
                    if (rows.length > 0) {
                        ecs = []
                        for (const row of rows) {
                            ecs.push(row.ic)
                        }
                        for (const row of result) {
                            if (ecs.includes(row.ic)) row.error += "Dải số hđ không hợp lệ(Invalid invoice number range). "
                        }
                    }
                    rs = await conn.query("select a.ic ic from s_tmp a where exists (select 1 from s_inv where ic=a.ic)")
                    rows = rs[0]
                    if (rows.length > 0) {
                        ecs = []
                        for (const row of rows) {
                            ecs.push(row.ic)
                        }
                        for (const row of result) {
                            if (ecs.includes(row.ic)) row.error += "Mã hđ đã tồn tại(Invoice code already exists). "
                        }
                    }
                    await conn.query("create temporary table s_imp ENGINE=MEMORY as select form,serial,max(idt) idt from s_inv where stax=? and status>1 group by form,serial", [taxc])
                    rs = await conn.query("select a.ic ic from s_tmp a where exists (select 1 from s_imp where form=a.form and serial=a.serial and idt>=a.idt)")
                    rows = rs[0]
                    if (rows.length > 0) {
                        ecs = []
                        for (const row of rows) {
                            ecs.push(row.ic)
                        }
                        for (const row of result) {
                            if (ecs.includes(row.ic)) row.error += "Đã có hđ có ngày lớn hơn đã cấp số(Invoice code already exists or has a date greater is issued number). "
                        }
                    }
                    //replace
                    binds = []
                    for (const row of result) {
                        if (row.rform && row.rserial && row.rseq) {
                            const rrdt = row.rrdt
                            if (rrdt) {
                                const dt = moment(rrdt, mfd)
                                if (dt.toDate() > now) row.error += "Ngày văn bản lớn hơn hiện tại(Already have a invoice with a larger date that issued numbers). "
                                else row.rrdt = dt.format(dtf)
                                if (moment(row.rrdt).toDate() > moment(row.idt).toDate()) row.error += "Ngày văn bản lớn hơn Ngày hóa đơn thay thế(The date in the text is greater than the date of the replacement invoice). "
                            }
                            else row.rrdt = row.idt
                            binds.push([row.ic, row.rform, row.rserial, row.rseq, moment(row.rrdt, dtf).endOf("day").subtract(1, "seconds").toDate()])
                        }
                    }
                    if (binds.length > 0) {
                        await conn.query("truncate table s_tmp")
                        await conn.query("insert into s_tmp(ic,form,serial,seq,idt) values ?", [binds])
                        rs = await conn.query("select form,serial,seq,count(form),count(serial),count(seq) from s_tmp group by form,serial,seq having count(form)>1 and count(serial)>1 and count(seq)>1")
                        rows = rs[0]
                        if (rows.length > 0) {
                            ecs = []
                            for (const row of rows) {
                                ecs.push(`${row.form}-${row.serial}-${row.seq}`)
                            }
                            throw new Error(`HĐ bị thay thế bị trùng : ${ecs.join()} \n (The replaced invoice is duplicated: ${ecs.join()})`)
                        }
                        rs = await conn.query(`select a.ic ic,b.id id,b.idt idt,b.sec sec,b.btax btax,b.bname bname,b.doc->>'$.root' root from s_tmp a,s_inv b where b.stax=? and b.status=3 and b.form=a.form and b.serial=a.serial and b.seq=a.seq and a.idt>=b.idt`, [taxc])
                        rows = rs[0]
                        if (rows.length > 0) {
                            let ps = {}
                            for (const row of rows) {
                                ps[row.ic] = { id: row.id, idt: row.idt, sec: row.sec, btax: row.btax, bname: row.bname, root: row.root }
                            }
                            for (const row of result) {
                                if (row.rform && row.rserial && row.rseq) {
                                    const p = ps[row.ic]
                                    if (p) {
                                        row.pid = p.id
                                        if(moment(row.idt,dtf).endOf("day").toDate() < p.idt) row.error += "Ngày HĐ thay thế bé hơn ngày HĐ bị thay thế(The date of the replacement invoice is less than the date the invoice was replaced). "
                                        row.adj = {
                                            ref: row.rref ? row.rref : row.pid,
                                            rdt: row.rrdt,
                                            rea: row.rrea,
                                            des: `Thay thế cho HĐ số ${row.rseq} ký hiệu ${row.rserial} mẫu số ${row.rform} ngày ${moment(p.idt).format(mfd)} mã ${p.sec}`,
                                            seq: `${row.rform}-${row.rserial}-${row.rseq}`,
                                            idt: p.idt,
                                            typ: 1
                                        }
                                        row.root = util.isEmpty(p.root) ? { sec: p.sec, form: row.rform, serial: row.rserial, seq: row.rseq, idt: moment(p.idt).format(mfd), btax: p.btax, bname: p.bname } : p.root
                                    }
                                    else row.error += "Hđ bị thay thế không tồn tại(The replaced invoice does not exist). "
                                } else row.error += "Thông tin của HĐ bị thay thế không hợp lệ(Information of the replaced invoice is invalid). "
                            }
                        }
                        else {
                            for (const row of result) {
                                if (row.rform && row.rserial && row.rseq) row.error += "Thông tin của HĐ bị thay thế hoặc HĐ thay thế không hợp lệ hoặc sai ngày văn bản(Information of the replaced invoice or the replacement invoice is invalid or wrong date of the text). "
                            }
                        }
                    }
                    //replace
                    //console.timeEnd("chk2")
                }
                if (checkerr()) {
                    //console.time("error")
                    const key = getKeyByValue("ic")
                    let errs = []
                    for (const row of result) {
                        if (row.error) {
                            let err = { error: row.error }
                            err[key] = row.ic
                            errs.push(err)
                        }
                    }
                    let arr1 = [], ic0 = "^_^", ic1
                    for (const a of arr) {
                        ic1 = a[key]
                        if (ic1 === ic0) {
                            arr1.push(a)
                        }
                        else {
                            ic0 = ic1
                            arr1.push({ ...a, ...(errs.find(err => String(err[key]) === String(ic1))) })
                        }
                    }
                    /*
                    for (const row of errs) {
                        const ic = row.ic
                        for (const a of arr) {
                            if (a[key] === ic) {
                                a.error = row.error
                                break
                            }
                        }
                    }
                    */
                    header.error = "Chi tiết lỗi (Details error)"
                    arr1.unshift(header)
                    const ewb = xlsx.utils.book_new(), ews = xlsx.utils.json_to_sheet(arr1, { skipHeader: 1 })
                    xlsx.utils.book_append_sheet(ewb, ews, sheetName)
                    res.json({ buffer: xlsx.write(ewb, { bookType: "xlsx", type: "buffer" }) })
                    //console.timeEnd("error")
                }
                else res.json({ data: result })
            }
        }
        catch (err) {
            res.json({ err: err.message })
        }
        finally {
            await conn.release()
        }
    },
    uplupd: async (req, res, next) => {
        let conn
        try {
            conn = await dbs.getConnection()
            const file = req.file, body = req.body, type = body.type, token = SEC.decode(req), taxc = token.taxc, now = new Date(), NOW = moment(now).format(dtf)
            const wb = xlsx.read(file.buffer, { type: "buffer", cellText: false, cellDates: true, dateNF: "dd/mm/yyyy", sheetRows: config.MAX_ROW_EXCEL })
            const sheetName = wb.SheetNames[0], ws = wb.Sheets[sheetName]
            if (!["GNS", "00ORGS"].includes(type)) {
                for (let i in ws) {
                    if (ws[i].t == "d") ws[i].v = moment(ws[i].v).add(1, 'hours').format(mfd)
                }
            }
            //const opts = { blankrows: false, range: 1, header: type == "GNS" ? GNS : "A", raw: true, defval: null }
            const opts = ["01GTKT", "02GTTT"].includes(type) ? { blankrows: false, header: "A", defval: null } : { blankrows: false, range: 1, header: type == "GNS" ? GNS : "A" }
            const arr = xlsx.utils.sheet_to_json(ws, opts)
            if (util.isEmpty(arr)) throw new Error("Không đọc được số liệu \n (Data cannot be read)")
            let header
            if (type == "01GTKT") {
                header = arr[0]
                arr.splice(0, 1)
            } else if (type == "02GTTT") {
                header = arr[0]
                arr.splice(0, 1)
            }
            let result = [], error = false
            const checkerr = () => {
                for (const row of result) {
                    if (!util.isEmpty(row.error)) return true
                }
                return false
            }
            if (type) {
                const map = await cache(type)
                if (["01GTKT","02GTTT"].includes(type)) {
                    const iform = body.form, iserial = body.serial, VAT = (type === "01GTKT"), schema = await cache_schema(type), HDT = await cache_dts(type, 2), DDT = await cache_dts(type, 1)
                    let key = "^_^", obj, items
                    let count = 1, icCol = getKeyByValue(map, "ic")
                    for (let r of arr) {
                        const row = objectMapper(r, map)
                        row.error = ""
                        if (!row.form || !row.serial || !row.seq) throw new Error("Thiếu thông tin xác định hóa đơn \n (Missing information to identify the invoice)")
                        row.seq = row.seq.toString().padStart(config.SEQ_LEN, "0")
                        let key2 = row.form+row.serial+row.seq
                        if (key == key2) {
                            r[icCol] = obj.ic
                            items.push(row.items)
                        }
                        else {
                            if (obj) {
                                obj.items = items
                                result.push(obj)
                            }
                            obj = row
                            obj.ic = r[icCol] = "random"+count++
                            items = [row.items]
                            key = key2
                        }
                    }
                    if (obj) {
                        obj.items = items
                        result.push(obj)
                        arr[arr.length-1][icCol] = obj.ic
                    }
                    for (const row of result) {
                        row.ic = row.ic.toString()
                        if (row.idt) {
                            const idt = moment(row.idt, mfd)
                            if (idt.isValid()) {
                                row.idt = idt.format(dtf)
                                if (idt.toDate() > now) row.error += "Ngày hđ lớn hơn ngày hiện tại (The invoice date is greater than the current date). "

                            }
                            else row.error += "Ngày hđ sai định dạng(Invoice date in wrong format). "
                        }
                        else {
                            row.idt = NOW
                        }
                        for (const hdt of HDT) {
                            let val = row[hdt]
                            if (val) {
                                const idt = moment(val, mfd)
                                if (idt.isValid()) row[hdt] = idt.format(dtf)
                                else row.error += `${hdt} sai định dạng ngày(${hdt} is the wrong date format). `
                            }
                        }

                        let curr = row.curr, exrt = row.exrt
                        if (!util.isNumber(exrt)) {
                            exrt = 1
                            row.exrt = 1
                        }
                        if (curr == 'VND' && exrt !== 1) throw new Error("VND tỉ giá phải bằng 1 \n (VND exchange rate must be 1)")
                        if (row.btax && !util.checkmst(row.btax.toString())) row.error += "MST không hợp lệ (Invalid tax code). "
                        if (row.bmail && !util.checkemail(row.bmail)) row.error += "Email không hợp lệ (Invalid email). "
                        let i = 1, vat = 0, sum = 0, total = 0, totalv, items = row.items
                        let catobj = JSON.parse(JSON.stringify(config.CATTAX))
                        for (const item of items) {
                            if (VAT && !util.isNumber(item.vrt)) throw new Error("Thiếu thuế suất \n (Missing tax rates)")
                            for (const ddt of DDT) {
                                if (item[ddt]) {
                                    const idt = moment(item[ddt], mfd)
                                    if (idt.isValid()) item[ddt] = idt.format(dtf)
                                    else throw new Error(`items.${ddt} sai định dạng ngày.  \n (items.${ddt} wrong date format)`)
                                }
                            }
                            let p = item.price, q = item.quantity, amt = 0, vmt = 0, vrt = 0
                            if (util.isNumber(p) && util.isNumber(q)) item.amount = p * q
                            else {
                                if (!p) delete item.price
                                if (!q) delete item.quantity
                            }
                            if (!util.isNumber(item.amount)) throw new Error("Thiếu thành tiền \n (Missing total amount)")
                            item.line = i++
                            let objtax = catobj.find(x => x.vatCode == item.vatCode)
                            if (objtax) {
                                item.vrt = objtax.vrt
                            } else {
                                let objvrttotax = catobj.find(x => x.id == item.vrt)
                                if (objvrttotax) {
                                    item.vatCode = objvrttotax.vatCode
                                } else {
                                    erri += "Sai mã loại thuế. (Wrong VAT code)"
                                }
                            }
                            item.type = item.type ? item.type.toString().toUpperCase() : ""
                            switch (item.type) {
                                case "KM":
                                    item.type = KM
                                    break
                                case "CK":
                                    item.type = CK
                                    break
                                case "MT":
                                    item.type = MT
                                    break
                                default:
                                    item.type = ""
                                    break
                            }
                            if (KMMT.includes(item.type)) item.amount = 0
                            else {
                                amt = item.amount
                                if (item.type == CK) amt = -amt
                                sum += amt
                                if (VAT) {
                                    vrt = Number(item.vrt)
                                    if (vrt > 0) {
                                        vmt = amt * vrt / 100
                                        vat += vmt
                                    }
                                }
                            }
                            item.total = amt + vmt
                            if (VAT) {
                                if (vrt >= 0) item.vrn = `${vrt}%`
                                else item.vrn = "\\"
                                item.vat = vmt
                                item.vrt = `${vrt}`
                            }
                        }
                        // Duy fix lam tron
						vat = parseFloat(vat.toFixed(2))
						total = sum + vat
                        row.sum = sum
                        row.total = total
                        row.sumv = Math.round(sum * exrt)
                        totalv = Math.round(total * exrt)
                        row.totalv = totalv
                        row.word = n2w.n2w(totalv, curr)
                        if (VAT) {
                            row.vat = vat
                            row.vatv = Math.round(vat * exrt)
                            row.tax = await tax(items, exrt)
                        }
                        row.stax = taxc
                        if (!row.form) row.form = iform
                        if (!row.serial) row.serial = iserial
                        else if(!(/^(([ABCDEGHKLMNPQRSTUVXY]{2})\/[0-9]{2}E)$/.test(row.serial))) row.error += "Sai định dạng ký hiệu(wrong symbol format). "
                    }
                    for (const row of result) {
                        const ret = jv.validate(schema, row)
                        if (!ret) row.error += jv.errorsText()
                    }
                    error = checkerr(result)
                    if (!error) {
                        let binds = [], rs, ecs, rows
                        for (const row of result) {
                            binds.push([row.ic, row.form, row.serial, row.seq.toString().padStart(config.SEQ_LEN,"0")])
                        }
                        await conn.query("DROP TEMPORARY TABLE IF EXISTS s_tmp")
                        await conn.query("create TEMPORARY table s_tmp(ic varchar(36),idt datetime,form varchar(11),serial varchar(6),seq varchar(7),unique(ic)) ENGINE=MEMORY")
                        await conn.query("insert into s_tmp(ic,form,serial,seq) values ?", [binds])
                        rs = await conn.query(`select a.ic "ic", b.id "id" from s_tmp a,s_inv b where b.stax=? and b.status=2 and a.form=b.form and a.serial=b.serial and a.seq=b.seq `, [taxc])
                        rows = rs[0]
                        let ps = {}
                        for (const row of rows) {
                            ps[row.ic] = row.id
                        }
                        for (const row of result) {
                            const r = ps[row.ic]
                            if(r) {
                                row.pid = r
                            } else {
                                row.error += "Không tìm thấy hóa đơn phù hợp để cập nhật(Can not find suitable invoice to update). "
                            }
                        }
                        error = checkerr(result)
                    }
                    if (error) {
                        const key = getKeyByValue(map, "ic")
                        for (const row of result) {
                            if (row.error) {
                                const ic = row.ic
                                for (const a of arr) {
                                    if (a[key] == ic) {
                                        a.error = row.error
                                        break
                                    } 
                                }
                            }
                        }
                        for (const a of arr) {
                            a[key] = ""
                        }
                        header.error = "Lỗi (Error)"
                        arr.unshift(header)
                        const ewb = xlsx.utils.book_new()
                        const ews = xlsx.utils.json_to_sheet(arr, { skipHeader: 1 })
                        xlsx.utils.book_append_sheet(ewb, ews, sheetName)
                        const buffer = xlsx.write(ewb, { bookType: 'xlsx', type: "buffer" })
                        res.json({ buffer: buffer })
                    }
                    else {
                        for (const a of result) {
                            a.ic = ""
                        }
                        res.json({ data: result })
                    }
                }
            }
            await conn.commit()
            const sSysLogs = { fnc_id: 'xls_upl', src: config.SRC_LOGGING_DEFAULT, dtl: `Đọc file hóa đơn ${file.originalname}`, msg_id: file.originalname};
            logging.ins(req,sSysLogs,next)
        }
        catch (err) {
            res.json({ err: err.message })
        }
        finally {
            await conn.release()
        }
    }
}
module.exports = Service