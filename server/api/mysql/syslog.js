"use strict"
const path = require("path")
const fs = require("fs")
const docxt = require("docxtemplater")
const jszip = require("jszip")
const moment = require("moment")
const xlsxtemp = require("xlsx-template")
const util = require("../util")
const config = require("../config")
const logger4app = require("../logger4app")
const redis = require("../redis")
const sec = require("../sec")
const dbs = require("./dbs")


const Service = {
    
    get: async (req, res, next) => {
        try {
            const query = req.query, filter = JSON.parse(query.filter)
            const sort = query.sort, start = query.start ? query.start : 0, count = query.count ? query.count : 10
            let where = " where dt between ? and ?", order, sql, result, rows, ret, val
            const fm = moment(filter.fd), tm = moment(filter.td)
            const fd = fm.startOf("day").format("YYYY-MM-DD HH:mm:ss"), td = tm.endOf("day").format("YYYY-MM-DD HH:mm:ss")
            let binds = [fd, td]
            Object.keys(filter).forEach((key) => {
                val = filter[key]
                if (val) {
                    switch (key) {
                        case "fd":
                            break
                        case "td":
                            break
                        case "srclog":
                            where += ` and SRC=?`
                            binds.push(val)
                            break
                        case "type":
                            where += ` and FNC_ID=?`
                            binds.push(val)
                            break
                        case "user":
                            where += ` and USER_ID like ?`
                            binds.push('%'+val+'%')
                            break
                        case "msg_id":
                            where += ` and MSG_ID like ?`
                            binds.push('%'+val+'%')
                            break
                        case "msg_status":
                            where += ` and MSG_STATUS = ?`
                            binds.push(val)
                            break
                        default:
                            where += ` and ${key} like ?`
                            binds.push('%'+val+'%')
                            break
                    }
                }
            })
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = ` order by ${key} ${sort[key]}`
                })
            }
            else order = " order by id desc, dt desc"
            sql = `select id id,DATE_FORMAT(dt,'%d/%m/%Y %T') dt,fnc_id,fnc_name,fnc_url,action,user_id,user_name,src,dtl,msg_id,CASE WHEN msg_status=0 THEN 'Lỗi' WHEN msg_status=1 THEN 'Thành công' ELSE '' END  msg_status,r1,r2,r3,r4,doc from s_sys_logs ${where} ${order} LIMIT ${count} OFFSET ${start}`
            result = await dbs.query(sql, binds)
            
            ret = { data: result[0], pos: start }
            if (start == 0) {
                sql = `select count(*) "total" from s_sys_logs ${where}`
                result = await dbs.query(sql, binds)
                //rows = result.rows
                ret.total_count = result[0][0].total
            }
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    xls: async (req, res, next) => {
        try {
            const query = req.query, filter = JSON.parse(query.filter)
            const sort = query.sort
            let where = " where dt between ? and ?", order, sql, result, rows, val, json
            const fm = moment(filter.fd), tm = moment(filter.td)
            const fd = fm.startOf("day").format("YYYY-MM-DD HH:mm:ss"), td = tm.endOf("day").format("YYYY-MM-DD HH:mm:ss")
            let binds = [fd, td]
           
            let i = 3
            Object.keys(filter).forEach((key) => {
                val = filter[key]
                logger4app.debug(val)
                if (val) {
                    switch (key) {
                        case "fd":
                            break
                        case "td":
                            break
                        case "srclog":
                            where += ` and SRC=?`
                            binds.push(val)
                            break
                        case "type":
                            where += ` and FNC_ID=?`
                            binds.push(val)
                            break
                        case "user":
                            where += ` and USER_ID like ?`
                            binds.push('%'+val+'%')
                            break
                        case "msg_id":
                            where += ` and MSG_ID like ?`
                            binds.push('%'+val+'%')
                            break
                        case "msg_status":
                            where += ` and MSG_STATUS = ?`
                            binds.push(val)
                            break
                        default:
                            where += ` and ${key} like ?`
                            binds.push('%'+val+'%')
                            break
                    }
                }
            })
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = ` order by ${key} ${sort[key]}`
                })
            }
            else order = " order by dt desc, id"
            sql = `select null id,DATE_FORMAT(dt,'%d/%m/%Y %T') dt,fnc_id,fnc_name,fnc_url,action,user_id,user_name,src,dtl,msg_id,msg_status,CASE WHEN msg_status=0 THEN 'Lỗi' WHEN msg_status=1 THEN 'Thành công' ELSE '' END  msg_status,r1,r2,r3,r4,doc from s_sys_logs ${where} ${order} LIMIT ${config.MAX_ROW_EXCEL_EXPORT} OFFSET 0 `
            result = await dbs.query(sql, binds)
            
            rows = result[0]
            const fn = "temp/SYSLOG.xlsx"
            json = { table: rows }
            const file = path.join(__dirname, "..", "..", "..", fn)
            const xlsx = fs.readFileSync(file)
            const template = new xlsxtemp(xlsx)
            template.substitute(1, json)
            res.end(template.generate(), "binary")
        }
        catch (err) {
            logger4app.debug("excel" + err)
            next(err)
        }
    }
}
module.exports = Service   