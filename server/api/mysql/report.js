"use strict"
const excel = require("exceljs")
const numeral = require("numeral")
const path = require("path")
const fs = require("fs")
const moment = require("moment")
const xlsxtemp = require("xlsx-template")
const dbs = require("./dbs")
const config = require("../config")
const logger4app = require("../logger4app")
const sec = require("../sec")
const util = require("../util")
const redis = require("../redis")
const ous = require("./ous")
const mfd = config.mfd
const grant = config.ou_grant
const sendApprMail = config.sendApprMail
const sendCancMail = config.sendCancMail
const xls_data = config.xls_data
const listITYPE = [...config.ITYPE,{id:"*", value:"Tất cả"}]
const KCT = "Không chịu thuế"
const KKK = "Không kê khai nộp thuế"
const CK = "Chiết khấu"
const charList = (a, z, d = 1) => (a=a.charCodeAt(),z=z.charCodeAt(),[...Array(Math.floor((z-a)/d)+1)].map((_,i)=>String.fromCharCode(a+i*d)))
const calctax = async (rows, exrt) => {
    let catobj = JSON.parse(JSON.stringify(config.CATTAX))
    let arr = catobj
    for (let row of rows) {
        let amt = row.amount
        if (!util.isNumber(amt)) continue
        let vat = row.vat
        if (!util.isNumber(vat)) continue
        const vrt = row.vrt
        let obj = arr.find(x => x.vrt == vrt)
        if (typeof obj == "undefined") continue
        if (row.type == CK) amt = -amt
        obj.amt += amt
        if (amt == 0) obj.isZero = true
        if (obj.hasOwnProperty("vat")) obj.vat += (xls_data) ? vat : amt * vrt / 100
    }
    let tar = arr.filter(obj => { return obj.amt !== 0 || (obj.hasOwnProperty("isZero") && delete obj.isZero) })
    for (let row of tar) {
        row.amtv = Math.round(row.amt * exrt)
        if (row.hasOwnProperty("vat")) row.vatv = row.vat * exrt
    }
    return tar
}
const tenhd = (type) => {
    let result = listITYPE.find(item => item.id === type)
    return result.value
}



const Service = {
    getBKHDDT: async (rows) => {
        
        let rowbc = [], idarr = []
        let i = 0
        let maptype = {"-1": "1", "0": "2", "5": "3", "10": "4", "-2": "5"}
        //Lấy danh sách các hóa đơn điều chỉnh
        for (const row of rows) {
            let items = JSON.parse(row.items)
            row.itemamount=0
            row.itemvat = 0
            row.itemtotal = 0
            row.form = row.form
            row.formserial = `${row.form} - ${row.serial}`
            row.serial = row.serial
            row.seq = row.seq
            row.idt = row.idt
            row.form = row.form
            row.formserial = `${row.form} - ${row.serial}`
            row.serial = row.serial
            row.seq = row.seq
            row.idt = row.idt
            row.bname = (row.bname != null && row.bname != "") ? row.bname : row.buyer
            row.btax = row.btax
            row.itemname = "Rượu"
            for (const item of items) {
                i++
                row.itemquantity += row.quantity
                row.itemamount += item.amount
                row.itemvrn = row.vrn
                row.itemvrt = (item.vrt >= 0) ? item.vrt : ""
                row.itemvrttype = (maptype[String(item.vrt)]) ? maptype[String(item.vrt)] : ""
                row.itemvat += item.vat
                row.itemtotal += item.total
                if (row.adjtype == 3) {
                    row.itemamount *= -1
                    row.itemvat *= -1
                    row.itemtotal *= -1
                }
            }
            if (row.status == 4) {
                row.itemnote = "Hóa đơn hủy"
                row.itemvrt = ""
                row.itemamount = 0
                row.itemvat = 0
                row.itemtotal = 0
            }
            if (row.btax == row.stax) {
                row.itemamount = 0
            }
            if (!idarr.includes(row.id)) {
                idarr.push(row.id)
                rowbc.push(row)
            }

        }

        return rowbc
    },
    getInvoiceRep: async (req, res, next) => {
        try {
            const query = req.body
            const fm = moment(query.fd), tm = moment(query.td), stax = query.stax
            const fd = fm.startOf("day").format("YYYY-MM-DD HH:mm:ss"), td = tm.endOf("day").format("YYYY-MM-DD HH:mm:ss")
            let sql, result, where, rows, json, fn, binds
            fn = "temp/HSY_DAILY_DETAIL.xlsx"
            where = `where ((a.idt between ? and ? and a.status = 3) or (a.cdt between ? and ? and a.status = 4)) and a.stax=? and a.status in (3,4) and a.type='01GTKT'`
            binds = [fd, td, fd, td, stax]
            sql = `select id id, DATE_FORMAT(a.idt,'%d/%m/%Y') idt,a.buyer buyer, a.form form,a.serial serial,a.seq seq,a.stax stax,a.btax btax,a.bname bname,a.adjdes adjdes,a.cde cde,a.adjtyp adjtyp,a.doc->"$.dif" dif,a.doc->"$.root" root,a.doc->"$.adj" adj,note note, a.doc->"$.items" items, 1 factor, status from s_inv a ${where} `
            result = await dbs.query(sql, binds)
            rows = result[0]
            let rowbc = [], idarr = []
            let maptype = { "-1": "1", "0": "2", "5": "3", "10": "4", "-2": "5" }

            for (let row of rows) {
                // logger4app.debug("row111", row);

                let ramount = 0
                let rquantity = 0
                let rtotal = 0
                let items = JSON.parse(JSON.stringify(row.items))
                row.itemquantity = 0
                row.itemprice = 0
                row.itemamount = 0
                row.itemvat = 0
                row.itemtotal = 0
                row.form = row.form
                row.formserial = `${row.form} - ${row.serial}`
                row.serial = row.serial
                row.seq = row.seq
                row.idt = row.idt
                row.form = row.form
                row.formserial = `${row.form} - ${row.serial}`
                row.serial = row.serial
                row.seq = row.seq
                row.idt = row.idt
                row.bname = (row.bname != null && row.bname != "") ? row.bname : row.buyer
                row.btax = row.btax
                row.itemname = "Rượu"
                let rowitem = { ...row }

                for (let item of items) {

                    //row.itembname = item.bname
                    let rowitem1 = { ...rowitem }
                    rowitem1.itemname = item.name
                    rowitem1.itemquantity = (item.quantity == null) ? 0 : Number(item.quantity)
                    rowitem1.itemprice = item.price
                    rowitem1.itemamount = (item.amount == null) ? 0 : Number(item.amount)
                    rowitem1.itemvrt = (item.vrt >= 0) ? item.vrt : ""
                    rowitem1.itemvrttype = (maptype[String(item.vrt)]) ? maptype[String(item.vrt)] : ""
                    rowitem1.itemvat = item.vat
                    rowitem1.itemtotal = (item.total == null) ? 0 : Number(item.total)

                    rquantity += item.quantity

                    if (rowitem.status === 4) {
                        rowitem1.itemnote = "Hóa đơn hủy"
                        rowitem1.itemvrt = ""
                        rowitem1.itemamount = 0
                        rowitem1.itemvat = 0
                        rowitem1.itemtotal = 0
                    }
                    else {
                        ramount += Number(item.amount)
                        rtotal += Number(item.total)
                    }
                    rowbc.push(rowitem1)
                }
                if (rowitem.btax == rowitem.stax) {
                    rowitem.itemamount = 0
                }

                if (!idarr.includes(row.id)) {
                    idarr.push(row.id)
                    let rowtotal = {}
                    rowtotal.bname = "Sub Total"
                    rowtotal.itemquantity = rquantity
                    rowtotal.itemamount = ramount
                    rowtotal.itemtotal = !rtotal ? 0 : rtotal

                    rowbc.push(rowtotal)
                }

            }
            json = { table: rowbc }
            const file = path.join(__dirname, "..", "..", "..", fn)
            const xlsx = fs.readFileSync(file)
            const template = new xlsxtemp(xlsx)
            template.substitute(1, json)
            return template.generate({type: 'base64'})
        } catch (err) {
            throw err
        }
    },

    getInvoiceSum: async (req, res, next) => {
        try {
            const query = req.body
            const fm = moment(query.fd), tm = moment(query.td), stax = query.stax
            const fd = fm.startOf("day").format("YYYY-MM-DD HH:mm:ss"), td = tm.endOf("day").format("YYYY-MM-DD HH:mm:ss")
            let sql, result, where, rows, json, fn, binds
            fn = "temp/HSY_DAILY_SUM.xlsx"
            where = `where ((a.idt between ? and ? and a.status = 3) or (a.cdt between ? and ? and a.status = 4)) and a.stax=? and a.status in (3,4) and a.type='01GTKT'`
            binds = [fd, td, fd, td, stax]
            sql = `select id id, DATE_FORMAT(a.idt,'%d/%m/%Y') idt,a.sum sum, a.total total, a.buyer buyer, a.form form,a.serial serial,a.seq seq,a.btax btax,a.bname bname,a.adjdes adjdes,a.cde cde,a.adjtyp adjtyp,a.doc->"$.dif" dif,a.doc->"$.root" root,a.doc->"$.adj" adj,note note, a.doc->"$.items" items, 1 factor, status from s_inv a ${where} `
            result = await dbs.query(sql, binds)
            rows = result[0]
            let rowbc = [], idarr = []
            let maptype = { "-1": "1", "0": "2", "5": "3", "10": "4", "-2": "5" }

            //Lấy danh sách các hóa đơn điều chỉnh
            let sumamount = 0
            let sumquantity = 0
            let sumtotal = 0
            for (let row of rows) {
                let items = JSON.parse(JSON.stringify(row.items))
                // row.itemvrt = 0
                row.itemquantity = 0
                row.itemprice = 0
                row.itemamount = 0
                row.itemvat = 0
                row.itemtotal = 0
                row.form = row.form
                row.sum = (row.sum == null) ? 0 : Number(row.sum)
                row.total = (row.total == null) ? 0 : Number(row.total)
                row.formserial = `${row.form} - ${row.serial}`
                row.serial = row.serial
                row.seq = row.seq
                row.idt = row.idt
                row.form = row.form
                row.formserial = `${row.form} - ${row.serial}`
                row.serial = row.serial
                row.seq = row.seq
                row.idt = row.idt
                row.bname = (row.bname != null && row.bname != "") ? row.bname : row.buyer
                row.btax = row.btax
                row.itemname = "Rượu"
                for (let item of items) {

                    row.itemquantity += (item.quantity == null ) ? 0 : Number(item.quantity)
                    row.itemvrt = (item.vrt >= 0) ? item.vrt : ""
                    row.itemvrttype = (maptype[String(item.vrt)]) ? maptype[String(item.vrt)] : ""

                }

                if (row.status == 4) {
                    row.itemnote = "Hóa đơn hủy"
                    row.itemvrt = ""
                    row.itemamount = 0
                    row.itemvat = 0
                    row.itemtotal = 0
                }
                if (!idarr.includes(row.id)) {
                    idarr.push(row.id)
                    rowbc.push(row)
                }
                sumamount += Number(row.sum)
                sumquantity += row.itemquantity
                sumtotal += Number(row.total)
            }
            let rowtotal = {}
            rowtotal.bname = "Total (Sub total every day)"
            rowtotal.sum = sumamount
            rowtotal.itemquantity = sumquantity
            rowtotal.total = !sumtotal ? 0 : sumtotal
            rowbc.push(rowtotal)

            json = { table: rowbc }

            const file = path.join(__dirname, "..", "..", "..", fn)
            const xlsx = fs.readFileSync(file)
            const template = new xlsxtemp(xlsx)
            template.substitute(1, json)
            return template.generate({type: 'base64'})
        } catch (err) {
            throw err
        }
    },

    get: async (req, res, next) => {
        try {
            const token = sec.decode(req), query = req.body, taxc = token.taxc
            const tn = query.tn, type = query.type, report = query.report,ou=(typeof  query.ou=="undefined"?"*":query.ou), cqtstatus = query.cqtstatus
            const fm = moment(query.fd), tm = moment(query.td)
            const fd = fm.startOf("day").format("YYYY-MM-DD HH:mm:ss"), td = tm.endOf("day").format("YYYY-MM-DD HH:mm:ss")
            const fr = fm.format(config.mfd), to = tm.format(config.mfd), rt = moment().format(config.mfd)
            const now = new Date(), ndate = now.getDate(), nmonth = now.getMonth() + 1, nyear = now.getFullYear()
            let sql, result, where, binds, json, fn, rows, having, where1, where2
            result = await dbs.query("select fadd from s_ou where id=? limit 1", [token.ou])
            let sfaddr
            if (result[0].length > 0) sfaddr = result[0][0].fadd
            if (report == 1) {
                if (config.ent == "yusen") {
                    // fn = "temp/08BKHDVAT_YUSEN.xlsx"
                } else if (config.ent == "hsy") {
                    fn = "temp/08BKHDVAT_HSY.xlsx"
                } else {
                    fn = "temp/08BKHDVAT.xlsx"
                }
                let workbook = new excel.Workbook()
                let worksheet = workbook.addWorksheet("sheet 1")

                numeral.locale("vi")
                const borderStyles = {
                    top: { style: "thin" },
                    left: { style: "thin" },
                    bottom: { style: "thin" },
                    right: { style: "thin" }
                };

                worksheet.columns = [
                    { key: "index", width: 6, style: { alignment: { vertical: 'middle', horizontal: 'center' } } },
                    { key: "form", width: 15 },
                    { key: "serial", width: 20 },
                    { key: "seq", width: 20 },
                    { key: "idt", width: 25 },
                    { key: "bname", width: 40 },
                    { key: "btax", width: 25 },
                    // { key: "sumv", width: 25 },
                    // { key: "vatv", width: 25 },
                    { key: "curr", width: 25 },
                    { key: "exrt", width: 25 },
                    { key: "sum", width: 25 },
                    { key: "vat", width: 25 },
                    { key: "note", width: 60 },
                ];

                worksheet.mergeCells('A1:J1');
                worksheet.getCell('A1').value = 'CỘNG HOÀ XÃ HỘI CHỦ NGHĨA VIỆT NAM'
                worksheet.getCell('A1').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('A1').alignment = { horizontal: 'center' }

                worksheet.mergeCells('A2:J2');
                worksheet.getCell('A2').value = 'ĐỘC LẬP TỰ DO HẠNH PHÚC'
                worksheet.getCell('A2').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('A2').alignment = { horizontal: 'center' }

                worksheet.mergeCells('A6:J6');
                worksheet.getCell('A6').value = 'BẢNG KÊ HOÁ ĐƠN ĐÃ SỬ DỤNG THEO NGƯỜI BÁN'
                worksheet.getCell('A6').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('A6').alignment = { horizontal: 'center' }

                worksheet.mergeCells('A7:J7')
                worksheet.getCell('A7').value = `Từ ngày ${moment(fd).format('DD/MM/YYYY')} đến ngày ${moment(td).format('DD/MM/YYYY')}`
                worksheet.getCell('A7').font = { name: 'Times New Roman', italic: true }
                worksheet.getCell('A7').alignment = { horizontal: 'center' }

                worksheet.mergeCells('I3:J3');
                worksheet.getCell('I3').value = ' Mẫu số: 08/BK-HĐXT'
                worksheet.getCell('I3').border = borderStyles
                worksheet.getCell('I3').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('I3').alignment = { horizontal: 'center' }

                worksheet.mergeCells('I4:J4');
                worksheet.getCell('I4').value = 'Ban hành kèm theo Quyết định số 1209/QĐ-BTC ngày 23/06/2015 của Bộ Tài chính Mẫu biểu báo cáo theo nghị định 119/2018'
                worksheet.getCell('I4').border = borderStyles
                worksheet.getCell('I4').font = { name: 'Times New Roman'}
                worksheet.getCell('I4').alignment = { horizontal: 'center' }

                let sname= token.on 
                worksheet.mergeCells('A8:F8')
                worksheet.getCell('A8').value = `Tên người bán: ${sname||""}`
                worksheet.getCell('A8').font = { name: 'Times New Roman' }
                worksheet.getCell('A8').alignment = { horizontal: 'left' }

                worksheet.mergeCells('G8:J8')
                worksheet.getCell('G8').value = `Đơn vị báo cáo: ${sname||""}`
                worksheet.getCell('G8').font = { name: 'Times New Roman' }
                worksheet.getCell('G8').alignment = { horizontal: 'left' }

                let stax= taxc
                worksheet.mergeCells('A9:F9')
                worksheet.getCell('A9').value = `MST người bán: ${stax}`
                worksheet.getCell('A9').font = { name: 'Times New Roman' }
                worksheet.getCell('A9').alignment = { horizontal: 'left' }

                worksheet.mergeCells('A10:A11')
                worksheet.getCell('A10').value = 'STT'
                worksheet.getCell('A10').border = borderStyles
                worksheet.getCell('A10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('A10').alignment = { horizontal: 'center' }

                worksheet.mergeCells('B10:E10')
                worksheet.getCell('B10').value = 'Hóa đơn, chứng từ bán ra'
                worksheet.getCell('B10').border = borderStyles
                worksheet.getCell('B10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('B10').alignment = { horizontal: 'center' }

                worksheet.mergeCells('F10:F11')
                worksheet.getCell('F10').value = 'Tên đơn vị mua'
                worksheet.getCell('F10').border = borderStyles
                worksheet.getCell('F10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('F10').alignment = { horizontal: 'center' }

                worksheet.mergeCells('G10:G11')
                worksheet.getCell('G10').value = 'Mã số thuế người mua'
                worksheet.getCell('G10').border = borderStyles
                worksheet.getCell('G10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('G10').alignment = { horizontal: 'center' }

                worksheet.mergeCells('H10:H11')
                worksheet.getCell('H10').value = 'Loại Tiền'
                worksheet.getCell('H10').border = borderStyles
                worksheet.getCell('H10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('H10').alignment = { horizontal: 'center' }

                worksheet.mergeCells('I10:I11')
                worksheet.getCell('I10').value = 'Tỷ Giá'
                worksheet.getCell('I10').border = borderStyles
                worksheet.getCell('I10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('I10').alignment = { horizontal: 'center' }

                worksheet.mergeCells('J10:J11')
                worksheet.getCell('J10').value = 'Doanh thu chưa có thuế GTGT'
                worksheet.getCell('J10').border = borderStyles
                worksheet.getCell('J10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('J10').alignment = { horizontal: 'center' }

                worksheet.mergeCells('K10:K11')
                worksheet.getCell('K10').value = 'Thuế GTGT'
                worksheet.getCell('K10').border = borderStyles
                worksheet.getCell('K10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('K10').alignment = { horizontal: 'center' }

                worksheet.mergeCells('L10:L11')
                worksheet.getCell('L10').value = 'Ghi chú'
                worksheet.getCell('L10').border = borderStyles
                worksheet.getCell('L10').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('L10').alignment = { horizontal: 'center' }


                worksheet.mergeCells('B11:D11')
                worksheet.getCell('B11').value = 'Ký hiệu mẫu hóa đơn, ký hiệu hóa đơn, số hóa đơn'
                worksheet.getCell('B11').border = borderStyles
                worksheet.getCell('B11').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('B11').alignment = { horizontal: 'center' }

                worksheet.getCell('E11').value = 'Ngày, tháng, năm lập hóa đơn'
                worksheet.getCell('E11').border = borderStyles
                worksheet.getCell('E11').font = { name: 'Times New Roman', bold: true }
                worksheet.getCell('E11').alignment = { horizontal: 'center' }

                worksheet.getCell('A12').value = '(1)'
                worksheet.getCell('A12').border = borderStyles
                worksheet.getCell('A12').font = { name: 'Times New Roman' }
                worksheet.getCell('A12').alignment = { horizontal: 'center' }

                worksheet.getCell('B12').value = '(2)'
                worksheet.getCell('B12').border = borderStyles
                worksheet.getCell('B12').font = { name: 'Times New Roman' }
                worksheet.getCell('B12').alignment = { horizontal: 'center' }

                worksheet.getCell('C12').value = '(3)'
                worksheet.getCell('C12').border = borderStyles
                worksheet.getCell('C12').font = { name: 'Times New Roman' }
                worksheet.getCell('C12').alignment = { horizontal: 'center' }

                worksheet.getCell('D12').value = '(4)'
                worksheet.getCell('D12').border = borderStyles
                worksheet.getCell('D12').font = { name: 'Times New Roman' }
                worksheet.getCell('D12').alignment = { horizontal: 'center' }

                worksheet.getCell('E12').value = '(5)'
                worksheet.getCell('E12').border = borderStyles
                worksheet.getCell('E12').font = { name: 'Times New Roman' }
                worksheet.getCell('E12').alignment = { horizontal: 'center' }

                worksheet.getCell('F12').value = '(6)'
                worksheet.getCell('F12').border = borderStyles
                worksheet.getCell('F12').font = { name: 'Times New Roman' }
                worksheet.getCell('F12').alignment = { horizontal: 'center' }

                worksheet.getCell('G12').value = '(7)'
                worksheet.getCell('G12').border = borderStyles
                worksheet.getCell('G12').font = { name: 'Times New Roman' }
                worksheet.getCell('G12').alignment = { horizontal: 'center' }

                worksheet.getCell('H12').value = '(8)'
                worksheet.getCell('H12').border = borderStyles
                worksheet.getCell('H12').font = { name: 'Times New Roman' }
                worksheet.getCell('H12').alignment = { horizontal: 'center' }

                worksheet.getCell('I12').value = '(9)'
                worksheet.getCell('I12').border = borderStyles
                worksheet.getCell('I12').font = { name: 'Times New Roman' }
                worksheet.getCell('I12').alignment = { horizontal: 'center' }

                worksheet.getCell('J12').value = '(10)'
                worksheet.getCell('J12').border = borderStyles
                worksheet.getCell('J12').font = { name: 'Times New Roman' }
                worksheet.getCell('J12').alignment = { horizontal: 'center' }

                worksheet.getCell('K12').value = '(11)'
                worksheet.getCell('K12').border = borderStyles
                worksheet.getCell('K12').font = { name: 'Times New Roman' }
                worksheet.getCell('K12').alignment = { horizontal: 'center' }

                worksheet.getCell('L12').value = '(12)'
                worksheet.getCell('L12').border = borderStyles
                worksheet.getCell('L12').font = { name: 'Times New Roman' }
                worksheet.getCell('L12').alignment = { horizontal: 'center' }

                let row_stt = 15
                let dataRange = charList('A', 'L')
                where = `where a.idt between ? and ? and a.stax=? and a.status=3 and a.type='01GTKT'`// and a.cid is null`
                if (config.ent == 'yusen') where = `where a.idt between ? and ? and a.stax=? and a.status in (3,4) and a.type='01GTKT'`// and a.cid is null`
                binds = [fd, td, taxc]
                if (grant && token.ou == token.u) {
                    where += ` and ou in (select cid from s_ou_tree where pid=?)` 
                   
                    binds.push(token.u)
                }               
                if (!ou.includes("*")) {
                    where += ` and a.ou=?`
                    binds.push(ou)
                }
                if (cqtstatus!="*") {
                    where += ` and a.status_received=?`
                    binds.push(cqtstatus)
                }
                let taxsql = /*`case when a.adjtyp=2 then a.doc->"$.tax" else a.doc->"$.tax" end`*/`a.doc->"$.tax"`
                if (config.ent == 'ghtk') taxsql = `a.doc->"$.items"`
                //Lấy các hóa đơn đã duyệt, thêm trường factor đánh dấu hệ số dương
                sql = `select id id, DATE_FORMAT(a.idt,'%d/%m/%Y') idt,a.form form,a.serial serial,a.seq seq,a.stax stax,a.btax btax,a.bname bname,a.adjdes adjdes,a.cde cde,a.adjtyp adjtyp,a.doc->"$.dif" dif,a.doc->"$.root" root,a.doc->"$.adj" adj,${taxsql} taxs,note note, a.doc->"$.items" items,a.doc->"$.adjType" adjtype, 1 factor, status, a.c5 c5,a.sumv,a.vatv,a.totalv,a.curr,a.exrt from s_inv a ${where} `
                if (config.BKHDVAT_EXRA_OPTION == '2') {
                    //Lấy hóa đơn hủy thay thế => status = 4, ngày lấy theo dt, factor đánh dấu hệ số âm
                    where1 = `where a.cdt between ? and ? and a.stax=? and a.status=4 and a.type='01GTKT' `

                    sql += ` union all select id id, DATE_FORMAT(a.cdt,'%d/%m/%Y') idt,a.form form,a.serial serial,a.seq seq,a.stax stax,a.btax btax,a.bname bname,a.adjdes adjdes,a.cde cde,a.adjtyp adjtyp,a.doc->"$.dif" dif,a.doc->"$.root" root,a.doc->"$.adj" adj,${taxsql} taxs,CASE WHEN (doc->"$.cancel.rea" IS NOT NULL and doc->"$.cancel.rea" <> "") THEN doc->"$.cancel.rea" ELSE cde END note, a.doc->"$.items" items,a.doc->"$.adjType" adjtype, -1 factor, status,a.c5 c5,a.sumv,a.vatv,a.totalv,a.curr,a.exrt from s_inv a ${where1} `
                    
                    binds.push(fd)
                    binds.push(td)
                    binds.push(taxc)
                    if (cqtstatus!="*") {
                        sql += ` and a.status_received=?`
                        binds.push(cqtstatus)
                    }

                    //Hóa đơn bị hủy, thay thế => status = 4, ngày lấy theo idt, factor đánh dấu hệ số dương
                    where2 = `where a.idt between ? and ? and a.stax=? and a.type='01GTKT' and a.status=4 `
                    sql += ` UNION ALL select id id, DATE_FORMAT(a.idt,'%d/%m/%Y') idt,a.form form,a.serial serial,a.seq seq,a.stax stax,a.btax btax,a.bname bname,a.adjdes adjdes,a.cde cde,a.adjtyp adjtyp,a.doc->"$.dif" dif,a.doc->"$.root" root,a.doc->"$.adj" adj,${taxsql} taxs,note note, a.doc->"$.items" items, a.doc->"$.adjType" adjtype, 1 factor, status,a.c5 c5,a.sumv ,a.vatv ,a.totalv,a.curr,a.exrt  from s_inv a ${where2} `
                    binds.push(fd)
                    binds.push(td)
                    binds.push(taxc)
                    if (cqtstatus!="*") {
                        sql += ` and a.status_received=?`
                        binds.push(cqtstatus)
                    }
                }
                sql = 'Select * from (' + sql + ') as t order by idt,form,serial,seq'
                result = await dbs.query(sql, binds)
                rows = result[0]
                //Begin HUNGLQ them doan lay du lieu dong theo danh muc loai thue
                //Lay ra danh muc loai thue suat truoc
                let arr = JSON.parse(JSON.stringify(config.CATTAX))
                //Sap xep theo ty le thue vrt truoc, phai nhan voi 100 khong thi vrt 7 lai lon hon 10
                for (let itemArr of arr) {
                    itemArr.sortprop = String(itemArr.vrt * 100).padStart(config.SEQ_LEN, "0")
                }
                arr = util.sortobj(arr, 'sortprop', 1)
                let objRep = {}
                //Lap danh muc loai thue suat de tao key object truoc
                for (let vVat of arr) {
                    //Dat key la ky tu dau va vrt nhan voi 100 de co so nguyen (de phong truong hop vrt = 5.26, 3.5)
                    let vatKey = `VAT${vVat.vrt * 100}`
                    //Neu dinh thay bang key la orriginal vrt thi bo comment doan duoi nay
                    //let vatKey = `VAT${vVat.original_vrt * 100}`
                    objRep[vatKey] = {
                        totalSumByVrt: 0, //Tong cua du lieu sum theo tung vrt, gan truoc bang 0
                        totalVatByVrt: 0, //Tong cua du lieu vat theo tung vrt, gan truoc bang 0
                        index: 0, //gan so thu tu, gan truoc bang 0
                        key_vrn: vVat.vrnVATRep, // danh dáu key len dong dien dai
                        dataVat: [] //Du lieu bao cao, gan bang mang rong truoc, ti nua push du lieu sau
                    }
                }
                //End HUNGLQ them doan lay du lieu dong theo danh muc loai thue
                if (config.ent == "hsy") {
                    let rowbc = await Service.getBKHDDT(rows)
                    logger4app.debug(rowbc)
                    json = { table: rowbc }
                } else {
                    for (const row of rows) {
                        let  c5 ,idt, form, serial, seq, btax, bname, taxs = util.parseJson(row.taxs)
                        if (config.ent == "ghtk") {
                            taxs = await calctax(taxs,1)
                        }
                        if (row.adjtyp == 2) {
                            let hd 
                            if (row.adj) {
                                hd = util.parseJson(row.adj)
                            }
                            row.note += `Điều chỉnh tăng/ giảm thông tin cho HĐ ${hd.seq}`
                        }
                        if (row.adjtyp == 1) {
                            let hdtt
                            if (row.adj) {
                                hdtt = util.parseJson(row.adj)
                            }
                            row.note += `Thay thế cho hóa đơn ${hdtt.seq}`
                        }
                        if(row.cde && row.cde.includes('điều chỉnh')){
                            let result_seq = await dbs.query('select seq from s_inv where pid=? and form=? and serial=?', [row.id,row.form,row.serial])
                            let row_seq = result_seq[0]
                            let seq_new
                            if(row_seq.length>0) seq_new = row_seq[0].seq
                            row.note += `${row.note ? '\n':''}Bị điều chỉnh cho HĐ ${row.form}-${row.serial}-${seq_new}`
                        }
                        if(row.cde && row.cde.includes('thay thế')){
                            let result_seq = await dbs.query('select seq from s_inv where pid=? and form=? and serial=?', [row.id,row.form,row.serial])
                            let row_seq = result_seq[0]
                            let seq_new
                            if(row_seq.length>0) seq_new = row_seq[0].seq
                            row.note += `${row.note ? '\n':''}Bị thay thế cho HĐ ${row.form}-${row.serial}-${seq_new}`
                        }
                            //note cu
                            // if (row.adjdes != null) {
                                // let root = (row.root != null) ? row.root : {}
                                // idt = moment(root.idt).format(config.mfd)
                                // let rootadj = row.adj
                                // if (rootadj.seq) {
                                //     let arrseq = String(rootadj.seq).split("-")
                                //     if (arrseq.length >= 3) {
                                //         form = arrseq[0]
                                //         serial = arrseq[1]
                                //         seq = arrseq[2]
                                //     }
                                // }
                                // row.note = row.adjdes
                                //row.note = `Bị điều chỉnh bởi HĐ ${row.form}-${row.serial}-${row.seq}`
                                // btax = root.btax
                                // bname = root.bname
                            // }
                            // else {
                                // if (row.cde != null) {
                                    // row.note = row.cde
                                // }
                                // idt = row.idt
                                // form = row.form
                                // serial = row.serial
                                // seq = row.seq
                                // btax = row.btax
                                // bname = row.bname
                            // }
                        
                        //Dữ liệu của hóa đơn nào lấy của hóa đơn đấy
                        c5= row.c5
                        idt = row.idt
                        form = row.form
                        serial = row.serial
                        seq = row.seq
                        btax = row.btax
                        bname = row.bname 
                        let vbname = {bname : bname}
                        
                        for (const c of config.SPECIAL_CHAR) {
                            vbname = JSON.parse(JSON.stringify(vbname).replace(c, ""))
                            bname = vbname.bname
                        }
                         if (config.ent == "yusen") {//doi voi yushen lay tien tong vi chi co 1 thua xuat,sua loi lẹch tien do sua tong hoa don
                             let taxct = []
                             if(taxs.length>0) taxct.push(taxs[0]) 
                            
                             taxs = taxct
                         }
                         if(((row.status != 4) && (config.ent == "yusen")) || (config.ent != "yusen") ){
                            if (row.adjtyp == 2) {
                                //Nếu chênh lệch dương thì gán số ấm vì là điều chỉnh giảm
                                if (row.dif) {
                                    let dif = util.parseJson(row.dif)
                                    if (dif.total > 0) row.factor = -1 * row.factor
                                }
                                else {
                                    if (row.adj) {
                                        let adj = util.parseJson(row.adj)
                                        if (adj.adjType && adj.adjType == '3') row.factor = -1 * row.factor
                                    }
                                }
                            }   
                        }
                        
                        if(row.c5 == "CKTM"){
                            row.factor = -1 * row.factor
                        }
                        for (const tax of taxs) {
                            

                            let vrt = Number(tax.vrt), s = util.isNumber(tax.amtv) ? tax.amtv * row.factor : 0, v = util.isNumber(tax.vatv) ? (tax.vatv * row.factor) : 0
                            if(row.curr=="VND") {
                                s = util.isNumber(tax.amtv) ? Math.abs(tax.amtv) * row.factor : 0
                                v = util.isNumber(tax.vatv) ? (Math.abs(tax.vatv) * row.factor) : 0
                            } else {
                                s = util.isNumber(tax.amt) ? Math.abs(tax.amt) * row.factor : 0
                                v = util.isNumber(tax.vat) ? (Math.abs(tax.vat) * row.factor) : 0
                            }
                            let r = { idt: idt, form: form, serial: serial, seq: seq, btax: btax, bname, sum: s, vav: v, note: row.note, curr:row.curr, exrt: row.exrt }
                            if (config.ent == "yusen") {
                                s =  row.sumv * row.factor ;
                                v =  row.vatv * row.factor ;
                                r = { idt: idt, form: form, serial: serial, seq: seq, btax: btax, bname, sumv: s, vatv: v, note: row.note};
                            }
                            //Begin HUNGLQ them doan lay du lieu dong theo danh muc loai thue
                            //Bat dau day du lieu vao objRep, neu doan nay on bo cai switch o duoi di
                            const vatKey = `VAT${vrt * 100}`
                            objRep[vatKey].dataVat.push(r)
                            objRep[vatKey].totalSumByVrt += s
                            objRep[vatKey].totalVatByVrt += v
                            //End HUNGLQ them doan lay du lieu dong theo danh muc loai thue
                        }
                    }
                    // json = { stax: taxc, sname: token.on, fd: fr, td: to, rt: rt, a0: a0, s0: s0, a5: a5, s5: s5, v5: v5, a10: a10, s10: s10, v10: v10, a1: a1, s1: s1, a2: a2, s2: s2, a526: a526, s526: s526, v526: v526, s: (s0 + s5 + s10 + s1 + s2 + s526), v: (v5 + v10 + v526) }
                    //Begin HUNGLQ them doan lay du lieu dong theo danh muc loai thue
                    //Lay du lieu va day ra bien lu danh sach bao cao 
                    //Khai bao bien day ra bao cao
                    let s=0,v=0
                    for (var key in objRep) {
                        //Neu mang co du lieu thi moi xu
                        if (objRep[key].dataVat.length <= 0) continue
                        //push cai dong dien giai truoc
                        let rowname
                        rowname = worksheet.addRow({ index: objRep[key].key_vrn , form: "", serial: "", seq: "", idt: "",bname: "",btax: "",sum: "",vat: "", note: "", curr: "", exrt: ""})
                        worksheet.mergeCells(row_stt, 1, row_stt, 12)
                        rowname.getCell('A').border = borderStyles
                        rowname.alignment = { horizontal: 'left' }
                        rowname.font = { name: 'Times New Roman', bold: true }
                        row_stt = row_stt + 1
                        //Push tiep du lieu bao cao
                        for (const rep of objRep[key].dataVat) {
                            rep.index = ++objRep[key].index
                            //danh dau de format font chữ checksum
                            // rep.checksum = 0
                            // arrRep.push(rep)
                            let rowdata = worksheet.addRow(rep)
                            rowdata.font = { name: 'Times New Roman' }
                            //set border for range of data
                            dataRange.map(x => {
                                rowdata.getCell(x).border = borderStyles
                                if(x=='L') rowdata.getCell(x).alignment = { wrapText:true }
                            })
                            row_stt = row_stt + 1
                        }
                        // arrRep.push({index:`TỔNG`,sumv:objRep[key].totalSumByVrt,vatv:objRep[key].totalVatByVrt,checksum:1})
                        rowname = worksheet.addRow({ index: `TỔNG` , form: "", serial: "", seq: "", idt: "",bname: "",btax: "",sum:objRep[key].totalSumByVrt,vat: objRep[key].totalVatByVrt, note: "", curr:"", exrt: ""})
                        worksheet.mergeCells(row_stt, 1, row_stt, 7)
                        worksheet.getCell(`H${row_stt}`).border = borderStyles
                        worksheet.getCell(`I${row_stt}`).border = borderStyles
                        worksheet.getCell(`J${row_stt}`).border = borderStyles
                        rowname.getCell('A').border = borderStyles
                        worksheet.getCell(`A${row_stt}`).alignment = { horizontal: 'left' }
                        // rowname.alignment = { horizontal: 'left' }
                        rowname.font = { name: 'Times New Roman', bold: true }
                        row_stt = row_stt + 1
                        s+=objRep[key].totalSumByVrt
                        v+=objRep[key].totalVatByVrt
                        //Push cai dong tong
                    }
                    //End HUNGLQ them doan lay du lieu dong theo danh muc loai thue 
                    // json = { stax: taxc, sname: token.on, fd: fr, td: to, rt: rt, arrRep: arrRep,s:s,v:v }

                    // return json
                    worksheet.mergeCells(row_stt, 1, row_stt, 9)
                    worksheet.getCell(`A${row_stt}`).value = `${rt}`
                    worksheet.getCell(`A${row_stt}`).border = borderStyles
                    worksheet.getCell(`A${row_stt}`).font = { name: 'Times New Roman' }
                    worksheet.getCell(`A${row_stt}`).alignment = { horizontal: 'left' }

                    worksheet.getCell(`J${row_stt}`).value = `${s}`
                    worksheet.getCell(`J${row_stt}`).border = borderStyles
                    worksheet.getCell(`J${row_stt}`).font = { name: 'Times New Roman', bold: true }
                    worksheet.getCell(`J${row_stt}`).alignment = { horizontal: 'right' }


                    worksheet.getCell(`K${row_stt}`).value = `${v}`
                    worksheet.getCell(`K${row_stt}`).border = borderStyles
                    worksheet.getCell(`K${row_stt}`).font = { name: 'Times New Roman', bold: true }
                    worksheet.getCell(`K${row_stt}`).alignment = { horizontal: 'right' }


                    const buffer = await workbook.xlsx.writeBuffer()
                    res.end(buffer, "binary")
                    return
                }
            }
            else if (report == 2) {
                fn = "temp/08BKHDXT.xlsx"
                where = "where idt between ? and ? and stax=?"
                binds = [fd, td, taxc]
                if (grant && token.ou == token.u) {
                    where += ` and ou in (select cid from s_ou_tree where pid=?)` 
                   
                    binds.push(token.u)
                }               
                if (!ou.includes("*")) {
                    where += ` and ou=?`
                    binds.push(ou)
                }
                if (type !== "*") {
                    where += " and type=?"
                    binds.push(type)
                }
                if (cqtstatus!="*") {
                    where += ` and status_received=?`
                    binds.push(cqtstatus)
                }
                sql = `select DATE_FORMAT(idt,'%d/%m/%Y') idt,c5 c5,form form,serial serial,seq seq,btax btax,bname bname,buyer buyer,CASE WHEN status=1 THEN 'Chờ cấp số' WHEN status=2 THEN 'Chờ duyệt' WHEN status=3 THEN 'Đã duyệt' WHEN status=4 THEN 'Đã hủy' WHEN status=6 THEN 'Chờ hủy' ELSE status END status,status cstatus, sumv sumv,vatv vatv,CONCAT_WS('-',note,cde,adjdes) note from s_inv ${where} order by idt,form,serial,seq`
                result = await dbs.query(sql, binds)
                rows = result[0]
                let i = 0
                for (const row of rows) {
                    row.index = ++i
                    if(row.c5 == "CKTM"){
                        row.sumv = row.sumv * -1
                        row.vatv = row.vatv * -1
                    }
                    if (row.cstatus == 4) {
                        delete row["sumv"]
                        delete row["vatv"]
                    }
                }
                json = { stax: taxc, sname: token.on, fd: fr, td: to, rt: rt, tn: tn, table: rows }
            }
            else if (report == 3) {
                let conn,data=[]
             try{
                        conn = await dbs.getConnection()
                        await conn.beginTransaction()
                        fn = "temp/05BCHD.xlsx"
                        let where, binds, type_name,where_iv
                        if (type == "*") {
                            where = ""
                            binds = [ fd, taxc, fd, td, taxc, fd, td, taxc]
                        }
                        else {
                            where = " and type=?"
                            binds = [ fd, taxc, type, fd, td, taxc, type, fd, td, taxc, type]
                            type_name = tenhd(type)
                        }
                        //{trungpq10 them tieu chi tim kiem theo chi nhanh
                        where_iv = where
                        if (grant && token.ou == token.u) {
                            where_iv += ` and ou in (select cid from s_ou_tree where pid=?)` 
                            binds = [ fd, taxc, type,token.u, fd, td, taxc, type,token.u, fd, td, taxc, type,token.u]
        
                        }               
                        if (!ou.includes("*")) {
                            where_iv += ` and ou=?`
                            binds = [fd, taxc, type,token.u,ou, fd, td, taxc, type,token.u,ou,  fd, td, taxc, type,token.u,ou]
                        }
                        // sua dap ung nhieu dai so khac nhau chung ky hieu
                        sql =  `select form,serial,0 min1,0 max1,min min0,max max0,0 maxu0,0 minu,0 maxu, 0 minc,0 maxc,0 cancel,null clist from s_serial where taxc=? and fd<? and   (status not in (2,3,4) or (td>=? and status=2 ) or (td>=? and status=4 )) ${where} order by form,serial,min`
                        result = await dbs.query(sql, [taxc, fd,fd,fd,type])
                        rows = result[0]
                        let rows_serial = [],formc = "",serialc = "",bindsc = [],rows_serialarr = []
                        for (const row of rows)
                        {
                            if (formc==row.form && serialc==row.serial)
                            {
                                for (let rowc of rows_serial){
                                    if(rowc.form==row.form && rowc.serial==row.serial) {
                                        rowc.max0 = row.max0
                                    }
                                
                                }
                            }else{
                                rows_serial.push(row)
                            }
                            formc=row.form
                            serialc=row.serial
                        }
                        let c_id = moment(new Date()).format("YYYYMMDDHHHHmmssSSS")
                        for (let rowc of rows_serial){
                           
                            rowc.c_id=c_id
                            rowc=Object.values(rowc)
                            rows_serialarr.push(rowc)
                        }
                       
                        if(rows_serialarr.length>0){
                            await conn.query("insert into s_report_tmp(form,serial,min1, max1, min0, max0, maxu0,minu, maxu,minc,maxc, cancel,clist,c_id) values ?", [rows_serialarr])
                        }
                        

                        sql =  `select form,serial,min min1,max max1,0 min0,0 max0,0 maxu0,0 minu,0 maxu,0 minc,0 maxc,0 cancel,null clist from s_serial where taxc=? and fd between ? and ? and status !=3 ${where} order by form,serial,min`
                        result = await dbs.query(sql, [taxc, fd, td,type])
                        rows = result[0]
                         rows_serial=[],formc="",serialc="",bindsc=[],rows_serialarr=[]
                        for (const row of rows)
                        {
                            if (formc==row.form && serialc==row.serial)
                            {
                                for (let rowc of rows_serial){
                                    if(rowc.form==row.form && rowc.serial==row.serial) {
                                        rowc.max1 = row.max1
                                    }
                                
                                }
                            }else{
                                rows_serial.push(row)
                            }
                            formc=row.form
                            serialc=row.serial
                        }
                        let c_id2 = moment(new Date()).format("YYYYMMDDHHHHmmssSSS")
                        for (let rowc of rows_serial){
                           
                            rowc.c_id=c_id2
                            rowc=Object.values(rowc)
                            rows_serialarr.push(rowc)
                        }
                       
                        if(rows_serialarr.length > 0){
                            await conn.query("insert into s_report_tmp(form,serial,min1, max1, min0, max0, maxu0,minu, maxu,minc,maxc,  cancel,clist,c_id) values ?", [rows_serialarr])
                        }

                        sql =  `select form,serial,0 min1,0 max1,0 min0,0 max0,0 maxu0,0 minu,0 maxu, cur minc,max maxc,0 cancel,null clist from s_serial where taxc=? and fd < ? and td between ? and ?  and status =2 ${where} order by form,serial,min`
                        result = await dbs.query(sql, [taxc,  td,fd,td,type])
                        rows = result[0]
                         rows_serial=[],formc="",serialc="",bindsc=[],rows_serialarr=[]
                        for (const row of rows)
                        {
                            if (formc==row.form && serialc==row.serial)
                            {
                                for (let rowc of rows_serial){
                                    if(rowc.form==row.form && rowc.serial==row.serial) {
                                        rowc.maxc = row.maxc
                                    }
                                
                                }
                            }else{
                                rows_serial.push(row)
                            }
                            formc=row.form
                            serialc=row.serial
                        }
                        let c_id3 = moment(new Date()).format("YYYYMMDDHHHHmmssSSS")
                        for (let rowc of rows_serial){
                           
                            rowc.c_id=c_id3
                            rowc=Object.values(rowc)
                            rows_serialarr.push(rowc)
                        }
                       
                        if(rows_serialarr.length > 0){
                            await conn.query("insert into s_report_tmp(form,serial,min1, max1, min0, max0, maxu0,minu, maxu,minc,maxc, cancel,clist,c_id) values ?", [rows_serialarr])
                        }
                    
                        sql = `select a.form form,a.serial serial,sum(a.min1) min1,sum(a.max1) max1,sum(a.min0) min0,sum(a.max0) max0,sum(a.maxu0) maxu0,sum(a.minu) minu,sum(a.maxu) maxu,sum(a.minc) minc,sum(a.maxc) maxc,sum(a.cancel) cancel,max(a.clist) clist from (
                        select form,serial,0 min1,0 max1,min0,max0,0 maxu0,0 minu,0 maxu,0 minc,0 maxc,0 cancel,null clist from s_report_tmp where c_id= ${c_id}
                        union
                        select form,serial,min1,max1,min0,max0,maxu0,minu,maxu,0 minc,0 maxc,cancel,null clist from s_report_tmp where c_id= ${c_id2}
                        union
                        select form,serial,min1,max1,min0,max0,maxu0,minu,maxu,minc,maxc,cancel,null clist from s_report_tmp where c_id= ${c_id3}
                        union
                        select form,serial,0 min1,0 max1,0 min0,0 max0,max(CAST(seq as UNSIGNED)) maxu0,0 minu,0 maxu,0 minc,0 maxc,0 cancel,null clist from s_inv where idt<? and stax=? and status>2 ${where_iv} group by form,serial
                        union
                        select form,serial,0 min1,0 max1,0 min0,0 max0,0 maxu0,min(CAST(seq as UNSIGNED)) minu,max(CAST(seq as UNSIGNED)) maxu,0 minc,0 maxc,sum(CASE WHEN status=4 THEN 1 ELSE 0 END) cancel,null clist from s_inv where idt between ? and ? and stax=? and status>2 ${where_iv} group by form,serial
                        union
                        select form,serial,0 min1,0 max1,0 min0,0 max0,0 maxu0,0 minu,0 maxu,0 minc,0 maxc,0 cancel,JSON_ARRAYAGG(CAST(seq as UNSIGNED)) clist from s_inv where idt between ? and ? and stax=? and status=4 ${where_iv} group by form,serial
                        ) a group by a.form,a.serial`
                        result = await conn.query(sql, binds)
                        rows = result[0]
                        await conn.query(`Delete from s_report_tmp where c_id=${c_id}`)
                        await conn.query(`Delete from s_report_tmp where c_id=${c_id2}`)
                        await conn.query(`Delete from s_report_tmp where c_id=${c_id3}`)
                        
                        let i = 0
                       
                        for (let row of rows) {
                            row.index = ++i
                            row.type_name = type == "*" ? tenhd(row.form.substr(0,6)) : type_name
                            const clist = row.clist
                            if (util.isEmpty(clist)) {
                                delete row.cancel
                            }
                            else {
                                let arr = clist.sort((a, b) => a - b), len = arr.length, str
                                if (len === 1) {
                                    str = arr[0].toString().padStart(config.SEQ_LEN, "0")
                                }
                                else if (len === 2) {
                                    const cs = arr[0] + 1 === arr[1] ? "-" : ";"
                                    for (let i = 0; i < len; i++) {
                                        arr[i] = arr[i].toString().padStart(config.SEQ_LEN, "0")
                                    }
                                    str = arr.join(cs)
                                }
                                else {
                                    let del = []
                                    for (let i = 1; i < len; i++) {
                                       
                                        if (Number(arr[i - 1]) + 2 === Number(arr[i + 1])) del.push(i)
                                    }
                                    for (let i = 0; i < len; i++) {
                                        if (del.includes(i)) arr[i] = null
                                        else arr[i] = arr[i].toString().padStart(config.SEQ_LEN, "0")
                                    }
                                    str = arr.join(";").replace(/\;\;+/g, '-')
                                }
                                row.clist = str
                            }
                            let max0 = parseInt(row.max0), min0 = parseInt(row.min0), max1 = parseInt(row.max1), min1 = parseInt(row.min1), maxu0 = parseInt(row.maxu0), minu = parseInt(row.minu), maxu = parseInt(row.maxu), cancel = parseInt(row.cancel),minc=parseInt(row.minc),maxc=parseInt(row.maxc)
                            //use
                            let su = 0, s0 = 0
                            if (maxu > 0) {
                                su = maxu - minu + 1
                                row.su = su
                                row.fu = minu.toString().padStart(7, '0')
                                row.tu = maxu.toString().padStart(7, '0')
                                row.used = cancel > 0 ? (su - cancel) : su
                            }
                            //tondau
                            if (max1 > 0) {
                                if(max0 > 0){
                                    s0 = max1 - min0 + 1
                                    row.s0 = s0
                                   
                                }else{
                                    s0 = max1 - min1 + 1
                                    row.s0 = s0
                                   
                                }
                                row.f1 = min1.toString().padStart(7, '0')
                                row.t1 = max1.toString().padStart(7, '0')
                            }
                             if (max0 > 0) {
                                if (maxu0 > 0) {
                                    if (max0 > maxu0) {
                                        if(max1 > 0){
                                            s0 = max1 - maxu0
                                        }else{
                                            s0 = max0 - maxu0
                                        }
                                       
                                        row.s0 = s0
                                        row.f0 = (maxu0 + 1).toString().padStart(7, '0')
                                        row.t0 = max0.toString().padStart(7, '0')
                                    }
                                    if (max0 == maxu0 && max1>maxu0) {
                                        s0 = max1 - maxu0
                                        row.s0 = s0
                                       
                                    }
                                }
                                else {
                                    if(max1 > 0){
                                        s0 = max1 - min0 + 1
                                    }else{
                                        s0 = max0 - min0 + 1
                                    }
                                   // s0 = max0 - min0 + 1
                                    row.s0 = s0
                                    row.f0 = min0.toString().padStart(7, '0')
                                    row.t0 = max0.toString().padStart(7, '0')
                                }
                            }
                            
                            //toncuoi
                            if (maxu > 0) {
                                if (s0 > su) {
                                    row.s2 = s0 - su
                                    row.f2 = (maxu + 1).toString().padStart(7, '0')
                                    if (max1 > 0) row.t2 = row.t1
                                    else if (max0 > 0) row.t2 = row.t0
                                }
                            }
                            else {
                                if (max1 > 0) {
                                    if(min0 > 0){
                                        row.s2=  max1 - (maxu0==0?min0:maxu0+1) + 1
                                        row.f2 = (maxu0==0?min0:maxu0+1).toString().padStart(7, '0')
                                        row.t2 = row.t1
                                    }else{
                                        row.s2=  max1 - min1 + 1
                                        row.f2 = min1.toString().padStart(7, '0')
                                        row.t2 = row.t1
                                    }
                                  
                                }
                                else if (max0 > 0) {
                                    row.s2 = s0
                                    row.f2 = row.f0
                                    row.t2 = row.t0
                                }
                            }
                            //huy ph
                            if(maxc>0){
                               
                               
                                minc= (minc==0?(maxu0==0?(min0>0?min0-1:(maxu>0?(min1+maxu-1):min1-1)):maxu0):minc)+1
                                minc= (minc==0?1:minc)
                                row.countc = maxc - (minc)+1
                                row.listc = `${minc.toString().padStart(7, '0')} - ${maxc.toString().padStart(7, '0')}`
                                if(Number(maxc) >=Number( row.t2)){
                                    row.s2=  0
                                    row.f2 = ""
                                    row.t2 = ""
                                }
                               
                                if ( row.used >0) row.used=row.used- row.countc

                            }else{
                               
                            }
                         //Tổng số sử dụng, xóa bỏ, mất, hủy

                          if(maxc>0)  
                          {   
                              if(maxc>maxu)
                              {
                                  su = maxc - (minu==0?minc:minu) 
                                  row.su = su+1
                                  row.fu =(minu==0? minc.toString().padStart(7, '0'):minu.toString().padStart(7, '0'))
                                  row.tu = maxc.toString().padStart(7, '0')
                              }else{
                               // su = maxc - (minu==0?minc:minu) 
                               // row.su = su+1
                               //   row.fu =(minu==0? minc.toString().padStart(7, '0'):minu.toString().padStart(7, '0'))
                                 // row.tu = maxc.toString().padStart(7, '0')
                              }
                           
                          }  
                          else  
                          {
                            if(maxu>0){
                              row.tu = maxu.toString().padStart(7, '0')
                            }
                          }
                          
                          if(typeof row.countc=="undefined")row.countc=0
                          if(typeof row.s0=="undefined")row.s0=0
                          if(typeof row.su=="undefined")row.su=0
                          if(typeof row.used=="undefined")row.used=0
                          if(typeof row.cancel=="undefined")row.cancel=0
                          if(typeof row.s2=="undefined")row.s2=0

                          if ((row.countc==0) && (row.s0==0) && (row.su==0) && (row.used==0) && (row.cancel==0) && (row.s2==0)){

                          }else{
                            data.push(row)
                          }

                        }
                        await conn.commit()
                   }catch (err) {
                    await conn.rollback()
                    next(err)
                  }
                  finally {
                    await conn.release()
                  }
                json = { stax: taxc, sfaddr: sfaddr, now, ndate, nmonth, nyear, sname: token.on, period: `Kỳ báo cáo ${query.period}`, fd: fr, td: to, rt: rt, tn: tn, table: data }
            }
            else if (report == 5) {
                fn = "temp/BCDetail.xlsx"
                where = " ((idt between ? and ?) or (cdt between ? and ?)) and stax=?"
                let order = " order by stax,form,serial,seq"
                binds = [fd, td, fd, td, taxc]
                if (type != "*") {
                    where += " and type=?"
                    binds.push(type)
                }
                //{trungpq10 them tieu chi tim kiem theo chi nhanh
                if (grant && token.ou == token.u) {
                    where += ` and ou in (select cid from s_ou_tree where pid=?)` 
                    binds.push(token.u)
 
                }               
                if (ou != "*") {
                    where += ` and ou=?`
                    binds.push(ou)
                }
                sql = `select type, form, serial, seq "seq", idt, dt, c0 "polnum", c1 "duedate", totalv, c3 "paymode", c4 "lifeins" ,bname ,baddr, btax, c5 "paydate", c6 "effectdate", c7 "content", c8 "servname", bmail, cdt,
                CASE WHEN status=1 THEN 'Chờ cấp số' WHEN status=2 THEN 'Chờ duyệt' WHEN status=3 THEN 'Đã duyệt' WHEN status=4 THEN 'Đã hủy' WHEN status=6 THEN 'Chờ hủy' ELSE status END istatus,
                CASE WHEN adjtyp=1 THEN 'Thay thế' WHEN adjtyp=2 THEN 'Điều chỉnh' ELSE cde END kind,
                doc->'$.adj' "adj", doc->'$.cancel' "cancel", doc->'$.items' "items",
                status, cde, adjtyp  from s_inv where ${where} ${order}`
                result = await dbs.query(sql, binds)
                rows = result[0]
                let tableD = []
				let indx = 1
                rows.forEach((row) => {
                    row.type = tenhd(row.type)
                    row.dt = moment(row.dt).format(mfd)
                    row.idt = moment(row.idt).format(mfd)
                    let duedate = moment(row.duedate), paydate = moment(row.paydate), effectdate = moment(row.effectdate), cdt = moment(row.cdt)
                    row.duedate = duedate.isValid() ? duedate.format(mfd) : ""
                    row.paydate = paydate.isValid() ? paydate.format(mfd) : ""
                    row.effectdate = effectdate.isValid() ? effectdate.format(mfd) : ""
                    row.cdt = cdt.isValid() ? cdt.format(mfd) : ""
                    if (![1,2].includes(row.adjtyp)) {
                        row.kind = row.kind ? row.kind.split(" ").splice(0,3).join(" ") : ""
                    }
                    let rea = "", ref = ""
                    if (row.cancel) {
                        rea = row.cancel.rea
                        ref = row.cancel.ref
                    } else if (row.adj) {
                        rea = row.adj.rea
                        ref = row.adj.ref
                    } 
                    row.totalv = parseInt(row.totalv)
                    row.rea = rea
                    row.ref = ref
                    row.tsMail = (row.bmail && ((row.status == 3 && sendApprMail) || (row.status == 4))) ? row.dt : ''
                    row.items.forEach((v,i) => {
                        let newv = JSON.parse(JSON.stringify(row))
						newv.id = indx++
                        newv.itemName = v.name
                        tableD.push(newv)
                    })
                })
                let groupby = " group by stax, form, serial"
                sql = `
                select stax,form,serial,sum(totalv) "totalv",sum(total) "total", count(1) "can",0 "appr", 0 "wappr", 0 "wseq", 0 "wcan" from s_inv where ${where} and status = 4 ${groupby}
                union
                select stax,form,serial,sum(totalv) "totalv",sum(total) "total", 0 "can",count(1) "appr", 0 "wappr", 0 "wseq", 0 "wcan" from s_inv where ${where} and status = 3 ${groupby}
                union
                select stax,form,serial,sum(totalv) "totalv",sum(total) "total", 0 "can",0 "appr", count(1) "wappr", 0 "wseq", 0 "wcan" from s_inv where ${where} and status = 2 ${groupby}
                union
                select stax,form,serial,sum(totalv) "totalv",sum(total) "total", 0 "can",0 "appr", 0 "wappr", count(1) "wseq", 0 "wcan" from s_inv where ${where} and status = 1 ${groupby}
                union
                select stax,form,serial,sum(totalv) "totalv",sum(total) "total", 0 "can",0 "appr", 0 "wappr", 0 "wseq", count(1) "wcan" from s_inv where ${where} and status = 6 ${groupby}
                order by stax,form,serial`
                binds.push(...binds,...binds,...binds,...binds)
                result = await dbs.query(sql, binds)
                rows = result[0]
                let tableS = [], sumAmt = 0, sumTotal = 0
                rows.forEach((row,index) => {
                    let name = ""
                    if (row.can) {
                        name = "Đã hủy"
                        row.amt = row.can
                    } else if (row.appr) {
                        name = "Đã duyệt"
                        row.amt = row.appr
                    } else if (row.wappr) {
                        name = "Chờ duyệt"
                        row.amt = row.wappr
                    } else if (row.wseq) {
                        name = "Chờ cấp số"
                        row.amt = row.wseq
                    } else if (row.wcan) {
                        name = "Chờ hủy"
                        row.amt = row.wcan
                    }
                    row.name = name
                    row.totalv = parseFloat(row.totalv)
                    sumAmt += row.amt
                    sumTotal += row.totalv
                    tableS.push(row)
                })
                // logger4app.debug(tableD)
                json = { taxc, sumAmt, sumTotal, tableD: tableD, tableS: tableS }
            }else if (report == "19") {//BAO CAO CHI TIET
                const org = await ous.org(token)
                fn = "temp/rbkct.xlsx"
                where = "where idt between ? and ? and stax=? and  status>=3"
                binds = [fd, td, taxc]
                if (type !== "*") {
                    where += " and type=?"
                    binds.push(type)
                }
                if (ou && ou != "*") {
                    where += " and ou in (?)"
                    binds.push(ou.split(","))
                }
                sql = `select DATE_FORMAT(idt,'%d/%m/%Y') idt,form,serial,seq,btax,bname,bcode,baddr,buyer,exrt,sumv,vatv,totalv,c4,doc->'$.items' items from s_inv ${where} order by form,serial,seq` //,concat_ws('-',note,cde,adjdes) note
                result = await dbs.query(sql, binds)
                rows = result[0]
                let i = 0
                let table = []
                for (let row of rows) {
                    let items = row.items,  exrt = Number(row.exrt)
                    
                    delete row["items"]
                    for (let item of items) {
                        let vrt = item.vrt, dau = [CK].includes(item.type) ? -1 : 1
                        if (row.c4 == 'CKTM' && config.ent == "ghtk") {
                            dau = -1
                            row.sumv = row.sumv * dau
                            row.vatv = row.vatv * dau
                            row.totalv = row.totalv * dau
                        }
                        item.id = ++i
                        item.amount = Math.round(Number(item.amount)  * exrt * dau)
                        item.vat = Math.round(Number(item.vat)  * exrt * dau)
                        item.total = Math.round(Number(item.total)  * exrt * dau)
                        if (vrt == -1) item.vrn = KCT
                        else if (vrt == -2) item.vrn = KKK
                        else if (vrt == 0) item.vrn = "0%"
                        item = { ...row, ...item }
                        table.push(item)
                    }
                }
                json = { stax: taxc, sname: org.name, saddr: org.addr, fd: fr, td: to, rt: rt, tn: tn, table: table }
            }
            const file = path.join(__dirname, "..", "..", "..", fn)
            const xlsx = fs.readFileSync(file)
            const template = new xlsxtemp(xlsx)
            template.substitute(1, json)
            res.end(template.generate(), "binary")
        } catch (err) {
            next(err)
        }
    }
}


module.exports = Service;
