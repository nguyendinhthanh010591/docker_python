'use strict'
const mysql = require('mysql2/promise')
const config = require("../config")
const logger4app = require("../logger4app")
const decrypt = require('../encrypt')
let poolConfig = JSON.parse(JSON.stringify(config.poolConfig))
poolConfig.password = decrypt.decrypt(poolConfig.password)
const pool = mysql.createPool(poolConfig)
module.exports = pool