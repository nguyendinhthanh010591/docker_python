"use strict"
const SEC = require("../sec")
const dbs = require("./dbs")
const config = require("../config")
const logger4app = require("../logger4app")
const logging = require("../logging")
const FORMAT = {
    "atr": ["label", "value", "disabled", "hidden", "required"],
    "atrG": ["pid","hidden"],
    "feature": ["id", "action"],
    "featureG": ["id", "action"]
}
let chkSameK = (a,b) => {
    if (Object.keys(a).length == Object.keys(b).length) {
        for(let item in a) {
            if (!(item=="id" && (a[item] && b[item]))) {
                if(a[item] != b[item]) return false
            }
        }
        return true
    } else return false
}
const Service = {
    get: async (req, res, next) => {
        try {
            let sql, result
            sql = `select id id, fnc_name funcn, fnc_url funcu, fld_name fldn, fld_id fldid, fld_typ fldtyp, atr atr, feature feature, status status from s_f_fld`
            result = await dbs.query(sql, [])
            res.json(result[0])
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, operation = body.webix_operation
            const atr = body.atr ? JSON.parse(body.atr) : {}, feature = body.feature ? JSON.parse(body.feature) : {}
            const fldid = body.fldid, fldn = body.fldn, funcn = body.funcn, funcu = body.funcu, status = body.status, fldtyp = body.fldtyp
            let sql, result, binds, isOK = false
            let fatr, ffeature
            if (fldtyp == "datagrid") {
                fatr = "atrG"
                ffeature = "featureG"
            } else {
                fatr = "atr"
                ffeature = "feature"
            }
            let format = FORMAT[fatr]
            for (let a in atr) if (!format.includes(a)) delete atr[a]
            format = FORMAT[ffeature]
            for (let a in feature) if (!format.includes(a)) delete feature[a]
            if (operation == "insert") {
                sql = `insert into s_f_fld (fnc_name, fnc_url, fld_name, fld_id, fld_typ, atr, feature, status) values (?, ?, ?, ?, ?, ?, ?, ?)`
                binds = [funcn,funcu,fldn,fldid,fldtyp,JSON.stringify(atr),JSON.stringify(feature),status]
                result = await dbs.query(sql, binds)
            } else if (operation == "update") {
                //UPDATE
                const id = body.id
                sql = `update s_f_fld set fnc_name=?, fnc_url=?, fld_name=?, fld_id=?, fld_typ=?, atr=?, feature=?, status=? where id=? `
                binds = [funcn,funcu,fldn,fldid,fldtyp,JSON.stringify(atr),JSON.stringify(feature),status, id]
                result = await dbs.query(sql, binds)
            } else if (operation == "delete") {
                //DELETE
                const id = body.id
                sql = `delete from s_f_fld where id=? `
                binds = [id]
                result = await dbs.query(sql, binds)
            }
            if(result[0].affectedRows) isOK = true
            const sSysLogs = { fnc_id: 'field_post', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật thông tin cấu hình thuộc tính các trường trên chức năng ${body.funcn} (${operation})`, msg_id: fldid, doc: JSON.stringify(body) };
            logging.ins(req, sSysLogs, next)
            res.json(isOK)
        } catch (err) {
            next(err)
        }
    },
    getCusConf: async (req, res, next) => {
        try {
            const token = SEC.decode(req), query = req.query, url = query.url
            let params = JSON.parse(query.params)
            let sql, result, binds, rows
            sql = `select fld_id fldid, fld_typ fldtyp, atr atr, feature feature from s_f_fld where status=1 and fnc_url=?`
            binds = [url]
            result = await dbs.query(sql,binds)
            if (result[0].length > 0) {
                rows = result[0]
                for(let i=0; i<rows.length; i++) {
                    let item = rows[i]
                    item.atr = JSON.parse(item.atr)
                    item.feature = JSON.parse(item.feature)
                    if(!chkSameK(params, item.feature)) {
                        rows.splice(i,1)
                        --i
                    }
                    //else if (item.fldtyp == "datagrid") {}
                }
                res.json(rows)
            } else {
                res.json([])
            }
        } catch (err) {
            next(err)
        }
    }
}
module.exports = Service   