"use strict"
const config = require("../config")
const logger4app = require("../logger4app")
const dbs = require("./dbs")
const logging = require("../logging")
const util = require("../util")
const moment = require("moment")
const Service = {
    fbi: async (req, res, next) => {
        try {
            const sql = `select id "id",taxc "taxc",name "name",fadd "fadd",mail "mail",tel "tel",acc "acc",bank "bank",code "code" from s_org where id=?`
            const result = await dbs.query(sql, [req.params.id])
            res.json(result[0])
        }
        catch (err) {
            next(err)
        }
    },
    ous: async (req, res, next) => {
        try {
            const query = req.query
            let rows
            if (query.filter) {
                let name = `${query.filter.value}`
                if (name) {
                    const sql = `select id "id",name "name",taxc "taxc",addr "addr",mail "mail",tel "tel",acc "acc",bank "bank",prov "prov",dist "dist",ward "ward",fadd "fadd",code "code" from s_org where MATCH(taxc, code, name, name_en, fadd, tel, mail, acc, bank, c0, c1, c2, c3, c4, c5, c6) AGAINST (? IN NATURAL LANGUAGE MODE) ORDER BY id DESC LIMIT ${config.limit}`
                    const result = await dbs.query(sql, [name])
                    rows = result[0]
                }
            }
            res.json(rows)
        }
        catch (err) {
            next(err)
        }
    },
    fbn: async (req, res, next) => {
        try {
            const query = req.query
            let rows
            if (query.filter) {
                let name = `${query.filter.value}`
                if (name) {
                    const sql = `select taxc "taxc",name "name",fadd "fadd",mail "mail",tel "tel",acc "acc",bank "bank",code "code" from s_org where MATCH(taxc, code, name, name_en, fadd, tel, mail, acc, bank, c0, c1, c2, c3, c4, c5, c6) AGAINST (? IN NATURAL LANGUAGE MODE) ORDER BY id DESC LIMIT ${config.limit}`
                    const result = await dbs.query(sql, [name])
                    rows = result[0]
                }
            }
            res.json(rows)
        }
        catch (err) {
            next(err)
        }
    },
    get: async (req, res, next) => {
        try {
            const query = req.query
            let name = `${query.name}`, rows,start = query.start ? query.start : 0, count = query.count ? query.count : 10,where='',sql,result, sort = query.sort, order = ""
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = ` order by ${key} ${sort[key]}`
                })
            } else order = ` order by taxc `
            if (name) {
                where = ' MATCH(taxc, code, name, name_en, fadd, tel, mail, acc, bank, c0, c1, c2, c3, c4, c5, c6) AGAINST (? IN NATURAL LANGUAGE MODE)'
                 sql = `select id "id",code "code",taxc "taxc",name "name",name_en "name_en",addr "addr",prov "prov",dist "dist",ward "ward",mail "mail",tel "tel",acc "acc",bank "bank",fadd "fadd",c0 "c0",c1 "c1",c2 "c2",c3 "c3",c4 "c4",c5 "c5",c6 "c6",c7 "c7",c8 "c8",c9 "c9",type "type" from s_org where ${where} ${order} LIMIT ${count} OFFSET ${start}`
                 result = await dbs.query(sql, [name])
                rows = result[0]
            } else {
                 sql = `select id "id",code "code",taxc "taxc",name "name",name_en "name_en",addr "addr",prov "prov",dist "dist",ward "ward",mail "mail",tel "tel",acc "acc",bank "bank",fadd "fadd",c0 "c0",c1 "c1",c2 "c2",c3 "c3",c4 "c4",c5 "c5",c6 "c6",c7 "c7",c8 "c8",c9 "c9",type "type" from s_org ${order} LIMIT ${count} OFFSET ${start}`
                 result = await dbs.query(sql)
                rows = result[0]
            }
           
            let ret = { data: rows, pos: start }
            if (start == 0) {
                if (name){
                    sql = `select count(*) total from s_org where ${where}`
                    result = await dbs.query(sql, [name])
                }else  {
                    sql = `select count(*) total from s_org `
                    result = await dbs.query(sql)
                }
               
               
                ret.total_count = result[0][0].total
            }
           
            return res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const body = req.body, operation = body.webix_operation
            let binds, sql, result, sSysLogs = { fnc_id: '', src: config.SRC_LOGGING_DEFAULT, dtl: ``}
            const taxc = body.taxc ? body.taxc : null, code = body.code ? body.code : null
            if (operation == "update") {
                sql = `update s_org set code=?,taxc=?,name=?,name_en=?,prov=?,dist=?,ward=?,addr=?,mail=?,tel=?,acc=?,bank=?,c0=?,c1=?,c2=?,c3=?,c4=?,c5=?,c6=?,c7=?,c8=?,c9=? where id=?`
                binds = [code, taxc, body.name, body.name_en, body.prov, body.dist, body.ward, body.addr, body.mail, body.tel, body.acc, body.bank, body.c0, body.c1, body.c2, body.c3, body.c4, body.c5, body.c6, body.c7, body.c8, body.c9, body.id]
                sSysLogs = { fnc_id: 'org_upd', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật thông tin tổ chức id ${body.id}`, msg_id: body.id, doc: JSON.stringify(body)};
            }
            else if (operation == "insert") {
                sql = `insert into s_org set ?`
                binds = { name: body.name, name_en: body.name_en, taxc: taxc, code: code, prov: body.prov, dist: body.dist, ward: body.ward, addr: body.addr, tel: body.tel, mail: body.mail, acc: body.acc, bank: body.bank, c0: body.c0, c1: body.c1, c2: body.c2, c3: body.c3, c4: body.c4, c5: body.c5, c6: body.c6, c7: body.c7, c8: body.c8, c9: body.c9 }
                sSysLogs = { fnc_id: 'org_ins', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm mới thông tin tổ chức: ${JSON.stringify(body)}`, msg_id: body.id, doc: JSON.stringify(body)};
            }
            else if (operation == "delete") {
                sql = `delete from s_org where id=?`
                binds = [body.id]
                sSysLogs = { fnc_id: 'org_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa thông tin tổ chức id ${body.id}`, msg_id: body.id, doc: JSON.stringify(body)};
            }
            result = await dbs.query(sql, binds)
            logging.ins(req,sSysLogs,next) 
            const row = result[0]
            if (operation == "insert") res.json({ id: row.insertId })
            else res.json(row.affectedRows)
        }
        catch (err) {
            next(err)
        }
    },
    ins: async (req, res, next) => {
        try {
            const body = req.body
            const cols = ['id', 'name', 'name_en', 'taxc', 'code', 'addr', 'tel', 'mail', 'acc', 'bank', 'c0', 'c1', 'c2', 'c3', 'c4', 'c5', 'c6', 'c7', 'c8', 'c9']
            let arr = []
            const sql = `insert into s_org set ?`
            for (const row of body) {
                let id = row.id

                try {
                    for (const col of cols) {
                        if (!row.hasOwnProperty(col)) row[col] = null
                    }
                    // if (util.isEmpty(row.code)){
                    //     var nowMilliseconds = moment(new Date()).format("YYYYMMDDHHMMssSSS");
                    //     row.code='A'+nowMilliseconds;
                    // }
                    delete row["id"]
                    delete row["chk"]
                    delete row["error"]
                    await dbs.query(sql, row)
                    const sSysLogs = { fnc_id: 'org_ins', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm mới thông tin tổ chức: ${JSON.stringify(row)}`, msg_id: id, doc: JSON.stringify(row)};
                    logging.ins(req,sSysLogs,next) 
                    arr.push({ id: id, error: "OK" })
                } catch (err) {
                    //if (err.message.toLowerCase().includes("duplicate entry") || err.message.toLowerCase().includes("unique constraint")) err.message = "duplicate"
                    logger4app.debug(err)
                    if (err.errno == 1062) 
                    {
                        // neu ton tại thi update tức xóa đi insert lại
                         if (config.ent == 'ssi'|| config.ent == 'yusen'){
                            await dbs.query('delete from s_org where code=?', [row.code])
                            try{
                                await dbs.query(sql, row)
                                const sSysLogs = { fnc_id: 'org_ins', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm mới thông tin tổ chức: ${JSON.stringify(row)}`, msg_id: id, doc: JSON.stringify(row)};
                                logging.ins(req,sSysLogs,next) 
                                arr.push({ id: id, error: "OK" })
                            } catch (err) {
                                err.message = "duplicate"
                                arr.push({ id: id, error: err.message })
                            }
                           
                         }else{
                            err.message = "duplicate"
                            arr.push({ id: id, error: err.message })
                         }
                       
                     }

                   
                }
            }
            res.json(arr)
        }
        catch (err) {
            next(err)
        }
    },
    obbcode: async (code) => {
        try {
            let rows, ob
            const sql = `select id "id",code "code",taxc "taxc",name "name",name_en "name_en",addr "addr",prov "prov",dist "dist",ward "ward",mail "mail",tel "tel",acc "acc",bank "bank",fadd "fadd",c0 "c0",c1 "c1",c2 "c2",c3 "c3",c4 "c4",c5 "c5",c6 "c6",c7 "c7",c8 "c8",c9 "c9",type "type" from s_org where code = :code`
            const result = await dbs.query(sql, [code])
            rows = result.rows
            ob = (rows && rows.length > 0) ? rows[0] : null
            return ob
        }
        catch (err) {
            next(err)
        }
    }
}
module.exports = Service