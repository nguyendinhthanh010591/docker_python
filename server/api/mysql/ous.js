"use strict"
const util = require("../util")
const redis = require("../redis")
const sec = require("../sec")
const dbs = require("./dbs")
const config = require("../config")
const logger4app = require("../logger4app")
const logging = require("../logging")
const Service = {
    taxc: async (req, res, next) => {
        try {
            const sql = `select taxc "id",taxc "value" from s_ou where taxc is not null order by taxc`
            const result = await dbs.query(sql)
            res.json(result[0])
        }
        catch (err) {
            next(err)
        }
    },
    taxn: async (req, res, next) => {
        try {
            const sql = `select taxc "id",CONCAT_WS(' | ',taxc,name) "value" from s_ou where taxc is not null order by taxc`
            const result = await dbs.query(sql)
            res.json(result[0])
        }
        catch (err) {
            next(err)
        }
    },
    taxt: async (req, res, next) => {
        try {
            const sql = `select taxc "id",CONCAT(taxc,'-',name) "value" from s_ou where taxc is not null order by taxc`
            const result = await dbs.query(sql)
            res.json(result[0])
        }
        catch (err) {
            next(err)
        }
    },
    seq: async (req, res, next) => {
        try {
            const mst = req.params.mst
            const sql = `select count(*) "rc" from s_inv where stax=? and status=?`
            const result = await dbs.query(sql, [mst, 1])
            const row = result[0][0]
            res.json(row)
        }
        catch (err) {
            next(err)
        }
    },
    level3: async (req, res, next) => {
        try {
            const sql = `select id "id",name "value" from s_ou where level<=3 start with pid is null connect by PRIOR id=pid`//status=1 and
            const result = await dbs.query(sql)
            res.json(result[0])
        }
        catch (err) {
            next(err)
        }
    },
    get: async (req, res, next) => {
        try {
            const sql = `select b.id id,b.taxc taxc,b.mst mst,b.status status,b.code code,b.name name,b.pid pid,b.addr addr,b.fadd fadd,b.prov prov,b.dist dist,b.ward ward,b.mail mail,b.tel tel,b.acc acc,b.bank bank,b.paxo paxo,b.taxo taxo,b.sign sign,b.seq seq,a.depth level, b.temp temp,b.receivermail receivermail,b.dsignissuer dsignissuer,b.dsignto dsignto,b.dsignfrom dsignfrom,b.dsignsubject dsignsubject,b.dsignserial dsignserial from s_ou_tree a join s_ou b on b.id = a.cid where a.pid=1 order by a.path`
            const result = await dbs.query(sql)
            const rows = result[0]
            let arr = [], obj
            for (const row of rows) {
                if (!row.pid) arr.push(row)
                else {
                    obj = util.findDFS(arr, row.pid)
                    if (obj) {
                        if (!obj.hasOwnProperty('data')) obj.data = []
                        obj.data.push(row)
                    }
                }
            }
            res.json(arr)
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const body = req.body, operation = body.webix_operation
            let binds, sql, result, taxc = body.taxc ? body.taxc : null, sSysLogs = { fnc_id: '', src: config.SRC_LOGGING_DEFAULT, dtl: ``}
            if (taxc) await redis.del(`OUS.${taxc}`)
            switch (operation) {
                case "update":
                    const seq = body.seq ? body.seq : 1, sign = body.sign ? body.sign : 1, pwd = body.pwd
                    body.code = body.code ? body.code : null
                    sql = "update s_ou set name=?,addr=?,taxc=?,prov=?,dist=?,ward=?,mail=?,tel=?,acc=?,bank=?,paxo=?,taxo=?,seq=?,sign=?,usr=?,mst=?,status=?,code=?,temp=?,receivermail=?,dsignissuer=?,dsignto=?,dsignfrom=?,dsignsubject=?,dsignserial=?"
                    binds = [body.name, body.addr, taxc, body.prov, body.dist, body.ward, body.mail, body.tel, body.acc, body.bank, body.paxo, body.taxo, seq, sign, body.usr, body.mst, body.status, body.code, body.temp, body.receivermail, body.dsignissuer, body.dsignto, body.dsignfrom,body.dsignsubject, body.dsignserial]
                    if (pwd) {
                        sql += ",pwd=?"
                        binds.push(util.encrypt(pwd))
                    }
                    binds.push(body.id)
                    sql += " where id=?"
                    sSysLogs = { fnc_id: 'ou_upd', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật thông tin đơn vị id ${body.id}`, msg_id: body.id, doc: JSON.stringify(body)};
                    break
                case "insert":
                    sql = "insert into s_ou (name,pid,mst,code,temp) values (?,?,?,?,?)"
                    binds = [body.name, body.parent, body.mst, body.code, body.temp]
                    sSysLogs = { fnc_id: 'ou_ins', src: config.SRC_LOGGING_DEFAULT, dtl: `Thêm mới thông tin đơn vị: ${JSON.stringify(body)}`, msg_id: body.id, doc: JSON.stringify(body)};
                    break
                case "delete":
                    sql = "delete from s_ou where id=?"
                    binds = [body.id]
                    sSysLogs = { fnc_id: 'ou_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa thông tin đơn vị id ${body.id}`, msg_id: body.id, doc: JSON.stringify(body)};
                    break
                default:
                    throw new Error(`${operation} là hoạt động không hợp lệ \n (${operation} is invalid operation)`)
            }
            result = await dbs.query(sql, binds)
            logging.ins(req,sSysLogs,next) 
            if (operation == "insert") res.json({ id: result[0].insertId })
            else res.json(result[0].affectedRows)
        }
        catch (err) {
            next(err)
        }
    },
    bytaxc: async (req, res, next) => {
        try {
            const taxc = req.params.taxc
            //const sql = `select id "id",lpad('_',3*(level - 1),'_')||name "value" from s_ou where taxc=? start with pid is null connect by prior id=pid`
            const sql = `select id "id",name "value" from s_ou where mst=? start with pid is null connect by prior id=pid`//and status=1
            const result = await dbs.query(sql, [taxc])
            res.json(result[0])
        }
        catch (err) {
            next(err)
        }
    },
    byid: async (req, res, next) => {
        try {
            const id = req.params.id
            const sql = `select b.id "id",b.name "value" from s_ou_tree a join s_ou b on b.id = a.cid where a.pid=? order by a.path`// where status=1
            const result = await dbs.query(sql, [id])
            res.json(result[0])
        }
        catch (err) {
            next(err)
        }
    },
    bytoken: async (req, res, next) => {
        try {
            const token = sec.decode(req)
            // const sql = `select id "id",name "value" from s_ou start with id=? connect by prior id=pid`// where status=1
            const sql = `select b.id "id",mst "mst",code "code", b.name "value" from s_ou_tree a join s_ou b on b.id = a.cid where a.pid=? order by a.path`// where status=1
            const result = await dbs.query(sql, [token.ou])
            res.json(result[0])
        }
        catch (err) {
            next(err)
        }
    },
    bytokenou: async (req, res, next) => {
        try {
            const token = sec.decode(req)
            // const sql = `select id "id",name "value" from s_ou start with id=? connect by prior id=pid`// where status=1
            const sql = `select b.id "id",mst "mst",code "code", b.name "value" from s_ou_tree a join s_ou b on b.id = a.cid where a.pid=? and b.mst in (select d.taxc from s_manager c,s_ou d where c.taxc=d.taxc and c.user_id=? ) order by a.path`// where status=1
            const result = await dbs.query(sql, [token.ou,token.uid])
            res.json(result[0])
        }
        catch (err) {
            next(err)
        }
    },
    obt: async (taxc) => {
        let row
        const sql = `select so.id "id",so.taxc "taxc",so.paxo "paxo",st2.name "paxoname",so.taxo "taxo",st.name "taxoname",so.name "name",so.fadd "addr",so.mail "mail",so.tel "tel",so.acc "acc",so.bank "bank",so.seq "seq",so.sign "sign",so.usr "usr",so.pwd "pwd",so.sc "sc",so.sct "sct",so.bcc "bcc",so.qrc "qrc",so.na "na",so.np "np",so.nq "nq",so.itype "itype",so.ts "ts",so.degree_config "degree_config" from s_ou so left join s_taxo st on st.id=so.taxo left join s_taxo st2 on st2.id=so.paxo where taxc=?`
        const result = await dbs.query(sql, [taxc])
        row = result[0][0]
        return row
    },
    gettosign: async (taxc) => {
        const sql = `select id "id",taxc "taxc",taxo "taxo",name "name",fadd "addr",mail "mail",tel "tel",acc "acc",bank "bank",seq "seq",sign "sign",usr "usr",pwd "pwd",temp "temp" from s_ou where taxc=?`
        const result = await dbs.query(sql, [taxc])
        let row = result[0][0]
        return row
    },
    org: async (token) => {
        return await Service.obt(token.taxc)
    },
    obid: async (id) => {
        const sql = `select so.id "id",so.taxc "taxc",so.paxo "paxo",st2.name "paxoname",so.taxo "taxo",st.name "taxoname",so.name "name",so.fadd "addr",so.mail "mail",so.tel "tel",so.acc "acc",so.bank "bank",so.seq "seq",so.sign "sign",so.usr "usr",so.pwd "pwd",so.sc "sc",so.sct "sct",so.bcc "bcc",so.qrc "qrc",so.na "na",so.np "np",so.nq "nq",so.itype "itype",so.ts "ts",so.degree_config "degree_config" from s_ou so left join s_taxo st on st.id=so.taxo left join s_taxo st2 on st2.id=so.paxo where so.id=?`
        const result = await dbs.query(sql, [id])
        let row = result[0][0]
        return row
    },
    oball: async () => {
        const sql = `select id "id",taxc "taxc",taxo "taxo",name "name",fadd "addr",mail "mail",tel "tel",acc "acc",bank "bank",seq "seq",sign "sign",usr "usr",pwd "pwd",temp "temp" from s_ou`
        const result = await dbs.query(sql, [])
        let rows = result[0]
        return rows
    }
}
module.exports = Service