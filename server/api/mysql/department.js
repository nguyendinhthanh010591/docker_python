"use strict"
const config = require("../config")
const logger4app = require("../logger4app")
const util = require("../util")
const dbs = require("./dbs")
const { bind } = require("file-loader")

const Service= {
    get: async(req,res,next)=>{
        try{
            let binds=[], where="where 1=1", order
            const query= req.query, sort=query.sort
            if(query.filter){
                let filter = JSON.parse(query.filter), val, i = 1
                Object.keys(filter).forEach(key => {
                    val = filter[key]
                    if(val){
                        where += ` and upper(${key}) like @${i++}`
                        binds.push(`%${val.toUpperCase()}%`)
                    }
                }
                )
            }
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = ` order by ${key} ${sort[key]}`
                })
            } else order = ` order by deptid`
            const sql=`select deptid "deptid",deptname "deptname" from s_dept ${where} ${order}`
            const result= await dbs.query(sql,binds)
            res.json(result.recordset)
        }
        catch(err){
            next(err)
        }
    },
    post: async(req,res,next)=>{
        try{
            let body=req.body,binds, result, operation = body.webix_operation,sql
            switch(operation){
                case "insert":
                    sql="insert into s_dept (deptname,deptid) values (@1,@2)"
                    binds=[body.deptname,body.deptid]
                    break
                case "delete":
                    sql="delete from s_dept where deptid=@1"
                    binds=[body.deptid]
                    break
                case "update":
                    sql="update s_dept set deptname=@1 where deptid=@2"
                    binds=[body.deptname,body.deptid]
                    break
                default:
                    throw new Error(`${operation} là không hợp lệ \n (${operation} is invalid)`)
            }
            result = await dbs.query(sql, binds)
            if (operation == "insert") res.json(result.recordset)
            else res.json(result.rowsAffected[0])
        }
        catch(err){
            next(err)
        }
    }
}


module.exports=Service