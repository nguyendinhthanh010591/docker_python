"use strict"
const moment = require("moment")
const dbs = require("./dbs")
const config = require("../config")
const logger4app = require("../logger4app")
const logging = require("../logging")
const Service = {
    get: async (req, res, next) => {
        try {
            const query = req.query, filter = JSON.parse(query.filter)
            const sort = query.sort, start = query.start ? query.start : 0, count = query.count ? query.count : 10
            let where = ` where dt between ? and ?`, order, sql, result, ret
            let binds = [new Date(filter.fd), new Date(moment(filter.td).endOf("day"))]
            Object.keys(filter).forEach(key => {
                if (key == "curr") {
                    where += ` and cur=?`
                    binds.push(filter[key])
                }
            })
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = ` order by ${key} ${sort[key]}`
                })
            }
            else order = " order by dt desc"
            sql = `select id "id",cur "cur",val "val",dt "dt" from s_ex ${where} ${order} LIMIT ${count} offset ${start}`
            result = await dbs.query(sql, binds)
            ret = { data: result[0], pos: start }
            if (start == 0) {
                sql = `select count(*) "total" from s_ex ${where}`
                result = await dbs.query(sql, binds)
                ret.total_count = result[0][0].total
            }
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const body = req.body, operation = body.webix_operation
            let binds, sql
            switch (operation) {
                case "update":
                    sql = `update s_ex set val=? where id=?`
                    binds = [body.val, body.id]
                    break
                case "insert":
                    sql = `insert into s_ex set ?`
                    binds = { cur: body.cur, val: body.val, dt: new Date(body.dt) }
                    break
                case "delete":
                    sql = `delete from s_ex where id=?`
                    binds = [body.id]
                    break
                default:
                    throw new Error(`${operation} là hoạt động không hợp lệ \n (${operation} is invalid operation)`)
            }
            const result = await dbs.query(sql, binds)
            const sSysLogs = { fnc_id: 'exch_post', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật thông tin tỷ giá ngoại tệ ${body.cur} (${operation})`, msg_id: body.id, doc: JSON.stringify(body) };
            logging.ins(req, sSysLogs, next)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    rate: async (req, res, next) => {
        try {
            const query = req.query, cur = query.cur, dt = query.dt
            const sql = `select val "val" from s_ex where cur=? and dt=(select max(dt) from s_ex where cur=? and dt<=?)`
            const result = await dbs.query(sql, [cur, cur, new Date(dt)])
            const rows = result[0]
            const val = rows.length ? rows[0].val : 0
            res.json(val)
        }
        catch (err) {
            next(err)
        }
    }
}
module.exports = Service   