"use strict"
const express = require("express")
const multer = require("multer")
const sign = require("./sign")
const ads = require("./ads")
const sec = require("./sec")
const config = require("./config")
const logger4app = require("./logger4app")
const editor = require("./editor")
const email = require("./email")
const sms = require("./sms")
const util = require("./util")
const inc = require("./inc")
const redis = require("./redis")
const ds = require("./ds")
const wno = require("./wno")
const bth = require("./bth")
const dbtype = config.dbtype
const cat = require(`./cat`)
const col = require(`./col`)
const home = require(`./home`)
const sea = require(`./sea`)
const user = require(`./user`)
const groups = require(`./groups`)
const field = require(`./field`)
const inv = require(`./inv`)
const tran = require(`./${dbtype}/tran`)
const itran = require(`./${dbtype}/itran`)
const serial = require(`./serial`)
const syslog = require(`./syslog`)
const report = require(`./${dbtype}/report`)
const org = require(`./org`)
const ous = require(`./ous`)
const repcom = require(`./${dbtype}/repcom`)
const loc = require(`./loc`)
const gns = require(`./gns`)
const exch = require(`./exch`)
const minutes = require(`./minutes`)
const tvan = require("./tvan")
const xls = require(`./xls`)
const syn = require(`./${dbtype}/syn`)
const ven = require(`./${dbtype}/vendor`)
const seg = require(`./${dbtype}/segment`)
const dep = require(`./${dbtype}/department`)
const passport = require('passport')
const bin = require('./bin')
const statement = require('./statement')
const cbconn = require('./cbconn')
const entry = require(`./${dbtype}/entry`)
const moment = require("moment")
// const reportHDB = require(`./${dbtype}/reportHDB`)
const MINUTES_MIMES = ["msword", "vnd.openxmlformats-officedocument.wordprocessingml.document", "pdf"]

const XLS_MIMES = ["vnd.ms-excel", "vnd.openxmlformats-officedocument.spreadsheetml.sheet"]
const filterMEMO = (req, file, cb) => {
  const extension = file.mimetype.split("/")[1]
  if (!XLS_MIMES.includes(extension)) return cb(new Error("Chỉ cho tải lên các file có định dạng xls, xlsx \n (Only upload files in xls, xlsx format)"), false)
  cb(null, true)
}
const filterCA = (req, file, cb) => {
  if (file.mimetype !== "application/x-pkcs12") return cb(new Error("Chỉ cho tải lên file định dạng pkcs12 \n (Only upload files in pkcs12 format)"), false)
  cb(null, true)
}
const filterXML = (req, file, cb) => {
  if (file.mimetype !== "text/xml") return cb(new Error("Chỉ cho tải lên file định dạng xml \n (Only upload files in xml format)"), false)
  cb(null, true)
}
const filterMINUTES = (req, file, cb) => {
  if (config.ent != 'vib') {
    const extension = file.mimetype.split("/")[1]
    if (!MINUTES_MIMES.includes(extension)) return cb(new Error("Chỉ cho tải lên các file có định dạng doc, docx, pdf \n (Only upload files in doc, docx, pdf format)"), false)
  }

  cb(null, true)
}
const PDF_FILTER = (req, file, cb) => {
  if (file.mimetype !== "application/pdf") return cb(new Error("Chỉ nhận file PDF"), false)
  cb(null, true)
}
const uploadCA = multer({ storage: multer.memoryStorage(), fileFilter: filterCA, limits: { fileSize: 8192 } }).single("upload")
const uploadMEMO = multer({ storage: multer.memoryStorage(), fileFilter: filterMEMO, limits: { fileSize: 16777216 } }).single("upload")
const uploadXML = multer({ storage: multer.memoryStorage(), fileFilter: filterXML, limits: { fileSize: 1048576 } }).single("upload")
const uploadMINUTES = multer({ storage: multer.memoryStorage(), fileFilter: filterMINUTES, limits: { fileSize: 16777216 } }).single("upload")
const uploadPDF = multer({ storage: multer.memoryStorage(), fileFilter: PDF_FILTER, limits: { fileSize: 16777216 } }).single("file")
const jwt = require("jsonwebtoken")
const router = express.Router()

router.get("/home", home.get)
router.post("/tok", sec.tok)
router.get("/sysdate", (req, res) => {
  logger4app.debug(moment(new Date()).format('YYYY-MM-DD'))
  res.json(moment(new Date()).format('YYYY-MM-DD'))
})
//if (!config.user_adfs) {
router.post("/signin", sec.signin)
//}
if (config.user_adfs) {

  router.get("/signinsaml", (req, res, next) => {
    passport.authenticate(config.passportsaml.strategy, { successRedirect: '/login', failureRedirect: '/signinsaml' }, (err, profile) => {
      // control will not come here ????   
    })(req, res, next);

  });

  router.post('/signinsaml/callback', sec.signinadfs)
  if(config.ent=="hsy") {
    router.get(
      '/logout', 
      function(req, res) {
        passport._strategy('saml').logout(req, function(err, requestUrl) {
          req.logout();
          res.redirect('/');
        }
      );
    })
  }
} else if(config.useAzureAD) {
  router.get(`/signin-azure-ad`, sec.signinAzureAD, (req, res) => {
    console.log("Signin azure ad ", JSON.stringify(req.user));
    res.redirect("/");
  });
  router.post(`/signin-azure-ad-success`, 
    sec.signinAzureADSuccess, 
    (req, res, next) => {
      let passportInstance = req.session.passport;
      return req.session.regenerate(function (err){
        if (err) {
          return next(err);
        }
        req.session.passport = passportInstance;
        return req.session.save(next);
      });
    },
    (req, res) => {
      console.log("Signin azure ad success ", JSON.stringify(req.user));
      let logintoken = jwt.sign({ username: (req.user?req._json:req.preferred_username || "") }, config.secret);
      res.redirect(`/#!/login?logintoken=${logintoken}`);
    });
  // router.get(`/signin-azure-ad-failed`, sec.signinAzureADFailed, (req, res) => {
  //   console.log("Signin azure ad failed ", JSON.stringify(req.user));
  //   res.redirect("/");
  // });
  router.get(`/logout`, sec.logoutAzureAD);
}

router.get("/select", col.dcd)
router.post("/reset", ads.reset)
router.post("/pwd", ads.pwd)
//ads
router.post("/ads/add1", ads.add0)
router.post("/ads/add2", ads.add2)
router.post("/ads/add", ads.add1)
router.post("/ads/mail", ads.mail)
//cat
router.get("/cat/refresh", inc.refresh)
router.get("/cat/seq/:key", inc.seq)
router.get("/cat/kache/:type", inc.kache)
router.get("/cat/kacheSegment/:type", inc.segment)
router.get("/cat/serial/:type", inc.getserial)
router.route("/cat/serialbyform").get(inc.getserial)
router.get("/cat/nokache/:type", cat.nokache)

router.get("/cat/kache2/:type/:id", cat.kache)
router.get("/cat/bytoken", ous.bytoken)
router.get("/kat/:type", cat.get)
router.post("/pkat", cat.post)//chi chức năng danh mục mới dc post
//1-hethong
router.get("/cus/:itype", col.cus)
router.post("/cus", col.cusLang)
router.post("/dts", col.dtsLang)
router.get("/dts/:itype", col.dts)
//col
router.get("/col", col.get)
router.post("/col", col.post)
//dtl
router.get("/dtl", col.getdtl)
router.post("/dtl", col.postdtl)
// bin
router.get("/bin/get", bin.get)
router.get("/bin/xls", bin.xls)
router.get("/bin/rbi/:id", bin.rbi)
router.get("/bin/view/:id", bin.view)
router.get("/bin/del/:id", bin.del)
router.post("/bin/post", bin.post)
router.put("/bin/put/:id", bin.put)
router.post("/info/upload", uploadXML, bin.xview2)
router.post("/bin/upload/xml", uploadXML, bin.upload)
if (dbtype == "mssql") {
  router.post("/bin/detailS", bin.detailS)
  router.post("/bin/upload", uploadXML, bin.upload)
}
//xls
router.get("/xls", xls.get)
router.post("/xls", xls.post)
//edit
router.route("/editor").get(editor.get).post(editor.post)
router.route("/tmp").post(editor.tmp)
router.route("/ous/tmp").post(editor.tmpApi)
router.get("/editor/htm", editor.htm)
router.get("/editor/app", editor.app)
//ema
router.route("/email").get(email.get).post(email.post)
//org
router.route("/org").get(org.get).post(org.post)
router.get("/org/fbi/:id", org.fbi)
router.get("/org/fbn", org.fbn)
router.get("/org/ous", org.ous)
router.post("/org/ins", org.ins)
router.post("/org/upload", uploadMEMO, xls.upload)
router.get("/org/afbt", serial.getAllFormByType)
router.post("/org/getcif", cbconn.getcif)

//user
router.route("/user").get(user.get).post(user.post)
router.get("/user/role/:uid", user.role)
router.post("/user/member", user.member)
router.get("/user/ou/:uid", user.ou)
router.post("/user/manager", user.manager)
router.get("/user/ubr/:rid", user.ubr)
router.post("/user/mbr", user.mbr)
router.get("/user/disable/:uid", user.disable)
router.get("/user/enable/:uid", user.enable)
if (config.ent == 'scb') {
  router.get("/user/xls", user.xls)
  router.get("/inv/dep", inv.depINV)
  router.get("/segment", seg.segment)
  router.get("/user/roledata/:uid", user.roledata)
  router.post("/user/system", user.system)

}
if (config.ent == "ctbc") router.get("/user/soc", user.soc)
//group
router.route("/groups").get(groups.get).post(groups.update)
router.post("/groups/member", groups.member)
router.post("/groups/member1", groups.member1)
router.post("/groups/add", groups.add)
router.post("/groups/update", groups.update)
router.get("/groups/role/:uid", groups.role)
router.get("/groups/grant/:uid", groups.grant)
router.get("/groups/roleAll/:uid", groups.roleAll)
router.get("/groups/gir/:rid", groups.gir)
router.post("/groups/mbr", groups.mbr)
router.get("/groups/disable/:uid", groups.disable)
router.get("/groups/enable/:uid", groups.enable)
if (config.ent == 'scb') {
  router.get("/groups/xls", groups.xls)
}
if (config.ent == "ctbc") router.get("/groups/soc", groups.soc)
//field
router.route("/fld").get(field.get)
router.route("/pfld").post(field.post)//chi có role mới dc post
router.route("/fld/conf").get(field.getCusConf)

//ou
router.post("/ous/smtp", email.smtp)
router.route("/ous").get(ous.get)
router.route("/pous").post(ous.post)
router.get("/ous/checkca", sign.checkca)
router.post("/ous/vca", sign.infob64)
router.post("/ous/vca/upfile", sign.upb64)
router.get("/ous/taxc", ous.taxc)
router.get("/ous/taxn", ous.taxn)
router.get("/ous/taxt", ous.taxt)
router.get("/ous/seq/:mst", ous.seq)
router.get("/ous/bytaxc/:taxc", ous.bytaxc)
router.get("/ous/byid/:id", ous.byid)
router.get("/ous/bytoken", ous.bytoken)
if (dbtype == "pgsql" || dbtype == "orcl") {
  router.get("/ous/getou", ous.getou)
  // if (dbtype == "pgsql") {
  //   router.get("/ous/getbranch", ous.getbranch)
  // }
}
router.get("/ous/bytokenou", ous.bytokenou)
if (config.ent == "sgr") router.get("/ous/byousgr", ous.byousgr)
if (config.ent == "baca") router.get("/ous/bytokenouba", ous.bytokenoubaca)
router.post("/ous/upload", uploadCA, sign.uploadCA)
router.post("/ous/signdata", inv.signdoc)
router.post("/ous/signxdata", inv.signxml)
router.post("/ous/apiSendToVan", inv.apiSendToVan)
router.post("/ous/apiSendBackToVan", inv.SendbackToVan)
router.post("/ous/sendmaildoc", inv.sendmaildoc)
router.post("/ous/apiSendMailCoMa", inv.sendMailCoMa)
//2-danhmuc
//ex
router.route("/exch").get(exch.get).post(exch.post)
router.get("/exch/rate", exch.rate)
//lo
router.get("/loc/:pid", loc.get)
router.post("/loc", loc.post)
//gn
router.route("/gns").get(gns.get).post(gns.post)
router.get("/gns/fbn", gns.fbn)
router.get("/gns/fbw", gns.fbw)
router.post("/gns/upload", uploadMEMO, xls.upload)
router.post("/gns/ins", gns.ins)
if (config.ent == "vib") {
  router.get("/gns/xls", gns.xls)
}
//ven
router.route("/ven").get(ven.get).post(ven.post)

//segment
if (dbtype == "mssql") {
  router.route("/seg").get(seg.get).post(seg.post)
  router.route("/dep").get(dep.get).post(dep.post)
  router.route("/rol").get(dep.roledep).post(dep.mbr)
  router.get("/rol/:rid", dep.gir)
}
router.post("/inv/dellAll", inv.delAll)

//ent
router.post("/ent", entry.post)
if (config.ent == "vib") {
  router.post("/ent/check", entry.checkTrung)
  router.post("/ent/getID", entry.getID)
}
router.post("/ent/cancel", cbconn.getcel)
router.post("/ent/appcal", cbconn.getcal)
if (config.ent == "vib") router.post("/ent/cancelinv", inv.vib_cancel)

//3-phathanh
//serial
router.route("/serial/seou").get(serial.getseou).post(serial.postseou)
router.get("/serial/seou/:mst/:se", serial.getsexou)
router.route("/serial/seusr").get(serial.getseusr).post(serial.postseusr)
router.get("/serial/seusr/:usid", serial.getsexusr)
router.route("/serial").get(serial.get).post(serial.post)
router.get("/serial/docx/:type/:taxc", serial.docx)
router.get("/serial/sync/:taxc", serial.sync)
router.get("/serial/fbt", serial.getFormByType)
router.get("/serial/sbf", serial.getSerialByForm)
router.get("/serial/afbt", serial.getAllFormByType)
router.get("/serial/asbf", serial.getAllSerialByForm)
router.get("/serial/asbf1wl", serial.getAllSerialByForm1WithListinv)
router.get("/serial/asbf1nl", serial.getAllSerialByForm1NoListinv)
router.get("/serial/alls", serial.getAllSerial)
router.get("/serial/taxt", ous.taxt)
router.get("/serial/taxn", ous.taxn)
router.get("/serial/xml/all/:taxc", serial.xmlall)
router.post("/serial/syncredisdb", serial.syncredisdb)
router.post("/serial/apiseq", serial.apiseq)
router.get("/serial/tbt", serial.getAllTypeByTax)
router.get("/serial/sendtype", serial.getSendType)



if (config.ent == "vib") router.get("/serial/exrep", serial.exreport)
//11-duyettbph
router.get("/sap/:id", serial.approve)
//12-huytbph
router.get("/sca/:id", serial.cancel)
router.post("/sap/mul", serial.mulapprove)
router.post("/sca/mul", serial.mulcancel)
router.post("/serial/extend", serial.extend)


//4-lậpHĐ
//inv_etl
if (config.ent == "vib") {
  router.route("/etl").get(inv.getetl).post(inv.etlpost)
}
//inv
router.route("/inv/:id").get(inc.load).put(inv.put)
router.post("/inv", inv.post)
router.post("/inv/pdf/sign", inc.pdf)
router.get("/inv/pdf/conf", inc.conf)
router.get("/inv/mail/:id", inc.mail)
router.post("/inv/mail/send", email.send)
router.get("/inv/sms/:id", inc.sms)
router.post("/inv/sms/send", sms.send)
router.get("/inv/htm/:id", inc.htm)
router.post("/inv/view", inc.view)
router.get("/inv/xml/:id", inc.xml)
router.get("/inv/xml/stm/:id", inc.xml_stm)
router.get("/inv/cvt/:id", inc.cvt)

router.post("/inv/go/24", inv.go24)
router.get("/inv/count/12", inv.count12)
router.get("/inv/hstvan/:id", inv.hstvan)
router.get("/inv/del/:id/:pid", inv.del)
router.get("/inv/status/:id/:status", inv.status)
router.post("/inv/xls/ins", inv.ins)
if (config.ent == "sgr") {
  router.post("/inv/cancelSGR", inv.cancelSGR)


}
if (dbtype == "mssql") {

  router.post("/inv/uplosg", uploadMEMO, xls.uplosg)

}
router.post("/inv/seq/del", inv.deleteinv)
router.post("/inv/xls/wcan", inv.wcan)
router.post("/inv/xls/upds", inv.upds)
router.post("/inv/upload", uploadMEMO, xls.upload)

router.post("/inv/uplupd", uploadMEMO, xls.uplupd)
router.post("/inv/invs/del", inv.deleteinv)
router.post("/inv/invs/mails", inv.mailinvs)
router.post("/inv/syncredisdb", inv.syncRedisDb)
router.post("/inv/gettrans", cbconn.gettrans)
router.post("/inv/resetbth", inv.resetBTH)
router.get("/inv/viewXml/:id", inv.hscqtXml)
if (config.ent == "hlv") {
  router.post("/inv/invs/del_gd", inv.deleteinv_gd)
}
if (dbtype == "orcl") {
  router.get("/inv/getID/:id", inv.getIDFile)
  router.post("/inv/delFile", inv.delFile)

  router.post("/inv/changeD", inv.changeDate)
  router.post("/inv/changeS", inv.changeStatus)

}
router.post("/ous/getTemp", ous.getTemp)

if (dbtype == "mssql") {
  router.post("/inv/invs/dds", inv.ddsSent)
  router.post("/inv/invs/ddsStt", inv.ddsStt)
  router.post("/inv/invs/rpt", inv.synReport)
  router.post("/inv/invs/detailS", inv.detailS)
  router.get("/tran/user/getsystem", user.getsystem)
}
//sync
if (dbtype == "mysql") {
  router.post("/invoice-search", inv.api_search)
  router.post("/invoice-download", inv.api_download)
  router.route("/syn").get(syn.get).post(syn.get)
  router.get("/syn/xls", syn.xls)
  router.post("/syn/ins_another", syn.ins)
  router.post("/syn/sync", syn.sync)
  router.get("/syn/xlss", syn.xlss)
  router.get("/syn", syn.get)
}
//minutes
router.post("/inv/minutes/replace", minutes.minutesReplace)
router.post("/inv/minutes/cancel", minutes.minutesCancel)
router.post("/inv/minutes/create", minutes.minutesCreate)
router.get("/inv/minutes/custom/:id", minutes.minutesCustom)
router.get("/inv/minutes/listinvadj/:id", minutes.minutesListInvAdj)
router.get("/inv/minutes/wnadj/:id", minutes.minutesWnAdj)
router.post("/inv/minutes/upload", uploadMINUTES, minutes.upload)
//router.post("/inv/minutes/uploadvib", uploadMINUTES, inv.upload)
//5-duyetHD
//router.post("/app/cloudtvan", tvan.cloudTvanApi)
router.route("/app").get(inv.appget).post(inv.apprpost).put(inv.apprput)
if (dbtype == "orcl") {
  router.route("/appou").post(inv.apprpostou).get(inv.appgetou)
}
router.get("/app/htm/:id", inc.htm)
router.get("/app/all", inv.apprall)
router.route("/seq").get(inv.seqget).post(inv.seqpost)
if (dbtype == "mssql") {
  router.route("/appseq").get(inv.seqget).post(inv.apprseqpost)
}
router.post("/dss/pdf/sign/upload", uploadPDF, ds.signpdf)
router.post("/dss/pdf/verify/upload", uploadPDF, ds.verifypdf)
router.get("/dss/conf", ds.conf)

router.post("/sav", inv.sav)
router.route("/sign").get(inv.signget).put(inv.signput)
router.get("/seq/fbt", serial.getFormByTypeInv)
router.get("/seq/sbf", serial.getSerialByFormInv)
router.get("/seq/asbf", serial.getAllSerialByFormInv)
router.get("/seq/afbt", serial.getAllFormByTypeInv)
router.get("/seq/asbfin123", serial.getAllSerialByFormInvIn123)
//6-report
router.get("/rpt", report.get)
router.post("/rpt", report.get)
router.get("/rpt/bytoken", ous.bytoken)
if (config.ent == "scb") {
  router.get("/com", repcom.getsys)
  router.get("/com/rpt", repcom.get)
}
//13-reportHDB
// router.get("/rptHDB", reportHDB.get)
// router.get("/rptHDB/getou", reportHDB.getou)
// router.get("/rptHDB/gettax", reportHDB.gettax)
//reportBVB
if (config.ent == "bvb") {
  router.get("/sea/fbtrp", serial.getFormByTypeReport)
  router.get("/sea/sbfrp", serial.getSerialByFormReport)
}
//7-tracuu
router.get("/sea/sbf", serial.getSerialByFormInv)
router.get("/sea/fbt", serial.getFormByTypeInv)
router.get("/sea/fbt/editor", serial.getFormByTypeInvEditor)
router.get("/sea/sbf/editor", serial.getSerialByFormEditor)
router.get("/sea", inv.get)
router.get("/sea/xls", inv.xls)
router.get("/sea/pdf", inv.print)
router.get("/sea/pdfmerge", inv.printmerge)
if (config.ent == 'sgr') {
  router.get("/sea/print", inv.printall)
}
router.get("/sea/rel/:id", inv.relate)
router.get("/sea/relwno/:id", inv.relatewno)
router.get("/sea/rels/:id", inv.relateadjs)
router.get("/sea/fbw", gns.fbw)
router.get("/sea/afbt", serial.getAllFormByTypeInv)
router.get("/sea/asbf", serial.getAllSerialByFormInv)
router.get("/sea/asbf/adj", serial.getAllSerialManualByFormInv)
router.get("/sea/htm/:id", inc.htm)
router.get("/sea/htmn", inc.htmn)
if (dbtype == "mssql") {
  router.post("/iwcall", inv.trashAll)
  router.get("/tran/detail/:id", inv.detailTran)
}
//7.1- tra cuu giao dich
if (dbtype == "orcl") {
  router.get("/tran/tran_ol", tran.getol)
  router.post("/tran/sav_ol", tran.sav_ol)
  router.post("/tran/createiv", tran.getinv)
  router.get("/tran/getTMP", tran.getTemp)
}
if (dbtype == "mssql") {
  router.get("/tran", tran.get)
  router.post("/tran/dtl", tran.del)
  router.post("/tran/syn", tran.syn)
  router.post("/tran/edit", tran.editPost)
  router.post("/tran/col", tran.col)

  router.post("/tran/app", tran.app)
  router.get("/tran/exp", tran.exportD)
  router.post("/tran/rep", tran.rep)
  router.post("/tran/rexp", tran.rexportD)
  router.post("/tran/rej", itran.rej)

  router.get("/itran", itran.get)
  router.post("/itran/dtl", itran.del)
  router.post("/itran/syn", itran.syn)
  router.post("/itran/edit", itran.editPost)
  router.post("/itran/col", itran.col)

  router.post("/itran/app", itran.app)
  router.get("/itran/exp", itran.exportD)
  router.post("/itran/rep", itran.rep)
  router.post("/itran/rexp", itran.rexportD)
  router.post("/itran/update-status", itran.updateStatus)
  router.get("/sea/sbfOu", serial.getSerialByFormInvAndOu)
  router.get("/sea/fbtOu", serial.getFormByTypeInvAndOu)
}
//8-find
router.get("/seek/xml0/:id", util.verify, sea.xml0)
router.get("/seek/getcapt", util.getcapt)
router.get("/seek/search0/:id", util.verify, sea.search0)
router.get("/seek/xls", util.verify, sea.xlssearch)
//if (dbtype == "orcl") router.get("/seek/rpxls1", util.verify, sea.rpxlssearch)
router.get("/seek/search2", util.verify, sea.search2)
router.get("/seek/change", util.verify, sea.change)
router.get("/seek/xml1", sea.xml1)
router.get("/seek/search1", sea.search1)

if (dbtype == "mssql") {
  router.post("/seek/searchP", sea.searchP)
  router.get("/seek/searchvcm", sea.searchvcm)
  router.post("/seek/searchvcmpost", sea.searchvcmpost)
}
router.get("/seek/reset", sea.reset)
router.get("/seek/login", sea.login)
router.post("/seek/xml", uploadXML, sign.upload)
if (dbtype == "mssql") {
  router.get("/seek/getou", sea.getou)

  router.get("/seek/getbranch", sea.getbranch)
  router.get("/seek/search_eco", util.verify, sea.search_eco)
  router.get("/seek/search_vin", util.verify, sea.search_vin)
}


//8-dieuchinh
router.post("/adj", inv.adj)
router.post("/adjniss", inv.adjniss)
router.route("/adj/:id").get(inc.load).put(inv.put)
router.get("/adj/del/:id/:pid", inv.del)
//9-thaythe
router.post("/rep", inv.rep)
//10-huy
router.post("/ica/:id", inv.cancel)
router.post("/iwc/:id", inv.trash)
router.post("/ist", inv.istatus)
//11-log
router.route("/psyslog").get(syslog.get)
router.route("/psyslog/xls").get(syslog.xls)
//12-JS Report
router.post("/seek/report", util.jsReportRender)
//router.post("/view/pdf", util.jsReportRenderById)
//update note
if (config.ent == 'vib') {
  router.post("/inv/note", inv.note)
  router.post("/inv/noteseq", inv.noteseq)
  router.post("/inv/numbertran", inv.upNumberTran)
}
//13-redis
if (config.dbCache) {
  router.post("/redis/apiget", redis.apiget)
  router.post("/redis/apiset", redis.apiset)
  router.post("/redis/apiincr", redis.apiincr)
}
//statement
router.route("/stm/:id").get(statement.load)//.put(statement.put)//dieu chinh
router.post("/stm", statement.post)//dky ms
router.get("/stg", statement.get)//tim kiem
router.get("/stm/sendback/:id", statement.SendbackToVanSTM)//gui lai cqt
router.route("/sta").post(statement.apprpost).put(statement.apprput)//duyet
router.get("/stm/htm/:id", statement.htm)//xem
router.get("/stm/htmtct/:id", statement.vpdftctresp)//xem html tct response
//api.get("/stg/get", statement.check)//check
router.post("/stm/hascode", statement.hascode)
router.get("/stm/hscqt/:id", statement.hscqt)
router.get("/stm/viewXml/:id", statement.hscqtXml)
router.get("/stg/cfgcert", statement.getConfigCertData)
//wrong_notice
router.post("/wno/xml", wno.genXML)
router.post("/wno/mail", wno.sendmailwn)
router.post("/wno/now", wno.sendnow)
router.get("/wno/sendback/:id", wno.SendbackToVanWNO)
router.post("/wno/wait", wno.sendtbss)
router.route("/wno/lwngroup").get(wno.getWaitingWNList)
router.get("/wno/hscqt/:id", wno.hscqt)
router.get("/wno/htm/:id", wno.htm)
router.get('/wno', wno.getInvWrongNotice)
router.get('/wno/excel', wno.searchDatatbss)
router.get("/wno/viewXml/:id", wno.hscqtXml)
//Bảng tổng hợp
router.route("/bth/sdatainv").post(bth.searchData)
router.post("/bth", bth.save)
router.post("/bth/app", bth.approvalbth_post).put("/bth/app", bth.approvalbth_put)
router.post("/bth/sendback/:id", bth.SendbackToVanBTH)
router.route("/bth/sbthdata").get(bth.searchDataBth)
router.get("/bth/htm/:id", bth.htm)
router.post("/bth/dell", bth.dellbth)
router.post("/bths/dell", bth.dellbths)
router.post("/bth/delinvadj", bth.delinvadj)
router.post("/bth/htmnew", bth.htmnew)
router.route("/bth/searchAppr").get(bth.searchBthApproved)
router.post("/bth/sendInvAdj", bth.saveInvAdj)
router.get("/bth/searchInvAdj/:id", bth.searchInvAdj)
router.get("/bth/hscqt/:id", bth.hscqt)
router.get("/bth/excel/custom/:id", bth.minutesCustom)
router.get("/bth/xml/:id", inc.xmlListInv)
router.get("/bth/viewXml/:id", bth.hscqtXml)
//mail direct download attachment
router.get("/invoice-mailpdf", inc.downloadpdf)
router.get("/invoice-mailxml", inc.downloadxml)
module.exports = router