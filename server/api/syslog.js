"use strict"
const path = require("path")
const fs = require("fs")
const docxt = require("docxtemplater")
const xlsxtemp = require("xlsx-template")
const jszip = require("jszip")
const moment = require("moment")
const util = require("./util")
const config = require("./config")
const logger4app = require("./logger4app")
const redis = require("./redis")
const sec = require("./sec")
const inc = require("./inc")
const dtf=config.dtf
const sql_get = {
    sql_get_mssql: `null id,format(dt,'dd/MM/yyyy HH:mm:ss') dt,dt dt_order,fnc_id,fnc_name,fnc_url,action,user_id,user_name,src,dtl,msg_id,CASE WHEN msg_status=0 THEN N'Lỗi' WHEN msg_status=1 THEN N'Thành công' ELSE '' END  msg_status,r1,r2,r3,r4,doc`,
    sql_get_orcl: `TO_CHAR(ID) "id",TO_CHAR(DT,'dd/MM/yyyy HH24:mi:ss') "dt",FNC_ID "fnc_id",FNC_NAME "fnc_name",FNC_URL "fnc_url",ACTION "action",USER_ID "user_id",USER_NAME "user_name",SRC "src",DTL "dtl",MSG_ID "msg_id",CASE WHEN msg_status=0 THEN 'Lỗi' WHEN msg_status=1 THEN 'Thành công' ELSE '' END "msg_status",r1 "r1",r2 "r2",r3 "r3",r4 "r4",doc "doc"`,
    sql_get_mysql: `id id,DATE_FORMAT(dt,'%d/%m/%Y %T') dt,fnc_id,fnc_name,fnc_url,action,user_id,user_name,src,dtl,msg_id,CASE WHEN msg_status=0 THEN 'Lỗi' WHEN msg_status=1 THEN 'Thành công' ELSE '' END  msg_status,r1,r2,r3,r4,doc`,
    sql_get_pgsql: `id,to_char(dt,'DD/MM/YYYY HH24:MI:SS') dt,fnc_id,fnc_name,fnc_url,action,user_id,user_name,src,dtl,msg_id,CASE WHEN msg_status=0 THEN 'Lỗi' WHEN msg_status=1 THEN 'Thành công' ELSE '' END msg_status,r1,r2,r3,r4,doc`
}
const sql_xls = {
    sql_xls_mssql: `null id,format(dt,'dd/MM/yyyy HH:mm:ss') dt,fnc_id,fnc_name,fnc_url,action,user_id,user_name,src,dtl,msg_id,CASE WHEN msg_status=0 THEN N'Lỗi' WHEN msg_status=1 THEN N'Thành công' ELSE '' END  msg_status,r1,r2,r3,r4,doc`,
    sql_xls_orcl: `TO_CHAR(ID) "id",TO_CHAR(DT,'dd/MM/yyyy HH24:mi:ss') "dt",FNC_ID "fnc_id",FNC_NAME "fnc_name",FNC_URL "fnc_url",ACTION "action",USER_ID "user_id",USER_NAME "user_name",SRC "src",DTL "dtl",MSG_ID "msg_id",CASE WHEN msg_status=0 THEN 'Lỗi' WHEN msg_status=1 THEN 'Thành công' ELSE '' END "msg_status",r1 "r1",r2 "r2",r3 "r3",r4 "r4",doc "doc"`,
    sql_xls_mysql: `null id,DATE_FORMAT(dt,'%d/%m/%Y %T') dt,fnc_id,fnc_name,fnc_url,action,user_id,user_name,src,dtl,msg_id,msg_status,CASE WHEN msg_status=0 THEN 'Lỗi' WHEN msg_status=1 THEN 'Thành công' ELSE '' END  msg_status,r1,r2,r3,r4,doc`,
    sql_xls_pgsql: `id,to_char(dt,'DD/MM/YYYY HH24:MI:SS') dt,fnc_id,fnc_name,fnc_url,action,user_id,user_name,src,dtl,msg_id,CASE WHEN msg_status=0 THEN 'Lỗi' WHEN msg_status=1 THEN 'Thành công' ELSE '' END msg_status,r1,r2,r3,r4,doc`
}

const Service = {
    
    get: async (req, res, next) => {
        try {
            const query = req.query, filter = JSON.parse(query.filter)
            const sort = query.sort, start = query.start ? query.start : 0, count = query.count ? query.count : 10
            let where = " where dt between ? and ?", order, sql, result, rows, ret, val
            const fm = moment(filter.fd), tm = moment(filter.td)
            const fd = (`${config.dbtype}` == "mssql" || `${config.dbtype}` == "mysql") ? fm.startOf("day").format("YYYY-MM-DD HH:mm:ss") : new Date(filter.fd),
                td = (`${config.dbtype}` == "mssql" || `${config.dbtype}` == "mysql") ? tm.endOf("day").format("YYYY-MM-DD HH:mm:ss") : new Date(moment(filter.td).endOf("day"))
            let binds = [fd, td]
            let pagerobj = inc.getsqlpagerandbind(start, count)
            let i = 3
            Object.keys(filter).forEach((key) => {
                val = filter[key]
                if (val) {
                    switch (key) {
                        case "fd":
                            break
                        case "td":
                            break
                        case "srclog":
                            where += ` and SRC=?`
                            binds.push(val)
                            break
                        case "type":
                            where += ` and FNC_ID=?`
                            binds.push(val)
                            break
                        case "user":
                            where += ` and USER_ID like ?`
                            binds.push('%'+val+'%')
                            break
                        case "msg_id":
                            where += ` and MSG_ID like ?`
                            binds.push('%'+val+'%')
                            break
                        case "msg_status":
                            where += ` and MSG_STATUS = ?`
                            binds.push(val)
                            break
                        default:
                            where += ` and ${key} like ?`
                            binds.push('%'+val+'%')
                            break
                    }
                }
            })
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = ` order by ${key} ${sort[key]}`
                })
            }
            else order = " order by dt desc, id"
            sql = `select ${sql_get[`sql_get_${config.dbtype}`]} from s_sys_logs ${where} ${order} ${pagerobj.vsqlpagerstr}`
            for (let t of pagerobj.vsqlpagerbind) binds.push(t)
            result = await inc.execsqlselect(sql, binds)
            ret = { data: result, pos: start }
            if (start == 0) {
                sql = `select count(*) "total" from s_sys_logs ${where}`
                let bindscount = []
                for (let i = 0; i < binds.length - 2; i++) bindscount.push(binds[i])
                result = await inc.execsqlselect(sql, bindscount)
                ret.total_count = result[0].total
            }
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    xls: async (req, res, next) => {
        try {
            const query = req.query, filter = JSON.parse(query.filter), type = query.type
            const sort = query.sort
            let where = " where dt between ? and ?", order, sql, result, rows, val, json
            const fm = moment(filter.fd), tm = moment(filter.td)
            const fd = fm.startOf("day").format("YYYY-MM-DD HH:mm:ss"), td = tm.endOf("day").format("YYYY-MM-DD HH:mm:ss")
            let binds = [fd, td]
           
            let i = 3
            Object.keys(filter).forEach((key) => {
                val = filter[key]
                logger4app.debug(val)
                if (val) {
                    switch (key) {
                        case "fd":
                            break
                        case "td":
                            break
                        case "srclog":
                            where += ` and SRC=?`
                            binds.push(val)
                            break
                        case "type":
                            where += ` and FNC_ID=?`
                            binds.push(val)
                            break
                        case "user":
                            where += ` and USER_ID like ?`
                            binds.push('%'+val+'%')
                            break
                        case "msg_id":
                            where += ` and MSG_ID like ?`
                            binds.push('%'+val+'%')
                            break
                        case "msg_status":
                            where += ` and MSG_STATUS = ?`
                            binds.push(val)
                            break
                        default:
                            where += ` and ${key} like ?`
                            binds.push('%'+val+'%')
                            break
                    }
                }
            })
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = ` order by ${key} ${sort[key]}`
                })
            }
            else order = " order by dt desc, id"
            sql = `select ${sql_xls[`sql_xls_${config.dbtype}`]} from s_sys_logs ${where} ${order} offset 0 rows fetch next ${config.MAX_ROW_EXCEL_EXPORT} rows only `
            result = await inc.execsqlselect(sql, binds)
            
            rows = result
            let fn = "temp/SYSLOG.xlsx"
            if(type ==1){
                fn = "temp/AUDIT_LOG_SCB.xlsx"
            }
            json = { table: rows }
            const file = path.join(__dirname, "..", "..", "..", fn)
            const xlsx = fs.readFileSync(file)
            const template = new xlsxtemp(xlsx)
            template.substitute(1, json)
            res.end(template.generate(), "binary")
        }
        catch (err) {
            logger4app.debug("excel" + err)

            next(err)


        }
    }
}
module.exports = Service   