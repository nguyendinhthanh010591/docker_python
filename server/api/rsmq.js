"use strict"
const config = require("./config")
const logger4app = require("./logger4app")
if (!config.dbCache) {
    const RSMQ = require("rsmq")
    const decrypt = require("./encrypt")
    let redis = JSON.parse(JSON.stringify(config.redisConfig))
    if (redis.password) redis.password = decrypt.decrypt(redis.password)
    const rsmq = new RSMQ(redis)
    module.exports = rsmq
} else {
    module.exports = {}
}