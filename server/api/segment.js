"use strict"
const config = require("../config")
const logger4app = require("../logger4app")
const util = require("../util")
const inc = require("./inc")
const { bind } = require("file-loader")

const Service= {
    get: async(req,res,next)=>{
        try{
            let binds=[], where="where 1=1", order
            const query= req.query, sort=query.sort
            if(query.filter){
                let filter = JSON.parse(query.filter), val, i = 1
                Object.keys(filter).forEach(key => {
                    val = filter[key]
                    if(val){
                        where += ` and upper(${key}) like ?`
                        binds.push(`%${val.toUpperCase()}%`)
                    }
                }
                )
            }
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = ` order by ${key} ${sort[key]}`
                })
            } else order = ` order by id`
            const sql=`select id "id",seg_name "seg_name" from s_segment ${where} ${order}`
            const result= await inc.execsqlselect(sql,binds)
            res.json(result)
        }
        catch(err){
            next(err)
        }
    },
    post: async(req,res,next)=>{
        try{
            let body=req.body,binds, result, operation = body.webix_operation,sql
            switch(operation){
                case "insert":
                    sql="insert into s_segment (seg_name) values (?)"
                    binds=[body.seg_name]
                    break
                case "delete":
                    sql="delete from s_segment where id=?"
                    binds=[body.id]
                    break
                case "update":
                    sql="update s_segment set seg_name=? where id=?"
                    binds=[body.seg_name,body.id]
                    break
                default:
                    throw new Error(`${operation} là không hợp lệ \n (${operation} is invalid)`)
            }
            result = await inc.execsqlinsupddel(sql, binds)
            if (operation == "insert") res.json(result)
            else res.json(result)
        }
        catch(err){
            next(err)
        }
 },
    segment: async(req,res,next) =>{
        let result, sql, data
        sql= `select name "id",name "value" from s_cat where type = 'SEGMENT' order by id`
        result = await inc.execsqlselect(sql, [])
        data = result
        res.json(data)
    }
}


module.exports=Service