"use strict"
const moment = require('moment')
const sec = require("./sec")
const inc = require("./inc")
const config = require("./config")
const logger4app = require("./logger4app")
const serial_usr_grant = config.serial_usr_grant
const ENT = config.ent
const tgcvalue = {
    tgcvalue_mssql: { tgcvalue1: `FORMAT(idt,'dd/MM/yy')`, tgcvalue2: `CONCAT(RIGHT(CONCAT('00',DATEPART(ww,idt)),2),'/',FORMAT(idt,'yy'))`, tgcvalue3: `FORMAT(idt,'MM/yy')` },
    tgcvalue_mysql: { tgcvalue1: `DATE_FORMAT(idt,'%d/%m/%Y')`, tgcvalue2: `DATE_FORMAT(idt,'%v/%x')`, tgcvalue3: `DATE_FORMAT(idt,'%m/%Y')` },
    tgcvalue_orcl: { tgcvalue1: `to_char(idt,'DD/MM/YYYY')`, tgcvalue2: `to_char(idt,'WW/YY')`, tgcvalue3: `to_char(idt,'MM/YY')` },
    tgcvalue_pgsql: { tgcvalue1: `to_char(idt,'DD/MM/YYYY')`, tgcvalue2: `to_char(idt,'WW/YY')`, tgcvalue3: `to_char(idt,'MM/YY')` },
}
const sql_home_01 = {
    sql_home_01_mssql: `select 
    coalesce(sum(1),0) i0, 
    coalesce(sum(CASE status WHEN 1 THEN 1 ELSE 0 end),0) i1,
    coalesce(sum(CASE status WHEN 2 THEN 1 ELSE 0 end),0) i2,
    coalesce(sum(CASE status WHEN 3 THEN 1 ELSE 0 end),0) i3,
    coalesce(sum(CASE status WHEN 4 THEN 1 ELSE 0 end),0) i4 
    from s_inv`,
    sql_home_01_mysql: `select 
    coalesce(sum(1),0) i0, 
    coalesce(sum(CASE status WHEN 1 THEN 1 ELSE 0 end),0) i1,
    coalesce(sum(CASE status WHEN 2 THEN 1 ELSE 0 end),0) i2,
    coalesce(sum(CASE status WHEN 3 THEN 1 ELSE 0 end),0) i3,
    coalesce(sum(CASE status WHEN 4 THEN 1 ELSE 0 end),0) i4 
    from s_inv`,
    sql_home_01_orcl: `select 
    nvl(sum(i0),0) "i0", 
    nvl(sum(i1),0) "i1",
    nvl(sum(i2),0) "i2",
    nvl(sum(i3),0) "i3",
    nvl(sum(i4),0) "i4"
    from s_inv_sum`,
    sql_home_01_pgsql: `select 
    coalesce(sum(1),0) i0, 
    coalesce(sum(CASE status WHEN 1 THEN 1 ELSE 0 end),0) i1,
    coalesce(sum(CASE status WHEN 2 THEN 1 ELSE 0 end),0) i2,
    coalesce(sum(CASE status WHEN 3 THEN 1 ELSE 0 end),0) i3,
    coalesce(sum(CASE status WHEN 4 THEN 1 ELSE 0 end),0) i4 
    from s_inv`,
}
const sql_home_02 = {
    sql_home_02_mssql: `,
    sum(1) i0,
    sum(CASE status WHEN 1 THEN 1 ELSE 0 end) i1,
    sum(CASE status WHEN 2 THEN 1 ELSE 0 end) i2,
    sum(CASE status WHEN 3 THEN 1 ELSE 0 end) i3,
    sum(CASE status WHEN 4 THEN 1 ELSE 0 end) i4 `,
    sql_home_02_mysql: `,
    sum(1) i0,
    sum(CASE status WHEN 1 THEN 1 ELSE 0 end) i1,
    sum(CASE status WHEN 2 THEN 1 ELSE 0 end) i2,
    sum(CASE status WHEN 3 THEN 1 ELSE 0 end) i3,
    sum(CASE status WHEN 4 THEN 1 ELSE 0 end) i4 `,
    sql_home_02_orcl: `,
    sum(1) "i0",
    sum(CASE status WHEN 1 THEN 1 ELSE 0 end) "i1",
    sum(CASE status WHEN 2 THEN 1 ELSE 0 end) "i2",
    sum(CASE status WHEN 3 THEN 1 ELSE 0 end) "i3",
    sum(CASE status WHEN 4 THEN 1 ELSE 0 end) "i4" `,
    sql_home_02_pgsql: `,
    sum(1) i0,
    sum(CASE status WHEN 1 THEN 1 ELSE 0 end) i1,
    sum(CASE status WHEN 2 THEN 1 ELSE 0 end) i2,
    sum(CASE status WHEN 3 THEN 1 ELSE 0 end) i3,
    sum(CASE status WHEN 4 THEN 1 ELSE 0 end) i4 `,
}
const sql_home_03 = {
    sql_home_03_mssql: `, 
    sum(coalesce(totalv,0))/? totalv,
    sum(coalesce(vatv,0))/? vatv `,
    sql_home_03_mysql: `, 
    sum(coalesce(CASE WHEN adjtyp=2 THEN CAST(doc->'$.totalv' as UNSIGNED) ELSE totalv END,0))/? totalv,
    sum(coalesce(CASE WHEN adjtyp=2 THEN CAST(doc->'$.vatv' as UNSIGNED)   ELSE vatv   END,0))/? vatv `,
    sql_home_03_orcl: `, 
    sum(NVL(CASE WHEN adjtyp=2 THEN TO_NUMBER(a.doc.totalv) ELSE totalv END,0))/? "totalv",
    sum(NVL(CASE WHEN adjtyp=2 THEN TO_NUMBER(a.doc.vatv)   ELSE vatv   END,0))/? "vatv" `,
    sql_home_03_pgsql: `, 
    sum(coalesce(CASE WHEN adjtyp=2 THEN CAST(doc->>'totalv' as bigint) ELSE totalv END,0))/? totalv,
    sum(coalesce(CASE WHEN adjtyp=2 THEN CAST(doc->>'vatv' as bigint)   ELSE vatv   END,0))/? vatv `,
}
const sql_home_04 = {
    sql_home_04_mssql: `CONCAT(form,'-',serial) serial,max,cur,CONCAT(ROUND(((max - cur)/max) *100,8),'%') rate,(max-cur) kd`,
    sql_home_04_mysql: `CONCAT(form,'-',serial) serial,max max,cur cur,CONCAT(ROUND(((max - cur)/max) *100,8),'%') rate,(max-cur) kd`,
    sql_home_04_orcl: `form||'-'||serial "serial",max "max",cur "cur",CONCAT(ROUND(((max - cur)/max) *100,8),'%') "rate",(max-cur) "kd"`,
    sql_home_04_pgsql: `CONCAT(form,'-',serial) serial,max,cur,CONCAT(ROUND(((max - cur)/max) *100,8),'%') rate,(max-cur) kd`,
}
const Service = {
    get: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, query = req.query
            let form_combo = query.form_combo, serial_combo = query.serial_combo
            if (!form_combo) form_combo = '*'
            if (!serial_combo) serial_combo = '*'
            let fd = moment(query.fd).format("YYYY-MM-DD 00:00:00")
            let td = moment(query.td).format("YYYY-MM-DD 23:59:59")
            if (config.dbtype == "orcl") {
                fd = new Date(moment(query.fd).format("YYYY-MM-DD 00:00:00"))
                td = new Date(moment(query.td).format("YYYY-MM-DD 23:59:59"))
            }
            const tg = query.tg, dvt = query.dvt
            if (!['1', '1000', '1000000'].includes(dvt)) throw new Error("Tham số ĐVT không hợp lệ \n (Invalid DVT parameter)")
            let tgc = tgcvalue[`tgcvalue_${config.dbtype}`]['tgcvalue'+tg]
            let sql, rs, rows, kq = {}, thsd, invs = 0, totalv = 0, vatv = 0
            //1-thsd
            let where1 = `where stax=? and idt BETWEEN ? and ? and (? ='*' or form=?) and (?='*' or serial = ?)`
            let bind1 = [taxc, fd, td, form_combo, form_combo, serial_combo, serial_combo]
            if (serial_usr_grant) {
                where1 = `, (select taxc, type types, form forms, serial serials from s_serial ss, s_seusr ss2 where ss.id = ss2.se and ss2.usrid = ?) ss where stax=? and stax = ss.taxc and type = ss.types and form = ss.forms and serial = ss.serials`
                bind1 = [token.uid, taxc]
            }

            sql = `${sql_home_01[`sql_home_01_${config.dbtype}`]} ${where1}`
            rs = await inc.execsqlselect(sql, bind1)
            thsd = rs[0]
            kq.thsd = thsd
            //2-hd
            let where2 = `where idt BETWEEN ? and ? and stax=?`
            let bind2 = [fd, td, taxc]
            if (serial_usr_grant) {
                where2 = `, (select taxc, type types, form forms, serial serials from s_serial ss, s_seusr ss2 where ss.id = ss2.se and ss2.usrid = ?) ss where idt BETWEEN ? and ? and stax=? and stax = ss.taxc and type = ss.types and form = ss.forms and serial = ss.serials`
                bind2 = [token.uid, fd, td, taxc]
            }
            sql = `select ${tgc} tg${sql_home_02[`sql_home_02_${config.dbtype}`]} 
            from s_inv ${where2} 
            group by ${tgc} order by ${tgc}`
            rs = await inc.execsqlselect(sql, bind2)
            rows = rs
            kq.invoice = rows

            //Chinh lai doan lay hoa don
            where2 = `where status in (1,2,3,4) and idt BETWEEN ? and ? and stax=? and (? ='*' or form=?) and (?='*' or serial = ?)`
            bind2 = [fd, td, taxc, form_combo, form_combo, serial_combo, serial_combo]
            if (serial_usr_grant) {
                where2 = `, (select taxc, type types, form forms, serial serials from s_serial ss, s_seusr ss2 where ss.id = ss2.se and ss2.usrid = ?) ss where status in (1,2,3,4) and idt BETWEEN ? and ? and stax=? and (? ='*' or form=?) and (?='*' or serial = ?) and stax = ss.taxc and type = ss.types and form = ss.forms and serial = ss.serials`
                bind2 = [token.uid, fd, td, taxc, form_combo, form_combo, serial_combo, serial_combo]
            }
            sql = `select count(*) "i0" from s_inv ${where2}`
            rs = await inc.execsqlselect(sql, bind2)
            rows = rs
            invs = rows[0].i0

            //3-totalv
            let where3 = `where idt BETWEEN ? and ? and stax=? and status=3 and type in ('01GTKT','02GTTT') and cid is null 
            and (? ='*' or form=?) and (?='*' or serial = ?)`
            let bind3 = [dvt, dvt, fd, td, taxc, form_combo, form_combo, serial_combo, serial_combo]
            if (serial_usr_grant) {
                where3 = `, (select taxc, type types, form forms, serial serials from s_serial ss, s_seusr ss2 where ss.id = ss2.se and ss2.usrid = ?) ss where idt BETWEEN ? and ? and stax=? and status=3 and type in ('01GTKT','02GTTT') and cid is null 
                and (? ='*' or form=?) and (?='*' or serial = ?) and stax = ss.taxc and type = ss.types and form = ss.forms and serial = ss.serials`
                bind3 = [dvt, dvt, token.uid, fd, td, taxc, form_combo, form_combo, serial_combo, serial_combo]
            }
            //let vatsql = (ENT != "vcm") ? `CASE WHEN adjtyp=2 THEN TRY_CAST(JSON_VALUE(doc,'$.vatv') as numeric(20,2))   ELSE vatv   END,0` : `vatv,0`
            //let totalsql = (ENT != "vcm") ? `CASE WHEN adjtyp=2 THEN TRY_CAST(JSON_VALUE(doc,'$.totalv') as numeric(20,2)) ELSE totalv END,0` : `totalv,0`
            sql = `select ${tgc} tg${sql_home_03[`sql_home_03_${config.dbtype}`]} 
            from s_inv a ${where3} 
            group by ${tgc} order by ${tgc}`
            rs = await inc.execsqlselect(sql, bind3)
            rows = rs
            kq.revenue = rows
            for (const row of rows) {
                totalv += parseFloat(row.totalv)
                vatv += parseFloat(row.vatv)
            }
            kq.total = { invs: invs, totalv: totalv, vatv: vatv }
            //4-serial
            let where4 = `where taxc=?`
            let bind4 = [taxc]
            if (serial_usr_grant) {
                where4 = `, s_seusr ss where id = ss.se and ss.usrid = ? and taxc=?`
                bind4 = [token.uid, taxc]
            }
            sql = `select ${sql_home_04[`sql_home_04_${config.dbtype}`]} from s_serial ${where4} and status=1 
            order by form,serial`
            rs = await inc.execsqlselect(sql, bind4)
            kq.serial = rs
            res.json(kq)
        } catch (err) {
            next(err)
        }
    }
}
module.exports = Service