"use strict"
const moment = require("moment")
const config = require("./config")
const logger4app = require("./logger4app")
const FTPClient = require('ftp')
const fs = require('fs')
const SFTPClient = require('ssh2-sftp-client')
const email = require("./email")
const decrypt = require('./encrypt')
//const ftpConfig=config.ftpConfig
const Service = {
    ftpupload: async (content, filepath) => {
        let ftpConfig = JSON.parse(JSON.stringify(config.ftpConfig))
        ftpConfig.password = decrypt.decrypt(ftpConfig.password)
        let ftpClient
        if (ftpConfig.SFTP) {
            ftpClient = new SFTPClient()
            ftpClient.connect(ftpConfig)
                .then(() => {
                    return ftpClient.put(content, filepath)
                })
                .then(() => {
                    return ftpClient.end()
                })
                .catch(err => {
                    logger4app.error(err.message)
                })
        }
        else {
            ftpClient = new FTPClient()
            ftpClient.on('ready', function () {
                ftpClient.put(content, filepath, function (err) {
                    if (err) throw err;
                    ftpClient.end();
                });
            });
            // connect to localhost:21 as anonymous
            ftpClient.connect(ftpConfig);
        }

    },
    ftpupload_ctbcDH: async (content, filepath) => {
        let ftpConfig = JSON.parse(JSON.stringify(config.ftpConfigDH))
        ftpConfig.password = decrypt.decrypt(ftpConfig.password)
        let ftpClient
        if (ftpConfig.SFTP) {
            ftpClient = new SFTPClient()
            ftpClient.connect(ftpConfig)
                .then(() => {
                    return ftpClient.put(content, filepath)
                })
                .then(() => {
                    return ftpClient.end()
                })
                .catch(err => {
                    logger4app.error(err.message)
                })
        }
        else {
            ftpClient = new FTPClient()
            ftpClient.on('ready', function () {
                ftpClient.put(content, filepath, function (err) {
                    if (err) throw err;
                    ftpClient.end();
                });
            });
            // connect to localhost:21 as anonymous
            ftpClient.connect(ftpConfig);
        }

    },

    ftpupload_ctbcACCGR: async (content, filepath) => {
        let ftpConfig = JSON.parse(JSON.stringify(config.ftpConfigAG))
        ftpConfig.password = decrypt.decrypt(ftpConfig.password)
        let ftpClient
        if (ftpConfig.SFTP) {
            ftpClient = new SFTPClient()
            ftpClient.connect(ftpConfig)
                .then(() => {
                    return ftpClient.put(content, filepath)
                })
                .then(() => {
                    return ftpClient.end()
                })
                .catch(err => {
                    logger4app.error(err.message)
                })
        }
        else {
            ftpClient = new FTPClient()
            ftpClient.on('ready', function () {
                ftpClient.put(content, filepath, function (err) {
                    if (err) throw err;
                    ftpClient.end();
                });
            });
            // connect to localhost:21 as anonymous
            ftpClient.connect(ftpConfig);
        }

    },
    ftpupload_ctbcXML: async (content, filepath) => {
        let ftpConfig = JSON.parse(JSON.stringify(config.ftpConfigXML))
        ftpConfig.password = decrypt.decrypt(ftpConfig.password)
        let ftpClient
        if (ftpConfig.SFTP) {
            ftpClient = new SFTPClient()
            ftpClient.connect(ftpConfig)
                .then(() => {
                    return ftpClient.put(content, filepath)
                })
                .then(() => {
                    return ftpClient.end()
                })
                .catch(err => {
                    logger4app.error(err.message)
                })
        }
        else {
            ftpClient = new FTPClient()
            ftpClient.on('ready', function () {
                ftpClient.put(content, filepath, function (err) {
                    if (err) throw err;
                    ftpClient.end();
                });
            });
            // connect to localhost:21 as anonymous
            ftpClient.connect(ftpConfig);
        }

    },
    uploadattach_mzh:
        async (content, name,date) => {
            let ftpConfig = JSON.parse(JSON.stringify(config.ftpConfig))
            ftpConfig.password = decrypt.decrypt(ftpConfig.password)
            let ftpClient
            if (ftpConfig.SFTP)
                ftpClient = new SFTPClient()
            else 
                ftpClient = new FTPClient()
            try {
              
                if (name) {
                   
                    const year =(moment(date)).format('YYYY')
                    const Month = (moment(date)).format('MMM')
                    const day = (moment(date)).format('DD')
                    ftpClient.on('ready', function() {
                        ftpClient.mkdir(`${year}`, function(err) {
                            ftpClient.mkdir(`${year}/${Month}`, function(err) {
                                ftpClient.mkdir(`${year}/${Month}/${day}`, function(err) {
                                    ftpClient.put(Buffer.from(content), `${year}/${Month}/${day}/`+name, function(err) {
                                        if (err) throw err;
                                        ftpClient.end();
                                      });
                                })
                            })
                        })
                       
                       
                       // ftpClient.put(Buffer.from(content), filepath);
                      });
                      // connect to localhost:21 as anonymous
                      ftpClient.connect(ftpConfig);
                }
            } catch (err) {
                logger4app.debug(`uploadattach file ${name}: `, err.message)
                throw (err)
            } finally {

            }
        },
        uploadattach:        async (doc, xml) => {
            /*
            let ftpConfig = JSON.parse(JSON.stringify(config.ftpConfig))
            ftpConfig.password = decrypt.decrypt(ftpConfig.password)
            let ftpClient
            if (ftpConfig.SFTP)
                ftpClient = new SFTPClient()
            else 
                ftpClient = new FTPClient()
            */
            try {
                const arr = doc.bmail.trim().split(/[ ,;]+/).map(e => e.trim())
                let attachments = await email.getOnlyAttachFile(doc, xml, arr)
                if (doc && (doc.status == 3) && attachments[0].filename) {
                    let filepath = config.ftpdestPath + attachments[0].filename
                    Service.ftpupload(Buffer.from(attachments[0].content), filepath)
                    /*
                    let ftpConfig = JSON.parse(JSON.stringify(config.ftpConfig))
                    ftpConfig.password = decrypt.decrypt(ftpConfig.password)
                    ftpClient.connect(ftpConfig)
                        .then(() => {
                            return ftpClient.put(Buffer.from(attachments[0].content), filepath)
                        })
                        .then(() => {
                            return ftpClient.end()
                        })
                        .catch(err => {
                            logger4app.error(err.message)
                        })
                        */
                }
            } catch (err) {
                logger4app.debug(`uploadattach file ${doc.id}: `, err.message)
                throw (err)
            } finally {

            }
        },
        uploadmailctbc: async (doc, xml) => {
            /*
            let ftpConfig = JSON.parse(JSON.stringify(config.ftpConfig))
            ftpConfig.password = decrypt.decrypt(ftpConfig.password)
            let ftpClient
            if (ftpConfig.SFTP)
                ftpClient = new SFTPClient()
            else 
                ftpClient = new FTPClient()
            */    
            let filepath
            try {
                let filename = `VX01-${(doc.bcode && doc.bcode != '') ? doc.bcode : ''}-${moment(new Date()).format('YYYYMMDD')}-${moment(new Date()).format('HHmmssSSS')}`.split("/").join(".")
                
                filepath = config.ftpdestPathXML + filename + '.bsnsxml'
                //fs.writeFileSync('C:/test/'+ filename + '.bsnsxml', Buffer.from(xml))
                await Service.ftpupload_ctbcXML(Buffer.from(xml), filepath)
                
                /*
                ftpClient.connect(ftpConfig)
                    .then(() => {
                        return ftpClient.put(Buffer.from(xml), filepath)
                    })
                    .then(() => {
                        return ftpClient.end()
                    })
                    .catch(err => {
                        logger4app.error(err.message)
                    })
                */
            } catch (err) {
                logger4app.debug(`uploadattach uploadmailctbc file ${filepath}: `, err)
                throw (err)
            } finally {

            }
        },
        uploadsocctbcDH:
        async (filecontent, checkdh) => {
            /*
            let ftpConfig = JSON.parse(JSON.stringify(config.ftpConfig))
            ftpConfig.password = decrypt.decrypt(ftpConfig.password)
            let ftpClient
            if (ftpConfig.SFTP)
                ftpClient = new SFTPClient()
            else 
                ftpClient = new FTPClient()
            */
            
            let filepath, tail, filename

            try {
                // filename = `VN-EVAT-${moment(new Date()).format('YYYYMMDD')}-${moment(new Date()).format('HHmm')}`.split("/").join(".")
                if(checkdh==0){
                    filename = `VN-EVAT-${moment(new Date()).format('YYYYMMDD')}`.split("/").join(".")
                    tail = '.D'
                }else{
                    filename = `VN-EVAT-${moment(new Date()).format('YYYYMMDD')}`.split("/").join(".")
                    tail = '.H'
                }
                filepath = config.ftpdestPathDH + filename + tail
                Service.ftpupload_ctbcDH(Buffer.from(filecontent,{ encoding: 'utf8'}), filepath)
                /*
                ftpClient.connect(ftpConfig)
                    .then(() => {
                        return ftpClient.put(Buffer.from(filecontent,{ encoding: 'utf8'}), filepath)
                    })
                    .then(() => {
                        return ftpClient.end()
                    })
                    .catch(err => {
                        logger4app.error(err.message)
                    })
                    */
            } catch (err) {
                throw (err)
            } finally {

            }
        },
        uploadsocctbcACCGR:
        async (filecontent, check) => {
            let filepath, tail, filename
            try {
                if(check==2){
                    filename = `VN-EVAT_ACCT`.split("/").join(".")
                    tail = '.csv'
                }else {
                    filename = `VN-EVAT_GROUP`.split("/").join(".")
                    tail = '.csv'
                }
                filepath = config.ftpdestPathAG + filename + tail
                Service.ftpupload_ctbcACCGR(Buffer.from(filecontent,{ encoding: 'utf8'}), filepath)
            } catch (err) {
                throw (err)
            }
        },
        uploadsplunkscb:
        async (filecontent,check) => {
            /*
            let ftpConfig = JSON.parse(JSON.stringify(config.ftpConfig))
            ftpConfig.password = decrypt.decrypt(ftpConfig.password)
            let ftpClient
            if (ftpConfig.SFTP)
                ftpClient = new SFTPClient()
            else 
                ftpClient = new FTPClient()
            */
            
            let filepath ,filename
            try {
                if(check==1){
                    filename = `ENVOICE_Audit_${moment(new Date()).format('DDMMYYYY')}${moment(new Date()).format('HHmm')}`.split("/").join(".")
                }else if(check==2){
                    filename = `ENVOICE_ID_${moment(new Date()).format('DDMMYYYY')}${moment(new Date()).format('HHmm')}`.split("/").join(".")
                }else if(check==3){
                    filename = `ENVOICE_Login_Logout_${moment(new Date()).format('DDMMYYYY')}${moment(new Date()).format('HHmm')}`.split("/").join(".")
                }else {
                    filename = `ENVOICE_Profile_${moment(new Date()).format('DDMMYYYY')}${moment(new Date()).format('HHmm')}`.split("/").join(".")
                }
                filepath = config.splunkReportPath + filename + '.csv'
                //Service.ftpupload(Buffer.from(filecontent,{ encoding: 'utf8'}), filepath)
                fs.writeFileSync(filepath, filecontent)
                /*
                ftpClient.connect(ftpConfig)
                    .then(() => {
                        return ftpClient.put(Buffer.from(filecontent,{ encoding: 'utf8'}), filepath)
                    })
                    .then(() => {
                        return ftpClient.end()
                    })
                    .catch(err => {
                        logger4app.error(err.message)
                    })
                */
            } catch (err) {
                logger4app.debug(`uploadsplunkscb file ${filepath}: `, err)
                throw (err)
            } finally {

            }
        }
}
module.exports = Service