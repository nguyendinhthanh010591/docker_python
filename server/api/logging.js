"use strict"
const config = require("./config")
const logger4app = require("./logger4app")
const dbtype = config.dbtype
const dbs = require(`./${dbtype}/dbs`)
const moment = require("moment")
//Ham lấy IP truy cập đến
const getCallerIP = (request) => {
    let ip = request.headers['x-forwarded-for'] || request.connection.remoteAddress || request.socket.remoteAddress || request.connection.socket.remoteAddress
    ip = ip.split(',')[0]
    ip = ip.split(':').slice(-1); //in case the ip returned in a format: "::ffff:146.xxx.xxx.xxx"
    return ip[0]
}
const funcName = (fncid) => {
    let result = config.FUNCNAME_LOGGING.find(item => item.id === fncid)
    return result.value
}
const funcEnableLog = (fncid) => {
    try {
        let result = config.FUNCNAME_LOGGING.find(item => item.id === fncid)
        return result.logenable
    }
    catch (err) {
        return "N"
    }
}
const funcExtFld = (fncid) => {
    let result = config.FUNCNAME_LOGGING.find(item => item.id === fncid)
    return result.extfld
}
const Service = {
    ins: async (req, sSysLogs, next) => {
        try {
            if (config.LOGGING_AUDIT_OPT == "0") {
                return
            }
            const iplogin = getCallerIP(req)
            let uid = sSysLogs.uid, token
            if (sSysLogs.fnc_id != 'user_login'){
                const sec = require("./sec")
                token = sec.decode(req)
                uid = token.uid
            }
            
            const fnc_log_enable = funcEnableLog(sSysLogs.fnc_id)
            
            if (fnc_log_enable != "Y") {
                return
            }

            const doc = typeof sSysLogs.doc != "undefined" ? sSysLogs.doc : null, msg_id = typeof sSysLogs.msg_id != "undefined" ? sSysLogs.msg_id : null, fnc_id = sSysLogs.fnc_id, fnc_name = funcName(sSysLogs.fnc_id), fnc_url = req.originalUrl, action = req.path, src = sSysLogs.src, dtl = sSysLogs.dtl, r1 = "", r2 = "", r3 = "", r4 = ""
            let  user_name = uid, rows, where, sql, result
            let dt = new Date(), extcols, extcolname = ``, extcolval = ``, binds=[], i = 0
            let docarr = JSON.parse(doc)

            switch (dbtype) {
                case "mssql":
                    where = `where id = @1`    
                    sql = `SELECT name name FROM s_user ${where}`  
                    
                    result = await dbs.query(sql, [uid])
                    rows = result.recordset
                    if (rows.length != 0) {
                        user_name = rows[0].name
                    }

                    extcols = funcExtFld(sSysLogs.fnc_id)
                    dt = moment(new Date()).format("YYYY-MM-DD HH:mm:ss")

                    binds = [dt, fnc_id, fnc_name, fnc_url, action, uid, user_name, src, dtl, msg_id, doc]

                    for (const fld of extcols) {
                        extcolname += `,[r${i+1}]`
                        extcolval += `,@${i+12}`
                        binds.push(docarr[fld])
                        i++
                    }

                    //Nếu là SCB thêm IP client
                    if (['scb'].includes(config.ent)) {
                        extcolname += `,[ip]`
                        extcolval += `,@${i+12}`
                        binds.push(iplogin)
                        i++
                    }

                    await dbs.query(`INSERT INTO [s_sys_logs] ([id],[dt],[fnc_id],[fnc_name],[fnc_url],[action],[user_id],[user_name],[src],[dtl],[msg_id],[doc]${extcolname}) VALUES (replace(replace(replace(convert(varchar, getdate(),20),'-',''),':',''),' ','') + replace(str(next value for ISEQ_SYS_LOGS, 3),' ','0'),@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11${extcolval})`, binds)

                    break
                case "mysql":
                    where = `where id = ?`    
                    sql = `SELECT name name FROM s_user ${where}`  
                    
                    result = await dbs.query(sql, [uid])
                    rows = result[0]
                    if (rows.length != 0) {
                        user_name = rows[0].name
                    }
                
                    extcols = funcExtFld(sSysLogs.fnc_id)

                    dt = moment(new Date()).format("YYYY-MM-DD HH:mm:ss")
                    binds = [dt, fnc_id, fnc_name, fnc_url, action, uid, user_name, src, dtl, msg_id, doc]
                    for (const fld of extcols) {
                        extcolname += `,r${i+1}`
                        extcolval += `,?`
                        binds.push(docarr[fld])
                        i++
                    }

                  
                    
                    await dbs.query(`INSERT INTO s_sys_logs (dt,fnc_id,fnc_name,fnc_url,action,user_id,user_name,src,dtl,msg_id,doc${extcolname}) VALUES (?,?,?,?,?,?,?,?,?,?,?${extcolval})`, binds)

                    break
                case "orcl":
                    where = `where id = :1`    
                    sql = `SELECT name "name" FROM s_user ${where}`  
                    
                    result = await dbs.query(sql, [uid])
                    rows = result.rows
                    if (rows.length != 0) {
                        user_name = rows[0].name
                    }

                    extcols = funcExtFld(sSysLogs.fnc_id)
                    binds = [dt, fnc_id, fnc_name, fnc_url, action, uid, user_name, src, dtl, msg_id, doc]
                    for (const fld of extcols) {
                        extcolname += `,r${i+1}`
                        extcolval += `,:${i+12}`
                        binds.push(docarr[fld])
                        i++
                    }

                
                    
                    await dbs.query(`INSERT INTO s_sys_logs (id,dt,fnc_id,fnc_name,fnc_url,action,user_id,user_name,src,dtl,msg_id,doc${extcolname}) VALUES (to_char(sysdate,'YYYYMMDDHH24MISS')|| lpad(ISEQ_SYS_LOGS.nextval,3,'0'),:1,:2,:3,:4,:5,:6,:7,:8,:9,:10,:11${extcolval})`, binds)

                    break
                case "pgsql":
                    where = `where id = $1`    
                    sql = `SELECT name "name" FROM s_user ${where}`  
                    
                    result = await dbs.query(sql, [uid])
                    rows = result.rows
                    if (rows.length != 0) {
                        user_name = rows[0].name
                    }
                    binds = [dt, fnc_id, fnc_name, fnc_url, action, uid, user_name, src, dtl, msg_id, doc]
                    extcols = funcExtFld(sSysLogs.fnc_id)

                    for (const fld of extcols) {
                        extcolname += `,r${i+1}`
                        extcolval += `,$${i+12}`
                        binds.push(docarr[fld])
                        i++
                    }

                    dt = moment(new Date()).format("YYYY-MM-DD HH:mm:ss")
                    
                    await dbs.query(`INSERT INTO s_sys_logs (id,dt,fnc_id,fnc_name,fnc_url,action,user_id,user_name,src,dtl,msg_id,doc${extcolname}) VALUES (nextval('ISEQ_SYS_LOGS'),$1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11${extcolval})`, binds)

                    break
            }

        } catch (err) {
            err.message = `Logging Insert: ${err.message}` 
            logger4app.debug(err)
        }
    },
    infoMSG: async (srcerrcode, lang, param = [], type) => {
        try {
            let where, sql, ret, result, binds, rows
            sql = `select src_err_code "src_err_code", usr_code "usr_code", usr_msg "usr_msg", usr_msg_ent "usr_msg_ent", type_msg "type_msg" from s_err_msg `
            ret = null
            /*
            if (type == "JSON") {
                ret = `[{"src_err_code":"${srcerrcode}","usr_code":"INV-00000","usr_msg":"","usr_msg_ent":"","type_msg":"INFO"}]`
            } else {
                ret = `INV-00000 (${srcerrcode}):`
            }
            */
            if ((srcerrcode == null) || (srcerrcode == "")) {
                return ret
            }

            where = ` where 1=1`

            switch (dbtype) {
                case "orcl":
                    where += ` and src_err_code = :1`
                    binds = [srcerrcode]
                    sql += where
                    result = await dbs.query(sql, binds)
                    rows = result.rows

                    break
                case "mysql":
                    where += ` and src_err_code = ?`
                    binds = [srcerrcode]
                    sql += where
                    result = await dbs.query(sql, binds)
                    rows = result[0]

                    break
                case "mssql":
                    where += ` and src_err_code = @1`
                    binds = [srcerrcode]
                    sql += where
                    result = await dbs.query(sql, binds)
                    rows = result.recordset

                    break
                case "pgsql":
                    where += ` and src_err_code = :1`
                    binds = [srcerrcode]
                    sql += where
                    result = await dbs.query(sql, binds)
                    rows = result.rows

                    break
            }

            if (rows.length == 0) {
                return ret
            } else {
                let inv_err_msg = rows[0].usr_msg
                if (param != null) {
                    let i = 0
                    for (const p of param) {
                        const strrep = "${p" + i + "}"
                        inv_err_msg = inv_err_msg.replace(strrep, p)
                        i++
                    }
                    rows[0].usr_msg = inv_err_msg
                }
            }
            if (type == "JSON") {
                ret = JSON.stringify(rows)
            } else {
                if (lang == null || lang == "vi") {
                    ret = `${rows[0].usr_code} (${srcerrcode}): ${rows[0].usr_msg}`
                }
                else {
                    ret = `${rows[0].usr_code} (${srcerrcode}): ${rows[0].usr_msg_ent}`
                }

            }

            return ret
        } catch (err) {
            logger4app.debug(err)
            throw err
        }
    },
    infoMSGExp: async (srcerrcode, srcerrmsg, lang, param = []) => {

        try {
            let msginfo, tmpsrcmsg, typeinfo
            if (srcerrcode != null) {
                if (srcerrcode.indexOf(`INV`) > 0) {
                    tmpsrcmsg = srcerrcode.substring(1, 10)
                    typeinfo = `USRINFO`
                } else {
                    tmpsrcmsg = srcerrcode
                    typeinfo = `SYSERR`
                }
            }

            msginfo = await Service.infoMSG(tmpsrcmsg, lang, param, `JSON`)
            if (msginfo != null) {
                let vJSON = JSON.parse(msginfo)
                if (typeinfo = `USRINFO`) {
                    srcerrcode = `${vJSON[0].usr_code}: ${vJSON[0].usr_msg}`
                } else if (typeinfo = `SYSERR`) {
                    srcerrcode = `${vJSON[0].usr_msg} (${srcerrmsg})`
                }
            } else {
                srcerrcode = `${srcerrcode} (${srcerrmsg})`
            }

            return srcerrcode
        } catch (err) {
            logger4app.debug(err)
            throw err
        }

    }

}
module.exports = Service