"use strict"
const axios = require("axios")
const util = require("./util")
const config = require("./config")
const moment = require("moment")
const dtf = config.dtf
axios.defaults.baseURL = config.hsm_url 
axios.defaults.headers.common["Authorization"] = config.hsm_auth
axios.defaults.headers.post["Content-Type"] = "application/json"
exports.xml = async (rows,type) => {
    let arr = []
    for (let row of rows) {
        let doc = util.isObject(row.doc) ? row.doc : util.parseJson(row.doc), taxc = doc.stax,xml
        doc.adt = moment().format(dtf)
        try {
            console.log(`Bat dau ky so cua hoa don`)
            if (config.ent == 'ssi') {
                doc.totalv = util.isNumber(doc.totalv) ? Math.round(doc.totalv) : doc.totalv
                doc.sumv = util.isNumber(doc.sumv) ? Math.round(doc.sumv) : doc.sumv
                doc.vatv = util.isNumber(doc.vatv) ? Math.round(doc.vatv) : doc.vatv
            }
            const inv = require(`./inv`)
            if(type =="HD") xml = await util.j2x( await inv.getcol(doc), row.id)
            else xml = await util.j2x( doc,row.id)
            let signSample = await util.getSignSample(doc.type, config.DEGREE_CONFIG == "123" ? doc.adt : doc.idt, row.id),str = xml ,xml_sign_time = signSample.signingTime
            , xml_end_tag_replace = signSample.xmlEndTagReplace
            , object_signing_time = `${xml_sign_time}${xml_end_tag_replace}`,dat
            str = str.replace(xml_end_tag_replace, object_signing_time)
            // let dat = { signerType: "HSM", taxCode: taxc, systemCode: config.hsm_sys_code , xmlData: str }

            if(type=="HD"){
                str = str.replace(`<DLHDon>`,`<DLHDon Id="${row.id}">`)               
                dat = {
                id: `${row.id}`,
                signerType: "HSM",
                taxCode: taxc,
                systemCode: config.hsm_sys_code,
                idObject: `SigningTime-NBan-${row.id}`,
                tagSign:"DLHDon",
                tagEnd:"</DLHDon>",
                xmlData: `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>${str}`
            }}
            if(type=="BTH"){
                str = str.replace(`<DLBTHop>`,`<DLBTHop Id="${row.id}">`)
                dat={
                id: `${row.id}`,
                signerType: "HSM",
                taxCode: taxc,
                systemCode: config.hsm_sys_code,
                idObject: `SigningTime-NNT-${row.id}`,
                tagSign:"DLBTHop",
                tagEnd:"</DLBTHop>",
                xmlData: `<?xml version="1.0" encoding="UTF-8" standalone="yes"?>${str}`
            }}
            if(type=="TK"){
                str = str.replace(`<DLTKhai>`,`<DLTKhai Id="${row.id}">`)
                dat = {
                id: `${row.id}`,
                signerType: "HSM",
                taxCode: taxc,
                systemCode: config.hsm_sys_code,
                idObject: `SigningTime-NNT-${row.id}`,
                tagSign:"DLTKhai",
                tagEnd:"</DLTKhai>",
                xmlData: `<?xml version="1.0" encoding="UTF-8"?>${str}`
            }}
            const { data } = await axios({ method: "POST", data: dat })
            const errors = data.errors
            if (errors && errors.length > 0) throw new Error(errors[0])
            row.xml = data.signedXmlData
            row.xml = row.xml.replace(`<?xml version="1.0" encoding="UTF-8"?>`,``)
            row.xml = row.xml.replace(`<?xml version="1.0" encoding="UTF-8" standalone="no"?>`,``)

            console.log(`Ket thuc ky so cua hoa don ${taxc} - ${doc.form} - ${doc.serial} - ${doc.id} - Trang thai thanh cong `)
            arr.push(row)
        } catch (err) {
            console.trace(err)
            console.log(`Thuc hien ky so cua hoa don ${taxc} - ${doc.form} - ${doc.serial} - ${doc.id} - Trang thai that bai - nguyen nhan ${JSON.stringify(err)} `)
        }
    }
    return arr
}    
exports.signxml = async (stax, xml) => {
    let rxml
    const taxc = stax, dat = { signerType: "HSM", taxCode: taxc, systemCode: config.hsm_sys_code , xmlData: xml }
    try {
        console.log(`Bat dau ki so hoa don ${String(xml).substr(0,150)} - trang thai dang ky`)
        const { data } = await axios({ method: "POST", data: dat })
        const errors = data.errors
        if (errors && errors.lenght > 0) throw new Error(errors[0])
        rxml = data.signedXmlData
        console.log(`Ket thuc ki so hoa don ${String(xml).substr(0,150)} - trang thai thanh cong`)

    } catch (err) {
        console.log(`Thuc hien ki so hoa don ${String(xml).substr(0,150)} trang thai loi - nguyen nhan ${err}`)
        console.error(err)
    }

    return rxml
}  