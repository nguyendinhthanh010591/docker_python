"use strict"
const { Client, Change, Attribute } = require('ldapts')
const crypto = require("crypto")
const config = require("./config")
const logger4app = require("./logger4app")
const rsmq = require("./rsmq")
const handlebars = require("handlebars")
const user = require(`./${config.dbtype}/user`)
const AD = (config.ldap === "AD")
const LP = (config.ldapPrivate === 1)
const islocal = (config.is_use_local_user === 1)
const util = require("./util")
const inc = require("./inc")
const decrypt = require("./encrypt")
const encodePassword = (pwd) => {
    return Buffer.from('"' + pwd + '"', "utf16le").toString()
}
const generatePwd = () => {
    const len = 6, pwd = crypto.randomBytes(Math.ceil(len / 2)).toString("hex").slice(0, len)
    return `Z1@${pwd}`
}
const modify = async (dn, change) => {
    let client
    try {
        let adminPW = JSON.parse(JSON.stringify(config.adminPW))
        adminPW = decrypt.decrypt(adminPW)
        client = new Client(config.ldapsConfig)
        await client.bind(config.adminDN, adminPW)
        await client.modify(dn, change)
    }
    catch (err) {
        throw err
    }
    finally {
        await client.unbind()
    }
}
const createUser = async (uid, mail, next) => {
    let client
    try {
        const pwd = generatePwd(), person = { objectClass: "inetOrgPerson", mail: mail }, tag = AD ? "cn" : "uid", dn = `${tag}=${uid},${config.baseDN}`
        if (!islocal) {
            let adminPW = JSON.parse(JSON.stringify(config.adminPW))
            adminPW = decrypt.decrypt(adminPW)
            try {
                client = new Client(config.ldapsConfig)
                await client.bind(config.adminDN, adminPW)
                if (AD) {
                    person.sAMAccountName = uid
                    person.distinguishedName = dn
                    person.unicodePwd = encodePassword(pwd)
                    person.userAccountControl = "512"
                }
                else {
                    person.cn = uid
                    person.sn = uid
                    person.userPassword = pwd
                }
                await client.add(dn, person)
                let html
                try {
                    const source = await util.tempmail(1)
                    const template = handlebars.compile(source)
                    const obj = { uid: uid, pwd: pwd }
                    html = template(obj)
                } catch (error) {
                    html = `Mật khẩu đăng nhập hệ thống hóa đơn điện tử <br>- Tài khoản : ${uid}<br>- Mật khẩu: ${pwd}`
                }
                //const html = `Mật khẩu đăng nhập hệ thống hóa đơn điện tử <br>- Tài khoản : ${uid}<br>- Mật khẩu: ${pwd}`
                const content = { from: config.mail, to: mail, subject: `Hóa đơn điện tử mật khẩu đăng nhập ${uid}`, html: html }
                if (config.dbCache)
                    await inc.inserttempmail(content)
                else
                    await rsmq.sendMessageAsync({ qname: config.qname, message: JSON.stringify(content) })
            }
            catch (err) {
                if (err.code == 68 || err.code == 44) throw new Error(`Tài khoản ${uid} đã tồn tại \n (Account ${uid} already exists )`)
                throw err
            }
            finally {
                await client.unbind()
            }
        }
        
        return next(null, pwd)
    }
    catch (err) {
        throw err
    }
}
const Service = {
    change_pass_reminder : async (uid, mail, days_left) => {
        try {
            let html, obj = {uid: uid, days_left: days_left}
            html = `Chào {{uid}}, <br> <br>Mật khẩu account {{uid}} sẽ bị hết hạn trong {{days_left}} ngày. Hãy thay đổi mật khẩu của bạn ngay khi có thể. <br> <br>Xin cảm ơn. <br> <br>Đây là mail tự động gửi từ hệ thống, vui lòng không reply lại.`
            const source = html
            const template = handlebars.compile(source)
            html = template(obj)

            const content = { from: config.mail, to: mail, subject: `Thông báo mật khẩu sắp hết hạn`, html: html }
            if (config.dbCache)
                await inc.inserttempmail(content)
            else
                await rsmq.sendMessageAsync({ qname: config.qname, message: JSON.stringify(content) })
        }
        catch (err) {
            throw err
        }
    },
    search: async (filter) => {
        let client
        try {
            let adminPW = JSON.parse(JSON.stringify(config.adminPW))
            adminPW = decrypt.decrypt(adminPW)
            client = new Client(config.ldapsConfig)
            await client.bind(config.adminDN, adminPW)
            const result = await client.search(config.baseDN, filter)
            return result.searchEntries
        }
        catch (err) {
            throw err
        }
        finally {
            await client.unbind()
        }
    },
    add0: async (req, res, next) => {
        try {
            const body = req.body
            const id = body.id,ou = body.ou,uid = AD ? "sAMAccountName" : "uid", filter = `(${uid}=${id})`
            let result, row, mail, cn=""
            if (!(config.user_adfs || config.useAzureAD)) {
                result = await Service.search({ filter: filter, scope: "sub", attributes: ["cn", "mail", "displayName"], sizeLimit: 1 })
                if (result.length == 0) throw new Error(`Tài khoản ${id} không tìm thấy trong LDAP \n (Account ${id} couldn't be found)`)
                row = result[0]
                mail = row.mail
                cn = row.cn
            
                if (["aia"].includes(config.ent)) cn = row.displayName
            }
            else {
                mail = ""
                cn = id
            }
            const obj = { id: id, name: cn, mail: (mail.length > 0 ? mail : null), pos: null, ou: ou, code: null, local:0 }
            const rs = await user.ins(req, obj)
            if (rs > 0) res.send(`Đã tạo tài khoản ${id} (Account was created)`)
            else res.send(`Không tạo được tài khoản ${id} (Can't create an account)`)
        }
        catch (err) {
            next(err)
        }
    },
    add1: async (req, res, next) => {
        try {
            const body = req.body, name = body.name, mail = body.mail, pos = body.pos, ou = body.ou
            let id = body.id
            id = id.replace(/\s+/g, '');
            await createUser(id, mail,(err,jpass)=>{
                if (err) return next(err)
            })
            const obj = { id: id, name: name, mail: mail, pos: pos, ou: ou, code: body.code }
            const rs = await user.ins(req, obj)
            if (rs > 0) res.send(`Đã tạo tài khoản ${id} (Account was created)`)
            else res.send(`Không tạo được tài khoản ${id} (Can't create an account)`)
        }
        catch (err) {
            next(err)
        }
    },
    add2: async (req, res, next) => {
        try {
            const body = req.body, name = body.name, mail = body.mail, pos = body.pos, ou = body.ou/*, local = body.local*/
            let id = body.id
            let pass
            id = id.replace(/\s+/g, '');
            await createUser(id, mail,(err,jpass)=>{
                if (err) return next(err)
                pass = jpass
            })
            // ma hoa base64 truoc luu
            let buff = new Buffer(pass);
            let base64data = buff.toString('base64');
            const obj = { id: id, name: name, mail: mail, pos: pos, ou: ou, code: body.code, local: 1, pass: base64data}
            const rs = await user.ins(req, obj)
            let html, uid = id, pwd = pass
            if (config.ent=="apple") {html = `Mật khẩu đăng nhập hệ thống hóa đơn điện tử <br>- Tài khoản : ${uid}<br>- Mật khẩu: ${pwd}`}
            else{
                try {
                    const source = await util.tempmail(1)
                    const template = handlebars.compile(source)
                    const obj = { uid: uid, pwd: pwd }
                    html = template(obj)
                } catch (error) {
                    html = `Mật khẩu đăng nhập hệ thống hóa đơn điện tử <br>- Tài khoản : ${uid}<br>- Mật khẩu: ${pwd}`
                }
            }
            //const html = `Mật khẩu đăng nhập hệ thống hóa đơn điện tử <br>- Tài khoản : ${uid}<br>- Mật khẩu: ${pwd}`
            const content = { from: config.mail, to: mail, subject: `Hóa đơn điện tử mật khẩu đăng nhập ${uid}`, html: html }
            if (config.dbCache)
                await inc.inserttempmail(content)
            else
                await rsmq.sendMessageAsync({ qname: config.qname, message: JSON.stringify(content) })
            if (rs > 0) res.send(`Đã tạo tài khoản ${id} (Account was created)`)
            else res.send(`Không tạo được tài khoản ${id} (Can't create an account)`)
        }
        catch (err) {
            next(err)
        }
    },
    mail: async (req, res, next) => {
        try {
            if (!LP) throw new Error('Tính năng chỉ dùng trên LDAP riêng \n (Features are for use only above LDAP)')
            const body = req.body, uid = body.uid, mail = body.mail, tag = AD ? "cn" : "uid", dn = `${tag}=${uid},${config.baseDN}`
            const change = new Change({ operation: "replace", modification: new Attribute({ type: "mail", values: [mail] }) })
            await modify(dn, change)
            res.json(1)
        } catch (err) {
            next(err)
        }
    },
    pwd: async (req, res, next) => {
        let client
        try {
            if (!(LP || islocal)) throw new Error('Tính năng chỉ dùng trên LDAP riêng hoặc thông tin NSD lưu trong CSDL \n (This feature is only used on private LDAP or user information stored in the database)')
            const body = req.body, uid = body.uid, oldpwd = body.oldpwd, newpwd = body.newpwd, tag = AD ? "cn" : "uid", dn = `${tag}=${uid},${config.baseDN}`
            let a1, a2
            let u = await user.obid(uid)
            if (u && u.local) {
                await user.updatelocaluserpass(uid, Buffer.from(oldpwd, 'utf8').toString('base64'), Buffer.from(newpwd, 'utf8').toString('base64'))
            } else {
                if (AD) {
                    a1 = { type: "unicodePwd", values: [encodePassword(oldpwd)] }
                    a2 = { type: "unicodePwd", values: [encodePassword(newpwd)] }
                }
                else {
                    a1 = { type: "userPassword", values: [oldpwd] }
                    a2 = { type: "userPassword", values: [newpwd] }
                }
                try {
                    const c1 = new Change({ operation: "delete", modification: new Attribute(a1) })
                    const c2 = new Change({ operation: "add", modification: new Attribute(a2) })
                    client = new Client(config.ldapsConfig)
                    await client.bind(dn, oldpwd)
                    await client.modify(dn, [c1, c2])
                }
                catch (err) {
                    if (err.code == 49) next(new Error("Mật khẩu cũ không đúng \n (Old password is incorrect)"))
                    else next(err)
                }
                finally {
                    if (client) await client.unbind()
                }
            }
            res.send(`Mật khẩu của tài khoản ${uid} đã được thay đổi (The account's password ${uid} has been changed)`)
        }
        catch (err) {
            next(err)
        }
    },
    reset: async (req, res, next) => {
        let client
        try {
            if (!(LP || islocal)) throw new Error('Tính năng chỉ dùng trên LDAP riêng hoặc thông tin NSD lưu trong CSDL \n (This feature is only used on private LDAP or user information stored in the database)')
            const body = req.body, username = body.username
            let u = await user.obid(username)
            if (!u) throw new Error(`Tài khoản ${username} không tồn tại \n (Account ${username} does not exist)`)
            if (!u.mail) throw new Error(`Tài khoản ${username} không tồn tại mail\n (Mail account ${username} does not exist)`)
            const mail = u.mail;
            const uid = AD ? "sAMAccountName" : "uid", filter = `(&(${uid}=${username})(mail=${mail}))`
            const where = { filter: filter, scope: "sub", attributes: ["dn"], sizeLimit: 1 }
            const pwd = generatePwd()
            
            if (u && u.local) {
                await user.resetlocaluserpass(username, mail, Buffer.from(pwd, 'utf8').toString('base64'))
            } else {
                try {
                    let adminPW = JSON.parse(JSON.stringify(config.adminPW))
                    adminPW = decrypt.decrypt(adminPW)
                    client = new Client(config.ldapsConfig)
                    await client.bind(config.adminDN, adminPW)
                    const result = await client.search(config.baseDN, where)
                    if (!result.searchEntries.length) throw new Error(`Tài khoản ${username} Mail ${mail} không tồn tại \n (Account ${username} Mail ${mail} does not exist)`)
                    const dn = result.searchEntries[0].dn
                    const atr = AD ? { type: "unicodePwd", values: [encodePassword(pwd)] } : { type: "userPassword", values: [pwd] }
                    const change = new Change({ operation: "replace", modification: new Attribute(atr) })
                    await client.modify(dn, change)
                }
                catch (err) {
                    throw err
                }
                finally {
                    if (client) await client.unbind()
                }
            }
            const source = await util.tempmail(5)
            const template = handlebars.compile(source)
            const obj = { uid: username, pwd: pwd }
            let html = template(obj)
            const content = { from: config.mail, to: mail, subject: `Hóa đơn điện tử đặt lại mật khẩu đăng nhập ${username}`, html: html }
            if (config.dbCache)
                await inc.inserttempmail(content)
            else
                await rsmq.sendMessageAsync({ qname: config.qname, message: JSON.stringify(content) })
            res.send(`Mật khẩu của tài khoản ${username} đã được gửi đến ${mail} (Account password ${username} has been sent to ${mail})`)
        }
        catch (err) {
            next(err)
        }

    },
    getUserFromOUD: async () => {
        const email = require("./email")
        try {
            let client, arruseroud = [], entries
            if (config.test_sms_fpt) {
                //Môi trường FPT
                try {
                    logger4app.debug(`Begin run method getUserFromOUD`)
                    let adminPW = JSON.parse(JSON.stringify(config.adminPW))
                    adminPW = decrypt.decrypt(adminPW)
                    //Tạo filter
                    let filter = config.searchUsersFilter

                    logger4app.debug(`filter : ${filter}`)

                    client = new Client(config.ldapsConfig)
                    logger4app.debug(`config.ldapsConfig : ${JSON.stringify(config.ldapsConfig)}`)
                    await client.bind(config.adminDN, adminPW)
                    const where = { filter: filter, scope: "sub", attributes: config.ldapattributes }
                    logger4app.debug(`where : ${JSON.stringify(where)}`)

                    logger4app.debug(`config.baseDNUsersFilter : ${JSON.stringify(config.baseDNUsersFilter)}`)
                    //Query OUD lấy danh sách user
                    const result = await client.search(config.baseDNUsersFilter, where)
                    logger4app.debug(`searchEntries: ${JSON.stringify(result.searchEntries)}`)
                    //Lặp danh sách lấy từ OUD
                    entries = result.searchEntries
                    if (!(entries && entries.length > 0)) return
                }
                catch (err) {
                    logger4app.debug(err)
                    throw err
                }
                finally {
                    await client.unbind()
                }
            } else {
                //Môi trường SCB
                logger4app.debug(`Begin run method getUserFromOUD`)
                let adminPW = JSON.parse(JSON.stringify(config.adminPW))
                adminPW = decrypt.decrypt(adminPW)
                //Tạo filter
                let filter = ""

                let grpnames = await user.groupAll(), vsubfilter = ``, entriestmp = [], entriesuid = []
                for (let grpname of grpnames) {
                    try {
                        vsubfilter = `(ismemberof=cn=${grpname.name},ou=einvoice,ou=apps,o=standardchartered)`

                        filter = `(&(&(objectclass=scbperson)(|${vsubfilter})))`

                        logger4app.debug(`filter : ${filter}`)

                        client = new Client(config.ldapsConfig)
                        logger4app.debug(`config.ldapsConfig : ${JSON.stringify(config.ldapsConfig)}`)
                        await client.bind(config.adminDN, adminPW)
                        const where = { filter: filter, scope: "sub", attributes: config.ldapattributes }
                        logger4app.debug(`where : ${JSON.stringify(where)}`)

                        logger4app.debug(`config.baseDNUsersFilter : ${JSON.stringify(config.baseDNUsersFilter)}`)
                        //Query OUD lấy danh sách user
                        const result = await client.search(config.baseDNUsersFilter, where)
                        logger4app.debug(`searchEntries: ${JSON.stringify(result.searchEntries)}`)
                        //Lặp danh sách lấy từ OUD
                        entriestmp = result.searchEntries
                    }
                    catch (err) {
                        logger4app.debug(err)
                        throw err
                    }
                    finally {
                        await client.unbind()
                    }
                    for (let row of entriestmp) {
                        if (!entriesuid.includes(row.cn)) {
                            entriesuid.push(row.cn)
                            entries.push(row)
                        }
                    }
                }
                if (!(entries && entries.length > 0)) return
            }

            for (let row of entries) {
                //Tạo thông tin user để update DB, một số giá trị fix => điền vào sau
                let mail = row.mail, name = (row.fullName && row.fullName.length > 0) ? row.fullName : row.cn, cn = row.cn, ou = 1, pos = (row.employeeType && row.employeeType.length > 0) ? row.employeeType : null
                //Query tiếp để lấy thuộc tính ismemberof
                const filter = `(cn=${cn})`
                const ruser = await Service.search({ filter: filter, scope: "sub", attributes: ["cn", "mail", "displayName", "ismemberof"], sizeLimit: 1 })
                logger4app.debug(`ruser ${JSON.stringify(ruser)}`)            
                //Lấy danh sách group từ thuộc tính ismemberof
                let mbr = [], usergroup = []
                if (ruser && ruser.length > 0 && ruser[0].ismemberof) {
                    if (!Array.isArray(ruser[0].ismemberof))
                        mbr.push(ruser[0].ismemberof)
                    else
                        mbr = ruser[0].ismemberof
                    logger4app.debug(`mbr ${mbr}`)                       
                    for (let m of mbr) {
                        let gr = m.split(',')[0].split('=')[1], vcheckinvoice = m.split(',')[1].split('=')[1]
                        logger4app.debug(`gr ${gr}`)      
                        logger4app.debug(`vcheckinvoice ${vcheckinvoice}`)                     
                        if (vcheckinvoice == 'einvoice') usergroup.push(gr)
                    }
                }
                //Gán lại ou tương ứng
                ou = 1
                const obj = { id: cn, name: name, mail: (mail.length > 0 ? mail : null), pos: pos, ou: ou, code: cn, usergroup: usergroup }
                logger4app.debug(`obj ${JSON.stringify(obj)}`)
                //Đưa vào mảng tạm
                arruseroud.push(obj)
            }

            //Xử lý dữ liệu mảng tạm
            await user.getUserFromOUD(arruseroud)
            logger4app.debug(`End run method getUserFromOUD`)
            await email.sendAdminMail('', 'getUserFromOUD_Succ')
        } catch (err) {
            logger4app.debug(err)
            await email.sendAdminMail(err, 'getUserFromOUD_Err')
        }
    },
    cks_expired_remind: async (ou, days_left) => {
        try {
            let html,subject, obj = {ou_name: ou.name, days_left: days_left, ou_mst: ou.mst, dsignissuser: ou.dsignissuser }
            html = `Chào {{ou_name}}, <br> <br>CKS {{dsignissuser}} của quý khách sẽ bị hết hạn trong {{days_left}} ngày . <br> Vui lòng cập nhật!`
            subject=`Thông báo CKS sắp hết hạn`
            const source = html
            const template = handlebars.compile(source)
            html = template(obj)

            const content = { from: config.mail, to: ou.receivermail, subject: subject, html: html }
            
            if (config.dbCache)
                await inc.inserttempmail(content)
            else
                await rsmq.sendMessageAsync({ qname: config.qname, message: JSON.stringify(content)})
        }
        catch (err) {
            throw err
        }
    },
    send_noti_serial_limit_apple: async (ouse) => {
        try {
            let html,subject, obj = {ou_name: ouse.org.name, serial: ouse.serial }
            html = `Dear {{ou_name}}, <br/> e-Invoice number range {{serial}} has reached 80% utilisation. <br> Please apply for additional series, if needed, and update on FIS portal.`
            subject=`<${ouse.org?ouse.org.name: ''}> e-Invoice Number Range Usage at 80%`
            const source = html
            const template = handlebars.compile(source)
            html = template(obj)

            const content = { from: config.mail, to: ouse.receivermail, subject: subject, html: html }
            
            if (config.dbCache)
                await inc.inserttempmail(content)
            else
                await rsmq.sendMessageAsync({ qname: config.qname, message: JSON.stringify(content)})
        }
        catch (err) {
            throw err
        }
    },
}
module.exports = Service