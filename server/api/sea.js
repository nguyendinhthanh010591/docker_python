"use strict"
const moment = require("moment")
const bcrypt = require("bcryptjs")
const jwt = require("jsonwebtoken")
const config = require("./config")
const logger4app = require("./logger4app")
const util = require("./util")
const inc = require("./inc")
const ous = require("./ous")
const rsmq = require("./rsmq")
const sec = require("./sec")
const inv = require(`./inv`)
const handlebars = require("handlebars")
const ext = require("./ext")
const seasecret = config.seasecret
const seaexpires = config.seaexpires
const mfd = config.mfd
const ENT = config.ent
const rbi = (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            let doc = await inv.invdocbid(id)
            resolve(doc)
        } catch (err) {
            reject(err)
        }
    })
}
const sql_xlssearch = {
    sql_xlssearch_mssql: `sec,pid,cid,id,FORMAT(idt,'dd/MM/yyyy') idt,case when status = 3 then N'Đã duyệt' when status = 4 then N'Đã hủy' end status,type,form ,serial,seq,stax,sname,saddr,stel,smail,sacc,sbank,btax,bname,buyer,baddr,btel,bmail,bacc,bbank,note,sumv,vatv,totalv,adjdes,cde,status_received`,
    sql_xlssearch_orcl: `sec "sec",pid "pid",cid "cid",id "id",TO_CHAR(idt,'${mfd}') "idt",DECODE(status,3,'Đã duyệt',4,'Đã hủy') "status",type "type",form "form",serial "serial",seq "seq",stax "stax",sname "sname",saddr "saddr",stel "stel",smail "smail",sacc "sacc",sbank "sbank",btax "btax",bname "bname",buyer "buyer",baddr "baddr",btel "btel",bmail "bmail",bacc "bacc",bbank "bbank",note "note",sumv "sumv",vatv "vatv",totalv "totalv",adjdes "adjdes",cde "cde",status_received "status_received"`,
    sql_xlssearch_mysql: `sec "sec",pid "pid",cid "cid",id "id",DATE_FORMAT(idt,'%d/%m/%Y') "idt",
    case
        when status = 3 then 'Đã duyệt'
        when status = 4 then 'Đã hủy'
    end "status",type "type",form "form",serial "serial",seq "seq",stax "stax",sname "sname",saddr "saddr",stel "stel",smail "smail",sacc "sacc",sbank "sbank",btax "btax",bname "bname",buyer "buyer",baddr "baddr",btel "btel",bmail "bmail",bacc "bacc",bbank "bbank",note "note",sumv "sumv",vatv "vatv",totalv "totalv",adjdes "adjdes",cde "cde", status_received "status_received"`,
    sql_xlssearch_pgsql: `sec,pid,cid,id,to_char(idt,'DD/MM/YYYY') idt,case when status = 3 then 'Đã duyệt' when status = 4 then 'Đã hủy' end status,type,form ,serial,seq,stax,sname,saddr,stel,smail,sacc,sbank,btax,bname,buyer,baddr,btel,bmail,bacc,bbank,note,sumv,vatv,totalv,adjdes,cde,status_received`
}
const Service = {
    login: async (req, res, next) => {
        try {
            await util.chkCaptcha(req)
            const query = req.query, taxc = query.username
            const result = await inc.execsqlselect(`select pwd "pwd" from s_org where taxc=? and pwd is not null`, [taxc])
            const rows = result
            if (rows.length == 0) throw new Error(`Không tìm thấy tài khoản ${taxc} \n (Cannot found the account ${taxc})`)
            const pwd = rows[0].pwd
            if (!bcrypt.compareSync(query.password, pwd)) throw new Error(`Mật khẩu không đúng \n (Incorrect password) `)
            const json = { taxc: taxc }
            jwt.sign(json, seasecret, { expiresIn: seaexpires }, (err, token) => {
                if (err) return next(err)
                json.token = token
                json.expires = new Date(Date.now() + seaexpires * 1000)
                return res.json(json)
            })
        } catch (err) {
            next(err)
        }
    },
    getou: async (req, res, next) => {
        try {
            const token = sec.decode(req)

            const sql = `select id "id",mst "mst",code "code", name "value" from s_ou  where pid in (select id from s_ou where  pid is null)`
            const result = await inc.execsqlselect(sql, [])
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    getbranch: async (req, res, next) => {
        try {
            const  taxc = req.query.taxc
           // or id=@1
            const sql = `select id "id",mst "mst",code "code", name "value" from s_ou  where pid = (select id from s_ou where taxc =?) or taxc=?`
            const result = await inc.execsqlselect(sql, [taxc])
            res.json(result)
           
        }
        catch (err) {
            next(err)
        }
    },
    change: async (req, res, next) => {
        try {
            const token = sec.decode(req), taxc = token.taxc, query = req.query
            let result, rows, pwd
            result = await inc.execsqlselect(`select pwd "pwd" from s_org where taxc=@1 and pwd is not null`, [taxc])
            rows = result
            if (rows.length == 0) throw new Error(`Không tìm thấy tài khoản ${taxc} \n (Cannot found the account ${taxc})`)
            pwd = rows[0].pwd
            if (!bcrypt.compareSync(query.pwd1, pwd)) throw new Error(`Mật khẩu cũ không đúng \n (Old password is incorrect)`)
            pwd = util.bcryptpwd(query.pwd2)
            result = await inc.execsqlinsupddel(`update s_org set pwd=? where taxc=?`, [pwd, taxc])
            res.json(result)
        } catch (err) {
            next(err)
        }
    },
    reset: async (req, res, next) => {
        try {
            await util.chkCaptcha(req)
            const query = req.query, taxc = query.taxc.trim(), mail = query.mail.trim()
            let result, rows
            result = await inc.execsqlselect(`select mail "mail" from s_org where taxc=? and lower(mail) like ?`, [taxc, `%${mail.toLowerCase()}%`])
            rows = result
            if (rows.length == 0) throw new Error(`Tài khoản ${taxc} Mail ${mail} không tồn tại \n (Account ${taxc} Mail ${mail} does not exist)`)
            const pwd = util.pwd()
            await inc.execsqlinsupddel("update s_org set pwd=? where taxc=?", [util.bcryptpwd(pwd), taxc])
            let content
            if (ENT == "vcm") { 
                const subject = "Mật khẩu đăng nhập hệ thống hóa đơn điện tử"
                const html = `Kính gửi Quý khách hàng,<br>Xin trân trọng cảm ơn Quý khách hàng đã sử dụng dịch vụ của chúng tôi.<br>Tài khoản để tra cứu của quý khách như sau:<br> - Tên đăng nhập: ${taxc} <br> - Mật khẩu: ${pwd} <br>Quý khách cũng có thể sử dụng tài khoản của mình đăng nhập vào https://hoadon.winmart.vn để xem, kiểm tra và quản lý tất cả các hóa đơn đã phát hành cho Quý khách.`
                content = { sender: config.mail, from: config.mail, to: mail, subject: subject, html: html }
            }
            else {
                const subject = "Mật khẩu đăng nhập hệ thống hóa đơn điện tử"
                let html
                try {
                    const source = await util.tempmail(2)
                    const template = handlebars.compile(source)
                    const obj = { taxc: taxc, pwd: pwd }
                    html = template(obj)
                } catch (error) {
                    html = `- Tài khoản : ${taxc}<br>- Mật khẩu: ${pwd}`
                }
                //const html = `- Tài khoản : ${taxc}<br>- Mật khẩu: ${pwd}`
                content = { sender: config.mail, from: config.mail, to: mail, subject: subject, html: html }
            }
            if (config.dbCache)
                await inc.inserttempmail(content)
            else
                await rsmq.sendMessageAsync({ qname: config.qname, message: JSON.stringify(content) })
            res.send(`Mật khẩu của tài khoản ${taxc} đã được gửi đến ${mail} \n (The account's password ${taxc} have been sent ${mail})`)
        } catch (err) {
            next(err)
        }
    },
    xml0: async (req, res, next) => {
        try {
            const token = sec.decode(req), id = req.params.id
            const result = await inc.execsqlselect(`select xml "xml" from s_inv where id=? and btax=? and status in (3,4)`, [id, token.taxc])
            const rows = result
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn ${id} \n (No invoice found ${id})`)
            res.json({ xml: rows[0].xml })
        }
        catch (err) {
            next(err)
        }
    },
    search0: async (req, res, next) => {
        try {
            const token = sec.decode(req), id = req.params.id
            const result = await inc.execsqlselect(`select pid "pid",cid "cid",cde "cde",adjtyp "adjtyp",adjdes "adjdes",status "status", status_received "status_received",doc "doc",xml "xml" from s_inv where id=? and btax=? and status in (3,4)`, [id, token.taxc])
            const rows = result
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn ${id} \n (No invoice found ${id})`)
            const row = rows[0], doc = JSON.parse(row.doc), org = await ous.obt(doc.stax), idx = org.temp
            const form = doc.form, type = doc.type, serial = doc.serial
            const fn = form.length > 1 ? util.fn(form, idx) : `${type}.${form}.${serial}.${idx}`
            const tmp = await util.template(fn)
            let obj ={ status: doc.status, doc: doc, tmp: tmp }
            let reqjsr = ext.createRequest(obj)
            // res.json({ status: status, doc: doc, tmp: tmp })
             const bodyBuffer = await  util.jsReportRenderAttach(reqjsr)
             let sizeInByte = bodyBuffer.byteLength  
            res.json({pdf: `data:application/pdf;base64, ${bodyBuffer.toString('base64')}`, sizePDF: sizeInByte, adjtyp: row.adjtyp, adjdes: row.adjdes, pid: row.pid, cid: row.cid, cde: row.cde, status: row.status, status_received: row.status_received })
        }
        catch (err) {
            next(err)
        }
    },
    xml1: async (req, res, next) => {
        try {
            await util.chkCaptcha(req)
            const query = req.query, sec = query.sec,ou = query.ou,branch = query.branch,form = query.form,serial = query.serial,seq = query.seq
            let result,code_ou
            if('*'==branch) code_ou = ou
            else code_ou = branch
            if (ENT == 'vcm') {
                result = await inc.execsqlselect(`select xml "xml" from s_inv where  form=? and serial=? and seq=? and c6=? and status in (3,4,6)`, [ form, serial, seq, sec])
            }
            else {
                result = await inc.execsqlselect(`select xml "xml" from s_inv where sec=? and status in (3,4)`, [sec])
            }
            const rows = result
            logger4app.debug(rows)
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn mã tra cứu: ${sec} \n (Could not find invoice for code: ${sec})`)
            res.json({ xml: rows[0].xml })
        }
        catch (err) {
            next(err)
        }
    },
    search1: async (req, res, next) => {
        try {
            await util.chkCaptcha(req)
            const query = req.query, sec = query.sec
            let code_ou
         
            const result = await inc.execsqlselect(`select id "id",pid "pid",cid "cid",cde "cde",adjtyp "adjtyp",adjdes "adjdes",status "status", status_received "status_received",doc "doc", stax "stax" from s_inv where sec=? and status in (3,4,6)`, [sec])
           
            const rows = result
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn mã tra cứu: ${sec} \n (Invoice not found for code: ${sec})`)
            const row = rows[0], doc = await rbi(row.id)
            const org = await ous.obt(doc.stax)
            const idx = org.temp
            const form = doc.form, type = doc.type, serial = doc.serial
            const fn = form.length > 1 ? util.fn(form, idx) : `${type}.${form}.${serial}.${idx}`
            const tmp = await util.template(fn)
           
            let obj ={ status: doc.status, doc: doc, tmp: tmp }
            let reqjsr = ext.createRequest(obj)
            // res.json({ status: status, doc: doc, tmp: tmp })
             const bodyBuffer = await  util.jsReportRenderAttach(reqjsr)
             let sizeInByte = bodyBuffer.byteLength   
             //res.contentType("application/pdf")    
             //res.end(bodyBuffer, "binary")
         
           //  res.json({ pdf: `data:application/pdf;base64, ${bodyBuffer.toString('base64')}`, sizePDF: sizeInByte})
           res.json({  pdf: `data:application/pdf;base64, ${bodyBuffer.toString('base64')}`, sizePDF: sizeInByte, adjtyp: row.adjtyp, adjdes: row.adjdes, pid: row.pid, cid: row.cid, cde: row.cde, status: row.status,status_received: row.status_received, ent: config.ent })
        }
        catch (err) {
            next(err)
        }
    },
    searchP: async(req,res,next) =>{
        try {
            let reqp = req 
            reqp.query = req.body
            await util.chkCaptcha(reqp)
            const query = req.body, sec = query.sec
            let code_ou
         
            const result = await inc.execsqlselect(`select id "id",pid "pid",cid "cid",cde "cde",adjtyp "adjtyp",adjdes "adjdes",status "status",doc "doc" from s_inv where sec=? and status in (3,4,6)`, [sec])
           
            const rows = result
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn mã tra cứu: ${sec} \n (Invoice not found for code: ${sec})`)
            const row = rows[0], doc = await rbi(row.id)
            const org = await ous.obt(doc.stax)
            const idx = org.temp
            const form = doc.form, type = doc.type, serial = doc.serial
            const fn = form.length > 1 ? util.fn(form, idx) : `${type}.${form}.${serial}.${idx}`
            const tmp = await util.template(fn)
            res.json({ doc: doc, tmp: tmp, adjtyp: row.adjtyp, adjdes: row.adjdes, pid: row.pid, cid: row.cid, cde: row.cde, status: row.status, ent: config.ent })
        }
        catch (err) {
            next(err)
        }
    },
    searchvcm: async (req, res, next) => {
        try {
            await util.chkCaptcha(req)
            const query = req.query, sec = query.sec,ou = query.ou,branch = query.branch,form = query.form,serial = query.serial,seq = query.seq
            let code_ou,result
            if('*'==branch) code_ou = ou
            else code_ou = branch
            if(ENT=='vcm'){
                 result = await inc.execsqlselect(`select id "id",pid "pid",cid "cid",cde "cde",adjtyp "adjtyp",adjdes "adjdes",status "status",doc "doc" from s_inv where ou=? and form=? and serial=? and seq=? and status in (3,4,6)`, [code_ou,form,serial,seq])
            }else{
                 result = await inc.execsqlselect(`select id "id",pid "pid",cid "cid",cde "cde",adjtyp "adjtyp",adjdes "adjdes",status "status",doc "doc" from s_inv where sec=? and status in (3,4,6)`, [sec])
            }
            
            const rows = result
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn mã tra cứu: ${sec} \n (Invoice not found for code: ${sec})`)
            const row = rows[0], doc = await rbi(row.id)
            const org = await ous.obt(doc.stax)
            const idx = org.temp, fn = util.fn(doc.form, idx), tmp = await util.template(fn)
            res.json({ doc: doc, tmp: tmp, adjtyp: row.adjtyp, adjdes: row.adjdes, pid: row.pid, cid: row.cid, cde: row.cde, status: row.status, ent: config.ent })
        }
        catch (err) {
            next(err)
        }
    },
    searchvcmpost: async (req, res, next) => {
        try {
            await util.chkCaptcha(req)
            const query = req.body, sec = query.sec,ou = query.ou,branch = query.branch,form = query.form,serial = query.serial,seq = query.seq
            let code_ou,result
            if('*'==branch) code_ou = ou
            else code_ou = branch
            if(ENT=='vcm'){
                 result = await inc.execsqlselect(`select id "id",pid "pid",cid "cid",cde "cde",adjtyp "adjtyp",adjdes "adjdes",status "status",doc "doc" from s_inv where  form=? and serial=? and seq=? and c6=? and status in (3,4,6)`, [form,serial,seq,sec])
            }else{
                 result = await inc.execsqlselect(`select id "id",pid "pid",cid "cid",cde "cde",adjtyp "adjtyp",adjdes "adjdes",status "status",doc "doc" from s_inv where sec=? and status in (3,4,6)`, [sec])
            }
            
            const rows = result
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn \n (Invoice not found)`)
            const row = rows[0], doc = await rbi(row.id)
            const org = await ous.obt(doc.stax)
            const idx = org.temp, fn = util.fn(doc.form, idx), tmp = await util.template(fn)
            let obj ={ status: doc.status, doc: doc, tmp: tmp }
            let reqjsr = ext.createRequest(obj)
            // res.json({ status: status, doc: doc, tmp: tmp })
             const bodyBuffer = await  util.jsReportRenderAttach(reqjsr)
             let sizeInByte = bodyBuffer.byteLength  
            res.json({pdf: `data:application/pdf;base64, ${bodyBuffer.toString('base64')}`, sizePDF: sizeInByte, adjtyp: row.adjtyp, adjdes: row.adjdes, pid: row.pid, cid: row.cid, cde: row.cde, status: row.status, ent: config.ent })
        }
        catch (err) {
            next(err)
        }
    },
    search2: async (req, res, next) => {
        try {
            const token = sec.decode(req), query = req.query, filter = JSON.parse(query.filter)
            const sort = query.sort, start = query.start ? query.start : 0, count = query.count ? query.count : 10
            let order, val, sql, where, result, binds, ret, i = 4
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = `order by ${key} ${sort[key]}`
                })
            }
            else order = "order by id desc"
            where = "where idt between ? and ? and btax=? and  status in (3,4,6)"
            binds = [moment(new Date(filter.fd)).format('YYYY-MM-DD HH:mm:ss'), moment(moment(filter.td).endOf("day")).format('YYYY-MM-DD HH:mm:ss'), token.taxc]
            for (const key of Object.keys(filter)) {
                val = filter[key]
                if (val) {
                    switch (key) {
                        case "fd":
                            break
                        case "td":
                            break
                        case "status_received":
                            if(filter.status_received != "*") {
                                where += " and status_received = ?"
                                binds.push(filter.status_received)
                            }
                            break
                        default:
                            where += ` and ${key} like ?`
                            binds.push(`%${val}%`)
                    }
                }
            }
            sql = `select sec "sec",pid "pid",cid "cid",id "id",idt "idt",status "status",status_received "status_received",type "type",form "form",serial "serial",seq "seq",stax "stax",sname "sname",saddr "saddr",stel "stel",smail "smail",sacc "sacc",sbank "sbank",btax "btax",bname "bname",buyer "buyer",baddr "baddr",btel "btel",bmail "bmail",bacc "bacc",bbank "bbank",note "note",sumv "sumv",vatv "vatv",totalv "totalv",adjdes "adjdes",cde "cde" from s_inv ${where} ${order} ${inc.getsqlpager(parseInt(start), parseInt(count))}`
            // OFFSET ? ROWS FETCH NEXT ? ROWS ONLY
            // binds.push(parseInt(start), parseInt(count))
            result = await inc.execsqlselect(sql, binds)
            ret = { data: result, pos: start }
            if (result.length > 0) {
                if (start == 0) {
                    sql = `select count(*) "total" from s_inv ${where}`
                    result = await inc.execsqlselect(sql, binds)
                    ret.total_count = result.total
                }
                res.json(ret)
            }else{
                throw new Error("Không tìm thấy hóa đơn \n (Cannot find Invoice)")
            }
        }
        catch (err) {
            next(err)
        }
    },
    search_eco: async (req, res, next) => {
        try {
            const token = sec.decode(req), query = req.query, filter = JSON.parse(query.filter)
            const sort = query.sort, start = query.start ? query.start : 0, count = query.count ? query.count : 10
            let order, val, sql, where, result, binds, ret, i = 4
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = `order by ${key} ${sort[key]}`
                })
            }
            else order = "order by id desc"
            where = `where idt between ? and ? and btax=? and status in (3,4,6) and ou in (select id "id" from s_ou where pid in (select id "id" from s_ou where taxc = '0106827752') or taxc = '0106827752')`
            binds = [moment(new Date(filter.fd)).format('YYYY-MM-DD HH:mm:ss'), moment(moment(filter.td).endOf("day")).format('YYYY-MM-DD HH:mm:ss'), token.taxc]
            for (const key of Object.keys(filter)) {
                val = filter[key]
                if (val) {
                    switch (key) {
                        case "fd":
                            break
                        case "td":
                            break
                        default:
                            where += ` and ${key} like ?`
                            binds.push(`%${val}%`)
                    }
                }
            }
            sql = `select sec,pid,cid,id,cast(idt as date) idt,status,type,form,serial,seq,stax,sname,saddr,stel,smail,sacc,sbank,btax,bname,buyer,baddr,btel,bmail,bacc,bbank,note,sumv,vatv,totalv,adjdes,cde from s_inv ${where} ${order} OFFSET ? ROWS FETCH NEXT ? ROWS ONLY`
            binds.push(parseInt(start), parseInt(count))
            result = await inc.execsqlselect(sql, binds)
            ret = { data: result, pos: start }
            if (start == 0) {
                sql = `select count(*) "total" from s_inv ${where}`
                result = await dbs.query(sql, binds)
                ret.total_count = result.total
            }
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    search_vin: async (req, res, next) => {
        try {
            const token = sec.decode(req), query = req.query, filter = JSON.parse(query.filter)
            const sort = query.sort, start = query.start ? query.start : 0, count = query.count ? query.count : 10
            let order, val, sql, where, result, binds, ret, i = 4
            if (sort) {
                Object.keys(sort).forEach(key => {
                    order = `order by ${key} ${sort[key]}`
                })
            }
            else order = "order by id desc"
            where = "where idt between @1 and @2 and btax=@3 and status in (3,4,6) and ou in  (select id from s_ou where pid in (select id from s_ou where taxc = '0104918404') or taxc = '0104918404')"
            binds = [moment(new Date(filter.fd)).format('YYYY-MM-DD HH:mm:ss'), moment(moment(filter.td).endOf("day")).format('YYYY-MM-DD HH:mm:ss'), token.taxc]
            for (const key of Object.keys(filter)) {
                val = filter[key]
                if (val) {
                    switch (key) {
                        case "fd":
                            break
                        case "td":
                            break
                        default:
                            where += ` and ${key} like @${i++}`
                            binds.push(`%${val}%`)
                    }
                }
            }
            sql = `select sec "sec",pid "pid",cid "cid",id "id",idt "idt",status "status",type "type",form "form",serial "serial",seq "seq",stax "stax",sname "sname",saddr "saddr",stel "stel",smail "smail",sacc "sacc",sbank "sbank",btax "btax",bname "bname",buyer "buyer",baddr "baddr",btel "btel",bmail "bmail",bacc "bacc",bbank "bbank",note "note",sumv "sumv",vatv "vatv",totalv "totalv",adjdes "adjdes",cde "cde" from s_inv ${where} ${order} OFFSET ? ROWS FETCH NEXT ? ROWS ONLY`
            binds.push(parseInt(start), parseInt(count))
            result = await inc.execsqlselect(sql, binds)
            ret = { data: result, pos: start }
            if (start == 0) {
                sql = `select count(*) "total" from s_inv ${where}`
                result = await dbs.query(sql, binds)
                ret.total_count = result.total
            }
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    xlssearch: async (req, res, next) => {
        try {
            const token = sec.decode(req), query = req.query, filter = JSON.parse(query.filter)
            let order, val, sql, where, result, binds, i = 4
            order = "order by id"
            where = "where idt between ? and ? and btax=? and status in (3,4)"
            binds = [moment(new Date(filter.fd)).format('YYYY-MM-DD HH:mm:ss'), moment(moment(filter.td).endOf("day")).format('YYYY-MM-DD HH:mm:ss'), token.taxc]
            for (const key of Object.keys(filter)) {
                val = filter[key]
                if (val) {
                    switch (key) {
                        case "fd":
                            break
                        case "td":
                            break
                        case "status_received":
                            if(filter.status_received != "*") {
                                where += " and status_received = ?"
                                binds.push(filter.status_received)
                            }
                            break
                        default:
                            where += ` and ${key} like ?`
                            binds.push(`%${val}%`)
                    }
                }
            }
            sql = `select ${sql_xlssearch[`sql_xlssearch_${config.dbtype}`]} from s_inv ${where} ${order}`
            result = await inc.execsqlselect(sql, binds)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
}
module.exports = Service