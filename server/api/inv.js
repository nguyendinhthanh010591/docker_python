"use strict"
const Hashids = require("hashids/cjs")
const moment = require("moment")
const config = require("./config")
const logger4app = require("./logger4app")
const email = require("./email")
const sign = require("./sign")
const spdf = require("./spdf")
const util = require("./util")
const redis = require("./redis")
const hbs = require("./hbs")
const fcloud = require("./fcloud")
const hsm = require("./hsm")
const SEC = require("./sec")
const SERIAL = require("./serial")
//const dbs = require("./dbs")
const COL = require("./col")
const ous = require("./ous")
const user = require("./user")
const path = require("path")
const fs = require("fs")
const archiver = require('archiver');
const ifile = require(`./ifile`)
const xlsxtemp = require("xlsx-template")
const logging = require("./logging")
const ftp = require("./ftp")
const sms = require("./sms")
const rsmq = require("./rsmq")
const inc = require("./inc")
const ads = require("./ads")
const qname = config.qname
const hashids = new Hashids(config.secret, 10, "abcdefghijklmnopqrstuvwxyz1234567890")
const CARR = config.CARR
const grant = config.ou_grant
const serial_usr_grant = config.serial_usr_grant
const mfd = config.mfd
const dtf = config.dtf
const ENT = config.ent
const invoice_seq = config.invoice_seq
const UPLOAD_MINUTES = config.upload_minute
const useJobSendmail = config.useJobSendmail
const useRedisQueueSendmail = config.useRedisQueueSendmail
const sql_xls_01 = {
    sql_xls_01_mssql: `id,sec,ic,uc,FORMAT(idt,'dd/MM/yyyy') idt,case when status=1 then N'Chờ cấp số' when status=2 then N'Chờ duyệt' when status=3 then N'Đã duyệt' when status=4 then N'Đã hủy' when status=5 then N'Đã gửi' when status=6 then N'Chờ hủy' end status,ou,type,form,serial,seq,btax,bname,buyer,baddr,note,sumv,vatv,totalv,sum,vat,total,adjdes,cde,bmail,curr,sname sname,case when sendmailstatus = 0 then N'Chưa gửi' when sendmailstatus = 1 then N'Đã gửi' end sendmailstatus,case when status_received  = 0 then N'Chờ gửi CQT' when status_received  = 1 then N'Gửi CQT' when status_received  = 2 then N'Gửi không thành công' when status_received  = 8 then N'Kiểm tra hợp lệ' when status_received  = 9 then N'Kiểm tra không hợp lệ' when status_received  = 10 then N'Đã cấp mã' end status_received ,error_msg_van,CONCAT(CONCAT(list_invid, ';'),list_invid_bx)  list_invid`,
    sql_xls_01_mysql: `id id,sec sec,ic ic,uc uc,DATE_FORMAT(idt,'%d/%m/%Y') idt,case when status = 1 then 'Chờ cấp số'  when status = 2 then 'Chờ duyệt'  when status = 3 then 'Đã duyệt' when status = 4 then 'Đã hủy'  when status = 5 then 'Đã gửi'  when status = 6 then 'Chờ hủy' end status,ou ou,type type,form form,serial serial,seq seq,btax btax,bname bname,buyer buyer,baddr baddr,note note,sumv sumv,vatv vatv,totalv totalv,sum sum,vat vat,total total,adjdes adjdes,cde cde,bmail bmail,curr curr,sname sname,case when sendmailstatus = 0 then 'Chưa gửi' when sendmailstatus = 1 then 'Đã gửi' end sendmailstatus,case when status_received  = 0 then N'Chờ gửi CQT' when status_received  = 1 then N'Gửi CQT' when status_received  = 2 then N'Gửi không thành công' when status_received  = 8 then N'Kiểm tra hợp lệ' when status_received  = 9 then N'Kiểm tra không hợp lệ' when status_received  = 10 then N'Đã cấp mã' end status_received,error_msg_van error_msg_van,CONCAT(CONCAT(list_invid, ';'),IFNULL(list_invid_bx, ''))  list_invid`,
    sql_xls_01_orcl: `id "id",sec "sec",ic "ic",to_char(idt,'dd/mm/yyyy') "idt",DECODE(status,1,'Chờ cấp số',2,'Chờ duyệt',3,'Đã duyệt',4,'Đã hủy',5,'Đã gửi',6,'Chờ hủy') "status",type "type",form "form",serial "serial",seq "seq",btax "btax",bname "bname",buyer "buyer",baddr "baddr",note "note",sumv "sumv",vatv "vatv",totalv "totalv",sum "sum",vat "vat",total "total",adjdes "adjdes",cde "cde", bmail "bmail", curr "curr",uc "uc", ou "ou",sname "sname",DECODE(sendmailstatus,0,'Chưa gửi',1,'Đã gửi') "sendmailstatus",case when status_received  = 0 then N'Chờ gửi CQT' when status_received  = 1 then N'Gửi CQT' when status_received  = 2 then N'Gửi không thành công' when status_received  = 8 then N'Kiểm tra hợp lệ' when status_received  = 9 then N'Kiểm tra không hợp lệ' when status_received  = 10 then N'Đã cấp mã' end status_received,error_msg_van "error_msg_van",CONCAT(CONCAT(list_invid, ';'),list_invid_bx)  "list_invid"`,
    sql_xls_01_pgsql: `id,sec,ic,uc,to_char(idt,'DD/MM/YYYY') idt,case when status=1 then 'Chờ cấp số' when status=2 then 'Chờ duyệt' when status=3 then 'Đã duyệt' when status=4 then 'Đã hủy' when status=5 then 'Đã gửi' when status=6 then 'Chờ hủy' end status,ou,type,form,serial,seq,btax,bname,buyer,baddr,note,sumv,vatv,totalv,sum,vat,total,adjdes,cde,bmail,curr,sname sname,case when sendmailstatus = 0 then 'Chưa gửi' when sendmailstatus = 1 then 'Đã gửi' end sendmailstatus,case when status_received  = 0 then N'Chờ gửi CQT' when status_received  = 1 then N'Gửi CQT' when status_received  = 2 then N'Gửi không thành công' when status_received  = 8 then N'Kiểm tra hợp lệ' when status_received  = 9 then N'Kiểm tra không hợp lệ' when status_received  = 10 then N'Đã cấp mã' end status_received,error_msg_van error_msg_van,CONCAT(CONCAT(list_invid, ';'),list_invid_bx) list_invid`
}
// Thêm trường error_msg_van
const sql_get_01 = {
    sql_get_01_mssql: `0 chk,id,sec,ic,uc,curr,pid,cid,cast(idt as date) idt,cast(cdt as date) cdt,status, status_received,error_msg_van, status_tbss,ou,type,form,serial,seq,btax,bname,bcode,buyer,baddr,btel,stax,note,sumv,vatv,totalv,sum,vat,total,adjdes,adjtyp,cde,cvt,exrt,bmail,sendmailnum sendmailnum, cast(sendmaildate as date) sendmaildate, sendsmsnum sendsmsnum, sendsmsstatus sendsmsstatus, cast(sendsmsdate as date) sendsmsdate,case when sendmailstatus = 0 then N'Chưa gửi' when sendmailstatus = 1 then N'Đã gửi' end sendmailstatus,ordno ordno,orddt orddt,whsto whsto,whsfr whsfr,case when adjtyp = 1 then JSON_VALUE(doc,'$.adj.rea') when adjtyp = 2 then JSON_VALUE(doc,'$.cancel.rea') when status = 4 then JSON_VALUE(doc,'$.cancel.rea') else '' END rea, inv_adj,CONCAT(CONCAT(list_invid, ';'),list_invid_bx) list_invid,wnadjtype,wno_adj,JSON_VALUE([doc], '$.adjdif') "adjdif"`,
    sql_get_01_mysql: `0 chk,id id,sec sec,ic ic,uc uc,curr curr,pid pid,cid cid,idt idt,cdt cdt,status status,error_msg_van error_msg_van, status_received status_received, status_tbss status_tbss,stax stax,ou ou,type type,form form,serial serial,seq seq,btax btax,bname bname,buyer buyer,baddr baddr,invlistdt invlistdt, note note,sumv sumv,vatv vatv,totalv totalv,sum sum,vat vat,total total,adjdes adjdes,adjtyp adjtyp,cde cde,cvt cvt,bmail bmail,case when sendmailstatus = 0 then 'Chưa gửi' when sendmailstatus = 1 then 'Đã gửi' end sendmailstatus,ordno ordno,orddt orddt,whsto whsto,whsfr whsfr, inv_adj,CONCAT(CONCAT(list_invid, ';'),IFNULL(list_invid_bx, '')) list_invid, wnadjtype, wno_adj, doc ->> '$.adjdif' "adjdif"`,
    sql_get_01_orcl: `0 "chk",id "id",sec "sec",ic "ic",uc "uc",curr "curr" ,pid "pid",cid "cid",idt "idt",cdt "cdt",status "status", status_received "status_received", status_tbss "status_tbss", ou "ou", stax "stax",type "type",form "form",serial "serial",seq "seq",btax "btax",bname "bname",buyer "buyer",baddr "baddr",note "note",invlistdt "invlistdt",sumv "sumv",vatv "vatv",totalv "totalv",sum "sum",vat "vat",total "total",adjdes "adjdes",adjtyp "adjtyp",cde "cde", cvt "cvt", bmail "bmail",sendmailnum "sendmailnum", sendmailstatus "sendmailstatus", sendmaildate "sendmaildate", sendsmsnum "sendsmsnum", sendsmsstatus "sendsmsstatus", sendsmsdate "sendsmsdate", DECODE(sendmailstatus,0,'Chưa gửi',1,'Đã gửi') "sendmailstatus",ordno "ordno",orddt "orddt",whsto "whsto",whsfr "whsfr", error_msg_van "error_msg_van", inv_adj "inv_adj", CONCAT(CONCAT(list_invid, ';'),list_invid_bx) "list_invid", wnadjtype "wnadjtype",wno_adj "wno_adj", JSON_VALUE(doc, '$.adjdif') "adjdif"`,
    sql_get_01_pgsql: `0 chk,id,sec,ic,uc,curr,pid,cid,idt,cdt,status,status_received,status_tbss,ou,type,form,serial,seq,btax,bname,buyer,baddr,note,sumv,vatv,totalv,sum,vat,total,adjdes,adjtyp,cde,cvt,stax,bmail,case when sendmailstatus = 0 then 'Chưa gửi' when sendmailstatus = 1 then 'Đã gửi' end sendmailstatus,ordno,orddt,whsto,whsfr, error_msg_van, inv_adj,CONCAT(CONCAT(list_invid, ';'),list_invid_bx) list_invid, wnadjtype, wno_adj, doc->>'adjdif' "adjdif"`
}
const sql_go24_01 = {
    sql_go24_01_mssql: `cast(seq as int)`,
    sql_go24_01_mysql: `cast(seq as SIGNED)`,
    sql_go24_01_orcl: `TO_NUMBER(seq)`,
    sql_go24_01_pgsql: `cast(seq as int)`
}
const sql_relate_01 = {
    sql_relate_01_mssql: `where stax=? and sec<>? and (JSON_VALUE(doc,'$.root.sec')=?`,
    sql_relate_01_mysql: `where a.stax=? and a.sec<>? and (a.doc->"$.root.sec"=?`,
    sql_relate_01_orcl: `where a.stax=? and a.sec<>'{'||?||'}' and (json_value(a.doc format json , '$.root.sec' returning varchar2(10) null on error) = ?`,
    sql_relate_01_pgsql: `where stax=$1 and sec<>$2 and (doc->>'root.sec'=$2`
}
const sql_relate_02 = {
    sql_relate_02_mssql: `or sec=? or JSON_VALUE(doc,'$.root.sec')=?`,
    sql_relate_02_mysql: `or a.sec=? or a.doc->"$.root.sec"=?`,
    sql_relate_02_orcl: `or a.sec=? or json_value(a.doc format json , '$.root.sec' returning varchar2(10) null on error) = ?`,
    sql_relate_02_pgsql: `or sec=? or doc->>'root.sec'=?`
}
const sql_relatewno = {
    sql_relatewno_mssql: `select id "id", JSON_VALUE([doc], '$.noti_dt') "noti_dt", status "status", status_cqt "status_cqt", dien_giai "dien_giai"`,
    sql_relatewno_orcl: `select id "id", JSON_VALUE(doc, '$.noti_dt') "noti_dt", status "status", status_cqt "status_cqt", dien_giai "dien_giai"`,
    sql_relatewno_mysql: `select id "id", doc ->> '$.noti_dt' "noti_dt", status "status", status_cqt "status_cqt", dien_giai "dien_giai"`,
    sql_relatewno_pgsql: `select id "id", doc->>'noti_dt' "noti_dt", status "status", status_cqt "status_cqt", dien_giai "dien_giai"`
}
const sql_api_search = {
    sql_api_search_mssql: `c0,form form,serial serial,seq seq,FORMAT(idt,'dd/MM/yyyy') idt,sec sec,status status,total total,id id`,
    sql_api_search_mysql: `c0,form form,serial serial,seq seq,DATE_FORMAT(idt, "%d-%m-%Y") idt,sec sec,status status,total total,id id`,
    sql_api_search_orcl: `c0 "c0",form "form",serial "serial",seq "seq",to_char(idt,'dd/mm/yyyy') "idt",sec "sec",status "status",total "total",id "id"`,
    sql_api_search_pgsql: `c0,form form,serial serial,seq seq,to_char(idt,'DD/MM/YYYY') idt,sec sec,status status,total total,id id`
}
const sql_history = {
    sql_history_mssql:   `select vh.id,
                            vh.type_invoice,
                            vh.id_ref,
                            vh.status,
                            FORMAT(vh.datetime_trans,'dd/MM/yyyy HH:mm:ss') datetime_trans,
                            vh.description ,
                            vh.r_xml "r_xml"
                        from van_history vh join s_inv si on vh.id_ref=si.id 
                        where vh.type_invoice=2 and vh.id_ref=? order by vh.datetime_trans ASC`
    ,
    sql_history_mysql:  `select vh.id,
                            vh.type_invoice,
                            vh.id_ref,
                            vh.status,
                            DATE_FORMAT(vh.datetime_trans,'%d/%m/%Y %T') datetime_trans,
                            vh.description ,
                            vh.r_xml 
                        from van_history vh join s_inv si on vh.id_ref=si.id 
                        where vh.type_invoice=2 and vh.id_ref=? order by vh.datetime_trans ASC`
    ,
    sql_history_orcl:  `select vh.id "id",
                            vh.type_invoice "type_invoice",
                            vh.id_ref "id_ref",
                            vh.status "status",
                            to_char(vh.datetime_trans,'DD/MM/YYYY HH24:MI:SS') "datetime_trans",
                            vh.description "description" ,
                            vh.r_xml "r_xml"
                        from van_history vh join s_inv si on vh.id_ref=si.id 
                        where vh.type_invoice=2 and vh.id_ref=? order by vh.datetime_trans ASC`
    ,
    sql_history_pgsql:  `select vh.id,
                            vh.type_invoice,
                            vh.id_ref,
                            vh.status,
                            to_char(vh.datetime_trans,'DD/MM/YYYY HH:MI:SS') datetime_trans,
                            vh.description ,
                            vh.r_xml 
                        from van_history vh join s_inv si on vh.id_ref=si.id 
                        where vh.type_invoice=2 and vh.id_ref=? order by vh.datetime_trans ASC
    `
}
const rbi = (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            const result = await inc.execsqlselect(`select doc "doc", uc "uc", ou "ou", c0 "c0", c1 "c1", c2 "c2", c3 "c3", c4 "c4", c5"c5", c6 "c6", c7 "c7", c8 "c8", c9 "c9" from s_inv where id=?`, [id])
            const rows = result
            if (rows.length == 0) reject(new Error(`Không tìm thấy Hóa đơn ${id} \n (Invoice not found ${id})`))
            let uc = rows[0].uc, ou = rows[0].ou, c0 = rows[0].c0, c1 = rows[0].c1, c2 = rows[0].c2, c3 = rows[0].c3,
                c4 = rows[0].c4, c5 = rows[0].c5, c6 = rows[0].c6, c7 = rows[0].c7, c8 = rows[0].c8, c9 = rows[0].c9
            let doc =  util.parseJson(rows[0].doc)  // VinhHQ12 sửa 4 DB
            doc.uc = uc
            doc.ou = ou
            doc.c0 = c0
            doc.c1 = c1
            doc.c2 = c2
            doc.c3 = c3
            doc.c4 = c4
            doc.c5 = c5
            doc.c6 = c6
            doc.c7 = c7
            doc.c8 = c8
            doc.c9 = c9
            doc.ent = ENT
            if (ENT == 'vcm') {
                let taxc_root = await ous.pidtaxc(ou)
                doc.taxc_root = taxc_root
            }
            //Lấy thông tin user của user lập hóa đơn
            let usercreate = await user.obid(uc)
            //Lấy thông tin ou của user lập hóa đơn
            let useroucreate = await ous.obid(ou)
            //Gán vào doc và trả ra
            doc.usercreate = usercreate
            doc.useroucreate = useroucreate
            doc.ouname = useroucreate.name
            doc.acccf = useroucreate.acc //config thong tin cho sgr
            //Lay thong tin so thu tu template de sau nay dung truy van template hoa don
            const org = await ous.obt(doc.stax), idx = org.temp
            const form = doc.form, type = doc.type, serial = doc.serial
            const fn = form.length > 1 ? util.fn(form, idx) : `${type}.${form}.${serial}.${idx}`
            doc.tempfn = fn
            resolve(doc)
        } catch (err) {
            reject(err)
        }
    })
}
const insListInvCancelCaseReplace = async (token, id,cdt) => {
    let  resultCancel = await inc.execsqlselect(`select inv.id "id",inv.status_received "status_received",inv.doc "doc",sr.invtype "invtype",sr.sendtype "sendtype",idt "idt" from s_inv inv, s_serial sr where inv.form = sr.form and inv.serial=sr.serial and inv.stax = sr.taxc and inv.id=?`, [id]), cancelInv = resultCancel[0],doc=cancelInv.doc
    doc = util.parseJson(doc)
    let idt = new Date(Date.parse(doc.idt)),m = idt.getMonth(), y = idt.getFullYear()
    let cdt2 = new Date(Date.parse(cdt)),mc = cdt2.getMonth(), yc = cdt2.getFullYear()
    //ngay huy khac thang
    if(cancelInv.sendtype==2 && (yc!=y || (yc==y && mc>m)))  await  inc.execsqlinsupddel(`INSERT INTO s_list_inv_cancel (inv_id,invtype,doc,ou,cdt) VALUES (?,?,?,?,?)`, [cancelInv.id, cancelInv.invtype,cancelInv.doc,token.ou,cdt])
}
const oubi = (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            const result = await inc.execsqlselect(`select name "name" from s_ou where id=?`, [id])
            let name
            const rows = result
            if (rows.length == 0)
                name = ""
            else
                name = rows[0].name
            resolve(name)
        } catch (err) {
            reject(err)
        }
    })
}
const chkou = async (id, token) => {
    if (grant) {
        if (token.ou !== token.u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)
        const result = await inc.execsqlselect(`select ou "ou" from s_inv where id=?`, [id])
        const rows = result
        if (rows.length > 0 && rows[0].ou !== token.u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)
    }
}
const chkDds = async (ic, rows) => {
    let check = false
    for (let row of rows) {
        if (ic == row.CustomerAbbr) check = true
    }

    if (check) return 1
    else return 0

}

const processFile = async (doc, xml) => {
    let dfs = moment().format('YYYYMMDD')
    let cus = await inc.execsqlselect(`select * from s_org where code = ?`, [doc.bcode])
    if(cus.length > 0){
        let result = await inc.execsqlselect(`select used_number from s_dbs_mail_seq sr where seq_date = ?`, [dfs])
        var seq = 0;
        if(result.length == 0){
            await inc.execsqlinsupddel(`INSERT INTO s_dbs_mail_seq (seq_date, used_number) values (?,?)`, [dfs, 1])
            seq = 1;
        }else{
            seq = result[0].used_number;
            seq++;
            await inc.execsqlinsupddel(`update s_dbs_mail_seq set used_number=? where seq_date=?`, [seq, dfs])
        }
        let dir = config.folder_mail;
        let file_name = '409-'+dfs+'-'+String(seq).padStart(4, '0');
        if (!fs.existsSync(dir)){
            fs.mkdirSync(dir);
        }
        //save ctl file
        fs.writeFile(dir+file_name+'.ctl', '', function (err) {
            if (err) throw err;
            console.log('File ctl is created successfully.');
        });
        // create zip file using zip A-VAT B-FX C-INT đuôi series
        var type = '';
        let lastChar = doc.serial.substr(doc.serial.length - 1)
        if(lastChar == 'A'){
            type = 'VAT-'
        }else if(lastChar == 'B'){
            type = 'FX-'
        }else if(lastChar == 'C'){
            type = 'INT-'
        }
        let file_zip_name = type+cus[0].code+'_'+dfs+'_'+String(seq).padStart(3, '0');
        let attachments = await ifile.getFileMailDBS(doc, xml,file_zip_name,cus[0])
        // save zip file
       // var writeStream = fs.createWriteStream(dir+file_zip_name+'.zip');
        // write zip data with a base64 encoding
       // writeStream.write(attachments[0].content, 'base64');
      //  writeStream.end()
        //save xml
        let email = cus[0].mail
        let sendmaildate = moment().format('DD-MM-YYYY hh:mm:ss')
        let sxml = '<conted-request><request-header><system-id>409</system-id><request-id>'+file_name+'</request-id><request-type>DD</request-type><request-date>'+sendmaildate +'</request-date><template-id>DIRECT-HTM</template-id><delivery>EMAIL</delivery></request-header><request-body><request-data><record><content-recipient>'+email+'</content-recipient><email-subject>Your DBS VAT invoice</email-subject><email-atth-filename>'+file_zip_name+'.zip' + '</email-atth-filename><email-default-body /><content-id>'+file_name+`-1</content-id><record-field><field-name>DIRECT_CONTENT</field-name><field-value>&lt;p style="margin-bottom: .0001pt;"&gt;&lt;span style="font-size: 9.0pt; line-height: 107%; font-family: 'MS Shell Dlg 2',sans-serif; color: black;"&gt;Dear Customer,&lt;/span&gt;&lt;/p&gt;&lt;p style="margin-bottom: .0001pt;"&gt;&amp;nbsp;&lt;/p&gt;&lt;p style="margin-bottom: .0001pt;"&gt;&lt;span style="font-size: 9.0pt; line-height: 107%; font-family: 'MS Shell Dlg 2',sans-serif; color: black;"&gt;Your VAT Invoices are ready for viewing. Please use the password from the bank to open the VAT invoice.&lt;/span&gt;&lt;/p&gt;&lt;p style="margin-bottom: .0001pt;"&gt;&amp;nbsp;&lt;/p&gt;&lt;p style="margin-bottom: .0001pt;"&gt;&lt;span style="font-size: 9.0pt; line-height: 107%; font-family: 'MS Shell Dlg 2',sans-serif; color: black;"&gt;Please contact us at our corporate hotline at +84 28 3914 7888 between 9:00am to 5:00pm (Vietnam Time) for enquiry.&lt;/span&gt;&lt;/p&gt;&lt;p style="margin-bottom: .0001pt;"&gt;&amp;nbsp;&lt;/p&gt;&lt;p style="margin-bottom: .0001pt;"&gt;&lt;span style="font-size: 9.0pt; line-height: 107%; font-family: 'MS Shell Dlg 2',sans-serif; color: black;"&gt;This is an automatically generated notification. Please do not reply to this email.&lt;/span&gt;&lt;/p&gt;&lt;p &lt;p style="margin-bottom: .0001pt;"&gt;&amp;nbsp;&lt;/p&gt;&lt;p style="margin-bottom: .0001pt;"&gt;&lt;span style="font-size: 9.0pt; line-height: 107%; font-family: 'MS Shell Dlg 2',sans-serif; color: black;"&gt;Yours Sincerely,&lt;/span&gt;&lt;/p&gt;&lt;p style="margin-bottom: .0001pt;"&gt;&lt;span style="font-size: 9.0pt; line-height: 107%; font-family: 'MS Shell Dlg 2',sans-serif; color: black;"&gt;DBS Bank Ltd. - Ho Chi Minh City Branch&lt;/span&gt;&lt;/p&gt;&lt;p style="margin-bottom: .0001pt;"&gt;&amp;nbsp;&lt;/p&gt;&lt;p style="margin-bottom: .0001pt;"&gt;&lt;span style="font-size: 9.0pt; line-height: 107%; font-family: 'MS Shell Dlg 2',sans-serif; color: black;"&gt;CONFIDENTIAL NOTE: The information contained in this email is intended only for the use of the individual or entity named above and may contain information that is privileged, confidential and exempt from disclosure under applicable law. If the reader of this message is not the intended recipient, you are hereby notified that any dissemination, distribution or copying of this communication is strictly prohibited. If you have received this message in error, please immediately notify the sender and delete the mail. Thank you.&lt;/span&gt;&lt;/p&gt;</field-value></record-field></record></request-data></request-body></conted-request>`;		  
        fs.writeFile(dir+file_name+'.xml', sxml, function (err) {
            if (err) throw err;
            console.log('File xml is created successfully.');
        });
        // Close the file descriptor
        // fs.close(writeStream, (err) => {
        //     if (err)
        //     console.error('Failed to close file', err);
        //     else {
        //     console.log("\n> File Closed successfully");
        //     }
        // });
    }

}

const chkDdsPass = async (ic, rows) => {
    let pass = 0
    for (let row of rows) {
        if (ic == row.CustomerAbbr) return pass = row.PASSWORD
    }
    return pass
}
const Service = {
    getcol: async (json) => {
        try {
            let doc = util.parseJson(JSON.stringify(json))
            let configfield = {}
            let configfieldhdr = await COL.cache(doc.type, 'vi')
            let configfielddtl = await COL.cachedtls(doc.type, 'vi')
            configfield.configfieldhdr = configfieldhdr
            configfield.configfielddtl = configfielddtl
            doc.configfield = configfield
            let doctmp = await rbi(json.id)
            doc.org = doctmp.useroucreate

            return doc
        }
        catch (err) {
            throw err
        }
    },
    invdocbid: async (invid) => {
        try {
            let doc = await rbi(invid)
            return doc
        }
        catch (err) {
            logger4app.debug(`invdocbid ${invid}:`, err)
        }
    },
    insertDocTempJob: async (invid, doc, xml) => {
        try {
            if (!config.useRedisQueueSendmail) {
                let ret = await inc.execsqlinsupddel(`INSERT INTO s_invdoctemp (invid, doc, status, xml) values (?,?,?,?)`, [invid, JSON.stringify(doc), 0, xml])
            } else {
                await rsmq.sendMessageAsync({ qname: qname, message: JSON.stringify({ invid: invid, status: doc.status, subject: doc.subject, bmail: doc.bmail, sendmanual: doc.sendmanual }) })
            }
        }
        catch (err) {
            logger4app.debug(`insertDocTempJob ${invid}:`, err.message)
        }
    },
    insertDocTempJobAPI: async (invid, doc, xml) => {
        try {
            if (!config.useRedisQueueSendmailAPI) {
                let ret = await inc.execsqlinsupddel(`INSERT INTO s_invdoctemp (invid, doc, status, xml) values (?,?,?,?)`, [invid, JSON.stringify(doc), 0, xml])
            } else {
                await rsmq.sendMessageAsync({ qname: qname, message: JSON.stringify({ invid: invid, status: doc.status, subject: doc.subject, bmail: doc.bmail, sendmanual: doc.sendmanual }) })
            }
        }
        catch (err) {
            logger4app.debug(`insertDocTempJob API ${invid}:`, err.message)
        }
    },
    updateSendMailStatus: async (doc) => {
        try {
            let sql, binds
            //Cập nhật ngày và trạng thái gửi mail, số lần gửi mail
            logger4app.debug(`worker.mailshortmsg doc ${doc.serial} ${doc.form} ${doc.seq}`)
            let docupd = await rbi(doc.id)
            // logger4app.debug(`worker.mailshortmsg docupd ${JSON.stringify(docupd)}`)
            if (docupd.SendMailNum) {
                try {
                    docupd.SendMailNum = parseInt(docupd.SendMailNum) + 1
                } catch (err) { docupd.SendMailNum = 1 }
            } else {
                docupd.SendMailNum = 1
            }
            docupd.SendMailStatus = 1
            docupd.SendMailDate = moment(new Date()).format(dtf)
            sql = `update s_inv set doc=? where id=?`
            binds = [JSON.stringify(docupd), doc.id]
            let ret = await inc.execsqlinsupddel(sql, binds)
        }
        catch (err) {
            throw err
        }
    },
    updateSendSMSStatus: async (doc, smsstatus) => {
        try {
            let sql, binds
            let docupd = await rbi(doc.id)
            if ((config.sendApprSMS) || (config.sendCancSMS)) {
                //Cập nhật ngày và trạng thái gửi SMS, số lần gửi SMS
                if (docupd.SendSMSNum) {
                    try {
                        docupd.SendSMSNum = parseInt(docupd.SendSMSNum) + 1
                    } catch (err) { docupd.SendSMSNum = 1 }
                } else {
                    docupd.SendSMSNum = 1
                }
                docupd.SendSMSStatus = smsstatus.errnum == 0 ? 1 : -1
                docupd.SendSMSDate = moment(new Date()).format(dtf)
                docupd.SendSMSDesc = smsstatus.resdesc
                sql = `update s_inv set doc=? where id=?`
                binds = [JSON.stringify(docupd), doc.id]
                let ret = await inc.execsqlinsupddel(sql, binds)
            }
        }
        catch (err) {
            throw err
        }
    },
    checkrefno: async (doc) => {
        try {
            if (!config.refno) return []
            let str = "", sql, arr = [], binds = [], where = "where 1=1 ", mappingfield = config.refno.mappingfield, rows
            if (mappingfield.split(".")[0] == "doc") {
                rows = [doc]
            } else {
                rows = doc.items
            }
            const crr = eval(`[...new Set(rows.map(x => x.${mappingfield.split(".")[1]}))]`)
            crr.forEach(function (value, i) {
                str += `?,`
                binds.push(value)
            })
            where += ` and ref_no in (${str.slice(0, -1)}) `
            sql = `select ref_no "ref_no" from s_invd ${where}`
            const result = await inc.execsqlselect(sql, binds)
            rows = result
            rows.forEach((v, i) => {
                arr.push(v.ref_no)
            })
            return arr
        } catch (err) {
            throw err
        }
    },
    checkrefdate: async (doc) => {
        try {
            if (!config.refdate) return []
            let arr = [], mappingfield = config.refdate.mappingfield, rows
            const idt = moment(doc.idt, dtf).toDate()
            if (mappingfield.split(".")[0] == "doc") {
                rows = [doc]
            } else {
                rows = doc.items
            }
            const crr = eval(`[...new Set(rows.map(x => x.${mappingfield.split(".")[1]}))]`)
            crr.forEach(function (value, i) {
                let refdate = moment(value, dtf).toDate()
                if (refdate > idt) arr.push(value)
            })

            return arr
        } catch (err) {
            throw err
        }
    },
    saverefno: async (doc) => {
        try {
            const id = doc.id
            if (!config.refno) return 1
            let numc = 0, sql = `INSERT INTO s_invd (ref_no,inv_id) VALUES (?,?) `, mappingfield = config.refno.mappingfield, rows
            if (mappingfield.split(".")[0] == "doc") {
                rows = [doc]
            } else {
                rows = doc.items
            }
            const crr = eval(`[...new Set(rows.map(x => x.${mappingfield.split(".")[1]}))]`)
            for (let i = 0; i < crr.length; i++) {
                let value = crr[i]
                try {
                    let ret = await inc.execsqlinsupddel(sql, [value, id])
                    numc++
                } catch (error) {
                    logger4app.error(error)
                }
            }
            return numc
        } catch (err) {
            throw err
        }
    },
    delrefno: async (doc) => {
        try {
            if (!config.refno) return 1
            let numc = 0, sql = `DELETE from s_invd where ref_no = ? `, mappingfield = config.refno.mappingfield, rows
            if (mappingfield.split(".")[0] == "doc") {
                rows = [doc]
            } else {
                rows = doc.items
            }
            const crr = eval(`[...new Set(rows.map(x => x.${mappingfield.split(".")[1]}))]`)
            for (let i = 0; i < crr.length; i++) {
                let value = crr[i]
                try {
                    let ret = await inc.execsqlinsupddel(sql, [value])
                    numc++
                } catch (error) {
                    logger4app.error(error)
                }
            }
            return numc
        } catch (err) {
            throw err
        }
    },
    count12: async (req, res, next) => {
        try {
            const token = SEC.decode(req), query = req.query
            const result = await inc.execsqlselect(`select count(*) rc from s_inv where stax=? and form=? and serial=? and status in (1, 2)`, [token.taxc, query.form, query.serial])
            res.json(result[0])
        }
        catch (err) {
            next(err)
        }
    },
    seqget: async (req, res, next) => {
        try {
            // const token = SEC.decode(req), query = req.query, filter = util.parseJson(query.filter), start = query.start ? query.start : 0, count = query.count ? query.count : 10
            // let td = filter.td
            // let where = " where stax=? and form=? and serial=? and dt<=? and status=1"
            // let binds = [token.taxc, filter.form, filter.serial,td]
            // let sql = `select id id,idt idt,status status,ou ou,type type,form form,serial serial,seq seq,btax btax,bname bname,buyer buyer,baddr baddr,sumv sumv,vatv vatv,totalv totalv,sum "sum",vat "vat",total "total",adjdes adjdes,uc uc,ic ic from s_inv ${where} order by idt,id LIMIT ${count} OFFSET ${start}`
            // let result = await dbs.query(sql, binds)
            // let ret = { data: result[0], pos: start }
            // if (start == 0) {
            //     sql = `select count(*) total from s_inv ${where}`
            //     result = await dbs.query(sql, binds)
            //     ret.total_count = result[0][0].total
            // }
            // res.json(ret)
            const token = SEC.decode(req), query = req.query, filter = util.parseJson(query.filter), start = query.start ? query.start : 0, count = query.count ? query.count : 10
            let td = moment(filter.td).endOf("day").format("YYYY-MM-DD HH:mm:ss")
            if (config.dbtype == "orcl") td = new Date(moment(filter.td).endOf("day").format("YYYY-MM-DD HH:mm:ss"))
            let where = "where stax=? and form=? and serial=? and idt<=? and status=1"
            // if(ENT=='scb')where +=` and c0 in (select system from s_user_sys where userid='${token.uid}') `

            let binds = [token.taxc, filter.form, filter.serial, td]
            let sql = `select id "id",idt "idt",status "status",ou "ou",type "type",form "form",serial "serial",seq "seq",btax "btax",bname "bname",buyer "buyer",baddr "baddr",sumv "sumv",vatv "vatv",totalv "totalv",sum "sum",vat "vat",total "total",adjdes "adjdes",uc "uc",ic "ic",note "note" from s_inv ${where} order by idt,id ${inc.getsqlpager(start, count)}`

            let result = await inc.execsqlselect(sql, binds)
            let ret = { data: result, pos: start }
            if (start == 0) {
                sql = `select count(*) "total" from s_inv ${where}`
                result = await inc.execsqlselect(sql, binds)
                ret.total_count = result[0].total
            }
            res.json(ret)
        } catch (error) {
            next(error)
        }
    },
    seqpost: async (req, res, next) => {
        let key
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const wait = 60, taxc = token.taxc, body = req.body, form = body.form, serial = body.serial, idt = moment(body.idt).format("YYYY-MM-DD HH:mm:ss"), seqList = body.seqList, idSD = (body.id) ? body.id : null
            key = `APPR.${taxc}.${form}.${serial}`
            const val = await redis.get(key)

            if (val == "1") throw new Error(`Đang cấp số cho HĐ MST=${taxc} mẫu số=${form} ký hiệu=${serial}. Bạn hãy đợi ${wait}s ! \n (Sequencing for invoice Tax.No=${taxc} Form=${form} Serial=${serial}. Should be waiting for ${wait}s !)`)
            await redis.set(key, "1", "EX", wait)
            let sql, binds, where = ''
            if (seqList.length > 0) {
                // cấp 1 hoặc lỗ chỗ trong 1 ngày
                binds = [taxc, form, serial]
                let str = "", i = 4
                for (const row of seqList) {
                    str += `?,`
                    binds.push(row)
                }
                where = ` where stax=? and form=? and serial=? and status=1 and id IN (${str.slice(0, -1)}) `
                //  if(ENT=='scb')where +=` and c0 in (select system from s_user_sys where userid='${token.uid}') `
                sql = `select id "id",pid "pid",ou "ou",adjtyp "adjtyp",idt "idt",sec "sec",c6 "c6",total "total" from s_inv ${where} order by idt,id`
            } else {
                let docRS = await inc.execsqlselect(`select dt "dt" from s_inv where id = ?`, [idSD])
                let dt = (docRS && docRS.length > 0 && docRS[0].dt) ? docRS[0].dt : moment(moment(idt).endOf("day")).format(dtf)
                if (config.dbtype == "orcl") dt = new Date(dt)
                where = ` where dt <= ? and stax=? and form=? and serial=? and status=1`
                //if(ENT=='scb')where +=` and c0 in (select system from s_user_sys where userid='${token.uid}') `
                // cấp theo dải bé hơn ngày hiện tại
                sql = `select id "id",pid "pid",ou "ou",adjtyp "adjtyp",idt "idt",sec "sec",c6 "c6",total "total" from s_inv ${where} order by idt,dt,id`
                binds = [dt, taxc, form, serial]
            }
            const result = await inc.execsqlselect(sql, binds)
            const rows = result
            const sup = `update s_inv set doc=? where id=? and stax=? and status=1`, pup = `update s_inv set cde=?,doc=? where id=?`
            let count = 0, countER = 0
            for (const row of rows) {
                let seq, s7, rs, id, pid, cER = 0, doc
                if (ENT == "scb") {
                    let docRS = await rbi(row.id)
                    for (const item of docRS.items) {
                        if (item.statusGD == 4) {
                            countER++
                            cER++
                            break
                        }
                    }
                    if (row.total < 0) {
                        cER++
                        countER++
                    }
                }
                if (cER != 0) {
                    continue
                }
                try {
                    id = row.id
                    seq = await SERIAL.sequence(taxc, form, serial, row.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    doc = await rbi(id)
                    doc.status = 2
                    doc.seq = s7
                    rs = await inc.execsqlinsupddel(sup, [JSON.stringify(doc), id, taxc])
                    if (rs > 0) {
                        count++
                        pid = row.pid
                        if (!util.isEmpty(pid)) {
                            try {
                                let pdoc = await rbi(pid)
                                if (ENT == "sgr" && row.adjtyp == 1) {
                                    let ret = await inc.execsqlinsupddel(`update s_inv set doc=? where id=?`, [JSON.stringify(pdoc), pid])
                                }
                                else {
                                    
                                    const cde = `${(row.adjtyp == 1 ? "Bị thay thế" : "Bị điều chỉnh")} bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${moment(row.idt).format(mfd)} mã ${ENT != "vcm" ? row.sec : row.c6}`
                                    let pdoc = await rbi(pid)
                                    pdoc.cde = cde
                                    if (row.adjtyp == 1) {
                                        let cdt = moment(new Date()).format(dtf)
                                        pdoc.cdetemp = cde
                                        pdoc.cdt = cdt
                                        pdoc.canceller = token.uid
                                        pdoc.status = 4

                                    }
                                    let ret = await inc.execsqlinsupddel(pup, [cde, JSON.stringify(pdoc), pid])
                                }
                            } catch (err) { logger4app.error(err) }
                        }
                    }
                    else throw new Error(`Không cấp số cho hóa đơn ${id} \n (Don't provide numbers for invoice ${id})`)
                    const rtolog = await rbi(id)
                    const sSysLogs = { fnc_id: 'inv_seqpost', src: config.SRC_LOGGING_DEFAULT, dtl: `Cấp số hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(rtolog) };
                    logging.ins(req, sSysLogs, next)
                } catch (err) {
                    throw err
                }
            }
            if (ENT == "scb") {
                res.json({ count: count, countER: countER })
            } else {
                res.json(count)
            }
        }
        catch (err) {
            next(err)
        }
        finally {
            if (!util.isEmpty(key)) await redis.del(key)
        }
    },
    apprseqpost: async (req, res, next) => {
        let key
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const wait = 60, taxc = token.taxc, body = req.body, ids = body.ids, form = body.form, serial = body.serial, idt = new Date(body.idt)
            let i = 1, str = "", binds = []
            binds = [idt, taxc, form, serial]
            let sql, result
            sql = `select id "id",pid "pid",ou "ou",adjtyp "adjtyp",idt "idt",sec "sec",doc "doc",bmail "bmail" from s_inv where idt<=? and stax=? and form=? and serial=? and status=1 order by idt,id`

            result = await inc.execsqlselect(sql, binds)
            const rows = result
            if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
            if (rows.length > config.MAX_ROW_APPROVE) throw new Error(`Dữ liệu ký duyệt vượt quá ${config.MAX_ROW_APPROVE} cho phép \n (Signing data exceeds ${config.MAX_ROW_APPROVE} characters for allowed)`)
            key = `APPR.${taxc}.${form}.${serial}`
            const val = await redis.get(key)
            if (val == "1") throw new Error(`Đang duyệt cấp số cho HĐ MST=${taxc} mẫu số=${form} ký hiệu=${serial}. Bạn hãy đợi ${wait}s ! \n ( Approving to sequence for invoice Tax.No=${taxc} Form=${form} Serial=${serial}. Should be waiting for ${wait}s !)`)
            await redis.set(key, "1", "EX", wait)

            const org = await ous.obt(taxc), signtype = org.sign
            if (signtype == 1) {
                let arr = []
                for (const row of rows) {
                    const doc = util.parseJson(row.doc), id = row.id, xml = util.j2x(doc, id)
                    // push {inc: id,...} thay vi {id: id,...} vì con ký anh Đại đang để là inc thay vì id CLOUD
                    arr.push({ inc: id, xml: Buffer.from(xml).toString("base64") })
                }
                res.json({ mst: taxc, date: new Date(), ref: config.PATH_XML, arr: arr })
            }
            else {
                let count = 0, seq, s7

                //sql = `update s_inv set doc=JSON_MODIFY(JSON_MODIFY(doc,'$.seq',@1),'$.status',3),xml=@2,dt=@3 where id=@4 and stax=@5 and status=1`
                sql = `update s_inv set doc=?,xml=?,dt=? where id=? and stax=? and status=1`

                if (signtype == 2) {
                    //hunglq chinh sua thong bao loi
                    let ca, pwd
                    try {
                        pwd = util.decrypt(org.pwd)
                        ca = await sign.ca(taxc, pwd)
                        for (const row of rows) {
                            let doc = util.parseJson(row.doc)
                            await sign.checkCABeforeSign(ca, doc.idt)
                        }
                    } catch (err) {
                        throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)')
                    }
                    await inc.checkrevokeAPI(taxc)
                    for (const row of rows) {
                        let doc = util.parseJson(row.doc)
                        const id = row.id

                        seq = await SERIAL.sequence(taxc, form, serial, row.idt)
                        s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                        doc.seq = s7
                        doc.status = 3
                        await sign.checkCABeforeSign(ca, doc.idt)
                        const xml = util.signature(sign.sign(ca, await util.j2x((await Service.getcol(doc)), id), id), doc.idt)
                        let ret = await inc.execsqlinsupddel(sql, [JSON.stringify(doc), xml, moment(new Date()).format(dtf), id, taxc])

                        if (doc.adj) {
                            let cdt = moment(new Date()).format(dtf)
                            if (!util.isEmpty(row.pid)) {
                                try {
                                    // let cde = `${(row.adjtyp == 1 ? "Bị thay thế" : "Bị điều chỉnh")} bởi HĐ số ${s7} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)} mã ${doc.sec}`
                                    // let pdoc = await rbi(row.pid)

                                    let cde = ''
                                    result = await inc.execsqlselect(`select doc "doc",cid "cid",cde "cde" from s_inv where id=?`, [row.pid])
                                    const rows = result
                                    let cid, pdoc = util.parseJson(rows[0].doc)
                                    if (row.adjtyp == 2) {
                                        if (rows[0].cde) cde = rows[0].cde + `_/_`
                                        else cde = `Bị điều chỉnh`
                                    }

                                    if (row.adjtyp == 1) {
                                        cde = `Bị thay thế bởi HĐ số ${s7} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)}`
                                    }
                                    if (rows[0].cid) cid = rows[0].cid + "," + row.id
                                    else cid = row.id

                                    //HungLQ them lan dieu chinh cho VCM, neu on on thi sua chung
                                    if (config.ent == "vcm" && row.adjtyp == 2) {
                                        if (pdoc.adjnum) {
                                            pdoc.adjnum = parseInt(pdoc.adjnum) + 1
                                        } else {
                                            pdoc.adjnum = 1
                                        }
                                        //   cde = `Lần điều chỉnh thứ ${pdoc.adjnum}.` + cde
                                    }
                                    if (config.ent == "vcm" && row.adjtyp == 1) {
                                        pdoc.cdetemp = null
                                        pdoc.cdt = cdt
                                        pdoc.canceller = token.uid
                                        pdoc.status = 4

                                    }
                                    pdoc.cde = cde
                                    ret = await inc.execsqlinsupddel(`update s_inv set cde=?,doc=?,cid=? where id=?`, [cde, JSON.stringify(pdoc), cid, row.pid])
                                } catch (err) { logger4app.error(err) }
                            }

                        }
                        doc.status = 3
                        if (useJobSendmail) {
                            await Service.insertDocTempJob(id, doc, xml)
                        }
                        const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                        logging.ins(req, sSysLogs, next)
                        count++
                    }
                }
                else {

                    let kqs, usr, pwd
                    try {
                        if (signtype == 3) {
                            usr = org.usr
                            pwd = util.decrypt(org.pwd)
                            kqs = await fcloud.xml(usr, pwd, taxc, rows)
                        }
                        else kqs = await hsm.xml(rows, "HD")
                    } catch (err) {
                        throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)')
                    }
                    for (const row of kqs) {
                        let doc = util.parseJson(row.doc)
                        const id = row.id, xml = util.signature(row.xml, doc.idt)
                        seq = await SERIAL.sequence(taxc, form, serial, row.idt)
                        s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                        doc.seq = s7
                        doc.status = 3
                        let ret = await inc.execsqlinsupddel(sql, [JSON.stringify(doc), xml, moment(new Date()).format(dtf), id, taxc])
                        if (doc.adj) {
                            let cdt = moment(new Date()).format(dtf)
                            if (!util.isEmpty(row.pid)) {
                                try {
                                    let cde = `${(row.adjtyp == 1 ? "Bị thay thế" : "Bị điều chỉnh")} bởi HĐ số ${s7} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)} mã ${ENT != "vcm" ? doc.sec : doc.c6}`
                                    //let cde = row.adjtyp == 1?` Bị thay thế bởi HĐ số ${s7} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)} mã ${doc.sec}`:``
                                    let pdoc = await rbi(row.pid)
                                    //HungLQ them lan dieu chinh cho VCM, neu on on thi sua chung
                                    if (config.ent == "vcm" && row.adjtyp == 2) {
                                        if (pdoc.adjnum) {
                                            pdoc.adjnum = parseInt(pdoc.adjnum) + 1
                                        } else {
                                            pdoc.adjnum = 1
                                        }
                                        //cde= `Lần điều chỉnh thứ ${pdoc.adjnum}.` + cde
                                    }
                                    if (config.ent == "vcm" && row.adjtyp == 1) {
                                        pdoc.cdetemp = null
                                        pdoc.cdt = cdt
                                        pdoc.canceller = token.uid
                                        pdoc.status = 4

                                    }
                                    pdoc.cde = cde
                                    ret = await inc.execsqlinsupddel(`update s_inv set cde=?,doc=? where id=?`, [cde, JSON.stringify(pdoc), row.pid])
                                } catch (err) { logger4app.error(err) }
                            }

                        }

                        doc.status = 3
                        if (useJobSendmail) {
                            await Service.insertDocTempJob(id, doc, xml)
                        }
                        const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                        logging.ins(req, sSysLogs, next)
                        count++
                    }
                }


                res.json(count)
            }
        }
        catch (err) {
            next(err)
        }
        finally {
            if (!util.isEmpty(key)) await redis.del(key)
        }
    },
    apprall: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, org = await ous.obt(taxc), signtype = org.sign
            let ret
            if (signtype !== 2) throw new Error("Chỉ hỗ trợ kiểu ký bằng file \n (Only support sign by file type)")
            const iwh = await Service.iwh(req, true)
            const sql = `select id "id",doc "doc",bmail "bmail",btel "btel" from s_inv ${iwh.where}`
            const result = await inc.execsqlselect(sql, iwh.binds)
            const rows = result
            if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
            if (rows.length > config.MAX_ROW_APPROVE) throw new Error(`Dữ liệu ký duyệt vượt quá ${config.MAX_ROW_APPROVE} cho phép \n (Signing data exceeds ${config.MAX_ROW_APPROVE} characters for allowed)`)
            let pwd, ca
            //hunglq chinh sua thong bao loi
            try {
                pwd = util.decrypt(org.pwd)
                ca = await sign.ca(taxc, pwd)
                
            } catch (err) {
                throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)')
            }
            await inc.checkrevokeAPI(taxc)
            try {
                for (const row of rows) {
                    let doc = util.parseJson(row.doc)
                    await sign.checkCABeforeSign(ca, doc.idt)
                }
            } catch(err) {
                throw new Error(err)
            }
            let count = 0
            const sql2 = `update s_inv set doc=?,xml=?,dt=? where id=? and status=2`
            for (const row of rows) {
                let doc = util.parseJson(row.doc), xml
                const id = row.id
                //const xml = util.signature(sign.sign(ca, await util.j2x((await Service.getcol(doc)), id), id), doc.idt)
                doc.adt = moment().format(dtf)
                if(doc.type =='02GTTT') doc.inv_ntz = 0
                if(doc.type =='07KPTQ') doc.inv_ntz = 1
                const str = await util.j2x(await Service.getcol(doc), id)
                , PATH_XML = await util.getPathXml(doc.type)
                , signSample = await util.getSignSample(doc.type, config.DEGREE_CONFIG == "123" ? doc.adt : doc.idt, id)
                await sign.checkCABeforeSign(ca, doc.idt)
                //if (util.isTicket(doc.type) && ts == "0") xml = str
                if (util.isTicket(doc.type)) xml = str
                else {
                    // xml = util.signature(sign.sign(ca, str, inc, PATH_XML, signSample), signSample)
                    xml = sign.sign123(ca, str, id, PATH_XML, signSample) //Tam thoi rem lai khi nao xong phan ky bo ra
                }
                doc.status_received  = 0
                doc.status = 3

                let result2 = await inc.execsqlinsupddel(sql2, [JSON.stringify(doc), xml, util.convertDate(moment(new Date()).format(dtf)), id])
                if (ENT == 'mzh') {
                    let filename = await ifile.getFileMailToFolder(doc.id)
                    ret = await inc.execsqlinsupddel("update s_inv set file_name=? where id=?", [filename, doc.id])
                } else {
                    if (result2 > 0) {
                        doc.status = 3
                        if((doc.serial).substring(0, 1) == 'K'){
                            if (useJobSendmail) {
                                await Service.insertDocTempJob(id, doc, xml)
                            } else if(useRedisQueueSendmail){
                                if (!util.isEmpty(doc.bmail)) {
                                    let mailstatus = await email.rsmqmail(doc)
                                    if (mailstatus) await Service.updateSendMailStatus(doc)
                                }
                                if (!util.isEmpty(doc.btel)) {
                                    let smsstatus = await sms.smssend(doc)
                                    if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                                }
                            }
                        }
                        count++
                    }
                }
                let  resultTvan = await inc.execsqlselect(`select sr.invtype,sr.sendtype from  s_serial sr where sr.form = ? and sr.serial=? and sr.taxc=? `, [doc.form,doc.serial,taxc])
                let row_sr = resultTvan[0]

                if(row_sr.sendtype == 1) 
                {
                    await inc.execsqlinsupddel(`INSERT INTO van_einv_listid_msg (pid, type, status_scan) values (?,?,?)`, [id, 'HD', 0])
                }
                const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                logging.ins(req, sSysLogs, next)
            }
            res.json(count)
        }
        catch (err) {
            next(err)
            console.trace(err)
        }
    },
    apprpost: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const taxc = token.taxc, body = req.body, ids = body.ids
            let i = 1, str = "", binds = [], ret
            for (const row of ids) {
                str += `?,`
                binds.push(row)
            }
            binds.push(taxc)
            let sql, result
            sql = `select id "id",doc "doc",bmail "bmail",btel "btel", pid "pid", type "type", form "form", serial "serial" from s_inv where id in (${str.slice(0, -1)}) and stax=? and status=2`
            result = await inc.execsqlselect(sql, binds)
            const rows = result
            if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
            if (rows.length > config.MAX_ROW_APPROVE) throw new Error(`Dữ liệu ký duyệt vượt quá ${config.MAX_ROW_APPROVE} cho phép \n (Signing data exceeds ${config.MAX_ROW_APPROVE} characters for allowed)`)
            const org = await ous.obt(taxc), signtype = org.sign
            if (signtype == 1) {
                let arr = []
                for (const row of rows) {
                    const doc = util.parseJson(row.doc), id = row.id, xml = await util.j2x(doc, id)
                    , signSample = await util.getSignSample(doc.type, config.DEGREE_CONFIG == "123" ? doc.adt : doc.idt, id)

                    // push {inc: id,...} thay vi {id: id,...} vì con ký anh Đại đang để là inc thay vì id CLOUD
                    // arr.push({ inc: id, xml: Buffer.from(xml).toString("base64") })
                    
                    let  str = xml ,xml_sign_time = signSample.signingTime
                        , xml_end_tag_replace = signSample.xmlEndTagReplace
                        , object_signing_time = `${xml_sign_time}${xml_end_tag_replace}`
                        // Add signature that have signing time to xml
                        str = str.replace(xml_end_tag_replace, object_signing_time)
                        str = str.replace(`<DLHDon>`,`<DLHDon Id="${id}">`)
                        arr.push({
                            id :id,
                            xml: Buffer.from(str).toString("base64"),
                            reference1:id,                            
                            reference2 :`SigningTime-${signSample.tagBegin}-${id}`,
                            tagSign:"DLHDon",
                            tagEnd:"</DLHDon>",
                            idt: moment(doc.idt).format(dtf)
                        })
                }
                res.json({ mst: taxc, date: new Date(), ref: config.PATH_XML, arr: arr })
            }
            else {
                let count = 0, seq, s7
                sql = `update s_inv set doc=?,xml=?,dt=? where id=? and stax=? and status=2`
                let subquery = `select pid "pid" from s_inv where id=? and pid is not null`

                if (signtype == 2) {
                    //hunglq chinh sua thong bao loi
                    let ca, pwd
                    try {
                        pwd = util.decrypt(org.pwd)
                        ca = await sign.ca(taxc, pwd)
                        
                    } catch (err) {
                        throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)')
                    }
                    await inc.checkrevokeAPI(taxc)
                    try {
                        for (const row of rows) {
                            let doc = util.parseJson(row.doc)
                            await sign.checkCABeforeSign(ca, doc.idt)
                        }
                    } catch (err) {
                        throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)')
                    }
                    for (const row of rows) {
                        let doc = util.parseJson(row.doc), xml
                        const id = row.id
                       // const tvan = require("./tvan")
                      
                        //check conect truoc khi duyet
                        // if(row_sr.sendtype != 2) {
                        //     await tvan.checkAPI();
                        // }
                        // util.viewadj(doc)
                        doc.adt = moment().format(dtf)
                        if(doc.type =='02GTTT') doc.inv_ntz = 0
                        if(doc.type =='07KPTQ') doc.inv_ntz = 1
                        const incid = row.id
                            , str = await util.j2x(await Service.getcol(doc), incid)
                            , PATH_XML = await util.getPathXml(doc.type)
                            , signSample = await util.getSignSample(doc.type, config.DEGREE_CONFIG == "123" ? doc.adt : doc.idt, incid)
                            , sendtype = row.sendtype
                        await sign.checkCABeforeSign(ca, doc.idt)
                        //if (util.isTicket(doc.type) && ts == "0") xml = str
                        if (util.isTicket(doc.type)) xml = str
                        else {
                            // xml = util.signature(sign.sign(ca, str, inc, PATH_XML, signSample), signSample)
                            xml = sign.sign123(ca, str, incid, PATH_XML, signSample) //Tam thoi rem lai khi nao xong phan ky bo ra
                        }
                        doc.status_received  = 0
                        doc.status = 3
                        sql = `update s_inv set doc=?,dt=?,xml=? where id=? and stax=? and status=2`
                        
                       ret = await inc.execsqlinsupddel(sql, [JSON.stringify(doc), moment(new Date()).format(dtf),xml, id, taxc])
                        // let paramsDataTVan = { 
                        //     ids: [incid],
                        //     type: ""
                        // }
                        row.hascode = doc.hascode
                        let  resultTvan = await inc.execsqlselect(`select sr.invtype,sr.sendtype from  s_serial sr where sr.form = ? and sr.serial=? and sr.taxc=? `, [doc.form,doc.serial,taxc]), row_sr = resultTvan[0]

                        if(row_sr.sendtype == 1) 
                        {
                            await inc.execsqlinsupddel(`INSERT INTO van_einv_listid_msg (pid, type, status_scan) values (?,?,?)`, [incid, 'HD', 0])
                        }
                        
                       // await tvan.dataTvanApi(token, paramsDataTVan, req)

                        //let  resulthashcode = await inc.execsqlselect(`select hascode from s_statement where cqtstatus=5 and status=2 and stax=? `, [taxc]), row_hc = resulthashcode[0]
                        if((doc.serial).substring(0, 1) == 'K'){
                            doc.status = 3
                            if (useJobSendmail) {
                                await Service.insertDocTempJob(id, doc, xml)
                            } else if(useRedisQueueSendmail ){
                                if (!util.isEmpty(doc.bmail)) {
                                    let mailstatus = await email.rsmqmail(doc)
                                    if (mailstatus) await Service.updateSendMailStatus(doc)
                                }
                                if (!util.isEmpty(doc.btel)) {
                                    let smsstatus = await sms.smssend(doc)
                                    if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                                }
                            }
                        }
                        
                        // if (!(row.hascode=="0" && row.sendtype=="2")) await tvan.dataTvanApi(token, paramsDataTVan, req)
                        
                        if(config.ent == "apple") {
                            // Thinhpq10 thêm trigger gửi email khi dải số đạt giới hạn
                            const ouserial = await ous.ousebytaxc(taxc, row.serial, row.form, row.type)
                            if(ouserial.cur && ouserial.max && ouserial.cur>ouserial.max* config.range_usage_serial) {
                                ouserial.serial = row.serial
                                ouserial.form = row.form
                                ouserial.type = row.type
                                ouserial.org = org
                                await ads.send_noti_serial_limit_apple(ouserial)
                            }
                            // Thinhpq10 insert dữ liệu vào bảng s_xml khi duyệt xong
                            sql = `insert into s_xml(id, doc, xml, type, status) values (?, ?, ?, ?, ?)`
                            // 1: create, 2: adjusted, 3:replace, 4: cancel, 5: pxk, 6: adjusted hdr, 7: replace pxk
                            let type_inv
                            if(row.type.includes("GTKT")) {
                                if(!row.pid) type_inv=1
                                else {
                                    let {adj} = row.doc
                                    if(adj && adj.typ==1) type_inv=2
                                    if(adj && adj.typ==2) type_inv=3
                                }
                                if(row.status==4) type_inv=4
                            }
                            if(row.type.includes("XKNB")) {
                                if(!row.pid) type_inv=5
                                else {
                                    let {adj} = row.doc
                                    if(adj && adj.typ==1) type_inv=6
                                    if(adj && adj.typ==2) type_inv=7
                                }
                            }
                            await inc.execsqlinsupddel(sql, [id, JSON.stringify(doc),xml, type_inv, 0])
                        }

                        const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                        logging.ins(req, sSysLogs, next)
                        count++

                    }
                }
                else {
                    //hunglq chinh sua thong bao loi
                    let kqs, usr, pwd
                    try {
                        if (signtype == 3) {
                            usr = org.usr
                            pwd = util.decrypt(org.pwd)
                            kqs = await fcloud.xml(usr, pwd, taxc, rows)
                        }
                        else kqs = await hsm.xml(rows, "HD")
                    } catch (err) {
                        throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)')
                    }
                    for (const row of kqs) {
                        let doc = util.parseJson(row.doc)
                        const id = row.id, xml = util.signature(row.xml, doc.idt)
                        const subResult = await inc.execsqlselect(subquery, [id])
                        let subRows = subResult
                        if (subRows.length > 0 && ENT == "sgr" && doc.adj && doc.adj.typ == 1) {
                            let pdoc
                            if (subRows[0].pid) {
                                try {
                                    pdoc = rbi(subRows[0].pid)
                                    const cde = `Bị thay thế bởi HĐ số ${doc.seq} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)} mã ${doc.sec}`
                                    let cdt = moment(new Date()).format(dtf)
                                    // let subQu=`update s_inv set doc=JSON_MODIFY(doc,'$.status',4) where id='${subRows[0].pid}'`
                                    // await dbs.query(subQu)
                                    pdoc.cdt = cdt
                                    pdoc.status = 4
                                    pdoc.cde = cde
                                    pdoc.canceller = token.uid
                                    ret = await inc.execsqlinsupddel(`update s_inv set doc=?,cid=?,cde=? where id=?`, [JSON.stringify(pdoc), id, cde, subRows[0].pid])
                                } catch (ex) {
                                    logger4app.debug(ex)
                                }
                            }
                            
                        }
                        doc.status_received  = 0
                        doc.status = 3
                        ret = await inc.execsqlinsupddel(sql, [JSON.stringify(doc), xml, util.convertDate(moment(new Date()).format(dtf)), id, taxc])
                        row.hascode = doc.hascode
                        let  resultTvan = await inc.execsqlselect(`select sr.invtype "invtype",sr.sendtype "sendtype" from  s_serial sr where sr.form = ? and sr.serial=? and sr.taxc=? `, [doc.form,doc.serial,taxc]), row_sr = resultTvan[0]
                        if(row_sr.sendtype == 1) 
                        {
                            await inc.execsqlinsupddel(`INSERT INTO van_einv_listid_msg (pid, type, status_scan) values (?,?,?)`, [id, 'HD', 0])
                        }
                        
                        if((doc.serial).substring(0, 1) == 'K'){
                            doc.status = 3
                            if (useJobSendmail) {
                                await Service.insertDocTempJob(id, doc, xml)
                            } else {
                                if (!util.isEmpty(doc.bmail)) {
                                    let mailstatus = await email.rsmqmail(doc)
                                    if (mailstatus) await Service.updateSendMailStatus(doc)
                                }
                                if (!util.isEmpty(doc.btel)) {
                                    let smsstatus = await sms.smssend(doc)
                                    if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                                }
                            }
                        }
                        const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                        logging.ins(req, sSysLogs, next)
                        count++
                    }
                }

                if (config.DEGREE_CONFIG == '123') {
                    res.json({ count: count, nd123: 'nd123' })
                } else res.json(count)
            }
        }
        catch (err) {
            next(err)
            console.trace(err)
        }
    },
    apprput: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, body = req.body, incs = body.incs, signs = body.signs
            let i = 1, str = "", binds = [], ret
            for (const row of incs) {
                str += `?,`
                binds.push(row)
            }
            binds.push(taxc)
            let sql, result, rows, count = 0
            sql = `select id "id",doc "doc",bmail "bmail",btel "btel", pid "pid" from s_inv where id in (${str.slice(0, -1)}) and stax=? and status=2 `
            result = await inc.execsqlselect(sql, binds)
            rows = result
            if (rows.length > 0) {
                sql = `update s_inv set doc=?,xml=?,dt=? where id=? and stax=? and status=2`
                for (const row of rows) {
                    let doc = util.parseJson(row.doc)
                    const id = row.id, sign = signs.find(item => item.incs === id)
                    if (sign && sign.xml) {
                        const xml = util.signature(Buffer.from(sign.xml, "base64").toString(), doc.idt)
                        doc.status = 3
                        ret = await inc.execsqlinsupddel(sql, [JSON.stringify(doc), xml, moment(new Date()).format(dtf), id, taxc])
                        if (useJobSendmail) {
                            await Service.insertDocTempJob(id, doc, xml)
                        }
                        doc.status_received  = 0
                        row.hascode = doc.hascode
                        let  resultTvan = await inc.execsqlselect(`select sr.invtype,sr.sendtype from  s_serial sr where sr.form = ? and sr.serial=? and sr.taxc=? `, [doc.form,doc.serial,taxc]), row_sr = resultTvan[0]

                        if(row_sr.sendtype == 1) 
                        {
                            await inc.execsqlinsupddel(`INSERT INTO van_einv_listid_msg (pid, type, status_scan) values (?,?,?)`, [incs, 'HD', 0])
                        }
                        const sSysLogs = { fnc_id: 'inv_apprall', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                        logging.ins(req, sSysLogs, next)

                        count++
                    }
                }
            }
            res.json(count)
        }
        catch (err) {
            next(err)
        }
    },
    iwh: async (req, all, decodereq) => {
        const token = (!decodereq) ? SEC.decode(req) : req.token, u = token.u, query = req.query, filter = util.parseJson(query.filter), sort = query.sort
        let keys = Object.keys(filter), kar = ["ou", "status", "status_received", "type", "form", "serial", "status_tbss", "paym", "curr", "cvt"], i = 4
        let tree = "", val, ext = "", extxls = "", cols, where, binds = [util.convertDate(moment(new Date(filter.fd)).format(dtf)), util.convertDate(moment(moment(filter.td).endOf("day")).format(dtf)), token.taxc], order, resultsgr, taxcsgr
        if (config.dbtype == "orcl") binds = [util.convertDate(filter.fd), util.convertDate(moment(filter.td).endOf("day")), token.taxc]
        if (ENT == "sgr" || ENT == "vcm") {
            where = `where idt  between ? and ?`
        } else where = `where idt  between ? and ? and stax=?`
        //  if(ENT=='scb')where +=` and c0 in (select system from s_user_sys where userid='${token.uid}') `

        if (grant && token.ou == u) {
            //TODO
            if (all) {
                //tree = `with tree as (select ${u} id union all select child.id from s_ou child,tree parent where parent.id=child.pid)`
                //where += ` and ou in (select id from tree)`
            }
            else {
                where += ` and ou=?`
                binds.push(u)
            }
        }
       // if (query.adjtypAll == "true") where += ` and ( (status = 3 and adjtyp in (1,2)) or status = 4)` // tim kiem hoa don thay the, dieu chinh, huy
        if (keys.includes("type")) {
            cols = await COL.cache(filter["type"])
            if (cols.length > 0) {
                let exts = []
                let extsxls = []
                for (const col of cols) {
                    let cid = col.id
                    exts.push(`${cid} "${cid}"`)
                    //Xu ly rieng cho excel neu la truong date thi format lai
                    if (col.view == "datepicker") {
                        switch (config.dbtype) {
                            case "mssql":
                                extsxls.push(`FORMAT(CONVERT(DATETIME, ${cid}, 102),'dd/MM/yyyy') "${cid}"`)
                                break
                            case "orcl":
                                extsxls.push(`TO_CHAR(TO_DATE(SUBSTR(${cid},1,10),'YYYY-MM-DD'),'${mfd}') "${String(cid).toLowerCase()}"`)
                                break
                            case "mysql":
                                extsxls.push(`DATE_FORMAT(STR_TO_DATE(${cid}, '%Y-%m-%d'),'%d/%m/%Y') "${cid}"`)
                                break
                            case "pgsql":
                                extsxls.push(`to_char(${cid},'DD/MM/YYYY') "${cid}"`)
                                break
                            default:
                                vsqlpager = ``
                                break
                        } 
                    }
                    else
                        extsxls.push(`${cid} "${cid}"`)
                }
                ext = `,${exts.join()}`
                extxls = `,${extsxls.join()}`
            }
        }
        for (const key of kar) {
            if (config.ent == "vcm" || config.ent == "sgr") {//tra cuu voi mutiselect
                if (keys.includes(key) && key.includes("serial")) {

                    val = filter[key]
                    let str = "", arr = String(val).split(",")
                    for (const row of arr) {
                        str += `'${row}',`
                    }
                    where += ` and ${key} in (${str.slice(0, -1)})`
                }
                else if (keys.includes(key) && key.includes("ou")) {

                    val = filter[key]
                    let str = "", arr = String(val).split(",")
                    for (const row of arr) {
                        let d = row.split("__")
                        str += `${d[0]},`
                    }
                    where += ` and ${key} in (${str.slice(0, -1)})`
                    if (ENT == "sgr" || ENT == "vcm") {
                        resultsgr = await inc.execsqlselect(`select taxc "taxc" from s_ou where id in (${str.slice(0, -1)})`)
                        if (resultsgr.length > 0) {
                            taxcsgr = resultsgr
                            let str = ""
                            for (const oldtaxcsgr of taxcsgr) {
                                str += `'${oldtaxcsgr.taxc}',`
                            }
                            where += ` and stax in (${str.slice(0, -1)})`

                        }

                    }
                    //binds.push(filter[key])
                } else {
                    if (keys.includes(key)) {
                        where += ` and ${key}=?`
                        binds.push(filter[key])
                    }
                }
            } else {
                if (keys.includes(key)) {
                    where += ` and ${key}=?`
                    binds.push(filter[key])
                }
            }

        }

        kar.push("fd", "td")
        for (const key of kar) {
            delete keys[key]
        }
        for (const key of keys) {
            val = filter[key]
            if (val && !kar.includes(key)) {
                switch (key) {
                    case "seq":
                        where += ` and seq like ?`
                        binds.push(`%${val}`)
                        break
                    case "btax":
                        where += ` and btax like ?`
                        binds.push(`%${val}%`)
                        break
                    case "bacc":
                        where += ` and bacc like ?`
                        binds.push(`%${val}%`)
                        break
                    case "id":
                        where += ` and id = ?`
                        binds.push(val)
                        break
                    case "list_invid":
                        where += ` and (list_invid = ? or list_invid_bx = ?)`
                        binds.push(val)
                        binds.push(val)
                        break
                    case "user":
                        where += ` and uc like ?`
                        binds.push(`%${val}%`)
                        break
                    case "th_type":
                        where += ` and id in (select distinct inv_id from s_inv_adj where th_type = ?)`
                        binds.push(val)
                        break
                    case "status_tbss":
                        where += ` and status_tbss = ?`
                        binds.push(val)
                        break
                    case "vat":
                        let catobj = util.parseJson(JSON.stringify(config.CATTAX))
                        let objtax = catobj.find(x => x.vatCode == val)
                        if (ENT == "vcm") {
                            where += ` and ((SELECT count(*) abc FROM OPENJSON (JSON_QUERY (doc,'$.tax')) WITH ( vrt numeric(26,4) '$.vrt',amt numeric(26,4) '$.amt',vat numeric(26,4) '$.vat') as a where (a.vrt =  @${i++} and (a.amt > 0 or (a.amt = 0 and a.vat != 0))) or (a.vrt =  @${i++} and  (TRY_CAST(json_value([doc],'$.score') AS numeric(26,4)) != 0 and a.amt > 0))) > 0)`
                            binds.push(String(objtax.vrt))
                            binds.push(String(objtax.vrt))
                        } else {
                            let catobj = util.parseJson(JSON.stringify(config.CATTAX))
                            let objtax = catobj.find(x => x.vatCode == val)
                            switch (config.dbtype) {
                                case "mssql":
                                    where += ` and ((SELECT count(*) abc FROM OPENJSON (JSON_QUERY (doc, '$.tax')) WITH ( vrt varchar(10) '$.vrt') as a  where a.vrt = ?) > 0 or (SELECT count(*) abc FROM OPENJSON (JSON_QUERY (doc, '$.tax')) WITH ( vrt varchar(10) '$.vrt') as a  where a.vrt = ?) > 0)`
                                    binds.push(String(objtax.vrt))
                                    binds.push(String(objtax.vrt))
                                    break
                                case "orcl":
                                    where += ` and instr(',' || replace(replace(replace(json_query(doc, '$.tax.vrt' with wrapper), '"', null), '[', null), ']', null), ?) > 0`
                                    binds.push(`,${String(objtax.vrt)}`)
                                    break
                                case "mysql":
                                    where += ` and (JSON_CONTAINS(doc->'$.tax[*].vrt',?)=1 or JSON_CONTAINS(doc->'$.tax[*].vrt',?)=1)`
                                    binds.push(`"${String(objtax.vrt)}"`)
                                    binds.push(`${String(objtax.vrt)}`)
                                    break
                                case "pgsql":
                                    where += ` and (doc->'tax') @> '[{"vrt":${String(objtax.vrt)}}]'`
                                    break
                                default:
                                    break
                            } 
                        }
                        break
                    case "adjtyp":
                        switch (val) {
                            case 3:
                                where += ` and cid is not null and cde like ?`
                                binds.push("Bị thay thế%")
                                break
                            case 4:
                                where += ` and cid is not null and cde like ?`
                                binds.push("Bị điều chỉnh%")
                                break
                            default:
                                where += ` and adjtyp=?`
                                binds.push(val)
                                break
                        }
                        break
                    case "ikind2":
                        switch (val) {
                            case 5:
                                where += ` and status=4 and cid is null`
                                break
                            case 3:
                                where += ` and cid is not null and cde like ?`
                                binds.push("Bị thay thế%")
                                break
                            case 4:
                                where += ` and cid is not null and cde like ?`
                                binds.push("Bị điều chỉnh%")
                                break
                         
                        }
                        break
                    case "sendmailstatus":
                        switch (val) {
                            case "0":
                                where += ` and sendmailstatus = ?`
                                binds.push(val)
                                break
                            case "1":
                                where += ` and sendmailstatus = ?`
                                binds.push(val)
                                break
                            default:
                                break
                        }
                        break
                    case "sendsmsstatus":
                        switch (val) {
                            case "0":
                                where += ` and sendmailstatus = ?`
                                binds.push(val)
                                break
                            case "1":
                                where += ` and sendsmsstatus = ?`
                                binds.push(val)
                                break
                            case "-1":
                                where += ` and sendsmsstatus = ?`
                                binds.push(val)
                                break
                            default:
                                break
                        }
                        break
                    case "mail":
                        break
                    default:
                        let b = true
                        if (CARR.includes(key) && cols) {
                            let obj = cols.find(x => x.id == key)
                            if (typeof obj !== "undefined") {
                                const view = obj.view
                                if (view == "datepicker") {
                                    b = false
                                    where += ` and date(${key})=?`
                                    binds.push(new Date(val))
                                }
                                else if (view == "multicombo") {
                                    b = false
                                    where += ` and ${key} in (${val.split(",")})`
                                }
                                else if (view == "combo") {
                                    b = false
                                    where += ` and ${key}=?`
                                    binds.push(val)
                                }
                                else if (view == "text") {
                                    if (obj.type == "number") {
                                        b = false
                                        where += ` and ${key} like ?`
                                        binds.push(`%${val}%`)
                                    }
                                }
                            }
                        }
                        if (b) {
                            where += ` and upper(${key}) like ?`
                            binds.push(`%${val.toString().toUpperCase()}%`)
                        }
                        break
                }
            }
        }
        // if (serial_usr_grant) {
        //     where += ` and stax = ss.taxc and type = ss.types and form = ss.forms and serial = ss.serials`
        // }
        if (serial_usr_grant) {
            where += ` and exists (select 1 from s_serial ss, s_seusr ss2 where ss.id = ss2.se and ss2.usrid = '${token.uid}' and s_inv.stax = ss.taxc and s_inv.type = ss.type and s_inv.form = ss.form and s_inv.serial = ss.serial)`

        }
        // if (ENT == "scb" && config.scb_dept_check) {
        //     where += ` and ((c2 like '') or exists (select 1 from s_deptusr dd where s_inv.c2 = dd.deptid and dd.usrid = '${token.uid}')) `
        // }
        if (sort) {
            Object.keys(sort).forEach(key => {
                order = `order by ${key} ${sort[key]}`
            })
        }
        else order = "order by idt desc,ABS(id) desc, type, form, serial, seq"
        return { where: where, binds: binds, ext: ext, order: order, extxls: extxls, paramindex: i }
    },
    xls: async (req, res, next) => {
        try {
            const query = req.query, filter = util.parseJson(query.filter)
            let json, rows, fn
            if (ENT == "mzh") {
                fn = "temp/KQTCHDMZH.xlsx"
            } else if (ENT == "ctbc") {
                fn = "temp/KQTCHD_CTBC.xlsx"
            } else if (config.DEGREE_CONFIG == "123"){
                fn = "temp/KQTCHD_nd123.xlsx"
            } else {
                fn = "temp/KQTCHD.xlsx"
            }
            const iwh = await Service.iwh(req, true)
            let cols = await COL.cacheLang(filter["type"], req.query.lang)
            let colhds = req.query.colhd.split(",")
            let extsh = []
            for (const col of cols) {
                let clbl = col.label
                extsh.push(clbl)
            }
            let sql = ''
            if (ENT == "mzh") {
                sql = `select id,sec,ic,uc,FORMAT(idt,'dd/MM/yyyy') idt,FORMAT(cdt,'dd/MM/yyyy') cdt,case when status=1 then N'Chờ cấp số' when status=2 then N'Chờ duyệt' when status=3 then N'Đã duyệt' when status=4 then N'Đã hủy' when status=5 then N'Đã gửi' when status=6 then N'Chờ hủy' end status,ou,type,form,serial,seq,btax,bname,buyer,baddr,note,sumv,vatv,totalv,sum,vat,total,adjdes,cde,bmail,curr,JSON_VALUE(JSON_QUERY (doc, '$.tax[0]'),'$.vrn') vrn,case when dds_sent=0 then N'Chưa gửi' when dds_sent=1 then N'Đã gửi' else  N'' end dds_sent,case when dds_stt=0 then N'Sent Successfully' when dds_stt=1 then N'Send Failed' when dds_stt=2 then N'Send To Custome' when dds_stt=3 then N'New' else  N'' end dds_stt,file_name filen,exrt exrt,sname sname${iwh.extxls} from s_inv ${iwh.where} ${iwh.order} `
            } else if (ENT == "ctbc") {
                sql = `select id,sec,ic,uc,FORMAT(idt,'dd/MM/yyyy') idt,FORMAT(cdt,'dd/MM/yyyy') cdt,case when status=1 then N'Chờ cấp số' when status=2 then N'Chờ duyệt' when status=3 then N'Đã duyệt' when status=4 then N'Đã hủy' when status=5 then N'Đã gửi' when status=6 then N'Chờ hủy' end status,ou,type,form,serial,seq,btax,bname,buyer,baddr,note,sumv,vatv,totalv,sum,vat,total,adjdes,cde,bmail,curr,sname sname${iwh.extxls}, bcode, bacc, error_msg_van,list_invid,sendmailstatus,status_received ,error_msg_van,CONCAT(CONCAT(list_invid, ';'),list_invid_bx)  list_invid from s_inv ${iwh.where} ${iwh.order} `
            } else {
                sql = `select si.id,sec,ic,uc,FORMAT(idt,'dd/MM/yyyy') idt,FORMAT(cdt,'dd/MM/yyyy') cdt,case when si.status=1 then N'Chờ cấp số' when si.status=2 then N'Chờ duyệt' when si.status=3 then N'Đã duyệt' when si.status=4 then N'Đã hủy' when si.status=5 then N'Đã gửi' when si.status=6 then N'Chờ hủy' end status,ou,type,form,serial,seq,btax,bname,buyer,baddr,note,sumv,vatv,totalv,sum,vat,total,adjdes,cde,bmail,curr,sname sname,case when sendmailstatus = 0 then N'Chưa gửi' when sendmailstatus = 1 then N'Đã gửi' end sendmailstatus,case when status_received  = 0 then N'Chờ gửi CQT' when status_received  = 1 then N'Gửi CQT' when status_received  = 2 then N'Gửi không thành công' when status_received  = 8 then N'Kiểm tra hợp lệ' when status_received  = 9 then N'Kiểm tra không hợp lệ' when status_received  = 10 then N'Đã cấp mã' end status_received ,error_msg_van,CONCAT(CONCAT(list_invid, ';'),list_invid_bx)  list_invid from s_inv si ${iwh.where} order by idt desc,ABS(si.id) desc, type, form, serial, seq `
            }
            const result = await inc.execsqlselect(sql, iwh.binds)
            rows = result
            //Them ham tra gia tri theo mang de hien thi tương ứng voi tung cot
            for (let row of rows) {

                row.sumv = parseInt(row.sumv)
                row.vatv = parseInt(row.vatv)
                row.totalv = parseInt(row.totalv)
                if (row.c0 == "01/01/1900") {
                    row.c0 = ""
                }
                if (row.c1 == "01/01/1900") {
                    row.c1 = ""
                }
                try {
                    row.ou = await oubi(row.ou)
                }
                catch (err) {

                }
                let extsval = []
                for (const col of cols) {
                    extsval.push(row[col.id])
                }
                row.extsval = extsval
                row = util.rvControlcharactersObject(row)
            }
            json = { table: rows, extsh: extsh, colhds: colhds }
            const file = path.join(__dirname, "..", "..", fn)
            const xlsx = fs.readFileSync(file)
            const template = new xlsxtemp(xlsx)
            template.substitute(1, json)
            res.end(template.generate(), "binary")
        }
        catch (err) {
            next(err)
        }
    },
    appget: async (req, res, next) => {
        try {
            const query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 10, iwh = await Service.iwh(req, false)
            let sql, result, ret, i = iwh.paramindex, pagerobj = inc.getsqlpagerandbind(start, count)
            let rea
            switch (config.dbtype) {
                case "mssql":
                    rea = `JSON_VALUE(doc, '$.cancel.rea') rea`
                    break
                case "orcl":
                    rea = `JSON_VALUE(doc, '$.cancel.rea') "rea"`
                    break
                case "mysql":
                    rea = `doc->>'$.cancel.rea' rea`
                    break
                case "pgsql":
                    rea = `doc -> 'cancel' ->> 'rea' rea`
                    break
                default:
                    break
            }
            sql = `select 0 "chk",id "id",sec "sec",ic "ic",uc "uc",curr "curr" ,pid "pid",cid "cid",idt "idt",status "status",ou "ou",type "type",form "form",serial "serial",seq "seq",btax "btax",bname "bname",buyer "buyer",baddr "baddr",note "note",sumv "sumv",vatv "vatv",totalv "totalv",sum "sum",vat "vat",total "total",adjdes "adjdes",adjtyp "adjtyp",cde "cde", cvt "cvt",${rea}, status_tbss "status_tbss", bmail "bmail"${iwh.ext},error_msg_van from s_inv ${iwh.where} ${iwh.order} ${pagerobj.vsqlpagerstr}`
            
            let binds = iwh.binds
            for (let t of pagerobj.vsqlpagerbind) binds.push(t)
            result = await inc.execsqlselect(sql, binds)
            ret = { data: result, pos: start }
            if (start == 0) {
                sql = `select count(*) "total" from s_inv ${iwh.where}`
                let lengthBinds = iwh.binds.length, bind2=[]
                for (let i = 0; i < lengthBinds - 2;i++) bind2.push(binds[i])
                result = await inc.execsqlselect(sql, bind2)
                ret.total_count = result[0].total
            }
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    get: async (req, res, next) => {
        try {
            //console.time("timeget")
            const query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 10, iwh = await Service.iwh(req, true)
            let sql, result, ret, i = iwh.paramindex, pagerobj = inc.getsqlpagerandbind(start, count)
            if (ENT == 'mzh') {
                sql = `select 0 chk,id,sec,ic,uc,curr,pid,cid,cast(idt as date) idt,cast(cdt as date) cdt,status,ou,type,form,serial,seq,btax,bname,buyer,baddr,note,sum sumv,vat vatv,total totalv,sum,vat,total,adjdes,adjtyp,cde,cvt,exrt,JSON_VALUE(JSON_QUERY (doc, '$.tax[0]'),'$.vrn') vrn,dds_sent,dds_stt,file_name filen,file_dds,bmail${iwh.ext}, error_msg_van,inv_adj from s_inv ${iwh.where} ${iwh.order} OFFSET  @${i++} ROWS FETCH NEXT @${i++} ROWS ONLY`

            } else {
                switch (ENT) {
                    case 'vcm': sql = `select 0 chk,id,sec,ic,uc,curr,pid,cid,cast(idt as date) idt,cast(cdt as date) cdt,status,ou,type,form,serial,seq,btax,bname,buyer,baddr,note,sumv,vatv,totalv,sum,vat,total,adjdes,adjtyp,cde,cvt,exrt,bmail${iwh.ext}, error_msg_van,inv_adj,CONCAT(CONCAT(list_invid, ';'),list_invid_bx) list_invid from s_inv WITH(INDEX(s_inv_idx_final)) ${iwh.where} ${iwh.order} OFFSET @${i++} ROWS FETCH NEXT @${i++} ROWS ONLY`
                        break
                    case 'ctbc': sql = `select 0 chk,id,sec,ic,uc,curr,pid,cid,cast(idt as date) idt,cast(cdt as date) cdt,status,ou,type,form,serial,seq,btax,bname,buyer,baddr,btel,note,sumv,vatv,totalv,sum,vat,total,adjdes,adjtyp,cde,cvt,exrt,bmail${iwh.ext} ,sendmailnum sendmailnum, case when sendmailstatus = 0 then N'Chưa gửi' when sendmailstatus = 1 then N'Đã gửi' end sendmailstatus, cast(sendmaildate as date) sendmaildate, sendsmsnum sendsmsnum, sendsmsstatus sendsmsstatus, cast(sendsmsdate as date) sendsmsdate, bacc,bcode,error_msg_van,inv_adj,CONCAT(CONCAT(list_invid, ';'),list_invid_bx) list_invid, status_received from s_inv ${iwh.where} ${iwh.order} OFFSET ? ROWS FETCH NEXT ? ROWS ONLY`
                        break
                    // case 'sgr': sql = `select 0 chk,id,sec,ic,uc,curr,pid,cid,cast(idt as date) idt,status,ou,type,form,serial,seq,btax,bname,buyer,baddr,btel,note,sumv,vatv,totalv,sum,vat,total,adjdes,adjtyp,cde,cvt,exrt,bmail,sendmailnum sendmailnum, sendmailstatus sendmailstatus,cast(sendmaildate as datetime) sendmaildatedetail, cast(sendmaildate as date) sendmaildate, sendsmsnum sendsmsnum, sendsmsstatus sendsmsstatus, cast(sendsmsdate as date) sendsmsdate from s_inv ${iwh.where} ${iwh.order} OFFSET @${i++} ROWS FETCH NEXT @${i++} ROWS ONLY`
                    //     break  
                    case 'sgr': sql = `select 0 chk,id,sec,ic,uc,curr,pid,cid,cast(idt as date) idt,cast(cdt as date) cdt,status,ou,type,form,serial,seq,btax,bname,buyer,baddr,btel,note,sumv,vatv,totalv,sum,vat,total,adjdes,adjtyp,cde,cvt,exrt,bmail,sendmailnum sendmailnum, sendmailstatus sendmailstatus,sendmaildate sendmaildate, sendsmsnum sendsmsnum, sendsmsstatus sendsmsstatus, sendsmsdate sendsmsdate${iwh.ext},error_msg_van,inv_adj,CONCAT(CONCAT(list_invid, ';'),list_invid_bx) list_invid from s_inv ${iwh.where} ${iwh.order} OFFSET @${i++} ROWS FETCH NEXT @${i++} ROWS ONLY`
                        break

                    default: sql = `select ${sql_get_01[`sql_get_01_${config.dbtype}`]}${iwh.ext} from s_inv ${iwh.where} ${iwh.order} ${pagerobj.vsqlpagerstr}`
                }
            }
            let binds = []
            for (let t of pagerobj.vsqlpagerbind) iwh.binds.push(t)
            result = await inc.execsqlselect(sql, iwh.binds)
            ret = { data: result, pos: start }
            const token = SEC.decode(req)
            for (let row of result) {
                let sqlu = `select sendtype "sendtype" from s_serial where taxc=? and type=? and serial=? and fd<=? and degree_config = ?`
                let resultu = await inc.execsqlselect(sqlu, [token.taxc, row.type, row.serial, util.convertDate(moment(new Date()).format(dtf)), config.DEGREE_CONFIG])
                if(resultu.length > 0) row.sendtype = resultu[0].sendtype
                if (row.sendtype && row.sendtype == 2) {
                    let resultInvAdj =  await inc.execsqlselect(`select id "id" from s_inv_adj where inv_id = ?`, [row.id])
                    if (resultInvAdj.length > 0) row.th_type = 1
                }
            }
            if (start == 0) {
                sql = `select count(*) "total" from s_inv ${iwh.where}`
                for (let i = 0; i < iwh.binds.length - 2;i++) binds.push(iwh.binds[i])
                result = await inc.execsqlselect(sql, binds)
                ret.total_count = result[0].total
            }
            //console.timeEnd("timeget")
            const sSysLogs = { fnc_id: 'inv_get', src: config.SRC_LOGGING_DEFAULT, dtl: `Tra cứu hóa đơn ${query.filter}`, msg_id: "", doc: query.filter };
            logging.ins(req, sSysLogs, next)
            res.json(ret)
        }
        catch (err) {
            logger4app.debug(err)
            next(err)
        }
    },
    api_search: async (req, res, next) => { //VinhHQ12 thêm từ mysql sang
        try {
            //console.time("time")
            const query = req.body
            let c0 = query.policy_number,sql,result
            logger4app.debug('_______________________api_search runing start_________________'+ new Date())
            sql = `select ${sql_api_search[`sql_api_search_${config.dbtype}`]}  from s_inv where c0=? and status in (3,4)`
            result = await inc.execsqlselect(sql, [c0])
           
             logger4app.debug('_______________________api_search stop_________________'+ new Date())
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    
    api_download: async (req, res, next) => {
        try {
            //console.time("time")
            const query = req.body
            let id = query.id,sql,result,doc
            logger4app.debug('_______________________api_download runing start_________________'+ new Date())
            sql = `select doc "doc" from s_inv where id=? and status in (3,4)`
            result = await inc.execsqlselect(sql, [Number(id)])
            if (result.length > 0) {
                doc = result[0].doc
                const status = doc.status,org = await ous.obt(doc.stax), idx = org.temp
                if (!([3, 4].includes(status))) doc.sec = ""
                // const fn = util.fn(doc.form, idx), tmp = await util.template(fn)
                const fn = doc.form.length > 1 ? util.fn(doc.form, idx): `${doc.type}.${doc.form}.${doc.serial}.${idx}`
                let tmp = await util.template(fn)
                let obj = { status: status, doc: doc, tmp: tmp }
                let reqjsr = ext.createRequest(obj)
                let pdf = await renderAsync(reqjsr)
                res.json({status:1,data:pdf.toString("base64")})
            }else{
                res.json('')
            }
            //Create PDF
            res.json(pdf.toString("base64"))
             logger4app.debug('_______________________api_download stop_________________'+ new Date())
            
        }
        catch (err) {
            next(err)
        }
    },
    signget: async (req, res, next) => {
        try {
            const token = SEC.decode(req), u = token.u
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const id = req.query.id, taxc = token.taxc
            let sql, result, binds = [id, taxc], ret
            if (ENT == 'vcm') {//ko co cho cap so
                sql = `select id "id",doc "doc",bmail "bmail",btel "btel", pid "pid" from s_inv where id=? and stax=? and status=1 `
            } else {
                sql = `select id "id",doc "doc",bmail "bmail",btel "btel", pid "pid" from s_inv where id=? and stax=? and status=2 `
            }

            if (grant) {
                sql += ' and ou=?'
                binds.push(u)
            }
            result = await inc.execsqlselect(sql, binds)
            const rows = result
            if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
            const org = await ous.obt(taxc), signtype = org.sign, row = rows[0]
            let doc = util.parseJson(row.doc)
            let xml, ca
            if (signtype == 1) {
                const usr = org.usr, pwd = util.decrypt(org.pwd)
                xml = util.j2x(doc, id)
                const arr = [{ inc: id, xml: Buffer.from(xml).toString("base64") }]
                res.json({ lib: usr, sid: 0, pin: pwd, mst: taxc, date: new Date(), ref: config.PATH_XML, arr: arr })
            }
            else {
                try {
                    if (signtype == 2) {
                        const pwd = util.decrypt(org.pwd)
                        ca = await sign.ca(taxc, pwd)
                        await inc.checkrevokeAPI(taxc)
                        await sign.checkCABeforeSign(ca, doc.idt)
                        xml = util.signature(sign.sign(ca, await util.j2x((await Service.getcol(doc)), id), id), doc.idt)
                    }
                    else if (signtype == 3) {


                        const usr = org.usr, pwd = util.decrypt(org.pwd)
                        const kqs = await fcloud.xml(usr, pwd, taxc, rows), kq = kqs[0]
                        xml = kq.xml


                    }
                    else {
                        const kqs = await hsm.xml(rows, "HD"), kq = kqs[0]
                        xml = util.signature(kq.xml, doc.idt)
                    }
                } catch (err) {
                    //logger4app.debug("signtype :"+ signtype +"_" + err.message)
                    throw new Error('Lỗi cấu hình chữ ký số không hợp lệ \n (Error of invalid digital signature configuration)')
                }

                if (ENT == 'vcm') {//ko co cho cap so
                    let seq = await SERIAL.sequence(taxc, doc.form, doc.serial, doc.idt)
                    let s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    doc.seq = s7
                    const xml = sign.sign(ca, util.j2x(doc, id), id)
                    doc.status = 3
                    ret = await inc.execsqlinsupddel(`update s_inv set doc=?,xml=?,dt=? where id=? and stax=? and status=1`, [JSON.stringify(doc), xml, moment(new Date()).format(dtf), id, taxc])
                    if (doc.adj && doc.adj.typ == 1) {
                        let cdt = moment(new Date()).format(dtf)
                        if (!util.isEmpty(row.pid)) {
                            try {
                                // let cde = `${(row.adjtyp == 1 ? "Bị thay thế" : "Bị điều chỉnh")} bởi HĐ số ${s7} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)} mã ${doc.sec}`
                                // let pdoc = await rbi(row.pid)

                                let cde = ''
                                result = await inc.execsqlselect(`select doc "doc",cid "cid",cde "cde" from s_inv where id=?`, [row.pid])
                                const rows2 = result
                                let cid, pdoc = util.parseJson(rows2[0].doc)
                                cde = `Bị thay thế bởi HĐ số ${doc.seq} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)}`

                                cid = doc.id


                                pdoc.cdetemp = null
                                pdoc.cdt = cdt
                                pdoc.canceller = token.uid
                                pdoc.status = 4
                                pdoc.cde = cde
                                await inc.execsqlinsupddel(`update s_inv set cde=?,doc=?,cid=? where id=?`, [cde, JSON.stringify(pdoc), cid, row.pid])
                            } catch (err) { logger4app.error(err) }
                        }

                    }

                } else {
                    doc.status = 3
                    await inc.execsqlinsupddel(`update s_inv set doc=?,xml=?,dt=? where id=? and stax=? and status=2`, [JSON.stringify(doc), xml, moment(new Date()).format(dtf), id, taxc])
                }
                if (ENT == "sgr" && doc.adj && doc.adj.typ == 1) {
                    let pdoc
                    if (!util.isEmpty(row.pid)) {
                        try {
                            pdoc = rbi(row.pid)
                            const cde = `Bị thay thế bởi HĐ số ${doc.seq} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)} mã ${doc.sec}`
                            let cdt = moment(new Date()).format(dtf)
                            pdoc.cdt = cdt
                            pdoc.status = 4
                            pdoc.cde = cde
                            pdoc.canceller = token.uid
                            ret = await inc.execsqlinsupddel(`update s_inv set doc=?,cid=?,cde=? where id=?`, [JSON.stringify(pdoc), id, cde, row.pid])
                        } catch (ex) {
                            logger4app.debug(ex)
                        }
                    }


                }
                doc.status = 3
                if (useJobSendmail) {
                    await Service.insertDocTempJob(doc.id, doc, xml)
                }
                const sSysLogs = { fnc_id: 'inv_signget', src: config.SRC_LOGGING_DEFAULT, dtl: `Ký số hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                logging.ins(req, sSysLogs, next)
                res.json(1)
            }

        }
        catch (err) {
            next(err)
        }
    },

    signput: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, body = req.body, signs = body.signs
            let sign = signs[0], id = sign.inc, xml = Buffer.from(sign.xml, "base64").toString()
            let result, ret
            if (ENT == 'vcm') {//ko co cho cap so
                result = await inc.execsqlselect(`select id "id",doc "doc",bmail "bmail",btel "btel", pid "pid" from s_inv where id=? and stax=? and status=1`, [id, taxc])
            } else {
                result = await inc.execsqlselect(`select id "id",doc "doc",bmail "bmail",btel "btel", pid "pid" from s_inv where id=? and stax=? and status=2`, [id, taxc])
            }
            const rows = result
            if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
            const row = rows[0]
            let doc = util.parseJson(row.doc)
            xml = util.signature(xml, doc.idt)
            doc.status = 3
            if (ENT == 'vcm') {//ko co cho cap so
                await inc.execsqlinsupddel(`update s_inv set doc=?,xml=?,dt=? where id=? and stax=? and status=1`, [JSON.stringify(doc), xml, moment(new Date()).format(dtf), id, taxc])

                if (doc.adj && doc.adj.typ == 1) {
                    let cdt = moment(new Date()).format(dtf)
                    if (!util.isEmpty(row.pid)) {
                        try {
                            // let cde = `${(row.adjtyp == 1 ? "Bị thay thế" : "Bị điều chỉnh")} bởi HĐ số ${s7} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)} mã ${doc.sec}`
                            // let pdoc = await rbi(row.pid)

                            let cde = ''
                            result = await inc.execsqlselect(`select doc "doc",cid "cid",cde "cde" from s_inv where id=?`, [row.pid])
                            const rows2 = result.recordset
                            let cid, pdoc = util.parseJson(rows2[0].doc)
                            cde = `Bị thay thế bởi HĐ số ${doc.seq} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)}`

                            cid = doc.id


                            pdoc.cdetemp = null
                            pdoc.cdt = cdt
                            pdoc.canceller = token.uid
                            pdoc.status = 4
                            pdoc.cde = cde
                            ret = await inc.execsqlinsupddel(`update s_inv set cde=?,doc=?,cid=? where id=?`, [cde, JSON.stringify(pdoc), cid, row.pid])
                        } catch (err) { logger4app.error(err) }
                    }

                }
            } else {
                ret = await inc.execsqlinsupddel(`update s_inv set doc=?,xml=?,dt=? where id=? and stax=? and status=2`, [JSON.stringify(doc), xml, moment(new Date()).format(dtf), id, taxc])
            }
            if (ENT == "sgr") {
                let subquery = `select pid "pid" from s_inv where id=? and pid is not null`
                const subResult = await inc.execsqlselect(subquery, [id])
                let subRows = subResult
                let pdoc
                if (!util.isEmpty(subRows[0].pid)) {
                    try {
                        pdoc = rbi(subRows[0].pid)
                        const cde = `Bị thay thế bởi HĐ số ${doc.seq} ký hiệu ${doc.serial} mẫu số ${doc.form} ngày ${moment(doc.idt).format(mfd)} mã ${doc.sec}`
                        let cdt = moment(new Date()).format(dtf)
                        pdoc.cdt = cdt
                        pdoc.status = 4
                        pdoc.cde = cde
                        pdoc.canceller = token.uid
                        ret = await inc.execsqlinsupddel(`update s_inv set doc=?,cid=?,cde=? where id=?`, [JSON.stringify(pdoc), id, cde, subRows[0].pid])
                    } catch (ex) {
                        logger4app.debug(ex)
                    }
                }
            }

            doc.status = 3
            if (useJobSendmail) {
                await Service.insertDocTempJob(doc.id, doc, xml)
            }
            const sSysLogs = { fnc_id: 'inv_signget', src: config.SRC_LOGGING_DEFAULT, dtl: `Ký số hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
            logging.ins(req, sSysLogs, next)
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },
    put: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, id = req.params.id
            const dt = moment(body.idt), eod = dt.endOf('day').toDate(), dtsc = dt.format('YYYYMMDD'), idtc = (moment(new Date())).format('YYYYMMDD'), taxc = token.taxc
            if (dtsc != idtc && !body.seq) {
                let result = await inc.execsqlselect(`select count(*) "rc" from s_inv where idt>? and form=? and serial=? and stax=? and (status in (2,3,5,6)${(!["sgr"].includes(config.ent)) ? ' or (status = 4 and xml is not null)' : ''})`, [util.convertDate(eod), body.form, body.serial, taxc])
                let row = result[0]
                if (row.rc > 0) throw new Error(`Tồn tại HĐ có ngày lớn hơn ${moment(body.idt).format('DD/MM/YYYY')} và đã cấp số \n (Invoice has a date larger than  ${moment(body.idt).format('DD/MM/YYYY')} and issued a number)`)
            }
            await chkou(id, token)
            if (ENT=='hsy' && body.stax == body.btax ) {
                // amount total amountv totalv = 0
                body.sum = 0 
                body.sumv = 0 
                body.total = 0 
                body.totalv = 0 
            }
            await Service.delrefno(body)
            const hdb = await Service.checkrefno(body)
            if (hdb.length > 0) throw new Error(`Số bút toán đã tồn tại: ${hdb.join()} \n(Entry number already exists: ${hdb.join()})`)
            const checkrefdate = await Service.checkrefdate(body)
            if (checkrefdate.length > 0) throw new Error(`Ngày bút toán lớn hơn ngày hóa đơn: ${checkrefdate.join(',')} \n(Ref Date is greater than Invoice Date: ${checkrefdate.join(',')})`)
            let ret = await inc.execsqlinsupddel(`update s_inv set doc=?,idt=? where id=? and status=?`, [JSON.stringify(body), util.convertDate(moment(body.idt).format("YYYY-MM-DD HH:mm:ss")), id, body.status])
            await Service.saverefno(body)
            const sSysLogs = { fnc_id: 'inv_put', src: config.SRC_LOGGING_DEFAULT, dtl: `Sửa hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(body) };
            logging.ins(req, sSysLogs, next)
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },
    ins: async (req, res, next) => {
        try {
            if (ENT == "sgr") {
                Service.insSGR(req, res, next)
            } else {
                const token = SEC.decode(req)
                if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
                const taxc = token.taxc, uid = token.uid, ou = token.ou, org = await ous.obt(taxc), AUTO = (org.seq === 1), body = req.body
                let invs = body.invs
                const sql = `insert into s_inv(id,pid,sec,ic,idt,ou,uc,doc) values (?,?,?,?,?,?,?,?)`
                //Thêm đoạn sort lại danh sách hóa đơn trước khi check số
                //Ghep dữ liệu cần sort
                for (let inv of invs) {
                    inv.sortprop = `#${inv.form}#${inv.serial}#${inv.seq}`
                }
                invs = util.sortobj(invs, 'sortprop', 0)
                //check rieng cho sgr khi hoa don co so
                let check_seq = true
                if (ENT == 'sgr') {
                    let sequence = 0, oldseq, oldform, oldserial
                    let seqc = invs[0].seq, formc = invs[0].form, serialc = invs[0].serial
                    for (let inv of invs) {
                        let seqc = inv.seq
                        if (Number(inv.seq) < Number(seqc)) {
                            seqc = inv.seq
                        }
                        if (sequence > 0) {
                            if ((Number(oldseq) + 1) != Number(inv.seq) && oldform == inv.form && oldserial == inv.serial) {
                                throw new Error(`Số hóa đơn không liên tiếp trên file excel \n The number of invoices is not continuous on Excel file `)
                            }
                        }
                        oldseq = inv.seq
                        oldform = inv.form
                        oldserial = inv.serial
                        sequence++
                    }
                    const uk = `${taxc}.${invs[0].form}.${invs[0].serial}`, key = `SERIAL.${uk}`
                    const curc = await redis.get(key)
                    if (Number(seqc) != Number(curc) + 1) throw new Error(`Số hóa đơn không liên tiếp với số trên hệ thống. Số hiện tại (${curc}) `)
                }
                if (AUTO) {
                    //Ghep dữ liệu cần sort
                    for (let inv of invs) {
                        inv.sortprop = `#${(moment(inv.idt, dtf).format('YYYYMMDD'))}#${inv.ic}`
                    }
                    invs = util.sortobj(invs, 'sortprop', 0)
                }
                for (let inv of invs) {
                    let ic, id, sec, form, serial, type, pid, idt, seq, s7, rs, tradeamount = 0
                    try {
                        form = inv.form
                        serial = inv.serial
                        type = config.DEGREE_CONFIG == "119" ? form.substr(0, 6) : inv.type
                        ic = inv.ic
                        inv.sid = ic
                        pid = inv.pid
                        idt = moment(inv.idt, dtf)
                        id = await redis.incr("INC")
                        sec = util.generateSecCode(config.FNC_GEN_SEC, id)
                        inv.id = id
                        inv.sec = sec
                        inv.type = type
                        inv.name = util.tenhd(type)
                        inv.stax = taxc
                        inv.sname = org.name
                        inv.saddr = org.addr
                        inv.smail = org.mail
                        inv.stel = org.tel
                        inv.taxo = org.taxo
                        inv.sacc = org.acc
                        inv.sbank = org.bank
                        for (let item of inv.items) {
                            if (item.type == 'Chiết khấu'){
                                tradeamount += Math.abs(item.amount)
                            }
                        }
                        const ouob = await ous.obid(ou)
                        inv.systemCode = ouob.code
                        let NUMBER_DECIMAL = 0
                            // inv.tradeamount = Math.round(tradeamount * Math.pow(10, NUMBER_DECIMAL)) / Math.pow(10, NUMBER_DECIMAL)
                        if (inv.curr = 'VND') {
                            inv.tradeamount = Math.round(tradeamount * Math.pow(10, NUMBER_DECIMAL)) / Math.pow(10, NUMBER_DECIMAL)
                        } else {
                            inv.tradeamount = parseFloat(tradeamount.toFixed(2))
                        }
                        if (config.DEGREE_CONFIG == "123") {
                            inv.hascode = (await inc.getstatementinfo(taxc)).hascode
                            inv.status_received = 0
                        }
                        if (ENT == 'vcm') {
                            inv.c6 = sec
                        }
                        const sqlcheck = `select count(*) "rcount" from s_inv where stax=? and (status in (2,3,5,6)${(!["sgr"].includes(config.ent)) ? ' or (status = 4 and xml is not null)' : ''}) and form=? and serial=? and idt>?`
                        const resultcheck = await inc.execsqlselect(sqlcheck, [taxc, form, serial, (moment(idt.toDate()).endOf("day")).toDate()])
                        const rowscheck = resultcheck
                        if (rowscheck[0].rcount > 0) throw new Error("Đã tồn tại hóa đơn được cấp số có ngày hóa đơn lớn hơn hóa đơn hiện tại \n (A numbered invoice already exists that has a larger invoice date than the current invoice)")

                        if (AUTO) {
                            seq = await SERIAL.sequence(taxc, form, serial, inv.idt)
                            s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                            inv.seq = s7
                            inv.status = 2
                        }
                        else {
                            s7 = "......."
                            inv.status = 1
                        }
                        if (ENT == 'sgr') {
                            inv.status = 2
                            s7 = inv.seq
                            if (inv.adjust) {
                                delete inv["sic"]
                                delete inv["adjust"]
                                delete inv["oseq"]
                                delete inv["des"]
                            }
                        }
                        //trungpq10 sưa lung so
                        try {
                            rs = await inc.execsqlinsupddel(sql, [id, pid, sec, ic, moment(idt.toDate()).format(dtf), ou, uid, JSON.stringify(inv)])
                        } catch (error) {
                            if (AUTO) {
                                await SERIAL.err(taxc, form, serial, seq)
                            }
                            throw error
                        }
                        //Đồng bộ lại serial nếu là SGR
                        if (ENT == 'sgr') {
                            await SERIAL.syncredisdbsgr(taxc, form, serial)
                        }
                        if (!rs || rs < 0) throw new Error(`Không cấp số cho hóa đơn ${id} \n (Don't provide numbers for invoice ${id})`)
                        const sSysLogs = { fnc_id: 'xls_upl', src: config.SRC_LOGGING_DEFAULT, dtl: 'Đọc hóa đơn từ excel', msg_id: id, doc: JSON.stringify(inv) };
                        logging.ins(req, sSysLogs, next)
                    } catch (e) {
                        throw e
                    }
                }
                res.json(invs.length)
            }
        }
        catch (err) {
            next(err)
        }
    },
    insSGR: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const taxc = token.taxc, uid = token.uid, ou = token.ou, org = await ous.obt(taxc), AUTO = (org.seq === 1), body = req.body
            let invs = body.invs
            //Thêm đoạn sort lại danh sách hóa đơn trước khi check số
            //Ghep dữ liệu cần sort
            for (let inv of invs) {
                inv.sortprop = `#${inv.form}#${inv.serial}#${inv.seq}`
                inv.sortidt = `${(moment(inv.idt, dtf).format('YYYYMMDD'))}`
            }
            invs = util.sortobj(invs, 'sortprop', 0)
            //check rieng cho sgr khi hoa don co so
            let check_seq = true, binds = [], rs
            let sequence = 0, oldseq, oldform, oldserial, oldidt
            let seqc = invs[0].seq, formc = invs[0].form, serialc = invs[0].serial
            for (let inv of invs) {
                let seqc = inv.seq, idtc = inv.sortidt
                if (Number(inv.seq) < Number(seqc)) {
                    seqc = inv.seq
                }
                if (sequence > 0) {
                    if ((Number(oldseq) + 1) != Number(inv.seq) && oldform == inv.form && oldserial == inv.serial) {
                        throw new Error(`Số hóa đơn không liên tiếp trên file excel \n The number of invoices is not continuous on Excel file `)
                    }
                    if (oldidt > idtc && oldseq < seqc) {
                        throw new Error(`Ngày hóa đơn không liên tiếp, theo thứ tự số hóa đơn \n (The invoice date is not continuous, in order of the number of invoices)`)
                    }
                }
                oldidt = inv.sortidt
                oldseq = inv.seq
                oldform = inv.form
                oldserial = inv.serial
                sequence++
            }
            const uk = `${taxc}.${invs[0].form}.${invs[0].serial}`, key = `SERIAL.${uk}`
            const curc = await redis.get(key)
            if (Number(seqc) != Number(curc) + 1) throw new Error(`Số hóa đơn không liên tiếp với số trên hệ thống. Số hiện tại (${curc}) `)
            if (AUTO) {
                //Ghep dữ liệu cần sort
                for (let inv of invs) {
                    inv.sortprop = `#${(moment(inv.idt, dtf).format('YYYYMMDD'))}#${inv.ic}`
                }
                invs = util.sortobj(invs, 'sortprop', 0)
            }
            let arrsql = [], arrserial = []
            for (let inv of invs) {
                let ic, id, sec, form, serial, type, pid, idt, seq, s7
                try {
                    form = inv.form
                    serial = inv.serial
                    type = form.substr(0, 6)
                    ic = inv.ic
                    pid = inv.pid
                    idt = moment(inv.idt, dtf)
                    id = await redis.incr("INC")
                    sec = util.generateSecCode(config.FNC_GEN_SEC, id)
                    inv.id = id
                    inv.sec = sec
                    inv.type = type
                    inv.name = util.tenhd(type)
                    inv.stax = taxc
                    inv.sname = org.name
                    inv.saddr = org.addr
                    inv.smail = org.mail
                    inv.stel = org.tel
                    inv.taxo = org.taxo
                    inv.sacc = org.acc
                    inv.sbank = org.bank
                    inv.ou = ou
                    const ouob = await ous.obid(ou)
                    inv.systemCode = ouob.code

                    if (ENT == 'vcm') {
                        inv.c6 = sec
                    }
                    if (AUTO) {
                        const sqlcheck = `select count(*) "rcount" from s_inv where stax=? and (status in (2,3,5,6)${(!["sgr"].includes(config.ent)) ? ' or (status = 4 and xml is not null)' : ''}) and form=? and serial=? and idt>?`
                        const resultcheck = await inc.execsqlselect(sqlcheck, [taxc, form, serial, (moment(idt.toDate()).endOf("day")).toDate()])
                        const rowscheck = resultcheck
                        if (rowscheck[0].rcount > 0) throw new Error("Đã tồn tại hóa đơn được cấp số có ngày hóa đơn lớn hơn hóa đơn hiện tại \n (A numbered invoice already exists that has a larger invoice date than the current invoice)")
                        seq = await SERIAL.sequence(taxc, form, serial, inv.idt)
                        s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                        inv.seq = s7
                        inv.status = 2
                    }
                    else {
                        s7 = "......."
                        inv.status = 1
                    }
                    if (ENT == 'sgr') {
                        inv.status = 2
                        s7 = inv.seq
                        if (inv.adjust) {
                            delete inv["sic"]
                            delete inv["adjust"]
                            delete inv["oseq"]
                            delete inv["des"]
                        }
                    }
                    binds.push([id, pid, sec, ic, moment(idt.toDate()).format(dtf), ou, uid, JSON.stringify(inv)])
                    arrserial.push({ taxc, form, serial })

                    if (pid) {
                        try {
                            let cde
                            if (inv.adj.typ == 2) {
                                cde = `Bị điều chỉnh bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${idt.format(mfd)} mã ${ENT != "vcm" ? inv.sec : inv.c6}`
                                let cdt = moment(idt.toDate()).format(dtf)
                                //await dbs.query(`update s_inv set doc=JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(doc,'$.cdt',@1),'$.status',3),'$.cde',@2),'$.canceller',@3),cid=@4,cde=@5 where id=@6 and status=3`, [cdt, cde, token.uid, id, cde, pid])
                                arrsql.push({ sql: `update s_inv set doc=JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(doc,'$.cdt',@1),'$.status',3),'$.cde',@2),'$.canceller',@3),cid=@4,cde=@5 where id=@6 and status=3`, bind: [cdt, cde, token.uid, id, cde, pid], slog: { fnc_id: 'xls_upl', src: config.SRC_LOGGING_DEFAULT, dtl: 'Đọc hóa đơn từ excel', msg_id: id, doc: JSON.stringify(inv) } })
                            } else if (inv.adj.typ == 1) {
                                cde = `Bị thay thế bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${idt.format(mfd)} mã ${ENT != "vcm" ? inv.sec : inv.c6}`
                                let cdt = moment(idt.toDate()).format(dtf)
                                arrsql.push({ sql: `update s_inv set doc=JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(doc,'$.cdt',@1),'$.status',4),'$.cde',@2),'$.canceller',@3),cid=@4,cde=@5 where id=@6 and status=3`, bind: [cdt, cde, token.uid, id, cde, pid], slog: { fnc_id: 'xls_upl', src: config.SRC_LOGGING_DEFAULT, dtl: 'Đọc hóa đơn từ excel', msg_id: id, doc: JSON.stringify(inv) } })
                                //await dbs.query(`update s_inv set doc=JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(doc,'$.cdt',@1),'$.status',4),'$.cde',@2),'$.canceller',@3),cid=@4,cde=@5 where id=@6 and status=3`, [cdt, cde, token.uid, id, cde, pid])
                            }

                        } catch (error) {
                            logger4app.error(error)
                        }
                    }

                } catch (e) {
                    if (AUTO) {
                        if (!((e.number == 2627) && (String(e.message).toUpperCase().indexOf("S_INV_UID_01") >= 0))) await SERIAL.err(taxc, form, serial, seq)
                    }
                    throw e
                }
            }
            rs = await dbs.queriestran(binds)
            //Đồng bộ lại serial nếu là SGR
            for (let ser of arrserial) {
                await SERIAL.syncredisdbsgr(ser.taxc, ser.form, ser.serial)
            }
            for (let s of arrsql) {
                await dbs.query(s.sql, s.bind)
                const sSysLogs = s.slog
                logging.ins(req, sSysLogs, next)
            }
            res.json(rs.rowsAffected[0])
        }
        catch (err) {
            next(err)
        }
    },
    upds: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const body = req.body, invs = body.invs
            for (let inv of invs) {
                try {
                    const pid = inv.pid, doc = await rbi(pid), idt = doc.idt
                    delete inv["pid"]
                    for (let i in inv) {
                        doc[i] = inv[i]
                    }
                    doc.idt = idt
                    let ret = await inc.execsqlinsupddel(`update s_inv set doc=? where id=? and status=2`, [JSON.stringify(doc), pid])
                    const sSysLogs = { fnc_id: 'inv_adj', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật hóa đơn id ${pid}`, msg_id: pid, doc: JSON.stringify(doc) };
                    logging.ins(req, sSysLogs, next)
                } catch (e) {
                    throw e
                }
            }
            res.json(invs.length)
        }
        catch (err) {
            next(err)
        }
    },
    wcan: async (req, res, next) => {
        const token = SEC.decode(req), taxc = token.taxc, body = req.body, invs = body.invs
        const sql = `update s_inv set doc=? where id=? and stax=? and status in (2,3)`
        for (const item of invs) {
            try {
                let doc = rbi(item.id), ret
                const cancel = { typ: 3, ref: item.ref, rdt: item.rdt, rea: item.rea }
                doc.cancel = cancel
                doc.status = 6
                const result = await inc.execsqlinsupddel(sql, [JSON.stringify(doc), item.id, taxc])
                if (result > 0) {
                    if (item.pid) ret = await inc.execsqlinsupddel(`update s_inv set cid=null,cde=null where id=?`, [item.pid])
                    item.error = 'OK'
                }
                else item.error = 'NOT OK'
            }
            catch (e) {
                item.error = e.message
            }
        }
        res.json({ save: true, data: invs })
    },
    sav: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body
            if (grant && token.ou !== token.u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)
            const hdb = await Service.checkrefno(body)
            if (hdb.length > 0) throw new Error(`Số bút toán đã tồn tại: ${hdb.join()} \n(Entry number already exists: ${hdb.join()})`)
            const checkrefdate = await Service.checkrefdate(body)
            if (checkrefdate.length > 0) throw new Error(`Ngày bút toán lớn hơn ngày hóa đơn: ${checkrefdate.join(',')} \n(Ref Date is greater than Invoice Date: ${checkrefdate.join(',')})`)

            const taxc = token.taxc, uid = token.uid, ou = token.ou, org = await ous.obt(taxc), form = body.form, serial = body.serial, id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC, id)
            let seq, s7, result, row
            const dt = moment(body.idt), eod = dt.endOf('day').toDate(), dts = dt.format(mfd), dtsc = dt.format('YYYYMMDD'), idtc = moment(new Date()).format('YYYYMMDD')
            try {
                if (dtsc != idtc &&  body.seq) {
                    result = await inc.execsqlselect(`select count(*) "rc" from s_inv where idt>? and form=? and serial=? and (status in (2,3,5,6)${(!["sgr"].includes(config.ent)) ? ' or (status = 4 and xml is not null)' : ''})`, [eod, form, serial])
                    row = result[0]
                    if (row.rc > 0) throw new Error(`Tồn tại HĐ có ngày lớn hơn ${moment(body.idt).format('DD/MM/YYYY')} và đã cấp số \n (Invoice has a date larger than ${moment(body.idt).format('DD/MM/YYYY')} and issued a number)`)
                }

                body.id = id
                body.sec = sec
                body.stax = org.taxc
                body.sname = org.name
                body.saddr = org.addr
                body.smail = org.mail
                body.stel = org.tel
                body.taxo = org.taxo
                body.sacc = org.acc
                body.sbank = org.bank

                const ouob = await ous.obid(ou)
                body.systemCode = ouob.code
                if (ENT != 'vcm') {
                    seq = await SERIAL.sequence(taxc, form, serial, body.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    body.seq = s7
                    if (ENT == 'onprem') {
                        body.status = 3
                    } else body.status = 2

                } else {
                    body.status = 1
                    body.c6 = sec

                }
                let ret = await inc.execsqlinsupddel(`insert into s_inv(id,sec,idt,ou,uc,doc) values (?,?,?,?,?,?)`, [id, sec, moment(body.idt).format("YYYY-MM-DD HH:mm:ss"), ou, uid, JSON.stringify(body)])
                await Service.saverefno(body)
                const sSysLogs = { fnc_id: 'inv_post', src: config.SRC_LOGGING_DEFAULT, dtl: `Lập hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(body) };
                logging.ins(req, sSysLogs, next)
                res.json(id)
            }
            catch (err) {
                throw err
            }
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body
            if (grant && token.ou !== token.u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)
            const hdb = await Service.checkrefno(body)
            if (hdb.length > 0) throw new Error(`Số bút toán đã tồn tại: ${hdb.join()} \n(Entry number already exists: ${hdb.join()})`)
            const checkrefdate = await Service.checkrefdate(body)
            if (checkrefdate.length > 0) throw new Error(`Ngày bút toán lớn hơn ngày hóa đơn: ${checkrefdate.join(',')} \n(Ref Date is greater than Invoice Date: ${checkrefdate.join(',')})`)
            const taxc = token.taxc, uid = token.uid, ou = token.ou, org = await ous.obt(taxc), AUTO = (org.seq === 1), form = body.form, serial = body.serial,serial23 = body.serial.slice(1,3) , id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC, id), ic = body.ic
            let seq, s7, result, row
            const dt = moment(body.idt), eod = dt.endOf('day').toDate(), dts = dt.format(mfd), dtsc = dt.format('YYYYMMDD'), idtc = (moment(new Date())).format('YYYYMMDD'), curYear = dtsc.slice(2,4)
            try {
                if (dtsc != idtc && body.seq) {
                    result = await inc.execsqlselect(`select count(*) "rc" from s_inv where idt>? and form=? and serial=? and stax=? and (status in (2,3,5,6)${(!["sgr"].includes(config.ent)) ? ' or (status = 4 and xml is not null)' : ''})`, [util.convertDate(eod), form, serial, taxc])
                    row = result[0]
                    if (row.rc > 0) throw new Error(`Tồn tại HĐ có ngày lớn hơn ${moment(body.idt).format('DD/MM/YYYY')} và đã cấp số \n (Invoice has a date larger than  ${moment(body.idt).format('DD/MM/YYYY')} and issued a number)`)
                }

                result = await inc.execsqlselect(`select count(*) "rc" from s_serial where fd>? and form=? and serial=? and status=1 and taxc=? `, [util.convertDate(eod), form, serial, taxc])
                row = result[0]
                if (row.rc > 0) throw new Error(`Ngày hiệu lực TBPH phải nhỏ hơn hoặc bằng ngày hóa đơn ${moment(body.idt).format('DD/MM/YYYY')}  \n (The serial effective date must be less than or equal to the invoice date ${moment(row.fd).format('DD/MM/YYYY')})`)
                if (serial23 != curYear) throw new Error(`Năm của ngày hóa đơn phải thuộc năm của ký hiệu hóa đơn`)
                if (config.ent == "apple"){
                    if (ic) {
                        let resultIC = await inc.execsqlselect (`select count(*) "ic" from s_inv where ic = ?`, [ic])
                        if (resultIC[0].ic > 0) throw new Error(`IC ${ic} đã tồn tại \n (IC already exists)`)
                    }
                    body.ic = ic
                    body.sid = ic
                }
                
                body.id = id
                body.sec = sec
                body.stax = org.taxc
                body.sname = org.name
                body.saddr = org.addr
                body.smail = org.mail
                body.stel = org.tel
                body.taxo = org.taxo
                body.sacc = org.acc
                body.sbank = org.bank
                for(const row of body.items ){
                    result = await inc.execsqlselect(`select COUNT(*) "rc" from s_serial ss where ss.taxc= ? and ss.type = ? and ss.form = ? and ss.serial =? and ss.invtype = 2`, [taxc,body.type,form,serial])
                    if (result[0].rc > 0 && !row.code) throw new Error(`Thiếu mã hàng hoá với hoá đơn đăng ký dải số là "Xăng dầu"\n (Lack of goods code with invoices for registration of digital strip is "gasoline")`)
                    if(!row.quantity) row.quantity = null
                    if(!row.price) row.price = null
                    if(!row.discountrate) row.discountrate = null
                    if(!row.discountamt) row.discountamt = null
                }
                const ouob = await ous.obid(ou)
                body.systemCode = ouob.code
                body.status_received = 0
                body.ma_cqthu = ""
                if (AUTO) {
                    seq = await SERIAL.sequence(taxc, form, serial, body.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    body.seq = s7
                    body.status = 2
                }
                else body.status = 1
                if (ENT == 'vcm') {
                    body.c6 = sec
                }
                if (ENT == 'apple') {
					//trungpq10 sưa lung so
                    try{
                        let ret = await inc.execsqlinsupddel(`insert into s_inv(id,ic,sec,idt,ou,uc,doc) values (?,?,?,?,?,?,?)`, [id, ic, sec, util.convertDate(moment(body.idt).format("YYYY-MM-DD HH:mm:ss")), ou, uid, JSON.stringify(body)])
                    } catch (error) {
                        if (AUTO) {
                            await SERIAL.err(taxc, form, serial, seq)
                        }
                        throw error
                    }		   
                }
                else {
					//trungpq10 sưa lung so
                    try{
                        let ret = await inc.execsqlinsupddel(`insert into s_inv(id,sec,idt,ou,uc,doc) values (?,?,?,?,?,?)`, [id, sec, new Date(moment(body.idt, dtf).toDate()), ou, uid, JSON.stringify(body)])
                    } catch (error) {
                        if (AUTO) {
                            await SERIAL.err(taxc, form, serial, seq)
                        }
                        throw error
                    }	
				}
                await Service.saverefno(body)
                const sSysLogs = { fnc_id: 'inv_post', src: config.SRC_LOGGING_DEFAULT, dtl: `Lập hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(body) };
                logging.ins(req, sSysLogs, next)
                res.json(id)
            }
            catch (err) {
                console.log(err)
                throw err
            }
        }
        catch (err) {
            console.log(err)
            next(err)
        }
    },
    adj: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, pid = body.pid, sumv = body.sumv
            await chkou(pid, token)
            const uid = token.uid, ou = token.ou, taxc = body.stax, form = body.form, serial = body.serial,serial23= body.serial.slice(1,3), org = await ous.obt(taxc), AUTO = (org.seq === 1), id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC, id), idt = moment(body.idt).format('YYYY-MM-DD 00:00:00'), eff_year= idt.slice(2,4), dt = moment(body.idt), eod = dt.endOf('day').toDate()
            let seq, s7, result, insid, ret,row
            result = await inc.execsqlselect(`select count(*) "rc" from s_serial where fd>? and form=? and serial=? and status=1 and taxc=? `, [util.convertDate(eod), form, serial, taxc])
            row = result[0]
            if (row.rc > 0) throw new Error(`Ngày hiệu lực TBPH phải nhỏ hơn hoặc bằng ngày hóa đơn ${moment(body.idt).format('DD/MM/YYYY')}  \n (The serial effective date must be less than or equal to the invoice date ${moment(row.fd).format('DD/MM/YYYY')})`)
            if(serial23 != eff_year) throw new Error(`Năm của ngày hóa đơn phải thuộc năm của ký hiệu hóa đơn`)
            if (ENT == "vcm") {//check tong tien nhieu hoa don dieu chinh cho 1 hoa don
                result = await inc.execsqlselect(`select doc "doc",cid "cid",pid "pid",adjtyp "adjtyp" from s_inv where id=?`, [pid])
                const rows = result
                if (!util.isEmpty(rows[0].pid) && body.sumv != 0 && rows[0].adjtyp != 1) throw new Error(`Chỉ được phép điều chỉnh thông tin cho hóa đơn điều chỉnh ${pid} \n (Adjustment is only allowed for adjustment invoice ${pid})`)
                if (body.dif && body.dif.sumv > 0) {

                    if (rows.length == 0) throw (new Error(`Không tìm thấy Hóa đơn gốc ${pid} \n (Cannot find the original invoice ${pid})`))
                    const cid = rows[0].cid, sumg = util.parseJson(rows[0].doc).sumv
                    if (cid) {
                        result = await inc.execsqlselect(`select doc "doc" from s_inv where id in (${cid})`, [])
                        const rowsi = result.recordset
                        if (rowsi.length == 0) throw (new Error(`Không tìm thấy Hóa đơn điều chỉnh của hóa đơn gốc ${cid} \n (Cannot found the invoice adjustment of the original invoice ${cid})`))
                        let suml = 0, sumi = 0
                        for (let row of rowsi) {
                            let doc = util.parseJson(row.doc)
                            if (doc.dif && doc.dif.sumv > 0) suml += doc.sumv
                            if (doc.dif && doc.dif.sumv < 0) sumi += doc.sumv
                        }
                        if (suml + sumv > sumg + sumi) throw (new Error(`Tổng thành tiền của các hóa đơn điều chỉnh giảm cho cùng một hóa đơn gốc phải <= Thành tiền của các hóa đơn điều chỉnh tăng cho cùng hóa đơn gốc + Thành tiền chưa thuế trên hóa đơn gốc ${pid} \n (The total amount of invoice reduction adjustments for the same original invoice must <= the total amount of invoice increasing adjustments for the same original invoice + the total amount without tax of the original invoice ${pid})`))
                    }
                }
                body.c6 = sec

            }
            try {
                body.id = id
                body.sec = sec
                body.stax = org.taxc
                body.sname = org.name
                body.saddr = org.addr
                body.smail = org.mail
                body.stel = org.tel
                body.taxo = org.taxo
                body.sacc = org.acc
                body.sbank = org.bank
                const ouob = await ous.obid(ou)
                body.systemCode = ouob.code
                body.ma_cqthu = ""
                if (AUTO) {
                    seq = await SERIAL.sequence(taxc, form, serial, body.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    body.seq = s7
                    body.status = 2
                    body.status_received = 0
                }
                else {
                    s7 = "......."
                    body.status = 1
                    body.status_received = 0
                }								
                //TRUNGPQ10 SUA LUNG SO
				 try {													
                result = await inc.execsqlinsupddel(`insert into s_inv (id,pid,sec,idt,ou,uc,doc) values (?,?,?,?,?,?,?)`, [id, pid, sec, idt, ou, uid, JSON.stringify(body)])
                } catch (error) {
                    if (AUTO) {
                        await SERIAL.err(taxc, form, serial, seq)
                    }
                    throw error
                }
				let cde = ''
                result = await inc.execsqlselect(`select doc "doc",cid "cid",cde "cde" from s_inv where id=?`, [pid])
                const rows = result
                let cid, pdoc = util.parseJson(rows[0].doc)
                // if (ENT == "vcm") {//nhieu điều chỉnh cho 1
                //     if (rows[0].cde) cde = rows[0].cde + `_/_`
                //     else cde = `Bị điều chỉnh`

                //     if (rows[0].cid) cid = rows[0].cid + "," + id
                //     else cid = id
                // } else {
                //     cde = `Bị điều chỉnh bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${moment(idt).format(mfd)} mã ${sec}.`
                //     cid = id
                // }
                if (rows[0].cde) cde = `Bị điều chỉnh` // VinhHQ12 sửa điều chỉnh nhiều hóa đơn
                else cde = `Bị điều chỉnh bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${moment(idt).format(mfd)} mã ${sec}.`

                if (rows[0].cid) cid = rows[0].cid + "," + id
                else cid = id

                pdoc.cde = cde

                ret = await inc.execsqlinsupddel(`update s_inv set cid=?,cde=?,doc=? where id=?`, [cid, cde, JSON.stringify(pdoc), pid])

                const sSysLogs = { fnc_id: 'inv_adj', src: config.SRC_LOGGING_DEFAULT, dtl: `Điều chỉnh hóa đơn id ${pid}`, msg_id: pid, doc: JSON.stringify(pdoc) };
                logging.ins(req, sSysLogs, next)
                res.json(id)
            }
            catch (err) {
                throw err
            }
        }
        catch (err) {
            next(err)
        }
    },
    adjniss: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, pid = body.pid, sumv = body.sumv

            await chkou(pid, token)
            const uid = token.uid, ou = token.ou, u = token.u, taxc = body.stax, form = body.form, serial = body.serial, org = await ous.obt(taxc), AUTO = (org.seq === 1), id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC, id), idt = new Date(body.idt)
            let seq, s7, result, insid, chk = 0, ret
            if (ENT == "vcm") {//check tong tien nhieu hoa don dieu chinh cho 1 hoa don
                result = await inc.execsqlselect(`select doc "doc",cid "cid",pid "pid",adjtyp "adjtyp" from s_inv where id=?`, [pid])
                let rows = result.recordset
                if (!util.isEmpty(rows[0].pid) && body.sumv != 0 && rows[0].adjtyp != 1) throw new Error(`Chỉ được phép điều chỉnh thông tin cho hóa đơn điều chỉnh ${pid} \n (Adjustment is only allowed for adjustment invoice ${pid})`)
                if (body.dif && body.dif.sumv > 0) {

                    if (rows.length == 0) throw (new Error(`Không tìm thấy Hóa đơn gốc ${pid} \n (Cannot find the original invoice ${pid})`))
                    const cid = rows[0].cid, sumg = util.parseJson(rows[0].doc).sumv
                    if (cid) {
                        result = await inc.execsqlselect(`select doc "doc" from s_inv where id in (${cid})`, [])
                        rows = result.recordset
                        if (rows.length == 0) throw (new Error(`Không tìm thấy Hóa đơn điều chỉnh của hóa đơn gốc ${cid} \n (Cannot found the invoice adjustment of the original invoice ${cid})`))
                        let suml = 0, sumi = 0
                        for (let row of rows) {
                            let doc = util.parseJson(row.doc)
                            if (doc.dif.sumv > 0) suml += doc.sumv
                            if (doc.dif.sumv < 0) sumi += doc.sumv
                        }
                        if (suml + sumv > sumg + sumi) throw (new Error(`Tổng thành tiền của các hóa đơn điều chỉnh giảm cho cùng một hóa đơn gốc phải <= Thành tiền của các hóa đơn điều chỉnh tăng cho cùng hóa đơn gốc + Thành tiền chưa thuế trên hóa đơn gốc ${pid} \n (The total amount of invoice reduction adjustments for the same original invoice must <= the total amount of invoice increasing adjustments for the same original invoice + the total amount without tax of the original invoice ${pid})`))
                    }
                }
                body.c6 = sec

            }
            try {
                body.id = id
                body.sec = sec
                const ouob = await ous.obid(ou)
                body.systemCode = ouob.code


                body.status = 1

                result = await inc.execsqlinsupddel(`insert into s_inv (id,pid,sec,idt,ou,uc,doc) values (?,?,?,?,?,?,?)`, [id, pid, sec, idt, ou, uid, JSON.stringify(body)])
                chk = 1
                //if (result.rowsAffected[0]) insid = result.recordset[0].id
                //const cde = `Bị điều chỉnh bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${moment(idt).format(mfd)} mã ${sec}.`
                //const pdoc = await rbi(pid)

                let cde = ''
                result = await inc.execsqlselect(`select doc "doc",cid "cid",cde "cde" from s_inv where id=?`, [pid])
                rows = result.recordset
                let cid, pdoc = util.parseJson(rows[0].doc)
                if (ENT == "vcm") {//nhieu điều chỉnh cho 1
                    if (rows[0].cde) cde = rows[0].cde + `_/_`
                    else cde = `Bị điều chỉnh`

                    if (rows[0].cid) cid = rows[0].cid + "," + id
                    else cid = id
                } else {
                    cde = `Bị điều chỉnh bởi HĐ số ${s7} ký hiệu ${serial} mẫu số ${form} ngày ${moment(idt).format(mfd)} mã ${sec}.`
                    cid = id
                }
                pdoc.cde = cde
                //HungLQ them lan dieu chinh cho VCM, neu on on thi sua chung
                if (config.ent == "vcm") {
                    if (pdoc.adjnum) {
                        pdoc.adjnum = parseInt(pdoc.adjnum) + 1
                    } else {
                        pdoc.adjnum = 1
                    }
                    // pdoc.cde = `Lần điều chỉnh thứ ${pdoc.adjnum}.` + cde
                }
                let cdt = moment(new Date()).format(dtf)
                pdoc.cdt = cdt
                pdoc.canceller = token.uid
                await inc.execsqlinsupddel(`update s_inv set cid=?,cde=?,doc=? where id=?`, [cid, cde, JSON.stringify(pdoc), pid])

                // ky xml
                let sql, binds = [id, taxc]
                sql = `select id "id",doc "doc",bmail "bmail" from s_inv where id=? and stax=? and status=1 `
                if (grant) {
                    sql += ' and ou=?'
                    binds.push(u)
                }
                result = await inc.execsqlinsupddel(sql, binds)
                rows = result
                if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
                const org = await ous.obt(taxc), signtype = org.sign, row = rows[0]
                let doc = util.parseJson(row.doc)
                let xml
                if (signtype == 1) {
                    const usr = org.usr, pwd = util.decrypt(org.pwd)
                    xml = util.j2x(doc, id)
                    const arr = [{ id: id, xml: Buffer.from(xml).toString("base64") }]
                    res.json({ lib: usr, sid: 0, pin: pwd, mst: taxc, date: new Date(), ref: config.PATH_XML, arr: arr })
                }
                else {
                    await inc.checkrevokeAPI(taxc)
                    try {
                        if (signtype == 2) {
                            const pwd = util.decrypt(org.pwd)
                            const ca = await sign.ca(taxc, pwd)
                            await sign.checkCABeforeSign(ca, doc.idt)
                            xml = sign.sign(ca, util.j2x(doc, id), id)
                        }
                        else if (signtype == 3) {
                            const usr = org.usr, pwd = util.decrypt(org.pwd)
                            const kqs = await fcloud.xml(usr, pwd, taxc, rows), kq = kqs[0]
                            xml = kq.xml
                        }
                        else {
                            const kqs = await hsm.xml(rows, "HD"), kq = kqs[0]
                            xml = kq.xml
                        }
                    } catch (err) {

                        // if(chk==1) err.message=`Đã lưu hóa đơn ${insid} .`+ err.message
                        throw new Error(`Đã lưu tạm hóa đơn ${id}. Cấu hình chữ ký số không hợp lệ ! \n (${id} has been temporarily saved. Invalid digital signature configuration!)`)
                    }
                    seq = await SERIAL.sequence(taxc, form, serial, body.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    body.seq = s7
                    body.status = 3
                    ret = await inc.execsqlinsupddel(`update s_inv set doc=?,xml=?,dt=? where id=? and stax=? and status=1`, [JSON.stringify(body), xml, util.convertDate(moment(new Date()).format(dtf)), id, taxc])
                    doc.status = 3
                    doc.seq = s7
                    if (useJobSendmail) {
                        await Service.insertDocTempJob(doc.id, doc, xml)
                    }

                    const sSysLogs = { fnc_id: 'inv_adj', src: config.SRC_LOGGING_DEFAULT, dtl: `Điều chỉnh hóa đơn id ${pid}`, msg_id: pid, doc: JSON.stringify(pdoc) };
                    logging.ins(req, sSysLogs, next)
                    res.json(id)
                }
            }
            catch (err) {
                if (AUTO) {
                    if (!((err.number == 2627) && (String(err.message).toUpperCase().indexOf("S_INV_UID_01") >= 0))) await SERIAL.err(taxc, form, serial, seq)
                }
                // if(chk==1) err.message=`Đã lưu hóa đơn ${insid} .`+ err.message
                throw err
            }
        }
        catch (err) {

            next(err)
        }
    },
    rep: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            let body = req.body, pid = body.pid
          
            await chkou(pid, token)
            const hdb = await Service.checkrefno(body)
            if (hdb.length > 0) throw new Error(`Số bút toán đã tồn tại: ${hdb.join()} \n (Entry number already exists: ${hdb.join()})`)
            const checkrefdate = await Service.checkrefdate(body)
            if (checkrefdate.length > 0) throw new Error(`Ngày bút toán lớn hơn ngày hóa đơn: ${checkrefdate.join(',')} \n(Ref Date is greater than Invoice Date: ${checkrefdate.join(',')})`)
            const uid = token.uid, ou = token.ou, taxc = body.stax, form = body.form, serial = body.serial, org = await ous.obt(taxc), AUTO = (org.seq === 1), id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC, id), idt = moment(new Date()).format('YYYY-MM-DD 00:00:00')
            let seq, s7, result, insid, ret
            try {
                if (config.ent == "jpm") {
                    let dt = moment(body.idt), eod = dt.endOf('day').toDate(), dtsc = dt.format('YYYYMMDD'), idtc = (moment(new Date())).format('YYYYMMDD')
                    if (dtsc != idtc && body.seq) {
                        result = await inc.execsqlselect(`select count(*) "rc" from s_inv where idt>? and form=? and serial=? and stax=? and (status in (2,3,5,6)${(!["sgr"].includes(config.ent)) ? ' or (status = 4 and xml is not null)' : ''})`, [util.convertDate(eod), form, serial, taxc])
                        let row = result[0]
                        if (row.rc > 0) throw new Error(`Tồn tại HĐ có ngày lớn hơn ${moment(body.idt).format('DD/MM/YYYY')} và đã cấp số \n (Invoice has a date larger than  ${moment(body.idt).format('DD/MM/YYYY')} and issued a number)`)
                    }
                }
                body.id = id
                body.sec = sec
                body.stax = org.taxc
                body.sname = org.name
                body.saddr = org.addr
                body.smail = org.mail
                body.stel = org.tel
                body.taxo = org.taxo
                body.sacc = org.acc
                body.sbank = org.bank
                const ouob = await ous.obid(ou)
                body.systemCode = ouob.code
                body.status_received = 0 
                body.ma_cqthu = ""
                // AutoSeq exists nghĩa là PHAT HANH ở THAY THE, voi vcm ky mới cấp số
                if ((AUTO || body.AutoSeq) && ENT != 'vcm') {
                    if (body.AutoSeq) delete body["AutoSeq"]
                    seq = await SERIAL.sequence(taxc, form, serial, body.idt)
                    s7 = seq.toString().padStart(config.SEQ_LEN, "0")
                    body.seq = s7
                    body.status = 2
                }
                else {
                    s7 = "......."
                    body.status = 1
                }
                if (ENT == 'vcm') body.c6 = sec
				 //TRUNGPQ10 SUA LUNG SO
				 try {																																											 
                result = await inc.execsqlinsupddel(`insert into s_inv (id,pid,sec,idt,ou,uc,doc) values (?,?,?,?,?,?,?)`, [id, pid, sec, util.convertDate(idt), ou, uid, JSON.stringify(body)])
				} catch (error) {
                    if (AUTO) {
                        await SERIAL.err(taxc, form, serial, seq)
                    }
                    throw error
                }		
                //if (result.rowsAffected[0]) insid = result.recordset[0].id
                const cde = `Bị thay thế bởi HĐ số ${s7} ký hiệu ${body.serial} mẫu số ${body.form} ngày ${moment(idt).format(mfd)} mã ${ENT != "vcm" ? sec : body.c6}`
                let cdt = moment(new Date()).format(dtf)
                let pdoc
                try {
                    pdoc = await rbi(pid)
                } catch (ex) {
                    logger4app.debug(ex)
                }
                if (pdoc) {
                    if (invoice_seq == 0 && ENT == "scb") {
                        pdoc.cdetemp = cde
                        pdoc.status = 4
                        ret = await inc.execsqlinsupddel(`update s_inv set doc=? where id=?`, [JSON.stringify(pdoc), pid])
                    } else if (invoice_seq == 0) {
                        pdoc.cdetemp = cde
                        ret = await inc.execsqlinsupddel(`update s_inv set doc=? where id=?`, [JSON.stringify(pdoc), pid])
                    } 
                     if (ENT != "sgr") {
                        pdoc.cdetemp = cde
                        pdoc.status = 4
                        pdoc.cdt = cdt
                        pdoc.canceller = token.uid
                        ret = await inc.execsqlinsupddel(`update s_inv set doc=?,cid=?,cde=? where id=?`, [JSON.stringify(pdoc), id, cde, pid])
                    }
                }
                await Service.saverefno(body)
                const sSysLogs = { fnc_id: 'inv_replace', src: config.SRC_LOGGING_DEFAULT, dtl: `Thay thế hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(body) };
                logging.ins(req, sSysLogs, next)
                res.json(id)
            }
            catch (err) {
				//trungpq10 rao đa sửa bên trên không chèn vào đây nữa
																																																   
               // if (AUTO) {
               //     if (!((err.number == 2627) && (String(err.message).toUpperCase().indexOf("S_INV_UID_01") >= 0))) await SERIAL.err(taxc, form, serial, seq)
               // }
                throw err
            }
        }
        catch (err) {
            next(err)
        }
    },
    go24: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, body = req.body, form = body.form, serial = body.serial, seq = body.seq, id = body.id, d = body.d, direct = d > 0 ? "tiến" : "lùi"
            await chkou(id, token)
            const idt = new Date(body.idt)
            idt.setDate(idt.getDate() + d)
            const dt = moment(idt), sod = dt.startOf('day').toDate(), eod = dt.endOf('day').toDate(), dts = dt.format(mfd)
            let result, row, ret
            if (d > 0) {
                const today = moment().endOf('day').toDate()
                if (eod > today) throw new Error(`Ngày tiến hóa đơn ${dts} lớn hơn ngày hiện tại \n (The forward date of the invoice ${dts} is greater than the current date)`)
            }
            else {
                result = await inc.execsqlselect(`select count(*) "rc" from s_serial where taxc=? and form=? and serial=? and fd>?`, [taxc, form, serial, eod])
                row = result[0]
                if (row.rc > 0) throw new Error(`Ngày lùi hóa đơn ${dts} không có TBPH \n (The backdate of the invoice ${dts} doesn't have released information)`)
            }
            if (seq) {
                const iseq = parseInt(seq)
                result = await inc.execsqlselect(`select count(*) "rc" from s_inv where idt<? and form=? and serial=? and (status=1 or ((status in (2,3,5,6)${(!["sgr"].includes(config.ent)) ? ' or (status = 4 and xml is not null)' : ''}) and ${sql_go24_01[`sql_go24_01_${config.dbtype}`]}>?))`, [sod, form, serial, iseq])
                row = result.recordset[0]
                if (row.rc > 0) throw new Error(`Hóa đơn không ${direct} được: tồn tại HĐ có ngày nhỏ hơn ${dts} chưa cấp số hoặc có số lớn hơn ${seq}  \n (Invoice can not ${direct}: Existing invoices with dates less than ${dts} and not issued a number or number greater than ${seq})`)
                result = await inc.execsqlselect(`select count(*) "rc" from s_inv where idt>? and form=? and serial=? and (status in (2,3,5,6)${(!["sgr"].includes(config.ent)) ? ' or (status = 4 and xml is not null)' : ''}) and ${sql_go24_01[`sql_go24_01_${config.dbtype}`]}<?`, [eod, form, serial, iseq])
                row = result.recordset[0]
                if (row.rc > 0) throw new Error(`Hóa đơn không ${direct} được: tồn tại HĐ có ngày lớn hơn ${dts} và số lớn hơn ${seq} \n (Invoice can not ${direct}: Existing invoice has a date larger than ${dts} and a number larger ${seq}) `)
            }
            else {
                result = await inc.execsqlselect(`select count(*) "rc" from s_inv where idt>? and form=? and serial=? and (status in (2,3,5,6)${(!["sgr"].includes(config.ent)) ? ' or (status = 4 and xml is not null)' : ''})`, [eod, form, serial])
                row = result.recordset[0]
                if (row.rc > 0) throw new Error(`Hóa đơn không ${direct} được : tồn tại HĐ có ngày lớn hơn ${dts} và đã cấp số \n (Invoice can not ${direct}: Existing invoice has a date larger than${dts} and issued a number)`)
            }
            let doc = await rbi(id)
            ret = await inc.execsqlinsupddel(`update s_inv set idt=?,doc=? where id=? and status in (1, 2)`, [sod, JSON.stringify(doc), id])
            res.json(1)
        } catch (err) {
            next(err)
        }
    },
    status: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, id = req.params.id
            let prstatus = await inc.execsqlselect(`select status "status" from s_inv where id=${id}`), ret
            if (prstatus[0].status == 4) {
                throw new Error("Hóa đơn này đã được duyệt hủy \n (This invoice has been approved for cancellation)")
            } else {
                let doc, status = req.params.status
                await chkou(id, token)
                doc = await rbi(id)
                let oldstatus = doc.status
                if (status == 3 || status == 2) {
                    let doctmp = util.parseJson(JSON.stringify(doc))
                    delete doctmp["cancel"]
                    ret = await inc.execsqlinsupddel(`update s_inv set doc=? where id=?`, [JSON.stringify(doctmp), id])
                    const r = await inc.execsqlselect(`select 1 from s_inv where id=? and xml is not null`, [id])
                    if (r.length > 0 && config.ent != "scb") status = 3
                    else status = 2
                    //Xóa biên bản
                    if (UPLOAD_MINUTES) ret = await inc.execsqlinsupddel(`DELETE FROM s_inv_file where id=?`, [id])
                }
                else if (status == 4) {
                    doc = await rbi(id)
                    let doctmp = util.parseJson(JSON.stringify(doc))
                    const cancel = { typ: 3, ref: id, rdt: moment().format(dtf) }
                    if (doc.pid && doc.adj.typ == 2) ret = await inc.execsqlinsupddel(`update s_inv set cid=null,cde=null where id=?`, [doc.pid])
                    let cdt = moment(new Date()).format(dtf)
                    doctmp.cdt = cdt
                    doctmp.canceller = token.uid
                    doctmp.cancel = cancel
                    ret = await inc.execsqlinsupddel(`update s_inv set doc=? where id=? and stax=?`, [JSON.stringify(doctmp), id, taxc])
                   // await insListInvCancelCaseReplace(token,id,cdt)
                }
                doc.status = status
                let result = await inc.execsqlinsupddel(`update s_inv set doc=? where id=? and stax=?`, [JSON.stringify(doc), id, taxc])
                if (result > 0 && (oldstatus == 3 || oldstatus == 6)) {
                    if (useJobSendmail) {
                        if (((status == 4) && !util.isEmpty(doc.bmail)) || ((status == 4) && !util.isEmpty(doc.btel))) {
                            doc.status = 4
                            await Service.insertDocTempJob(doc.id, doc, ``)
                        }
                    }
                    const sSysLogs = { fnc_id: 'inv_istatus', src: config.SRC_LOGGING_DEFAULT, dtl: `Chuyển đổi trạng thái hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
                    logging.ins(req, sSysLogs, next)
                }
                res.json(1)
            }
        } catch (err) {
            next(err)
        }
    },
    cancel: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, id = req.params.id
            await chkou(id, token)
            let doc = await rbi(id), cancel = { typ: body.typ, ref: body.ref, rdt: body.rdt, rea: body.rea }, ret
            let result, rows
            if (doc.pid && doc.adj.typ == 2) {
                result = await inc.execsqlselect(`select cid "cid",cde "cde" from s_inv where id=?`, [doc.pid])
                rows = result
                let cid = rows[0].cid
                if (cid && cid.split(',').length > 1) {
                    let cids = cid.split(','), cidst = ''
                    for (let c of cids) {
                        if (c != id) cidst = cidst + ',' + c
                    }
                    cidst = cidst.slice(1)
                    ret = await inc.execsqlinsupddel(`update s_inv set cid=? where id=?`, [cidst, doc.pid])
                } else {
                    ret = await inc.execsqlinsupddel(`update s_inv set cid=null,cde=null where id=?`, [doc.pid])
                }

            }
            let cdt = moment(new Date()).format(dtf)
            doc = await rbi(id)
            let oldstatus = doc.status
            if (ENT == 'sgr') {
                doc.cdt = doc.idt
            } else doc.cdt = cdt
            doc.canceller = token.uid
            doc.status = 4
            doc.cancel = cancel
            result = await inc.execsqlinsupddel(`update s_inv set doc=? where id=? and stax=?`, [JSON.stringify(doc), id, token.taxc])
            await insListInvCancelCaseReplace(token,id,util.convertDate(cdt))
            if (ENT == 'mzh') {
                ret = await inc.execsqlinsupddel(`update s_trans set status=4,seq=? where inv_id=?`, [doc.seq,id])

            }
            if (result && oldstatus == 3) {
                if (useJobSendmail) {
                    if ((!util.isEmpty(doc.bmail)) || (!util.isEmpty(doc.btel))) {
                        doc.status = 4
                        await Service.insertDocTempJob(doc.id, doc, ``)
                    }
                }
            }
            const sSysLogs = { fnc_id: 'inv_cancel', src: config.SRC_LOGGING_DEFAULT, dtl: `Hủy hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doc) };
            logging.ins(req, sSysLogs, next)
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },
    trash: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, id = req.params.id
            await chkou(id, token)
            //Lay du lieu doc truoc khi xoa de luu log
            let doclog = await rbi(id), doc = JSON.parse(JSON.stringify(doclog))
            const cancel = { typ: body.typ, ref: body.ref, rdt: body.rdt, rea: body.rea }
            doc.cancel = cancel
            doc.status = 6
            let ret = await inc.execsqlinsupddel(`update s_inv set doc=? where id=? and stax=? and status in (2,3)`, [JSON.stringify(doc), id, token.taxc])
            res.json(1)
            const sSysLogs = { fnc_id: 'inv_adj', src: config.SRC_LOGGING_DEFAULT, dtl: `Cập nhật hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doclog) };
            logging.ins(req, sSysLogs, next)
        }
        catch (err) {
            next(err)
        }
    },
    trashAll: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, invs = body.invs, note = body.note
            for (let inv of invs) {
                let doc = await rbi(inv.id)
                const cancel = { typ: 3, ref: inv.id, rdt: moment(new Date()).format("YYYY-MM-DD"), rea: note }
                doc.status = 6
                doc.cancel = cancel
                let ret = await inc.execsqlinsupddel(`update s_inv set doc=? where id=? and stax=? and status in (2,3)`, [JSON.stringify(doc), inv.id, token.taxc])

            }

            res.json(1)

        }
        catch (err) {
            next(err)
        }
    },
    istatus: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, body = req.body, ids = body.ids, os = parseInt(body.os), now = moment().format(dtf)
            let rc = 0, ns = body.ns, ret
            for (let id of ids) {
                try {
                    let doc, sql, binds, doclog, sSysLogs
                    doclog = await rbi(id)
                    let oldstatus = doclog.status
                    if (ns == 4) {
                        const re = await inc.execsqlselect(`select doc "doc",cdt "cdt",seq "seq" from s_inv where id=?`, [id])
                        const rws = re
                        if (rws.length == 0) throw new Error(`Không tìm thấy Hóa đơn ${id} \n (Invoice not found ${id})`)
                        doc = util.parseJson(rws[0].doc)
                        doc.cdt = moment(new Date()).format(dtf)
                        //Lay du lieu doc truoc khi xoa de luu log
                        doclog = await rbi(id)
                        let cdt = rws[0].cdt
                        if (cdt) {
                            doc.status = ns
                            sql = `update s_inv set doc=? where id=? and stax=? and status=?`
                            binds = [JSON.stringify(doc), id, taxc, os]
                        } else {
                            if (ENT == 'sgr') {
                                cdt = doc.idt
                            } else cdt = moment(new Date()).format(dtf)
                            doc.status = ns
                            doc.cdt = cdt
                            doc.canceller = token.uid
                            sql = `update s_inv set doc=? where id=? and stax=? and status=?`
                            binds = [JSON.stringify(doc), id, taxc, os]

                        }
                        if (ENT == 'mzh') {
                            ret = await inc.execsqlinsupddel(`update  s_trans set status=4,seq=? where inv_id=?`, [id, rws[0].seq])

                        }
                        if (doc.pid && doc.adj.typ == 2) ret = await inc.execsqlinsupddel(`update s_inv set cid=null,cde=null where id=?`, [doc.pid])
                        if (!doc.cancel) {
                            doc.cancel = { typ: 3, ref: id, rdt: now }
                            ret = await inc.execsqlinsupddel(`update s_inv set doc=? where id=? and stax=? and status=?`, [JSON.stringify(doc), id, taxc, os])
                        }
                        await insListInvCancelCaseReplace(token,id,cdt)
                        sSysLogs = { fnc_id: 'inv_cancel', src: config.SRC_LOGGING_DEFAULT, dtl: `Hủy hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doclog) };
                    } else {
                        //Lay du lieu doc truoc khi xoa de luu log
                        doclog = await rbi(id)
                        doc = util.parseJson(JSON.stringify(doclog))
                        if (ns == 2 || ns == 3) {
                            const r = await inc.execsqlselect(`select 1 from s_inv where id=? and xml is not null`, [id])
                            if (r.length > 0) ns = 3
                            else ns = 2
                        }
                        doc.status = ns
                        sql = `update s_inv set doc=? where id=? and stax=? and status=?`
                        binds = [JSON.stringify(doc), id, taxc, os]
                        sSysLogs = { fnc_id: 'inv_istatus', src: config.SRC_LOGGING_DEFAULT, dtl: `Chuyển đổi trạng thái hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doclog) };
                    }
                    const result = await inc.execsqlinsupddel(sql, binds)
                    if (result > 0 && (oldstatus == 3 || oldstatus == 6 || oldstatus == 2)) {
                        if (useJobSendmail) {
                            if (((ns == 4) && !util.isEmpty(doc.bmail)) || ((ns == 4) && !util.isEmpty(doc.btel))) {
                                doc.status = 4
                                await Service.insertDocTempJob(doc.id, doc, ``)
                            }
                        }
                        rc++
                    }

                    logging.ins(req, sSysLogs, next)
                } catch (e) {
                    logger4app.error(e)
                }
            }
            res.json(rc)
        }
        catch (err) {
            next(err)
        }
    },
    del: async (req, res, next) => {
        try {
            const token = SEC.decode(req), id = req.params.id, pid = req.params.pid
            await chkou(id, token)
            //Lay du lieu doc truoc khi xoa de luu log
            let doclog = await rbi(id), ret
            if (ENT == "scb") {
                for (const item of doclog.items) {
                    if (item.tranID) {
                        ret = await inc.execsqlinsupddel(`update s_trans set status = ?, inv_id = null, inv_date = null where tranID = ?`, [item.statusGD, item.tranID])
                    }
                }
            }
            const result = await inc.execsqlinsupddel(`delete from s_inv where id=? and stax=? and status=?`, [id, token.taxc, 1])
            if (result > 0 && pid > 0) {
                let doc = await rbi(pid)
                doc.status = 3
                //Xóa doc.cde , nếu không có cũng ko ảnh hưởng.
                delete doc["cde"]
                delete doc["cdt"]
                delete doc["canceller"]
                // await dbs.query(`update s_inv set doc=JSON_MODIFY(doc,'$.status',3),cid=null,cde=null where id=@1`, [pid])

                let sqlpinv = await inc.execsqlselect(`select cid,cde from s_inv where id=?`, [pid]), cid, cde
                if(sqlpinv && sqlpinv.length >0) {
                    cid =  sqlpinv[0].cid
                    cde =  sqlpinv[0].cde
                }
                if(cid){
                    cid = cid.toString().split(',').filter(e=> e!=id).join(',') // Xóa id của hd cần xóa trong trường cid ở hóa đơn gốc
                    if(!cid){
                        cde=null
                        cid=null
                    } else {
                        cde =  "Bị điều chỉnh"
                    }
                    await inc.execsqlinsupddel(`update s_inv set doc=?, cid=?, cde=? where id=? `, [JSON.stringify(doc),cid,cde, pid])
                }

                //Xóa biên bản
                await inc.execsqlinsupddel(`DELETE FROM s_inv_file where id=?`, [pid])
            }
            const sSysLogs = { fnc_id: 'inv_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa hóa đơn id ${id}`, msg_id: id, doc: JSON.stringify(doclog) };
            logging.ins(req, sSysLogs, next)
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },
    ddsSent: async (req, res, next) => {
        let pooldds
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const taxc = token.taxc, uid = token.uid, ou = token.ou,
                org = await ous.obt(taxc), AUTO = (org.seq === 1), body = req.body,
                invs = body.invs
            let count = 1
            let status, message, results, resultsdds, resultsddspas, ret
            try {
                const token = SEC.decode(req)
                let configdds = config.poolConfigdds

                resultsdds = await dbs.dds()

                if (resultsdds.recordset.length > 0) {

                    resultsddspas = await dbs.ddspass()

                }

                if (resultsddspas && resultsddspas.recordset.length > 0) {
                    for (let inv of invs) {
                        let chk = await chkDds(inv.c9, resultsdds.recordset)
                        if (chk == 1) {
                            let pass = await chkDdsPass(inv.c9, resultsddspas.recordset)
                            if (pass) {


                                let file = await ifile.getFileMailPass(inv.id, pass)
                                let date = new Date();
                                await ftp.uploadattach_mzh(file.zip, file.filename, date)
                                const pooldds2 = dbs.getConn2()

                                await dbs.ddsIns(inv, date)

                                //logger4app.debug('gửi dds success hoa đơn id :'+ inv.id)
                                ret = await inc.execsqlinsupddel("update  s_inv set dds_sent=1,file_dds=? where id=?", [`${inv.c9}-${inv.curr}-${inv.seq}.zip`.split("/").join("."), inv.id])
                            }

                        }
                    }
                }

                status = 1
                message = 'Success'
            } catch (e) {
                //dbs.getConn()
                status = 2
                throw e


            }

            res.json({ message: message, status: status })
        }
        catch (err) {
            next(err)
        }
    },
    ddsStt: async (req, res, next) => {
        let pooldds
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const body = req.body,
                invs = body.invs,
                td = body.td,
                fd = body.fd

            let status, message, resultsdds, ret
            try {

                resultsdds = await dbs.ddsStt(fd, td)
                const rows = resultsdds.recordset
                for (let inv of invs) {
                    for (let row of rows) {
                        if (row.CustomerAbbr == inv.c9 && row.FilePath == inv.file_dds) {
                            ret = await inc.execsqlinsupddel("update  s_inv set dds_stt=? where c9=? and file_dds=?", [row.StatusCode, row.CustomerAbbr, row.FilePath])
                        }

                    }
                }

                status = 1
                message = 'Success'
            } catch (e) {
                //dbs.getConn()
                status = 2
                throw e


            }

            res.json({ message: message, status: status })
        }
        catch (err) {
            next(err)
        }
    },
    relate: async (req, res, next) => {
        try {
            const token = SEC.decode(req), id = req.params.id
            let sql, result, colvcm = ENT == "vcm" ? `,c6 "c6"` : "", ret
            result = await inc.execsqlselect(`select doc "doc",pid "pid" from s_inv where id=?`, [id])
            const rows = result
            if (rows.length == 0) reject(new Error(`Không tìm thấy Hóa đơn ${id} \n (Invoice not found ${id})`))
            const doc = util.parseJson(rows[0].doc), sec = doc.sec, root = doc.root, rsec = root ? root.sec : null, pid = rows[0].pid
            let binds = [token.taxc, sec, sec]
            sql = `select id "id",idt "idt",type "type",form "form",serial "serial",seq "seq",sec "sec",status "status",adjdes "adjdes",cde "cde"${colvcm} from s_inv a ${sql_relate_01[`sql_relate_01_${config.dbtype}`]}`
            if (rsec) {
                if (pid) {
                    sql += ` ${sql_relate_02[`sql_relate_02_${config.dbtype}`]} or id in (${pid})`
                } else {
                    sql += ` ${sql_relate_02[`sql_relate_02_${config.dbtype}`]} `
                }

                binds.push(rsec)
                binds.push(rsec)
            }

            sql += ") order by id"
            result = await inc.execsqlselect(sql, binds)
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    relatewno: async (req, res, next) => {
        try {
            let id = req.params.id
            let sql
            sql = `${sql_relatewno[`sql_relatewno_${config.dbtype}`]} from s_wrongnotice_process where id_inv like '%${id}%' order by id`
            let result = await inc.execsqlselect(sql, [])
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    relateadjs: async (req, res, next) => {
        try {
            const token = SEC.decode(req), id = req.params.id, colvcm = ENT == "vcm" ? `,c6 "c6"` : ""
            let sql, result
            sql = `select id "id",idt "idt",type "type",form "form",serial "serial",seq "seq",sec "sec",status "status",adjdes "adjdes",cde "cde"${colvcm} from s_inv a where id in (${id}) order by id`

            result = await inc.execsqlselect(sql, [])
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    deleteinv: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const taxc = token.taxc, uid = token.uid, ou = token.ou,
                org = await ous.obt(taxc), AUTO = (org.seq === 1), body = req.body,
                invs = body.invs
            let count = 1, countseq = 0, oldupdate = 0, ret
            let status, message, seqsgrstt2 = [], idsgr = []

            try {
                const token = SEC.decode(req)
                //xoa nhieu hoa don cho sgr khong lung so
                if (config.ent == "sgr") {
                    for (let invsgr of invs) {
                        if (invsgr.status == 2) {
                            seqsgrstt2.push(invsgr.seq)
                            idsgr.push(invsgr.id)
                        }
                        if (invsgr.status == 1) {
                            await chkou(invsgr.id, token)
                            //Lay du lieu doc truoc khi xoa de luu log
                            let doclog = await rbi(invsgr.id)
                            ret = await inc.execsqlinsupddel(`delete from s_inv where id=? and status in (1) `, [invsgr.id])
                            await Service.delrefno(doclog)
                            ret = await inc.execsqlinsupddel(`update  s_inv set cid=null,cde=null where id=? and cid=?`, [invsgr.pid, invsgr.id])
                            countseq++
                            const sSysLogs = { fnc_id: 'inv_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa hóa đơn id ${invsgr.id}`, msg_id: invsgr.id, doc: JSON.stringify(doclog) };
                            logging.ins(req, sSysLogs, next)
                        }
                    }
                    if (seqsgrstt2.length > 0) {
                        let minseq = Math.min.apply(Math, seqsgrstt2), sql, result, rowseq, uk, max, key, sqlidsgr, rowsgr, idtsgr, checksgr, seqsgr, invsgrcd
                        max = minseq - 1
                        sqlidsgr = await inc.execsqlselect(`select stax "stax",form "form",serial "serial" from s_inv where id in (?)`, idsgr)
                        rowsgr = sqlidsgr[0]
                        //kiểm tra xem có tồn tại shd đã duyệt lơn hơn shd hiện tại
                        seqsgr = await inc.execsqlselect(`select count(*) "dem" from s_inv where stax=? and form=? and serial=? and seq>? and status not in(1,2)`, [rowsgr.stax, rowsgr.form, rowsgr.serial, minseq])
                        if (seqsgr[0].dem > 0) {
                            throw new Error(`Đã có hóa đơn có số lớn hơn đã được duyệt./nThere have been a larger number of invoices that have been approved `)
                        }
                        //xoá hoá đơn chờ cấp số có ngày lơn hơn ngày được chọn
                        idtsgr = await inc.execsqlselect(`select idt "idt" from s_inv where stax=? and form=? and serial=? and seq=?`, [rowsgr.stax, rowsgr.form, rowsgr.serial, minseq])
                        invsgrcd = await inc.execsqlselect(`select id "id", pid "pid", cid "cid" from s_inv where stax=? and form=? and serial=? and idt>=? and status in (1) `, [rowsgr.stax, rowsgr.form, rowsgr.serial, idtsgr[0].idt])
                        let rowinvsgrcd = invsgrcd
                        for (let rowinvcd of rowinvsgrcd) {
                            await chkou(rowinvcd.id, token)
                            //Lay du lieu doc truoc khi xoa de luu log
                            let doclog = await rbi(rowinvcd.id)
                            ret = await inc.execsqlinsupddel(`delete from s_inv where id=? and status in (1) `, [rowinvcd.id])
                            await Service.delrefno(doclog)
                            ret = await inc.execsqlinsupddel(`update  s_inv set cid=null,cde=null where id=? and cid=?`, [rowinvcd.pid, rowinvcd.id])
                            countseq++
                            const sSysLogs = { fnc_id: 'inv_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa hóa đơn id ${rowinvcd.id}`, msg_id: rowinvcd.id, doc: JSON.stringify(doclog) };
                            logging.ins(req, sSysLogs, next)

                        }
                        sql = `select id "id", pid "pid", stax "stax", form "form", serial "serial" from s_inv where stax=? and form=? and serial=? and seq>=? and status in (2)`
                        result = await inc.execsqlselect(sql, [rowsgr.stax, rowsgr.form, rowsgr.serial, minseq])
                        rowseq = result
                        for (let oldrowseq of rowseq) {
                            uk = `${oldrowseq.stax}.${oldrowseq.form}.${oldrowseq.serial}`
                            key = `SERIAL.${uk}`
                            await redis.set(key, max)
                            ret = await inc.execsqlinsupddel(`update s_serial set cur=? where taxc=? and form=? and serial=? and status=1 and min<=? and max>?`, [max, oldrowseq.stax, oldrowseq.form, oldrowseq.serial])
                            await chkou(oldrowseq.id, token)
                            //Lay du lieu doc truoc khi xoa de luu log
                            let doclog = await rbi(oldrowseq.id)
                            ret = await inc.execsqlinsupddel(`delete from s_inv where id=? `, [oldrowseq.id])
                            countseq++
                            await Service.delrefno(doclog)
                            let doc = util.parseJson(JSON.stringify(doclog))
                            doc.status = 3
                            ret = await inc.execsqlinsupddel(`update  s_inv set cid=null,cde=null,doc=? where id=? and cid=?`, [JSON.stringify(doc), oldrowseq.pid, oldrowseq.id])
                            const sSysLogs = { fnc_id: 'inv_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa hóa đơn id ${oldrowseq.id}`, msg_id: oldrowseq.id, doc: JSON.stringify(doclog) };
                            logging.ins(req, sSysLogs, next)
                        }
                    }

                } else {
                    for (let inv of invs) {
                        oldupdate = 0
                        await chkou(inv.id, token)
                        //Lay du lieu doc truoc khi xoa de luu log
                        let doclog = await rbi(inv.id), sql, result, rowseq
                        
                        if (ENT == "scb") {
                            for (const item of doclog.items) {
                                if (item.tranID) {
                                    ret = await inc.execsqlinsupddel(`update s_trans set status = ?, inv_id = null, inv_date = null where tranID = ?`, [item.statusGD, item.tranID])
                                }
                            }
                        }
                        if (config.ent == 'mzh') {
                            ret = await inc.execsqlinsupddel(`update  s_trans set status=stattus_temp,inv_id=inv_id_tmp,exrt=null,inv_date=null,ma_nv=null,trancol=null where inv_id=?`, [inv.id])
                        }
                        await inc.execsqlinsupddel(`delete from s_inv where id=? and status in (1) `, [inv.id])
                        await Service.delrefno(doclog)

                        sql = `select id "id", pid "pid", stax "stax", form "form", serial "serial" from s_inv where id = ? and status in (4)` // status=4 : Trang thái: Đã hủy
                        result = await inc.execsqlselect(sql, [inv.pid])
                        rowseq = result

                        for (let oldrowseq of rowseq) {
                            let olddoc = await rbi(oldrowseq.id)
                            let doc = util.parseJson(JSON.stringify(olddoc))
                            doc.status = 3
                            doc.cdetemp=""
                            ret = await inc.execsqlinsupddel(`update  s_inv set cid=null,cde=null,doc=? where id=?`, [JSON.stringify(doc),oldrowseq.id])
                            oldupdate = 1
                        }
                        if(oldupdate == 0 && inv.adjtyp == 2){
                            let cde,cid, rsck = await inc.execsqlselect(`select cid "cid",cde "cde" from s_inv where id=?`, [inv.pid])
                            if(rsck && rsck.length >0) {
                                cid =  rsck[0].cid
                                cde =  rsck[0].cde
                            }
                            if(cid){
                                // Xóa id của hd cần xóa trong trường cid ở hóa đơn gốc
                                cid = cid.toString().split(',').filter(e=> e!=inv.id).join(',')
                                if(!cid){
                                    cde=null
                                    cid=null
                                } else {
                                    cde =  "Bị điều chỉnh"
                                }
                                await inc.execsqlinsupddel(`update s_inv set cid=?, cde=? where id=? `, [cid,cde, inv.pid])
                            }
                        }
                        countseq++
                        const sSysLogs = { fnc_id: 'inv_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa hóa đơn id ${inv.id}`, msg_id: inv.id, doc: JSON.stringify(doclog) };
                        logging.ins(req, sSysLogs, next)
                    }
                }
                status = 1
                message = 'Success'
            } catch (e) {
                status = 2
                throw e
            }
            res.json({ message: message, status: status, countseq: countseq })
        }
        catch (err) {
            next(err)
        }
    },
    mailinvs: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body, invs = body.invs
            let status, message

            try {
                for (let inv of invs) {
                    let docmail = await rbi(inv.id)
                    let doc = util.parseJson(JSON.stringify(docmail)), xml = ''
                    if (config.ent == 'vcm') {
                        let taxc_root = await ous.pidtaxc(doc.ou)
                        doc.taxc_root = taxc_root
                    }
                    //logger4app.debug(`begin send mail : ${doc.stax} - ${doc.form} - ${doc.serial} - ${doc.seq}`)
                    if (useJobSendmail) {
                        await Service.insertDocTempJob(doc.id, doc, xml)
                    } else {
						if (config.ent == 'dbs') {
							await processFile(doc,xml)
                            await Service.updateSendMailStatus(doc)
						} else if(useRedisQueueSendmail) {
							if (!util.isEmpty(doc.bmail)) {
								
								let mailstatus = await email.senddirectmail(doc, xml)
								if (mailstatus) await Service.updateSendMailStatus(doc)
							}
							if (!util.isEmpty(doc.btel)) {
								let smsstatus = await sms.smssend(doc)
								if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
							}
						}
                    }
                }
                status = 1
                message = 'Success'
            } catch (e) {
                status = 2
                message = 'Error'
                throw e
            }

            res.json({ message: message, status: status })
        }
        catch (err) {
            next(err)
        }
    },
    sendmaildoc: async (req, res, next) => {
        try {
            let doc = req.body, xml = doc.xml
            //logger4app.debug(`inv.sendmaildoc doc ${JSON.stringify(doc)}`)
            if (ENT == 'vcm') {
                let taxc_root = await ous.pidtaxc(doc.ou)
                doc.taxc_root = taxc_root
            }
            logger4app.debug(`begin send mail : ${doc.stax} - ${doc.form} - ${doc.serial} - ${doc.seq}`)
            if (config.useJobSendmail) {
                await Service.insertDocTempJob(doc.id, doc, xml)
            } else {
                if (!util.isEmpty(doc.bmail)) {
                    let mailstatus = await email.senddirectmail(doc, xml)
                    if (mailstatus) await Service.updateSendMailStatus(doc)
                }
                if (!util.isEmpty(doc.btel)) {
                    let smsstatus = await sms.smssend(doc)
                    if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                }
            }
            logger4app.debug(`end send mail : ${doc.stax} - ${doc.form} - ${doc.serial} - ${doc.seq}`)
            res.json({ result: "1" })
        }
        catch (err) {
            throw err
        }

    },
    sendMailCoMa: async (req, res) => {
       
        try {
            const id = req.body.id
            logger4app.debug('sendMailCoMa id : ' + id)
            console.log('sendMailCoMa id : ' + id)
            var sqlinv = 'select id "id",doc "doc",xml_received "xml_received" from s_inv where id=?'
            //logger4app.debug('sqlinv : ', sqlinv)
            var resultinv = await inc.execsqlselect(sqlinv, [id])
            var doc = util.parseJson(resultinv[0].doc)
            if(resultinv.length>0){
                if((doc.serial).substring(0, 1) == 'C' && doc.bmail){
                    doc.status = 3
                    if (useJobSendmail) {
                        await Service.insertDocTempJob(resultinv[0].id, doc, resultinv[0].xml_received)
                    } else if(useRedisQueueSendmail){
                        if (!util.isEmpty(doc.bmail)) {
                            let mailstatus = await email.rsmqmail(doc)
                            if (mailstatus) await Service.updateSendMailStatus(doc)
                        }
                        if (!util.isEmpty(doc.btel)) {
                            let smsstatus = await sms.smssend(doc)
                            if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                        }
                    }
                }
            }
            
        }
        catch (err) {
            next(err)
        }

    },
    signdoc: async (req, res, next) => {
        try {
            let doc = req.body
            const org = await ous.gettosign(doc.stax)
            //logger4app.debug(org)
            let signtype = org.sign
            let xml, binds, result
            let sql

            if (signtype == 2) {
                const pwd = util.decrypt(org.pwd)
                const ca = await sign.ca(doc.stax, pwd)
                await inc.checkrevokeAPI(doc.stax)
                await sign.checkCABeforeSign(ca, doc.idt)
                let signSample = await util.getSignSample(doc.type, moment().format(dtf) , doc.id)
            //  xml = util.signature(sign.sign(ca, await util.j2x(doc, doc.id), doc.id),doc.idt)
                let str = await util.j2x(doc, doc.id)
                let PATH_XML = await util.getPathXml(doc.type)
                xml = sign.sign123(ca, str, doc.id, PATH_XML, signSample)
                
            }
            else if (signtype == 3) {
                sql = `select id,doc,bmail,pid from s_inv where id=@1 and stax=@2 and status=2`
                binds = [doc.id, doc.stax]
                result = await dbs.query(sql, binds)
                const rows = result.recordset
                if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
                const usr = org.usr, pwd = util.decrypt(org.pwd)
                const kqs = await fcloud.xml(usr, pwd, doc.stax, rows), kq = kqs[0]
                xml = kq.xml
            }
            else {
                xml = await hsm.signxml(doc.stax, await util.j2x(doc, doc.id))
                xml = util.signature(xml, doc.idt)
            }
            
            res.json({ xml: xml })
        }
        catch (err) {
            next(err)
        }

    },
    signxml: async (req, res, next) => {
        try {
            const xml = req.body.xml, stax = req.body.stax, id = req.body.id, idt = req.body.idt
            const org = await ous.gettosign(stax)
            //logger4app.debug(org)
            let signtype = org.sign
            let binds, result, xmlresult
            let sql
            //console.time(`${stax} - ${id}`);
            if (signtype == 1) {
                const usr = org.usr, pwd = util.decrypt(org.pwd)
                xmlresult = util.j2x(doc, doc.id)
                const arr = [{ inc: id, xml: Buffer.from(xml).toString("base64") }]
                res.json({ lib: usr, sid: 0, pin: pwd, mst: stax, date: new Date(), ref: config.PATH_XML, arr: arr })
            }
            else {
                if (signtype == 2) {
                    const pwd = util.decrypt(org.pwd)
                    const ca = await sign.ca(stax, pwd)
                    await inc.checkrevokeAPI(stax)
                    await sign.checkCABeforeSign(ca, doc.idt)
                    xmlresult = util.signature(sign.sign(ca, xml, id), idt)
                }
                else if (signtype == 3) {
                    sql = `select id,doc,bmail,pid from s_inv where id=@1 and stax=@2 and status=2`
                    binds = [id, stax]
                    result = await dbs.query(sql, binds)
                    const rows = result.recordset
                    if (rows.length == 0) throw new Error("Không có dữ liệu để ký \n (There is no data to sign)")
                    const usr = org.usr, pwd = util.decrypt(org.pwd)
                    const kqs = await fcloud.xml(usr, pwd, stax, rows), kq = kqs[0]
                    xmlresult = kq.xml
                }
                else {
                    xmlresult = await hsm.signxml(stax, xml)
                    xmlresult = util.signature(xmlresult, idt)
                }
            }
            //console.timeEnd(`${stax} - ${id}`);
            res.json({ xml: xmlresult })
        }
        catch (err) {
            next(err)
        }

    },
    apiSendToVan: async (req, res) => {
        try {
            const id = req.body.id
            // const tvan = require("./tvan") 
            // const token = SEC.decode(req)
            // let paramsDataTVan = { 
            //     ids: [id],
            //     type: ""
            // }
            // await tvan.dataTvanApi(token, paramsDataTVan, req)
            await inc.execsqlinsupddel(`INSERT INTO van_einv_listid_msg (pid, type, status_scan) values (?,?,?)`, [id, 'HD', 0])
            res.json({ result: 1 })
        }
        catch (err) {
           console.trace(err)
           res.json({ result: 0 })
        }

    },
    SendbackToVan: async (req, res) => {
        try {
            const listID = req.body.listC
            for(let i= 0;i<listID.length;i++){
                await inc.execsqlinsupddel(`INSERT INTO van_einv_listid_msg (pid, type, status_scan) values (?,?,?)`, [listID[i], 'HD', 0]) 
            }
            res.json({ result: 1 })
        }
        catch (err) {
           console.trace(err)
           res.json({ result: 0 })
        }

    },
    resetBTH:async (req, res) => {
        try {
            const listID = req.body.listC
            for(let i= 0;i<listID.length;i++){
                let result = await inc.execsqlselect(`SELECT serial "serial", doc "doc" ,list_invid_bx "list_invid_bx" FROM s_inv where id=?`, [listID[i]])
                let doc = JSON.parse(result[0].doc)
                doc.status_received = 0
                if(result[0].list_invid_bx!=null){
                    await inc.execsqlinsupddel(`UPDATE s_inv SET error_msg_van = null, th_type = NULL,doc = ?, list_invid_bx = NULL where id=?`, [JSON.stringify(doc),listID[i]])
                
                }else{
                    await inc.execsqlinsupddel(`UPDATE s_inv SET error_msg_van = null, th_type = NULL, doc = ?, list_invid = NULL where id=?`, [JSON.stringify(doc),listID[i]])
                }
            }
            res.json({ result: 1 })
        } catch (err) {
            console.trace(err)
            res.json({ result: 0 })
        }  
    },
    syncRedisDb: async (req, res, next) => {
        try {
            var sqlinv = 'select max(id) "id" from s_inv'
            //logger4app.debug('sqlinv : ', sqlinv)
            var resultinv = await inc.execsqlselect(sqlinv, [])
            var maxid = resultinv[0].id
            //logger4app.debug('resultinv.recordset[0] : ', resultinv.recordset[0])
            //logger4app.debug('maxid : ', maxid)
            var keyidinv = `INC`, curid = await redis.get(keyidinv)
            if (curid < maxid) {
                await redis.set(keyidinv, maxid)
            }
            res.json({ result: "1" })
        } catch (err) {
            next(err)
        }
    },
    print: async (req, res, next) => {
        try {
            const iwh = await Service.iwh(req, true)
            let sql = `select doc "doc" from s_inv  ${iwh.where} order by serial,seq`//ko dung index chi dinh vi se cham
            if (ENT == 'vcm') sql = `select doc "doc" from s_inv WITH(INDEX(s_inv_idx_final)) ${iwh.where} order by serial,seq`//dung index chi dinh nhanh
            const result = await inc.execsqlselect(sql, iwh.binds)
            const rows = result
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn \n No invoice be found`)
            const token = SEC.decode(req), org = await ous.org(token), idx = org.temp, filter = util.parseJson(req.query.filter)
            const form = filter.form, type = filter.type, serial = filter.serial
            const fn = form.length > 1 ? util.fn(form, idx) : `${type}.${form}.${serial}.${idx}`
            const tmp = await util.template(fn)
            // const token = SEC.decode(req), mst = token.mst, org = await ous.org(token), qrc = org.qrc == 1, filter = util.parseJson(req.query.filter)
            // for (const row of rows) {
            //     const doc = row.doc
            //     util.viewadj(doc)
            //if (qrc) row.qrcode = await util.qrcode(row.doc)
            // }
            for (const row of rows) {
                let doc = util.parseJson(row.doc)
                doc.tempfn = fn
                row.doc = doc
            }
            res.json({ docs: rows, tmp: tmp })
        } catch (err) {
            next(err)
        }
    },
    printmerge: async (req, res, next) => {
        try {
            const iwh = await Service.iwh(req, true)
            let sql = `select doc "doc" from s_inv  ${iwh.where} order by serial,seq`//ko dung index chi dinh vi se cham
            if (ENT == 'vcm') sql = `select doc "doc" from s_inv WITH(INDEX(s_inv_idx_final)) ${iwh.where} order by serial,seq`//dung index chi dinh nhanh
            const result = await inc.execsqlselect(sql, iwh.binds)
            const rows = result
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn \n No invoice be found`)
            const token = SEC.decode(req), org = await ous.org(token), idx = org.temp, filter = util.parseJson(req.query.filter)
            const form = filter.form, type = filter.type, serial = filter.serial
            const fn = form.length > 1 ? util.fn(form, idx) : `${type}.${form}.${serial}.${idx}`
            const tmp = await util.template(fn)
            // const token = SEC.decode(req), mst = token.mst, org = await ous.org(token), qrc = org.qrc == 1, filter = util.parseJson(req.query.filter)
            // for (const row of rows) {
            //     const doc = row.doc
            //     util.viewadj(doc)
            //if (qrc) row.qrcode = await util.qrcode(row.doc)
            // }
            let jsons = []
            for (const row of rows) {
                let doc = util.parseJson(row.doc)
                doc.tempfn = fn
                row.doc = doc
                jsons.push({ doc: row.doc })
            }
            let respdf = await util.jsReportRenderAttachPdfs(jsons)
            res.json(respdf)
        } catch (err) {
            next(err)
        }
    },
    printall: async (req, res, next) => {
        try {
            const iwh = await Service.iwh(req, true)
            let where = iwh.where, binds = iwh.binds
            let oldid = req.query.arr
            where += ` and id in('${oldid}')`
            let sql = `select id "id", doc "doc" from s_inv  ${where} order by serial,seq`//ko dung index chi dinh vi se cham
            const result = await inc.execsqlselect(sql, binds)
            const rows = result
            if (rows.length == 0) throw new Error(`Không tìm thấy hóa đơn \n No invoice be found`)
            const token = SEC.decode(req), org = await ous.org(token), idx = org.temp, filter = util.parseJson(req.query.filter)
            const form = filter.form, type = filter.type, serial = filter.serial
            const fn = form.length > 1 ? util.fn(form, idx) : `${type}.${form}.${serial}.${idx}`
            const tmp = await util.template(fn)
            for (const row of rows) {
                row.doc = util.parseJson(row.doc)
                row.doc.tempfn = fn
            }
            res.json({ docs: rows, tmp: tmp })
        } catch (err) {
            next(err)
        }
    },
    synReport: async (req, res, next) => {
        try {
            const body = req.body
            let sql, binds, fn, rows, json, result, status, fd = body.fd, td = body.td, rowd = []
            let c0, c1 = 0, c2 = 0, c3 = 0

            //so sánh status
            if (body.status == 1) {
                status = "Chờ cấp số"
            } else if (body.status == 2) {
                status = "Chờ duyệt"
            } else if (body.status == 6) {
                status = "Chờ hủy"
            } else if (body.status == 3) {
                status = "Đã duyệt"
            }

            let where = '1=1 '

            fn = "temp/MZH_INVSReport.xlsx"

            sql = `select a.curr,JSON_VALUE(JSON_QUERY (doc, '$.tax[0]'),'$.vrt') vrt,sum(a.sum) as sum, sum(a.sumv) as sumv, sum(a.vat) as vat, sum(a.vatv) as vatv from s_inv a where idt between @1 and @2 and ${where} and a.status=@3 group by a.curr,JSON_VALUE(JSON_QUERY (doc, '$.tax[0]'),'$.vrt') order by curr ,cast(JSON_VALUE(JSON_QUERY (doc, '$.tax[0]'),'$.vrt') as int)`
            binds = [fd, td, body.status]

            result = await dbs.query(sql, binds)
            rows = result.recordset
            let lable = '', sumv = 0, vatv = 0
            for (let row of rows) {
                if (lable == '') {
                    row.c0 = 'CURRENCY ( Loại tiền)'
                    row.c1 = row.curr
                    row.c2 = ''
                    row.c3 = ''
                    lable = row.curr
                    rowd.push(row)
                    switch (row.vrt) {
                        case "-1":
                            c0 = "-       Not subject to VAT ( Không chịu thuế)"
                            break;
                        case "0":
                            c0 = "-       EPZ customer ( 0%)"
                            break;
                        case "5":
                            c0 = "-       5%"

                            break;
                        case "10":
                            c0 = "-       10%"

                            break
                        default:
                            logger4app.debug("Không có giá trị tương ứng");
                            break;
                    }

                    c1 = ''
                    c2 = row.sum
                    c3 = row.vat
                    rowd.push({ c0: c0, c1: c1, c2: c2, c3: c3 })
                    sumv += row.sum
                    vatv += row.vat
                } else {
                    if (row.curr != lable) {
                        row.c0 = 'TOTAL'
                        row.c1 = lable
                        row.c2 = sumv
                        row.c3 = vatv
                        rowd.push(row)
                        sumv = 0
                        vatv = 0

                        rowd.push({ c0: "", c1: "", c2: "", c3: "" })
                        // rowd.push(row)

                        lable = row.curr
                        rowd.push({ c0: 'CURRENCY ( Loại tiền)', c1: row.curr, c2: "", c3: "" })
                        switch (row.vrt) {
                            case "-1":
                                c0 = "-       Not subject to VAT ( Không chịu thuế)"
                                break;
                            case "0":
                                c0 = "-       EPZ customer ( 0%)"
                                break;
                            case "5":
                                c0 = "-       5%"
                                break;
                            case "10":
                                c0 = "-       10%"
                                break
                            default:
                                logger4app.debug("Không có giá trị tương ứng");
                                break;
                        }

                        c1 = ''
                        c2 = row.sum
                        c3 = row.vat
                        rowd.push({ c0: c0, c1: c1, c2: c2, c3: c3 })
                        sumv += row.sum
                        vatv += row.vat
                        //rowd.push(row)
                    } else {
                        switch (row.vrt) {
                            case "-1":
                                row.c0 = "-      Not subject to VAT ( Không chịu thuế)"
                                break;
                            case "0":
                                row.c0 = "-       EPZ customer ( 0%)"
                                break;
                            case "5":
                                row.c0 = "-       5%"
                                break;
                            case "10":
                                row.c0 = "-       10%"
                                break
                            default:
                                logger4app.debug("Không có giá trị tương ứng");
                                break;
                        }

                        row.c1 = ''
                        row.c2 = row.sum
                        row.c3 = row.vat
                        rowd.push(row)
                        sumv += row.sum
                        vatv += row.vat
                    }
                }

            }
            if (rows.length > 0) {
                rowd.push({ c0: 'TOTAL', c1: lable, c2: sumv, c3: vatv })
            }
            json = { fd: moment(fd).format("DD/MM/YYYY"), ft: moment(td).format("DD/MM/YYYY"), status: status, table: rowd }

            const file = path.join(__dirname, "..", "..", "..", fn)
            const xlsx = fs.readFileSync(file)
            const template = new xlsxtemp(xlsx)
            template.substitute(1, json)
            res.end(template.generate(), "binary")

        } catch (err) {
            next(err)
        }
    },
    detailS: async (req, res, next) => {
        try {
            let fn, json, where, body = req.body, sql, where1, result, rows, where2
            let month = body.month, year = body.year
            fn = "temp/MZH_INVSDetailS.xlsx"

            where = `where MONTH(idt)=${month} and YEAR(idt)=${year} and status=3   `

            let name = `JSON_VALUE(JSON_QUERY (doc, '$.items[0]'),'$.name')`

            sql = `select form, serial, seq, idt, bname, buyer, baddr,btax,curr,sum,status, note, ${name} name,JSON_VALUE(JSON_QUERY (doc, '$.tax[0]'),'$.vrt') vrt,JSON_VALUE(JSON_QUERY (doc, '$.tax[0]'),'$.vrn') vrn,vat,total,exrt,adjtyp from s_inv ${where} `

            if (config.BKHDVAT_EXRA_OPTION == '2') {
                //Hóa đơn bị hủy
                where1 = `where MONTH(cdt)=${month} and YEAR(cdt)=${year} and status=4 and xml is not null `
                sql += ` union all select form, serial, seq, cdt as idt, bname, buyer, baddr,btax,curr,sum,status, note, ${name} name,JSON_VALUE(JSON_QUERY (doc, '$.tax[0]'),'$.vrt') vrt,JSON_VALUE(JSON_QUERY (doc, '$.tax[0]'),'$.vrn') vrn,vat,total,exrt,adjtyp from s_inv ${where1}`
            }

            //Hóa đơn thay thế
            where2 = `where MONTH(idt)=${month} and YEAR(idt)=${year} and status=3 and pid is not null`
            sql += `union all select form, serial, seq, idt, bname, buyer, baddr,btax,curr,sum,status, note, ${name} name,JSON_VALUE(JSON_QUERY (doc, '$.tax[0]'),'$.vrt') vrt,JSON_VALUE(JSON_QUERY (doc, '$.tax[0]'),'$.vrn') vrn,vat,total,exrt,adjtyp from s_inv ${where2}`

            // sql='select * from (' + sql + ') t order by curr,form,serial,seq'
            sql = `select * from (${sql}) t order by curr,cast(vrt as int),form,serial,seq`
            result = await dbs.query(sql)
            rows = result.recordset

            //Lấy tỉ suất vào ngày cuối cùng của tháng
            let sql2, result2, subrows, usdEx, vndEx, moneyEx, objectMoneyEx = {}
            let today = new Date();
            let dd = String(today.getDate()).padStart(2, '0');
            let mm = String(today.getMonth() + 1).padStart(2, '0');
            let lastMonth = parseInt(month) - 1
            let lastYear = parseInt(year) - 1
            let lastDateMonth

            lastDateMonth = new Date(parseInt(year), parseInt(month), 0)

            if (lastDateMonth.getDay() == 6) {
                const t = lastDateMonth.getDate() + (6 - lastDateMonth.getDay() - 1);
                lastDateMonth.setDate(t);
            } else if (lastDateMonth.getDay() == 0) {
                const t = lastDateMonth.getDate() + (6 - lastDateMonth.getDay() - 1) - 7;
                lastDateMonth.setDate(t);
            }

            let whereDate = lastDateMonth.getDate()
            let whereMonth = lastDateMonth.getMonth() + 1
            let whereYear = lastDateMonth.getFullYear()

            sql2 = `select  val,cur,dt from s_ex where MONTH(dt)=${whereMonth} and YEAR(dt)=${whereYear} order by cur,dt desc`
            // and DAY(dt)=${whereDate}`
            result2 = await dbs.query(sql2)
            subrows = result2.recordset
            // logger4app.debug(subrows)

            for (const rowsub of subrows) {
                if (!objectMoneyEx[rowsub.cur]) {
                    objectMoneyEx[rowsub.cur] = rowsub.val
                    moneyEx = rowsub.val
                }


            }

            let a0 = [], a3 = [], a4 = [], a10 = [], a1 = [], a2 = []
            let i0 = 0, i3 = 0, i4 = 0, i10 = 0, i1 = 0, i2 = 0
            let s0 = 0, s1 = 0, s3 = 0, s4 = 0, s10 = 0
            let v0 = 0, v1 = 0, v3 = 0, v4 = 0, v10 = 0
            let t0 = 0, t1 = 0, t3 = 0, t4 = 0, t10 = 0
            let sumExC01 = 0, sumExC02 = 0, sumExC03 = 0, sumExC04 = 0, sumVrt01 = 0, sumVrt02 = 0, sumVrt03 = 0, sumVrt04 = 0, totalIvs2 = 0
            let nextCurr01 = '', lastCurr01 = '', nextCurr03 = '', lastCurr03 = '', nextCurr04 = '', lastCurr04 = ''
            let objectSum, textSum, textCurr, s = 0, v = 0, t = 0, sumExC = 0, sumVrt = 0, totalIvs = 0
            let sumExC3 = 0, sumVrt3 = 0, totalIvs3 = 0
            let sumExC4 = 0, sumVrt4 = 0, totalIvs4 = 0
            let sumExC10 = 0, sumVrt10 = 0, totalIvs10 = 0
            let sumExC1 = 0, sumVrt1 = 0, totalIvs1 = 0
            let finalSumExc = 0, finalVrt = 0, finaltotalIvs = 0
            let indexA1 = [], indexA3 = [], indexA4 = [], indexA10 = []
            let countA1 = 0, countA3 = 0, countA4 = 0, countA10 = 0
            let countExist = 0

            for (const element of rows) {
                if (element.adjtyp == 2 || element.adjtyp == 3) {
                    indexA3.push(rows.indexOf(element))
                }
                if (element.status == 3 && element.adjtyp != 2 && element.adjtyp != 3 && element.adjtyp != 1) {
                    indexA1.push(rows.indexOf(element))
                }
                if (element.status == 4 && element.adjtyp != 2 && element.adjtyp != 3 && element.adjtyp != 1) {
                    indexA10.push(rows.indexOf(element))
                }
                if (element.adjtyp == 1) {
                    indexA4.push(rows.indexOf(element))
                }

                // Remove exist
                for (const keyRows in rows) {
                    if (element.seq == rows[keyRows].seq) {
                        countExist++
                    }
                    if (countExist > 1) {
                        rows.splice(keyRows, 1)
                        break
                    }
                }
                countExist = 0
            }


            for (const row of rows) {
                let form, serial, seq, idt, bname, buyer, baddr, btax, name, curr, sum, vrn, vat, exrt, total, exChange, vrtChange, sumTotal = 0
                moneyEx = objectMoneyEx[row.curr]

                form = row.form
                serial = row.serial
                seq = row.seq
                idt = row.idt
                baddr = row.baddr
                buyer = row.bname
                btax = row.btax
                name = row.name
                curr = row.curr


                sum = row.sum
                vrn = row.vrn
                vat = row.vat
                exrt = row.exrt
                total = row.total

                exChange = row.sum * moneyEx
                vrtChange = row.vat * moneyEx
                sumTotal = exChange + vrtChange

                let r = {
                    form: form,
                    serial: serial,
                    seq: seq,
                    idt: moment(idt).format("DD/MM/YYYY"),
                    buyer: buyer,
                    baddr: baddr,
                    btax: btax,
                    name: name,
                    curr: curr,
                    sum: sum,
                    vrn: vrn,
                    vat: vat,
                    exrt: exrt,
                    total: total,
                    exChange: exChange,
                    vrtChange: vrtChange,
                    sumTotal: sumTotal
                }
                if (row.adjtyp == 2 || row.adjtyp == 3) {
                    lastCurr03 = row.curr
                    if (rows[rows.indexOf(row) + 1]) {
                        nextCurr03 = rows[rows.indexOf(row) + 1].curr
                    }


                    // r.name = (row.adjtyp == 2) ? 'Hóa đơn điều chỉnh tăng' :'Hóa đơn điều chỉnh giảm'
                    r.name = row.note
                    r.moneyEx = moneyEx
                    r.index = ++i3
                    a3.push(r)

                    s3 += row.sum
                    v3 += row.vat
                    t3 += row.total
                    sumExC3 += exChange
                    sumVrt3 += vrtChange
                    totalIvs3 += sumTotal

                    if (rows[indexA3[countA3]] && (!rows[indexA3[countA3 + 1]] || row.curr != rows[indexA3[countA3 + 1]].curr)) {
                        textSum = 'TỔNG'
                        textCurr = row.curr

                        objectSum = {
                            index: textSum,
                            curr: textCurr,
                            sum: s3,
                            vat: v3,
                            total: t3,
                            exChange: sumExC3,
                            vrtChange: sumVrt3,
                            sumTotal: totalIvs3
                        }
                        finalSumExc += sumExC
                        finalVrt += sumVrt
                        r = { ...objectSum }
                        // r.index = ++i1
                        a3.push(r)

                        lastCurr03 = row.curr
                        s3 = 0
                        v3 = 0
                        t3 = 0
                        sumExC3 = 0
                        sumVrt3 = 0
                        totalIvs3 = 0
                    }
                    countA3++
                }
                else if (row.adjtyp == 1) {
                    // if(row.curr=='USD'){
                    //     sumExC03+=exChange
                    //     sumVrt03+=vrtChange
                    //     totalIvs3+=sumTotal
                    //     s3+=row.sum
                    //     v3+=row.vat
                    //     t3+=row.total
                    //     r.index = ++i3
                    //     a3.push(r)
                    // }
                    // else if(row.curr=='VND'){
                    //     sumExC04+=exChange
                    //     sumVrt04+=vrtChange
                    //     totalIvs4+=sumTotal
                    //     s4+=row.sum
                    //     v4+=row.vat 
                    //     t4+=row.total
                    //     r.index=++i4
                    //     a4.push(r)
                    // }
                    lastCurr04 = row.curr
                    if (rows[rows.indexOf(row) + 1]) {
                        nextCurr04 = rows[rows.indexOf(row) + 1].curr
                    }

                    s0 += row.sum
                    v0 += row.vat
                    t0 += row.total
                    r.moneyEx = moneyEx
                    r.index = ++i4
                    a4.push(r)

                    s4 += row.sum
                    v4 += row.vat
                    t4 += row.total
                    sumExC4 += exChange
                    sumVrt4 += vrtChange
                    totalIvs4 += sumTotal

                    if (rows[indexA4[countA4]] && (!rows[indexA4[countA4 + 1]] || row.curr != rows[indexA4[countA4 + 1]].curr)) {
                        textSum = 'TỔNG'
                        textCurr = lastCurr04

                        objectSum = {
                            index: textSum,
                            curr: textCurr,
                            sum: s4,
                            vat: v4,
                            total: t4,
                            exChange: sumExC4,
                            vrtChange: sumVrt4,
                            sumTotal: totalIvs4
                        }
                        finalSumExc += sumExC
                        finalVrt += sumVrt
                        r = { ...objectSum }
                        // r.index = ++i1
                        a4.push(r)


                        s4 = 0
                        v4 = 0
                        t4 = 0
                        sumExC4 = 0
                        sumVrt4 = 0
                        totalIvs4 = 0
                    }
                    countA4++
                }
                else if (row.status == 4) {
                    r.name = 'HỦY'
                    // r = {
                    //     form:form,
                    //     serial:serial,
                    //     seq:seq,
                    //     idt:moment(idt).format("DD/MM/YYYY"),
                    //     buyer:buyer,
                    //     baddr:baddr,
                    //     btax:btax,
                    //     name:name,
                    //     curr:curr,
                    //     sum:sum,
                    //     vrn:vrn,
                    //     vat:vat,
                    //     exrt:exrt,
                    //     exChange:exChange,
                    //     vrtChange:vrtChange,
                    //     sumTotal:sumTotal
                    // }
                    r.index = ++i10
                    a10.push(r)

                    s10 += row.sum
                    v10 += row.vat
                    t10 += row.total
                    sumExC10 += exChange
                    sumVrt10 += vrtChange
                    totalIvs10 += sumTotal

                    if (rows[indexA10[countA10]] && (!rows[indexA10[countA10 + 1]] || row.curr != rows[indexA10[countA10 + 1]].curr)) {
                        textSum = 'TỔNG'
                        textCurr = row.curr

                        objectSum = {
                            index: textSum,
                            curr: textCurr,
                            sum: s10,
                            vat: v10,
                            total: t10,
                            exChange: sumExC10,
                            vrtChange: sumVrt10,
                            sumTotal: totalIvs10
                        }
                        finalSumExc += sumExC
                        finalVrt += sumVrt
                        r = { ...objectSum }
                        // r.index = ++i1
                        a10.push(r)

                        s10 = 0
                        v10 = 0
                        t10 = 0
                        sumExC10 = 0
                        sumVrt10 = 0
                        totalIvs10 = 0
                    }
                    countA10++
                }
                else if (row.status == 3) {
                    // logger4app.debug(row.curr)
                    // if(row.curr=='USD'){	  
                    // }
                    // else if(row.curr='VND'){
                    //     sumExC02+=exChange
                    //     sumVrt02+=vrtChange
                    //     totalIvs2+=sumTotal
                    //     s1+=row.sum


                    //     v1+=row.vat 
                    //     t1+=row.total
                    //     r.index=++i2
                    //     a2.push(r)
                    //     // logger4app.debug(a2)
                    // }

                    if (rows[rows.indexOf(row) + 1]) {
                        nextCurr01 = rows[rows.indexOf(row) + 1].curr
                    }

                    r.moneyEx = moneyEx
                    r.index = ++i1
                    a1.push(r)

                    s1 += row.sum
                    v1 += row.vat
                    t1 += row.total
                    sumExC1 += exChange
                    sumVrt1 += vrtChange
                    totalIvs1 += sumTotal

                    if (rows[indexA1[countA1]] && (!rows[indexA1[countA1 + 1]] || row.curr != rows[indexA1[countA1 + 1]].curr)) { //(rows.length - 2) == rows.indexOf(row)
                        textSum = 'TỔNG'


                        objectSum = {
                            index: textSum,
                            curr: row.curr,
                            sum: s1,
                            vat: v1,
                            total: t1,
                            exChange: sumExC1,
                            vrtChange: sumVrt1,
                            sumTotal: totalIvs1
                        }
                        finalSumExc += sumExC
                        finalVrt += sumVrt
                        r = { ...objectSum }
                        // r.index = ++i1
                        a1.push(r)


                        s1 = 0
                        v1 = 0
                        t1 = 0
                        sumExC1 = 0
                        sumVrt1 = 0
                        totalIvs1 = 0
                    }
                    countA1++
                }
            }

            finaltotalIvs = finalSumExc + finalVrt



            json = {
                month: month,
                year: year,
                a0: a0,
                a10: a10,
                s0: s0,
                v0: v0,
                t0: t0,
                a1: a1,
                s1: s1,
                v1: v1,
                t1: t1,
                a2: a2,
                a3: a3,
                a4: a4,
                s3: s3,
                v3: v3,
                t3: t3,
                s4: s4,
                v4: v4,
                t4: t4,
                vndEx: vndEx,
                usdEx: usdEx,
                sumExC01: sumExC01,
                sumExC02: sumExC02,
                sumVrt01: sumVrt01,
                sumVrt02: sumVrt02,
                totalIvs2: totalIvs2,
                totalIvs1: totalIvs1,
                finalSumExc: finalSumExc,
                finalVrt: finalVrt,
                finaltotalIvs: finaltotalIvs
            }
            // logger4app.debug(json)

            const file = path.join(__dirname, "..", "..", "..", fn)
            const xlsx = fs.readFileSync(file)
            const template = new xlsxtemp(xlsx)
            template.substitute(1, json)
            res.end(template.generate(), "binary")

        } catch (err) {
            next(err)
        }
    },
    detailTran: async (req, res, next) => {
        try {
            const param = req.params
            let sql, binds, result, dataQuery, dataDetail = []
            sql = `select JSON_QUERY(doc,'$.items'), status 'status', c0 'c0' from s_inv where id=@1`
            binds = [param.id]
            result = await dbs.query(sql, binds)
            dataQuery = result.recordset
            dataDetail = util.parseJson(result.recordset[0][Object.keys(result.recordset[0])[0]])
            for (let row of dataDetail) {
                if ((result.recordset[0].status == 2 || result.recordset[0].status == 3)) row.statusGD = 1
                // row.status = result.recordset[0].status
                row.trantype = result.recordset[0].c0
            }

            res.json({
                data: dataDetail
            })
        }
        catch (err) {
            next(err)
        }
    },
    hstvan: async (req, res, next) => {
        try {
            const token = SEC.decode(req), schema = token.schema, params = req.params, id = params.id
            let sql =  sql_history[`sql_history_${config.dbtype}`]
            let binds = [id]
            let result = await inc.execsqlselect(sql, binds)
            res.json(result)
        } catch (err) {
            next(err)
        }
    },
    hscqtXml: async (req, res, next) => {
        try {
            const token = SEC.decode(req), schema = token.schema, params = req.params, id = params.id
            let sql =  `select vh.r_xml from van_history vh where id = ?`
            let binds = [id]
            let result = await inc.execsqlselect(sql, binds)
            res.json(result)
        } catch (err) {
            next(err)
        }
    },
    cancelSGR: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, body = req.body
            let doc, arr = body.arr, status = 4, cdt
            for (let item of arr) {
                logger4app.debug(item);
                await chkou(item.id, token)
                doc = await rbi(item.id)
                const cancel = { typ: 3, ref: item.id, rdt: moment().format(dtf) }
                if (doc.adj && doc.pid && doc.adj.typ == 2) await dbs.query(`update s_inv set cid=null,cde=null where id=@1`, [doc.pid])
                if (ENT == 'sgr') {
                    cdt = doc.idt
                } else cdt = moment(new Date()).format(dtf)
                await dbs.query(`update s_inv set doc=JSON_MODIFY(JSON_MODIFY(JSON_MODIFY(doc,'$.cdt',@1),'$.canceller',@2),'$.cancel',JSON_QUERY(@3)) where id=@4 and stax=@5`, [cdt, token.uid, JSON.stringify(cancel), item.id, taxc])
                let result = await dbs.query(`update s_inv set doc=JSON_MODIFY(doc,'$.status',@1) where id=@2 and stax=@3`, [status, item.id, taxc])
                if (result.rowsAffected[0] > 0) {
                    if (useJobSendmail) {
                        if (((status == 4) && !util.isEmpty(doc.bmail)) || ((status == 4) && !util.isEmpty(doc.btel))) {
                            doc.status = 4
                            await Service.insertDocTempJob(doc.id, doc, ``)
                        }
                    } else if(useRedisQueueSendmail){
                        if ((status == 4) && !util.isEmpty(doc.bmail)) {
                            doc.status = 4
                            if (!util.isEmpty(doc.bmail)) {
                                let mailstatus = await email.rsmqmail(doc)
                                if (mailstatus) await Service.updateSendMailStatus(doc)
                            }
                        }
                        if ((status == 4) && !util.isEmpty(doc.btel)) {
                            doc.status = 4
                            if (!util.isEmpty(doc.btel)) {
                                let smsstatus = await sms.smssend(doc)
                                if (smsstatus) await Service.updateSendSMSStatus(doc, smsstatus)
                            }
                        }
                    }
                    const sSysLogs = { fnc_id: 'inv_istatus', src: config.SRC_LOGGING_DEFAULT, dtl: `Chuyá»ƒn Ä‘á»•i tráº¡ng thÃ¡i hÃ³a Ä‘Æ¡n id ${item.id}`, msg_id: item.id, doc: JSON.stringify(doc) };
                    logging.ins(req, sSysLogs, next)
                }
            }
            res.json(1)
        } catch (err) {
            next(err)
        }
    },

    depINV: async (req, res, next) => {
        try {
            const token = SEC.decode(req), uid = token.uid
            let sql, binds, result, data
            sql = `select deptid "id", deptid "value" from s_dept where deptid in (select deptid from s_deptusr where usrid = @1) `
            binds = [uid]
            result = await inc.execsqlselect(sql, binds)
            data = result.recordset
            res.json(data)

        } catch (err) {
            next(err)
        }
    },
    delAll: async (req, res, next) => {
        try {
            const token = SEC.decode(req), body = req.body
            let sql, result, binds, rows, sqlinv, resultinv, query = { filter: JSON.stringify(body) }, reqtmp = { query: query, token: token }, iwh = await Service.iwh(reqtmp, false, true), countseq = 0, ret
            //Chi cho phép xóa các hóa đơn với trạng thai nhất định
            if (!['1'].includes(body.status)) throw new Error(`Chỉ được xóa các hóa đơn với các trạng thái được phép xóa \n (Only invoices with statuses allowed to be deleted can be deleted)`)
            //Check so ban ghi truoc khi thuc hien xoa
            const sqlcount = `select count(*) "total" from s_inv ${iwh.where}`
            const resultcount = await inc.execsqlselect(sqlcount, iwh.binds)
            if (resultcount[0].total > config.MAX_ROW_DELETE_ALL) throw new Error(`Số lượng bản ghi cần xóa vượt quá giới hạn cho phép \n (The number of records to be deleted exceeds the allowed limit)`)
            //Lấy các hóa đơn cần xóa
            sqlinv = `select pid "pid", id "id", adjtyp "adjtyp" from s_inv ${iwh.where}`
            resultinv = await inc.execsqlselect(sqlinv, iwh.binds)
            rows = resultinv
            for (let inv of rows) {
                await chkou(inv.id, token)
                //Lay du lieu doc truoc khi xoa de luu log
                let doclog = await rbi(inv.id)
                if (config.ent == "scb") {
                    for (const item of doclog.items) {
                        if (item.tranID) {
                            ret = await inc.execsqlinsupddel(`update s_trans set status = ?, inv_id = null, inv_date = null where tranID = ?`, [item.statusGD, item.tranID])
                        }
                    }
                }
                ret = await inc.execsqlinsupddel(`delete from s_inv where id=?`, [inv.id])
                await Service.delrefno(doclog)
                if(inv.adjtyp == 2){
                    let cde,cid, rsck = await inc.execsqlselect(`select cid,cde from s_inv where id=?`, [inv.pid])
                    if(rsck && rsck.length >0) {
                        cid =  rsck[0].cid
                        cde =  rsck[0].cde
                    }
                    if(cid){
                        // Xóa id của hd cần xóa trong trường cid ở hóa đơn gốc
                        cid = cid.toString().split(',').filter(e=> e!=inv.id).join(',')
                        if(!cid){
                            cde=null
                            cid=null
                        } else {
                            cde =  "Bị điều chỉnh"
                        }
                        await inc.execsqlinsupddel(`update s_inv set cid=?, cde=? where id=? `, [cid,cde, inv.pid])
                    }
                } else if(inv.adjtyp == 1){
                    const docinv = await rbi(inv.pid)
                    docinv.status=3
                    delete docinv["cde"]
                    delete docinv["cdt"]
                    delete docinv["canceller"]
                    await inc.execsqlinsupddel(`update s_inv set cid=null,cde=null, doc=? where id=?`, [JSON.stringify(docinv),inv.pid])
                } else {
                    await inc.execsqlinsupddel(`update s_inv set cid=null,cde=null where id=? and cid=?`, [inv.pid, inv.id])
                }

                countseq++
                if (config.ent == 'mzh') {
                    ret = await inc.execsqlinsupddel(`update  s_trans set status=stattus_temp,inv_id=inv_id_tmp,exrt=null,inv_date=null,ma_nv=null,trancol=null where inv_id=?`, [inv.id])
                }
                const sSysLogs = { fnc_id: 'inv_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa hóa đơn id ${inv.id}`, msg_id: inv.id, doc: JSON.stringify(doclog) };
                logging.ins(req, sSysLogs, next)
            }

            res.json({ countseq: countseq })
        } catch (err) {
            next(err)
        }
    },
    getIDFile: async (req, res, next) => {
        try {
            const id = req.params.id
            let sql, result, ret, count
            sql = `Select id_file from s_inv where id = ${id}`
            result = await dbs.query(sql)
            count = util.parseJson(result.rows[0].ID_FILE)
            ret = { data: count }
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    delFile: async (req, res, next) => {
        try {
            const body = req.body, arrID = body.arrID, id = body.id
            let sql, result, ret, count, maSQL, countL = 0, newFILE
            sql = `Select id_file from s_inv where id = '${id}'`
            result = await dbs.query(sql)
            count = util.parseJson(result.rows[0].ID_FILE)
            for (let i = 0; i < count.length; i++) {
                for (let j = 0; j < arrID.length; j++) {
                    if ((count[i].id).localeCompare(arrID[j]) == 0 && count[i].id) {
                        count.splice(i, 1);
                        i = i - 1;
                        break;
                    }
                }
            }
            ret = count.length
            if (count.length == 0) {
                count = ''
            } else {
                count = JSON.stringify(count)
            }
            maSQL = `update s_inv set ID_FILE = '${count}' where id = '${id}'`
            await dbs.query(maSQL)
            res.json(ret)
        }
        catch (err) {
            next(err)
        }

    },
    changeDate: async (req, res, next) => {
        try {
            const body = req.body
            let id = body.idEnv, dateChange = moment(body.dateC, dtf), result, sqlDate, resultD, row
            let doc = await rbi(id)
            //Lấy ngày phát hành
            sqlDate = `Select fd "fd",td "td" from s_serial where form = :1 and serial = :2 and taxc = :3`
            resultD = await dbs.query(sqlDate, [doc.form, doc.serial, doc.stax])
            let fd = resultD.rows[0].fd, td = resultD.rows[0].td
            const dt = moment(body.dateC), eod = dt.endOf('day').toDate(), dtsc = dt.format('YYYYMMDD'), idtc = (moment(new Date())).format('YYYYMMDD')
            //Check ngay nho hon so lon hon hoac ngay lon hon so nho hon
            result = await dbs.query(`select count(*) "rc" from s_inv where ((trunc(idt)>to_date('${moment(dt).format('DD/MM/YYYY')}','dd/mm/yyyy') and to_number(seq)<to_number('${doc.seq}')) or (trunc(idt)<to_date('${moment(dt).format('DD/MM/YYYY')}','dd/mm/yyyy') and to_number(seq)>to_number('${doc.seq}'))) and type=:type and form=:form and serial=:serial and stax=:stax and status>1 and status<>4 and id<>${id} and rownum <= 100`, [doc.type, doc.form, doc.serial, doc.stax])
            row = result.rows[0]
            if (row.rc > 0) throw new Error(`Tồn tại HĐ có ngày lớn hơn, số nhỏ hơn hoặc ngày nhỏ hơn, số lớn hơn \n (Invoice has a larger date, smaller invoice number or smaller date, larger date)`)

            if (dateChange.toDate() < fd) {
                fd = moment(fd).format('DD-MM-YYYY HH:mm:ss')
                res.json({ fd: fd, stt: 0 })
            } else if (td && dateChange.toDate() > td) {
                td = moment(td).format('DD-MM-YYYY HH:mm:ss')
                res.json({ td: td, stt: 1 })
            } else {
                //Update ngày hóa đơn
                doc.idt = moment(dateChange).format('YYYY-MM-DD HH:mm:ss')
                let sql = `Update s_inv set idt = :1, doc = :2 where id =:3`
                result = await dbs.query(sql, [dateChange.toDate(), JSON.stringify(doc), id])
                const sSysLogs = { fnc_id: 'inv_date_change', src: config.SRC_LOGGING_DEFAULT, dtl: `Sửa ngày hóa đơn của hóa đơn ${id} từ ${doc.idt} sang ${dateChange}`, msg_id: id, doc: JSON.stringify(body) };
                await logging.ins(req, sSysLogs, next)
                return res.json(1)
            }
        } catch (err) {
            next(err)
        }
    },
    changeStatus: async (req, res, next) => {
        try {
            const body = req.body
            let id = body.id, result, oldST
            let doc = await rbi(id)
            oldST = doc.status
            doc.status = 2
            let sql = `Update s_inv set doc = :1 where id =:2`
            result = await dbs.query(sql, [JSON.stringify(doc), id])
            const sSysLogs = { fnc_id: 'inv_status_change', src: config.SRC_LOGGING_DEFAULT, dtl: `Sửa trạng thái hóa đơn của hóa đơn ${id} từ ${exSTATUS(oldST)} sang ${exSTATUS(doc.status)}`, msg_id: id, doc: JSON.stringify(doc) };
            await logging.ins(req, sSysLogs, next)
            return res.json(1)
        } catch (err) {
            next(err)
        }
    },
    apprpostou: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const taxc = token.taxc, body = req.body, ids = body.ids
            let i = 0, str = "", result, sql
            for (const id of ids) {
                sql = `select id "id",doc "doc",bmail "bmail",btel "btel" from s_inv where id  =:1 and stax=:stax and status=2 and edit=1`
                result = await dbs.query(sql, [id, taxc])

                let doc = result.rows[0].doc
                doc = util.parseJson(doc)

                doc.bname = doc.bname_
                doc.btax = doc.btax_
                doc.baddr = doc.baddr_
                doc.bmail = doc.bmail_
                doc.btel = doc.btel_
                doc.buyer = doc.buyer_
                doc.bacc = doc.bacc_

                doc.edit = 2
                await dbs.query(`update s_inv set doc=:doc where id=:id`, [JSON.stringify(doc), id])

                const sSysLogs = { fnc_id: 'inv_put', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt thông tin khách hàng id ${id}`, msg_id: id, doc: JSON.stringify(body) };
                logging.ins(req, sSysLogs, next)

            }
            res.json(1)

        }
        catch (err) {
            next(err)
        }
    },
    appgetou: async (req, res, next) => {
        try {
            const token = SEC.decode(req), query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 10
            let sql, result, ret, iwh = await Service.iwh(req, false)
            if (HDB) {
                iwh.where += " and ou = :ou"
                iwh.binds.push(token.ou)
            }
            sql = `select 0 "chk",id "id",sec "sec",ic "ic",uc "uc",curr "curr" ,pid "pid",cid "cid",idt "idt",status "status",ou "ou",type "type",form "form",serial "serial",seq "seq",btax_ "btax",bname_ "bname",buyer_ "buyer",baddr_ "baddr",note "note",sumv "sumv",vatv "vatv",totalv "totalv",sum "sum",vat "vat",total "total",adjdes "adjdes",adjtyp "adjtyp",cde "cde", cvt "cvt", bmail "bmail"${iwh.ext} from s_inv ${iwh.where} ${iwh.order} OFFSET ${start} ROWS FETCH NEXT ${count} ROWS ONLY`
            result = await dbs.query(sql, iwh.binds)
            ret = { data: result.rows, pos: start }
            if (start == 0) {
                sql = `select count(*) "total" from s_inv ${iwh.where}`
                result = await dbs.query(sql, iwh.binds)
                ret.total_count = result.rows[0].total
            }
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },

}
module.exports = Service