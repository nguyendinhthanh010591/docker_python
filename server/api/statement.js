"use strict"
//const FormData = require("form-data")
//const axios = require("axios")
const objectMapper = require("object-mapper")
const { v4: uuidv4 } = require("uuid")
const moment = require("moment")
const config = require("./config")
const logger4app = require("./logger4app")
const dbtype = config.dbtype
const dbs = require(`./${dbtype}/dbs`)
const sign = require("./sign")
const util = require("./util")
const inc = require("./inc")
const tvan = require("./tvan")
const logging = require("./logging")
//const enumf = require("../util/enumf")
const SEC = require("./sec")
//const redis = require("../util/redis")
//const hbs = require("../util/hbs")
const fcloud = require("./fcloud")
//const SERIAL = require("../api/serial")
//const COL = require("../api/col")
const ous = require(`./ous`)
const ext = require("./ext")
const hsm = require("./hsm")
const dtf = config.dtf
// const WAIT = config.wait
// const MAXD = moment("31/12/2099", mfd).endOf("day").toDate()
// const CARR = ["c0", "c1", "c2", "c3", "c4", "c5", "c6", "c7", "c8", "c9"]
/*
const execsqlselect = (sql, binds) => {
    return new Promise(async (resolve, reject) => {
        try {
            let sqltmp = `${sql} `, arrtobind = sqltmp.split("?"), sqlrun = ``
            if (arrtobind.length == 1) {
                sqlrun = sqltmp
            } else {
                for (let i = 1; i < arrtobind.length; i++) {
                    let vbind
                    switch (dbtype) {
                        case "mssql":
                            vbind = `@${i}`
                            break
                        case "mysql":
                            vbind = `?`
                            break
                        case "orcl":
                            vbind = `:${i}`
                            break
                        case "pgsql":
                            vbind = `$${i}`
                            break
                    }
                    sqlrun += (arrtobind[i - 1] + vbind)
                }
                sqlrun += arrtobind[arrtobind.length - 1]
            }
            const result = await dbs.query(sqlrun, binds)
            let rows
            switch (dbtype) {
                case "mssql":
                    rows = result.recordset
                    break
                case "mysql":
                    rows = result[0]
                    break
                case "orcl":
                    rows = result.rows
                    break
                case "pgsql":
                    rows = result.rows
                    break
            }
            resolve(rows)
        } catch (err) {
            reject(err)
        }
    })
}
const execsqlinsupddel = (sql, binds) => {
    return new Promise(async (resolve, reject) => {
        try {
            let sqltmp = `${sql} `, arrtobind = sqltmp.split("?"), sqlrun = ``
            if (arrtobind.length == 1) {
                sqlrun = sqltmp
            } else {
                for (let i = 1; i < arrtobind.length; i++) {
                    let vbind
                    switch (dbtype) {
                        case "mssql":
                            vbind = `@${i}`
                            break
                        case "mysql":
                            vbind = `?`
                            break
                        case "orcl":
                            vbind = `:${i}`
                            break
                        case "pgsql":
                            vbind = `$${i}`
                            break
                    }
                    sqlrun += (arrtobind[i - 1] + vbind)
                }
                sqlrun += arrtobind[arrtobind.length - 1]
            }
            await dbs.query(sqlrun, binds)
            resolve(1)
        } catch (err) {
            reject(err)
        }
    })
}
*/
const sql_history = {
    sql_history_mssql:  `select vh.id,
                            vh.type_invoice,
                            vh.id_ref,
                            vh.status,
                            FORMAT(vh.datetime_trans,'dd/MM/yyyy HH:mm:ss') datetime_trans,
                            vh.description,
                            vh.r_xml
                            from van_history vh join s_statement ss on vh.id_ref=ss.id where vh.type_invoice=5 and vh.id_ref=? order by vh.datetime_trans`,
    sql_history_mysql:  `select vh.id,
                            vh.type_invoice,
                            vh.id_ref,
                            vh.status,
                            DATE_FORMAT(vh.datetime_trans,'%d/%m/%Y %T') datetime_trans,
                            vh.description,
                            vh.r_xml
                        from van_history vh join s_statement ss on vh.id_ref=ss.id where vh.type_invoice=5 and vh.id_ref=? order by vh.datetime_trans`,
    sql_history_orcl:  `select vh.id "id",
                            vh.type_invoice "type_invoice",
                            vh.id_ref "id_ref",
                            vh.status "status",
                            to_char(vh.datetime_trans,'DD/MM/YYYY HH24:MI:SS') "datetime_trans",
                            vh.description "description",
                            vh.r_xml "r_xml"
                        from van_history vh join s_statement ss on vh.id_ref=ss.id where vh.type_invoice=5 and vh.id_ref=? order by vh.datetime_trans`,
    sql_history_pgsql:  `select vh.id,
                            vh.type_invoice,
                            vh.id_ref,
                            vh.status,
                            to_char(vh.datetime_trans,'DD/MM/YYYY HH:MI:SS') datetime_trans,
                            vh.description,
                            vh.r_xml
                        from van_history vh join s_statement ss on vh.id_ref=ss.id where vh.type_invoice=5 and vh.id_ref=? order by vh.datetime_trans`
}

const rbi = (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            const result = await inc.execsqlselect(`select doc "doc", status "status",cqtstatus "cqtstatus", vdt "vdt" from s_statement where id=?`, [id])
            let rows = result
            if (rows.length == 0) reject(new Error(`Không tìm thấy Tờ khai ${id}`))
            rows[0].doc = util.parseJson(rows[0].doc)
            resolve(rows[0])
        } catch (err) {
            reject(err)
        }
    })
}

const Service = {
    post: async (req, res, next) => {
        try {
            const token = SEC.decode(req), org = await ous.org(token), body = req.body
            let id
            let result, sql
            body.uc = token.uid
            body.stax = org.taxc
            body.sname = org.name
            body.taxo = org.taxo
            if (body.action == "create" || body.action == "update") {
                if (body.hascode == 1) {
                    body.dtsendtype = body.dtsendtype1
                    body.cdlqtctn = 0
                }
                if (body.hascode == 0) {
                    body.dtsendtype = body.dtsendtype2
                    body.cdlqtctn = 1
                }
            }
            //body.sendtype==1 ? body.nntdbkkhan=1 : body.nntdbkkhan=0
            //body.sendtype==2 ? body.nntktdnubnd=1 : body.nntktdnubnd=0
            body.nntdbkkhan = 0
            body.nntktdnubnd = 0
            body.cdlttdcqt = 0
            body.dtsendtype.includes("sendinv") ? body.sendinv = 1 : body.sendinv = 0
            body.dtsendtype.includes("listinv") ? body.listinv = 1 : body.listinv = 0
            body.invtype.includes("01GTKT") ? body.type_gtgt = 1 : body.type_gtgt = 0
            body.invtype.includes("02GTTT") || body.invtype.includes("07KPTQ") ? body.type_hdbhang = 1 : body.type_hdbhang = 0
            //body.invtype.includes("TSCONG") ? body.type_tscong=1 : body.type_tscong=0
            //body.invtype.includes("TSQGIA") ? body.type_tsqgia=1 : body.type_tsqgia=0
            body.type_tscong = 0
            body.type_tsqgia = 0
            body.invtype.includes("01/TVE") || body.invtype.includes("02/TVE") ? body.type_khac = 1 : body.type_khac = 0
            body.invtype.includes("03XKNB") || body.invtype.includes("04HGDL") ? body.type_ctu = 1 : body.type_ctu = 0
            body.createdt = moment(body.createdt).format(dtf)
            if (dbtype == "orcl") body.createdt = new Date(body.createdt)
            delete body.dtsendtype1
            delete body.dtsendtype2
            switch (body.action) {
                //if (body.action == "create") {
                case "create":
                    delete body.action
                    delete body.id
                    sql = `INSERT INTO s_statement (type, 
                        formname,
                        regtype,
                        sname,
                        stax,
                        paxoname,
                        paxo,
                        taxoname,
                        taxo,
                        contactname,
                        contactaddr,
                        contactemail,
                        contactphone,
                        place,
                        createdt,
                        hascode,
                        sendtype,
                        dtsendtype,
                        invtype,
                        doc)
                    VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`
                    result = await inc.execsqlinsupddel(sql, [body.type, body.formname, body.regtype, org.name, org.taxc, org.paxoname, org.paxo, org.taxoname, org.taxo, body.contactname, body.contactaddr, body.contactemail, body.contactphone, body.place, body.createdt, body.hascode, body.sendtype, body.dtsendtype, body.invtype, JSON.stringify(body)])
                    res.json({affectedRows: result})
                    let sSysLogs = { fnc_id: 'stm_post', src: config.SRC_LOGGING_DEFAULT, dtl: `Lập tờ khai `}
                    logging.ins(req, sSysLogs, next)
                    break
                //if (body.action == "approve"){
                case "cancel":
                    id = body.id
                    sql = `update s_statement set status=3 where id=?`
                    result = await inc.execsqlinsupddel(sql, [id])
                    res.json({affectedRows: result})

                    break
                case "delete":
                    id = body.id
                    sql = `delete from s_statement where id=?`
                    result = await inc.execsqlinsupddel(sql, [id])
                    res.json({affectedRows: result})

                    break
                case "update":
                    delete body.action
                    sql = `update s_statement set regtype=?, contactname=?, contactaddr=?, contactphone=?, contactemail =?, place=?, createdt=?, hascode=?, sendtype=?, dtsendtype=?, invtype=?, doc=? where id=?`
                    let binds = [body.regtype, body.contactname, body.contactaddr, body.contactphone, body.contactemail
                        , body.place, body.createdt, body.hascode, body.sendtype, body.dtsendtype, body.invtype, JSON.stringify(body), body.id]
                    result = await inc.execsqlinsupddel(sql, binds)
                    res.json({affectedRows: result})

                default:
                    break
            }
            logging.ins(req, sSysLogs, next)
        } catch (err) {
            next(err)
        }
    },

    hascode: async (req, res, next) => {
        try {
            const token = SEC.decode(req), org = await ous.org(token), body = req.body
            let sql = `update s_ou set hascode=? where taxc=?`
            let binds = [body.hascode, org.taxc]
            let result = await inc.execsqlinsupddel(sql, binds)
            res.json({affectedRows: result})
        } catch (err) {
            next(err)
        }
    },
    hscqt: async (req, res, next) => {
        try {
            const token = SEC.decode(req), schema = token.schema, params = req.params, id = params.id
            let sql =  sql_history[`sql_history_${config.dbtype}`]
            let binds = [id]
            let result = await inc.execsqlselect(sql, binds)
            res.json(result)
        } catch (err) {
            next(err)
        }
    },
    hscqtXml: async (req, res, next) => {
        try {
            const token = SEC.decode(req), schema = token.schema, params = req.params, id = params.id
            let sql =  `select vh.r_xml "r_xml" from van_history vh where id = ?`
            let binds = [id]
            let result = await inc.execsqlselect(sql, binds)
            res.json(result)
        } catch (err) {
            next(err)
        }
    },
    getConfigCertData: async (req, res, next) => {
        try {
            const token = SEC.decode(req), org = await ous.org(token), mst = token.taxc
            // GET INFO CERT
            let certData
            if (org.sign == 2) {
                let certDatainfo = await sign.getFileSignInfo(token.taxc, util.decrypt(org.pwd))
                let objmap = {
                    "issuer": "cn",
                    "subject": "subject",
                    "mst": "mst",
                    "serial": "serialNumber",
                    "fd": "fd",
                    "td": "td"
                }
                certData = objectMapper(certDatainfo, objmap)
            }
            else if (org.sign == 3) {
                let usr = util.decrypt(org.usr)
                certData = await fcloud.getCaSignInfo(usr, mst)
            }
            else {
                // USB TOKEN
            }
            //
            res.json(certData ? [certData] : [])
        } catch (err) {
            next(err)
        }
    },

    get: async (req, res, next) => {
        try {
            const token = SEC.decode(req), taxc = token.taxc, query = req.query, filter = JSON.parse(query.filter),
                form_from = filter.form_from, form_to = filter.form_to, fd = moment(form_from).startOf("day").format(dtf), td = moment(form_to).endOf("day").format(dtf)
            if (filter.status == 1 || filter.status == 3) {
                delete filter.cqtstatus
            }
            let sql, result, binds = [taxc, fd, td], val
            if (dbtype == "orcl") binds =  [taxc, new Date(fd), new Date(td)]
            let where = " where stax =? and createdt between ? and ?"
            Object.keys(filter).forEach((key) => {
                val = filter[key]
                if (val) {
                    switch (key) {
                        case "stax":
                            break
                        case "form_from":
                            break
                        case "form_to":
                            break
                        case "ou":
                            where += ` and ou.id=?`
                            binds.push(val)
                            break
                        case "status":
                            where += ` and st.status=?`
                            binds.push(val)
                            break
                        default:
                            where += ` and ${key}=?`
                            binds.push(val)
                            break
                    }
                }
            })
            // sql = `select id "id",invtype "invtype",createdt "createdt",sname "sname",stax "stax",taxoname "taxoname",hascode "hascode",sendtype "sendtype",dtsendtype "dtsendtype",regtype "regtype",vdt "vdt",status "status",cqtstatus "cqtstatus",uc "uc" from s_statement ${where} order by id desc`
            sql = `select distinct st.id "id",st.invtype "invtype", cast(st.createdt as date) "createdt",st.sname "sname",st.stax "stax",st.taxoname "taxoname",st.hascode "hascode",st.sendtype "sendtype",st.dtsendtype "dtsendtype",st.regtype "regtype",st.vdt "vdt",st.status "status",st.cqtstatus "cqtstatus",st.uc "uc" from s_statement st, s_ou ou ${where} and st.stax = ou.taxc order by st.id desc`
            //result = await dbs.query(sql, binds)
            result = await inc.execsqlselect(sql, binds)
            let sSysLogs = { fnc_id: 'stm_get', src: config.SRC_LOGGING_DEFAULT, dtl: `Tra cứu tờ khai `}
            logging.ins(req, sSysLogs, next)
            res.json(result)
        } catch (err) {
            next(err)
        }
    },

    load: async (req, res, next) => {
        try {
            const token = SEC.decode(req), org = await ous.org(token)
            //if (!(role.includes(21) || role.includes(22))) return res.status(403).send("Không có quyền thực hiện")
            const schema = token.schema, id = req.params.id
            let doc = await rbi(id)
            doc=util.parseJson(doc)
            let doct = doc.doc
            doct= util.parseJson(doct)
            doct.taxo = org.taxo
            doct.taxoname = org.taxoname
            doc.doc=doct
            res.json(doc)
        } catch (err) {
            next(err)
        }
    },

    // check: async (req, res, next) => {
    //     try {
    //         const token = SEC.decode(req), schema = token.schema, mst = token.mst
    //         let sql = `select * from s_statement where stax=? and status=7 and regtype=1 `
    //         let result = await dbs.query(sql, [mst])
    //         res.json(result[0].length)
    //     } catch (err) {
    //         next(err)
    //     }
    // },

    apprpost: async (req, res, next) => {
        try {
            const token = SEC.decode(req), mst = token.mst || token.taxc, body = req.body, id = body.id,
                org = await ous.org(token), ts = org.ts, degree_config = config.DEGREE_CONFIG
            let sql, result, rows, syslog
            sql = `select id "id",doc "doc" from s_statement where id in (?) and stax=? and status=1`
            result = await inc.execsqlselect(sql, [id[0], token.taxc])  // Duyệt 1 tờ khai 1 lần
            rows = result
            if (rows.length == 0) throw new Error("Không có dữ liệu để ký")
            await inc.checkrevokeStatement(org)
            //sql = `update s_statement set doc=JSON_SET(doc,'$.status',2,'$.adt',?),status=2,xml=? where id=? and stax=? and status=1`
            sql = `update s_statement set doc=?,status=2,xml=? where id=? and stax=? and status=1`
            let count = 0
            if (org.sign == 2) {
                const ca = await sign.ca(token.taxc, util.decrypt(org.pwd))
                for (const row of rows) {
                    let doc = util.parseJson(row.doc), xml
                    doc.adt = moment().format(dtf)
                    doc.status = 2
                    util.viewadj(doc)
                    const id = row.id
                    const str = await util.j2x(doc, id)
                    const PATH_XML = await util.getPathXml(doc.type)
                    const signSample = await util.getSignSample(doc.type, doc.adt/*doc.createdt*/, id)
                    if (util.isTicket(doc.type) && ts == "0") xml = str
                    else {
                        xml = sign.sign123(ca, str, id, PATH_XML, signSample)
                    }
                    //await dbs.query(sql, [doc.adt, xml, id, mst])
                    const ret = await inc.execsqlinsupddel(sql, [JSON.stringify(doc), xml, id, token.taxc])
                    let jsontvan = JSON.parse(JSON.stringify(doc))
                    jsontvan.msgType = 'REG'
                    jsontvan.id = id
                    // let paramsDataTVan = { 
                    //     ids: [id],
                    //     type: '01/ÐKTÐ-HÐÐT'
                    // }
                    try {
                        await inc.execsqlinsupddel(`INSERT INTO van_einv_listid_msg (pid, type, status_scan) values (?,?,?)`, [id, 'TK', 0])

                       // await tvan.dataTvanApi(token, paramsDataTVan, req)
                        await inc.execsqlinsupddel(`update s_statement set statustvan=0 where id=? and stax=? and status=2`, [ id, token.taxc])
                    } catch (error) {
                        await inc.execsqlinsupddel(`update s_statement set status=1 where id=? and stax=?`, [ id, token.taxc])
                        throw new Error(error)
                    }
                   
                  //  let tvanret = await tvan.sendMsgToTvanQueue(jsontvan, xml)
                    //await Log.insLog(req, syslog, next)
                    count++
                }
                //if (arr.length > 0) res.json({ mst: mst, date: new Date(), ref: config.PATH_XML, arr: arr })
                res.json(count)
            }
            else if (org.sign == 3) {
                for (const row of rows) {
                    row.doc.adt = moment().format(dtf)
                    util.viewadj(row.doc)
                }
                const usr = util.decrypt(org.usr), pwd = util.decrypt(org.pwd), arr = await fcloud.xml(usr, pwd, mst, rows, ts, degree_config)
                for (let row of arr) {
                    await dbs.query(sql, [row.doc.adt, row.xml, row.id, mst])
                    count++
                    //const syslog = { func_id: "inv_appr", action: "Duyệt hóa đơn", dtl: JSON.stringify({ form: row.form, serial: row.serial, seq: row.seq, sec: row.sec }) }
                    //await Log.insLog(req, syslog, next)
                    //await wnoInsdb2group(token, row.doc)
                }
                //if (mail == 1) sendfiles(schema, org, mst, incs, { funcId: enumf.MAIL.APP_APPROVE_INVOICE })
                res.json(count)
            }
            else if (org.sign == 4) {
                for (const row of rows) {
                    let doc = util.parseJson(row.doc)
                    doc.adt = moment().format(dtf)
                    doc.status = 2
                    util.viewadj(doc)
                }
                const  arr = await hsm.xml(rows,"TK")
                for (let row of arr) {
                    const ret = await inc.execsqlinsupddel(sql, [row.doc, row.xml, row.id, token.taxc])
                    let jsontvan = util.parseJson(row.doc)
                    jsontvan.msgType = 'REG'
                    jsontvan.id = id
                    try {
                        await inc.execsqlinsupddel(`INSERT INTO van_einv_listid_msg (pid, type, status_scan) values (?,?,?)`, [row.id, 'TK', 0])
                        await inc.execsqlinsupddel(`update s_statement set statustvan=0 where id=? and stax=? and status=2`, [ row.id, token.taxc])
                    } catch (error) {
                        await inc.execsqlinsupddel(`update s_statement set status=1 where id=? and stax=?`, [ id, token.taxc])
                        throw new Error(error)
                    }
                    count++
                }
                res.json(count)
            }
            else {
                let arr = []
                for (const row of rows) {
                    let doc = util.parseJson(row.doc)
                    doc.adt = moment().format(dtf)
                    util.viewadj(doc)
                    const id = row.id, xml = await util.j2x(doc, id), inc = id
                        , PATH_XML = await util.getPathXml(doc.type)
                        , signSample = await util.getSignSample(doc.type, doc.adt/*doc.createdt*/, inc)
               //     if (degree_config == "123") await dbs.query(`update s_statement set doc=JSON_SET(doc,'$.adt',?) where id=? and stax=? and status=1`, [doc.adt, id, mst])
                        let  str = xml ,xml_sign_time = signSample.signingTime
                        , xml_end_tag_replace = signSample.xmlEndTagReplace
                        , object_signing_time = `${xml_sign_time}${xml_end_tag_replace}`
                        // Add signature that have signing time to xml
                        str = str.replace(xml_end_tag_replace, object_signing_time)
                        str = str.replace(`<DLTKhai>`,`<DLTKhai Id="${id}">`)
                        arr.push({
                            id :inc,
                            xml: Buffer.from(str).toString("base64"),
                            reference1:id,                            
                            reference2 :`SigningTime-${signSample.tagBegin}-${inc}`,
                            tagSign:"DLTKhai",
                            tagEnd:"</DLTKhai>",
                            idt: moment().format(dtf)
                        })
                    // if (util.isTicket(doc.type) && ts == "0") {
                    //     await dbs.query(sql, [doc.createdt, xml, id, mst])
                    //     count++
                    //const syslog = { func_id: "inv_appr", action: "Duyệt hóa đơn", dtl: JSON.stringify({ form: row.form, serial: row.serial, seq: row.seq, sec: row.sec }) }
                    //await Log.insLog(req, syslog, next)
                    //await wnoInsdb2group(token, doc)
                    // }
                    // else {
                    //     arr.push({ id: id, createdt: doc.createdt, xml: Buffer.from(xml).toString("base64") })
                    // }
                }
                if (arr.length > 0) { res.json({ mst: mst, date: new Date(), arr: arr,SIGN_TYPE: org.sign}) }
                // if (arr.length > 0) res.json({ mst: mst, date: new Date(), ref: config.PATH_XML, arr: arr })
                else res.json(count)
            }
            let sSysLogs = { fnc_id: 'stm_appr', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt tờ khai `}
            logging.ins(req, sSysLogs, next)
        }
        catch (err) {
            next(err)
        }
    },

    apprput: async (req, res, next) => {
        try {
            const token = SEC.decode(req), schema = token.schema, mst = token.taxc, body = req.body, id = body.incs, signs = body.signs, org = await ous.org(token) // mail = body.mail, 
            let sql, result, rows, count = 0
            sql = `select id,doc from s_statement where id in (?) and stax=? and status=1`
            result = await inc.execsqlselect(sql, [id, mst])
            rows = result
            if (result.length > 0) {
                // sql = `update s_statement set doc=JSON_SET(doc,'$.status',2),status=2,xml=? where id=? and stax=? and status=1`
                sql = `update s_statement set doc=?,status=2,xml=? where id=? and stax=? and status=1`

                for (const row of rows) {
                    const id = row.id, sign = signs.find(item => item.incs === id)
                    let doc =  util.parseJson(row.doc)
                    // , signSample = await util.getSignSample(doc.type, doc.createdt, id)
                    if (sign && sign.xml) {
                        let xml = Buffer.from(sign.xml, "base64").toString() // xml = util.signature(sml, signSample)
                        xml = xml.replace(`<?xml version="1.0" encoding="UTF-8" standalone="no"?>`,``)
                        doc.status = 2
                        await inc.execsqlselect(sql, [JSON.stringify(doc),xml, id, mst])
                        // if (mail == 1) {
                        //     row.doc.status = 2
                        //     row.xml = xml
                        // }
                        try {
                            await inc.execsqlinsupddel(`INSERT INTO van_einv_listid_msg (pid, type, status_scan) values (?,?,?)`, [id, 'TK', 0])
    
                           // await tvan.dataTvanApi(token, paramsDataTVan, req)
                            await inc.execsqlinsupddel(`update s_statement set statustvan=0 where id=? and stax=? and status=2`, [ id, token.taxc])
                        } catch (error) {
                            await inc.execsqlinsupddel(`update s_statement set status=1 where id=? and stax=?`, [ id, token.taxc])
                            throw new Error(error)
                        }
                        count++
                    }
                }
                // if (mail == 1) await sendfiles(schema, org, mst, id, { funcId: enumf.MAIL.APP_APPROVE_INVOICE })
            }
            let sSysLogs = { fnc_id: 'stm_appr', src: config.SRC_LOGGING_DEFAULT, dtl: `Duyệt tờ khai `}
            logging.ins(req, sSysLogs, next)
            res.json(count)
        }
        catch (err) {
            next(err)
        }
    },
    SendbackToVanSTM: async (req, res) => {
        try {
            const id = req.params.id
            await inc.execsqlinsupddel(`INSERT INTO van_einv_listid_msg (pid, type, status_scan) values (?,?,?)`, [id, 'TK', 0])
            await inc.execsqlinsupddel(`update s_statement set cqtstatus=0 where id=? `, [id])
            res.json({ result: 1 })
        }
        catch (err) {
           console.trace(err)
           res.json({ result: 0 })
        }

    },

    htm: async (req, res, next) => {
        try {
            const token = SEC.decode(req), id = req.params.id, body = await rbi(id), doc = util.parseJson(body.doc), org = await ous.org(token), qrc = org.qrc
            doc.regtype == 1 ? doc.regtype = true : doc.regtype = false
            doc.hascode == 1 ? doc.hascode = true : doc.hascode = false
            doc.nntdbkkhan == 1 ? doc.nntdbkkhan = true : doc.nntdbkkhan = false
            doc.nntktdnubnd == 1 ? doc.nntktdnubnd = true : doc.nntktdnubnd = false
            doc.cdlttdcqt == 1 ? doc.cdlttdcqt = true : doc.cdlttdcqt = false
            doc.cdlqtctn == 1 ? doc.cdlqtctn = true : doc.cdlqtctn = false
            doc.sendinv == 1 ? doc.sendinv = true : doc.sendinv = false
            doc.listinv == 1 ? doc.listinv = true : doc.listinv = false
            doc.type_gtgt == 1 ? doc.type_gtgt = true : doc.type_gtgt = false
            doc.type_hdbhang == 1 ? doc.type_hdbhang = true : doc.type_hdbhang = false
            doc.type_tscong == 1 ? doc.type_tscong = true : doc.type_tscong = false
            doc.type_tsqgia == 1 ? doc.type_tsqgia = true : doc.type_tsqgia = false
            doc.type_khac == 1 ? doc.type_khac = true : doc.type_khac = false
            doc.type_ctu == 1 ? doc.type_ctu = true : doc.type_ctu = false
            doc.idt = `ngày ${new Date(doc.createdt).getDate()} tháng ${new Date(doc.createdt).getMonth() + 1} năm ${new Date(doc.createdt).getFullYear()}.`
            for (let item of doc.items) {
                item.sign_from = moment(item.sign_from).format('DD/MM/YYYY hh:mm:ss')
                item.sign_to = moment(item.sign_to).format('DD/MM/YYYY hh:mm:ss')
                switch (item.action) {
                    case "1":
                        item.action = "Thêm mới"
                        break
                    case "2":
                        item.action = "Gia hạn"
                        break
                    case "3":
                        item.action = "Ngưng sử dụng"
                        break
                    default:
                        break
                }
            }
            if (!([2, 3, 4].includes(body.status))) doc.sec = ""
            //if (qrc) doc.qrcode = await util.qrcode(doc, qrc)
            const tmp = await util.templatestm(doc.stax, doc.form)
            //const tmp = await util.template(doc.stax, doc.form)
            let obj = { doc: doc, tmp: tmp }
            let reqjsr = ext.createRequest(obj)
            // res.json({ status: status, doc: doc, tmp: tmp })
            const bodyBuffer = await util.jsReportRenderAttach(reqjsr)
            let sizeInByte = bodyBuffer.byteLength
            res.json({ pdf: `data:application/pdf;base64, ${bodyBuffer.toString('base64')}`, sizePDF: sizeInByte, vdt: body.vdt, status: body.status,cqtstatus: body.cqtstatus })
            //res.json({ doc: doc, vdt: body.vdt, tmp: tmp, status: body.status })
        } catch (err) {
            next(err)
        }
    },
    // VIEW PDF TCT RESPONSE
    vpdftctresp: async (req, res, next) => {
        try {
            const token = SEC.decode(req), schema = token.schema, id = req.params.id, query = req.query, status = `${query.status}`, fn = query.fn
                , body = await rbi(id), org = await ous.org(token)
            let sql, result, rows, colsSelect = "",
                objResult = { doc: JSON.stringify({}), tmp: "" }

            if (fn == "statement_tct_received") colsSelect = "json_received"
            else if (fn == "statement_tct_accepted") colsSelect = "json_accepted"
            sql = `select ${colsSelect} "jsondata",cqtstatus "cqtstatus",createdt "createdt"  from s_statement where id=?`
            result = await inc.execsqlselect(sql, [id])
            rows = result
            if (rows.length) {
                let doc = rows[0].jsondata
                doc = util.parseJson(doc)
                doc.createdt = rows[0].createdt
                doc = JSON.stringify(doc)
                const tmp = await util.templatestmtct(fn)

                // TEST DOC - JSON
                // doc = {"noti_form":"01/TB-TNĐT","noti_name":"Thông báo về việc không tiếp nhân tờ khai đăng ký/thay đổi thông tin (Mẫu 01/ĐKTĐ-HĐĐT)","noti_taxnum":"0103210000000051","place":"Hà Nội","noti_taxdt":"2021-10-31","stax":"0107816697","sname":"Mã số Thuế Test 55","name":"Tờ khai đăng ký/thay đổi thông tin sử dụng hóa đơn điện tử","tax_transcode":"V010412856565169BFDB53A4206B118DEAD857096B9","time_send":"2021-10-31 10:23:23","tax_status":1,"tax_time":"2021-10-31 10:23:23","err_code":"1008","err_des":"Trạng thái MST của NNT không hoạt động"}
                //

                // objResult.doc = doc
                // objResult.tmp = tmp
                let obj = { doc: doc, tmp: tmp }
                let reqjsr = ext.createRequest(obj)
                // res.json({ status: status, doc: doc, tmp: tmp })
                const bodyBuffer = await util.jsReportRenderAttach(reqjsr)
                let sizeInByte = bodyBuffer.byteLength
                res.json({ pdf: `data:application/pdf;base64, ${bodyBuffer.toString('base64')}` })
            } else {
                throw new Error("not_found***statement")
            }
            //res.json(objResult)
        } catch (err) {
            next(err)
        }
    }
}
module.exports = Service