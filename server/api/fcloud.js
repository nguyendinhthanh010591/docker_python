"use strict"
const path = require("path")
const fs = require("fs")
const soapRequest = require("easy-soap-request")
const crypto = require("crypto")
const forge = require("node-forge")
const util = require("./util")
const redis = require("./redis")
const sign = require("./sign")
const url = "https://trusted-hub.com:8443/eSignCloud/Services?wsdl"
const relyingParty = "FIS"
const relyingPartyUser = "fis"
const relyingPartyPassword = "fis@123"
const relyingPartySignature = "XzG6OMBpYCrasYoEb++TvY2E5QF5mUCZaGY09LbQ2jZ0SFbJmX6kCQZy8bjN6bbfWJGqbqacBbEuVtRfGrTuWF4vLUGi0TOzbkWSmQrFUo0/DNbmYaNj+7TROwg+KYyFginphlaH7xhCCgkm0eHZNjK7yWobfazWGRx7h/yodKCIJeeTjdX/g0x5YlZK7WrInPWt2f3rFCrDMt4dWTQMg8fbZBC3Wy2cvnWt1m8rHa8S1Y4B2pJD0PU4Kr//MSRqOlJQkg8Pi34Q6p27vZ+orme3wGoOIUQwpRdZwKfw0oY4nZQeyuO48jwc+7s8ne2jlw6OrBItNXUisdBXEqs1yA=="
const relyingPartyKeyStore = "fis.p12"
const relyingPartyKeyStorePassword = "fis@123"
const relyingKeyRedis = `FCLOUD.${relyingParty}`
const algorithm = "SHA-256"
const getSign = async (str) => {
    let b64 = await redis.get(relyingKeyRedis)
    if (!b64) {
        const FILE = path.join(__dirname, "..", "..", `temp/${relyingPartyKeyStore}`)
        b64 = fs.readFileSync(FILE, "base64")
        await redis.set(relyingKeyRedis, b64)
    }
    const der = forge.util.decode64(b64)
    const asn = forge.asn1.fromDer(der)
    const p12 = forge.pkcs12.pkcs12FromAsn1(asn, false, relyingPartyKeyStorePassword)
    const bags = p12.getBags({ bagType: forge.pki.oids.pkcs8ShroudedKeyBag })
    const bag = bags[forge.pki.oids.pkcs8ShroudedKeyBag][0]
    const key = forge.pki.privateKeyToPem(bag.key)
    const signer = crypto.createSign("RSA-SHA1")
    signer.update(str)
    signer.end()
    return signer.sign(key, "base64")
}

const certificate = async (agreementUUID, mst) => {
    let cert
    const header = { "Content-Type": "text/xmlcharset=UTF-8", "soapAction": `${url}#getCertificateDetailForSignCloud` }
    const timestamp = Date.now(), pkcs1Signature = await getSign(`${relyingPartyUser}${relyingPartyPassword}${relyingPartySignature}${timestamp}`)
    const xml = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:api="http://api.esigncloud.mobileid.vn/">
    <soapenv:Header/>
    <soapenv:Body>
        <api:getCertificateDetailForSignCloud>
            <signCloudReq>
                <agreementUUID>${agreementUUID}</agreementUUID>
                <credentialData>
                    <password>${relyingPartyPassword}</password>
                    <pkcs1Signature>${pkcs1Signature}</pkcs1Signature>
                    <signature>${relyingPartySignature}</signature>
                    <timestamp>${timestamp}</timestamp>
                    <username>${relyingPartyUser}</username>
                </credentialData>
                <relyingParty>${relyingParty}</relyingParty>
            </signCloudReq>
        </api:getCertificateDetailForSignCloud>
    </soapenv:Body>
    </soapenv:Envelope>`
    const { response } = await soapRequest({ url: url, headers: header, xml: xml, timeout: 30000 })
    const { body, statusCode } = response
    if (statusCode === 200) {
        const xbody = util.x2j(body)
        const json = xbody['S:Envelope']['S:Body']['ns2:getCertificateDetailForSignCloudResponse'].return
        if (json.responseCode === 0 || json.responseCode === 1018) {
            cert = json.certificate
            const sysdate = new Date(), info = sign.info(cert)
            if (info.mst !== mst) throw new Error(`MST của chứng thư số ${info.mst} khác MST người dùng ${mst} \n (The tax code of digital certificate ${info.mst} different from the tax code of customer)`)
            if (sysdate > new Date(info.td) || sysdate < new Date(info.fd)) throw new Error("Thời hạn chứng thư số không hợp lệ \n(Invalid digital certificate period)")
        }
    }
    return cert
}

exports.xml = async (agreementUUID, authorizeCode, mst, rows) => {
    //await certificate(agreementUUID, mst)
    const header = { "Content-Type": "text/xmlcharset=UTF-8", "soapAction": `${url}#prepareFileForSignCloud` }
    const timestamp = Date.now(), pkcs1Signature = await getSign(`${relyingPartyUser}${relyingPartyPassword}${relyingPartySignature}${timestamp}`)
    for (let row of rows) {
        const id = row.id, doc = JSON.parse(row.doc), str = util.j2x(doc, id, true)
        const xml = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:api="http://api.esigncloud.mobileid.vn/">
            <soapenv:Header/>
            <soapenv:Body>
                <api:prepareFileForSignCloud>
                    <signCloudReq>
                        <agreementUUID>${agreementUUID}</agreementUUID>
                        <authorizeCode>${authorizeCode}</authorizeCode>
                        <authorizeMethod>4</authorizeMethod>
                        <credentialData>
                            <password>${relyingPartyPassword}</password>
                            <pkcs1Signature>${pkcs1Signature}</pkcs1Signature>
                            <signature>${relyingPartySignature}</signature>
                            <timestamp>${timestamp}</timestamp>
                            <username>${relyingPartyUser}</username>
                        </credentialData>
                        <mimeType>application/xml</mimeType>
                        <xmlDocument><![CDATA[${str}]]></xmlDocument>
                        <relyingParty>${relyingParty}</relyingParty>
                        <signCloudMetaData>
                            <singletonSigning>
                                <entry>
                                    <key>ALGORITHM</key>
                                    <value>${algorithm}</value>
                                </entry>
                                <entry>
                                    <key>SIGNATUREFORMAT</key>
                                    <value>DSIG_SINGLE_NODE</value>
                                </entry>
                                <entry>
                                    <key>CANONICALIZATIONMETHOD</key>
                                    <value>EXCLUSIVE</value>
                                </entry>
                                <entry>
                                    <key>ATTRIBUTENAME</key>
                                    <value>Id</value>
                                </entry>
                                <entry>
                                    <key>NODETOBESIGNED</key>
                                    <value>_${id}</value>
                                </entry>
                            </singletonSigning>
                     </signCloudMetaData>
                    </signCloudReq>
                </api:prepareFileForSignCloud>
            </soapenv:Body>
            </soapenv:Envelope>`
        const { response } = await soapRequest({ url: url, headers: header, xml: xml, timeout: 30000 })
        const { body, statusCode } = response
        if (statusCode === 200) {
            const xbody = util.x2j(body)
            const json = xbody['S:Envelope']['S:Body']['ns2:prepareFileForSignCloudResponse'].return
            if (json.responseCode === 0 || json.responseCode === 1018) row.xml = Buffer.from(json.signedFileData, "base64").toString()
            else throw new Error(`${json.responseCode}:${json.responseMessage}`)
        }
        else throw new Error(response)
    }
    return rows
}


exports.pdf = async (agreementUUID, authorizeCode, mst, pdf) => {
    //await certificate(agreementUUID, mst)
    const header = { "Content-Type": "text/xmlcharset=UTF-8", "soapAction": `${url}#prepareFileForSignCloud` }
    const timestamp = Date.now(), pkcs1Signature = await getSign(`${relyingPartyUser}${relyingPartyPassword}${relyingPartySignature}${timestamp}`)
    const xml = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:api="http://api.esigncloud.mobileid.vn/">
            <soapenv:Header/>
            <soapenv:Body>
                <api:prepareFileForSignCloud>
                    <signCloudReq>
                        <agreementUUID>${agreementUUID}</agreementUUID>
                        <authorizeCode>${authorizeCode}</authorizeCode>
                        <authorizeMethod>4</authorizeMethod>
                        <credentialData>
                            <password>${relyingPartyPassword}</password>
                            <pkcs1Signature>${pkcs1Signature}</pkcs1Signature>
                            <signature>${relyingPartySignature}</signature>
                            <timestamp>${timestamp}</timestamp>
                            <username>${relyingPartyUser}</username>
                        </credentialData>
                        <relyingParty>${relyingParty}</relyingParty>
                        <mimeType>application/pdf</mimeType>
                        <signingFileData>${pdf.toString("base64")}</signingFileData>
                        <signCloudMetaData>
                            <singletonSigning>
                                <entry>
                                    <key>VISIBLESIGNATURE</key>
                                    <value>False</value>
                                </entry>
                            </singletonSigning>
                        </signCloudMetaData>
                    </signCloudReq>
                </api:prepareFileForSignCloud>
            </soapenv:Body>
            </soapenv:Envelope>`
    const res = await soapRequest({ url: url, headers: header, xml: xml })
    const response = res.response
    let b64
    if (response.statusCode === 200) {
        const body = util.x2j(response.body)
        const json = body['S:Envelope']['S:Body']['ns2:prepareFileForSignCloudResponse'].return
        if (json.responseCode === 0 || json.responseCode === 1018) b64 = json.signedFileData
        else throw new Error(`${json.responseCode}:${json.responseMessage}`)
    }
    else throw new Error(response)
    return b64
}
