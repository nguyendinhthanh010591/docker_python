"use strict"
var uuid = require('uuid');
var axios = require('axios');
const config = require("./config")
var uuid = require('uuid');
const dbtype = config.dbtype
const inc = require("./inc")
const inv = require(`./inv`)
const ous = require("./ous")
const SEC = require("./sec")
const logger4app = require("./logger4app")
const sql_get_inv_01 = {
    sql_get_inv_01_mssql: `SELECT id "id" FROM s_inv WHERE status = 3 and statustaxo = 3 order by id, dt OFFSET 0 ROWS FETCH NEXT ${config.MAX_ROW_JOB_GET} ROWS ONLY `,
    sql_get_inv_01_mysql: `DELETE FROM s_listvalues WHERE keyname=?`,
    sql_get_inv_01_orcl: `DELETE FROM s_listvalues WHERE keyname=:1 AND SYSDATE > TO_DATE(keyvalue, 'YYYY-MM-DD HH24:MI:SS')`,
    sql_get_inv_01_pgsql: `DELETE FROM s_listvalues WHERE keyname=$1`,
}
const sql_get_reg_01 = {
    sql_get_reg_01_mssql: `SELECT id "id" FROM s_inv WHERE status = 3 and statustaxo = 3 order by id, dt OFFSET 0 ROWS FETCH NEXT ${config.MAX_ROW_JOB_GET} ROWS ONLY `,
    sql_get_reg_01_mysql: `DELETE FROM s_listvalues WHERE keyname=?`,
    sql_get_reg_01_orcl: `DELETE FROM s_listvalues WHERE keyname=:1 AND SYSDATE > TO_DATE(keyvalue, 'YYYY-MM-DD HH24:MI:SS')`,
    sql_get_reg_01_pgsql: `DELETE FROM s_listvalues WHERE keyname=$1`,
}
const pretty_xml = (xml) => {
    let new_xml = xml
    // Delete tag <?xml
    new_xml = new_xml.replace(/<\?xml\/?[^>]+(>|$)/, "");
    return new_xml
  }
const Service = {
   
      dataTvanToOut: async (decode_token, body_data, req) => {
      
        try {
          const body = body_data
     
         
          const {
            ids,
            type
          } = body
            , _01DKTD_HDDT = "01/ÐKTÐ-HÐÐT"
            , _04SS_HDDT = "04/SS-HÐÐT"
            , _01TH_HDDT = "01/TH-HĐĐT"
          let select_query
            , table
            , where
            , arr_bind
            , type_xml
            , is_inv = false
            , is_modify_id = false
            , prefix_modify = ""
          switch (type) {
            case _01DKTD_HDDT:
              is_modify_id = true
              prefix_modify = "TKDK_"
              type_xml = 5
              select_query =  `xml "xml", id "id"`
              table = "s_statement"
              where = "id  = ?" // and stax = ?
              arr_bind = [ids[0]] // , mst
              break
            case _04SS_HDDT:
              is_modify_id = true
              prefix_modify = "TBSS_"
              type_xml = 4
              select_query =  `xml "xml", id "id"`
              table = "s_wrongnotice_process"
              where = "id_wn  = ?" //  and stax = ?
              arr_bind = [ids[0]] // , mst
              break
            case _01TH_HDDT:
              is_modify_id = true
              prefix_modify = "BTH_"
              type_xml = 1
              select_query =  `xml "xml", id "id",period "period",s_tax "stax",listinvoice_num "listinvoice_num"`
              table = "s_list_inv"
              where = "id  = ?" // and stax = ?
              arr_bind = [ids[0]] // , mst
              break
            default:
              is_inv = true
              type_xml = 2
              select_query =  `xml "xml", id "id", serial "serial"`
              table = "s_inv"
              where = "id  = ?" // and stax = ?   
              arr_bind = [ids[0]] // , mst
              break
          }
          const sql = `select ${select_query} from ${table} where ${where}`
            , bind = arr_bind
            , result = await inc.execsqlselect(sql, bind)
            , rows = result

          if (type_xml==1){
            let data = rows[0],stax=data.stax,period=data.period,listinvoice_num=data.listinvoice_num, resultck
            if(config.dbtype=="mssql") resultck = await inc.execsqlselect(`select count(id) "count" from s_list_inv where PERIOD = ? and S_TAX=? and CAST(LISTINVOICE_NUM AS INT) <? and ISNULL(STATUS_RECEIVED, 0) not in (12,131,13) `, [period,stax,Number(listinvoice_num)])
            if(config.dbtype=="mysql") resultck = await inc.execsqlselect(`select count(id) "count" from s_list_inv where PERIOD = ? and S_TAX=? and CAST(LISTINVOICE_NUM AS SIGNED) <? AND IFNULL(STATUS_RECEIVED, 0) NOT IN (12,131,13) `, [period,stax,Number(listinvoice_num)])
            if(config.dbtype=="orcl") resultck = await inc.execsqlselect(`select count(id) "count" from s_list_inv where PERIOD = ? and S_TAX=? and to_number(LISTINVOICE_NUM) <? and nvl(STATUS_RECEIVED,0) not in (12,131,13)`, [period,stax,Number(listinvoice_num)])
            let count = resultck[0].count
            if (count > 0 ) {
              await inc.execsqlselect(`update van_einv_listid_msg set status_scan=2 where pid=? and status_scan=0`, [ids[0]])
              return null
            }
          } 
          
          for (let i = 0; i < rows.length; i++) {
            const row = rows[i]
            let {
                xml,
                id,
                serial
              } = row
            
            if (is_modify_id) { id = `${prefix_modify}${id}` }
            if (is_inv) {
              
              if (serial.substr(0, 1) == 'C') {
                type_xml = 3
              }
              
            }
           
            let params =  {
              id: id,
              type_xml,
              xml
            }
        
              return params
          
          }
        
        } catch (err) {
          console.trace( err && err.message)
          throw new Error(err)
         
        }
      }

}
module.exports = Service