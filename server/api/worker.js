"use strict"

const nodemailer = require("nodemailer")
const config = require("./config")
const logger4app = require("./logger4app")
const util = require("./util")
const dbtype = config.dbtype
const invobj = require(`./${dbtype}/inv`)
const ouobj = require(`./${dbtype}/ous`)
const emailobj = require("./email")
const sms = require("./sms")
const decrypt = require('./encrypt')
const dbCache = config.dbCache


//Hàm này dùng trong trường hợp config.useJobSendmail = 1, config.useRedisQueueSendmail = 1, trường hợp này tránh đẩy msg dài vào queue dẫn đến lỗi Message too long
const mailshortmsg = async (msg) => {
    let info, mailstatus, smsstatus
    try {
        logger4app.debug(`worker.mailshortmsg msg ${msg}`)
        let msgobj = JSON.parse(msg)
        let invid = msgobj.invid, status = msgobj.status, docmail = await invobj.invdocbid(invid)
        logger4app.debug(`worker.mailshortmsg invid ${invid}`)
        logger4app.debug(`worker.mailshortmsg doc ${docmail.form}-${docmail.serial}-${docmail.seq}`)
        let doc = JSON.parse(JSON.stringify(docmail))
        doc.status = (status) ? status : doc.status
        doc.sendmanual = msgobj.sendmanual
        doc.subject = (msgobj.subject) ? msgobj.subject : doc.subject
        doc.bmail = (msgobj.bmail) ? msgobj.bmail : doc.bmail
		if (!util.isEmpty(doc.bmail)) 
        {
            //Cập nhật ngày và trạng thái gửi mail
            try {
                logger4app.debug(`worker.mailshortmsg start sending mail`+doc.bmail)
                mailstatus = await emailobj.senddirectmail(doc, '')
                if (mailstatus) await invobj.updateSendMailStatus(doc)
            }
            catch (ex) {
                logger4app.debug(ex)
                mailstatus = ex.mesage
            }
        }
        if (!util.isEmpty(doc.btel)) {
            try {
                //Cập nhật ngày và trạng thái gửi sms
                smsstatus = await sms.smssend(doc)
                if (smsstatus) await invobj.updateSendSMSStatus(doc, smsstatus)
            }
            catch (ex) {
                smsstatus = ex.mesage
            }
        }
        info = `mailstatus : ${JSON.stringify(mailstatus)} \n smsstatus : ${JSON.stringify(smsstatus)}`
        
    }
    catch (err) {
        throw err
    }
    return info
}
const email = async (msg) => {
    let msgmail = JSON.parse(msg)
    let smtpConfig = JSON.parse(JSON.stringify(config.smtpConfig))
    if (smtpConfig.auth && smtpConfig.auth.pass) smtpConfig.auth.pass = decrypt.decrypt(smtpConfig.auth.pass)
    if (msgmail&&msgmail.ou) {
        const oumail = await ouobj.obid(msgmail.ou)
        if (oumail.smtpconf) {
            let smtpconf = JSON.parse(oumail.smtpconf)
            smtpConfig = smtpconf.smtpConfig
            msgmail.from = smtpconf.mail
            msgmail.sender = (smtpconf.sender) ? smtpconf.sender : smtpconf.mail
        }
    }
    let transporter, info
    try {
        transporter = nodemailer.createTransport(smtpConfig)
        info = await transporter.sendMail(msgmail)
    } catch (err) {
        throw err
    } finally {
        try {
            if (transporter) transporter.close()
        } catch {

        }
    }
    return info
}
if (!dbCache && !config.disable_worker) {
    const rsmq = require("./rsmq")
    const opts = { autostart: true, alwaysLogErrors: true, timeout: 0, rsmq: rsmq }
    const RSMQWorker = require("rsmq-worker")
    const number_of_worker = (config.number_of_worker) ? config.number_of_worker : 1
    for (let i = 1; i <= number_of_worker; i++) {
        const worker = new RSMQWorker(config.qname, opts)
        worker.on("ready", () => logger4app.debug(`WORKER ${i} READY`))
        worker.on("deleted", id => logger4app.debug(`DELETED ${i} `, id))
        worker.on("error", (err, msg) => logger4app.error(`ERROR ${i} `, err, msg.id))
        worker.on("exceeded", msg => logger4app.debug(`EXCEEDED ${i} `, msg.id))
        worker.on("timeout", msg => logger4app.debug(`TIMEOUT ${i} `, msg.id, msg.rc))
        worker.on("message", (msg, next, id) => {
            logger4app.debug(`MESSAGE ${i} `, id)
            let msgobj = JSON.parse(msg)
            let invid = msgobj.invid
            if (!invid) {
                email(msg).then(info => {
                    logger4app.debug(info)
                    next()
                }).catch(err => {
                    logger4app.error(err)
                    next(err)
                })
            } else if (config.useJobSendmail && config.useRedisQueueSendmail) {
                mailshortmsg(msg).then(info => {
                    logger4app.debug(info)
                    next()
                }).catch(err => {
                    logger4app.error(err)
                    next(err)
                })
            }
            else {
                email(msg).then(info => {
                    logger4app.debug(info)
                    next()
                }).catch(err => {
                    logger4app.error(err)
                    next(err)
                })
            }
        })
        //module.exports = worker
    }
} else {
    module.exports = {}
}