"use strict"
const Minizip = require('minizip-asm.js')
var generator = require('generate-password')
var genpass = require('./genpass')
const handlebars = require("handlebars")
const inc = require("./inc")
const ext = require("./ext")
const util = require(`./util`)
const ous = require(`./ous`)
const config = require("./config")
const xlsxtemp = require("xlsx-template")
const logger4app = require("./logger4app")
var fs = require('fs')
var sevenBin = require('7zip-bin')
var seven = require('node-7z')
const pathTo7zip = sevenBin.path7za
var uuid = require('uuid');
const moment = require("moment")
const path = require("path")
const COL = require("./col")
const dbtype = config.dbtype
const sendmail_pdf_xml_notzip = config.sendmail_pdf_xml_notzip
const genPassFile=config.genPassFile
var archiver = require('archiver');


const create_zip = (file, doc,pass) => {
    return new Promise(async (resolve, reject) => {
        try {
            let filename = `${doc.c9}-${doc.curr}-${doc.seq}`.split("/").join(".")
            const xml = fs.readFileSync(`${file}.xml`)
            const pdf = fs.readFileSync(`${file}.pdf`)
            var output = fs.createWriteStream(`${file}.zip`);
            var archive
            if(!pass) {
                archive = archiver('zip', {
                    zlib: { level: 9 }
                })
            } else {
                if(!archiver.isRegisteredFormat('zip-encryptabled')) archiver.registerFormat('zip-encryptabled', require('archiver-zip-encryptable'));
                archive = archiver('zip-encryptabled', {
                    zlib: { level: 9 },
                    forceLocalTime: true,
                    password: pass,
                    encryptionMethod: 'zip20'
                });
            }
            archive.pipe(output);
            archive.append(xml, { name: `${filename}.xml` });
            archive.append(pdf, { name: `${filename}.pdf` });
           
            archive.finalize();
           
            output.on("finish", () => { resolve(true) });
          
           /// resolve('output.getContents()')
        } catch (err) {
            reject(err)
        } finally {
            // if(fs.existsSync(`${file}.zip`)) fs.unlinkSync(`${file}.zip`)
            if(fs.existsSync(`${file}.xml`)) fs.unlinkSync(`${file}.xml`)
            if(fs.existsSync(`${file}.pdf`)) fs.unlinkSync(`${file}.pdf`)
        }
    })
}
const create_mail_zip = (filename, file,pass) => {
    return new Promise(async (resolve, reject) => {
        try {
            const xml = file.xml
            const pdf = file.pdf
            var output = fs.createWriteStream(filename);
            var archive
            if(!pass) {
                archive = archiver('zip', {
                    zlib: { level: 9 }
                });
            } else {
                if(!archiver.isRegisteredFormat('zip-encryptabled')) archiver.registerFormat('zip-encryptabled', require('archiver-zip-encryptable'));
                archive = archiver('zip-encryptabled', {
                    zlib: { level: 9 },
                    forceLocalTime: true,
                    password: pass,
                    encryptionMethod: 'zip20'
                });
            }
            archive.pipe(output);
            archive.append(xml, { name: `${filename}.xml` });
            archive.append(pdf, { name: `${filename}.pdf` });
           
            archive.finalize();
            
            output.on("finish", () => {
                try {
                    let tmpfile = fs.readFileSync(filename)
                    tmpfile= new Buffer(tmpfile).toString('base64')
                    resolve(tmpfile) 
                } catch(err) {
                    console.log(err, "error file")
                }
                
            })
        } catch (err) {
            reject(err)
        }
    })
}

const createFileMailDBS = (filename, file,pass,cus,doc) => {
    return new Promise(async (resolve, reject) => {
        try {
            // const xml = file.xml
            const pdf = file.pdf
            var output 
            var archive
            if(!pass) {
                output=fs.createWriteStream(config.folder_mail + filename+'.zip');
                archive = archiver('zip', {
                    zlib: { level: 9 }
                });
            } else { }
            
            // export xlsx
            var template = '';
            try {
                let json, rows
                let colhds = []
                let extsh = []
                colhds.push(doc.bname)
                colhds.push(doc.baddr)
                colhds.push(doc.btax)
                colhds.push(doc.bacc)
                colhds.push(doc.curr)
                colhds.push(doc.serial)
                colhds.push(moment(doc.idt).format('DD/MM/YYYY'))
                
                colhds.push(doc.c1)
                colhds.push(doc.c2)
                colhds.push(doc.seq)
                let rowItem = doc.items
                rowItem = rowItem.map(row=> ({...row, quantity: 1, unit: doc.curr }))
                json = { table: rowItem, extsh: extsh, colhds: colhds }
                const file = path.join(__dirname, "..", "..", 'temp/EXCEL_DBS_EMAIL.xlsx')
                const xlsx = fs.readFileSync(file)
                template = new xlsxtemp(xlsx)
                template.substitute(1, json)
            } catch (err) {
                console.trace(err)
                reject(err)
            }
            
            var binaryStr = template.generate();
            var buff = new Buffer.from(binaryStr, "binary") //binary
            if (!pass) {
                archive.pipe(output);
                archive.append(pdf, { name: `${filename}.pdf` });
                archive.append(buff, { name: `${filename}.xlsx` });

                archive.on("progress", (progress)=> {
                    if(progress && progress.entries && progress.entries.processed==progress.entries.total) archive.finalize();
                })
                archive.on("finish", (e) => {
                    try {
                        resolve(true) 
                    } catch(err) {
                        console.log(err, "error file")
                    }
                    
                })
                archive.on("error" ,  (err)=>{
                    console.log(err, " error")
                    archive.finalize();
                })
                output.on("finish", () => {
                    try {
                        resolve(true) 
                    } catch(err) {
                        console.trace(err)
                    }
                })
                output.on("error" , (err)=>{
                    reject(false)
                })
            } else {
                let fzip= await create7zip(pdf, buff, pass, filename)
                resolve(true)
            }
        } catch (err) {
            console.trace(err)
            reject(err)
        }
    })
}

const renderAsync = async (json) => {
    let items = json.data.items
    for (let i of items) {
        i.price = i.price || ""
        i.quantity = i.quantity || ""
        i.total = i.total || ""
        //i.vat = i.vat || ""
    }
    let bodyBuffer = await util.jsReportRenderAttach(json)
    
    return bodyBuffer
    
}

const create7zip = (pdf, xml, password, filename) => {
    return new Promise((resolve, reject) => {
        let file, config_7zip = config.config_7zip, vuuid = uuid.v4(), endofile=".xml"
        if (config.ent =="dbs") {
            vuuid=filename
            endofile = '.xlsx'
        }
        let filePDF = config_7zip.PathFileToZip + vuuid + ".pdf"
        let fileXML = config_7zip.PathFileToZip + vuuid + endofile
        let file7z = config_7zip.PathSave7zipFile + vuuid + ".zip"
        try {
            //Tao file tam tu cac bien pdf, xml va luu vao thu muc config_7zip.PathFileToZip, ten file su dung uuid de tao
            let PDFFile = fs.openSync(filePDF, 'w')
            console.log(`create7zip `, filename, filePDF)
            fs.writeFileSync(PDFFile, pdf, (err) => {
                if (err) throw err;
            })
            fs.fsyncSync(PDFFile);
            fs.closeSync(PDFFile);
    
            let XMLFile = fs.openSync(fileXML, 'w')
            console.log(`create7zip `, filename, fileXML)
            fs.writeFileSync(XMLFile, xml, (err) => {
                if (err) throw err;
            })
            fs.fsyncSync(XMLFile);
            fs.closeSync(XMLFile);

            //Chuyen thanh file 7z lu tai thu muc config_7zip.PathSave7zipFile, su dung uuid de dat ten file
            console.log(`create7zip `, filename, file7z)
            let myStream = seven.add(file7z, [filePDF, fileXML], {
                recursive: true,
                $bin: pathTo7zip,
                password: password,
                deleteFilesAfter: true
            })
            console.log("password---------->", password)
    
            console.time("_end")
            myStream.on('end', (data)=>{
                console.timeEnd("_end")
                tmpfile = fs.readFileSync(file7z)
                file = new Buffer(tmpfile).toString('base64')
                // console.log(`create7zip return base64`, filename, file)
                if(myStream) myStream.destroy()
                if(fs.existsSync(filePDF)) fs.unlinkSync(filePDF)
                if(fs.existsSync(fileXML)) fs.unlinkSync(fileXML)
                resolve(file)
                if(fs.existsSync(file7z)) fs.unlinkSync(file7z) // (giữ lại file zip)
            })
    
            console.time("_error")
            myStream.on('error', (err)=>{
                console.log(err, "err")
                console.timeEnd("_error")
                if(myStream) myStream.destroy()
                if(fs.existsSync(filePDF)) fs.unlinkSync(filePDF)
                if(fs.existsSync(fileXML)) fs.unlinkSync(fileXML)
                reject()
                if(fs.existsSync(file7z)) fs.unlinkSync(file7z) // (giữ lại file zip)
            })
    
        } catch (err) {
            reject(err)
        } finally {
            console.log(`create7zip delete zip file`, filename, file7z)
            // if(fs.existsSync(file7z)) fs.unlinkSync(file7z) // (giữ lại file zip)
        }
    })
}

const Service = {
    getFileMail: async (id) => {    
        try {
            let sql, rows, row, binds = [id], file = {}, doc
            let arrfile = [] //Khởi tạo mảng lưu file trả ra
            sql = `select xml "xml", doc "doc" from s_inv where id = ?`
            rows = await inc.execsqlselect(sql, binds)
            if (rows.length) {
                row = rows[0]
                file.xml = row.xml
                doc = util.parseJson(row.doc)
            }
            //Create PDF
            const status = doc.status, org = await ous.obt(doc.stax), idx = org.temp
            if (!([3, 4].includes(status))) doc.sec = ""
            const fn = doc.form.length > 1 ? util.fn(doc.form, idx): `${doc.type}.${doc.form}.${doc.serial}.${idx}`
            let tmp = await util.template(fn)
            let obj = { status: status, doc: doc, tmp: tmp }
            let reqjsr = ext.createRequest(obj)
            file.pdf = await renderAsync(reqjsr)
            
            //var password = generator.generate({length: 10,numbers: true, symbols:true,lowercase:true,uppercase:true,strict:true})
            var password = await genpass.generate(doc, {})
            let filename = `${doc.form}-${doc.serial}-${doc.seq}`.split("/").join(".")
            //Tạo object password
            let passwordobj = {}
            //Nếu có password thì gán vào object password
            if ((genPassFile && config.ent!="jpm") || (genPassFile && config.ent=="cimb" && doc.c6)) {
                passwordobj = {password: password}
            }
            // Kiểm tra tham số sendmail_pdf_xml_notzip, nếu = 0 thì zip
            if (!sendmail_pdf_xml_notzip) {
                let fzip, extfile = 'zip'
                if (!config.config_7zip) {
                    //Create Zip
                    var mz = new Minizip()
                    // mz.append("haha/abc.txt", "duy ham", {password: "~~~"})
                    if (file.xml) mz.append(`${filename}.xml`, file.xml, passwordobj)
                    if (file.pdf) mz.append(`${filename}.pdf`, file.pdf, passwordobj)
                    fzip = mz.zip()
                    fzip = fzip.toString("base64")
                } else {
                    fzip = await create7zip(file.pdf, file.xml, passwordobj.password, filename)
                    extfile = '7z'
                }
                //Create mau password
                const source = await util.tempmail(-2)
                const template = handlebars.compile(source)
                const htmPassword =  (genPassFile && config.ent!="cimb") || (genPassFile && config.ent=="cimb" && doc.c6) ? template({ ...doc, ...{ password, filename: `${filename}.${extfile}` } }) : null
                const subjectMailPassword =  (genPassFile && config.ent!="cimb") || (genPassFile && config.ent=="cimb" && doc.c6) ? (await util.getSubjectMailPassword({ ...doc, ...{ password, filename: `${filename}.${extfile}` } }, '')) : null
                arrfile.push({ content: fzip, encoding: 'base64', subjectmailpass: subjectMailPassword, password: htmPassword, passwordtext: password, filename: `${filename}.${extfile}`, filetype: `${extfile}` })
            } //Nếu không thì không zip và chẳng đặt mật khẩu
            else {
                const htmPassword = null
                arrfile.push({content:Buffer.from(file.xml, 'utf8').toString('base64'), encoding: 'base64', password:htmPassword, passwordtext: password, filename:`${filename}.xml`, filetype: 'xml'})
                arrfile.push({content:file.pdf.toString("base64"), encoding: 'base64', password:htmPassword, passwordtext: password, filename:`${filename}.pdf`, filetype: 'pdf'})
            }
            
            let res = arrfile
            return res
        }
        catch (err) {
            throw new Error(err)
        }
    },
    getFileMailPass: async (id,pass) => {    
        try {
            let sql, result, row, binds = [id], file ,doc,filename
            sql = `select file_name "file_name",doc "doc" from s_inv where id=?`
            result = await inc.execsqlselect(sql, binds)
            if (result.length) {
                row = result
                file = row.file_name
                doc = util.parseJson(row.doc)
            }

            let a =  await create_zip(file,doc,pass)
            if(config.ent=='mzh') filename = `${doc.c9}-${doc.curr}-${doc.seq}`.split("/").join(".")
           
            const zip = fs.readFileSync(`${file}.zip`)
            filename = [`${filename}.zip`].join('');
            let res = {zip:zip, filename:`${filename}`}
            if(fs.existsSync(`${file}.zip`)) fs.unlinkSync(`${file}.zip`)
            return res
        }
        catch (err) {
            throw new Error(err)
        }
    },
    getFileMailToFolder: async (id) => {    
        try {
            let sql, result, row, binds = [id], file = {}, doc,idt
            sql = `select xml "xml", doc "doc",idt "idt" from s_inv where id=?`
            result = await inc.execsqlselect(sql, binds)
            if (result.length) {
                row = result
                file.xml = row.xml
                doc = util.parseJson(row.doc)
                idt = row.idt
            }
            //Create PDF
            const status = doc.status, org = await ous.obt(doc.stax), idx = org.temp
            if (!([3, 4].includes(status))) doc.sec = ""
            const fn = doc.form.length > 1 ? util.fn(doc.form, idx): `${doc.type}.${doc.form}.${doc.serial}.${idx}`
            let tmp = await util.template(fn)
            let obj = { status: status, doc: doc, tmp: tmp }
            let reqjsr = ext.createRequest(obj)
            file.pdf = await renderAsync(reqjsr)
            //Create Zip
            //var mz = new Minizip()
            // mz.append("haha/abc.txt", "duy ham", {password: "~~~"})
          //  var password = generator.generate({length: 10,numbers: true, symbols:true,lowercase:true,uppercase:true,strict:true})
            let filename = `${doc.form}-${doc.serial}-${doc.seq}`.split("/").join(".")
            if(config.ent=='mzh') filename = `${doc.c9}-${doc.curr}-${doc.seq}`.split("/").join(".")
            logger4app.debug('creating ' + filename);
            const year =(moment(idt)).format('YYYY')
            const Month = (moment(idt)).format('MMM')
            const day = (moment(idt)).format('DD')
            var dir = config.path_zip + `${year}/${Month}/${day}/`
            if (!fs.existsSync(config.path_zip + `${year}`)){
                fs.mkdirSync(config.path_zip + `${year}`);
            }
            if (!fs.existsSync(config.path_zip + `${year}/${Month}`)){
                fs.mkdirSync(config.path_zip + `${year}/${Month}`);
            }
            if (!fs.existsSync(config.path_zip + `${year}/${Month}`)){
                fs.mkdirSync(config.path_zip + `${year}/${Month}`);
            }
            if (!fs.existsSync(config.path_zip + `${year}/${Month}/${day}`)){
                fs.mkdirSync(config.path_zip + `${year}/${Month}/${day}`);
            }
            fs.writeFileSync(dir + `${filename}.xml`, file.xml);
            fs.writeFileSync(dir +`${filename}.pdf`, file.pdf);
            
            
          return config.path_zip + `${year}/${Month}/${day}/`+filename
            //
           
        }
        catch (err) {
            throw new Error(err)
        }
    },
    getFileMailFromJob: async (doc,xml) => {
        try {
            let sql, rows, row, binds = [doc.id], file = {xml: xml}
            let arrfile = [] //Khởi tạo mảng lưu file trả ra
            sql = `select xml "xml", xml_received "xml_received", status_received "status_received", doc "doc" from s_inv where id=?`
            rows = await inc.execsqlselect(sql, binds)
            if (rows.length) {
                row = rows[0]
                if (row.status_received != 10) {
                    file.xml = (xml) ? xml : row.xml
                }
                else {
                    file.xml = (xml) ? xml : row.xml_received
                }
                //doc = util.parseJson(row.doc)
            }
            //Create PDF
            const status = doc.status, org = await ous.obt(doc.stax), idx = org.temp
            if (!([3, 4].includes(status))) doc.sec = ""
            const fn = doc.form.length > 1 ? util.fn(doc.form, idx): `${doc.type}.${doc.form}.${doc.serial}.${idx}`
            let tmp = await util.template(fn)
            let obj = { status: status, doc: doc, tmp: tmp }
            let reqjsr = ext.createRequest(obj)
            file.pdf = await renderAsync(reqjsr)
            
            //var password = generator.generate({length: 10,numbers: true, symbols:true,lowercase:true,uppercase:true,strict:true})
            var password = await genpass.generate(doc, {})
            let filename = `${doc.form}-${doc.serial}-${doc.seq}`.split("/").join(".")
            //Tạo object password
            let passwordobj = {}
            //Nếu có password thì gán vào object password
            if (((genPassFile && config.ent!="cimb") || (genPassFile && config.ent=="cimb" && doc.c6) )&& password) {
                passwordobj = {password: password}
            }
            // Kiểm tra tham số sendmail_pdf_xml_notzip, nếu = 0 thì zip
            if (!sendmail_pdf_xml_notzip) {
                let fzip, extfile = 'zip'
                if (!config.config_7zip) {
                    // Thinhpq10 zip file using 7zip
                    fzip = await create_mail_zip(filename, file, passwordobj.password )

                    if(fs.existsSync(filename)) fs.unlinkSync(filename)
                } else {
                    fzip = await create7zip(file.pdf, file.xml, passwordobj.password, filename)
                    extfile = '7z'
                }
                //Create mau password
                const source = await util.tempmail(-2)
                const template = handlebars.compile(source)
                const htmPassword = (genPassFile && config.ent!="cimb") || (genPassFile && config.ent=="cimb" && doc.c6) ? template({ ...doc, ...{ password, filename: `${filename}.${extfile}` } }) : null
                const subjectMailPassword = (genPassFile && config.ent!="cimb") || (genPassFile && config.ent=="cimb" && doc.c6) ? (await util.getSubjectMailPassword({ ...doc, ...{ password, filename: `${filename}.${extfile}` } }, '')) : null
                arrfile.push({ content: fzip, encoding: 'base64', subjectmailpass: subjectMailPassword, password: htmPassword, passwordtext: password, filename: `${filename}.${extfile}`, filetype: `${extfile}` })
            } //Nếu không thì không zip và chẳng đặt mật khẩu
            else {
                const htmPassword = null
                arrfile.push({content:Buffer.from(file.xml, 'utf8').toString('base64'), encoding: 'base64', password:htmPassword, passwordtext: password, filename:`${filename}.xml`, filetype: 'xml'})
                arrfile.push({content:file.pdf.toString("base64"), encoding: 'base64', password:htmPassword, passwordtext: password, filename:`${filename}.pdf`, filetype: 'pdf'})
            }
            
            let res = arrfile
            return res
        }
        catch (err) {
            throw new Error(err)
        }
    },

    getFileMailDBS: async (doc,xml,filename,cus) => {
        try {
            let sql, rows, row, binds = [doc.id], file = {xml: xml}
            let arrfile = [] //Khởi tạo mảng lưu file trả ra
            sql = `select xml "xml", xml_received "xml_received", status_received "status_received", doc "doc" from s_inv where id=?`
            rows = await inc.execsqlselect(sql, binds)
            if (rows.length) {
                row = rows[0]
                if (row.status_received != 10) {
                    file.xml = (xml) ? xml : row.xml
                }
                else {
                    file.xml = (xml) ? xml : row.xml_received
                }
                //doc = util.parseJson(row.doc)
            }
            //Create PDF
            const status = doc.status, org = await ous.obt(doc.stax), idx = org.temp
            if (!([3, 4].includes(status))) doc.sec = ""
            const fn = doc.form.length > 1 ? util.fn(doc.form, idx): `${doc.type}.${doc.form}.${doc.serial}.${idx}`
            let tmp = await util.template(fn)
            let obj = { status: status, doc: doc, tmp: tmp }
            let reqjsr = ext.createRequest(obj)
            file.pdf = await renderAsync(reqjsr)
            
            //var password = generator.generate({length: 10,numbers: true, symbols:true,lowercase:true,uppercase:true,strict:true})
            // var password = await genpass.generate(doc, {})
            // let filename = `${doc.form}-${doc.serial}-${doc.seq}`.split("/").join(".")

            //Tạo object password
            let passwordobj = {}
            //Nếu có password thì gán vào object password
            // if (((genPassFile && config.ent!="cimb") || (genPassFile && config.ent=="cimb" && doc.c6) )&& password) {
            //     passwordobj = {password: password}
            // }
            passwordobj = {password: cus.c7}
            // Kiểm tra tham số sendmail_pdf_xml_notzip, nếu = 0 thì zip
            if (!sendmail_pdf_xml_notzip) {
                let fzip, extfile = 'zip'
                if(config.ent == "dbs") fzip = await createFileMailDBS(filename, file, passwordobj.password,cus,doc)
                else if (!config.config_7zip) {
                    // Duongpt35 zip file DBS
                    

                   // if(fs.existsSync(filename)) fs.unlinkSync(filename)
                } else {
                    fzip = await create7zip(file.pdf, file.xml, passwordobj.password, filename)
                    extfile = '7z'
                }
                //Create mau password
                // const source = await util.tempmail(-2)
                // const template = handlebars.compile(source)
                // const htmPassword = (genPassFile && config.ent!="cimb") || (genPassFile && config.ent=="cimb" && doc.c6) ? template({ ...doc, ...{ password, filename: `${filename}.${extfile}` } }) : null
                // const subjectMailPassword = (genPassFile && config.ent!="cimb") || (genPassFile && config.ent=="cimb" && doc.c6) ? (await util.getSubjectMailPassword({ ...doc, ...{ password, filename: `${filename}.${extfile}` } }, '')) : null
                // arrfile.push({ content: fzip, encoding: 'base64', subjectmailpass: subjectMailPassword, password: htmPassword, passwordtext: password, filename: `${filename}.${extfile}`, filetype: `${extfile}` })
            } //Nếu không thì không zip và chẳng đặt mật khẩu
            else {
                const htmPassword = null
                arrfile.push({content:Buffer.from(file.xml, 'utf8').toString('base64'), encoding: 'base64', password:htmPassword, passwordtext: password, filename:`${filename}.xml`, filetype: 'xml'})
                arrfile.push({content:file.pdf.toString("base64"), encoding: 'base64', password:htmPassword, passwordtext: password, filename:`${filename}.pdf`, filetype: 'pdf'})
            }
            
            let res = arrfile
            return res
        }
        catch (err) {
            throw new Error(err)
        }
    }
}
module.exports = Service