
"use strict"
const moment = require("moment")
const sec = require("./sec")
const inc = require("./inc")
const util = require("./util")
const config = require("./config")
const logger4app = require("./logger4app")
const n2w_jsreport = require("./n2w_jsreport")
const UPLOAD_MINUTES = config.upload_minute
const mfd = config.mfd
const ENT = config.ent
const formatItems = (rows) => {
    for (const row of rows) {
		if (!row.unit) row.unit = ""
        row.price = util.fn2(row.price)
        row.amount = util.fn2(row.amount)
        if (!row.quantity) row.quantity = ""
        row.name=String(row.name).replace("<br>","")
        if (row.discountamt == null) row.discountamt = ''
        if (row.discountrate == null) row.discountrate = ''
        if (row.price == null) row.price = ''
        if (row.quantity == null) row.quantity = ''
    }
    return rows
}
const Service = {
    minutesReplace: async (req, res, next) => {
        try {
            const token = sec.decode(req), id = req.body.id, adj = req.body.inv.adj
            const result = await inc.execsqlselect(`select doc "doc" from s_inv where id=?`, [id])
            const doc = util.parseJson(result[0].doc)
            doc.idt = moment(doc.idt).format(mfd)
            doc.items = formatItems(doc.items)
            doc.vatv = util.fn2(doc.vatv)
            doc.sumv = util.fn2(doc.sumv)
            doc.totalv = util.fn2(doc.totalv)
            doc.ref = adj.ref
            doc.rea = adj.rea
            doc.btel= (doc.btel) ? doc.btel : ''
            doc.bmail = (doc.bmail) ? doc.bmail : ''
            doc.btax= (doc.btax) ? doc.btax : ''
            doc.rdt = moment(adj.rdt).format(mfd)
            doc.isvat = doc.type == "01GTKT" ? true : false
            let file_name=""
            if (ENT=="vcm") file_name="VCM-BBThaythe.docx"
            else if (ENT=="ctbc") {
                if (doc.curr == "VND") {
                    doc.sum = doc.sumv
                    doc.vat = doc.vatv
                    doc.total = doc.totalv
                }
                else {
                    doc.word = n2w_jsreport.d2w(Math.abs(doc.total), doc.curr)
                }
                doc.inv_rep = ''
                file_name = "01gtktmrep_ctbc.docx"
            }
            else file_name= doc.type.toLowerCase() + "mrep.docx"
            const out = util.docxrender(doc, file_name)
            res.end(out, "binary")
        } catch (err) {
            next(err)
        }
    },
    minutesCancel: async (req, res, next) => {
        try {
            const doc = req.body, adj = doc.adj
            doc.idt = moment(doc.idt).format(mfd)
            doc.items = formatItems(doc.items)
            doc.vatv = util.fn2(doc.vatv)
            doc.sumv = util.fn2(doc.sumv)
            doc.totalv = util.fn2(doc.totalv)
            doc.ref = adj.ref
            doc.rea = adj.rea
            doc.btel= (doc.btel) ? doc.btel : ''
            doc.bmail = (doc.bmail) ? doc.bmail : ''
            doc.btax= (doc.btax) ? doc.btax : ''
            doc.rdt = moment(adj.rdt).format(mfd)
            doc.isvat = doc.type == "01GTKT" ? true : false
            let file_name=""
            if (ENT=="vcm") file_name="VCM-BBHuy.docx"
            else if (ENT=="ctbc") {
                if (doc.curr == "VND") {
                    doc.sum = doc.sumv
                    doc.vat = doc.vatv
                    doc.total = doc.totalv
                }
                else {
                    doc.word = n2w_jsreport.d2w(Math.abs(doc.total), doc.curr)
                }
                file_name = "01gtktmcan_ctbc.docx"
            }
            else file_name= doc.type.toLowerCase() + "mcan.docx"
            const out = util.docxrender(doc, file_name)
            res.end(out, "binary")
        } catch (err) {
            next(err)
        }
    },
    minutesCreate: async (req, res, next) => {
        try {
            const doc = req.body, adj = doc.adj
            doc.idt = moment(doc.idt).format(mfd)
            doc.ref = adj.ref
            doc.rea = adj.rea
            doc.des = adj.des
			doc.btel= (doc.btel) ? doc.btel : ''
            doc.bmail = (doc.bmail) ? doc.bmail : ''
            doc.btax= (doc.btax) ? doc.btax : ''
            doc.rdt = moment(adj.rdt).format(mfd)
            doc.items0 = formatItems(doc.items0)
            doc.items1 = formatItems(doc.items1)
            doc.isvat = doc.type == "01GTKT" ? true : false
            if (!doc.seq) doc.seq = "............"
            let file_name=""
            if (ENT=="vcm"){
                file_name="VCM-BBDieuchinh.docx"
                doc.root.idt = moment(doc.root.idt).format(mfd)
            } 
            else file_name= doc.type.toLowerCase() + "madj.docx"
            const out = util.docxrender(doc, file_name)
            res.end(out, "binary")
        } catch (err) {
            next(err)
        }
    },
    minutesListInvAdj: async (req, res, next) => {
        try {
            const id = req.params.id
            let result = await inc.execsqlselect(`select doc "doc" from s_inv where id in (select inv_id from s_inv_adj where id = ?)`, [id])
            let doc = util.parseJson(result[0].doc)
            result = await inc.execsqlselect(`select note "note", so_tb "so_tb", ngay_tb "ngay_tb" from s_inv_adj where id=?`, [id])
            let adj = result[0]
            doc.idt = moment(doc.idt).format(mfd)
            doc.ref = adj.so_tb ? adj.so_tb : ".........."
            doc.rea = adj.note
			doc.btel= (doc.btel) ? doc.btel : ''
            doc.bmail = (doc.bmail) ? doc.bmail : ''
            doc.btax= (doc.btax) ? doc.btax : ''
            doc.rdt = adj.ngay_tb ? moment(adj.ngay_tb).format(mfd) : "..............."
            doc.items0 = formatItems(doc.items)
            doc.items1 = formatItems(doc.items)
            doc.isvat = doc.type == "01GTKT" ? true : false
            if (!doc.seq) doc.seq = "............"
            let file_name=""
            if (ENT=="vcm"){
                file_name="VCM-BBDieuchinh.docx"
                doc.root.idt = moment(doc.root.idt).format(mfd)
            } 
            else file_name= doc.type.toLowerCase() + "bthadj.docx"
            const out = util.docxrender(doc, file_name)
            res.end(out, "binary")
        } catch (err) {
            next(err)
        }
    },
    minutesWnAdj: async (req, res, next) => {
        try {
            const id = req.params.id
            let result = await inc.execsqlselect(`select doc "doc" from s_inv where id=?`, [id])
            let doc = util.parseJson(result[0].doc)
            result = await inc.execsqlselect(`select doc "doc" from s_wrongnotice_process where id_inv=? order by dt desc`, [id])
            let adj = result[0]
            let docx = util.parseJson(adj.doc)
            doc.idt = moment(doc.idt).format(mfd)
            doc.ref = (docx && docx.noti_taxnum) ? docx.noti_taxnum : '.......'
            doc.rea = (docx && docx.items) ? docx.items[0].rea : ''
			doc.btel= (doc.btel) ? doc.btel : ''
            doc.bmail = (doc.bmail) ? doc.bmail : ''
            doc.btax= (doc.btax) ? doc.btax : ''
            doc.rdt = (docx && docx.noti_taxdt) ? moment(docx.noti_taxdt).format(mfd) : "..............."
            // let itemsDf = doc.items
            // doc.items0 = formatItems(doc.items)
            // doc.items1 = formatItems(itemsDf)
            doc.items0 = formatItems(doc.items)
            doc.items1 = formatItems(doc.items)
            doc.isvat = doc.type == "01GTKT" ? true : false
            if (!doc.seq) doc.seq = "............"
            let file_name=""
            if (ENT=="vcm"){
                file_name="VCM-BBDieuchinh.docx"
                doc.root.idt = moment(doc.root.idt).format(mfd)
            } 
            else file_name= doc.type.toLowerCase() + "bthadj.docx"
            const out = util.docxrender(doc, file_name)
            res.end(out, "binary")
        } catch (err) {
            next(err)
        }
    },
    minutesCustom: async (req, res, next) => {
        try {
            const token = sec.decode(req), id = req.params.id, download = req.query.download
            let result = await inc.execsqlselect(`select pid "pid",type "type",status "status",adjtyp "adjtyp",doc "doc", ic "ic" from s_inv where id=?`, [id])
            let rows = result, row = rows[0],ic = row.ic, type = row.type, status = row.status, doc = util.parseJson(row.doc), fn, pid = row.pid, adjdif = doc.adjdif
            if (UPLOAD_MINUTES && download) {
                let typ, iid = pid
                if (status == 4) {
                    typ = "can"
                    iid = id
                } else {
                    let adjtyp = row.adjtyp
                    if (adjtyp == 1) {
                        typ = "rep"
                    } else if (adjtyp == 2) {
                        typ = "adj"
                    }
                }
                if (!typ) {
                    iid = id
                    typ = "normal"
                }
                let minutes = await inc.execsqlselect(`select id "id", type "type", content "content" from s_inv_file where id=? and type=?`, [iid, typ])
                if (!minutes.length) throw new Error("Không tìm thấy biên bản! \n (Cannot found minutes)")
                let file = Buffer.from(minutes.content,"base64")
                res.end(file, "binary")
            } else {
                if (adjdif == 1) {
                    let adj = doc.adj
                    let items1 = doc.items, items0 = []
                    doc.idt = moment(doc.idt).format(mfd)
                    doc.ref = adj.ref
                    doc.rea = adj.rea
                    doc.des = adj.des
                    doc.btel= (doc.btel) ? doc.btel : ''
                    doc.bmail = (doc.bmail) ? doc.bmail : ''
                    doc.btax= (doc.btax) ? doc.btax : ''
                    doc.rdt = moment(adj.rdt).format(mfd)
                    doc.items0 = formatItems(items0)
                    doc.items1 = formatItems(items1)
                    doc.isvat = type == "01GTKT" ? true : false
                    if (!doc.seq) doc.seq = "............"
                    if (ENT=="vcm"){
                        fn="VCM-BBDieuchinh.docx"
                        doc.root.idt = moment(doc.root.idt).format(mfd)
                    } else
                    fn = doc.type.toLowerCase() + "madj.docx"
                }
                else if (status == 4) {
                    let cancel = doc.cancel
                    if (!cancel) throw new Error(`Hóa đơn không có biên bản. \n (Invoices don't have Minutes)`)
                    doc.idt = moment(doc.idt).format(mfd)
                    doc.items = formatItems(doc.items)
                    doc.vatv = util.fn2(doc.vatv)
                    doc.sumv = util.fn2(doc.sumv)
                    doc.totalv = util.fn2(doc.totalv)
                    doc.ref = cancel.ref
                    doc.rea = cancel.rea
                    doc.cdt_cancel = moment(new Date()).format(mfd)
                    doc.cancel_rea = ic
					doc.btel= (doc.btel) ? doc.btel : ''
					doc.bmail = (doc.bmail) ? doc.bmail : ''
					doc.btax= (doc.btax) ? doc.btax : ''
                    doc.rdt = moment(cancel.rdt).format(mfd)
                    doc.isvat = type == "01GTKT" ? true : false
                    if (ENT=="vcm") fn="VCM-BBHuy.docx"
                    else if (ENT=="ctbc") {
                        if (doc.curr == "VND") {
                            doc.sum = doc.sumv
                            doc.vat = doc.vatv
                            doc.total = doc.totalv
                        }
                        else {
                            doc.word = n2w_jsreport.d2w(Math.abs(doc.total), doc.curr)
                        }
                        fn = "01gtktmcan_ctbc.docx"
                    }
                    else if(ENT == "kbank") fn="01gtktmcan_kbank.docx"
                    else fn= doc.type.toLowerCase() + "mcan.docx"
                   // fn = "mcan.docx"
                }
                else {
                    let adjtyp = row.adjtyp
                    if (adjtyp && pid) {
                        let adj = doc.adj
                        result = await inc.execsqlselect(`select doc "doc" from s_inv where id=?`, [pid])
                        row = result
                        let pdoc = util.parseJson(row[0].doc)
                        if (adjtyp == 1) {
                            let seq_ed = doc.seq, form_ed = doc.form, serial_ed = doc.serial, idt_ed = moment(doc.idt).format(mfd)
                            doc = pdoc
                            doc.idt = moment(doc.idt).format(mfd)
                            doc.items = formatItems(doc.items)
                            doc.vatv = util.fn2(doc.vatv)
                            doc.sumv = util.fn2(doc.sumv)
                            doc.totalv = util.fn2(doc.totalv)
                            doc.ref = adj.ref
                            doc.rea = adj.rea
							doc.btel= (doc.btel) ? doc.btel : ''
							doc.bmail = (doc.bmail) ? doc.bmail : ''
							doc.btax= (doc.btax) ? doc.btax : ''
                            doc.rdt = moment(adj.rdt).format(mfd)
                            doc.isvat = type == "01GTKT" ? true : false
                            if (ENT=="vcm") fn="VCM-BBThaythe.docx"
                            else if (ENT=="ctbc") {
                                if (doc.curr == "VND") {
                                    doc.sum = doc.sumv
                                    doc.vat = doc.vatv
                                    doc.total = doc.totalv
                                    
                                }
                                else {
                                    doc.word = n2w_jsreport.d2w(Math.abs(doc.total), doc.curr)
                                }
                                doc.inv_rep = `bằng hóa đơn số ${seq_ed}, ký hiệu ${serial_ed} mẫu số ${form_ed}, phát hành ngày ${idt_ed} `
                                fn = "01gtktmrep_ctbc.docx"
                            }
                            else fn= doc.type.toLowerCase() + "mrep.docx"
                          //  fn = "mrep.docx"
                        }
                        else if (adjtyp == 2) {
                            let items1 = doc.items, items0 = pdoc.items
                            if(doc.adjType == 3) {
                                for (let row of items1) {
                                    if (row.quantity) row.quantity = row.quantity * -1 
                                        else row.quantity = 0
                                    if (row.price) row.price = row.price * -1
                                        else row.price = 0
                                    if (row.amount) row.amount = row.amount * -1
                                        else row.amount = 0
                                }
                            }
                            doc.idt = moment(doc.idt).format(mfd)
                            doc.ref = adj.ref
                            doc.rea = adj.rea
                            doc.des = adj.des
							doc.btel= (doc.btel) ? doc.btel : ''
							doc.bmail = (doc.bmail) ? doc.bmail : ''
							doc.btax= (doc.btax) ? doc.btax : ''
                            doc.rdt = moment(adj.rdt).format(mfd)
                            doc.items0 = formatItems(items0)
                            doc.items1 = formatItems(items1)
                            doc.isvat = type == "01GTKT" ? true : false
                            if (!doc.seq) doc.seq = "............"
                            if (ENT=="vcm"){
                                fn="VCM-BBDieuchinh.docx"
                                doc.root.idt = moment(doc.root.idt).format(mfd)
                            } else
                            fn = doc.type.toLowerCase() + "madj.docx"
                        }
                    }
                }
                if (fn) {
                    for (let i in doc) if (util.isEmpty(doc[i])) doc[i] = ""
                    for (let row of doc.items) 
                    {
                        for (let i in row)
                        {
                            if (util.isEmpty(row[i])) row[i] = ""
                        }
                       
                        if (typeof  row.unit=="undefined") 
                        {
                            row.unit="";
                        }
                    }
                    const out = util.docxrender(doc, fn)
                    res.end(out, "binary")
                }
                else {
                    throw new Error(`Hóa đơn không có biên bản. \n (Invoices don't have Minutes)`)
                }
            }

        } catch (err) {
            next(err)
        }
    },
    upload: async (req, res, next) => {
        let ok = false,err
        try {
            const token = sec.decode(req), taxc = token.taxc
            const file = req.file, body = req.body, type = body.type.toString().toLowerCase(), id = body.id
            let content = file.buffer, contentb64 = content.toString('base64')
            let sql, result, binds
            sql = `select 1 from s_inv_file where id=? and type=?`
            binds = [id, type]
            result = await inc.execsqlselect(sql, binds)
            if (result.length) {
                sql = "update s_inv_file set id=?, type=?, content=? where id=? and type=?"
                binds = [id, type, contentb64, id, type]
            } else {
                sql = "insert into s_inv_file (id, type, content) VALUES (?,?,?)"
                binds = [id, type, contentb64]
            }
            result = await inc.execsqlinsupddel(sql, binds)
            if (result) ok=true
        } catch (error) {
            err = error.message
        }
        res.json({ok, err})
    }
}
module.exports = Service 