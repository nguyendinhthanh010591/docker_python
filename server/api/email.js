"use strict"
const path = require("path")
const axios = require("axios")
const fs = require("fs")
const moment = require("moment")
const redis = require("./redis")
const util = require("./util")
const config = require("./config")
const logger4app = require("./logger4app")
const dbtype = config.dbtype
const rsmq = require("./rsmq")
const hbs = require("./hbs")
const handlebars = require("handlebars")
const nodemailer = require("nodemailer")

const ouobj = require(`./${dbtype}/ous`)
const ifile = require(`./ifile`)
const org = require(`./${config.dbtype}/org`)
const decrypt = require('./encrypt')
const sendmail_pdf_xml=config.sendmail_pdf_xml
const sendcanmail_pdf_xml=config.sendcanmail_pdf_xml
const sender=config.sender
const mail=config.mail
const qname=config.qname
const sendApprMail=config.sendApprMail
const sendCancMail=config.sendCancMail
const genPassFile=config.genPassFile
const not_sendmail_pass=config.not_sendmail_pass 
const test_sms_fpt = config.test_sms_fpt
const pathtemplate = config.pathtemplate
const checkattachsizelimit = async (attachments) => {
    let attachs = attachments, check_attach_size_limit = config.check_attach_size_limit, filenameall = ""
    if (check_attach_size_limit) {
        let maxsize = 10 //Mặc định 10MB
        try {
            //Lấy giá trị max size cần check
            maxsize = parseFloat(check_attach_size_limit)
        } catch (error) {
            maxsize = 10
        }
        let sizeatt = 0
        maxsize = maxsize * 1024 * 1024
        //Mở từng file ra và lấy byte length, cộng dồn lại
        for (const file of attachs) {
            sizeatt += Buffer.byteLength(file.content)
            filenameall += `${file.filename};`
        }
        //Nếu byte length tổng nhỏ hơn max size cần check thì gán bằng mảng rỗng để không gửi attach
        if (sizeatt > maxsize) {
            attachs = []
            logger4app.debug(`Tổng dung lượng các file ${filenameall} vượt quá dung lượng cho phép (${maxsize}MB) \n The total file size of ${filenameall} has exceeded the allowed size (${maxsize}MB)`)
            throw new Error(`Tổng dung lượng các file ${filenameall} vượt quá dung lượng cho phép (${maxsize}MB) \n The total file size of ${filenameall} has exceeded the allowed size (${maxsize}MB)`)
        }
    }
    return attachs
}
const checkbeforesend = async (json) => {
    let vcheck = true
    if (json.sendmanual) return vcheck
    if (config.ent == 'aia') {
        if (json.c5 == 'Y' && (!util.isEmpty(json.bmail))) vcheck = true
        else vcheck = false
    }
    if (config.ent == 'vib') {
        if (json.bcode && (!util.isEmpty(json.bmail)))
        {
            let orgob = await org.obbcode(json.bcode)
            if (orgob && orgob.c0 == '1') vcheck = true
            else vcheck = false
        }
        else vcheck = false
    }
    /*
    if (config.ent == 'baca') {
        if (json.bcode && (!util.isEmpty(json.bmail)))
        {
            let orgob = await org.obbcode(json.bcode)
            if (orgob && orgob.c0 == '1') vcheck = true
            else vcheck = false
        }
        else vcheck = false
    }
    */
    return vcheck
}
const directemail = async (msg) => {
    let msgmail = util.parseJson(msg)
    
    let smtpConfig = util.parseJson(JSON.stringify(config.smtpConfig))
    if (smtpConfig.auth && smtpConfig.auth.pass) smtpConfig.auth.pass = decrypt.decrypt(smtpConfig.auth.pass)
    if (msgmail.ou) {
        const oumail = await ouobj.obid(msgmail.ou)
        if (oumail.smtpconf) {
            let smtpconf = util.parseJson(oumail.smtpconf)
            smtpConfig = smtpconf.smtpConfig
            msgmail.from = smtpconf.mail
            msgmail.sender = (smtpconf.sender) ? smtpconf.sender : smtpconf.mail
        }
    }
    let transporter, info
    try {
        transporter = nodemailer.createTransport(smtpConfig)
        info = await transporter.sendMail(msgmail)
    } catch (err) {
        throw err
    } finally {
        try {
            if (transporter) transporter.close()
        } catch {

        }
    }
    return info
}
const getAttachFile = async (json, arr) => {
    let id = json.id, status = json.status, attachs = []
    if ((sendmail_pdf_xml && status==3) || (sendcanmail_pdf_xml && status==4)) {
        attachs = await ifile.getFileMail(json.id)
        //Kiểm tra dung lượng vượt quá size cho phép thì không gửi đính kèm file attach
        attachs = await checkattachsizelimit(attachs)
        //attachs.push({filename: file.filename, content: file.zip.toString("base64"), encoding: 'base64'})
        if (genPassFile && !not_sendmail_pass) {
            for (const file of attachs) {
                if (!file.password) { continue; }
                let ouhd
                let arrcc = config.mailcc ? config.mailcc.trim().split(/[ ,;]+/).map(e => e.trim()) : []
                if (config.ent == "dtt") {
                    const invobj = require(`./${config.dbtype}/inv`)
                    let sdoc = await invobj.invdocbid(id)
                    arrcc = (sdoc.c4) ? sdoc.c4.trim().split(/[ ,;]+/).map(e => e.trim()) : []
                }
                //Nếu là sgr lấy thêm id ou
                if (config.ent == "sgr") {
                    const invobj = require(`./${config.dbtype}/inv`)
                    let sdoc = await invobj.invdocbid(id)
                    ouhd = sdoc.ou
                }
                const arrbcc = config.mailbcc ? config.mailbcc.trim().split(/[ ,;]+/).map(e => e.trim()) : []
                const content = {  sender: (sender) ? sender : mail, from: mail, to: arr, cc: arrcc, bcc: arrbcc, subject: file.subjectmailpass, html: file.password, ou: ouhd }
                await rsmq.sendMessageAsync({ qname: qname, message: JSON.stringify(content) })
            }
        }
    }
    return attachs
}

const sendMailInfobip = async (json) => {
    try {
        // let attachments = await getAttachFile(json, arr)
        const axios = require("axios")
        const FormData = require('form-data');
        const html = await hbs.j2m(json)
        const formData = new FormData();
        formData.append("from", config.infobipConfig.senderEmail);
        formData.append("to", json.to.trim().split(/[ ,;]+/).map(e => e.trim()).join(';'))
        if (config.mailcc) formData.append("cc", config.mailcc);
        if (config.mailbcc) formData.append("bcc", config.mailbcc);
        formData.append("subject", json.subject);
        formData.append("html", html);
        //formData.append("attachment", attachments);
        try {
            const response = await axios.post(config.infobipConfig.apiUrl, formData, {
                headers: {
                    'Content-Type': `multipart/form-data; boundary=${formData._boundary}`,
                    Authorization: `App ${config.infobipConfig.apiKey}`
                }
            });

            console.log('Email sent successfully:', response.data);
            return response.data
        } catch (error) {
            console.error('Error sending email:', error.response.data);
        }
    }
    catch (err) {
        console.log(err)
    }
}

const sendmail = async (json) => {
    try {
        const arr = json.to.trim().split(/[ ,;]+/).map(e => e.trim()), id = json.id
        let arrcc = config.mailcc ? config.mailcc.trim().split(/[ ,;]+/).map(e => e.trim()) : []
        let ouhd
        if (config.ent == "dtt") {
            const invobj = require(`./${config.dbtype}/inv`)
            let sdoc = await invobj.invdocbid(id)
            arrcc = (sdoc.c4) ? sdoc.c4.trim().split(/[ ,;]+/).map(e => e.trim()) : []
        }
        //Nếu là sgr lấy thêm id ou
        if (config.ent == "sgr") {
            const invobj = require(`./${config.dbtype}/inv`)
            let sdoc = await invobj.invdocbid(id)
            ouhd = sdoc.ou
        }
        const arrbcc = config.mailbcc ? config.mailbcc.trim().split(/[ ,;]+/).map(e => e.trim()) : []

        let attachments = await getAttachFile(json, arr), msgid
        for (const file of attachments) {
            if (file.passwordtext) { 
                json.attachment_password = file.passwordtext
                break; 
            }
        }
        const html = await hbs.j2m(json)
        const content = { sender: (sender) ? sender : mail, from: mail, to: arr, cc: arrcc, bcc: arrbcc, subject: json.subject, html: html, attachments: attachments, ou: ouhd ? ouhd : null }

        if ((sendApprMail && json.status == 3) || (sendCancMail && json.status == 4)) {
            if (config.infobipConfig) msgid = await sendMailInfobip(util.parseJson(content))
            else if (!config.useJobSendmail) {
                msgid = await rsmq.sendMessageAsync({ qname: qname, message: JSON.stringify(content)})
            } else {
                msgid = await directemail(JSON.stringify(content))
            }
        }
        return msgid
    } catch (err) {
        throw err
    }
}
const Service = {
    sendMailInfobip: async (json) => {
        try {
            const axios = require("axios")
            const FormData = require('form-data');
            const html = await hbs.j2m(util.parseJson(json))
            const formData = new FormData();
            formData.append("from", config.infobipConfig.senderEmail);
            if (!json.to) json.to=json.bmail
            formData.append("to", json.to.trim().split(/[ ,;]+/).map(e => e.trim()).join(';'))
            if (config.mailcc) formData.append("cc", config.mailcc);
            if (config.mailbcc) formData.append("bcc", config.mailbcc);
            formData.append("subject", json.subject);
            formData.append("html", html);
            //formData.append("attachment", attachments);
            try {
                axios.defaults.timeout = 9000000
                const response = await axios.post(config.infobipConfig.apiUrl, formData, {
                    headers: {
                        'Content-Type': `multipart/form-data; boundary=${formData._boundary}`,
                        Authorization: `App ${config.infobipConfig.apiKey}`
                    }
                });
    
                console.log('Email sent successfully:', JSON.stringify(response.data));
                return response.data
            } catch (error) {
                console.error('Error sending email:', error.response.data);
            }
        }
        catch (err) {
            console.log(err)
        }
    },
    sendAdminMail: async(err, AdminMailconfig) => {
        const adminMails = config.AdminMail
        let adminMail, vcheck = false
        if ((!adminMails) || (adminMails.length <= 0)) return
        for (let admail of adminMails) {
            if (admail.idmail == AdminMailconfig) {
                vcheck = true
                adminMail = admail
                break
            }
        }
        if (!vcheck) return
        let html
        const source = adminMail.content
        const template = handlebars.compile(source)
        const obj = { err: err }
        html = template(obj)
        const content = { from: config.mail, to: adminMail.to, cc: adminMail.cc, subject: adminMail.subject, html: html }
        await directemail(JSON.stringify(content))
    },
    //Hàm này chỉ dùng để lấy file attach chứ chưa gửi mail, ftp luôn
    getOnlyAttachFile : async (json, xml, arr) => {
        let status = json.status, attachs = []
        if ((sendmail_pdf_xml && status == 3) || (sendcanmail_pdf_xml && status==4)) {
            attachs = await ifile.getFileMailFromJob(json, xml)
            //Kiểm tra dung lượng vượt quá size cho phép thì không gửi đính kèm file attach
            attachs = await checkattachsizelimit(attachs)
            
            //attachs.push({ filename: file.filename, content: file.zip, encoding: 'base64', htmlpass: file.password })
        }
        return attachs
    },
    rsmqmail: async (doc) => {
        try {
            //const from = `${doc.sname} ${doc.smail} <${config.mail}>`
            let vcheck = await checkbeforesend(doc)
            if (!vcheck) return
            if(["vcm"].includes(config.ent)){
                const dbs = require("./mssql/dbs")
                const ous = require("./mssql/ous")
                const result = await dbs.query("select ou ou from s_inv where id=@1", [doc.id])
                const rows = result.recordset
                if (rows.length == 0) reject(new Error(`Không tìm thấy Hóa đơn ${id} \n (Invoice not found ${id})`))
                let  ou = rows[0].ou
                
                let taxc_root =  await ous.pidtaxc(ou)
                doc.taxc_root = taxc_root
            }
            if(["sgr"].includes(config.ent)){
                const dbs = require("./mssql/dbs")
                const ous = require("./mssql/ous")
                const result = await dbs.query("select ou ou from s_inv where id=@1", [doc.id])
                const rows = result.recordset
                if (rows.length == 0) reject(new Error(`Không tìm thấy Hóa đơn ${id} \n (Invoice not found ${id})`))
                let  ou = rows[0].ou
                let ou_if = await ous.obid(ou)                   
                doc.acccf = ou_if.acc
            }
            const arr = doc.bmail.trim().split(/[ ,;]+/).map(e => e.trim())
            let arrcc = config.mailcc ? config.mailcc.trim().split(/[ ,;]+/).map(e => e.trim()) : []
            let ouhd
            if (config.ent == "dtt") {
                arrcc = (sdoc.c4) ? sdoc.c4.trim().split(/[ ,;]+/).map(e => e.trim()) : []
            }
            //Nếu là sgr lấy thêm id ou
            if (config.ent == "sgr") {
                const invobj = require(`./${config.dbtype}/inv`)
                let sdoc = await invobj.invdocbid(doc.id)
                ouhd = sdoc.ou
            }
            if (config.ent == 'scb') {
                const cat = require(`./${config.dbtype}/cat`)
                logger4app.debug(`email doc.c1 ${doc.c1}`)
                doc.seg_desc = await cat.byidtype(doc.c1, 'SEGMENT')
                logger4app.debug(`email doc.seg_desc ${doc.seg_desc}`)
            }

            const arrbcc = config.mailbcc ? config.mailbcc.trim().split(/[ ,;]+/).map(e => e.trim()) : []
            let attachments = await getAttachFile(doc, arr), msgid
            const subject = await util.getSubjectMail(doc, '')

            //Lay mat khau file attach dua vao noi dung doc hoa don    
            for (const file of attachments) {
                if (file.passwordtext) { 
                    doc.attachment_password = file.passwordtext
                    break; 
                }
            }

            const html = await hbs.j2m(doc)
            const content = { sender: (sender) ? sender : mail, from: mail, to: arr, cc: arrcc, bcc: arrbcc, subject: subject, html: html, attachments: attachments, ou: ouhd  }
            
            if ((sendApprMail && doc.status == 3) || (sendCancMail && doc.status == 4)) msgid = await rsmq.sendMessageAsync({ qname: qname, message: JSON.stringify(content) })
            
            return msgid
        } catch (err) {
            throw err
        }
    },
    senddirectmail: async (doc, xml) => {
        try {
            //const from = `${doc.sname} ${doc.smail} <${config.mail}>`
            //Thêm biến lưu các content mail trả ra cho CTBC
            //  logger4app.debug(`email.senddirectmail doc ${JSON.stringify(doc.form)}-${JSON.stringify(doc.serial)}-${JSON.stringify(doc.seq)}`)
            let contentmailctbc = [], contentmailscb = []
            let vcheck = await checkbeforesend(doc)
            if (!vcheck) return
            const arr = doc.bmail.trim().split(/[ ,;]+/).map(e => e.trim())
            let arrcc = config.mailcc ? config.mailcc.trim().split(/[ ,;]+/).map(e => e.trim()) : []
            let ouhd
            if (config.ent == "dtt") {
                arrcc = (sdoc.c4) ? sdoc.c4.trim().split(/[ ,;]+/).map(e => e.trim()) : []
            }

            if(["sgr"].includes(config.ent)){
                const dbs = require("./mssql/dbs")
                const ous = require("./mssql/ous")
                const result = await dbs.query("select ou ou from s_inv where id=@1", [doc.id])
                const rows = result.recordset
                if (rows.length == 0) reject(new Error(`Không tìm thấy Hóa đơn ${id} \n (Invoice not found ${id})`))
                let  ou = rows[0].ou
                let ou_if = await ous.obid(ou)                   
                doc.acccf = ou_if.acc
                ouhd=ou
            }


            if (config.ent == 'scb') {
                const cat = require(`./${config.dbtype}/cat`)
                logger4app.debug(`email doc.c1 ${doc.c1}`)
                doc.seg_desc = await cat.byidtype(doc.c1, 'SEGMENT')
                logger4app.debug(`email doc.seg_desc ${doc.seg_desc}`)
                
            }
            
            const arrbcc = config.mailbcc ? config.mailbcc.trim().split(/[ ,;]+/).map(e => e.trim()) : []
            let attachments = await Service.getOnlyAttachFile(doc, xml, arr), msgid
            const subject = (doc.subject) ? doc.subject : (await util.getSubjectMail(doc, ''))

            //Lay mat khau file attach dua vao noi dung doc hoa don
            for (const file of attachments) {
                if (file.passwordtext) { 
                    doc.attachment_password = file.passwordtext
                    break; 
                }
            }
            const html = await hbs.j2m(doc)
            let attachs = []
            if ((sendmail_pdf_xml || sendcanmail_pdf_xml) && attachments.length > 0) {
                attachs = attachments
            }
            let content = { sender: (sender) ? sender : mail, from: mail, to: arr, cc: arrcc, bcc: arrbcc, subject: subject, html: html, attachments: attachs, ou: ouhd }
            if ((sendApprMail && doc.status == 3) || (sendCancMail && doc.status == 4)) {
                
                //msgid = await rsmq.sendMessageAsync({ qname: qname, message: JSON.stringify(content) })
                if (config.ent == 'scb') { // Goi ham send to queue cua Solace
                    contentmailscb.push(content)
                } else if (config.ent == 'ctbc') {
                    contentmailctbc.push(content)
                } else {
                    msgid = await directemail(JSON.stringify(content))
                }
                logger4app.debug("msgid" + msgid )
                
            }
            if ((genPassFile && config.ent!="cimb") || (genPassFile && config.ent=="cimb" && doc.c6) && !not_sendmail_pass) {
                if(config.ent!="cimb") {
                    for (const file of attachs) {
                        if (!file.password) { continue; }
                        content = { sender: (sender) ? sender : mail, from: mail, to: arr, cc: arrcc, bcc: arrbcc, subject: file.subjectmailpass, html: file.password }
                        //msgid = await rsmq.sendMessageAsync({ qname: qname, message: JSON.stringify(content) })
                        if (config.ent == 'scb') {
                            contentmailscb.push(content)
                        } else if (config.ent == 'ctbc') {
                            contentmailctbc.push(content)
                        } else msgid = await directemail(JSON.stringify(content))
                    }
                }
                
            }
            if (config.ent == 'ctbc') {
                return contentmailctbc
            } else if (config.ent == 'scb') {
                return contentmailscb
            } else return msgid
        } catch (err) {
            console.trace(err)
            throw err
        }
    },
    send: async (req, res, next) => {
        try {
            let id = req.body.id
            if(config.ent == "shvl") {
                await Service.sendMailInfobip(req.body)
                const invobj = require(`./inv`)
                let doc = await invobj.invdocbid(id)
                await invobj.updateSendMailStatus(util.parseJson(doc))
            }
            else if (!config.useJobSendmail) {
                await sendmail(req.body)
                const invobj = require(`./inv`)
                let doc = await invobj.invdocbid(id)
                await invobj.updateSendMailStatus(util.parseJson(doc))
            } else {
                const invobj = require(`./inv`)
                let doc = await invobj.invdocbid(id)
                let doctmp = util.parseJson(doc)
                doctmp.bmail = req.body.to
                doctmp.subject = req.body.subject
                doctmp.sendmanual = 1
                await invobj.insertDocTempJob(id, doctmp, '')
            }

            res.json("ok")
        }
        catch (err) {
            next(err)
        }
    },
    sendfromdoctemp: async (content) => {
        try {
            let msgid = await directemail(content)
            logger4app.debug("msgid ", msgid )
        }
        catch (err) {
            throw err
        }
    },
    smtp: async (req, res, next) => {
        try {
            const body = req.body, operation = body.webix_operation, key = "SMTP"
            let result
            switch (operation) {
                case "upd":
                    const port = body.port, mail = body.mail
                    const smtp = { type: body.type, mail: mail, host: body.host, port: port, secure: (port == 465), auth: { user: mail, pass: body.pwd } }
                    await redis.set(key, JSON.stringify(smtp))
                    break
                case "del":
                    await redis.del(key)
                    break
                case "sel":
                    const row = await redis.get(key)
                    if (row) {
                        result = util.parseJson(row)
                        delete result["auth"]
                        delete result["secure"]
                    }
                    break
                default:
                    throw new Error(`${operation} là hoạt động không hợp lệ \n (${operation} is invalid operation`)
            }
            res.json(result)
        }
        catch (err) {
            next(err)
        }
    },
    get: async (req, res, next) => {
        try {
            const query = req.query, etype = query.etype
            const content = await util.tempmail(etype)
            res.json(content)
        }
        catch (err) {
            next(err)
        }
    },
    post: async (req, res, next) => {
        try {
            const body = req.body, content = body.content, etype = body.etype
            let typemail
            if (etype == -2) typemail = "temp/passmail"
            else if (etype == 4) typemail = "temp/canmail"
            else if (etype == 1) typemail = "temp/usermail"
            else if (etype == 2) typemail = "temp/cusmail"
            else if (etype == 5) typemail = "temp/resetusermail"
            else typemail = "temp/mail"
            const KEY = util.etbis(etype)
            let FILE
            if (pathtemplate) {
                FILE = path.join(pathtemplate, (typemail.split("/"))[1])
            } else
                FILE = path.join(__dirname, "..", "..", typemail)
            await redis.set(KEY, content)
            fs.writeFileSync(FILE, content)
            res.json(`Đã lưu thành công mẫu email (Template email has been saved successfully)`)
        }
        catch (err) {
            next(err)
        }
    }
}
module.exports = Service