"use strict"
//import { PDFDocument } from 'pdf-lib';
const handlebars = require("handlebars")
const pdflib = require('pdf-lib')
const Hashids = require("hashids/cjs")
const svgCaptcha = require('svg-captcha')
const bcrypt = require("bcryptjs")
const path = require("path")
const fs = require("fs")
const he = require("he")
const axios = require("axios")
const crypto = require("crypto")
const cheerio = require("cheerio")
const moment = require("moment")
const fxp = require("fast-xml-parser")

const js2xmlparser = require("js2xmlparser")
const objectMapper = require("object-mapper")
const Cryptr = require('cryptr')
const jwt = require("jsonwebtoken")
const numeral = require("numeral")
const jszip = require("jszip")
const docxtemplater = require("docxtemplater")
const config = require("./config")
const logger4app = require("./logger4app")
const n2w_jsreport = require("./n2w_jsreport")
const cryptr = new Cryptr(config.secret)
const redis = require("./redis")
const ext = require("./ext")
const decrypt = require("./encrypt")

var xmlescape = require('xml-escape');

const { totp } = require('otplib')

totp.options = (config.otp) ? config.otp : {}
var decode = require('decode-html');
const captchaType = config.captchaType
const config_jsrep = config.config_jsrep
const seasecret = config.seasecret
const check_mst = config.check_mst
const pathtemplate = config.pathtemplate
const ENT = config.ent
const KCT = "Không chịu thuế"
const KKK = "Không kê khai nộp thuế"
const KHAC = "Khác"
const KM = "Khuyến mãi"
const CK = "Chiết khấu"
const MT = "Mô tả"
const DCG = "Điều chỉnh giảm"
const DCT = "Điều chỉnh tăng"
const optj2x = { declaration: { include: false }, format: { pretty: false } }
let keydds = 'einvoicemzh';
keydds = crypto.createHash('sha256').update(String(keydds)).digest('base64').substr(0, 32);
//NĐ 123
const _01GTKT = "01GTKT"
    , _01TVE = "01/TVE"
    , _02TVE = "02/TVE"
    , _02GTTT = "02GTTT"
    , _07KPTQ = "07KPTQ"
    , _03XKNB = "03XKNB"
    , _04HGDL = "04HGDL"
    , _01BLP = "01BLP"
    , _02BLP = "02BLP"
    , _01DKTD_HDDT = "01/ĐKTĐ-HĐĐT"
    , _04SS_HDDT = "04/SS-HĐĐT"
    , _01TH_HDDT = "01/TH-HĐĐT"
const MAP_HDon_XML = {
    "@": "@",
    "TTChung.PBan": "TTChung.PBan",
    "TTChung.THDon": "TTChung.THDon",
    "TTChung.KHMSHDon": "TTChung.KHMSHDon",
    "TTChung.KHHDon": "TTChung.KHHDon",
    "TTChung.SHDon": "TTChung.SHDon",
    "TTChung.MHSo": "TTChung.MHSo",
    // "TTChung.TDLap": "TTChung.TDLap",
    "TTChung.NLap": "TTChung.NLap",
    "TTChung.HDDCKPTQuan": "TTChung.HDDCKPTQuan",
    "TTChung.SBKe": "TTChung.SBKe",
    "TTChung.NBKe": "TTChung.NBKe",
    "TTChung.DVTTe": "TTChung.DVTTe",
    "TTChung.TGia": "TTChung.TGia",
    "TTChung.HTTToan": "TTChung.HTTToan",
    "TTChung.MSTTCGP": "TTChung.MSTTCGP",
    "TTChung.MSTDVNUNLHDon": "TTChung.MSTDVNUNLHDon",
    "TTChung.TDVNUNLHDon": "TTChung.TDVNUNLHDon",
    "TTChung.DCDVNUNLHDon": "TTChung.DCDVNUNLHDon",
    // "TTChung.TTNCC": "TTChung.TTNCC",
    // "TTChung.DDTCuu": "TTChung.DDTCuu",
    // "TTChung.MTCuu": "TTChung.MTCuu",
    // "TTChung.THTTTKhac": "TTChung.THTTTKhac",
    // "TTChung.HTHDBTThe": "TTChung.HTHDBTThe",
    // "TTChung.TDLHDBTThe": "TTChung.TDLHDBTThe",
    // "TTChung.KHMSHDBTThe": "TTChung.KHMSHDBTThe",
    // "TTChung.KHHDBTThe": "TTChung.KHHDBTThe",
    // "TTChung.SHDBTThe": "TTChung.SHDBTThe",
    // "TTChung.HTHDBDChinh": "TTChung.HTHDBDChinh",
    // "TTChung.TDLHDDChinh": "TTChung.TDLHDDChinh",
    // "TTChung.KHMSHDDChinh": "TTChung.KHMSHDDChinh",
    // "TTChung.KHHDBDChinh": "TTChung.KHHDBDChinh",
    // "TTChung.SHDBDChinh": "TTChung.SHDBDChinh",
    // "TTChung.TChat": "TTChung.TChat",
    "TTChung.TTHDLQuan": "TTChung.TTHDLQuan",
    "TTChung.TTKhac.TTin[]": "TTChung.TTKhac.TTin[]",

    "NDHDon.NBan": "NDHDon.NBan",
    "NDHDon.NMua": "NDHDon.NMua",
    "NDHDon.DSHHDVu": "NDHDon.DSHHDVu",

    "NDHDon.TToan.THTTLTSuat": "NDHDon.TToan.THTTLTSuat",
    // "NDHDon.TToan.TCDChinh": "NDHDon.TToan.TCDChinh",
    "NDHDon.TToan.TgTCThue": "NDHDon.TToan.TgTCThue",
    "NDHDon.TToan.TgTThue": "NDHDon.TToan.TgTThue",
    "NDHDon.TToan.DSLPhi": "NDHDon.TToan.DSLPhi",
    // "NDHDon.TToan.TCDCCKTMai": "NDHDon.TToan.TCDCCKTMai",
    "NDHDon.TToan.TTCKTMai": "NDHDon.TToan.TTCKTMai",
    "NDHDon.TToan.TGTKhac": "NDHDon.TToan.TGTKhac",
    // "NDHDon.TToan.TCDCTTTToan": "NDHDon.TToan.TCDCTTTToan",
    "NDHDon.TToan.TgTTTBSo": "NDHDon.TToan.TgTTTBSo",
    "NDHDon.TToan.TgTTTBChu": "NDHDon.TToan.TgTTTBChu",
}
const MAP_ERR_NOTI_XML = {
    "noti_taxtype": "Loai",
    "noti_taxid": "MCQT",
    "noti_taxname": "TCQT",
    "sname": "TNNT",
    "stax": "MST",
    "place": "DDanh",
    "noti_dt": "NTBao",
    // "createdt": "NTBNNT"
}
const MAP_ERR_NOTI_LIST_INV = {
    "line": "STT",
    "ma_cqthu": "MCCQT",
    "form": "KHMSHDon",
    "serial": "KHHDon",
    "seq": "SHDon",
    "idt": "Ngay",
    "type_ref": "LADHDDT",
    "noti_type": "TCTBao",
    "rea": "LDo"
}
const MAP_LIST_INV_XML = {
    "listinvoice_num": "TTChung.SBTHDLieu",
    "period_type": "TTChung.LKDLieu",
    "period": "TTChung.KDLieu",
    "times": "TTChung.LDau",
    "idt": "TTChung.NLap",
    "sname": "TTChung.TNNT",
    "stax": "TTChung.MST",
    "item_type": "TTChung.LHHoa"
}
const MAP_REG_DEC_ORDER_XML = {
    "@": "@",
    "TTChung.PBan": "TTChung.PBan",
    "TTChung.MSo": "TTChung.MSo",
    "TTChung.Ten": "TTChung.Ten",
    "TTChung.HThuc": "TTChung.HThuc",
    "TTChung.TNNT": "TTChung.TNNT",
    "TTChung.MST": "TTChung.MST",
    "TTChung.CQTQLy": "TTChung.CQTQLy",
    "TTChung.MCQTQLy": "TTChung.MCQTQLy",
    "TTChung.NLHe": "TTChung.NLHe",
    "TTChung.DCLHe": "TTChung.DCLHe",
    "TTChung.DCTDTu": "TTChung.DCTDTu",
    "TTChung.DTLHe": "TTChung.DTLHe",
    "TTChung.DDanh": "TTChung.DDanh",
    "TTChung.NLap": "TTChung.NLap",
    "NDTKhai.HTHDon.CMa": "NDTKhai.HTHDon.CMa",
    "NDTKhai.HTHDon.KCMa": "NDTKhai.HTHDon.KCMa",
    "NDTKhai.HTGDLHDDT.NNTDBKKhan": "NDTKhai.HTGDLHDDT.NNTDBKKhan",
    "NDTKhai.HTGDLHDDT.NNTKTDNUBND": "NDTKhai.HTGDLHDDT.NNTKTDNUBND",
    "NDTKhai.HTGDLHDDT.CDLTTDCQT": "NDTKhai.HTGDLHDDT.CDLTTDCQT",
    "NDTKhai.HTGDLHDDT.CDLQTCTN": "NDTKhai.HTGDLHDDT.CDLQTCTN",
    "NDTKhai.PThuc.CDDu": "NDTKhai.PThuc.CDDu",
    "NDTKhai.PThuc.CBTHop": "NDTKhai.PThuc.CBTHop",
    "NDTKhai.LHDSDung.HDGTGT": "NDTKhai.LHDSDung.HDGTGT",
    "NDTKhai.LHDSDung.HDBHang": "NDTKhai.LHDSDung.HDBHang",
    "NDTKhai.LHDSDung.HDBTSCong": "NDTKhai.LHDSDung.HDBTSCong",
    "NDTKhai.LHDSDung.HDBHDTQGia": "NDTKhai.LHDSDung.HDBHDTQGia",
    "NDTKhai.LHDSDung.HDKhac": "NDTKhai.LHDSDung.HDKhac",
    "NDTKhai.LHDSDung.CTu": "NDTKhai.LHDSDung.CTu",
    "NDTKhai.DSCTSSDung.CTS": "NDTKhai.DSCTSSDung.CTS"
}
const MAP_ERR_NOTI_ORDER_XML = {
    "@": "@",
    "PBan": "PBan",
    "MSo": "MSo",
    "Ten": "Ten",
    "Loai": "Loai",
    "So": "So",
    "NTBCCQT": "NTBCCQT",
    "MCQT": "MCQT",
    "TCQT": "TCQT",
    "TNNT": "TNNT",
    "MST": "MST",
    "MDVQHNSach": "MDVQHNSach",
    "DDanh": "DDanh",
    "NTBao": "NTBao",
    // "NTBNNT": "NTBNNT",
    "DSHDon.HDon": "DSHDon.HDon"
}
const MAP_REG_DEC_LIST = {
    "line": "STT",
    "sign_org": "TTChuc",
    "sign_seri": "Seri",
    "sign_from": "TNgay",
    "sign_to": "DNgay",
    "action": "HThuc"
}

const MAP_REG_DEC_XML = {
    "regtype": "TTChung.HThuc",
    "sname": "TTChung.TNNT",
    "stax": "TTChung.MST",
    "taxo": "TTChung.MCQTQLy",
    "taxoname": "TTChung.CQTQLy",
    "contactname": "TTChung.NLHe",
    "contactaddr": "TTChung.DCLHe",
    "contactemail": "TTChung.DCTDTu",
    "contactphone": "TTChung.DTLHe",
    "place": "TTChung.DDanh",
    "createdt": "TTChung.NLap",
    "nntdbkkhan": "NDTKhai.HTGDLHDDT.NNTDBKKhan",
    "nntktdnubnd": "NDTKhai.HTGDLHDDT.NNTKTDNUBND",
    "sendinv": "NDTKhai.PThuc.CDDu",
    "listinv": "NDTKhai.PThuc.CBTHop",
    "type_gtgt": "NDTKhai.LHDSDung.HDGTGT",
    "type_hdbhang": "NDTKhai.LHDSDung.HDBHang",
    "type_tscong": "NDTKhai.LHDSDung.HDBTSCong",
    "type_tsqgia": "NDTKhai.LHDSDung.HDBHDTQGia",
    "type_khac": "NDTKhai.LHDSDung.HDKhac",
    "type_ctu": "NDTKhai.LHDSDung.CTu"
}
const MAP_LIST_INV_LIST = {
    "line": "STT",
    "form": "KHMSHDon",
    "serial": "KHHDon",
    "seq": "SHDon",
    "idt": "NLap",
    "bname": "TNMua",
    "btax": "MSTNMua",
    "bcode": "MKHang",
    "code": "MHHDVu",
    "name": "THHDVu",
    "unit": "DVTinh",
    "quantity": "SLuong",
    "amount": "TTCThue",
    "vrn": "TSuat",
    "vat": "TgTThue",
    "total": "TgTTToan",
    "kindinv": "TThai",
    "type_ref": "LHDCLQuan",
    "note": "GChu",
    // "curr": "DVTTe",
    "exrt": "TGia"
}
const MAP_LIST_INV_ORDER_XML = {
    "@": "@",
    "TTChung.PBan": "TTChung.PBan",
    "TTChung.MSo": "TTChung.MSo",
    "TTChung.Ten": "TTChung.Ten",
    "TTChung.SBTHDLieu": "TTChung.SBTHDLieu",
    "TTChung.LKDLieu": "TTChung.LKDLieu",
    "TTChung.KDLieu": "TTChung.KDLieu",
    "TTChung.LDau": "TTChung.LDau",
    "TTChung.BSLThu": "TTChung.BSLThu",
    "TTChung.NLap": "TTChung.NLap",
    "TTChung.TNNT": "TTChung.TNNT",
    "TTChung.MST": "TTChung.MST",
    "TTChung.HDDIn": "TTChung.HDDIn",
    "TTChung.LHHoa": "TTChung.LHHoa",
    "TTChung.DVTTe": "TTChung.DVTTe",
    "NDBTHDLieu.DSDLieu": "NDBTHDLieu.DSDLieu"
}
const convertNegativeNum = (numOri) => {
	
    if(!numOri) return 0 
    let numNegative = parseFloat(numOri)
    numNegative = Math.abs(numNegative)
    if (numNegative !== 0) { numNegative = -1*numNegative }
    return numNegative
}
const mapping_reg_declare_xml = (jsonReal, inc, b) => {
    let json = JSON.parse(JSON.stringify(jsonReal))
        , CLONE_MAP_REG_DEC_XML = {...MAP_REG_DEC_XML}
    const {
        createdt,
        hascode,
        items
    } = json
    json = Service.rvControlcharactersObject(json)
    let obj = objectMapper(json, CLONE_MAP_REG_DEC_XML)
        , CMa
        , KCMa
        , CDLQTCTN
    obj.TTChung.PBan = config.XML_REG_VERSION
    obj.TTChung.MSo = "01/ĐKTĐ-HĐĐT"
    obj.TTChung.Ten = "Tờ khai đăng ký/thay đổi thông tin sử dụng hóa đơn điện tử"
    obj.TTChung.NLap = Service.convertDateAsMoment(obj.TTChung.NLap)
    if (hascode == 1) {
        CMa = 1
        KCMa = 0
        CDLQTCTN = 0
    } else {
        CMa = 0
        KCMa = 1
        CDLQTCTN = 1
    }
    obj.NDTKhai.HTHDon = {
        ...obj.NDTKhai.HTHDon,
        CMa,
        KCMa
    }
    obj.NDTKhai.HTGDLHDDT = {
        ...obj.NDTKhai.HTGDLHDDT,
        CDLTTDCQT: 0,
        CDLQTCTN
    }
    if (items) {
        for (let row of items) {
            let {
                sign_from = '',
                sign_to = ''
            } = row
            row.sign_from = sign_from.replace(" ", "T")
            row.sign_to = sign_to.replace(" ", "T")
            row = Service.rvControlcharactersObject(row)
        }
        const CTS = items.map(row => objectMapper(row, MAP_REG_DEC_LIST))
        obj.NDTKhai.DSCTSSDung = { CTS }
    }
    let dltkhai = { TTChung: obj.TTChung, NDTKhai: obj.NDTKhai }
    if (b) { dltkhai["@"] = { Id: `_${inc}` } }
    dltkhai = objectMapper(dltkhai, MAP_REG_DEC_ORDER_XML)
    const xml = js2xmlparser.parse("TKhai", { DLTKhai: dltkhai }, optj2x)
    return xml
}
const mapping_erroneous_notice_xml = (jsonReal, inc, b) => {
    let json = JSON.parse(JSON.stringify(jsonReal))
        , CLONE_MAP_ERR_NOTI_XML = { ...MAP_ERR_NOTI_XML }
    const {
        noti_taxtype,
        noti_taxnum = '',
        noti_taxdt = '',
        items
    } = json
    json = Service.rvControlcharactersObject(json)
    
    let obj = objectMapper(json, CLONE_MAP_ERR_NOTI_XML)
    obj.PBan = config.XML_TBSS_VERSION
    obj.MSo = "04/SS-HĐĐT"
    obj.Ten = "Thông báo hóa đơn điện tử có sai sót"
    obj.NTBao = Service.convertDateAsMoment(obj.NTBao)
    // obj.NTBNNT = Service.convertDateAsMoment(obj.NTBNNT)
    if (noti_taxtype == 2) {
        obj.So = noti_taxnum
        obj.NTBCCQT = Service.convertDateAsMoment(noti_taxdt)
    }
    if (items) {
        const HDon = items.map(row => {
            let item = JSON.parse(JSON.stringify(row))
            item.idt = Service.convertDateAsMoment(item.idt)
            item = Service.rvControlcharactersObject(item)
            return objectMapper(item, MAP_ERR_NOTI_LIST_INV)
        })
        obj.DSHDon = { HDon }
    }
    let dltbao = JSON.parse(JSON.stringify(obj))
    if (b) { dltbao["@"] = { Id: `_${inc}` } }
    dltbao = objectMapper(dltbao, MAP_ERR_NOTI_ORDER_XML)
    const xml = js2xmlparser.parse("TBao", { DLTBao: dltbao }, optj2x)
    return xml
}
const mapping_list_inv_xml = (jsonReal, inc, b) => {
    let json = JSON.parse(JSON.stringify(jsonReal))
        , CLONE_MAP_LIST_INV_XML = { ...MAP_LIST_INV_XML }
    const {
        idt,
        times,
        items,
        curr="VND"
    } = json
    json =  Service.rvControlcharactersObject(json)
    // json.idt = idt.replace(" ", "T")
    json.idt = Service.convertDateAsMoment(idt)
   //  CLONE_MAP_LIST_INV_XML["times"] = "TTChung.BSLThu" 
    let obj = objectMapper(json, CLONE_MAP_LIST_INV_XML)
    obj.TTChung.PBan = config.XML_BTH_VERSION
    obj.TTChung.MSo = "01/TH-HĐĐT"
    obj.TTChung.Ten = "Bảng tổng hợp dữ liệu hóa đơn điện tử"
    obj.TTChung.HDDIn = 0
    if(times==0) {
        obj.TTChung.LDau="1"
        obj.TTChung.BSLThu="0"
    }else{
        obj.TTChung.LDau="0"
        obj.TTChung.BSLThu=times
    }
    if (curr) obj.TTChung.DVTTe = curr
    if (items) {
        let DSDLieu = {}
        let DLieu = []
        // const HDon = items.map(row => objectMapper(row, MAP_LIST_INV_LIST))
        // DSDLieu = { HDon }
        for (let inv of items) {
            let objMapInv = {}
            inv.idt = Service.convertDateAsMoment(inv.idt)
            if (inv.vrt !== undefined) { inv.vrt = `${inv.vrt}%` }
            objMapInv = objectMapper(inv, MAP_LIST_INV_LIST)
            if (objMapInv.MSTNMua && !Service.checkmst(objMapInv.MSTNMua.toString())){               
                //objMapInv.MKHang = objMapInv.MSTNMua
                objMapInv.MSTNMua = ''
            }
            const {
                adj
            } = inv
            if (adj) {
                if (adj.seq) {
                    if(!adj.form) adj.form= adj.seq.split('-')[0]
                    if(!adj.serial) adj.serial= adj.seq.split('-')[1]
                    if(adj.seq.includes('-')) adj.seq=adj.seq.split('-')[2]
                }
                if (adj.type) { objMapInv.TThai = adj.type }
                if (adj.type_ref) { objMapInv.LHDCLQuan = adj.type_ref }
                if (adj.form) { objMapInv.KHMSHDCLQuan = adj.form }
                if (adj.serial) { objMapInv.KHHDCLQuan = adj.serial }
                if (adj.seq) { objMapInv.SHDCLQuan = adj.seq }
                if (adj.periodtype) { objMapInv.LKDLDChinh = adj.periodtype }
                if (adj.periodt) { objMapInv.KDLDChinh = adj.periodt }
                if (adj.inform_num) { objMapInv.STBao = adj.inform_num }
                if (adj.inform_dt) { objMapInv.NTBao = Service.convertDateAsMoment(adj.inform_dt) }
                if (adj.note) { objMapInv.GChu = Service.rvControlcharacters(adj.note) }
            }
            DLieu.push(objMapInv)
        }
        DSDLieu = { DLieu }
        obj.NDBTHDLieu = { DSDLieu }
    }
    let dlbthop = JSON.parse(JSON.stringify(obj))
    if (b) { dlbthop["@"] = { Id: `_${inc}` } }
    dlbthop = objectMapper(dlbthop, MAP_LIST_INV_ORDER_XML)
    const xml = js2xmlparser.parse("BTHDLieu", { DLBTHop: dlbthop }, optj2x)
    return xml
}
const convertPositiveNum = (numOri) => {
    if(!numOri) return 0
    let numPositive = parseFloat(numOri)
    numPositive = Math.abs(numPositive)
    return numPositive
}
const convertPositiveNumCK = (numOri) => {
    if(!numOri) return 0
    let numPositive = parseFloat(numOri)
 
    return numPositive
}
const mapping_inv_xml = async (json_original, inc, b) => {
    let json = JSON.parse(JSON.stringify(json_original))
        , REP_MAP_HDR = {...MAP_HDR}
    const DEFALUT_TYPE_REF = 3
    // json = Service.prettyJson(json)
    if(json.curr !== "VND") { json = Service.formatNumData(json) }
    const {
        type = '',
        uc = '',
        adj = {},
        dif = {},
        adjType = {},
        org = {},
        idt = '',
        fees = [],
        // sec = '',
        paym = '',
        curr = '',
        tradeamount = '',
        tradeamountv = '',
        docid = '',
        invlistnum = '',
        invlistdt = '',
        inv_ntz = '',
        unl_tax = '',
        unl_name = '',
        unl_addr = '',
        type_ref
    } = json
    let {
        tax = [],
        items = [],
        ud = '',
        tradestatus = '',
        // statusRoot = undefined,
    } = json
    let isAdjInv = false
    let is123Standard = false
    if (type_ref == 1) { is123Standard = true }
    const taxOriginal = uc.split(".")[0]
    let arrSame01GTKT = [_01GTKT, _01TVE]
        , arrSame02GTTT = [_02GTTT, _07KPTQ]
        , arrType0304 = [_03XKNB, _04HGDL]
        , arrTypeNotTaxs = [...arrSame02GTTT, _01BLP, _02BLP, _02TVE]
        , paymSample = JSON.stringify(config.PAYMETHOD)
        , configFieldInv = []
    paymSample = JSON.parse(paymSample)
    if (json.configfield && json.configfield.configfieldhdr && json.configfield.configfieldhdr.length > 0) {
        configFieldInv = json.configfield.configfieldhdr
    }
    //const col = require("../api/col")
    //if(configFieldInv.length == 0) configFieldInv = await col.cache(taxOriginal, type)
    if(ud !== undefined && ud !== null && ud.toString() == "0") ud = 2
    if(tradestatus !== undefined && tradestatus !== null && tradestatus.toString() == "0") tradestatus = 2
    if(curr === "VND") {
        REP_MAP_HDR["sumv"] = REP_MAP_HDR["sum"]
        REP_MAP_HDR["vatv"] = REP_MAP_HDR["vat"]
        REP_MAP_HDR["totalv"] = REP_MAP_HDR["total"]
        delete REP_MAP_HDR["sum"]
        delete REP_MAP_HDR["vat"]
        delete REP_MAP_HDR["total"]
    } 
    if (arrType0304.includes(type)) {
        json.saddr = Service.rvControlcharacters(json.whsfr)
        json.baddr = Service.rvControlcharacters(json.whsto)
        json.bname = Service.rvControlcharacters(json.recvr)
        if(json.smail !== undefined) delete json.smail
        // if(json.bmail !== undefined) delete json.bmail
        if(json.bmail !== undefined && type === _04HGDL) { delete json.bmail }
        if(json.sacc !== undefined) delete json.sacc
        if(json.whsfr !== undefined) delete json.whsfr
        if(json.sbank !== undefined) delete json.sbank
        if(json.bbank !== undefined) delete json.bbank
    }
    if (!arrSame01GTKT.includes(type)) {
        if(json.tax !== undefined) delete json.tax
        if(json.vat !== undefined) delete json.vat
        if(json.vatv !== undefined) delete json.vatv
    }
    if (arrTypeNotTaxs.includes(type)) {
        if(REP_MAP_HDR["sum"] !== undefined) delete REP_MAP_HDR["sum"]
        if(REP_MAP_HDR["sumv"] !== undefined) delete REP_MAP_HDR["sumv"]
        if(dif.sum !== undefined) delete dif.sum
        if(dif.sumv !== undefined) delete dif.sumv
    }
    if (type === _07KPTQ) { json.name = "HÓA ĐƠN BÁN HÀNG(Dành cho tổ chức, cá nhân trong khu phi thuế quan)" }
    if (type === _04HGDL) {
        // Check 'trans' before 'contr'
        let objToArrKeys = Object.keys(REP_MAP_HDR)
        if(objToArrKeys.indexOf("contr") < objToArrKeys.indexOf("trans")) {
            // swap contr and trans in REP_MAP_HDR obj
            let tempContr = REP_MAP_HDR["contr"]
            let tempTrans = REP_MAP_HDR["trans"]
            REP_MAP_HDR = swapKeyToValue(REP_MAP_HDR)
            REP_MAP_HDR[tempContr] = "trans"
            REP_MAP_HDR[tempTrans] = "contr"
            REP_MAP_HDR = swapKeyToValue(REP_MAP_HDR)
            REP_MAP_HDR["trans"] = tempTrans
            REP_MAP_HDR["contr"] = tempContr
        }
        if(json.ordno !== undefined) delete json.ordno
    }
    if (type !== _03XKNB) {
        delete REP_MAP_HDR["receiver"]
        delete REP_MAP_HDR["exporter"]
    }
    if (curr !== "VND") {
        try {json.word = n2w_jsreport.d2w(Math.abs(json.total), curr)} catch(e){}
    }
    if (json.cedate) json.cedate = Service.convertDateAsMoment(json.cedate)
    let checkckck=false
    
    json = Service.rvControlcharactersObject(json)
    let obj = objectMapper(json, REP_MAP_HDR), TChatHD = 1
    for (let row of items) {
        if(row.type=="Chiết khấu" || row.type=="CK")  checkckck=true
    }
    if(checkckck) {
        obj.NDHDon.TToan.TgTTTBSo = convertPositiveNum(obj.NDHDon.TToan.TgTTTBSo)
        // obj.NDHDon.TToan.TTCKTMai = convertPositiveNum(obj.NDHDon.TToan.TTCKTMai)
        if (type === _01GTKT) {
            obj.NDHDon.TToan.TgTCThue = convertPositiveNum(obj.NDHDon.TToan.TgTCThue)
            obj.NDHDon.TToan.TgTThue = convertPositiveNum(obj.NDHDon.TToan.TgTThue)
        }
        obj.NDHDon.TToan.TgTTTBChu = n2w_jsreport.d2w(Math.abs(json.total), curr)
    }
    if (json.status == 4) TChatHD = 7
    obj.TTChung.PBan = config.XML_INV_VERSION
    // if(type==_03XKNB) obj.TTChung.PBan = "2.0.0"
    // obj.TTChung.TDLap = idt.replace(" ", "T")
    obj.TTChung.NLap = idt.split(" ")[0]
    // obj.TTChung.TTNCC = "FPT eInvoice"
    obj.TTChung.DDTCuu = "tracuuhoadon.fpt.com.vn"
    // if(sec != '') { obj.TTChung.MTCuu = sec }
    if([_01GTKT, _02GTTT, _07KPTQ, _01TVE, _02TVE].includes(type)) {
        if(notEmptyStr(docid)) { obj.TTChung.MHSo = docid }
        if(notEmptyStr(unl_tax)) { obj.TTChung.MSTDVNUNLHDon = unl_tax }
        if(notEmptyStr(unl_name)) { obj.TTChung.TDVNUNLHDon = Service.rvControlcharacters(unl_name) }
        if(notEmptyStr(unl_addr)) { obj.TTChung.DCDVNUNLHDon = Service.rvControlcharacters(unl_addr) }
        if([_01GTKT, _02GTTT, _07KPTQ].includes(type)) {
            if(notEmptyStr(invlistdt)) { 
                obj.TTChung.NBKe = Service.convertDateAsMoment(invlistdt)
            }
            if(notEmptyStr(invlistnum)) { obj.TTChung.SBKe = invlistnum }
        }
    }
    //if([_01GTKT, _02GTTT, _03XKNB, _01TVE, _02TVE].includes(type)) {
        obj.TTChung.MSTTCGP = "0104128565"
    //}
    if(paym != '' && Array.isArray(paymSample) && paymSample.some(obj => obj.id == paym)) {
        obj.TTChung.HTTToan = paym
    }
    if(arrSame02GTTT.includes(type)) { obj.TTChung.HDDCKPTQuan = inv_ntz }
    
    let ttkTTChung = []
    for (let i = 0; i < 10; i++) {
        let keyJson = `c${i}`
        let valJson = json[keyJson]
        if(valJson) {
            let objRemakeKey = configFieldInv.find((obj) => obj.id == keyJson) || null
            if(objRemakeKey != null) keyJson = objRemakeKey.label || keyJson // || objRemakeKey.placeholder
            ttkTTChung.push(TTIN(keyJson, valJson))
        }
    }
    if (json.note) ttkTTChung.push(TTIN("note", Service.rvControlcharacters(json.note)))
    if(ttkTTChung.length) obj.TTChung.TTKhac = { TTin: ttkTTChung} //{ TTin: [TTIN("sec", json.sec)] }
    let isDecreAdjMoney = false
    if (adj && Object.keys(adj).length !== 0 && adj.constructor === Object) {
        let TTHDLQuan = new Object()
        if(ud && ud.toString() == "0") ud = 2
        if (adj.typ == 1) { // Replacement invoice
            
            TTHDLQuan.TCHDon = 1
           
           if((adj.form) && (adj.form).length >1) TTHDLQuan.LHDCLQuan = 3
           else TTHDLQuan.LHDCLQuan = 1
            TTHDLQuan.KHMSHDCLQuan = adj.form
            TTHDLQuan.KHHDCLQuan = adj.serial
            TTHDLQuan.SHDCLQuan = adj.seq
        } else if (adj.typ == 2) { // Adjustment invoice
            isAdjInv = true
            if(curr !== "VND") try {dif.word = n2w_jsreport.d2w(Math.abs(dif.total), curr)} catch(e){}
         
            TTHDLQuan.TCHDon = 2
          
           if((adj.form) && (adj.form).length >1) TTHDLQuan.LHDCLQuan = 3
           else TTHDLQuan.LHDCLQuan = 1
            TTHDLQuan.KHMSHDCLQuan = adj.form
            TTHDLQuan.KHHDCLQuan = adj.serial
            TTHDLQuan.SHDCLQuan = adj.seq
            if ([_01GTKT, _02GTTT, _07KPTQ].includes(type)) {
                if ((notEmptyStr(dif.sum) && dif.sum > 0)|| (notEmptyStr(dif.total) && dif.total > 0) || adj.adjType==3 || adjType==3) {
                    isDecreAdjMoney = true
                    obj.NDHDon.TToan.TgTTTBSo = convertNegativeNum(obj.NDHDon.TToan.TgTTTBSo)
                    // obj.NDHDon.TToan.TTCKTMai = convertNegativeNum(obj.NDHDon.TToan.TTCKTMai)
                    if (type === _01GTKT) {
                        obj.NDHDon.TToan.TgTCThue = convertNegativeNum(obj.NDHDon.TToan.TgTCThue)
                        obj.NDHDon.TToan.TgTThue = convertNegativeNum(obj.NDHDon.TToan.TgTThue)
                    }
                } else {
                    obj.NDHDon.TToan.TgTTTBSo = convertPositiveNum(obj.NDHDon.TToan.TgTTTBSo)
                    // obj.NDHDon.TToan.TTCKTMai = convertPositiveNum(obj.NDHDon.TToan.TTCKTMai)
                    if (type === _01GTKT) {
                        obj.NDHDon.TToan.TgTCThue = convertPositiveNum(obj.NDHDon.TToan.TgTCThue)
                        obj.NDHDon.TToan.TgTThue = convertPositiveNum(obj.NDHDon.TToan.TgTThue)
                    }
                }
            }
        }
        if ([_01GTKT, _02GTTT, _07KPTQ, _03XKNB].includes(type)) { 
            let adjIdt = Service.convertDateAsMoment(adj.idt)
            TTHDLQuan.NLHDCLQuan = adjIdt
        }
        let noteBonusAdj = Service.rvControlcharacters(adj.rea ? ` - Nội dung: ${adj.rea}` : '')
        if (adj.ref && adj.rdt) {
            TTHDLQuan.GChu = `Theo số biên bản: ${adj.ref} ngày ${moment(adj.rdt).format(config.mfd)}${noteBonusAdj}`
        }
        else {
            TTHDLQuan.GChu = `${noteBonusAdj}`
        }
        obj.TTChung.TTHDLQuan = TTHDLQuan
    }
   
    let taxs = JSON.parse(JSON.stringify(tax))
    // if (adj && adj.typ == 2) {
    //     if (json.items && json.items.length) taxs = Service.taxListExist(Service.getDifOfItems(json.pay.items, json.items), json.exrt)
    //     else taxs = [{ amtv: "", vatv: "", vrt: "" }]
    // } else {
    //     // taxListExist là tính tax ko dựa vào amount =0 mà cứ xuất hiện là tính
    //     taxs = Service.taxListExist(json.items, json.exrt)
    // }
     //kiem tra ck thi giu nguyen tien
     let checkck=false
    //  for (let row of items) {
    //    if(row.type=="Chiết khấu" || row.type=="CK")  checkck=true
    //  }
    if (Array.isArray(taxs) && !arrTypeNotTaxs.includes(type)) {
        let REP_MAP_TAX = {...MAP_TAX}
        for (const row of taxs) {
            if (`${row.vrt + ""}`.length) row.vrt = mapvrt(row.vrn, row.vrt, is123Standard).toUpperCase()
            if(row.status !== undefined && row.status !== null && row.status.toString() == "0")  row.status = 2
            if(!checkck){
                row.amt = (Service.isNumeric(row.amt)) ? Math.abs(row.amt) : 0
                row.vat = (Service.isNumeric(row.vat)) ? Math.abs(row.vat) : 0
            }else{
                row.amt = (Service.isNumeric(row.amt)) ? row.amt : 0
                row.vat = (Service.isNumeric(row.vat)) ? row.vat : 0
            }
          
            // if(curr === "VND") {
            //     row.amtv = (Service.isNumeric(row.amtv)) ? Math.abs(row.amtv) : 0
            //     row.vatv = (Service.isNumeric(row.vatv)) ? Math.abs(row.vatv) : 0
            // }
            if(type === _01GTKT) {
                if(isDecreAdjMoney) {
                    row.amt = convertNegativeNum(row.amt)
                    row.vat = convertNegativeNum(row.vat)
                    if (notEmptyStr(row.amtv)) { row.amtv = convertNegativeNum(row.amtv) }
                    if (notEmptyStr(row.vatv)) { row.vatv = convertNegativeNum(row.vatv) }
                } else {
                   
                    if(!checkck){
                        row.amt = convertPositiveNum(row.amt)
                        row.vat = convertPositiveNum(row.vat)
                        if (notEmptyStr(row.amtv)) { row.amtv = convertPositiveNum(row.amtv) }
                        if (notEmptyStr(row.vatv)) { row.vatv = convertPositiveNum(row.vatv) }
                    }else{
                        row.amt = convertPositiveNumCK(row.amt)
                        row.vat = convertPositiveNumCK(row.vat)
                        if (notEmptyStr(row.amtv)) { row.amtv = convertPositiveNumCK(row.amtv) }
                        if (notEmptyStr(row.vatv)) { row.vatv = convertPositiveNumCK(row.vatv) }
                    }
                    
                }
            }
        }
        if(curr === "VND") {
            REP_MAP_TAX["amtv"] = REP_MAP_TAX["amt"]
            REP_MAP_TAX["vatv"] = REP_MAP_TAX["vat"]
            delete REP_MAP_TAX["amt"]
            delete REP_MAP_TAX["vat"]
            if (taxs.length == 0) {
                taxs.push({
                    vrt: '',
                    amtv: '',
                    vatv: ''
                })
            }
        }
        if (taxs.length == 0) {
            taxs.push({
                vrt: '',
                amt: '',
                vat: ''
            })
        }
        if ([_01GTKT, _02GTTT].includes(type)) {
            const LTSuat = taxs.map(row => objectMapper(row, REP_MAP_TAX))
            // Tổng giảm trừ khác: Thẻ HDon\DLHDon\NDHDon\TToan chứa thông tin về số tiền thanh toán, số tiền thuế trên hóa đơn
            // const TGTKhac = 0 (chưa có công thức tính tính đến ngày 11/1/2023)
            obj.NDHDon.TToan= {...obj.NDHDon.TToan, THTTLTSuat: { LTSuat }}
        }
    }
    // Duy sửa để tính đủ loại thúe suất - END

    if (items) {
        for (let row of items) {
            row = Service.rvControlcharactersObject(row)
            if (isAdjInv) {
               
                if ((notEmptyStr(dif.sum) && dif.sum > 0)|| (notEmptyStr(dif.total) && dif.total > 0) ||  adj.adjType==3 || adjType==3) {
                    row.quantity = convertNegativeNum(row.quantity)
                    row.price = convertNegativeNum(row.price)
                    if(notEmptyStr(row.amtdiscount)) { row.amtdiscount = convertNegativeNum(row.amtdiscount) }
                    row.amount = convertNegativeNum(row.amount)
                } else {
                    row.quantity = convertPositiveNum(row.quantity)
                    row.price = convertPositiveNum(row.price)
                    if(notEmptyStr(row.amtdiscount)) { row.amtdiscount = convertPositiveNum(row.amtdiscount) }
                    row.amount = convertPositiveNum(row.amount)
                }
            }
            switch (row.type) {
                case KM:
                    row.type = 2
                    break
                case MT:
                    row.type = 4
                    break
                case CK:
                    row.type = 3
                    break
                case DCG:
                    row.type = 1
                    break
                default:
                    row.type = 1
                    break
            }
            if(row.status !== undefined && row.status !== null && row.status.toString() == "0")  row.status = 2
            if (row.hasOwnProperty("vrn")) row.vrn = mapvrt(row.vrn, row.vrt, is123Standard).toUpperCase()
            if (row.vrn == "KHAC" && row.hasOwnProperty("vrt")){
                row.vrn = `${row.vrn}:${row.vrt}%`
            }
            if (arrType0304.includes(type)) {
                if(json.perdiscount !== undefined) delete json.perdiscount
                if(json.amtdiscount !== undefined) delete json.amtdiscount
            }
        }
        if (items.length == 0) {
            items.push({
                name: '',
                amount: ''
            })
        }
      
        const HHDVu = items.map(row => objectMapper(row, MAP_DTL))
        let rowsDtl = []
        //sqlDtl = "select id,idx,label,type,status,itype,xc,atr from invoice.s_dtl where taxc=? and itype=? order by idx"
        //resultDtl = await dbs.query(sqlDtl, [taxOriginal, type])
        if (json.configfield && json.configfield.configfielddtl && json.configfield.configfielddtl.length > 0) {
            rowsDtl = json.configfield.configfielddtl
        }
        for(let iItem = 0; iItem < items.length; iItem++) {
            let ttkItem = []
            let item = items[iItem]
            for (let i = 0; i < 10; i++) {
                let keyItem = `c${i}`
                let valItem = item[keyItem]
                let objRemakeKey = rowsDtl.find((obj) => `c${obj.idx}` == keyItem) || null
                if(objRemakeKey != null) keyItem = objRemakeKey.label || keyItem
                if(valItem || valItem == '') ttkItem.push(TTIN(keyItem, valItem))
            }
            if (item.note) ttkItem.push(TTIN("note", item.note))
            if (ttkItem.length) HHDVu[iItem].TTKhac = { TTin: ttkItem }
        }
        obj.NDHDon.DSHHDVu = { HHDVu }
    }
    let ttk = []
    if (curr !== "VND") {
        if (json.sum) ttk.push(TTIN("sum", json.sum, "numeric"))
        if (json.total) ttk.push(TTIN("total", json.total, "numeric"))
        if (json.tradeamount) ttk.push(TTIN("tradeamount", json.tradeamount, "numeric"))
        if (json.discount) ttk.push(TTIN("discount", json.discount, "numeric"))
        if (json.vat) ttk.push(TTIN("vat", json.vat, "numeric"))
        if (json.sc) ttk.push(TTIN("sc", json.sc, "numeric"))
    }
    if (ttk.length) obj.NDHDon.TToan.TTKhac = { TTin: ttk }
    // if (json.scv) obj.NDHDon.TToan.DSLPhi = { LPhi: { TLPhi: "Phí dịch vụ", TPhi: json.scv } }
    if(fees && fees.length > 0) {
        for(let iFee = 0; iFee < fees.length; iFee++) {
            const row = fees[iFee]
            if(row.status && row.status.toString() == "0")  row.status = 2
        }
        const LPhi = fees.map(row => objectMapper(row, MAP_FEE))
        obj.NDHDon.TToan.DSLPhi = { LPhi } 
    }
    if (arrType0304.includes(type)) { /* type == "03XKNB" || type == "04HGDL" */
        if(obj.NDHDon && obj.NDHDon.TToan) delete obj.NDHDon.TToan
    }
    let dlhdon = { TTChung: obj.TTChung, NDHDon: obj.NDHDon }
    if (b) dlhdon["@"] = { Id: `_${inc}` }
    dlhdon = objectMapper(dlhdon, MAP_HDon_XML)
   // dlhdon =JSON.parse(Service.rvControlcharactersObjectJsonString(JSON.stringify(dlhdon)))
    const xml = js2xmlparser.parse("HDon", { DLHDon: dlhdon }, optj2x)
   
    return xml
}
const genTmpFromContent = (content) => {
    const $ = cheerio.load(content)
    const sty = $("style").html(), bgi = $("#bgi").html(), hdr = $("#hdr").html(), dtl = $("#dtl").html(), body = $("body").html(), title = $("#hdr").attr("title"), ftr = $("#ftr").html(), rft = $("#rft").html(), lft = $("#lft").html()
    const tmp = { sty: sty, bgi: bgi, hdr: hdr, dtl: dtl, title: title, body: body, ftr: ftr, rft: rft, lft: lft }
    for (const key in tmp) tmp[key] = (tmp[key] && tmp[key] != null) ? decode(tmp[key]) : tmp[key]
    return tmp
}
//NĐ 123    
const MAP_HDon_635 = {
    "@": "@",
    "TTChung.PBan": "TTChung.PBan",
    "TTChung.THDon": "TTChung.THDon",
    "TTChung.KHMSHDon": "TTChung.KHMSHDon",
    "TTChung.KHHDon": "TTChung.KHHDon",
    "TTChung.SHDon": "TTChung.SHDon",
    // "TTChung.TDLap": "TTChung.TDLap",
    "TTChung.NLap": "TTChung.NLap",
    "TTChung.DVTTe": "TTChung.DVTTe",
    "TTChung.TGia": "TTChung.TGia",
    "TTChung.TTNCC": "TTChung.TTNCC",
    "TTChung.DDTCuu": "TTChung.DDTCuu",
    "TTChung.MTCuu": "TTChung.MTCuu",
    "TTChung.HTTToan": "TTChung.HTTToan",
    "TTChung.THTTTKhac": "TTChung.THTTTKhac",
    // "TTChung.HTHDBTThe": "TTChung.HTHDBTThe",
    // "TTChung.TDLHDBTThe": "TTChung.TDLHDBTThe",
    // "TTChung.KHMSHDBTThe": "TTChung.KHMSHDBTThe",
    // "TTChung.KHHDBTThe": "TTChung.KHHDBTThe",
    // "TTChung.SHDBTThe": "TTChung.SHDBTThe",
    // "TTChung.HTHDBDChinh": "TTChung.HTHDBDChinh",
    // "TTChung.TDLHDDChinh": "TTChung.TDLHDDChinh",
    // "TTChung.KHMSHDDChinh": "TTChung.KHMSHDDChinh",
    // "TTChung.KHHDBDChinh": "TTChung.KHHDBDChinh",
    // "TTChung.SHDBDChinh": "TTChung.SHDBDChinh",
    // "TTChung.TChat": "TTChung.TChat",
    "TTChung.TTHDLQuan": "TTChung.TTHDLQuan",
    "TTChung.TTKhac.TTin[]": "TTChung.TTKhac.TTin[]",

    "NDHDon.NBan": "NDHDon.NBan",
    "NDHDon.NMua": "NDHDon.NMua",
    "NDHDon.DSHHDVu": "NDHDon.DSHHDVu",

    "NDHDon.TToan.THTTLTSuat": "NDHDon.TToan.THTTLTSuat",
    "NDHDon.TToan.TCDChinh": "NDHDon.TToan.TCDChinh",
    "NDHDon.TToan.TgTCThue": "NDHDon.TToan.TgTCThue",
    "NDHDon.TToan.TgTThue": "NDHDon.TToan.TgTThue",
    "NDHDon.TToan.DSLPhi": "NDHDon.TToan.DSLPhi",
    "NDHDon.TToan.TCDCCKTMai": "NDHDon.TToan.TCDCCKTMai",
    "NDHDon.TToan.TTCKTMai": "NDHDon.TToan.TTCKTMai",
    "NDHDon.TToan.TCDCTTTToan": "NDHDon.TToan.TCDCTTTToan",
    "NDHDon.TToan.TgTTTBSo": "NDHDon.TToan.TgTTTBSo",
    "NDHDon.TToan.TgTTTBChu": "NDHDon.TToan.TgTTTBChu",
}
const MAP_HDR = {
    // "taxo": "TTChung.TTKhac.taxo",
    // "cancel": "TTChung.TTKhac.cancel",
    // "adj": "TTChung.TTKhac.adj",
    // "dif": "TTChung.TTKhac.dif",
    // "tax": "TTChung.TTKhac.tax",
    // "inc": "TTChung.TTKhac.inc",
    // "id": "TTChung.TTKhac.id",
    // "pid": "TTChung.TTKhac.pid",
    // "sec": "TTChung.TTKhac.sec",
    // "exrt": "TTChung.TTKhac.exrt",
    // "note": "TTChung.TTKhac.note",
    // "paym": "TTChung.TTKhac.paym",
    // "type": "TTChung.TTKhac.type",
    // "form": "TTChung.TTKhac.form",
    // "serial": "TTChung.TTKhac.serial",
    // "ou": "TTChung.TTKhac.ou",
    // "uc": "TTChung.TTKhac.uc",
    // "name": "TTChung.THDon",
    // "seq": "TTChung.SHDon",
    // "idt": "TTChung.NLap",
    // "curr": "TTChung.DVTTe",
    // "sname": "NDHDon.NBan.Ten",
    // "stax": "NDHDon.NBan.MST",
    // "saddr": "NDHDon.NBan.DChi",
    // "stel": "NDHDon.NBan.TTKhac.Tel",
    // "smail": "NDHDon.NBan.TTKhac.Mail",
    // "sacc": "NDHDon.NBan.TTKhac.Acc",
    // "sbank": "NDHDon.NBan.TTKhac.Bank",
    // "bname": "NDHDon.NMua.Ten",
    // "btax": "NDHDon.NMua.MST",
    // "baddr": "NDHDon.NMua.DChi",
    // "btel": "NDHDon.NMua.TTKhac.Tel",
    // "bmail": "NDHDon.NMua.TTKhac.Mail",
    // "bacc": "NDHDon.NMua.TTKhac.Acc",
    // "bbank": "NDHDon.NMua.TTKhac.Bank",
    // "buyer": "NDHDon.NMua.TTKhac.Buyer",
    // "discount": "NDHDon.TToan.TTKhac.discount",
    // "sum": "NDHDon.TToan.TTKhac.sum",
    // "sumv": "NDHDon.TToan.TgTCThue",
    // "vat": "NDHDon.TToan.TTKhac.vat",
    // "vatv": "NDHDon.TToan.TgTThue",
    // "total": "NDHDon.TToan.TTKhac.total",
    // "totalv": "NDHDon.TToan.TgTTTBSo",
    // "word": "NDHDon.TToan.TgTTTBChu"
    "form": "TTChung.KHMSHDon",
    "serial": "TTChung.KHHDon",
    "seq": "TTChung.SHDon",
    "name": "TTChung.THDon",
    "curr": "TTChung.DVTTe",
    "exrt": "TTChung.TGia",
    "sname": "NDHDon.NBan.Ten",
    "stax": "NDHDon.NBan.MST",
    "cenumber": "NDHDon.NBan.HDKTSo",
    "cedate": "NDHDon.NBan.HDKTNgay",
    "ordno": "NDHDon.NBan.LDDNBo",
    "saddr": "NDHDon.NBan.DChi",
    "stel": "NDHDon.NBan.SDThoai",
    "smail": "NDHDon.NBan.DCTDTu",
    "sacc": "NDHDon.NBan.STKNHang",
    "sbank": "NDHDon.NBan.TNHang",
    "whsfr": "NDHDon.NBan.DCKXHang",
    "contr": "NDHDon.NBan.HDSo",
    "trans": "NDHDon.NBan.TNVChuyen",
    "vehic": "NDHDon.NBan.PTVChuyen",
    "bname": "NDHDon.NMua.Ten",
    "btax": "NDHDon.NMua.MST",
    "baddr": "NDHDon.NMua.DChi",
    "bcode": "NDHDon.NMua.MKHang",
    "btel": "NDHDon.NMua.SDThoai",
    //"bmail": "NDHDon.NMua.DCTDTu",
    "buyer": "NDHDon.NMua.HVTNMHang",
    "bacc": "NDHDon.NMua.STKNHang",
    "bbank": "NDHDon.NMua.TNHang",
    "sum": "NDHDon.TToan.TgTCThue",
    "vat": "NDHDon.TToan.TgTThue",
    "tradeamount": "NDHDon.TToan.TTCKTMai",
    "discount": "NDHDon.TToan.TGTKhac",
    "total": "NDHDon.TToan.TgTTTBSo",
    "word": "NDHDon.TToan.TgTTTBChu"
}
const MAP_DTL = {
    // "line": "STT",
    // "type": "LHHDVu",
    // "name": "Ten",
    // "unit": "DVTinh",
    // "price": "DGia",
    // "quantity": "SLuong",
    // "vrt": "TSuat",
    // "amount": "ThTien"
    "type": "TChat",
    "status": "TCDChinh",
    "line": "STT",
    "code": "MHHDVu",
    "name": "THHDVu",
    "unit": "DVTinh",
    "quantity": "SLuong",
    "price": "DGia",
    "discountrate": "TLCKhau",
    "discountamt": "STCKhau",
    "amount": "ThTien",
    "vrn": "TSuat",
}
const MAP_TAX = {
    //status: "TCDChinh",
    vrt: "TSuat",
    amt: "ThTien",
    vat: "TThue",
}
let MAP_FEE = {
    // status: "TCDChinh",
    name: "TLPhi",
    amount: "TPhi",
}
const PAYM_SAMPLE = [
    { id: "TM", value: "1" },
    { id: "CK", value: "2" },
    { id: "TM/CK", value: "3" },
    { id: "DTCN", value: "4" },
    { id: "KTT", value: "5" }
]

const ITEM_VRT = [
    { vrt: "10%", vrn: "10%" },
    { vrt: "8%", vrn: "8%" },
    { vrt: "5%", vrn: "5%" },
    { vrt: "0%", vrn: "0%" },
    { vrt: "KCT", vrn: "-1" },
    { vrt: "KKKNT", vrn: "-2" },
    { vrt: "KCT", vrn: KCT },
    { vrt: "KKKNT", vrn: KKK },
    { vrt: "khac", vrn: KHAC }
]

const ITEM_VRT_EXC = [
    { vrt: "10%", vrn: "10%", vrnc: "10" },
    { vrt: "5%", vrn: "5%", vrnc: "5" },
    { vrt: "0%", vrn: "0%", vrnc: "0" },
    { vrt: "KCT", vrn: "-1", vrnc: "-1" },
    { vrt: "KKKNT", vrn: "-2", vrnc: "-2" }
]
// CloneTT32 - 20211010_1620
const MAP_HDon_TT32 = {
    "@": "@",
    "TTChung.PBan": "TTChung.PBan",
    "TTChung.THDon": "TTChung.THDon",
    "TTChung.KHMSHDon": "TTChung.KHMSHDon",
    "TTChung.KHHDon": "TTChung.KHHDon",
    "TTChung.SHDon": "TTChung.SHDon",
    // "TTChung.TDLap": "TTChung.TDLap",
    "TTChung.NLap": "TTChung.NLap",
    "TTChung.DVTTe": "TTChung.DVTTe",
    "TTChung.TGia": "TTChung.TGia",
    "TTChung.TTNCC": "TTChung.TTNCC",
    "TTChung.DDTCuu": "TTChung.DDTCuu",
    "TTChung.MTCuu": "TTChung.MTCuu",
    "TTChung.HTTToan": "TTChung.HTTToan",
    "TTChung.THTTTKhac": "TTChung.THTTTKhac",
    // "TTChung.HTHDBTThe": "TTChung.HTHDBTThe",
    // "TTChung.TDLHDBTThe": "TTChung.TDLHDBTThe",
    // "TTChung.KHMSHDBTThe": "TTChung.KHMSHDBTThe",
    // "TTChung.KHHDBTThe": "TTChung.KHHDBTThe",
    // "TTChung.SHDBTThe": "TTChung.SHDBTThe",
    // "TTChung.HTHDBDChinh": "TTChung.HTHDBDChinh",
    // "TTChung.TDLHDDChinh": "TTChung.TDLHDDChinh",
    // "TTChung.KHMSHDDChinh": "TTChung.KHMSHDDChinh",
    // "TTChung.KHHDBDChinh": "TTChung.KHHDBDChinh",
    // "TTChung.SHDBDChinh": "TTChung.SHDBDChinh",
    // "TTChung.TChat": "TTChung.TChat",
    "TTChung.TTHDLQuan": "TTChung.TTHDLQuan",
    "TTChung.TTKhac.TTin[]": "TTChung.TTKhac.TTin[]",

    "NDHDon.NBan": "NDHDon.NBan",
    "NDHDon.NMua": "NDHDon.NMua",
    "NDHDon.DSHHDVu": "NDHDon.DSHHDVu",

    "NDHDon.TToan.THTTLTSuat": "NDHDon.TToan.THTTLTSuat",
    "NDHDon.TToan.TCDChinh": "NDHDon.TToan.TCDChinh",
    "NDHDon.TToan.TgTCThue": "NDHDon.TToan.TgTCThue",
    "NDHDon.TToan.TgTThue": "NDHDon.TToan.TgTThue",
    "NDHDon.TToan.DSLPhi": "NDHDon.TToan.DSLPhi",
    "NDHDon.TToan.TCDCCKTMai": "NDHDon.TToan.TCDCCKTMai",
    "NDHDon.TToan.TTCKTMai": "NDHDon.TToan.TTCKTMai",
    "NDHDon.TToan.TCDCTTTToan": "NDHDon.TToan.TCDCTTTToan",
    "NDHDon.TToan.TgTTTBSo": "NDHDon.TToan.TgTTTBSo",
    "NDHDon.TToan.TgTTTBChu": "NDHDon.TToan.TgTTTBChu",
}
const MAP_HDR_TT32 = {
    "form": "TTChung.KHMSHDon",
    "serial": "TTChung.KHHDon",
    "seq": "TTChung.SHDon",
    "name": "TTChung.THDon",
    "curr": "TTChung.DVTTe",
    "exrt": "TTChung.TGia",
    "sname": "NDHDon.NBan.Ten",
    "stax": "NDHDon.NBan.MST",
    "cenumber": "NDHDon.NBan.HDKTSo",
    "cedate": "NDHDon.NBan.HDKTNgay",
    "ordno": "NDHDon.NBan.LDDNBo",
    "saddr": "NDHDon.NBan.DChi",
    "stel": "NDHDon.NBan.SDThoai",
    "smail": "NDHDon.NBan.DCTDTu",
    "sacc": "NDHDon.NBan.STKNHang",
    "sbank": "NDHDon.NBan.TNHang",
    "whsfr": "NDHDon.NBan.DCKXHang",
    "contr": "NDHDon.NBan.HDSo",
    "trans": "NDHDon.NBan.TNVChuyen",
    "vehic": "NDHDon.NBan.PTVChuyen",
    "bname": "NDHDon.NMua.Ten",
    "btax": "NDHDon.NMua.MST",
    "baddr": "NDHDon.NMua.DChi",
    "receiver": "NDHDon.NMua.HVTNNHang",
    "bcode": "NDHDon.NMua.MKHang",
    "btel": "NDHDon.NMua.SDThoai",
    // "bmail": "NDHDon.NMua.DCTDTu",
    "buyer": "NDHDon.NMua.HVTNMHang",
    "bacc": "NDHDon.NMua.STKNHang",
    "bbank": "NDHDon.NMua.TNHang",
    "sum": "NDHDon.TToan.TgTCThue",
    "vat": "NDHDon.TToan.TgTThue",
     "discount": "NDHDon.TToan.TTCKTMai",
    "total": "NDHDon.TToan.TgTTTBSo",
    "word": "NDHDon.TToan.TgTTTBChu"
}
const MAP_DTL_TT32 = {
    "type": "TChat",
    "status": "TCDChinh",
    "line": "STT",
    "code": "MHHDVu",
    "name": "THHDVu",
    "unit": "DVTinh",
    "quantity": "SLuong",
    "price": "DGia",
    "discountrate": "TLCKhau",
    "discountamt": "STCKhau",
    "amount": "ThTien",
    "vrn": "TSuat",
}
const MAP_TAX_TT32 = {
    status: "TCDChinh",
    vrt: "TSuat",
    amt: "ThTien",
    vat: "TThue",
}
let MAP_FEE_TT32 = {
    // status: "TCDChinh",
    name: "TLPhi",
    amount: "TPhi",
}
// End CloneTT32 - 20211010_1620
const TTIN = (id, val, type = "string") => { return { TTruong: id, DLieu: val, KDLieu: type } }
const mapvrt = (vrn, vrt = null) => {
    let rtVrt = vrn
    let convertVrt
    let objItemVrt = ITEM_VRT.find(obj => obj.vrn == rtVrt)
    if (!objItemVrt) objItemVrt = ITEM_VRT_EXC.find(obj => obj.vrnc == vrt) // In case that vrn specal word
    if (objItemVrt) convertVrt = JSON.parse(JSON.stringify(objItemVrt))
    else {
        convertVrt = {}
        convertVrt.vrt = "khac"
    }
    if (vrt != null && convertVrt.vrt == "khac") {
        convertVrt.vrt = `${convertVrt.vrt}:${vrt}%`
    }
    if (convertVrt) rtVrt = convertVrt.vrt
    return rtVrt
}
const swapKeyToValue = (obj) => {
    var resObj = {};
    for(var key in obj){
        resObj[obj[key]] = key;
    }
    return resObj;
}
const notEmptyStr = (str) => {
    let res_boolean = false
        , convertStr = String(str)
    if (convertStr.length > 0 && convertStr !== 'undefined') { res_boolean = true} 
    return res_boolean
}
const optx2j = {
    parseTrueNumberOnly: true,
    attrValueProcessor: (val, attrName) => he.decode(val, { isAttributeValue: true }),
    tagValueProcessor: (val, tagName) => he.decode(val),
}
const convertHTML = (str) => {
    // &colon;&rpar;
    var Regcheck = /\W\s/gi;
    var htmlListObj = {
        '&': "&amp;",
        //'"': "&quot;",
        //"'": "&apos;",
        //"<": "&lt;",
        //">": "&gt;"
    };

    var a = str.split("");
    var b = [];


    for (var i in a) {

        if (htmlListObj.hasOwnProperty(a[i])) {
            b.push(htmlListObj[a[i]]);
        } else { b.push(a[i]); }
    }

    var c = b.join("");

    return c;

}
const Service = {
    jsReportRender: async (req, res, next) => {//Hàm gọi JS 
        try {
            let data = JSON.stringify(req.body), JSREPORT_PASS = decrypt.decrypt(config_jsrep.JSREPORT_PASS)
            //Build lai template de chan loi bao mat lien quan tu client
            let doc = req.body.data, tmp = await Service.template(doc.tempfn), reqdata = { status: doc.status, doc: doc, tmp: tmp }
            data = ext.createRequest(reqdata)
            const token = `Basic ${Buffer.from(`${config_jsrep.JSREPORT_ACC}:${JSREPORT_PASS}`, 'utf8').toString('base64')}`
            let bodyBuffer
            if (config_jsrep.jsreportrender) {
                let jsreport = require('jsreport-client')(config_jsrep.JSREPORT_URL, config_jsrep.JSREPORT_ACC, JSREPORT_PASS)
                const json = data
                const res = await jsreport.render(json)
                bodyBuffer = await res.body()
            } else {
                data = JSON.stringify(data)
                let configj = {
                    method: 'post',
                    url: `${config_jsrep.JSREPORT_URL}`,
                    headers: {
                        'Authorization': token,
                        'Content-Type': 'application/json;charset=utf-8',
                        'Cookie': 'render-complete=true'
                    },
                    responseType: 'arraybuffer',
                    data: data
                };

                await axios(configj)
                    .then(function (response) {
                        bodyBuffer = response.data
                    })
                    .catch(function (response) {
                        try {
                            logger4app.error((response.response.data).toString())
                        } catch (err) {
                            logger4app.error((response.message).toString())
                        }


                        throw Error("Lỗi kết nối server JsReport \n (Error connecting to JsReport server)")
                    })
            }
            let sizeInByte = bodyBuffer.byteLength
            //res.contentType("application/pdf")    
            //res.end(bodyBuffer, "binary")

            res.json({ pdf: `data:application/pdf;base64, ${bodyBuffer.toString('base64')}`, sizePDF: sizeInByte })
        }
        catch (err) {
            logger4app.error(err)
            throw Error("Lỗi kết nối server JsReport \n (Error connecting to JsReport server)")
        }
        //return bodyBuffer
    },
    jsReportRenderAttach: async (json) => {//Hàm gọi JS 
        try {
            let data = JSON.stringify(json), JSREPORT_PASS = decrypt.decrypt(config_jsrep.JSREPORT_PASS)
            const token = `Basic ${Buffer.from(`${config_jsrep.JSREPORT_ACC}:${JSREPORT_PASS}`, 'utf8').toString('base64')}`
            let bodyBuffer
            if (config_jsrep.jsreportrender) {
                let jsreport = require('jsreport-client')(config_jsrep.JSREPORT_URL, config_jsrep.JSREPORT_ACC, JSREPORT_PASS)
                const res = await jsreport.render(json)
                bodyBuffer = await res.body()
            } else {
                let configj = {
                    method: 'post',
                    url: `${config_jsrep.JSREPORT_URL}`,
                    headers: {
                        'Authorization': token,
                        'Content-Type': 'application/json',
                        'Cookie': 'render-complete=true'
                    },
                    responseType: 'arraybuffer',
                    data: data
                };

                await axios(configj)
                    .then(function (response) {
                        bodyBuffer = response.data
                    })
                    .catch(function (response) {
                        logger4app.error((response.response.data).toString())
                        throw (response.response.data).toString()
                    })
            }
            return bodyBuffer
        }
        catch (err) {
            logger4app.error(err)
            throw Error("Lỗi kết nối server JsReport \n (Error connecting to JsReport server)")
        }
        //return bodyBuffer
    },
    jsReportRenderAttachPdfs: async (jsons) => {//Hàm gọi JS 
        try {
            let b64
            let PDFDocument = pdflib.PDFDocument
            // Create a new PDFDocument
            const pdfDoc = await PDFDocument.create()
            //Duyet danh sach doc de tạo pdf cho tung file
            for (const json of jsons) {
                //Tao file PDF hoa don
                let doc = json.doc, tmp = await Service.template(doc.tempfn), reqdata = { status: doc.status, doc: doc, tmp: tmp }
                let data = ext.createRequest(reqdata)
                const pdfbyte = await Service.jsReportRenderAttach(data)
                const pdfdoc = await PDFDocument.load(pdfbyte)
                //Lay so trang va add so trang can copy
                const cpages = pdfdoc.getPageCount()
                let pagearr = []
                for (let i = 0; i < cpages; i++) pagearr.push(i)
                //Copy vao 1 file
                const [pdfpage] = await pdfDoc.copyPages(pdfdoc, pagearr)
                pdfDoc.addPage(pdfpage)
            }

            const pdfBytes = await pdfDoc.save()
            const pdfBase64 = await pdfDoc.saveAsBase64({ dataUri: true })
            let sizeInByte = pdfBytes.byteLength

            return { pdf: pdfBase64, sizePDF: sizeInByte }
        }
        catch (err) {
            throw err
        }
        //return bodyBuffer
    },
    sortobj: (items, property, direction) => {

        function compare(a, b) {
            if (!a[property] && !b[property]) {
                return 0;
            } else if (a[property] && !b[property]) {
                return -1;
            } else if (!a[property] && b[property]) {
                return 1;
            } else {
                const value1 = a[property].toString().toUpperCase(); // ignore upper and lowercase
                const value2 = b[property].toString().toUpperCase(); // ignore upper and lowercase
                if (value1 < value2) {
                    return direction === 0 ? -1 : 1;
                } else if (value1 > value2) {
                    return direction === 0 ? 1 : -1;
                } else {
                    return 0;
                }

            }
        }

        return items.sort(compare);
    },
    encodehtml: (obj) => {
        let data = obj
        if (data) {
            for (var key in data) {
                data[key] = (data[key]) ? he.encode(data[key].toString()) : data[key];
            }
        }
        return data;
    },
    encodehtmls: (objs) => {
        let datas = objs
        for (let data of datas) {
            data = Service.encodehtml(data)
        }
        return datas;
    },
    encodevaluehtml: (val) => {
        let ret = val
        ret = (val) ? he.encode(val) : val;
        return ret;
    },
    getSubjectMail: async (json, subjectmail) => {
        let doc = JSON.parse(JSON.stringify(json))
        if (doc) {
            for (var key in doc) {
                if (["idt", "cdt"].includes(key)) doc[key] = moment(doc[key]).format(config.mfd)
            }
        }
        let vSubjectMail = `Hóa đơn điện tử số {{seq}} ký hiệu {{serial}} mẫu số {{form}} ngày {{idt}}`
        if (config.ent == "hsy") vSubjectMail = `Moet Hennessy Vietnam Distribution Shareholding Company – 0309932527 – einvoice number {{seq}} serial {{serial}} form {{form}} date {{idt}}`
        if (doc.status == 3 && config.apprSubjectMail) vSubjectMail = config.apprSubjectMail
        if (doc.status == 4 && config.canSubjectMail) vSubjectMail = config.canSubjectMail

        const template = handlebars.compile(vSubjectMail)
        const vSubject = template(doc)
        return vSubject
    },
    getSubjectMailPassword: async (json, subjectmailpassword) => {
        let doc = JSON.parse(JSON.stringify(json))
        if (doc) {
            for (var key in doc) {
                if (["idt", "cdt"].includes(key)) doc[key] = moment(doc[key]).format(config.mfd)
            }
        }
        let vSubjectMail = `Mật khẩu cho file {{filename}}`
        if (config.SubjectMailPassword) vSubjectMail = config.SubjectMailPassword

        const template = handlebars.compile(vSubjectMail)
        const vSubject = template(doc)
        return vSubject
    },
    getcapt: async (req, res, next) => {
        var captcha = svgCaptcha.createMathExpr({ mathMin: 1, mathMax: 9, mathOperator: "+", noise: 5, width: 80, height: 35, fontSize: 55 })
        let keynum = await redis.incr("CAPTCHA")
        let key = `CAPTCHA.${keynum}`
        await redis.set(key, captcha.text, 'EX', 30)
        res.status(200).json({ data: `<center>${captcha.data}</center>`, key: key })
    },
    verify: (req, res, next) => {
        try {
            let header = req.headers && req.headers.authorization, matches = header ? /^Bearer (\S+)$/.exec(header) : null, token = matches && matches[1]
            if (!token) return next(new Error("Hết phiên đăng nhập, đề nghị đăng nhập lại. \n(End of login session, please re-login)"))
            jwt.verify(token, seasecret, (err) => {
                if (err) return next(err)
                return next()
            })
        }
        catch (err) {
            next(err)
        }
    },
    createotp: () => {
        //let {totp} = require('otplib')
        try {
            //totp.options = config.otp
            var uuid = require('uuid');

            let vuuid = uuid.v4()
            const secret = vuuid

            let token
            token = totp.generate(secret);
            logger4app.debug(`OTP token:`, token);
            //logger4app.debug(secret);
            //logger4app.debug(totp.timeUsed());
            //logger4app.debug(totp.timeRemaining());
            return { token: secret, otp: token }
        }
        catch (err) {
            throw err
        }
    },
    verifyotp: (ptoken, psecret) => {
        //let { totp } = require('otplib')
        try {
            let token = ptoken, secret = psecret
            //totp.options = config.otp
            const isValid = totp.verify({ token, secret });
            logger4app.debug(isValid)

            return isValid
        }
        catch (err) {
            throw err
        }
    },
    checkdate: (str) => {
        const date = moment(str, dtf)
        return date.isValid()
    },
    fn2: num => {
        if (num == null || isNaN(num)) return null
        return numeral(num).format('0,0.[00]')
    },
    docxrender: (doc, fn) => {
        const file = fs.readFileSync(path.join(__dirname, "..", "..", `temp/${fn}`))
        const zip = new jszip(file)
        const docx = new docxtemplater()
        let docrender = convertHTML(JSON.stringify(doc))
        docx.loadZip(zip)
        docx.setData(JSON.parse(docrender))
        docx.render()
        const out = docx.getZip().generate({ type: "nodebuffer" })
        return out
    },
    checkemail: (str) => {
        const rows = str.trim().split(/[ ,;]+/).map(e => e.trim())
        for (const row of rows) {
            if (row && !(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(row))) return false
        }
        return true
    },
    checkmst: (mst) => {
        if (check_mst && check_mst == 0) return true
        if (!(/^[0-9]{10}$|^[0-9]{10}\-[0-9]{3}$/.test(mst))) return false
        const ms1 = parseInt(mst.substr(0, 1))
        const ms2 = parseInt(mst.substr(1, 1))
        const ms3 = parseInt(mst.substr(2, 1))
        const ms4 = parseInt(mst.substr(3, 1))
        const ms5 = parseInt(mst.substr(4, 1))
        const ms6 = parseInt(mst.substr(5, 1))
        const ms7 = parseInt(mst.substr(6, 1))
        const ms8 = parseInt(mst.substr(7, 1))
        const ms9 = parseInt(mst.substr(8, 1))
        const ms10 = parseInt(mst.substr(9, 1))
        const a = 31 * ms1 + 29 * ms2 + 23 * ms3 + 19 * ms4 + 17 * ms5 + 13 * ms6 + 7 * ms7 + 5 * ms8 + 3 * ms9
        if (ms10 != 10 - (a % 11)) return false
        else return true
    },
    fn: (form, idx) => {
        const type = form.substring(0, 6), i3 = parseInt(form.slice(-3))
        return `${type}.${i3}.${idx}`
    },
    etbis: (stt) => {
        // Get Email Type By Invoice Status
        let KEY = "TEMPMAIL."
        switch (parseInt(stt)) {
            case -2:
                KEY += "PASSWORD"
                break
            case -1:
                KEY += "NORMAL"
                break
            case 1:
                KEY += "USER"
                break
            case 2:
                KEY += "CUSTOMER"
                break
            case 4:
                KEY += "CANCEL"
                break
            case 5:
                KEY += "RESETUSERPASSWORD"
                break
            default:
                KEY += "NORMAL"
                break
        }
        return KEY
    },
    tempmail: async (etype) => {
        let KEY = Service.etbis(etype)
        let content = ""
        try {
            content = await redis.get(KEY)
        } catch (err) {
            console.log(err)
        }

        if (!content || content == "") {
            let typemail
            if (etype == -2) typemail = "temp/passmail"
            else if (etype == 4) typemail = "temp/canmail"
            else if (etype == 1) typemail = "temp/usermail"
            else if (etype == 2) typemail = "temp/cusmail"
            else if (etype == 5) typemail = "temp/resetusermail"
            else typemail = "temp/mail"

            let FILE
            if (pathtemplate) {
                FILE = path.join(pathtemplate, (typemail.split("/"))[1])
            } else
                FILE = path.join(__dirname, "..", "..", typemail)
            try {
                content = fs.readFileSync(FILE, "utf8")
            } catch (err) {
                content = ""
            }
            try {
                await redis.set(KEY, content)
            } catch (err) {

            }
        }
        return content
    },
    template: async (fn) => {
        let tmp = ""
        try {
            tmp = await redis.get(`TMP.${fn}`)
        } catch (err) {
            console.log(err, "err")
        }
        if (tmp && tmp != "") tmp = JSON.parse(tmp)
        else {
            let file
            if (pathtemplate) {
                file = path.join(pathtemplate, `${fn}`)
            } else
                file = path.join(__dirname, "..", "..", `temp/${fn}`)
            if (!fs.existsSync(file)) file = path.join(__dirname, "..", "..", "temp/inv")
            const content = fs.readFileSync(file, "utf8")
            const $ = cheerio.load(content, { decodeEntities: false })
            const sty = $('style').html(), inv = $('#inv').html(), bgi = $('#bgi').html()
            //, hdr=$('#hdr').html(), dtl=$('#dtl').html()
            //tmp = { sty: sty, inv: inv, bgi: bgi, hdr: hdr, dtl: dtl }
            tmp = { sty: sty, inv: inv, bgi: bgi }
            await redis.set(`TMP.${fn}`, JSON.stringify(tmp))
        }
        for (const key in tmp) {
            if (tmp[key]) if (tmp[key]) tmp[key] = decode(tmp[key])
        }
        return tmp
    },
    chkcaptoff: async (token) => {
        const key = token.key, answer = token.answer, result = await redis.get(key)
        return (result === answer)
    },
    decrypt256: text => {
        text = Buffer.from(text, 'hex')
        const iv = text.slice(0, 16);
        // Get the rest
        text = text.slice(16);
        // Create a decipher
        const decipher = crypto.createDecipheriv('aes-256-cbc', keydds, iv);
        // Actually decrypt it
        const result = Buffer.concat([decipher.update(text), decipher.final()]);
        logger4app.debug(result.toString());
        return result.toString()

    },

    decryptDS: configpool => {
        try {
            configpool.user = Service.decrypt256(configpool.user)
            configpool.password = Service.decrypt256(configpool.password)
            configpool.database = Service.decrypt256(configpool.database)
            configpool.server = Service.decrypt256(configpool.server)
        } catch (error) {

        }

        return configpool
    },
    tenhd: (type) => {
        let result = config.ITYPE.find(item => item.id === type)
        return result.value.toUpperCase()
    },
    bcryptpwd: (pwd) => {
        return bcrypt.hashSync(pwd, 8)
    },
    pwd: () => {
        return crypto.randomBytes(4).toString("hex")
    },
    queue: () => {
        const rsmq = require("./rsmq")
        rsmq.deleteQueueAsync({ qname: config.qname, maxsize: 10868 }).then(resp => {
            if (resp === 1) logger4app.debug(config.qname + " delete")
            rsmq.createQueueAsync({ qname: config.qname, maxsize: -1 }).then(resp => {
                if (resp === 1) logger4app.debug(config.qname + " created")
            })
        })
    },
    formatNumData: (json) => {
        let {
            dif = {},
        } = json
        const round2Decimals = (num) => {
            num = Math.round(parseFloat(num) * 100) / 100
            return num
        }
        const round4Decimals = (num) => {
            num = Math.round(parseFloat(num) * 10000) / 10000
            return num
        }
        const roundObj = (obj) => {
            let {
                fees: feesInv = [],
                tax: taxsInv = [],
                items: itemsInv = [],
                exrt: exrtInv = 0,
                amount: amountInv = 0,
                sum: sumInv = 0,
                total: totalInv = 0,
                vat: vatInv = 0,
                tradeamount: tradeamountInv = 0,
            } = obj
            if (exrtInv) json.exrt = round2Decimals(exrtInv)
            if (amountInv) json.amount = round4Decimals(amountInv)
            if (sumInv) json.sum = round4Decimals(sumInv)
            if (totalInv) json.total = round4Decimals(totalInv)
            if (vatInv) json.vat = round4Decimals(vatInv)
            if (tradeamountInv) json.tradeamount = round4Decimals(tradeamountInv)
            for (let iItem = 0; iItem < itemsInv.length; iItem++) {
                let objItem = itemsInv[iItem]
                let {
                    quantity: qtyItem = 0,
                    price: priceItem = 0,
                    amount: amtItem = 0,
                    total: totalItem = 0,
                    perdiscount: perDcItem = 0,
                    amtdiscount: amtDcItem = 0,
                } = objItem
                if (qtyItem) objItem.quantity = round4Decimals(qtyItem)
                if (priceItem) objItem.price = round4Decimals(priceItem)
                if (amtItem) objItem.amount = round4Decimals(amtItem)
                if (totalItem) objItem.total = round4Decimals(totalItem)
                if (perDcItem) objItem.perdiscount = round4Decimals(perDcItem)
                if (amtDcItem) objItem.amtdiscount = round4Decimals(amtDcItem)
            }
            for (let iTax = 0; iTax < taxsInv.length; iTax++) {
                let objTax = taxsInv[iTax]
                let {
                    amt: amtTax = 0,
                    vat: vatTax = 0,
                } = objTax
                if (amtTax) objTax.amt = round4Decimals(amtTax)
                if (vatTax) objTax.vat = round4Decimals(vatTax)
            }
            for (let iFee = 0; iFee < feesInv.length; iFee++) {
                let objFee = feesInv[iFee]
                let {
                    amount: amtFee = 0,
                } = objFee
                if (amtFee) objFee.amount = round4Decimals(amtFee)
            }
            return obj
        }
        dif = roundObj(dif)
        json = roundObj(json)
        return json
    },
    prettyJson: (json) => {
        // Object.keys(json).forEach(key => {
        for (const key in json) {
            let value = json[key]
            if (value === '' || value === 'undefined' || value === null) {
                delete json[key]
                continue
            }
            if (Array.isArray(value)) json[key] = Service.prettyArray(value)
            if (typeof value === 'object') json[key] = Service.prettyJson(value)
        }
        return json
    },
    prettyArray: (array) => {
        for (let i = 0; i < array.length; i++) {
            let value = array[i]
            if (value === '' || value === 'undefined' || value === null) {
                array.splice(i, 1) // Delete item array
                continue
            }
            if (Array.isArray(value)) array[i] = Service.prettyArray(value)
            if (typeof value === 'object') array[i] = Service.prettyJson(value)
        }
        return array
    },
    i2x: (jsonReal, inc, b) => {
        let json = JSON.parse(JSON.stringify(jsonReal))
        let REP_MAP_HDR = { ...MAP_HDR_TT32 }
        try { delete REP_MAP_HDR['bmail'] } catch (e) { }
        json.name = Service.tenhd(json.type)
        json = Service.prettyJson(json)
        if (json.curr !== "VND") json = Service.formatNumData(json)
        const {
            type = '',
            uc = '',
            adj = {},
            dif = {},
            idt = '',
            fees = [],
            sec = '',
            paym = '',
            curr = '',
            tradeamount = '',
            tradeamountv = '',
            // api: appInv = undefined
        } = json
        let {
            tax = [],
            items = [],
            ud = '',
            tradestatus = '',
            // statusRoot = undefined,
        } = json
        let isAdjInv = false
        const taxOriginal = uc.split(".")[0]
        let arrSame01GTKT = ["01GTKT", "01/TVE"]
        let arrSame02GTTT = ["02GTTT", "07KPTQ"]
        let arrType0304 = ["03XKNB", "04HGDL"]
        let arrTypeNotTaxs = [...arrSame02GTTT, "01BLP", "02BLP", "02/TVE"]
        let paymSample = PAYM_SAMPLE
        let configFieldInv = [] //await redis.get(`COL.${taxOriginal}.${type}`)
        //configFieldInv = JSON.parse(configFieldInv) || []
        //const col = require("../api/col")
        //if(configFieldInv.length == 0) configFieldInv = await col.cache(taxOriginal, type)
        if (json.configfield && json.configfield.configfieldhdr && json.configfield.configfieldhdr.length > 0) {
            configFieldInv = json.configfield.configfieldhdr
        }
        if (ud !== undefined && ud !== null && ud.toString() == "0") ud = 2
        if (tradestatus !== undefined && tradestatus !== null && tradestatus.toString() == "0") tradestatus = 2
        if (curr === "VND") {
            REP_MAP_HDR["sumv"] = REP_MAP_HDR["sum"]
            REP_MAP_HDR["vatv"] = REP_MAP_HDR["vat"]
            REP_MAP_HDR["totalv"] = REP_MAP_HDR["total"]
            delete REP_MAP_HDR["sum"]
            delete REP_MAP_HDR["vat"]
            delete REP_MAP_HDR["total"]
        }
        if (arrType0304.includes(type)) {
            json.saddr = json.whsfr
            json.baddr = json.whsto
            json.bname = json.recvr
            if (json.smail !== undefined) delete json.smail
            if (json.bmail !== undefined) delete json.bmail
            if (json.sacc !== undefined) delete json.sacc
            if (json.whsfr !== undefined) delete json.whsfr
            if (json.sbank !== undefined) delete json.sbank
            if (json.bbank !== undefined) delete json.bbank
        }
        if (!arrSame01GTKT.includes(type)) {
            if (json.tax !== undefined) delete json.tax
            if (json.vat !== undefined) delete json.vat
            if (json.vatv !== undefined) delete json.vatv
        }
        if (arrTypeNotTaxs.includes(type)) {
            if (REP_MAP_HDR["sum"] !== undefined) delete REP_MAP_HDR["sum"]
            if (REP_MAP_HDR["sumv"] !== undefined) delete REP_MAP_HDR["sumv"]
            if (dif.sum !== undefined) delete dif.sum
            if (dif.sumv !== undefined) delete dif.sumv
        }
        if (type == "04HGDL") {
            // Check 'trans' before 'contr'
            let objToArrKeys = Object.keys(REP_MAP_HDR)
            if (objToArrKeys.indexOf("contr") < objToArrKeys.indexOf("trans")) {
                // swap contr and trans in REP_MAP_HDR obj
                let tempContr = REP_MAP_HDR["contr"]
                let tempTrans = REP_MAP_HDR["trans"]
                REP_MAP_HDR = swapKeyToValue(REP_MAP_HDR)
                REP_MAP_HDR[tempContr] = "trans"
                REP_MAP_HDR[tempTrans] = "contr"
                REP_MAP_HDR = swapKeyToValue(REP_MAP_HDR)
                REP_MAP_HDR["trans"] = tempTrans
                REP_MAP_HDR["contr"] = tempContr
            }
            if (json.ordno !== undefined) delete json.ordno
        }
        if (curr !== "VND") {
            try { json.word = n2w_jsreport.d2w(json.total, curr) } catch (e) { }
        }
        if (json.cedate) json.cedate = moment(json.cedate).format("YYYY-MM-DD")
        let obj = objectMapper(json, REP_MAP_HDR), TChatHD = 1
        if (json.status == 4) TChatHD = 7
        obj.TTChung.PBan = "1.1.0"
        // obj.TTChung.TDLap = idt.replace(" ", "T")
        obj.TTChung.NLap = idt.split(" ")[0]
        obj.TTChung.TTNCC = config.ttncc
        obj.TTChung.DDTCuu = config.ddtcuu
        if (sec != '') obj.TTChung.MTCuu = sec
        if (paym != '' && Array.isArray(paymSample) && paymSample.some(obj => obj.id == paym)) {
            obj.TTChung.HTTToan = null
            if (!PAYM_SAMPLE.some(obj => { return obj.id == paym })) {
                obj.TTChung.THTTTKhac = paym
                obj.TTChung.HTTToan = "9"
            }
            if (!obj.TTChung.HTTToan) obj.TTChung.HTTToan = PAYM_SAMPLE.find(obj => obj.id == paym).value
        }
        let ttkTTChung = []
        for (let i = 0; i < 10; i++) {
            let keyJson = `c${i}`
            let valJson = json[keyJson]
            if (String(valJson) != 'undefined' && String(valJson) != 'null') {
                let objRemakeKey = configFieldInv.find((obj) => obj.id == keyJson) || null
                if (objRemakeKey != null) keyJson = objRemakeKey.label || keyJson // || objRemakeKey.placeholder
                ttkTTChung.push(TTIN(`${keyJson} (c${i})`, valJson))
            }
        }
        if (json.note) ttkTTChung.push(TTIN("note", json.note))
        if (ttkTTChung.length) obj.TTChung.TTKhac = { TTin: ttkTTChung } //{ TTin: [TTIN("sec", json.sec)] }
        if (adj && Object.keys(adj).length !== 0 && adj.constructor === Object) {
            let TTHDLQuan = new Object()
            if (ud && ud.toString() == "0") ud = 2
            if (adj.typ == 1) { // Replacement invoice
                // TChatHD = 3
                // obj.TTChung.HTHDBTThe = 1
                // obj.TTChung.TDLHDBTThe = adj.idt.replace(" ", "T")
                // obj.TTChung.KHMSHDBTThe = adj.form
                // obj.TTChung.KHHDBTThe = adj.serial
                // obj.TTChung.SHDBTThe = adj.seq
                if((adj.form) && (adj.form).length >1) TTHDLQuan.LHDCLQuan = 3
                else TTHDLQuan.LHDCLQuan = 1
                TTHDLQuan.TCHDon = 1
                // TTHDLQuan.LHDCLQuan = 1
                const arrseq = adj.seq.split("-")
                TTHDLQuan.LHDCLQuan = ''
                TTHDLQuan.KHMSHDCLQuan = arrseq[0] //adj.form
                TTHDLQuan.KHHDCLQuan = arrseq[1] //adj.serial
                TTHDLQuan.SHDCLQuan = arrseq[2] //adj.seq
            } else if (adj.typ == 2) { // Adjustment invoice
                isAdjInv = true
                if (curr !== "VND") try { dif.word = n2w_jsreport.d2w(Math.abs(dif.total), curr) } catch (e) { }
                // TChatHD = 5
                // obj.TTChung.HTHDBDChinh = 2
                // obj.TTChung.TDLHDDChinh = adj.idt.replace(" ", "T")
                // obj.TTChung.KHMSHDDChinh = adj.form
                // obj.TTChung.KHHDBDChinh = adj.serial
                // obj.TTChung.SHDBDChinh = adj.seq
                TTHDLQuan.TCHDon = 2
                // TTHDLQuan.LHDCLQuan = 2
                if((adj.form) && (adj.form).length >1) TTHDLQuan.LHDCLQuan = 3
                else TTHDLQuan.LHDCLQuan = 1
                const arrseq = adj.seq.split("-")
                TTHDLQuan.KHMSHDCLQuan = arrseq[0] //adj.form
                TTHDLQuan.KHHDCLQuan = arrseq[1] //adj.serial
                TTHDLQuan.SHDCLQuan = arrseq[2] //adj.seq
                obj.NDHDon.TToan.TCDChinh = ud
                // obj.NDHDon.TToan.TCDChinh = (appInv !== undefined && statusRoot !== undefined) ? statusRoot : ud
                // if(appInv !== undefined && statusRoot === undefined) delete obj.NDHDon.TToan.TCDChinh
                obj.NDHDon.TToan.TCDCTTTToan = ud
                if (tradestatus != '') obj.NDHDon.TToan.TCDCCKTMai = tradestatus
                if (dif.vat && dif.vat !== undefined && dif.vat !== '') obj.NDHDon.TToan.TgTThue = Math.abs(dif.vat)
                if (dif.total) obj.NDHDon.TToan.TgTTTBSo = Math.abs(dif.total)
                if (dif.word) obj.NDHDon.TToan.TgTTTBChu = dif.word
                if (dif.tradeamount !== undefined && dif.tradeamount !== null) obj.NDHDon.TToan.TTCKTMai = Math.abs(dif.tradeamount)
                if (dif.sum !== undefined && dif.sum !== '') obj.NDHDon.TToan.TgTCThue = Math.abs(dif.sum)
                if (curr === "VND") {
                    if (dif.vatv && dif.vatv !== undefined && dif.vatv !== '') obj.NDHDon.TToan.TgTThue = Math.abs(dif.vatv)
                    if (dif.totalv) obj.NDHDon.TToan.TgTTTBSo = Math.abs(dif.totalv)
                    if (dif.sumv !== undefined && dif.sumv !== '') obj.NDHDon.TToan.TgTCThue = Math.abs(dif.sumv)
                    if (dif.tradeamountv !== undefined && dif.tradeamountv !== null) obj.NDHDon.TToan.TTCKTMai = Math.abs(dif.tradeamountv)
                }

                //Không hiêu sao điều chỉnh thông tin mà lại check thẻ abc nên đành tạo dữ liệu đểu vậy
                if (!obj.NDHDon.TToan.TgTTTBSo || !Service.isNumber(obj.NDHDon.TToan.TgTTTBSo)) obj.NDHDon.TToan.TgTTTBSo = 0
                if (!obj.NDHDon.TToan.TgTTTBChu) obj.NDHDon.TToan.TgTTTBChu = ''

                MAP_FEE = {
                    status: "TCDChinh",
                    ...MAP_FEE
                }
              //  items = (dif && Object.keys(dif).length !== 0 && dif.constructor === Object && Array.isArray(dif.items)) ? dif.items : items
              //  tax = (dif && Object.keys(dif).length !== 0 && dif.constructor === Object && Array.isArray(dif.tax)) ? dif.tax : tax
            }
            TTHDLQuan.GChu = `Theo số biên bản: ${adj.ref} ngày ${moment(adj.rdt).format(config.mfd)}`
            //TTHDLQuan.LHDCLQuan = 3
            obj.TTChung.TTHDLQuan = TTHDLQuan
        }
        if (arrTypeNotTaxs.includes(type)) {
            try { delete obj.NDHDon.TToan.TCDChinh } catch (e) { }
        }
        if ((obj.NDHDon.TToan.TTCKTMai === undefined || dif.tradeamount === null || dif.tradeamount === undefined) && !isAdjInv) obj.NDHDon.TToan.TTCKTMai = (curr === "VND" && tradeamountv != '') ? tradeamountv : tradeamount
        // obj.TTChung.TChat = TChatHD
        // Duy sửa để tính đủ loại thúe suất
        /* 
        if (tax) {
            for (const row of tax) {
                row.vrt = mapvrt(row.vrt)
            }
            const LTSuat = tax.map(row => objectMapper(row, MAP_TAX))
            obj.NDHDon.TToan.THTTLTSuat = { LTSuat }
        }
        */
        let taxs = JSON.parse(JSON.stringify(tax))
        // if (adj && adj.typ == 2) {
        //     if (json.items && json.items.length) taxs = Service.taxListExist(Service.getDifOfItems(json.pay.items, json.items), json.exrt)
        //     else taxs = [{ amtv: "", vatv: "", vrt: "" }]
        // } else {
        //     // taxListExist là tính tax ko dựa vào amount =0 mà cứ xuất hiện là tính
        //     taxs = Service.taxListExist(json.items, json.exrt)
        // }
        if (Array.isArray(taxs) && !arrTypeNotTaxs.includes(type)) {
            let REP_MAP_TAX = { ...MAP_TAX_TT32 }

            //Không hiêu sao điều chỉnh thông tin mà lại check thẻ tax nên đành tạo dữ liệu đểu vậy
            let taxstemp = [{ amtv: "", vatv: "", vrt: "" }]
            if (!taxs || taxs.length <= 0) taxs = taxstemp

            for (const row of taxs) {
                if (`${row.vrt + ""}`.length) row.vrt = mapvrt(row.vrn, row.vrt).toUpperCase()
                if (row.status !== undefined && row.status !== null && row.status.toString() == "0") row.status = 2
                row.amt = (Service.isNumeric(row.amt)) ? Math.abs(row.amt) : 0
                row.vat = (Service.isNumeric(row.vat)) ? Math.abs(row.vat) : 0
                if (curr === "VND") {
                    row.amtv = (Service.isNumeric(row.amtv)) ? Math.abs(row.amtv) : 0
                    row.vatv = (Service.isNumeric(row.vatv)) ? Math.abs(row.vatv) : 0
                }
            }
            if (curr === "VND") {
                REP_MAP_TAX["amtv"] = REP_MAP_TAX["amt"]
                REP_MAP_TAX["vatv"] = REP_MAP_TAX["vat"]
                delete REP_MAP_TAX["amt"]
                delete REP_MAP_TAX["vat"]
            }
            const LTSuat = taxs.map(row => objectMapper(row, REP_MAP_TAX))
            obj.NDHDon.TToan.THTTLTSuat = { LTSuat }
        }
        // Duy sửa để tính đủ loại thúe suất - END

        if (items) {

            //Không hiêu sao điều chỉnh thông tin mà lại check thẻ items nên đành tạo dữ liệu đểu vậy
            let itemstmp = {
                //"type": "TChat",
                //"status": "TCDChinh",
                "line": "1",
                //"code": "MHHDVu",
                "name": "",
                //"unit": "DVTinh",
                "quantity": 0,
                "price": 0,
                //"perdiscount": "TLCKhau",
                //"amtdiscount": "STCKhau",
                "amount": 0,
                "vrt": "0",
                "vrn": "0%"
            }
            if (items.length <= 0) items = [itemstmp]

            for (let row of items) {
                row.type = row.type ? row.type.toString().toUpperCase() : ""
                switch (row.type) {
                    case "KM":
                        row.type = 2
                        break
                    case "CK":
                        row.type = 3
                        break
                    case "MT":
                        row.type = 4
                        break
                    case "MÔ TẢ":
                        row.type = 4
                        break
                    case "KHUYẾN MÃI":
                        row.type = 2
                        break
                    case "CHIẾT KHẤU":
                        row.type = 3
                        break
                    default:
                        row.type = 1
                        break
                }

                if (row.status !== undefined && row.status !== null && row.status.toString() == "0") row.status = 2
                if (row.hasOwnProperty("vrn")) row.vrn = mapvrt(row.vrn, row.vrt).toUpperCase()
                if (row.vrn == "KHAC" && row.hasOwnProperty("vrt")) row.vrn = `${row.vrn}:${row.vrt}`
                if (arrType0304.includes(type)) {
                    if (json.perdiscount !== undefined) delete json.perdiscount
                    if (json.amtdiscount !== undefined) delete json.amtdiscount
                }
            }
            const HHDVu = items.map(row => objectMapper(row, MAP_DTL_TT32))
            let rowsDtl = []
            //sqlDtl = "select id,idx,label,type,status,itype,xc,atr from invoice.s_dtl where taxc=? and itype=? order by idx"
            //resultDtl = await dbs.query(sqlDtl, [taxOriginal, type])
            if (json.configfield && json.configfield.configfielddtl && json.configfield.configfielddtl.length > 0) {
                rowsDtl = json.configfield.configfielddtl
            }
            //rowsDtl = resultDtl[0]
            let catobj = JSON.parse(JSON.stringify(config.CATTAX))
            for (let iItem = 0; iItem < items.length; iItem++) {
                let ttkItem = []
                let item = items[iItem]
                for (let i = 0; i < 10; i++) {
                    let keyItem = `c${i}`
                    let valItem = item[keyItem]
                    let objRemakeKey = rowsDtl.find((obj) => `${obj.id}` == keyItem) || null
                    if (objRemakeKey != null) keyItem = objRemakeKey.header.text || keyItem
                    if (String(valItem) != 'undefined' && String(valItem) != 'null') ttkItem.push(TTIN(`${keyItem} (c${i})`, valItem))
                    //if(valItem || valItem == '') ttkItem.push(TTIN(`${keyItem} (c${i})`, valItem))
                }
                let objtax = catobj.find(x => x.vatCode == item.vatCode)
                if (objtax) {
                    if (objtax.vrt != objtax.original_vrt) {
                        ttkItem.push(TTIN(`${objtax.vrngdtlbl}`, objtax.vrngdt))
                    }
                }
                if (item.note) ttkItem.push(TTIN("note", item.note))
                if (ttkItem.length) HHDVu[iItem].TTKhac = { TTin: ttkItem }
            }
            obj.NDHDon.DSHHDVu = { HHDVu }
        }
        let ttk = []
        if (curr !== "VND") {
            ttk.push(TTIN("sum", json.sum, "numeric"))
            ttk.push(TTIN("total", json.total, "numeric"))
            if (json.discount) ttk.push(TTIN("discount", json.discount, "numeric"))
            if (json.vat) ttk.push(TTIN("vat", json.vat, "numeric"))
            if (json.sc) ttk.push(TTIN("sc", json.sc, "numeric"))
        }
        if (ttk.length) obj.NDHDon.TToan.TTKhac = { TTin: ttk }
        // if (json.scv) obj.NDHDon.TToan.DSLPhi = { LPhi: { TLPhi: "Phí dịch vụ", TPhi: json.scv } }
        if (fees && fees.length > 0) {
            for (let iFee = 0; iFee < fees.length; iFee++) {
                const row = fees[iFee]
                if (row.status && row.status.toString() == "0") row.status = 2
            }
            const LPhi = fees.map(row => objectMapper(row, MAP_FEE_TT32))
            obj.NDHDon.TToan.DSLPhi = { LPhi }
        }
        if (arrType0304.includes(type)) { /* type == "03XKNB" || type == "04HGDL" */
            if (obj.NDHDon && obj.NDHDon.TToan) delete obj.NDHDon.TToan
        }
        let dlhdon = { TTChung: obj.TTChung, NDHDon: obj.NDHDon }
        if (b) dlhdon["@"] = { Id: `_${inc}` }
        dlhdon = objectMapper(dlhdon, MAP_HDon_TT32)
        const xml = js2xmlparser.parse("HDon", { DLHDon: dlhdon }, optj2x)
        return xml
    },
    signature: (xml, idt) => {
        const result = xml.replace("<Signature", "<DSCKS><NBan><Signature").replace("</Signature>", `<Object><SignatureProperties><SignatureProperty Id="TimeStamp" Target=""><SigningTime>${idt.replace(" ", "T")}</SigningTime></SignatureProperty></SignatureProperties></Object></Signature></NBan></DSCKS>`)
        return result
    },
    j2xTT32: (json, id, hasId) => {
        const xml = Service.i2x(json, id, true)
        /*
        if (hasId) return `<HDon><DLHDon Id="_${id}">${xml}</DLHDon></HDon>`
        else return `<HDon><DLHDon>${xml}</DLHDon></HDon>`
        */
        return xml
    },
    x2j: (xml) => {
        return fxp.parse(xml)
    },
    x2jb: (xml) => {
        try {
            return fxp.parse(xml, optx2j, true)
        } catch (e) {
            throw e
        }
    },
    encrypt: str => {
        return cryptr.encrypt(str)
    },
    decrypt: str => {
        return cryptr.decrypt(str)
    },
    reCaptcha: (token, ip) => {
        return new Promise((resolve, reject) => {
            try {
                axios.defaults.headers.common["Authorization"] = 'Authorization: Basic Og=='
                axios.get(`${config.recaptchaUrl}?secret=${config.recaptchaSecret}&response=${token}&remoteip=${ip}`).then(res => resolve(res.data)).catch(err => reject(err))
            }
            catch (err) {
                reject(err)
            }
        })
    },
    chkCaptcha: async (req) => {
        if (captchaType == 0) {
        } else if (captchaType == 1) {
            const data = await Service.chkcaptoff(JSON.parse(req.query.token))
            if (!data) throw new Error("Nhập sai mã Captcha.\n(Enter the wrong Captcha code.)")
        } else if (captchaType == 2) {
            const query = req.query, data = await Service.reCaptcha(query.token, req.connection.remoteAddress)
            if (!data.success || data.score < config.recaptchaScore || data.action !== query.action) throw new Error(`Lỗi Google reCAPTCHA không hợp lệ : ${JSON.stringify(data)} \n(Error Google reCAPTCHA is not valid : ${JSON.stringify(data)})`)
        } else {
            throw new Error("Không thể nhận dạng Captcha.\n(Captcha cannot be identified.)")
        }
    },
    findDFS: (arr, pid) => {
        for (let obj of arr || []) {
            if (obj.id == pid) return obj
            const o = Service.findDFS(obj.data, pid)
            if (o) return o
        }
    },
    toDate: (str) => {
        const arr = str.split("/")
        return new Date(arr[2], arr[1] - 1, arr[0])
    },
    toTimestamp: (str) => {
        const arr = str.split("/")
        return new Date(arr[2], arr[1] - 1, arr[0], 0, 0, 0)
    },
    isObject: (val) => {
        if (val === null) return false
        return ((typeof val === "function") || (typeof val === "object"))
    },
    contains: (obj, arr) => {
        for (let i = 0; i < arr.length; i++) {
            if (arr[i] === obj) return true
        }
        return false
    },
    isNumeric: (n) => {
        return !isNaN(parseFloat(n)) && isFinite(n)
    },
    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms))
    },
    isNumber(val) {
        return ("number" == typeof val)
    },
    isEmpty(val) {
        if (val == null) return true
        if ("string" == typeof val) return val.trim().length === 0
        if ("number" == typeof val) return String(val).trim() === 0
        if (Array.isArray(val)) return val.length === 0
        if (Object.keys(val).length === 0 && val.constructor === Object) return true
        return false
    },
    generateSecCodeVCM() {
        try {
            //get byte
            let bytes = [], i, secret_number = 4, isix = 6
            const randomPool = "abcdefghijklmnopqrstuvwyxzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

            let chars = [], rpool = randomPool.split('')
            //logger4app.debug(rpool)
            for (i = 0; i < isix; i++) {
                chars.push(rpool[Math.floor(Math.random() * Math.floor(rpool.length))])
            }

            let randomNo = chars.join('')
            //logger4app.debug(randomNo)

            for (i = 0; i < randomNo.length; ++i) {
                bytes.push(randomNo.charCodeAt(i));
            }

            let base64str = Buffer.from(bytes).toString('base64').toString('utf8')
            base64str = base64str.replace("I", "&").replace("l", "@").replace("1", "$").replace("0", "%").replace("j", ">").replace("O", "<")
            base64str = base64str.substring(secret_number, secret_number + base64str.length - secret_number).concat(base64str.substring(0, secret_number));
            //logger4app.debug(base64str)
            return base64str
        }
        catch (err) {
            logger4app.debug(err)
            return ``
        }
    },
    generateSecCodeFPT(id) {
        try {
            const hashids = new Hashids(config.secret, 10, "abcdefghijklmnopqrstuvwxyz1234567890")
            return hashids.encode(id)
        }
        catch (err) {
            logger4app.debug(err)
            return ``
        }
    },
    generateSecCode(pent, id) {
        try {
            if (pent == `fpt`) {
                return Service.generateSecCodeFPT(id)
            } else {
                return eval(pent) //Service.generateSecCodeVCM()
            }

        }
        catch (err) {
            logger4app.debug(err)
            return ``
        }
    },
    //NĐ 123
    viewadj: (doc) => {
        const dif = doc.dif
        if (dif) {
           // doc.items = dif.items
            doc.word = dif.word
            // if (dif.hasOwnProperty("tax")) doc.tax = dif.tax
            doc.sum = Math.abs(dif.sum)
            doc.sumv = Math.abs(dif.sumv)
            doc.vat = Math.abs(dif.vat)
            doc.vatv = Math.abs(dif.vatv)
            doc.total = Math.abs(dif.total)
            doc.totalv = Math.abs(dif.totalv)
            doc.tradeamount = Math.abs(dif.tradeamount)
        }
    },
    convertDateAsMoment: (date) => {
        return moment(date).format("YYYY-MM-DD")
    },
    j2x: async (jsonReal, inc, b = false) => {
        // let json = JSON.parse(JSON.stringify(jsonPattern)) //Temp
        let json = JSON.parse(JSON.stringify(jsonReal))
            , xml
        json = Service.prettyJson(json)
        const {
            type = ''
        } = json
        switch (type) {
            case _01DKTD_HDDT:
                xml = mapping_reg_declare_xml(json, inc, b)
                break
            case _04SS_HDDT:
                xml = mapping_erroneous_notice_xml(json, inc, b)
                break
            case "01/TH-HĐĐT":
                xml = mapping_list_inv_xml(json, inc, b)
                break
            default:
                xml = await mapping_inv_xml(json, inc, b)
               
                break
        }
        return xml
    },
    getPathXml: (type) => {
        let pathXml = config.PATH_XML
        switch (type) {
            case _01DKTD_HDDT:
                pathXml = pathXml.replace('DLHDon', 'DLTKhai')
                break
            case _04SS_HDDT:
                pathXml = pathXml.replace('DLHDon', 'DLTBao')
                break
            case _01TH_HDDT:
                pathXml = pathXml.replace('DLHDon', 'DLBTHop')
                break
        }
        return pathXml
    },
    getSignSample: (type, idt, inc) => {
        let signdt = idt || new Date()
        let tagBegin = 'NBan'
            , tagEnd = '/NBan'
            , xmlEndTagReplace = '</HDon>'
        switch (type) {
            case _01DKTD_HDDT:
            case _04SS_HDDT:
            case _01TH_HDDT:
                    tagBegin = 'NNT'
                    , tagEnd = '/NNT'
                break
        }
        switch (type) {
            case _01DKTD_HDDT:
                xmlEndTagReplace = '</TKhai>'
                break
            case _04SS_HDDT:
                xmlEndTagReplace = '</TBao>'
                break
            case _01TH_HDDT:
                xmlEndTagReplace = '</BTHDLieu>'
                break
        }
        let objXmlInSign = `<Object Id="SigningTime-${tagBegin}-${inc}"><SignatureProperties xmlns=""><SignatureProperty Id="SignatureProperty-${tagBegin}-${inc}" Target="#${tagBegin}-${inc}"><SigningTime>${/*idt*/moment(signdt).format(config.dtf).replace(" ", "T")}</SigningTime></SignatureProperty></SignatureProperties></Object>`
        let objSignSample = {
            tagBegin,
            tagEnd,
            xmlEndTagReplace,
            strReplaceTagSignBegin: `<DSCKS><${tagBegin}><Signature Id="${tagBegin}-${inc}"`,
            strReplaceTagSignEnd: `<DSCKS><${tagBegin}><Signature Id="${tagBegin}-${inc}" xmlns="http://www.w3.org/2000/09/xmldsig#">${objXmlInSign}</Signature><${tagEnd}></DSCKS>`,
            signingTime: `<DSCKS><${tagBegin}><Signature  xmlns="http://www.w3.org/2000/09/xmldsig#">${objXmlInSign}</Signature><${tagEnd}></DSCKS>`
            // strReplaceTagSignBegin: `<DSCKS><${tagBegin}><Signature`,
            // strReplaceTagSignEnd: `<Object><SignatureProperties><SignatureProperty Id="TimeStamp" Target=""><SigningTime>${idt.replace(" ", "T")}</SigningTime></SignatureProperty></SignatureProperties></Object></Signature><${tagEnd}></DSCKS>`
        }
        return objSignSample
    },
    isTicket: (type) => {
        return ["01/TVE", "02/TVE"].includes(type)
    },
    templatestm: async (mst, form) => {
        let content = await redis.get(`TEMPSTM`)
        //fs.writeFileSync(`E:/Temp/einvoice/template123/stm.html`, content)
        if (!content) {
            let FILE = path.join(__dirname, "..", "..", "temp/TEMPSTM")
            try {
                content = fs.readFileSync(FILE, "utf8")
            } catch (err) {
                content = ""
            }
            try {
                await redis.set(`TEMPSTM`, content)
            } catch (err) {

            }
        }
        return genTmpFromContent(content)
    },
    templatebth: async (doc) => {
        let content = await redis.get(`TEMPBTH`)
        //fs.writeFileSync(`E:/Temp/einvoice/template123/stm.html`, content)
        if (!content) {
            let FILE = path.join(__dirname, "..", "..", "temp/TEMPBTH")
            try {
                content = fs.readFileSync(FILE, "utf8")
            } catch (err) {
                content = ""
            }
            try {
                await redis.set(`TEMPBTH`, content)
            } catch (err) {

            }
        }
        return genTmpFromContent(content)
    },
    templatewno: async (doc) => {
        let content = await redis.get(`TEMPWNO`)
        //fs.writeFileSync(`E:/Temp/einvoice/template123/stm.html`, content)
        if (!content) {
            let FILE = path.join(__dirname, "..", "..", "temp/TEMPWNO")
            try {
                content = fs.readFileSync(FILE, "utf8")
            } catch (err) {
                content = ""
            }
            try {
                await redis.set(`TEMPWNO`, content)
            } catch (err) {

            }
        }
        return genTmpFromContent(content)
    },
    templatestmtct: async (fn) => {
        let content = (fn == 'statement_tct_received') ? await redis.get(`TEMPSTMTCTREC`) : await redis.get(`TEMPSTMTCTACC`)
        //fs.writeFileSync(`E:/Temp/einvoice/template123/stmtct.html`, content)
        if (!content) {
            let FILE = (fn == 'statement_tct_received') ? path.join(__dirname, "..", "..", "temp/TEMPSTMTCTREC") : path.join(__dirname, "..", "..", "temp/TEMPSTMTCTACC")
            try {
                content = fs.readFileSync(FILE, "utf8")
            } catch (err) {
                content = ""
            }
            try {
                (fn == 'statement_tct_received') ? await redis.set(`TEMPSTMTCTREC`, content) : await redis.set(`TEMPSTMTCTACC`, content)
            } catch (err) {

            }
        }
        return genTmpFromContent(content)
    },
    rvControlcharactersObjectJsonString(str) {
        let a = str.split(`"`),b=[]
       a = Service.rvControlcharactersObject(a)
        // for (var i in a) {

        //     b.push(Service.rvControlcharacters(a[i]));
        // }
    
        var c =  a.join(`"`);
    
        return c;
        
    },
    //ham xoa cac ky tu control dau vao la string vd \u001d , \u001f, \b
    rvControlcharacters(str) {      
        if (typeof str === 'string' || str instanceof String)  return str.replace(/[\u0000-\u001F\u007F-\u009F]/g, "")
       
        else return str
    },
     //ham xoa cac ky tu control dau vao la object
    rvControlcharactersObject(object) {
        let keys = Object.keys(object)
        for (const key of keys) {
            object[key] = Service.rvControlcharacters(object[key])
        }
        return object
    },
    escapeStringxml(str) {      
        if (typeof str === 'string' || str instanceof String)  return xmlescape(str.replace(/[\u0000-\u001F\u007F-\u009F]/g, ""))
       
        else return str
    },
    //ham xoa cac ky tu control dau vao la object
    escapeStringxmlObject(object) {
        let keys = Object.keys(object)
        for (const key of keys) {
            object[key] = Service.escapeStringxml(object[key])
        }
        return object
    },
    parseJson(doc) {
        if(typeof doc === 'object') return doc
        else return JSON.parse(doc)
    },
    convertDate(date) {
        if(config.dbtype =='orcl') return new Date(date)
        return date
    },
    convertNumber(number) {
        if(config.dbtype =='orcl') return new Number(number)
        return number
    },
    roundNumberInBth(num, curr) {
        if(isNaN(num)) return num
        if(curr=="VND") return Number(Number(num).toFixed(0))
        return Number(Number(num).toFixed(config.GRIDNFCONF))
    }
}
module.exports = Service