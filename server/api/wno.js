"use strict"
const config = require("./config")
const ServiceInv = require('./inv')
const SEC = require("./sec")
const dbtype = config.dbtype
const dbs = require(`./${dbtype}/dbs`)
const redis = require("./redis")
const ftp = require("./ftp")
const util = require("./util")
const hbs = require("./hbs")
const sign = require("./sign")
const fcloud = require("./fcloud")
const inc = require("./inc")
const logging = require("./logging")
//const enumf = require("./enumf")
const tvan = require("./tvan")
const ous = require(`./${dbtype}/ous`)
const moment = require("moment")
const ext = require("./ext")
const WN_ID_KEY = "WN.ID"
const dtf = config.dtf
const COL = require("./col")
const ENUM_STATUS = {
    WAIT: "WAIT",
    SENT: "SENT",
    // 2 STATUS RESPONSE
    TAX_ACCEPT: "TAX_ACCEPT",
    TAX_DENY: "TAX_DENY",
}
const CARR = config.CARR
const sql_getWaitingWNList_01 = {
    sql_getWaitingWNList_01_mssql: `id row_id, JSON_VALUE([doc], '$.stax') stax, JSON_VALUE([doc], '$.place') place,JSON_VALUE([doc], '$.noti_taxtype') noti_taxtype, JSON_VALUE([doc], '$.sname') sname, JSON_VALUE([doc], '$.noti_dt') noti_dt, JSON_VALUE([doc], '$.noti_taxdt') noti_taxdt, JSON_VALUE([doc], '$.noti_taxid') noti_taxid, JSON_VALUE([doc], '$.noti_taxnum') noti_taxnum, JSON_VALUE([doc], '$.noti_taxname') noti_taxname, JSON_VALUE([doc], '$.budget_relationid') budget_relationid,JSON_VALUE([doc], '$.adj.rea') rea,doc doc,FORMAT(dt,'dd/MM/yyyy') dt, status status, status_cqt status_cqt, dien_giai dien_giai, form form, serial serial, seq seq`,
    sql_getWaitingWNList_01_mysql: `id "row_id", doc ->> '$.stax' "stax", doc ->> '$.place' "place", doc ->> '$.noti_taxtype' "noti_taxtype", doc ->> '$.sname' "sname", doc ->> '$.noti_dt' "noti_dt", doc ->> '$.noti_taxdt' "noti_taxdt", doc ->> '$.noti_taxid' "noti_taxid", doc ->> '$.noti_taxnum' "noti_taxnum", doc ->> '$.noti_taxname' "noti_taxname", doc ->> '$.budget_relationid' "budget_relationid",doc ->> '$.adj.rea' "rea",doc "doc",DATE_FORMAT(dt,'%d/%m/%Y') "dt", status "status", status_cqt "status_cqt", dien_giai "dien_giai", form "form", serial "serial", seq "seq"`,
    sql_getWaitingWNList_01_orcl: `id "row_id", JSON_VALUE(doc, '$.stax') "stax", JSON_VALUE(doc, '$.place') "place",JSON_VALUE(doc, '$.noti_taxtype') "noti_taxtype", JSON_VALUE(doc, '$.sname') "sname", JSON_VALUE(doc, '$.noti_dt') "noti_dt", JSON_VALUE(doc, '$.noti_taxdt') "noti_taxdt", JSON_VALUE(doc, '$.noti_taxid') "noti_taxid", JSON_VALUE(doc, '$.noti_taxnum') "noti_taxnum", JSON_VALUE(doc, '$.noti_taxname') "noti_taxname", JSON_VALUE(doc, '$.budget_relationid') "budget_relationid",JSON_VALUE(doc, '$.adj.rea') "rea",doc "doc",to_char(dt,'dd/MM/yyyy') "dt", status "status", status_cqt "status_cqt", dien_giai "dien_giai", form "form", serial "serial", seq "seq"`,
    sql_getWaitingWNList_01_pgsql: `id "row_id", doc->>'stax' "stax", doc->>'place' "place",doc->>'noti_taxtype' "noti_taxtype", doc->>'sname' "sname", doc->>'noti_dt' "noti_dt", doc->>'noti_taxdt' "noti_taxdt", doc->>'noti_taxid' "noti_taxid", doc->>'noti_taxnum' "noti_taxnum", doc->>'noti_taxname' "noti_taxname", doc->>'budget_relationid' "budget_relationid", doc->'adj'->>'rea' "rea",doc "doc",to_char(dt,'DD/MM/YYYY') "dt", status "status", status_cqt "status_cqt", dien_giai "dien_giai", form "form", serial "serial", seq "seq"`
}

const sql_history = {
    sql_history_mssql:   `select htg.id,FORMAT(htg.datetime_trans,'dd/MM/yyyy HH:mm:ss') datetime_trans,htg.status,htg.description,htg.r_xml from van_history htg
    join s_wrongnotice_process swn on htg.id_ref=swn.id 
    where htg.type_invoice=4 and htg.id_ref=? order by htg.datetime_trans`
    ,
    sql_history_mysql:  `select htg.id,DATE_FORMAT(htg.datetime_trans,'%d/%m/%Y %T') datetime_trans,htg.status,htg.description,htg.r_xml from van_history htg
    join s_wrongnotice_process swn on htg.id_ref=swn.id 
    where htg.type_invoice=4 and htg.id_ref=? order by htg.datetime_trans`
    ,
    sql_history_orcl:  `select htg.id,to_char(htg.datetime_trans,'DD/MM/YYYY HH24:MI:SS') "datetime_trans",htg.status "status",htg.description "description",htg.r_xml "r_xml" from van_history htg
    join s_wrongnotice_process swn on htg.id_ref=swn.id 
    where htg.type_invoice=4 and htg.id_ref=? order by htg.datetime_trans`
    ,
    sql_history_pgsql:  `select htg.id,to_char(htg.datetime_trans,'DD/MM/YYYY HH:MI:SS') datetime_trans,htg.status,htg.description,htg.r_xml from van_history htg
    join s_wrongnotice_process swn on htg.id_ref=swn.id 
    where htg.type_invoice=4 and htg.id_ref=? order by htg.datetime_trans`
}

const sql_sendtbss = {
    sql_sendtbss_mssql: `update s_inv set doc=JSON_MODIFY(doc,'$.id_tbss',?) where id=?`,
    sql_sendtbss_mysql: `update s_inv set doc=JSON_SET(doc,'$.id_tbss',?) where id=?`,
    sql_sendtbss_orcl: `update s_inv set doc=json_mergepatch(doc, '{"id_tbss":?}') where id=?`,
    sql_sendtbss_pgsql: `update s_inv set doc=jsonb_set(doc,'{id_tbss}',?) where id=?`
}

const sql_wnadjtype = {
    sql_wnadjtype_mssql: `update s_inv set doc=JSON_MODIFY(doc,'$.wnadjtype',4) where id=?`,
    sql_wnadjtype_mysql: `update s_inv set doc=JSON_SET(doc,'$.wnadjtype',4) where id=?`,
    sql_wnadjtype_orcl: `update s_inv set doc=json_mergepatch(doc, '{"wnadjtype":4}') where id=?`,
    sql_wnadjtype_pgsql: `update s_inv set doc=jsonb_set(doc,'{wnadjtype}',4) where id=?`
}

// const sql_sendtbss_get = {
//     sql_sendtbss_get_mssql:`select 0 "chk",id "id",sec "sec",ic "ic",uc "uc",curr "curr" ,pid "pid",cid "cid",cast(idt as date) "idt",status "status",ou "ou",type "type",form "form",serial "serial",seq "seq",btax "btax",bname "bname",stax "stax",buyer "buyer",baddr "baddr",note "note",sumv "sumv",vatv "vatv",totalv "totalv",sum "sum",vat "vat",total "total",adjdes "adjdes",adjtyp "adjtyp",cde "cde", cvt "cvt",concat(JSON_VALUE(doc, '$.cancel.rea'), JSON_VALUE(doc, '$.adj.rea')) "rea", status_tbss "status_tbss", bmail "bmail",c0 "c0",c1 "c1",c6 "c6",error_msg_van, JSON_VALUE(doc, '$.ma_cqthu') "ma_cqthu", JSON_VALUE(doc, '$.adj.form') "adjform" from s_inv where id= ? `,
//     sql_sendtbss_get_mysql:`select 0 "chk",id "id",sec "sec",ic "ic",uc "uc",curr "curr" ,pid "pid",cid "cid",idt "idt",status "status",ou "ou",type "type",form "form",serial "serial",seq "seq",btax "btax",bname "bname",stax "stax",buyer "buyer",baddr "baddr",note "note",sumv "sumv",vatv "vatv",totalv "totalv",sum "sum",vat "vat",total "total",adjdes "adjdes",adjtyp "adjtyp",cde "cde", cvt "cvt",concat(doc->>'$.cancel.rea, doc->>'$.adj.rea) "rea", status_tbss "status_tbss", bmail "bmail",c0 "c0",c1 "c1",c6 "c6",error_msg_van "error_msg_van", doc ->> '$.ma_cqthu' "ma_cqthu", doc->>'$.adj.form "adjform" from s_inv where id= ? `,
//     sql_sendtbss_get_orcl:`select 0 "chk",id "id",sec "sec",ic "ic",uc "uc",curr "curr" ,pid "pid",cid "cid",idt "idt",status "status",ou "ou",type "type",form "form",serial "serial",seq "seq",btax "btax",bname "bname",stax "stax",buyer "buyer",baddr "baddr",note "note",sumv "sumv",vatv "vatv",totalv "totalv",sum "sum",vat "vat",total "total",adjdes "adjdes",adjtyp "adjtyp",cde "cde", cvt "cvt",concat(JSON_VALUE(doc, '$.cancel.rea'), JSON_VALUE(doc, '$.adj.rea')) "rea", status_tbss "status_tbss", bmail "bmail",c0 "c0",c1 "c1",c6 "c6",error_msg_van "error_msg_van", JSON_VALUE(doc, '$.ma_cqthu') "ma_cqthu", JSON_VALUE(doc, '$.adj.form') "adjform" from s_inv where id= ? `,
//     sql_sendtbss_get_pgsql:`select 0 "chk",id "id",sec "sec",ic "ic",uc "uc",curr "curr" ,pid "pid",cid "cid",idt "idt",status "status",ou "ou",type "type",form "form",serial "serial",seq "seq",btax "btax",bname "bname",stax "stax",buyer "buyer",baddr "baddr",note "note",sumv "sumv",vatv "vatv",totalv "totalv",sum "sum",vat "vat",total "total",adjdes "adjdes",adjtyp "adjtyp",cde "cde", cvt "cvt",concat(doc -> 'cancel' ->> 'rea', doc -> 'adj' ->> 'rea' rea) "rea", status_tbss "status_tbss", bmail "bmail",c0 "c0",c1 "c1",c6 "c6",error_msg_van "error_msg_van", doc->>'ma_cqthu' "ma_cqthu", doc -> 'adj' ->> 'form' "adjform" from s_inv where id= ? `,
// }
const sql_sendtbss_get = {
    sql_sendtbss_get_mssql:`select 0 "chk",id "id",sec "sec",ic "ic",uc "uc",curr "curr" ,pid "pid",cid "cid",cast(idt as date) "idt",status "status",ou "ou",type "type",form "form",serial "serial",seq "seq",btax "btax",bname "bname",stax "stax",buyer "buyer",baddr "baddr",note "note",sumv "sumv",vatv "vatv",totalv "totalv",sum "sum",vat "vat",total "total",adjdes "adjdes",adjtyp "adjtyp",cde "cde", cvt "cvt",concat(JSON_VALUE(doc, '$.cancel.rea'), JSON_VALUE(doc, '$.adj.rea')) "rea", status_tbss "status_tbss", bmail "bmail",c0 "c0",c1 "c1",c6 "c6",error_msg_van, JSON_VALUE(doc, '$.ma_cqthu') "ma_cqthu", JSON_VALUE(doc, '$.adj.form') "adjform", doc "doc" from s_inv where id= ? `,
    sql_sendtbss_get_mysql:`select 0 "chk",id "id",sec "sec",ic "ic",uc "uc",curr "curr" ,pid "pid",cid "cid",idt "idt",status "status",ou "ou",type "type",form "form",serial "serial",seq "seq",btax "btax",bname "bname",stax "stax",buyer "buyer",baddr "baddr",note "note",sumv "sumv",vatv "vatv",totalv "totalv",sum "sum",vat "vat",total "total",adjdes "adjdes",adjtyp "adjtyp",cde "cde", cvt "cvt",concat(doc->>'$.cancel.rea', doc->>'$.adj.rea') "rea", status_tbss "status_tbss", bmail "bmail",c0 "c0",c1 "c1",c6 "c6",error_msg_van "error_msg_van", doc ->> '$.ma_cqthu' "ma_cqthu", doc->>'$.adj.form' "adjform", doc "doc" from s_inv where id= ? `,
    sql_sendtbss_get_orcl:`select 0 "chk",id "id",sec "sec",ic "ic",uc "uc",curr "curr" ,pid "pid",cid "cid",idt "idt",status "status",ou "ou",type "type",form "form",serial "serial",seq "seq",btax "btax",bname "bname",stax "stax",buyer "buyer",baddr "baddr",note "note",sumv "sumv",vatv "vatv",totalv "totalv",sum "sum",vat "vat",total "total",adjdes "adjdes",adjtyp "adjtyp",cde "cde", cvt "cvt",concat(JSON_VALUE(doc, '$.cancel.rea'), JSON_VALUE(doc, '$.adj.rea')) "rea", status_tbss "status_tbss", bmail "bmail",c0 "c0",c1 "c1",c6 "c6",error_msg_van "error_msg_van", JSON_VALUE(doc, '$.ma_cqthu') "ma_cqthu", JSON_VALUE(doc, '$.adj.form') "adjform", doc "doc" from s_inv where id= ? `,
    sql_sendtbss_get_pgsql:`select 0 "chk",id "id",sec "sec",ic "ic",uc "uc",curr "curr" ,pid "pid",cid "cid",idt "idt",status "status",ou "ou",type "type",form "form",serial "serial",seq "seq",btax "btax",bname "bname",stax "stax",buyer "buyer",baddr "baddr",note "note",sumv "sumv",vatv "vatv",totalv "totalv",sum "sum",vat "vat",total "total",adjdes "adjdes",adjtyp "adjtyp",cde "cde", cvt "cvt",concat(doc -> 'cancel' ->> 'rea', doc -> 'adj' ->> 'rea' rea) "rea", status_tbss "status_tbss", bmail "bmail",c0 "c0",c1 "c1",c6 "c6",error_msg_van "error_msg_van", doc->>'ma_cqthu' "ma_cqthu", doc -> 'adj' ->> 'form' "adjform", doc "doc" from s_inv where id= ? `,
}

const rbi = (id) => {
    return new Promise(async (resolve, reject) => {
        try {
            const result = await inc.execsqlselect(`select doc "doc" from s_wrongnotice_process where id=?`, [id])
            const rows = result
            if (rows.length == 0) reject(new Error(`Không tìm thấy Thông báo sai sót ${id}`))
            resolve(rows[0])
        } catch (err) {
            reject(err)
        }
    })
}

const formatWnoData =  (data) => {
    return new Promise(async (resolve, reject) => {
    try {    
    
    let listData=[]
    for (let item of data.items) {
        const result = await inc.execsqlselect(`select doc "doc" from s_inv where id=?`, [item.id])
        const rows = result
        const doc = util.parseJson(rows[0].doc)
        let ma_cqthu = doc.ma_cqthu?doc.ma_cqthu:""
        if (item.noti_type == 2 || item.noti_type == 3) {
            let result_maCQT = await inc.execsqlselect(`select doc "doc" from s_inv where id = ?`, [doc.pid])
            if (result_maCQT.length > 0) {
                let docRoot = util.parseJson(result_maCQT[0].doc)
                ma_cqthu = docRoot.ma_cqthu ? docRoot.ma_cqthu : ""
            }
        }
        let items=[]
        item = {
            row_id: item.row_id,
            type : doc.type,
            form: item.form,
            id: item.id,
            idt: item.idt,
            line: item.line,
            noti_type: item.noti_type,
            seq: item.seq,
            isgroup: item.isgroup,
            serial: item.serial,
            type_ref: item.type_ref,
            rea: item.rea,
            bmail: item.bmail,
            ma_cqthu:ma_cqthu
        }
        items.push(item)
        let new_data = {
            budget_relationid: data.budget_relationid,
            sname: data.sname,
            place: data.place,
            noti_taxtype: data.noti_taxtype,
            noti_taxnum: data.noti_taxnum,
            noti_taxname: data.noti_taxname,
            noti_taxid: data.noti_taxid,
            noti_taxdt: data.noti_taxdt,
            noti_dt: data.noti_dt,
            stax: data.stax,
            items: items
        }
        listData.push(new_data)
    }
    
    resolve(listData)
    } catch (err) {
        reject(err)
    }
})
}
const insdb2group = async (token, inv) => {
    const schema = token.schema
    let type=''
    if(inv.adj && [1].includes(inv.adj.typ)) type=3
    if(inv.adj && [2].includes(inv.adj.typ)) type=2
    let check32_78 = inv.form
    let data = {
        id: inv.id,
        form: inv.form,
        serial: inv.serial,
        seq: inv.seq,
        bmail: inv.bmail,
        idt: inv.idt,
        type_ref: (check32_78.length > 1) ? 3 : 1,
        noti_type: type,
        rea:inv.adj.rea
    }
    let result, rows, row, sql, binds, _affectedRows = 0
    sql = `INSERT INTO s_wrongnotice_process SET ?`
    binds = { id_inv: data.id,ou:token.ou, doc: JSON.stringify(data), xml: "", status: ENUM_STATUS.WAIT }
    result = await inc.execsqlinsupddel(sql, binds)
    _affectedRows += result.affectedRows

    return _affectedRows
}
const insdb2group_tbss = async (token, inv) => {
    let type=''
    if(inv.adj && [1].includes(inv.adj.typ)) type=3
    if(inv.adj && [2].includes(inv.adj.typ)) type=2
    let result, sql, binds, _affectedRows = 0
    sql = `INSERT INTO s_wrongnotice_process (id_wn, id_inv,ou, doc, xml, status,taxc,type,form,serial,seq) VALUES (?,?,?,?,?,?,?,?,?,?,?)` //
    binds = [inv.id_wn, inv.items[0].row_id,inv.ou, JSON.stringify(inv),inv.xml, ENUM_STATUS.SENT, inv.stax,inv.items[0].type,inv.items[0].form,inv.items[0].serial,inv.items[0].seq ]
    result = await inc.execsqlinsupddel(sql, binds)
    _affectedRows += result

    return _affectedRows
}
const signandinsdb = async (data, org, token) => {

    // [STEP] Save DB after send 2 tax
    let result, rows, row, sql, binds, _affectedRows = 0,taxc = token.taxc
    //let conn, sSysLogs
    try {
        // conn = await dbs.getConnection()
        // await conn.beginTransaction()
        sql = `INSERT INTO s_wrongnotice_process (id_wn, ou,taxc, id_inv, doc, xml, status, status_cqt,form,serial,seq,type) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)` //
        for (let daItem of data.arr) {
            
            // SIGN [START]
            let signSample // , PATH_XML// Dung chung
            let id_wn = daItem.data.id_wn, xml
            delete daItem.data.id_wn
            if (org.sign == 1) {
                signSample = await util.getSignSample(daItem.data.type, daItem.idt, daItem.inc)
                if (daItem.xml) {
                    const sml = Buffer.from(daItem.xml, "base64").toString()
                    // xml = util.signature(sml, signSample)
                    xml = sml
                }
                // xml = data.xml
                // delete data.xml
            } else if (org.sign == 2) {
                let ca = await sign.ca(daItem.data.stax, util.decrypt(org.pwd))
                const PATH_XML = await util.getPathXml(daItem.data.type)
                const signSample = await util.getSignSample(daItem.data.type, moment().format(config.dtf), id_wn)
                xml = sign.sign123(ca, Buffer.from(daItem.xml, "base64").toString(), id_wn, PATH_XML, signSample)
            } 
            // else if (org.sign == 3) {
            //     const usr = util.decrypt(org.usr), pwd = util.decrypt(org.pwd), mst = token.mst, ts = org.ts, arr = await fcloud.xml(usr, pwd, mst, [daItem], ts)
            //     xml = arr[0].xml
            // }
            // SIGN [END]
            let inv_id='',json_data=daItem.data
            let  items = []
            let form,serial,seq,type
            //chi dc phep 1 dong
            for (let item of json_data.items) {
                if (item.isgroup) {
                    await inc.execsqlinsupddel(`DELETE FROM s_wrongnotice_process WHERE (id = ?);`, [item.row_id]) 
                }  
                items.push({id:item.id,bmail:item.bmail,noti_type: item.noti_type})          
                delete item.isgroup
                delete item.bmail
                delete item.row_id           
                inv_id = inv_id+item.id + ','   
                form=  item.form    
                serial=  item.serial   
                seq=  item.seq  
                type=  item.type   
            }
            json_data.adt = moment().format(dtf)
            binds = [ id_wn , token.ou,taxc, inv_id.slice(0, -1), JSON.stringify(json_data),  xml, ENUM_STATUS.SENT, 0,form,serial,seq,type ]
            result = await inc.execsqlinsupddel(sql, binds)
            _affectedRows += result     
            if(daItem.action !="GOM"){
                // for (let item of items) {               
                //     let bmail = item.bmail            
                //     if (bmail) await Service.sendmailwn(daItem.data, org, bmail, token)               
                // }
             }
            // gui xong mail moi update hoa don do lock bang
            
            // if(daItem.action=="DIEUCHINH"){            
            //     let id = items[0].id
            //     result = await inc.execsqlselect(`select doc "doc" from s_inv where id=?`, [id])
            //     let doc = JSON.parse(result[0].doc)
            //     doc.status = 3
            //     doc.id_tbss = id_wn
            //     delete doc["adj"]
            //     delete doc["root"]
            //     delete doc["pid"]
            //     delete doc["pay"]
            //     await inc.execsqlinsupddel(`update s_inv set doc=?,pid=null,edt=idt where id=?`, [JSON.stringify(doc),id])
            // }
            //else{
                for (let item of items) {               
                    let id = item.id    
                    result = await inc.execsqlselect(`select doc "doc" from s_inv where id=?`, [id])
                    let doc = util.parseJson(result[0].doc)
                    doc.id_tbss = id_wn
                    if (item.noti_type == 4 && json_data.noti_taxtype == 1) doc.wnadjtype = 4
                    else{
                        if (doc.wnadjtype) delete doc["wnadjtype"]
                    }
                    await inc.execsqlinsupddel(`update s_inv set doc=? where id=?`, [JSON.stringify(doc),id])
                    await inc.execsqlinsupddel(`update s_inv set wno_adj=? where id=?`, [item.noti_type, id])
                    if(config.ent == 'apple'){
                        const tmp = await util.templatewno(doc.stax, doc.form)
                        let obj = { doc: JSON.stringify(json_data), tmp: tmp }
                        let reqjsr = ext.createRequest(obj)
                        const bodyBuffer = await util.jsReportRenderAttach(reqjsr)
                        if (doc.status == 4 && doc.type == "01GTKT")
                            await inc.execsqlinsupddel(`update s_xml set doc =?, pdf_wn=? ,xml_wn =?, type = 4 where id=?`, [JSON.stringify(doc),`${bodyBuffer.toString('base64')}`,xml,id])
                        else
                            await inc.execsqlinsupddel(`update s_xml set doc =?, pdf_wn=? ,xml_wn =? where id=?`, [JSON.stringify(doc),`${bodyBuffer.toString('base64')}`,xml,id])
                    }
                }
            //}
        }
       
        // await conn.commit()
    }
    catch (err) {
        //await conn.rollback()
        throw (err)
    }
    //finally {
        //await conn.release()
    //}
    return _affectedRows
}
const Service = {
    iwh: async (req, all, decodereq) => {
        const token = (!decodereq) ? SEC.decode(req) : req.token, u = token.u, query = req.query, filter = util.parseJson(query.filter), sort = query.sort
        let keys = Object.keys(filter), kar = ["ou", "status", "type", "form", "serial", "th_type", "paym", "curr", "cvt"], i = 4
        let tree = "", val, ext = "", extxls = "", cols, where, binds = [util.convertDate(moment(new Date(filter.fd)).format(dtf)), util.convertDate(moment(moment(filter.td).endOf("day")).format(dtf)), token.taxc], order, resultsgr, taxcsgr
        if (config.dbtype == "orcl") binds = [util.convertDate(filter.fd), util.convertDate(moment(filter.td).endOf("day")), token.taxc]
        where = `where idt  between ? and ? and stax=? and status in (3,4) `
        //  if(ENT=='scb')where +=` and c0 in (select system from s_user_sys where userid='${token.uid}') `

        if (config.ou_grant && token.ou == u) {
            //TODO
            if (all) {
                //tree = `with tree as (select ${u} id union all select child.id from s_ou child,tree parent where parent.id=child.pid)`
                //where += ` and ou in (select id from tree)`
            }
            else {
                where += ` and ou=?`
                binds.push(u)
            }
        }
       // if (query.adjtypAll == "true") where += ` and ( (status = 3 and adjtyp in (1,2)) or status = 4)` // tim kiem hoa don thay the, dieu chinh, huy
        if (keys.includes("type")) {
            cols = await COL.cache(filter["type"])
            if (cols.length > 0) {
                let exts = []
                let extsxls = []
                for (const col of cols) {
                    let cid = col.id
                    exts.push(`${cid} "${cid}"`)
                    //Xu ly rieng cho excel neu la truong date thi format lai
                    if (col.view == "datepicker") {
                        switch (config.dbtype) {
                            case "mssql":
                                extsxls.push(`FORMAT(CONVERT(DATETIME, ${cid}, 102),'dd/MM/yyyy') "${cid}"`)
                                break
                            case "orcl":
                                extsxls.push(`TO_CHAR(TO_DATE(SUBSTR(${cid},1,10),'YYYY-MM-DD'),'${mfd}') "${String(cid).toLowerCase()}"`)
                                break
                            case "mysql":
                                extsxls.push(`DATE_FORMAT(STR_TO_DATE(${cid}, '%Y-%m-%d'),'%d/%m/%Y') "${cid}"`)
                                break
                            case "pgsql":
                                extsxls.push(`to_char(${cid},'DD/MM/YYYY') "${cid}"`)
                                break
                            default:
                                vsqlpager = ``
                                break
                        } 
                    }
                    else
                        extsxls.push(`${cid} "${cid}"`)
                }
                ext = `,${exts.join()}`
                extxls = `,${extsxls.join()}`
            }
        }
        for (const key of kar) {
            if (config.ent == "vcm" || config.ent == "sgr") {//tra cuu voi mutiselect
                if (keys.includes(key) && key.includes("serial")) {

                    val = filter[key]
                    let str = "", arr = String(val).split(",")
                    for (const row of arr) {
                        str += `'${row}',`
                    }
                    where += ` and ${key} in (${str.slice(0, -1)})`
                }
                else if (keys.includes(key) && key.includes("ou")) {

                    val = filter[key]
                    let str = "", arr = String(val).split(",")
                    for (const row of arr) {
                        let d = row.split("__")
                        str += `${d[0]},`
                    }
                    where += ` and ${key} in (${str.slice(0, -1)})`
                    if (config.ent == "sgr" || config.ent == "vcm") {
                        resultsgr = await inc.execsqlselect(`select taxc "taxc" from s_ou where id in (${str.slice(0, -1)})`)
                        if (resultsgr.length > 0) {
                            taxcsgr = resultsgr
                            let str = ""
                            for (const oldtaxcsgr of taxcsgr) {
                                str += `'${oldtaxcsgr.taxc}',`
                            }
                            where += ` and stax in (${str.slice(0, -1)})`

                        }

                    }
                    //binds.push(filter[key])
                } else {
                    if (keys.includes(key)) {
                        where += ` and ${key}=?`
                        binds.push(filter[key])
                    }
                }
            } else {
                if (keys.includes(key)) {
                    where += ` and ${key}=?`
                    binds.push(filter[key])
                }
            }

        }

        kar.push("fd", "td")
        for (const key of kar) {
            delete keys[key]
        }
        if (keys.includes("status_tbss")) {
            where += ` and status_tbss = ?`
            binds.push(filter["status_tbss"])
        }
        else {
            where += ` and status_tbss in (0,16,171) `
        }


        if (keys.includes("ikind2")) {
            switch (filter["ikind2"]) {
                case 4:
                    where += ` and status = 4 and cid is null and status_received in (0,8,10)`
                    break
                case 1:
                    where += ` and adjtyp = 1 and status in (3,4)`
                    break
                case 2:
                    where += ` and adjtyp = 2 and status in (3,4)`
                    break
            }
        }
        else {
             where +=` and ((status = 4 and cid is null and status_received in (0,8,10)) or adjtyp in (1,2))`
        }


        for (const key of keys) {
            val = filter[key]
            if (val && !kar.includes(key)) {
                switch (key) {
                    case "seq":
                        where += ` and seq like ?`
                        binds.push(`%${val}`)
                        break
                    case "btax":
                        where += ` and btax like ?`
                        binds.push(`%${val}%`)
                        break
                    case "bacc":
                        where += ` and bacc like ?`
                        binds.push(`%${val}%`)
                        break
                    case "id":
                        where += ` and id = ?`
                        binds.push(val)
                        break
                    case "list_invid":
                        where += ` and list_invid = ?`
                        binds.push(val)
                        break
                    case "user":
                        where += ` and uc like ?`
                        binds.push(`%${val}%`)
                        break
                    case "status_tbss":
                        break
                    case "adjtyp":
                        switch (val) {
                            case 3:
                                where += ` and cid is not null and cde like ?`
                                binds.push("Bị thay thế%")
                                break
                            case 4:
                                where += ` and cid is not null and cde like ?`
                                binds.push("Bị điều chỉnh%")
                                break
                            default:
                                where += ` and adjtyp = ?`
                                binds.push(val)
                                break
                        }
                        break
 					case "ikind2":
                        break
                    default:
                        let b = true
                        if (CARR.includes(key) && cols) {
                            let obj = cols.find(x => x.id == key)
                            if (typeof obj !== "undefined") {
                                const view = obj.view
                                if (view == "datepicker") {
                                    b = false
                                    where += ` and date(${key})=?`
                                    binds.push(new Date(val))
                                }
                                else if (view == "multicombo") {
                                    b = false
                                    where += ` and ${key} in (${val.split(",")})`
                                }
                                else if (view == "combo") {
                                    b = false
                                    where += ` and ${key}=?`
                                    binds.push(val)
                                }
                                else if (view == "text") {
                                    if (obj.type == "number") {
                                        b = false
                                        where += ` and ${key} like ?`
                                        binds.push(`%${val}%`)
                                    }
                                }
                            }
                        }
                        if (b) {
                            where += ` and upper(${key}) like ?`
                            binds.push(`%${val.toString().toUpperCase()}%`)
                        }
                        break
                }
            }
        }
        // if (serial_usr_grant) {
        //     where += ` and stax = ss.taxc and type = ss.types and form = ss.forms and serial = ss.serials`
        // }
        if (config.serial_usr_grant) {
            where += ` and exists (select 1 from s_serial ss, s_seusr ss2 where ss.id = ss2.se and ss2.usrid = '${token.uid}' and s_inv.stax = ss.taxc and s_inv.type = ss.type and s_inv.form = ss.form and s_inv.serial = ss.serial)`

        }
        where += ` and not exists (select 1 from s_serial ss where s_inv.stax = '${token.taxc}' and s_inv.form = ss.form and s_inv.serial = ss.serial and ss.sendtype = 2)`
        // if (ENT == "scb" && config.scb_dept_check) {
        //     where += ` and ((c2 like '') or exists (select 1 from s_deptusr dd where s_inv.c2 = dd.deptid and dd.usrid = '${token.uid}')) `
        // }
        if (sort) {
            Object.keys(sort).forEach(key => {
                order = `order by ${key} ${sort[key]}`
            })
        }
        else order = "order by idt desc,ABS(id) desc, type, form, serial, seq"
        return { where: where, binds: binds, ext: ext, order: order, extxls: extxls, paramindex: i }
    },
    genXML: async (req, res, next) => {
        try {
            const token = SEC.decode(req), org = await ous.org(token), body = req.body
            await inc.checkrevokeAPI(token.taxc)
            let listdata = await formatWnoData(body)
            let arr = []
            for(let data of listdata){
                const id_wn = await redis.incr(WN_ID_KEY)
           
                data.type = "04/SS-HĐĐT"
                data.createdt = moment(new Date()).format(config.dtf)
                data.id_wn = id_wn
                
                const xml = await util.j2x(data, id_wn), inc = id_wn
                const PATH_XML = await util.getPathXml(data.type)
                const signSample = await util.getSignSample(data.type, data.createdt, inc)
                if (org.sign == 1) {
                    // let xml_sign_time = signSample.signingTime
                    // , str = xml
                    // , xml_end_tag_replace = signSample.xmlEndTagReplace
                    // , object_signing_time = `${xml_sign_time}${xml_end_tag_replace}`
                    // str = str.replace(xml_end_tag_replace, object_signing_time)
                    // str = str.replace(`<DLTBao>`,`<DLTBao Id="${id_wn}">`)


                    let  str = xml ,xml_sign_time = signSample.signingTime
                        , xml_end_tag_replace = signSample.xmlEndTagReplace
                        , object_signing_time = `${xml_sign_time}${xml_end_tag_replace}`
                        // Add signature that have signing time to xml
                        str = str.replace(xml_end_tag_replace, object_signing_time)
                        str = str.replace(`<DLTBao>`,`<DLTBao Id="${id_wn}">`)
                    arr.push({
                        id :inc,
                        xml: Buffer.from(str).toString("base64"),
                        reference1: inc,                            
                        reference2 :`SigningTime-${signSample.tagBegin}-${inc}`,
                        tagSign:"DLTBao",
                        tagEnd:"</DLTBao>",
                        idt: moment().format(dtf),
                        data: data
                    })
                }
                else {
                    arr.push({
                        inc: inc,
                        idt: data.createdt,
                        xml: Buffer.from(xml).toString("base64"),
                        ref: PATH_XML,
                        signSample,
                        data: data,
                        action:body.action
                    })
                }
            }
            
            const responseData = { mst: token.taxc, date: new Date(), arr: arr, SIGN_TYPE: org.sign }
            res.json(responseData)
        }
        catch (err) {
            next(err)
        }
    },
    signandinsdb: async (data, org, token) => {
        return await signandinsdb(data, org, token)
    },
    insdb2group: async (token, inv) => {
        return await insdb2group(token, inv)
    },
    sendtbss: async (req, res, next) => {
        try {
            const body = req.body
            //let sql = `select 0 "chk",id "id",sec "sec",ic "ic",uc "uc",curr "curr" ,pid "pid",cid "cid",cast(idt as date) "idt",status "status",ou "ou",type "type",form "form",serial "serial",seq "seq",btax "btax",bname "bname",stax "stax",buyer "buyer",baddr "baddr",note "note",sumv "sumv",vatv "vatv",totalv "totalv",sum "sum",vat "vat",total "total",adjdes "adjdes",adjtyp "adjtyp",cde "cde", cvt "cvt",concat(JSON_VALUE(doc, '$.cancel.rea'), JSON_VALUE(doc, '$.adj.rea')) rea, status_tbss "status_tbss", bmail "bmail",c0 "c0",c1 "c1",c6 "c6",error_msg_van, JSON_VALUE(doc, '$.ma_cqthu') "ma_cqthu", JSON_VALUE(doc, '$.adj.form') "adjform" from s_inv where id= ? `
            let sql = sql_sendtbss_get[`sql_sendtbss_get_${config.dbtype}`]
            let binds = [body.id]
            let result = await inc.execsqlselect(sql, binds)
            let rows = result[0]
											
            let type_ref=1
            const doc = util.parseJson(rows.doc)
            let check78_32 = (body.noti_type == 1 || body.noti_type == 4) ? doc.form : doc.adj.form
            type_ref = (check78_32.length > 1) ? 3 : 1    
            const ous = require(`./ous`)
            const org = await ous.obt(rows.stax)
            let ma_cqthu = doc.ma_cqthu?doc.ma_cqthu:""
            if (body.noti_type == 2 || body.noti_type == 3) {
                let result_maCQT = await inc.execsqlselect(`select doc "doc" from s_inv where id = ?`, [rows.pid])
                if (result_maCQT.length > 0) {
                    let docRoot = util.parseJson(result_maCQT[0].doc)
                    ma_cqthu = docRoot.ma_cqthu ? docRoot.ma_cqthu : ""
                }
            }
            let items=[],listData=[]
            let item = {
                row_id: rows.id,
                type : doc.type,
                form: (body.noti_type == 1 || body.noti_type == 4) ? doc.form : doc.adj.form,
                id: rows.id,
                idt: (body.noti_type == 1 || body.noti_type == 4) ? doc.idt : doc.adj.idt,
                line: 1,
                noti_type: body.noti_type,
                seq: (body.noti_type == 1 || body.noti_type == 4) ? doc.seq : doc.adj.seq,
                serial: (body.noti_type == 1 || body.noti_type == 4) ? doc.serial : doc.adj.serial,
                type_ref: type_ref,
                rea: rows.rea,
                bmail: rows.bmail,
                ma_cqthu: ma_cqthu
            }
            items.push(item)
            let new_data = {
                budget_relationid: '',
                sname: org.name,//
                place: org.place,
                noti_taxtype: 1,
                noti_taxnum: '',
                noti_taxname: org ? org.taxoname : "",
                noti_taxid: org ? org.taxo : "",
                noti_taxdt: '',
                noti_dt:moment(new Date()).format(config.dtf),//
                stax: rows.stax,//
                items: items
            }
            listData.push(new_data)
            const id_wn2 = await redis.incr(WN_ID_KEY)
            listData[0].type = "04/SS-HĐĐT"
            listData[0].createdt = moment(new Date()).format(config.dtf)
            listData[0].id_wn = id_wn2
            await inc.checkrevokeAPI(rows.stax)
            let xml = await util.j2x(listData[0], id_wn2)
            let signSample
            if (org.sign == 1) {
                signSample = await util.getSignSample(listData[0].type, listData[0].createdt, id_wn2)
                if (xml) {
                    const sml = xml
                    xml = sml
                }
            } else if (org.sign == 2) {
                let ca = await sign.ca(listData[0].stax, util.decrypt(org.pwd))
                const PATH_XML = await util.getPathXml(listData[0].type)
                const signSample = await util.getSignSample(listData[0].type, moment().format(config.dtf), id_wn2)
                xml = sign.sign123(ca,xml, id_wn2, PATH_XML, signSample)
            } 
            rows.xml = Buffer.from(`<?xml version="1.0" encoding="utf-8"?> ${xml}`).toString("base64")
            new_data.xml = xml
            new_data.id_wn = id_wn2
            new_data.ou = rows.ou
            let _affectedRows = await insdb2group_tbss('', new_data)

            // await inc.execsqlinsupddel(`update s_inv set doc=JSON_SET(doc,'$.id_tbss',?) where id=?`, [id_wn2,body.id])
            if (config.dbtype == 'orcl') {
                let resultDoc = await inc.execsqlselect(`select doc "doc" from s_inv where id = ?`,[body.id])
                let docInv = resultDoc[0].doc
                docInv = util.parseJson(docInv)
                docInv.id_tbss = id_wn2
                await inc.execsqlinsupddel(`update s_inv set doc = ? where id = ?`, [JSON.stringify(docInv), body.id])
            }
            else {
                await inc.execsqlinsupddel(sql_sendtbss[`sql_sendtbss_${config.dbtype}`], [id_wn2,body.id])
            }
            await inc.execsqlinsupddel(`update s_inv set wno_adj = ? where id=?`, [body.noti_type,body.id])

            if (body.noti_type == 4 && listData[0].noti_taxtype == 1) {
                // await inc.execsqlinsupddel(`update s_inv set doc=JSON_SET(doc,'$.wnadjtype',4) where id=?`, [body.noti_type,body.id])
                if (config.dbtype == 'orcl') {
                    let resultDoc = await inc.execsqlselect(`select doc "doc" from s_inv where id = ?`,[body.id])
                    let docInv = resultDoc[0].doc
                    docInv = util.parseJson(docInv)
                    docInv.wnadjtype = 4
                    await inc.execsqlinsupddel(`update s_inv set doc= ? where id = ?`, [JSON.stringify(docInv), body.id])
                }
                else {
                    await inc.execsqlinsupddel(sql_wnadjtype[`sql_wnadjtype_${config.dbtype}`], [body.id])
                }
            }
            if(config.ent == 'apple'){
                const tmp = await util.templatewno(doc.stax, doc.form)
                if (new_data.items[0].noti_type == 1) new_data.items[0].noti_type = "Hủy"
                if (new_data.items[0].noti_type == 2) new_data.items[0].noti_type = "Điều chỉnh"   
                if (new_data.items[0].noti_type == 3) new_data.items[0].noti_type = "Thay thế"
                if (new_data.items[0].noti_type == 4) new_data.items[0].noti_type = "Giải trình"  
                new_data.items[0].idt = moment(new_data.items[0].idt).format('DD/MM/YYYY')
                let obj = { doc: new_data, tmp: tmp }
                let reqjsr = ext.createRequest(obj)
                const bodyBuffer = await util.jsReportRenderAttach(reqjsr)
                await inc.execsqlinsupddel(`update s_xml set pdf_wn=? ,xml_wn =? where id=?`, [`${bodyBuffer.toString('base64')}`,rows.xml,body.id])
            }
            //
           
            await inc.execsqlinsupddel(`INSERT INTO van_einv_listid_msg (pid, type, status_scan) values (?,?,?)`, [id_wn2, 'TBSS', 0])
            let sSysLogs = { fnc_id: 'wrongnotice_send', src: config.SRC_LOGGING_DEFAULT, dtl: `Gửi thông báo sai sót id_wn: ${body.id}`, msg_id: id_wn2, doc: JSON.stringify(body) };
            logging.ins(req, sSysLogs, next)
            res.json({ affectedRows: _affectedRows })
        }
        catch (err) {
            console.trace(err)
            next(err)
        }
    },
    sendnow: async (req, res, next) => {
        try {
            const token = SEC.decode(req), org = await ous.org(token), body = req.body
            await inc.checkrevokeAPI(token.taxc)
            let _affectedRows = await signandinsdb(body, org, token)
            for (let daItem of body.arr) {
                let  id_wn = daItem.inc
                // let paramsDataTVan = { 
                //     ids: daItem.inc,
                //     type: "04/SS-HÐÐT"
                // }
                //await tvan.dataTvanApi(token, paramsDataTVan, req)
                await inc.execsqlinsupddel(`INSERT INTO van_einv_listid_msg (pid, type, status_scan) values (?,?,?)`, [daItem.inc, 'TBSS', 0])
                let sSysLogs = { fnc_id: 'wrongnotice_send', src: config.SRC_LOGGING_DEFAULT, dtl: `Gửi thông báo sai sót id_wn: ${id_wn}`, msg_id: id_wn, doc: JSON.stringify(body) };
                logging.ins(req, sSysLogs, next)
            }
          
            res.json({ affectedRows: _affectedRows })
        }
        catch (err) {
            next(err)
        }
    },
    SendbackToVanWNO: async (req, res) => {
        try {
            const id = req.params.id
            await inc.execsqlinsupddel(`INSERT INTO van_einv_listid_msg (pid, type, status_scan) values (?,?,?)`, [id, 'TBSS', 0])
            res.json({ result: 1 })
        }
        catch (err) {
           console.trace(err)
           res.json({ result: 0 })
        }

    },
    getWaitingWNList: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            const query = req.query,
                start = query.start ? query.start : 0,
                count = query.count ? query.count : 10,
                filter = util.parseJson(query.filter),
                status = filter.status,
                pagerobj = inc.getsqlpagerandbind(start, count)
            let type = filter.type, form = filter.form, serial = filter.serial, ou = filter.ou, seq = filter.seq
            let   statusCQT = filter.status_cqt, where =``
            let sqlbase, sql, result, ret, binds = []
            if (type && type !="*") {
               where += ` and type = ?`
               binds.push(type)
            } 
            if (form && form !="*") {
                where += ` and form = ?`
                binds.push(form)
            }
            if (serial && serial !="*") {
                where += ` and serial = ?`
                binds.push(serial)
            }
            if (ou && ou !="*") where += ` and ou = ${ou}`
            if (seq) {
                where += ` and seq like ?`
                binds.push(`%${seq}`)
            } 
            if (statusCQT && statusCQT !="*") where += ` and status_cqt = ${statusCQT}`
            let queryCol = `${sql_getWaitingWNList_01[`sql_getWaitingWNList_01_${config.dbtype}`]}`
            sqlbase =  `select ${queryCol} from s_wrongnotice_process where 1=1 and taxc ='${token.taxc}' ${where} and id_wn is not null and dt >= ? and dt <= ? ` 
            sql = `${sqlbase} order by row_id desc ${pagerobj.vsqlpagerstr}`
            binds.push(filter.fd)
            binds.push(moment(filter.td).endOf("day").format(dtf))
            for (let t of pagerobj.vsqlpagerbind) binds.push(t)
            // let result1 = await dbs.format(sql, binds)
            result = await inc.execsqlselect(sql, binds)
            ret = { data: result, pos: start }
            let lengthBinds = binds.length
            if (start == 0) {
                sql = config.dbtype=="orcl"? `select count(*) "total" from (${sqlbase}) "swp"`: `select count(1) "total" from (${sqlbase}) as swp`
                for (let i = 0; i < lengthBinds - 2;i++) binds.push(binds[i])
                result = await inc.execsqlselect(sql, binds)
                ret.total_count = result[0].total
            }
            res.json(ret)
        }
        catch (err) {
            next(err)
            console.trace(err)
        }
    },
    searchDatatbss: async (req, res, next) => {
        try {
            let result = await Service.getWaitingWNList(req, res, next)
            return res.json(result)

        }
        catch (err) {
            next(err)
        }
    },
    sendmailwn: async (data, org, bmail, token) => {
        try {
            let taxc = data.stax//util.schemaToTaxc(schema)
            let source = await ftp.getTempMail(taxc, "wnoti")
            const html = await hbs.j2mwn(source, data)
            const tmp = await util.templatewn(source)
            // Mail
            const smtp = org.smtp, from = smtp ? smtp.mail || smtp.auth.user : config.mail
            const to = bmail.trim().split(/[ ,;]+/).map(e => e.trim())
            const content = { from: from, to: to, subject: "Thông báo sai sót", html: html }
            if (!util.isEmpty(org.bcc)) content.bcc = org.bcc
            const msg = { config: smtp, content: content, uc: token.uid }
            //const retu = await util.pushMailToDbQueue(enumf.MAIL.APP_WRONG_NOTICE, { qname: config.qname, message: JSON.stringify(msg) })
        }
        catch (err) {
            throw err
            //next(err)
        }
    },
    htm: async (req, res, next) => {
        try {
            const id = req.params.id, body = await rbi(id), doc = JSON.parse(body.doc)
            doc.crdt = `ngày ${new Date(doc.createdt).getDate()} tháng ${new Date(doc.createdt).getMonth()+1} năm ${new Date(doc.createdt).getFullYear()}.`
            for (let item of doc.items){
                item.idt = moment(item.idt).format('DD/MM/YYYY')
                if (item.type_ref == 1) item.type_ref ="Hóa đơn điện tử theo Nghị định 123/2020/NĐ-CP"
                if (item.type_ref == 3) item.type_ref ="Các loại hóa đơn theo Nghị định số 51/2010/NĐ-CP và Nghị định số 04/2014/NĐ-CP (Trừ hóa đơn điện tử có mã xác thực của cơ quan thuế theo Quyết định số 1209/QĐ-BTC và Quyết định số 2660/QĐ-BTC)"
                if (item.noti_type == 1) item.noti_type = "Hủy"
                if (item.noti_type == 2) item.noti_type = "Điều chỉnh"   
                if (item.noti_type == 3) item.noti_type = "Thay thế"
                if (item.noti_type == 4) item.noti_type = "Giải trình" 
            }
            doc.adt = doc.adt ? doc.adt : doc.createdt
            const tmp = await util.templatewno(doc.stax, doc.form)
            let obj = { doc: doc, tmp: tmp }
            let reqjsr = ext.createRequest(obj)
            // res.json({ status: status, doc: doc, tmp: tmp })
            const bodyBuffer = await util.jsReportRenderAttach(reqjsr)
            let sizeInByte = bodyBuffer.byteLength
            res.json({ pdf: `data:application/pdf;base64, ${bodyBuffer.toString('base64')}`, sizePDF: sizeInByte})
        } catch (err) {
            next(err)
        }
    },
    hscqt: async (req, res, next) => {
        try {
            const token = SEC.decode(req), schema = token.schema, params = req.params, id = params.id
            // let sql = `select htg.datetime_trans,htg.status,htg.description from van_history htg
            //     join s_wrongnotice_process swn on htg.id_ref=swn.id 
            //     where htg.type_invoice=4 and htg.id_ref=? order by htg.datetime_trans`
            let sql = sql_history[`sql_history_${config.dbtype}`]
            let binds = [id]
            let result = await inc.execsqlselect(sql, binds)
            res.json(result)
        } catch (err) {
            next(err)
        }
    },
    hscqtXml: async (req, res, next) => {
        try {
            const params = req.params, id = params.id
            let sql =  `select vh.r_xml from van_history vh where id = ?`
            let binds = [id]
            let result = await inc.execsqlselect(sql, binds)
            res.json(result)
        } catch (err) {
            next(err)
        }
    },
    getInvWrongNotice: async (req, res, next) => {
        try {
            const query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 10, filter= query.filter, iwh = await Service.iwh(req, false)
            let sql, result, ret, i = iwh.paramindex, pagerobj = inc.getsqlpagerandbind(start, count)
            let rea, idt
            switch (config.dbtype) {
                case "mssql":
                    rea = `concat(JSON_VALUE(doc, '$.cancel.rea'), JSON_VALUE(doc, '$.adj.rea')) rea`
                    idt = `cast(idt as date) "idt"`
                    break
                case "orcl":
                    rea = `JSON_VALUE(doc, '$.*.rea') "rea"`
                    idt = `idt "idt"`
                    break
                case "mysql":
                    rea = `concat_ws('',doc->>'$.cancel.rea', doc->>'$.adj.rea') rea`
                    idt = `idt "idt"`
                    break
                case "pgsql":
                    rea = `concat(doc -> 'cancel' ->> 'rea', doc -> 'adj' ->> 'rea') rea`
                    idt = `idt "idt"`
                    break
                default:
                    break
            }
            sql = `select 0 "chk",id "id",sec "sec",ic "ic",uc "uc",curr "curr" ,pid "pid",cid "cid",${idt},ou "ou",type "type",form "form",serial "serial",seq "seq",btax "btax",bname "bname",buyer "buyer",baddr "baddr",note "note",sumv "sumv",vatv "vatv",totalv "totalv",sum "sum",vat "vat",total "total",adjdes "adjdes",cde "cde", cvt "cvt",${rea}, status_tbss "status_tbss", bmail "bmail"${iwh.ext},error_msg_van "error_msg_van" ,status "status",doc "doc", adjtyp "adjtyp" from s_inv ${iwh.where} ${iwh.order} ${pagerobj.vsqlpagerstr}`

            let binds = iwh.binds
            for (let t of pagerobj.vsqlpagerbind) binds.push(t)
            result = await inc.execsqlselect(sql, binds)
            ret = { data: result, pos: start }
            for (let row of result) {
                row.doc = util.parseJson(row.doc)
                if (row.adjtyp == 1){
                    row.ikind2 = 1
                }
                else if (row.adjtyp == 2){
                    row.ikind2 = 2
                }
                else if (row.status == 4 && !row.cid) {
                    row.ikind2 = 4
                }
            }
            if (start == 0) {
                sql = `select count(*) "total" from s_inv ${iwh.where}`
                let lengthBinds = iwh.binds.length, bind2=[]
                for (let i = 0; i < lengthBinds - 2;i++) bind2.push(binds[i])
                result = await inc.execsqlselect(sql, bind2)
                ret.total_count = result[0].total
            }
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    }
}
module.exports = Service