const config = require('./config')

const solace = require('solclientjs');
const factoryProps = new solace.SolclientFactoryProperties();
factoryProps.profile = solace.SolclientFactoryProfiles.version10;
solace.SolclientFactory.init(factoryProps);

solace.SolclientFactory.setLogLevel(solace.LogLevel.WARN);

module.exports.QueueProducer = function (queueName, messageText) {
    let producer = {};
    producer.session = null;
    producer.queueName = queueName;

    producer.log = function (line) {
        const now = new Date();
        const time = [('0' + now.getHours()).slice(-2), ('0' + now.getMinutes()).slice(-2),
        ('0' + now.getSeconds()).slice(-2)];
        const timestamp = '[' + time.join(':') + '] ';
        logger4app.debug(timestamp + line);
    };

    producer.log('\n*** Producer to queue "' + producer.queueName + '" is ready to connect ***');

    if (producer.session !== null) {
        producer.log('Already connected and ready to send messages.');
        return;
    }
    try {
        producer.session = solace.SolclientFactory.createSession({
            url: config.solace_host_url,
            vpnName: config.solace_vpn,
            userName: config.solace_username,
            password: config.solace_password,
        });
    } catch (error) {
        producer.log(error.toString());
    }
    producer.session.on(solace.SessionEventCode.UP_NOTICE, function (sessionEvent) {
        producer.log('=== Successfully connected and ready to send messages. ===');
        producer.sendMessage();
        producer.exit();
    });
    producer.session.on(solace.SessionEventCode.CONNECT_FAILED_ERROR, function (sessionEvent) {
        producer.log('Connection failed to the message router: ' + sessionEvent.infoStr +
            ' - check correct parameter values and connectivity!');
    });
    producer.session.on(solace.SessionEventCode.DISCONNECTED, function (sessionEvent) {
        producer.log('Disconnected.');
        if (producer.session !== null) {
            producer.session.dispose();
            producer.session = null;
        }
    });
    try {
        producer.session.connect();
    } catch (error) {
        producer.log(error.toString());
    }
    producer.sendMessage = function () {
        if (producer.session !== null) {
            var message = solace.SolclientFactory.createMessage();
            producer.log('Sending message "' + messageText + '" to queue "' + producer.queueName + '"...');
            message.setDestination(solace.SolclientFactory.createDurableQueueDestination(producer.queueName));
            message.setBinaryAttachment(messageText);
            message.setDeliveryMode(solace.MessageDeliveryModeType.PERSISTENT);
            try {
                producer.session.send(message);
                producer.log('Message sent.');
            } catch (error) {
                producer.log(error.toString());
            }
        } else {
            producer.log('Cannot send messages because not connected to Solace message router.');
        }
    };
    producer.exit = function () {
        producer.disconnect();
        setTimeout(function () {
            process.exit();
        }, 1000); // wait for 1 second to finish
    };
    producer.disconnect = function () {
        producer.log('Disconnecting from Solace message router...');
        if (producer.session !== null) {
            try {
                producer.session.disconnect();
            } catch (error) {
                producer.log(error.toString());
            }
        } else {
            producer.log('Not connected to Solace message router.');
        }
    };

    return producer;
};