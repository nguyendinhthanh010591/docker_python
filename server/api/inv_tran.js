"use strict"
const Hashids = require("hashids/cjs")
const moment = require("moment")
const config = require("../config")
const logger4app = require("../logger4app")

const util = require("../util")
const redis = require("../redis")

const SEC = require("../sec")

const dbs = require("./dbs")
const numeral = require("numeral")
const ous = require("./ous")
const path = require("path")
const fs = require("fs")
const xlsxtemp = require("xlsx-template")

const objectMapper = require("object-mapper")

const grant = config.ou_grant

const mfd = config.mfd
const dtf = config.dtf
const ENT = config.ent
const invoice_seq = config.invoice_seq
const UPLOAD_MINUTES = config.upload_minute
const KM = "Khuyến mãi"
const CK = "Chiết khấu"
const MT = "Mô tả"
const KCT = "Không chịu thuế"
const KKK = "Không kê khai nộp thuế"
const n2w = require("../n2w")
const { NUMBER } = require("oracledb")
const HDC =  ["ic", "form", "serial", "seq", "idt", "buyer", "bname", "btax", "baddr", "btel", "bmail", "bacc", "bbank", "paym", "curr", "exrt", "note", "sum", "vat", "total", "items.type", "items.name", "items.unit", "items.price", "items.quantity", "items.vrt", "items.amount", "rform", "rserial", "rseq", "rref", "rrdt", "rrea"] 
const tax = async (rows, exrt) => {
    let catobj = JSON.parse(JSON.stringify(config.CATTAX))
    let arr = catobj
    for (let row of rows) {
      let amt = row.amount
      if (!util.isNumber(amt)) continue
      const vrt = row.vrt
      let obj = arr.find(x => x.vrt == vrt)
      if (typeof obj == "undefined") continue
      if (row.type == CK) amt = -amt
      obj.amt += amt
      if (obj.hasOwnProperty("vat")) obj.vat += amt * vrt / 100
    }
    let tar = arr.filter(obj => { return obj.amt !== 0 })
    for (let row of tar) {
      row.amtv = Math.round(row.amt * exrt)
      if (row.hasOwnProperty("vat")) row.vatv = Math.round(row.vat * exrt)
    }
    return tar
  }

const chkou = async (id, token) => {
    if (grant) {
        if (token.ou !== token.u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)
        const result = await dbs.query(`select ou from s_inv where id=@1`, [id])
        const rows = result.recordset
        if (rows.length > 0 && rows[0].ou !== token.u) throw new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`)
    }
}
const Service = {
   
    iwh: async (req, all) => {
        const token = SEC.decode(req), u = token.u, query = req.query, filter = JSON.parse(query.filter), sort = query.sort
        let keys = Object.keys(filter), kar = [], i = 3
        let  val,  cols, where = `where valueDate between @1 and @2 `, binds = [moment(new Date(filter.fd)).format(dtf), moment(moment(filter.td).endOf("day")).format(dtf)], order
       
        kar.push("fd", "td")
        for (const key of kar) {
            delete keys[key]
        }
        for (const key of keys) {
            val = filter[key]
            if (val ) {
                switch (key) {
                    case "status":
                        where += ` and status in (${val})`
                      
                        break
                    case "trantype":
                        where += ` and trantype = @${i++}`
                        binds.push(`${val}`)
                        break
                    case "customerID":
                        where += ` and customerID like @${i++}`
                        binds.push(`%${val}%`)
                        break
                    case "customerAcc":
                            where += ` and customerAcc like @${i++}`
                            binds.push(`%${val}%`)
                            break
                    case "curr":
                        where += ` and curr = @${i++}`
                        binds.push(`${val}`)
                        break
                    case "segment":
                        where += ` and segment = @${i++}`
                        binds.push(`${val}`)
                        break
                    case "inv_id":
                            where += ` and inv_id = @${i++}`
                            binds.push(`${val}`)
                            break
                    case "inv_date":
                        where += ` and inv_date = @${i++}`
                        binds.push(moment(new Date(`${val}`)).format(dtf))
                        break
                    case "refNo":
                            where += ` and refNo like @${i++}`
                            binds.push(`%${val}%`)
                        break
                    case "content":
                            where += ` and upper(vcontent) like @${i++}`
                            binds.push(`%${val.toString().toUpperCase()}%`)
                        break
                    case "vat":
                        where += ` and vrt in (${val})`
                      
                        break
                
                }
            }
        }
      
        if (sort) {
            Object.keys(sort).forEach(key => {
                order = `order by ${key} ${sort[key]}`
            })
        }
        else order = "order by valueDate desc"
        return { where: where, binds: binds,  order: order }
    },
    
   
    get: async (req, res, next) => {
        try {
            //console.time("timeget")
            const query = req.query, start = query.start ? query.start : 0, count = query.count ? query.count : 100, iwh = await Service.iwh(req, true)
            let sql, result, ret
            sql = `select 0 chk,[ID]
            ,[refNo]
            ,[valueDate]
            ,[customerID]
            ,[customerName]
            ,[taxCode]
            ,[customerAddr]
            ,[isSpecial]
            ,[curr]
            ,[exrt]
            ,[vrt]
            ,ISNULL(chargeAmount, 0) chargeAmount
            ,[vcontent]
            ,[amount]
            ,[vat]
            ,status
            ,[total]
            ,FORMAT (create_date, 'dd/MM/yyyy HH:mm:ss') create_date
            , FORMAT (last_update, 'dd/MM/yyyy HH:mm:ss') last_update
            ,[ma_nv]
            ,[ma_ks]
             ,inv_id,
             form,serial,seq,trantype,trancol,price,quantity,mailGroup
            ,[tranID] from s_trans ${iwh.where} ${iwh.order} OFFSET ${start} ROWS FETCH NEXT ${count} ROWS ONLY`
            result = await dbs.query(sql, iwh.binds)
            ret = { data: result.recordset, pos: start }
            if (start == 0) {
                sql = `select count(*) total from s_trans ${iwh.where}`
                result = await dbs.query(sql, iwh.binds)
                ret.total_count = result.recordset[0].total
            }
            //console.timeEnd("timeget")
            res.json(ret)
        }
        catch (err) {
            next(err)
        }
    },
    app: async (req, res, next) => {
        try {
                const token = SEC.decode(req)
                if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
                const  body = req.body,invs = body.invs
                let str ='',where=''
                for (const inv of invs) {
                
                    str += `${inv.ID},`
                }
                where += ` and ID in (${str.slice(0, -1)})`
                await dbs.query(`update  s_trans set status=3,ma_ks=@1 where 1=1 ${where} `, [token.uid])
                res.json({ message: 'ok', status: 1 })
            }catch (err) {
            next(err)
        }
    },
    
    iwhc: async (form, invs,flag) => {
        let  where,binds=[]
        where = ` where status in (0,3) `
       if(flag==0){
            let keys = Object.keys(form), i = 3
            let  val
           
            binds = [moment(new Date(form.fd)).format(dtf), moment(moment(form.td).endOf("day")).format(dtf)]
            where += ` and valueDate between @1 and @2 `
           
          
            for (const key of keys) {
                val = form[key]
                if (val ) {
                    switch (key) {
                        case "status":
                            where += ` and status in (${val})`
                        
                            break
                        case "trantype":
                            where += ` and trantype = @${i++}`
                            binds.push(`${val}`)
                            break
                        case "customerID":
                            where += ` and customerID like @${i++}`
                            binds.push(`%${val}%`)
                            break
                        case "customerAcc":
                                where += ` and customerAcc like @${i++}`
                                binds.push(`%${val}%`)
                                break
                        case "curr":
                            where += ` and curr = @${i++}`
                            binds.push(`${val}`)
                            break
                        case "segment":
                            where += ` and segment = @${i++}`
                            binds.push(`${val}`)
                            break
                        case "inv_id":
                                where += ` and inv_id = @${i++}`
                                binds.push(`${val}`)
                                break
                        case "inv_date":
                            where += ` and inv_date = @${i++}`
                            binds.push(moment(new Date(`${val}`)).format(dtf))
                            break
                        case "refNo":
                                where += ` and refNo like @${i++}`
                                binds.push(`%${val}%`)
                            break
                        case "content":
                                where += ` and upper(vcontent) like @${i++}`
                                binds.push(`%${val.toString().toUpperCase()}%`)
                            break
                        case "vat":
                            where += ` and vrt in (${val})`
                        
                            break
                    
                    }
                }
            }
            return { where: where, binds: binds }
       }else{
            let str =''
            for (const inv of invs) {
            
                str += `${inv.ID},`
            }
            where += ` and ID in (${str.slice(0, -1)})`
            return { where: where, binds: [] }
       }
        
      
       
       
    },
    col: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const  body = req.body,taxc = token.taxc, uid = token.uid,ou = token.ou,name=token.uid,org = await ous.obt(taxc),
                invs = body.invs,
                flag= body.flag,
                form = body.form,
                col = body.col, //kieu gom
                idt = body.idt,iwh = await Service.iwhc(form,invs ,flag)
               
            let Obj = {
                "customerID": "c9","customerAddr": "baddr","trantype":"c0"
                , "customerName": "bname", "taxCode": "btax", "curr": "curr", "exrt": "exrt"
                , "amount": "items.amount", "vcontent": "items.name",  "vrt": "items.vrt","valueDate": "items.c0","refNo": "items.c1","chargeAmount": "items.c2","vat": "items.vat","total": "items.total","price": "items.price","quantity": "items.quantity"
                };
         
            let status, message,rs,key='',invd,items =[],count=0,length,rssr, where='',invc,sum=0,sumv=0,vat=0,vatv=0,total=0,totalv=0,exrt,curr='',rsc

                try {
                  
                    let str =''
                    // for (const inv of invs) {
                      
                    //     str += `${inv.ID},`
                    // }
                    // where += ` and ID in (${str.slice(0, -1)})`

                    
                    rssr =  await dbs.query(`select distinct form,serial,type from s_serial where taxc=@1  and fd<=@2 and status=1`, [taxc, moment(new Date()).format("YYYY-MM-DD HH:mm:ss")])
                    if (rssr.recordset.length<1) throw new Error("Không tìm thấy dải ký hiệu đang hiệu lực ")
                 
                    rsc = await dbs.query(`select count(*) rc from s_inv where idt>@1 and form=@2 and serial=@3 and status>1 and status<>4`, [moment(body.idt).endOf('day').toDate(), rssr.recordset[0].form, rssr.recordset[0].serial])
                  
                    if (rsc.recordset[0].rc > 0) throw new Error(`Tồn tại HĐ có ngày lớn hơn ${moment(body.idt).format('DD/MM/YYYY')} và đã cấp số \n (Invoice has a date larger than ${moment(body.idt).format('DD/MM/YYYY')} and issued a number)`)
                   
                    rsc = await dbs.query(`select FORMAT (max(valueDate), 'yyyy-MM-dd HH:mm:ss') valueDate  from s_trans where  1=1 ${where}`, [])

                    if (moment(rsc.recordset[0].valueDate).toDate() > moment(body.idt).toDate()) throw new Error(`Ngày lớn hơn phải >= ngày giao dịch \n (Invoice date must >= transaction date)`)

                    if(col==0){
                        rs=    await dbs.query(`select 0 chk,[ID]
                        ,[refNo]
                        ,FORMAT (valueDate, 'dd/MM/yyyy') valueDate
                        ,[customerID]
                        ,[customerName]
                        ,[taxCode]
                        ,[customerAddr]
                        ,[isSpecial]
                        ,trantype
                        ,[curr]
                        ,[exrt]
                        ,[vrt]
                        ,[chargeAmount]
                        ,[vcontent]
                        ,[amount]
                        ,[vat]
                        ,status
                        ,price	  
                        ,quantity
                        ,[total]
                        from s_trans  ${iwh.where}  order by isSpecial,customerID,vrt,curr,trantype,taxCode`, iwh.binds)
                        length = rs.recordset.length
                        let line =1
                        for (const r of rs.recordset) {
                            if (curr !=r.curr){
                                let  sql = `select id,cur,val,dt from s_ex  where dt between @1 and @2 and cur = @3`
                                let  result = await dbs.query(sql, [moment(body.idt).format("YYYY-MM-DD HH:mm:ss"), moment(body.idt).endOf("day").format("YYYY-MM-DD HH:mm:ss"),r.curr])
                                  if(result.recordset.length<1) throw new Error("Không tìm thấy tỷ giá cho ngày: "+ moment(body.idt).format("DD-MM-YYYY"))
                                 exrt = result.recordset[0].val
                            }
                            curr =r.curr
                            count++
                            invd = objectMapper(r, Obj)
                           
                            let item=invd.items
                            let keyc = r.isSpecial + '_' + r.customerID +'_'+ r.vrt +'_'+ r.curr +'_'+ r.trantype +'_'+ r.taxCode 
                            if(key == keyc){
                                sum = sum + item.amount
                                sumv = Math.round(sum * Number(exrt))
                                if(item.vat){
                                    vat = vat + item.vat
                                    vatv = Math.round(vat * Number(exrt))
                                }
                                total = total + item.total
                                totalv = Math.round(total * Number(exrt))
                                item.line=line++
                                items.push(item)
                               
                            }else{
                                line =1
                                if(count==1){
                                    invc = invd
                                    invc.exrt=exrt
                                    sum = sum + item.amount
                                    sumv = Math.round(sum * Number(exrt))
                                    if(item.vat){
                                        vat = vat + item.vat
                                        vatv = Math.round(vat * Number(exrt))
                                    }
                                    total = total + item.total
                                    totalv = Math.round(total * Number(exrt))
                                    item.line=line++
                                    items.push(item)
                                }else{

                                 
                                    let id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC,id)
                                    invc.items = items
                                    invc.tax = await tax(items, Number(invc.exrt))
                                    invc.sum = sum
                                    invc.sumv = sumv
                                    invc.vat = vat
                                    invc.vatv = vatv
                                    invc.total = total
                                    invc.totalv = totalv
                                    invc.word = n2w.n2w(totalv, invc.curr)
                                   // invc.exrt=exrt
                                    invc.stax = taxc
                                    invc.form = rssr.recordset[0].form
                                    invc.serial = rssr.recordset[0].serial
                                    invc.type = rssr.recordset[0].type
                                    invc.sec = sec
                                    invc.status = 1
                                    invc.idt = moment(body.idt).format("YYYY-MM-DD HH:mm:ss")
                                    invc.id = id
                                    invc.stax = org.taxc
                                    invc.sname = org.name
                                    invc.saddr = org.addr
                                    invc.smail = org.mail
                                    invc.stel = org.tel
                                    invc.taxo = org.taxo
                                    invc.sacc = org.acc
                                    invc.sbank = org.bank
                                    await dbs.query(`insert into s_inv(id,sec,idt,ou,uc,doc) values (@1,@2,@3,@4,@5,@6)`, [id, sec, moment(body.idt).format("YYYY-MM-DD HH:mm:ss"), ou, uid, JSON.stringify(invc)])
                                    //await dbs.query(`update  s_trans set stattus_temp=status  where 1=1 ${where} and isSpecial =@1 and customerID=@2 and vrt=@3 and curr=@4 and trantype=@5 and taxCode=@6 `, [ key.split('_')[0], key.split('_')[1],key.split('_')[2],key.split('_')[3],key.split('_')[4],key.split('_')[5]])
                                    await dbs.query(`update  s_trans set stattus_temp=status,status=1,inv_id =@1,exrt=@2,trancol=@3,ma_nv=@4,form=@5,serial=@6,inv_date=@7  
                                    where 1=1 ${where} and isSpecial =@8 and customerID=@9 and vrt=@10 and curr=@11 and trantype=@12 and taxCode=@13`, 
                                    [id,invc.exrt,0,name,invc.form,invc.serial,moment(body.idt).format("YYYY-MM-DD"), key.split('_')[0], key.split('_')[1],key.split('_')[2],key.split('_')[3],key.split('_')[4],key.split('_')[5]])
                                    logger4app.debug('Gom Insert thanh cong hoa don id : ' +id )
                                    invc = invd
                                    invc.exrt=exrt
                                    items =[]
                                    sum = 0
                                    sumv = 0
                                    vat = 0 
                                    vatv = 0
                                    total = 0
                                    totalv = 0
                                    sum = sum + item.amount
                                    sumv = Math.round(sum * Number(exrt))
                                    if(item.vat){
                                        vat = vat + item.vat
                                        vatv = Math.round(vat * Number(exrt))
                                    }
                                    total = total + item.total
                                    totalv = Math.round(total * Number(exrt))
                                    item.line=line++
                                    items.push(item)
                                }
                               
                            }
                            key = keyc
                            if (length==count){
                              
                                let id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC,id)
                                invc.items = items
                                invc.tax = await tax(items, Number(invc.exrt))
                                invc.sum = sum
                                invc.sumv = sumv
                                invc.vat = vat
                                invc.vatv = vatv
                                invc.total = total
                                invc.totalv = totalv
                                invc.word = n2w.n2w(totalv, invc.curr)
                               
                                invc.stax = taxc
                                invc.form = rssr.recordset[0].form
                                invc.serial = rssr.recordset[0].serial
                                invc.type = rssr.recordset[0].type
                                invc.sec = sec
                                invc.status = 1
                                //invc.exrt=exrt
                                invc.id = id
                                invc.stax = org.taxc
                                invc.sname = org.name
                                invc.saddr = org.addr
                                invc.smail = org.mail
                                invc.stel = org.tel
                                invc.taxo = org.taxo
                                invc.sacc = org.acc
                                invc.sbank = org.bank
                                invc.idt = moment(body.idt).format("YYYY-MM-DD HH:mm:ss")
                                await dbs.query(`insert into s_inv(id,sec,idt,ou,uc,doc) values (@1,@2,@3,@4,@5,@6)`, [id, sec, moment(body.idt).format("YYYY-MM-DD HH:mm:ss"), ou, uid, JSON.stringify(invc)])
                                //await dbs.query(`update  s_trans set stattus_temp=status  where 1=1 ${where} and (isSpecial+'_'+customerID+'_'+vrt+'_'+curr+'_'+CONVERT(varchar(10),trantype)+'_'+taxCode)=@1`, [ key])
                                //await dbs.query(`update  s_trans set status=1,inv_id =@1,exrt=@2,trancol=@4,ma_nv=@5,form=@6,serial=@7,inv_date=@8 where 1=1 ${where} and (isSpecial+'_'+customerID+'_'+vrt+'_'+curr+'_'+CONVERT(varchar(10),trantype)+'_'+taxCode)=@3`, [id,invc.exrt, key,0,name,invc.form,invc.serial,moment(body.idt).format("YYYY-MM-DD")])
                                await dbs.query(`update  s_trans set stattus_temp=status,status=1,inv_id =@1,exrt=@2,trancol=@3,ma_nv=@4,form=@5,serial=@6,inv_date=@7  where 1=1 ${where} and isSpecial =@8 and customerID=@9 and vrt=@10 and curr=@11 and trantype=@12 and taxCode=@13`, [id,invc.exrt,0,name,invc.form,invc.serial,moment(body.idt).format("YYYY-MM-DD"), key.split('_')[0], key.split('_')[1],key.split('_')[2],key.split('_')[3],key.split('_')[4],key.split('_')[5]])
                                logger4app.debug('Gom Insert thanh cong hoa don id : ' +id )
                            }
                        }
                    }
                    if(col==1){
                        rs=    await dbs.query(`select 0 chk,[ID]
                        ,[refNo]
                        ,FORMAT (valueDate, 'dd/MM/yyyy') valueDate
                        ,[customerID]
                        ,[customerName]
                        ,[taxCode]
                        ,[customerAddr]
                        ,[isSpecial]
                        ,trantype
                        ,[curr]
                        ,[exrt]
                        ,[vrt]
                        ,[chargeAmount]
                        ,[vcontent]
                        ,[amount]
                        ,[vat]
                        ,status
                        ,price	  
                        ,quantity	  
                        ,[total]
                        from s_trans where  1=1 ${where} order by customerID,vrt,curr,trantype,isSpecial,taxCode,valueDate`, [])
                        length = rs.recordset.length
                        let line =1
                        for (const r of rs.recordset) {
                            if (curr !=r.curr){
                                let  sql = `select id,cur,val,dt from s_ex  where dt between @1 and @2 and cur = @3`
                                let  result = await dbs.query(sql, [ moment(body.idt).format("YYYY-MM-DD HH:mm:ss"), moment(body.idt).endOf("day").format("YYYY-MM-DD HH:mm:ss"),r.curr])
                                  if(result.recordset.length<1) throw new Error("Không tìm thấy tỷ giá cho ngày: "+ moment(body.idt).format("DD-MM-YYYY"))
                                 exrt = result.recordset[0].val
                            }
                            curr =r.curr
                            count++
                            invd = objectMapper(r, Obj)
                           
                            let item=invd.items
                            let keyc = r.isSpecial + '_' + r.customerID +'_'+ r.vrt +'_'+ r.curr +'_'+ r.trantype +'_'+ r.taxCode +'_'+ r.valueDate 
                            if(key == keyc){
                                sum = sum + item.amount
                                sumv = Math.round(sum * Number(exrt))
                                if(item.vat){
                                    vat = vat + item.vat
                                    vatv = Math.round(vat * Number(exrt))
                                }
                                total = total + item.total
                                totalv = Math.round(total * Number(exrt))
                                item.line=line++
                                items.push(item)
                               
                            }else{
                                line=1
                                if(count==1){
                                    invc = invd
                                    invc.exrt=exrt
                                    sum = sum + item.amount
                                    sumv = Math.round(sum * Number(exrt))
                                    if(item.vat){
                                        vat = vat + item.vat
                                        vatv = Math.round(vat * Number(exrt))
                                    }
                                    total = total + item.total
                                    totalv = Math.round(total * Number(exrt))
                                    items.push(item)
                                }else{

                                 
                                    let id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC,id)
                                    invc.items = items
                                    invc.tax = await tax(items, Number(invc.exrt))
                                    invc.sum = sum
                                    invc.sumv = sumv
                                    invc.vat = vat
                                    invc.vatv = vatv
                                    invc.total = total
                                    invc.totalv = totalv
                                    invc.word = n2w.n2w(totalv, invc.curr)
                                   
                                    invc.stax = taxc
                                    invc.form = rssr.recordset[0].form
                                    invc.serial = rssr.recordset[0].serial
                                    invc.type = rssr.recordset[0].type
                                    invc.sec = sec
                                    invc.status = 1
                                    invc.idt = moment(body.idt).format("YYYY-MM-DD HH:mm:ss")
                                    invc.id = id
                                    invc.stax = org.taxc
                                    invc.sname = org.name
                                    invc.saddr = org.addr
                                    invc.smail = org.mail
                                    invc.stel = org.tel
                                    invc.taxo = org.taxo
                                    invc.sacc = org.acc
                                    invc.sbank = org.bank
                                    await dbs.query(`insert into s_inv(id,sec,idt,ou,uc,doc) values (@1,@2,@3,@4,@5,@6)`, [id, sec, moment(body.idt).format("YYYY-MM-DD HH:mm:ss"), ou, uid, JSON.stringify(invc)])
                                    //await dbs.query(`update  s_trans set stattus_temp=status  where 1=1 ${where} and (isSpecial+'_'+customerID+'_'+vrt+'_'+curr+'_'+CONVERT(varchar(10),trantype)+'_'+taxCode)=@1`, [ key])
                                    //await dbs.query(`update  s_trans set status=1,inv_id =@1,exrt=@2,trancol=@4,ma_nv=@5,form=@6,serial=@7,inv_date=@8 where 1=1 ${where} and (isSpecial+'_'+customerID+'_'+vrt+'_'+curr+'_'+CONVERT(varchar(10),trantype)+'_'+taxCode+'_'+ FORMAT (valueDate, 'dd/MM/yyyy') )=@3`, [id,invc.exrt, key,0,name,invc.form,invc.serial,moment(body.idt).format("YYYY-MM-DD")])
                                    await dbs.query(`update  s_trans set stattus_temp=status,status=1,inv_id =@1,exrt=@2,trancol=@3,ma_nv=@4,form=@5,serial=@6,inv_date=@7  where 1=1 ${where} and isSpecial =@8 and customerID=@9 and vrt=@10 and curr=@11 and trantype=@12 and taxCode=@13 and  FORMAT (valueDate, 'dd/MM/yyyy')=@14`, [id,invc.exrt,1,name,invc.form,invc.serial,moment(body.idt).format("YYYY-MM-DD"), key.split('_')[0], key.split('_')[1],key.split('_')[2],key.split('_')[3],key.split('_')[4],key.split('_')[5],key.split('_')[6]])
                                    //r.isSpecial + '_' + r.customerID +'_'+ r.vrt +'_'+ r.curr +'_'+ r.trantype +'_'+ r.taxCode +'_'+ r.valueDate
                                    logger4app.debug('Gom Insert thanh cong hoa don id : ' +id )
                                    invc = invd
                                    invc.exrt=exrt
                                    items =[]
                                    sum = 0
                                    sumv = 0
                                    vat = 0 
                                    vatv = 0
                                    total = 0
                                    totalv = 0
                                    sum = sum + item.amount
                                    sumv = Math.round(sum * Number(exrt))
                                    if(item.vat){
                                        vat = vat + item.vat
                                        vatv = Math.round(vat * Number(exrt))
                                    }
                                    total = total + item.total
                                    totalv = Math.round(total * Number(exrt))
                                    item.line=line++
                                    items.push(item)
                                }
                               
                            }
                            key = keyc
                            if (length==count){
                              
                                let id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC,id)
                                invc.items = items
                                invc.tax = await tax(items, Number(invc.exrt))
                                invc.sum = sum
                                invc.sumv = sumv
                                invc.vat = vat
                                invc.vatv = vatv
                                invc.total = total
                                invc.totalv = totalv
                                invc.word = n2w.n2w(totalv, invc.curr)
                               
                                invc.stax = taxc
                                invc.form = rssr.recordset[0].form
                                invc.serial = rssr.recordset[0].serial
                                invc.type = rssr.recordset[0].type
                                invc.sec = sec
                                invc.status = 1
                                invc.idt = moment(body.idt).format("YYYY-MM-DD HH:mm:ss")
                                invc.id = id
                                invc.stax = org.taxc
                                invc.sname = org.name
                                invc.saddr = org.addr
                                invc.smail = org.mail
                                invc.stel = org.tel
                                invc.taxo = org.taxo
                                invc.sacc = org.acc
                                invc.sbank = org.bank
                                await dbs.query(`insert into s_inv(id,sec,idt,ou,uc,doc) values (@1,@2,@3,@4,@5,@6)`, [id, sec, moment(body.idt).format("YYYY-MM-DD HH:mm:ss"), ou, uid, JSON.stringify(invc)])
                                //await dbs.query(`update  s_trans set stattus_temp=status  where 1=1 ${where} and (isSpecial+'_'+customerID+'_'+vrt+'_'+curr+'_'+CONVERT(varchar(10),trantype)+'_'+taxCode)=@1`, [ key])
                               // await dbs.query(`update  s_trans set status=1,inv_id =@1,exrt=@2,trancol=@4,ma_nv=@5,form=@6,serial=@7,inv_date=@8 where 1=1 ${where} and (isSpecial+'_'+customerID+'_'+vrt+'_'+curr+'_'+CONVERT(varchar(10),trantype)+'_'+taxCode+'_'+ FORMAT (valueDate, 'dd/MM/yyyy') )=@3`, [id,invc.exrt, key,0,name,invc.form,invc.serial,moment(body.idt).format("YYYY-MM-DD")])
                               await dbs.query(`update  s_trans set stattus_temp=status,status=1,inv_id =@1,exrt=@2,trancol=@3,ma_nv=@4,form=@5,serial=@6,inv_date=@7  where 1=1 ${where} and isSpecial =@8 and customerID=@9 and vrt=@10 and curr=@11 and trantype=@12 and taxCode=@13 and  FORMAT (valueDate, 'dd/MM/yyyy')=@14`, [id,invc.exrt,1,name,invc.form,invc.serial,moment(body.idt).format("YYYY-MM-DD"), key.split('_')[0], key.split('_')[1],key.split('_')[2],key.split('_')[3],key.split('_')[4],key.split('_')[5],key.split('_')[6]])
                                logger4app.debug('Gom Insert thanh cong hoa don id : ' +id )
                            }
                        }
                    }
                    status = 1
                    message ='Success'
                } catch (e) {
                    status = 2
                    throw e

      
               }

            res.json({ message: message, status: status })
        }
        catch (err) {
            next(err)
        }
    },
    rep: async (req, res, next) => {
        try {
            const token = SEC.decode(req)
            if (grant && token.ou !== token.u) return next(new Error(`Không có quyền sửa dữ liệu của đơn vị khác \n (Denied access for editing data of another branches)`))
            const  body = req.body, taxc = token.taxc, uid = token.uid, ou = token.ou, name=token.uid,org = await ous.obt(taxc),
                   invs = body.invs,
                   param = body.param  , 
                   exrt  = param.exrtr    
               
            let Obj = {
                "customerID": "c9","customerAddr": "baddr","trantype":"c0"
                , "customerName": "bname", "taxCode": "btax", "curr": "curr", "exrt": "exrt"
                , "amount": "items.amount", "vcontent": "items.name",  "vrt": "items.vrt","valueDate": "items.c0","refNo": "items.c1","chargeAmount": "items.c2","vat": "items.vat","total": "items.total","price": "items.price","quantity": "items.quantity"
                };
         
            let status, message,rs,invd,items =[],rssr, where='',invc={},sum=0,sumv=0,vat=0,vatv=0,total=0,totalv=0,rsc,trantype='',ids='',rsinv

                try {
                  
                    let str =''
                    for (const inv of invs) {
                      
                        str += `${inv.ID},`
                    }
                    where += ` and ID in (${str.slice(0, -1)})`

                    
                    rssr =  await dbs.query(`select distinct form,serial,type from s_serial where taxc=@1  and fd<=@2 and status=1`, [taxc, moment(new Date()).format("YYYY-MM-DD HH:mm:ss")])
                    if (rssr.recordset.length<1) throw new Error("Không tìm thấy dải ký hiệu đang hiệu lực ")
                 
                    rsc = await dbs.query(`select count(*) rc from s_inv where idt>@1 and form=@2 and serial=@3 and status>1 and status<>4`, [moment(param.idtr).endOf('day').toDate(), rssr.recordset[0].form, rssr.recordset[0].serial])
                  
                    if (rsc.recordset[0].rc > 0) throw new Error(`Tồn tại HĐ có ngày lớn hơn ${moment(param.idtr).format('DD/MM/YYYY')} và đã cấp số \n (Invoice has a date larger than ${moment(param.idtr).format('DD/MM/YYYY')} and issued a number)`)
                   
                  
                      
                        //let  sql = `select id,cur,val,dt from s_ex  where dt between @1 and @2 and cur = @3`
                        // let  result = await dbs.query(sql, [new Date(param.idtr), new Date(moment(param.idtr).endOf("day")),param.currr])
                        // if(result.recordset.length<1) throw new Error("Không tìm thấy tỷ giá cho ngày: "+ moment(param.idtr).format("DD-MM-YYYY"))
                        //  exrt = result.recordset[0].val
                        rs =    await dbs.query(`select 0 chk,[ID]
                        ,[refNo]
                        ,FORMAT (valueDate, 'dd/MM/yyyy') valueDate
                        ,[customerID]
                        ,[customerName]
                        ,[taxCode]
                        ,[customerAddr]
                        ,[isSpecial]
                        ,trantype
                        ,[curr]
                        ,[exrt]
                        ,[vrt]
                        ,[chargeAmount]
                        ,[vcontent]
                        ,[amount]
                        ,[vat]
                        ,status
                        ,price
                        ,quantity
                        ,[total],
                        form,
                        serial,
                        seq,
                        inv_id
                        from s_trans where  1=1 ${where} order by form,
                        serial,
                        seq`, [])
                       // length = rs.recordset.length
                        let line =1
                        for (const r of rs.recordset) {
                            if (r.status==3 || r.status==4)      ids += `${r.inv_id},`
                        
                            trantype = r.trantype
                            invd = objectMapper(r, Obj)
                           
                            let item = invd.items
                         
                            sum = sum + item.amount
                            sumv = Math.round(sum * Number(exrt))
                            if(item.vat){
                                vat = vat + item.vat
                                vatv = Math.round(vat * Number(exrt))
                            }
                            total = total + item.total
                            totalv = Math.round(total * Number(exrt))
                            item.line=line++
                            items.push(item)
                     
                        }
                        ids = `${ids.slice(0, -1)}`
                        //lay thong tin hoa don goc
                        rsinv = await dbs.query(`select * from s_inv where id in (${ids})`, [])


                        let id = await redis.incr("INC"), sec = util.generateSecCode(config.FNC_GEN_SEC,id)
                        invc.items = items
                        invc.tax = await tax(items, Number(exrt))
                        invc.sum = sum
                        invc.sumv = sumv
                        invc.vat = vat
                        invc.vatv = vatv
                        invc.total = total
                        invc.totalv = totalv
                        invc.word = n2w.n2w(totalv, param.currr)
                        
                        invc.stax = taxc
                        invc.ou = ou
                        invc.uc =name
                        invc.btax =rs.recordset[0].taxCode
                        invc.curr =rs.recordset[0].curr
                        invc.baddr =rs.recordset[0].customerAddr
                        invc.bname =rs.recordset[0].customerName
                        invc.form = rssr.recordset[0].form
                        invc.serial = rssr.recordset[0].serial
                        invc.type = rssr.recordset[0].type
                        invc.sec = sec
                        invc.status = 1
                        invc.exrt=exrt
                        invc.idt = moment(body.idt).format("YYYY-MM-DD HH:mm:ss")
                        invc.pid=ids
                        invc.id=id
                        invc.c9=rs.recordset[0].customerID
                        
                        invc.stax = org.taxc
                        invc.sname = org.name
                        invc.saddr = org.addr
                        invc.smail = org.mail
                        invc.stel = org.tel
                        invc.taxo = org.taxo
                        invc.sacc = org.acc
                        invc.sbank = org.bank
                        invc.note = param.note
                        const adj = { seq: `${rsinv.recordset[0].form}-${rsinv.recordset[0].serial}-${rsinv.recordset[0].seq}`, ref: rsinv.recordset[0].id, rdt: moment(new Date()).format("YYYY-MM-DD"), idt: moment(rsinv.recordset[0].idt).format("YYYY-MM-DD"), typ: 1, des:param.note }
                        invc.adj = adj
                        const root = { sec: rsinv.recordset[0].sec, form: rsinv.recordset[0].form, serial: rsinv.recordset[0].serial, seq: rsinv.recordset[0].seq, idt: moment(rsinv.recordset[0].idt).format("YYYY-MM-DD"), btax: rsinv.recordset[0].btax, bname: rsinv.recordset[0].bname }
                        invc.root = root
                        await dbs.query(`insert into s_inv(id,sec,idt,ou,uc,doc) values (@1,@2,@3,@4,@5,@6)`, [id, sec, moment(body.idt).format("YYYY-MM-DD HH:mm:ss"), ou, uid, JSON.stringify(invc)])
                        //await dbs.query(`update  s_trans set stattus_temp=status  where 1=1 ${where} `, [])
                        await dbs.query(`update  s_trans set inv_id_tmp = inv_id, stattus_temp=status,status=1,inv_id =@1,exrt=@2,trancol=2,ma_nv=@4,form=@5,serial=@6,inv_date=@7 where 1=1 ${where} `, [id,exrt,trantype,name,invc.form,invc.serial, moment(body.idt).format("YYYY-MM-DD")])
                        

                        logger4app.debug('Gom Thay the thanh cong hoa don id : ' +id )
                   
                    
                    status = 1
                    message ='Success'
                } catch (e) {
                    status = 2
                    throw e

      
               }

            res.json({ message: message, status: status })
        }
        catch (err) {
            next(err)
        }
    },
    del: async (req, res, next) => {
        try {
            const token = SEC.decode(req), u = token.u, query = req.body
        
            let   where = `where valueDate between @1 and @2 and trantype=@3`, binds = [moment(new Date(query.fdp)).format(dtf), moment(moment(query.tdp).endOf("day")).format(dtf),query.trantype]
          
            const result = await dbs.query(`delete from s_trans where valueDate between @1 and @2 and trantype=@3 and status=0`, binds)
           
          //  const sSysLogs = { fnc_id: 'trans_del', src: config.SRC_LOGGING_DEFAULT, dtl: `Xóa giao dich tu ngay ${moment(new Date(query.fdp)).format(dtf)} den ${moment(new Date(query.tdp)).format(dtf)}`, msg_id: '', doc: '' };
         //   logging.ins(req, sSysLogs, next)
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },
    syn: async (req, res, next) => {
        try {
            const token = SEC.decode(req), u = token.u, query = req.body
            let  resultsdds,prc='',count,rs
            if (query.trantype==0)  prc='proc_VAT_CollectionData4Numbering_Commission'
            else prc='proc_VAT_CollectionData4Numbering_Interest'
            
            resultsdds=await dbs.syn(prc,query.fdp,query.tdp)
            if (resultsdds.recordset.length==0)  res.json(2)

            for (let r of resultsdds.recordset) {
                if(r.IsSpecial) r.IsSpecial=1 
                else r.IsSpecial=0;
                if(r.CategoryName=='5%') r.CategoryName=5
                if(r.CategoryName=='0%') r.CategoryName=0
                 if(r.CategoryName=='10%') r.CategoryName=10
                 if(r.CategoryName=='-') r.CategoryName='-1'
                
                
                if (query.trantype==0)
               {
                rs = await dbs.query(`select tranID,status from  s_trans where tranID=@1`, [r.MonthlyVATDataID])
                
                    if(rs.recordset.length == 0){
                        await dbs.query(`insert into s_trans(
                            refNo,
                            valueDate,
                            customerID,
                            customerName,
                            taxCode,
                            customerAddr,
                            isSpecial,
                            curr,
                            vrt,
                            chargeAmount,
                            vcontent,
                            amount,
                            vat,
                            total,
                            create_date,
                            tranID,
                            trantype,
                            quantity,
                            price,mailGroup,
                            status
                        
                           ) values (@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21)`, [r.ReferenceNo,  moment(r.ValueDate).format("YYYY-MM-DD"), r.CustomerAbbr, r.VATCustomerName,r.TaxCodeNo==null?'':r.TaxCodeNo,
                            r.VATCustomerAddr,r.IsSpecial==null?'':r.IsSpecial,r.ServiceChargeCCYName,r.CategoryName,r.TransactionAmount==null?r.TransactionCCYName:(r.TransactionCCYName+' '+String(numeral(r.TransactionAmount).format('0,0.[00]'))),r.ContentTemplate_VN,r.ServiceChargeAmount==null?null:(r.ServiceChargeAmount).toFixed(config.GRIDNFCONF),(r.VATAmount?r.VATAmount.toFixed(config.GRIDNFCONF):0),(r.ServiceChargeAmount+(r.VATAmount?r.VATAmount:0)).toFixed(config.GRIDNFCONF), moment(new Date()).format("YYYY-MM-DD HH:mm:ss"),
                            r.MonthlyVATDataID,0,r.Quantity,(r.UnitPrice?r.UnitPrice.toFixed(config.GRIDNFCONF):null),r.MailingCodeGroup,0])
                            
                        }else{
                        if(rs.recordset[0].status==0 ){
                            await dbs.query(`delete from  s_trans where tranID=@1`, [r.MonthlyVATDataID])
                            await dbs.query(`insert into s_trans(
                                refNo,
                                valueDate,
                                customerID,
                                customerName,
                                taxCode,
                                customerAddr,
                                isSpecial,
                                curr,
                                vrt,
                                chargeAmount,
                                vcontent,
                                amount,
                                vat,
                                total,
                                create_date,
                                tranID,
                                trantype,
                                quantity,
                                price,mailGroup,
                                status
                            
                                     ) values (@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21)`, [r.ReferenceNo,  moment(r.ValueDate).format("YYYY-MM-DD"), r.CustomerAbbr, r.VATCustomerName,r.TaxCodeNo==null?'':r.TaxCodeNo,
                                r.VATCustomerAddr,r.IsSpecial==null?'':r.IsSpecial,r.ServiceChargeCCYName,r.CategoryName,r.TransactionAmount==null?r.TransactionCCYName:(r.TransactionCCYName+' '+String(numeral(r.TransactionAmount).format('0,0.[00]'))),r.ContentTemplate_VN,r.ServiceChargeAmount==null?null:(r.ServiceChargeAmount).toFixed(config.GRIDNFCONF),(r.VATAmount?r.VATAmount.toFixed(config.GRIDNFCONF):0),(r.ServiceChargeAmount+(r.VATAmount?r.VATAmount:0)).toFixed(config.GRIDNFCONF),moment(new Date()).format("YYYY-MM-DD HH:mm:ss"),
                                r.MonthlyVATDataID,0,r.Quantity,(r.UnitPrice?r.UnitPrice.toFixed(config.GRIDNFCONF):null),r.MailingCodeGroup,0])
                          }
                    }
                
               } 
                else
                   {
                    rs = await dbs.query(`select tranID,status from  s_trans where tranID=@1`, [r.MonthlyVATData4InterestID])
                   
                    if(rs.recordset.length == 0){
                            await dbs.query(`insert into s_trans(
                                refNo,
                                valueDate,
                                customerID,
                                customerName,
                                taxCode,
                                customerAddr,
                                isSpecial,
                                curr,
                                vrt,
                                chargeAmount,
                                vcontent,
                                amount,
                                vat,
                                total,
                                create_date,
                                tranID,
                                trantype,
                                quantity,
                                price,mailGroup,
                                status
                            
                                      ) values (@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21)`, [r.RefNo,  moment(r.ValueDate).format("YYYY-MM-DD"), r.CustomerAbbr, r.VATCustomerName,r.TaxCodeNo==null?'':r.TaxCodeNo, r.VATCustomerAddr,r.IsSpecial==null?'':r.IsSpecial,r.InterestCCY,r.CategoryName,r.DepositLoanAmount==null?r.TransactionCCYName:r.TransactionCCYName+' '+String(numeral(r.DepositLoanAmount).format('0,0.[00]')),r.ContentTemplate_VN,(r.InterestAmount).toFixed(config.GRIDNFCONF),(r.VATAmount?r.VATAmount.toFixed(config.GRIDNFCONF):0),(r.InterestAmount+(r.VATAmount?r.VATAmount:0)).toFixed(config.GRIDNFCONF),moment(new Date()).format("YYYY-MM-DD HH:mm:ss"),  r.MonthlyVATData4InterestID,1,r.Quantity,(r.UnitPrice?r.UnitPrice.toFixed(config.GRIDNFCONF):null),r.MailingCodeGroup,0])
                            }else{
                                if(rs.recordset[0].status==0 ){
                                    await dbs.query(`delete from  s_trans where tranID=@1`, [r.MonthlyVATData4InterestID])
                                    await dbs.query(`insert into s_trans(
                                        refNo,
                                        valueDate,
                                        customerID,
                                        customerName,
                                        taxCode,
                                        customerAddr,
                                        isSpecial,
                                        curr,
                                        vrt,
                                        chargeAmount,
                                        vcontent,
                                        amount,
                                        vat,
                                        total,
                                        create_date,
                                        tranID,
                                        trantype,
                                        quantity,
                                        price,mailGroup,
                                        status
                                    
                                                          ) values (@1,@2,@3,@4,@5,@6,@7,@8,@9,@10,@11,@12,@13,@14,@15,@16,@17,@18,@19,@20,@21)`, [r.RefNo,  moment(r.ValueDate).format("YYYY-MM-DD"), r.CustomerAbbr, r.VATCustomerName,r.TaxCodeNo==null?'':r.TaxCodeNo, r.VATCustomerAddr,r.IsSpecial==null?'':r.IsSpecial,r.InterestCCY,r.CategoryName,r.DepositLoanAmount==null?r.TransactionCCYName:r.TransactionCCYName+' '+String(numeral(r.DepositLoanAmount).format('0,0.[00]')),r.ContentTemplate_VN,(r.InterestAmount).toFixed(config.GRIDNFCONF),(r.VATAmount?r.VATAmount.toFixed(config.GRIDNFCONF):0),(r.InterestAmount+(r.VATAmount?r.VATAmount:0)).toFixed(config.GRIDNFCONF),moment(new Date()).format("YYYY-MM-DD HH:mm:ss"),  r.MonthlyVATData4InterestID,1,r.Quantity,(r.UnitPrice?r.UnitPrice.toFixed(config.GRIDNFCONF):null),r.MailingCodeGroup,0])
                                  }
                            }
                    } 

            }         
         
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },
   
    
    editPost: async (req, res, next) => {
        try {
            const  body = req.body
            let binds, sql
          
                sql = `update s_trans set refNo=@1,customerID=@2,customerName=@3,taxCode=@4,exrt=@5,curr=@6,vrt=@7,chargeAmount=@8,vat=@9,total=@10,vcontent=@11,status=2,amount=@12,mailGroup=@13,price=@14,quantity=@15,customerAddr=@16,isSpecial=@17,valuedate=@18  where id=@19`
                binds =[body.refNo, body.customerID, body.customerName, body.taxCode, body.exrt, body.curr, body.vrt, String(body.chargeAmount), body.vat, body.total,body.vcontent,body.amount,body.mailGroup,body.price,body.quantity,body.customerAddr,body.isSpecial,body.valueDate,body.ID]
                await dbs.query(sql, binds)
               
         
            res.json(1)
        }
        catch (err) {
            next(err)
        }
    },

    rexportD: async (req, res, next) => {
        try {
            
            const body = req.body
            let sql, binds,fn,rows,json,result, status=" ", trantype,rowd=[]
            const fdp=body.fdp, tdp=body.tdp,sts=body.status, tran=body.trantype
            let c0,c1=0,c2=0,c3=0

            logger4app.debug(sts);
            if(tran=='0'){
                trantype='Phí'
            }
            else{
                trantype='Lãi'
            }
            if(sts.length>0){
                for(const t of sts){
                   
                    if(t=='0'){
                        status+='Chờ phát hành,'
                    }
                    else if(t=='1'){
                        status+='Đã phát hành,'
                    }
                    else if(t=='2'){
                        status+='Chờ duyệt,'
                    }
                    else if(t=='3'){
                        status+='Chờ phát hành lại,'
                    }
                    else if(t=='4'){
                        status+='Đã hủy,'
                    }
                }
            }
            else{
                status+="Tất cả,"
            }
            
            let where='1=1 '
            if(sts) where += ` and status in (${sts})`
           
            
            fn="temp/BCExport.xlsx"

            sql=`select curr,vrt,sum(amount) as amount,sum(vat) as vat from s_trans where valueDate between @1 and @2 and ${where} and trantype=@4 group by curr,vrt order by curr ,cast(vrt as int)`
            binds=[fdp,tdp,sts,tran]

            result = await dbs.query(sql, binds)
            rows = result.recordset
            logger4app.debug(rows)
            let lable='',sumv=0,vatv=0
            for(let row of rows){
                if (lable ==''){
                    row.c0 = 'CURRENCY ( Loại tiền)'
                    row.c1 = row.curr
                    row.c2 =''
                    row.c3 =''
                    lable = row.curr
                    rowd.push(row)
                    switch(row.vrt){
                        case "-1":
                            c0  = "-       Not subject to VAT ( Không chịu thuế)"
                            break;
                        case "0":
                            c0  = "-       EPZ customer ( 0%)"
                            break;
                        case "5":
                            c0  = "-       5%"
                           
                            break;
                        case "10":
                            c0  = "-       10%"
                           
                            break
                        default:
                            logger4app.debug("Không có giá trị tương ứng");
                            break;
                    }
                   
                    c1 = ''
                    c2 = row.amount
                    c3 = row.vat
                    rowd.push({c0:c0,c1:c1,c2:c2,c3:c3})
                    sumv += row.amount
                    vatv += row.vat
                }else{
                    if(row.curr != lable){
                        row.c0 = 'TOTAL'
                        row.c1 = lable
                        row.c2 = sumv
                        row.c3 = vatv
                        rowd.push(row)
                        sumv=0
                        vatv=0
                      
                        rowd.push({c0:"",c1:"",c2:"",c3:""})
                       // rowd.push(row)
                      
                        lable = row.curr
                        rowd.push({c0:'CURRENCY ( Loại tiền)',c1:row.curr,c2:"",c3:""})
                        switch(row.vrt){
                            case "-1":
                                c0  = "-       Not subject to VAT ( Không chịu thuế)"
                                break;
                            case "0":
                                c0  = "-       EPZ customer ( 0%)"
                                break;
                            case "5":
                                c0  = "-       5%"
                                break;
                            case "10":
                                c0  = "-       10%"
                                break
                            default:
                                logger4app.debug("Không có giá trị tương ứng");
                                break;
                        }
                       
                        c1 = ''
                        c2 = row.amount
                        c3 = row.vat
                        rowd.push({c0:c0,c1:c1,c2:c2,c3:c3})
                        sumv += row.amount
                        vatv += row.vat
                        //rowd.push(row)
                    }else{
                        switch(row.vrt){
                            case "-1":
                                row.c0  = "-      Not subject to VAT ( Không chịu thuế)"
                                break;
                            case "0":
                                row.c0  = "-       EPZ customer ( 0%)"
                                break;
                            case "5":
                                row.c0  = "-       5%"
                                break;
                            case "10":
                                row.c0  = "-       10%"
                                break
                            default:
                                logger4app.debug("Không có giá trị tương ứng");
                                break;
                        }
                       
                        row.c1 = ''
                        row.c2 = row.amount
                        row.c3 = row.vat
                        rowd.push(row)
                        sumv += row.amount
                        vatv += row.vat
                    }
                }
                
            }
            if (rows.length > 0){
                rowd.push({c0:'TOTAL',c1:lable,c2:sumv,c3:vatv})
            }
            
            // sum=a1+a2+a3+a4
            // sumB=b1+b2+b3+b4
            // sumT=totalF+totalT
            // sumTB=totalFB+totalTB

            // logger4app.debug(status)


            json={fd:moment(fdp).format("DD/MM/YYYY"), ft:moment(tdp).format("DD/MM/YYYY"), trt:trantype, stt: status.slice(0, -1), table:rowd}

            const file = path.join(__dirname, "..", "..", "..", fn)
            const xlsx = fs.readFileSync(file)
            const template = new xlsxtemp(xlsx)
            template.substitute(1, json)
            res.end(template.generate(), "binary")
        }
        catch (err) {
            next(err)
        }
    },
    exportD: async (req, res, next) =>{
        try {
            //console.time("timeget")
            const body = req.body
            let sql, result, rows,fn,json
            const  iwh = await Service.iwh(req, true)
         
            fn="temp/BCExport2_MZH.xlsx"

            sql = `select 0 chk,[ID]
            ,[refNo]
            ,[valueDate]
            ,[customerID]
            ,[customerName]
            ,[taxCode]
            ,[customerAddr]
            ,[isSpecial]
            ,[curr]
            ,[exrt]
            ,[vrt]
            ,ISNULL(chargeAmount, 0) chargeAmount
            ,[vcontent]
            ,[amount]
            ,[vat]
            ,status
            ,[total]
            ,FORMAT (create_date, 'dd/MM/yyyy HH:mm:ss') create_date
            , FORMAT (last_update, 'dd/MM/yyyy HH:mm:ss') last_update
            , FORMAT (inv_date, 'dd/MM/yyyy') inv_date
            , trantype
            ,trancol
            ,price
            ,quantity
            ,mailGroup
            ,[ma_nv]
            ,[ma_ks],
            inv_id
            ,[tranID] from s_trans ${iwh.where} ${iwh.order} `
          //  binds=[body.fd,body.td,body.trantype]
            result = await dbs.query(sql, iwh.binds)
            rows=result.recordset
            for(const row of rows){
                //format valueDate
                row.valueDate=moment(row.valueDate).format("DD/MM/YYYY")

                //config cột loại Kh
                if(row.isSpecial=='1'){
                    row.isSpecial="Special"
                }else row.isSpecial="Normal"

                if(row.trantype=='0'){
                    row.trantype="Phí"
                }else row.trantype="Lãi"

                if(row.trancol=='0'){
                    row.trancol="Tháng"
                }else if(row.trancol=='1') row.trancol="Ngày"
                else if (row.trancol==2)  row.trancol="Xuất lẻ"
                //Config các cột status
                if(row.status=='0'){
                    row.status='Chờ phát hành'
                }
                else if(row.status=='1'){
                    row.status='Đã phát hành'
                }
                else if(row.status=='2'){
                    row.status='Chờ duyệt'
                }
                else if(row.status=='3'){
                    row.status='Chờ phát hành lại'
                }
                else if(row.status=='4'){
                    row.status='Đã hủy'
                }
            }

            json = {table:rows}

            const file = path.join(__dirname, "..", "..", "..", fn)
            const xlsx = fs.readFileSync(file)
            const template = new xlsxtemp(xlsx)
            template.substitute(1, json)
            res.end(template.generate(), "binary")
        }
        catch (err) {
            next(err)
        }
    }
}
module.exports = Service