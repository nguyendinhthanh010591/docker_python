DROP SEQUENCE test_seq_inv
/

CREATE SEQUENCE test_seq_inv INCREMENT BY 1
                          START WITH 40641
                          MINVALUE 1
                          MAXVALUE 9999999999999999999999999999
                          NOCYCLE
                          NOORDER
                          CACHE 20
                          NOKEEP
                          GLOBAL
/

DECLARE
    v_fd          DATE := TO_DATE ('01/01/2020', 'DD/MM/YYYY');
    v_td          DATE := TO_DATE ('01/01/2020', 'DD/MM/YYYY');
    v_recperday   NUMBER := 1;
    v_doc         CLOB
        := '{"bname":"Công ty TNHH Hệ thống thông tin FPT","idt":"#idt#","type":"01GTKT","btax":"0104128565","buyer":"","btel":"","bmail":"","form":"01GTKT0/001","bcode":"FIS","baddr":"Keangnam","bacc":"","serial":"AC/20E","curr":"VND","exrt":1,"paym":"TM","bbank":"","seq":"#seq#","note":"","sumv":0,"sum":0,"score":0,"discount":"","vatv":0,"vat":0,"word":"Không đồng","totalv":0,"total":0,"c0":"1","c1":"0","c2":"","c3":"","c4":"","tax":[{"vrt":"0","vrn":"0%","amt":0,"amtv":0}],"name":"HÓA ĐƠN GIÁ TRỊ GIA TĂNG","items":[{"vrt":"0","quantity":1,"id":1601005985819,"type":"Khuyến mãi","amount":0,"name":"sss","line":1,"vat":0,"total":0,"unit":"cái","price":100000,"vrn":"0%"}],"id":18,"sec":"#sec#","stax":"0100233488","sname":"Ngân Hàng Thương mại Cổ phần Quốc Tế Việt Nam","saddr":"Tầng 1 (tầng Trệt) và Tầng 2 Tòa nhà Sailing Tower, Số 111A Pasteur, Phường Bến Nghé, Quận 1, TP Hồ Chí Minh","smail":null,"stel":null,"taxo":"70100","sacc":null,"sbank":null,"status":3,"uc":"__admin","ou":1,"usercreate":{"id":"__admin","mail":"quangtrungphambk@gmail.com","ou":1,"uc":1,"name":"admin","pos":null,"code":null},"useroucreate":{"id":1,"taxc":"0100233488","taxo":"70100","name":"Ngân Hàng Thương mại Cổ phần Quốc Tế Việt Nam","addr":"Tầng 1 (tầng Trệt) và Tầng 2 Tòa nhà Sailing Tower, Số 111A Pasteur, Phường Bến Nghé, Quận 1, TP Hồ Chí Minh","mail":null,"tel":null,"acc":null,"bank":null,"seq":2,"sign":2,"usr":null,"pwd":"c4246832846055334ded310479102bae0fc4e547ac8b174b35f618d7c11c2aedf2965547983c444a0904c98b1ef7bfd2d34080342d22e7a941fbf31ca8e29a510e2ec591a27a0d3bfffabaf09db2731442acc63a99ea540d338edf3329bba3eebd798fada83e","temp":1}}';
    v_ou          NUMBER := 1;
    v_uc          VARCHAR2 (100) := '__admin';
BEGIN
    prc_gen_data_invoice (v_fd,
                          v_td,
                          v_recperday,
                          v_doc,
                          v_ou,
                          v_uc);
END;
/

DECLARE
    v_td    DATE := TO_DATE ('01-JAN-2020');
    v_ed    DATE := TO_DATE ('31-JAN-2020');
    v_doc   VARCHAR2 (3000)
        := '{"bname":"Công ty TNHH Hệ thống thông tin FPT","idt":"#idt#","type":"01GTKT","btax":"0104128565","buyer":"","btel":"","bmail":"","form":"01GTKT0/001","bcode":"FIS","baddr":"Keangnam","bacc":"","serial":"AC/20E","curr":"VND","exrt":1,"paym":"TM","bbank":"","seq":"#seq#","note":"","sumv":0,"sum":0,"score":0,"discount":"","vatv":0,"vat":0,"word":"Không đồng","totalv":0,"total":0,"c0":"1","c1":"0","c2":"","c3":"","c4":"","tax":[{"vrt":"0","vrn":"0%","amt":0,"amtv":0}],"name":"HÓA ĐƠN GIÁ TRỊ GIA TĂNG","items":[{"vrt":"0","quantity":1,"id":1601005985819,"type":"Khuyến mãi","amount":0,"name":"sss","line":1,"vat":0,"total":0,"unit":"cái","price":100000,"vrn":"0%"}],"id":18,"sec":"#sec#","stax":"0100233488","sname":"Ngân Hàng Thương mại Cổ phần Quốc Tế Việt Nam","saddr":"Tầng 1 (tầng Trệt) và Tầng 2 Tòa nhà Sailing Tower, Số 111A Pasteur, Phường Bến Nghé, Quận 1, TP Hồ Chí Minh","smail":null,"stel":null,"taxo":"70100","sacc":null,"sbank":null,"status":3,"uc":"__admin","ou":1,"usercreate":{"id":"__admin","mail":"quangtrungphambk@gmail.com","ou":1,"uc":1,"name":"admin","pos":null,"code":null},"useroucreate":{"id":1,"taxc":"0100233488","taxo":"70100","name":"Ngân Hàng Thương mại Cổ phần Quốc Tế Việt Nam","addr":"Tầng 1 (tầng Trệt) và Tầng 2 Tòa nhà Sailing Tower, Số 111A Pasteur, Phường Bến Nghé, Quận 1, TP Hồ Chí Minh","mail":null,"tel":null,"acc":null,"bank":null,"seq":2,"sign":2,"usr":null,"pwd":"c4246832846055334ded310479102bae0fc4e547ac8b174b35f618d7c11c2aedf2965547983c444a0904c98b1ef7bfd2d34080342d22e7a941fbf31ca8e29a510e2ec591a27a0d3bfffabaf09db2731442acc63a99ea540d338edf3329bba3eebd798fada83e","temp":1}}';
BEGIN
    FOR d IN 0 .. 365
    LOOP
        --DBMS_OUTPUT.put_line (TO_DATE ('01-JAN-2020') + d);
        INSERT INTO s_inv (id,
                           sec,
                           idt,
                           doc,
                           dt,
                           ou,
                           uc)
            SELECT test_seq_inv.NEXTVAL,
                   LPAD (standard_hash (test_seq_inv.CURRVAL, 'MD5'),
                         10,
                         'H'),
                   TO_DATE ('01-JAN-2020') + d,
                   REPLACE (
                       REPLACE (
                           REPLACE (
                               v_doc,
                               '#sec#',
                               LPAD (
                                   standard_hash (test_seq_inv.CURRVAL,
                                                  'MD5'),
                                   10,
                                   'H')),
                           '#seq#',
                           LPAD (TO_NUMBER (seq) + 40000 * (d + 1), 7, '0')),
                       '#idt#',
                       TO_CHAR (TO_DATE ('01-JAN-2020') + d, 'YYYY-MM-DD')),
                   TO_DATE ('01-JAN-2020') + d,
                   1,
                   '__admin'
              FROM s_inv
             WHERE     idt = TO_DATE ('01-JAN-2020')
                   AND stax = '0100233488'
                   AND TYPE = '01GTKT'
                   AND serial = 'LQ/19E'
                   AND TO_NUMBER (seq) <= 40000;

        COMMIT;
        EXIT WHEN TO_DATE ('01-JAN-2020') + d > TO_DATE ('31-JUL-2020');
    END LOOP;
END;