CREATE TABLE [s_xls_temp] ([id] [varchar](20), [ic] [varchar](50), [idt] [datetime], [form] [varchar](11), [serial] [varchar](10), [seq] [varchar](8), [dt] [datetime] CONSTRAINT [DF__s_xls_temp__dt__5AB9788F] DEFAULT GETDATE() NOT NULL);

CREATE TABLE [s_inv_adj] ([id] [bigint] IDENTITY (1, 1) NOT NULL, [inv_id] [bigint], [form] [int], [serial] [nvarchar](10), [stax] [nvarchar](20), [type] [int], [note] [nvarchar](MAX), [so_tb] [nvarchar](50), [ngay_tb] [date], [createdate] [datetime], [statuscqt] [int], [msgcqt] [nvarchar](MAX), [listinv_id] [bigint], [th_type] [int] NOT NULL, [seq] [nvarchar](8), CONSTRAINT [PK__s_inv_ad__3213E83FA7E7893B] PRIMARY KEY ([id]));

CREATE TABLE [s_listvalues] ([keyname] [varchar](60) NOT NULL, [keyvalue] [nvarchar](MAX) NOT NULL, [keytype] [varchar](30) NOT NULL);

CREATE TABLE [info_taxpayer] ([id] [numeric](18, 0) IDENTITY (0, 1) NOT NULL, [taxc] [nvarchar](14) NOT NULL, [namecom] [nvarchar](255), [type] [nvarchar](10), [status] [numeric](18, 0), [effectdate] [datetime], [managercomc] [nvarchar](10), [chapter] [nvarchar](10), [changedate] [datetime], [economictype] [nvarchar](10), [contbusires] [nvarchar](10), [passport] [nvarchar](20), [createdby] [nvarchar](150) NOT NULL, [statusscan] [numeric](18, 0) NOT NULL, [messres] [nvarchar](255) NOT NULL, [statuscoderes] [numeric](18, 0) NOT NULL, [createddate] [datetime], [updateddate] [datetime]);

CREATE TABLE [type_invoice] ([id] [numeric](18, 0) IDENTITY (0, 1) NOT NULL, [name] [nvarchar](255));

CREATE TABLE [van_einv_listid_msg] ([id] [bigint] IDENTITY (1, 1) NOT NULL, [pid] [bigint], [type] [nvarchar](10), [status_scan] [int], [createdate] [datetime] CONSTRAINT [DF_van_einv_listid_msg_createdate] DEFAULT GETDATE(), [sendvandate] [datetime], [msg] [nvarchar](MAX), [xml] [nvarchar](MAX), [parent_id] [nchar](10));

CREATE TABLE [s_inv_sum] ([idt] [date] NOT NULL, [stax] [varchar](20) NOT NULL, [type] [varchar](6) NOT NULL, [form] [varchar](11) NOT NULL, [serial] [varchar](8), [i0] [numeric](18, 0), [i1] [numeric](18, 0), [i2] [numeric](18, 0), [i3] [numeric](18, 0), [i4] [numeric](18, 0));

CREATE TABLE [s_dbs_mail_seq] ([id] [int] IDENTITY (1, 1) NOT NULL, [seq_date] [int], [used_number] [int] CONSTRAINT [DF__s_dbs_mai__used___38EE7070] DEFAULT 0, CONSTRAINT [PK__s_dbs_ma__3213E83F46956F3E] PRIMARY KEY ([id]));

CREATE TABLE [s_statement](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[type] [nvarchar](15) NULL,
	[formname] [nvarchar](100) NULL,
	[regtype] [int] NULL,
	[sname] [nvarchar](400) NULL,
	[stax] [nvarchar](14) NULL,
	[taxoname] [nvarchar](100) NULL,
	[taxo] [nvarchar](5) NULL,
	[contactname] [nvarchar](50) NULL,
	[contactaddr] [nvarchar](400) NULL,
	[contactemail] [nvarchar](50) NULL,
	[contactphone] [nvarchar](20) NULL,
	[place] [nvarchar](50) NULL,
	[createdt] [datetime] NULL,
	[hascode] [int] NULL,
	[sendtype] [int] NULL,
	[dtsendtype] [nvarchar](20) NULL,
	[invtype] [nvarchar](100) NULL,
	[doc] [nvarchar](max) NULL,
	[status] [int] NOT NULL,
	[cqtstatus] [int] NULL,
	[vdt] [datetime] NULL,
	[cqtmess] [nvarchar](255) NULL,
	[xml] [nvarchar](max) NULL,
	[statustvan] [int] NOT NULL,
	[xml_received] [nvarchar](max) NULL,
	[xml_accepted] [nvarchar](max) NULL,
	[paxoname] [nvarchar](100) NULL,
	[paxo] [nvarchar](5) NULL,
	[json_received] [nvarchar](max) NULL,
	[json_accepted] [nvarchar](max) NULL,
	[uc]  AS (CONVERT([nvarchar](20),json_value([doc],'$.uc'))),
 CONSTRAINT [s_statement_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
CREATE TABLE [temp_taxpayer] ([id] [numeric](18, 0) IDENTITY (0, 1) NOT NULL, [taxc] [nvarchar](14) NOT NULL, [createdby] [nvarchar](150) NOT NULL, [statusscan] [numeric](18, 0) NOT NULL, [creaateddate] [datetime], [updateddate] [datetime]);

CREATE TABLE [van_msg_out] ([id] [numeric](18, 0) IDENTITY (0, 1) NOT NULL, [invid] [nvarchar](36) NOT NULL, [xml2base64] [nvarchar](MAX) NOT NULL, [createdby] [nvarchar](150) NOT NULL, [statustvan] [numeric](18, 0), [createddate] [datetime], [senddate] [datetime] CONSTRAINT [DF__Van_msg_o__sendd__756D6ECB] DEFAULT GETDATE(), [idtypeinv] [numeric](18, 0) NOT NULL, [messsendcode] [nvarchar](10));

CREATE TABLE [van_history] ([id] [numeric](36, 0) IDENTITY (1, 1) NOT NULL, [type_invoice] [numeric](18, 0) NOT NULL, [id_ref] [bigint], [status] [numeric](18, 0), [datetime_trans] [datetime], [description] [nvarchar](MAX));

DECLARE @TableName SYSNAME set @TableName = N'van_history'; DECLARE @FullTableName SYSNAME set @FullTableName = N'dbo.van_history'; DECLARE @ColumnName SYSNAME set @ColumnName = N'type_invoice'; DECLARE @MS_DescriptionValue NVARCHAR(3749); SET @MS_DescriptionValue = N'1: Bảng tổng hợp hóa đơn, 2: Hóa đơn không mã, 3: Hóa đơn có mã, 4: Thông báo sai xót, 5: Tờ khai đăng ký';DECLARE @MS_Description NVARCHAR(3749) set @MS_Description = NULL; SET @MS_Description = (SELECT CAST(Value AS NVARCHAR(3749)) AS [MS_Description] FROM sys.extended_properties AS ep WHERE ep.major_id = OBJECT_ID(@FullTableName) AND ep.minor_id=COLUMNPROPERTY(ep.major_id, @ColumnName, 'ColumnId') AND ep.name = N'MS_Description'); IF @MS_Description IS NULL BEGIN EXEC sys.sp_addextendedproperty @name  = N'MS_Description', @value = @MS_DescriptionValue, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = @TableName, @level2type = N'COLUMN', @level2name = @ColumnName; END ELSE BEGIN EXEC sys.sp_updateextendedproperty @name  = N'MS_Description', @value = @MS_DescriptionValue, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = @TableName, @level2type = N'COLUMN', @level2name = @ColumnName; END;

CREATE TABLE [van_msg_out_dtl] ([id] [numeric](18, 0) IDENTITY (0, 1) NOT NULL, [iddatatvan] [numeric](18, 0) NOT NULL, [invid] [nvarchar](36) NOT NULL, [createdby] [nvarchar](150) NOT NULL, [statustvan] [numeric](18, 0), [statusres] [numeric](18, 0), [messsendcode] [nvarchar](10), [messsend] [nvarchar](MAX), [messres] [nvarchar](255), [messrescode] [nvarchar](10), [senddate] [datetime], [resdate] [datetime], [transid] [nvarchar](225), [idtypeinv] [numeric](18, 0) NOT NULL, [status_response] [int] CONSTRAINT [DF__van_msg_o__statu__0B27A5C0] DEFAULT 0, [datecheckmsg] [datetime] CONSTRAINT [DF__van_msg_o__datec__0C1BC9F9] DEFAULT GETDATE());

CREATE TABLE [van_msg_in_dtl](
	[id] [numeric](18, 0) IDENTITY(0,1) NOT NULL,
	[receivejson] [nvarchar](max) NOT NULL,
	[taxc]  AS (CONVERT([nvarchar](max),json_value([receivejson],'$.mst'))),
	[namenotice]  AS (CONVERT([nvarchar](500),json_value([receivejson],'$.tenTBao'))),
	[typenotice]  AS (CONVERT([nvarchar](255),json_value([receivejson],'$.loaiTBao'))),
	[contentnotice]  AS (CONVERT([nvarchar](max),json_value([receivejson],'$.ndungTBao'))),
	[createddtnotice]  AS (CONVERT([datetime],json_value([receivejson],'$.ngayTaoTBao'))),
	[createddate] [datetime] NULL,
	[status_scan] [numeric](18, 0) NULL,
	[updateddate] [datetime] NULL,
	[transid] [nvarchar](225) NULL,
	[status_response] [int] NULL,
	[datecheckmsg] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

CREATE TABLE [sign_xml] ([id] [numeric](18, 0) IDENTITY (0, 1) NOT NULL, [tax_code] [nvarchar](14), [type_xml] [nvarchar](20), [xmlOriginal] [nvarchar](MAX) NOT NULL, [xmlSigned] [nvarchar](MAX), [createddate] [datetime], [updateddate] [datetime]);

CREATE TABLE [s_list_inv_id] ([id] [bigint], [inv_id] [bigint], [loai] [varchar](50));

CREATE TABLE [s_user_pass] ([user_id] [nvarchar](20) NOT NULL, [pass] [varchar](200) NOT NULL, [change_date] [datetime] NOT NULL, CONSTRAINT [PK__s_user_p__518C386C17C39B87] PRIMARY KEY ([user_id], [pass]));

CREATE TABLE [van_msg_in] ([id] [numeric](18, 0) IDENTITY (0, 1) NOT NULL, [receive_arr_json] [nvarchar](MAX) NOT NULL, [status_scan] [numeric](18, 0), [createddate] [datetime], [updateddate] [datetime], [createby] [nvarchar](50), [err_msg] [nvarchar](MAX));

DECLARE @TableName SYSNAME set @TableName = N'van_msg_in'; DECLARE @FullTableName SYSNAME set @FullTableName = N'dbo.van_msg_in'; DECLARE @ColumnName SYSNAME set @ColumnName = N'status_scan'; DECLARE @MS_DescriptionValue NVARCHAR(3749); SET @MS_DescriptionValue = N'0: not scan, pending: 3, success: 1, error: 2';DECLARE @MS_Description NVARCHAR(3749) set @MS_Description = NULL; SET @MS_Description = (SELECT CAST(Value AS NVARCHAR(3749)) AS [MS_Description] FROM sys.extended_properties AS ep WHERE ep.major_id = OBJECT_ID(@FullTableName) AND ep.minor_id=COLUMNPROPERTY(ep.major_id, @ColumnName, 'ColumnId') AND ep.name = N'MS_Description'); IF @MS_Description IS NULL BEGIN EXEC sys.sp_addextendedproperty @name  = N'MS_Description', @value = @MS_DescriptionValue, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = @TableName, @level2type = N'COLUMN', @level2name = @ColumnName; END ELSE BEGIN EXEC sys.sp_updateextendedproperty @name  = N'MS_Description', @value = @MS_DescriptionValue, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = @TableName, @level2type = N'COLUMN', @level2name = @ColumnName; END;

CREATE TABLE [s_list_inv] ([id] [bigint] NOT NULL, [listinv_code] [nvarchar](15), [s_tax] [nvarchar](14) NOT NULL, [ou] [int] NOT NULL, [created_date] [datetime] CONSTRAINT [DF_s_list_inv_created_date] DEFAULT GETDATE() NOT NULL, [created_by] [nvarchar](50), [approve_date] [datetime], [approve_by] [nvarchar](20), [s_name] [nvarchar](400) NOT NULL, [item_type] [nvarchar](20) NOT NULL, [listinvoice_num] [nvarchar](5), [period_type] [nvarchar](1) NOT NULL, [period] [nvarchar](10) NOT NULL, [status] [int] CONSTRAINT [DF_s_list_inv_status] DEFAULT 0 NOT NULL, [doc] [nvarchar](MAX), [xml] [nvarchar](MAX), [times] [int] CONSTRAINT [DF__s_list_in__times__3BFFE745] DEFAULT 0 NOT NULL, [status_received] [int], [xml_received] [nvarchar](MAX), [json_received] [nvarchar](MAX), [edittimes] [int], [error_msg_van] [nvarchar](MAX), CONSTRAINT [s_list_inv_pk] PRIMARY KEY ([id]));

drop table s_list_inv_cancel 
CREATE TABLE [s_list_inv_cancel](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[inv_id] [int] NOT NULL,
	[doc] [nvarchar](max) NOT NULL,
	[invtype] [int] NOT NULL,
	[ou] [int] NOT NULL,
	[serial]  AS (CONVERT([nvarchar](6),json_value([doc],'$.serial'))),
	[type]  AS (CONVERT([nvarchar](10),json_value([doc],'$.type'))),
	[idt]  AS (CONVERT([datetime],json_value([doc],'$.idt'))),
	[cdt] [datetime] NOT NULL,
	[listinv_id] [bigint] NULL,
	[stax]  AS (CONVERT([nvarchar](14),json_value([doc],'$.stax'))),
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]


CREATE NONCLUSTERED INDEX [s_inv_adj_idt_idx] ON [s_inv_adj]([createdate], [stax]);

CREATE NONCLUSTERED INDEX [s_list_inv_cancel_cdt_ou] ON [s_list_inv_cancel]([cdt], [ou]);

CREATE NONCLUSTERED INDEX [s_inv_idt_idx] ON [s_inv]([idt], [stax]);

CREATE NONCLUSTERED INDEX [s_inv_listinvid_idx] ON [s_inv]([list_invid]);

CREATE NONCLUSTERED INDEX [s_inv_ou_idx] ON [s_inv]([ou]);

CREATE UNIQUE CLUSTERED INDEX [s_listvalues_uk] ON [s_listvalues]([keyname]);



ALTER TABLE [s_role] ADD CONSTRAINT [s_role_fk01] FOREIGN KEY ([pid]) REFERENCES [s_role] ([id]) ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE NONCLUSTERED INDEX [van_msg_out_date_idx] ON [van_msg_out]([createddate]);

CREATE UNIQUE NONCLUSTERED INDEX [S_ORG_UID_01] ON [s_org]([vtaxc]);

CREATE UNIQUE NONCLUSTERED INDEX [S_ORG_UID_02] ON [s_org]([code]);

CREATE NONCLUSTERED INDEX [van_history_id_ref_idx] ON [van_history]([id_ref]);

CREATE NONCLUSTERED INDEX [van_msg_out_dtl_status_response_idx] ON [van_msg_out_dtl]([status_response]);

CREATE NONCLUSTERED INDEX [van_msg_out_dtl_tranid_idx] ON [van_msg_out_dtl]([transid]);

CREATE NONCLUSTERED INDEX [NonClusteredIndex_status_typenotice] ON [van_msg_in_dtl]([typenotice], [status_scan]);

CREATE NONCLUSTERED INDEX [van_msg_in_dtl_status_idx] ON [van_msg_in_dtl]([status_scan]);

CREATE NONCLUSTERED INDEX [van_msg_in_dtl_tranid_idx] ON [van_msg_in_dtl]([transid]);

CREATE NONCLUSTERED INDEX [S_LIST_INV_ID_IDX] ON [s_list_inv_id]([id]);

ALTER TABLE [s_user_pass] ADD CONSTRAINT [FK__s_user_pa__user___49CEE3AF] FOREIGN KEY ([user_id]) REFERENCES [s_user] ([id]) ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE NONCLUSTERED INDEX [van_msg_in_statsu_idx] ON [van_msg_in]([status_scan]);

CREATE NONCLUSTERED INDEX [s_list_inv_idx] ON [s_list_inv]([s_tax], [created_date]);

ALTER TABLE [s_role] ADD [pid] [int];

ALTER TABLE [s_role] ADD [sort] [nvarchar](10);

ALTER TABLE [s_role] ADD [active] [tinyint];

ALTER TABLE [s_role] ADD [menu_detail] [nvarchar](150);

ALTER TABLE [s_user] ADD [local] [int] CONSTRAINT DF__s_user__local__45FE52CB DEFAULT 0 NOT NULL;

ALTER TABLE [s_user] ADD [change_pass_date] [datetime] CONSTRAINT DF__s_user__change_p__42E1EEFE DEFAULT GETDATE();

ALTER TABLE [s_user] ADD [login_number] [int] CONSTRAINT DF__s_user__login_nu__46F27704 DEFAULT 0 NOT NULL;

ALTER TABLE [s_user] ADD [change_pass] [int] CONSTRAINT DF__s_user__change_p__44CA3770 DEFAULT 0 NOT NULL;

ALTER TABLE [s_user] ADD [update_date] [datetime];

ALTER TABLE [s_serial] ADD [ut] [datetime] CONSTRAINT DF_s_serial_ut DEFAULT GETDATE() NOT NULL;

ALTER TABLE [s_serial] ADD [intg] [int] CONSTRAINT DF_s_serial_intg DEFAULT 2 NOT NULL;

ALTER TABLE [s_sys_logs] ADD [ip] [varchar](50);

ALTER TABLE [s_serial] ADD [alertleft] [int] CONSTRAINT DF_s_serial_alertleft DEFAULT 0 NOT NULL;

ALTER TABLE [s_serial] ADD [leftwarn] [int] CONSTRAINT DF_s_serial_leftwarn DEFAULT 1;

ALTER TABLE [s_serial] ADD [sendtype] [int];

ALTER TABLE [s_serial] ADD [invtype] [int];

ALTER TABLE [s_serial] ADD [degree_config] [int] CONSTRAINT DF_s_serial_degree_config DEFAULT 119;

ALTER TABLE [s_ou] ADD [c1] [varchar](255);

ALTER TABLE [s_ou] ADD [c2] [varchar](255);

ALTER TABLE [s_ou] ADD [c3] [varchar](255);

ALTER TABLE [s_ou] ADD [c4] [varchar](255);

ALTER TABLE [s_org] ADD [vtaxc] [nvarchar](14);

ALTER TABLE [s_ou] ADD [c5] [varchar](255);

ALTER TABLE [s_ou] ADD [c6] [varchar](255);

ALTER TABLE [s_ou] ADD [c7] [varchar](255);

ALTER TABLE [s_ou] ADD [c8] [varchar](255);

ALTER TABLE [s_ou] ADD [c9] [varchar](255);

ALTER TABLE [s_ou] ADD [smtpconf] [nvarchar](MAX);

ALTER TABLE [s_ou] ADD [filenameca] [varchar](255);

ALTER TABLE [b_inv] ADD [month] [varchar](100);

ALTER TABLE [s_ou] ADD [sct] [tinyint] CONSTRAINT DF_s_ou_sct DEFAULT 0 NOT NULL;

ALTER TABLE [b_inv] ADD [year] [varchar](100);

ALTER TABLE [b_inv] ADD [category] [varchar](100);

ALTER TABLE [s_ou] ADD [sc] [tinyint] CONSTRAINT DF_s_ou_sc DEFAULT 0 NOT NULL;

ALTER TABLE [s_ou] ADD [bcc] [varchar](100);

ALTER TABLE [s_ou] ADD [qrc] [tinyint] CONSTRAINT DF_s_ou_qrc DEFAULT 0 NOT NULL;

ALTER TABLE [s_ou] ADD [na] [tinyint] CONSTRAINT DF_s_ou_na DEFAULT 2 NOT NULL;

ALTER TABLE [s_ou] ADD [nq] [tinyint] CONSTRAINT DF_s_ou_nq DEFAULT 2 NOT NULL;

ALTER TABLE [s_ou] ADD [np] [tinyint] CONSTRAINT DF_s_ou_np DEFAULT 5 NOT NULL;

ALTER TABLE [s_ou] ADD [itype] [varchar](255) CONSTRAINT DF_s_ou_itype DEFAULT '01GTKT' NOT NULL;

ALTER TABLE [s_ou] ADD [ts] [tinyint] CONSTRAINT DF_s_ou_ts DEFAULT 1 NOT NULL;

ALTER TABLE [s_ou] ADD [dsignfrom] [datetime];

ALTER TABLE [s_ou] ADD [dsignto] [datetime];

ALTER TABLE [s_ou] ADD [degree_config] [tinyint] CONSTRAINT DF_s_ou_degree_config DEFAULT 119 NOT NULL;

ALTER TABLE [s_ou] ADD [cfg_ihasdocid] [varchar](45);

ALTER TABLE [s_ou] ADD [cfg_uselist] [varchar](45) CONSTRAINT DF_s_ou_cfg_uselist DEFAULT '' NOT NULL;

ALTER TABLE [s_ou] ADD [hascode] [tinyint] CONSTRAINT DF__s_ou__hascode__31B762FC DEFAULT 0 NOT NULL;

ALTER TABLE [s_ou] ADD [dsignserial] [nvarchar](100);

ALTER TABLE [s_ou] ADD [dsignsubject] [nvarchar](200);

ALTER TABLE [s_ou] ADD [dsignissuer] [nvarchar](200);

ALTER TABLE [s_ou] ADD [receivermail] [nvarchar](200);

ALTER TABLE [s_ou] ADD [adsconf] [nvarchar](MAX);

ALTER TABLE [s_ou] ADD [place] [nvarchar](255);

ALTER TABLE [s_inv] ADD [taxocode] [nvarchar](20);

ALTER TABLE [s_inv] ADD [invlistdt] [datetime];

ALTER TABLE [s_inv] ADD [discountamt] [numeric](18, 0);

ALTER TABLE [s_inv] ADD [ordno] [nvarchar](500);

ALTER TABLE [s_inv] ADD [whsfr] [nvarchar](500);

ALTER TABLE [s_inv] ADD [whsto] [nvarchar](500);

ALTER TABLE [s_inv] ADD [edt] [datetime];


ALTER TABLE [s_inv] ADD [xml_received] [nvarchar](MAX);

ALTER TABLE [s_inv] ADD [hascode] [numeric](38, 0);

ALTER TABLE [s_inv] ADD [invtype] [nvarchar](500);

ALTER TABLE [s_inv] ADD [invlistnum] [nvarchar](50);

ALTER TABLE [s_inv] ADD [period_type] [nvarchar](1);

ALTER TABLE [s_inv] ADD [list_invid] [bigint];

ALTER TABLE [s_inv] ADD [status_tbss] [int] CONSTRAINT DF__s_inv__status_tb__26CFC035 DEFAULT 0;

ALTER TABLE [s_inv] ADD [orddt] [datetime];

ALTER TABLE [s_inv] ADD [error_msg_van] [nvarchar](MAX);

ALTER TABLE [s_inv] ADD [vehic] [nvarchar](500);

ALTER TABLE [s_inv] ADD [inv_adj] [bigint];

ALTER TABLE [s_inv] ADD [period] [nvarchar](10);

ALTER TABLE [s_inv] ADD [th_type] [int];

ALTER TABLE [s_inv] ADD [wno_adj] [int];

ALTER TABLE [s_inv] ADD [wnadjtype] [int];

ALTER TABLE [s_inv] ADD [list_invid_bx] [bigint];

ALTER TABLE [s_vendor] DROP CONSTRAINT [UQ__s_vendor__24D3AF4B1AE29B8A];

ALTER TABLE [s_ou] ALTER COLUMN [c0] [varchar](255);

--alter table s_org drop index s_org_code_uk
--ALTER TABLE [s_org] ALTER COLUMN [code] [nvarchar](100);


ALTER TABLE [s_role] ALTER COLUMN [code] [nvarchar](100) NULL;

ALTER TABLE [s_org] ALTER COLUMN [fadd] [nvarchar](500);

ALTER TABLE [s_role] ALTER COLUMN [name] [nvarchar](255) NULL;


--ALTER TABLE [s_ou] ALTER COLUMN [temp] [int];

--ALTER TABLE [s_ou] ALTER COLUMN [temp] [int] NULL;

DECLARE @sql [nvarchar](MAX)
SELECT @sql = N'ALTER TABLE [s_ou] DROP CONSTRAINT ' + QUOTENAME([df].[name]) FROM [sys].[columns] AS [c] INNER JOIN [sys].[default_constraints] AS [df] ON [df].[object_id] = [c].[default_object_id] WHERE [c].[object_id] = OBJECT_ID(N'[s_ou]') AND [c].[name] = N'temp'
EXEC sp_executesql @sql;

alter table s_inv drop column sendmaildate
 ALTER TABLE s_inv add 	[sendmaildate]  AS (CONVERT([datetime],json_value([doc],'$.SendMailDate')))

alter table s_inv drop column sendsmsdate
	 ALTER TABLE s_inv add 	[sendsmsdate]  AS (CONVERT([datetime],json_value([doc],'$.SendSMSlDate')))

-- ALTER TABLE s_inv add [vseq]  AS (TRY_CAST(isnull(case when TRY_CAST(json_value([doc],'$.seq') AS [nvarchar](18))='' then NULL else TRY_CAST(json_value([doc],'$.seq') AS [nvarchar](18)) end,CONVERT([nvarchar](18),[id])) AS [nvarchar](18))) PERSISTED

alter table s_inv drop column sendmailstatus
ALTER TABLE s_inv add [sendmailstatus]  AS (case when CONVERT([int],json_value([doc],'$.SendMailStatus')) IS NULL then (0) else CONVERT([int],json_value([doc],'$.SendMailStatus')) end)
	
alter table s_inv drop column sendsmsstatus
ALTER TABLE s_inv add [sendsmsstatus]  AS (case when CONVERT([int],json_value([doc],'$.SendSMSStatus')) IS NULL then (0) else CONVERT([int],json_value([doc],'$.SendSMSStatus')) end)
	
alter table s_inv drop column sendmailnum
ALTER TABLE s_inv add [sendmailnum]  AS (case when CONVERT([int],json_value([doc],'$.SendMailNum')) IS NULL then (0) else CONVERT([int],json_value([doc],'$.SendMailNum')) end)
	
alter table s_inv drop column sendsmsnum
ALTER TABLE s_inv add [sendsmsnum]  AS (case when CONVERT([int],json_value([doc],'$.SendSMSNum')) IS NULL then (0) else CONVERT([int],json_value([doc],'$.SendSMSNum')) end)
	
alter table s_inv drop column bcode
ALTER TABLE s_inv add [bcode]  AS (CONVERT([nvarchar](500),json_value([doc],'$.bcode')))

alter table s_inv drop column taxocode
ALTER TABLE s_inv add [taxocode]  AS (CONVERT([nvarchar](20),json_value([doc],'$.taxocode'))) 

alter table s_inv drop column invlistdt
ALTER TABLE s_inv add [invlistdt]  AS (CONVERT([datetime],json_value([doc],'$.invlistdt')))

alter table s_inv drop column bcode
ALTER TABLE s_inv add [bcode]  AS (CONVERT([nvarchar](500),json_value([doc],'$.bcode')))

alter table s_inv drop column discountamt
--ALTER TABLE s_inv add [discountamt]  AS (case when CONVERT([numeric],json_value([doc],'$.discountamt')) IS NULL then (0) else CONVERT([numeric],json_value([doc],'$.discountamt')) end)
alter table s_inv drop column ordno
ALTER TABLE s_inv add [ordno]  AS (CONVERT([nvarchar](500),json_value([doc],'$.ordno')))
	
alter table s_inv drop column whsfr
ALTER TABLE s_inv add [whsfr]  AS (CONVERT([nvarchar](500),json_value([doc],'$.whsfr')))

alter table s_inv drop column whsto
ALTER TABLE s_inv add [whsto]  AS (CONVERT([nvarchar](500),json_value([doc],'$.whsto')))

alter table s_inv drop column status_received
ALTER TABLE s_inv add [status_received]  AS (case when CONVERT([int],json_value([doc],'$.status_received')) IS NULL then (0) else CONVERT([int],json_value([doc],'$.status_received')) end)

alter table s_inv drop column hascode
ALTER TABLE s_inv add  [hascode]  AS (CONVERT([numeric](500),json_value([doc],'$.hascode')))

alter table s_inv drop column invtype
ALTER TABLE s_inv add  [invtype]  AS (CONVERT([nvarchar](500),json_value([doc],'$.invtype')))
  
ALTER TABLE s_inv DROP COLUMN [seq]
ALTER TABLE s_inv add  [seq]  AS (CONVERT([nvarchar](8),json_value([doc],'$.seq')))

ALTER TABLE s_inv DROP COLUMN [orddt]
ALTER TABLE s_inv add  [orddt]  AS (CONVERT([datetime],json_value([doc],'$.orddt')))

ALTER TABLE s_inv DROP COLUMN [vehic]
ALTER TABLE s_inv add  [vehic]  AS (CONVERT([nvarchar](500),json_value([doc],'$.vehic')))



-- Disable chưc năng thông báo sai sot
update s_role set active=0 where name like '%sai sót%'

ALTER TABLE ctbc_einvoiceapp.dbo.s_statement DROP CONSTRAINT DF__s_stateme__statu__7C8F6DA6 GO
ALTER TABLE ctbc_einvoiceapp.dbo.s_statement ADD  DEFAULT 1 FOR status GO

ALTER TABLE s_statement ADD CONSTRAINT [DF__status]  DEFAULT 1 FOR status 
ALTER TABLE s_statement ADD CONSTRAINT [DF__statustvan] DEFAULT 1 FOR statustvan



ALTER TABLE [s_role] ADD CONSTRAINT [s_role_fk01] FOREIGN KEY ([pid]) REFERENCES [s_role] ([id]) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE [van_msg_in_dtl] ADD CONSTRAINT [PK_van_msg_in_dtl] PRIMARY KEY ([id]);

ALTER TABLE [s_user_pass] ADD CONSTRAINT [FK__s_user_pa__user___49CEE3AF] FOREIGN KEY ([user_id]) REFERENCES [s_user] ([id]) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE [s_org] ALTER COLUMN [code] [nvarchar](100);

ALTER TABLE [van_msg_in_dtl] ADD CONSTRAINT [DF__van_msg_i__creat__0C50D423] DEFAULT GETDATE() FOR [createddate];

ALTER TABLE [van_msg_in_dtl] ADD CONSTRAINT [DF__van_msg_i__datec__093F5D4E] DEFAULT GETDATE() FOR [datecheckmsg];

ALTER TABLE [s_statement] ADD CONSTRAINT [DF__s_stateme__hasco__3A4CA8FD] DEFAULT 0 FOR [hascode];

ALTER TABLE [s_statement] ALTER COLUMN [status] [int] NOT NULL;

ALTER TABLE [van_msg_in_dtl] ADD CONSTRAINT [DF__van_msg_i__statu__0A338187] DEFAULT 0 FOR [status_response];

ALTER TABLE [s_statement] ALTER COLUMN [statustvan] [int] NOT NULL;

ALTER TABLE [s_ou] ALTER COLUMN [temp] [int] NULL;

ALTER TABLE [van_msg_in_dtl] ADD CONSTRAINT [DF__van_msg_i__trans__084B3915] DEFAULT '0' FOR [transid];
