--Tạo thêm cột cho SCB Report Matrix
ALTER TABLE dbo.s_role ADD scbname varchar(100) NULL;

--Xóa hết bảng s_group_role
delete from dbo.s_group_role;

--Xóa hết bảng s_role
delete from dbo.s_role;

--Xóa hết bảng s_group_user
delete from dbo.s_group_user where CHARINDEX('__', USER_ID) <= 0;

--Xóa hết bảng s_user
delete from dbo.s_user where CHARINDEX('__', ID) <= 0;

--Xóa hết bảng group
delete from dbo.s_group where (id != 1 or name != 'ADMIN');

--Insert lại bảng s_role
INSERT INTO s_role (id, name, CODE, scbname) VALUES(1, N'Quản trị hệ thống', N'PERM_FULL_MANAGE', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(2, N'Cấu hình hệ thống', N'PERM_SYS_CONFIG', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(3, N'Quản lý người dùng', N'PERM_SYS_USER', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(4, N'Quản lý nhóm', N'PERM_SYS_GROUP', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(5, N'Phân quyền', N'PERM_SYS_ROLE', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(6, N'Tạo mẫu hóa đơn', N'PERM_SYS_TEMPLATE', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(7, N'Tạo mẫu Email', N'PERM_SYS_EMAIL', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(8, N'Cấu hình hóa đơn', N'PERM_SYS_INV_CONFIG', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(9, N'Cấu hình trường', N'PERM_SYS_FIELD_CONFIG', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(10, N'Quản trị danh mục', N'PERM_CATEGORY_MANAGE', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(11, N'Khách hàng', N'PERM_CATEGORY_CUSTOMER', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(12, N'Hàng hóa, dịch vụ', N'PERM_CATEGORY_ITEMS', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(13, N'Tỷ giá', N'PERM_CATEGORY_RATE', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(14, N'Danh mục khác', N'PERM_CATEGORY_ANOTHER', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(15, N'Thông báo phát hành', N'PERM_TEMPLATE_REGISTER', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(16, N'Gán quyền sử dụng TBPH', N'PERM_TEMPLATE_REGISTER_GRANT', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(17, N'Lập hóa đơn', N'PERM_CREATE_INV_MANAGE', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(18, N'Lập hóa đơn từ Excel', N'PERM_CREATE_INV_EXCEL', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(19, N'Lập hóa đơn từ nguồn khác', N'PERM_CREATE_INV_ANOTHER', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(20, N'Cấp số', N'PERM_CREATE_INV_SEQ', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(21, N'Duyệt và xuất hóa đơn', N'PERM_APPROVE_INV_MANAGE', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(22, N'Báo cáo', N'PERM_REPORT_INV', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(23, N'Tra cứu hóa đơn', N'PERM_SEARCH_INV', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(24, N'Điều chỉnh hóa đơn', N'PERM_INVOICE_ADJUST', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(25, N'Thay thế hóa đơn', N'PERM_INVOICE_REPLACE', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(26, N'Hủy hóa đơn', N'PERM_INVOICE_CANCEL', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(27, N'Duyệt thông báo phát hành', N'PERM_TEMPLATE_APPROVE', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(28, N'Hủy thông báo phát hành', N'PERM_TEMPLATE_VOID', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(29, N'Tích hợp', N'PERM_INTEGRATED', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(30, N'Chờ hủy', N'PERM_VOID_WAIT', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(31, N'Log hệ thống', N'PERM_LOG_SYS', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(33, N'Tra cứu giao dịch', N'PERM_TRANS', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(34, N'Nhà cung cấp', N'PERM_CATEGORY_VENDOR', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(35, N'Duyệt giao dịch', N'PERM_TRANS_APPR', NULL);
INSERT INTO s_role (id, name, CODE, scbname) VALUES(36, N'Gửi mail thủ công', N'PERM_MAIL', NULL);

--Insert bảng s_group
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(2, N'Admin_Maker', NULL, N'1', N'1', N'Create, edit, delete user profiles');
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(3, N'Admin_Checker', NULL, N'1', N'1', N'Approve /Review the Creation, change or deletion of user profiles');
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(4, N'CodeSetup_Maker', NULL, N'1', N'1', N'Setup codes, program parameters and configuration, audit log, language update, View Cache,  edit and trigger Schedule tasks');
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(5, N'CodeSetup_Checker', NULL, N'1', N'1', N'Approval for Setup codes, program parameters and configuration, audit log, language update, View Cache,  edit and trigger Schedule tasks');
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(6, N'ACS_maker', NULL, N'1', N'1', N'Generate invoice from system data and manual invoice');
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(7, N'ACS_checker', NULL, N'1', N'1', N'Approval/Review the invoice, generate invoice number and issue invoice. Can view all the invoices with status');
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(8, N'SSO_maker', NULL, N'1', N'1', N'Generate invoice from system data and manual invoice');
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(9, N'SSO_checker', NULL, N'1', N'1', N'Approval/Review the invoice, generate invoice number and issue invoice. Can view all the invoices with status');
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(10, N'Trade_maker', NULL, N'1', N'1', N'Generate invoice from system data and manual invoice');
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(1005, N'Trade_checker', NULL, N'1', N'1', N'Approval/Review the invoice, generate invoice number and issue invoice. Can view all the invoices with status');
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(1006, N'Card Ops_maker', NULL, N'1', N'1', N'Generate invoice from system data and manual invoice');
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(1007, N'Card Ops_checker', NULL, N'1', N'1', N'Approval/Review the invoice, generate invoice number and issue invoice. Can view all the invoices with status');
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(1008, N'RLS_maker', NULL, N'1', N'1', N'Generate invoice from system data and manual invoice');
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(1009, N'RLS_checker', NULL, N'1', N'1', N'Approval/Review the invoice, generate invoice number and issue invoice. Can view all the invoices with status');
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(1010, N'FMO_maker', NULL, N'1', N'1', N'Generate invoice from system data and manual invoice');
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(1011, N'FMO_checker', NULL, N'1', N'1', N'Approval/Review the invoice, generate invoice number and issue invoice. Can view all the invoices with status');
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(1012, N'CMO_maker', NULL, N'1', N'1', N'Generate invoice from system data and manual invoice');
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(1013, N'CMO_checker', NULL, N'1', N'1', N'Approval/Review the invoice, generate invoice number and issue invoice. Can view all the invoices with status');
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(1014, N'GLS_maker', NULL, N'1', N'1', N'Generate invoice from system data and manual invoice');
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(1015, N'GLS_checker', NULL, N'1', N'1', N'Approval/Review the invoice, generate invoice number and issue invoice. Can view all the invoices with status');
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(1016, N'Branch Ops_maker', NULL, N'1', N'1', N'Generate invoice from system data and manual invoice');
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(1017, N'Branch Ops_checker', NULL, N'1', N'1', N'Approval/Review the invoice, generate invoice number and issue invoice. Can view all the invoices with status');
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(1018, N'CRC_maker', NULL, N'1', N'1', N'Generate invoice from system data and manual invoice');
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(1019, N'CRC_checker', NULL, N'1', N'1', N'Approval/Review the invoice, generate invoice number and issue invoice. Can view all the invoices with status');
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(1020, N'Tax_Finance_Viewer', NULL, N'1', N'1', N'View all tax reports and standard reports');
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(1021, N'Viewer_All', NULL, N'1', N'1', N'View reconciliation reports, tax reports, audit logs reports');
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(1022, N'Viewer_invoices with status', NULL, N'1', N'1', N'View invoice with status only');
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(1023, N'System support', NULL, N'1', N'1', N'View database connection details');

--Insert lại bảng s_group_role
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '1', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(2.0, '1', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '3', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '5', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '6', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '7', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '8', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '9', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '10', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '11', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '12', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '13', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '14', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '15', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '16', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '17', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '18', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '19', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '20', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '21', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '22', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '23', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '24', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '25', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '26', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '27', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '28', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '29', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '30', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '31', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '33', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '34', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '35', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '36', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '2', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(2.0, '2', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(3.0, '3', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(2.0, '3', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(3.0, '2', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(4.0, '2', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1.0, '4', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(2.0, '4', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1023.0, '2', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1023.0, '1', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(2.0, '33', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(5.0, '1', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(5.0, '2', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(5.0, '31', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1012.0, '11', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1012.0, '12', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1016.0, '11', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(6.0, '11', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(6.0, '12', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(6.0, '17', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1016.0, '12', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(6.0, '18', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(6.0, '25', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(6.0, '26', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(10.0, '11', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(10.0, '12', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(10.0, '17', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(10.0, '18', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(8.0, '11', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(10.0, '25', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(10.0, '26', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1006.0, '11', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1006.0, '12', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1008.0, '11', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1008.0, '12', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1016.0, '17', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1016.0, '18', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1016.0, '25', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1016.0, '26', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1018.0, '11', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1018.0, '12', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(7.0, '23', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(9.0, '23', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(4.0, '7', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(4.0, '13', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(4.0, '15', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(4.0, '31', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(8.0, '12', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(8.0, '17', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(8.0, '18', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(8.0, '25', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(8.0, '26', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1006.0, '17', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1006.0, '18', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1006.0, '25', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1006.0, '26', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1008.0, '17', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1008.0, '18', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1008.0, '25', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1008.0, '26', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1010.0, '11', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1010.0, '12', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1010.0, '17', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1010.0, '18', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1010.0, '25', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1010.0, '26', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1012.0, '17', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1012.0, '18', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1012.0, '25', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1012.0, '26', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1018.0, '17', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1018.0, '18', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1014.0, '11', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1014.0, '12', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1014.0, '17', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1014.0, '18', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1014.0, '25', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1014.0, '26', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1018.0, '25', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1018.0, '26', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1005.0, '23', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1007.0, '23', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1009.0, '23', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1011.0, '23', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1015.0, '23', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1013.0, '23', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1017.0, '23', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1019.0, '23', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1021.0, '23', NULL);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID, STATUS) VALUES(1022.0, '23', NULL);

--Thêm dữ liệu cột scb name cho báo cáo Securrity matrix
UPDATE S_ROLE SET scbname = 'SystemManagement' where code = 'PERM_FULL_MANAGE';
UPDATE S_ROLE SET scbname = 'SystemConfiguration' where code = 'PERM_SYS_CONFIG';
UPDATE S_ROLE SET scbname = 'UserManagement' where code = 'PERM_SYS_USER';
UPDATE S_ROLE SET scbname = 'GroupManagement' where code = 'PERM_SYS_GROUP';
UPDATE S_ROLE SET scbname = 'Decentralization' where code = 'PERM_SYS_ROLE';
UPDATE S_ROLE SET scbname = 'CreateInvoiceTemplate' where code = 'PERM_SYS_TEMPLATE';
UPDATE S_ROLE SET scbname = 'EmailTemplateCreation' where code = 'PERM_SYS_EMAIL';
UPDATE S_ROLE SET scbname = 'InvoiceConfiguration' where code = 'PERM_SYS_INV_CONFIG';
UPDATE S_ROLE SET scbname = 'FieldConfiguration' where code = 'PERM_SYS_FIELD_CONFIG';
UPDATE S_ROLE SET scbname = 'DirectoryAdministration' where code = 'PERM_CATEGORY_MANAGE';
UPDATE S_ROLE SET scbname = 'Customer' where code = 'PERM_CATEGORY_CUSTOMER';
UPDATE S_ROLE SET scbname = 'GoodsAndServices' where code = 'PERM_CATEGORY_ITEMS';
UPDATE S_ROLE SET scbname = 'ExchangeRate' where code = 'PERM_CATEGORY_RATE';
UPDATE S_ROLE SET scbname = 'OtherCategories' where code = 'PERM_CATEGORY_ANOTHER';
UPDATE S_ROLE SET scbname = 'ReleaseNotice' where code = 'PERM_TEMPLATE_REGISTER';
UPDATE S_ROLE SET scbname = 'AssignTheRightToUseTbph' where code = 'PERM_TEMPLATE_REGISTER_GRANT';
UPDATE S_ROLE SET scbname = 'Invoicing' where code = 'PERM_CREATE_INV_MANAGE';
UPDATE S_ROLE SET scbname = 'InvoicingFromExcel' where code = 'PERM_CREATE_INV_EXCEL';
UPDATE S_ROLE SET scbname = 'BillingFromOtherSources' where code = 'PERM_CREATE_INV_ANOTHER';
UPDATE S_ROLE SET scbname = 'NumberLevel' where code = 'PERM_CREATE_INV_SEQ';
UPDATE S_ROLE SET scbname = 'BrowseAndIssueInvoices' where code = 'PERM_APPROVE_INV_MANAGE';
UPDATE S_ROLE SET scbname = 'Report' where code = 'PERM_REPORT_INV';
UPDATE S_ROLE SET scbname = 'LookUpInvoices' where code = 'PERM_SEARCH_INV';
UPDATE S_ROLE SET scbname = 'InvoiceAdjustment' where code = 'PERM_INVOICE_ADJUST';
UPDATE S_ROLE SET scbname = 'ReplaceInvoices' where code = 'PERM_INVOICE_REPLACE';
UPDATE S_ROLE SET scbname = 'CancellingInvoice' where code = 'PERM_INVOICE_CANCEL';
UPDATE S_ROLE SET scbname = 'BrowseReleaseNotices' where code = 'PERM_TEMPLATE_APPROVE';
UPDATE S_ROLE SET scbname = 'CancellationOfReleaseNotices' where code = 'PERM_TEMPLATE_VOID';
UPDATE S_ROLE SET scbname = 'Integration' where code = 'PERM_INTEGRATED';
UPDATE S_ROLE SET scbname = 'WaitForCancellation' where code = 'PERM_VOID_WAIT';
UPDATE S_ROLE SET scbname = 'LogSystem' where code = 'PERM_LOG_SYS';
UPDATE S_ROLE SET scbname = 'TransactionLookup' where code = 'PERM_TRANS';
UPDATE S_ROLE SET scbname = 'Supplier' where code = 'PERM_CATEGORY_VENDOR';
UPDATE S_ROLE SET scbname = 'TransactionApproval' where code = 'PERM_TRANS_APPR';
UPDATE S_ROLE SET scbname = 'SendMailManually' where code = 'PERM_MAIL';

