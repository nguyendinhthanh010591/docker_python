-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema einvoice
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema einvoice
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `einvoice` DEFAULT CHARACTER SET utf8mb4 ;
USE `einvoice` ;

-- -----------------------------------------------------
-- Table `einvoice`.`s_cat`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `einvoice`.`s_cat` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `type` VARCHAR(30) NOT NULL,
  `des` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `type` (`type` ASC, `name` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 140
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `einvoice`.`s_dcd`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `einvoice`.`s_dcd` (
  `id` VARCHAR(30) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `type` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`type`, `id`, `name`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `einvoice`.`s_dcm`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `einvoice`.`s_dcm` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `itype` VARCHAR(6) NOT NULL,
  `idx` TINYINT(4) NOT NULL,
  `dtl` TINYINT(4) NOT NULL DEFAULT '2',
  `lbl` VARCHAR(100) NOT NULL,
  `typ` VARCHAR(20) NOT NULL,
  `atr` JSON NULL DEFAULT NULL,
  `status` TINYINT(4) NOT NULL DEFAULT '2',
  `xc` VARCHAR(2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `itype` (`itype` ASC, `idx` ASC, `dtl` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 121
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `einvoice`.`s_ex`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `einvoice`.`s_ex` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cur` VARCHAR(3) NOT NULL,
  `val` DECIMAL(8,2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `dt` (`dt` ASC, `cur` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `einvoice`.`s_gns`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `einvoice`.`s_gns` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(30) NOT NULL,
  `name` VARCHAR(255) NOT NULL,
  `unit` VARCHAR(30) NOT NULL DEFAULT 'N/A',
  `price` DECIMAL(17,2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `code` (`code` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 20
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `einvoice`.`s_inv`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `einvoice`.`s_inv` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sec` VARCHAR(10) NOT NULL,
  `ic` VARCHAR(36) NULL DEFAULT NULL,
  `pid` INT(10) UNSIGNED NULL DEFAULT NULL,
  `cid` INT(10) UNSIGNED NULL DEFAULT NULL,
  `cde` VARCHAR(255) NULL DEFAULT NULL,
  `idt` DATE NOT NULL,
  `doc` JSON NOT NULL,
  `xml` TEXT NULL DEFAULT NULL,
  `dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ou` INT(10) UNSIGNED NOT NULL,
  `uc` VARCHAR(20) NOT NULL,
  `cvt` TINYINT(4) NULL DEFAULT '0',
  `status` TINYINT(4) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.status'))) VIRTUAL,
  `type` VARCHAR(10) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.type'))) VIRTUAL,
  `form` VARCHAR(11) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.form'))) VIRTUAL,
  `serial` VARCHAR(6) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.serial'))) VIRTUAL,
  `seq` VARCHAR(7) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.seq'))) VIRTUAL,
  `paym` VARCHAR(10) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.paym'))) VIRTUAL,
  `curr` VARCHAR(3) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.curr'))) VIRTUAL,
  `exrt` DECIMAL(8,2) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.exrt'))) VIRTUAL,
  `sname` VARCHAR(255) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.sname'))) VIRTUAL,
  `stax` VARCHAR(14) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.stax'))) VIRTUAL,
  `saddr` VARCHAR(255) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.saddr'))) VIRTUAL,
  `smail` VARCHAR(255) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.smail'))) VIRTUAL,
  `stel` VARCHAR(255) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.stel'))) VIRTUAL,
  `sacc` VARCHAR(255) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.sacc'))) VIRTUAL,
  `sbank` VARCHAR(255) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.sbank'))) VIRTUAL,
  `buyer` VARCHAR(100) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.buyer'))) VIRTUAL,
  `bname` VARCHAR(255) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.bname'))) VIRTUAL,
  `btax` VARCHAR(14) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.btax'))) VIRTUAL,
  `baddr` VARCHAR(255) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.baddr'))) VIRTUAL,
  `bmail` VARCHAR(255) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.bmail'))) VIRTUAL,
  `btel` VARCHAR(255) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.btel'))) VIRTUAL,
  `bacc` VARCHAR(255) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.bacc'))) VIRTUAL,
  `bbank` VARCHAR(255) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.bbank'))) VIRTUAL,
  `note` VARCHAR(255) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.note'))) VIRTUAL,
  `sum` DECIMAL(17,2) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.sum'))) VIRTUAL,
  `sumv` DECIMAL(17,2) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.sumv'))) VIRTUAL,
  `vat` DECIMAL(17,2) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.vat'))) VIRTUAL,
  `vatv` DECIMAL(17,2) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.vatv'))) VIRTUAL,
  `total` DECIMAL(17,2) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.total'))) VIRTUAL,
  `totalv` DECIMAL(17,2) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.totalv'))) VIRTUAL,
  `adjseq` VARCHAR(26) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.adj.seq'))) VIRTUAL,
  `adjdes` VARCHAR(255) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.adj.des'))) VIRTUAL,
  `adjtyp` TINYINT(4) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.adj.typ'))) VIRTUAL,
  `c0` VARCHAR(255) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.c0'))) VIRTUAL,
  `c1` VARCHAR(255) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.c1'))) VIRTUAL,
  `c2` VARCHAR(255) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.c2'))) VIRTUAL,
  `c3` VARCHAR(255) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.c3'))) VIRTUAL,
  `c4` VARCHAR(255) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.c4'))) VIRTUAL,
  `c5` VARCHAR(255) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.c5'))) VIRTUAL,
  `c6` VARCHAR(255) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.c6'))) VIRTUAL,
  `c7` VARCHAR(255) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.c7'))) VIRTUAL,
  `c8` VARCHAR(255) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.c8'))) VIRTUAL,
  `c9` VARCHAR(255) GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.c9'))) VIRTUAL,
  PRIMARY KEY (`id`, `idt`),
  UNIQUE INDEX `sec` (`sec` ASC, `idt` ASC) VISIBLE,
  UNIQUE INDEX `ic` (`ic` ASC, `idt` ASC) VISIBLE,
  INDEX `pid` (`pid` ASC) VISIBLE,
  INDEX `s_inv_idx1` (`stax` ASC, `status` ASC, `form` ASC, `serial` ASC, `seq` ASC, `idt` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 289505
DEFAULT CHARACTER SET = utf8mb4 PARTITION BY RANGE(idt) PARTITIONS 61( PARTITION p2000 VALUES LESS THAN (2020-01-01),  PARTITION p2001 VALUES LESS THAN (2020-02-01),  PARTITION p2002 VALUES LESS THAN (2020-03-01),  PARTITION p2003 VALUES LESS THAN (2020-04-01),  PARTITION p2004 VALUES LESS THAN (2020-05-01),  PARTITION p2005 VALUES LESS THAN (2020-06-01),  PARTITION p2006 VALUES LESS THAN (2020-07-01),  PARTITION p2007 VALUES LESS THAN (2020-08-01),  PARTITION p2008 VALUES LESS THAN (2020-09-01),  PARTITION p2009 VALUES LESS THAN (2020-10-01),  PARTITION p2010 VALUES LESS THAN (2020-11-01),  PARTITION p2011 VALUES LESS THAN (2020-12-01),  PARTITION p2100 VALUES LESS THAN (2021-01-01),  PARTITION p2101 VALUES LESS THAN (2021-02-01),  PARTITION p2102 VALUES LESS THAN (2021-03-01),  PARTITION p2103 VALUES LESS THAN (2021-04-01),  PARTITION p2104 VALUES LESS THAN (2021-05-01),  PARTITION p2105 VALUES LESS THAN (2021-06-01),  PARTITION p2106 VALUES LESS THAN (2021-07-01),  PARTITION p2107 VALUES LESS THAN (2021-08-01),  PARTITION p2108 VALUES LESS THAN (2021-09-01),  PARTITION p2109 VALUES LESS THAN (2021-10-01),  PARTITION p2110 VALUES LESS THAN (2021-11-01),  PARTITION p2111 VALUES LESS THAN (2021-12-01),  PARTITION p2200 VALUES LESS THAN (2022-01-01),  PARTITION p2201 VALUES LESS THAN (2022-02-01),  PARTITION p2202 VALUES LESS THAN (2022-03-01),  PARTITION p2203 VALUES LESS THAN (2022-04-01),  PARTITION p2204 VALUES LESS THAN (2022-05-01),  PARTITION p2205 VALUES LESS THAN (2022-06-01),  PARTITION p2206 VALUES LESS THAN (2022-07-01),  PARTITION p2207 VALUES LESS THAN (2022-08-01),  PARTITION p2208 VALUES LESS THAN (2022-09-01),  PARTITION p2209 VALUES LESS THAN (2022-10-01),  PARTITION p2210 VALUES LESS THAN (2022-11-01),  PARTITION p2211 VALUES LESS THAN (2022-12-01),  PARTITION p2300 VALUES LESS THAN (2023-01-01),  PARTITION p2301 VALUES LESS THAN (2023-02-01),  PARTITION p2302 VALUES LESS THAN (2023-03-01),  PARTITION p2303 VALUES LESS THAN (2023-04-01),  PARTITION p2304 VALUES LESS THAN (2023-05-01),  PARTITION p2305 VALUES LESS THAN (2023-06-01),  PARTITION p2306 VALUES LESS THAN (2023-07-01),  PARTITION p2307 VALUES LESS THAN (2023-08-01),  PARTITION p2308 VALUES LESS THAN (2023-09-01),  PARTITION p2309 VALUES LESS THAN (2023-10-01),  PARTITION p2310 VALUES LESS THAN (2023-11-01),  PARTITION p2311 VALUES LESS THAN (2023-12-01),  PARTITION p2400 VALUES LESS THAN (2024-01-01),  PARTITION p2401 VALUES LESS THAN (2024-02-01),  PARTITION p2402 VALUES LESS THAN (2024-03-01),  PARTITION p2403 VALUES LESS THAN (2024-04-01),  PARTITION p2404 VALUES LESS THAN (2024-05-01),  PARTITION p2405 VALUES LESS THAN (2024-06-01),  PARTITION p2406 VALUES LESS THAN (2024-07-01),  PARTITION p2407 VALUES LESS THAN (2024-08-01),  PARTITION p2408 VALUES LESS THAN (2024-09-01),  PARTITION p2409 VALUES LESS THAN (2024-10-01),  PARTITION p2410 VALUES LESS THAN (2024-11-01),  PARTITION p2411 VALUES LESS THAN (2024-12-01),  PARTITION pmax VALUES LESS THAN (MAXVALUE)) ;


-- -----------------------------------------------------
-- Table `einvoice`.`s_loc`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `einvoice`.`s_loc` (
  `id` VARCHAR(7) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `pid` VARCHAR(7) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `pid` (`pid` ASC) VISIBLE,
  CONSTRAINT `s_loc_ibfk_1`
    FOREIGN KEY (`pid`)
    REFERENCES `einvoice`.`s_loc` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `einvoice`.`s_ou`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `einvoice`.`s_ou` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pid` INT(10) UNSIGNED NULL DEFAULT NULL,
  `code` VARCHAR(20) NULL DEFAULT NULL,
  `name` VARCHAR(255) NOT NULL,
  `taxc` VARCHAR(14) NULL DEFAULT NULL,
  `mst` VARCHAR(14) NOT NULL,
  `paxo` VARCHAR(5) NULL DEFAULT NULL,
  `taxo` VARCHAR(5) NULL DEFAULT NULL,
  `prov` VARCHAR(3) NULL DEFAULT NULL,
  `dist` VARCHAR(5) NULL DEFAULT NULL,
  `ward` VARCHAR(7) NULL DEFAULT NULL,
  `addr` VARCHAR(255) NULL DEFAULT NULL,
  `fadd` VARCHAR(255) NULL DEFAULT NULL,
  `tel` VARCHAR(255) NULL DEFAULT NULL,
  `mail` VARCHAR(255) NULL DEFAULT NULL,
  `acc` VARCHAR(255) NULL DEFAULT NULL,
  `bank` VARCHAR(255) NULL DEFAULT NULL,
  `status` TINYINT(4) NOT NULL DEFAULT '1',
  `sign` TINYINT(4) NOT NULL DEFAULT '1',
  `seq` TINYINT(4) NOT NULL DEFAULT '1',
  `usr` VARCHAR(255) NULL DEFAULT NULL,
  `pwd` VARCHAR(255) NULL DEFAULT NULL,
  `temp` TINYINT(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `taxc` (`taxc` ASC) VISIBLE,
  UNIQUE INDEX `code_UNIQUE` (`code` ASC) VISIBLE,
  INDEX `pid` (`pid` ASC) VISIBLE,
  CONSTRAINT `s_ou_ibfk_1`
    FOREIGN KEY (`pid`)
    REFERENCES `einvoice`.`s_ou` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 61
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `einvoice`.`s_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `einvoice`.`s_user` (
  `id` VARCHAR(20) NOT NULL,
  `code` VARCHAR(20) NULL DEFAULT NULL,
  `mail` VARCHAR(50) NULL DEFAULT NULL,
  `ou` INT(10) UNSIGNED NOT NULL,
  `uc` TINYINT(4) NOT NULL DEFAULT '1',
  `name` VARCHAR(50) NOT NULL,
  `pos` VARCHAR(50) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `code_UNIQUE` (`code` ASC) VISIBLE,
  INDEX `ou` (`ou` ASC) VISIBLE,
  CONSTRAINT `s_user_ibfk_1`
    FOREIGN KEY (`ou`)
    REFERENCES `einvoice`.`s_ou` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `einvoice`.`s_manager`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `einvoice`.`s_manager` (
  `user_id` VARCHAR(20) NOT NULL,
  `taxc` VARCHAR(14) NOT NULL,
  PRIMARY KEY (`user_id`, `taxc`),
  INDEX `taxc` (`taxc` ASC) VISIBLE,
  CONSTRAINT `s_manager_ibfk_1`
    FOREIGN KEY (`user_id`)
    REFERENCES `einvoice`.`s_user` (`id`),
  CONSTRAINT `s_manager_ibfk_2`
    FOREIGN KEY (`taxc`)
    REFERENCES `einvoice`.`s_ou` (`taxc`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `einvoice`.`s_role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `einvoice`.`s_role` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 15
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `einvoice`.`s_member`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `einvoice`.`s_member` (
  `user_id` VARCHAR(20) NOT NULL,
  `role_id` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`, `role_id`),
  INDEX `role_id` (`role_id` ASC) VISIBLE,
  CONSTRAINT `s_member_ibfk_1`
    FOREIGN KEY (`user_id`)
    REFERENCES `einvoice`.`s_user` (`id`),
  CONSTRAINT `s_member_ibfk_2`
    FOREIGN KEY (`role_id`)
    REFERENCES `einvoice`.`s_role` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `einvoice`.`s_org`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `einvoice`.`s_org` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `name_en` VARCHAR(255) NULL DEFAULT NULL,
  `taxc` VARCHAR(14) NULL DEFAULT NULL,
  `prov` VARCHAR(3) NULL DEFAULT NULL,
  `dist` VARCHAR(5) NULL DEFAULT NULL,
  `ward` VARCHAR(7) NULL DEFAULT NULL,
  `addr` VARCHAR(255) NULL DEFAULT NULL,
  `fadd` VARCHAR(255) NULL DEFAULT NULL,
  `tel` VARCHAR(255) NULL DEFAULT NULL,
  `mail` VARCHAR(255) NULL DEFAULT NULL,
  `acc` VARCHAR(255) NULL DEFAULT NULL,
  `bank` VARCHAR(255) NULL DEFAULT NULL,
  `status` VARCHAR(2) NOT NULL DEFAULT '00',
  `c0` VARCHAR(255) NULL DEFAULT NULL,
  `c1` VARCHAR(255) NULL DEFAULT NULL,
  `c2` VARCHAR(255) NULL DEFAULT NULL,
  `c3` VARCHAR(255) NULL DEFAULT NULL,
  `c4` VARCHAR(255) NULL DEFAULT NULL,
  `c5` VARCHAR(255) NULL DEFAULT NULL,
  `c6` VARCHAR(255) NULL DEFAULT NULL,
  `c7` VARCHAR(255) NULL DEFAULT NULL,
  `c8` VARCHAR(255) NULL DEFAULT NULL,
  `c9` VARCHAR(255) NULL DEFAULT NULL,
  `fn` VARCHAR(512) GENERATED ALWAYS AS (coalesce(nullif(`taxc`,''),coalesce(nullif(`code`,''),concat_ws('-',`name`,`tel`,`mail`,`fadd`)))) STORED,
  `code` VARCHAR(50) NULL DEFAULT NULL,
  `pwd` VARCHAR(60) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `taxc` (`taxc` ASC) VISIBLE,
  UNIQUE INDEX `fn` (`fn` ASC) VISIBLE,
  UNIQUE INDEX `code_UNIQUE` (`code` ASC) VISIBLE,
  INDEX `name` (`name` ASC) VISIBLE,
  INDEX `fadd` (`fadd` ASC) VISIBLE,
  FULLTEXT INDEX `taxc_2` (`taxc`, `code`, `name`, `name_en`, `fadd`, `tel`, `mail`, `acc`, `bank`, `c0`, `c1`, `c2`, `c3`, `c4`, `c5`, `c6`) VISIBLE,
  FULLTEXT INDEX `taxc_3` (`taxc`, `code`, `name`, `name_en`, `fadd`, `tel`, `mail`, `acc`, `bank`, `c0`, `c1`, `c2`, `c3`, `c4`, `c5`, `c6`) VISIBLE,
  FULLTEXT INDEX `taxc_4` (`taxc`, `code`, `name`, `name_en`, `fadd`, `tel`, `mail`, `acc`, `bank`, `c0`, `c1`, `c2`, `c3`, `c4`, `c5`, `c6`) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 1048631
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `einvoice`.`s_ou_tree`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `einvoice`.`s_ou_tree` (
  `pid` INT(10) UNSIGNED NOT NULL,
  `cid` INT(10) UNSIGNED NOT NULL,
  `depth` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `path` TEXT NOT NULL,
  PRIMARY KEY (`pid`, `cid`),
  INDEX `cid` (`cid` ASC, `depth` ASC) VISIBLE,
  INDEX `pid` (`pid` ASC) VISIBLE,
  INDEX `cid_2` (`cid` ASC) VISIBLE,
  CONSTRAINT `s_ou_tree_ibfk_1`
    FOREIGN KEY (`pid`)
    REFERENCES `einvoice`.`s_ou` (`id`)
    ON DELETE CASCADE,
  CONSTRAINT `s_ou_tree_ibfk_2`
    FOREIGN KEY (`cid`)
    REFERENCES `einvoice`.`s_ou` (`id`)
    ON DELETE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `einvoice`.`s_serial`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `einvoice`.`s_serial` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `taxc` VARCHAR(14) NOT NULL,
  `type` VARCHAR(6) NOT NULL,
  `form` VARCHAR(11) NOT NULL,
  `serial` VARCHAR(6) NOT NULL,
  `min` INT(10) UNSIGNED NOT NULL,
  `max` INT(10) UNSIGNED NOT NULL,
  `cur` INT(10) UNSIGNED NOT NULL,
  `status` TINYINT(4) NOT NULL DEFAULT '3',
  `fd` DATETIME NOT NULL,
  `td` DATETIME NULL DEFAULT NULL,
  `dt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `uc` VARCHAR(20) NOT NULL,
  `uses` TINYINT(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `taxc` (`taxc` ASC, `form` ASC, `serial` ASC) VISIBLE,
  INDEX `uc` (`uc` ASC) VISIBLE,
  CONSTRAINT `s_serial_ibfk_1`
    FOREIGN KEY (`uc`)
    REFERENCES `einvoice`.`s_user` (`id`),
  CONSTRAINT `s_serial_ibfk_2`
    FOREIGN KEY (`taxc`)
    REFERENCES `einvoice`.`s_ou` (`taxc`))
ENGINE = InnoDB
AUTO_INCREMENT = 163
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `einvoice`.`s_seou`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `einvoice`.`s_seou` (
  `se` INT(10) UNSIGNED NOT NULL,
  `ou` INT(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`se`, `ou`),
  INDEX `ou` (`ou` ASC) VISIBLE,
  CONSTRAINT `s_seou_ibfk_1`
    FOREIGN KEY (`ou`)
    REFERENCES `einvoice`.`s_ou` (`id`),
  CONSTRAINT `s_seou_ibfk_2`
    FOREIGN KEY (`se`)
    REFERENCES `einvoice`.`s_serial` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `einvoice`.`s_taxo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `einvoice`.`s_taxo` (
  `id` VARCHAR(5) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4;


-- -----------------------------------------------------
-- Table `einvoice`.`s_xls`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `einvoice`.`s_xls` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `itype` VARCHAR(6) NOT NULL,
  `jc` VARCHAR(20) NOT NULL,
  `xc` VARCHAR(2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `itype` (`itype` ASC, `jc` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 169
DEFAULT CHARACTER SET = utf8mb4;

USE `einvoice` ;

-- -----------------------------------------------------
-- function cap_first
-- -----------------------------------------------------

DELIMITER $$
USE `einvoice`$$
CREATE DEFINER=`appuser`@`%` FUNCTION `cap_first`(input varchar(255)) RETURNS varchar(255) CHARSET utf8mb4
    DETERMINISTIC
begin
	declare len int;
	declare i int;
	set len   = char_length(input);
	set input = lower(input);
	set i = 0;
	while (i < len) do
		if (mid(input,i,1) = ' ' or i = 0) then
			if (i < len) then
				set input = concat(left(input,i),upper(mid(input,i + 1,1)),right(input,len - i - 1));
			end if;
		end if;
		set i = i + 1;
	end while;
	return input;
end$$

DELIMITER ;

-- -----------------------------------------------------
-- function get_fadd
-- -----------------------------------------------------

DELIMITER $$
USE `einvoice`$$
CREATE DEFINER=`appuser`@`%` FUNCTION `get_fadd`(
  addr varchar(255),
  prov varchar(3),
  dist varchar(5),
  ward varchar(7)
) RETURNS varchar(255) CHARSET utf8mb4
    DETERMINISTIC
begin 
return concat_ws(', ',nullif(addr, ''),get_local_name(ward),get_local_name(dist), get_local_name(prov));
end$$

DELIMITER ;

-- -----------------------------------------------------
-- function get_local_name
-- -----------------------------------------------------

DELIMITER $$
USE `einvoice`$$
CREATE DEFINER=`appuser`@`%` FUNCTION `get_local_name`(pcode varchar(7)) RETURNS varchar(100) CHARSET utf8mb4
    DETERMINISTIC
begin 
declare pname varchar(100);
if (nullif(pcode, '') is not null) then
	select  name into pname from s_loc where id = pcode;
end if;
return pname;
end$$

DELIMITER ;
USE `einvoice`;

DELIMITER $$
USE `einvoice`$$
CREATE
DEFINER=`appuser`@`%`
TRIGGER `einvoice`.`s_ou_ai`
AFTER INSERT ON `einvoice`.`s_ou`
FOR EACH ROW
begin
	insert into s_ou_tree (pid, cid, depth, path) values  (new.id, new.id, 0, concat(new.id, '/'));
	insert into s_ou_tree (pid, cid, depth, path) select  pid,new.id,depth + 1,concat(path, new.id, '/') 
from s_ou_tree
where cid = new.pid;
end$$

USE `einvoice`$$
CREATE
DEFINER=`appuser`@`%`
TRIGGER `einvoice`.`s_ou_au`
AFTER UPDATE ON `einvoice`.`s_ou`
FOR EACH ROW
begin 
if new.pid != old.pid  or new.pid is null  or old.pid is null then 
if old.pid is not null then 
delete t2 from s_ou_tree t1 join s_ou_tree t2 on t1.cid = t2.cid 
where t1.pid = old.id 
and t2.depth > t1.depth;
end if;
if new.pid is not null then
insert into s_ou_tree (pid, cid, depth, path) select  t1.pid, t2.cid, t1.depth + t2.depth + 1, concat(t1.path, t2.path) 
from s_ou_tree t1,s_ou_tree t2
where  t1.cid = new.pid and t2.pid = old.id;
end if;
end if;
end$$

USE `einvoice`$$
CREATE
DEFINER=`appuser`@`%`
TRIGGER `einvoice`.`s_ou_bi`
BEFORE INSERT ON `einvoice`.`s_ou`
FOR EACH ROW
begin 
 set new.fadd = get_fadd(new.addr,new.prov,new.dist,new.ward);
end$$

USE `einvoice`$$
CREATE
DEFINER=`appuser`@`%`
TRIGGER `einvoice`.`s_ou_bu`
BEFORE UPDATE ON `einvoice`.`s_ou`
FOR EACH ROW
begin 
 set new.fadd = get_fadd(new.addr,new.prov,new.dist,new.ward);
end$$

USE `einvoice`$$
CREATE
DEFINER=`appuser`@`%`
TRIGGER `einvoice`.`s_org_bi`
BEFORE INSERT ON `einvoice`.`s_org`
FOR EACH ROW
begin
  set new.fadd = get_fadd(new.addr,new.prov,new.dist,new.ward);
end$$

USE `einvoice`$$
CREATE
DEFINER=`appuser`@`%`
TRIGGER `einvoice`.`s_org_bu`
BEFORE UPDATE ON `einvoice`.`s_org`
FOR EACH ROW
begin
  set new.fadd = get_fadd(new.addr,new.prov,new.dist,new.ward);
end$$


DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
