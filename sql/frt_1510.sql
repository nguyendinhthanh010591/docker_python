ALTER TABLE "public"."s_user" ADD "local" INTEGER DEFAULT 0 NOT NULL;

ALTER TABLE "public"."s_user" ADD "change_pass_date" TIMESTAMP(3) WITHOUT TIME ZONE DEFAULT now();

ALTER TABLE "public"."s_user" ADD "login_number" INTEGER DEFAULT 0 NOT NULL;

ALTER TABLE "public"."s_user" ADD "change_pass" INTEGER DEFAULT 0 NOT NULL;

ALTER TABLE "public"."s_user" ADD "update_date" TIMESTAMP(3) WITHOUT TIME ZONE;

ALTER TABLE "public"."s_user" ALTER COLUMN "code" TYPE VARCHAR(20) USING ("code"::VARCHAR(20));

ALTER TABLE "public"."s_user" ALTER COLUMN "create_date" TYPE TIMESTAMP(3) WITHOUT TIME ZONE USING ("create_date"::TIMESTAMP(3) WITHOUT TIME ZONE);

ALTER TABLE "public"."s_user" ALTER COLUMN  "create_date" SET DEFAULT now();

ALTER TABLE "public"."s_user" ALTER COLUMN "id" TYPE VARCHAR(20) USING ("id"::VARCHAR(20));

ALTER TABLE "public"."s_user" ALTER COLUMN "last_login" TYPE TIMESTAMP(3) WITHOUT TIME ZONE USING ("last_login"::TIMESTAMP(3) WITHOUT TIME ZONE);

ALTER TABLE "public"."s_user" ALTER COLUMN "mail" TYPE VARCHAR(50) USING ("mail"::VARCHAR(50));

ALTER TABLE "public"."s_user" ALTER COLUMN "name" TYPE VARCHAR(50) USING ("name"::VARCHAR(50));

ALTER TABLE "public"."s_user" ALTER COLUMN "ou" TYPE INTEGER USING ("ou"::INTEGER);

ALTER TABLE "public"."s_user" ALTER COLUMN "pass" TYPE VARCHAR(200) USING ("pass"::VARCHAR(200));

ALTER TABLE "public"."s_user" ALTER COLUMN "pos" TYPE VARCHAR(50) USING ("pos"::VARCHAR(50));

ALTER TABLE "public"."s_user" ALTER COLUMN "uc" TYPE SMALLINT USING ("uc"::SMALLINT);


ALTER TABLE "public"."s_role" ADD "pid" INTEGER;

ALTER TABLE "public"."s_role" ADD "sort" VARCHAR(10);

ALTER TABLE "public"."s_role" ADD "active" SMALLINT;

ALTER TABLE "public"."s_role" ADD "menu_detail" VARCHAR(150);

ALTER TABLE "public"."s_role" ALTER COLUMN "code" TYPE VARCHAR(100) USING ("code"::VARCHAR(100));

ALTER TABLE "public"."s_role" ALTER COLUMN "id" TYPE INTEGER USING ("id"::INTEGER);

ALTER TABLE "public"."s_role" ALTER COLUMN  "id" SET DEFAULT NULL;

ALTER TABLE "public"."s_role" ALTER COLUMN "name" TYPE VARCHAR(255) USING ("name"::VARCHAR(255));

ALTER TABLE "public"."s_role" ALTER COLUMN  "name" DROP NOT NULL;

ALTER TABLE "public"."s_role" ADD CONSTRAINT "s_role_fk01" FOREIGN KEY ("pid") REFERENCES "public"."s_role" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE TABLE "public"."s_xls_temp" ("id" VARCHAR(20), "ic" VARCHAR(50), "idt" TIMESTAMP(3) WITHOUT TIME ZONE, "form" VARCHAR(11), "serial" VARCHAR(10), "seq" VARCHAR(8), "dt" TIMESTAMP(3) WITHOUT TIME ZONE DEFAULT now() NOT NULL);



CREATE TABLE "s_inv_adj" ("id" BIGSERIAL NOT NULL, "inv_id" bigint, "form" INTEGER, "serial" VARCHAR(10), "stax" VARCHAR(20), "type" INTEGER, "note" text, "so_tb" VARCHAR(50), "ngay_tb" date, "createdate" TIMESTAMP(3) WITHOUT TIME ZONE, "statuscqt" INTEGER, "msgcqt" text, "listinv_id" bigint, "th_type" INTEGER NOT NULL, "seq" VARCHAR(8), "curr" VARCHAR(3), CONSTRAINT "PK__s_inv_ad__3213E83FA7E7893B" PRIMARY KEY ("id"));

CREATE TABLE "s_list_inv_cancel" ("id" BIGSERIAL NOT NULL, "inv_id" INTEGER NOT NULL, "doc" jsonb NOT NULL, "invtype" INTEGER NOT NULL, "ou" INTEGER NOT NULL, "serial" VARCHAR(6), "type" VARCHAR(10), "idt" TIMESTAMP(3) WITHOUT TIME ZONE, "cdt" TIMESTAMP(3) WITHOUT TIME ZONE NOT NULL, "listinv_id" bigint, "stax" VARCHAR(14), "curr" VARCHAR(500), CONSTRAINT "PK__s_list_i__3213E83F0E4528B3" PRIMARY KEY ("id"));

CREATE TABLE "s_listvalues" ("keyname" VARCHAR(60) NOT NULL, "keyvalue" text NOT NULL, "keytype" VARCHAR(30) NOT NULL);

CREATE TABLE "info_taxpayer" ("id" numeric(18) NOT NULL, "taxc" VARCHAR(14) NOT NULL, "namecom" VARCHAR(255), "type" VARCHAR(10), "status" numeric(18), "effectdate" TIMESTAMP(3) WITHOUT TIME ZONE, "managercomc" VARCHAR(10), "chapter" VARCHAR(10), "changedate" TIMESTAMP(3) WITHOUT TIME ZONE, "economictype" VARCHAR(10), "contbusires" VARCHAR(10), "passport" VARCHAR(20), "createdby" VARCHAR(150) NOT NULL, "statusscan" numeric(18) NOT NULL, "messres" VARCHAR(255) NOT NULL, "statuscoderes" numeric(18) NOT NULL, "createddate" TIMESTAMP(3) WITHOUT TIME ZONE, "updateddate" TIMESTAMP(3) WITHOUT TIME ZONE);

create sequence "info_taxpayer_id_seq" start with 1;

CREATE TABLE "type_invoice" ("id" numeric(18) NOT NULL, "name" VARCHAR(255));

create sequence "type_invoice_id_seq" start with 1;

CREATE TABLE "van_einv_listid_msg" ("id" BIGSERIAL NOT NULL, "pid" bigint, "type" VARCHAR(10), "status_scan" INTEGER, "createdate" TIMESTAMP(3) WITHOUT TIME ZONE DEFAULT now(), "sendvandate" TIMESTAMP(3) WITHOUT TIME ZONE, "msg" text, "xml" text, "parent_id" NCHAR(10), "status_sendback" INTEGER);

CREATE TABLE "s_inv_sum" ("idt" date NOT NULL, "stax" VARCHAR(20) NOT NULL, "type" VARCHAR(6) NOT NULL, "form" VARCHAR(11) NOT NULL, "serial" VARCHAR(8), "i0" numeric(18), "i1" numeric(18), "i2" numeric(18), "i3" numeric(18), "i4" numeric(18));

CREATE TABLE "b_inv" ("id" SERIAL NOT NULL, "doc" jsonb NOT NULL, "xml" text, "idt" TIMESTAMP(3) WITHOUT TIME ZONE, "adt" TIMESTAMP(3) WITHOUT TIME ZONE, "type" VARCHAR(10), "form" VARCHAR(11), "serial" VARCHAR(8), "seq" VARCHAR(7), "paym" VARCHAR(100), "curr" VARCHAR(3), "exrt" numeric(8, 2), "sname" VARCHAR(255), "stax" VARCHAR(14), "saddr" VARCHAR(255), "smail" VARCHAR(255), "stel" VARCHAR(255), "sacc" VARCHAR(255), "sbank" VARCHAR(255), "buyer" VARCHAR(255), "bname" VARCHAR(255), "bcode" VARCHAR(100), "btax" VARCHAR(14), "baddr" VARCHAR(255), "bmail" VARCHAR(255), "btel" VARCHAR(255), "bacc" VARCHAR(255), "bbank" VARCHAR(255), "note" VARCHAR(255), "sumv" numeric(19, 4), "vatv" numeric(19, 4), "totalv" numeric(19, 4), "ou" INTEGER, "uc" VARCHAR(20), "paid" INTEGER, CONSTRAINT "PK__b_inv__3213E83F5F1E19DA" PRIMARY KEY ("id"));

CREATE TABLE "s_statement" ("id" BIGSERIAL NOT NULL, "type" VARCHAR(15), "formname" VARCHAR(100), "regtype" INTEGER, "sname" VARCHAR(400), "stax" VARCHAR(14), "taxoname" VARCHAR(100), "taxo" VARCHAR(5), "contactname" VARCHAR(50), "contactaddr" VARCHAR(400), "contactemail" VARCHAR(50), "contactphone" VARCHAR(20), "place" VARCHAR(50), "createdt" TIMESTAMP(3) WITHOUT TIME ZONE, "hascode" INTEGER DEFAULT 0, "sendtype" INTEGER, "dtsendtype" VARCHAR(20), "invtype" VARCHAR(100), "doc" jsonb, "status" INTEGER DEFAULT 1 NOT NULL, "cqtstatus" INTEGER, "vdt" TIMESTAMP(3) WITHOUT TIME ZONE, "cqtmess" VARCHAR(255), "xml" text, "statustvan" INTEGER DEFAULT 0 NOT NULL, "xml_received" text, "xml_accepted" text, "paxoname" VARCHAR(100), "paxo" VARCHAR(5), "json_received" text, "json_accepted" text, "uc" VARCHAR(20), CONSTRAINT "s_statement_pk" PRIMARY KEY ("id"));

CREATE TABLE "temp_taxpayer" ("id" numeric(18) NOT NULL, "taxc" VARCHAR(14) NOT NULL, "createdby" VARCHAR(150) NOT NULL, "statusscan" numeric(18) NOT NULL, "creaateddate" TIMESTAMP(3) WITHOUT TIME ZONE, "updateddate" TIMESTAMP(3) WITHOUT TIME ZONE);

create sequence "temp_taxpayer_id_seq" start with 1;

CREATE TABLE "van_msg_out" ("id" numeric(18) NOT NULL, "invid" VARCHAR(36) NOT NULL, "xml2base64" text NOT NULL, "createdby" VARCHAR(150) NOT NULL, "statustvan" numeric(18), "createddate" TIMESTAMP(3) WITHOUT TIME ZONE, "senddate" TIMESTAMP(3) WITHOUT TIME ZONE DEFAULT now(), "idtypeinv" numeric(18) NOT NULL, "messsendcode" VARCHAR(10));

create sequence "van_msg_out_id_seq" start with 1;

CREATE TABLE "s_trans" ("ID" numeric(20) NOT NULL, "refNo" VARCHAR(50), "valueDate" date, "customerID" VARCHAR(50), "customerName" VARCHAR(500), "taxCode" VARCHAR(50), "customerAddr" VARCHAR(500), "isSpecial" VARCHAR(1), "curr" VARCHAR(10), "exrt" FLOAT(53), "vrt" VARCHAR(50), "chargeAmount" VARCHAR(50), "vcontent" VARCHAR(500), "amount" DECIMAL(18, 2), "vat" DECIMAL(18, 2), "total" DECIMAL(18, 2), "create_date" TIMESTAMP(3) WITHOUT TIME ZONE DEFAULT now(), "tranID" VARCHAR(100), "trantype" numeric(1), "trancol" numeric(1), "status" numeric(1), "customerAcc" VARCHAR(50), "inv_id" numeric(18), "inv_date" date, "ma_nv" VARCHAR(50), "ma_ks" VARCHAR(50), "last_update" TIMESTAMP(3) WITHOUT TIME ZONE, "segment" VARCHAR(50), CONSTRAINT "PK_s_trans" PRIMARY KEY ("ID"));

CREATE TABLE "van_history" ("id" numeric(36) NOT NULL, "type_invoice" numeric(18) NOT NULL, "id_ref" bigint, "status" numeric(18), "datetime_trans" TIMESTAMP(3) WITHOUT TIME ZONE, "description" text, "r_xml" text);

CREATE TABLE "van_msg_out_dtl" ("id" numeric(18) NOT NULL, "iddatatvan" numeric(18) NOT NULL, "invid" VARCHAR(36) NOT NULL, "createdby" VARCHAR(150) NOT NULL, "statustvan" numeric(18), "statusres" numeric(18), "messsendcode" VARCHAR(10), "messsend" text, "messres" VARCHAR(255), "messrescode" VARCHAR(10), "senddate" TIMESTAMP(3) WITHOUT TIME ZONE, "resdate" TIMESTAMP(3) WITHOUT TIME ZONE, "transid" VARCHAR(225), "idtypeinv" numeric(18) NOT NULL, "status_response" INTEGER DEFAULT 0, "datecheckmsg" TIMESTAMP(3) WITHOUT TIME ZONE DEFAULT now());

create sequence "van_msg_out_dtl_id_seq" start with 1;

CREATE TABLE "van_msg_in_dtl" ("id" numeric(18) NOT NULL, "receivejson" text NOT NULL, "taxc" text, "namenotice" VARCHAR(500), "typenotice" INTEGER, "contentnotice" text, "createddtnotice" TIMESTAMP(3) WITHOUT TIME ZONE, "createddate" TIMESTAMP(3) WITHOUT TIME ZONE DEFAULT now(), "status_scan" numeric(18), "updateddate" TIMESTAMP(3) WITHOUT TIME ZONE, "transid" VARCHAR(225) DEFAULT '0', "status_response" INTEGER DEFAULT 0, "datecheckmsg" TIMESTAMP(3) WITHOUT TIME ZONE DEFAULT now(), "msg_in_id" numeric(18), CONSTRAINT "PK_van_msg_in_dtl" PRIMARY KEY ("id"));

create sequence "van_msg_in_dtl_id_seq" start with 1;

CREATE TABLE "sign_xml" ("id" numeric(18) NOT NULL, "tax_code" VARCHAR(14), "type_xml" VARCHAR(20), "xmlOriginal" text NOT NULL, "xmlSigned" text, "createddate" TIMESTAMP(3) WITHOUT TIME ZONE, "updateddate" TIMESTAMP(3) WITHOUT TIME ZONE);

create sequence "sign_xml_id_seq" start with 1;

CREATE TABLE "s_list_inv_id" ("id" bigint, "inv_id" bigint, "loai" VARCHAR(50));

CREATE TABLE "s_tmp_list_inv" ("keyname" VARCHAR(60), "id" bigint, "kindinv" INTEGER, "curr" VARCHAR(10), "idt" TIMESTAMP(3) WITHOUT TIME ZONE, "type" VARCHAR(10), "form" VARCHAR(11), "serial" VARCHAR(6), "seq" VARCHAR(8));



CREATE TABLE "s_invdoctemp" ("id" BIGSERIAL NOT NULL, "invid" bigint NOT NULL, "doc" jsonb NOT NULL, "dt" TIMESTAMP(3) WITHOUT TIME ZONE DEFAULT now() NOT NULL, "status" INTEGER NOT NULL, "xml" text, "iserror" INTEGER DEFAULT 0 NOT NULL, "error_desc" text, CONSTRAINT "PRIMARY_s_invdoctemp" PRIMARY KEY ("id"));

CREATE TABLE "s_wrongnotice_process" ("id" SERIAL NOT NULL, "ou" INTEGER, "id_wn" INTEGER, "id_inv" text, "doc" jsonb, "xml" text, "dt" TIMESTAMP(3) WITHOUT TIME ZONE DEFAULT now() NOT NULL, "status" VARCHAR(15) NOT NULL, "status_cqt" INTEGER, "xml_received" text, "json_received" text, "xml_accepted" text, "json_accepted" text, "dien_giai" text, "taxc" VARCHAR(14), "form" VARCHAR(14), "serial" VARCHAR(14), "seq" VARCHAR(14), "type" VARCHAR(14), "transid" VARCHAR(255), CONSTRAINT "s_wrongnotice_process_pk" PRIMARY KEY ("id"));

CREATE TABLE "s_user_pass" ("user_id" VARCHAR(20) NOT NULL, "pass" VARCHAR(200) NOT NULL, "change_date" TIMESTAMP(3) WITHOUT TIME ZONE NOT NULL, CONSTRAINT "PK__s_user_p__518C386C17C39B87" PRIMARY KEY ("user_id", "pass"));

CREATE TABLE "van_msg_in" ("id" numeric(18) NOT NULL, "receive_arr_json" jsonb NOT NULL, "status_scan" numeric(18), "createddate" TIMESTAMP(3) WITHOUT TIME ZONE, "updateddate" TIMESTAMP(3) WITHOUT TIME ZONE, "createby" VARCHAR(50), "err_msg" text);

create sequence "van_msg_in_id_seq" start with 1;


CREATE TABLE "s_revoce_ca" ("taxc" VARCHAR(20) NOT NULL, "serial_number" VARCHAR(255) NOT NULL, "valid_from" TIMESTAMP(3) WITHOUT TIME ZONE DEFAULT NULL, "valid_to" TIMESTAMP(3) WITHOUT TIME ZONE DEFAULT NULL, "status" INTEGER DEFAULT 0, "description" VARCHAR(1000), "check_date" TIMESTAMP(3) WITHOUT TIME ZONE, "created_date" TIMESTAMP(3) WITHOUT TIME ZONE DEFAULT now(), CONSTRAINT "einv_revoce_pk" PRIMARY KEY ("taxc"));

CREATE TABLE "s_list_inv" ("id" bigint NOT NULL, "listinv_code" VARCHAR(15), "s_tax" VARCHAR(14) NOT NULL, "ou" INTEGER NOT NULL, "created_date" TIMESTAMP(3) WITHOUT TIME ZONE DEFAULT now() NOT NULL, "created_by" VARCHAR(50), "approve_date" TIMESTAMP(3) WITHOUT TIME ZONE, "approve_by" VARCHAR(20), "s_name" VARCHAR(400) NOT NULL, "item_type" VARCHAR(20) NOT NULL, "listinvoice_num" VARCHAR(5), "period_type" VARCHAR(1) NOT NULL, "period" VARCHAR(10) NOT NULL, "status" INTEGER DEFAULT 0 NOT NULL, "doc" jsonb, "xml" text, "times" INTEGER DEFAULT 0 NOT NULL, "status_received" INTEGER, "xml_received" text, "json_received" text, "edittimes" INTEGER, "error_msg_van" text, "curr" VARCHAR(3), "transid" VARCHAR(255), CONSTRAINT "s_list_inv_pk" PRIMARY KEY ("id"));

-- ALTER TABLE "s_serial" ADD CONSTRAINT "FK__s_serial__uc__30592A6F" FOREIGN KEY ("uc") REFERENCES "s_user" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

-- ALTER TABLE "b_inv" ADD CONSTRAINT "UQ__b_inv__B09EB31CAC3B778A" UNIQUE ("stax", "form", "serial", "seq");

-- ALTER TABLE "s_wrongnotice_process" ADD CONSTRAINT "UQ__s_wrongn__01487863BB3969F7" UNIQUE ("id_wn");

-- ALTER TABLE "s_user_pass" ADD CONSTRAINT "FK__s_user_pa__user___49CEE3AF" FOREIGN KEY ("user_id") REFERENCES "s_user" ("id") ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE "s_serial" ADD "ut" TIMESTAMP(3) WITHOUT TIME ZONE DEFAULT now() NOT NULL;

ALTER TABLE "s_serial" ADD "intg" INTEGER DEFAULT 2 NOT NULL;

ALTER TABLE "s_sys_logs" ADD "ip" VARCHAR(50);

ALTER TABLE "s_serial" ADD "alertleft" INTEGER DEFAULT 0 NOT NULL;

ALTER TABLE "s_serial" ADD "leftwarn" INTEGER DEFAULT 1;

ALTER TABLE "s_serial" ADD "sendtype" INTEGER;

ALTER TABLE "s_serial" ADD "invtype" INTEGER;

ALTER TABLE "s_serial" ADD "degree_config" INTEGER DEFAULT 119;

ALTER TABLE "s_org" ADD "vtaxc" VARCHAR(14);

ALTER TABLE "s_ou" ADD "smtpconf" text;

ALTER TABLE "s_ou" ADD "filenameca" VARCHAR(255);

ALTER TABLE "s_ou" ADD "sct" SMALLINT DEFAULT 0 NOT NULL;

ALTER TABLE "s_ou" ADD "sc" SMALLINT DEFAULT 0 NOT NULL;

ALTER TABLE "s_ou" ADD "bcc" VARCHAR(100);

ALTER TABLE "s_ou" ADD "qrc" SMALLINT DEFAULT 0 NOT NULL;

ALTER TABLE "s_ou" ADD "na" SMALLINT DEFAULT 2 NOT NULL;

ALTER TABLE "s_ou" ADD "nq" SMALLINT DEFAULT 2 NOT NULL;

ALTER TABLE "s_ou" ADD "np" SMALLINT DEFAULT 5 NOT NULL;

ALTER TABLE "s_ou" ADD "itype" VARCHAR(255) DEFAULT '01GTKT' NOT NULL;

ALTER TABLE "s_ou" ADD "ts" SMALLINT DEFAULT 1 NOT NULL;

ALTER TABLE "s_ou" ADD "dsignfrom" TIMESTAMP(3) WITHOUT TIME ZONE;

ALTER TABLE "s_ou" ADD "dsignto" TIMESTAMP(3) WITHOUT TIME ZONE;

ALTER TABLE "s_ou" ADD "degree_config" SMALLINT DEFAULT 119 NOT NULL;

ALTER TABLE "s_ou" ADD "cfg_ihasdocid" VARCHAR(45);

ALTER TABLE "s_ou" ADD "cfg_uselist" VARCHAR(45) DEFAULT '' NOT NULL;

ALTER TABLE "s_ou" ADD "hascode" SMALLINT DEFAULT 0 NOT NULL;

ALTER TABLE "s_ou" ADD "dsignserial" VARCHAR(100);

ALTER TABLE "s_ou" ADD "dsignsubject" VARCHAR(200);

ALTER TABLE "s_ou" ADD "dsignissuer" VARCHAR(200);

ALTER TABLE "s_ou" ADD "receivermail" VARCHAR(200);



ALTER TABLE "s_ou" ADD "adsconf" text;

ALTER TABLE "s_ou" ADD "place" VARCHAR(255);

ALTER TABLE "s_ou" ADD "chk_revoke" SMALLINT DEFAULT 2;

ALTER TABLE "s_inv" ADD "dds_sent" INTEGER DEFAULT 0;

ALTER TABLE "s_inv" ADD "dds_stt" INTEGER;

ALTER TABLE "s_inv" ADD "dds" INTEGER;

ALTER TABLE "s_inv" ADD "file_name" VARCHAR(100);

ALTER TABLE "s_inv" ADD "sendmaildate" TIMESTAMP(3) WITHOUT TIME ZONE;

ALTER TABLE "s_inv" ADD "sendsmsdate" TIMESTAMP(3) WITHOUT TIME ZONE;

ALTER TABLE "s_inv" ADD "vseq" VARCHAR(18);

ALTER TABLE "s_inv" ADD "sendmailstatus" INTEGER;

ALTER TABLE "s_inv" ADD "sendsmsstatus" INTEGER;

ALTER TABLE "s_inv" ADD "sendmailnum" INTEGER;

ALTER TABLE "s_inv" ADD "sendsmsnum" INTEGER;

ALTER TABLE "s_inv" ADD "bcode" VARCHAR(500);

ALTER TABLE "s_inv" ADD "taxocode" VARCHAR(20);

ALTER TABLE "s_inv" ADD "invlistdt" TIMESTAMP(3) WITHOUT TIME ZONE;

ALTER TABLE "s_inv" ADD "discountamt" numeric(18);

ALTER TABLE "s_inv" ADD "ordno" VARCHAR(500);

ALTER TABLE "s_inv" ADD "whsfr" VARCHAR(500);

ALTER TABLE "s_inv" ADD "whsto" VARCHAR(500);

ALTER TABLE "s_inv" ADD "edt" TIMESTAMP(3) WITHOUT TIME ZONE;

ALTER TABLE "s_inv" ADD "hascode" numeric(38);

ALTER TABLE "s_inv" ADD "invtype" VARCHAR(500);

ALTER TABLE "s_inv" ADD "invlistnum" VARCHAR(50);

ALTER TABLE "s_inv" ADD "period_type" VARCHAR(1);

ALTER TABLE "s_inv" ADD "list_invid" bigint;

ALTER TABLE "s_inv" ADD "orddt" TIMESTAMP(3) WITHOUT TIME ZONE;

ALTER TABLE "s_inv" ADD "vehic" VARCHAR(500);

ALTER TABLE "s_inv" ADD "inv_adj" bigint;

ALTER TABLE "s_inv" ADD "period" VARCHAR(10);

ALTER TABLE "s_inv" ADD "th_type" INTEGER;

ALTER TABLE "s_inv" ADD "wno_adj" INTEGER;

ALTER TABLE "s_inv" ADD "wnadjtype" INTEGER;

ALTER TABLE "s_inv" ADD "list_invid_bx" bigint;

ALTER TABLE "s_inv" ADD "check_send_back" INTEGER;

ALTER TABLE "s_inv" ADD "transid" VARCHAR(255);

ALTER TABLE "s_seusr" DROP CONSTRAINT "s_seusr_fk2";

ALTER TABLE "s_seusr" ADD CONSTRAINT "s_seusr_fk2" FOREIGN KEY ("se") REFERENCES "s_serial" ("id");

alter table "s_group" alter column  "approve" set default '1';

alter table "s_group" alter column "des" type varchar(200) using ("des"::varchar(200));

alter table "s_group" alter column  "des" set default '1';

alter table "s_group_role" alter column "group_id" type float(53) using ("group_id"::float(53));

alter table "s_group_user" alter column "group_id" type float(53) using ("group_id"::float(53));

alter table "s_group" alter column "id" type integer using ("id"::integer);

alter table "s_invd" alter column "id" type decimal(18) using ("id"::decimal(18));

alter table "s_invd" alter column  "id" set default nextval('iseq_invd') ;

alter table "s_invd" alter column "inv_id" type decimal(18) using ("inv_id"::decimal(18));

alter table "s_group" alter column "name" type varchar(200) using ("name"::varchar(200));

alter table "s_group" alter column  "name" drop not null;

alter table "s_group" alter column "ou" type integer using ("ou"::integer);

alter table "s_group_role" alter column "role_id" type varchar(20) using ("role_id"::varchar(20));

alter table "s_refcode" alter column "abbreviation" type text using ("abbreviation"::text);

-- alter table "s_org" alter column "acc" type varchar(255) using ("acc"::varchar(255));

-- alter table "s_ou" alter column "acc" type varchar(255) using ("acc"::varchar(255));

alter table "s_sys_logs" alter column "action" type varchar(255) using ("action"::varchar(255));

alter table "s_role" alter column "active" type smallint using ("active"::smallint);

-- alter table "s_org" alter column "addr" type varchar(255) using ("addr"::varchar(255));




-- alter table "s_ou" alter column "addr" type varchar(255) using ("addr"::varchar(255));

-- alter table "s_inv" alter column "adjdes" type varchar(255) using ("adjdes"::varchar(255));

-- --alter table "s_inv" alter column  "adjdes" set default null;

-- alter table "s_inv" alter column "adjseq" type varchar(26) using ("adjseq"::varchar(26));

-- --alter table "s_inv" alter column  "adjseq" set default null;

-- alter table "s_inv" alter column "adjtyp" type smallint using ("adjtyp"::smallint);

-- --alter table "s_inv" alter column  "adjtyp" set default null;

alter table "s_dcm" alter column "atr" type varchar(4000) using ("atr"::varchar(4000));

alter table "s_f_fld" alter column "atr" type varchar(255) using ("atr"::varchar(255));

-- --alter table "s_inv" alter column "bacc" type varchar(255) using ("bacc"::varchar(255));

-- --alter table "s_inv" alter column  "bacc" set default null;

-- --alter table "s_inv" alter column "baddr" type varchar(255) using ("baddr"::varchar(255));

-- --alter table "s_inv" alter column  "baddr" set default null;

--alter table "s_org" alter column "bank" type varchar(255) using ("bank"::varchar(255));

alter table "s_ou" alter column "bank" type varchar(255) using ("bank"::varchar(255));

-- alter table "s_inv" alter column "bbank" type varchar(255) using ("bbank"::varchar(255));

-- alter table "s_inv" alter column  "bbank" set default null;

-- alter table "s_inv" alter column "bmail" type varchar(255) using ("bmail"::varchar(255));

-- alter table "s_inv" alter column  "bmail" set default null;


-- alter table "s_inv" alter column "bname" type varchar(500) using ("bname"::varchar(500));

-- alter table "s_inv" alter column  "bname" set default null;


-- alter table "s_inv" alter column "btax" type varchar(14) using ("btax"::varchar(14));

-- alter table "s_inv" alter column  "btax" set default null;


-- alter table "s_inv" alter column "btel" type varchar(255) using ("btel"::varchar(255));

-- alter table "s_inv" alter column  "btel" set default null;


-- alter table "s_inv" alter column "buyer" type varchar(500) using ("buyer"::varchar(500));

-- alter table "s_inv" alter column  "buyer" set default null;


-- alter table "s_inv" alter column "c0" type varchar(255) using ("c0"::varchar(255));

-- alter table "s_inv" alter column  "c0" set default null;


alter table "s_org" alter column "c0" type varchar(255) using ("c0"::varchar(255));



-- alter table "s_inv" alter column "c1" type varchar(255) using ("c1"::varchar(255));

-- alter table "s_inv" alter column  "c1" set default null;


alter table "s_org" alter column "c1" type varchar(255) using ("c1"::varchar(255));

-- alter table "s_inv" alter column "c2" type varchar(255) using ("c2"::varchar(255));

-- alter table "s_inv" alter column  "c2" set default null;

alter table "s_org" alter column "c2" type varchar(255) using ("c2"::varchar(255));

-- alter table "s_inv" alter column "c3" type varchar(255) using ("c3"::varchar(255));

-- alter table "s_inv" alter column  "c3" set default null;
alter table "s_org" alter column "c3" type varchar(255) using ("c3"::varchar(255));

-- alter table "s_inv" alter column "c4" type varchar(255) using ("c4"::varchar(255));

-- alter table "s_inv" alter column  "c4" set default null;


alter table "s_org" alter column "c4" type varchar(255) using ("c4"::varchar(255));



-- alter table "s_inv" alter column "c5" type varchar(255) using ("c5"::varchar(255));

-- alter table "s_inv" alter column  "c5" set default null;


alter table "s_org" alter column "c5" type varchar(255) using ("c5"::varchar(255));



-- alter table "s_inv" alter column "c6" type varchar(255) using ("c6"::varchar(255));

-- alter table "s_inv" alter column  "c6" set default null;


alter table "s_org" alter column "c6" type varchar(255) using ("c6"::varchar(255));



-- alter table "s_inv" alter column "c7" type varchar(255) using ("c7"::varchar(255));

-- alter table "s_inv" alter column  "c7" set default null;


alter table "s_org" alter column "c7" type varchar(255) using ("c7"::varchar(255));



-- alter table "s_inv" alter column "c8" type varchar(255) using ("c8"::varchar(255));

-- alter table "s_inv" alter column  "c8" set default null;


alter table "s_org" alter column "c8" type varchar(255) using ("c8"::varchar(255));



-- alter table "s_inv" alter column "c9" type varchar(255) using ("c9"::varchar(255));

-- alter table "s_inv" alter column  "c9" set default null;


alter table "s_org" alter column "c9" type varchar(255) using ("c9"::varchar(255));



alter table "s_report_tmp" alter column "c_id" type bigint using ("c_id"::bigint);

alter table "s_report_tmp" alter column  "c_id" drop not null;


alter table "s_report_tmp" alter column "cancel" type integer using ("cancel"::integer);


-- alter table "s_inv" alter column "cde" type varchar(255) using ("cde"::varchar(255));

-- alter table "s_inv" alter column "cdt" type timestamp(3) without time zone using ("cdt"::timestamp(3) without time zone);


alter table "s_user" alter column "change_pass" type integer using ("change_pass"::integer);

alter table "s_user" alter column "change_pass_date" type timestamp(3) without time zone using ("change_pass_date"::timestamp(3) without time zone);

alter table "s_user" alter column  "change_pass_date" set default now();

-- alter table "s_inv" alter column "cid" type varchar(3000) using ("cid"::varchar(3000));


alter table "s_report_tmp" alter column "clist" type text using ("clist"::text);


alter table "s_gns" alter column "code" type varchar(30) using ("code"::varchar(30));

alter table "s_org" alter column "code" type varchar(100) using ("code"::varchar(100));


alter table "s_ou" alter column "code" type varchar(20) using ("code"::varchar(20));


alter table "s_role" alter column "code" type varchar(100) using ("code"::varchar(100));


alter table "s_user" alter column "code" type varchar(20) using ("code"::varchar(20));


-- alter table "s_inv_file" alter column "content" type text using ("content"::text);


alter table "s_user" alter column "create_date" type timestamp(3) without time zone using ("create_date"::timestamp(3) without time zone);

alter table "s_user" alter column  "create_date" set default now();




alter table "s_ex" alter column "cur" type varchar(3) using ("cur"::varchar(3));


-- alter table "s_inv" alter column "curr" type varchar(3) using ("curr"::varchar(3));

-- alter table "s_inv" alter column  "curr" set default null;


-- alter table "s_inv" alter column "cvt" type smallint using ("cvt"::smallint);


alter table "s_cat" alter column "des" type varchar(100) using ("des"::varchar(100));

alter table "s_serial" alter column "des" type varchar(200) using ("des"::varchar(200));

--alter table "s_org" alter column "dist" type varchar(5) using ("dist"::varchar(5));


--alter table "s_ou" alter column "dist" type varchar(5) using ("dist"::varchar(5));


-- alter table "s_inv" alter column "doc" type jsonb using ("doc"::jsonb);


alter table "s_sys_logs" alter column "doc" type jsonb using ("doc"::jsonb);


alter table "s_refcode" alter column "domain" type text using ("domain"::text);


alter table "s_ex" alter column "dt" type timestamp(3) without time zone using ("dt"::timestamp(3) without time zone);

alter table "s_ex" alter column  "dt" set default now();


-- alter table "s_inv" alter column "dt" type timestamp(3) without time zone using ("dt"::timestamp(3) without time zone);

-- alter table "s_inv" alter column  "dt" set default now();


alter table "s_serial" alter column "dt" type timestamp(3) without time zone using ("dt"::timestamp(3) without time zone);

alter table "s_serial" alter column  "dt" set default now();

alter table "s_sys_logs" alter column "dt" type timestamp(3) without time zone using ("dt"::timestamp(3) without time zone);

alter table "s_xls_temp" alter column "dt" type timestamp(3) without time zone using ("dt"::timestamp(3) without time zone);

alter table "s_xls_temp" alter column  "dt" set default now();

alter table "s_dcm" alter column "dtl" type smallint using ("dtl"::smallint);


alter table "s_sys_logs" alter column "dtl" type varchar(1000) using ("dtl"::varchar(1000));

-- alter table "s_inv" alter column "error_msg_van" type text using ("error_msg_van"::text);

-- alter table "s_inv" alter column  "exrt" set default null;


--alter table "s_org" alter column "fadd" type varchar(500) using ("fadd"::varchar(500));


--alter table "s_ou" alter column "fadd" type varchar(255) using ("fadd"::varchar(255));


alter table "s_serial" alter column "fd" type timestamp(3) without time zone using ("fd"::timestamp(3) without time zone);

alter table "s_f_fld" alter column "feature" type varchar(255) using ("feature"::varchar(255));


alter table "s_f_fld" alter column "fld_id" type varchar(30) using ("fld_id"::varchar(30));


alter table "s_f_fld" alter column "fld_name" type varchar(30) using ("fld_name"::varchar(30));


alter table "s_f_fld" alter column "fld_typ" type varchar(50) using ("fld_typ"::varchar(50));


alter table "s_sys_logs" alter column "fnc_id" type varchar(30) using ("fnc_id"::varchar(30));

alter table "s_f_fld" alter column "fnc_name" type varchar(30) using ("fnc_name"::varchar(30));


alter table "s_sys_logs" alter column "fnc_name" type varchar(255) using ("fnc_name"::varchar(255));

alter table "s_f_fld" alter column "fnc_url" type varchar(15) using ("fnc_url"::varchar(15));


alter table "s_sys_logs" alter column "fnc_url" type varchar(255) using ("fnc_url"::varchar(255));

-- alter table "s_inv" alter column "form" type varchar(11) using ("form"::varchar(11));

-- alter table "s_inv" alter column  "form" set default null;


alter table "s_report_tmp" alter column "form" type varchar(50) using ("form"::varchar(50));


alter table "s_serial" alter column "form" type varchar(11) using ("form"::varchar(11));

alter table "s_refcode" alter column "highval" type text using ("highval"::text);


-- alter table "s_inv" alter column "ic" type varchar(36) using ("ic"::varchar(36));


alter table "s_cat" alter column "id" type integer using ("id"::integer);

alter table "s_cat" alter column  "id" set default null;

alter table "s_dcd" alter column "id" type varchar(30) using ("id"::varchar(30));

alter table "s_dcm" alter column "id" type integer using ("id"::integer);

alter table "s_dcm" alter column  "id" set default null;


alter table "s_ex" alter column "id" type integer using ("id"::integer);

alter table "s_ex" alter column  "id" set default null;


alter table "s_f_fld" alter column "id" type integer using ("id"::integer);

alter table "s_f_fld" alter column  "id" set default null;


alter table "s_gns" alter column "id" type integer using ("id"::integer);

alter table "s_gns" alter column  "id" set default null;

-- alter table "s_inv" alter column "id" type bigint using ("id"::bigint);


-- alter table "s_inv_file" alter column "id" type bigint using ("id"::bigint);


alter table "s_loc" alter column "id" type varchar(7) using ("id"::varchar(7));


alter table "s_org" alter column "id" type integer using ("id"::integer);

alter table "s_org" alter column  "id" set default null;


alter table "s_ou" alter column "id" type integer using ("id"::integer);

alter table "s_ou" alter column  "id" set default null;


alter table "s_role" alter column "id" type integer using ("id"::integer);


alter table "s_serial" alter column "id" type integer using ("id"::integer);

alter table "s_serial" alter column  "id" set default null;

alter table "s_sys_logs" alter column "id" type decimal(18) using ("id"::decimal(18));

--alter table "s_sys_logs" alter column  "id" set default replace(replace(replace(convert([varchar],now(),(20)),'-',''),':',''),' ','')+replace(str(next value for [iseq_sys_logs],(3)),' ','0');

alter table "s_taxo" alter column "id" type varchar(5) using ("id"::varchar(5));


alter table "s_user" alter column "id" type varchar(20) using ("id"::varchar(20));


alter table "s_xls" alter column "id" type integer using ("id"::integer);

alter table "s_xls" alter column  "id" set default null;


-- alter table "s_inv" alter column "idt" type timestamp(3) without time zone using ("idt"::timestamp(3) without time zone);


alter table "s_xls_temp" alter column "idt" type timestamp(3) without time zone using ("idt"::timestamp(3) without time zone);

alter table "s_dcm" alter column "idx" type smallint using ("idx"::smallint);


alter table "s_dcm" alter column "itype" type varchar(6) using ("itype"::varchar(6));


alter table "s_xls" alter column "itype" type varchar(6) using ("itype"::varchar(6));


alter table "s_xls" alter column "jc" type varchar(20) using ("jc"::varchar(20));


alter table "s_user" alter column "last_login" type timestamp(3) without time zone using ("last_login"::timestamp(3) without time zone);

alter table "s_dcm" alter column "lbl" type varchar(100) using ("lbl"::varchar(100));


alter table "s_dcm" alter column "lbl_en" type varchar(200) using ("lbl_en"::varchar(200));

alter table "s_dcm" alter column  "lbl_en" drop not null;


alter table "s_user" alter column "local" type integer using ("local"::integer);

alter table "s_user" alter column "login_number" type integer using ("login_number"::integer);

alter table "s_refcode" alter column "lowval" type text using ("lowval"::text);


--alter table "s_org" alter column "mail" type varchar(255) using ("mail"::varchar(255));


--alter table "s_ou" alter column "mail" type varchar(255) using ("mail"::varchar(255));


alter table "s_user" alter column "mail" type varchar(50) using ("mail"::varchar(50));


alter table "s_report_tmp" alter column "max0" type integer using ("max0"::integer);


alter table "s_report_tmp" alter column "max1" type integer using ("max1"::integer);


alter table "s_report_tmp" alter column "maxc" type integer using ("maxc"::integer);


alter table "s_report_tmp" alter column "maxu" type integer using ("maxu"::integer);


alter table "s_report_tmp" alter column "maxu0" type integer using ("maxu0"::integer);


alter table "s_refcode" alter column "meaning" type text using ("meaning"::text);


alter table "s_role" alter column "menu_detail" type varchar(150) using ("menu_detail"::varchar(150));

alter table "s_report_tmp" alter column "min0" type integer using ("min0"::integer);


alter table "s_report_tmp" alter column "min1" type integer using ("min1"::integer);


alter table "s_report_tmp" alter column "minc" type integer using ("minc"::integer);


alter table "s_report_tmp" alter column "minu" type integer using ("minu"::integer);

alter table "s_report_tmp" alter column  "minu" drop not null;


alter table "s_sys_logs" alter column "msg_id" type varchar(255) using ("msg_id"::varchar(255));


alter table "s_sys_logs" alter column "msg_status" type integer using ("msg_status"::integer);

alter table "s_ou" alter column "mst" type varchar(14) using ("mst"::varchar(14));


alter table "s_cat" alter column "name" type varchar(100) using ("name"::varchar(100));

alter table "s_dcd" alter column "name" type varchar(100) using ("name"::varchar(100));

alter table "s_gns" alter column "name" type varchar(255) using ("name"::varchar(255));

alter table "s_loc" alter column "name" type varchar(100) using ("name"::varchar(100));


--alter table "s_org" alter column "name" type varchar(500) using ("name"::varchar(500));


alter table "s_ou" alter column "name" type varchar(255) using ("name"::varchar(255));


alter table "s_role" alter column "name" type varchar(255) using ("name"::varchar(255));


alter table "s_taxo" alter column "name" type varchar(100) using ("name"::varchar(100));


alter table "s_user" alter column "name" type varchar(50) using ("name"::varchar(50));


--alter table "s_org" alter column "name_en" type varchar(500) using ("name_en"::varchar(500));


-- alter table "s_inv" alter column "note" type varchar(255) using ("note"::varchar(255));

-- alter table "s_inv" alter column  "note" set default null;


-- alter table "s_inv" alter column "ou" type integer using ("ou"::integer);


alter table "s_seou" alter column "ou" type integer using ("ou"::integer);

alter table "s_user" alter column "ou" type integer using ("ou"::integer);



alter table "s_ou" alter column "paxo" type varchar(5) using ("paxo"::varchar(5));


-- alter table "s_inv" alter column "paym" type varchar(10) using ("paym"::varchar(10));

-- alter table "s_inv" alter column  "paym" set default null;


-- alter table "s_inv" alter column "pid" type varchar(3000) using ("pid"::varchar(3000));


alter table "s_loc" alter column "pid" type varchar(7) using ("pid"::varchar(7));


alter table "s_ou" alter column "pid" type integer using ("pid"::integer);


alter table "s_role" alter column "pid" type integer using ("pid"::integer);

alter table "s_user" alter column "pos" type varchar(50) using ("pos"::varchar(50));


alter table "s_serial" alter column "priority" type integer using ("priority"::integer);

--alter table "s_org" alter column "prov" type varchar(3) using ("prov"::varchar(3));


--alter table "s_ou" alter column "prov" type varchar(3) using ("prov"::varchar(3));


alter table "s_org" alter column "pwd" type varchar(255) using ("pwd"::varchar(255));


alter table "s_ou" alter column "pwd" type varchar(255) using ("pwd"::varchar(255));


alter table "s_sys_logs" alter column "r1" type varchar(255) using ("r1"::varchar(255));

alter table "s_sys_logs" alter column "r2" type varchar(255) using ("r2"::varchar(255));

alter table "s_sys_logs" alter column "r3" type varchar(255) using ("r3"::varchar(255));

alter table "s_sys_logs" alter column "r4" type varchar(255) using ("r4"::varchar(255));

alter table "s_member" alter column "role_id" type integer using ("role_id"::integer);


-- alter table "s_inv" alter column "sacc" type varchar(255) using ("sacc"::varchar(255));

-- alter table "s_inv" alter column  "sacc" set default null;


-- alter table "s_inv" alter column "saddr" type varchar(255) using ("saddr"::varchar(255));

-- alter table "s_inv" alter column  "saddr" set default null;


-- alter table "s_inv" alter column "sbank" type varchar(255) using ("sbank"::varchar(255));

-- alter table "s_inv" alter column  "sbank" set default null;


alter table "s_seou" alter column "se" type integer using ("se"::integer);

alter table "s_seusr" alter column "se" type integer using ("se"::integer);


-- alter table "s_inv" alter column "sec" type varchar(10) using ("sec"::varchar(10));


-- alter table "s_inv" alter column "seq" type varchar(8) using ("seq"::varchar(8));

-- alter table "s_inv" alter column  "seq" set default null;


alter table "s_ou" alter column "seq" type smallint using ("seq"::smallint);


-- alter table "s_inv" alter column "serial" type varchar(6) using ("serial"::varchar(6));

-- alter table "s_inv" alter column  "serial" set default null;


alter table "s_report_tmp" alter column "serial" type varchar(50) using ("serial"::varchar(50));


alter table "s_serial" alter column "serial" type varchar(6) using ("serial"::varchar(6));

alter table "s_ou" alter column "sign" type smallint using ("sign"::smallint);


-- alter table "s_inv" alter column "smail" type varchar(255) using ("smail"::varchar(255));

-- alter table "s_inv" alter column  "smail" set default null;


-- alter table "s_inv" alter column "sname" type varchar(255) using ("sname"::varchar(255));

-- alter table "s_inv" alter column  "sname" set default null;


alter table "s_role" alter column "sort" type varchar(10) using ("sort"::varchar(10));

alter table "s_sys_logs" alter column "src" type varchar(20) using ("src"::varchar(20));

alter table "s_dcm" alter column "status" type smallint using ("status"::smallint);


alter table "s_f_fld" alter column "status" type smallint using ("status"::smallint);


-- alter table "s_inv" alter column "status" type smallint using ("status"::smallint);

-- alter table "s_inv" alter column  "status" set default null;


alter table "s_org" alter column "status" type varchar(2) using ("status"::varchar(2));


alter table "s_ou" alter column "status" type smallint using ("status"::smallint);


alter table "s_serial" alter column "status" type smallint using ("status"::smallint);

-- alter table "s_inv" alter column "status_received" type integer using ("status_received"::integer);

-- alter table "s_inv" alter column "status_tbss" type integer using ("status_tbss"::integer);

-- alter table "s_inv" alter column "stax" type varchar(14) using ("stax"::varchar(14));

-- alter table "s_inv" alter column  "stax" set default null;


-- alter table "s_inv" alter column "stel" type varchar(255) using ("stel"::varchar(255));

-- alter table "s_inv" alter column  "stel" set default null;


-- alter table "s_inv" alter column  "sum" set default null;


-- alter table "s_inv" alter column  "sumv" set default null;

alter table "s_manager" alter column "taxc" type varchar(14) using ("taxc"::varchar(14));


--alter table "s_org" alter column "taxc" type varchar(14) using ("taxc"::varchar(14));


alter table "s_ou" alter column "taxc" type varchar(14) using ("taxc"::varchar(14));


alter table "s_serial" alter column "taxc" type varchar(14) using ("taxc"::varchar(14));

alter table "s_ou" alter column "taxo" type varchar(5) using ("taxo"::varchar(5));


alter table "s_serial" alter column "td" type timestamp(3) without time zone using ("td"::timestamp(3) without time zone);

--alter table "s_org" alter column "tel" type varchar(255) using ("tel"::varchar(255));


--alter table "s_ou" alter column "tel" type varchar(255) using ("tel"::varchar(255));


alter table "s_ou" alter column "temp" type integer using ("temp"::integer);

alter table "s_ou" alter column  "temp" drop not null;

alter table "s_ou" alter column  "temp" set default null;


-- alter table "s_inv" alter column  "total" set default null;


-- alter table "s_inv" alter column  "totalv" set default null;

alter table "s_dcm" alter column "typ" type varchar(20) using ("typ"::varchar(20));


alter table "s_cat" alter column "type" type varchar(30) using ("type"::varchar(30));

alter table "s_dcd" alter column "type" type varchar(30) using ("type"::varchar(30));

-- alter table "s_inv" alter column "type" type varchar(10) using ("type"::varchar(10));

-- alter table "s_inv" alter column  "type" set default null;



alter table "s_org" alter column "type" type integer using ("type"::integer);


alter table "s_serial" alter column "type" type varchar(6) using ("type"::varchar(6));

-- alter table "s_inv" alter column "uc" type varchar(20) using ("uc"::varchar(20));


alter table "s_serial" alter column "uc" type varchar(20) using ("uc"::varchar(20));

alter table "s_user" alter column "uc" type smallint using ("uc"::smallint);


alter table "s_gns" alter column "unit" type varchar(30) using ("unit"::varchar(30));

alter table "s_user" alter column "update_date" type timestamp(3) without time zone using ("update_date"::timestamp(3) without time zone);

alter table "s_manager" alter column "user_id" type varchar(20) using ("user_id"::varchar(20));


alter table "s_member" alter column "user_id" type varchar(20) using ("user_id"::varchar(20));


alter table "s_sys_logs" alter column "user_id" type varchar(20) using ("user_id"::varchar(20));

alter table "s_sys_logs" alter column "user_name" type varchar(255) using ("user_name"::varchar(255));

alter table "s_serial" alter column "uses" type smallint using ("uses"::smallint);

alter table "s_ou" alter column "usr" type varchar(255) using ("usr"::varchar(255));


alter table "s_seusr" alter column "usrid" type varchar(20) using ("usrid"::varchar(20));



-- alter table "s_inv" alter column  "vat" set default null;


-- alter table "s_inv" alter column  "vatv" set default null;

--alter table "s_org" alter column "ward" type varchar(7) using ("ward"::varchar(7));


--alter table "s_ou" alter column "ward" type varchar(7) using ("ward"::varchar(7));


alter table "s_dcm" alter column "xc" type varchar(2) using ("xc"::varchar(2));


alter table "s_xls" alter column "xc" type varchar(2) using ("xc"::varchar(2));


-- alter table "s_inv" alter column "xml" type text using ("xml"::text);


-- alter table "s_inv" alter column "xml_received" type text using ("xml_received"::text);

----------------------- migrate core -> frt  ------------------------------------------------


ALTER TABLE "public"."s_org" ALTER COLUMN  "status_received" SET DEFAULT 0;
ALTER TABLE "public"."s_org" ALTER COLUMN "id" TYPE INTEGER USING ("id"::INTEGER);

ALTER TABLE "public"."s_org" ALTER COLUMN  "id" SET DEFAULT nextval('s_org_id_seq'::regclass);

ALTER TABLE "public"."s_org" ALTER COLUMN "type" TYPE SMALLINT USING ("type"::SMALLINT);

ALTER TABLE public.s_group_role ALTER COLUMN role_id TYPE int4 USING role_id::int4;

ALTER TABLE public.s_group_user ALTER COLUMN group_id TYPE int4 USING group_id::int4;
delete from s_group_role ;
insert into s_group_role 
select 1, id from s_role;

alter table s_inv add seq varchar(8) generated always as (doc ->> 'seq'::text) STORED;