﻿--Table: s_inv

--DROP TABLE IF EXISTS s_inv;

CREATE TABLE s_inv (
  id        int(10) UNSIGNED AUTO_INCREMENT NOT NULL,
  sec       varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  ic        varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  pid       varchar(255),
  cid       varchar(255),
  cde       varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  idt       datetime NOT NULL,
  doc       json,
  xml       longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  dt        datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  ou        int(10) UNSIGNED NOT NULL,
  uc        varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  cvt       tinyint DEFAULT 0,
  `status`  tinyint AS (if((json_unquote(json_extract(`doc`,'$.status')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.status')))) VIRTUAL,
  `type`    varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.type')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.type')))) VIRTUAL,
  form      varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.form')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.form')))) VIRTUAL,
  `serial`  varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.serial')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.serial')))) VIRTUAL,
  seq       varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.seq')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.seq')))) VIRTUAL,
  paym      varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.paym')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.paym')))) VIRTUAL,
  curr      varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.curr')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.curr')))) VIRTUAL,
  exrt      decimal(8,2) AS (if((json_unquote(json_extract(`doc`,'$.exrt')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.exrt')))) VIRTUAL,
  sname     varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.sname')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.sname')))) VIRTUAL,
  stax      varchar(14) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.stax')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.stax')))) VIRTUAL,
  saddr     varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.saddr')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.saddr')))) VIRTUAL,
  smail     varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.smail')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.smail')))) VIRTUAL,
  stel      varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.stel')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.stel')))) VIRTUAL,
  sacc      varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.sacc')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.sacc')))) VIRTUAL,
  sbank     varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.sbank')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.sbank')))) VIRTUAL,
  buyer     varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.buyer')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.buyer')))) VIRTUAL,
  bcode     varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.bcode')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.bcode')))) VIRTUAL,
  bname     varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.bname')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.bname')))) VIRTUAL,
  btax      varchar(14) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.btax')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.btax')))) VIRTUAL,
  baddr     varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.baddr')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.baddr')))) VIRTUAL,
  bmail     varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.bmail')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.bmail')))) VIRTUAL,
  btel      varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.btel')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.btel')))) VIRTUAL,
  bacc      varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.bacc')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.bacc')))) VIRTUAL,
  bbank     varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.bbank')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.bbank')))) VIRTUAL,
  note      varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.note')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.note')))) VIRTUAL,
  `sum`     decimal(17,2) AS (if((json_unquote(json_extract(`doc`,'$.sum')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.sum')))) VIRTUAL,
  sumv      decimal(17,2) AS (if((json_unquote(json_extract(`doc`,'$.sumv')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.sumv')))) VIRTUAL,
  vat       decimal(17,2) AS (if((json_unquote(json_extract(`doc`,'$.vat')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.vat')))) VIRTUAL,
  vatv      decimal(17,2) AS (if((json_unquote(json_extract(`doc`,'$.vatv')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.vatv')))) VIRTUAL,
  total     decimal(17,2) AS (if((json_unquote(json_extract(`doc`,'$.total')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.total')))) VIRTUAL,
  totalv    decimal(17,2) AS (if((json_unquote(json_extract(`doc`,'$.totalv')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.totalv')))) VIRTUAL,
  adjseq    varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.adj.seq')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.adj.seq')))) VIRTUAL,
  adjdes    varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.adj.des')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.adj.des')))) VIRTUAL,
  adjtyp    tinyint AS (if((json_unquote(json_extract(`doc`,'$.adj.typ')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.adj.typ')))) VIRTUAL,
  c0        varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.c0')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.c0')))) VIRTUAL,
  c1        varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.c1')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.c1')))) VIRTUAL,
  c2        varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.c2')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.c2')))) VIRTUAL,
  c3        varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.c3')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.c3')))) VIRTUAL,
  c4        varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.c4')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.c4')))) VIRTUAL,
  c5        varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.c5')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.c5')))) VIRTUAL,
  c6        varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.c6')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.c6')))) VIRTUAL,
  c7        varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.c7')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.c7')))) VIRTUAL,
  c8        varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.c8')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.c8')))) VIRTUAL,
  c9        varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci AS (if((json_unquote(json_extract(`doc`,'$.c9')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.c9')))) VIRTUAL,
  cdt       datetime AS (json_unquote(json_extract(`doc`,'$.cdt'))) VIRTUAL,
  /* Keys */
  PRIMARY KEY (id)
) ENGINE = InnoDB
  CHARACTER SET utf8 COLLATE utf8_bin;

CREATE UNIQUE INDEX ic
  ON s_inv
  (ic);

CREATE INDEX pid
  ON s_inv
  (pid);

CREATE UNIQUE INDEX sec
  ON s_inv
  (sec);

CREATE INDEX s_inv_btax_idx
  ON s_inv
  (`status`, `type`, btax);

CREATE INDEX s_inv_cdt_IDX
  ON s_inv
  (cdt, stax, ou, `type`, form, `serial`, `status`);

CREATE INDEX s_inv_idt_idx
  ON s_inv
  (idt, stax, ou, `type`, form, `serial`, `status`);

CREATE UNIQUE INDEX s_inv_uid_01
  ON s_inv
  (stax, form, `serial`, seq);