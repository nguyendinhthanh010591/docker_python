Select * from s_role sr

/*insert user*/
INSERT INTO s_user (id, code, mail, ou, uc, name, pos, pass) VALUES('__admin', NULL, 'administrator@fpt.com.vn', 45, 1, 'QTHT', 'QTHT', 'YWRtaW5AMTIz');

/*insert group*/
INSERT INTO s_group (ID, NAME, OU, STATUS, APPROVE, DES) VALUES(1, 'ADMIN', 1, '1', '1', 'ADMIN');

/*insert role*/
INSERT INTO s_role (id, name, code) VALUES(2, 'Cấu hình hệ thống', 'PERM_SYS_CONFIG');
INSERT INTO s_role (id, name, code) VALUES(3, 'Quản lý người dùng', 'PERM_SYS_USER');
INSERT INTO s_role (id, name, code) VALUES(4, 'Quản lý nhóm', 'PERM_SYS_GROUP');
INSERT INTO s_role (id, name, code) VALUES(5, 'Phân quyền', 'PERM_SYS_ROLE');
INSERT INTO s_role (id, name, code) VALUES(6, 'Tạo mẫu hóa đơn', 'PERM_SYS_TEMPLATE');
INSERT INTO s_role (id, name, code) VALUES(7, 'Tạo mẫu Email', 'PERM_SYS_EMAIL');
INSERT INTO s_role (id, name, code) VALUES(8, 'Cấu hình hóa đơn', 'PERM_SYS_INV_CONFIG');
INSERT INTO s_role (id, name, code) VALUES(10, 'Quản trị danh mục', 'PERM_CATEGORY_MANAGE');
INSERT INTO s_role (id, name, code) VALUES(11, 'Khách hàng', 'PERM_CATEGORY_CUSTOMER');
INSERT INTO s_role (id, name, code) VALUES(12, 'Hàng hóa, dịch vụ', 'PERM_CATEGORY_ITEMS');
INSERT INTO s_role (id, name, code) VALUES(13, 'Tỷ giá', 'PERM_CATEGORY_RATE');
INSERT INTO s_role (id, name, code) VALUES(14, 'Danh mục khác', 'PERM_CATEGORY_ANOTHER');
INSERT INTO s_role (id, name, code) VALUES(15, 'Thông báo phát hành', 'PERM_TEMPLATE_REGISTER');
INSERT INTO s_role (id, name, code) VALUES(17, 'Lập hóa đơn', 'PERM_CREATE_INV_MANAGE');
INSERT INTO s_role (id, name, code) VALUES(18, 'Lập hóa đơn từ Excel', 'PERM_CREATE_INV_EXCEL');
INSERT INTO s_role (id, name, code) VALUES(20, 'Cấp số', 'PERM_CREATE_INV_SEQ');
INSERT INTO s_role (id, name, code) VALUES(21, 'Duyệt và xuất hóa đơn', 'PERM_APPROVE_INV_MANAGE');
INSERT INTO s_role (id, name, code) VALUES(22, 'Báo cáo', 'PERM_REPORT_INV');
INSERT INTO s_role (id, name, code) VALUES(23, 'Tra cứu hóa đơn', 'PERM_SEARCH_INV');
INSERT INTO s_role (id, name, code) VALUES(24, 'Điều chỉnh hóa đơn', 'PERM_INVOICE_ADJUST');
INSERT INTO s_role (id, name, code) VALUES(25, 'Thay thế hóa đơn', 'PERM_INVOICE_REPLACE');
INSERT INTO s_role (id, name, code) VALUES(26, 'Hủy hóa đơn', 'PERM_INVOICE_CANCEL');
INSERT INTO s_role (id, name, code) VALUES(27, 'Duyệt thông báo phát hành', 'PERM_TEMPLATE_APPROVE');
INSERT INTO s_role (id, name, code) VALUES(28, 'Hủy thông báo phát hành', 'PERM_TEMPLATE_VOID');
INSERT INTO s_role (id, name, code) VALUES(29, 'Tích hợp', 'PERM_INTEGRATED');
INSERT INTO s_role (id, name, code) VALUES(30, 'Chờ hủy', 'PERM_VOID_WAIT');
INSERT INTO s_role (id, name, code) VALUES(31, 'Log hệ thống', 'PERM_LOG_SYS');

/*insert group role*/
INSERT INTO s_group_role (GROUP_ID, ROLE_ID) VALUES(1, 2);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID) VALUES(1, 3);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID) VALUES(1, 4);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID) VALUES(1, 5);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID) VALUES(1, 6);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID) VALUES(1, 7);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID) VALUES(1, 8);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID) VALUES(1, 10);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID) VALUES(1, 11);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID) VALUES(1, 12);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID) VALUES(1, 13);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID) VALUES(1, 14);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID) VALUES(1, 15);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID) VALUES(1, 17);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID) VALUES(1, 18);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID) VALUES(1, 20);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID) VALUES(1, 21);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID) VALUES(1, 22);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID) VALUES(1, 23);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID) VALUES(1, 24);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID) VALUES(1, 25);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID) VALUES(1, 26);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID) VALUES(1, 27);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID) VALUES(1, 28);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID) VALUES(1, 29);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID) VALUES(1, 30);
INSERT INTO s_group_role (GROUP_ID, ROLE_ID) VALUES(1, 31);

/*insert group user*/
INSERT INTO s_group_user (USER_ID, GROUP_ID) VALUES('__admin', 1);





