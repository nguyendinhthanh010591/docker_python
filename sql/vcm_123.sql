CREATE TABLE [s_xls_temp] ([id] [varchar](20), [ic] [varchar](50), [idt] [datetime], [form] [varchar](11), [serial] [varchar](10), [seq] [varchar](8), [dt] [datetime] CONSTRAINT [DF__s_xls_temp__dt__5AB9788F] DEFAULT GETDATE() NOT NULL);

CREATE TABLE [s_list_inv_cancel] ([id] [bigint] IDENTITY (1, 1) NOT NULL, [inv_id] [int] NOT NULL, [doc] [nvarchar](MAX) NOT NULL, [invtype] [int] NOT NULL, [ou] [int] NOT NULL, [serial] [nvarchar](6), [type] [nvarchar](10), [idt] [datetime], [cdt] [datetime] NOT NULL, [listinv_id] [bigint], [stax] [nvarchar](14), CONSTRAINT [PK__s_list_i__3213E83F0E4528B3] PRIMARY KEY ([id]));

CREATE TABLE [s_listvalues] ([keyname] [varchar](60) NOT NULL, [keyvalue] [nvarchar](MAX) NOT NULL, [keytype] [varchar](30) NOT NULL);

CREATE TABLE [info_taxpayer] ([id] [numeric](18, 0) IDENTITY (0, 1) NOT NULL, [taxc] [nvarchar](14) NOT NULL, [namecom] [nvarchar](255), [type] [nvarchar](10), [status] [numeric](18, 0), [effectdate] [datetime], [managercomc] [nvarchar](10), [chapter] [nvarchar](10), [changedate] [datetime], [economictype] [nvarchar](10), [contbusires] [nvarchar](10), [passport] [nvarchar](20), [createdby] [nvarchar](150) NOT NULL, [statusscan] [numeric](18, 0) NOT NULL, [messres] [nvarchar](255) NOT NULL, [statuscoderes] [numeric](18, 0) NOT NULL, [createddate] [datetime], [updateddate] [datetime]);

CREATE TABLE [type_invoice] ([id] [numeric](18, 0) IDENTITY (0, 1) NOT NULL, [name] [nvarchar](255));

CREATE TABLE [van_einv_listid_msg] ([id] [bigint] IDENTITY (1, 1) NOT NULL, [pid] [bigint], [type] [nvarchar](10), [status_scan] [int], [createdate] [datetime] CONSTRAINT [DF_van_einv_listid_msg_createdate] DEFAULT GETDATE(), [sendvandate] [datetime], [msg] [nvarchar](MAX), [xml] [nvarchar](MAX));

CREATE TABLE [s_inv_sum] ([idt] [date] NOT NULL, [stax] [varchar](20) NOT NULL, [type] [varchar](6) NOT NULL, [form] [varchar](11) NOT NULL, [serial] [varchar](8), [i0] [numeric](18, 0), [i1] [numeric](18, 0), [i2] [numeric](18, 0), [i3] [numeric](18, 0), [i4] [numeric](18, 0));

CREATE TABLE [b_inv] ([id] [int] IDENTITY (1, 1) NOT NULL, [doc] [nvarchar](MAX) NOT NULL, [xml] [nvarchar](MAX), [idt] [datetime], [type] [varchar](10), [form] [varchar](11), [serial] [varchar](8), [seq] [varchar](7), [paym] [varchar](100), [curr] [varchar](3), [exrt] [numeric](8, 2), [sname] [varchar](255), [stax] [varchar](14), [saddr] [varchar](255), [smail] [varchar](255), [stel] [varchar](255), [sacc] [varchar](255), [sbank] [varchar](255), [buyer] [varchar](255), [bname] [varchar](255), [bcode] [varchar](100), [btax] [varchar](14), [baddr] [varchar](255), [bmail] [varchar](255), [btel] [varchar](255), [bacc] [varchar](255), [bbank] [varchar](255), [note] [varchar](255), [sumv] [numeric](19, 4), [vatv] [numeric](19, 4), [totalv] [numeric](19, 4), [ou] [int], [uc] [varchar](20), [paid] [int], [month] [varchar](100), [year] [varchar](100), [category] [varchar](100), CONSTRAINT [PK__b_inv__3213E83FFD9C2485] PRIMARY KEY ([id]));

CREATE TABLE [s_statement] ([id] [bigint] IDENTITY (1, 1) NOT NULL, [type] [nvarchar](15), [formname] [nvarchar](100), [regtype] [int], [sname] [nvarchar](400), [stax] [nvarchar](14), [taxoname] [nvarchar](100), [taxo] [nvarchar](5), [contactname] [nvarchar](50), [contactaddr] [nvarchar](400), [contactemail] [nvarchar](50), [contactphone] [nvarchar](20), [place] [nvarchar](50), [createdt] [datetime], [hascode] [int] CONSTRAINT [DF__s_stateme__hasco__3A4CA8FD] DEFAULT 0, [sendtype] [int], [dtsendtype] [nvarchar](20), [invtype] [nvarchar](100), [doc] [nvarchar](MAX), [status] [int] CONSTRAINT [DF__s_stateme__statu__3B40CD36] DEFAULT 1 NOT NULL, [cqtstatus] [int], [vdt] [datetime], [cqtmess] [nvarchar](255), [xml] [nvarchar](MAX), [statustvan] [int] CONSTRAINT [DF__s_stateme__statu__3C34F16F] DEFAULT 0 NOT NULL, [xml_received] [nvarchar](MAX), [xml_accepted] [nvarchar](MAX), [paxoname] [nvarchar](100), [paxo] [nvarchar](5), [json_received] [nvarchar](MAX), [json_accepted] [nvarchar](MAX), [uc] [nvarchar](20), CONSTRAINT [s_statement_pk] PRIMARY KEY ([id]));

CREATE TABLE [temp_taxpayer] ([id] [numeric](18, 0) IDENTITY (0, 1) NOT NULL, [taxc] [nvarchar](14) NOT NULL, [createdby] [nvarchar](150) NOT NULL, [statusscan] [numeric](18, 0) NOT NULL, [creaateddate] [datetime], [updateddate] [datetime]);

CREATE TABLE [van_msg_out] ([id] [numeric](18, 0) IDENTITY (0, 1) NOT NULL, [invid] [nvarchar](36) NOT NULL, [xml2base64] [nvarchar](MAX) NOT NULL, [createdby] [nvarchar](150) NOT NULL, [statustvan] [numeric](18, 0), [createddate] [datetime], [senddate] [datetime] CONSTRAINT [DF__Van_msg_o__sendd__756D6ECB] DEFAULT GETDATE(), [idtypeinv] [numeric](18, 0) NOT NULL, [messsendcode] [nvarchar](10));

CREATE TABLE [s_trans] ([ID] [numeric](20, 0) IDENTITY (1, 1) NOT NULL, [refNo] [nvarchar](50), [valueDate] [date], [customerID] [nvarchar](50), [customerName] [nvarchar](500), [taxCode] [nvarchar](50), [customerAddr] [nvarchar](500), [isSpecial] [varchar](1), [curr] [varchar](10), [exrt] [float](53), [vrt] [nvarchar](50), [chargeAmount] [nvarchar](50), [vcontent] [nvarchar](500), [amount] [decimal](18, 2), [vat] [decimal](18, 2), [total] [decimal](18, 2), [create_date] [datetime] CONSTRAINT [DF_s_trans_sysdate] DEFAULT GETDATE(), [tranID] [nvarchar](100), [trantype] [numeric](1, 0), [trancol] [numeric](1, 0), [status] [numeric](1, 0), [customerAcc] [nvarchar](50), [inv_id] [numeric](18, 0), [inv_date] [date], [ma_nv] [nvarchar](50), [ma_ks] [nvarchar](50), [last_update] [datetime], [segment] [nvarchar](50), CONSTRAINT [PK_s_trans] PRIMARY KEY ([ID]));



CREATE TABLE [van_history] ([id] [numeric](36, 0) IDENTITY (1, 1) NOT NULL, [type_invoice] [numeric](18, 0) NOT NULL, [id_ref] [nvarchar](MAX), [status] [numeric](18, 0), [datetime_trans] [datetime], [description] [nvarchar](MAX));

DECLARE @TableName SYSNAME set @TableName = N'van_history'; DECLARE @FullTableName SYSNAME set @FullTableName = N'dbo.van_history'; DECLARE @ColumnName SYSNAME set @ColumnName = N'type_invoice'; DECLARE @MS_DescriptionValue NVARCHAR(3749); SET @MS_DescriptionValue = N'1: Báº£ng tá»•ng há»£p hÃ³a Ä‘Æ¡n, 2: HÃ³a Ä‘Æ¡n khÃ´ng mÃ£, 3: HÃ³a Ä‘Æ¡n cÃ³ mÃ£, 4: ThÃ´ng bÃ¡o sai xÃ³t, 5: Tá»� khai Ä‘Äƒng kÃ½';DECLARE @MS_Description NVARCHAR(3749) set @MS_Description = NULL; SET @MS_Description = (SELECT CAST(Value AS NVARCHAR(3749)) AS [MS_Description] FROM sys.extended_properties AS ep WHERE ep.major_id = OBJECT_ID(@FullTableName) AND ep.minor_id=COLUMNPROPERTY(ep.major_id, @ColumnName, 'ColumnId') AND ep.name = N'MS_Description'); IF @MS_Description IS NULL BEGIN EXEC sys.sp_addextendedproperty @name  = N'MS_Description', @value = @MS_DescriptionValue, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = @TableName, @level2type = N'COLUMN', @level2name = @ColumnName; END ELSE BEGIN EXEC sys.sp_updateextendedproperty @name  = N'MS_Description', @value = @MS_DescriptionValue, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = @TableName, @level2type = N'COLUMN', @level2name = @ColumnName; END;

CREATE TABLE [van_msg_out_dtl] ([id] [numeric](18, 0) IDENTITY (0, 1) NOT NULL, [iddatatvan] [numeric](18, 0) NOT NULL, [invid] [nvarchar](36) NOT NULL, [createdby] [nvarchar](150) NOT NULL, [statustvan] [numeric](18, 0), [statusres] [numeric](18, 0), [messsendcode] [nvarchar](10), [messsend] [nvarchar](MAX), [messres] [nvarchar](255), [messrescode] [nvarchar](10), [senddate] [datetime], [resdate] [datetime], [transid] [nvarchar](225), [idtypeinv] [numeric](18, 0) NOT NULL);

CREATE TABLE [van_msg_in_dtl] ([id] [numeric](18, 0) IDENTITY (0, 1) NOT NULL, [receivejson] [nvarchar](MAX) NOT NULL, [taxc] [nvarchar](MAX), [namenotice] [nvarchar](500), [typenotice] [nvarchar](255), [contentnotice] [nvarchar](MAX), [createddtnotice] [datetime], [createddate] [datetime] CONSTRAINT [DF__van_msg_i__creat__0C50D423] DEFAULT GETDATE(), [status_scan] [numeric](18, 0), [updateddate] [datetime], [transid] [nvarchar](225));

CREATE TABLE [sign_xml] ([id] [numeric](18, 0) IDENTITY (0, 1) NOT NULL, [tax_code] [nvarchar](14), [type_xml] [nvarchar](20), [xmlOriginal] [nvarchar](MAX) NOT NULL, [xmlSigned] [nvarchar](MAX), [createddate] [datetime], [updateddate] [datetime]);

CREATE TABLE [s_invdoctemp] ([id] [bigint] IDENTITY (1, 1) NOT NULL, [invid] [bigint] NOT NULL, [doc] [nvarchar](MAX) NOT NULL, [dt] [datetime] CONSTRAINT [DF_s_invdoctemp_dt] DEFAULT GETDATE() NOT NULL, [status] [int] NOT NULL, [xml] [nvarchar](MAX), [iserror] [int] CONSTRAINT [DF_s_invdoctemp_iserror] DEFAULT 0 NOT NULL, [error_desc] [nvarchar](MAX), CONSTRAINT [PRIMARY] PRIMARY KEY ([id]));

CREATE TABLE [s_wrongnotice_process] ([id] [int] IDENTITY (1, 1) NOT NULL, [ou] [int], [id_wn] [int], [id_inv] [nvarchar](MAX), [doc] [nvarchar](MAX), [xml] [nvarchar](MAX), [dt] [datetime] CONSTRAINT [DF_s_wrongnotice_process_dt] DEFAULT GETDATE() NOT NULL, [status] [nvarchar](15) NOT NULL, [status_cqt] [int], [xml_received] [nvarchar](MAX), [json_received] [nvarchar](MAX), [xml_accepted] [nvarchar](MAX), [json_accepted] [nvarchar](MAX), [dien_giai] [nvarchar](MAX), [taxc] [nvarchar](14), [form] [nvarchar](14), [serial] [nvarchar](14), [seq] [nvarchar](14), [type] [nvarchar](14), CONSTRAINT [s_wrongnotice_process_pk] PRIMARY KEY ([id]));

CREATE TABLE [s_user_pass] ([user_id] [nvarchar](20) NOT NULL, [pass] [varchar](200) NOT NULL, [change_date] [datetime] NOT NULL, CONSTRAINT [PK__s_user_p__518C386C17C39B87] PRIMARY KEY ([user_id], [pass]));

CREATE TABLE [van_msg_in] ([id] [numeric](18, 0) IDENTITY (0, 1) NOT NULL, [receive_arr_json] [nvarchar](MAX) NOT NULL, [status_scan] [numeric](18, 0), [createddate] [datetime], [updateddate] [datetime]);

DECLARE @TableName SYSNAME set @TableName = N'van_msg_in'; DECLARE @FullTableName SYSNAME set @FullTableName = N'dbo.van_msg_in'; DECLARE @ColumnName SYSNAME set @ColumnName = N'status_scan'; DECLARE @MS_DescriptionValue NVARCHAR(3749); SET @MS_DescriptionValue = N'0: not scan, pending: 3, success: 1, error: 2';DECLARE @MS_Description NVARCHAR(3749) set @MS_Description = NULL; SET @MS_Description = (SELECT CAST(Value AS NVARCHAR(3749)) AS [MS_Description] FROM sys.extended_properties AS ep WHERE ep.major_id = OBJECT_ID(@FullTableName) AND ep.minor_id=COLUMNPROPERTY(ep.major_id, @ColumnName, 'ColumnId') AND ep.name = N'MS_Description'); IF @MS_Description IS NULL BEGIN EXEC sys.sp_addextendedproperty @name  = N'MS_Description', @value = @MS_DescriptionValue, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = @TableName, @level2type = N'COLUMN', @level2name = @ColumnName; END ELSE BEGIN EXEC sys.sp_updateextendedproperty @name  = N'MS_Description', @value = @MS_DescriptionValue, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = @TableName, @level2type = N'COLUMN', @level2name = @ColumnName; END;

CREATE TABLE [s_list_inv] ([id] [bigint] NOT NULL, [listinv_code] [nvarchar](15), [s_tax] [nvarchar](14) NOT NULL, [ou] [int] NOT NULL, [created_date] [datetime] CONSTRAINT [DF_s_list_inv_created_date] DEFAULT GETDATE() NOT NULL, [created_by] [nvarchar](50), [approve_date] [datetime], [approve_by] [nvarchar](20), [s_name] [nvarchar](400) NOT NULL, [item_type] [nvarchar](20) NOT NULL, [listinvoice_num] [nvarchar](5), [period_type] [nvarchar](1) NOT NULL, [period] [nvarchar](10) NOT NULL, [status] [int] CONSTRAINT [DF_s_list_inv_status] DEFAULT 0 NOT NULL, [doc] [nvarchar](MAX), [xml] [nvarchar](MAX), [times] [int] CONSTRAINT [DF__s_list_in__times__3BFFE745] DEFAULT 0 NOT NULL, [status_received] [int], [xml_received] [nvarchar](MAX), [json_received] [nvarchar](MAX), [edittimes] [int], CONSTRAINT [s_list_inv_pk] PRIMARY KEY ([id]));

CREATE NONCLUSTERED INDEX [s_list_inv_cancel_cdt_ou] ON [s_list_inv_cancel]([cdt], [ou]);

ALTER TABLE [s_inv] ADD CONSTRAINT [FK__s_inv__uc__47A6A41B] FOREIGN KEY ([uc]) REFERENCES [s_user] ([id]) ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE UNIQUE CLUSTERED INDEX [s_listvalues_uk] ON [s_listvalues]([keyname]);

ALTER TABLE [b_inv] ADD CONSTRAINT [UQ__b_inv__B09EB31C78CA9B41] UNIQUE ([stax], [form], [serial], [seq]);

ALTER TABLE [s_role] ADD CONSTRAINT [s_role_fk01] FOREIGN KEY ([pid]) REFERENCES [s_role] ([id]) ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE NONCLUSTERED INDEX [idx_invID] ON [s_trans]([inv_id]);

CREATE UNIQUE NONCLUSTERED INDEX [idx_tranID] ON [s_trans]([tranID]);

CREATE UNIQUE NONCLUSTERED INDEX [S_ORG_UID_02] ON [s_org]([code]);

ALTER TABLE [s_wrongnotice_process] ADD CONSTRAINT [UQ__s_wrongn__01487863BB3969F7] UNIQUE ([id_wn]);

ALTER TABLE [s_user_pass] ADD CONSTRAINT [FK__s_user_pa__user___49CEE3AF] FOREIGN KEY ([user_id]) REFERENCES [s_user] ([id]) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE [s_inv] ADD CONSTRAINT [FK__s_inv__ou__46B27FE2] FOREIGN KEY ([ou]) REFERENCES [s_ou] ([id]) ON UPDATE NO ACTION ON DELETE NO ACTION;

CREATE SEQUENCE [ISEQ_VENDOR] START WITH 1 INCREMENT BY 1 MINVALUE -9223372036854775808 MAXVALUE 9223372036854775807;

ALTER TABLE [s_role] ADD [pid] [int];

ALTER TABLE [s_role] ADD [sort] [nvarchar](10);

ALTER TABLE [s_role] ADD [active] [tinyint];

ALTER TABLE [s_role] ADD [menu_detail] [nvarchar](150);

ALTER TABLE [s_user] ADD [local] [int] CONSTRAINT DF__s_user__local__45FE52CB DEFAULT 0 NOT NULL;

ALTER TABLE [s_user] ADD [change_pass_date] [datetime] CONSTRAINT DF__s_user__change_p__42E1EEFE DEFAULT GETDATE();

ALTER TABLE [s_user] ADD [login_number] [int] CONSTRAINT DF__s_user__login_nu__46F27704 DEFAULT 0 NOT NULL;

ALTER TABLE [s_user] ADD [change_pass] [int] CONSTRAINT DF__s_user__change_p__44CA3770 DEFAULT 0 NOT NULL;

ALTER TABLE [s_user] ADD [update_date] [datetime];

ALTER TABLE [s_serial] ADD [ut] [datetime] CONSTRAINT DF_s_serial_ut DEFAULT GETDATE() NOT NULL;

ALTER TABLE [s_serial] ADD [intg] [int] CONSTRAINT DF_s_serial_intg DEFAULT 2 NOT NULL;

ALTER TABLE [s_sys_logs] ADD [ip] [varchar](50);

ALTER TABLE [s_serial] ADD [alertleft] [int] CONSTRAINT DF_s_serial_alertleft DEFAULT 0 NOT NULL;

ALTER TABLE [s_serial] ADD [leftwarn] [int] CONSTRAINT DF_s_serial_leftwarn DEFAULT 1;

ALTER TABLE [s_serial] ADD [sendtype] [int];

ALTER TABLE [s_serial] ADD [invtype] [int];

ALTER TABLE [s_serial] ADD [degree_config] [int] CONSTRAINT DF_s_serial_degree_config DEFAULT 119;

ALTER TABLE [s_ou] ADD [smtpconf] [nvarchar](MAX);

ALTER TABLE [s_ou] ADD [sct] [tinyint] CONSTRAINT DF_s_ou_sct DEFAULT 0 NOT NULL;

ALTER TABLE [s_ou] ADD [sc] [tinyint] CONSTRAINT DF_s_ou_sc DEFAULT 0 NOT NULL;

ALTER TABLE [s_ou] ADD [bcc] [varchar](100);

ALTER TABLE [s_ou] ADD [qrc] [tinyint] CONSTRAINT DF_s_ou_qrc DEFAULT 0 NOT NULL;

ALTER TABLE [s_ou] ADD [na] [tinyint] CONSTRAINT DF_s_ou_na DEFAULT 2 NOT NULL;

ALTER TABLE [s_ou] ADD [nq] [tinyint] CONSTRAINT DF_s_ou_nq DEFAULT 2 NOT NULL;

ALTER TABLE [s_ou] ADD [np] [tinyint] CONSTRAINT DF_s_ou_np DEFAULT 5 NOT NULL;

ALTER TABLE [s_ou] ADD [itype] [varchar](255) CONSTRAINT DF_s_ou_itype DEFAULT '01GTKT' NOT NULL;

ALTER TABLE [s_ou] ADD [ts] [tinyint] CONSTRAINT DF_s_ou_ts DEFAULT 1 NOT NULL;

ALTER TABLE [s_ou] ADD [dsignfrom] [datetime];

ALTER TABLE [s_ou] ADD [dsignto] [datetime];

ALTER TABLE [s_ou] ADD [degree_config] [tinyint] CONSTRAINT DF_s_ou_degree_config DEFAULT 119 NOT NULL;

ALTER TABLE [s_ou] ADD [cfg_ihasdocid] [varchar](45);

ALTER TABLE [s_ou] ADD [cfg_uselist] [varchar](45) CONSTRAINT DF_s_ou_cfg_uselist DEFAULT '' NOT NULL;

ALTER TABLE [s_ou] ADD [hascode] [tinyint] CONSTRAINT DF__s_ou__hascode__31B762FC DEFAULT 0 NOT NULL;

ALTER TABLE [s_ou] ADD [dsignserial] [nvarchar](100);

ALTER TABLE [s_ou] ADD [dsignsubject] [nvarchar](200);

ALTER TABLE [s_ou] ADD [dsignissuer] [nvarchar](200);

ALTER TABLE [s_ou] ADD [receivermail] [nvarchar](200);

ALTER TABLE [s_ou] ADD [adsconf] [nvarchar](MAX);

ALTER TABLE [s_inv] ADD [dds_sent] [int] CONSTRAINT DF_s_inv_dds_sent DEFAULT 0;

ALTER TABLE [s_inv] ADD [dds_stt] [int];

ALTER TABLE [s_inv] ADD [dds] [int];

ALTER TABLE [s_inv] ADD [file_name] [nvarchar](100);

ALTER TABLE [s_inv] ADD [sendmaildate] [datetime];

ALTER TABLE [s_inv] ADD [sendsmsdate] [datetime];

ALTER TABLE [s_inv] ADD [vseq] [nvarchar](18);

ALTER TABLE [s_inv] ADD [sendmailstatus] [int];

ALTER TABLE [s_inv] ADD [sendsmsstatus] [int];

ALTER TABLE [s_inv] ADD [sendmailnum] [int];

ALTER TABLE [s_inv] ADD [sendsmsnum] [int];

ALTER TABLE [s_inv] ADD [bcode] [nvarchar](500);

ALTER TABLE [s_inv] ADD [taxocode] [nvarchar](20);

ALTER TABLE [s_inv] ADD [invlistdt] [datetime];

ALTER TABLE [s_inv] ADD [discountamt] [numeric](18, 0);

ALTER TABLE [s_inv] ADD [ordno] [nvarchar](500);

ALTER TABLE [s_inv] ADD [whsfr] [nvarchar](500);

ALTER TABLE [s_inv] ADD [whsto] [nvarchar](500);

ALTER TABLE [s_inv] ADD [edt] [datetime];

ALTER TABLE [s_inv] ADD [status_received] [int];

ALTER TABLE [s_inv] ADD [xml_received] [nvarchar](MAX);

ALTER TABLE [s_inv] ADD [hascode] [numeric](38, 0);

ALTER TABLE [s_inv] ADD [invtype] [nvarchar](500);

ALTER TABLE [s_inv] ADD [invlistnum] [nvarchar](50);

ALTER TABLE [s_inv] ADD [period_type] [nvarchar](1);

ALTER TABLE [s_inv] ADD [period] [date];

ALTER TABLE [s_inv] ADD [list_invid] [bigint];

ALTER TABLE [s_inv] ADD [status_tbss] [int];

ALTER TABLE [s_inv] ADD [orddt] [datetime];

ALTER TABLE [s_inv] ADD [error_msg_van] [nvarchar](MAX);

ALTER TABLE [s_inv] ADD [vehic] [nvarchar](500);

ALTER TABLE [s_inv_dup] DROP CONSTRAINT [FK__s_inv_dup__ou__3DB3258D];

ALTER TABLE [s_inv_dup] DROP CONSTRAINT [FK__s_inv_dup__uc__3EA749C6];

ALTER TABLE [s_inv_dup] DROP CONSTRAINT [UQ__s_inv_dup__DDDFBCCD088013EC];

ALTER TABLE [s_org] ALTER COLUMN [code] [nvarchar](100);

ALTER TABLE [s_role] ALTER COLUMN [code] [nvarchar](100);

ALTER TABLE [s_role] ALTER COLUMN [code] [nvarchar](100) NULL;

ALTER TABLE [s_org] ALTER COLUMN [fadd] [nvarchar](500);


ALTER TABLE [s_role] ALTER COLUMN [name] [nvarchar](100) NULL;

ALTER TABLE [dbo].[s_inv]
DROP COLUMN seq;
ALTER TABLE [dbo].[s_inv] ADD seq AS (CONVERT([nvarchar](8),json_value([doc],'$.seq')))	 

---thêm cột lý do
insert into s_xls (id, itype, jc, xc) values (280, '01GTKT', 'rea', 'AU')


