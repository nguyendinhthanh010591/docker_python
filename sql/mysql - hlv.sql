ALTER TABLE `einvoice`.`s_sys_logs` 
MODIFY COLUMN `r1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'trường dự phòng 1 (Dùng cho HLV, lưu thông tin mẫu số hóa đơn)' AFTER `dtl`,
MODIFY COLUMN `r2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'trường dự phòng 2 (Dùng cho HLV, lưu thông tin ký hiệu hóa đơn)' AFTER `r1`,
MODIFY COLUMN `r3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'trường dự phòng 3 (Dùng cho HLV, lưu thông tin số hóa đơn)' AFTER `r2`,
MODIFY COLUMN `r4` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'trường dự phòng 4 (Dùng cho HLV, lưu thông tin số hợp đồng)' AFTER `r3`,
ADD COLUMN `msg_cno` int(4) NULL COMMENT 'Lần thay đổi của msg id (chỉ dùng cho HLV)' AFTER `msg_status`;

INDEX `s_inv_policy_number`(`c0`) USING BTREE COMMENT 'danh cho api'