/****** Object: Procedure [dbo].[prc_gen_data_invoice]   Script Date: 4/7/2020 11:52:25 PM ******/
USE [einv_deloitte];
GO
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
CREATE PROCEDURE [dbo].[prc_gen_data_invoice]
@p_fd date, @p_td date, @p_recperday int, @p_doc nvarchar(MAX), @p_ou int, @p_uc nvarchar(20)
WITH EXEC AS CALLER
AS
BEGIN
   DECLARE @invdate   DATE;
   DECLARE @idinv   BIGINT;
   DECLARE @docinv   NVARCHAR (MAX);
   DECLARE @sec   NVARCHAR (10);
   SET @invdate = @p_fd;
   SET @docinv = @p_doc;
   SET @idinv = (SELECT Max (id) FROM s_inv);

   --Lặp từ ngày đến ngày
   WHILE (@invdate <= @p_td)
   BEGIN
      --Lặp để tạo số hóa đơn 1 ngày
      DECLARE @cnt   INT = 1;

      WHILE (@cnt <= @p_recperday)
         BEGIN
            PRINT concat ('XXX', @cnt);
            --Gán lại biến doc
            SET @docinv = @p_doc;
            --Gán lại ngày hóa đơn
            SET @docinv =
                   JSON_MODIFY
                   (@docinv, '$.idt', CONVERT (VARCHAR (10), @invdate, 23));
            --Gán lại số hóa đơn
            SET @docinv =
                   JSON_MODIFY
                   (
                      @docinv,
                      '$.seq',
                      CONVERT
                      (VARCHAR (7), RIGHT (concat ('0000000', @cnt), 7)));
            
            SET @idinv = @idinv + 1;

            --Gán lại mã sec
            SET @sec = master.dbo.fn_varbintohexstr(HASHBYTES('SHA1', CONVERT(NVARCHAR(10), @idinv)));
            
            --Gán lại sec
            SET @docinv =
                   JSON_MODIFY
                   (
                      @docinv,
                      '$.sec',
                      @sec);
            
            PRINT @sec;
            
            INSERT INTO dbo.s_inv (id,
                                   sec,
                                   idt,
                                   doc,
                                   dt,
                                   ou,
                                   uc)
            VALUES (@idinv,
                    @sec,
                    @invdate,
                    @docinv,
                    @invdate,
                    @p_ou,
                    @p_uc);
            
            SET @cnt = @cnt + 1;
         END;

      SET @invdate = DATEADD (day, 1, @invdate)
   END
END;
GO
