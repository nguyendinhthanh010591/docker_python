CREATE INDEX s_inv_dt_idx_vcm_report 
	ON dbo.s_inv (dt)
GO

DROP INDEX s_inv_pid_idx ON dbo.s_inv;

ALTER TABLE dbo.s_inv DROP CONSTRAINT FK__s_inv__pid__3CBF0154;

ALTER TABLE dbo.s_inv ALTER COLUMN pid varchar(3000);

CREATE NONCLUSTERED INDEX s_inv_pid_idx ON dbo.s_inv (  pid ASC  )  
	 WITH (  PAD_INDEX = OFF ,FILLFACTOR = 100  ,SORT_IN_TEMPDB = OFF , IGNORE_DUP_KEY = OFF , STATISTICS_NORECOMPUTE = OFF , ONLINE = OFF , ALLOW_ROW_LOCKS = ON , ALLOW_PAGE_LOCKS = ON  )
	 ON [PRIMARY ];

ALTER TABLE dbo.s_inv ALTER COLUMN cid varchar(3000);
	 
ALTER TABLE [dbo].[s_inv] DROP COLUMN [buyer]
GO

ALTER TABLE [dbo].[s_inv] ADD [buyer] AS (CONVERT([nvarchar](500),json_value([doc],'$.buyer')))	 
GO

CREATE TRIGGER [dbo].[s_org_aui] ON [dbo].[s_org] 
AFTER INSERT,UPDATE
AS
BEGIN
	declare @id int,@addr nvarchar(255),@prov nvarchar(3),@dist nvarchar(5),@ward nvarchar(7), @type int
	select 
		@id = ins.id,
		@addr = ins.addr,
		@prov = ins.prov,
		@dist = ins.dist,
		@ward = ins.ward, 
		@type = ins.type
	from 
		inserted ins

	if @type = 1 
		begin
			update 
				s_org 
				set 
					fadd=dbo.get_fadd(@addr,@prov,@dist,@ward) 
				where 
					id=@id
		end
END;

/****** Object:  Index [s_inv_idt_IDX_XX]    Script Date: 9/10/2020 7:24:09 PM ******/
CREATE NONCLUSTERED INDEX [s_inv_idx_stfs] ON [dbo].[s_inv]
(
	[stax] ASC,
	[type] ASC,
	[form] ASC,
	[serial] ASC,
	[idt] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON);


