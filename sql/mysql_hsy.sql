CREATE TABLE s_invdoctemp (
    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID tự sinh',
  `invid` int(11) NOT NULL COMMENT 'ID bảng hóa đơn S_INV',
  `doc` json NOT NULL COMMENT 'Trường doc bảng hóa đơn S_INV',
  `dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Ngày hệ thống',
  `status` int(11) NOT NULL COMMENT 'Trạng thái. 0 - Chưa gửi, 1 - Đã gửi',
  `xml` text COMMENT 'XML hóa đơn có chữ ký số',
  `iserror` int(1) NOT NULL DEFAULT '0' COMMENT 'Trạng thái nghiệp vụ. 1 - Có lỗi, 0 - Không có lỗi',
  `error_desc` text COMMENT 'Mô tả lỗi trong trường hợp xử lý nghiệp vụ có lỗi',
  /* Keys */
  PRIMARY KEY (id)
) ENGINE = InnoDB
  COMMENT = 'Bảng lưu thông tin dữ liệu hóa đơn chưa ký và kết xuất file';

CREATE INDEX s_invdoctemp_index01
  ON s_invdoctemp
  (dt, `status`, `iserror`);