use einv_deloitte;

drop table s_inv;
drop table s_seou;
drop table s_serial;
drop table s_ex;
drop table s_manager;
drop table s_member;
drop table s_user; 
drop table s_role;
drop table s_ou;
drop table s_org;
drop table s_cat;
drop table s_taxo;
drop table s_loc;
drop table s_gns;
drop table s_dcd;
drop table s_dcm;
drop table s_xls;
drop table s_f_fld;
drop function dbo.get_fadd;
drop function dbo.get_local_name;
----------------------------------------------------------------
create table s_loc (
  id 	nvarchar(7)   not null,
  name  nvarchar(100) not null,
  pid   nvarchar(7),
  primary key (id),
  index s_loc_pid_idx(pid)
);
--alter table s_loc add constraint s_loc_fk  foreign key (pid) references s_loc(id);
----------------------------------------------------------------
create function dbo.get_local_name (@pcode nvarchar(7))  returns nvarchar(100) WITH SCHEMABINDING as
begin
   declare @pname nvarchar(100)
   if (@pcode is null)
     set @pname=null
   else   
    select @pname = name from dbo.s_loc where id = @pcode
   return @pname
end;
----------------------------------------------------------------
create function dbo.get_fadd (@paddr nvarchar(255), @pprov nvarchar(3), @pdist nvarchar(5), @pward nvarchar(7)) returns nvarchar(255) WITH SCHEMABINDING as
begin
   declare @vward   nvarchar (100) = dbo.get_local_name(@pward)
   declare @vdist   nvarchar (100) = dbo.get_local_name(@pdist)
   declare @vprov   nvarchar (100) = dbo.get_local_name(@pprov)
   declare @tmp     nvarchar (255) = @paddr
   if (@vward is not null)
        set @tmp = CONCAT(@tmp,', ',@vward)
   if (@vdist is not null)
        set @tmp = CONCAT(@tmp,', ',@vdist)
   if (@vprov is not null)
        set @tmp = CONCAT(@tmp,', ',@vprov) 
   return @tmp
end;
----------------------------------------------------------------------------------------------------------------------
create table s_gns (
  id    int identity (1,1) not null,
  code  nvarchar(30)  not null,
  name  nvarchar(255) not null,
  unit  nvarchar(30)  default 'N/A' not null,
  price numeric(17,2),
  primary key (id),
  unique(code)
);
----------------------------------------------------------------------------------------------------------------------
create table s_dcd (
  id    nvarchar(30)   not null,
  name  nvarchar(100) not null,
  type  nvarchar(30)   not null,
  primary key (type,id,name)
);
---------------------------------------------------------------------
create table s_dcm (
  id    int identity (1,1) not null,
  itype nvarchar(6) not null,
  idx   tinyint    not null,
  dtl   tinyint    default 2 not null,
  lbl   nvarchar(100) not null,
  typ 	nvarchar(20)   not null,
  atr   nvarchar(4000),
  status tinyint default 2 not null,
  xc     nvarchar(2),
  primary key (id),
  unique (itype,idx,dtl),
  check (ISJSON(atr)>0)
 );
--------------------------------------------------------------------------------------------------------------------

create table s_xls (
  id int identity (1,1) not null,
  itype nvarchar(6)  not null,
  jc    nvarchar(20) not null,
  xc    nvarchar(2),
  primary key (id),
  unique (itype,jc)
);
----------------------------------------------------------------
create table s_cat (
	id int identity (1,1) not null,
	name   nvarchar(100)  not null,
	[type] nvarchar(30)    not null,
	des    nvarchar(255),
	primary key (id),
	unique ([type],name)
);
----------------------------------------------------------------
create table s_taxo (
  id   nvarchar(5)    not null,
  name nvarchar(100)  not null,
  primary key (id)
);
----------------------------------------------------------------
create table s_org (
  id   		int identity (1,1) not null,
  code      nvarchar(50),
  name 		nvarchar(255) not null,
  name_en   nvarchar(255),
  taxc 		nvarchar(14),
  prov 		nvarchar(3),
  dist 		nvarchar(5),
  ward 		nvarchar(7),
  addr 		nvarchar(255),
  tel 		nvarchar(255),
  mail 		nvarchar(255),
  acc 		nvarchar(255),
  bank 		nvarchar(255),
  status 	nvarchar(2) default '00' not null,
  c0        nvarchar(255),
  c1        nvarchar(255),
  c2        nvarchar(255),
  c3        nvarchar(255),
  c4        nvarchar(255),
  c5        nvarchar(255),
  c6        nvarchar(255),
  c7        nvarchar(255),
  c8        nvarchar(255),
  c9        nvarchar(255),
  pwd       nvarchar(255),
  fadd 		nvarchar(255),
  primary key (id)
 );
CREATE UNIQUE NONCLUSTERED INDEX s_org_taxc_idx ON dbo.s_org(taxc) WHERE taxc IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX s_org_code_idx ON dbo.s_org(code) WHERE code IS NOT NULL;


DROP FULLTEXT CATALOG s_org_catalog;
CREATE FULLTEXT CATALOG s_org_catalog;
DROP FULLTEXT INDEX ON s_org; 
CREATE FULLTEXT INDEX ON s_org(code,taxc,name,name_en,fadd,tel,mail,acc,bank,c0,c1,c2,c3,c4,c5,c6,c7,c8,c9)  KEY INDEX PK__s_org__3213E83F9BB26010 ON s_org_catalog;
select * from s_org where CONTAINS (*,N'"13 duy"');

DROP TRIGGER s_org_aui;
CREATE TRIGGER s_org_aui ON dbo.s_org 
AFTER INSERT,UPDATE
AS
BEGIN
 DECLARE @id int,@addr nvarchar(255),@prov nvarchar(3),@dist nvarchar(5),@ward nvarchar(7)
 SELECT @id=ins.id	   FROM INSERTED ins
 select @addr=ins.addr FROM INSERTED ins
 SELECT @prov=ins.prov FROM INSERTED ins
 SELECT @dist=ins.dist FROM INSERTED ins
 SELECT @ward=ins.ward FROM INSERTED ins
 UPDATE s_org set fadd=dbo.get_fadd(@addr,@prov,@dist,@ward) where id=@id
END;
 ---------------------------------------------------------------
create table s_ou(
id      int identity (1,1) not null,
pid     int,
code    nvarchar(20),
name    nvarchar(255) not null,
taxc    nvarchar(14),
mst     nvarchar(14)  not null, 
paxo    nvarchar(5),
taxo    nvarchar(5),
prov    nvarchar(3),
dist    nvarchar(5),
ward    nvarchar(7),
addr    nvarchar(255),
tel     nvarchar(255),
mail    nvarchar(255),
acc     nvarchar(255),
bank    nvarchar(255),
status  tinyint	default 1 not null,
sign    tinyint	default 1 not null,
seq     tinyint	default 1 not null,
usr     nvarchar(255), 
pwd     nvarchar(255),
temp    tinyint	default 1 not null,
fadd    as dbo.get_fadd(addr,prov,dist,ward),
primary key(id),
foreign key(pid) references s_ou(id),
index s_ou_pid_idx (pid)
);
--alter table s_ou add fadd  as dbo.get_fadd(addr,prov,dist,ward); 
CREATE UNIQUE NONCLUSTERED INDEX s_ou_taxc_idx ON dbo.s_ou(taxc) WHERE taxc IS NOT NULL;
CREATE UNIQUE NONCLUSTERED INDEX s_ou_code_idx ON dbo.s_ou(code) WHERE code IS NOT NULL;
----------------------------------------------------------------
create table s_user (
  id 	nvarchar(20) not null,
  code  nvarchar(20),
  mail 	nvarchar(50),
  ou 	int not null,
  uc    tinyint  default 1 not null,
  name  nvarchar(50) not null,
  pos   nvarchar(50),
  primary key (id),
  foreign key (ou) references s_ou(id)
);
CREATE UNIQUE NONCLUSTERED INDEX s_user_code_idx ON dbo.s_user(code) WHERE code IS NOT NULL;
create index s_user_ou_idx on s_user(ou); 
----------------------------------------------------------------
create table s_role (
  id   int identity (1,1) not null,
  name nvarchar(100) not null,
  primary key (id)
);
----------------------------------------------------------------------------------------------
create table s_member (
  user_id nvarchar(20) not null,
  role_id int not null,
  primary key(user_id,role_id),
  foreign key(user_id) references s_user(id),
  foreign key(role_id) references s_role(id)
);
----------------------------------------------------------------------------------------------
create table s_manager (
  user_id nvarchar(20) not null,
  taxc    nvarchar(14) not null,
  primary key(user_id,taxc),
  foreign key(user_id) references s_user(id)
 );
----------------------------------------------------------------------------------------------
create table s_ex (
  id  int identity (1,1) not null,
  dt  datetime default GETDATE() not null,
  cur nvarchar(3)  not null,
  val numeric(8,2) not null,
  primary key (id),
  unique (dt,cur)
);
---------------------------------------------------------------------------------------------------------------------
create table s_serial (
  id 	 int identity (1,1) not null,
  taxc   nvarchar(14) not null,
  type   nvarchar(6)  not null,
  form   nvarchar(11) not null,
  serial nvarchar(6)  not null,
  min 	 numeric(7,0)  not null,
  max    numeric(7,0)  not null,
  cur    numeric(7,0)  not null,
  status tinyint  default 3 not null,
  fd datetime not null,
  td datetime,
  dt datetime default GETDATE() not null,
  uc nvarchar(20)     not null,
  uses  tinyint  default 1 not null,
  primary key (id),
  unique (taxc,form,serial),
  check (min>0 and (cur>=min or cur=0) and max>=cur),
  foreign key (uc) references s_user(id),
  index s_serial_uc_idx(uc)
);
----------------------------------------------------------------
create table s_seou (
  se int not null, 
  ou int not null, 
  constraint s_seou_pk primary key (se, ou), 
  constraint s_seou_fk1 foreign key (ou) references s_ou (id), 
  constraint s_seou_fk2 foreign key (se) references s_serial (id)
 );
 ------------------------------------------------------------------
create table s_inv (
id  	bigint,
sec     nvarchar(10) not null,
ic  	nvarchar(36),
idt 	datetime not null,
pid 	bigint,
cid 	bigint,
cde 	nvarchar(255),
doc 	nvarchar(max) not null,
xml 	nvarchar(max),
dt  	datetime default GETDATE() not null,
ou  	int  not null,
uc  	nvarchar(20) not null,
cvt     tinyint default 0, 
status  as CAST(json_value(doc,'$.status')  as tinyint), 
type    as CAST(json_value(doc,'$.type')    as nvarchar(10)),
form    as CAST(json_value(doc,'$.form')    as nvarchar(11)),
serial  as CAST(json_value(doc,'$.serial')  as nvarchar(6)),
seq     as CAST(json_value(doc,'$.seq')     as nvarchar(7)),
paym    as CAST(json_value(doc,'$.paym')    as nvarchar(10)),
curr    as CAST(json_value(doc,'$.curr')    as nvarchar(3)),
exrt    as CAST(json_value(doc,'$.exrt')    as numeric(8,2)),
sname   as CAST(json_value(doc,'$.sname')   as nvarchar(255)),
stax    as CAST(json_value(doc,'$.stax')    as nvarchar(14)),
saddr   as CAST(json_value(doc,'$.saddr')   as nvarchar(255)),
smail   as CAST(json_value(doc,'$.smail')   as nvarchar(255)),
stel    as CAST(json_value(doc,'$.stel' )   as nvarchar(255)),
sacc    as CAST(json_value(doc,'$.sacc' )   as nvarchar(255)),
sbank   as CAST(json_value(doc,'$.sbank')   as nvarchar(255)),
buyer   as CAST(json_value(doc,'$.buyer')   as nvarchar(255)),
bname   as CAST(json_value(doc,'$.bname')   as nvarchar(255)),
btax    as CAST(json_value(doc,'$.btax')    as nvarchar(14)),
baddr   as CAST(json_value(doc,'$.baddr')   as nvarchar(255)),
bmail   as CAST(json_value(doc,'$.bmail')   as nvarchar(255)),
btel    as CAST(json_value(doc,'$.btel')    as nvarchar(255)),
bacc    as CAST(json_value(doc,'$.bacc')    as nvarchar(255)),
bbank   as CAST(json_value(doc,'$.bbank')   as nvarchar(255)),
note    as CAST(json_value(doc,'$.note')    as nvarchar(255)),
sum     as CAST(json_value(doc,'$.sum')     as numeric(17,2)),
sumv    as CAST(json_value(doc,'$.sumv')    as numeric(17,2)),
vat     as CAST(json_value(doc,'$.vat')     as numeric(17,2)),
vatv    as CAST(json_value(doc,'$.vatv')    as numeric(17,2)),
total   as CAST(json_value(doc,'$.total')      as numeric(17,2)),
totalv  as CAST(json_value(doc,'$.totalv')     as numeric(17,2)),
adjseq  as CAST(json_value(doc,'$.adj.seq')    as nvarchar(26)),
adjdes  as CAST(json_value(doc,'$.adj.des')    as nvarchar(255)),
adjtyp  as CAST(json_value(doc,'$.adj.typ')    as tinyint),
c0      as CAST(json_value(doc,'$.c0')    as nvarchar(255)),
c1      as CAST(json_value(doc,'$.c1')    as nvarchar(255)),
c2      as CAST(json_value(doc,'$.c2')    as nvarchar(255)),
c3      as CAST(json_value(doc,'$.c3')    as nvarchar(255)),
c4      as CAST(json_value(doc,'$.c4')    as nvarchar(255)),
c5      as CAST(json_value(doc,'$.c5')    as nvarchar(255)),
c6      as CAST(json_value(doc,'$.c6')    as nvarchar(255)),
c7      as CAST(json_value(doc,'$.c7')    as nvarchar(255)),
c8      as CAST(json_value(doc,'$.c8')    as nvarchar(255)),
c9      as CAST(json_value(doc,'$.c9')    as nvarchar(255)),
primary key(id),
unique(sec),
check (isjson(doc)>0),
foreign key (pid) references s_inv(id),
foreign key (ou)  references s_ou(id),
foreign key (uc)  references s_user(id),
index s_inv_pid_idx(pid),
index s_inv_ou_idx(ou),
index s_inv_uc_idx(uc),
index s_inv_idt_idx(idt,stax,ou,type,form,serial,status)
);

--------------------------------------------------------
--init user
insert into s_user(id,ou,name) values ('admin',1,'admin');
declare @i int=0, @uid nvarchar(20)= 'admin'
while @i < 14
begin
     select @i = @i + 1
     insert into s_member(user_id,role_id) values (@uid,@i)
end


-------------------
  CREATE TABLE s_group
   (	"ID" int IDENTITY(1,1), 
	"NAME" Nvarchar(200) not null, 
	"OU" int, 
	"STATUS" varchar(1 ) not null, 
	"APPROVE" varchar(1 ) DEFAULT 1, 
	"DES" Nvarchar(200 ) DEFAULT 1
   )  ;
   --------------------------
CREATE TABLE s_group_role 
   (	"GROUP_ID" FLOAT NOT NULL , 
	"ROLE_ID" FLOAT NOT NULL , 
	"STATUS" varchar(1)
   )
   ----------------
    CREATE TABLE s_group_user 
   (	"USER_ID" varchar(20) NOT NULL , 
	"GROUP_ID" float NOT NULL 
   )
   ---------------
   delete s_member
   delete s_role

  ALTER TABLE s_role
  ADD [CODE] [varchar](200) NOT NULL;

INSERT INTO s_role VALUES (1, N'Quản trị hệ thống', 'PERM_FULL_MANAGE');
INSERT INTO s_role VALUES (2, N'Cấu hình hệ thống', 'PERM_SYS_CONFIG');
INSERT INTO s_role VALUES (3, N'Quản lý người dùng', 'PERM_SYS_USER');
INSERT INTO s_role VALUES (4, N'Quản lý nhóm', 'PERM_SYS_GROUP');
INSERT INTO s_role VALUES (5, N'Phân quyền', 'PERM_SYS_ROLE');
INSERT INTO s_role VALUES (6, N'Tạo mẫu hóa đơn', 'PERM_SYS_TEMPLATE');
INSERT INTO s_role VALUES (7, N'Tạo mẫu Email', 'PERM_SYS_EMAIL');
INSERT INTO s_role VALUES (8, N'Cấu hình hóa đơn', 'PERM_SYS_INV_CONFIG');
INSERT INTO s_role VALUES (9, N'Cấu hình trường', 'PERM_SYS_FIELD_CONFIG');
INSERT INTO s_role VALUES (10, N'Quản trị danh mục', 'PERM_CATEGORY_MANAGE');
INSERT INTO s_role VALUES (11, N'Khách hàng', 'PERM_CATEGORY_CUSTOMER');
INSERT INTO s_role VALUES (12, N'Hàng hóa, dịch vụ', 'PERM_CATEGORY_ITEMS');
INSERT INTO s_role VALUES (13, N'Tỷ giá', 'PERM_CATEGORY_RATE');
INSERT INTO s_role VALUES (14, N'Danh mục khác', 'PERM_CATEGORY_ANOTHER');

INSERT INTO s_role VALUES (15, N'Thông báo phát hành', 'PERM_TEMPLATE_REGISTER');
INSERT INTO s_role VALUES (16, N'Gán quyền sử dụng TBPH', 'PERM_TEMPLATE_REGISTER_GRANT');

INSERT INTO s_role VALUES (17, N'Lập hóa đơn', 'PERM_CREATE_INV_MANAGE');
INSERT INTO s_role VALUES (18, N'Lập hóa đơn từ Excel', 'PERM_CREATE_INV_EXCEL');
INSERT INTO s_role VALUES (19, N'Lập hóa đơn từ nguồn khác', 'PERM_CREATE_INV_ANOTHER');
INSERT INTO s_role VALUES (20, N'Cấp số', 'PERM_CREATE_INV_SEQ');
INSERT INTO s_role VALUES (21, N'Duyệt và xuất hóa đơn', 'PERM_APPROVE_INV_MANAGE');
INSERT INTO s_role VALUES (22, N'Báo cáo', 'PERM_REPORT_INV');
INSERT INTO s_role VALUES (23, N'Tra cứu hóa đơn', 'PERM_SEARCH_INV');
INSERT INTO s_role VALUES (24, N'Điều chỉnh hóa đơn', 'PERM_INVOICE_ADJUST');
INSERT INTO s_role VALUES (25, N'Thay thế hóa đơn', 'PERM_INVOICE_REPLACE');
INSERT INTO s_role VALUES (26, N'Hủy hóa đơn', 'PERM_INVOICE_CANCEL');
INSERT INTO s_role VALUES (27, N'Duyệt thông báo phát hành', 'PERM_TEMPLATE_APPROVE');
INSERT INTO s_role VALUES (28, N'Hủy thông báo phát hành', 'PERM_TEMPLATE_VOID');
INSERT INTO s_role VALUES (29, N'Tích hợp', 'PERM_INTEGRATED');
INSERT INTO s_role VALUES (30, N'Chờ hủy', 'PERM_VOID_WAIT');
INSERT INTO s_role VALUES (31, N'Log hệ thống', 'PERM_LOG_SYS');
INSERT INTO s_role VALUES (32, N'Tiến lùi ngày hóa đơn', 'PERM_REDIRECT_IDT');
-----------------------------------------------------------------------------------
--can de not null ,luc dau chay cho phep null sau khi co gia trị chuyen ve not null
  ALTER TABLE s_dcm
  ADD [lbl_en] [nvarchar](200) NOT NULL ;

  update s_dcm set lbl_en=lbl-- day là de mac dinh chua dat ten

  
-- ----------------------------
-- Table structure for s_f_fld
-- ----------------------------
  CREATE TABLE s_f_fld
   (	
        id int identity (1,1) not null,
        fnc_name nvarchar(30) NOT NULL , 
        fnc_url nvarchar(15) NOT NULL , 
        fld_name nvarchar(30) NOT NULL , 
        fld_id nvarchar(30) NOT NULL , 
        fld_typ nvarchar(50) NOT NULL, 
        atr nvarchar(255), 
        feature nvarchar(255), 
        status tinyint default 0 not null,
        unique(fnc_url,fld_id,feature),
        constraint s_f_fld_pk primary key (id), 
   ) 
   -- "ID" IS 'ID duy nhất, Primary key';
   -- "FNC_NAME" IS 'Tên chức năng';
   -- "FNC_URL" IS 'URL chức năng';
   -- "FLD_NAME" IS 'Tên trường tương ứng trên chức năng';
   -- "ATTR" IS 'Thuộc tính, là chuỗi Json khai báo';
   -- "STATUS" IS 'Trạng thái; 1 - active, 0 - inactive';
   -- "FLD_ID" IS 'id trường';
   -- "FLD_type" IS 'Kiểu item của trường trên form (other, datagrid)';
   -- "feature" IS 'JSON nhận dạng màn hình ví dụ lập hđ';
   --  "S_F_FLD"  IS 'Bảng lưu các trường của từng chức năng và cấu hình các thuộc tính tương ứng';

-- ----------------------------
-- Table structure for s_inv_file
-- ----------------------------
CREATE TABLE s_inv_file (
  id bigint NOT NULL,
  type VARCHAR(45) NOT NULL,
  content varchar(max) NOT NULL,
  PRIMARY KEY (id,type),
  foreign key (id) references s_inv(id)
  )
-- COMMENT = 'ID, type, content của các file hóa đơn';
-- type: adj, rep, can

----alter pass to user

Alter table s_user add [pass] [varchar](200) default 'YWRtaW5AMTIz';


--SEQUENCE cho bảng S_SYS_LOGS
USE [einv_deloitte];
GO
CREATE SEQUENCE [dbo].[ISEQ_SYS_LOGS]
	AS int
	START WITH 1
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 1000
	NO CACHE
	CYCLE
GO

-- ----------------------------
-- Table structure for s_sys_logs
-- ----------------------------
IF EXISTS (SELECT * FROM sys.all_objects WHERE object_id = OBJECT_ID(N'[dbo].[s_sys_logs]') AND type IN ('U'))
	DROP TABLE [dbo].[s_sys_logs]
GO

CREATE TABLE [dbo].[s_sys_logs] (
  [id] decimal(18)  default (replace(replace(replace(CONVERT([varchar],getdate(),(20)),'-',''),':',''),' ','')+replace(str(NEXT VALUE FOR [ISEQ_SYS_LOGS],(3)),' ','0')) NOT NULL,
  [dt] datetime  NOT NULL,
  [fnc_id] nvarchar(30) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [fnc_name] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [fnc_url] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [action] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [user_id] nvarchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [user_name] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [src] nvarchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [dtl] nvarchar(1000) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [r1] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [r2] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [r3] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [r4] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [msg_id] nvarchar(255) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [msg_status] int  NULL
)
GO

ALTER TABLE [dbo].[s_sys_logs] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'ID tự sinh',
'SCHEMA', N'dbo',
'TABLE', N's_sys_logs',
'COLUMN', N'id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'Ngày ghi nhận log',
'SCHEMA', N'dbo',
'TABLE', N's_sys_logs',
'COLUMN', N'dt'
GO

EXEC sp_addextendedproperty
'MS_Description', N'Mã chức năng',
'SCHEMA', N'dbo',
'TABLE', N's_sys_logs',
'COLUMN', N'fnc_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'Tên chức năng',
'SCHEMA', N'dbo',
'TABLE', N's_sys_logs',
'COLUMN', N'fnc_name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'URL thực thi',
'SCHEMA', N'dbo',
'TABLE', N's_sys_logs',
'COLUMN', N'fnc_url'
GO

EXEC sp_addextendedproperty
'MS_Description', N'Action thực thi',
'SCHEMA', N'dbo',
'TABLE', N's_sys_logs',
'COLUMN', N'action'
GO

EXEC sp_addextendedproperty
'MS_Description', N'ID user',
'SCHEMA', N'dbo',
'TABLE', N's_sys_logs',
'COLUMN', N'user_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'Tên user',
'SCHEMA', N'dbo',
'TABLE', N's_sys_logs',
'COLUMN', N'user_name'
GO

EXEC sp_addextendedproperty
'MS_Description', N'Nguồn phát sinh log',
'SCHEMA', N'dbo',
'TABLE', N's_sys_logs',
'COLUMN', N'src'
GO

EXEC sp_addextendedproperty
'MS_Description', N'Chi tiết log',
'SCHEMA', N'dbo',
'TABLE', N's_sys_logs',
'COLUMN', N'dtl'
GO

EXEC sp_addextendedproperty
'MS_Description', N'Trường dự phòng 1',
'SCHEMA', N'dbo',
'TABLE', N's_sys_logs',
'COLUMN', N'r1'
GO

EXEC sp_addextendedproperty
'MS_Description', N'Trường dự phòng 2',
'SCHEMA', N'dbo',
'TABLE', N's_sys_logs',
'COLUMN', N'r2'
GO

EXEC sp_addextendedproperty
'MS_Description', N'Trường dự phòng 3',
'SCHEMA', N'dbo',
'TABLE', N's_sys_logs',
'COLUMN', N'r3'
GO

EXEC sp_addextendedproperty
'MS_Description', N'Trường dự phòng 4',
'SCHEMA', N'dbo',
'TABLE', N's_sys_logs',
'COLUMN', N'r4'
GO

EXEC sp_addextendedproperty
'MS_Description', N'trường lưu msg id (tên file, id chuỗi msg XML, id chuỗi Json)',
'SCHEMA', N'dbo',
'TABLE', N's_sys_logs',
'COLUMN', N'msg_id'
GO

EXEC sp_addextendedproperty
'MS_Description', N'trạng thái xử lý msg (0 - xử lý lỗi, 1 - xử lý thành công)',
'SCHEMA', N'dbo',
'TABLE', N's_sys_logs',
'COLUMN', N'msg_status'
GO

EXEC sp_addextendedproperty
'MS_Description', N'Bảng lưu log audit thao tác của NSD APP, API',
'SCHEMA', N'dbo',
'TABLE', N's_sys_logs'
GO


-- ----------------------------
-- Indexes structure for table s_sys_logs
-- ----------------------------
CREATE NONCLUSTERED INDEX [S_SYS_LOGS_IDX01]
ON [dbo].[s_sys_logs] (
  [src] ASC,
  [fnc_id] ASC,
  [user_id] ASC
)
WITH (
  FILLFACTOR = 100
)
GO

CREATE NONCLUSTERED INDEX [S_SYS_LOGS_IDX02]
ON [dbo].[s_sys_logs] (
  [src] ASC,
  [msg_id] ASC,
  [msg_status] ASC
)
WITH (
  FILLFACTOR = 100
)
GO


-- ----------------------------
-- Primary Key structure for table s_sys_logs
-- ----------------------------
ALTER TABLE [dbo].[s_sys_logs] ADD CONSTRAINT [PK__s_sys_lo__3213E83F8BEDA11D] PRIMARY KEY NONCLUSTERED ([id])
WITH (PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO

CREATE INDEX s_inv_btax_idx 
	ON dbo.s_inv (status, type, btax)
GO

CREATE INDEX S_SERIAL_DT_IDX 
	ON dbo.s_serial (taxc, fd, td)
GO	


--tạo bảng detail hóa đơn	
USE [einv_deloitte];
GO
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
CREATE TABLE [dbo].[s_invd] (
[ID] decimal(18, 0) NOT NULL DEFAULT (NEXT VALUE FOR [ISEQ_INVD]),
[INV_ID] decimal(18, 0) NULL,
[REF_NO] varchar(50) NOT NULL,
CONSTRAINT [PK__S_INVD__3214EC27439F5269]
PRIMARY KEY CLUSTERED ([ID] ASC)
WITH ( PAD_INDEX = OFF,
FILLFACTOR = 100,
IGNORE_DUP_KEY = OFF,
STATISTICS_NORECOMPUTE = OFF,
ALLOW_ROW_LOCKS = ON,
ALLOW_PAGE_LOCKS = ON,
DATA_COMPRESSION = NONE )
 ON [PRIMARY]
)
ON [PRIMARY];
GO
ALTER TABLE [dbo].[s_invd] SET (LOCK_ESCALATION = TABLE);
GO

CREATE NONCLUSTERED INDEX [S_INVD_IDX01]
ON [dbo].[s_invd]
([INV_ID])
WITH
(
PAD_INDEX = OFF,
FILLFACTOR = 100,
IGNORE_DUP_KEY = OFF,
STATISTICS_NORECOMPUTE = OFF,
ONLINE = OFF,
ALLOW_ROW_LOCKS = ON,
ALLOW_PAGE_LOCKS = ON,
DATA_COMPRESSION = NONE
)
ON [PRIMARY];
GO
/****** Object: Index [dbo].[s_invd].[S_INVD_IDX02]   Script Date: 22/04/2020 4:32:50 PM ******/

CREATE NONCLUSTERED INDEX [S_INVD_IDX02]
ON [dbo].[s_invd]
([REF_NO])
WITH
(
PAD_INDEX = OFF,
FILLFACTOR = 100,
IGNORE_DUP_KEY = OFF,
STATISTICS_NORECOMPUTE = OFF,
ONLINE = OFF,
ALLOW_ROW_LOCKS = ON,
ALLOW_PAGE_LOCKS = ON,
DATA_COMPRESSION = NONE
)
ON [PRIMARY];
GO

/****** Object: Sequence [dbo].[ISEQ_INVD]   Script Date: 22/04/2020 4:33:59 PM ******/
USE [einv_deloitte];
GO
CREATE SEQUENCE [dbo].[ISEQ_INVD]
	AS bigint
	START WITH 1
	INCREMENT BY 1
	NO MINVALUE
	NO MAXVALUE
	CACHE
	NO CYCLE
GO

ALTER TABLE [dbo].[s_inv]
 ADD [cdt] AS (CONVERT([datetime],json_value([doc],'$.cdt')))
GO

-- ----------------------------
-- Table structure for S_ERR_MSG
-- ----------------------------
CREATE TABLE [dbo].[S_ERR_MSG] (
  [SRC_ERR_CODE] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [USR_CODE] varchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL,
  [USR_MSG] varchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [USR_MSG_ENT] varchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS  NULL,
  [TYPE_MSG] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[S_ERR_MSG] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'Mã thông báo nguồn',
'SCHEMA', N'dbo',
'TABLE', N'S_ERR_MSG',
'COLUMN', N'SRC_ERR_CODE'
GO

EXEC sp_addextendedproperty
'MS_Description', N'Mã thông báo hiển thị cho NSD',
'SCHEMA', N'dbo',
'TABLE', N'S_ERR_MSG',
'COLUMN', N'USR_CODE'
GO

EXEC sp_addextendedproperty
'MS_Description', N'Mô tả thông báo hiển thị cho NSD. Nếu cần truyền tham số đặt tên các tham số là ${p0}, ${p1},...,${pn}',
'SCHEMA', N'dbo',
'TABLE', N'S_ERR_MSG',
'COLUMN', N'USR_MSG'
GO

EXEC sp_addextendedproperty
'MS_Description', N'Mô tả thông báo hiển thị cho NSD bằng tiếng Anh. Nếu cần truyền tham số đặt tên các tham số là ${p0}, ${p1},...,${pn}',
'SCHEMA', N'dbo',
'TABLE', N'S_ERR_MSG',
'COLUMN', N'USR_MSG_ENT'
GO

EXEC sp_addextendedproperty
'MS_Description', N'Loại thông báo. INFO - Thông báo, WARNING - Cảnh báo, ERROR - Lỗi',
'SCHEMA', N'dbo',
'TABLE', N'S_ERR_MSG',
'COLUMN', N'TYPE_MSG'
GO

EXEC sp_addextendedproperty
'MS_Description', N'Bảng lưu danh mục mã thông báo NSD',
'SCHEMA', N'dbo',
'TABLE', N'S_ERR_MSG'
GO


-- ----------------------------
-- Primary Key structure for table S_ERR_MSG
-- ----------------------------
ALTER TABLE [dbo].[S_ERR_MSG] ADD CONSTRAINT [PK__S_ERR_MS__30E854EA804CF9E5] PRIMARY KEY CLUSTERED ([SRC_ERR_CODE])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX s_inv_cdt_idx ON dbo.s_inv (  stax ASC  , ou ASC  , type ASC  , form ASC  , serial ASC  , status ASC  )  
	 WITH (  PAD_INDEX = OFF ,FILLFACTOR = 100  ,SORT_IN_TEMPDB = OFF , IGNORE_DUP_KEY = OFF , STATISTICS_NORECOMPUTE = OFF , ONLINE = OFF , ALLOW_ROW_LOCKS = ON , ALLOW_PAGE_LOCKS = ON  )
	 ON [PRIMARY ];
------- trungpq10 them cột des trong s_serial mô tả mục đích sử dụng tbph
ALTER TABLE [dbo].[s_serial]
 ADD [des]  [nvarchar](200)
---------------them loại dữ liệu 
ALTER TABLE [s_org]  ADD [type] [int] NULL
ALTER TABLE [dbo].[s_org] ADD  CONSTRAINT [DF_s_org_type]  DEFAULT ((1)) FOR [type]
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'kiểu dữ liệu 1_nhập tay/2_tự động' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N's_org', @level2type=N'COLUMN',@level2name=N'type'


/* Chạy riêng cho Deloitte vì Deloitte chỉ ghi log API
ALTER TABLE [dbo].[s_sys_logs] 
ADD  CONSTRAINT [UK_s_sys_logs_msg_id]
UNIQUE NONCLUSTERED ([msg_id] ASC)
WITH ( PAD_INDEX = OFF,
FILLFACTOR = 100,
IGNORE_DUP_KEY = OFF,
STATISTICS_NORECOMPUTE = OFF,
ALLOW_ROW_LOCKS = ON,
ALLOW_PAGE_LOCKS = ON,
DATA_COMPRESSION = NONE )
 ON [PRIMARY]
GO
<<<<<<< .mine
*/
--xoa table s_seou tạo lại de design lại bảng s_serial
DROP TABLE [dbo].[s_seou]
GO


---tạo lại bảng s_seriall


ALTER TABLE [dbo].[s_serial] DROP CONSTRAINT [CK__s_serial__2F650636]
GO

ALTER TABLE [dbo].[s_serial] DROP CONSTRAINT [FK__s_serial__uc__30592A6F]
GO

ALTER TABLE [dbo].[s_serial] DROP CONSTRAINT [DF__s_serial__uses__2E70E1FD]
GO

ALTER TABLE [dbo].[s_serial] DROP CONSTRAINT [DF__s_serial__dt__2D7CBDC4]
GO

ALTER TABLE [dbo].[s_serial] DROP CONSTRAINT [DF__s_serial__status__2C88998B]
GO

/****** Object:  Table [dbo].[s_serial]    Script Date: 18/05/2020 10:24:15 AM ******/
DROP TABLE [dbo].[s_serial]
GO

/****** Object:  Table [dbo].[s_serial]    Script Date: 18/05/2020 10:24:15 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[s_serial](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[taxc] [nvarchar](14) NOT NULL,
	[type] [nvarchar](6) NOT NULL,
	[form] [nvarchar](11) NOT NULL,
	[serial] [nvarchar](6) NOT NULL,
	[min] [numeric](8, 0) NOT NULL,
	[max] [numeric](8, 0) NOT NULL,
	[cur] [numeric](8, 0) NOT NULL,
	[status] [tinyint] NOT NULL,
	[fd] [datetime] NOT NULL,
	[td] [datetime] NULL,
	[dt] [datetime] NOT NULL,
	[uc] [nvarchar](20) NOT NULL,
	[uses] [tinyint] NOT NULL,
	[des] [nvarchar](200) NULL,
	[priority] [int] NOT NULL,
 CONSTRAINT [PK__s_serial__3213E83F0A16F9FB] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UQ__s_serial__AFB6B964CDDDDB1F] UNIQUE NONCLUSTERED 
(
	[taxc] ASC,
	[form] ASC,
	[serial] ASC,
	[priority] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[s_serial] ADD  CONSTRAINT [DF__s_serial__status__2C88998B]  DEFAULT ((3)) FOR [status]
GO

ALTER TABLE [dbo].[s_serial] ADD  CONSTRAINT [DF__s_serial__dt__2D7CBDC4]  DEFAULT (getdate()) FOR [dt]
GO

ALTER TABLE [dbo].[s_serial] ADD  CONSTRAINT [DF__s_serial__uses__2E70E1FD]  DEFAULT ((1)) FOR [uses]
GO

ALTER TABLE [dbo].[s_serial]  WITH CHECK ADD  CONSTRAINT [FK__s_serial__uc__30592A6F] FOREIGN KEY([uc])
REFERENCES [dbo].[s_user] ([id])
GO

ALTER TABLE [dbo].[s_serial] CHECK CONSTRAINT [FK__s_serial__uc__30592A6F]
GO

ALTER TABLE [dbo].[s_serial]  WITH CHECK ADD  CONSTRAINT [CK__s_serial__2F650636] CHECK  (([min]>(0) AND  (cur>=min or cur=0) AND [max]>=[cur]))
GO

ALTER TABLE [dbo].[s_serial] CHECK CONSTRAINT [CK__s_serial__2F650636]
GO

----TẠO LẠI BẢNG s_seou


ALTER TABLE [dbo].[s_seou] DROP CONSTRAINT [s_seou_fk2]
GO

ALTER TABLE [dbo].[s_seou] DROP CONSTRAINT [s_seou_fk1]
GO

/****** Object:  Table [dbo].[s_seou]    Script Date: 18/05/2020 10:36:00 AM ******/


/****** Object:  Table [dbo].[s_seou]    Script Date: 18/05/2020 10:36:00 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[s_seou](
	[se] [int] NOT NULL,
	[ou] [int] NOT NULL,
 CONSTRAINT [s_seou_pk] PRIMARY KEY CLUSTERED 
(
	[se] ASC,
	[ou] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[s_seou]  WITH CHECK ADD  CONSTRAINT [s_seou_fk1] FOREIGN KEY([ou])
REFERENCES [dbo].[s_ou] ([id])
GO

ALTER TABLE [dbo].[s_seou] CHECK CONSTRAINT [s_seou_fk1]
GO

ALTER TABLE [dbo].[s_seou]  WITH CHECK ADD  CONSTRAINT [s_seou_fk2] FOREIGN KEY([se])
REFERENCES [dbo].[s_serial] ([id])
GO

ALTER TABLE [dbo].[s_seou] CHECK CONSTRAINT [s_seou_fk2]
GO

ALTER TABLE [dbo].[s_sys_logs] ADD [doc] nvarchar(max) NULL
GO

EXEC sp_addextendedproperty
'MS_Description', N'Thông tin dữ liệu ghi log, lưu dưới dạng json',
'SCHEMA', N'dbo',
'TABLE', N's_sys_logs',
'COLUMN', N'doc'

--Tăng độ rộng của trường người mua lên 500 ký tư
ALTER TABLE [dbo].[s_inv] DROP COLUMN [bname]
GO

ALTER TABLE [dbo].[s_inv] ADD [bname] AS (CONVERT([nvarchar](500),json_value([doc],'$.bname')));

ALTER TABLE einv_deloitte.dbo.s_org ALTER COLUMN name nvarchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL GO
ALTER TABLE einv_deloitte.dbo.s_org ALTER COLUMN name_en nvarchar(500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL GO

----------------------------------bang tam phuc vu bao cao
CREATE TABLE [dbo].[s_report_tmp](
	[c_id] [bigint] NULL,
	[form] [nvarchar](50) NULL,
	[serial] [nvarchar](50) NULL,
	[min1] [int] NULL,
	[max1] [int] NULL,
	[min0] [int] NULL,
	[max0] [int] NULL,
	[maxu0] [int] NULL,
	[minu] [int] NULL,
	[maxu] [int] NULL,
	[cancel] [int] NULL,
	[clist] [varchar](max) NULL,
	[minc] [int] NULL,
	[maxc] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE INDEX s_report_tmp_id 
	ON dbo.[s_report_tmp] ([c_id])
GO
--------------------------

CREATE TABLE [dbo].[s_seusr] (
  [se] int  NOT NULL,
  [usrid] nvarchar(20) COLLATE SQL_Latin1_General_CP1_CI_AS  NOT NULL
)
GO

ALTER TABLE [dbo].[s_seusr] SET (LOCK_ESCALATION = TABLE)
GO

EXEC sp_addextendedproperty
'MS_Description', N'ID dải TBPH tham chiếu bảng s_serial',
'SCHEMA', N'dbo',
'TABLE', N's_seusr',
'COLUMN', N'se'
GO

EXEC sp_addextendedproperty
'MS_Description', N'User ID tham chiếu bảng s_user',
'SCHEMA', N'dbo',
'TABLE', N's_seusr',
'COLUMN', N'usrid'
GO

EXEC sp_addextendedproperty
'MS_Description', N'Bảng lưu thông tin phân quyền dải TBPH theo user',
'SCHEMA', N'dbo',
'TABLE', N's_seusr'
GO


-- ----------------------------
-- Primary Key structure for table s_seusr
-- ----------------------------
ALTER TABLE [dbo].[s_seusr] ADD CONSTRAINT [s_seusr_pk] PRIMARY KEY CLUSTERED ([se], [usrid])
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)  
ON [PRIMARY]
GO


-- ----------------------------
-- Foreign Keys structure for table s_seusr
-- ----------------------------
ALTER TABLE [dbo].[s_seusr] ADD CONSTRAINT [s_seusr_fk1] FOREIGN KEY ([usrid]) REFERENCES [dbo].[s_user] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

ALTER TABLE [dbo].[s_seusr] ADD CONSTRAINT [s_seusr_fk2] FOREIGN KEY ([se]) REFERENCES [dbo].[s_serial] ([id]) ON DELETE NO ACTION ON UPDATE NO ACTION
GO

------------------------------------------------
CREATE UNIQUE INDEX s_org_code_uk
   ON s_org (code)
   
ALTER TABLE dbo.s_ou ADD c0 varchar(255) NULL GO
ALTER TABLE dbo.s_ou ADD c1 varchar(255) NULL GO
ALTER TABLE dbo.s_ou ADD c2 varchar(255) NULL GO
ALTER TABLE dbo.s_ou ADD c3 varchar(255) NULL GO
ALTER TABLE dbo.s_ou ADD c4 varchar(255) NULL GO
ALTER TABLE dbo.s_ou ADD c5 varchar(255) NULL GO
ALTER TABLE dbo.s_ou ADD c6 varchar(255) NULL GO
ALTER TABLE dbo.s_ou ADD c7 varchar(255) NULL GO
ALTER TABLE dbo.s_ou ADD c8 varchar(255) NULL GO
ALTER TABLE dbo.s_ou ADD c9 varchar(255) NULL GO
   
ALTER TABLE dbo.s_sys_logs DROP CONSTRAINT DF_s_sys_logs_id GO

DROP SEQUENCE [dbo].[ISEQ_SYS_LOGS]
GO

CREATE SEQUENCE [dbo].[ISEQ_SYS_LOGS] 
 AS [int]
 START WITH 1
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 999
 CYCLE 
 NO CACHE 
GO

ALTER TABLE dbo.s_sys_logs ADD  DEFAULT replace(replace(replace(CONVERT([varchar],getdate(),(20)),'-',''),':',''),' ','')+replace(str(NEXT VALUE FOR [ISEQ_SYS_LOGS],(3)),' ','0') FOR id 
GO

/****** Object:  Table [dbo].[b_inv]    Script Date: 17/9/2020 9:41:28 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[b_inv](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[doc] [nvarchar](max) NOT NULL,
	[xml] [nvarchar](max) NULL,
	[idt]  AS (CONVERT([datetime],json_value([doc],'$.idt'))),
	[type]  AS (CONVERT([varchar](10),json_value([doc],'$.type'))),
	[form]  AS (CONVERT([varchar](11),json_value([doc],'$.form'))),
	[serial]  AS (CONVERT([varchar](8),json_value([doc],'$.serial'))),
	[seq]  AS (CONVERT([varchar](7),json_value([doc],'$.seq'))),
	[paym]  AS (CONVERT([varchar](100),json_value([doc],'$.paym'))),
	[curr]  AS (CONVERT([varchar](3),json_value([doc],'$.curr'))),
	[exrt]  AS (CONVERT([numeric](8,2),json_value([doc],'$.exrt'))),
	[sname]  AS (CONVERT([varchar](255),json_value([doc],'$.sname'))),
	[stax]  AS (CONVERT([varchar](14),json_value([doc],'$.stax'))),
	[saddr]  AS (CONVERT([varchar](255),json_value([doc],'$.saddr'))),
	[smail]  AS (CONVERT([varchar](255),json_value([doc],'$.smail'))),
	[stel]  AS (CONVERT([varchar](255),json_value([doc],'$.stel'))),
	[sacc]  AS (CONVERT([varchar](255),json_value([doc],'$.sacc'))),
	[sbank]  AS (CONVERT([varchar](255),json_value([doc],'$.sbank'))),
	[buyer]  AS (CONVERT([varchar](255),json_value([doc],'$.buyer'))),
	[bname]  AS (CONVERT([varchar](255),json_value([doc],'$.bname'))),
	[bcode]  AS (CONVERT([varchar](100),json_value([doc],'$.bcode'))),
	[btax]  AS (CONVERT([varchar](14),json_value([doc],'$.btax'))),
	[baddr]  AS (CONVERT([varchar](255),json_value([doc],'$.baddr'))),
	[bmail]  AS (CONVERT([varchar](255),json_value([doc],'$.bmail'))),
	[btel]  AS (CONVERT([varchar](255),json_value([doc],'$.btel'))),
	[bacc]  AS (CONVERT([varchar](255),json_value([doc],'$.bacc'))),
	[bbank]  AS (CONVERT([varchar](255),json_value([doc],'$.bbank'))),
	[note]  AS (CONVERT([varchar](255),json_value([doc],'$.note'))),
	[sumv]  AS (CONVERT([numeric](19,4),json_value([doc],'$.sumv'))),
	[vatv]  AS (CONVERT([numeric](19,4),json_value([doc],'$.vatv'))),
	[totalv]  AS (CONVERT([numeric](19,4),json_value([doc],'$.totalv'))),
	[ou]  AS (CONVERT([int],json_value([doc],'$.ou'))),
	[uc]  AS (CONVERT([varchar](20),json_value([doc],'$.uc'))),
	[paid]  AS (CONVERT([int],json_value([doc],'$.paid'))),
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[stax] ASC,
	[form] ASC,
	[serial] ASC,
	[seq] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [s_user] ADD [create_date] [datetime] CONSTRAINT df_create_date DEFAULT GETDATE();

ALTER TABLE [s_user] ADD [last_login] [datetime];

ALTER TABLE [dbo].[s_inv] ADD [vseq] AS TRY_CAST(ISNULL(case when TRY_CAST(JSON_VALUE ([doc], '$.seq') as nvarchar(18)) = '' then null else TRY_CAST(JSON_VALUE ([doc], '$.seq') as nvarchar(18)) end, CAST(id as nvarchar(18))) as nvarchar(18))
GO

/****** Object:  Index [S_INV_UID_01]    Script Date: 20/10/2020 11:28:44 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [S_INV_UID_01] ON [dbo].[s_inv]
(
	[STAX] ASC, 
	[FORM] ASC, 
	[SERIAL] ASC, 
	[VSEQ] ASC
)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];

CREATE TABLE [dbo].[s_invdoctemp](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[invid] [bigint] NOT NULL,
	[doc] [nvarchar](max) NOT NULL,
	[dt] [datetime] NOT NULL,
	[status] [int] NOT NULL,
	[xml] [nvarchar](max) NULL,
	[iserror] [int] NOT NULL,
	[error_desc] [nvarchar](max) NULL,
 CONSTRAINT [PRIMARY] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[s_invdoctemp] ADD  CONSTRAINT [DF_s_invdoctemp_dt]  DEFAULT (getdate()) FOR [dt]
GO

ALTER TABLE [dbo].[s_invdoctemp] ADD  CONSTRAINT [DF_s_invdoctemp_iserror]  DEFAULT ((0)) FOR [iserror]
GO

ALTER TABLE [s_user] ADD [local] [int] CONSTRAINT DF__s_user__local__45FE52CB DEFAULT 0 NOT NULL;

ALTER TABLE [s_user] ADD [change_pass_date] [datetime] DEFAULT getdate();

ALTER TABLE [s_user] ADD [login_number] [int] CONSTRAINT DF__s_user__login_nu__46F27704 DEFAULT 0 NOT NULL;

ALTER TABLE s_user ADD change_pass int DEFAULT 0 NOT NULL;

CREATE TABLE [dbo].[s_user_pass] (
[user_id] nvarchar(20) NOT NULL,
[pass] varchar(200) NOT NULL,
[change_date] datetime NOT NULL,
CONSTRAINT [PK__s_user_pass]
PRIMARY KEY CLUSTERED ([user_id] ASC, [pass] ASC, [change_date] ASC)
WITH ( PAD_INDEX = OFF,
FILLFACTOR = 100,
IGNORE_DUP_KEY = OFF,
STATISTICS_NORECOMPUTE = OFF,
ALLOW_ROW_LOCKS = ON,
ALLOW_PAGE_LOCKS = ON,
DATA_COMPRESSION = NONE )
 ON [PRIMARY],
CONSTRAINT [FK__s_user_pa__user_id]
FOREIGN KEY ([user_id])
REFERENCES [dbo].[s_user] ( [id] )
)
ON [PRIMARY];
GO
ALTER TABLE [dbo].[s_user_pass] SET (LOCK_ESCALATION = TABLE);
GO

/****** Object:  Table [dbo].[b_inv]    Script Date: 10/2/2022 12:10:28 PM ******/
ALTER TABLE [dbo].[s_inv] ADD [orddt] AS (CONVERT([datetime],json_value([doc],'$.orddt')))