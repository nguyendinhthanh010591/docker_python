CREATE TABLE `hennessy`.`s_xls_temp` (`id` VARCHAR(20) NULL, `ic` VARCHAR(50) NULL, `idt` datetime(3) NULL, `form` VARCHAR(11) NULL, `serial` VARCHAR(10) NULL, `seq` VARCHAR(8) NULL, `dt` datetime(3) DEFAULT getdate() NOT NULL);

CREATE TABLE `hennessy`.`s_inv_adj` (`id` BIGINT AUTO_INCREMENT NOT NULL, `inv_id` BIGINT NULL, `form` INT NULL, `serial` NVARCHAR(10) NULL, `stax` NVARCHAR(20) NULL, `type` INT NULL, `note` NVARCHAR(-1) NULL, `so_tb` NVARCHAR(50) NULL, `ngay_tb` date NULL, `createdate` datetime(3) NULL, `statuscqt` INT NULL, `msgcqt` NVARCHAR(-1) NULL, `listinv_id` BIGINT NULL, `th_type` INT NOT NULL, `seq` NVARCHAR(8) NULL, CONSTRAINT `PK__s_inv_ad__3213E83FA7E7893B` PRIMARY KEY (`id`));

CREATE TABLE `hennessy`.`s_list_inv_cancel` (`id` BIGINT AUTO_INCREMENT NOT NULL, `inv_id` INT NOT NULL, `doc` NVARCHAR(-1) NOT NULL, `invtype` INT NOT NULL, `ou` INT NOT NULL, `serial` NVARCHAR(6) NULL, `type` NVARCHAR(10) NULL, `idt` datetime(3) NULL, `cdt` datetime(3) NOT NULL, `listinv_id` BIGINT NULL, `stax` NVARCHAR(14) NULL, CONSTRAINT `PK__s_list_i__3213E83F0E4528B3` PRIMARY KEY (`id`));

CREATE TABLE `hennessy`.`s_seusr` (`se` INT NOT NULL, `usrid` NVARCHAR(20) NOT NULL, CONSTRAINT `s_seusr_pk` PRIMARY KEY (`se`, `usrid`)) COMMENT='B?ng luu thông tin phân quy?n d?i TBPH theo user';

ALTER TABLE `hennessy`.`s_seusr` COMMENT = 'B?ng luu thông tin phân quy?n d?i TBPH theo user';

CREATE TABLE `hennessy`.`s_listvalues` (`keyname` VARCHAR(60) NOT NULL, `keyvalue` NVARCHAR(-1) NOT NULL, `keytype` VARCHAR(30) NOT NULL);

CREATE TABLE `hennessy`.`info_taxpayer` (`id` numeric(18) AUTO_INCREMENT NOT NULL, `taxc` NVARCHAR(14) NOT NULL, `namecom` NVARCHAR(255) NULL, `type` NVARCHAR(10) NULL, `status` numeric(18) NULL, `effectdate` datetime(3) NULL, `managercomc` NVARCHAR(10) NULL, `chapter` NVARCHAR(10) NULL, `changedate` datetime(3) NULL, `economictype` NVARCHAR(10) NULL, `contbusires` NVARCHAR(10) NULL, `passport` NVARCHAR(20) NULL, `createdby` NVARCHAR(150) NOT NULL, `statusscan` numeric(18) NOT NULL, `messres` NVARCHAR(255) NOT NULL, `statuscoderes` numeric(18) NOT NULL, `createddate` datetime(3) NULL, `updateddate` datetime(3) NULL) AUTO_INCREMENT=0;

CREATE TABLE `hennessy`.`type_invoice` (`id` numeric(18) AUTO_INCREMENT NOT NULL, `name` NVARCHAR(255) NULL) AUTO_INCREMENT=0;

CREATE TABLE `hennessy`.`van_einv_listid_msg` (`id` BIGINT AUTO_INCREMENT NOT NULL, `pid` BIGINT NULL, `type` NVARCHAR(10) NULL, `status_scan` INT NULL, `createdate` datetime(3) DEFAULT getdate() NULL, `sendvandate` datetime(3) NULL, `msg` NVARCHAR(-1) NULL, `xml` NVARCHAR(-1) NULL, `parent_id` NCHAR(10) NULL);

CREATE TABLE `hennessy`.`s_inv_sum` (`idt` date NOT NULL, `stax` VARCHAR(20) NOT NULL, `type` VARCHAR(6) NOT NULL, `form` VARCHAR(11) NOT NULL, `serial` VARCHAR(8) NULL, `i0` numeric(18) NULL, `i1` numeric(18) NULL, `i2` numeric(18) NULL, `i3` numeric(18) NULL, `i4` numeric(18) NULL);

CREATE TABLE `hennessy`.`b_inv` (`id` INT AUTO_INCREMENT NOT NULL, `doc` NVARCHAR(-1) NOT NULL, `xml` NVARCHAR(-1) NULL, `idt` datetime(3) NULL, `type` VARCHAR(10) NULL, `form` VARCHAR(11) NULL, `serial` VARCHAR(8) NULL, `seq` VARCHAR(7) NULL, `paym` VARCHAR(100) NULL, `curr` VARCHAR(3) NULL, `exrt` numeric(8, 2) NULL, `sname` VARCHAR(255) NULL, `stax` VARCHAR(14) NULL, `saddr` VARCHAR(255) NULL, `smail` VARCHAR(255) NULL, `stel` VARCHAR(255) NULL, `sacc` VARCHAR(255) NULL, `sbank` VARCHAR(255) NULL, `buyer` VARCHAR(255) NULL, `bname` VARCHAR(255) NULL, `bcode` VARCHAR(100) NULL, `btax` VARCHAR(14) NULL, `baddr` VARCHAR(255) NULL, `bmail` VARCHAR(255) NULL, `btel` VARCHAR(255) NULL, `bacc` VARCHAR(255) NULL, `bbank` VARCHAR(255) NULL, `note` VARCHAR(255) NULL, `sumv` numeric(19, 4) NULL, `vatv` numeric(19, 4) NULL, `totalv` numeric(19, 4) NULL, `ou` INT NULL, `uc` VARCHAR(20) NULL, `paid` INT NULL, `month` VARCHAR(100) NULL, `year` VARCHAR(100) NULL, `category` VARCHAR(100) NULL, CONSTRAINT `PK__b_inv__3213E83FFD9C2485` PRIMARY KEY (`id`));

CREATE TABLE `hennessy`.`s_dbs_mail_seq` (`id` INT AUTO_INCREMENT NOT NULL, `seq_date` INT NULL, `used_number` INT DEFAULT 0 NULL, CONSTRAINT `PK__s_dbs_ma__3213E83F46956F3E` PRIMARY KEY (`id`));

CREATE TABLE `hennessy`.`s_statement` (`id` BIGINT AUTO_INCREMENT NOT NULL, `type` NVARCHAR(15) NULL, `formname` NVARCHAR(100) NULL, `regtype` INT NULL, `sname` NVARCHAR(400) NULL, `stax` NVARCHAR(14) NULL, `taxoname` NVARCHAR(100) NULL, `taxo` NVARCHAR(5) NULL, `contactname` NVARCHAR(50) NULL, `contactaddr` NVARCHAR(400) NULL, `contactemail` NVARCHAR(50) NULL, `contactphone` NVARCHAR(20) NULL, `place` NVARCHAR(50) NULL, `createdt` datetime(3) NULL, `hascode` INT DEFAULT 0 NULL, `sendtype` INT NULL, `dtsendtype` NVARCHAR(20) NULL, `invtype` NVARCHAR(100) NULL, `doc` NVARCHAR(-1) NULL, `status` INT DEFAULT 1 NOT NULL, `cqtstatus` INT NULL, `vdt` datetime(3) NULL, `cqtmess` NVARCHAR(255) NULL, `xml` NVARCHAR(-1) NULL, `statustvan` INT DEFAULT 0 NOT NULL, `xml_received` NVARCHAR(-1) NULL, `xml_accepted` NVARCHAR(-1) NULL, `paxoname` NVARCHAR(100) NULL, `paxo` NVARCHAR(5) NULL, `json_received` NVARCHAR(-1) NULL, `json_accepted` NVARCHAR(-1) NULL, `uc` NVARCHAR(20) NULL, CONSTRAINT `s_statement_pk` PRIMARY KEY (`id`));

CREATE TABLE `hennessy`.`temp_taxpayer` (`id` numeric(18) AUTO_INCREMENT NOT NULL, `taxc` NVARCHAR(14) NOT NULL, `createdby` NVARCHAR(150) NOT NULL, `statusscan` numeric(18) NOT NULL, `creaateddate` datetime(3) NULL, `updateddate` datetime(3) NULL) AUTO_INCREMENT=0;

CREATE TABLE `hennessy`.`van_msg_out` (`id` numeric(18) AUTO_INCREMENT NOT NULL, `invid` NVARCHAR(36) NOT NULL, `xml2base64` NVARCHAR(-1) NOT NULL, `createdby` NVARCHAR(150) NOT NULL, `statustvan` numeric(18) NULL, `createddate` datetime(3) NULL, `senddate` datetime(3) DEFAULT getdate() NULL, `idtypeinv` numeric(18) NOT NULL, `messsendcode` NVARCHAR(10) NULL) AUTO_INCREMENT=0;

CREATE TABLE `hennessy`.`s_trans` (`ID` numeric(20) AUTO_INCREMENT NOT NULL, `refNo` NVARCHAR(50) NULL COMMENT 'số giao dịch', `valueDate` date NULL COMMENT 'ngay giao dich', `customerID` NVARCHAR(50) NULL, `customerName` NVARCHAR(500) NULL, `taxCode` NVARCHAR(50) NULL, `customerAddr` NVARCHAR(500) NULL, `isSpecial` VARCHAR(1) NULL COMMENT 'loai khach hang', `curr` VARCHAR(10) NULL, `exrt` FLOAT(53) NULL, `vrt` NVARCHAR(50) NULL, `chargeAmount` NVARCHAR(50) NULL, `vcontent` NVARCHAR(500) NULL, `amount` DECIMAL(18, 2) NULL, `vat` DECIMAL(18, 2) NULL, `total` DECIMAL(18, 2) NULL, `create_date` datetime(3) DEFAULT getdate() NULL, `tranID` NVARCHAR(100) NULL, `trantype` numeric(1) NULL COMMENT '0_phi, 1_lai', `trancol` numeric(1) NULL COMMENT 'Loại gom(0_thang,1_ngay,2_xuatle)', `status` numeric(1) NULL, `customerAcc` NVARCHAR(50) NULL, `inv_id` numeric(18) NULL, `inv_date` date NULL, `ma_nv` NVARCHAR(50) NULL, `ma_ks` NVARCHAR(50) NULL, `last_update` datetime(3) NULL, `segment` NVARCHAR(50) NULL COMMENT 'phân khuc khach hang', CONSTRAINT `PK_s_trans` PRIMARY KEY (`ID`));

CREATE TABLE `hennessy`.`van_history` (`id` numeric(36) AUTO_INCREMENT NOT NULL, `type_invoice` numeric(18) NOT NULL COMMENT '1: Bảng tổng hợp hóa đơn, 2: Hóa đơn không mã, 3: Hóa đơn có mã, 4: Thông báo sai xót, 5: Tờ khai đăng ký', `id_ref` BIGINT NULL, `status` numeric(18) NULL, `datetime_trans` datetime(3) NULL, `description` NVARCHAR(-1) NULL);

CREATE TABLE `hennessy`.`van_msg_out_dtl` (`id` numeric(18) AUTO_INCREMENT NOT NULL, `iddatatvan` numeric(18) NOT NULL, `invid` NVARCHAR(36) NOT NULL, `createdby` NVARCHAR(150) NOT NULL, `statustvan` numeric(18) NULL, `statusres` numeric(18) NULL, `messsendcode` NVARCHAR(10) NULL, `messsend` NVARCHAR(-1) NULL, `messres` NVARCHAR(255) NULL, `messrescode` NVARCHAR(10) NULL, `senddate` datetime(3) NULL, `resdate` datetime(3) NULL, `transid` NVARCHAR(225) NULL, `idtypeinv` numeric(18) NOT NULL, `status_response` INT DEFAULT 0 NULL, `datecheckmsg` datetime(3) DEFAULT getdate() NULL) AUTO_INCREMENT=0;

CREATE TABLE `van_msg_in_dtl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `receivejson` json NOT NULL,
  `taxc` varchar(14) DEFAULT NULL,
  `namenotice` varchar(500) CHARACTER SET utf8mb4 GENERATED ALWAYS AS (if((json_unquote(json_extract(`receivejson`,'$.tenTBao')) = 'null'),NULL,json_unquote(json_extract(`receivejson`,'$.tenTBao')))) VIRTUAL,
  `typenotice` varchar(255) CHARACTER SET utf8mb4 GENERATED ALWAYS AS (if((json_unquote(json_extract(`receivejson`,'$.loaiTBao')) = 'null'),NULL,json_unquote(json_extract(`receivejson`,'$.loaiTBao')))) VIRTUAL,
  `contentnotice` longtext CHARACTER SET utf8mb4 GENERATED ALWAYS AS (if((json_unquote(json_extract(`receivejson`,'$.ndungTBao')) = 'null'),NULL,json_unquote(json_extract(`receivejson`,'$.ndungTBao')))) VIRTUAL,
  `createddtnotice` datetime GENERATED ALWAYS AS (if((json_unquote(json_extract(`receivejson`,'$.ngayTaoTBao')) = 'null'),NULL,json_unquote(json_extract(`receivejson`,'$.ngayTaoTBao')))) VIRTUAL,
  `createddate` datetime DEFAULT CURRENT_TIMESTAMP,
  `status_scan` decimal(18,0) DEFAULT NULL,
  `updateddate` datetime(3) DEFAULT NULL,
  `transid` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `van_msg_in_dtl_status_scan_IDX` (`status_scan`) USING BTREE,
  KEY `van_msg_in_dtl_transid_IDX` (`transid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16071 DEFAULT CHARSET=utf8;

CREATE TABLE `hennessy`.`sign_xml` (`id` numeric(18) AUTO_INCREMENT NOT NULL, `tax_code` NVARCHAR(14) NULL, `type_xml` NVARCHAR(20) NULL, `xmlOriginal` NVARCHAR(-1) NOT NULL, `xmlSigned` NVARCHAR(-1) NULL, `createddate` datetime(3) NULL, `updateddate` datetime(3) NULL) AUTO_INCREMENT=0;

CREATE TABLE `hennessy`.`s_list_inv_id` (`id` BIGINT NULL, `inv_id` BIGINT NULL, `loai` VARCHAR(50) NULL);

CREATE TABLE `hennessy`.`s_wrongnotice_process` (`id` INT AUTO_INCREMENT NOT NULL, `ou` INT NULL, `id_wn` INT NULL, `id_inv` NVARCHAR(-1) NULL, `doc` NVARCHAR(-1) NULL, `xml` NVARCHAR(-1) NULL, `dt` datetime(3) DEFAULT getdate() NOT NULL, `status` NVARCHAR(15) NOT NULL, `status_cqt` INT NULL, `xml_received` NVARCHAR(-1) NULL, `json_received` NVARCHAR(-1) NULL, `xml_accepted` NVARCHAR(-1) NULL, `json_accepted` NVARCHAR(-1) NULL, `dien_giai` NVARCHAR(-1) NULL, `taxc` NVARCHAR(14) NULL, `form` NVARCHAR(14) NULL, `serial` NVARCHAR(14) NULL, `seq` NVARCHAR(14) NULL, `type` NVARCHAR(14) NULL, CONSTRAINT `s_wrongnotice_process_pk` PRIMARY KEY (`id`));

CREATE TABLE `hennessy`.`s_user_pass` (`user_id` NVARCHAR(20) NOT NULL, `pass` VARCHAR(200) NOT NULL, `change_date` datetime(3) NOT NULL, CONSTRAINT `PK__s_user_p__518C386C17C39B87` PRIMARY KEY (`user_id`, `pass`));

CREATE TABLE `hennessy`.`van_msg_in` (`id` numeric(18) AUTO_INCREMENT NOT NULL, `receive_arr_json` NVARCHAR(-1) NOT NULL, `status_scan` numeric(18) NULL COMMENT '0: not scan, pending: 3, success: 1, error: 2', `createddate` datetime(3) NULL, `updateddate` datetime(3) NULL, `createby` NVARCHAR(50) NULL, `err_msg` NVARCHAR(-1) NULL) AUTO_INCREMENT=0;

CREATE TABLE `hennessy`.`s_list_inv` (`id` BIGINT NOT NULL, `listinv_code` NVARCHAR(15) NULL, `s_tax` NVARCHAR(14) NOT NULL, `ou` INT NOT NULL, `created_date` datetime(3) DEFAULT getdate() NOT NULL, `created_by` NVARCHAR(50) NULL, `approve_date` datetime(3) NULL, `approve_by` NVARCHAR(20) NULL, `s_name` NVARCHAR(400) NOT NULL, `item_type` NVARCHAR(20) NOT NULL, `listinvoice_num` NVARCHAR(5) NULL, `period_type` NVARCHAR(1) NOT NULL, `period` NVARCHAR(10) NOT NULL, `status` INT DEFAULT 0 NOT NULL, `doc` NVARCHAR(-1) NULL, `xml` NVARCHAR(-1) NULL, `times` INT DEFAULT 0 NOT NULL, `status_received` INT NULL, `xml_received` NVARCHAR(-1) NULL, `json_received` NVARCHAR(-1) NULL, `edittimes` INT NULL, `error_msg_van` NVARCHAR(-1) NULL, CONSTRAINT `s_list_inv_pk` PRIMARY KEY (`id`));

ALTER TABLE `hennessy`.`s_inv` ADD CONSTRAINT `FK__s_inv__uc__47A6A41B` FOREIGN KEY (`uc`) REFERENCES `hennessy`.`s_user` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE `hennessy`.`s_inv` ADD CONSTRAINT `UQ__s_inv__DDDFBCCD25D7EFEB` UNIQUE (`sec`);

ALTER TABLE `hennessy`.`b_inv` ADD CONSTRAINT `UQ__b_inv__B09EB31C78CA9B41` UNIQUE (`stax`, `form`, `serial`, `seq`);

ALTER TABLE `hennessy`.`s_inv_file` ADD CONSTRAINT `FK__s_inv_file__id__489AC854` FOREIGN KEY (`id`) REFERENCES `hennessy`.`s_inv` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE `hennessy`.`s_role` ADD CONSTRAINT `s_role_fk01` FOREIGN KEY (`pid`) REFERENCES `hennessy`.`s_role` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE `hennessy`.`s_wrongnotice_process` ADD CONSTRAINT `UQ__s_wrongn__01487863BB3969F7` UNIQUE (`id_wn`);

ALTER TABLE `hennessy`.`s_user_pass` ADD CONSTRAINT `FK__s_user_pa__user___49CEE3AF` FOREIGN KEY (`user_id`) REFERENCES `hennessy`.`s_user` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE `hennessy`.`s_seusr` ADD CONSTRAINT `s_seusr_fk1` FOREIGN KEY (`usrid`) REFERENCES `hennessy`.`s_user` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE `hennessy`.`s_seusr` ADD CONSTRAINT `s_seusr_fk2` FOREIGN KEY (`se`) REFERENCES `hennessy`.`s_serial` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE `hennessy`.`s_inv` ADD CONSTRAINT `FK__s_inv__ou__46B27FE2` FOREIGN KEY (`ou`) REFERENCES `hennessy`.`s_ou` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE `hennessy`.`s_group_role` ADD `STATUS` VARCHAR(1) NULL;

ALTER TABLE `hennessy`.`s_role` ADD `pid` INT NULL;

ALTER TABLE `hennessy`.`s_role` ADD `sort` NVARCHAR(10) NULL;

ALTER TABLE `hennessy`.`s_role` ADD `active` TINYINT(3) NULL;

ALTER TABLE `hennessy`.`s_role` ADD `menu_detail` NVARCHAR(150) NULL;

ALTER TABLE `hennessy`.`s_user` ADD `local` INT DEFAULT 0 NOT NULL;

ALTER TABLE `hennessy`.`s_user` ADD `change_pass_date` datetime(3) DEFAULT getdate() NULL;

ALTER TABLE `hennessy`.`s_user` ADD `login_number` INT DEFAULT 0 NOT NULL;

ALTER TABLE `hennessy`.`s_user` ADD `change_pass` INT DEFAULT 0 NOT NULL;

ALTER TABLE `hennessy`.`s_user` ADD `update_date` datetime(3) NULL;

ALTER TABLE `hennessy`.`s_serial` ADD `ut` datetime(3) DEFAULT getdate() NOT NULL;

ALTER TABLE `hennessy`.`s_serial` ADD `intg` INT DEFAULT 2 NOT NULL;

ALTER TABLE `hennessy`.`s_sys_logs` ADD `ip` VARCHAR(50) NULL;

ALTER TABLE `hennessy`.`s_serial` ADD `alertleft` INT DEFAULT 0 NOT NULL;

ALTER TABLE `hennessy`.`s_serial` ADD `leftwarn` INT DEFAULT 1 NULL;

ALTER TABLE `hennessy`.`s_serial` ADD `sendtype` INT NULL;

ALTER TABLE `hennessy`.`s_serial` ADD `invtype` INT NULL;

ALTER TABLE `hennessy`.`s_serial` ADD `degree_config` INT DEFAULT 119 NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `c0` VARCHAR(255) NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `c1` VARCHAR(255) NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `c2` VARCHAR(255) NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `c3` VARCHAR(255) NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `c4` VARCHAR(255) NULL;

ALTER TABLE `hennessy`.`s_org` ADD `vtaxc` NVARCHAR(14) NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `c5` VARCHAR(255) NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `c6` VARCHAR(255) NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `c7` VARCHAR(255) NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `c8` VARCHAR(255) NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `c9` VARCHAR(255) NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `smtpconf` NVARCHAR(-1) NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `filenameca` VARCHAR(255) NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `sct` TINYINT(3) DEFAULT 0 NOT NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `sc` TINYINT(3) DEFAULT 0 NOT NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `bcc` VARCHAR(100) NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `qrc` TINYINT(3) DEFAULT 0 NOT NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `na` TINYINT(3) DEFAULT 2 NOT NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `nq` TINYINT(3) DEFAULT 2 NOT NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `np` TINYINT(3) DEFAULT 5 NOT NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `itype` VARCHAR(255) DEFAULT '01GTKT' NOT NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `ts` TINYINT(3) DEFAULT 1 NOT NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `dsignfrom` datetime(3) NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `dsignto` datetime(3) NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `degree_config` TINYINT(3) DEFAULT 119 NOT NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `cfg_ihasdocid` VARCHAR(45) NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `cfg_uselist` VARCHAR(45) DEFAULT '' NOT NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `hascode` TINYINT(3) DEFAULT 0 NOT NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `dsignserial` NVARCHAR(100) NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `dsignsubject` NVARCHAR(200) NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `dsignissuer` NVARCHAR(200) NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `receivermail` NVARCHAR(200) NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `adsconf` NVARCHAR(-1) NULL;

ALTER TABLE `hennessy`.`s_ou` ADD `place` NVARCHAR(255) NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `dds_sent` INT DEFAULT 0 NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `dds_stt` INT NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `dds` INT NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `file_name` NVARCHAR(100) NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `sendmaildate` datetime(3) NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `sendsmsdate` datetime(3) NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `vseq` NVARCHAR(18) NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `sendmailstatus` INT NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `sendsmsstatus` INT NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `sendmailnum` INT NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `sendsmsnum` INT NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `bcode` NVARCHAR(500) NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `taxocode` NVARCHAR(20) NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `invlistdt` datetime(3) NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `discountamt` numeric(18) NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `ordno` NVARCHAR(500) NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `whsfr` NVARCHAR(500) NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `whsto` NVARCHAR(500) NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `edt` datetime(3) NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `status_received` INT NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `xml_received` NVARCHAR(-1) NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `hascode` numeric(38) NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `invtype` NVARCHAR(500) NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `invlistnum` NVARCHAR(50) NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `period_type` NVARCHAR(1) NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `list_invid` BIGINT NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `status_tbss` INT DEFAULT 0 NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `error_msg_van` NVARCHAR(-1) NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `vehic` NVARCHAR(500) NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `inv_adj` BIGINT NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `period` NVARCHAR(10) NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `th_type` INT NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `wno_adj` INT NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `wnadjtype` INT NULL;

ALTER TABLE `hennessy`.`s_inv` ADD `list_invid_bx` BIGINT NULL;

ALTER TABLE `hennessy`.`s_manager` DROP FOREIGN KEY `FK__s_manager__user___498EEC8D`;

ALTER TABLE `hennessy`.`s_manager` ADD CONSTRAINT `FK__s_manager__user___498EEC8D` FOREIGN KEY (`user_id`) REFERENCES `hennessy`.`s_user` (`id`);

ALTER TABLE `hennessy`.`s_member` DROP FOREIGN KEY `FK__s_member__role_i__2116E6DF`;

ALTER TABLE `hennessy`.`s_member` ADD CONSTRAINT `FK__s_member__role_i__2116E6DF` FOREIGN KEY (`role_id`) REFERENCES `hennessy`.`s_role` (`id`);

ALTER TABLE `hennessy`.`s_member` DROP FOREIGN KEY `FK__s_member__user_i__4B7734FF`;

ALTER TABLE `hennessy`.`s_member` ADD CONSTRAINT `FK__s_member__user_i__4B7734FF` FOREIGN KEY (`user_id`) REFERENCES `hennessy`.`s_user` (`id`);

ALTER TABLE `hennessy`.`s_ou` DROP FOREIGN KEY `FK__s_ou__pid__4C6B5938`;

ALTER TABLE `hennessy`.`s_ou` ADD CONSTRAINT `FK__s_ou__pid__4C6B5938` FOREIGN KEY (`pid`) REFERENCES `hennessy`.`s_ou` (`id`);

ALTER TABLE `hennessy`.`s_serial` DROP FOREIGN KEY `FK__s_serial__uc__30592A6F`;

ALTER TABLE `hennessy`.`s_serial` ADD CONSTRAINT `FK__s_serial__uc__30592A6F` FOREIGN KEY (`uc`) REFERENCES `hennessy`.`s_user` (`id`);

ALTER TABLE `hennessy`.`s_user` DROP FOREIGN KEY `FK__s_user__ou__531856C7`;

ALTER TABLE `hennessy`.`s_user` ADD CONSTRAINT `FK__s_user__ou__531856C7` FOREIGN KEY (`ou`) REFERENCES `hennessy`.`s_ou` (`id`);

ALTER TABLE `hennessy`.`s_seou` DROP FOREIGN KEY `s_seou_fk1`;

ALTER TABLE `hennessy`.`s_seou` ADD CONSTRAINT `s_seou_fk1` FOREIGN KEY (`ou`) REFERENCES `hennessy`.`s_ou` (`id`);

ALTER TABLE `hennessy`.`s_seou` DROP FOREIGN KEY `s_seou_fk2`;

ALTER TABLE `hennessy`.`s_seou` ADD CONSTRAINT `s_seou_fk2` FOREIGN KEY (`se`) REFERENCES `hennessy`.`s_serial` (`id`);

ALTER TABLE `hennessy`.`s_cat` COMMENT = 'B?ng luu thông tin danh m?c';

ALTER TABLE `hennessy`.`s_invd` COMMENT = 'B?ng chi ti?t hóa don';

ALTER TABLE `hennessy`.`s_sys_logs` COMMENT = 'B?ng luu log audit thao tác c?a NSD APP, API';

ALTER TABLE `hennessy`.`s_group` MODIFY `APPROVE` VARCHAR(1);

ALTER TABLE `hennessy`.`s_group` ALTER `APPROVE` SET DEFAULT '1';

ALTER TABLE `hennessy`.`s_group` MODIFY `DES` NVARCHAR(200);

ALTER TABLE `hennessy`.`s_group` ALTER `DES` SET DEFAULT '1';

ALTER TABLE `hennessy`.`s_group_role` MODIFY `GROUP_ID` FLOAT(53);

ALTER TABLE `hennessy`.`s_group_user` MODIFY `GROUP_ID` FLOAT(53);

ALTER TABLE `hennessy`.`s_group` MODIFY `ID` INT;

ALTER TABLE `hennessy`.`s_invd` MODIFY `ID` DECIMAL(18);

ALTER TABLE `hennessy`.`s_invd` ALTER `ID` SET DEFAULT NEXT VALUE FOR [ISEQ_INVD];

ALTER TABLE `hennessy`.`s_invd` MODIFY `INV_ID` DECIMAL(18);

ALTER TABLE `hennessy`.`s_group` MODIFY `NAME` NVARCHAR(200);

ALTER TABLE `hennessy`.`s_group` MODIFY `OU` INT;

ALTER TABLE `hennessy`.`s_invd` MODIFY `REF_NO` VARCHAR(50);

ALTER TABLE `hennessy`.`s_group_role` MODIFY `ROLE_ID` VARCHAR(20);

ALTER TABLE `hennessy`.`s_group` MODIFY `STATUS` VARCHAR(1);

ALTER TABLE `hennessy`.`s_group_user` MODIFY `USER_ID` VARCHAR(20);

ALTER TABLE `hennessy`.`s_refcode` MODIFY `abbreviation` VARCHAR(-1);

ALTER TABLE `hennessy`.`s_org` MODIFY `acc` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_ou` MODIFY `acc` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_sys_logs` MODIFY `action` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_org` MODIFY `addr` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_ou` MODIFY `addr` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_inv` MODIFY `adjdes` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_inv` MODIFY `adjseq` NVARCHAR(26);

ALTER TABLE `hennessy`.`s_inv` MODIFY `adjtyp` TINYINT(3);

ALTER TABLE `hennessy`.`s_dcm` MODIFY `atr` NVARCHAR(4000);

ALTER TABLE `hennessy`.`s_f_fld` MODIFY `atr` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_inv` MODIFY `bacc` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_inv` MODIFY `baddr` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_org` MODIFY `bank` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_ou` MODIFY `bank` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_inv` MODIFY `bbank` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_inv` MODIFY `bmail` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_inv` MODIFY `bname` NVARCHAR(500);

ALTER TABLE `hennessy`.`s_inv` MODIFY `btax` NVARCHAR(14);

ALTER TABLE `hennessy`.`s_inv` MODIFY `btel` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_inv` MODIFY `buyer` NVARCHAR(500);

ALTER TABLE `hennessy`.`s_inv` MODIFY `c0` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_org` MODIFY `c0` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_inv` MODIFY `c1` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_org` MODIFY `c1` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_inv` MODIFY `c2` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_org` MODIFY `c2` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_inv` MODIFY `c3` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_org` MODIFY `c3` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_inv` MODIFY `c4` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_org` MODIFY `c4` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_inv` MODIFY `c5` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_org` MODIFY `c5` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_inv` MODIFY `c6` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_org` MODIFY `c6` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_inv` MODIFY `c7` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_org` MODIFY `c7` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_inv` MODIFY `c8` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_org` MODIFY `c8` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_inv` MODIFY `c9` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_org` MODIFY `c9` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_report_tmp` MODIFY `c_id` BIGINT;

ALTER TABLE `hennessy`.`s_report_tmp` MODIFY `cancel` INT;

ALTER TABLE `hennessy`.`s_inv` MODIFY `cde` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_inv` MODIFY `cdt` datetime(3);

ALTER TABLE `hennessy`.`s_inv` MODIFY `cid` VARCHAR(3000);

ALTER TABLE `hennessy`.`s_report_tmp` MODIFY `clist` VARCHAR(-1);

ALTER TABLE `hennessy`.`s_gns` MODIFY `code` NVARCHAR(30);

ALTER TABLE `hennessy`.`s_org` MODIFY `code` NVARCHAR(100);

ALTER TABLE `hennessy`.`s_ou` MODIFY `code` NVARCHAR(20);

ALTER TABLE `hennessy`.`s_role` MODIFY `code` NVARCHAR(100);

ALTER TABLE `hennessy`.`s_role` MODIFY `code` NVARCHAR(100) NULL;

ALTER TABLE `hennessy`.`s_user` MODIFY `code` NVARCHAR(20);

ALTER TABLE `hennessy`.`s_inv_file` MODIFY `content` VARCHAR(-1);

ALTER TABLE `hennessy`.`s_user` MODIFY `create_date` datetime(3);

ALTER TABLE `hennessy`.`s_user` ALTER `create_date` SET DEFAULT getdate();

ALTER TABLE `hennessy`.`s_ex` MODIFY `cur` NVARCHAR(3);

ALTER TABLE `hennessy`.`s_serial` MODIFY `cur` numeric(8);

ALTER TABLE `hennessy`.`s_inv` MODIFY `curr` NVARCHAR(3);

ALTER TABLE `hennessy`.`s_inv` MODIFY `cvt` TINYINT(3);

ALTER TABLE `hennessy`.`s_cat` MODIFY `des` NVARCHAR(100);

ALTER TABLE `hennessy`.`s_serial` MODIFY `des` NVARCHAR(200);

ALTER TABLE `hennessy`.`s_org` MODIFY `dist` NVARCHAR(5);

ALTER TABLE `hennessy`.`s_ou` MODIFY `dist` NVARCHAR(5);

ALTER TABLE `hennessy`.`s_inv` MODIFY `doc` NVARCHAR(-1);

ALTER TABLE `hennessy`.`s_invdoctemp` MODIFY `doc` NVARCHAR(-1);

ALTER TABLE `hennessy`.`s_sys_logs` MODIFY `doc` NVARCHAR(-1);

ALTER TABLE `hennessy`.`s_refcode` MODIFY `domain` VARCHAR(-1);

ALTER TABLE `hennessy`.`s_ex` MODIFY `dt` datetime(3);

ALTER TABLE `hennessy`.`s_ex` ALTER `dt` SET DEFAULT getdate();

ALTER TABLE `hennessy`.`s_inv` MODIFY `dt` datetime(3);

ALTER TABLE `hennessy`.`s_inv` ALTER `dt` SET DEFAULT getdate();

ALTER TABLE `hennessy`.`s_invdoctemp` MODIFY `dt` datetime(3);

ALTER TABLE `hennessy`.`s_invdoctemp` ALTER `dt` SET DEFAULT getdate();

ALTER TABLE `hennessy`.`s_serial` MODIFY `dt` datetime(3);

ALTER TABLE `hennessy`.`s_serial` ALTER `dt` SET DEFAULT getdate();

ALTER TABLE `hennessy`.`s_sys_logs` MODIFY `dt` datetime(3);

ALTER TABLE `hennessy`.`s_dcm` MODIFY `dtl` TINYINT(3);

ALTER TABLE `hennessy`.`s_sys_logs` MODIFY `dtl` NVARCHAR(1000);

ALTER TABLE `hennessy`.`s_invdoctemp` MODIFY `error_desc` NVARCHAR(-1);

ALTER TABLE `hennessy`.`s_inv` MODIFY `exrt` numeric(8, 2);

ALTER TABLE `hennessy`.`s_org` MODIFY `fadd` NVARCHAR(500);

ALTER TABLE `hennessy`.`s_ou` MODIFY `fadd` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_serial` MODIFY `fd` datetime(3);

ALTER TABLE `hennessy`.`s_f_fld` MODIFY `feature` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_f_fld` MODIFY `fld_id` NVARCHAR(30);

ALTER TABLE `hennessy`.`s_f_fld` MODIFY `fld_name` NVARCHAR(30);

ALTER TABLE `hennessy`.`s_f_fld` MODIFY `fld_typ` NVARCHAR(50);

ALTER TABLE `hennessy`.`s_f_fld` MODIFY `fld_typ` NVARCHAR(50) NOT NULL;

ALTER TABLE `hennessy`.`s_sys_logs` MODIFY `fnc_id` NVARCHAR(30);

ALTER TABLE `hennessy`.`s_f_fld` MODIFY `fnc_name` NVARCHAR(30);

ALTER TABLE `hennessy`.`s_sys_logs` MODIFY `fnc_name` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_f_fld` MODIFY `fnc_url` NVARCHAR(15);

ALTER TABLE `hennessy`.`s_sys_logs` MODIFY `fnc_url` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_inv` MODIFY `form` NVARCHAR(11);

ALTER TABLE `hennessy`.`s_report_tmp` MODIFY `form` NVARCHAR(50);

ALTER TABLE `hennessy`.`s_serial` MODIFY `form` NVARCHAR(11);

ALTER TABLE `hennessy`.`s_refcode` MODIFY `highval` VARCHAR(-1);

ALTER TABLE `hennessy`.`s_inv` MODIFY `ic` NVARCHAR(36);

ALTER TABLE `hennessy`.`s_cat` MODIFY `id` INT;

ALTER TABLE `hennessy`.`s_dcd` MODIFY `id` NVARCHAR(30);

ALTER TABLE `hennessy`.`s_dcm` MODIFY `id` INT;

ALTER TABLE `hennessy`.`s_ex` MODIFY `id` INT;

ALTER TABLE `hennessy`.`s_f_fld` MODIFY `id` INT;

ALTER TABLE `hennessy`.`s_gns` MODIFY `id` INT;

ALTER TABLE `hennessy`.`s_inv` MODIFY `id` BIGINT;

ALTER TABLE `hennessy`.`s_inv_file` MODIFY `id` BIGINT;

ALTER TABLE `hennessy`.`s_invdoctemp` MODIFY `id` BIGINT;

ALTER TABLE `hennessy`.`s_loc` MODIFY `id` NVARCHAR(7);

ALTER TABLE `hennessy`.`s_org` MODIFY `id` INT;

ALTER TABLE `hennessy`.`s_ou` MODIFY `id` INT;

ALTER TABLE `hennessy`.`s_role` MODIFY `id` INT;

ALTER TABLE `hennessy`.`s_serial` MODIFY `id` INT;

ALTER TABLE `hennessy`.`s_sys_logs` MODIFY `id` DECIMAL(18);

ALTER TABLE `hennessy`.`s_sys_logs` ALTER `id` SET DEFAULT replace(replace(replace(CONVERT([varchar],getdate(),(20)),'-',''),':',''),' ','')+replace(str(NEXT VALUE FOR [ISEQ_SYS_LOGS],(3)),' ','0');

ALTER TABLE `hennessy`.`s_taxo` MODIFY `id` NVARCHAR(5);

ALTER TABLE `hennessy`.`s_user` MODIFY `id` NVARCHAR(20);

ALTER TABLE `hennessy`.`s_xls` MODIFY `id` INT;

ALTER TABLE `hennessy`.`s_inv` MODIFY `idt` datetime(3);

ALTER TABLE `hennessy`.`s_dcm` MODIFY `idx` TINYINT(3);

ALTER TABLE `hennessy`.`s_invdoctemp` MODIFY `invid` BIGINT;

ALTER TABLE `hennessy`.`s_invdoctemp` MODIFY `iserror` INT;

ALTER TABLE `hennessy`.`s_dcm` MODIFY `itype` NVARCHAR(6);

ALTER TABLE `hennessy`.`s_xls` MODIFY `itype` NVARCHAR(6);

ALTER TABLE `hennessy`.`s_xls` MODIFY `jc` NVARCHAR(20);

ALTER TABLE `hennessy`.`s_user` MODIFY `last_login` datetime(3);

ALTER TABLE `hennessy`.`s_dcm` MODIFY `lbl` NVARCHAR(100);

ALTER TABLE `hennessy`.`s_dcm` MODIFY `lbl_en` NVARCHAR(200);

ALTER TABLE `hennessy`.`s_refcode` MODIFY `lowval` VARCHAR(-1);

ALTER TABLE `hennessy`.`s_org` MODIFY `mail` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_ou` MODIFY `mail` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_user` MODIFY `mail` NVARCHAR(50);

ALTER TABLE `hennessy`.`s_serial` MODIFY `max` numeric(8);

ALTER TABLE `hennessy`.`s_report_tmp` MODIFY `max0` INT;

ALTER TABLE `hennessy`.`s_report_tmp` MODIFY `max1` INT;

ALTER TABLE `hennessy`.`s_report_tmp` MODIFY `maxc` INT;

ALTER TABLE `hennessy`.`s_report_tmp` MODIFY `maxu` INT;

ALTER TABLE `hennessy`.`s_report_tmp` MODIFY `maxu0` INT;

ALTER TABLE `hennessy`.`s_refcode` MODIFY `meaning` VARCHAR(-1);

ALTER TABLE `hennessy`.`s_serial` MODIFY `min` numeric(8);

ALTER TABLE `hennessy`.`s_report_tmp` MODIFY `min0` INT;

ALTER TABLE `hennessy`.`s_report_tmp` MODIFY `min1` INT;

ALTER TABLE `hennessy`.`s_report_tmp` MODIFY `minc` INT;

ALTER TABLE `hennessy`.`s_report_tmp` MODIFY `minu` INT;

ALTER TABLE `hennessy`.`s_sys_logs` MODIFY `msg_id` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_sys_logs` MODIFY `msg_status` INT;

ALTER TABLE `hennessy`.`s_ou` MODIFY `mst` NVARCHAR(14);

ALTER TABLE `hennessy`.`s_cat` MODIFY `name` NVARCHAR(100);

ALTER TABLE `hennessy`.`s_dcd` MODIFY `name` NVARCHAR(100);

ALTER TABLE `hennessy`.`s_gns` MODIFY `name` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_loc` MODIFY `name` NVARCHAR(100);

ALTER TABLE `hennessy`.`s_org` MODIFY `name` NVARCHAR(500);

ALTER TABLE `hennessy`.`s_ou` MODIFY `name` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_role` MODIFY `name` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_role` MODIFY `name` NVARCHAR(255) NULL;

ALTER TABLE `hennessy`.`s_taxo` MODIFY `name` NVARCHAR(100);

ALTER TABLE `hennessy`.`s_user` MODIFY `name` NVARCHAR(50);

ALTER TABLE `hennessy`.`s_org` MODIFY `name_en` NVARCHAR(500);

ALTER TABLE `hennessy`.`s_inv` MODIFY `note` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_inv` MODIFY `ou` INT;

ALTER TABLE `hennessy`.`s_seou` MODIFY `ou` INT;

ALTER TABLE `hennessy`.`s_user` MODIFY `ou` INT;

ALTER TABLE `hennessy`.`s_user` MODIFY `pass` VARCHAR(200);

ALTER TABLE `hennessy`.`s_ou` MODIFY `paxo` NVARCHAR(5);

ALTER TABLE `hennessy`.`s_inv` MODIFY `paym` NVARCHAR(10);

ALTER TABLE `hennessy`.`s_inv` MODIFY `pid` VARCHAR(3000);

ALTER TABLE `hennessy`.`s_loc` MODIFY `pid` NVARCHAR(7);

ALTER TABLE `hennessy`.`s_ou` MODIFY `pid` INT;

ALTER TABLE `hennessy`.`s_user` MODIFY `pos` NVARCHAR(50);

ALTER TABLE `hennessy`.`s_gns` MODIFY `price` numeric(17, 2);

ALTER TABLE `hennessy`.`s_serial` MODIFY `priority` INT;

ALTER TABLE `hennessy`.`s_org` MODIFY `prov` NVARCHAR(3);

ALTER TABLE `hennessy`.`s_ou` MODIFY `prov` NVARCHAR(3);

ALTER TABLE `hennessy`.`s_org` MODIFY `pwd` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_ou` MODIFY `pwd` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_sys_logs` MODIFY `r1` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_sys_logs` MODIFY `r2` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_sys_logs` MODIFY `r3` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_sys_logs` MODIFY `r4` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_member` MODIFY `role_id` INT;

ALTER TABLE `hennessy`.`s_inv` MODIFY `sacc` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_inv` MODIFY `saddr` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_inv` MODIFY `sbank` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_seou` MODIFY `se` INT;

ALTER TABLE `hennessy`.`s_inv` MODIFY `sec` NVARCHAR(10);

ALTER TABLE `hennessy`.`s_inv` MODIFY `seq` NVARCHAR(8);

ALTER TABLE `hennessy`.`s_ou` MODIFY `seq` TINYINT(3);

ALTER TABLE `hennessy`.`s_inv` MODIFY `serial` NVARCHAR(6);

ALTER TABLE `hennessy`.`s_report_tmp` MODIFY `serial` NVARCHAR(50);

ALTER TABLE `hennessy`.`s_serial` MODIFY `serial` NVARCHAR(6);

ALTER TABLE `hennessy`.`s_ou` MODIFY `sign` TINYINT(3);

ALTER TABLE `hennessy`.`s_inv` MODIFY `smail` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_inv` MODIFY `sname` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_sys_logs` MODIFY `src` NVARCHAR(20);

ALTER TABLE `hennessy`.`s_dcm` MODIFY `status` TINYINT(3);

ALTER TABLE `hennessy`.`s_f_fld` MODIFY `status` TINYINT(3);

ALTER TABLE `hennessy`.`s_inv` MODIFY `status` TINYINT(3);

ALTER TABLE `hennessy`.`s_invdoctemp` MODIFY `status` INT;

ALTER TABLE `hennessy`.`s_org` MODIFY `status` NVARCHAR(2);

ALTER TABLE `hennessy`.`s_ou` MODIFY `status` TINYINT(3);

ALTER TABLE `hennessy`.`s_serial` MODIFY `status` TINYINT(3);

ALTER TABLE `hennessy`.`s_inv` MODIFY `stax` NVARCHAR(14);

ALTER TABLE `hennessy`.`s_inv` MODIFY `stel` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_inv` MODIFY `sum` numeric(17, 2);

ALTER TABLE `hennessy`.`s_inv` MODIFY `sumv` numeric(17, 2);

ALTER TABLE `hennessy`.`s_manager` MODIFY `taxc` NVARCHAR(14);

ALTER TABLE `hennessy`.`s_org` MODIFY `taxc` NVARCHAR(14);

ALTER TABLE `hennessy`.`s_ou` MODIFY `taxc` NVARCHAR(14);

ALTER TABLE `hennessy`.`s_serial` MODIFY `taxc` NVARCHAR(14);

ALTER TABLE `hennessy`.`s_ou` MODIFY `taxo` NVARCHAR(5);

ALTER TABLE `hennessy`.`s_serial` MODIFY `td` datetime(3);

ALTER TABLE `hennessy`.`s_org` MODIFY `tel` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_ou` MODIFY `tel` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_ou` MODIFY `temp` INT;

ALTER TABLE `hennessy`.`s_ou` MODIFY `temp` INT NULL;

ALTER TABLE `hennessy`.`s_ou` ALTER `temp` DROP DEFAULT;

ALTER TABLE `hennessy`.`s_inv` MODIFY `total` numeric(17, 2);

ALTER TABLE `hennessy`.`s_inv` MODIFY `totalv` numeric(17, 2);

ALTER TABLE `hennessy`.`s_dcm` MODIFY `typ` NVARCHAR(20);

ALTER TABLE `hennessy`.`s_cat` MODIFY `type` NVARCHAR(30);

ALTER TABLE `hennessy`.`s_dcd` MODIFY `type` NVARCHAR(30);

ALTER TABLE `hennessy`.`s_inv` MODIFY `type` NVARCHAR(10);

ALTER TABLE `hennessy`.`s_inv_file` MODIFY `type` VARCHAR(45);

ALTER TABLE `hennessy`.`s_org` MODIFY `type` INT;

ALTER TABLE `hennessy`.`s_serial` MODIFY `type` NVARCHAR(6);

ALTER TABLE `hennessy`.`s_inv` MODIFY `uc` NVARCHAR(20);

ALTER TABLE `hennessy`.`s_serial` MODIFY `uc` NVARCHAR(20);

ALTER TABLE `hennessy`.`s_user` MODIFY `uc` TINYINT(3);

ALTER TABLE `hennessy`.`s_gns` MODIFY `unit` NVARCHAR(30);

ALTER TABLE `hennessy`.`s_manager` MODIFY `user_id` NVARCHAR(20);

ALTER TABLE `hennessy`.`s_member` MODIFY `user_id` NVARCHAR(20);

ALTER TABLE `hennessy`.`s_sys_logs` MODIFY `user_id` NVARCHAR(20);

ALTER TABLE `hennessy`.`s_sys_logs` MODIFY `user_name` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_serial` MODIFY `uses` TINYINT(3);

ALTER TABLE `hennessy`.`s_ou` MODIFY `usr` NVARCHAR(255);

ALTER TABLE `hennessy`.`s_ex` MODIFY `val` numeric(8, 2);

ALTER TABLE `hennessy`.`s_inv` MODIFY `vat` numeric(17, 2);

ALTER TABLE `hennessy`.`s_inv` MODIFY `vatv` numeric(17, 2);

ALTER TABLE `hennessy`.`s_org` MODIFY `ward` NVARCHAR(7);

ALTER TABLE `hennessy`.`s_ou` MODIFY `ward` NVARCHAR(7);

ALTER TABLE `hennessy`.`s_dcm` MODIFY `xc` NVARCHAR(2);

ALTER TABLE `hennessy`.`s_xls` MODIFY `xc` NVARCHAR(2);

ALTER TABLE `hennessy`.`s_inv` MODIFY `xml` NVARCHAR(-1);

ALTER TABLE `hennessy`.`s_invdoctemp` MODIFY `xml` NVARCHAR(-1);

ALTER TABLE `hennessy`.`s_inv` DROP PRIMARY KEY;

ALTER TABLE `hennessy`.`s_inv` ADD PRIMARY KEY (`id`);

ALTER TABLE `hennessy`.`s_sys_logs` DROP PRIMARY KEY;

ALTER TABLE `hennessy`.`s_sys_logs` ADD PRIMARY KEY (`id`);


ALTER TABLE `s_inv` ADD `orddt` datetime  GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.orddt')) = ''),NULL,json_unquote(json_extract(`doc`,'$.orddt')))) VIRTUAL;


ALTER TABLE `s_inv` ADD `status_received` INT GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.status_received')) = 'null'),0,json_unquote(json_extract(`doc`,'$.status_received')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `hascode` numeric(38) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.hascode')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.hascode')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `invtype` NVARCHAR(500) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.invtype')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.invtype')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `sendmaildate`  datetime GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.SendMailDate'))) VIRTUAL;

ALTER TABLE `s_inv` ADD `sendsmsdate` datetime  GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.SendSMSlDate'))) VIRTUAL;


ALTER TABLE `s_inv` ADD `orddt` datetime  GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.orddt')) = ''),NULL,json_unquote(json_extract(`doc`,'$.orddt')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `vehic` NVARCHAR(500)  GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.vehic')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.vehic')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `ordno` NVARCHAR(500) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.ordno')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.ordno')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `whsfr` NVARCHAR(500)  GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.whsfr')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.whsfr')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `whsto` NVARCHAR(500) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.whsto')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.whsto')))) VIRTUAL;
ALTER TABLE `s_inv` ADD `status_received` INT GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.status_received')) = 'null'),0,json_unquote(json_extract(`doc`,'$.status_received')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `xml_received` longtext COLLATE utf8_bin;

ALTER TABLE `s_inv` ADD `hascode` numeric(38) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.hascode')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.hascode')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `invtype` NVARCHAR(500) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.invtype')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.invtype')))) VIRTUAL;

ALTER TABLE `s_inv` DROP COLUMN `vseq`;
ALTER TABLE `s_inv` ADD `vseq` NVARCHAR(18) GENERATED ALWAYS AS (if(IFNULL(json_unquote(json_extract(`doc`,'$.seq')), '') > '',json_unquote(json_extract(`doc`,'$.seq')),id)) VIRTUAL;

ALTER TABLE `s_inv` ADD `sendmailstatus` INT GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.SendMailStatus')) = 0),NULL,json_unquote(json_extract(`doc`,'$.SendMailStatus')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `sendsmsstatus` INT GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.SendSMSStatus')) = 0),NULL,json_unquote(json_extract(`doc`,'$.SendSMSStatus')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `sendmailnum` INT GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.SendMailNum')) = 0),NULL,json_unquote(json_extract(`doc`,'$.SendMailNum')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `sendsmsnum` INT GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.SendSMSNum')) = 0),NULL,json_unquote(json_extract(`doc`,'$.SendSMSNum')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `taxocode` NVARCHAR(20) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.taxocode')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.taxocode')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `invlistdt` datetime  GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.invlistdt')) = ''),NULL,json_unquote(json_extract(`doc`,'$.invlistdt')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `discountamt` numeric(18) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.discountamt')) = 	null),0,json_unquote(json_extract(`doc`,'$.discountamt')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `sendmaildate`  datetime GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.SendMailDate'))) VIRTUAL;

ALTER TABLE `s_inv` ADD `sendsmsdate` datetime  GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.SendSMSlDate'))) VIRTUAL;