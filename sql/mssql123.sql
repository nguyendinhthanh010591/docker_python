CREATE TABLE [dbo].[s_list_inv_cancel](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[inv_id] [int] NOT NULL,
	[stax]  AS (CONVERT([nvarchar](14),json_value([doc],'$.stax'))),
	[ou] [int] NOT NULL,
	[doc] [nvarchar](max) NOT NULL,
	[invtype] [int] NOT NULL,	
	[serial]  AS (CONVERT([nvarchar](6),json_value([doc],'$.serial'))),
	[type]  AS (CONVERT([nvarchar](10),json_value([doc],'$.type'))),
	[idt]  AS (CONVERT([datetime],json_value([doc],'$.idt'))),
	[cdt] [datetime] NOT NULL,
	[listinv_id] [bigint] NULL,
	
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


CREATE NONCLUSTERED INDEX [s_list_inv_cancel_cdt_ou] ON [dbo].[s_list_inv_cancel]
(
	cdt,ou
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

	 ALTER TABLE s_inv
add 	 [sendmaildate]  AS (CONVERT([datetime],json_value([doc],'$.SendMailDate')))
	 ALTER TABLE s_inv add 	[sendsmsdate]  AS (CONVERT([datetime],json_value([doc],'$.SendSMSlDate')))
     ALTER TABLE s_inv add [vseq]  AS (TRY_CAST(isnull(case when TRY_CAST(json_value([doc],'$.seq') AS [nvarchar](18))='' then NULL else TRY_CAST(json_value([doc],'$.seq') AS [nvarchar](18)) end,CONVERT([nvarchar](18),[id])) AS [nvarchar](18))) PERSISTED
	 ALTER TABLE s_inv add [sendmailstatus]  AS (case when CONVERT([int],json_value([doc],'$.SendMailStatus')) IS NULL then (0) else CONVERT([int],json_value([doc],'$.SendMailStatus')) end)
	ALTER TABLE s_inv add [sendsmsstatus]  AS (case when CONVERT([int],json_value([doc],'$.SendSMSStatus')) IS NULL then (0) else CONVERT([int],json_value([doc],'$.SendSMSStatus')) end)
	ALTER TABLE s_inv add [sendmailnum]  AS (case when CONVERT([int],json_value([doc],'$.SendMailNum')) IS NULL then (0) else CONVERT([int],json_value([doc],'$.SendMailNum')) end)
	ALTER TABLE s_inv add [sendsmsnum]  AS (case when CONVERT([int],json_value([doc],'$.SendSMSNum')) IS NULL then (0) else CONVERT([int],json_value([doc],'$.SendSMSNum')) end)
	ALTER TABLE s_inv add [bcode]  AS (CONVERT([nvarchar](500),json_value([doc],'$.bcode')))
	ALTER TABLE s_inv add [taxocode]  AS (CONVERT([nvarchar](20),json_value([doc],'$.taxocode')))
	ALTER TABLE s_inv add [invlistdt]  AS (CONVERT([datetime],json_value([doc],'$.invlistdt')))
	ALTER TABLE s_inv add [discountamt]  AS (case when CONVERT([numeric],json_value([doc],'$.discountamt')) IS NULL then (0) else CONVERT([numeric],json_value([doc],'$.discountamt')) end)
	ALTER TABLE s_inv add [ordno]  AS (CONVERT([nvarchar](500),json_value([doc],'$.ordno')))
	ALTER TABLE s_inv add [whsfr]  AS (CONVERT([nvarchar](500),json_value([doc],'$.whsfr')))
	ALTER TABLE s_inv add [whsto]  AS (CONVERT([nvarchar](500),json_value([doc],'$.whsto')))
	ALTER TABLE s_inv add [edt] [datetime] NULL
	ALTER TABLE s_inv add [status_received]  AS (case when CONVERT([int],json_value([doc],'$.status_received')) IS NULL then (0) else CONVERT([int],json_value([doc],'$.status_received')) end)
	ALTER TABLE s_inv add  [xml_received] [nvarchar](max) NULL
	ALTER TABLE s_inv add  [hascode]  AS (CONVERT([numeric](500),json_value([doc],'$.hascode')))
	ALTER TABLE s_inv add  [invtype]  AS (CONVERT([nvarchar](500),json_value([doc],'$.invtype')))
    ALTER TABLE s_inv add 	[invlistnum] [nvarchar](50) NULL
    ALTER TABLE s_inv add 	[period_type] [nvarchar](1) NULL
   ALTER TABLE s_inv ADD period NVARCHAR(10) NULL;

	ALTER TABLE s_inv add [list_invid] [bigint] NULL
	ALTER TABLE s_inv add [status_tbss] [int] NULL
	ALTER TABLE s_inv add  [seq]  AS (CONVERT([nvarchar](8),json_value([doc],'$.seq')))
	ALTER TABLE s_inv add  [orddt]  AS (CONVERT([datetime],json_value([doc],'$.orddt')))
	ALTER TABLE s_inv add  [error_msg_van] [nvarchar](max) NULL
	ALTER TABLE s_inv add  [vehic]  AS (CONVERT([nvarchar](500),json_value([doc],'$.vehic')))


INSERT INTO [dbo].[s_listvalues]
           ([keyname]
           ,[keyvalue]
           ,[keytype])
     VALUES
           ('INCLIST'
           ,0
           ,'String')
GO



CREATE TABLE [dbo].[s_list_inv](
	[id] [bigint]  NOT NULL,
	[listinv_code] [nvarchar](15) NULL,
	[s_tax] [nvarchar](14) NOT NULL,
	[ou] int NOT NULL,
	[created_date] [datetime] NOT NULL,
	[created_by] [nvarchar](50) NULL,
	[approve_date] [datetime] NULL,
	[approve_by] [nvarchar](20) NULL,
	[s_name] [nvarchar](400) NOT NULL,
	
	[item_type] [nvarchar](20) NOT NULL,
	[listinvoice_num] [nvarchar](5) NULL,
	[period_type] [nvarchar](1) NOT NULL,
	[period] [nvarchar](10) NOT NULL,
	[status] [int] NOT NULL,
	[doc] [nvarchar](max) NULL,
	[xml] [nvarchar](max) NULL,
	[times] [int] NOT NULL,
 CONSTRAINT [s_list_inv_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[s_list_inv] ADD  CONSTRAINT [DF_s_list_inv_created_date]  DEFAULT (getdate()) FOR [created_date]
GO

GO

ALTER TABLE [dbo].[s_list_inv] ADD  CONSTRAINT [DF_s_list_inv_status]  DEFAULT ((0)) FOR [status]
GO

ALTER TABLE [dbo].[s_list_inv] ADD  DEFAULT ((0)) FOR [times]
GO

update s_listvalues set keyvalue=N'<!DOCTYPE html><html><head>      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">      <title>Mau 4 -daphuc</title>      <style>          @page {              size: a4;          }            .VATTEMP {              border: #000 solid 1px !important;              border-collapse: collapse;          }            .VATTEMP-notop2 {              border: #000 solid 0px !important;              border-top: 0 none !important;              border-collapse: collapse;              margin-left: -8px;          }            .VATTEMP-nobot2 {              border: #000 solid 0px !important;              border-bottom: 0 none !important;              border-collapse: collapse;              margin-left: -8px;          }            .VATTEMP-notop {              border-bottom: #000 solid 1px !important;              border-collapse: collapse;          }            table {              border-collapse: collapse;          }            table td {              font-family: "Times New Roman", Times, serif;          }              .VATTEMP-nobot {              border: #000 solid 1px !important;              border-top: 0 none !important;              border-collapse: collapse;              border-right: 0 none !important;              border-left: 0 none !important;          }            .VATTEMP-notop-nobot {              border: #000 solid 1px !important;              border-top: 0 none !important;              border-bottom: 0 none !important;              border-collapse: collapse;          }            .VATTEMP-noline {              border: 0px none !important;          }            .content_total {              font-size: 14px;              font-family: "Times New Roman", Times, serif;              padding: 2px;              color: #000;          }            .content_num {              font-size: 12px;              font-family: "Times New Roman", Times, serif;              color: #000;          }            .content_inv {              border-bottom: 1px dotted #000 !important;              border-collapse: collapse;              padding-top: 2px;              padding-bottom: 2px;              font-family: "Times New Roman", Times, serif;              color: #006;              font-size: 14px;          }            .mst {              border: #036 1px solid !important;              border-collapse: collapse;              padding-left: 5px;              padding-right: 5px;              padding-bottom: 2px;              padding-top: 2px;          }            .INVOICE {              color: #F00;              font-size: 22px;              font-weight: 600;              font-family: "Times New Roman", Times, serif;              padding: 5px;          }            #invoice_content {              border-left: #000 solid 1px !important;              border-right: #000 solid 1px !important;              border-top: 0 none !important;              border-bottom: 0 none !important;              border-collapse: collapse;          }              .tbl_1 {              border-collapse: collapse;              border: #000 solid 1px !important;          }            .tbl_2 {              border-collapse: collapse;              border: #000 solid 1px !important;              border-top: 0 none !important;          }            .tbl_3 {              border: #000 solid 1px !important;          }            .none_top {              border-top: 0 none;              border-left: 1px solid #000000 !important;              border-right: 1px solid #000000 !important;              border-bottom: 1px dotted #000000 !important;              border-collapse: collapse;          }            .none_bottom {              border-bottom: 0px solid #000 !important;              border-left: 1px solid #000000 !important;              border-right: 1px solid #000000 !important;              border-top: 1px solid #000000 !important;              border-collapse: collapse;          }      </style>  </head>    <body>      <div align="center">          <div style="width: 100%;">              <!-- phan thong tin chi tiet -->              <table class="VATTEMP-noline" style="width: 100%; border-collapse: collapse;" border="0px" align="center">                  <thead>                      <tr style="display: none;">                          <td> </td>                      </tr>                  </thead>                  <tbody>                      <tr>                          <td>                              <p style="text-align: right;"><span style="font-size: 8pt;"><strong>Mẫu số:                                          01/TH-HĐĐT</strong></span></p>                              <p style="text-align: center;"><span style="font-size: 8pt;"><strong>BẢNG TỔNG HỢP DỮ LIỆU                                          HÓA ĐƠN ĐIỆN TỬ GỬI CƠ QUAN THUẾ</strong></span></p>                              <p style="text-align: center;"><span style="font-size: 8pt;"><em>[01] Kỳ dữ liệu: {{pd}}</em></span></p>                              <p style="text-align: center;"><span style="font-size: 8pt;"><em>[02] Lần đầu [{{times1}}]     [03]                                          Bổ sung lần thứ [{{times2}}]    [04] Sửa đổi lần thứ [ ]</em></span></p>                              <p><span style="font-size: 8pt;"><em>[05]</em> Tên người nộp thuế: {{sname}}</span></p>                          </td>                      </tr>                      <tr>                          <td>                              <table style="width: 100%;" width="100%">                                  <tbody>                                      <tr style="height: 30px;">                                          <td style="width: 10%;" width="7%">                                              <p><span style="font-size: 8pt;"><em>[06]</em> Mã số thuế:</span></p>                                          </td>                                          <td style="width: 6%;" width="5%">                                              <p><span style="font-size: 8pt;"><em></em></span></p>                                          </td>                                          <td style="width: 6%; border: 1px solid;text-align:center" width="6%">                                              <p><span style="font-size: 8pt;"><em>{{obj.mst0}} </em> </span></p>                                          </td>                                          <td style="width: 6%; border: 1px solid;text-align:center" width="6%">                                              <p><span style="font-size: 8pt;"><em>{{obj.mst1}} </em></span></p>                                          </td>                                          <td style="width: 6%; border: 1px solid;text-align:center" width="6%">                                              <p><span style="font-size: 8pt;"><em>{{obj.mst2}} </em></span></p>                                          </td>                                          <td style="width: 6%; border: 1px solid;text-align:center" width="6%">                                              <p><span style="font-size: 8pt;"><em>{{obj.mst3}} </em></span></p>                                          </td>                                          <td style="width: 6%; border: 1px solid;text-align:center" width="6%">                                              <p><span style="font-size: 8pt;"><em>{{obj.mst4}} </em></span></p>                                          </td>                                          <td style="width: 6%; border: 1px solid;text-align:center" width="6%">                                              <p><span style="font-size: 8pt;"><em>{{obj.mst5}} </em></span></p>                                          </td>                                          <td style="width: 6%; border: 1px solid;text-align:center" width="6%">                                              <p><span style="font-size: 8pt;"><em> {{obj.mst6}}</em></span></p>                                          </td>                                          <td style="width: 6%; border: 1px solid;text-align:center" width="6%">                                              <p><span style="font-size: 8pt;"><em> {{obj.mst7}}</em></span></p>                                          </td>                                          <td style="width: 6%; border: 1px solid;text-align:center" width="6%">                                              <p><span style="font-size: 8pt;"><em> {{obj.mst8}}</em></span></p>                                          </td>                                          <td style="width: 6%; border: 1px solid;text-align:center" width="6%">                                              <p><span style="font-size: 8pt;"><em> {{obj.mst9}}</em></span></p>                                          </td>                                          <td style="width: 6%;" width="6%">                                              <p><span style="font-size: 8pt;"><em> </em></span></p>                                          </td>                                          <td style="width: 6%; border: 1px solid;text-align:center" width="6%">                                              <p><span style="font-size: 8pt;"><em>{{obj.mst11}} </em></span></p>                                          </td>                                          <td style="width: 6%; border: 1px solid;text-align:center" width="6%">                                              <p><span style="font-size: 8pt;"><em> {{obj.mst12}}</em></span></p>                                          </td>                                          <td style="width: 6%; border: 1px solid;text-align:center" width="6%">                                              <p><span style="font-size: 8pt;"><em>{{obj.mst13}} </em></span></p>                                          </td>                                      </tr>                                  </tbody>                              </table>                          </td>                      </tr>                       <tr>                          <td>                              <p style="text-align: right;"><span style="font-size: 8pt;">Số: {{listinvoice_num}}.</span></p>                              <table class="tbl_1" style="height: 260px;" border="1">                                           <thead>                                      <tr>                                          <td style="text-align: center; width: 104.238px;">                                              <p><span style="font-size: 8pt;"><strong>STT</strong></span></p>                                          </td>                                          <td style="text-align: center; width: 104.258px;">                                              <p><span style="font-size: 8pt;"><strong>Ký hiệu mẫu số hóa đơn, ký hiệu hóa                                                          đơn</strong></span></p>                                          </td>                                          <td style="text-align: center; width: 104.082px;">                                              <p><span style="font-size: 8pt;"><strong>S</strong><strong>ố                                                      </strong><strong>hóa đơn</strong></span></p>                                          </td>                                          <td style="text-align: center; width: 104.59px;">                                              <p><span style="font-size: 8pt;"><strong>Ngày tháng năm lập hóa                                                          đơn</strong></span></p>                                          </td>                                          <td style="text-align: center; width: 104.609px;">                                              <p><span style="font-size: 8pt;"><strong>Tên người mua</strong></span></p>                                          </td>                                          <td style="text-align: center; width: 104.688px;">                                              <p><span style="font-size: 8pt;"><strong>Mã số thuế người mua / mã khách                                                          hàng</strong></span></p>                                          </td>                                          <td style="text-align: center; width: 104.375px;">                                              <p><span style="font-size: 8pt;"><strong>Mặt hàng</strong></span></p>                                          </td>                                          <td style="text-align: center; width: 104.609px;">                                              <p><span style="font-size: 8pt;"><strong>S</strong><strong>ố                                                      </strong><strong>lượng hàng hóa</strong></span></p>                                          </td>                                          <td style="text-align: center; width: 104.883px;">                                              <p><span style="font-size: 8pt;"><strong>Tổng giá trị hàng hóa, dịch vụ bán                                                          ra chưa có thuế GTGT</strong></span></p>                                          </td>                                          <td style="text-align: center; width: 104.883px;">                                              <p><span style="font-size: 8pt;"><strong>Thuế                                                          su</strong><strong>ấ</strong><strong>t thuế GTGT</strong></span>                                              </p>                                          </td>                                          <td style="text-align: center; width: 104.883px;">                                              <p><span style="font-size: 8pt;"><strong>Tổng số thuế GTGT</strong></span>                                              </p>                                          </td>                                          <td style="text-align: center; width: 104.629px;">                                              <p><span style="font-size: 8pt;"><strong>Tổng tiền thanh                                                          toán</strong></span></p>                                          </td>                                          <td style="text-align: center; width: 104.688px;">                                              <p><span style="font-size: 8pt;"><strong>Trạng thái</strong></span></p>                                          </td>                                          <td style="text-align: center; width: 104.805px;">                                              <p><span style="font-size: 8pt;"><strong>Thông tin hóa đơn liên                                                          quan</strong></span></p>                                          </td>                                          <td style="text-align: center; width: 104.18px;">                                              <p><span style="font-size: 8pt;"><strong>Ghi                                                          ch</strong><strong>ú</strong></span></p>                                          </td>                                      </tr>           </thead>                                          <tbody>                                      <tr>                                          <td style="text-align: center; width: 104.238px;">                                              <p><span style="font-size: 8pt;">1</span></p>                                          </td>                                          <td style="text-align: center; width: 104.258px;">                                              <p><span style="font-size: 8pt;">2</span></p>                                          </td>                                          <td style="text-align: center; width: 104.082px;">                                              <p><span style="font-size: 8pt;">3</span></p>                                          </td>                                          <td style="text-align: center; width: 104.59px;">                                              <p><span style="font-size: 8pt;">4</span></p>                                          </td>                                          <td style="text-align: center; width: 104.609px;">                                              <p><span style="font-size: 8pt;">5</span></p>                                          </td>                                          <td style="text-align: center; width: 104.688px;">                                              <p><span style="font-size: 8pt;">6</span></p>                                          </td>                                          <td style="text-align: center; width: 104.375px;">                                              <p><span style="font-size: 8pt;">7</span></p>                                          </td>                                          <td style="text-align: center; width: 104.609px;">                                              <p><span style="font-size: 8pt;">8</span></p>                                          </td>                                          <td style="text-align: center; width: 104.883px;">                                              <p><span style="font-size: 8pt;">9</span></p>                                          </td>                                          <td style="text-align: center; width: 104.883px;">                                              <p><span style="font-size: 8pt;">10</span></p>                                          </td>                                          <td style="text-align: center; width: 104.883px;">                                              <p><span style="font-size: 8pt;">11</span></p>                                          </td>                                          <td style="text-align: center; width: 104.629px;">                                              <p><span style="font-size: 8pt;">12</span></p>                                          </td>                                          <td style="text-align: center; width: 104.688px;">                                              <p><span style="font-size: 8pt;">13</span></p>                                          </td>                                          <td style="text-align: center; width: 104.805px;">                                              <p><span style="font-size: 8pt;">14</span></p>                                          </td>                                          <td style="text-align: center; width: 104.18px;">                                              <p><span style="font-size: 8pt;">15</span></p>                                          </td>                                      </tr>             <tr style="display: none;">          <td><span style="font-family: arial; font-size: 12pt;">{{#each listinv}}</span></td>                   </tr>                                      <tr style="font-family: arial; font-size: 6pt;text-align:center">                                          <td style="width: 104.238px;"><p>{{line}}</p></td>                                          <td style="width: 104.258px;">{{form}}{{serial}} </td>                                          <td style="width: 104.082px;">{{seq}}</td>                                          <td style="width: 104.59px;">{{idt}}</td>                                          <td style="width: 104.609px;">{{bname}}</td>                                          <td style="width: 104.688px;">{{bcode}}</td>                                          <td style="width: 104.375px;">{{name}}</td>                                          <td style="width: 104.609px;">{{quantity}}</td>                                    <td style="width: 104.883px;text-align:right">{{fn2 amount}}</td>                                          <td style="width: 104.883px;text-align:right">{{vrt}}</td>                                          <td style="width: 104.883px;text-align:right">{{fn2 vat}}</td>                                          <td style="width: 104.629px;text-align:right">{{fn2 total}}</td>                                          <td style="width: 104.688px;">{{status}}</td>                                          <td style="width: 104.805px;">{{adjseq}}</td>                                          <td style="width: 104.18px;"><p>{{note}}</p></td>                                      </tr>                                       <tr style="display: none;">                                           <td>{{/each}}</td>                                          </tr>                                  </tbody>                              </table>                          </td>                      </tr>                     <tr>                          <td>                              <p><span style="font-size: 8pt;">Tôi cam đoan tài liệu khai trên là đúng và chịu trách nhiệm                                      trước pháp luật về những tài liệu đã khai./.</span></p>                              <p> </p>                              <table style="width: 100%;" width="100%">                                  <tbody>                                      <tr>                                          <td width="295">                                              <p> </p>                                          </td>                                          <td width="584">                                              <p style="text-align: center;"><span style="font-size: 8pt;"><em>{{crdt}}<br /></em><strong>NGƯỜI NỘP                                                          THUẾ<br /></strong><em>(Chữ ký số của người nộp                                                          thuế)</em></span></p>                                          </td>                                      </tr>                                  </tbody>                              </table>                              <p><span style="font-size: 8pt;"><strong><em>Ghi chú:</em></strong></span></p>                              <p><span style="font-size: 8pt;">- Trường hợp hóa đơn không nhất thiết có đầy đủ các nội                                      dung quy định tại khoản 14 Điều 10 Nghị định thì bỏ trống các chỉ tiêu không có trên                                      hóa đơn.</span></p>                              <p><span style="font-size: 8pt;">- [03] Bổ sung Mẫu bảng tổng hợp dữ liệu hóa đơn trường hợp                                      dữ liệu tổng hợp thiếu cần bổ sung.</span></p>                              <p><span style="font-size: 8pt;">- [04] Sửa đổi lần thứ [ ] trường hợp mẫu bảng tổng hợp dữ                                      liệu hóa đơn đã gửi có sai sót.</span></p>                              <p><span style="font-size: 8pt;">- Chỉ tiêu (6): người bán điền mã số thuế đối với người mua                                      là tổ chức kinh doanh, cá nhân kinh doanh có mã số thuế, trường hợp là cá nhân tiêu                                      dùng cuối cùng không có mã số thuế thì để trống; mã khách hàng đối với trường hợp                                      bán điện, nước cho khách hàng không có mã số thuế.</span></p>                          </td>                      </tr>                  </tbody>              </table>          </div>          <span style="font-size: 8pt;">              <!-- khung hinh an-->          </span>          <div id="bgi" style="display: none;"><span style="font-size: 8pt;">opacity:1; background-image: url(""),                  url(""); background-position: top 20px center; background-size: 195mm 277mm; background-repeat:                  no-repeat;</span></div>          <!-- thong tin footer -->      </div>  </body>    </html>' where keyname ='TEMPBTH';


CREATE TABLE [dbo].[s_wrongnotice_process_dtl](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[hdr_id] int NOT NULL,
	[form] [nvarchar](50) NULL,
	[inv_id] [bigint] NULL,
	[idt] [datetime] NULL,
	[line] [int] NULL,
	[noti_type] [int] NULL,
	[seq] [nvarchar](50) NULL,
	[serial] [nvarchar](50) NULL,
	[type_ref] [nvarchar](50) NULL,
	[rea] [nvarchar](max) NULL,
	[ma_cqthu] [nvarchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [s_wrongnotice_process]
add  dien_giai nvarchar(MAX);
ALTER TABLE [s_inv]
add  status_tbss int


ALTER TABLE [dbo].[s_inv]
DROP COLUMN seq;
ALTER TABLE [dbo].[s_inv] ADD seq AS (CONVERT([nvarchar](8),json_value([doc],'$.seq')))	 

/****** Object:  Table [dbo].[b_inv]    Script Date: 10/2/2022 12:10:28 PM ******/
ALTER TABLE [dbo].[s_inv] ADD [orddt] AS (CONVERT([datetime],json_value([doc],'$.orddt')))

s_role bê cả bảng quyền

ALTER TABLE s_inv
ADD inv_adj bigint;

CREATE TABLE [dbo].[van_einv_listid_msg](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[pid] [bigint] NULL,
	[type] [nvarchar](10) NULL,
	[status_scan] [int] NULL,
	[createdate] [datetime] NULL,
	[sendvandate] [datetime] NULL,
	[msg] [nvarchar](max) NULL,
	[xml] [nvarchar](max) NULL
)

CREATE NONCLUSTERED INDEX [van_msg_in_dtl_status_idx] ON [dbo].[van_msg_in_dtl]
(
	[status_scan] ASC,
	[createddate] ASC
)

CREATE NONCLUSTERED INDEX [van_msg_in_statsu_idx] ON [dbo].[van_msg_in]
(
	[status_scan] ASC
)

CREATE NONCLUSTERED INDEX [van_msg_in_dtl_status_idx] ON [dbo].[van_msg_in_dtl]
(
	[status_scan] ASC
)

CREATE NONCLUSTERED INDEX [van_msg_out_date_idx] ON [dbo].[van_msg_out]
(
	[createddate] ASC
)

CREATE NONCLUSTERED INDEX [van_history_id_ref_idx] ON [dbo].van_history
(
	id_ref ASC
)

  ALTER TABLE van_msg_out_dtl add status_response int 

CREATE NONCLUSTERED INDEX [van_msg_out_dtl_tranid_idx] ON [dbo].van_msg_out_dtl
(
	[transid] ASC
)
CREATE NONCLUSTERED INDEX [van_msg_out_dtl_status_response_idx] ON [dbo].[van_msg_out_dtl]
([status_response]
	
)
ALTER TABLE [dbo].van_msg_out_dtl ADD  DEFAULT (0) FOR [status_response]


ALTER TABLE van_msg_out_dtl add datecheckmsg datetime 

ALTER TABLE [dbo].van_msg_out_dtl ADD  DEFAULT (getdate()) FOR datecheckmsg

ALTER TABLE s_inv_adj add [listinv_id] [bigint] NULL

ALTER TABLE s_inv_adj ADD th_type int NOT NULL

ALTER TABLE s_inv_adj ADD seq nvarchar(8)

ALTER TABLE s_inv ADD th_type int

ALTER TABLE s_ou ADD place nvarchar(255);

ALTER TABLE s_inv ADD wno_adj int

ALTER TABLE s_inv add  [wnadjtype]  AS (CONVERT([int],json_value([doc],'$.wnadjtype')))


ALTER TABLE vcm_einvoiceapp.dbo.s_ou ADD place nvarchar(255) NULL;

ALTER TABLE s_list_inv add  [error_msg_van] [nvarchar](max) NULL;

ALTER TABLE s_list_inv add  status_received int NULL;

-- Thêm cục thuế doanh nghiệp lớn
INSERT INTO s_taxo (id,name) VALUES ('82500','Cục Thuế Doanh nghiệp lớn');
-- end (kiểm tra value của key CAT.TAXO trên redis )


-- thêm sửa quyền lập hóa đơn điều chỉnh khác
INSERT INTO s_role (id,name,code,pid,sort,active,menu_detail) VALUES (5001000,'Điều chỉnh hóa đơn khác','PERM_ADJ_INV_DIF',5000000,'5001000',1,'{ "id": "inv.inv?action=adjdif", "icon": "mdi mdi-file-plus", "value": "adj_dif" }')

UPDATE s_role SET menu_detail = '{"id": "inv.inv?action=createinv", "icon": "mdi mdi-file-plus", "value":"invoice_issue" }' WHERE id = 5010000
--end thêm sửa quyền lập hóa đơn điều chỉnh khác

-- mặc định trạng thái gửi TBSS là 0
UPDATE s_inv set status_tbss = 0 WHERE status_tbss is null
ALTER TABLE [dbo].[s_inv] ADD  DEFAULT ((0)) FOR [status_tbss]
--end mặc định trạng thái gửi TBSS là 0


CREATE TABLE [s_list_inv_id] ([id] [bigint], [inv_id] [bigint], [loai] [varchar](50));
create index "s_list_inv_id_idx" on "s_list_inv_id" ("id") 


create index s_list_inv_idx on s_list_inv (s_tax, created_date);


-- Nâng cấp 1510 cho bảng tổng hợp -- thinhpq10
alter table s_list_inv_cancel add  curr  AS (CONVERT([nvarchar](500),json_value([doc],'$.curr')))
alter table s_inv_adj add  curr nvarchar(3)
ALTER TABLE s_list_inv ADD curr nvarchar(3) NULL
CREATE TABLE s_tmp_list_inv (
	keyname varchar(60) NULL,
	id bigint NULL,
	kindinv int NULL,
	curr varchar(10) NULL,
	idt datetime NULL
)
-- end nâng cấp 1510
ALTER TABLE van_einv_listid_msg ADD parent_id nvarchar(10) NULL GO
ALTER TABLE van_einv_listid_msg ADD status_sendback int NULL GO
INSERT INTO s_role (id,name,code,pid,sort,active,menu_detail) VALUES
(4013300,N'Tra cứu hóa đơn - Reset BTH','PERM_SEARCH_RESET_BTH',4000000,'4013300',1,'')
,(4013400,N'Tra cứu hóa đơn - Gửi lại CQT','PERM_SEARCH_SENDBACK_CQT',4000000,'4013400',1,'')
,(6050150,N'Tra cứu bảng tổng hợp - Gửi lại BTH','PERM_SEARCH_BTH_SENDBACK',6050000,'6050150',1,NULL)
