CREATE TABLE s_dbs_mail_seq (
	id int IDENTITY(1,1) NOT NULL,
	seq_date int NULL,
	used_number int DEFAULT 0 NULL,
	CONSTRAINT PK__s_dbs_ma__3213E83F46956F3E PRIMARY KEY (id)
)