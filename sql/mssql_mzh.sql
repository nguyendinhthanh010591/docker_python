USE [Einv_MZH]
GO

/****** Object:  Table [dbo].[s_trans]    Script Date: 31/07/2020 5:57:52 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[s_trans](
	[ID] [numeric](18, 0) IDENTITY(1,1) NOT NULL,
	[refNo] [nvarchar](50) NULL,
	[valueDate] [date] NULL,
	[customerID] [nvarchar](50) NULL,
	[customerName] [nvarchar](500) NULL,
	[taxCode] [nvarchar](50) NULL,
	[customerAddr] [nvarchar](500) NULL,
	[isSpecial] [varchar](1) NULL,
	[curr] [varchar](10) NULL,
	[exrt] [float] NULL,
	[vrt] [varchar](3) NULL,
	[chargeAmount] [numeric](18, 2) NULL,
	[vcontent] [nvarchar](500) NULL,
	[amount] [numeric](18, 2) NULL,
	[vat] [numeric](18, 2) NULL,
	[total] [numeric](18, 2) NULL,
	[sysdate] [datetime] NULL,
	[tranID] [nvarchar](50) NULL,
	[trantype] [numeric](1, 0) NULL,
	[trancol] [numeric](1, 0) NULL,
 CONSTRAINT [PK_s_trans] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[s_trans] ADD  CONSTRAINT [DF_s_trans_sysdate]  DEFAULT (getdate()) FOR [sysdate]
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0_phi, 1_lai' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N's_trans', @level2type=N'COLUMN',@level2name=N'trantype'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Loại gom(0_thang,1_ngay,2_xuatle)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N's_trans', @level2type=N'COLUMN',@level2name=N'trancol'
GO



USE [Einv_MZH]
GO

/****** Object:  Index [s_trans_date]    Script Date: 31/07/2020 5:23:20 PM ******/
CREATE NONCLUSTERED INDEX [s_trans_date] ON [dbo].[s_trans]
(
	[valueDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



---------------------

INSERT INTO ROLE (ID,NAME,CODE)VALUES (33,	'Tra cứu giao dịch',	'PERM_TRANS');



  ALTER TABLE s_innv
  ADD [dds] [int],
  ADD [dds_sent] [int],
  ADD [dds_stt] [int] ;

	ALTER TABLE [dbo].[s_inv] ADD  CONSTRAINT [DF_s_inv_dds_sent]  DEFAULT ((0)) FOR [dds_sent]
	GO
  --------------------

  USE [Einv_MZH]
GO

/****** Object:  Table [dbo].[s_customer_pass]    Script Date: 20/08/2020 10:42:47 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[s_customer_pass](
	[custID] [nvarchar](50) NULL,
	[password] [nvarchar](50) NULL
) ON [PRIMARY]
GO


