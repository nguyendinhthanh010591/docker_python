-- Execute Stored Proc einv_deloitte.dbo.prc_gen_data_invoice

USE [einv_deloitte];
GO

DECLARE @return_value int;

DECLARE @p_fd Date;
DECLARE @p_td Date;


SET @p_fd = CONVERT(DATE, '2020-01-01', 121);
SET @p_td = CONVERT(DATE, '2020-01-05', 121);

EXEC  @return_value = einv_deloitte.dbo.prc_gen_data_invoice 
    @p_fd = @p_fd,
    @p_td = @p_td,
    @p_recperday = 100,
    @p_doc = N'{"bname":"1212","idt":"2020-06-19 17:34:31","type":"01GTKT","btax":"","buyer":"","btel":"","bmail":"","form":"01GTKT0/001","baddr":"1212","bacc":"","serial":"AA/19E","curr":"VND","exrt":1,"paym":"TM","bbank":"","seq":"0000001","note":"","sumv":90,"sum":90,"score":10,"discount":"","vatv":0,"vat":0,"word":"Chín mươi đồng","totalv":90,"total":90,"tax":[{"vrt":"0","vrn":"0%","amt":90,"amtv":90}],"name":"HÓA ĐƠN GIÁ TRỊ GIA TĂNG","items":[{"vrt":"0","quantity":1,"id":1592562791709,"line":1,"vat":0,"total":100,"name":"1212","price":100,"amount":100,"vrn":"0%"}],"id":6854,"sec":"RD@uS$Ax","stax":"0104918404-025","sname":"Chi nhánh Hải Phòng - Công ty Cổ phần Dịch vụ Thương mại Tổng hợp Vincommerce","saddr":"Khu trung tâm thương mại Vincom Lê Thánh Tông, Số 5 Đường Lê Thánh Tông, Phường Máy Tơ, Quận Ngô Quyền, Thành phố Hải Phòng, Việt Nam","smail":"","stel":"","taxo":"","sacc":"","sbank":"","status":3,"cdetemp":"Bị thay thế bởi HĐ số 0000002 ký hiệu AA/19E mẫu số 01GTKT0/001 ngày 19/06/2020 mã Uk9DdDdG","convertd":"23/06/2020","convertu":"hunglq"}',
    @p_ou = 1,
    @p_uc = N'hunglq';

GO