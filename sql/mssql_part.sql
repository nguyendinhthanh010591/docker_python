alter database einv_deloitte add filegroup g20;
alter database einv_deloitte add filegroup g21;
alter database einv_deloitte add filegroup g22;
alter database einv_deloitte add filegroup g23;
alter database einv_deloitte add filegroup g24;
alter database einv_deloitte add filegroup g25;

DECLARE @str nvarchar(max) = N'alter database einv_deloitte add file '
DECLARE @y varchar(2) = '25' 
DECLARE @p varchar(5)  
DECLARE @i int = 1  
DECLARE @j varchar(2)
WHILE @i <= 12  
BEGIN  
    SET @j = CAST(@i as varchar(2)) 
    SET @p='p'+@y+ REPLICATE('0', 2 - LEN(@j)) + @j
    SET @str += '(name='''+@p+''',filename=''E:\TuanNA\DELOITTE\DATA\'+@p+'.mdf'',size=1mb,filegrowth=8mb),'  
    SET @i = @i + 1  
END  
SET @str=SUBSTRING(@str,1,len(@str)-1)
SET @str += ' to filegroup g'+@y+';'  
PRINT @str
GO  

alter database einv_deloitte add file (name='p2501',filename='E:\TuanNA\DELOITTE\DATA\p2501.mdf',size=1mb,filegrowth=8mb),(name='p2502',filename='E:\TuanNA\DELOITTE\DATA\p2502.mdf',size=1mb,filegrowth=8mb),(name='p2503',filename='E:\TuanNA\DELOITTE\DATA\p2503.mdf',size=1mb,filegrowth=8mb),(name='p2504',filename='E:\TuanNA\DELOITTE\DATA\p2504.mdf',size=1mb,filegrowth=8mb),(name='p2505',filename='E:\TuanNA\DELOITTE\DATA\p2505.mdf',size=1mb,filegrowth=8mb),(name='p2506',filename='E:\TuanNA\DELOITTE\DATA\p2506.mdf',size=1mb,filegrowth=8mb),(name='p2507',filename='E:\TuanNA\DELOITTE\DATA\p2507.mdf',size=1mb,filegrowth=8mb),(name='p2508',filename='E:\TuanNA\DELOITTE\DATA\p2508.mdf',size=1mb,filegrowth=8mb),(name='p2509',filename='E:\TuanNA\DELOITTE\DATA\p2509.mdf',size=1mb,filegrowth=8mb),(name='p2510',filename='E:\TuanNA\DELOITTE\DATA\p2510.mdf',size=1mb,filegrowth=8mb),(name='p2511',filename='E:\TuanNA\DELOITTE\DATA\p2511.mdf',size=1mb,filegrowth=8mb),(name='p2512',filename='E:\TuanNA\DELOITTE\DATA\p2512.mdf',size=1mb,filegrowth=8mb) to filegroup g25;

DECLARE @str nvarchar(max) = N'CREATE PARTITION FUNCTION inv_part_function (datetime2) AS RANGE RIGHT FOR VALUES ('  
DECLARE @i datetime2 = '20200101'  
WHILE @i < '20251201'  
BEGIN  
    SET @str += '''' + CAST(@i as nvarchar(10)) + '''' + N','  
    SET @i = DATEADD(MM, 1, @i)  
END  
SET @str += '''' + CAST(@i as nvarchar(10))+ '''' + N');'  
PRINT @str
GO 

drop partition function inv_part_function; 
CREATE PARTITION FUNCTION inv_part_function (datetime2) AS RANGE RIGHT FOR VALUES ('2020-02-01','2020-03-01','2020-04-01','2020-05-01','2020-06-01','2020-07-01','2020-08-01','2020-09-01','2020-10-01','2020-11-01','2020-12-01','2021-01-01','2021-02-01','2021-03-01','2021-04-01','2021-05-01','2021-06-01','2021-07-01','2021-08-01','2021-09-01','2021-10-01','2021-11-01','2021-12-01','2022-01-01','2022-02-01','2022-03-01','2022-04-01','2022-05-01','2022-06-01','2022-07-01','2022-08-01','2022-09-01','2022-10-01','2022-11-01','2022-12-01','2023-01-01','2023-02-01','2023-03-01','2023-04-01','2023-05-01','2023-06-01','2023-07-01','2023-08-01','2023-09-01','2023-10-01','2023-11-01','2023-12-01','2024-01-01','2024-02-01','2024-03-01','2024-04-01','2024-05-01','2024-06-01','2024-07-01','2024-08-01','2024-09-01','2024-10-01','2024-11-01','2024-12-01','2025-01-01','2025-02-01','2025-03-01','2025-04-01','2025-05-01','2025-06-01','2025-07-01','2025-08-01','2025-09-01','2025-10-01','2025-11-01','2025-12-01');


create partition scheme inv_part_scheme as partition inv_part_function to (
[p2001],[p2002],[p2003],[p2004],[p2005],[p2006],[p2007],[p2008],[p2009],[p2010],[p2011],[p2012],
[p2101],[p2102],[p2103],[p2104],[p2105],[p2106],[p2107],[p2108],[p2109],[p2110],[p2111],[p2112],
[p2201],[p2202],[p2203],[p2204],[p2205],[p2206],[p2207],[p2208],[p2209],[p2210],[p2211],[p2212],
[p2301],[p2302],[p2303],[p2304],[p2305],[p2306],[p2307],[p2308],[p2309],[p2310],[p2311],[p2312],
[p2401],[p2402],[p2403],[p2404],[p2405],[p2406],[p2407],[p2408],[p2409],[p2410],[p2411],[p2412],
[p2501],[p2502],[p2503],[p2504],[p2505],[p2506],[p2507],[p2508],[p2509],[p2510],[p2511]
);



 



create partition function RangePartFunction (datetime) as range right for values ('2020101', '20200201')

create partition scheme RangePartScheme as Partition RangePartFunction
To ([Part_Before2015], [Part_201501], [Part_201504])
