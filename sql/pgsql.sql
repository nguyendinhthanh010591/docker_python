mkdir /data
cd /data
mkdir idx
mkdir cat
mkdir inv
chmod 755 -R /data
chown postgres -R /data
create tablespace idx location '/data/idx';
create tablespace cat location '/data/cat';
create tablespace inv location '/data/inv';
----------------------------------------------------------------
create table IF NOT EXISTS public.s_loc (
  id 	varchar(7)   not null,
  name  varchar(100) not null,
  pid   varchar(7),
  primary key (id)
) tablespace cat;
create index s_loc_pid_idx on public.s_loc(pid) tablespace idx;
alter table public.s_loc add constraint s_loc_fk  foreign key (pid) references public.s_loc(id);
----------------------------------------------------------------
create or replace function public.get_local_name(pcode varchar) returns varchar as 
$$
declare pname varchar(100);
begin
  select name into pname from public.s_loc where id=pcode;
  return pname;
end; 
$$ language  plpgsql;
----------------------------------------------------------------
create or replace function public.trg_fadd() returns trigger as
$body$
begin
   new.fadd:=concat_ws(', ',new.addr, public.get_local_name(new.ward), public.get_local_name(new.dist), public.get_local_name(new.prov));
   return new; 
end;
$body$ language plpgsql;
-----------------------------------------------------------
create or replace function public.get_fadd(addr varchar(255), prov varchar(3), dist varchar(5), ward varchar(7)) returns varchar(255) as 
$$
begin
   return concat_ws(', ', addr, public.get_local_name(ward), public.get_local_name(dist), public.get_local_name(prov));
end; 
$$ language  plpgsql;
----------------------------------------------------------------------------------------------------------------------
create table IF NOT EXISTS public.s_gns (
  id    SERIAL PRIMARY KEY,
  code  varchar(30)  not null,
  name  varchar(255) not null,
  unit  varchar(30)  default 'N/A' not null,
  price numeric(17,2),
  unique(code)
) tablespace cat;
----------------------------------------------------------------------------------------------------------------------
create table IF NOT EXISTS public.s_dcd (
  id    varchar(30)   not null,
  name  varchar(100) not null,
  type  varchar(30)   not null,
  primary key (type,id,name)
) tablespace cat;
---------------------------------------------------------------------
create table IF NOT EXISTS public.s_dcm (
  id    SERIAL PRIMARY KEY,
  itype varchar(6) not null,
  idx   smallint    not null,
  dtl   smallint    default 2 not null,
  lbl   varchar(100) not null,
  typ 	varchar(20)   not null,
  atr   JSON,
  status smallint default 2 not null,
  xc     varchar(2),
  unique (itype,idx,dtl)
 ) tablespace cat;
--------------------------------------------------------------------------------------------------------------------
create table IF NOT EXISTS public.s_xls (
  id SERIAL PRIMARY KEY,
  itype varchar(6)  not null,
  jc    varchar(20) not null,
  xc    varchar(2),
  unique (itype,jc)
) tablespace cat;
----------------------------------------------------------------
create table IF NOT EXISTS public.s_cat (
	id SERIAL PRIMARY KEY,
	name   varchar(100)  not null,
	type   varchar(30)   not null,
	des    varchar(255),
	unique (type,name)
) tablespace cat;
----------------------------------------------------------------
create table IF NOT EXISTS public.s_taxo (
  id   varchar(5)    not null,
  name varchar(100)  not null,
  primary key (id)
) tablespace cat;
----------------------------------------------------------------
create table IF NOT EXISTS public.s_org (
  id   		SERIAL PRIMARY KEY,
  code    varchar(50),
  name 		varchar(255) not null,
  name_en varchar(255),
  taxc 		varchar(14),
  prov 		varchar(3),
  dist 		varchar(5),
  ward 		varchar(7),
  addr 		varchar(255),
  tel 		varchar(255),
  mail 		varchar(255),
  acc 		varchar(255),
  bank 		varchar(255),
  status 	varchar(2) default '00' not null,
  c0      varchar(255),
  c1      varchar(255),
  c2      varchar(255),
  c3      varchar(255),
  c4      varchar(255),
  c5      varchar(255),
  c6      varchar(255),
  c7      varchar(255),
  c8      varchar(255),
  c9      varchar(255),
  pwd     varchar(255),
  fadd 		varchar(255),
  fts tsvector GENERATED ALWAYS AS (to_tsvector('english',name||' '||coalesce(code,'')||' '||coalesce(taxc,'')||coalesce(name_en,'')||' '||coalesce(fadd,'')||' '||coalesce(tel,'')||' '||coalesce(mail,'')||' '||coalesce(acc,'')||' '||coalesce(bank,'')||' '||coalesce(c0,'')||' '||coalesce(c1,'')||' '||coalesce(c2,'')||' '||coalesce(c3,'')||' '||coalesce(c4,'')||' '||coalesce(c5,'')||' '||coalesce(c6,'')||' '||coalesce(c7,'')||' '||coalesce(c8,'')||' '||coalesce(c9,''))) STORED
 ) tablespace cat;

create unique index s_org_taxc_idx on public.s_org(taxc) tablespace idx where taxc is not null;
create unique index s_org_code_idx on public.s_org(code) tablespace idx where code is not null;
create index s_org_fts_idx on public.s_org using gin (fts) tablespace idx;
--ALTER TABLE public.s_org DROP COLUMN fts;
--ALTER TABLE public.s_org  ADD COLUMN fts tsvector GENERATED ALWAYS AS (to_tsvector('english',name||' '||coalesce(code,'')||' '||coalesce(taxc,'')||coalesce(name_en,'')||' '||coalesce(fadd,'')||' '||coalesce(tel,'')||' '||coalesce(mail,'')||' '||coalesce(acc,'')||' '||coalesce(bank,'')||' '||coalesce(c0,'')||' '||coalesce(c1,'')||' '||coalesce(c2,'')||' '||coalesce(c3,'')||' '||coalesce(c4,'')||' '||coalesce(c5,'')||' '||coalesce(c6,'')||' '||coalesce(c7,'')||' '||coalesce(c8,'')||' '||coalesce(c9,''))) STORED;
--select name,fadd from s_org where fts  @@ 'hồ'
--select name,fadd, ts_rank(fts,query) rank from s_org,to_tsquery('Điện') query  where fts @@ query order  by rank
--SELECT * FROM ts_debug('english','Điện<->lực')
--phraseto_tsquery
select id,name,taxc,addr,mail,tel,acc,bank,prov,dist,ward,fadd from s_org where  fts @@ tsquery('010010018:*')
--DROP TRIGGER s_org_bui ON public.s_org;
create trigger s_org_bui before insert or update of addr,prov,dist,ward on public.s_org for each row execute procedure public.trg_fadd();
 ---------------------------------------------------------------
create table IF NOT EXISTS s_ou(
id      SERIAL PRIMARY KEY,
pid     int,
code    varchar(20),
name    varchar(255) not null,
taxc    varchar(14),
mst     varchar(14)  not null, 
paxo    varchar(5),
taxo    varchar(5),
prov    varchar(3),
dist    varchar(5),
ward    varchar(7),
addr    varchar(255),
tel     varchar(255),
mail    varchar(255),
acc     varchar(255),
bank    varchar(255),
status  smallint	default 1 not null,
sign    smallint	default 1 not null,
seq     smallint	default 1 not null,
usr     varchar(255), 
pwd     varchar(255),
temp    smallint	default 1 not null,
fadd    varchar(255),
foreign key(pid) references s_ou(id)
) tablespace cat;

create index s_ou_pid_idx on public.s_ou(pid) tablespace idx;
create unique index s_ou_taxc_idx on public.s_ou(taxc) tablespace idx where taxc is not null;
create unique index s_ou_code_idx on public.s_ou(code) tablespace idx where code is not null;
--drop trigger s_ou_bui on public.s_ou;
create trigger s_ou_bui before insert or update of addr,prov,dist,ward on public.s_ou for each row execute procedure public.trg_fadd();
----------------------------------------------------------------
create table IF NOT EXISTS public.s_user (
  id 	  varchar(20) not null,
  code  varchar(20),
  mail 	varchar(50),
  ou 	  int not null,
  uc    smallint  default 1 not null,
  name  varchar(50) not null,
  pos   varchar(50),
  primary key (id),
  foreign key (ou) references s_ou(id)
) tablespace cat;
create unique index s_user_code_idx on public.s_user(code)  tablespace idx where code is not null;
create index s_user_ou_idx on public.s_user(ou) tablespace idx;
----------------------------------------------------------------
create table IF NOT EXISTS public.s_role (
  id   SMALLSERIAL PRIMARY KEY,
  name varchar(100) not null
) tablespace cat;
----------------------------------------------------------------------------------------------
create table IF NOT EXISTS public.s_member (
  user_id varchar(20) not null,
  role_id smallint not null,
  primary key(user_id,role_id),
  foreign key(user_id) references public.s_user(id),
  foreign key(role_id) references public.s_role(id)
) tablespace cat;
----------------------------------------------------------------------------------------------
create table IF NOT EXISTS  public.s_manager (
  user_id varchar(20) not null,
  taxc    varchar(14) not null,
  primary key(user_id,taxc),
  foreign key(user_id) references  public.s_user(id)
 ) tablespace cat;
----------------------------------------------------------------------------------------------
create table IF NOT EXISTS  public.s_ex (
  id  SERIAL PRIMARY KEY,
  dt  TIMESTAMP DEFAULT NOW() not null,
  cur varchar(3)  not null,
  val numeric(8,2) not null,
  unique (dt,cur)
) tablespace cat;
---------------------------------------------------------------------------------------------------------------------
create table IF NOT EXISTS s_serial (
  id 	   SERIAL PRIMARY KEY,
  taxc   varchar(14) not null,
  type   varchar(6)  not null,
  form   varchar(11) not null,
  serial varchar(6)  not null,
  min 	 numeric(7,0)  not null,
  max    numeric(7,0)  not null,
  cur    numeric(7,0)  not null,
  status smallint  default 3 not null,
  fd TIMESTAMP not null,
  td TIMESTAMP,
  dt TIMESTAMP default NOW() not null,
  uc varchar(20)  not null,
  uses  smallint  default 1 not null,
  unique (taxc,form,serial),
  check (min>0 and cur>=min and max>=cur)
) tablespace cat;
----------------------------------------------------------------
create table IF NOT EXISTS s_seou (
  se int not null, 
  ou int not null, 
  constraint s_seou_pk primary key (se, ou), 
  constraint s_seou_fk1 foreign key (ou) references s_ou (id), 
  constraint s_seou_fk2 foreign key (se) references s_serial (id)
 ) tablespace cat;
------------------------------------------------------------------
drop table public.s_inv ;
create table if not exists public.s_inv (
id  	bigint,
sec   varchar(10) not null,
ic  	varchar(36),
idt 	timestamp not null,
pid 	bigint,
cid 	bigint,
cde 	varchar(255),
doc 	jsonb not null,
xml 	text,
dt  	timestamp default now() not null,
ou  	int  not null,
uc  	varchar(20) not null,
cvt   smallint default 0 
,status  smallint      generated always as (cast(doc->>'status' as smallint)) stored
,type    varchar(10)   generated always as (cast(doc->>'type'   as varchar(10))) stored
,form    varchar(11)   generated always as (cast(doc->>'form'   as varchar(11))) stored
,serial  varchar(6)    generated always as (cast(doc->>'serial' as varchar(6))) stored
,seq     varchar(7)    generated always as (cast(doc->>'seq'    as varchar(7))) stored
,paym    varchar(10)   generated always as (cast(doc->>'paym'   as varchar(10))) stored
,curr    varchar(3)    generated always as (cast(doc->>'curr'   as varchar(3))) stored
,exrt    numeric(8,2)  generated always as (cast(doc->>'exrt'   as numeric(8,2))) stored
,sname   varchar(255)  generated always as (cast(doc->>'sname'  as varchar(255))) stored
,stax    varchar(14)   generated always as (cast(doc->>'stax'   as varchar(14))) stored
,saddr   varchar(255)  generated always as (cast(doc->>'saddr'  as varchar(255))) stored
,smail   varchar(255)  generated always as (cast(doc->>'smail'  as varchar(255))) stored
,stel    varchar(255)  generated always as (cast(doc->>'stel'   as varchar(255))) stored
,sacc    varchar(255)  generated always as (cast(doc->>'sacc'   as varchar(255))) stored
,sbank   varchar(255)  generated always as (cast(doc->>'sbank'  as varchar(255))) stored
,buyer   varchar(255)  generated always as (cast(doc->>'buyer'  as varchar(255))) stored
,bname   varchar(255)  generated always as (cast(doc->>'bname'  as varchar(255))) stored
,btax    varchar(14)   generated always as (cast(doc->>'btax'   as varchar(14))) stored
,baddr   varchar(255)  generated always as (cast(doc->>'baddr'  as varchar(255))) stored
,bmail   varchar(255)  generated always as (cast(doc->>'bmail'  as varchar(255))) stored
,btel    varchar(255)  generated always as (cast(doc->>'btel'   as varchar(255))) stored
,bacc    varchar(255)  generated always as (cast(doc->>'bacc'   as varchar(255))) stored
,bbank   varchar(255)  generated always as (cast(doc->>'bbank'  as varchar(255))) stored
,note    varchar(255)  generated always as (cast(doc->>'note'   as varchar(255))) stored
,sum     numeric(17,2) generated always as (cast(doc->>'sum'    as numeric(17,2))) stored
,sumv    bigint        generated always as (cast(doc->>'sumv'   as bigint)) stored
,vat     numeric(17,2) generated always as (cast(doc->>'vat'    as numeric(17,2))) stored
,vatv    bigint        generated always as (cast(doc->>'vatv'   as bigint)) stored
,total   numeric(17,2) generated always as (cast(doc->>'total'  as numeric(17,2))) stored
,totalv  bigint        generated always as (cast(doc->>'totalv' as bigint)) stored
,adjseq  varchar(26)   generated always as (cast(doc->>'adj.seq' as varchar(26))) stored
,adjdes  varchar(255)  generated always as (cast(doc->>'adj.des' as varchar(255))) stored
,adjtyp  smallint      generated always as (cast(doc->>'adj.typ' as smallint)) stored
,c0      varchar(255)  generated always as (cast(doc->>'c0'  as varchar(255))) stored
,c1      varchar(255)  generated always as (cast(doc->>'c1'  as varchar(255))) stored
,c2      varchar(255)  generated always as (cast(doc->>'c2'  as varchar(255))) stored
,c3      varchar(255)  generated always as (cast(doc->>'c3'  as varchar(255))) stored
,c4      varchar(255)  generated always as (cast(doc->>'c4'  as varchar(255))) stored
,c5      varchar(255)  generated always as (cast(doc->>'c5'  as varchar(255))) stored
,c6      varchar(255)  generated always as (cast(doc->>'c6'  as varchar(255))) stored
,c7      varchar(255)  generated always as (cast(doc->>'c7'  as varchar(255))) stored
,c8      varchar(255)  generated always as (cast(doc->>'c8'  as varchar(255))) stored
,c9      varchar(255)  generated always as (cast(doc->>'c9'  as varchar(255))) stored
,cdt  	timestamp
,primary key(id)
,unique(sec)
,foreign key (pid) references s_inv(id)
,foreign key (ou)  references s_ou(id)
,foreign key (uc)  references s_user(id)
) tablespace inv;

create unique index s_inv_ic_idx on s_inv(ic) tablespace idx where ic is not null;
create index s_inv_pid_idx on s_inv(pid)  tablespace idx;
create index s_inv_ou_idx on s_inv(ou) tablespace idx;
create index s_inv_uc_idx on s_inv(uc) tablespace idx;
--create index s_inv_idt_idx on s_inv (idt,stax,ou,type,form,serial,status) tablespace idx;
--------------------------------------------------------
--init user
insert into s_user(id,ou,name) values ('admin',1,'admin');
do $$
begin
for i in 1..14 loop
   insert into public.s_member(user_id,role_id) values ('admin',i);
end loop;
end;
$$;
--------------------------------------------------------------------
--move idx to ablespace idx
select ' alter index '||indexname||' set tablespace idx;' from pg_indexes where schemaname='public';
select ' alter table '||tablename||' set tablespace cat;' from pg_tables where schemaname='public' and tableowner='postgres' and tablename<>'s_inv'
---------------------------------------------------------------------
ALTER TABLE s_dcm ADD lbl_en       varchar(100)  not null
update s_dcm set lbl_en=lbl
--------------------------------------------------------------
CREATE TABLE "s_group"  (
  "id" int4 NOT NULL GENERATED ALWAYS AS IDENTITY (
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
),
  "name" varchar(200) not null,
  "ou" int4 ,
  "status" varchar(1) ,
  "approve" varchar(1) ,
  "des" varchar(200) 
 
)

---------------------------------------
CREATE TABLE s_group_role 
   (	"group_id" int4 NOT NULL , 
	"role_id" int4 NOT NULL , 
	"status" VARCHAR(1)
   )
------------------------------------------
   	 CREATE TABLE s_group_user 
   (	"user_id" VARCHAR(20 ) NOT NULL , 
	"group_id" int4 NOT NULL 
   )

   --------------------------------------
    delete from	s_member
	 delete from s_role
	ALTER TABLE s_role ADD code       varchar(100)  not null

INSERT INTO s_role VALUES (1, 'Quản trị hệ thống', 'PERM_FULL_MANAGE');
INSERT INTO s_role VALUES (2, 'Cấu hình hệ thống', 'PERM_SYS_CONFIG');
INSERT INTO s_role VALUES (3, 'Quản lý người dùng', 'PERM_SYS_USER');
INSERT INTO s_role VALUES (4, 'Quản lý nhóm', 'PERM_SYS_GROUP');
INSERT INTO s_role VALUES (5, 'Phân quyền', 'PERM_SYS_ROLE');
INSERT INTO s_role VALUES (6, 'Tạo mẫu hóa đơn', 'PERM_SYS_TEMPLATE');
INSERT INTO s_role VALUES (7, 'Tạo mẫu Email', 'PERM_SYS_EMAIL');
INSERT INTO s_role VALUES (8, 'Cấu hình hóa đơn', 'PERM_SYS_INV_CONFIG');
INSERT INTO s_role VALUES (9, 'Cấu hình trường', 'PERM_SYS_FIELD_CONFIG');
INSERT INTO s_role VALUES (10, 'Quản trị danh mục', 'PERM_CATEGORY_MANAGE');
INSERT INTO s_role VALUES (11, 'Khách hàng', 'PERM_CATEGORY_CUSTOMER');
INSERT INTO s_role VALUES (12, 'Hàng hóa, dịch vụ', 'PERM_CATEGORY_ITEMS');
INSERT INTO s_role VALUES (13, 'Tỷ giá', 'PERM_CATEGORY_RATE');
INSERT INTO s_role VALUES (14, 'Danh mục khác', 'PERM_CATEGORY_ANOTHER');

INSERT INTO s_role VALUES (15, 'Thông báo phát hành', 'PERM_TEMPLATE_REGISTER');
INSERT INTO s_role VALUES (16, N'Gán quyền sử dụng TBPH', 'PERM_TEMPLATE_REGISTER_GRANT');

INSERT INTO s_role VALUES (17, 'Lập hóa đơn', 'PERM_CREATE_INV_MANAGE');
INSERT INTO s_role VALUES (18, 'Lập hóa đơn từ Excel', 'PERM_CREATE_INV_EXCEL');
INSERT INTO s_role VALUES (19, 'Lập hóa đơn từ nguồn khác', 'PERM_CREATE_INV_ANOTHER');
INSERT INTO s_role VALUES (20, 'Cấp số', 'PERM_CREATE_INV_SEQ');
INSERT INTO s_role VALUES (21, 'Duyệt và xuất hóa đơn', 'PERM_APPROVE_INV_MANAGE');
INSERT INTO s_role VALUES (22, 'Báo cáo', 'PERM_REPORT_INV');
INSERT INTO s_role VALUES (23, 'Tra cứu hóa đơn', 'PERM_SEARCH_INV');
INSERT INTO s_role VALUES (24, 'Điều chỉnh hóa đơn', 'PERM_INVOICE_ADJUST');
INSERT INTO s_role VALUES (25, 'Thay thế hóa đơn', 'PERM_INVOICE_REPLACE');
INSERT INTO s_role VALUES (26, 'Hủy hóa đơn', 'PERM_INVOICE_CANCEL');
INSERT INTO s_role VALUES (27, 'Duyệt thông báo phát hành', 'PERM_TEMPLATE_APPROVE');
INSERT INTO s_role VALUES (28, 'Hủy thông báo phát hành', 'PERM_TEMPLATE_VOID');
INSERT INTO s_role VALUES (29, 'Tích hợp', 'PERM_INTEGRATED');
INSERT INTO s_role VALUES (30, 'Chờ hủy', 'PERM_VOID_WAIT');
INSERT INTO s_role VALUES (31, 'Tiến lùi ngày hóa đơn', 'PERM_REDIRECT_IDT');

  
-- ----------------------------
-- Table structure for s_group
-- ----------------------------
drop table s_f_fld;
CREATE TABLE s_f_fld
(	
    id SERIAL PRIMARY KEY,
    fnc_name varchar(30) NOT NULL , 
    fnc_url varchar(15) NOT NULL , 
    fld_name varchar(30) NOT NULL , 
    fld_id varchar(30) NOT NULL , 
    fld_typ varchar(50) NOT NULL, 
    atr varchar(255), 
    feature varchar(255), 
    status smallint default 0 not null,
    unique(fnc_url,fld_id,feature)
) 
   -- "ID" IS 'ID duy nhất, Primary key';
   -- "FNC_NAME" IS 'Tên chức năng';
   -- "FNC_URL" IS 'URL chức năng';
   -- "FLD_NAME" IS 'Tên trường tương ứng trên chức năng';
   -- "ATTR" IS 'Thuộc tính, là chuỗi Json khai báo';
   -- "STATUS" IS 'Trạng thái; 1 - active, 0 - inactive';
   -- "FLD_ID" IS 'id trường';
   -- "FLD_type" IS 'Kiểu item của trường trên form (other, datagrid)';
   -- "feature" IS 'JSON nhận dạng màn hình ví dụ lập hđ';
   --  "S_F_FLD"  IS 'Bảng lưu các trường của từng chức năng và cấu hình các thuộc tính tương ứng';

-- ----------------------------
-- Table structure for s_inv_file
-- ----------------------------
CREATE TABLE s_inv_file (
  id int NOT NULL,
  type VARCHAR(45) NOT NULL,
  content text NOT NULL,
  PRIMARY KEY (id,type),
  foreign key (id) references s_inv(id)
  )
-- COMMENT = 'ID, type, content của các file hóa đơn';
-- type: adj, rep, can

----alter pass to user

Alter table s_user ADD pass VARCHAR(255) default 'YWRtaW5AMTIz';

-- ----------------------------
-- Table structure for s_sys_logs
-- ----------------------------
DROP TABLE IF EXISTS "public"."s_sys_logs";
create table public.s_sys_logs (
	"id" numeric not null default nextval('iseq_sys_logs'::regclass),
	"dt" date not null,
	"fnc_id" varchar(30) not null,
	"fnc_name" varchar(255) not null,
	"fnc_url" varchar(255) null,
	"action" varchar(255) null,
	"user_id" varchar(20) not null,
	"user_name" varchar(255) not null,
	"src" varchar(20) not null,
	"dtl" varchar(1000) null,
	"r1" varchar(255) null,
	"r2" varchar(255) null,
	"r3" varchar(255) null,
	"r4" varchar(255) null,
	msg_id varchar(255) null,
	msg_status int2 null,
	constraint s_sys_logs_pkey primary key ("id")
);

COMMENT ON COLUMN "public"."s_sys_logs"."ID" IS 'ID tự sinh';
COMMENT ON COLUMN "public"."s_sys_logs"."DT" IS 'Ngày ghi nhận log';
COMMENT ON COLUMN "public"."s_sys_logs"."FNC_ID" IS 'Mã chức năng';
COMMENT ON COLUMN "public"."s_sys_logs"."FNC_NAME" IS 'Tên chức năng';
COMMENT ON COLUMN "public"."s_sys_logs"."FNC_URL" IS 'URL thực thi';
COMMENT ON COLUMN "public"."s_sys_logs"."ACTION" IS 'Action thực thi';
COMMENT ON COLUMN "public"."s_sys_logs"."USER_ID" IS 'ID user';
COMMENT ON COLUMN "public"."s_sys_logs"."USER_NAME" IS 'Tên user';
COMMENT ON COLUMN "public"."s_sys_logs"."SRC" IS 'Nguồn phát sinh log';
COMMENT ON COLUMN "public"."s_sys_logs"."DTL" IS 'Chi tiết log';
COMMENT ON COLUMN "public"."s_sys_logs"."R1" IS 'Trường dự phòng 1';
COMMENT ON COLUMN "public"."s_sys_logs"."R2" IS 'Trường dự phòng 2';
COMMENT ON COLUMN "public"."s_sys_logs"."R3" IS 'Trường dự phòng 3';
COMMENT ON COLUMN "public"."s_sys_logs"."R4" IS 'Trường dự phòng 4';
COMMENT ON COLUMN "public"."s_sys_logs"."msg_id" IS 'trường lưu msg id (tên file, id chuỗi msg XML, id chuỗi Json)';
COMMENT ON COLUMN "public"."s_sys_logs"."msg_status" IS 'trạng thái xử lý msg (0 - xử lý lỗi, 1 - xử lý thành công)';
COMMENT ON TABLE "public"."s_sys_logs" IS 'Bảng lưu log audit thao tác của NSD APP, API';

-- ----------------------------
-- Indexes structure for table s_sys_logs
-- ----------------------------
CREATE INDEX "s_sys_logs_idx01" ON "public"."s_sys_logs" USING btree (
  "FNC_ID" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "USER_ID" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "SRC" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "s_sys_logs_idx02" ON "public"."s_sys_logs" USING btree (
  "SRC" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "msg_id" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "msg_status" "pg_catalog"."int2_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table s_sys_logs
-- ----------------------------
ALTER TABLE "public"."s_sys_logs" ADD CONSTRAINT "s_sys_logs_pkey" PRIMARY KEY ("ID");

--SEQUENCE cho bảng S_SYS_LOGS
DROP SEQUENCE ISEQ_SYS_LOGS;

CREATE SEQUENCE ISEQ_SYS_LOGS
  START WITH 1
  MAXVALUE 1000
  MINVALUE 0
  CYCLE;
  
CREATE INDEX s_inv_btax_idx 
	ON public.s_inv (status, "type", btax);

CREATE INDEX s_serial_dt_idx 
	ON public.s_serial (taxc, fd, td);	
	
CREATE SEQUENCE ISEQ_INVD
  START WITH 1;	
  
ALTER TABLE s_inv ADD cdt timestamp;

CREATE OR REPLACE FUNCTION public.trg_sinv_cdt()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$ declare v_cdt timestamp;

begin begin v_cdt := to_timestamp(cast(new.doc ->> 'cdt' as varchar(255)), 'YYYY-MM-DD HH24:MI:SS');

exception
when others then v_cdt := null;

end;

new.cdt := v_cdt;

return new;

end;

$function$
;

CREATE TRIGGER s_inv_bui
BEFORE insert OR UPDATE 
ON s_inv FOR EACH ROW
EXECUTE PROCEDURE public.trg_sinv_cdt()
;

-- ----------------------------
-- Table structure for s_err_msg
-- ----------------------------
CREATE TABLE "public"."s_err_msg" (
  "src_err_code" varchar(20) COLLATE "pg_catalog"."default" NOT NULL,
  "usr_code" varchar(20) COLLATE "pg_catalog"."default" NOT NULL,
  "usr_msg" varchar(500) COLLATE "pg_catalog"."default",
  "usr_msg_ent" varchar(500) COLLATE "pg_catalog"."default",
  "type_msg" varchar(10) COLLATE "pg_catalog"."default" NOT NULL
)
;
COMMENT ON COLUMN "public"."s_err_msg"."src_err_code" IS 'Mã thông báo nguồn';
COMMENT ON COLUMN "public"."s_err_msg"."usr_code" IS 'Mã thông báo hiển thị cho NSD';
COMMENT ON COLUMN "public"."s_err_msg"."usr_msg" IS 'Mô tả thông báo hiển thị cho NSD. Nếu cần truyền tham số đặt tên các tham số là ${p0}, ${p1},...,${pn}';
COMMENT ON COLUMN "public"."s_err_msg"."usr_msg_ent" IS 'Mô tả thông báo hiển thị cho NSD bằng tiếng Anh. Nếu cần truyền tham số đặt tên các tham số là ${p0}, ${p1},...,${pn}';
COMMENT ON COLUMN "public"."s_err_msg"."type_msg" IS 'Loại thông báo. INFO - Thông báo, WARNING - Cảnh báo, ERROR - Lỗi';
COMMENT ON TABLE "public"."s_err_msg" IS 'Bảng lưu danh mục mã thông báo NSD';

-- ----------------------------
-- Primary Key structure for table s_err_msg
-- ----------------------------
ALTER TABLE "public"."s_err_msg" ADD CONSTRAINT "s_err_msg_pkey" PRIMARY KEY ("src_err_code");

CREATE INDEX s_inv_cdt_idx ON public.s_inv USING btree ("stax", "ou", "type", "form", "serial", "status");

------- trungpq10 them cột des trong s_serial mô tả mục đích sử dụng th=bph
ALTER TABLE s_serial
 ADD des varchar(200)

-----------------------them loai du lieu cho khach hang
ALTER TABLE s_org
 ADD type int2 DEFAULT 1

 COMMENT ON COLUMN "public"."s_org"."type" IS 'kiểu dữ liệu 1_nhập tay/2_tự động';

 -------------------------------
 

  
ALTER TABLE "s_inv" 
  ALTER COLUMN "seq" TYPE varchar(8) COLLATE "pg_catalog"."default";  
  -- xoa s_seou tạo lại
  DROP TABLE IF EXISTS "s_seou";
  ---
  -- ----------------------------
-- Table structure for s_serial
-- ----------------------------
DROP TABLE IF EXISTS "public"."s_serial";
--tao seq
CREATE SEQUENCE "public"."s_serial_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 999999999
START 1
CACHE 1;

SELECT setval('"public"."s_serial_id_seq"', 1, false);

ALTER SEQUENCE "public"."s_serial_id_seq"
OWNED BY "public"."s_serial"."id";

ALTER SEQUENCE "public"."s_serial_id_seq" OWNER TO "postgres";

----tao bang
CREATE TABLE "public"."s_serial" (
  "id" int4 NOT NULL DEFAULT nextval('van_msg_dtl_seq'::regclass),
  "taxc" varchar(14) COLLATE "pg_catalog"."default" NOT NULL,
  "type" varchar(6) COLLATE "pg_catalog"."default" NOT NULL,
  "form" varchar(11) COLLATE "pg_catalog"."default" NOT NULL,
  "serial" varchar(6) COLLATE "pg_catalog"."default" NOT NULL,
  "min" numeric(8) NOT NULL,
  "max" numeric(8) NOT NULL,
  "cur" numeric(8) NOT NULL,
  "status" int2 NOT NULL DEFAULT 3,
  "fd" timestamp(6) NOT NULL,
  "td" timestamp(6),
  "dt" timestamp(6) NOT NULL DEFAULT now(),
  "uc" varchar(20) COLLATE "pg_catalog"."default" NOT NULL,
  "uses" int2 NOT NULL DEFAULT 1,
  "des" varchar(200) COLLATE "pg_catalog"."default",
  "priority"  numeric(2)
)
TABLESPACE "cat"
;

-- ----------------------------
-- Indexes structure for table s_serial
-- ----------------------------
CREATE INDEX "s_serial_dt_idx" ON "public"."s_serial" USING btree (
  "taxc" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "fd" "pg_catalog"."timestamp_ops" ASC NULLS LAST,
  "td" "pg_catalog"."timestamp_ops" ASC NULLS LAST
);

-- ----------------------------
-- Uniques structure for table s_serial
-- ----------------------------
ALTER TABLE "public"."s_serial" ADD CONSTRAINT "s_serial_taxc_form_serial_key" UNIQUE ("taxc", "form", "serial","priority") USING INDEX TABLESPACE "idx";

-- ----------------------------
-- Checks structure for table s_serial
-- ----------------------------
ALTER TABLE "public"."s_serial" ADD CONSTRAINT "s_serial_check" CHECK (min > 0::numeric AND (cur>=min or cur=0) AND max >= cur);

-- ----------------------------
-- Primary Key structure for table s_serial
-- ----------------------------
ALTER TABLE "public"."s_serial" ADD CONSTRAINT "s_serial_pkey" PRIMARY KEY ("id") USING INDEX TABLESPACE "idx";

ALTER TABLE "s_sys_logs" 
  ADD COLUMN "doc" jsonb;

COMMENT ON COLUMN "s_sys_logs"."doc" IS 'Thông tin dữ liệu ghi log, lưu dưới dạng json';

--Tăng độ rộng của trường người mua lên 500 ký tư
ALTER TABLE "s_inv" 
  ALTER COLUMN "bname" TYPE varchar(500) COLLATE "pg_catalog"."default";

  --------------------------------
  DROP TABLE IF EXISTS "public"."s_report_tmp";
CREATE TABLE "public"."s_report_tmp" (
  "c_id" int4 NOT NULL,
  "form" varchar(255) COLLATE "pg_catalog"."default",
  "serial" varchar(255) COLLATE "pg_catalog"."default",
  "min1" numeric(32),
  "max1" numeric(32),
  "min0" numeric(32),
  "max0" numeric(32),
  "maxu0" numeric(32),
  "minu" numeric(32) NOT NULL,
  "maxu" numeric(32),
  "cancel" numeric(32),
  "clist" varchar(4000) COLLATE "pg_catalog"."default",
  "minc" numeric(32),
  "maxc" numeric(32)
)
;

-- ----------------------------
-- Indexes structure for table s_report_tmp
-- ----------------------------
CREATE INDEX "c_id_idx" ON "public"."s_report_tmp" USING btree (
  "c_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

CREATE TABLE "s_seusr" (
  "se" int4 NOT NULL,
  "usrid" varchar(20) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Primary Key structure for table s_seusr
-- ----------------------------
ALTER TABLE "s_seusr" ADD CONSTRAINT "s_seusr_PRIMARY" PRIMARY KEY ("se", "usrid");

-- ----------------------------
-- Foreign Keys structure for table s_seusr
-- ----------------------------
ALTER TABLE "s_seusr" ADD CONSTRAINT "s_seusr_fk1" FOREIGN KEY ("se") REFERENCES "s_serial" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "s_seusr" ADD CONSTRAINT "s_seusr_fk2" FOREIGN KEY ("usrid") REFERENCES "s_user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-----------------------------------
drop INDEX s_org_taxc_idx ;

CREATE  INDEX "s_org_taxc_idx" ON "public"."s_org" USING btree (
  "taxc" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
) TABLESPACE "idx" WHERE taxc IS NOT NULL;

ALTER TABLE s_ou ADD c0 varchar(255) NULL;
COMMENT ON COLUMN s_ou.c0 IS 'Trường dự phòng 0';
ALTER TABLE s_ou ADD c1 varchar(255) NULL;
COMMENT ON COLUMN s_ou.c1 IS 'Trường dự phòng 1';
ALTER TABLE s_ou ADD c2 varchar(255) NULL;
COMMENT ON COLUMN s_ou.c2 IS 'Trường dự phòng 2';
ALTER TABLE s_ou ADD c3 varchar(255) NULL;
COMMENT ON COLUMN s_ou.c3 IS 'Trường dự phòng 3';
ALTER TABLE s_ou ADD c4 varchar(255) NULL;
COMMENT ON COLUMN s_ou.c4 IS 'Trường dự phòng 4';
ALTER TABLE s_ou ADD c5 varchar(255) NULL;
COMMENT ON COLUMN s_ou.c5 IS 'Trường dự phòng 5';
ALTER TABLE s_ou ADD c6 varchar(255) NULL;
COMMENT ON COLUMN s_ou.c6 IS 'Trường dự phòng 6';
ALTER TABLE s_ou ADD c7 varchar(255) NULL;
COMMENT ON COLUMN s_ou.c7 IS 'Trường dự phòng 7';
ALTER TABLE s_ou ADD c8 varchar(255) NULL;
COMMENT ON COLUMN s_ou.c8 IS 'Trường dự phòng 8';
ALTER TABLE s_ou ADD c9 varchar(255) NULL;
COMMENT ON COLUMN s_ou.c9 IS 'Trường dự phòng 9';

ALTER TABLE "public"."s_inv" 
  DROP COLUMN "adjseq",
  DROP COLUMN "adjdes",
  DROP COLUMN "adjtyp";
ALTER TABLE "public"."s_inv" ADD column "adjseq" varchar(30) GENERATED ALWAYS AS ((doc::json->>'adj')::json ->> 'seq') STORED;
ALTER TABLE "public"."s_inv" ADD column "adjdes" varchar(3000) GENERATED ALWAYS AS ((doc::json->>'adj')::json ->> 'des') STORED;
ALTER TABLE "public"."s_inv" ADD column "adjtyp" smallint GENERATED ALWAYS AS (((doc::json->>'adj')::json ->> 'typ')::smallint) STORED;

ALTER TABLE public.s_report_tmp ALTER COLUMN c_id TYPE numeric(20,0) USING c_id::numeric;

DROP INDEX public.s_user_code_idx;
CREATE UNIQUE INDEX s_user_code_idx
  ON public.s_user
  (code)
  WHERE ((code IS NOT NULL) AND ((code)::text <> ''::text));

ALTER TABLE "public"."s_inv" 
  DROP COLUMN "sumv",
  DROP COLUMN "vatv",
  DROP COLUMN "totalv";
ALTER TABLE "public"."s_inv" ADD column "sumv" numeric(17,2) GENERATED ALWAYS AS (((doc ->> 'sumv'::text))::numeric(17,2)) STORED;
ALTER TABLE "public"."s_inv" ADD column "vatv" numeric(17,2) GENERATED ALWAYS AS (((doc ->> 'vatv'::text))::numeric(17,2)) STORED;
ALTER TABLE "public"."s_inv" ADD column "totalv" numeric(17,2) GENERATED ALWAYS AS (((doc ->> 'totalv'::text))::numeric(17,2)) STORED;

ALTER TABLE public.s_sys_logs ALTER COLUMN dt TYPE timestamp USING dt::timestamp;

ALTER TABLE s_sys_logs ALTER COLUMN id DROP DEFAULT;

DROP SEQUENCE iseq_sys_logs;

CREATE SEQUENCE iseq_sys_logs
	INCREMENT BY 1
	NO MINVALUE
	NO MAXVALUE
	START 1
	CACHE 1
	CYCLE;
	
ALTER TABLE public.s_sys_logs ALTER COLUMN id SET DEFAULT nextval('iseq_sys_logs'::regclass);

ALTER TABLE public.s_inv drop constraint s_inv_un
CREATE UNIQUE INDEX s_inv$unq on  s_inv (stax,form,serial,(case when seq='' then sec else seq end))

DROP INDEX public.s_org_code_idx;

CREATE UNIQUE INDEX s_org_code_idx
  ON public.s_org
  (code)
  TABLESPACE idx
  WHERE ((code IS NOT NULL) AND ((code)::text <> ''::text));
  
ALTER TABLE public.s_user
  ADD COLUMN create_date timestamp DEFAULT NOW();
  
ALTER TABLE public.s_user
  ADD COLUMN last_login timestamp;    
  
ALTER TABLE public.s_user ADD "local" int2 NOT NULL DEFAULT 0;
ALTER TABLE public.s_user ADD change_pass_date timestamp NULL DEFAULT now();
ALTER TABLE public.s_user ADD login_number int4 NOT NULL DEFAULT 0;
ALTER TABLE public.s_user ADD change_pass int4 NOT NULL DEFAULT 0;

CREATE TABLE "public"."s_user_pass" ("user_id" VARCHAR(20) NOT NULL, "pass" VARCHAR(200) NOT NULL, "change_date" TIMESTAMP WITHOUT TIME ZONE, CONSTRAINT "s_user_pass_pk" PRIMARY KEY ("user_id", "pass"));

COMMENT ON COLUMN "public"."s_user_pass"."change_date" IS 'NgÃƒÂ y thay Ã„â€˜Ã¡Â»â€¢i mÃ¡ÂºÂ­t khÃ¡ÂºÂ©u';

ALTER TABLE "public"."s_user_pass" ADD CONSTRAINT "FK__s_user_pa__user___49CEE3AF" FOREIGN KEY ("user_id") REFERENCES "public"."s_user" ("id") ON UPDATE RESTRICT ON DELETE RESTRICT;

  