ALTER TABLE [dbo].[s_inv] DROP COLUMN [bcode]
GO

ALTER TABLE [dbo].[s_inv] ADD [bcode] AS (CONVERT([nvarchar](500),json_value([doc],'$.bcode')))	 
GO
