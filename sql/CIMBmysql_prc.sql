DROP TRIGGER IF EXISTS s_ou_ai;

DROP TRIGGER IF EXISTS `s_ou_bi`;
delimiter ;;
CREATE TRIGGER `s_ou_bi` BEFORE INSERT ON `s_ou` FOR EACH ROW begin 
 set new.fadd = get_fadd(new.addr,new.prov,new.dist,new.ward);
end
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table s_ou
-- ----------------------------
DROP TRIGGER IF EXISTS `s_ou_ai`;
delimiter ;;
CREATE TRIGGER `s_ou_ai` AFTER INSERT ON `s_ou` FOR EACH ROW begin
	insert into s_ou_tree (pid, cid, depth, path) values  (new.id, new.id, 0, concat(new.id, '/'));
	insert into s_ou_tree (pid, cid, depth, path) select  pid,new.id,depth + 1,concat(path, new.id, '/') 
from s_ou_tree
where cid = new.pid;
end
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table s_ou
-- ----------------------------
DROP TRIGGER IF EXISTS `s_ou_bu`;
delimiter ;;
CREATE TRIGGER `s_ou_bu` BEFORE UPDATE ON `s_ou` FOR EACH ROW begin 
 set new.fadd = get_fadd(new.addr,new.prov,new.dist,new.ward);
end
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table s_ou
-- ----------------------------
DROP TRIGGER IF EXISTS `s_ou_au`;
delimiter ;;
CREATE TRIGGER `s_ou_au` AFTER UPDATE ON `s_ou` FOR EACH ROW begin 
if new.pid != old.pid  or new.pid is null  or old.pid is null then 
if old.pid is not null then 
delete t2 from s_ou_tree t1 join s_ou_tree t2 on t1.cid = t2.cid 
where t1.pid = old.id 
and t2.depth > t1.depth;
end if;
if new.pid is not null then
insert into s_ou_tree (pid, cid, depth, path) select  t1.pid, t2.cid, t1.depth + t2.depth + 1, concat(t1.path, t2.path) 
from s_ou_tree t1,s_ou_tree t2
where  t1.cid = new.pid and t2.pid = old.id;
end if;
end if;
end
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table s_ou
-- ----------------------------
DROP TRIGGER IF EXISTS `s_ou_bd`;
delimiter ;;
CREATE TRIGGER `s_ou_bd` BEFORE DELETE ON `s_ou` FOR EACH ROW BEGIN
    
    delete from s_ou_tree where pid = old.id;
    delete from s_ou_tree where cid = old.id;
END
;;
delimiter ;

DROP FUNCTION IF EXISTS `get_local_name`;
delimiter ;;
CREATE FUNCTION `get_local_name`(pcode varchar(50))
 RETURNS varchar(100) CHARSET utf8mb4
  DETERMINISTIC
begin 
declare pname varchar(500);
if (nullif(pcode, '') is not null) then
	select  name into pname from s_loc where id = pcode;
end if;
return pname;
end
;;
delimiter ;

-- ----------------------------
-- Function structure for get_fadd
-- ----------------------------
DROP FUNCTION IF EXISTS `get_fadd`;
delimiter ;;
CREATE FUNCTION `get_fadd`(addr varchar(255),
  prov varchar(50),
  dist varchar(50),
  ward varchar(7))
 RETURNS varchar(255) CHARSET utf8mb4
  DETERMINISTIC
begin 
return concat_ws(', ',nullif(addr, ''),get_local_name(ward),get_local_name(dist), get_local_name(prov));
end
;;
delimiter ;


DROP TRIGGER IF EXISTS `s_org_bu`;
delimiter ;;
CREATE TRIGGER `s_org_bu` BEFORE UPDATE ON `s_org` FOR EACH ROW begin
  set new.fadd = get_fadd(new.addr,new.prov,new.dist,new.ward);
end
;;
delimiter ;

DROP TRIGGER IF EXISTS `s_ou_ai`;
delimiter ;;
CREATE TRIGGER `s_ou_ai` AFTER INSERT ON `s_ou` FOR EACH ROW begin
	insert into s_ou_tree (pid, cid, depth, path) values  (new.id, new.id, 0, concat(new.id, '/'));
	insert into s_ou_tree (pid, cid, depth, path) select  pid,new.id,depth + 1,concat(path, new.id, '/') 
from s_ou_tree
where cid = new.pid;
end
;;
delimiter ;

DROP PROCEDURE IF EXISTS `prc_check_id`;
delimiter ;;
CREATE PROCEDURE `prc_check_id`(p_id int)
BEGIN DECLARE v_count INT2;

SELECT
	COUNT(*)
INTO
	v_count
from
	s_inv
where
	id = p_id;

if v_count <= 0 THEN signal sqlstate '20000' set
message_text = 'Cannot find Ref Id in S_INV';

end IF;

END
;;
delimiter ;

DROP TRIGGER IF EXISTS `trg_s_invfile_check_id_bu`;
delimiter ;;
CREATE TRIGGER `trg_s_invfile_check_id_bu` BEFORE UPDATE ON `s_inv_file` FOR EACH ROW BEGIN CALL prc_check_id(NEW.id);

END
;;
delimiter ;


DROP TRIGGER IF EXISTS `trg_s_invfile_check_id_bi`;
delimiter ;;
CREATE TRIGGER `trg_s_invfile_check_id_bi` BEFORE INSERT ON `s_inv_file` FOR EACH ROW BEGIN CALL prc_check_id(NEW.id);

END
;;
delimiter ;





DROP FUNCTION IF EXISTS `cap_first`;
delimiter ;;
CREATE FUNCTION `cap_first`(input varchar(255))
 RETURNS varchar(255) CHARSET utf8mb4
  DETERMINISTIC
begin
	declare len int;
	declare i int;
	set len   = char_length(input);
	set input = lower(input);
	set i = 0;
	while (i < len) do
		if (mid(input,i,1) = ' ' or i = 0) then
			if (i < len) then
				set input = concat(left(input,i),upper(mid(input,i + 1,1)),right(input,len - i - 1));
			end if;
		end if;
		set i = i + 1;
	end while;
	return input;
end
;;
delimiter ;


drop table van_msg_out_dtl;
CREATE TABLE `van_msg_out_dtl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `iddatatvan` decimal(18,0) NOT NULL,
  `invid` varchar(36) NOT NULL,
  `createdby` varchar(150) NOT NULL,
  `statustvan` decimal(18,0) DEFAULT NULL,
  `statusres` decimal(18,0) DEFAULT NULL,
  `messsendcode` varchar(10) DEFAULT NULL,
  `messsend` longtext,
  `messres` varchar(255) DEFAULT NULL,
  `messrescode` varchar(10) DEFAULT NULL,
  `senddate` datetime(3) DEFAULT NULL,
  `resdate` datetime(3) DEFAULT NULL,
  `transid` varchar(225) DEFAULT NULL,
  `idtypeinv` decimal(18,0) NOT NULL,
  `status_response` int(11) DEFAULT '0',
  `datecheckmsg` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `van_msg_out_dtl_transid_IDX` (`transid`) USING BTREE,
  KEY `van_msg_out_dtl_status_response_IDX` (`status_response`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=710 DEFAULT CHARSET=utf8;

drop table van_msg_in_dtl;
CREATE TABLE `van_msg_in_dtl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `receivejson` json NOT NULL,
  `taxc` varchar(14) DEFAULT NULL,
  `namenotice` varchar(500) CHARACTER SET utf8mb4 GENERATED ALWAYS AS (if((json_unquote(json_extract(`receivejson`,'$.tenTBao')) = 'null'),NULL,json_unquote(json_extract(`receivejson`,'$.tenTBao')))) VIRTUAL,
  `typenotice` varchar(255) CHARACTER SET utf8mb4 GENERATED ALWAYS AS (if((json_unquote(json_extract(`receivejson`,'$.loaiTBao')) = 'null'),NULL,json_unquote(json_extract(`receivejson`,'$.loaiTBao')))) VIRTUAL,
  `contentnotice` longtext CHARACTER SET utf8mb4 GENERATED ALWAYS AS (if((json_unquote(json_extract(`receivejson`,'$.ndungTBao')) = 'null'),NULL,json_unquote(json_extract(`receivejson`,'$.ndungTBao')))) VIRTUAL,
  `createddtnotice` datetime GENERATED ALWAYS AS (if((json_unquote(json_extract(`receivejson`,'$.ngayTaoTBao')) = 'null'),NULL,json_unquote(json_extract(`receivejson`,'$.ngayTaoTBao')))) VIRTUAL,
  `createddate` datetime DEFAULT CURRENT_TIMESTAMP,
  `status_scan` decimal(18,0) DEFAULT NULL,
  `updateddate` datetime(3) DEFAULT NULL,
  `transid` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `van_msg_in_dtl_status_scan_IDX` (`status_scan`) USING BTREE,
  KEY `van_msg_in_dtl_transid_IDX` (`transid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16434 DEFAULT CHARSET=utf8;

drop table van_msg_out;
CREATE TABLE `van_msg_out` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invid` varchar(36) NOT NULL,
  `xml2base64` longtext NOT NULL,
  `createdby` varchar(150) NOT NULL,
  `statustvan` decimal(18,0) DEFAULT NULL,
  `createddate` datetime(3) DEFAULT NULL,
  `senddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idtypeinv` decimal(18,0) NOT NULL,
  `messsendcode` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `van_msg_out_createddate_IDX` (`createddate`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=712 DEFAULT CHARSET=utf8;


drop table van_msg_in;
CREATE TABLE `van_msg_in` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `receive_arr_json` longtext NOT NULL,
  `status_scan` decimal(18,0) DEFAULT NULL COMMENT '0: not scan, pending: 3, success: 1, error: 2',
  `createddate` datetime(3) DEFAULT NULL,
  `updateddate` datetime(3) DEFAULT NULL,
  `createby` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `van_msg_in_createddate_IDX` (`createddate`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2904 DEFAULT CHARSET=utf8;

drop table van_einv_listid_msg;
CREATE TABLE `van_einv_listid_msg` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `status_scan` int(11) DEFAULT NULL,
  `createdate` datetime DEFAULT CURRENT_TIMESTAMP,
  `sendvandate` datetime DEFAULT NULL,
  `msg` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `xml` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `van_einv_listid_msg_status_scan_IDX` (`status_scan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=563 DEFAULT CHARSET=utf8;



drop table s_list_inv;
CREATE TABLE `s_list_inv` (
  `id` bigint(20) NOT NULL,
  `listinv_code` varchar(15) DEFAULT NULL,
  `s_tax` varchar(14) NOT NULL,
  `ou` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `approve_date` datetime(3) DEFAULT NULL,
  `approve_by` varchar(20) DEFAULT NULL,
  `s_name` varchar(400) NOT NULL,
  `item_type` varchar(20) NOT NULL,
  `listinvoice_num` varchar(5) DEFAULT NULL,
  `period_type` varchar(1) NOT NULL,
  `period` varchar(10) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `doc` longtext,
  `xml` longtext,
  `times` int(11) NOT NULL DEFAULT '0',
  `status_received` int(11) DEFAULT NULL,
  `xml_received` longtext,
  `json_received` longtext,
  `edittimes` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `s_list_inv_s_tax_IDX` (`s_tax`,`created_date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


drop table s_list_inv;
CREATE TABLE `s_list_inv` (
  `id` bigint(20) NOT NULL,
  `listinv_code` varchar(15) DEFAULT NULL,
  `s_tax` varchar(14) NOT NULL,
  `ou` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `approve_date` datetime(3) DEFAULT NULL,
  `approve_by` varchar(20) DEFAULT NULL,
  `s_name` varchar(400) NOT NULL,
  `item_type` varchar(20) NOT NULL,
  `listinvoice_num` varchar(5) DEFAULT NULL,
  `period_type` varchar(1) NOT NULL,
  `period` varchar(10) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `doc` longtext,
  `xml` longtext,
  `times` int(11) NOT NULL DEFAULT '0',
  `status_received` int(11) DEFAULT NULL,
  `xml_received` longtext,
  `json_received` longtext,
  `edittimes` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `s_list_inv_s_tax_IDX` (`s_tax`,`created_date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8



drop table s_inv_adj;
CREATE TABLE `s_inv_adj` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `inv_id` bigint DEFAULT NULL,
  `form` int DEFAULT NULL,
  `serial` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `stax` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `type` int DEFAULT NULL,
  `note` longtext COLLATE utf8mb4_unicode_ci,
  `so_tb` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `ngay_tb` date DEFAULT NULL,
  `createdate` datetime(3) DEFAULT NULL,
  `statuscqt` int DEFAULT NULL,
  `msgcqt` longtext COLLATE utf8mb4_unicode_ci,
  `listinv_id` bigint DEFAULT NULL,
  `th_type` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci







ALTER TABLE s_inv MODIFY COLUMN period varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NULL;

alter table s_inv add th_type int NULL

alter table s_list_inv add  error_msg_van longtext NULL
alter table s_inv add  error_msg_van longtext NULL
alter table van_msg_out_dtl add status_response int DEFAULT 0 NULL
alter table van_msg_out_dtl add datecheckmsg datetime DEFAULT now() NULL




