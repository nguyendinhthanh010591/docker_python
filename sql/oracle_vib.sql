/* Mail */
ALTER TABLE S_INV
ADD (SENDMAILNUM NUMBER(2) GENERATED ALWAYS AS (JSON_VALUE("DOC" FORMAT JSON , '$.SendMailNum' RETURNING NUMBER(2) DEFAULT '0' ON ERROR)) VIRTUAL);

COMMENT ON COLUMN 
S_INV.SENDMAILNUM IS 
'Số lần gửi mail';

ALTER TABLE S_INV
ADD (SENDMAILSTATUS NUMBER(2) GENERATED ALWAYS AS (JSON_VALUE("DOC" FORMAT JSON , '$.SendMailStatus' RETURNING NUMBER(2) DEFAULT '0' ON ERROR)) VIRTUAL);

COMMENT ON COLUMN 
S_INV.SENDMAILSTATUS IS 
'Trạng thái gửi mail';

ALTER TABLE S_INV
ADD (SENDMAILDATE DATE GENERATED ALWAYS AS (JSON_VALUE("DOC" FORMAT JSON , '$.SendMailDate' RETURNING DATE NULL ON ERROR)) VIRTUAL);

COMMENT ON COLUMN 
S_INV.SENDMAILDATE IS 
'Ngày gửi mail';

/* SMS */
ALTER TABLE S_INV
ADD (SENDSMSNUM NUMBER(2) GENERATED ALWAYS AS (JSON_VALUE("DOC" FORMAT JSON , '$.SendSMSNum' RETURNING NUMBER(2) DEFAULT '0' ON ERROR)) VIRTUAL);

COMMENT ON COLUMN 
S_INV.SENDSMSNUM IS 
'Số lần gửi SMS';

ALTER TABLE S_INV
ADD (SENDSMSSTATUS NUMBER(2) GENERATED ALWAYS AS (JSON_VALUE("DOC" FORMAT JSON , '$.SendSMSStatus' RETURNING NUMBER(2) DEFAULT '0' ON ERROR)) VIRTUAL);

COMMENT ON COLUMN 
S_INV.SENDSMSSTATUS IS 
'Trạng thái gửi SMS';

ALTER TABLE S_INV
ADD (SENDSMSDATE DATE GENERATED ALWAYS AS (JSON_VALUE("DOC" FORMAT JSON , '$.SendSMSDate' RETURNING DATE NULL ON ERROR)) VIRTUAL);

COMMENT ON COLUMN 
S_INV.SENDSMSDATE IS 
'Ngày gửi SMS';

ALTER TABLE S_GNS
 ADD (VRT  NUMBER(5)                                DEFAULT 10);
 
 COMMENT ON COLUMN 
S_GNS.VRT IS 
'Loại thuế suất';

ALTER TABLE S_INV 
ADD (FILE_NAME VARCHAR2(500) );

ALTER TABLE S_INV 
ADD (ID_FILE VARCHAR2(255) );

-- EINVOICE.S_TRANS_CAN definition

CREATE TABLE "S_TRANS_CAN" 
   (	"TRAN_NO" VARCHAR2(20), 
	"NOTE" VARCHAR2(200), 
	"TOTAL" NUMBER(17,2), 
	"SID" VARCHAR2(50), 
	"SYSTEMCODE" VARCHAR2(200), 
	"BCODE" VARCHAR2(20), 
	"C4" VARCHAR2(20), 
	"BRANCHCODE" VARCHAR2(20), 
	"STT" NUMBER(17,2), 
	"CURR" VARCHAR2(30), 
	"UC" VARCHAR2(50), 
	"MA_KS" VARCHAR2(50), 
	"TRANSDATE" DATE, 
	"STATUS" VARCHAR2(10), 
	"MODULE" VARCHAR2(100), 
	 CONSTRAINT "PK_S_TRANS_CAN" PRIMARY KEY ("TRAN_NO")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS"  ENABLE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "USERS" ;

COMMENT ON COLUMN "S_TRANS_CAN"."TRAN_NO" IS 'Số bút toán';
   COMMENT ON COLUMN "S_TRANS_CAN"."NOTE" IS 'Ghi chú';
   COMMENT ON COLUMN "S_TRANS_CAN"."TOTAL" IS 'Số tiền';
   COMMENT ON COLUMN "S_TRANS_CAN"."SID" IS 'Mã định danh bút toán';
   COMMENT ON COLUMN "S_TRANS_CAN"."SYSTEMCODE" IS 'Chi nhánh';
   COMMENT ON COLUMN "S_TRANS_CAN"."BCODE" IS 'Mã khách hàng';
   COMMENT ON COLUMN "S_TRANS_CAN"."C4" IS 'Trường để lưu tài khoản GL
';
   COMMENT ON COLUMN "S_TRANS_CAN"."BRANCHCODE" IS 'Mã phòng';
   COMMENT ON COLUMN "S_TRANS_CAN"."STT" IS 'STT';
   COMMENT ON COLUMN "S_TRANS_CAN"."CURR" IS 'Loại tiền';
   COMMENT ON COLUMN "S_TRANS_CAN"."UC" IS 'Người lập';
   COMMENT ON COLUMN "S_TRANS_CAN"."MA_KS" IS 'Người duyệt';
   COMMENT ON COLUMN "S_TRANS_CAN"."TRANSDATE" IS 'Ngày giao dịch';
   COMMENT ON COLUMN "S_TRANS_CAN"."STATUS" IS 'Trạng thái';
   COMMENT ON COLUMN "S_TRANS_CAN"."MODULE" IS 'Phân loại';
   COMMENT ON TABLE "S_TRANS_CAN"  IS 'Bảng để lưu thông tin hủy bút toán GL/FL';

   update S_INV set ID_FILE = null;
commit;
alter table S_INV set unused column ID_FILE;
ALTER TABLE S_INV 
ADD (ID_FILE CLOB )

--------------------------------------------------------------------------
-- Run this script in EINVOICE@10.15.68.114:1521/BACA to make it look like EINVOICE@10.15.68.114:1521/EINVVIB.
--
-- Please review the script before using it to make sure it won't cause any unacceptable data loss.
--
-- EINVOICE@10.15.68.114:1521/BACA schema extracted by user EINVOICE
-- EINVOICE@10.15.68.114:1521/EINVVIB schema extracted by user EINVOICE
--------------------------------------------------------------------------
-- "Set define off" turns off substitution variables.
Set define off;

CREATE TABLE S_INV_SUM
(
  IDT     DATE                                  NOT NULL,
  STAX    VARCHAR2(20 BYTE)                     NOT NULL,
  TYPE    VARCHAR2(6 BYTE)                      NOT NULL,
  FORM    VARCHAR2(11 BYTE)                     NOT NULL,
  SERIAL  VARCHAR2(8 BYTE),
  I0      NUMBER,
  I1      NUMBER,
  I2      NUMBER,
  I3      NUMBER,
  I4      NUMBER
)
NOCOMPRESS 
NO INMEMORY
TABLESPACE USERS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
PARTITION BY RANGE (IDT)
INTERVAL( NUMTOYMINTERVAL(1,'MONTH'))
(  
  PARTITION P0 VALUES LESS THAN (TO_DATE(' 2020-04-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    NO INMEMORY
    LOGGING
    NOCOMPRESS 
    TABLESPACE INV
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION VALUES LESS THAN (TO_DATE(' 2020-05-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    NO INMEMORY
    LOGGING
    NOCOMPRESS 
    TABLESPACE INV
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION VALUES LESS THAN (TO_DATE(' 2020-06-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    NO INMEMORY
    LOGGING
    NOCOMPRESS 
    TABLESPACE INV
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION VALUES LESS THAN (TO_DATE(' 2020-07-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    NO INMEMORY
    LOGGING
    NOCOMPRESS 
    TABLESPACE INV
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION VALUES LESS THAN (TO_DATE(' 2020-10-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    NO INMEMORY
    LOGGING
    NOCOMPRESS 
    TABLESPACE INV
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION VALUES LESS THAN (TO_DATE(' 2020-11-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    NO INMEMORY
    LOGGING
    NOCOMPRESS 
    TABLESPACE INV
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION VALUES LESS THAN (TO_DATE(' 2020-12-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    NO INMEMORY
    LOGGING
    NOCOMPRESS 
    TABLESPACE INV
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION VALUES LESS THAN (TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    NO INMEMORY
    LOGGING
    NOCOMPRESS 
    TABLESPACE INV
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               )
)
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL
MONITORING
/

CREATE UNIQUE INDEX S_INV_SUM_UIDX01 ON S_INV_SUM
(IDT, STAX, TYPE, FORM, SERIAL)
  TABLESPACE IDX
  PCTFREE    10
  INITRANS   2
  MAXTRANS   255
  STORAGE    (
              BUFFER_POOL      DEFAULT
              FLASH_CACHE      DEFAULT
              CELL_FLASH_CACHE DEFAULT
             )
LOGGING
LOCAL (  
  PARTITION P0
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDX
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDX
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDX
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDX
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDX
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDX
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDX
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDX
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               )
)
NOPARALLEL
/

DROP INDEX S_INV_IDT_FN
/

CREATE INDEX S_INV_IDT_FN ON S_INV
(IDT, TYPE, FORM, SERIAL, STATUS)
  PCTFREE    10
  INITRANS   2
  MAXTRANS   255
  STORAGE    (
              BUFFER_POOL      DEFAULT
              FLASH_CACHE      DEFAULT
              CELL_FLASH_CACHE DEFAULT
             )
LOCAL (  
  PARTITION P0
    LOGGING
    NOCOMPRESS 
    TABLESPACE INV
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION
    LOGGING
    NOCOMPRESS 
    TABLESPACE INV
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION
    LOGGING
    NOCOMPRESS 
    TABLESPACE INV
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION
    LOGGING
    NOCOMPRESS 
    TABLESPACE INV
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION
    LOGGING
    NOCOMPRESS 
    TABLESPACE INV
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION
    LOGGING
    NOCOMPRESS 
    TABLESPACE INV
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION
    LOGGING
    NOCOMPRESS 
    TABLESPACE INV
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION
    LOGGING
    NOCOMPRESS 
    TABLESPACE INV
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION
    LOGGING
    NOCOMPRESS 
    TABLESPACE INV
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               )
)
NOPARALLEL
/

DROP INDEX S_INV_UID_01
/

CREATE UNIQUE INDEX S_INV_UID_01 ON S_INV
(STAX, FORM, SERIAL, NVL(JSON_VALUE("DOC" FORMAT JSON , '$.seq' RETURNING VARCHAR2(255) NULL ON ERROR),TO_CHAR("ID")))
LOGGING
TABLESPACE USERS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL
/

CREATE OR REPLACE PROCEDURE          PRC_INV_SUM (P_FD DATE, P_TD DATE)
IS
    v_td   DATE := P_FD;
    v_ed   DATE := P_TD;
BEGIN
    FOR d IN 0 .. 365
    LOOP
        BEGIN
            DELETE S_INV_SUM
             WHERE IDT = v_td + d;

            COMMIT;

            INSERT INTO S_INV_SUM (IDT,
                                   STAX,
                                   "TYPE",
                                   FORM,
                                   SERIAL,
                                   I0,
                                   I1,
                                   I2,
                                   I3,
                                   I4)
                  SELECT IDT,
                         STAX,
                         TYPE,
                         FORM,
                         SERIAL,
                         NVL (SUM (1), 0)
                             "i0",
                         NVL (SUM (CASE status WHEN 1 THEN 1 ELSE 0 END), 0)
                             "i1",
                         NVL (SUM (CASE status WHEN 2 THEN 1 ELSE 0 END), 0)
                             "i2",
                         NVL (SUM (CASE status WHEN 3 THEN 1 ELSE 0 END), 0)
                             "i3",
                         NVL (SUM (CASE status WHEN 4 THEN 1 ELSE 0 END), 0)
                             "i4"
                    FROM S_INV
                   WHERE IDT = v_td + d
                GROUP BY IDT,
                         STAX,
                         TYPE,
                         FORM,
                         SERIAL;

            COMMIT;
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        EXIT WHEN v_td + d > v_ed;
    END LOOP;
END PRC_INV_SUM;
/

BEGIN
  SYS.DBMS_SCHEDULER.CREATE_JOB
    (
       job_name        => 'EINVOICE.RUN_INV_SUM'
      ,start_date      => NULL
      ,repeat_interval => NULL
      ,end_date        => NULL
      ,job_class       => 'DEFAULT_JOB_CLASS'
      ,job_type        => 'PLSQL_BLOCK'
      ,job_action      => 'DECLARE
    -- Declarations
    l_P_FD   DATE;
    l_P_TD   DATE;
BEGIN
    -- Initialization
    l_P_FD := to_date(''01-JAN-2020'');
    l_P_TD := to_date(''31-DEC-2020'');

    -- Call
    EINVOICE.PRC_INV_SUM (P_FD => l_P_FD, P_TD => l_P_TD);

    -- Transaction Control
    COMMIT;
END;'
      ,comments        => NULL
    );
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.RUN_INV_SUM'
     ,attribute => 'RESTARTABLE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'EINVOICE.RUN_INV_SUM'
     ,attribute => 'MAX_FAILURES');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'EINVOICE.RUN_INV_SUM'
     ,attribute => 'MAX_RUNS');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.RUN_INV_SUM'
     ,attribute => 'STOP_ON_WINDOW_CLOSE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.RUN_INV_SUM'
     ,attribute => 'JOB_PRIORITY'
     ,value     => 3);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'EINVOICE.RUN_INV_SUM'
     ,attribute => 'SCHEDULE_LIMIT');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.RUN_INV_SUM'
     ,attribute => 'AUTO_DROP'
     ,value     => TRUE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.RUN_INV_SUM'
     ,attribute => 'RESTART_ON_RECOVERY'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.RUN_INV_SUM'
     ,attribute => 'RESTART_ON_FAILURE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.RUN_INV_SUM'
     ,attribute => 'STORE_OUTPUT'
     ,value     => TRUE);

  SYS.DBMS_SCHEDULER.ENABLE
    (name                  => 'EINVOICE.RUN_INV_SUM');
END;
/

BEGIN
  SYS.DBMS_SCHEDULER.DROP_JOB
    (job_name  => 'EINVOICE.INV_SUM_JOB');
END;
/

BEGIN
  SYS.DBMS_SCHEDULER.CREATE_JOB
    (
       job_name        => 'EINVOICE.INV_SUM_JOB'
      ,start_date      => TO_TIMESTAMP_TZ('2020/12/01 09:36:24.422518 UTC','yyyy/mm/dd hh24:mi:ss.ff tzr')
      ,repeat_interval => 'FREQ=DAILY;BYHOUR=1; BYMINUTE=0;BYSECOND=0'
      ,end_date        => NULL
      ,job_class       => 'DEFAULT_JOB_CLASS'
      ,job_type        => 'PLSQL_BLOCK'
      ,job_action      => 'DECLARE
    -- Declarations
    l_P_FD   DATE ;
    l_P_TD   DATE ;
BEGIN
    -- Initialization
    l_P_FD := trunc(sysdate) - 7;
    l_P_TD := trunc(sysdate);

    -- Call
    EINVOICE.PRC_INV_SUM (P_FD => l_P_FD, P_TD => l_P_TD);

    -- Transaction Control
    COMMIT;
END;'
      ,comments        => 'Job tổng hợp hóa đơn VIB'
    );
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.INV_SUM_JOB'
     ,attribute => 'RESTARTABLE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.INV_SUM_JOB'
     ,attribute => 'LOGGING_LEVEL'
     ,value     => SYS.DBMS_SCHEDULER.LOGGING_OFF);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'EINVOICE.INV_SUM_JOB'
     ,attribute => 'MAX_FAILURES');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'EINVOICE.INV_SUM_JOB'
     ,attribute => 'MAX_RUNS');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.INV_SUM_JOB'
     ,attribute => 'STOP_ON_WINDOW_CLOSE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.INV_SUM_JOB'
     ,attribute => 'JOB_PRIORITY'
     ,value     => 3);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'EINVOICE.INV_SUM_JOB'
     ,attribute => 'SCHEDULE_LIMIT');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.INV_SUM_JOB'
     ,attribute => 'AUTO_DROP'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.INV_SUM_JOB'
     ,attribute => 'RESTART_ON_RECOVERY'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.INV_SUM_JOB'
     ,attribute => 'RESTART_ON_FAILURE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.INV_SUM_JOB'
     ,attribute => 'STORE_OUTPUT'
     ,value     => TRUE);

  SYS.DBMS_SCHEDULER.ENABLE
    (name                  => 'EINVOICE.INV_SUM_JOB');
END;
/

SHOW ERRORS;

DROP TABLE S_REP_VAT_TMP CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE S_REP_VAT_TMP
(
  "id"         NUMBER,
  "idt"        VARCHAR2(10 BYTE),
  "form"       VARCHAR2(11 BYTE),
  "serial"     VARCHAR2(6 BYTE),
  "seq"        VARCHAR2(8 BYTE),
  "btax"       VARCHAR2(14 BYTE),
  "bname"      VARCHAR2(500 CHAR),
  "buyer"      VARCHAR2(500 CHAR),
  "status"     VARCHAR2(255 BYTE),
  "cstatus"    NUMBER,
  "vibstatus"  NUMBER,
  "adjdes"     VARCHAR2(255 BYTE),
  "cde"        VARCHAR2(255 BYTE),
  "adjtyp"     NUMBER(1),
  "dif"        VARCHAR2(4000 BYTE),
  "root"       VARCHAR2(4000 BYTE),
  "adj"        VARCHAR2(4000 BYTE),
  "taxs"       VARCHAR2(4000 BYTE),
  "note"       VARCHAR2(255 BYTE),
  "factor"     NUMBER,
  "refno"      VARCHAR2(255 BYTE),
  "glacc"      VARCHAR2(255 BYTE),
  "sumv"       NUMBER,
  "vatv"       NUMBER,
  "ou"         NUMBER
)
ON COMMIT PRESERVE ROWS
NOCACHE;

DROP PROCEDURE PRC_BCBH_REP;

CREATE OR REPLACE PROCEDURE prc_bcbh_rep (p_fd      DATE,
/* Formatted on 03-Dec-2020 17:24:07 (QP5 v5.276) */
                        p_td      DATE,
                        p_stax    VARCHAR2,
                        p_ou      NUMBER,
                        p_type    VARCHAR2)
IS
BEGIN
    DELETE FROM s_rep_vat_tmp;

    INSERT INTO s_rep_vat_tmp ("id",
                               "idt",
                               "form",
                               "serial",
                               "seq",
                               "btax",
                               "bname",
                               "buyer",
                               "status",
                               "cstatus",
                               "sumv",
                               "vatv",
                               "note",
                               "vibstatus")
        SELECT id "id",
               TO_CHAR (idt, 'DD/MM/YYYY') "idt",
               form "form",
               serial "serial",
               seq "seq",
               btax "btax",
               bname "bname",
               buyer "buyer",
               CASE
                   WHEN status = 1 THEN 'Chờ cấp số'
                   WHEN status = 2 THEN 'Chờ duyệt'
                   WHEN status = 3 THEN 'Đã duyệt'
                   WHEN status = 4 THEN 'Đã hủy'
                   WHEN status = 6 THEN 'Chờ hủy'
                   ELSE TO_CHAR (status)
               END
                   "status",
               status "cstatus",
               sumv "sumv",
               vatv "vatv",
               note || '-' || cde || '-' || adjdes "note",
               CASE WHEN status = 4 AND xml IS NULL THEN 7 ELSE status END
                   "vibstatus"
          FROM s_inv
         WHERE     idt BETWEEN p_fd AND p_td
               AND stax = p_stax
               AND ("TYPE" = p_type OR p_type = '*')
               AND EXISTS
                       (SELECT 1
                          FROM s_ou o
                         WHERE o.id = ou
                        START WITH o.id = p_ou
                        CONNECT BY PRIOR o.id = o.pid);
EXCEPTION
    WHEN NO_DATA_FOUND
    THEN
        NULL;
    WHEN OTHERS
    THEN
        -- Consider logging the error and then re-raise
        RAISE;
END prc_bcbh_rep;
/


DROP PROCEDURE PRC_VAT_REP;

CREATE OR REPLACE PROCEDURE PRC_VAT_REP (P_FD     DATE,
                                         P_TD     DATE,
                                         p_STAX   VARCHAR2,
                                         p_ou     NUMBER)
IS
BEGIN
    DELETE FROM s_rep_vat_tmp;

    INSERT INTO S_REP_VAT_TMP ("id",
                               "idt",
                               "form",
                               "serial",
                               "seq",
                               "btax",
                               "bname",
                               "adjdes",
                               "cde",
                               "adjtyp",
                               "dif",
                               "root",
                               "adj",
                               "taxs",
                               "note",
                               "factor",
                               "refno",
                               "glacc",
                               "ou")
        SELECT *
          FROM (SELECT id                                "id",
                       TO_CHAR (a.idt, 'DD/MM/YYYY')     "idt",
                       a.form                            "form",
                       a.serial                          "serial",
                       a.seq                             "seq",
                       a.btax                            "btax",
                       a.bname                           "bname",
                       a.adjdes                          "adjdes",
                       a.cde                             "cde",
                       a.adjtyp                          "adjtyp",
                       a.doc.dif                         "dif",
                       a.doc.root                        "root",
                       a.doc.adj                         "adj",
                       NULL                              "taxs",
                       note                              "note",
                       1                                 AS "factor",
                       c2                                "refno",
                       c1                                "glacc",
                       ou                                "ou"
                  FROM s_inv a
                 WHERE     a.idt BETWEEN P_FD AND P_TD
                       AND a.stax = p_STAX
                       AND a.TYPE = '01GTKT'
                       AND a.status in (3,4)
                       AND EXISTS
                               (    SELECT 1
                                      FROM s_ou o
                                     WHERE o.id = a.ou
                                START WITH o.id = p_ou
                                CONNECT BY PRIOR o.id = o.pid)
                UNION ALL
                SELECT id
                           "id",
                       TO_CHAR (a.cdt, 'DD/MM/YYYY')
                           "idt",
                       a.form
                           "form",
                       a.serial
                           "serial",
                       a.seq
                           "seq",
                       a.btax
                           "btax",
                       a.bname
                           "bname",
                       a.adjdes
                           "adjdes",
                       a.cde
                           "cde",
                       a.adjtyp
                           "adjtyp",
                       a.doc.dif
                           "dif",
                       a.doc.root
                           "root",
                       a.doc.adj
                           "adj",
                       NULL
                           "taxs",
                       CASE
                           WHEN JSON_VALUE (doc, '$.cancel.rea') IS NOT NULL
                           THEN
                               JSON_VALUE (doc, '$.cancel.rea')
                           ELSE
                               cde
                       END
                           "note",
                       -1
                           AS "factor",
                       c2
                           "refno",
                       c1
                           "glacc",
                       ou
                           "ou"
                  FROM s_inv a
                 WHERE     a.cdt BETWEEN P_FD AND P_TD
                       AND a.stax = p_STAX
                       AND a.TYPE = '01GTKT'
                       AND a.status = 4
                       AND a.xml IS NOT NULL
                       AND EXISTS
                               (    SELECT 1
                                      FROM s_ou o
                                     WHERE o.id = a.ou
                                START WITH o.id = p_ou
                                CONNECT BY PRIOR o.id = o.pid)/*
                UNION ALL
                SELECT id                                "id",
                       TO_CHAR (a.idt, 'DD/MM/YYYY')     "idt",
                       a.form                            "form",
                       a.serial                          "serial",
                       a.seq                             "seq",
                       a.btax                            "btax",
                       a.bname                           "bname",
                       a.adjdes                          "adjdes",
                       a.cde                             "cde",
                       a.adjtyp                          "adjtyp",
                       a.doc.dif                         "dif",
                       a.doc.root                        "root",
                       a.doc.adj                         "adj",
                       NULL                              "taxs",
                       note                              "note",
                       1                                 AS "factor",
                       c2                                "refno",
                       c1                                "glacc",
                       ou                                "ou"
                  FROM s_inv a
                 WHERE     a.idt BETWEEN P_FD AND P_TD
                       AND a.stax = p_STAX
                       AND a.TYPE = '01GTKT'
                       AND a.status = 4
                       AND a.xml IS NOT NULL
                       AND EXISTS
                               (    SELECT 1
                                      FROM s_ou o
                                     WHERE o.id = a.ou
                                START WITH o.id = p_ou
                                CONNECT BY PRIOR o.id = o.pid)*/);
EXCEPTION
    WHEN NO_DATA_FOUND
    THEN
        NULL;
    WHEN OTHERS
    THEN
        -- Consider logging the error and then re-raise
        RAISE;
END PRC_VAT_REP;
/


ADD (BACC_ AS ( JSON_VALUE("DOC" FORMAT JSON , '$.bacc_'  RETURNING VARCHAR2(14) NULL ON ERROR) ) VIRTUAL )	null

ADD (BUYER_ AS ( JSON_VALUE("DOC" FORMAT JSON , '$.buyer_'  RETURNING VARCHAR2(14) NULL ON ERROR) ) VIRTUAL )	null

ADD (BTEL_ AS ( JSON_VALUE("DOC" FORMAT JSON , '$.btel_' RETURNING VARCHAR2(14) NULL ON ERROR) ) VIRTUAL )	null

ADD (BMAIL_ AS ( JSON_VALUE("DOC" FORMAT JSON , '$.bmail_' RETURNING VARCHAR2(14) NULL ON ERROR) ) VIRTUAL )	null

ADD (BADDR_ AS ( JSON_VALUE("DOC" FORMAT JSON , '$.baddr_' RETURNING VARCHAR2(14) NULL ON ERROR) ) VIRTUAL )	null

ADD (BTAX_ AS ( JSON_VALUE("DOC" FORMAT JSON , '$.btax_' RETURNING VARCHAR2(14) NULL ON ERROR) ) VIRTUAL )	null

ADD (BNAME_ AS ( JSON_VALUE("DOC" FORMAT JSON , '$.bname_' RETURNING VARCHAR2(255) NULL ON ERROR) ) VIRTUAL )	null


ADD (EDIT AS ( JSON_VALUE("DOC" FORMAT JSON , '$.edit' RETURNING VARCHAR2(1) NULL ON ERROR) ) VIRTUAL )	null


ALTER TABLE S_INV 
MODIFY ( BACC_ AS ( JSON_VALUE("DOC" FORMAT JSON , '$.bacc_' RETURNING VARCHAR2(255) NULL ON ERROR) ) VIRTUAL )
ALTER TABLE S_INV 
MODIFY ( BUYER_ AS ( JSON_VALUE("DOC" FORMAT JSON , '$.buyer_' RETURNING VARCHAR2(255) NULL ON ERROR) ) VIRTUAL )

ALTER TABLE S_INV 
MODIFY ( BTEL_ AS ( JSON_VALUE("DOC" FORMAT JSON , '$.btel_' RETURNING VARCHAR2(255) NULL ON ERROR) ) VIRTUAL )
ALTER TABLE S_INV 
MODIFY ( BMAIL_ AS ( JSON_VALUE("DOC" FORMAT JSON , '$.bmail_' RETURNING VARCHAR2(255) NULL ON ERROR) ) VIRTUAL )

ALTER TABLE S_INV 
MODIFY ( BADDR_ AS ( JSON_VALUE("DOC" FORMAT JSON , '$.baddr_' RETURNING VARCHAR2(255) NULL ON ERROR) ) VIRTUAL )

BEGIN
  SYS.DBMS_SCHEDULER.DROP_JOB
    (job_name  => 'EINVOICE.ANALYS_S_INV_JOB');
END;
/

BEGIN
  SYS.DBMS_SCHEDULER.CREATE_JOB
    (
       job_name        => 'EINVOICE.ANALYS_S_INV_JOB'
      ,start_date      => TO_TIMESTAMP_TZ('2021/01/12 03:45:21.688015 UTC','yyyy/mm/dd hh24:mi:ss.ff tzr')
      ,repeat_interval => 'FREQ=HOURLY;INTERVAL=3;BYMINUTE=0;BYSECOND=0'
      ,end_date        => NULL
      ,job_class       => 'DEFAULT_JOB_CLASS'
      ,job_type        => 'PLSQL_BLOCK'
      ,job_action      => 'BEGIN
  
  SYS.DBMS_STATS.GATHER_TABLE_STATS (
     OwnName           => ''EINVOICE''
    ,TabName           => ''S_INV''
    ,Estimate_Percent  => SYS.DBMS_STATS.AUTO_SAMPLE_SIZE
    ,Method_Opt        => ''FOR ALL COLUMNS SIZE AUTO ''
    ,Degree            => NULL
    ,Cascade           => DBMS_STATS.AUTO_CASCADE
    ,No_Invalidate     => DBMS_STATS.AUTO_INVALIDATE
    ,Force             => FALSE);
    
    SYS.DBMS_STATS.GATHER_INDEX_STATS (
     OwnName           => ''EINVOICE''
    ,IndName           => ''S_INV_DOC_IDX''
    ,Estimate_Percent  => SYS.DBMS_STATS.AUTO_SAMPLE_SIZE
    ,Degree            => NULL
    ,No_Invalidate     => DBMS_STATS.AUTO_INVALIDATE
    ,Force             => FALSE);
    
    EXECUTE IMMEDIATE ''ALTER INDEX S_INV_DOC_IDX REBUILD'';
    
END;

'
      ,comments        => NULL
    );
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.ANALYS_S_INV_JOB'
     ,attribute => 'RESTARTABLE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.ANALYS_S_INV_JOB'
     ,attribute => 'LOGGING_LEVEL'
     ,value     => SYS.DBMS_SCHEDULER.LOGGING_OFF);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'EINVOICE.ANALYS_S_INV_JOB'
     ,attribute => 'MAX_FAILURES');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'EINVOICE.ANALYS_S_INV_JOB'
     ,attribute => 'MAX_RUNS');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.ANALYS_S_INV_JOB'
     ,attribute => 'STOP_ON_WINDOW_CLOSE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.ANALYS_S_INV_JOB'
     ,attribute => 'JOB_PRIORITY'
     ,value     => 3);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'EINVOICE.ANALYS_S_INV_JOB'
     ,attribute => 'SCHEDULE_LIMIT');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.ANALYS_S_INV_JOB'
     ,attribute => 'AUTO_DROP'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.ANALYS_S_INV_JOB'
     ,attribute => 'RESTART_ON_RECOVERY'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.ANALYS_S_INV_JOB'
     ,attribute => 'RESTART_ON_FAILURE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.ANALYS_S_INV_JOB'
     ,attribute => 'STORE_OUTPUT'
     ,value     => TRUE);

  SYS.DBMS_SCHEDULER.ENABLE
    (name                  => 'EINVOICE.ANALYS_S_INV_JOB');
END;
/

-- Create table temporary
DROP TABLE S_REP_VAT_TMP CASCADE CONSTRAINTS;
/
CREATE GLOBAL TEMPORARY TABLE S_REP_VAT_TMP
(
  "id"         NUMBER,
  "idt"        VARCHAR2(10 BYTE),
  "form"       VARCHAR2(11 BYTE),
  "serial"     VARCHAR2(6 BYTE),
  "seq"        VARCHAR2(8 BYTE),
  "btax"       VARCHAR2(14 BYTE),
  "bname"      VARCHAR2(500 CHAR),
  "buyer"      VARCHAR2(500 CHAR),
  "status"     VARCHAR2(255 BYTE),
  "cstatus"    NUMBER,
  "vibstatus"  NUMBER,
  "adjdes"     VARCHAR2(255 BYTE),
  "cde"        VARCHAR2(255 BYTE),
  "adjtyp"     NUMBER(1),
  "dif"        VARCHAR2(4000 BYTE),
  "root"       VARCHAR2(4000 BYTE),
  "adj"        VARCHAR2(4000 BYTE),
  "taxs"       VARCHAR2(4000 BYTE),
  "note"       VARCHAR2(255 BYTE),
  "factor"     NUMBER,
  "refno"      VARCHAR2(255 BYTE),
  "glacc"      VARCHAR2(255 BYTE),
  "sumv"       NUMBER,
  "vatv"       NUMBER,
  "ou"         NUMBER,
  "orderno"    NUMBER,
  "ouname"     VARCHAR2(255 BYTE),
  "itemcode"   VARCHAR2(30 BYTE),
  "itemname"   VARCHAR2(255 BYTE),
  "vrn"        VARCHAR2(100 BYTE),
  "vrt"        NUMBER,
  "index"      NUMBER
)
ON COMMIT PRESERVE ROWS
NOCACHE;
/
-- Create Index STAX IDT
/* Formatted on 04/04/2021 4:27:24 PM (QP5 v5.326) */
DROP INDEX S_INV_IDT_IDX;
/


DECLARE
    CURSOR c_par IS
        SELECT *
          FROM user_tab_partitions
         WHERE table_name = 'S_INV' AND partition_name <> 'P0';

    v_sql   VARCHAR2 (4000) := '';
BEGIN
    v_sql := v_sql || 'CREATE INDEX S_INV_IDT_IDX ON S_INV   ';
    v_sql := v_sql || '(IDT, STAX, OU, TYPE, FORM,    ';
    v_sql := v_sql || 'SERIAL, STATUS)   ';
    v_sql := v_sql || '  PCTFREE    10   ';
    v_sql := v_sql || '  INITRANS   2   ';
    v_sql := v_sql || '  MAXTRANS   255   ';
    v_sql := v_sql || '  STORAGE    (   ';
    v_sql := v_sql || '              BUFFER_POOL      DEFAULT   ';
    v_sql := v_sql || '             )   ';
    v_sql := v_sql || 'LOGGING   ';
    v_sql := v_sql || 'LOCAL (     ';
    v_sql := v_sql || '  PARTITION P0   ';
    v_sql := v_sql || '    LOGGING   ';
    v_sql := v_sql || '    NOCOMPRESS    ';
    v_sql := v_sql || '    TABLESPACE INV   ';
    v_sql := v_sql || '    INITRANS   2   ';
    v_sql := v_sql || '    MAXTRANS   255   ';
    v_sql := v_sql || '    STORAGE    (   ';
    v_sql := v_sql || '     BUFFER_POOL      DEFAULT   ';
    v_sql := v_sql || '      ), ';

    FOR vc_par IN c_par
    LOOP
        v_sql := v_sql || '  PARTITION   ';
        v_sql := v_sql || '    LOGGING   ';
        v_sql := v_sql || '    NOCOMPRESS   ';
        v_sql := v_sql || '    TABLESPACE INV   ';
        v_sql := v_sql || '    INITRANS   2   ';
        v_sql := v_sql || '    MAXTRANS   255   ';
        v_sql := v_sql || '    STORAGE    (   ';
        v_sql := v_sql || '      INITIAL          64K   ';
        v_sql := v_sql || '      NEXT             1M   ';
        v_sql := v_sql || '        MINEXTENTS       1   ';
        v_sql := v_sql || '        MAXEXTENTS       UNLIMITED   ';
        v_sql := v_sql || '       BUFFER_POOL      DEFAULT   ';
        v_sql := v_sql || '      ), ';
    END LOOP;

    v_sql := SUBSTR (v_sql, 1, LENGTH (v_sql) - 2) || '   )       ';

    EXECUTE IMMEDIATE v_sql;
/*
    DBMS_OUTPUT.put_line (SUBSTR (v_sql, 1, 200));
    DBMS_OUTPUT.put_line (SUBSTR (v_sql, 201, 200));
    DBMS_OUTPUT.put_line (SUBSTR (v_sql, 401, 200));
    DBMS_OUTPUT.put_line (SUBSTR (v_sql, 601, 200));
    DBMS_OUTPUT.put_line (SUBSTR (v_sql, 801, 200));
    DBMS_OUTPUT.put_line (SUBSTR (v_sql, 1001, 200));
    DBMS_OUTPUT.put_line (SUBSTR (v_sql, 1201, 200));
    DBMS_OUTPUT.put_line (SUBSTR (v_sql, 1401, 200));
    DBMS_OUTPUT.put_line (SUBSTR (v_sql, 1601, 200));
    DBMS_OUTPUT.put_line (SUBSTR (v_sql, 1801, 200));
    DBMS_OUTPUT.put_line (SUBSTR (v_sql, 2001, 200));
    DBMS_OUTPUT.put_line (SUBSTR (v_sql, 2201, 200));
    DBMS_OUTPUT.put_line (SUBSTR (v_sql, 2401, 200));
    DBMS_OUTPUT.put_line (SUBSTR (v_sql, 2601, 200));
    DBMS_OUTPUT.put_line (SUBSTR (v_sql, 2801, 200));
    DBMS_OUTPUT.put_line (SUBSTR (v_sql, 3001, 200));
    DBMS_OUTPUT.put_line (SUBSTR (v_sql, 3201, 200));
    DBMS_OUTPUT.put_line (SUBSTR (v_sql, 3401, 200));
    DBMS_OUTPUT.put_line (SUBSTR (v_sql, 3601, 200));
    DBMS_OUTPUT.put_line (SUBSTR (v_sql, 3801, 200));
    DBMS_OUTPUT.put_line (SUBSTR (v_sql, 4001, 200));
    DBMS_OUTPUT.put_line (SUBSTR (v_sql, 4201, 200));
*/
END;
/
--Create Index STAX CDT
DROP INDEX S_INV_CDT_IDX;
/
CREATE INDEX S_INV_CDT_IDX ON S_INV
(CDT, STAX, OU, TYPE, FORM, 
SERIAL, STATUS)
LOGGING
TABLESPACE USERS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           );
/

DROP PACKAGE EINVOICE.PKG_VAT_REPORT;

CREATE OR REPLACE PACKAGE EINVOICE.pkg_vat_report
AS
    /******************************************************************************
       NAME:       pkg_vat_report
       PURPOSE:

       REVISIONS:
       Ver        Date        Author           Description
       ---------  ----------  ---------------  ------------------------------------
       1.0        3/30/2021      hunglq       1. Created this package.
    ******************************************************************************/

    FUNCTION fnc_getouname (p_ou NUMBER)
        RETURN VARCHAR2;

    FUNCTION fnc_get_factor_adj (p_dif      VARCHAR2,
                                 p_adj      VARCHAR2,
                                 p_adjtyp   NUMBER)
        RETURN NUMBER;

    FUNCTION fnc_count_vat_rep (P_FD     DATE,
                                P_TD     DATE,
                                p_STAX   VARCHAR2,
                                p_ou     NUMBER)
        RETURN NUMBER;

    PROCEDURE prc_vat_rep (P_FD     DATE,
                           P_TD     DATE,
                           p_STAX   VARCHAR2,
                           p_ou     NUMBER);

    FUNCTION fnc_count_vat_bcbh (p_fd     DATE,
                                 p_td     DATE,
                                 p_stax   VARCHAR2,
                                 p_ou     NUMBER,
                                 p_type   VARCHAR2)
        RETURN NUMBER;

    PROCEDURE prc_bcbh_rep (p_fd     DATE,
                            p_td     DATE,
                            p_stax   VARCHAR2,
                            p_ou     NUMBER,
                            p_type   VARCHAR2);
END pkg_vat_report;
/

DROP PACKAGE BODY EINVOICE.PKG_VAT_REPORT;

CREATE OR REPLACE PACKAGE BODY EINVOICE.pkg_vat_report
AS
    FUNCTION fnc_getouname (p_ou NUMBER)
        RETURN VARCHAR2
    IS
        tmpVar   VARCHAR2 (255) := NULL;
    BEGIN
        BEGIN
            SELECT NAME
              INTO tmpVar
              FROM S_OU
             WHERE ID = p_ou;
        EXCEPTION
            WHEN OTHERS
            THEN
                tmpVar := NULL;
        END;

        RETURN tmpVar;
    END fnc_getouname;

    FUNCTION fnc_get_factor_adj (p_dif      VARCHAR2,
                                 p_adj      VARCHAR2,
                                 p_adjtyp   NUMBER)
        RETURN NUMBER
    IS
        tmpVar   NUMBER := 1;
    BEGIN
        BEGIN
            IF (p_adjtyp = 2)
            THEN
                IF (JSON_VALUE (p_dif, '$.total') > 0)
                THEN
                    tmpVar := -1;
                ELSE
                    IF (JSON_VALUE (p_adj, '$.adjType') = '3')
                    THEN
                        tmpVar := -1;
                    END IF;
                END IF;
            END IF;
        EXCEPTION
            WHEN OTHERS
            THEN
                -- Consider logging the error and then re-raise
                tmpVar := 0;
        END;

        RETURN tmpVar;
    END fnc_get_factor_adj;

    FUNCTION fnc_count_vat_rep (P_FD     DATE,
                                P_TD     DATE,
                                p_STAX   VARCHAR2,
                                p_ou     NUMBER)
        RETURN NUMBER
    IS
        v_ret   NUMBER;
    BEGIN
        BEGIN
            SELECT /*+ parallel(s_inv,auto) */
                   COUNT (*)
              INTO v_ret
              FROM (SELECT /*+ INDEX(S_INV_IDT_IDX)*/
                           id                                "id",
                           TO_CHAR (a.idt, 'DD/MM/YYYY')     "idt",
                           a.form                            "form",
                           a.serial                          "serial",
                           a.seq                             "seq",
                           a.btax                            "btax",
                           a.bname                           "bname",
                           a.adjdes                          "adjdes",
                           a.cde                             "cde",
                           a.adjtyp                          "adjtyp",
                           a.doc.dif                         "dif",
                           a.doc.root                        "root",
                           a.doc.adj                         "adj",
                           json_query (doc, '$.items')       "taxs",
                           note                              "note",
                           1                                 AS "factor",
                           c2                                "refno",
                           c1                                "glacc",
                           ou                                "ou"
                      FROM s_inv  a,
                           JSON_TABLE (
                               a.doc.items,
                               '$[*]'
                               COLUMNS (
                                   ROW_NUMBER FOR ORDINALITY,
                                   vrn VARCHAR2 (50) PATH '$.vrn',
                                   vrt NUMBER PATH '$.vrt',
                                   code VARCHAR2 (50) PATH '$.code',
                                   name VARCHAR2 (255) PATH '$.name',
                                   amount NUMBER PATH '$.amount',
                                   vat NUMBER PATH '$.vat',
                                   itemtype VARCHAR2 (100) PATH '$.type'))
                           AS it
                     WHERE     a.idt BETWEEN P_FD AND P_TD
                           AND a.stax = p_STAX
                           AND a.TYPE = '01GTKT'
                           AND a.status IN (3, 4)
                           AND EXISTS
                                   (    SELECT 1
                                          FROM s_ou o
                                         WHERE o.id = a.ou
                                    START WITH o.id = p_ou
                                    CONNECT BY PRIOR o.id = o.pid)
                    UNION ALL
                    SELECT /*+ INDEX(S_INV_CDT_IDX)*/
                           id                               "id",
                           TO_CHAR (a.cdt, 'DD/MM/YYYY')    "idt",
                           a.form                           "form",
                           a.serial                         "serial",
                           a.seq                            "seq",
                           a.btax                           "btax",
                           a.bname                          "bname",
                           a.adjdes                         "adjdes",
                           a.cde                            "cde",
                           a.adjtyp                         "adjtyp",
                           a.doc.dif                        "dif",
                           a.doc.root                       "root",
                           a.doc.adj                        "adj",
                           json_query (doc, '$.items')      "taxs",
                           CASE
                               WHEN JSON_VALUE (doc, '$.cancel.rea')
                                        IS NOT NULL
                               THEN
                                   JSON_VALUE (doc, '$.cancel.rea')
                               ELSE
                                   cde
                           END                              "note",
                           -1                               AS "factor",
                           c2                               "refno",
                           c1                               "glacc",
                           ou                               "ou"
                      FROM s_inv  a,
                           JSON_TABLE (
                               a.doc.items,
                               '$[*]'
                               COLUMNS (
                                   ROW_NUMBER FOR ORDINALITY,
                                   vrn VARCHAR2 (50) PATH '$.vrn',
                                   vrt NUMBER PATH '$.vrt',
                                   code VARCHAR2 (50) PATH '$.code',
                                   name VARCHAR2 (255) PATH '$.name',
                                   amount NUMBER PATH '$.amount',
                                   vat NUMBER PATH '$.vat',
                                   itemtype VARCHAR2 (100) PATH '$.type'))
                           AS it
                     WHERE     a.cdt BETWEEN P_FD AND P_TD
                           AND a.stax = p_STAX
                           AND a.TYPE = '01GTKT'
                           AND a.status = 4
                           AND a.xml IS NOT NULL
                           AND EXISTS
                                   (    SELECT 1
                                          FROM s_ou o
                                         WHERE o.id = a.ou
                                    START WITH o.id = p_ou
                                    CONNECT BY PRIOR o.id = o.pid));

            DBMS_OUTPUT.put_line (v_ret);
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                v_ret := NULL;
            WHEN OTHERS
            THEN
                -- Consider logging the error and then re-raise
                v_ret := NULL;
        END;

        RETURN v_ret;
    END fnc_count_vat_rep;

    PROCEDURE prc_vat_rep (P_FD     DATE,
                           P_TD     DATE,
                           p_STAX   VARCHAR2,
                           p_ou     NUMBER)
    IS
        CURSOR c_inv IS
            SELECT /*+ parallel(s_inv,auto) */
                   "id"         "id",
                   "idt"        "idt",
                   "form"       "form",
                   "serial"     "serial",
                   "seq"        "seq",
                   "btax"       "btax",
                   "bname"      "bname",
                   NULL         "buyer",
                   NULL         "status",
                   NULL         "cstatus",
                   NULL         "vibstatus",
                   "adjdes"     "adjdes",
                   "cde"        "cde",
                   "adjtyp"     "adjtyp",
                   "dif"        "dif",
                   "root"       "root",
                   "adj"        "adj",
                   "taxs"       "taxs",
                   "note"       "note",
                   "factor"     "factor",
                   "refno"      "refno",
                   "glacc"      "glacc",
                   NULL         "sumv",
                   NULL         "vatv",
                   "ou"         "ou",
                   NULL         "orderno",
                   NULL         "ouname",
                   NULL         "itemcode",
                   NULL         "itemname",
                   NULL         "vrn",
                   NULL         "vrt",
                   NULL         "index"
              FROM (SELECT /*+ PARALLEL_INDEX(s_inv, S_INV_IDT_IDX, 10) */
                           id                                "id",
                           TO_CHAR (a.idt, 'DD/MM/YYYY')     "idt",
                           a.form                            "form",
                           a.serial                          "serial",
                           a.seq                             "seq",
                           a.btax                            "btax",
                           a.bname                           "bname",
                           a.adjdes                          "adjdes",
                           a.cde                             "cde",
                           a.adjtyp                          "adjtyp",
                           a.doc.dif                         "dif",
                           a.doc.root                        "root",
                           a.doc.adj                         "adj",
                           json_query (doc, '$.items')       "taxs",
                           note                              "note",
                           1                                 AS "factor",
                           c2                                "refno",
                           c1                                "glacc",
                           ou                                "ou"
                      FROM s_inv a
                     WHERE     a.idt BETWEEN P_FD AND P_TD
                           AND a.stax = p_STAX
                           AND a.TYPE = '01GTKT'
                           AND a.status IN (3, 4)
                           AND EXISTS
                                   (    SELECT 1
                                          FROM s_ou o
                                         WHERE o.id = a.ou
                                    START WITH o.id = p_ou
                                    CONNECT BY PRIOR o.id = o.pid)
                    UNION ALL
                    SELECT /*+ PARALLEL_INDEX(s_inv, S_INV_CDT_IDX, 10) */
                           id                               "id",
                           TO_CHAR (a.cdt, 'DD/MM/YYYY')    "idt",
                           a.form                           "form",
                           a.serial                         "serial",
                           a.seq                            "seq",
                           a.btax                           "btax",
                           a.bname                          "bname",
                           a.adjdes                         "adjdes",
                           a.cde                            "cde",
                           a.adjtyp                         "adjtyp",
                           a.doc.dif                        "dif",
                           a.doc.root                       "root",
                           a.doc.adj                        "adj",
                           json_query (doc, '$.items')      "taxs",
                           CASE
                               WHEN JSON_VALUE (doc, '$.cancel.rea')
                                        IS NOT NULL
                               THEN
                                   JSON_VALUE (doc, '$.cancel.rea')
                               ELSE
                                   cde
                           END                              "note",
                           -1                               AS "factor",
                           c2                               "refno",
                           c1                               "glacc",
                           ou                               "ou"
                      FROM s_inv a
                     WHERE     a.cdt BETWEEN P_FD AND P_TD
                           AND a.stax = p_STAX
                           AND a.TYPE = '01GTKT'
                           AND a.status = 4
                           AND a.xml IS NOT NULL
                           AND EXISTS
                                   (    SELECT 1
                                          FROM s_ou o
                                         WHERE o.id = a.ou
                                    START WITH o.id = p_ou
                                    CONNECT BY PRIOR o.id = o.pid));

        TYPE inv_tab_type IS TABLE OF c_inv%ROWTYPE;

        vc_inv   inv_tab_type;
    BEGIN
        EXECUTE IMMEDIATE 'TRUNCATE TABLE s_rep_vat_tmp';

        OPEN c_inv;

        LOOP
            FETCH c_inv BULK COLLECT INTO vc_inv LIMIT 5000;

            FORALL i IN 1 .. vc_inv.COUNT
                INSERT INTO s_rep_vat_tmp
                     VALUES vc_inv (i);

            EXIT WHEN c_inv%NOTFOUND;
        END LOOP;

        CLOSE c_inv;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            NULL;
        WHEN OTHERS
        THEN
            -- Consider logging the error and then re-raise
            RAISE;
    END prc_vat_rep;

    FUNCTION fnc_count_vat_bcbh (p_fd     DATE,
                                 p_td     DATE,
                                 p_stax   VARCHAR2,
                                 p_ou     NUMBER,
                                 p_type   VARCHAR2)
        RETURN NUMBER
    IS
        v_ret   NUMBER;
    BEGIN
        BEGIN
            SELECT /*+ INDEX(S_INV_IDT_IDX) parallel(s_inv,auto) */
                   COUNT (*)
              INTO v_ret
              FROM s_inv  a,
                   JSON_TABLE (
                       a.doc.items,
                       '$[*]'
                       COLUMNS (ROW_NUMBER FOR ORDINALITY,
                                vrn VARCHAR2 (50) PATH '$.vrn',
                                vrt NUMBER PATH '$.vrt',
                                code VARCHAR2 (50) PATH '$.code',
                                name VARCHAR2 (255) PATH '$.name',
                                amount NUMBER PATH '$.amount',
                                vat NUMBER PATH '$.vat',
                                itemtype VARCHAR2 (100) PATH '$.type')) AS it
             WHERE     idt BETWEEN p_fd AND p_td
                   AND stax = p_stax
                   AND ("TYPE" = p_type OR p_type = '*')
                   AND EXISTS
                           (    SELECT 1
                                  FROM s_ou o
                                 WHERE o.id = ou
                            START WITH o.id = p_ou
                            CONNECT BY PRIOR o.id = o.pid);
        EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
                v_ret := NULL;
            WHEN OTHERS
            THEN
                -- Consider logging the error and then re-raise
                v_ret := NULL;
        END;

        RETURN v_ret;
    END fnc_count_vat_bcbh;

    PROCEDURE prc_bcbh_rep (p_fd     DATE,
                            p_td     DATE,
                            p_stax   VARCHAR2,
                            p_ou     NUMBER,
                            p_type   VARCHAR2)
    IS
        CURSOR c_inv (p_status VARCHAR2)
        IS
            SELECT /*+ parallel(s_inv,auto) */
                   "id"            "id",
                   "idt"           "idt",
                   "form"          "form",
                   "serial"        "serial",
                   "seq"           "seq",
                   "btax"          "btax",
                   "bname"         "bname",
                   "buyer"         "buyer",
                   "status"        "status",
                   "cstatus"       "cstatus",
                   "vibstatus"     "vibstatus",
                   "adjdes"        "adjdes",
                   "cde"           "cde",
                   "adjtyp"        "adjtyp",
                   "dif"           "dif",
                   "root"          "root",
                   "adj"           "adj",
                   "taxs"          "taxs",
                   "note"          "note",
                   "factor"        "factor",
                   "refno"         "refno",
                   "glacc"         "glacc",
                   "sumv"          "sumv",
                   "vatv"          "vatv",
                   "ou"            "ou",
                   "orderno"       "orderno",
                   "ouname"        "ouname",
                   "itemcode"      "itemcode",
                   "itemname"      "itemname",
                   "vrn"           "vrn",
                   "vrt"           "vrt",
                   "index"         "index"
              FROM (SELECT /*+ PARALLEL_INDEX(s_inv, S_INV_IDT_IDX, 10) */
                           id                                     "id",
                           TO_CHAR (idt, 'DD/MM/YYYY')            "idt",
                           form                                   "form",
                           serial                                 "serial",
                           seq                                    "seq",
                           btax                                   "btax",
                           bname                                  "bname",
                           buyer                                  "buyer",
                           CASE
                               WHEN status = 1 THEN 'Chờ cấp số'
                               WHEN status = 2 THEN 'Chờ duyệt'
                               WHEN status = 3 THEN 'Đã duyệt'
                               WHEN status = 4 THEN 'Đã hủy'
                               WHEN status = 6 THEN 'Chờ hủy'
                               ELSE TO_CHAR (status)
                           END                                    "status",
                           status                                 "cstatus",
                           CASE
                               WHEN status = 4 AND xml IS NULL THEN 7
                               ELSE status
                           END                                    "vibstatus",
                           NULL                                   "adjdes",
                           NULL                                   "cde",
                           NULL                                   "adjtyp",
                           NULL                                   "dif",
                           NULL                                   "root",
                           NULL                                   "adj",
                           json_query (doc, '$.items')            "taxs",
                           note || '-' || cde || '-' || adjdes    "note",
                           NULL                                   "factor",
                           NULL                                   "refno",
                           NULL                                   "glacc",
                           sumv                                   "sumv",
                           vatv                                   "vatv",
                           ou                                     "ou",
                           NULL                                   "orderno",
                           NULL                                   "ouname",
                           NULL                                   "itemcode",
                           NULL                                   "itemname",
                           NULL                                   "vrn",
                           NULL                                   "vrt",
                           NULL                                   "index"
                      FROM s_inv
                     WHERE     idt BETWEEN p_fd AND p_td
                           AND stax = p_stax
                           AND status = p_status
                           AND ("TYPE" = p_type OR p_type = '*')
                           AND EXISTS
                                   (    SELECT 1
                                          FROM s_ou o
                                         WHERE o.id = ou
                                    START WITH o.id = p_ou
                                    CONNECT BY PRIOR o.id = o.pid)
                    OFFSET 0 ROWS
                     FETCH NEXT 500000 ROWS ONLY);

        TYPE inv_tab_type IS TABLE OF c_inv%ROWTYPE;

        vc_inv   inv_tab_type;
    BEGIN
        EXECUTE IMMEDIATE 'TRUNCATE TABLE s_rep_vat_tmp';

        FOR v_status IN 1 .. 6
        LOOP
            OPEN c_inv (v_status);

            LOOP
                FETCH c_inv BULK COLLECT INTO vc_inv LIMIT 5000;

                FORALL i IN 1 .. vc_inv.COUNT
                    INSERT INTO s_rep_vat_tmp
                         VALUES vc_inv (i);

                EXIT WHEN c_inv%NOTFOUND;
            END LOOP;

            CLOSE c_inv;
        END LOOP;
    EXCEPTION
        WHEN NO_DATA_FOUND
        THEN
            NULL;
        WHEN OTHERS
        THEN
            -- Consider logging the error and then re-raise
            RAISE;
    END prc_bcbh_rep;
END pkg_vat_report;
/

ALTER TABLE EINVOICE.S_INV 
 PARALLEL ( DEGREE 10 INSTANCES Default );
 
ALTER TABLE EINVOICE.S_REP_VAT_TMP 
 PARALLEL ( DEGREE 10 INSTANCES Default );

DECLARE
    v_jsoncol       s_inv.doc%TYPE;
    v_json_obj      json_object_t;
    v_new_jsoncol   s_inv.doc%TYPE;

    --cursor lay du lieu bang s_inv, tuy theo dieu kien de lay du lieu
    CURSOR c_inv IS
        SELECT id
          FROM s_inv
         WHERE (    JSON_VALUE ("DOC" FORMAT JSON, '$.SendSMSDate')
                        IS NOT NULL
                AND INSTR (JSON_VALUE ("DOC" FORMAT JSON, '$.SendSMSDate'),
                           'T') >
                    0);
BEGIN
    --Lay du lieu bang s_inv theo vong lap
    FOR vc_inv IN c_inv
    LOOP
        --Gan du lieu vao bien
        SELECT doc
          INTO v_jsoncol
          FROM s_inv
         WHERE id = vc_inv.id;

        --DBMS_OUTPUT.put_line ('id '||JSON_VALUE(v_jsoncol FORMAT JSON , '$.SendMailDate'));

        --Gan lai sendmaildate
        v_json_obj :=
            TREAT (json_element_t.parse (v_jsoncol) AS json_object_t);
        v_json_obj.put (
            'SendSMSDate',
            TO_CHAR (
                  TO_DATE (
                      JSON_VALUE (v_jsoncol FORMAT JSON, '$.SendSMSDate'),
                      'YYYY-MM-DD"T"HH24:MI:SS".""ZZZZ"')
                + (7 / 24),
                'YYYY-MM-DD HH24:MI:SS'));
        v_new_jsoncol := v_json_obj.to_string;            --converts to string

        --Update vao bang s_inv
        UPDATE s_inv
           SET doc = v_new_jsoncol
         WHERE id = vc_inv.id;

        DBMS_OUTPUT.put_line ('id ' || vc_inv.id);

        COMMIT;
    END LOOP;
END;
/

DECLARE
    v_jsoncol       s_inv.doc%TYPE;
    v_json_obj      json_object_t;
    v_new_jsoncol   s_inv.doc%TYPE;

    --cursor lay du lieu bang s_inv, tuy theo dieu kien de lay du lieu
    CURSOR c_inv IS
        SELECT id
          FROM s_inv
         WHERE (    JSON_VALUE ("DOC" FORMAT JSON, '$.SendMailDate')
                        IS NOT NULL
                AND INSTR (JSON_VALUE ("DOC" FORMAT JSON, '$.SendMailDate'),
                           'T') >
                    0);
BEGIN
    --Lay du lieu bang s_inv theo vong lap
    FOR vc_inv IN c_inv
    LOOP
        --Gan du lieu vao bien
        SELECT doc
          INTO v_jsoncol
          FROM s_inv
         WHERE id = vc_inv.id;

        --DBMS_OUTPUT.put_line ('id '||JSON_VALUE(v_jsoncol FORMAT JSON , '$.SendMailDate'));

        --Gan lai sendmaildate
        v_json_obj :=
            TREAT (json_element_t.parse (v_jsoncol) AS json_object_t);
        v_json_obj.put (
            'SendMailDate',
            TO_CHAR (
                  TO_DATE (
                      JSON_VALUE (v_jsoncol FORMAT JSON, '$.SendMailDate'),
                      'YYYY-MM-DD"T"HH24:MI:SS".""ZZZZ"')
                + (7 / 24),
                'YYYY-MM-DD HH24:MI:SS'));
        v_new_jsoncol := v_json_obj.to_string;            --converts to string

        --Update vao bang s_inv
        UPDATE s_inv
           SET doc = v_new_jsoncol
         WHERE id = vc_inv.id;

        DBMS_OUTPUT.put_line ('id ' || vc_inv.id);

        COMMIT;
    END LOOP;
END;
/
 
ALTER TABLE S_INV DROP COLUMN SENDMAILDATE;

ALTER TABLE S_INV
 ADD (SENDMAILDATE  DATE GENERATED ALWAYS AS (TO_DATE(JSON_VALUE("DOC" FORMAT JSON , '$.SendMailDate'),'YYYY-MM-DD HH24:MI:SS')) VIRTUAL);			 
 
COMMENT ON COLUMN 
S_INV.SENDMAILDATE IS 
'Ngày gửi mail';  

ALTER TABLE S_INV DROP COLUMN SENDSMSDATE;

ALTER TABLE S_INV
 ADD (SENDSMSDATE  DATE GENERATED ALWAYS AS (TO_DATE(JSON_VALUE("DOC" FORMAT JSON , '$.SendSMSDate'),'YYYY-MM-DD HH24:MI:SS')) VIRTUAL);			 
 
COMMENT ON COLUMN 
S_INV.SENDSMSDATE IS 
'Ngày gửi SMS';  

CREATE INDEX EINVOICE.S_INV_RSEC_IDX
    ON EINVOICE.S_INV (json_value (doc FORMAT JSON,
                                   '$.root.sec'
                                   RETURNING VARCHAR2 (10)
                                   NULL ON ERROR))
    LOGGING
    TABLESPACE IDX
    PCTFREE 10
    INITRANS 2
    MAXTRANS 255
    STORAGE (INITIAL 64 K
             NEXT 1 M
             MINEXTENTS 1
             MAXEXTENTS UNLIMITED
             PCTINCREASE 0
             BUFFER_POOL DEFAULT);