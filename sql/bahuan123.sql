CREATE TABLE `s_xls_temp` (`id` VARCHAR(20) NULL, `ic` VARCHAR(50) NULL, `idt` datetime(3) NULL, `form` VARCHAR(11) NULL, `serial` VARCHAR(10) NULL, `seq` VARCHAR(8) NULL, `dt` datetime not null DEFAULT CURRENT_TIMESTAMP);

CREATE TABLE `s_list_inv_cancel` (`id` BIGINT AUTO_INCREMENT NOT NULL, `inv_id` INT NOT NULL, `doc` json DEFAULT NULL, `invtype` INT NOT NULL, `ou` INT NOT NULL, `serial` NVARCHAR(6) NULL, `type` NVARCHAR(10) NULL, `idt` datetime(3) NULL, `cdt` datetime(3) NOT NULL, `listinv_id` BIGINT NULL, `stax` NVARCHAR(14) NULL, CONSTRAINT `PK__s_list_i__3213E83F0E4528B3` PRIMARY KEY (`id`));

CREATE TABLE `info_taxpayer` (`id` bigint unsigned NOT NULL AUTO_INCREMENT, `taxc` NVARCHAR(14) NOT NULL, `namecom` NVARCHAR(255) NULL, `type` NVARCHAR(10) NULL, `status` numeric(18) NULL, `effectdate` datetime(3) NULL, `managercomc` NVARCHAR(10) NULL, `chapter` NVARCHAR(10) NULL, `changedate` datetime(3) NULL, `economictype` NVARCHAR(10) NULL, `contbusires` NVARCHAR(10) NULL, `passport` NVARCHAR(20) NULL, `createdby` NVARCHAR(150) NOT NULL, `statusscan` numeric(18) NOT NULL, `messres` NVARCHAR(255) NOT NULL, `statuscoderes` numeric(18) NOT NULL, `createddate` datetime(3) NULL, `updateddate` datetime NULL,primary key (id)) 

CREATE TABLE `type_invoice` (`id` bigint AUTO_INCREMENT NOT NULL, `name` NVARCHAR(255) NULL, primary key (id));

CREATE TABLE `s_statement` (`id` BIGINT AUTO_INCREMENT NOT NULL, `type` NVARCHAR(15) NULL, `formname` NVARCHAR(100) NULL, `regtype` INT NULL, `sname` NVARCHAR(400) NULL, `stax` NVARCHAR(14) NULL, `taxoname` NVARCHAR(100) NULL, `taxo` NVARCHAR(5) NULL, `contactname` NVARCHAR(50) NULL, `contactaddr` NVARCHAR(400) NULL, `contactemail` NVARCHAR(50) NULL, `contactphone` NVARCHAR(20) NULL, `place` NVARCHAR(50) NULL, `createdt` datetime(3) NULL, `hascode` INT DEFAULT 0 NULL, `sendtype` INT NULL, `dtsendtype` NVARCHAR(20) NULL, `invtype` NVARCHAR(100) NULL, `doc` longtext COLLATE utf8_bin, `status` INT DEFAULT 1 NOT NULL, `cqtstatus` INT NULL, `vdt` datetime(3) NULL, `cqtmess` NVARCHAR(255) NULL, `xml` longtext COLLATE utf8_bin, `statustvan` INT DEFAULT 0 NOT NULL, `xml_received` longtext COLLATE utf8_bin, `xml_accepted` longtext COLLATE utf8_bin, `paxoname` NVARCHAR(100) NULL, `paxo` NVARCHAR(5) NULL, `json_received` longtext COLLATE utf8_bin, `json_accepted` longtext COLLATE utf8_bin, `uc` NVARCHAR(20) NULL, CONSTRAINT `s_statement_pk` PRIMARY KEY (`id`));

CREATE TABLE `temp_taxpayer` (`id` int AUTO_INCREMENT NOT NULL, `taxc` NVARCHAR(14) NOT NULL, `createdby` NVARCHAR(150) NOT NULL, `statusscan` numeric(18) NOT NULL, `creaateddate` datetime(3) NULL, `updateddate` datetime(3) NULL,primary key (id))

CREATE TABLE `s_inv_sum` (`idt` date NOT NULL, `stax` VARCHAR(20) NOT NULL, `type` VARCHAR(6) NOT NULL, `form` VARCHAR(11) NOT NULL, `serial` VARCHAR(8) NULL, `i0` numeric(18) NULL, `i1` numeric(18) NULL, `i2` numeric(18) NULL, `i3` numeric(18) NULL, `i4` numeric(18) NULL);

CREATE TABLE `b_inv` (`id` INT AUTO_INCREMENT NOT NULL, `doc` longtext NOT NULL, `xml` longtext NULL, `idt` datetime(3) NULL, `type` VARCHAR(10) NULL, `form` VARCHAR(11) NULL, `serial` VARCHAR(8) NULL, `seq` VARCHAR(7) NULL, `paym` VARCHAR(100) NULL, `curr` VARCHAR(3) NULL, `exrt` numeric(8, 2) NULL, `sname` VARCHAR(255) NULL, `stax` VARCHAR(14) NULL, `saddr` VARCHAR(255) NULL, `smail` VARCHAR(255) NULL, `stel` VARCHAR(255) NULL, `sacc` VARCHAR(255) NULL, `sbank` VARCHAR(255) NULL, `buyer` VARCHAR(255) NULL, `bname` VARCHAR(255) NULL, `bcode` VARCHAR(100) NULL, `btax` VARCHAR(14) NULL, `baddr` VARCHAR(255) NULL, `bmail` VARCHAR(255) NULL, `btel` VARCHAR(255) NULL, `bacc` VARCHAR(255) NULL, `bbank` VARCHAR(255) NULL, `note` VARCHAR(255) NULL, `sumv` numeric(19, 4) NULL, `vatv` numeric(19, 4) NULL, `totalv` numeric(19, 4) NULL, `ou` INT NULL, `uc` VARCHAR(20) NULL, `paid` INT NULL, `month` VARCHAR(100) NULL, `year` VARCHAR(100) NULL, `category` VARCHAR(100) NULL, CONSTRAINT `PK__b_inv__3213E83FFD9C2485` PRIMARY KEY (`id`));



CREATE TABLE `s_invdoctemp` (`id` BIGINT AUTO_INCREMENT NOT NULL, `invid` BIGINT NOT NULL, `doc` longtext NOT NULL, `dt` datetime not null DEFAULT CURRENT_TIMESTAMP, `status` INT NOT NULL, `xml` longtext NULL, `iserror` INT DEFAULT 0 NOT NULL, `error_desc` longtext NULL, CONSTRAINT `PK_S_INVDOCTEMP` PRIMARY KEY (`id`));

CREATE TABLE `s_user_pass` (`user_id` NVARCHAR(20) NOT NULL, `pass` VARCHAR(200) NOT NULL, `change_date` datetime(3) NOT NULL, CONSTRAINT `PK__s_user_p__518C386C17C39B87` PRIMARY KEY (`user_id`, `pass`));

CREATE TABLE `van_msg_out_dtl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `iddatatvan` decimal(18,0) NOT NULL,
  `invid` varchar(36) NOT NULL,
  `createdby` varchar(150) NOT NULL,
  `statustvan` decimal(18,0) DEFAULT NULL,
  `statusres` decimal(18,0) DEFAULT NULL,
  `messsendcode` varchar(10) DEFAULT NULL,
  `messsend` longtext,
  `messres` varchar(255) DEFAULT NULL,
  `messrescode` varchar(10) DEFAULT NULL,
  `senddate` datetime(3) DEFAULT NULL,
  `resdate` datetime(3) DEFAULT NULL,
  `transid` varchar(225) DEFAULT NULL,
  `idtypeinv` decimal(18,0) NOT NULL,
  `status_response` int(11) DEFAULT '0',
  `datecheckmsg` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `van_msg_out_dtl_transid_IDX` (`transid`) USING BTREE,
  KEY `van_msg_out_dtl_status_response_IDX` (`status_response`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=684 DEFAULT CHARSET=utf8;

CREATE TABLE `van_msg_out` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invid` varchar(36) NOT NULL,
  `xml2base64` longtext NOT NULL,
  `createdby` varchar(150) NOT NULL,
  `statustvan` decimal(18,0) DEFAULT NULL,
  `createddate` datetime(3) DEFAULT NULL,
  `senddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idtypeinv` decimal(18,0) NOT NULL,
  `messsendcode` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `van_msg_out_createddate_IDX` (`createddate`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=686 DEFAULT CHARSET=utf8;





CREATE TABLE `van_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type_invoice` decimal(18,0) NOT NULL,
  `id_ref` decimal(18,0) DEFAULT NULL,
  `status` decimal(18,0) DEFAULT NULL,
  `datetime_trans` datetime(3) DEFAULT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  PRIMARY KEY (`id`),
  KEY `van_history_id_ref_IDX` (`id_ref`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4277 DEFAULT CHARSET=utf8;


CREATE TABLE `s_wrongnotice_process` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ou` int(11) DEFAULT NULL,
  `id_wn` int(11) DEFAULT NULL,
  `id_inv` longtext,
  `doc` longtext,
  `xml` longtext,
  `dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(15) NOT NULL,
  `status_cqt` int(11) DEFAULT NULL,
  `xml_received` longtext,
  `json_received` longtext,
  `xml_accepted` longtext,
  `json_accepted` longtext,
  `dien_giai` longtext,
  `taxc` varchar(14) DEFAULT NULL,
  `form` varchar(14) DEFAULT NULL,
  `serial` varchar(14) DEFAULT NULL,
  `seq` varchar(14) DEFAULT NULL,
  `type` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UQ__s_wrongn__01487863BB3969F7` (`id_wn`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

CREATE TABLE `van_msg_in` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `receive_arr_json` longtext NOT NULL,
  `status_scan` decimal(18,0) DEFAULT NULL COMMENT '0: not scan, pending: 3, success: 1, error: 2',
  `createddate` datetime(3) DEFAULT NULL,
  `updateddate` datetime(3) DEFAULT NULL,
  `createby` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `van_msg_in_createddate_IDX` (`createddate`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2686 DEFAULT CHARSET=utf8;

CREATE TABLE `s_list_inv` (
  `id` bigint(20) NOT NULL,
  `listinv_code` varchar(15) DEFAULT NULL,
  `s_tax` varchar(14) NOT NULL,
  `ou` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `approve_date` datetime(3) DEFAULT NULL,
  `approve_by` varchar(20) DEFAULT NULL,
  `s_name` varchar(400) NOT NULL,
  `item_type` varchar(20) NOT NULL,
  `listinvoice_num` varchar(5) DEFAULT NULL,
  `period_type` varchar(1) NOT NULL,
  `period` varchar(10) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `doc` longtext,
  `xml` longtext,
  `times` int(11) NOT NULL DEFAULT '0',
  `status_received` int(11) DEFAULT NULL,
  `xml_received` longtext,
  `json_received` longtext,
  `edittimes` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `s_list_inv_s_tax_IDX` (`s_tax`,`created_date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE INDEX `s_list_inv_cancel_cdt_ou` ON `s_list_inv_cancel`(`cdt`, `ou`);

CREATE UNIQUE INDEX `s_user_code_idx` ON `s_user`(([code] IS NOT NULL));

ALTER TABLE `s_inv` ADD CONSTRAINT `FK__s_inv__uc__47A6A41B` FOREIGN KEY (`uc`) REFERENCES `s_user` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;



CREATE INDEX `idx_invID` ON `s_trans`(`inv_id`);

CREATE UNIQUE INDEX `idx_tranID` ON `s_trans`(`tranID`);

CREATE UNIQUE INDEX `S_ORG_UID_01` ON `s_org`(`vtaxc`);


ALTER TABLE `s_vendor` ADD CONSTRAINT `UQ__s_vendor__24D3AF4B470912EE` UNIQUE (`taxno`);


ALTER TABLE s_inv ADD CONSTRAINT FK__s_inv__ou__46B27FE2 FOREIGN KEY(ou) REFERENCES s_ou(id);



ALTER TABLE `s_serial` ADD `ut` datetime  DEFAULT CURRENT_TIMESTAMP not null;



ALTER TABLE `s_serial` ADD `alertleft` INT NOT NULL DEFAULT 0;

ALTER TABLE `s_serial` ADD `intg` INT NOT NULL DEFAULT 2;





ALTER TABLE `s_serial` ADD `leftwarn` INT NULL DEFAULT 1;



ALTER TABLE `s_serial` ADD `sendtype` INT NULL;

ALTER TABLE `s_serial` ADD `invtype` INT NULL;

ALTER TABLE `s_serial` ADD `degree_config` INT NULL DEFAULT 119 ;

ALTER TABLE `s_org` ADD `vtaxc` NVARCHAR(14) NULL;

ALTER TABLE `s_inv` ADD `sendmaildate`  datetime GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.SendMailDate'))) VIRTUAL;

ALTER TABLE `s_inv` ADD `sendsmsdate` datetime  GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.SendSMSlDate'))) VIRTUAL;
// alter bo tu tang id
ALTER TABLE s_inv
  MODIFY id int(10) unsigned;
ALTER TABLE `s_inv` DROP COLUMN `vseq`;
ALTER TABLE `s_inv` ADD `vseq` NVARCHAR(18) GENERATED ALWAYS AS (if(IFNULL(json_unquote(json_extract(`doc`,'$.seq')), '') > '',json_unquote(json_extract(`doc`,'$.seq')),id)) VIRTUAL;

ALTER TABLE `s_inv` ADD `sendmailstatus` INT GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.SendMailStatus')) = 0),NULL,json_unquote(json_extract(`doc`,'$.SendMailStatus')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `sendsmsstatus` INT GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.SendSMSStatus')) = 0),NULL,json_unquote(json_extract(`doc`,'$.SendSMSStatus')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `sendmailnum` INT GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.SendMailNum')) = 0),NULL,json_unquote(json_extract(`doc`,'$.SendMailNum')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `sendsmsnum` INT GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.SendSMSNum')) = 0),NULL,json_unquote(json_extract(`doc`,'$.SendSMSNum')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `taxocode` NVARCHAR(20) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.taxocode')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.taxocode')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `invlistdt` datetime  GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.invlistdt')) = ''),NULL,json_unquote(json_extract(`doc`,'$.invlistdt')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `discountamt` numeric(18) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.discountamt')) = 	null),0,json_unquote(json_extract(`doc`,'$.discountamt')))) VIRTUAL;


ALTER TABLE `s_inv` ADD `vehic` NVARCHAR(500)  GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.vehic')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.vehic')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `ordno` NVARCHAR(500) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.ordno')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.ordno')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `whsfr` NVARCHAR(500)  GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.whsfr')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.whsfr')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `whsto` NVARCHAR(500) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.whsto')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.whsto')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `edt` datetime ;

ALTER TABLE `s_inv` ADD `status_received` INT GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.status_received')) = 'null'),0,json_unquote(json_extract(`doc`,'$.status_received')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `xml_received` longtext COLLATE utf8_bin;

ALTER TABLE `s_inv` ADD `hascode` numeric(38) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.hascode')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.hascode')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `invtype` NVARCHAR(500) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.invtype')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.invtype')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `invlistnum` NVARCHAR(50) NULL;

ALTER TABLE `s_inv` ADD `period_type` NVARCHAR(1) NULL;

ALTER TABLE 's_inv' ADD 'period' NVARCHAR(10) NULL;


ALTER TABLE `s_inv` ADD `list_invid` BIGINT NULL;

ALTER TABLE `s_inv` ADD `status_tbss` INT NULL;

ALTER TABLE `s_inv` ADD `orddt` datetime  GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.orddt')) = ''),NULL,json_unquote(json_extract(`doc`,'$.orddt')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `error_msg_van` longtext COLLATE utf8_bin NULL;

ALTER TABLE `s_group` ALTER `APPROVE` SET DEFAULT '1';

ALTER TABLE `s_group` ALTER `DES` SET DEFAULT '1';

ALTER TABLE `s_group_user` MODIFY `USER_ID` VARCHAR(20);

ALTER TABLE `s_refcode` MODIFY `abbreviation` longtext COLLATE utf8_bin;

ALTER TABLE `s_role` MODIFY `active` TINYINT(3);

ALTER TABLE `s_role` ALTER `active` DROP DEFAULT;

ALTER TABLE `b_inv` ADD CONSTRAINT `UQ__b_inv__B09EB31C78CA9B41` UNIQUE (`stax`, `form`, `serial`, `seq`);



ALTER TABLE `s_role` ADD CONSTRAINT `s_role_fk01` FOREIGN KEY (`pid`) REFERENCES `s_role` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE `s_user_pass` ADD CONSTRAINT `FK__s_user_pa__user___49CEE3AF` FOREIGN KEY (`user_id`) REFERENCES `s_user` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE `s_group_role` ADD `STATUS` VARCHAR(1) NULL;

ALTER TABLE `s_role` ADD `pid` INT NULL;

ALTER TABLE `s_role` ADD `sort` NVARCHAR(10) NULL;

ALTER TABLE `s_role` ADD `active` TINYINT(3) NULL;

ALTER TABLE `s_role` ADD `menu_detail` NVARCHAR(150) NULL;

ALTER TABLE `s_user` ADD `local` INT DEFAULT 0 NOT NULL;

ALTER TABLE `s_user` ADD `change_pass_date` datetime null DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `s_user` ADD `login_number` INT DEFAULT 0 NOT NULL;

ALTER TABLE `s_user` ADD `change_pass` INT DEFAULT 0 NOT NULL;

ALTER TABLE `s_user` ADD `update_date` datetime(3) NULL;

ALTER TABLE `s_user` ADD `local` INT DEFAULT 0 NOT NULL;

ALTER TABLE `s_user` ADD `change_pass_date` datetime null DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `s_report_tmp` MODIFY `c_id` BIGINT;

ALTER TABLE `s_report_tmp` MODIFY `cancel` INT;

ALTER TABLE `s_user_pass` MODIFY `change_date` datetime NOT NULL;

ALTER TABLE `s_report_tmp` MODIFY `clist` longtext COLLATE utf8_bin;

ALTER TABLE `s_gns` MODIFY `code` NVARCHAR(30);

ALTER TABLE `s_role` MODIFY `code` NVARCHAR(100);

ALTER TABLE `s_cat` MODIFY `des` NVARCHAR(100);



ALTER TABLE `s_refcode` MODIFY `domain` longtext COLLATE utf8_bin;

ALTER TABLE `s_f_fld` MODIFY `fld_typ` NVARCHAR(50) NOT NULL;

ALTER TABLE `s_inv_sum` MODIFY `form` VARCHAR(11);

ALTER TABLE `s_report_tmp` MODIFY `form` NVARCHAR(50);

ALTER TABLE `s_refcode` MODIFY `highval` longtext COLLATE utf8_bin;

ALTER TABLE `s_inv_sum` MODIFY `i0` numeric(18);

ALTER TABLE `s_inv_sum` MODIFY `i1` numeric(18);

ALTER TABLE `s_inv_sum` MODIFY `i2` numeric(18);

ALTER TABLE `s_inv_sum` MODIFY `i3` numeric(18);

ALTER TABLE `s_inv_sum` MODIFY `i4` numeric(18);



ALTER TABLE `s_dcd` MODIFY `id` NVARCHAR(30);









ALTER TABLE `s_inv_sum` MODIFY `idt` date;

ALTER TABLE `s_xls` MODIFY `itype` NVARCHAR(6);

ALTER TABLE `s_xls` MODIFY `jc` NVARCHAR(20);

ALTER TABLE `s_refcode` MODIFY `lowval` longtext COLLATE utf8_bin;



ALTER TABLE `s_refcode` MODIFY `meaning` longtext COLLATE utf8_bin;

ALTER TABLE `s_role` MODIFY `menu_detail` NVARCHAR(150);

ALTER TABLE `s_report_tmp` MODIFY `min0` INT;

ALTER TABLE `s_report_tmp` MODIFY `min1` INT;

ALTER TABLE `s_report_tmp` MODIFY `minc` INT;

ALTER TABLE `s_report_tmp` MODIFY `minu` INT;

ALTER TABLE `s_dcd` MODIFY `name` NVARCHAR(100);

ALTER TABLE `s_gns` MODIFY `name` NVARCHAR(255);

ALTER TABLE `s_loc` MODIFY `name` NVARCHAR(100);

ALTER TABLE `s_role` MODIFY `name` NVARCHAR(100);

ALTER TABLE `s_taxo` MODIFY `name` NVARCHAR(100);

ALTER TABLE `s_role` MODIFY `pid` INT;

ALTER TABLE `s_gns` MODIFY `price` numeric(17, 2);

ALTER TABLE `s_inv_sum` MODIFY `serial` VARCHAR(8);

ALTER TABLE `s_report_tmp` MODIFY `serial` NVARCHAR(50);

ALTER TABLE `s_role` MODIFY `sort` NVARCHAR(10);

ALTER TABLE `s_inv_sum` MODIFY `stax` VARCHAR(20);

ALTER TABLE `s_cat` MODIFY `type` NVARCHAR(30);

ALTER TABLE `s_dcd` MODIFY `type` NVARCHAR(30);

ALTER TABLE `s_inv_sum` MODIFY `type` VARCHAR(6);

ALTER TABLE `s_gns` MODIFY `unit` NVARCHAR(30);

ALTER TABLE `s_xls` MODIFY `xc` NVARCHAR(2);



CREATE TABLE `s_refcode` (
  `domain` varchar(60) NOT NULL,
  `abbreviation` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `meaning` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `lowval` longtext NOT NULL,
  `highval` longtext CHARACTER SET utf8 COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Bảng lưu tập hợp các giá trị tham chiếu của các trường chức năng, cột dữ liệu';

ALTER TABLE `s_sys_logs` ADD `ip` VARCHAR(50) NULL;
-- xoa loai hoa don
delete from s_cat where `type`='ITYPE';

ALTER TABLE `s_sys_logs` COMMENT = 'Bảng luu log audit thao tác của NSD APP, API';



CREATE TABLE `van_msg_in_dtl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `receivejson` json NOT NULL,
  `taxc` varchar(14) DEFAULT NULL,
  `namenotice` varchar(500) CHARACTER SET utf8mb4 GENERATED ALWAYS AS (if((json_unquote(json_extract(`receivejson`,'$.tenTBao')) = 'null'),NULL,json_unquote(json_extract(`receivejson`,'$.tenTBao')))) VIRTUAL,
  `typenotice` varchar(255) CHARACTER SET utf8mb4 GENERATED ALWAYS AS (if((json_unquote(json_extract(`receivejson`,'$.loaiTBao')) = 'null'),NULL,json_unquote(json_extract(`receivejson`,'$.loaiTBao')))) VIRTUAL,
  `contentnotice` longtext CHARACTER SET utf8mb4 GENERATED ALWAYS AS (if((json_unquote(json_extract(`receivejson`,'$.ndungTBao')) = 'null'),NULL,json_unquote(json_extract(`receivejson`,'$.ndungTBao')))) VIRTUAL,
  `createddtnotice` datetime GENERATED ALWAYS AS (if((json_unquote(json_extract(`receivejson`,'$.ngayTaoTBao')) = 'null'),NULL,json_unquote(json_extract(`receivejson`,'$.ngayTaoTBao')))) VIRTUAL,
  `createddate` datetime DEFAULT CURRENT_TIMESTAMP,
  `status_scan` decimal(18,0) DEFAULT NULL,
  `updateddate` datetime(3) DEFAULT NULL,
  `transid` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `van_msg_in_dtl_status_scan_IDX` (`status_scan`) USING BTREE,
  KEY `van_msg_in_dtl_transid_IDX` (`transid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16071 DEFAULT CHARSET=utf8;




CREATE TABLE `s_tmp` (`ic` VARCHAR(36) NULL, `idt` datetime(3) NULL, `form` VARCHAR(11) NULL, `serial` VARCHAR(6) NULL, `seq` VARCHAR(7) NULL);

CREATE TABLE `s_imp` ( `idt` datetime(3) NULL, `form` VARCHAR(11) NULL, `serial` VARCHAR(6) NULL);


// bang dung cho job
CREATE TABLE `s_listvalues` (
  `keyname` varchar(100) NOT NULL,
  `keyvalue` varchar(100) NOT NULL,
  `keytype` varchar(100) NOT NULL,
  UNIQUE KEY `s_listvalues_uk` (`keyname`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

// bang luu id gui tvan
CREATE TABLE `van_einv_listid_msg` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `status_scan` int(11) DEFAULT NULL,
  `createdate` datetime DEFAULT CURRENT_TIMESTAMP,
  `sendvandate` datetime DEFAULT NULL,
  `msg` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `xml` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `van_einv_listid_msg_status_scan_IDX` (`status_scan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=537 DEFAULT CHARSET=utf8;


delete from bahuan.s_group_role;

insert into bahuan.s_group_role(group_id,role_id,status) 
select 1,id,null from bahuan.s_role;
commit;



CREATE UNIQUE INDEX s_inv_stax_IDX USING BTREE ON s_inv (stax,form,serial,vseq);

-- Thêm inv:r7 xử lý form chờ hủy hủy // riêng lge
insert into s_f_fld(fnc_name,fnc_url, fld_name, fld_id, fld_typ, atr, feature, status) values ("Huy hoa don", "inv.inv","Form row 7", "inv:r7", "affectMany", '{"disabled":true}','{"id":"*","action":"cancel"}',1)

-- thêm cột inv_adj xử lý cho BTH
ALTER TABLE s_inv
ADD inv_adj bigint;

ALTER TABLE s_ou ADD (
  `sct` tinyint(3) NOT NULL DEFAULT '0',
  `sc` tinyint(3) NOT NULL DEFAULT '0',
  `bcc` varchar(100) DEFAULT NULL,
  `qrc` tinyint(3) NOT NULL DEFAULT '0',
  `na` tinyint(3) NOT NULL DEFAULT '2',
  `nq` tinyint(3) NOT NULL DEFAULT '2',
  `np` tinyint(3) NOT NULL DEFAULT '5',
  `itype` varchar(255) NOT NULL DEFAULT '01GTKT',
  `ts` tinyint(3) NOT NULL DEFAULT '1',
  `degree_config` tinyint(3) NOT NULL DEFAULT '119',
  `receivermail` varchar(200) DEFAULT NULL,
  `dsignfrom` datetime DEFAULT NULL,
  `dsignto` datetime DEFAULT NULL,
  `dsignserial` varchar(100) DEFAULT NULL,
  `dsignsubject` varchar(200) DEFAULT NULL,
  `dsignissuer` varchar(200) DEFAULT NULL);
  INSERT INTO bahuan.s_role (id,name,code,pid,sort,active,menu_detail) VALUES 
(1000000,'Hệ thống','SYS',NULL,'01000000',1,'{"icon":"mdi mdi-monitor-dashboard","value":"system"}')
,(1010000,'Cấu hình hệ thống','PERM_SYS_CONFIG',1000000,'01010000',1,'{"id":"sys.ous","icon":"mdi mdi-file-tree","value":"system_config"}')
,(1010100,'Cấu hình hệ thống - tìm kiếm','PERM_SYS_CONFIG',1010000,'01010100',1,'')
,(1010200,'Cấu hình hệ thống - thêm mới','PERM_SYS_CONFIG_CREATE',1010000,'01010200',1,'')
,(1010300,'Cấu hình hệ thống - cập nhật','PERM_SYS_CONFIG_UPDATE',1010000,'01010300',1,'')
,(1010400,'Cấu hình hệ thống - xóa','PERM_SYS_CONFIG_DELETE',1010000,'01010400',1,'')
,(1010500,'Cấu hình hệ thống - xem CKS','PERM_SYS_CONFIG_VIEW_CA',1010000,'01010500',1,'')
,(1020000,'Quản lý người dùng','PERM_SYS_USER',1000000,'01020000',1,'{"id":"sys.user","icon":"mdi mdi-account-edit","value":"user_manage"}')
,(1020100,'Quản lý người dùng - tìm kiếm','PERM_SYS_USER',1020000,'01020100',1,'')
,(1020200,'Quản lý người dùng - khôi phục','PERM_SYS_USER_RESTORE',1020000,'01020200',1,'')
;
INSERT INTO bahuan.s_role (id,name,code,pid,sort,active,menu_detail) VALUES 
(1020300,'Quản lý người dùng - hủy','PERM_SYS_USER_CANCEL',1020000,'01020300',1,'')
,(1020400,'Quản lý người dùng/Cập nhật - bỏ chọn','PERM_SYS_USER_TAPUPDATE_UNSELECT',1020000,'01020400',1,'')
,(1020500,'Quản lý người dùng/Cập nhật - cập nhật','PERM_SYS_USER_TAPUPDATE_UPDATE',1020000,'01020500',1,'')
,(1020600,'Quản lý người dùng/Cập nhật - thêm mới','PERM_SYS_USER_TAPUPDATE_CREATE',1020000,'01020600',1,'')
,(1020700,'Quản lý người dùng/Dữ liệu - lưu','PERM_SYS_USER_TAPDATA_SAVE',1020000,'01020700',1,'')
,(1020800,'Quản lý người dùng/Phân nhóm - lưu','PERM_SYS_USER_TAPGROUPGRANT_SAVE',1020000,'01020800',1,'')
,(1030000,'Quản lý nhóm','PERM_SYS_GROUP',1000000,'01030000',1,'{"id":"sys.group","icon":"mdi mdi-account-edit","value":"group_manage" }')
,(1030100,'Quản lý nhóm - tìm kiếm','PERM_SYS_GROUP',1030000,'01030100',1,'')
,(1030200,'Quản lý nhóm - khôi phục','PERM_SYS_GROUP_RESTORE',1030000,'01030200',1,'')
,(1030300,'Quản lý nhóm - hủy','PERM_SYS_GROUP_CANCEL',1030000,'01030300',1,'')
;
INSERT INTO bahuan.s_role (id,name,code,pid,sort,active,menu_detail) VALUES 
(1030400,'Quản lý nhóm/Cập nhật - bỏ chọn','PERM_SYS_GROUP_TAPUPDATE_UNSELECT',1030000,'01030400',1,'')
,(1030500,'Quản lý nhóm/Cập nhật - cập nhật','PERM_SYS_GROUP_TAPUPDATE_UPDATE',1030000,'01030500',1,'')
,(1030600,'Quản lý nhóm/Cập nhật - thêm mới','PERM_SYS_GROUP_TAPUPDATE_CREATE',1030000,'01030600',1,'')
,(1030700,'Quản lý nhóm/Dữ liệu - lưu','PERM_SYS_GROUP_TAPDATA_SAVE',1030000,'01030700',1,'')
,(1030800,'Quản lý nhóm/Phân nhóm - lưu','PERM_SYS_GROUP_TAPGROUPGRANT_SAVE',1030000,'01030800',1,'')
,(1040000,'Phân quyền','PERM_SYS_ROLE',1000000,'01040000',0,'{"id":"sys.role","icon:"mdi mdi-account-group","value":"grant"}')
,(1040100,'Phân quyền - tìm kiếm','PERM_SYS_ROLE',1040000,'01040100',0,'')
,(1040200,'Phân quyền - lưu','PERM_SYS_ROLE_SAVE',1040000,'01040200',0,'')
,(1050000,'Tạo mẫu hóa đơn','PERM_SYS_TEMPLATE',1000000,'01050000',1,'{"id":"sys.editor","icon":"mdi mdi-playlist-edit","value":"invoice_template"}')
,(1050100,'Tạo mẫu hóa đơn - tìm kiếm','PERM_SYS_TEMPLATE',1050000,'01050100',1,'')
;
INSERT INTO bahuan.s_role (id,name,code,pid,sort,active,menu_detail) VALUES 
(1050200,'Tạo mẫu hóa đơn - duyệt mẫu','PERM_SYS_TEMPLATE_APPROVE',1050000,'01050200',1,'')
,(1050300,'Tạo mẫu hóa đơn - lưu','PERM_SYS_TEMPLATE_SAVE',1050000,'01050300',1,'')
,(1050400,'Tạo mẫu hóa đơn - xem mẫu','PERM_SYS_TEMPLATE_VIEW',1050000,'01050400',1,'')
,(1050500,'Tạo mẫu hóa đơn - xuất mẫu','PERM_SYS_TEMPLATE_DOWLOAD',1050000,'01050500',1,'')
,(1060000,'Tạo mẫu Email','PERM_SYS_EMAIL',1000000,'01060000',1,'{"id":"sys.email","icon":"mdi mdi-email-plus-outline","value":"mail_template"}')
,(1060100,'Tạo mẫu Email - tìm kiếm','PERM_SYS_EMAIL',1060000,'01060100',1,'')
,(1060200,'Tạo mẫu Email - sửa/lưu','PERM_SYS_EMAIL_SAVE_UP',1060000,'01060200',1,'')
,(1070000,'Cấu hình hóa đơn','PERM_SYS_INV_CONFIG',1000000,'01070000',1,'{"id":"sys.col","icon":"mdi mdi-cogs","value":"invoice_config"}')
,(1070100,'Cấu hình hóa đơn - tìm kiếm','PERM_SYS_INV_CONFIG',1070000,'01070100',1,'')
,(1070200,'Cấu hình hóa đơn - sửa chi tiết hóa đơn','PERM_SYS_INV_CONFIG_EDIT_DETAIL',1070000,'01070200',1,'')
;
INSERT INTO bahuan.s_role (id,name,code,pid,sort,active,menu_detail) VALUES 
(1070300,'Cấu hình hóa đơn - sửa mẫu nhập excel','PERM_SYS_INV_CONFIG_EDIT_EXCEL',1070000,'01070300',1,'')
,(1070400,'Cấu hình hóa đơn - sửa thông tin chung','PERM_SYS_INV_CONFIG_EDIT_INFO',1070000,'01070400',1,'')
,(1080000,'Cấu hình trường','PERM_SYS_FIELD_CONFIG',1000000,'01080000',0,'{"id":"sys.field","icon":"mdi mdi-newspaper","value":"field_config"}')
,(1080100,'Cấu hình trường - tìm kiếm','PERM_SYS_FIELD_CONFIG',1080000,'01080100',0,'')
,(1080200,'Cấu hình trường - sửa thông tin','PERM_SYS_FIELD_CONFIG_EDIT',1080000,'01080200',0,'')
,(1090000,'Log hệ thống - tìm kiếm','PERM_LOG_SYS',1000000,'01090000',1,'{"id":"syslog","icon":"mdi mdi-math-log","value":"syslog"}')
,(2000000,'Quản trị danh mục','PERM_CATEGORY_MANAGE',NULL,'02000000',1,'{"icon":"mdi mdi-file-cabinet", "value":"catalog"}')
,(2010000,'Khách hàng','PERM_CATEGORY_CUSTOMER',2000000,'02010000',1,'{"id":"cat.org","icon": "mdi mdi-account-multiple-check", "value":"catalog_customer"}')
,(2010100,'Khách hàng - tìm kiếm','PERM_CATEGORY_CUSTOMER',2010000,'02010100',1,'')
,(2010200,'Khách hàng - thêm mới','PERM_CATEGORY_CUSTOMER_CREATE',2010000,'02010200',1,'')
;
INSERT INTO bahuan.s_role (id,name,code,pid,sort,active,menu_detail) VALUES 
(2010300,'Khách hàng - cập nhật','PERM_CATEGORY_CUSTOMER_UPDATE',2010000,'02010300',1,'')
,(2010400,'Khách hàng - bỏ chọn','PERM_CATEGORY_CUSTOMER_UNCHECK',2010000,'02010400',1,'')
,(2010500,'Khách hàng - excel','PERM_CATEGORY_CUSTOMER_EXCEL',2010000,'02010500',1,'')
,(2010600,'Khách hàng - lấy mã tự động','PERM_CATEGORY_CUSTOMER_GETCODE',2010000,'02010600',1,'')
,(2010700,'Khách hàng - xoá DL','PERM_CATEGORY_CUSTOMER_DELETE',2010000,'02010700',1,'')
,(2020000,'Hàng hóa, dịch vụ','PERM_CATEGORY_ITEMS',2000000,'02020000',1,'{"id":"cat.gns","icon":"mdi mdi-cart-outline","value": "catalog_items"}')
,(2020100,'Hàng hóa, dịch vụ - tìm kiếm','PERM_CATEGORY_ITEMS',2020000,'02020100',1,'')
,(2020200,'Hàng hóa, dịch vụ - thêm mới','PERM_CATEGORY_ITEMS_CREATE',2020000,'02020200',1,'')
,(2020300,'Hàng hóa, dịch vụ - cập nhật','PERM_CATEGORY_ITEMS_UPDATE',2020000,'02020300',1,'')
,(2020400,'Hàng hóa, dịch vụ - bỏ chọn','PERM_CATEGORY_ITEMS_UNCHECK',2020000,'02020400',1,'')
;
INSERT INTO bahuan.s_role (id,name,code,pid,sort,active,menu_detail) VALUES 
(2020500,'Hàng hóa, dịch vụ - excel','PERM_CATEGORY_ITEMS_EXCEL',2020000,'02020500',1,'')
,(2020600,'Hàng hóa, dịch vụ - xoá DL','PERM_CATEGORY_ITEMS_DELETE',2020000,'02020600',1,'')
,(2030000,'Tỷ giá','PERM_CATEGORY_RATE',2000000,'02030000',1,'{"id":"cat.exch","icon":"mdi mdi-currency-usd","value": "exchange_rate"}')
,(2030100,'Tỷ giá - tìm kiếm','PERM_CATEGORY_RATE',2030000,'02030100',1,'')
,(2030200,'Tỷ giá - thêm mới','PERM_CATEGORY_RATE_INSERT',2030000,'02030200',1,'')
,(2030300,'Tỷ giá - xoá','PERM_CATEGORY_RATE_DELETE',2030000,'02030300',1,'')
,(2030400,'Tỷ giá - kết xuất excel','PERM_CATEGORY_RATE_EXCEL',2030000,'02030400',1,'')
,(2030500,'Tỷ giá - cập nhật','PERM_CATEGORY_RATE_UPDATE',2030000,'02030500',1,'')
,(2040000,'Danh mục khác','PERM_CATEGORY_ANOTHER',2000000,'02040000',1,'{"id":"cat.cat","icon":"mdi mdi-newspaper","value": "catalog_catalog"}')
,(2040100,'Danh mục khác - tìm kiếm','PERM_CATEGORY_ANOTHER',2040000,'02040100',1,'')
;
INSERT INTO bahuan.s_role (id,name,code,pid,sort,active,menu_detail) VALUES 
(2040200,'Danh mục khác - thêm mới','PERM_CATEGORY_ANOTHER_CREATE',2040000,'02040200',1,'')
,(2040300,'Danh mục khác - cập nhật','PERM_CATEGORY_ANOTHER_UPDATE',2040000,'02040300',1,'')
,(2040400,'Danh mục khác - xoá','PERM_CATEGORY_ANOTHER_DELETE',2040000,'02040400',1,'')
,(2050000,'Địa bàn','PERM_CATEGORY_LOCATION',2000000,'02050000',1,'{"id":"cat.loc","icon":"mdi mdi-earth","value":"catalog_location"}')
,(2050100,'Địa bàn - tìm kiếm','PERM_CATEGORY_LOCATION',2050000,'02050100',1,'')
,(2050200,'Địa bàn - thêm mới','PERM_CATEGORY_LOCATION_CREATE',2050000,'02050200',1,'')
,(2050300,'Địa bàn - cập nhật','PERM_CATEGORY_LOCATION_UPDATE',2050000,'02050300',1,'')
,(2050400,'Địa bàn - xoá','PERM_CATEGORY_LOCATION_DELETE',2050000,'02050400',1,'')
,(3000000,'Thông báo phát hành','PERM_RELEASE_REGISTER',NULL,'03000000',1,'{"icon":"mdi mdi-bullhorn","value":"register_to_use_invoices"}')
,(3010000,'Thông báo phát hành','PERM_TEMPLATE_REGISTER',3000000,'03010000',1,'{"id":"serial","icon": "mdi mdi-numeric","value":"releasedND123"}')
;
INSERT INTO bahuan.s_role (id,name,code,pid,sort,active,menu_detail) VALUES 
(3010100,'Thông báo phát hành - tìm kiếm','PERM_TEMPLATE_REGISTER',3010000,'03010100',1,'')
,(3010200,'Thông báo phát hành - thêm mới','PERM_TEMPLATE_REGISTER_CREATE',3010000,'03010200',1,'')
,(3010300,'Thông báo phát hành - phê duyệt phát hành','PERM_TEMPLATE_REGISTER_APPROVE',3010000,'03010300',1,'')
,(3010400,'Thông báo phát hành - hủy phát hành','PERM_TEMPLATE_REGISTER_CANCEL',3010000,'03010400',1,'')
,(3010500,'Thông báo phát hành - xóa dl','PERM_TEMPLATE_REGISTER_DELETE',3010000,'03010500',1,'')
,(3020000,'Gán quyền sử dụng TBPH','PERM_TEMPLATE_REGISTER_GRANT',3000000,'03020000',0,'{"id":"seou","icon":"mdi mdi-account-multiple-check","value": "seou"}')
,(3020100,'Gán quyền sử dụng TBPH - tìm kiếm','PERM_TEMPLATE_REGISTER_GRANT',3020000,'03020100',0,'')
,(3020200,'Gán quyền sử dụng TBPH - lưu','PERM_TEMPLATE_REGISTER_GRANT_SAVE',3020000,'03020200',0,'')
,(3030000,'Gán TBPH theo người dùng','PERM_TEMPLATE_REGISTER_USRGRANT',3000000,'03030000',0,'{"id":"seusr","icon":"mdi mdi-account-multiple-check","value": "seusr"}')
,(3030100,'Gán TBPH theo người dùng - tìm kiếm','PERM_TEMPLATE_REGISTER_USRGRANT',3030000,'03030100',0,'')
;
INSERT INTO bahuan.s_role (id,name,code,pid,sort,active,menu_detail) VALUES 
(3030200,'Gán TBPH theo người dùng - Lưu','PERM_TEMPLATE_REGISTER_USRGRANT_SAVE',3030000,'03030200',0,'')
,(3030300,'Lập tờ khai','PERM_CREATE_STM',3000000,'03030300',1,'{"id":"reg.regstm","value":"stm_menu_create"}')
,(3030400,'Tra cứu tờ khai','PERM_SEARCH_STM',3000000,'03030400',1,'{"id":"reg.searchstm","value":"stm_menu_search"}')
,(3030500,'Đăng ký sử dụng','PERM_TEMPLATE_REGISTER_123',3000000,'03030500',0,'{"id":"serial123","value":"releasedND123"}')
,(3030501,'Đăng ký sử dụng - Tìm kiếm','PERM_TEMPLATE_REGISTER_SEARCH_123',3030500,'03030501',0,'')
,(3030502,'Đăng ký sử dụng - Thêm mới','PERM_TEMPLATE_REGISTER_CREATE_123',3030500,'03030502',0,'')
,(3030503,'Đăng ký sử dụng - Duyệt','PERM_TEMPLATE_REGISTER_APPROVE_123',3030500,'03030503',0,'')
,(3030504,'Đăng ký sử dụng - Hủy','PERM_TEMPLATE_REGISTER_CANCEL_123',3030500,'03030504',0,'')
,(3030505,'Đăng ký sử dụng - Xóa','PERM_TEMPLATE_REGISTER_DELETE_123',3030500,'03030505',0,'')
,(3040000,'Hóa đơn đầu vào','PERM_SEARCH_BINV',NULL,'03040000',1,'{"icon": "mdi mdi-file", "value": "input_invoice"}')
;
INSERT INTO bahuan.s_role (id,name,code,pid,sort,active,menu_detail) VALUES 
(3040100,'Tra cứu hóa đơn đầu vào - Tìm kiếm','PERM_SEARCH_BINV',3040000,'03040100',1,'{ "id": "inv.bin", "icon": "mdi mdi-file", "value":"input_invoice"}')
,(3040200,'Tra cứu hóa đơn đầu vào - Đọc file XML','PERM_SEARCH_BINV_READ_XML',3040000,'03040200',1,'')
,(3040300,'Tra cứu hóa đơn đầu vào - Xuất bảng kê','PERM_SEARCH_BINV_LIST_EXP',3040000,'03040300',1,'')
,(3040400,'Tra cứu hóa đơn đầu vào - Lập hóa đơn','PERM_SEARCH_BINV_MANAGE',3040000,'03040400',1,'')
,(3040500,'Tra cứu hóa đơn đầu vào - Copy hóa đơn','PERM_SEARCH_BINV_COPY',3040000,'03040500',1,'')
,(3040600,'Tra cứu hóa đơn đầu vào - Kết xuất excel','PERM_SEARCH_BINV_XLS',3040000,'03040600',1,'')
,(3040700,'Tra cứu hóa đơn đầu vào - Xem hóa đơn','PERM_SEARCH_BINV_VIEW',3040000,'03040700',1,'')
,(3040800,'Tra cứu hóa đơn đầu vào - Xóa hóa đơn','PERM_SEARCH_BINV_DELETE',3040000,'03040800',1,'')
,(3040900,'Tra cứu hóa đơn đầu vào - Sửa hóa đơn','PERM_SEARCH_BINV_EDIT',3040000,'03040900',1,'')
,(4000000,'Tra cứu hóa đơn','PERM_SEARCH_INV',NULL,'04000000',1,'{"icon":"mdi mdi-folder-search-outline","value":"invoice_search"}')
;
INSERT INTO bahuan.s_role (id,name,code,pid,sort,active,menu_detail) VALUES 
(4010100,'Tra cứu hóa đơn - Tìm kiếm','PERM_SEARCH_INV',4000000,'04010100',1,'{"id":"inv.invs","icon":"mdi mdi-folder-search-outline","value": "invoice_search"}')
,(4010200,'Tra cứu hóa đơn - Hủy','PERM_SEARCH_INV_CANCEL',4000000,'04010200',1,'')
,(4010300,'Tra cứu hóa đơn - Hủy/Tạo biên bản','PERM_SEARCH_INV_CANCEL_CREATE_MINUTES',4000000,'04010300',1,'')
,(4010400,'Tra cứu hóa đơn - Hủy/Hủy','PERM_SEARCH_INV_CANCEL_CANCEL',4000000,'04010400',1,'')
,(4010500,'Tra cứu hóa đơn - Hủy/Hiển thị','PERM_SEARCH_INV_CANCEL_VIEW',4000000,'04010500',1,'')
,(4010600,'Tra cứu hóa đơn - Chờ hủy','PERM_VOID_WAIT',4000000,'04010600',1,'')
,(4010700,'Tra cứu hóa đơn - Bỏ chờ hủy','PERM_SEARCH_INV_REJECT_CANCEL',4000000,'04010700',1,'')
,(4010800,'Tra cứu hóa đơn - Chuyển đổi','PERM_SEARCH_INV_CONVERT',4000000,'04010800',1,'')
,(4010900,'Tra cứu hóa đơn - Chuyển đổi/Download pdf file','PERM_SEARCH_INV_CONVERT_DOWNLOAD_FILE',4000000,'04010900',1,'')
,(4011000,'Tra cứu hóa đơn - Chuyển đổi/In','PERM_SEARCH_INV_CONVERT_PRINT',4000000,'04011000',1,'')
;
INSERT INTO bahuan.s_role (id,name,code,pid,sort,active,menu_detail) VALUES 
(4011100,'Tra cứu hóa đơn - Gửi nhiều','PERM_MAILS',4000000,'04011100',1,'')
,(4011200,'Tra cứu hóa đơn - Mail','PERM_MAIL',4000000,'04011200',1,'')
,(4011300,'Tra cứu hóa đơn - Print','PERM_SEARCH_INV_PRINT',4000000,'04011300',1,'')
,(4011400,'Tra cứu hóa đơn - Coppy','PERM_SEARCH_INV_COPPY',4000000,'04011400',1,'')
,(4011500,'Tra cứu hóa đơn - Tải biên bản','PERM_SEARCH_INV_DOWNLOAD_MINUTES',4000000,'04011500',1,'')
,(4011600,'Tra cứu hóa đơn - Download Excel','PERM_SEARCH_INV_EXCEL',4000000,'04011600',1,'')
,(4011700,'Tra cứu hóa đơn - Hiển thị','PERM_SEARCH_INV_VIEW',4000000,'04011700',1,'')
,(4011800,'Tra cứu hóa đơn - Hiển thị/Dowload as pdf file','PERM_SEARCH_INV_VIEW_DOWNLOAD_FILE',4000000,'04011800',1,'')
,(4011900,'Tra cứu hóa đơn - Hiển thị/Ký pdf','PERM_SEARCH_INV_VIEW_SIGN_PDF',4000000,'04011900',1,'')
,(4012000,'Tra cứu hóa đơn - Hiển thị/XML','PERM_SEARCH_INV_VIEW_XML',4000000,'04012000',1,'')
;
INSERT INTO bahuan.s_role (id,name,code,pid,sort,active,menu_detail) VALUES 
(4012100,'Tra cứu hóa đơn - Tìm hóa đơn liên quan','PERM_SEARCH_INV_SEARCH_RELATE',4000000,'04012100',1,'')
,(4012200,'Tra cứu hóa đơn - Xem chi tiết hóa đơn liên quan','PERM_SEARCH_INV_VIEWDETAIL_RELATE',4000000,'04012200',1,'')
,(4012300,'Tra cứu hóa đơn - Xóa hóa đơn','PERM_SEARCH_INV_DELETE',4000000,'04012300',1,'')
,(4012400,'Tra cứu hóa đơn - Sửa hóa đơn','PERM_SEARCH_INV_UPDATE',4000000,'04012400',1,'')
,(4012500,'Tra cứu hóa đơn - Thay thế','PERM_INVOICE_REPLACE',4000000,'04012500',1,'')
,(4012600,'Tra cứu hóa đơn - Điều chỉnh','PERM_INVOICE_ADJUST',4000000,'04012600',1,'')
,(4012700,'Tra cứu hóa đơn - Điều chỉnh/Chọn file','PERM_SEARCH_INV_ADJUST_UPLOAD_FILE',4000000,'04012700',1,'')
,(4012800,'Tra cứu hóa đơn - Điều chỉnh/Tạo biên bản','PERM_SEARCH_INV_ADJUST_CREATE_MINUTES',4000000,'04012800',1,'')
,(4012900,'Tra cứu hóa đơn - Điều chỉnh/Lưu','PERM_SEARCH_INV_ADJUST_SAVE',4000000,'04012900',1,'')
,(4013000,'Tra cứu hóa đơn - Điều chỉnh/Bỏ chọn','PERM_SEARCH_INV_ADJUST_UNSELECT',4000000,'04013000',1,'')
;
INSERT INTO bahuan.s_role (id,name,code,pid,sort,active,menu_detail) VALUES 
(4013100,'Tra cứu hóa đơn - Điều chỉnh/Thêm điều chỉnh','PERM_SEARCH_INV_ADJUST_ADDADJUST',4000000,'04013100',1,'')
,(4013200,'Tra cứu hóa đơn - Gửi SMS thủ công','PERM_SMS',4000000,'04013200',1,'')
,(4020000,'Quản lý giao dịch','PERM_TRAN',NULL,'04020000',0,'{"icon": "mdi mdi-database-search", "value":"tran_management"}')
,(4020100,'Tra cứu giao dịch','PERM_TRANS',4020000,'04020100',0,'{"id": "inv.itrans", "icon": "mdi mdi-database-search", "value": "tran_search"}')
,(4020101,'Tra cứu giao dịch - Tìm kiếm','PERM_TRANS',4020100,'04020101',0,'')
,(4020102,'Tra cứu giao dịch - Kết xuất','PERM_TRANS_EXP',4020100,'04020102',0,'')
,(4020103,'Tra cứu giao dịch - Tra cứu job','PERM_TRANS_JOB_SEARCH',4020100,'04020103',0,'')
,(4020104,'Tra cứu giao dịch - Active giao dịch','PERM_TRANS_TRANS_ACTIVE',4020100,'04020104',0,'')
,(4020105,'Tra cứu giao dịch - Inactive giao dịch','PERM_TRANS_TRANS_INACTIVE',4020100,'04020105',0,'')
,(4020106,'Tra cứu giao dịch - Xuất lẻ hóa đơn','PERM_TRANS_SINGLE_INV',4020100,'04020106',0,'')
;
INSERT INTO bahuan.s_role (id,name,code,pid,sort,active,menu_detail) VALUES 
(4020107,'Tra cứu giao dịch - Lập hóa đơn mới','PERM_TRANS_NEW_INV',4020100,'04020107',0,'')
,(4020108,'Tra cứu giao dịch - Lập hóa đơn cho nhiều giao dịch','PERM_TRANS_TRANS_INVS',4020100,'04020108',0,'')
,(4020200,'Duyệt giao dịch','PERM_TRANS_APPR',4020000,'04020200',0,'{"id": "inv.trans_app","icon": "mdi mdi-check-outline", "value": "tran_search_appr"}')
,(4020201,'Duyệt giao dịch - Tìm kiếm','PERM_TRANS_APPR',4020200,'04020201',0,'')
,(4020202,'Duyệt giao dịch - Phê duyệt','PERM_TRANS_APPR_ACCEPT',4020200,'04020202',0,'')
,(4020203,'Duyệt giao dịch - Từ chối','PERM_TRANS_APPR_REJECT',4020200,'04020203',0,'')
,(5000000,'Lập hóa đơn','PERM_CREATE_INV_MANAGE',NULL,'05000000',1,'{"icon": "mdi mdi-content-paste", "value": "invoice_management"}')
,(5010000,'Lập hóa đơn','PERM_CREATE_INV_MANAGE',5000000,'05010000',1,'{"id": "inv.inv", "icon": "mdi mdi-file-plus", "value":"invoice_issue" }')
,(5010100,'Lập hóa đơn - tạo mới','PERM_CREATE_INV_MANAGE',5010000,'05010100',1,'')
,(5010200,'Lập hóa đơn - hiển thị','PERM_CREATE_INV_MANAGE_VIEW',5010000,'05010200',1,'')
;
INSERT INTO bahuan.s_role (id,name,code,pid,sort,active,menu_detail) VALUES 
(5010300,'Lập hóa đơn - lưu','PERM_CREATE_INV_MANAGE_SAVE',5010000,'05010300',1,'')
,(5010400,'Lập hóa đơn - phát hành','PERM_CREATE_INV_MANAGE_ISSUE',5010000,'05010400',1,'')
,(5020000,'Lập hóa đơn online','PERM_INV_ONLINE',5000000,'05020000',0,'{"id": "inv.inv_ol", "icon": "mdi mdi-file-plus", "value": "inv_ol" }')
,(5020100,'Lập hóa đơn online - tìm kiếm','PERM_INV_ONLINE',5020000,'05020100',0,'')
,(5020200,'Lập hóa đơn online - lập hoá đơn','PERM_INV_ONLINE_INSERT',5020000,'05020200',0,'')
,(5030000,'Lập hóa đơn từ Excel','PERM_CREATE_INV_EXCEL',5000000,'05030000',1,'{"id": "inv.xls", "icon": "mdi mdi-upload-multiple", "value": "invoice_from_excel"}')
,(5030100,'Lập hóa đơn từ Excel - Chọn file','PERM_CREATE_INV_EXCEL',5030000,'05030100',1,'')
,(5030200,'Lập hóa đơn từ Excel - mẫu file','PERM_CREATE_INV_EXCEL_FORM',5030000,'05030200',1,'')
,(5030300,'Lập hóa đơn từ Excel - đọc file','PERM_CREATE_INV_EXCEL_READ',5030000,'05030300',1,'')
,(5030400,'Lập hóa đơn từ Excel - lưu file','PERM_CREATE_INV_EXCEL_SAVE',5030000,'05030400',1,'')
;
INSERT INTO bahuan.s_role (id,name,code,pid,sort,active,menu_detail) VALUES 
(5040000,'Cấp số','PERM_CREATE_INV_SEQ',5000000,'05040000',1,'{ "id": "inv.seq", "icon": "mdi mdi-sort-numeric", "value": "invoice_seq"}')
,(5040100,'Cấp số - tìm kiếm','PERM_CREATE_INV_SEQ',5040000,'05040100',1,'')
,(5040200,'Cấp số - cấp số','PERM_CREATE_INV_SEQ_ONE',5040000,'05040200',1,'')
,(5040300,'Cấp số - cấp số tất cả','PERM_CREATE_INV_SEQ_ALL',5040000,'05040300',1,'')
,(5050000,'Duyệt hoá đơn','PERM_APPROVE_INV_MANAGE',5000000,'05050000',1,'{"id": "inv.appr", "icon": "mdi mdi-check-outline", "value": "invoice_approve"}')
,(5050010,'Duyệt hoá đơn','PERM_APPROVE_INV_MANAGE',5000000,'05050010',0,'{"id": "inv.appr_nd123", "icon": "mdi mdi-check-outline", "value": "invoice_approve"}')
,(5050011,'Duyệt hoá đơn - tìm kiếm','PERM_APPROVE_INV_MANAGE',5050010,'05050011',1,'')
,(5050012,'Duyệt hoá đơn - hiển thị','PERM_APPROVE_INV_MANAGE_VIEW',5050010,'05050012',1,'')
,(5050013,'Duyệt hoá đơn - duyệt','PERM_APPROVE_INV_MANAGE_APPR',5050010,'05050013',1,'')
,(5050100,'Duyệt hoá đơn - tìm kiếm','PERM_APPROVE_INV_MANAGE',5050000,'05050100',1,'')
;
INSERT INTO bahuan.s_role (id,name,code,pid,sort,active,menu_detail) VALUES 
(5050200,'Duyệt hoá đơn - hiển thị','PERM_APPROVE_INV_MANAGE_VIEW',5050000,'05050200',1,'')
,(5050300,'Duyệt hoá đơn - duyệt','PERM_APPROVE_INV_MANAGE_APPR',5050000,'05050300',1,'')
,(5060000,'Hủy hóa đơn','PERM_INVOICE_CANCEL',5000000,'05060000',1,'{ "id": "inv.cancel", "icon": "mdi mdi-delete-variant", "value": "invoice_cancel"}')
,(5060100,'Hủy hóa đơn - tìm kiếm','PERM_INVOICE_CANCEL',5060000,'05060100',1,'')
,(5060200,'Hủy hóa đơn - hiển thị','PERM_INVOICE_CANCEL_VIEW',5060000,'05060200',1,'')
,(5060300,'Hủy hóa đơn - duyệt huỷ','PERM_INVOICE_CANCEL_APPR',5060000,'05060300',1,'')
,(5060400,'Hủy hóa đơn - bỏ chờ huỷ','PERM_INVOICE_CANCEL_WAIT',5060000,'05060400',1,'')
,(5070000,'Lập hóa đơn điều chỉnh từ Excel','PERM_CREATE_INV_ADJ_EXCEL',5000000,'05070000',0,'{"id": "inv.xls_adj", "icon": "mdi mdi-upload-multiple", "value":"invoice_adj_from_excel" }')
,(5070100,'Lập hóa đơn điều chỉnh từ Excel - Chọn file','PERM_CREATE_INV_ADJ_EXCEL',5070000,'05070100',0,'')
,(5070200,'Lập hóa đơn điều chỉnh từ Excel - mẫu file','PERM_CREATE_INV_ADJ_EXCEL_FORM',5070000,'05070200',0,'')
;
INSERT INTO bahuan.s_role (id,name,code,pid,sort,active,menu_detail) VALUES 
(5070300,'Lập hóa đơn điều chỉnh từ Excel - đọc file','PERM_CREATE_INV_ADJ_EXCEL_READ',5070000,'05070300',0,'')
,(5070400,'Lập hóa đơn điều chỉnh từ Excel - lưu file','PERM_CREATE_INV_ADJ_EXCEL_SAVE',5070000,'05070400',0,'')
,(5090000,'Xử lý thông báo sai sót','PERM_CREATE_WRONGNOTICE',NULL,'05090000',1,'{"icon": "mdi mdi-alert-circle", "value":"wrongnotice_process" }')
,(5090100,'Thông báo sai sót','PERM_CREATE_WRONGNOTICE',5090000,'05090100',1,'{"id": "wrongnotice.wngroup", "icon": "mdi mdi-upload-multiple", "value":"create_wrong_notice" }')
,(5090110,'Thông báo sai sót - Tìm kiếm','PERM_CREATE_WRONGNOTICE',5090100,'05090110',1,'')
,(5090120,'Thông báo sai sót - Tạo TBSS','PERM_CREATE_WRONGNOTICE_CREATE',5090100,'05090120',1,'')
,(5090130,'Thông báo sai sót - Hiển thị','PERM_CREATE_WRONGNOTICE_VIEW',5090100,'05090130',1,'')
,(5090200,'Tra cứu thông báo sai sót','PERM_CREATE_WRONGNOTICE',5090000,'05090200',1,'{"id": "wrongnotice.wngroupsearch", "icon": "mdi mdi-upload-multiple", "value":"wrongnotice_search" }')
,(5090210,'Tra cứu thông báo sai sót - Tìm kiếm','PERM_CREATE_WRONGNOTICE',5090200,'05090210',1,'')
,(5090220,'Tra cứu thông báo sai sót - Kết quả gửi thuế','PERM_CREATE_WRONGNOTICE_SEND_CQT',5090200,'5090220',1,'')
;
INSERT INTO bahuan.s_role (id,name,code,pid,sort,active,menu_detail) VALUES 
(5090230,'Tra cứu thông báo sai sót - Hiển thị','PERM_CREATE_WRONGNOTICE_VIEW',5090200,'5090230',1,'')
,(6000000,'Ký số tài liệu','PERM_SIGN',NULL,'06000000',0,'{"icon": "mdi mdi-file","value": "file_sign"}')
,(6010000,'Ký Số Tài Liệu - Xác thực','PERM_SIGN_VERIFY',6000000,'06010000',0,'')
,(6020000,'Ký Số Tài Liệu - Ký số','PERM_SIGN_DIGITAL',6000000,'06020000',0,'')
,(6030000,'Ký Số Tài Liệu - PDF file','PERM_SIGN_PDFFILE',6000000,'06030000',0,'')
,(6040000,'Ký Số Tài Liệu - Tải file','PERM_SIGN_UPFILE',6000000,'06040000',0,'')
,(6050000,'Bảng tổng hợp','PERM_BTH',NULL,'06050000',1,'{"icon": "mdi mdi-notebook", "value": "BTH"}')
,(6050100,'Tra cứu bảng tổng hợp','PERM_SEARCH_BTH',6050000,'06050100',1,'{ "id": "inv_bth.search_bth", "icon": "mdi mdi-cart-outline", "value": "search_bth" }')
,(6050110,'Tra cứu bảng tổng hợp - Tìm kiếm','PERM_SEARCH_BTH',6050100,'06050110',1,'')
,(6050120,'Tra cứu bảng tổng hợp - Xoá','PERM_SEARCH_BTH_DELETE',6050100,'6050120',1,'')
;
INSERT INTO bahuan.s_role (id,name,code,pid,sort,active,menu_detail) VALUES 
(6050130,'Tra cứu bảng tổng hợp - Hiển thị','PERM_SEARCH_BTH_VIEW',6050100,'6050130',1,'')
,(6050140,'Tra cứu bảng tổng hợp - Excel','PERM_SEARCH_BTH_EXCEL',6050100,'6050140',1,'')
,(6050200,'Tạo bảng tổng hợp','PERM_CREATE_BTH',6050000,'06050200',1,'{ "id": "inv_bth.create_bth", "icon": "mdi mdi-account-multiple-check", "value": "create_bth" }')
,(6050300,'Duyệt bảng tổng hợp','PERM_APPROVE_BTH',6050000,'06050300',1,'{ "id": "inv_bth.approval_bth", "icon": "mdi mdi-currency-usd", "value": "approval_bth" }')
,(6050310,'Duyệt bảng tổng hợp - Tìm kiếm','PERM_APPROVE_BTH',6050300,'06050310',1,'')
,(6050320,'Duyệt bảng tổng hợp - Duyệt','PERM_APPROVE_BTH_APPR',6050300,'06050320',1,'')
,(6050330,'Duyệt bảng tổng hợp - Hiển thị','PERM_APPROVE_BTH_VIEW',6050300,'06050330',1,'')
,(7000000,'Báo cáo, thống kê','PERM_REPORT_INV',NULL,'07000000',1,'{"id": "rep.report", "icon": "mdi mdi-printer", "value":"reports" }')
,(7010000,'Đặt lại mật khẩu','PERM_RESET_PASSWORD',NULL,'07010000',1,'')
;

  insert into bahuan.s_group_role(group_id,role_id,status) 
select 1,id,null from bahuan.s_role;
commit;

ALTER TABLE s_ou ADD place nvarchar(255);
ALTER TABLE s_inv ADD th_type int
ALTER TABLE s_inv_adj ADD th_type int NOT NULL
ALTER TABLE s_inv ADD wno_adj int
ALTER TABLE s_inv ADD  `wnadjtype` int GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.wnadjtype')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.wnadjtype')))) VIRTUAL
