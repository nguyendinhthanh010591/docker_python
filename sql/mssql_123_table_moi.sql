USE [app_onprem_dev]
GO
/****** Object:  Table [dbo].[s_inv_adj]    Script Date: 09/05/2022 11:13:05 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s_inv_adj](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[inv_id] [bigint] NULL,
	[form] [int] NULL,
	[serial] [nvarchar](10) NULL,
	[stax] [nvarchar](20) NULL,
	[type] [int] NULL,
	[note] [nvarchar](max) NULL,
	[so_tb] [nvarchar](50) NULL,
	[ngay_tb] [date] NULL,
	[createdate] [datetime] NULL,
	[statuscqt] [int] NULL,
	[msgcqt] [nvarchar](max) NULL,
	[listinv_id] [bigint] NULL,
	[th_type] [int] NOT NULL,
	[seq] [nvarchar](8) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s_list_inv]    Script Date: 09/05/2022 11:13:05 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s_list_inv](
	[id] [bigint] NOT NULL,
	[listinv_code] [nvarchar](15) NULL,
	[s_tax] [nvarchar](14) NOT NULL,
	[ou] [int] NOT NULL,
	[created_date] [datetime] NOT NULL,
	[created_by] [nvarchar](50) NULL,
	[approve_date] [datetime] NULL,
	[approve_by] [nvarchar](20) NULL,
	[s_name] [nvarchar](400) NOT NULL,
	[item_type] [nvarchar](20) NOT NULL,
	[listinvoice_num] [nvarchar](5) NULL,
	[period_type] [nvarchar](1) NOT NULL,
	[period] [nvarchar](10) NOT NULL,
	[status] [int] NOT NULL,
	[doc] [nvarchar](max) NULL,
	[xml] [nvarchar](max) NULL,
	[times] [int] NOT NULL,
	[status_received] [int] NULL,
	[xml_received] [nvarchar](max) NULL,
	[json_received] [nvarchar](max) NULL,
	[edittimes] [int] NULL,
	[error_msg_van] [nvarchar](max) NULL,
 CONSTRAINT [s_list_inv_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s_list_inv_cancel]    Script Date: 09/05/2022 11:13:05 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s_list_inv_cancel](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[inv_id] [int] NOT NULL,
	[doc] [nvarchar](max) NOT NULL,
	[invtype] [int] NOT NULL,
	[ou] [int] NOT NULL,
	[serial]  AS (CONVERT([nvarchar](6),json_value([doc],'$.serial'))),
	[type]  AS (CONVERT([nvarchar](10),json_value([doc],'$.type'))),
	[idt]  AS (CONVERT([datetime],json_value([doc],'$.idt'))),
	[cdt] [datetime] NOT NULL,
	[listinv_id] [bigint] NULL,
	[stax]  AS (CONVERT([nvarchar](14),json_value([doc],'$.stax'))),
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s_listvalues]    Script Date: 09/05/2022 11:13:05 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s_listvalues](
	[keyname] [varchar](60) NOT NULL,
	[keyvalue] [nvarchar](max) NOT NULL,
	[keytype] [varchar](30) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s_statement]    Script Date: 09/05/2022 11:13:06 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s_statement](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[type] [nvarchar](15) NULL,
	[formname] [nvarchar](100) NULL,
	[regtype] [int] NULL,
	[sname] [nvarchar](400) NULL,
	[stax] [nvarchar](14) NULL,
	[taxoname] [nvarchar](100) NULL,
	[taxo] [nvarchar](5) NULL,
	[contactname] [nvarchar](50) NULL,
	[contactaddr] [nvarchar](400) NULL,
	[contactemail] [nvarchar](50) NULL,
	[contactphone] [nvarchar](20) NULL,
	[place] [nvarchar](50) NULL,
	[createdt] [datetime] NULL,
	[hascode] [int] NULL,
	[sendtype] [int] NULL,
	[dtsendtype] [nvarchar](20) NULL,
	[invtype] [nvarchar](100) NULL,
	[doc] [nvarchar](max) NULL,
	[status] [int] NOT NULL,
	[cqtstatus] [int] NULL,
	[vdt] [datetime] NULL,
	[cqtmess] [nvarchar](255) NULL,
	[xml] [nvarchar](max) NULL,
	[statustvan] [int] NOT NULL,
	[xml_received] [nvarchar](max) NULL,
	[xml_accepted] [nvarchar](max) NULL,
	[paxoname] [nvarchar](100) NULL,
	[paxo] [nvarchar](5) NULL,
	[json_received] [nvarchar](max) NULL,
	[json_accepted] [nvarchar](max) NULL,
	[uc]  AS (CONVERT([nvarchar](20),json_value([doc],'$.uc'))),
 CONSTRAINT [s_statement_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[s_wrongnotice_process]    Script Date: 09/05/2022 11:13:06 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[s_wrongnotice_process](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ou] [int] NULL,
	[id_wn] [int] NULL,
	[id_inv] [nvarchar](max) NULL,
	[doc] [nvarchar](max) NULL,
	[xml] [nvarchar](max) NULL,
	[dt] [datetime] NOT NULL,
	[status] [nvarchar](15) NOT NULL,
	[status_cqt] [int] NULL,
	[xml_received] [nvarchar](max) NULL,
	[json_received] [nvarchar](max) NULL,
	[xml_accepted] [nvarchar](max) NULL,
	[json_accepted] [nvarchar](max) NULL,
	[dien_giai] [nvarchar](max) NULL,
	[taxc] [nvarchar](14) NULL,
	[form] [nvarchar](14) NULL,
	[serial] [nvarchar](14) NULL,
	[seq] [nvarchar](14) NULL,
	[type] [nvarchar](14) NULL,
 CONSTRAINT [s_wrongnotice_process_pk] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[id_wn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[van_einv_listid_msg]    Script Date: 09/05/2022 11:13:06 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[van_einv_listid_msg](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[pid] [bigint] NULL,
	[type] [nvarchar](10) NULL,
	[status_scan] [int] NULL,
	[createdate] [datetime] NULL,
	[sendvandate] [datetime] NULL,
	[msg] [nvarchar](max) NULL,
	[xml] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[van_history]    Script Date: 09/05/2022 11:13:06 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[van_history](
	[id] [numeric](36, 0) IDENTITY(1,1) NOT NULL,
	[type_invoice] [numeric](18, 0) NOT NULL,
	[id_ref] [bigint] NULL,
	[status] [numeric](18, 0) NULL,
	[datetime_trans] [datetime] NULL,
	[description] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[van_msg_in]    Script Date: 09/05/2022 11:13:06 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[van_msg_in](
	[id] [numeric](18, 0) IDENTITY(0,1) NOT NULL,
	[receive_arr_json] [nvarchar](max) NOT NULL,
	[status_scan] [numeric](18, 0) NULL,
	[createddate] [datetime] NULL,
	[updateddate] [datetime] NULL,
	[createby] [nvarchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[van_msg_in_dtl]    Script Date: 09/05/2022 11:13:06 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[van_msg_in_dtl](
	[id] [numeric](18, 0) IDENTITY(0,1) NOT NULL,
	[receivejson] [nvarchar](max) NOT NULL,
	[taxc]  AS (CONVERT([nvarchar](max),json_value([receivejson],'$.mst'))),
	[namenotice]  AS (CONVERT([nvarchar](500),json_value([receivejson],'$.tenTBao'))),
	[typenotice]  AS (CONVERT([nvarchar](255),json_value([receivejson],'$.loaiTBao'))),
	[contentnotice]  AS (CONVERT([nvarchar](max),json_value([receivejson],'$.ndungTBao'))),
	[createddtnotice]  AS (CONVERT([datetime],json_value([receivejson],'$.ngayTaoTBao'))),
	[createddate] [datetime] NULL,
	[status_scan] [numeric](18, 0) NULL,
	[updateddate] [datetime] NULL,
	[transid] [nvarchar](225) NULL,
	[status_response] [int] NULL,
	[datecheckmsg] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[van_msg_out]    Script Date: 09/05/2022 11:13:06 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[van_msg_out](
	[id] [numeric](18, 0) IDENTITY(0,1) NOT NULL,
	[invid] [nvarchar](36) NOT NULL,
	[xml2base64] [nvarchar](max) NOT NULL,
	[createdby] [nvarchar](150) NOT NULL,
	[statustvan] [numeric](18, 0) NULL,
	[createddate] [datetime] NULL,
	[senddate] [datetime] NULL,
	[idtypeinv] [numeric](18, 0) NOT NULL,
	[messsendcode] [nvarchar](10) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[van_msg_out_dtl]    Script Date: 09/05/2022 11:13:07 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[van_msg_out_dtl](
	[id] [numeric](18, 0) IDENTITY(0,1) NOT NULL,
	[iddatatvan] [numeric](18, 0) NOT NULL,
	[invid] [nvarchar](36) NOT NULL,
	[createdby] [nvarchar](150) NOT NULL,
	[statustvan] [numeric](18, 0) NULL,
	[statusres] [numeric](18, 0) NULL,
	[messsendcode] [nvarchar](10) NULL,
	[messsend] [nvarchar](max) NULL,
	[messres] [nvarchar](255) NULL,
	[messrescode] [nvarchar](10) NULL,
	[senddate] [datetime] NULL,
	[resdate] [datetime] NULL,
	[transid] [nvarchar](225) NULL,
	[idtypeinv] [numeric](18, 0) NOT NULL,
	[status_response] [int] NULL,
	[datecheckmsg] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[s_list_inv] ADD  CONSTRAINT [DF_s_list_inv_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[s_list_inv] ADD  CONSTRAINT [DF_s_list_inv_status]  DEFAULT ((0)) FOR [status]
GO
ALTER TABLE [dbo].[s_list_inv] ADD  DEFAULT ((0)) FOR [times]
GO
ALTER TABLE [dbo].[s_statement] ADD  DEFAULT ((0)) FOR [hascode]
GO
ALTER TABLE [dbo].[s_statement] ADD  DEFAULT ((1)) FOR [status]
GO
ALTER TABLE [dbo].[s_statement] ADD  DEFAULT ((0)) FOR [statustvan]
GO
ALTER TABLE [dbo].[s_wrongnotice_process] ADD  CONSTRAINT [DF_s_wrongnotice_process_dt]  DEFAULT (getdate()) FOR [dt]
GO
ALTER TABLE [dbo].[van_einv_listid_msg] ADD  CONSTRAINT [DF_van_einv_listid_msg_createdate]  DEFAULT (getdate()) FOR [createdate]
GO
ALTER TABLE [dbo].[van_msg_in_dtl] ADD  DEFAULT (getdate()) FOR [createddate]
GO
ALTER TABLE [dbo].[van_msg_in_dtl] ADD  DEFAULT ((0)) FOR [transid]
GO
ALTER TABLE [dbo].[van_msg_in_dtl] ADD  DEFAULT ((0)) FOR [status_response]
GO
ALTER TABLE [dbo].[van_msg_in_dtl] ADD  DEFAULT (getdate()) FOR [datecheckmsg]
GO
ALTER TABLE [dbo].[van_msg_out] ADD  DEFAULT (getdate()) FOR [senddate]
GO
ALTER TABLE [dbo].[van_msg_out_dtl] ADD  DEFAULT ((0)) FOR [status_response]
GO
ALTER TABLE [dbo].[van_msg_out_dtl] ADD  DEFAULT (getdate()) FOR [datecheckmsg]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1: Bảng tổng hợp hóa đơn, 2: Hóa đơn không mã, 3: Hóa đơn có mã, 4: Thông báo sai xót, 5: Tờ khai đăng ký' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'van_history', @level2type=N'COLUMN',@level2name=N'type_invoice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0: not scan, pending: 3, success: 1, error: 2' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'van_msg_in', @level2type=N'COLUMN',@level2name=N'status_scan'
GO
