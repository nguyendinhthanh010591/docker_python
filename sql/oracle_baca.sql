/* Mail */
ALTER TABLE S_INV
ADD (SENDMAILNUM NUMBER(2) GENERATED ALWAYS AS (JSON_VALUE("DOC" FORMAT JSON , '$.SendMailNum' RETURNING NUMBER(2) DEFAULT '0' ON ERROR)) VIRTUAL);

COMMENT ON COLUMN 
S_INV.SENDMAILNUM IS 
'Số lần gửi mail';

ALTER TABLE S_INV
ADD (SENDMAILSTATUS NUMBER(2) GENERATED ALWAYS AS (JSON_VALUE("DOC" FORMAT JSON , '$.SendMailStatus' RETURNING NUMBER(2) DEFAULT '0' ON ERROR)) VIRTUAL);

COMMENT ON COLUMN 
S_INV.SENDMAILSTATUS IS 
'Trạng thái gửi mail';

ALTER TABLE S_INV
ADD (SENDMAILDATE DATE GENERATED ALWAYS AS (JSON_VALUE("DOC" FORMAT JSON , '$.SendMailDate' RETURNING DATE NULL ON ERROR)) VIRTUAL);

COMMENT ON COLUMN 
S_INV.SENDMAILDATE IS 
'Ngày gửi mail';

/* SMS */
ALTER TABLE S_INV
ADD (SENDSMSNUM NUMBER(2) GENERATED ALWAYS AS (JSON_VALUE("DOC" FORMAT JSON , '$.SendSMSNum' RETURNING NUMBER(2) DEFAULT '0' ON ERROR)) VIRTUAL);

COMMENT ON COLUMN 
S_INV.SENDSMSNUM IS 
'Số lần gửi SMS';

ALTER TABLE S_INV
ADD (SENDSMSSTATUS NUMBER(2) GENERATED ALWAYS AS (JSON_VALUE("DOC" FORMAT JSON , '$.SendSMSStatus' RETURNING NUMBER(2) DEFAULT '0' ON ERROR)) VIRTUAL);

COMMENT ON COLUMN 
S_INV.SENDSMSSTATUS IS 
'Trạng thái gửi SMS';

ALTER TABLE S_INV
ADD (SENDSMSDATE DATE GENERATED ALWAYS AS (JSON_VALUE("DOC" FORMAT JSON , '$.SendSMSDate' RETURNING DATE NULL ON ERROR)) VIRTUAL);

COMMENT ON COLUMN 
S_INV.SENDSMSDATE IS 
'Ngày gửi SMS';

CREATE TABLE S_INV_SUM
(
  IDT     DATE                                  NOT NULL,
  STAX    VARCHAR2(20 BYTE)                     NOT NULL,
  TYPE    VARCHAR2(6 BYTE)                      NOT NULL,
  FORM    VARCHAR2(11 BYTE)                     NOT NULL,
  SERIAL  VARCHAR2(8 BYTE),
  I0      NUMBER,
  I1      NUMBER,
  I2      NUMBER,
  I3      NUMBER,
  I4      NUMBER
)
NOCOMPRESS 
NO INMEMORY
TABLESPACE USERS
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
PARTITION BY RANGE (IDT)
INTERVAL( NUMTOYMINTERVAL(1,'MONTH'))
(  
  PARTITION P0 VALUES LESS THAN (TO_DATE(' 2020-04-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    NO INMEMORY
    LOGGING
    NOCOMPRESS 
    TABLESPACE INV
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION VALUES LESS THAN (TO_DATE(' 2020-05-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    NO INMEMORY
    LOGGING
    NOCOMPRESS 
    TABLESPACE INV
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION VALUES LESS THAN (TO_DATE(' 2020-06-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    NO INMEMORY
    LOGGING
    NOCOMPRESS 
    TABLESPACE INV
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION VALUES LESS THAN (TO_DATE(' 2020-07-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    NO INMEMORY
    LOGGING
    NOCOMPRESS 
    TABLESPACE INV
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION VALUES LESS THAN (TO_DATE(' 2020-10-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    NO INMEMORY
    LOGGING
    NOCOMPRESS 
    TABLESPACE INV
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION VALUES LESS THAN (TO_DATE(' 2020-11-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    NO INMEMORY
    LOGGING
    NOCOMPRESS 
    TABLESPACE INV
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION VALUES LESS THAN (TO_DATE(' 2020-12-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    NO INMEMORY
    LOGGING
    NOCOMPRESS 
    TABLESPACE INV
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                INITIAL          8M
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION VALUES LESS THAN (TO_DATE(' 2021-01-01 00:00:00', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    NO INMEMORY
    LOGGING
    NOCOMPRESS 
    TABLESPACE INV
    PCTFREE    10
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               )
)
NOCACHE
RESULT_CACHE (MODE DEFAULT)
NOPARALLEL
MONITORING
/

CREATE UNIQUE INDEX S_INV_SUM_UIDX01 ON S_INV_SUM
(IDT, STAX, TYPE, FORM, SERIAL)
  TABLESPACE IDX
  PCTFREE    10
  INITRANS   2
  MAXTRANS   255
  STORAGE    (
              BUFFER_POOL      DEFAULT
              FLASH_CACHE      DEFAULT
              CELL_FLASH_CACHE DEFAULT
             )
LOGGING
LOCAL (  
  PARTITION P0
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDX
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDX
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDX
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDX
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDX
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDX
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDX
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MAXSIZE          UNLIMITED
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),  
  PARTITION
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDX
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               )
)
NOPARALLEL
/

DROP INDEX S_INV_UID_01
/

CREATE UNIQUE INDEX S_INV_UID_01 ON S_INV
(STAX, FORM, SERIAL, NVL(JSON_VALUE("DOC" FORMAT JSON , '$.seq' RETURNING VARCHAR2(255) NULL ON ERROR),TO_CHAR("ID")))
LOGGING
TABLESPACE USERS
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MAXSIZE          UNLIMITED
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL
/

CREATE OR REPLACE PROCEDURE          PRC_INV_SUM (P_FD DATE, P_TD DATE)
IS
    v_td   DATE := P_FD;
    v_ed   DATE := P_TD;
BEGIN
    FOR d IN 0 .. 365
    LOOP
        BEGIN
            DELETE S_INV_SUM
             WHERE IDT = v_td + d;

            COMMIT;

            INSERT INTO S_INV_SUM (IDT,
                                   STAX,
                                   "TYPE",
                                   FORM,
                                   SERIAL,
                                   I0,
                                   I1,
                                   I2,
                                   I3,
                                   I4)
                  SELECT IDT,
                         STAX,
                         TYPE,
                         FORM,
                         SERIAL,
                         NVL (SUM (1), 0)
                             "i0",
                         NVL (SUM (CASE status WHEN 1 THEN 1 ELSE 0 END), 0)
                             "i1",
                         NVL (SUM (CASE status WHEN 2 THEN 1 ELSE 0 END), 0)
                             "i2",
                         NVL (SUM (CASE status WHEN 3 THEN 1 ELSE 0 END), 0)
                             "i3",
                         NVL (SUM (CASE status WHEN 4 THEN 1 ELSE 0 END), 0)
                             "i4"
                    FROM S_INV
                   WHERE IDT = v_td + d
                GROUP BY IDT,
                         STAX,
                         TYPE,
                         FORM,
                         SERIAL;

            COMMIT;
        EXCEPTION
            WHEN OTHERS
            THEN
                NULL;
        END;

        EXIT WHEN v_td + d > v_ed;
    END LOOP;
END PRC_INV_SUM;
/

BEGIN
  SYS.DBMS_SCHEDULER.CREATE_JOB
    (
       job_name        => 'EINVOICE.RUN_INV_SUM'
      ,start_date      => NULL
      ,repeat_interval => NULL
      ,end_date        => NULL
      ,job_class       => 'DEFAULT_JOB_CLASS'
      ,job_type        => 'PLSQL_BLOCK'
      ,job_action      => 'DECLARE
    -- Declarations
    l_P_FD   DATE;
    l_P_TD   DATE;
BEGIN
    -- Initialization
    l_P_FD := to_date(''01-JAN-2020'');
    l_P_TD := to_date(''31-DEC-2020'');

    -- Call
    EINVOICE.PRC_INV_SUM (P_FD => l_P_FD, P_TD => l_P_TD);

    -- Transaction Control
    COMMIT;
END;'
      ,comments        => NULL
    );
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.RUN_INV_SUM'
     ,attribute => 'RESTARTABLE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'EINVOICE.RUN_INV_SUM'
     ,attribute => 'MAX_FAILURES');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'EINVOICE.RUN_INV_SUM'
     ,attribute => 'MAX_RUNS');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.RUN_INV_SUM'
     ,attribute => 'STOP_ON_WINDOW_CLOSE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.RUN_INV_SUM'
     ,attribute => 'JOB_PRIORITY'
     ,value     => 3);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'EINVOICE.RUN_INV_SUM'
     ,attribute => 'SCHEDULE_LIMIT');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.RUN_INV_SUM'
     ,attribute => 'AUTO_DROP'
     ,value     => TRUE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.RUN_INV_SUM'
     ,attribute => 'RESTART_ON_RECOVERY'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.RUN_INV_SUM'
     ,attribute => 'RESTART_ON_FAILURE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.RUN_INV_SUM'
     ,attribute => 'STORE_OUTPUT'
     ,value     => TRUE);

  SYS.DBMS_SCHEDULER.ENABLE
    (name                  => 'EINVOICE.RUN_INV_SUM');
END;
/

BEGIN
  SYS.DBMS_SCHEDULER.DROP_JOB
    (job_name  => 'EINVOICE.INV_SUM_JOB');
END;
/

BEGIN
  SYS.DBMS_SCHEDULER.CREATE_JOB
    (
       job_name        => 'EINVOICE.INV_SUM_JOB'
      ,start_date      => TO_TIMESTAMP_TZ('2020/12/01 09:36:24.422518 UTC','yyyy/mm/dd hh24:mi:ss.ff tzr')
      ,repeat_interval => 'FREQ=DAILY;BYHOUR=1; BYMINUTE=0;BYSECOND=0'
      ,end_date        => NULL
      ,job_class       => 'DEFAULT_JOB_CLASS'
      ,job_type        => 'PLSQL_BLOCK'
      ,job_action      => 'DECLARE
    -- Declarations
    l_P_FD   DATE ;
    l_P_TD   DATE ;
BEGIN
    -- Initialization
    l_P_FD := trunc(sysdate) - 7;
    l_P_TD := trunc(sysdate);

    -- Call
    EINVOICE.PRC_INV_SUM (P_FD => l_P_FD, P_TD => l_P_TD);

    -- Transaction Control
    COMMIT;
END;'
      ,comments        => 'Job tổng hợp hóa đơn VIB'
    );
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.INV_SUM_JOB'
     ,attribute => 'RESTARTABLE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.INV_SUM_JOB'
     ,attribute => 'LOGGING_LEVEL'
     ,value     => SYS.DBMS_SCHEDULER.LOGGING_OFF);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'EINVOICE.INV_SUM_JOB'
     ,attribute => 'MAX_FAILURES');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'EINVOICE.INV_SUM_JOB'
     ,attribute => 'MAX_RUNS');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.INV_SUM_JOB'
     ,attribute => 'STOP_ON_WINDOW_CLOSE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.INV_SUM_JOB'
     ,attribute => 'JOB_PRIORITY'
     ,value     => 3);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'EINVOICE.INV_SUM_JOB'
     ,attribute => 'SCHEDULE_LIMIT');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.INV_SUM_JOB'
     ,attribute => 'AUTO_DROP'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.INV_SUM_JOB'
     ,attribute => 'RESTART_ON_RECOVERY'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.INV_SUM_JOB'
     ,attribute => 'RESTART_ON_FAILURE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.INV_SUM_JOB'
     ,attribute => 'STORE_OUTPUT'
     ,value     => TRUE);

  SYS.DBMS_SCHEDULER.ENABLE
    (name                  => 'EINVOICE.INV_SUM_JOB');
END;
/

SHOW ERRORS;

DROP TABLE S_REP_VAT_TMP CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE S_REP_VAT_TMP
(
  "id"         NUMBER,
  "idt"        VARCHAR2(10 BYTE),
  "form"       VARCHAR2(11 BYTE),
  "serial"     VARCHAR2(6 BYTE),
  "seq"        VARCHAR2(8 BYTE),
  "btax"       VARCHAR2(14 BYTE),
  "bname"      VARCHAR2(500 CHAR),
  "buyer"      VARCHAR2(500 CHAR),
  "status"     VARCHAR2(255 BYTE),
  "cstatus"    NUMBER,
  "vibstatus"  NUMBER,
  "adjdes"     VARCHAR2(255 BYTE),
  "cde"        VARCHAR2(255 BYTE),
  "adjtyp"     NUMBER(1),
  "dif"        VARCHAR2(4000 BYTE),
  "root"       VARCHAR2(4000 BYTE),
  "adj"        VARCHAR2(4000 BYTE),
  "taxs"       VARCHAR2(4000 BYTE),
  "note"       VARCHAR2(255 BYTE),
  "factor"     NUMBER,
  "refno"      VARCHAR2(255 BYTE),
  "glacc"      VARCHAR2(255 BYTE),
  "sumv"       NUMBER,
  "vatv"       NUMBER,
  "ou"         NUMBER
)
ON COMMIT PRESERVE ROWS
NOCACHE;

DROP PROCEDURE PRC_BCBH_REP;

CREATE OR REPLACE PROCEDURE prc_bcbh_rep (p_fd      DATE,
/* Formatted on 03-Dec-2020 17:24:07 (QP5 v5.276) */
                        p_td      DATE,
                        p_stax    VARCHAR2,
                        p_ou      NUMBER,
                        p_type    VARCHAR2)
IS
BEGIN
    DELETE FROM s_rep_vat_tmp;

    INSERT INTO s_rep_vat_tmp ("id",
                               "idt",
                               "form",
                               "serial",
                               "seq",
                               "btax",
                               "bname",
                               "buyer",
                               "status",
                               "cstatus",
                               "sumv",
                               "vatv",
                               "note",
                               "vibstatus")
        SELECT id "id",
               TO_CHAR (idt, 'DD/MM/YYYY') "idt",
               form "form",
               serial "serial",
               seq "seq",
               btax "btax",
               bname "bname",
               buyer "buyer",
               CASE
                   WHEN status = 1 THEN 'Chờ cấp số'
                   WHEN status = 2 THEN 'Chờ duyệt'
                   WHEN status = 3 THEN 'Đã duyệt'
                   WHEN status = 4 THEN 'Đã hủy'
                   WHEN status = 6 THEN 'Chờ hủy'
                   ELSE TO_CHAR (status)
               END
                   "status",
               status "cstatus",
               sumv "sumv",
               vatv "vatv",
               note || '-' || cde || '-' || adjdes "note",
               CASE WHEN status = 4 AND xml IS NULL THEN 7 ELSE status END
                   "vibstatus"
          FROM s_inv
         WHERE     idt BETWEEN p_fd AND p_td
               AND stax = p_stax
               AND ("TYPE" = p_type OR p_type = '*')
               AND EXISTS
                       (SELECT 1
                          FROM s_ou o
                         WHERE o.id = ou
                        START WITH o.id = p_ou
                        CONNECT BY PRIOR o.id = o.pid);
EXCEPTION
    WHEN NO_DATA_FOUND
    THEN
        NULL;
    WHEN OTHERS
    THEN
        -- Consider logging the error and then re-raise
        RAISE;
END prc_bcbh_rep;
/


DROP PROCEDURE PRC_VAT_REP;

CREATE OR REPLACE PROCEDURE PRC_VAT_REP (P_FD     DATE,
                                         P_TD     DATE,
                                         p_STAX   VARCHAR2,
                                         p_ou     NUMBER)
IS
BEGIN
    DELETE FROM s_rep_vat_tmp;

    INSERT INTO S_REP_VAT_TMP ("id",
                               "idt",
                               "form",
                               "serial",
                               "seq",
                               "btax",
                               "bname",
                               "adjdes",
                               "cde",
                               "adjtyp",
                               "dif",
                               "root",
                               "adj",
                               "taxs",
                               "note",
                               "factor",
                               "refno",
                               "glacc",
                               "ou")
        SELECT *
          FROM (SELECT id                                "id",
                       TO_CHAR (a.idt, 'DD/MM/YYYY')     "idt",
                       a.form                            "form",
                       a.serial                          "serial",
                       a.seq                             "seq",
                       a.btax                            "btax",
                       a.bname                           "bname",
                       a.adjdes                          "adjdes",
                       a.cde                             "cde",
                       a.adjtyp                          "adjtyp",
                       a.doc.dif                         "dif",
                       a.doc.root                        "root",
                       a.doc.adj                         "adj",
                       NULL                              "taxs",
                       note                              "note",
                       1                                 AS "factor",
                       c2                                "refno",
                       c1                                "glacc",
                       ou                                "ou"
                  FROM s_inv a
                 WHERE     a.idt BETWEEN P_FD AND P_TD
                       AND a.stax = p_STAX
                       AND a.TYPE = '01GTKT'
                       AND a.status in (3,4)
                       AND EXISTS
                               (    SELECT 1
                                      FROM s_ou o
                                     WHERE o.id = a.ou
                                START WITH o.id = p_ou
                                CONNECT BY PRIOR o.id = o.pid)
                UNION ALL
                SELECT id
                           "id",
                       TO_CHAR (a.cdt, 'DD/MM/YYYY')
                           "idt",
                       a.form
                           "form",
                       a.serial
                           "serial",
                       a.seq
                           "seq",
                       a.btax
                           "btax",
                       a.bname
                           "bname",
                       a.adjdes
                           "adjdes",
                       a.cde
                           "cde",
                       a.adjtyp
                           "adjtyp",
                       a.doc.dif
                           "dif",
                       a.doc.root
                           "root",
                       a.doc.adj
                           "adj",
                       NULL
                           "taxs",
                       CASE
                           WHEN JSON_VALUE (doc, '$.cancel.rea') IS NOT NULL
                           THEN
                               JSON_VALUE (doc, '$.cancel.rea')
                           ELSE
                               cde
                       END
                           "note",
                       -1
                           AS "factor",
                       c2
                           "refno",
                       c1
                           "glacc",
                       ou
                           "ou"
                  FROM s_inv a
                 WHERE     a.cdt BETWEEN P_FD AND P_TD
                       AND a.stax = p_STAX
                       AND a.TYPE = '01GTKT'
                       AND a.status = 4
                       AND a.xml IS NOT NULL
                       AND EXISTS
                               (    SELECT 1
                                      FROM s_ou o
                                     WHERE o.id = a.ou
                                START WITH o.id = p_ou
                                CONNECT BY PRIOR o.id = o.pid)/*
                UNION ALL
                SELECT id                                "id",
                       TO_CHAR (a.idt, 'DD/MM/YYYY')     "idt",
                       a.form                            "form",
                       a.serial                          "serial",
                       a.seq                             "seq",
                       a.btax                            "btax",
                       a.bname                           "bname",
                       a.adjdes                          "adjdes",
                       a.cde                             "cde",
                       a.adjtyp                          "adjtyp",
                       a.doc.dif                         "dif",
                       a.doc.root                        "root",
                       a.doc.adj                         "adj",
                       NULL                              "taxs",
                       note                              "note",
                       1                                 AS "factor",
                       c2                                "refno",
                       c1                                "glacc",
                       ou                                "ou"
                  FROM s_inv a
                 WHERE     a.idt BETWEEN P_FD AND P_TD
                       AND a.stax = p_STAX
                       AND a.TYPE = '01GTKT'
                       AND a.status = 4
                       AND a.xml IS NOT NULL
                       AND EXISTS
                               (    SELECT 1
                                      FROM s_ou o
                                     WHERE o.id = a.ou
                                START WITH o.id = p_ou
                                CONNECT BY PRIOR o.id = o.pid)*/);
EXCEPTION
    WHEN NO_DATA_FOUND
    THEN
        NULL;
    WHEN OTHERS
    THEN
        -- Consider logging the error and then re-raise
        RAISE;
END PRC_VAT_REP;
/


BEGIN
  SYS.DBMS_SCHEDULER.DROP_JOB
    (job_name  => 'EINVOICE.ANALYS_S_INV_JOB');
END;
/

BEGIN
  SYS.DBMS_SCHEDULER.CREATE_JOB
    (
       job_name        => 'EINVOICE.ANALYS_S_INV_JOB'
      ,start_date      => TO_TIMESTAMP_TZ('2021/01/12 03:45:21.688015 UTC','yyyy/mm/dd hh24:mi:ss.ff tzr')
      ,repeat_interval => 'FREQ=HOURLY;INTERVAL=3;BYMINUTE=0;BYSECOND=0'
      ,end_date        => NULL
      ,job_class       => 'DEFAULT_JOB_CLASS'
      ,job_type        => 'PLSQL_BLOCK'
      ,job_action      => 'BEGIN
  
  SYS.DBMS_STATS.GATHER_TABLE_STATS (
     OwnName           => ''EINVOICE''
    ,TabName           => ''S_INV''
    ,Estimate_Percent  => SYS.DBMS_STATS.AUTO_SAMPLE_SIZE
    ,Method_Opt        => ''FOR ALL COLUMNS SIZE AUTO ''
    ,Degree            => NULL
    ,Cascade           => DBMS_STATS.AUTO_CASCADE
    ,No_Invalidate     => DBMS_STATS.AUTO_INVALIDATE
    ,Force             => FALSE);
    
    SYS.DBMS_STATS.GATHER_INDEX_STATS (
     OwnName           => ''EINVOICE''
    ,IndName           => ''S_INV_DOC_IDX''
    ,Estimate_Percent  => SYS.DBMS_STATS.AUTO_SAMPLE_SIZE
    ,Degree            => NULL
    ,No_Invalidate     => DBMS_STATS.AUTO_INVALIDATE
    ,Force             => FALSE);
    
    EXECUTE IMMEDIATE ''ALTER INDEX S_INV_DOC_IDX REBUILD'';
    
END;

'
      ,comments        => NULL
    );
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.ANALYS_S_INV_JOB'
     ,attribute => 'RESTARTABLE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.ANALYS_S_INV_JOB'
     ,attribute => 'LOGGING_LEVEL'
     ,value     => SYS.DBMS_SCHEDULER.LOGGING_OFF);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'EINVOICE.ANALYS_S_INV_JOB'
     ,attribute => 'MAX_FAILURES');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'EINVOICE.ANALYS_S_INV_JOB'
     ,attribute => 'MAX_RUNS');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.ANALYS_S_INV_JOB'
     ,attribute => 'STOP_ON_WINDOW_CLOSE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.ANALYS_S_INV_JOB'
     ,attribute => 'JOB_PRIORITY'
     ,value     => 3);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'EINVOICE.ANALYS_S_INV_JOB'
     ,attribute => 'SCHEDULE_LIMIT');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.ANALYS_S_INV_JOB'
     ,attribute => 'AUTO_DROP'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.ANALYS_S_INV_JOB'
     ,attribute => 'RESTART_ON_RECOVERY'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.ANALYS_S_INV_JOB'
     ,attribute => 'RESTART_ON_FAILURE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'EINVOICE.ANALYS_S_INV_JOB'
     ,attribute => 'STORE_OUTPUT'
     ,value     => TRUE);

  SYS.DBMS_SCHEDULER.ENABLE
    (name                  => 'EINVOICE.ANALYS_S_INV_JOB');
END;
/
