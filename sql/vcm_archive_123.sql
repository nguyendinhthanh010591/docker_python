ALTER TABLE [dbo].[s_inv]
DROP COLUMN seq;
ALTER TABLE [dbo].[s_inv] ADD seq AS (CONVERT([nvarchar](8),json_value([doc],'$.seq')))	 

ALTER TABLE s_inv add [sendmaildate]  AS (CONVERT([datetime],json_value([doc],'$.SendMailDate')))

	 ALTER TABLE s_inv add 	[sendsmsdate]  AS (CONVERT([datetime],json_value([doc],'$.SendSMSlDate')))
	 
     ALTER TABLE s_inv add [vseq]  AS (TRY_CAST(isnull(case when TRY_CAST(json_value([doc],'$.seq') AS [nvarchar](18))='' then NULL else TRY_CAST(json_value([doc],'$.seq') AS [nvarchar](18)) end,CONVERT([nvarchar](18),[id])) AS [nvarchar](18))) PERSISTED
	 
     ALTER TABLE s_inv add [sendmailstatus]  AS (case when CONVERT([int],json_value([doc],'$.SendMailStatus')) IS NULL then (0) else CONVERT([int],json_value([doc],'$.SendMailStatus')) end)
	
     ALTER TABLE s_inv add [sendsmsstatus]  AS (case when CONVERT([int],json_value([doc],'$.SendSMSStatus')) IS NULL then (0) else CONVERT([int],json_value([doc],'$.SendSMSStatus')) end)
	
     ALTER TABLE s_inv add [sendmailnum]  AS (case when CONVERT([int],json_value([doc],'$.SendMailNum')) IS NULL then (0) else CONVERT([int],json_value([doc],'$.SendMailNum')) end)
	
     ALTER TABLE s_inv add [sendsmsnum]  AS (case when CONVERT([int],json_value([doc],'$.SendSMSNum')) IS NULL then (0) else CONVERT([int],json_value([doc],'$.SendSMSNum')) end)
	
     ALTER TABLE s_inv add [bcode]  AS (CONVERT([nvarchar](500),json_value([doc],'$.bcode')))
     
	ALTER TABLE s_inv add [taxocode]  AS (CONVERT([nvarchar](20),json_value([doc],'$.taxocode')))
	
	ALTER TABLE s_inv add [invlistdt]  AS (CONVERT([datetime],json_value([doc],'$.invlistdt')))
	
	ALTER TABLE s_inv add [discountamt]  AS (case when CONVERT([numeric],json_value([doc],'$.discountamt')) IS NULL then (0) else CONVERT([numeric],json_value([doc],'$.discountamt')) end)
	
	ALTER TABLE s_inv add [ordno]  AS (CONVERT([nvarchar](500),json_value([doc],'$.ordno')))
	
	ALTER TABLE s_inv add [whsfr]  AS (CONVERT([nvarchar](500),json_value([doc],'$.whsfr')))
	
	ALTER TABLE s_inv add [whsto]  AS (CONVERT([nvarchar](500),json_value([doc],'$.whsto')))
	
	ALTER TABLE s_inv add [edt] [datetime] NULL
	
	ALTER TABLE s_inv add [status_received]  AS (case when CONVERT([int],json_value([doc],'$.status_received')) IS NULL then (0) else CONVERT([int],json_value([doc],'$.status_received')) end)
	
	ALTER TABLE s_inv add  [xml_received] [nvarchar](max) NULL
	
	ALTER TABLE s_inv add  [hascode]  AS (CONVERT([numeric](500),json_value([doc],'$.hascode')))
	
	ALTER TABLE s_inv add  [invtype]  AS (CONVERT([nvarchar](500),json_value([doc],'$.invtype')))
    
	ALTER TABLE s_inv add 	[invlistnum] [nvarchar](50) NULL
    
	ALTER TABLE s_inv add 	[period_type] [nvarchar](1) NULL
   
	ALTER TABLE s_inv ADD period NVARCHAR(10) NULL;

	ALTER TABLE s_inv add [list_invid] [bigint] NULL
	
	ALTER TABLE s_inv add [status_tbss] [int] NULL
	
	ALTER TABLE s_inv add  [orddt]  AS (CONVERT([datetime],json_value([doc],'$.orddt')))
	
	ALTER TABLE s_inv add  [error_msg_van] [nvarchar](max) NULL
	
	ALTER TABLE s_inv add  [vehic]  AS (CONVERT([nvarchar](500),json_value([doc],'$.vehic')))

	ALTER TABLE s_inv ADD inv_adj bigint
	
	--view
	ALTER VIEW [dbo].[s_role] AS SELECT * FROM [VCM_UAT_APP].[dbo].[s_role]
	
	ALTER VIEW [dbo].[s_ou] AS SELECT * FROM [VCM_UAT_APP].[dbo].[s_ou]
	
	CREATE VIEW [dbo].[s_statement] AS SELECT * FROM [VCM_UAT_APP].[dbo].[s_statement]
	
	ALTER VIEW [dbo].[s_user] AS SELECT * FROM [VCM_UAT_APP].[dbo].[s_user]
	
	ALTER VIEW [dbo].[s_sys_logs] AS SELECT * FROM [VCM_UAT_APP].[dbo].[s_sys_logs]
	
	ALTER VIEW [dbo].[s_ou] AS SELECT * FROM [VCM_UAT_APP].[dbo].[s_ou]
	
	
	
	
	
	
	
	
	
	
	
	
	
	