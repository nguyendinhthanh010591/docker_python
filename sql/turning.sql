ALTER TABLE s_inv
  ALTER COLUMN [type] 
  ADD PERSISTED;
       ALTER TABLE s_inv
  ALTER COLUMN [status] 
  ADD PERSISTED;
   ALTER TABLE s_inv
  ALTER COLUMN [stax] 
  ADD PERSISTED;
   ALTER TABLE s_inv
  ALTER COLUMN [form]
  ADD PERSISTED;
      ALTER TABLE s_inv
  ALTER COLUMN [serial]
  ADD PERSISTED;  
      ALTER TABLE s_inv
  ALTER COLUMN [seq]
  ADD PERSISTED;  
       ALTER TABLE s_inv
  ALTER COLUMN [paym]
  ADD PERSISTED; 
      ALTER TABLE s_inv
  ALTER COLUMN [curr]
  ADD PERSISTED;  
       ALTER TABLE s_inv
  ALTER COLUMN [exrt]
  ADD PERSISTED; 
       ALTER TABLE s_inv
  ALTER COLUMN [sname]
  ADD PERSISTED; 
 
      ALTER TABLE s_inv
  ALTER COLUMN [saddr]
  ADD PERSISTED;  
      ALTER TABLE s_inv
  ALTER COLUMN [smail]
  ADD PERSISTED;  
      ALTER TABLE s_inv
  ALTER COLUMN [stel]
  ADD PERSISTED;  
      ALTER TABLE s_inv
  ALTER COLUMN [sacc] 
  ADD PERSISTED; 
      ALTER TABLE s_inv
  ALTER COLUMN [sbank] 
  ADD PERSISTED;  
      ALTER TABLE s_inv
  ALTER COLUMN [bname] 
  ADD PERSISTED;  
      ALTER TABLE s_inv
  ALTER COLUMN [btax] 
  ADD PERSISTED;  
      ALTER TABLE s_inv
  ALTER COLUMN [baddr] 
  ADD PERSISTED;  
       ALTER TABLE s_inv
  ALTER COLUMN [bmail] 
  ADD PERSISTED; 
      ALTER TABLE s_inv
  ALTER COLUMN [btel] 
  ADD PERSISTED;  
      ALTER TABLE s_inv
  ALTER COLUMN [bacc] 
  ADD PERSISTED;  
     ALTER TABLE s_inv
  ALTER COLUMN [bbank] 
  ADD PERSISTED;  
      ALTER TABLE s_inv
  ALTER COLUMN [note] 
  ADD PERSISTED;  
      ALTER TABLE s_inv
  ALTER COLUMN [sum] 
  ADD PERSISTED;  
      ALTER TABLE s_inv
  ALTER COLUMN [sumv] 
  ADD PERSISTED;  
     ALTER TABLE s_inv
  ALTER COLUMN [vat] 
  ADD PERSISTED;   
      ALTER TABLE s_inv
  ALTER COLUMN [vatv] 
  ADD PERSISTED;  
     ALTER TABLE s_inv
  ALTER COLUMN [total] 
  ADD PERSISTED;   
      ALTER TABLE s_inv
  ALTER COLUMN [totalv] 
  ADD PERSISTED;  
       ALTER TABLE s_inv
  ALTER COLUMN [adjseq] 
  ADD PERSISTED; 
      ALTER TABLE s_inv
  ALTER COLUMN [adjdes] 
  ADD PERSISTED;  
      ALTER TABLE s_inv
  ALTER COLUMN [adjtyp] 
  ADD PERSISTED;  
      ALTER TABLE s_inv
  ALTER COLUMN [c0] 
  ADD PERSISTED;  
      ALTER TABLE s_inv
  ALTER COLUMN [c1] 
  ADD PERSISTED;  
      ALTER TABLE s_inv

  ALTER COLUMN [c2] 
  ADD PERSISTED;  
       ALTER TABLE s_inv
  ALTER COLUMN [c3] 
  ADD PERSISTED; 
      ALTER TABLE s_inv
  ALTER COLUMN [c4] 
  ADD PERSISTED;  
      ALTER TABLE s_inv
  ALTER COLUMN [c5] 
  ADD PERSISTED;  
       ALTER TABLE s_inv
  ALTER COLUMN [c6] 
  ADD PERSISTED; 
      ALTER TABLE s_inv
  ALTER COLUMN [c7] 
  ADD PERSISTED;  
      ALTER TABLE s_inv
  ALTER COLUMN [c8] 
  ADD PERSISTED;  
      ALTER TABLE s_inv
  ALTER COLUMN [c9]
  ADD PERSISTED;  
     ALTER TABLE s_inv
  ALTER COLUMN [cdt] 
  ADD PERSISTED;   
      ALTER TABLE s_inv
  ALTER COLUMN [buyer] 
  ADD PERSISTED;  
  


CREATE UNIQUE NONCLUSTERED INDEX [S_INV_UID_01] ON [dbo].[s_inv]
(
	[STAX] ASC, 
	[FORM] ASC, 
	[SERIAL] ASC, 
	[VSEQ] ASC
)