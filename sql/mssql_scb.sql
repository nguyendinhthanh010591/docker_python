ALTER TABLE [dbo].[s_role] ADD [scbname] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
/****** Object:  Table [dbo].[s_role_map_oud]    Script Date: 26/12/2020 8:10:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--Bảng map role group
CREATE TABLE [dbo].[s_role_map_oud](
	[einv_code] [varchar](200) NOT NULL,
	[oud_role_code] [varchar](200) NOT NULL,
 CONSTRAINT [role_map_oud_uk01] UNIQUE NONCLUSTERED 
(
	[einv_code] ASC,
	[oud_role_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 100) ON [PRIMARY]
) ON [PRIMARY]
GO

--Cột ngày update 
ALTER TABLE [dbo].[s_user] ADD [update_date] datetime NULL
GO

--Bảng map ou
CREATE TABLE [dbo].[s_ou_map_oud] (
[ouid] int NOT NULL,
[oud_ouid] varchar(200) NOT NULL,
CONSTRAINT [ou_map_oud_uk01]
UNIQUE NONCLUSTERED ([ouid] ASC, [oud_ouid] ASC)
WITH ( PAD_INDEX = OFF,
FILLFACTOR = 100,
IGNORE_DUP_KEY = OFF,
STATISTICS_NORECOMPUTE = OFF,
ALLOW_ROW_LOCKS = ON,
ALLOW_PAGE_LOCKS = ON,
DATA_COMPRESSION = NONE )
 ON [PRIMARY]
)
ON [PRIMARY]
WITH (DATA_COMPRESSION = NONE);
GO
ALTER TABLE [dbo].[s_ou_map_oud] SET (LOCK_ESCALATION = TABLE);
GO

--Create view for Onecert
CREATE VIEW [dbo].[vwUserAccount] AS
SELECT u.id
          AS ACCOUNT_NAME,
       CASE WHEN u.uc = 1 THEN 'Active' ELSE 'Inactive' END
          AS ACCOUNT_STATUS,
       g.[NAME]
          AS ENTITLEMENT_NAME,
       u.id
          AS ACCOUNT_OWNER,
       'User'
          AS ACCOUNT_TYPE,
       NULL
          AS ACCOUNT_DESCRIPTION,
       u.last_login
          AS LAST_LOGIN
FROM (dbo.s_group_user gu
      INNER JOIN dbo.s_group g ON (gu.GROUP_ID = g.ID))
     INNER JOIN dbo.s_user u ON (gu.[USER_ID] = u.id)
GO

CREATE VIEW [dbo].[vwEntitlement] AS
SELECT g.[NAME] AS ENTITLEMENT_NAME,
       g.DES AS ENTITLEMENT_DESCRIPTION,
       NULL AS ENTITLEMENT_OWNER,
       CASE WHEN g.STATUS = 1 THEN 'Yes' ELSE 'No' END AS IS_PRIVILEGED
FROM dbo.s_group g
GO

ALTER TABLE [dbo].[s_sys_logs] ADD [ip] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO

/****** Object: Table [dbo].[s_segment]   Script Date: 22/02/2021 3:10:17 PM ******/
CREATE TABLE [dbo].[s_segment] (
[id] bigint IDENTITY(1, 1) NOT NULL,
[seg_name] nvarchar(200) NULL,
CONSTRAINT [PK__s_segmen__3213E83F4DE9F591]
PRIMARY KEY CLUSTERED ([id] ASC)
WITH ( PAD_INDEX = OFF,
FILLFACTOR = 100,
IGNORE_DUP_KEY = OFF,
STATISTICS_NORECOMPUTE = OFF,
ALLOW_ROW_LOCKS = ON,
ALLOW_PAGE_LOCKS = ON,
DATA_COMPRESSION = NONE )
 ON [PRIMARY]
)
ON [PRIMARY];
GO
ALTER TABLE [dbo].[s_segment] SET (LOCK_ESCALATION = TABLE);
GO

--Thêm bảng s_dept, s_deptusr
CREATE TABLE [dbo].[s_dept] (
[deptid] varchar(10) NOT NULL,
[deptname] nvarchar(200) NOT NULL,
[desc] nvarchar(300) NULL,
CONSTRAINT [s_dept_pk]
PRIMARY KEY CLUSTERED ([deptid] ASC)
WITH ( PAD_INDEX = OFF,
FILLFACTOR = 100,
IGNORE_DUP_KEY = OFF,
STATISTICS_NORECOMPUTE = OFF,
ALLOW_ROW_LOCKS = ON,
ALLOW_PAGE_LOCKS = ON,
DATA_COMPRESSION = NONE )
 ON [PRIMARY]
)
ON [PRIMARY];
GO
ALTER TABLE [dbo].[s_dept] SET (LOCK_ESCALATION = TABLE);

CREATE TABLE [dbo].[s_deptusr] (
[deptid] varchar(10) NOT NULL,
[usrid] nvarchar(20) NOT NULL,
CONSTRAINT [PK__s_deptus__3D0FFE37417B81BC]
PRIMARY KEY CLUSTERED ([deptid] ASC, [usrid] ASC)
WITH ( PAD_INDEX = OFF,
FILLFACTOR = 100,
IGNORE_DUP_KEY = OFF,
STATISTICS_NORECOMPUTE = OFF,
ALLOW_ROW_LOCKS = ON,
ALLOW_PAGE_LOCKS = ON,
DATA_COMPRESSION = NONE )
 ON [PRIMARY],
CONSTRAINT [FK__s_deptusr__usrid__4FF1D159]
FOREIGN KEY ([usrid])
REFERENCES [dbo].[s_user] ( [id] ),
CONSTRAINT [FK__s_deptusr__depti__64ECEE3F]
FOREIGN KEY ([deptid])
REFERENCES [dbo].[s_dept] ( [deptid] )
)
ON [PRIMARY];
GO
ALTER TABLE [dbo].[s_deptusr] SET (LOCK_ESCALATION = TABLE);

GO
ALTER TABLE [dbo].[s_trans] ADD [deptid] varchar(10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO

--Bảng lưu dữ liệu tạm để gửi lên Solace Queue sau khi đã tạo xong email
DROP TABLE [dbo].[s_solace_msg_temp]
GO
SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
CREATE TABLE [dbo].[s_solace_msg_temp] (
	[id] bigint IDENTITY(1, 1),
	[topic_name] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[queue_name] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[msg_content] ntext COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[inv_id] bigint NULL,
	[inv_doctemp_id] bigint NULL,
	[co_id] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	CONSTRAINT [PK__s_solace__3213E83F9E94A68A] PRIMARY KEY([id]) WITH (FILLFACTOR=100,
		DATA_COMPRESSION = NONE) ON [PRIMARY]
)
GO
EXECUTE [sp_addextendedproperty]
	@name = N'MS_Description',
	@value = N'ID table',
	@level0type = 'SCHEMA',
	@level0name = N'dbo',
	@level1type = 'TABLE',
	@level1name = N's_solace_msg_temp',
	@level2type = 'COLUMN',
	@level2name = N'id'
GO
EXECUTE [sp_addextendedproperty]
	@name = N'MS_Description',
	@value = N'Topic Name of message',
	@level0type = 'SCHEMA',
	@level0name = N'dbo',
	@level1type = 'TABLE',
	@level1name = N's_solace_msg_temp',
	@level2type = 'COLUMN',
	@level2name = N'topic_name'
GO
EXECUTE [sp_addextendedproperty]
	@name = N'MS_Description',
	@value = N'Queue Name of message',
	@level0type = 'SCHEMA',
	@level0name = N'dbo',
	@level1type = 'TABLE',
	@level1name = N's_solace_msg_temp',
	@level2type = 'COLUMN',
	@level2name = N'queue_name'
GO
EXECUTE [sp_addextendedproperty]
	@name = N'MS_Description',
	@value = N'Content of message',
	@level0type = 'SCHEMA',
	@level0name = N'dbo',
	@level1type = 'TABLE',
	@level1name = N's_solace_msg_temp',
	@level2type = 'COLUMN',
	@level2name = N'msg_content'
GO
EXECUTE [sp_addextendedproperty]
	@name = N'MS_Description',
	@value = N'Invoice ID',
	@level0type = 'SCHEMA',
	@level0name = N'dbo',
	@level1type = 'TABLE',
	@level1name = N's_solace_msg_temp',
	@level2type = 'COLUMN',
	@level2name = N'inv_id'
GO
EXECUTE [sp_addextendedproperty]
	@name = N'MS_Description',
	@value = N's_invdoctemp ID',
	@level0type = 'SCHEMA',
	@level0name = N'dbo',
	@level1type = 'TABLE',
	@level1name = N's_solace_msg_temp',
	@level2type = 'COLUMN',
	@level2name = N'inv_doctemp_id'
GO

--Bảng lưu dữ liệu log các msg gửi lên và nhận về từ Solace Queue
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
DROP TABLE [dbo].[s_solace_msg]
GO

CREATE TABLE [dbo].[s_solace_msg] (
[msg_id] varchar(100) NOT NULL,
[type] int NOT NULL,
[dt] datetime NOT NULL DEFAULT (getdate()),
[co_id] varchar(100) NULL,
[topic_name] varchar(50) NULL,
[queue_name] varchar(50) NULL,
[msg_content] ntext NULL,
[inv_id] bigint NULL,
[inv_doctemp_id] bigint NULL,
CONSTRAINT [s_sol_msg_pk]
PRIMARY KEY CLUSTERED ([msg_id] ASC, [type] ASC)
WITH ( PAD_INDEX = OFF,
FILLFACTOR = 100,
IGNORE_DUP_KEY = OFF,
STATISTICS_NORECOMPUTE = OFF,
ALLOW_ROW_LOCKS = ON,
ALLOW_PAGE_LOCKS = ON,
DATA_COMPRESSION = NONE )
 ON [PRIMARY]
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY];
GO
ALTER TABLE [dbo].[s_solace_msg] SET (LOCK_ESCALATION = TABLE);
GO

ALTER TABLE [dbo].[s_solace_msg]
ALTER COLUMN [topic_name] varchar(100) COLLATE SQL_Latin1_General_CP1_CI_AS


-- 
ALTER TABLE [dbo].[s_trans]
ALTER COLUMN [depid] varchar(100) DEFAULT 0 not null



CREATE TABLE [dbo].[s_user_sys](
	[USERID] [nvarchar](50) NOT NULL,
	[SYSTEM] [nvarchar](50) NOT NULL
) ON [PRIMARY]


CREATE TABLE [dbo].[s_customer_mail](
	[code] [varchar](50) NOT NULL,
	[mail] [varchar](max) NOT NULL,
	[ods] [date] NOT NULL,
 CONSTRAINT [PK_S_CUSTOMER_MAIL] PRIMARY KEY CLUSTERED 
(
	[code] ASC,
	[ods] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [idx_strans_date] ON [dbo].[s_trans]
(
	[valueDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER TABLE [dbo].[s_trans] ADD [acclasscode] varchar(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL;

CREATE NONCLUSTERED INDEX [idx_invID]
ON [dbo].[s_trans]
([inv_id])
WITH
(
PAD_INDEX = OFF,
FILLFACTOR = 100,
IGNORE_DUP_KEY = OFF,
STATISTICS_NORECOMPUTE = OFF,
ONLINE = OFF,
ALLOW_ROW_LOCKS = ON,
ALLOW_PAGE_LOCKS = ON,
DATA_COMPRESSION = NONE
)
ON [PRIMARY];
GO

ALTER TABLE [dbo].[s_trans] ADD [exception] numeric(18, 0) NULL;

CREATE TABLE [dbo].[einv_psgl_reconciliation] (
[rowid] nvarchar(250) NOT NULL,
[s_deleted_flag] varchar(50) NULL,
[je_header_id] varchar(50) NULL,
[je_line_cr_local_amt] decimal(30, 5) NULL,
[je_line_dr_local_amt] decimal(30, 5) NULL,
[je_source_id] varchar(50) NULL,
[je_line_desc] nvarchar(500) NULL,
[je_line_dr_trans_amt] decimal(30, 5) NULL,
[je_line_cr_trans_amt] decimal(30, 5) NULL,
[ps_journal_dt] date NULL,
[ps_fiscal_year_id] decimal(30, 0) NULL,
[ps_journal_line_ref] varchar(50) NULL,
[je_header_desc] nvarchar(500) NULL,
[je_header_name] nvarchar(500) NULL,
[je_header_posted_dt] date NULL,
[ps_ledger_group_id] varchar(50) NULL,
[ps_transaction_dt] date NULL,
[period_key] numeric(38, 0) NULL,
[journal_line_key] varchar(50) NULL,
[sl_bal_rk_id] numeric(38, 0) NULL,
[process_name] nvarchar(500) NULL,
[ps_oprid] varchar(50) NULL,
[ps_chartfields_id] numeric(18, 0) NULL,
[ps_business_unit_id] varchar(50) NULL,
[ps_account_id] varchar(50) NULL,
[ps_operating_unit_id] varchar(50) NULL,
[currency_cd] varchar(50) NULL,
[ods] varchar(10) NULL,
[process_date] varchar(10) NULL,
[process_status] numeric(1, 0) NULL DEFAULT ((0)),
[process_mess] nvarchar(MAX) NULL,
CONSTRAINT [einv_psgl_reconciliation_PK]
PRIMARY KEY CLUSTERED ([rowid] ASC)
WITH ( PAD_INDEX = OFF,
FILLFACTOR = 100,
IGNORE_DUP_KEY = OFF,
STATISTICS_NORECOMPUTE = OFF,
ALLOW_ROW_LOCKS = ON,
ALLOW_PAGE_LOCKS = ON,
DATA_COMPRESSION = NONE )
 ON [PRIMARY]
)
ON [PRIMARY]
TEXTIMAGE_ON [PRIMARY];
GO
ALTER TABLE [dbo].[einv_psgl_reconciliation] SET (LOCK_ESCALATION = TABLE);
GO

/****** Object: Index [dbo].[einv_psgl_reconciliation].[idx_psgl_reconciliation_date]   Script Date: 09/04/2021 2:48:55 PM ******/

CREATE NONCLUSTERED INDEX [idx_psgl_reconciliation_date]
ON [dbo].[einv_psgl_reconciliation]
([ps_journal_dt] , [je_source_id] , [ps_account_id])
WITH
(
PAD_INDEX = OFF,
FILLFACTOR = 100,
IGNORE_DUP_KEY = OFF,
STATISTICS_NORECOMPUTE = OFF,
ONLINE = OFF,
ALLOW_ROW_LOCKS = ON,
ALLOW_PAGE_LOCKS = ON,
DATA_COMPRESSION = NONE
)
ON [PRIMARY];
GO