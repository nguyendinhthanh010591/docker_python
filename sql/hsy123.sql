CREATE TABLE `s_xls_temp` (`id` VARCHAR(20) NULL, `ic` VARCHAR(50) NULL, `idt` datetime(3) NULL, `form` VARCHAR(11) NULL, `serial` VARCHAR(10) NULL, `seq` VARCHAR(8) NULL, `dt` datetime not null DEFAULT CURRENT_TIMESTAMP);

CREATE TABLE `s_list_inv_cancel` (`id` BIGINT AUTO_INCREMENT NOT NULL, `inv_id` INT NOT NULL, `doc` json DEFAULT NULL, `invtype` INT NOT NULL, `ou` INT NOT NULL, `serial` NVARCHAR(6) NULL, `type` NVARCHAR(10) NULL, `idt` datetime(3) NULL, `cdt` datetime(3) NOT NULL, `listinv_id` BIGINT NULL, `stax` NVARCHAR(14) NULL, CONSTRAINT `PK__s_list_i__3213E83F0E4528B3` PRIMARY KEY (`id`));

CREATE TABLE `info_taxpayer` (`id` bigint unsigned NOT NULL AUTO_INCREMENT, `taxc` NVARCHAR(14) NOT NULL, `namecom` NVARCHAR(255) NULL, `type` NVARCHAR(10) NULL, `status` numeric(18) NULL, `effectdate` datetime(3) NULL, `managercomc` NVARCHAR(10) NULL, `chapter` NVARCHAR(10) NULL, `changedate` datetime(3) NULL, `economictype` NVARCHAR(10) NULL, `contbusires` NVARCHAR(10) NULL, `passport` NVARCHAR(20) NULL, `createdby` NVARCHAR(150) NOT NULL, `statusscan` numeric(18) NOT NULL, `messres` NVARCHAR(255) NOT NULL, `statuscoderes` numeric(18) NOT NULL, `createddate` datetime(3) NULL, `updateddate` datetime NULL,primary key (id)) 

CREATE TABLE `type_invoice` (`id` bigint AUTO_INCREMENT NOT NULL, `name` NVARCHAR(255) NULL, primary key (id));

CREATE TABLE `s_statement` (`id` BIGINT AUTO_INCREMENT NOT NULL, `type` NVARCHAR(15) NULL, `formname` NVARCHAR(100) NULL, `regtype` INT NULL, `sname` NVARCHAR(400) NULL, `stax` NVARCHAR(14) NULL, `taxoname` NVARCHAR(100) NULL, `taxo` NVARCHAR(5) NULL, `contactname` NVARCHAR(50) NULL, `contactaddr` NVARCHAR(400) NULL, `contactemail` NVARCHAR(50) NULL, `contactphone` NVARCHAR(20) NULL, `place` NVARCHAR(50) NULL, `createdt` datetime(3) NULL, `hascode` INT DEFAULT 0 NULL, `sendtype` INT NULL, `dtsendtype` NVARCHAR(20) NULL, `invtype` NVARCHAR(100) NULL, `doc` longtext COLLATE utf8_bin, `status` INT DEFAULT 1 NOT NULL, `cqtstatus` INT NULL, `vdt` datetime(3) NULL, `cqtmess` NVARCHAR(255) NULL, `xml` longtext COLLATE utf8_bin, `statustvan` INT DEFAULT 0 NOT NULL, `xml_received` longtext COLLATE utf8_bin, `xml_accepted` longtext COLLATE utf8_bin, `paxoname` NVARCHAR(100) NULL, `paxo` NVARCHAR(5) NULL, `json_received` longtext COLLATE utf8_bin, `json_accepted` longtext COLLATE utf8_bin, `uc` NVARCHAR(20) NULL, CONSTRAINT `s_statement_pk` PRIMARY KEY (`id`));

CREATE TABLE `temp_taxpayer` (`id` int AUTO_INCREMENT NOT NULL, `taxc` NVARCHAR(14) NOT NULL, `createdby` NVARCHAR(150) NOT NULL, `statusscan` numeric(18) NOT NULL, `creaateddate` datetime(3) NULL, `updateddate` datetime(3) NULL,primary key (id))

CREATE TABLE `s_inv_sum` (`idt` date NOT NULL, `stax` VARCHAR(20) NOT NULL, `type` VARCHAR(6) NOT NULL, `form` VARCHAR(11) NOT NULL, `serial` VARCHAR(8) NULL, `i0` numeric(18) NULL, `i1` numeric(18) NULL, `i2` numeric(18) NULL, `i3` numeric(18) NULL, `i4` numeric(18) NULL);

CREATE TABLE `b_inv` (`id` INT AUTO_INCREMENT NOT NULL, `doc` longtext NOT NULL, `xml` longtext NULL, `idt` datetime(3) NULL, `type` VARCHAR(10) NULL, `form` VARCHAR(11) NULL, `serial` VARCHAR(8) NULL, `seq` VARCHAR(7) NULL, `paym` VARCHAR(100) NULL, `curr` VARCHAR(3) NULL, `exrt` numeric(8, 2) NULL, `sname` VARCHAR(255) NULL, `stax` VARCHAR(14) NULL, `saddr` VARCHAR(255) NULL, `smail` VARCHAR(255) NULL, `stel` VARCHAR(255) NULL, `sacc` VARCHAR(255) NULL, `sbank` VARCHAR(255) NULL, `buyer` VARCHAR(255) NULL, `bname` VARCHAR(255) NULL, `bcode` VARCHAR(100) NULL, `btax` VARCHAR(14) NULL, `baddr` VARCHAR(255) NULL, `bmail` VARCHAR(255) NULL, `btel` VARCHAR(255) NULL, `bacc` VARCHAR(255) NULL, `bbank` VARCHAR(255) NULL, `note` VARCHAR(255) NULL, `sumv` numeric(19, 4) NULL, `vatv` numeric(19, 4) NULL, `totalv` numeric(19, 4) NULL, `ou` INT NULL, `uc` VARCHAR(20) NULL, `paid` INT NULL, `month` VARCHAR(100) NULL, `year` VARCHAR(100) NULL, `category` VARCHAR(100) NULL, CONSTRAINT `PK__b_inv__3213E83FFD9C2485` PRIMARY KEY (`id`));

CREATE TABLE `s_user_pass` (`user_id` NVARCHAR(20) NOT NULL, `pass` VARCHAR(200) NOT NULL, `change_date` datetime(3) NOT NULL, CONSTRAINT `PK__s_user_p__518C386C17C39B87` PRIMARY KEY (`user_id`, `pass`));

CREATE TABLE `van_msg_out_dtl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `iddatatvan` decimal(18,0) NOT NULL,
  `invid` varchar(36) NOT NULL,
  `createdby` varchar(150) NOT NULL,
  `statustvan` decimal(18,0) DEFAULT NULL,
  `statusres` decimal(18,0) DEFAULT NULL,
  `messsendcode` varchar(10) DEFAULT NULL,
  `messsend` longtext,
  `messres` varchar(255) DEFAULT NULL,
  `messrescode` varchar(10) DEFAULT NULL,
  `senddate` datetime(3) DEFAULT NULL,
  `resdate` datetime(3) DEFAULT NULL,
  `transid` varchar(225) DEFAULT NULL,
  `idtypeinv` decimal(18,0) NOT NULL,
  `status_response` int(11) DEFAULT '0',
  `datecheckmsg` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `van_msg_out_dtl_transid_IDX` (`transid`) USING BTREE,
  KEY `van_msg_out_dtl_status_response_IDX` (`status_response`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=684 DEFAULT CHARSET=utf8;

CREATE TABLE `van_msg_out` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `invid` varchar(36) NOT NULL,
  `xml2base64` longtext NOT NULL,
  `createdby` varchar(150) NOT NULL,
  `statustvan` decimal(18,0) DEFAULT NULL,
  `createddate` datetime(3) DEFAULT NULL,
  `senddate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idtypeinv` decimal(18,0) NOT NULL,
  `messsendcode` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `van_msg_out_createddate_IDX` (`createddate`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=686 DEFAULT CHARSET=utf8;




drop table van_history;
CREATE TABLE `van_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type_invoice` decimal(18,0) NOT NULL,
  `id_ref` decimal(18,0) DEFAULT NULL,
  `status` decimal(18,0) DEFAULT NULL,
  `datetime_trans` datetime(3) DEFAULT NULL,
  `description` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  PRIMARY KEY (`id`),
  KEY `van_history_id_ref_IDX` (`id_ref`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


CREATE TABLE `s_wrongnotice_process` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ou` int(11) DEFAULT NULL,
  `id_wn` int(11) DEFAULT NULL,
  `id_inv` longtext,
  `doc` longtext,
  `xml` longtext,
  `dt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(15) NOT NULL,
  `status_cqt` int(11) DEFAULT NULL,
  `xml_received` longtext,
  `json_received` longtext,
  `xml_accepted` longtext,
  `json_accepted` longtext,
  `dien_giai` longtext,
  `taxc` varchar(14) DEFAULT NULL,
  `form` varchar(14) DEFAULT NULL,
  `serial` varchar(14) DEFAULT NULL,
  `seq` varchar(14) DEFAULT NULL,
  `type` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UQ__s_wrongn__01487863BB3969F7` (`id_wn`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

CREATE TABLE `van_msg_in` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `receive_arr_json` longtext NOT NULL,
  `status_scan` decimal(18,0) DEFAULT NULL COMMENT '0: not scan, pending: 3, success: 1, error: 2',
  `createddate` datetime(3) DEFAULT NULL,
  `updateddate` datetime(3) DEFAULT NULL,
  `createby` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `van_msg_in_createddate_IDX` (`createddate`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2686 DEFAULT CHARSET=utf8;

CREATE TABLE `s_list_inv` (
  `id` bigint(20) NOT NULL,
  `listinv_code` varchar(15) DEFAULT NULL,
  `s_tax` varchar(14) NOT NULL,
  `ou` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `approve_date` datetime(3) DEFAULT NULL,
  `approve_by` varchar(20) DEFAULT NULL,
  `s_name` varchar(400) NOT NULL,
  `item_type` varchar(20) NOT NULL,
  `listinvoice_num` varchar(5) DEFAULT NULL,
  `period_type` varchar(1) NOT NULL,
  `period` varchar(10) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `doc` longtext,
  `xml` longtext,
  `times` int(11) NOT NULL DEFAULT '0',
  `status_received` int(11) DEFAULT NULL,
  `xml_received` longtext,
  `json_received` longtext,
  `edittimes` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `s_list_inv_s_tax_IDX` (`s_tax`,`created_date`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE INDEX `s_list_inv_cancel_cdt_ou` ON `s_list_inv_cancel`(`cdt`, `ou`);

-- CREATE UNIQUE INDEX `s_user_code_idx` ON `s_user`(code IS NOT NULL);

-- ALTER TABLE `s_inv` ADD CONSTRAINT `FK__s_inv__uc__47A6A41B` FOREIGN KEY (`uc`) REFERENCES `s_user` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;



CREATE INDEX `idx_invID` ON `s_trans`(`inv_id`);

CREATE UNIQUE INDEX `idx_tranID` ON `s_trans`(`tranID`);

CREATE UNIQUE INDEX `S_ORG_UID_01` ON `s_org`(`vtaxc`);


ALTER TABLE `s_vendor` ADD CONSTRAINT `UQ__s_vendor__24D3AF4B470912EE` UNIQUE (`taxno`);


ALTER TABLE s_inv ADD CONSTRAINT FK__s_inv__ou__46B27FE2 FOREIGN KEY(ou) REFERENCES s_ou(id);



ALTER TABLE `s_serial` ADD `ut` datetime  DEFAULT CURRENT_TIMESTAMP not null;



ALTER TABLE `s_serial` ADD `alertleft` INT NOT NULL DEFAULT 0;

ALTER TABLE `s_serial` ADD `intg` INT NOT NULL DEFAULT 2;





ALTER TABLE `s_serial` ADD `leftwarn` INT NULL DEFAULT 1;



ALTER TABLE `s_serial` ADD `sendtype` INT NULL;

ALTER TABLE `s_serial` ADD `invtype` INT NULL;

ALTER TABLE `s_serial` ADD `degree_config` INT NULL DEFAULT 119 ;

ALTER TABLE `s_org` ADD `vtaxc` NVARCHAR(14) NULL;

ALTER TABLE `s_inv` ADD `sendmaildate`  datetime GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.SendMailDate'))) VIRTUAL;

ALTER TABLE `s_inv` ADD `sendsmsdate` datetime  GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.SendSMSlDate'))) VIRTUAL;
-- alter bo tu tang id
ALTER TABLE s_inv
  MODIFY id int(10) unsigned;
ALTER TABLE `s_inv` DROP COLUMN `vseq`;
ALTER TABLE `s_inv` ADD `vseq` NVARCHAR(18) GENERATED ALWAYS AS (if(IFNULL(json_unquote(json_extract(`doc`,'$.seq')), '') > '',json_unquote(json_extract(`doc`,'$.seq')),id)) VIRTUAL;

ALTER TABLE `s_inv` ADD `sendmailstatus` INT GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.SendMailStatus')) = 0),NULL,json_unquote(json_extract(`doc`,'$.SendMailStatus')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `sendsmsstatus` INT GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.SendSMSStatus')) = 0),NULL,json_unquote(json_extract(`doc`,'$.SendSMSStatus')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `sendmailnum` INT GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.SendMailNum')) = 0),NULL,json_unquote(json_extract(`doc`,'$.SendMailNum')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `sendsmsnum` INT GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.SendSMSNum')) = 0),NULL,json_unquote(json_extract(`doc`,'$.SendSMSNum')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `taxocode` NVARCHAR(20) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.taxocode')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.taxocode')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `invlistdt` datetime  GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.invlistdt')) = ''),NULL,json_unquote(json_extract(`doc`,'$.invlistdt')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `discountamt` numeric(18) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.discountamt')) = 	null),0,json_unquote(json_extract(`doc`,'$.discountamt')))) VIRTUAL;


ALTER TABLE `s_inv` ADD `vehic` NVARCHAR(500)  GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.vehic')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.vehic')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `ordno` NVARCHAR(500) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.ordno')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.ordno')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `whsfr` NVARCHAR(500)  GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.whsfr')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.whsfr')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `whsto` NVARCHAR(500) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.whsto')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.whsto')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `edt` datetime ;

ALTER TABLE `s_inv` ADD `status_received` INT GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.status_received')) = 'null'),0,json_unquote(json_extract(`doc`,'$.status_received')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `xml_received` longtext COLLATE utf8_bin;

ALTER TABLE `s_inv` ADD `hascode` numeric(38) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.hascode')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.hascode')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `invtype` NVARCHAR(500) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.invtype')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.invtype')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `invlistnum` NVARCHAR(50) NULL;

ALTER TABLE `s_inv` ADD `period_type` NVARCHAR(1) NULL;

ALTER TABLE 's_inv' ADD 'period' NVARCHAR(10) NULL;


ALTER TABLE `s_inv` ADD `list_invid` BIGINT NULL;

ALTER TABLE `s_inv` ADD `status_tbss` INT NULL;

ALTER TABLE `s_inv` ADD `orddt` datetime  GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.orddt')) = ''),NULL,json_unquote(json_extract(`doc`,'$.orddt')))) VIRTUAL;

ALTER TABLE `s_inv` ADD `error_msg_van` longtext COLLATE utf8_bin NULL;

ALTER TABLE `s_group` ALTER `APPROVE` SET DEFAULT '1';

ALTER TABLE `s_group` ALTER `DES` SET DEFAULT '1';

ALTER TABLE `s_group_user` MODIFY `USER_ID` VARCHAR(20);

ALTER TABLE `s_refcode` MODIFY `abbreviation` longtext COLLATE utf8_bin;

ALTER TABLE `s_role` MODIFY `active` TINYINT(3);

ALTER TABLE `s_role` ALTER `active` DROP DEFAULT;

ALTER TABLE `b_inv` ADD CONSTRAINT `UQ__b_inv__B09EB31C78CA9B41` UNIQUE (`stax`, `form`, `serial`, `seq`);



ALTER TABLE `s_role` ADD CONSTRAINT `s_role_fk01` FOREIGN KEY (`pid`) REFERENCES `s_role` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE `s_user_pass` ADD CONSTRAINT `FK__s_user_pa__user___49CEE3AF` FOREIGN KEY (`user_id`) REFERENCES `s_user` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;

ALTER TABLE `s_group_role` ADD `STATUS` VARCHAR(1) NULL;

ALTER TABLE `s_role` ADD `pid` INT NULL;

ALTER TABLE `s_role` ADD `sort` NVARCHAR(10) NULL;

ALTER TABLE `s_role` ADD `active` TINYINT(3) NULL;

ALTER TABLE `s_role` ADD `menu_detail` NVARCHAR(150) NULL;

ALTER TABLE `s_user` ADD `local` INT DEFAULT 0 NOT NULL;

ALTER TABLE `s_user` ADD `change_pass_date` datetime null DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `s_user` ADD `login_number` INT DEFAULT 0 NOT NULL;

ALTER TABLE `s_user` ADD `change_pass` INT DEFAULT 0 NOT NULL;

ALTER TABLE `s_user` ADD `update_date` datetime(3) NULL;

ALTER TABLE `s_user` ADD `local` INT DEFAULT 0 NOT NULL;

ALTER TABLE `s_user` ADD `change_pass_date` datetime null DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `s_report_tmp` MODIFY `c_id` BIGINT;

ALTER TABLE `s_report_tmp` MODIFY `cancel` INT;

ALTER TABLE `s_user_pass` MODIFY `change_date` datetime NOT NULL;

ALTER TABLE `s_report_tmp` MODIFY `clist` longtext COLLATE utf8_bin;

ALTER TABLE `s_gns` MODIFY `code` NVARCHAR(30);

ALTER TABLE `s_role` MODIFY `code` NVARCHAR(100);

ALTER TABLE `s_cat` MODIFY `des` NVARCHAR(100);



ALTER TABLE `s_refcode` MODIFY `domain` longtext COLLATE utf8_bin;

ALTER TABLE `s_f_fld` MODIFY `fld_typ` NVARCHAR(50) NOT NULL;

ALTER TABLE `s_inv_sum` MODIFY `form` VARCHAR(11);

ALTER TABLE `s_report_tmp` MODIFY `form` NVARCHAR(50);

ALTER TABLE `s_refcode` MODIFY `highval` longtext COLLATE utf8_bin;

ALTER TABLE `s_inv_sum` MODIFY `i0` numeric(18);

ALTER TABLE `s_inv_sum` MODIFY `i1` numeric(18);

ALTER TABLE `s_inv_sum` MODIFY `i2` numeric(18);

ALTER TABLE `s_inv_sum` MODIFY `i3` numeric(18);

ALTER TABLE `s_inv_sum` MODIFY `i4` numeric(18);



ALTER TABLE `s_dcd` MODIFY `id` NVARCHAR(30);









ALTER TABLE `s_inv_sum` MODIFY `idt` date;

ALTER TABLE `s_xls` MODIFY `itype` NVARCHAR(6);

ALTER TABLE `s_xls` MODIFY `jc` NVARCHAR(20);

ALTER TABLE `s_refcode` MODIFY `lowval` longtext COLLATE utf8_bin;



ALTER TABLE `s_refcode` MODIFY `meaning` longtext COLLATE utf8_bin;

ALTER TABLE `s_role` MODIFY `menu_detail` NVARCHAR(150);

ALTER TABLE `s_report_tmp` MODIFY `min0` INT;

ALTER TABLE `s_report_tmp` MODIFY `min1` INT;

ALTER TABLE `s_report_tmp` MODIFY `minc` INT;

ALTER TABLE `s_report_tmp` MODIFY `minu` INT;

ALTER TABLE `s_dcd` MODIFY `name` NVARCHAR(100);

ALTER TABLE `s_gns` MODIFY `name` NVARCHAR(255);

ALTER TABLE `s_loc` MODIFY `name` NVARCHAR(100);

ALTER TABLE `s_role` MODIFY `name` NVARCHAR(100);

ALTER TABLE `s_taxo` MODIFY `name` NVARCHAR(100);

ALTER TABLE `s_role` MODIFY `pid` INT;

ALTER TABLE `s_gns` MODIFY `price` numeric(17, 2);

ALTER TABLE `s_inv_sum` MODIFY `serial` VARCHAR(8);

ALTER TABLE `s_report_tmp` MODIFY `serial` NVARCHAR(50);

ALTER TABLE `s_role` MODIFY `sort` NVARCHAR(10);

ALTER TABLE `s_inv_sum` MODIFY `stax` VARCHAR(20);

ALTER TABLE `s_cat` MODIFY `type` NVARCHAR(30);

ALTER TABLE `s_dcd` MODIFY `type` NVARCHAR(30);

ALTER TABLE `s_inv_sum` MODIFY `type` VARCHAR(6);

ALTER TABLE `s_gns` MODIFY `unit` NVARCHAR(30);

ALTER TABLE `s_xls` MODIFY `xc` NVARCHAR(2);



CREATE TABLE `s_refcode` (
  `domain` varchar(60) NOT NULL,
  `abbreviation` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `meaning` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `lowval` longtext NOT NULL,
  `highval` longtext CHARACTER SET utf8 COLLATE utf8_bin
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Bảng lưu tập hợp các giá trị tham chiếu của các trường chức năng, cột dữ liệu';

ALTER TABLE `s_sys_logs` ADD `ip` VARCHAR(50) NULL;
-- xoa loai hoa don
delete from s_cat where `type`='ITYPE';

ALTER TABLE `s_sys_logs` COMMENT = 'Bảng luu log audit thao tác của NSD APP, API';



CREATE TABLE `van_msg_in_dtl` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `receivejson` json NOT NULL,
  `taxc` varchar(14) DEFAULT NULL,
  `namenotice` varchar(500) CHARACTER SET utf8mb4 GENERATED ALWAYS AS (if((json_unquote(json_extract(`receivejson`,'$.tenTBao')) = 'null'),NULL,json_unquote(json_extract(`receivejson`,'$.tenTBao')))) VIRTUAL,
  `typenotice` int CHARACTER SET utf8mb4 GENERATED ALWAYS AS (if((json_unquote(json_extract(`receivejson`,'$.loaiTBao')) = 'null'),NULL,json_unquote(json_extract(`receivejson`,'$.loaiTBao')))) VIRTUAL,
  `contentnotice` longtext CHARACTER SET utf8mb4 GENERATED ALWAYS AS (if((json_unquote(json_extract(`receivejson`,'$.ndungTBao')) = 'null'),NULL,json_unquote(json_extract(`receivejson`,'$.ndungTBao')))) VIRTUAL,
  `createddtnotice` datetime GENERATED ALWAYS AS (if((json_unquote(json_extract(`receivejson`,'$.ngayTaoTBao')) = 'null'),NULL,json_unquote(json_extract(`receivejson`,'$.ngayTaoTBao')))) VIRTUAL,
  `createddate` datetime DEFAULT CURRENT_TIMESTAMP,
  `status_scan` decimal(18,0) DEFAULT NULL,
  `updateddate` datetime(3) DEFAULT NULL,
  `transid` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `van_msg_in_dtl_status_scan_IDX` (`status_scan`) USING BTREE,
  KEY `van_msg_in_dtl_transid_IDX` (`transid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=16071 DEFAULT CHARSET=utf8;




CREATE TABLE `s_tmp` (`ic` VARCHAR(36) NULL, `idt` datetime(3) NULL, `form` VARCHAR(11) NULL, `serial` VARCHAR(6) NULL, `seq` VARCHAR(7) NULL);

CREATE TABLE `s_imp` ( `idt` datetime(3) NULL, `form` VARCHAR(11) NULL, `serial` VARCHAR(6) NULL);


// bang dung cho job
CREATE TABLE `s_listvalues` (
  `keyname` varchar(100) NOT NULL,
  `keyvalue` varchar(100) NOT NULL,
  `keytype` varchar(100) NOT NULL,
  UNIQUE KEY `s_listvalues_uk` (`keyname`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

// bang luu id gui tvan
CREATE TABLE `van_einv_listid_msg` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `status_scan` int(11) DEFAULT NULL,
  `createdate` datetime DEFAULT CURRENT_TIMESTAMP,
  `sendvandate` datetime DEFAULT NULL,
  `msg` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  `xml` longtext CHARACTER SET utf8 COLLATE utf8_bin,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `van_einv_listid_msg_status_scan_IDX` (`status_scan`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=537 DEFAULT CHARSET=utf8;


delete from s_group_role;

insert into s_group_role(group_id,role_id,status) 
select 1,id,null from s_role;
commit;



CREATE UNIQUE INDEX s_inv_stax_IDX USING BTREE ON s_inv (stax,form,serial,vseq);

-- Thêm inv:r7 xử lý form chờ hủy hủy // riêng lge
insert into s_f_fld(fnc_name,fnc_url, fld_name, fld_id, fld_typ, atr, feature, status) values ("Huy hoa don", "inv.inv","Form row 7", "inv:r7", "affectMany", '{"disabled":true}','{"id":"*","action":"cancel"}',1)

-- thêm cột inv_adj xử lý cho BTH
ALTER TABLE s_inv
ADD inv_adj bigint;

ALTER TABLE s_ou ADD (
  `sct` tinyint(3) NOT NULL DEFAULT '0',
  `sc` tinyint(3) NOT NULL DEFAULT '0',
  `bcc` varchar(100) DEFAULT NULL,
  `qrc` tinyint(3) NOT NULL DEFAULT '0',
  `na` tinyint(3) NOT NULL DEFAULT '2',
  `nq` tinyint(3) NOT NULL DEFAULT '2',
  `np` tinyint(3) NOT NULL DEFAULT '5',
  `itype` varchar(255) NOT NULL DEFAULT '01GTKT',
  `ts` tinyint(3) NOT NULL DEFAULT '1',
  `degree_config` tinyint(3) NOT NULL DEFAULT '119',
  `receivermail` varchar(200) DEFAULT NULL,
  `dsignfrom` datetime DEFAULT NULL,
  `dsignto` datetime DEFAULT NULL,
  `dsignserial` varchar(100) DEFAULT NULL,
  `dsignsubject` varchar(200) DEFAULT NULL,
  `dsignissuer` varchar(200) DEFAULT NULL);
  


ALTER TABLE s_ou ADD place nvarchar(255);
ALTER TABLE s_inv ADD th_type int
ALTER TABLE s_inv_adj ADD th_type int NOT NULL
ALTER TABLE s_inv ADD wno_adj int
Alter table s_inv drop column wnadjtype
ALTER TABLE s_inv ADD  `wnadjtype` int GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.wnadjtype')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.wnadjtype')))) VIRTUAL

create table s_list_inv_id (id bigint, inv_id bigint, loai VARCHAR(50) )
create index s_list_inv_id_idx on s_list_inv_id (id) 
alter table s_inv add list_invid_bx bigint

ALTER TABLE `s_ou` ADD `c0` VARCHAR(255) NULL;

ALTER TABLE `s_ou` ADD `c1` VARCHAR(255) NULL;

ALTER TABLE `s_ou` ADD `c2` VARCHAR(255) NULL;

ALTER TABLE `s_ou` ADD `c3` VARCHAR(255) NULL;

ALTER TABLE `s_ou` ADD `c4` VARCHAR(255) NULL;

ALTER TABLE `s_ou` ADD `c5` VARCHAR(255) NULL;

ALTER TABLE `s_ou` ADD `c6` VARCHAR(255) NULL;

ALTER TABLE `s_ou` ADD `c7` VARCHAR(255) NULL;

ALTER TABLE `s_ou` ADD `c8` VARCHAR(255) NULL;

ALTER TABLE `s_ou` ADD `c9` VARCHAR(255) NULL;

ALTER TABLE `s_ou` ADD `smtpconf` longtext NULL;

ALTER TABLE `s_ou` ADD `filenameca` VARCHAR(255) NULL;

ALTER TABLE `s_ou` ADD `cfg_ihasdocid` VARCHAR(45) NULL;

ALTER TABLE `s_ou` ADD `cfg_uselist` VARCHAR(45) DEFAULT '' NOT NULL;

ALTER TABLE `s_ou` ADD `hascode` TINYINT(3) DEFAULT 0 NOT NULL;

ALTER TABLE `s_ou` ADD `adsconf` longtext NULL;

-- update s_role set active =1 where name like '%gán quyền%'
update s_role set active =0 where name like '%_BTH%'
update s_role set active =0 where code like 'PERM_SEARCH_BINV%'
update s_role set active =0 where name like 'PERM_TRAN%'

UPDATE s_inv set status_tbss = 0 WHERE status_tbss is null
update s_user add last_login datetime(3)
