/* Mail */
ALTER TABLE dbo.s_inv DROP COLUMN sendmailnum;
ALTER TABLE dbo.s_inv DROP COLUMN sendmailstatus;
ALTER TABLE dbo.s_inv DROP COLUMN sendmaildate;

ALTER TABLE [dbo].[s_inv] ADD [sendmailnum] AS (CASE WHEN CONVERT([tinyint],json_value([doc],'$.SendMailNum')) IS NULL THEN 0 ELSE CONVERT([tinyint],json_value([doc],'$.SendMailNum')) END);
ALTER TABLE [dbo].[s_inv] ADD [sendmailstatus] AS (CASE WHEN CONVERT([int],json_value([doc],'$.SendMailStatus')) IS NULL THEN 0 ELSE CONVERT([int],json_value([doc],'$.SendMailStatus')) END);
ALTER TABLE [dbo].[s_inv] ADD [sendmaildate] AS (CONVERT([datetime],json_value([doc],'$.SendMailDate')));

/* SMS */
ALTER TABLE dbo.s_inv DROP COLUMN sendsmsnum;
ALTER TABLE dbo.s_inv DROP COLUMN sendsmsstatus;
ALTER TABLE dbo.s_inv DROP COLUMN sendsmsdate;

ALTER TABLE [dbo].[s_inv] ADD [sendsmsnum] AS (CASE WHEN CONVERT([tinyint],json_value([doc],'$.SendSMSNum')) IS NULL THEN 0 ELSE CONVERT([tinyint],json_value([doc],'$.SendSMSNum')) END);
ALTER TABLE [dbo].[s_inv] ADD [sendsmsstatus] AS (CASE WHEN CONVERT([int],json_value([doc],'$.SendSMSStatus')) IS NULL THEN 0 ELSE CONVERT([int],json_value([doc],'$.SendSMSStatus')) END);
ALTER TABLE [dbo].[s_inv] ADD [sendsmsdate] AS (CONVERT([datetime],json_value([doc],'$.SendSMSlDate')));