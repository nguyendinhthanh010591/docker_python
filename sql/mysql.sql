create schema if not exists einvoice default character set utf8mb4;
use einvoice;
--------------------------------------------------------------------
delimiter $$
create  function cap_first(input varchar(255)) returns varchar(255) charset utf8mb4  deterministic
begin
	declare len int;
	declare i int;
	set len   = char_length(input);
	set input = lower(input);
	set i = 0;
	while (i < len) do
		if (mid(input,i,1) = ' ' or i = 0) then
			if (i < len) then
				set input = concat(left(input,i),upper(mid(input,i + 1,1)),right(input,len - i - 1));
			end if;
		end if;
		set i = i + 1;
	end while;
	return input;
end$$
delimiter ;
----------------------------------------------------------------------------------------------
create table if not exists s_taxo (
  id varchar(5)  not null,
  name varchar(100)  not null,
  primary key (id)
);
--insert into s_taxo  select * from invoice.s_taxo order by id
----------------------------------------------------------------------------------------------
create table if not exists s_loc (
  id varchar(7) 	not null,
  name varchar(100) not null,
  pid varchar(7),
  primary key (id),
  key(pid),
  foreign key (pid) references s_loc (id)
);
-------------------------------------------------------------------------------------------------
--insert into s_loc select * from invoice.s_loc order by id;
create table s_org (
  id int unsigned not null auto_increment,
  name 	varchar(255) not null,
  name_en varchar(255),
  taxc varchar(14),
  prov varchar(3),
  dist varchar(5),
  ward varchar(7),
  addr varchar(255),
  fadd varchar(255),
  tel varchar(255),
  mail varchar(255),
  acc varchar(255),
  bank varchar(255),
  status varchar(2) not null default '00',
  c0 varchar(255),
  c1 varchar(255),
  c2 varchar(255),
  c3 varchar(255),
  c4 varchar(255),
  c5 varchar(255),
  c6 varchar(255),
  c7 varchar(255),
  c8 varchar(255),
  c9 varchar(255),
  code  varchar(50),
  pwd   varchar(60),
  fn varchar(512) generated always as (coalesce(nullif(taxc,''),coalesce(nullif(code,''),concat_ws('-',name,tel,mail,fadd)))) stored,
  primary key (id),
  unique (taxc),
  unique (code),
  unique (fn),
  key (name),
  key (fadd)
); 
ALTER TABLE s_org ADD FULLTEXT (taxc, code, name, name_en, fadd, tel, mail, acc, bank, c0, c1, c2, c3, c4, c5, c6);
--select * from s_org WHERE MATCH(taxc, code, name, name_en, fadd, tel, mail, acc, bank, c0, c1, c2, c3, c4, c5, c6) AGAINST ('FPT Cầu Giấy' IN NATURAL LANGUAGE MODE)
/*
ALTER TABLE `einvoice`.`s_org` 
CHANGE COLUMN `fn` `fn` VARCHAR(512) GENERATED ALWAYS AS (coalesce(nullif(`taxc`,''),coalesce(nullif(`code`,''),concat_ws('-',`name`,`tel`,`mail`,`fadd`)))) STORED ;
*/

delimiter $$ 
create trigger s_org_bi
before insert on s_org
for each row
begin
  set new.fadd = get_fadd(new.addr,new.prov,new.dist,new.ward);
end$$
create trigger s_org_bu
before update on s_org
for each row
begin
  set new.fadd = get_fadd(new.addr,new.prov,new.dist,new.ward);
end$$
delimiter ;

--insert into s_org (name,taxc,prov,dist,ward,addr,status) select name,taxc,prov,dist,ward,addr,status from invoice.s_org where mst='0' and taxc is not null order by taxc
--------------------------------------------------------------------------------------------------------------------
create table if not exists s_ou(
  id int unsigned not null auto_increment,
  pid int unsigned,
  code varchar(20),
  name varchar(255) not null,
  taxc varchar(14),
  mst varchar(14) not null,
  paxo varchar(5),
  taxo varchar(5),
  prov varchar(3),
  dist varchar(5),
  ward varchar(7),
  addr varchar(255),
  fadd varchar(255), 
  tel varchar(255),
  mail varchar(255),
  acc varchar(255),
  bank varchar(255),
  status tinyint not null default 1,
  sign tinyint not null default 1,
  seq tinyint not null default 1,
  usr varchar(255),
  pwd varchar(255),
  temp tinyint not null default 1,
  primary key (id),
  unique(taxc),
  unique(code),
  index (pid),
  foreign key (pid) references s_ou(id)
);
--------------------------------------------------------------------------------------------------------------------
create table if not exists s_ou_tree(
  pid int unsigned not null,
  cid int unsigned not null,
  depth int unsigned not null default 0,
  path text not null,
  primary key (pid, cid),
  index (cid, depth),
  index (pid),
  index (cid),
  foreign key (pid) references s_ou(id) on delete cascade,
  foreign key (cid) references s_ou(id) on delete cascade
);
--------------------------------------------------------------------------------------------------------------------
DROP TRIGGER IF EXISTS s_ou_bi;
DROP TRIGGER IF EXISTS s_ou_bu;
DROP TRIGGER IF EXISTS s_ou_ai;
DROP TRIGGER IF EXISTS s_ou_au;
delimiter $$
create trigger  s_ou_bi
before insert on s_ou
for each row
begin 
 set new.fadd = get_fadd(new.addr,new.prov,new.dist,new.ward);
end$$

create trigger  s_ou_bu
before update on s_ou
for each row
begin 
 set new.fadd = get_fadd(new.addr,new.prov,new.dist,new.ward);
end$$

create trigger  s_ou_ai
after insert on s_ou
for each row
begin 
 insert into s_ou_tree (pid, cid, depth, path) values (new.id, new.id, 0, concat(new.id,'/')); 
 insert into s_ou_tree (pid, cid, depth, path) select pid, new.id, depth + 1, concat(path, new.id,'/') from s_ou_tree where cid = new.pid; 
end$$
create trigger   s_ou_au
after update on s_ou
for each row
begin
    if new.pid != old.pid or new.pid is null or old.pid is null then  
		if old.pid is not null then        
			delete t2 from s_ou_tree t1 join s_ou_tree t2 on t1.cid=t2.cid where t1.pid=old.id and t2.depth>t1.depth;  
		end if;        
		if new.pid is not null then 
			insert into s_ou_tree (pid, cid, depth, path) select t1.pid, t2.cid, t1.depth + t2.depth + 1, concat(t1.path, t2.path) from s_ou_tree t1, s_ou_tree t2 where t1.cid = new.pid and t2.pid = old.id;                            
		end if;                        
	end if;      
end$$
delimiter ;
-------------------------------------------------------------
create table if not exists s_user(
  id varchar(20) not null,
  code varchar(20),
  mail varchar(50),
  ou int unsigned not null,
  uc tinyint not null default 1,
  name varchar(50) not null,
  pos varchar(50),
  primary key (id),
  index (ou),
  unique (code),
  foreign key (ou) references s_ou(id)
);
------------------------------------------------------------------------------------------
create table if not exists s_role(
  id int unsigned not null auto_increment,
  name varchar(100) not null,
  primary key (id)
);
----------------------------------------------------------------------------------------------
create table if not exists s_member(
  user_id varchar(20) not null,
  role_id int unsigned not null,
  primary key (user_id, role_id),
  foreign key (user_id) references s_user(id),
  foreign key (role_id) references s_role(id)
);
----------------------------------------------------------------------------------------------
create table if not exists s_manager (
  user_id varchar(20) not null,
  taxc 	  varchar(14) not null,
  primary key (user_id, taxc),
  foreign key (user_id) references s_user (id),
  foreign key (taxc) references s_ou (taxc)
);
----------------------------------------------------------------------------------------------
create table if not exists s_ex(
  id int unsigned not null auto_increment,
  dt  datetime not null default current_timestamp,
  cur varchar(3) not null,
  val numeric(8, 2) not null,
  primary key (id),
  unique (dt, cur),
  check (val > 0)
);
---------------------------------------------------------------------------------------------------------------------
create table if not exists s_serial(
  id int unsigned not null auto_increment,
  taxc varchar(14) not null,
  type varchar(6) not null,
  form varchar(11) not null,
  serial varchar(6) not null,
  min int unsigned not null,
  max int unsigned not null,
  cur int unsigned not null,
  status tinyint not null default 3,
  fd datetime not null,
  td datetime,
  dt datetime not null default current_timestamp,
  uc varchar(20) not null,
  uses tinyint not null default 1,
  primary key (id),
  unique (taxc, form, serial),
  check (
    min > 0
    (cur>=min or cur=0)
    and max >= cur
  ),
  index (uc),
  foreign key (uc) references s_user (id),
  foreign key (taxc) references s_ou (taxc)
);
--------------------------------------------
create table if not exists s_gns(
  id 	int unsigned not null auto_increment,
  code 	varchar(30)  not null,
  name 	varchar(255) not null,
  unit 	varchar(30)  not null default 'N/A',
  price numeric(17, 2),
  primary key (id),
  unique (code)
);
----------------------------------------------
create table if not exists s_dcd (
  id   varchar(30) not null,
  name varchar(100) not null,
  type varchar(30) not null,
  primary key (type, id, name)
);
----------------------------------------
create table if not exists s_cat (
  id int unsigned not null auto_increment,
  name varchar(100) not null,
  type varchar(30)  not null,
  des  varchar2(255),  
  primary key (id),
  unique (type, name)
);
---------------------------------------------------------------------
create table if not exists s_dcm (
  id 	int unsigned not null auto_increment,
  itype varchar(6) not null,
  idx 	tinyint not null,
  dtl 	tinyint not null default 2,
  lbl 	varchar(100) not null,
  typ 	varchar(20) not null,
  atr 	json,
  status tinyint not null default 2,
  xc 	 varchar(2),
  primary key (id),
  unique(itype, idx, dtl)
);
--------------------------------------------------------------------------------------------------------------------
create table if not exists s_xls (
  id int unsigned  not null auto_increment,
  itype varchar(6) not null,
  jc varchar(20)   not null,
  xc varchar(2),
  primary key (id),
  unique (itype, jc)
);
----------------------------------------------------------------
drop table s_inv;
create table if not exists s_inv (
id  int unsigned not null auto_increment,
sec varchar(10) not null,
ic  varchar(36),
pid int unsigned,
cid int unsigned,
cde varchar(255),
idt date not null,
doc json not null,
xml text,
dt datetime not null default current_timestamp,
ou int unsigned not null,
uc varchar(20)  not null,
cvt tinyint(4) default 0
 `status` tinyint(4) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.status')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.status')))) VIRTUAL,
  `type` varchar(10) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.type')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.type')))) VIRTUAL,
  `form` varchar(11) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.form')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.form')))) VIRTUAL,
  `serial` varchar(6) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.serial')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.serial')))) VIRTUAL,
  `seq` varchar(7) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.seq')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.seq')))) VIRTUAL,
  `paym` varchar(10) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.paym')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.paym')))) VIRTUAL,
  `curr` varchar(3) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.curr')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.curr')))) VIRTUAL,
  `exrt` decimal(8,2) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.exrt')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.exrt')))) VIRTUAL,
  `sname` varchar(255) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.sname')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.sname')))) VIRTUAL,
  `stax` varchar(14) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.stax')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.stax')))) VIRTUAL,
  `saddr` varchar(255) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.saddr')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.saddr')))) VIRTUAL,
  `smail` varchar(255) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.smail')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.smail')))) VIRTUAL,
  `stel` varchar(255) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.stel')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.stel')))) VIRTUAL,
  `sacc` varchar(255) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.sacc')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.sacc')))) VIRTUAL,
  `sbank` varchar(255) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.sbank')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.sbank')))) VIRTUAL,
  `buyer` varchar(100) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.buyer')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.buyer')))) VIRTUAL,
  `bname` varchar(255) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.bname')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.bname')))) VIRTUAL,
  `btax` varchar(14) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.btax')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.btax')))) VIRTUAL,
  `baddr` varchar(255) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.baddr')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.baddr')))) VIRTUAL,
  `bmail` varchar(255) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.bmail')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.bmail')))) VIRTUAL,
  `btel` varchar(255) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.btel')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.btel')))) VIRTUAL,
  `bacc` varchar(255) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.bacc')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.bacc')))) VIRTUAL,
  `bbank` varchar(255) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.bbank')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.bbank')))) VIRTUAL,
  `note` varchar(255) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.note')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.note')))) VIRTUAL,
  `sum` decimal(17,2) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.sum')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.sum')))) VIRTUAL,
  `sumv` decimal(17,2) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.sumv')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.sumv')))) VIRTUAL,
  `vat` decimal(17,2) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.vat')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.vat')))) VIRTUAL,
  `vatv` decimal(17,2) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.vatv')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.vatv')))) VIRTUAL,
  `total` decimal(17,2) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.total')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.total')))) VIRTUAL,
  `totalv` decimal(17,2) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.totalv')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.totalv')))) VIRTUAL,
  `adjseq` varchar(26) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.adj.seq')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.adj.seq')))) VIRTUAL,
  `adjdes` varchar(255) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.adj.des')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.adj.des')))) VIRTUAL,
  `adjtyp` tinyint(4) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.adj.typ')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.adj.typ')))) VIRTUAL,
  `c0` varchar(255) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.c0')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.c0')))) VIRTUAL,
  `c1` varchar(255) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.c1')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.c1')))) VIRTUAL,
  `c2` varchar(255) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.c2')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.c2')))) VIRTUAL,
  `c3` varchar(255) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.c3')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.c3')))) VIRTUAL,
  `c4` varchar(255) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.c4')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.c4')))) VIRTUAL,
  `c5` varchar(255) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.c5')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.c5')))) VIRTUAL,
  `c6` varchar(255) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.c6')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.c6')))) VIRTUAL,
  `c7` varchar(255) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.c7')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.c7')))) VIRTUAL,
  `c8` varchar(255) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.c8')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.c8')))) VIRTUAL,
  `c9` varchar(255) GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.c9')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.c9')))) VIRTUAL,
,primary key(id,idt)
,unique(sec,idt)
,unique(ic,idt)
,index(pid)
) partition by range columns (idt) (
	partition p2000 values less than ('2020-01-01'),
	partition p2001 values less than ('2020-02-01'),
	partition p2002 values less than ('2020-03-01'),
	partition p2003 values less than ('2020-04-01'),
	partition p2004 values less than ('2020-05-01'),
	partition p2005 values less than ('2020-06-01'),
	partition p2006 values less than ('2020-07-01'),
	partition p2007 values less than ('2020-08-01'),
	partition p2008 values less than ('2020-09-01'),
	partition p2009 values less than ('2020-10-01'),
	partition p2010 values less than ('2020-11-01'),
	partition p2011 values less than ('2020-12-01'),
	partition p2100 values less than ('2021-01-01'),
	partition p2101 values less than ('2021-02-01'),
	partition p2102 values less than ('2021-03-01'),
	partition p2103 values less than ('2021-04-01'),
	partition p2104 values less than ('2021-05-01'),
	partition p2105 values less than ('2021-06-01'),
	partition p2106 values less than ('2021-07-01'),
	partition p2107 values less than ('2021-08-01'),
	partition p2108 values less than ('2021-09-01'),
	partition p2109 values less than ('2021-10-01'),
	partition p2110 values less than ('2021-11-01'),
	partition p2111 values less than ('2021-12-01'),
	partition p2200 values less than ('2022-01-01'),
	partition p2201 values less than ('2022-02-01'),
	partition p2202 values less than ('2022-03-01'),
	partition p2203 values less than ('2022-04-01'),
	partition p2204 values less than ('2022-05-01'),
	partition p2205 values less than ('2022-06-01'),
	partition p2206 values less than ('2022-07-01'),
	partition p2207 values less than ('2022-08-01'),
	partition p2208 values less than ('2022-09-01'),
	partition p2209 values less than ('2022-10-01'),
	partition p2210 values less than ('2022-11-01'),
	partition p2211 values less than ('2022-12-01'),
	partition p2300 values less than ('2023-01-01'),
	partition p2301 values less than ('2023-02-01'),
	partition p2302 values less than ('2023-03-01'),
	partition p2303 values less than ('2023-04-01'),
	partition p2304 values less than ('2023-05-01'),
	partition p2305 values less than ('2023-06-01'),
	partition p2306 values less than ('2023-07-01'),
	partition p2307 values less than ('2023-08-01'),
	partition p2308 values less than ('2023-09-01'),
	partition p2309 values less than ('2023-10-01'),
	partition p2310 values less than ('2023-11-01'),
	partition p2311 values less than ('2023-12-01'),
	partition p2400 values less than ('2024-01-01'),
	partition p2401 values less than ('2024-02-01'),
	partition p2402 values less than ('2024-03-01'),
	partition p2403 values less than ('2024-04-01'),
	partition p2404 values less than ('2024-05-01'),
	partition p2405 values less than ('2024-06-01'),
	partition p2406 values less than ('2024-07-01'),
	partition p2407 values less than ('2024-08-01'),
	partition p2408 values less than ('2024-09-01'),
	partition p2409 values less than ('2024-10-01'),
	partition p2410 values less than ('2024-11-01'),
	partition p2411 values less than ('2024-12-01'),
	partition pmax  values less than (maxvalue)
);
----------------------------------------------------------------------
delimiter $$ 
create function get_local_name(pcode varchar(7)) returns varchar(100) charset utf8mb4 deterministic 
begin 
declare pname varchar(100);
if (nullif(pcode, '') is not null) then
	select  name into pname from s_loc where id = pcode;
end if;
return pname;
end $$
delimiter ;
-----------------------------------
delimiter $$ 
create function get_fadd(
  addr varchar(255),
  prov varchar(3),
  dist varchar(5),
  ward varchar(7)
) returns varchar(255) charset utf8mb4 deterministic 
begin 
return concat_ws(', ',nullif(addr, ''),get_local_name(ward),get_local_name(dist), get_local_name(prov));
end $$ 
delimiter ;
-------------------------------------
delimiter $$ 
create trigger s_cust_bi before insert on s_cust 
for each row 
begin
set new.fadd = get_fadd(new.addr, new.prov, new.dist, new.ward);
end $$ 

create trigger s_cust_bu before update on s_cust 
for each row 
begin
set new.fadd = get_fadd(new.addr, new.prov, new.dist, new.ward);
end $$
delimiter ;
-------------------------------------------------
delimiter $$ 
create trigger s_ou_bi before insert on s_ou 
for each row 
begin
set new.fadd = get_fadd(new.addr, new.prov, new.dist, new.ward);
end $$ 

create trigger s_ou_bu before update on s_ou 
for each row 
begin
set new.fadd = get_fadd(new.addr, new.prov, new.dist, new.ward);
end $$
delimiter ;



drop trigger if exists s_ou_ai;
drop trigger if exists s_ou_au;

delimiter $$ 
create trigger s_ou_ai after insert on s_ou 
for each row 
begin
	insert into s_ou_tree (pid, cid, depth, path) values  (new.id, new.id, 0, concat(new.id, '/'));
	insert into s_ou_tree (pid, cid, depth, path) select  pid,new.id,depth + 1,concat(path, new.id, '/') 
from s_ou_tree
where cid = new.pid;
end $$

create trigger s_ou_au after update on s_ou for each row 
begin 
if new.pid != old.pid  or new.pid is null  or old.pid is null then 
if old.pid is not null then 
delete t2 from s_ou_tree t1 join s_ou_tree t2 on t1.cid = t2.cid 
where t1.pid = old.id 
and t2.depth > t1.depth;
end if;
if new.pid is not null then
insert into s_ou_tree (pid, cid, depth, path) select  t1.pid, t2.cid, t1.depth + t2.depth + 1, concat(t1.path, t2.path) 
from s_ou_tree t1,s_ou_tree t2
where  t1.cid = new.pid and t2.pid = old.id;
end if;
end if;
end $$
delimiter ;
----------------------------------------------------------------------------
create table if not exists s_seou(
  se int unsigned not null, 
  ou int unsigned not null, 
  primary key (se, ou), 
  foreign key (ou)  references s_ou(id),
  foreign key (se)  references s_serial (id)
);
----------------------------------------------------------------------------
ALTER TABLE s_inv DROP INDEX s_inv_idx1;
create index s_inv_idx1 on s_inv(stax,status,form,serial,seq,idt);



---------------------------------
ALTER TABLE s_dcm ADD lbl_en       varchar(100)  not null
update s_dcm set lbl_en=lbl
---------------------------------------------------------
DROP TABLE IF EXISTS `s_group_role`;
CREATE TABLE `s_group_role`  (
  `GROUP_ID` int(11) NOT NULL,
  `ROLE_ID` int(11) NOT NULL
)
-------------------------------
-- ----------------------------
-- Table structure for s_group_user
-- ----------------------------
DROP TABLE IF EXISTS `s_group_user`;
CREATE TABLE `s_group_user`  (
  `USER_ID` varchar(20)  NOT NULL,
  `GROUP_ID` int(11) NOT NULL
  
);
---------------------------------------
-- ----------------------------
-- Table structure for s_group
-- ----------------------------
DROP TABLE IF EXISTS `s_group`;
CREATE TABLE `s_group`  (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci not null,
  `OU` int(255) NULL DEFAULT NULL,
  `STATUS` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci not null,
  `APPROVE` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DES` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
)
---------------
-- ----------------------------
-- Table structure for s_role
-- ----------------------------
DROP TABLE IF EXISTS `s_role`;
CREATE TABLE `s_role`  (
  `id` int(10) UNSIGNED NOT NULL ,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of s_role
-- ----------------------------
INSERT INTO s_role VALUES (1, 'Quản trị hệ thống', 'PERM_FULL_MANAGE');
INSERT INTO s_role VALUES (2, 'Cấu hình hệ thống', 'PERM_SYS_CONFIG');
INSERT INTO s_role VALUES (3, 'Quản lý người dùng', 'PERM_SYS_USER');
INSERT INTO s_role VALUES (4, 'Quản lý nhóm', 'PERM_SYS_GROUP');
INSERT INTO s_role VALUES (5, 'Phân quyền', 'PERM_SYS_ROLE');
INSERT INTO s_role VALUES (6, 'Tạo mẫu hóa đơn', 'PERM_SYS_TEMPLATE');
INSERT INTO s_role VALUES (7, 'Tạo mẫu Email', 'PERM_SYS_EMAIL');
INSERT INTO s_role VALUES (8, 'Cấu hình hóa đơn', 'PERM_SYS_INV_CONFIG');
INSERT INTO s_role VALUES (9, 'Cấu hình trường', 'PERM_SYS_FIELD_CONFIG');
INSERT INTO s_role VALUES (10, 'Quản trị danh mục', 'PERM_CATEGORY_MANAGE');
INSERT INTO s_role VALUES (11, 'Khách hàng', 'PERM_CATEGORY_CUSTOMER');
INSERT INTO s_role VALUES (12, 'Hàng hóa, dịch vụ', 'PERM_CATEGORY_ITEMS');
INSERT INTO s_role VALUES (13, 'Tỷ giá', 'PERM_CATEGORY_RATE');
INSERT INTO s_role VALUES (14, 'Danh mục khác', 'PERM_CATEGORY_ANOTHER');

INSERT INTO s_role VALUES (15, 'Thông báo phát hành', 'PERM_TEMPLATE_REGISTER');
--INSERT INTO s_role VALUES (16, N'Gán quyền sử dụng TBPH', 'PERM_TEMPLATE_REGISTER_GRANT');

INSERT INTO s_role VALUES (17, 'Lập hóa đơn', 'PERM_CREATE_INV_MANAGE');
INSERT INTO s_role VALUES (18, 'Lập hóa đơn từ Excel', 'PERM_CREATE_INV_EXCEL');
INSERT INTO s_role VALUES (19, 'Lập hóa đơn từ nguồn khác', 'PERM_CREATE_INV_ANOTHER');
INSERT INTO s_role VALUES (20, 'Cấp số', 'PERM_CREATE_INV_SEQ');
INSERT INTO s_role VALUES (21, 'Duyệt và xuất hóa đơn', 'PERM_APPROVE_INV_MANAGE');
INSERT INTO s_role VALUES (22, 'Báo cáo', 'PERM_REPORT_INV');
INSERT INTO s_role VALUES (23, 'Tra cứu hóa đơn', 'PERM_SEARCH_INV');
INSERT INTO s_role VALUES (24, 'Điều chỉnh hóa đơn', 'PERM_INVOICE_ADJUST');
INSERT INTO s_role VALUES (25, 'Thay thế hóa đơn', 'PERM_INVOICE_REPLACE');
INSERT INTO s_role VALUES (26, 'Hủy hóa đơn', 'PERM_INVOICE_CANCEL');
INSERT INTO s_role VALUES (27, 'Duyệt thông báo phát hành', 'PERM_TEMPLATE_APPROVE');
INSERT INTO s_role VALUES (28, 'Hủy thông báo phát hành', 'PERM_TEMPLATE_VOID');
INSERT INTO s_role VALUES (29, 'Tích hợp', 'PERM_INTEGRATED');
INSERT INTO s_role VALUES (30, 'Chờ hủy', 'PERM_VOID_WAIT');
INSERT INTO s_role VALUES (31, 'Tiến lùi ngày hóa đơn', 'PERM_REDIRECT_IDT');


-- ----------------------------
-- Table structure for s_group
-- ----------------------------
DROP TABLE IF EXISTS `s_group`;
CREATE TABLE `s_group`  (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci not null,
  `OU` int(255) NULL DEFAULT NULL,
  `STATUS` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci not null,
  `APPROVE` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `DES` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`ID`) USING BTREE
)
---------------

-- ----------------------------
-- Table structure for s_group
-- ----------------------------
drop table s_f_fld cascade constraints purge;
CREATE TABLE s_f_fld
(	
    id int unsigned not null auto_increment,
    fnc_name varchar(30) NOT NULL , 
    fnc_url varchar(15) NOT NULL , 
    fld_name varchar(30) NOT NULL , 
    fld_id varchar(30) NOT NULL , 
    fld_typ varchar(50) NOT NULL, 
    atr varchar(255), 
    feature varchar(255), 
    status  tinyint NOT NULL DEFAULT 0,
    primary key (id),
    unique(fnc_url,fld_id,feature)
) 
   -- "ID" IS 'ID duy nhất, Primary key';
   -- "FNC_NAME" IS 'Tên chức năng';
   -- "FNC_URL" IS 'URL chức năng';
   -- "FLD_NAME" IS 'Tên trường tương ứng trên chức năng';
   -- "ATTR" IS 'Thuộc tính, là chuỗi Json khai báo';
   -- "STATUS" IS 'Trạng thái; 1 - active, 0 - inactive';
   -- "FLD_ID" IS 'id trường';
   -- "FLD_type" IS 'Kiểu item của trường trên form (other, datagrid)';
   -- "feature" IS 'JSON nhận dạng màn hình ví dụ lập hđ';
   --  "S_F_FLD"  IS 'Bảng lưu các trường của từng chức năng và cấu hình các thuộc tính tương ứng';

   

-- ----------------------------
-- Table structure for s_inv_file
-- ----------------------------

CREATE TABLE s_inv_file (
  id int(10) unsigned NOT NULL,
  type VARCHAR(45) NOT NULL,
  content MEDIUMTEXT NOT NULL,
  PRIMARY KEY (id,type)/*,
  foreign key (id) references s_inv(id)*/
  )
-- COMMENT = 'ID, type, content của các file hóa đơn';
-- type: adj, rep, can

----alter pass to user

Alter table s_user ADD (pass varchar(255) default 'YWRtaW5AMTIz');

-- ----------------------------
-- Table structure for s_sys_logs
-- ----------------------------
DROP TABLE IF EXISTS `s_sys_logs`;
CREATE TABLE `s_sys_logs`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id tự sinh',
  `dt` datetime(0) NOT NULL COMMENT 'ngày ghi nhận log',
  `fnc_id` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'mã chức năng',
  `fnc_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'tên chức năng',
  `fnc_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'url thực thi',
  `action` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'action thực thi',
  `user_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'id user',
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'tên user',
  `src` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'nguồn phát sinh log',
  `dtl` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'chi tiết log',
  `r1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'trường dự phòng 1',
  `r2` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'trường dự phòng 2',
  `r3` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'trường dự phòng 3',
  `r4` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'trường dự phòng 4',
  `msg_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'trường lưu msg id (tên file, id chuỗi msg XML, id chuỗi Json)',
  `msg_status` int(255) NULL DEFAULT NULL COMMENT 'trạng thái xử lý msg (0 - xử lý lỗi, 1 - xử lý thành công)',
  PRIMARY KEY (`id`, `dt`) USING BTREE,
  INDEX `S_SYS_LOGS_IDX01`(`fnc_id`, `user_id`, `src`) USING BTREE,
  INDEX `S_SYS_LOGS_IDX02`(`src`, `msg_id`, `msg_status`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=31581 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='bảng lưu log audit thao tác của nsd app, api'
PARTITION BY RANGE  COLUMNS(dt)
(PARTITION p2000 VALUES LESS THAN ('2020-01-01') ENGINE = InnoDB,
 PARTITION p2001 VALUES LESS THAN ('2020-02-01') ENGINE = InnoDB,
 PARTITION p2002 VALUES LESS THAN ('2020-03-01') ENGINE = InnoDB,
 PARTITION p2003 VALUES LESS THAN ('2020-04-01') ENGINE = InnoDB,
 PARTITION p2004 VALUES LESS THAN ('2020-05-01') ENGINE = InnoDB,
 PARTITION p2005 VALUES LESS THAN ('2020-06-01') ENGINE = InnoDB,
 PARTITION p2006 VALUES LESS THAN ('2020-07-01') ENGINE = InnoDB,
 PARTITION p2007 VALUES LESS THAN ('2020-08-01') ENGINE = InnoDB,
 PARTITION p2008 VALUES LESS THAN ('2020-09-01') ENGINE = InnoDB,
 PARTITION p2009 VALUES LESS THAN ('2020-10-01') ENGINE = InnoDB,
 PARTITION p2010 VALUES LESS THAN ('2020-11-01') ENGINE = InnoDB,
 PARTITION p2011 VALUES LESS THAN ('2020-12-01') ENGINE = InnoDB,
 PARTITION p2100 VALUES LESS THAN ('2021-01-01') ENGINE = InnoDB,
 PARTITION p2101 VALUES LESS THAN ('2021-02-01') ENGINE = InnoDB,
 PARTITION p2102 VALUES LESS THAN ('2021-03-01') ENGINE = InnoDB,
 PARTITION p2103 VALUES LESS THAN ('2021-04-01') ENGINE = InnoDB,
 PARTITION p2104 VALUES LESS THAN ('2021-05-01') ENGINE = InnoDB,
 PARTITION p2105 VALUES LESS THAN ('2021-06-01') ENGINE = InnoDB,
 PARTITION p2106 VALUES LESS THAN ('2021-07-01') ENGINE = InnoDB,
 PARTITION p2107 VALUES LESS THAN ('2021-08-01') ENGINE = InnoDB,
 PARTITION p2108 VALUES LESS THAN ('2021-09-01') ENGINE = InnoDB,
 PARTITION p2109 VALUES LESS THAN ('2021-10-01') ENGINE = InnoDB,
 PARTITION p2110 VALUES LESS THAN ('2021-11-01') ENGINE = InnoDB,
 PARTITION p2111 VALUES LESS THAN ('2021-12-01') ENGINE = InnoDB,
 PARTITION p2200 VALUES LESS THAN ('2022-01-01') ENGINE = InnoDB,
 PARTITION p2201 VALUES LESS THAN ('2022-02-01') ENGINE = InnoDB,
 PARTITION p2202 VALUES LESS THAN ('2022-03-01') ENGINE = InnoDB,
 PARTITION p2203 VALUES LESS THAN ('2022-04-01') ENGINE = InnoDB,
 PARTITION p2204 VALUES LESS THAN ('2022-05-01') ENGINE = InnoDB,
 PARTITION p2205 VALUES LESS THAN ('2022-06-01') ENGINE = InnoDB,
 PARTITION p2206 VALUES LESS THAN ('2022-07-01') ENGINE = InnoDB,
 PARTITION p2207 VALUES LESS THAN ('2022-08-01') ENGINE = InnoDB,
 PARTITION p2208 VALUES LESS THAN ('2022-09-01') ENGINE = InnoDB,
 PARTITION p2209 VALUES LESS THAN ('2022-10-01') ENGINE = InnoDB,
 PARTITION p2210 VALUES LESS THAN ('2022-11-01') ENGINE = InnoDB,
 PARTITION p2211 VALUES LESS THAN ('2022-12-01') ENGINE = InnoDB,
 PARTITION pmax VALUES LESS THAN (MAXVALUE) ENGINE = InnoDB)
;

SET FOREIGN_KEY_CHECKS = 1;

CREATE INDEX s_inv_btax_idx 
	ON s_inv (status, type, btax);
	
CREATE INDEX s_serial_dt_idx 
	ON s_serial (taxc, fd, td);	
	
CREATE TABLE `s_invd`  (
  `ID` bigint(18) NOT NULL AUTO_INCREMENT COMMENT 'ID tự sinh theo sequence',
  `INV_ID` decimal(18, 0) NULL DEFAULT NULL COMMENT 'ID tham chiếu đến bảng S_INV',
  `REF_NO` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'Số bút toán',
  PRIMARY KEY (`ID`) USING BTREE,
  INDEX `S_INVD_IDX01`(`INV_ID`) USING BTREE,
  INDEX `S_INVD_IDX02`(`REF_NO`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'Bảng chi tiết hóa đơn' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;	

ALTER TABLE `einvoice`.`s_inv` ADD COLUMN `cdt` datetime GENERATED ALWAYS AS (json_unquote(json_extract(`doc`,'$.cdt'))) VIRTUAL;

--Trigger check id in S_INV
CREATE DEFINER=`appuser`@`%` PROCEDURE `einvoice`.`prc_check_id`(p_id int)
BEGIN DECLARE v_count INT2;

SELECT
	COUNT(*)
INTO
	v_count
from
	s_inv
where
	id = p_id;

if v_count <= 0 THEN signal sqlstate '20000' set
message_text = 'Cannot find Ref Id in S_INV';

end IF;

END;


CREATE DEFINER=`appuser`@`%` TRIGGER trg_s_invfile_check_id_bi BEFORE
INSERT
	ON
	s_inv_file FOR EACH ROW BEGIN CALL prc_check_id(NEW.id);

END;

CREATE DEFINER=`appuser`@`%` TRIGGER trg_s_invfile_check_id_bu BEFORE
UPDATE
	ON
	s_inv_file FOR EACH ROW BEGIN CALL prc_check_id(NEW.id);

END;

-- ----------------------------
-- Table structure for s_err_msg
-- ----------------------------
CREATE TABLE `s_err_msg`  (
  `SRC_ERR_CODE` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'Mã thông báo nguồn',
  `USR_CODE` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'Mã thông báo hiển thị cho NSD',
  `USR_MSG` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Mô tả thông báo hiển thị cho NSD. Nếu cần truyền tham số đặt tên các tham số là ${p0}, ${p1},...,${pn}',
  `USR_MSG_ENT` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'Mô tả thông báo hiển thị cho NSD bằng tiếng Anh. Nếu cần truyền tham số đặt tên các tham số là ${p0}, ${p1},...,${pn}',
  `TYPE_MSG` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'Loại thông báo. INFO - Thông báo, WARNING - Cảnh báo, ERROR - Lỗi',
  PRIMARY KEY (`SRC_ERR_CODE`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'Bảng lưu danh mục mã thông báo NSD' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

CREATE INDEX s_inv_cdt_IDX USING BTREE ON s_inv (stax,ou,`type`,form,serial,status);

------- trungpq10 them cột des trong s_serial mô tả mục đích sử dụng th=bph
ALTER TABLE s_serial
 ADD (des varchar(200));

-----------------------------------------------------------------------------------------------
ALTER TABLE s_org  ADD (type int(1) NULL DEFAULT 1 COMMENT 'kiểu dữ liệu 1_nhập tay/2_tự động');

--Tăng số hóa đơn lên 8 ký tự
ALTER TABLE `s_inv` 
MODIFY COLUMN `seq` varchar(8) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.seq')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.seq')))) VIRTUAL NULL AFTER `serial`;


---xoa bang s_seou

DROP TABLE IF EXISTS `s_seou`;
----

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for s_serial
-- ----------------------------
DROP TABLE IF EXISTS `s_serial`;
CREATE TABLE `s_serial`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `taxc` varchar(14) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `type` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `form` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `serial` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `min` int(10) UNSIGNED NOT NULL,
  `max` int(10) UNSIGNED NOT NULL,
  `cur` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 3,
  `fd` datetime(0) NOT NULL,
  `td` datetime(0) NULL DEFAULT NULL,
  `dt` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `uc` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `uses` tinyint(4) NOT NULL DEFAULT 1,
  `des` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `priority` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `taxc`(`taxc`, `form`, `serial`,`priority`) USING BTREE,
  check (
    min > 0 and
    (cur>=min or cur=0)
    and max >= cur
  ),
  INDEX `uc`(`uc`) USING BTREE,
  INDEX `s_serial_dt_idx`(`taxc`, `fd`, `td`) USING BTREE,
  CONSTRAINT `s_serial_ibfk_1` FOREIGN KEY (`uc`) REFERENCES `s_user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `s_serial_ibfk_2` FOREIGN KEY (`taxc`) REFERENCES `s_ou` (`taxc`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 183 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
----
DROP TABLE IF EXISTS `s_seou`;
CREATE TABLE `s_seou`  (
  `se` int(10) UNSIGNED NOT NULL,
  `ou` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`se`, `ou`) USING BTREE,
  INDEX `ou`(`ou`) USING BTREE,
  CONSTRAINT `s_seou_ibfk_1` FOREIGN KEY (`ou`) REFERENCES `s_ou` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `s_seou_ibfk_2` FOREIGN KEY (`se`) REFERENCES `s_serial` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

ALTER TABLE `s_sys_logs` 
ADD COLUMN `doc` json NULL COMMENT 'Thông tin dữ liệu ghi log, lưu dưới dạng json' AFTER `msg_status`;

--Tăng độ rộng của trường người mua lên 500 ký tư
ALTER TABLE `einvoice`.`s_inv` 
MODIFY COLUMN `bname` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci GENERATED ALWAYS AS (if((json_unquote(json_extract(`doc`,'$.bname')) = 'null'),NULL,json_unquote(json_extract(`doc`,'$.bname')))) VIRTUAL NULL AFTER `buyer`;

ALTER TABLE s_org MODIFY COLUMN name varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL;
ALTER TABLE s_org MODIFY COLUMN name_en varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL;

---------tao table s_report_tmp


-- ----------------------------
-- Table structure for s_report_tmp
-- ----------------------------
DROP TABLE IF EXISTS `s_report_tmp`;
CREATE TABLE `s_report_tmp`  (
  `c_id` bigint(20) NULL DEFAULT NULL,
  `form` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `serial` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `min1` int(11) NULL DEFAULT NULL,
  `max1` int(11) NULL DEFAULT NULL,
  `min0` int(11) NULL DEFAULT NULL,
  `max0` int(11) NULL DEFAULT NULL,
  `maxu0` int(11) NULL DEFAULT NULL,
  `minu` int(11) NULL DEFAULT NULL,
  `maxu` int(11) NULL DEFAULT NULL,
  `cancel` int(11) NULL DEFAULT NULL,
  `clist` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `minc` int(11) NULL DEFAULT NULL,
  `maxc` int(11) NULL DEFAULT NULL,
  INDEX `c_id_idx`(`c_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;



---------------------------------------------------
ALTER TABLE s_inv MODIFY COLUMN adjseq varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci GENERATED ALWAYS AS (if((json_unquote(json_extract(doc,'$.adj.seq')) = 'null'),NULL,json_unquote(json_extract(doc,'$.adj.seq')))) VIRTUAL NULL;

CREATE TABLE `s_seusr`  (
  `se` int(10) UNSIGNED NOT NULL,
  `usrid` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`se`, `usrid`) USING BTREE,
  INDEX `s_seusr_FK2`(`usrid`) USING BTREE,
  CONSTRAINT `s_seusr_FK1` FOREIGN KEY (`se`) REFERENCES `s_serial` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `s_seusr_FK2` FOREIGN KEY (`usrid`) REFERENCES `s_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = 'Bảng lưu thông tin phân quyền dải TBPH theo user' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

-------------------------------------
   ALTER TABLE s_org 
   DROP INDEX s_org_taxc_uk;
	 
	 ALTER TABLE `s_org` ADD INDEX `s_org_taxc_uk` (`taxc`)
------------------
 ALTER TABLE s_org MODIFY COLUMN fn varchar(512) GENERATED ALWAYS AS (coalesce(coalesce(nullif(`code`,''),concat_ws('-',`name`,`tel`,`mail`,`fadd`)))) STORED NULL;
 
ALTER TABLE s_ou ADD c0 varchar(255) NULL;
ALTER TABLE s_ou ADD c1 varchar(255) NULL;
ALTER TABLE s_ou ADD c2 varchar(255) NULL;
ALTER TABLE s_ou ADD c3 varchar(255) NULL;
ALTER TABLE s_ou ADD c4 varchar(255) NULL;
ALTER TABLE s_ou ADD c5 varchar(255) NULL;
ALTER TABLE s_ou ADD c6 varchar(255) NULL;
ALTER TABLE s_ou ADD c7 varchar(255) NULL;
ALTER TABLE s_ou ADD c8 varchar(255) NULL;
ALTER TABLE s_ou ADD c9 varchar(255) NULL;

CREATE DEFINER=`appuser`@`%` TRIGGER s_ou_bd
  BEFORE DELETE
  ON s_ou
  FOR EACH ROW
BEGIN
    /*insert into xxx(xxx) values (concat('1 ',old.id));*/
    delete from s_ou_tree where pid = old.id;
    delete from s_ou_tree where cid = old.id;
END;

ALTER TABLE s_org
  MODIFY fadd varchar(512);

--Table: b_inv

DROP TABLE IF EXISTS b_inv;

CREATE TABLE b_inv (
  id        int(10) UNSIGNED AUTO_INCREMENT NOT NULL,
  doc       json NOT NULL,
  xml       mediumtext,
  idt       datetime AS (cast(json_unquote(json_extract(`doc`,'$.idt')) as datetime)) VIRTUAL,
  `type`    varchar(10) AS (json_unquote(json_extract(`doc`,'$.type'))) VIRTUAL,
  form      varchar(11) AS (json_unquote(json_extract(`doc`,'$.form'))) VIRTUAL,
  `serial`  varchar(8) AS (json_unquote(json_extract(`doc`,'$.serial'))) VIRTUAL,
  seq       varchar(7) AS (json_unquote(json_extract(`doc`,'$.seq'))) VIRTUAL,
  paym      varchar(100) AS (json_unquote(json_extract(`doc`,'$.paym'))) VIRTUAL,
  curr      varchar(3) AS (json_unquote(json_extract(`doc`,'$.curr'))) VIRTUAL,
  exrt      decimal(8,2) AS (json_unquote(json_extract(`doc`,'$.exrt'))) VIRTUAL,
  sname     varchar(255) AS (json_unquote(json_extract(`doc`,'$.sname'))) VIRTUAL,
  stax      varchar(14) AS (json_unquote(json_extract(`doc`,'$.stax'))) VIRTUAL,
  saddr     varchar(255) AS (json_unquote(json_extract(`doc`,'$.saddr'))) VIRTUAL,
  smail     varchar(255) AS (json_unquote(json_extract(`doc`,'$.smail'))) VIRTUAL,
  stel      varchar(255) AS (json_unquote(json_extract(`doc`,'$.stel'))) VIRTUAL,
  sacc      varchar(255) AS (json_unquote(json_extract(`doc`,'$.sacc'))) VIRTUAL,
  sbank     varchar(255) AS (json_unquote(json_extract(`doc`,'$.sbank'))) VIRTUAL,
  buyer     varchar(255) AS (json_unquote(json_extract(`doc`,'$.buyer'))) VIRTUAL,
  bname     varchar(255) AS (json_unquote(json_extract(`doc`,'$.bname'))) VIRTUAL,
  bcode     varchar(100) AS (json_unquote(json_extract(`doc`,'$.bcode'))) VIRTUAL,
  btax      varchar(14) AS (json_unquote(json_extract(`doc`,'$.btax'))) VIRTUAL,
  baddr     varchar(255) AS (json_unquote(json_extract(`doc`,'$.baddr'))) VIRTUAL,
  bmail     varchar(255) AS (json_unquote(json_extract(`doc`,'$.bmail'))) VIRTUAL,
  btel      varchar(255) AS (json_unquote(json_extract(`doc`,'$.btel'))) VIRTUAL,
  bacc      varchar(255) AS (json_unquote(json_extract(`doc`,'$.bacc'))) VIRTUAL,
  bbank     varchar(255) AS (json_unquote(json_extract(`doc`,'$.bbank'))) VIRTUAL,
  note      varchar(255) AS (json_unquote(json_extract(`doc`,'$.note'))) VIRTUAL,
  sumv      decimal(19,4) AS (json_unquote(json_extract(`doc`,'$.sumv'))) VIRTUAL,
  vatv      decimal(19,4) AS (json_unquote(json_extract(`doc`,'$.vatv'))) VIRTUAL,
  totalv    decimal(19,4) AS (json_unquote(json_extract(`doc`,'$.totalv'))) VIRTUAL,
  ou        int(10) UNSIGNED AS (json_unquote(json_extract(`doc`,'$.ou'))) VIRTUAL,
  uc        varchar(20) AS (json_unquote(json_extract(`doc`,'$.uc'))) VIRTUAL,
  paid      int(10) UNSIGNED AS (json_unquote(json_extract(`doc`,'$.paid'))) VIRTUAL,
  /* Keys */
  PRIMARY KEY (id)
) ENGINE = InnoDB;

CREATE UNIQUE INDEX stax
  ON b_inv
  (stax, form, `serial`, seq);  
  
ALTER TABLE s_user ADD create_date DATETIME DEFAULT CURRENT_TIMESTAMP NULL;
ALTER TABLE s_user ADD last_login DATETIME NULL;
  
DROP TABLE IF EXISTS s_inv_id;

CREATE TABLE s_inv_id (
  id   int(10) NOT NULL,
  sec  varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  ic   varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  /* Keys */
  PRIMARY KEY (id)
) ENGINE = InnoDB
  COLLATE utf8_bin;

CREATE UNIQUE INDEX ic
  ON s_inv_id
  (ic);

CREATE UNIQUE INDEX sec
  ON s_inv_id
  (sec);

CREATE DEFINER=`appuser`@`%` TRIGGER s_inv_bi
  AFTER INSERT
  ON s_inv
  FOR EACH ROW
BEGIN
  INSERT INTO s_inv_id (id, sec, ic) VALUES(new.id, new.sec, new.ic);
END;

CREATE DEFINER=`appuser`@`%` TRIGGER s_inv_bu
  BEFORE UPDATE
  ON s_inv
  FOR EACH ROW
BEGIN
  UPDATE s_inv_id SET sec=new.sec, ic=new.ic WHERE id=new.id;
END;

CREATE DEFINER=`appuser`@`%` TRIGGER s_inv_bd
  BEFORE DELETE
  ON s_inv
  FOR EACH ROW
BEGIN
  DELETE FROM s_inv_id WHERE id=old.id;
END;

ALTER TABLE einvoice.s_inv MODIFY COLUMN xml LONGTEXT NULL;  

ALTER TABLE s_user ADD `local` tinyint(4) DEFAULT 0 NOT NULL COMMENT 'User là local hay không. 0 - Local, 1 - AD, LDAP,...';
ALTER TABLE s_user ADD change_pass_date datetime DEFAULT CURRENT_TIMESTAMP NULL COMMENT 'Ngày đổi mật khẩu gần nhất';
ALTER TABLE s_user ADD login_number int(10) unsigned DEFAULT 0 NOT NULL COMMENT 'Số lần đăng nhập thành công';
ALTER TABLE s_user ADD change_pass int(10) unsigned DEFAULT 0 NOT NULL;

CREATE TABLE `app_einv_onprem`.`s_user_pass` (`user_id` VARCHAR(20) NOT NULL, `pass` VARCHAR(200) NOT NULL, `change_date` datetime(3) NULL COMMENT 'NgÃ y thay Ä‘á»•i máº­t kháº©u', CONSTRAINT `PK__s_user_p__518C386C17C39B87` PRIMARY KEY (`user_id`, `pass`));

ALTER TABLE `app_einv_onprem`.`s_user_pass` ADD CONSTRAINT `FK__s_user_pa__user___49CEE3AF` FOREIGN KEY (`user_id`) REFERENCES `app_einv_onprem`.`s_user` (`id`) ON UPDATE NO ACTION ON DELETE NO ACTION;


