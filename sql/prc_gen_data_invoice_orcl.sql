/* Formatted on 28-Nov-2020 21:21:08 (QP5 v5.276) */
-- Start of DDL Script for Procedure EINVOICE.PRC_GEN_DATA_INVOICE
-- Generated 28-Nov-2020 21:21:00 from EINVOICE@(DESCRIPTION =(ADDRESS_LIST =(ADDRESS = (PROTOCOL = TCP)(HOST = 10.15.68.114)(PORT = 1521)))(CONNECT_DATA =(SERVICE_NAME = einvvib)))

CREATE OR REPLACE PROCEDURE prc_gen_data_invoice (p_fd          IN DATE,
                                                  p_td          IN DATE,
                                                  p_recperday      NUMBER,
                                                  p_doc            CLOB,
                                                  p_ou             NUMBER,
                                                  p_uc             VARCHAR2)
IS
    v_invdate   DATE;
    v_idinv     NUMBER;
    v_docinv    CLOB;
    v_sec       VARCHAR2 (10);
    v_cnt       NUMBER;
BEGIN
    v_invdate := p_fd;
    v_docinv := p_doc;

    SELECT MAX (id) INTO v_idinv FROM s_inv;

    --Lặp từ ngày đến ngày
    WHILE (v_invdate <= p_td)
    LOOP
        BEGIN
            --Lặp để tạo số hóa đơn 1 ngày
            v_cnt := 1;

            WHILE (v_cnt <= p_recperday)
            LOOP
                BEGIN
                    DBMS_OUTPUT.put_line ('XXX' || v_cnt);

                    --Gán lại biến doc
                    v_docinv := p_doc;
                    --Gán lại ngày hóa đơn
                    /*
                    SET @docinv =
                           JSON_MODIFY
                           (@docinv, '$.idt', CONVERT (VARCHAR (10), @invdate, 23));
                    */
                    v_docinv :=
                        REPLACE (v_docinv,
                                 '#idt#',
                                 TO_CHAR (v_invdate, 'YYYY-MM-DD'));
                    --Gán lại số hóa đơn
                    v_docinv :=
                        REPLACE (v_docinv, '#seq#', LPAD (v_cnt, 7, '0'));
                    /*
                           JSON_MODIFY
                           (
                              @docinv,
                              '$.seq',
                              CONVERT
                              (VARCHAR (7), RIGHT (concat ('0000000', @cnt), 7)));
                    */
                    v_idinv := v_idinv + 1;

                    --Gán lại mã sec
                    --SET @sec = master.dbo.fn_varbintohexstr(HASHBYTES('SHA1', CONVERT(NVARCHAR(10), @idinv)));
                    SELECT LPAD (standard_hash (v_idinv, 'MD5'), 10, 'H')
                      INTO v_sec
                      FROM DUAL;

                    --Gán lại sec
                    /*
                    SET @docinv =
                           JSON_MODIFY
                           (
                              @docinv,
                              '$.sec',
                              @sec);
                    */
                    v_docinv := REPLACE (v_docinv, '#sec#', v_sec);
                    DBMS_OUTPUT.put_line ('XXX' || v_sec);

                    INSERT INTO s_inv (id,
                                       sec,
                                       idt,
                                       doc,
                                       dt,
                                       ou,
                                       uc)
                    VALUES (v_idinv,
                            v_sec,
                            v_invdate,
                            v_docinv,
                            v_invdate,
                            p_ou,
                            p_uc);

                    v_cnt := v_cnt + 1;
                EXCEPTION
                    WHEN OTHERS
                    THEN
                        NULL;
                END;
            END LOOP;

            --SET @invdate = DATEADD (day, 1, @invdate)
            v_invdate := v_invdate + 1;
        END;
    END LOOP;
EXCEPTION
    WHEN OTHERS
    THEN
        NULL;
END;                                                              -- Procedure
/



-- End of DDL Script for Procedure EINVOICE.PRC_GEN_DATA_INVOICE