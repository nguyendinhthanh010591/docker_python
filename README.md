### How to run
- npm install
- npm run server   to start backend server
- npm run start    to start the client app
- open ```http://localhost:8080```
#### Build production files
npm run build
npm run pm2
You can access the production version of the app at http://localhost:3000
To deploy on a remote server, copy content of "public" and "server" folders
-------------------------------------------------------------------------------
### mysql
https://tecadmin.net/install-mysql-on-centos-redhat-and-fedora/
(rpm -Uvh https://repo.mysql.com/mysql57-community-release-el7-11.noarch.rpm
yum install mysql-community-server)
yum install https://dev.mysql.com/get/mysql80-community-release-el7-2.noarch.rpm
yum --disablerepo=mysql80-community --enablerepo=mysql57-community install mysql-community-server
grep "A temporary password" /var/log/mysqld.log
systemctl start mysqld.service
mysql_secure_installation
systemctl restart mysqld.service
systemctl enable mysqld.service
----------------------------------------------------------------------------
1--node
yum install -y gcc-c++ make
curl -sL https://rpm.nodesource.com/setup_12.x | bash -
yum install -y nodejs
curl -sL https://rpm.nodesource.com/setup_10.x | bash -
yum install nodejs
----------------------------------------------------------------------------
--2--RedisBloom
--yum install gcc gcc-c++ kernel-devel make
--yum groupinstall "Development Tools"
--cd /tmp
--git clone https://github.com/RedisBloom/RedisBloom.git
--cd RedisBloom
--make
--cp redisbloom.so /etc/redisbloom.so
------------------------------------------------------------------
3--redis
yum -y install http://rpms.remirepo.net/enterprise/remi-release-7.rpm
yum --enablerepo=remi install redis
--edit redis.conf
vi /etc/redis.conf
bind 127.0.0.1 host192
protected-mode no
requirepass redis@6379
--loadmodule /etc/redisbloom.so
--start redis
systemctl enable redis
systemctl start redis
systemctl status redis
-------------------------------------------------------------------
4--pm2
npm install pm2@latest -g
--pm2.io account
---------------------------------------------------------------------------
5--openssl
1--windows : https://slproweb.com/products/Win32OpenSSL.html
2--linux : yum install openssl
3--Generate self-signed certificate
openssl req -x509 -sha256 -newkey rsa:2048 -keyout key.pem -out cert.pem -days 3650 -nodes -subj "/C=VN/O=FPT/OU=FIS/CN=einvoice"
openssl req -x509 -sha256 -newkey rsa:2048 -keyout key.pem -out cert.pem -days 3650        -subj "/C=VN/O=FPT/OU=FIS/CN=einvoice"
-nodes : Do not protect the private key with a passphrase
--pem2p12
openssl pkcs12 -export -out key.p12 -inkey key.pem -in cert.pem
--pfx2pem
openssl pkcs12 -in cert.pfx -out cert.pem
openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -keyout /etc/nginx/cert.key -out /etc/nginx/cert.crt -subj "/C=VN/O=FPT/OU=FIS/CN=einvoice"
openssl dhparam -out /etc/nginx/dhparam.pem 2048
----------------------------------------------------------------------------
6--nginx
vi /etc/yum.repos.d/nginx.repo
[nginx]
name=nginx repo
baseurl=http://nginx.org/packages/mainline/rhel/7/$basearch/
gpgcheck=0
enabled=1

yum install nginx
systemctl enable nginx
systemctl start nginx
systemctl status nginx
vi /etc/nginx/nginx.conf
...
nginx -t
systemctl restart nginx
--------------------------------------------------------------------------------
yum install vsftpd ftp -y
vi /etc/vsftpd/vsftpd.conf
systemctl enable vsftpd
systemctl start vsftpd
-------------------------------------------------------------------------------
yum install pango.x86_64 libXcomposite.x86_64 libXcursor.x86_64 libXdamage.x86_64 libXext.x86_64 libXi.x86_64 libXtst.x86_64 cups-libs.x86_64 libXScrnSaver.x86_64 libXrandr.x86_64 GConf2.x86_64 alsa-lib.x86_64 atk.x86_64 gtk3.x86_64 ipa-gothic-fonts xorg-x11-fonts-100dpi xorg-x11-fonts-75dpi xorg-x11-utils xorg-x11-fonts-cyrillic xorg-x11-fonts-Type1 xorg-x11-fonts-misc
yum install curl cabextract xorg-x11-font-utils fontconfig
rpm -i https://downloads.sourceforge.net/project/mscorefonts2/rpms/msttcore-fonts-installer-2.6-1.noarch.rpm
-------------------------------------------------------------------------------
openldap
1-setup
yum -y install openldap compat-openldap openldap-clients openldap-servers openldap-servers-sql openldap-devel
systemctl start slapd
systemctl enable slapd

2-slappasswd
slappasswd
{SSHA}YxP0CDKzm/hXaFVCvcr0Mjy/SjglHr+C

slappasswd -h {SSHA} -s admin@123

3-vi db.ldif
dn: olcDatabase={2}hdb,cn=config
changetype: modify
replace: olcSuffix
olcSuffix: dc=tlg,dc=com

dn: olcDatabase={2}hdb,cn=config
changetype: modify
replace: olcRootDN
olcRootDN: cn=ldapadm,dc=tlg,dc=com

dn: olcDatabase={2}hdb,cn=config
changetype: modify
replace: olcRootPW
olcRootPW: {SSHA}7MXVskneY+3ghIEFNm2Mct5qWflAMtuP

4-vi monitor.ldif
dn: olcDatabase={1}monitor,cn=config
changetype: modify
replace: olcAccess
olcAccess: {0}to * by dn.base="gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth" read by dn.base="cn=ldapadm,dc=tlg,dc=com" read by * none

dn: olcDatabase={2}hdb,cn=config
changetype: modify
add: olcAccess
olcAccess: {0}to attrs=userPassword,shadowLastChange by dn="cn=ldapadm,dc=tlg,dc=com" write by anonymous auth by self write by * none
olcAccess: {1}to dn.base="" by * read
olcAccess: {2}to * by dn="cn=ldapadm,dc=tlg,dc=com" write by * read


5-ssl
openssl req -new -x509 -nodes -out /etc/openldap/certs/tlg.crt -keyout /etc/openldap/certs/tlg.key -days 1095 -subj "/C=VN/O=TLG/CN=host56"

chown -R ldap:ldap /etc/openldap/certs/tlg*
chmod 0640 /etc/openldap/certs/tlg* 
ll /etc/openldap/certs/tlg*

6-vi certs.ldif
dn: cn=config
changetype: modify
replace: olcTLSCertificateFile
olcTLSCertificateFile: /etc/openldap/certs/tlg.crt

dn: cn=config
changetype: modify
replace: olcTLSCertificateKeyFile
olcTLSCertificateKeyFile: /etc/openldap/certs/tlg.key



7-modify
ldapmodify -Y EXTERNAL -H ldapi:/// -f db.ldif
ldapmodify -Y EXTERNAL -H ldapi:/// -f monitor.ldif
ldapmodify -Y EXTERNAL -H ldapi:/// -f certs.ldif
slaptest -u

8-
cp /usr/share/openldap-servers/DB_CONFIG.example /var/lib/ldap/DB_CONFIG
chown ldap:ldap /var/lib/ldap/*

9-
ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/cosine.ldif
ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/nis.ldif 
ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/inetorgperson.ldif

10-vi base.ldif
dn: dc=tlg,dc=com
dc: tlg
objectClass: top
objectClass: domain

dn: cn=ldapadm,dc=tlg,dc=com
objectClass: organizationalRole
cn: ldapadm
description: LDAP Manager

dn: ou=People,dc=tlg,dc=com
objectClass: organizationalUnit
ou: People

dn: ou=Group,dc=tlg,dc=com
objectClass: organizationalUnit
ou: Group

11-
ldapadd -x -W -D "cn=ldapadm,dc=tlg,dc=com" -f base.ldif

12-
vi /etc/sysconfig/slapd
SLAPD_URLS="ldapi:/// ldap:/// ldaps:///"
systemctl restart slapd
netstat -antup | grep -i 636

13-firewall
firewall-cmd --permanent --add-service=ldaps
firewall-cmd --reload
--------------------------------------------------------------------------------
chrome :
sudo yum install https://rpmfind.net/linux/centos/7.7.1908/os/x86_64/Packages/liberation-narrow-fonts-1.07.2-16.el7.noarch.rpm
sudo yum install https://rpmfind.net/linux/centos/7.7.1908/os/x86_64/Packages/liberation-fonts-1.07.2-16.el7.noarch.rpm
sudo yum install https://rpmfind.net/linux/centos/7.7.1908/os/x86_64/Packages/spax-1.5.2-13.el7.x86_64.rpm
sudo yum install https://rpmfind.net/linux/centos/7.7.1908/os/x86_64/Packages/redhat-lsb-submod-security-4.1-27.el7.centos.1.x86_64.rpm
sudo yum install https://rpmfind.net/linux/centos/7.7.1908/os/x86_64/Packages/redhat-lsb-core-4.1-27.el7.centos.1.x86_64.rpm
sudo yum install https://dl.google.com/linux/direct/google-chrome-stable_current_x86_64.rpm
which google-chrome
google-chrome &

npm uninstall puppeteer
env PUPPETEER_SKIP_CHROMIUM_DOWNLOAD=true npm i puppeteer
-----------------------------------------------------------------------------
redis-cli --scan --pattern TEMPLATE.* | xargs redis-cli del
redis-cli --scan --pattern COL.* | xargs redis-cli del
redis-cli --scan --pattern SERIAL.* | xargs redis-cli del

----------------------------------------------------------------
/usr/share/fonts/hdb
sudo fc-cache -f -v

### Cài gói xml-crypto 1.5.3 sử dụng để ký XML lớn và phân nhánh nhiều items
0-copy file xml-crypto-1.5.3.tgz trong thư mục einvoice\server\tool sang thư mục einvoice
1-npm uninstall xml-crypto -s
2-npm i xml-crypto-1.5.3.tgz -s

### Cài các gói thư viện cho ADFS
1-npm i cookie-parser -s
2-npm i passport-saml -s
3-npm i saml-encoder-decoder-js -s

### Cài các gói thư viện cho keyvault
1-npm i @azure/keyvault-secrets -s
2-npm i @azure/identity -s
3-npm i @types/node -s

### Cách sử dụng cột động
{"suggest":{"data":[{"id":1,"value":1},{"id":2,"value":2}]}}
{"suggest":{"url":"api/select?type=Z"}}


### Cấu hình hệ thống file config (SERVER)
1. vrt: 
2. ldapPrivate: 1-Dùng quản lý tài khoản riêng    2-Dùng quản lý tài khoản chung
3. ent: Phân biệt môi trường: HLV:'hlv' HDB:'hdb' LG:'lge' VIN:'vcm' Thanhlong:'tlg'
4. xls_idt: 1-Check ngày hóa đơn nhâp từ excel    0-Không check
5. ou_grant: 1-Check phân quyền theo ou          0-Không check
6. serial_grant: 1-Check phân quyền theo ký hiệu  0-Không check
7. ldap, ldapsConfig,baseDN,adminDN,adminPW,searchFilter: Thông tin nơi quản lý tài khoản
   7.  ldap: 'AD' hoặc 'OPENLDAP'   thì vd baseDN, adminDN, searchFilter lần lượt tương ứng: 
                'AD'                                                                                'OPENLDAP'
    baseDN: "DC=einvoice,DC=inter",                                                             baseDN: "dc=fptvn,dc=com",
    adminDN: "einvoice\\Administrator",                                                         adminDN: "cn=Manager,dc=fptvn,dc=com",
   "(&(objectCategory=person)(objectclass=user)(sAMAccountName={{username}}))"                  "(&(objectClass=inetOrgPerson)(uid={{username}}))"
8. poolConfig: Địa chỉ DB
9. redisConfig: Địa chỉ redis
10. sessionTimeout: 10000 là 10 giây (Thời gian hết hạn phiên)
11. upload_minute: Upload biên bản - 1               Tự động tạo biên bản - 0
12. MAX_ROW_EXCEL: Số lượng dòng max excel đọc vào
13. sendmail_pdf_xml: 0/1 on/off chức năng gửi mail đính kèm zip(xml,pdf)
14. sendApprMail: 0/1 on/off chức năng gửi mail khi DUYET
15. sendCancMail: 0/1 on/off chức năng gửi mail khi HUY
16. genPassFile: gen mật khẩu file zip attach khi gửi mail. 1 - Có, 0 - Không

### Cấu hình hệ thống file config (CLIENT)
1. minutes_ext: "docx,doc,pdf,..." để 1 loại biên bản extension mong muốn tải ra.
2. GRIDNFCONF: Số dấu phẩy sau thập phân của phần chi tiết hóa đơn

### Cấu hình trong file ext.js
ext_resetpass_login = [0,1]   1- Có dùng reset pass ở login   0- Không

### Cấu hình captcha phải dùng 2 chỗ và phải giống nhau
# 0-ko dung 1-offline 2-online(Google)
1. Cấu hình biến captchaType=[0,1,2] bên client vào ext.js
2. Cấu hình biến captchaType=[0,1,2] bên Server trong cons.js

### Cấu hình tên địa chỉ: VD: https://host35:8080/einvoice   ở đây homepage là einvoice
1. Mở file \public\codebase\config.js
2. Sửa tham số homepage="" thành homepage="einvoice"


### Cấu hình Hướng dẫn sử dụng:
Mở file public/codebase/config.js , trong khu CONFIG_HDSD bỏ rem phần set 2 biến HDSD_PORTAL, HDSD_APP tùy theo môi trường.

# Mã hóa các mật khẩu :
## Tham số poolConfig, password
## Tham số redisConfig, password
## Tham số adminPW
## Tham số config_jsrep, JSREPORT_PASS
## 1. Mã hóa mật khẩu, chạy các lệnh sau:
1.1. npm install crypto --save
1.2. cd đến thư mục server\tool 
1.3. npm i -g run-func
1.4. Mở file server\tool\encrypt.js, thay giá trị biến key theo mong muốn (ví dụ key = 'einvoice2021')
1.5. run-func encrypt.js encrypt value (value là giá trị cần mã hóa)
1.6. Copy giá trị mã hóa từ console vào các tham số cần set ở trên

## 2. Giải mã
2.1. Copy giá trị biến key trong file server\tool\encrypt.js vào giá trị biến key trong file server\api\encrypt.js

## 3. Restart server ứng dụng

------------------------------------------------------------------------
-Enable EPEL repo:
yum install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm

----------------------------------------------------------------------------
#PostgreSQL 12 
yum -y install https://download.postgresql.org/pub/repos/yum/reporpms/EL-7-x86_64/pgdg-redhat-repo-latest.noarch.rpm

yum -y install epel-release yum-utils
yum-config-manager --enable pgdg12
yum install postgresql12-server
/usr/pgsql-12/bin/postgresql-12-setup initdb
systemctl enable --now postgresql-12
systemctl status postgresql-12
su - postgres
sql -c "alter user postgres with password 'postgres'"
vi /var/lib/pgsql/12/data/postgresql.conf
listen_addresses = '*'
vi /var/lib/pgsql/12/data/pg_hba.conf
host all all 0.0.0.0/0 md5
systemctl restart postgresql-12
## run jsreport
pm2 start server.js --name jsreportapp