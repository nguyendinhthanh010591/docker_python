import "./styles/app.css"
import "babel-polyfill"
import { JetApp, JetView, plugins, StoreRouter, HashRouter } from "webix-jet"
const UPLOAD_URL = ["api/inv/upload", "api/ous/upload", "api/gns/upload", "api/org/upload", "api/inv/uplupd", "api/inv/minutes/upload","api/inv/bin/upload", "api/inv/minutes/uploadAtt","api/inv/uplosg"]
const use_adfs = () => {
    try {
        return adfs
    }
    catch (err) {
        return 0
    }
}
//Có sử dụng Azure AD hay không
const isUsingAzureAD = () => {
    try {
        return useAzureAD;
    } catch (error) {
        return false;
    }
};
const isValidSession = () => {
	const session = webix.storage.session.get("session")
	return session && session.token && session.uid && (new Date(session.expires) > new Date())
}
class Refresh extends JetView {
	config() {
		return {}
	}
	ready() {
		webix.ajax("api/cat/refresh").then(data => {
			webix.alert(data.text()).then(() => { this.show("/top/home") })
		})
	}
}
class Logout extends JetView {
	config() {
		return {}
	}
	ready() {
		webix.storage.session.clear()
		if (use_adfs() || isUsingAzureAD()) {
			window.location.replace(`/api/logout`);
			return;
		}
		window.location.replace(`/${homepage}`)
	}
}

export default class App extends JetApp {
	constructor(config) {
		const defaults = {
			id: APPNAME,
			version: VERSION,
			//routerPrefix: "",
			router: HashRouter,
			debug: false,
			start: "/top/home",
			views: {
				refresh: Refresh,
				logout: Logout
			}
		}
		super({ ...defaults, ...config })
	}
}
webix.ready(() => {
	const app = new App()
	app.use(plugins.Locale, { storage: webix.storage.local })
	const lang = webix.storage.local.get("lang")
	if (lang == "vi") webix.i18n.setLocale("vi-VN")
	app.attachEvent("app:guard", (url, view, nav) => {
		if (url !== "/login") {
			if (!(use_adfs() || isUsingAzureAD())) {
				if (!isValidSession()) nav.redirect = "/login"
			} else {
				if (!isValidSession()) {
					if (String(url).includes("/login")) {
						nav.redirect = url
					}
					else
						nav.redirect = "/login"
				}
			}
		}
	})
	webix.attachEvent("onBeforeAjax", (mode, url, data, req, headers, files, promise) => {
		if (!UPLOAD_URL.includes(url)) headers["Content-type"] = "application/json"
		const session = webix.storage.session.get("session")
		
		if (session) headers["Authorization"] = 'Bearer ' + session.token
	})
	webix.attachEvent("onAjaxError", (req) => {
        // if(!req.readyState || !req.status) webix.message("Lost connection", "error")
		if (req.status == 403) app.show("/top/home")
		if (req.status == 409) app.show("/login")
		if (req.responseType === "blob") {
			const reader = new FileReader()
			reader.onloadend = (e) => webix.message(reader.result, "error")
			reader.readAsText(req.response)
		}
		else if (req.responseText) webix.message(req.responseText, "error")
	})
	app.render()
})