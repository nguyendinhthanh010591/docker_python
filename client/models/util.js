export const size = 10
export const w = 800
export const h = 600
export const expire = 6000
export const ddate = 2592000000
export const FNC_LOGGING_EXT = "1" //1 - Có hiển thị trường mở rộng, 2 - Không hiển thị trường mở rộng
export const SEQ_LEN = 8
export const maxseq = parseInt("1".padEnd(SEQ_LEN + 1, "0")) - 1
export const all = { id: "*", value: "Tất cả" }
export const inv_del_status = [1]

const session = webix.storage.session.get("session")

export const msg_status = [
    { id: "0", value: "Lỗi" },
    { id: "1", value: "Thành công" }
]
export const MAX = 9007199254740992
export const PERIOD = [
    
	{ id: "N", value: "Ngày" },
	{ id: "T", value: "Tháng" },
	{ id: "Q", value: "Quý" },
]
const getSession = (name) => {
    let val
    const session = webix.storage.session.get("session")
    if (session) val = session[name]
    return val
}

const ent = getSession("ent"), role = getSession("role"), mst = getSession("mst"), fn = getSession("fn"), ou = getSession("ou"), taxc = getSession("taxc"), serial_grant = getSession("serial_grant"), serial_usr_grant = getSession("serial_usr_grant"), ldap_private = getSession("ldap_private"), uid = getSession("uid"), org_edit = getSession("ORG_EDIT"), config_chietkhau_col = getSession("config_chietkhau_col"), c0 = getSession("c0"), is_use_local_user = getSession("is_use_local_user"), localusr = getSession("localusr"), on = getSession("on"), org = getSession("org"), cattax = getSession("cattax"),
            ikind = getSession("ikind"), sstatus = getSession("sstatus"), ltype = getSession("ltype"), sitype = getSession("sitype"), funcname_logging = getSession("funcname_logging"), emailtype = getSession("emailtype"), trstatus = getSession("trstatus"), errtatus = getSession("errtatus"), paymethod = getSession("paymethod"), mailstatus = getSession("mailstatus"), smsstatus = getSession("smsstatus"), lsd = getSession("lsd"), lgd = getSession("lgd"), itype = getSession("itype"), grindfconf = getSession("grindfconf"), inv_cqt_status = getSession("inv_cqt_status"), wrongnotice_cqt_status = getSession("wrongnotice_cqt_status"),
            degree_config = getSession("degree_config"), statement = getSession("statement"), invtype = getSession("invtype"), stype = getSession("stype"), fitype = getSession("fitype"), dtsendtype = getSession("dtsendtype"), hascodelist = getSession("hascodelist"), istatustvan = getSession("istatustvan"), regtype = getSession("regtype"), reg_act = getSession("reg_act"), sign_type = getSession("sign_type"), sc = getSession("sc"), sendtype = getSession("sendtype"), statement_cqt_status = getSession("statement_cqt_status"), stmstatus = getSession("stmstatus"), goodstype = getSession("goodstype"), istatusbth = getSession("istatusbth"), noti_taxtype_option = getSession("noti_taxtype_option"), noti_type_combo = getSession("noti_type_combo"), status_cqt = getSession("status_cqt"), type_ref_combo = getSession("type_ref_combo"), type_th = getSession("type_th") 
export const KCT = "Không chịu thuế"
export const KKK = (ent == "vcm") ? "Không tính thuế" : ((ent == "yusen") ? "Không kê khai nộp thuế" : ((ent == "fhs") ? "" : "Không kê khai nộp thuế"))
export const KHAC = "Khác"
const vrnList = [
	{ id: "10%", value: "10%" },
	{ id: "8%", value: "8%" },
	{ id: "5%", value: "5%" },
	{ id: "0%", value: "0%" },
	{ id: KCT, value: KCT },
	{ id: KKK, value: KKK },
	{ id: KHAC, value: KHAC }
]
const vat = [
	{ id: "10", value: "10%" },
	{ id: "8", value: "8%" },
	{ id: "5", value: "5%" },
	{ id: "0", value: "0%" },
	{ id: "-1", value: KCT },
	{ id: "-2", value: KKK }
]
export const textn2 = {
	edit: v => {
		return webix.Number.format(v, gn2)
	},
	parse: v => {
		return webix.Number.parse(v, gn2)
	}
}
export const vrt2vrn = (vrn) => {
	const nVrn = Number(vrn)
	if (typeof nVrn == "number") {
		if (nVrn == -1) return KCT
		else if (nVrn == -2) return KKK
		else if ([0, 5, 8, 10].includes(nVrn)) return `${nVrn}%`
		else return KHAC
	}
}
export const vrn2vrt = (vrn) => {
	let item = vat.find(e => e.value == `${vrn}`)
	if (item) return `${Number(item.id)}`
}
export const gridnauto = {
	format: v => {
		return webix.Number.format(v, { groupDelimiter: ".", groupSize: 3, decimalDelimiter: ",", decimalSize: v % 1 ? `${v}`.split(".")[1].length : 0 })
	},
	editParse: v => {
		return webix.Number.parse(v, gn2)
	},
	editFormat: v => {
		return webix.Number.format(v, gn2)
	}
}
export const VRNLIST = vrnList
export const STATUS = session ? session.status : ""
export const TAX = session ? session.tax : ""
export const ENT = ent
export const ROLE = role
export const MST = mst
export const FN = fn
export const ON = on
export const ORG_ON = on
export const OU = ou
export const C0 = c0
export const TAXC = taxc
export const SERIAL_GRANT = serial_grant
export const SERIAL_USR_GRANT = serial_usr_grant
export const LDAP_PRIVATE = ldap_private
export const UID = uid
export const ORG_EDIT = org_edit
export const CONFIG_CHIETKHAU_COL = config_chietkhau_col
export const IS_USE_LOCAL_USER = is_use_local_user
export const LOCALUSR = localusr
export const CATTAX = cattax
export const NUMBER_DECIMAL_FORMAT = grindfconf||0
export const SESS_STATEMENT_HASCODE = statement ? statement.hascode : 0 //Co ma hay khong co ma
export const SESS_STATEMENT_SENTYPE = statement && statement.dtsendtype.includes("listinv") ? 1 : 0 //1 bth 0 chi tiet
export const DEGREE_CONFIG = degree_config ? degree_config : "119" //Dung nghi dinh nao
export const SIGN_TYPE = sign_type //Kieu ký so
export const SC = sc ? sc : "0" //Co tinh phi hay khong
export const ORG_TAXO = org ? org.taxo : ""
export const ORG_TAXONAME = org ? org.taxoname : ""
export const PLACE = org ? org.place : ""
export function bearer() {
    const session = webix.storage.session.get("session")
    if (session && session.token) return `Bearer ${session.token}`
    else return ''

}
const nf3 = { groupDelimiter: ".", groupSize: 3, decimalDelimiter: ",", decimalSize: (ent == "hdb" || ent == "fhs") ? 2 : 3 }
const nfconf = { groupDelimiter: ".", groupSize: 3, decimalDelimiter: ",", decimalSize: grindfconf }
const nf4Rate = { groupDelimiter: "", groupSize: 2, decimalDelimiter: ",", decimalSize: 4 }
const nf4 = { groupDelimiter: "", groupSize: 0, decimalDelimiter: "", decimalSize: 0 }
const nf4v = { groupDelimiter: ".", groupSize: 3, decimalDelimiter: "", decimalSize: 0 }
export const LinkedInputs = {
    $init: function (config) {
        let master = $$(config.master), list
        master.attachEvent("onChange", newv => {
            this.setValue()
            list = this.getList()
            list.clearAll()
            if (newv) list.load(config.dependentUrl + newv)
        })
    }
}

export const LinkedInputs2 = {
    $init: function (config) {
        let master = $$(config.master), list
        master.attachEvent("onChange", newv => {
            this.setValue("*")
            list = this.getList()
            list.clearAll()
            if (newv && newv != "*") list.load(config.dependentUrl + newv)
            list.add(all, 0)
        })
    }
}
export const LinkedInputs0 = {
    $init: function (config) {
        let master = $$(config.master), list
        master.attachEvent("onChange", newv => {
            let me = this
            me.setValue()
            list = me.getList()
            list.clearAll()
            if (newv) {
                list.load(config.dependentUrl + newv).then(data => {
                    let arr = data.json()
                    //if (arr.length > 0) me.setValue(arr[0].id)
                })
            }
        })
    }
}


export const LinkedInputs3 = {
    $init: function (config) {
        let master = $$(config.master), list
        master.attachEvent("onChange", newv => {
            let me = this
            me.setValue()
            list = me.getList()
            list.clearAll()
            if (newv) {
                list.load(config.dependentUrl + newv).then(data => {
                    let arr = data.json()
                    if (arr.length > 0) me.setValue(arr[0].id)
                })
            }
        })
    }
}
export const LinkedInputsOUSERIAL = {
    $init: function (config) {
        let master = $$(config.master), master1 = $$(config.master1), list

        master.attachEvent("onChange", newv => {
            let me = this
            me.setValue()
            list = me.getList()
            list.clearAll()
            if (newv) {
                list.load(config.dependentUrl + newv + "&ou=" + $$("iou").getValue()).then(data => {
                    let arr = data.json()
                    if (arr.length > 0) me.setValue(arr[0].id)
                })
            }
        })
    }
}

export const LinkedInputsTH = {
	$init: function (config) {
		let master = $$(config.master), list
		master.attachEvent("onChange", newv => {
			let me = this
			//this.setValue("*")
			list = this.getList()
			list.clearAll()
			if (newv && newv != "*") list.load(config.dependentUrl + newv).then(data => {
				let arr = data.json()
				if (arr.length > 0) me.setValue(arr[0].id)
			})
			//list.add(all, 0)
		})
	}
}

export function filter(obj, filter) {
    //return obj.value.toLowerCase().indexOf(filter.toLowerCase()) != -1
    return obj.value.toLowerCase().includes(filter.toLowerCase())
}

export function d2s(str) {
    if (str) return webix.i18n.dateFormatStr(new Date(str))
    else return ""
}
export function t2s(str) {
    if (str) return webix.i18n.fullDateFormatStr(new Date(str))
    else return ""
}
export function n2s(num, curr) {
    return webix.Number.format(num, { groupDelimiter: " ", groupSize: 3, decimalDelimiter: ".", decimalSize: (curr == "VND" ? 0 : 2) })
}

export const SMTP = [{ id: "GMAIL", value: "GMAIL" }, { id: "SMTP", value: "SMTP" }]
export const PORT = [{ id: 25, value: 25 }, { id: 465, value: 465 }, { id: 587, value: 587 }, { id: 2525, value: 2525 }]

export const LANGS = [{ id: "en", value: "en" }, { id: "vi", value: "vi" }]
export const SKINS = [
    { id: "flat-default", value: "Flat" },
    { id: "material-default", value: "Material" },
    { id: "contrast-default", value: "Contrast" }
]

export const RPP = [
    { id: 10, value: 10 },
    { id: 50, value: 50 },
    { id: 100, value: 100 },
    { id: 500, value: 500 },
    { id: 2000, value: 2000 },
]
export const RPP2 = [
    { id: 100, value: 100 },
    { id: 500, value: 500 },
    { id: 1000, value: 1000 }

]

export const SSTATUS = (app) => {
    const _ = app.getService("locale")._
    let arr = []
    for (let t of sstatus) {
        arr.push({ id: t.id, value: _(t.value) })
    }
    return arr
}

const getSUSTATUS = (app) => {
    let arr = SSTATUS(app)
    for (const s of []) {
        if (s.id != "3") arr.push(s)
    }
    return arr
}

export const SUSTATUS = (app) => { return getSUSTATUS(app) }

export const CARR = ["c0", "c1", "c2", "c3", "c4", "c5", "c6", "c7", "c8", "c9"]
export const AZ = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ"]
export const KM = "Khuyến mãi"
export const CK = "Chiết khấu"
export const MT = "Mô tả"
export const KMMT = [KM, MT]
export const LTYPE = (app) => {
    const _ = app.getService("locale")._
    let arr = []
    // switch (ent) {
    //     case "vib":
    //         arr = [
    //             { id: KM, value: _("inv_kmai") },
    //             { id: CK, value: _("inv_chkhau") }
    //         ]
    //         break
    //     default:
    //         arr = [
    //             { id: KM, value: _("inv_kmai") },
    //             { id: CK, value: _("inv_chkhau") },
    //             { id: MT, value: _("inv_mota") }
    //         ]
    // }
    for (let t of ltype) {
        arr.push({ id: t.id, value: _(t.value) })
    }
    return arr
}

export const ITEM_TYPE = (app) => {
    const _ = app.getService("locale")._
    let arr
    switch (ent) {
        case "vib":
            arr = [
                { id: "KM", value: _("inv_kmai") },
                { id: "CK", value: _("inv_chkhau") }
            ]
            break
        default:
            arr = [
                { id: "KM", value: _("inv_kmai") },
                { id: "CK", value: _("inv_chkhau") },
                { id: "MT", value: _("inv_mota") }
            ]
    }
    return arr
}

const GDR = "https://drive.google.com/drive/folders/"

export const HDSD = {
    system: `${GDR}1bK9sLRB-1-tvC-t5yffMACJFGIQHaNlT`,
    systemhdb: `${GDR}1qFEZ8I9wmlhqvYxZnEQQGPfA45A1yiL1`,
    usb: `${GDR}1QWGOEvLIWtjhPvo7ZoJ3SnUTrVLdO-ul`,
    fptca: `${GDR}1L-wNnNPK1ceAQaApo_0spMcBJ_1Tqysj`,
    mail: `${GDR}1qF4cQc53POZdf-LZ-WBUoHLMyihIkvKD`,
    exe: `https://drive.google.com/open?id=1rhRJlJImcKR-s3N1bFNEbmc4YRio2kkW`,
    video: "https://www.youtube.com/channel/UCV44VT0Ez76yk19qh40NjWA/videos"
}
export const SITYPE = (app) => {
    const _ = app.getService("locale")._
    // let arr = [
    //     { id: 1, value: _("serial_type_manual") },
    //     { id: 2, value: _("serial_type_integ") },
    //     { id: 3, value: _("serial_type_man_inte") },
    // ]
    let arr = []
    for (let t of sitype) {
        arr.push({ id: t.id, value: _(t.value) })
    }
    return arr
}
export const BTH_CQT_STATUS =(app) => {
    const _ = app.getService("locale")._
    let arr = []
    arr.push({ id: "0", value:  _("cqt_wait_send") })
    arr.push({ id: "1", value:  _("cqt_sent") })
    arr.push({ id: "2", value:  _("cqt_sent_fail") })
    
    arr.push({ id: "12", value:  _("wno_valid") })
    arr.push({ id: "131", value:  _("process_bth") })
    arr.push({ id: "13", value:  _("wno_invalid") })
   
    return arr
}
const getIkind = (app) => {
    const _ = app.getService("locale")._
    let arr = []
    // switch (ent) {
    //     case "hlv":
    //         arr = [{ id: "1", value: _("replace") }, { id: "3", value: _("replaced") }, { id: "4", value: _("adjusted") }]
    //         break
    //     case "ssi":
    //         arr = [{ id: "1", value: _("replace") }, { id: "3", value: _("replaced") }]
    //         break
    //     default:
    //         arr = [{ id: "1", value: _("replace") }, { id: "2", value: _("adjust") }, { id: "3", value: _("replaced") }, { id: "4", value: _("adjusted") }]
    // }
    for (let t of ikind) {
        if (t.status) arr.push({ id: t.id, value: _(t.value) })
    }
    return arr
}
export const IKIND = (app) => { return getIkind(app) }

const getIkind123 = (app) => {
    const _ = app.getService("locale")._
    let arr = []
    // switch (ent) {
    //     case "hlv":
    //         arr = [{ id: "1", value: _("replace") }, { id: "3", value: _("replaced") }, { id: "4", value: _("adjusted") }]
    //         break
    //     case "ssi":
    //         arr = [{ id: "1", value: _("replace") }, { id: "3", value: _("replaced") }]
    //         break
    //     default:
    //         arr = [{ id: "1", value: _("replace") }, { id: "2", value: _("adjust") }, { id: "3", value: _("replaced") }, { id: "4", value: _("adjusted") }]
    // }
    for (let t of ikind) {
        if (t.status123) arr.push({ id: t.id, value: _(t.value) })
    }
    return arr
}
export const IKIND123 = (app) => { return getIkind123(app) }

const _01GTKT = (app) => {
    const _ = app.getService("locale")._
    return { id: "01GTKT", value: _("inv_01gtkt") }
}
const _02GTTT = (app) => {
    const _ = app.getService("locale")._
    return { id: "02GTTT", value: _("inv_02gttt") }
}
const _03XKNB = (app) => {
    const _ = app.getService("locale")._
    return { id: "03XKNB", value: _("inv_03xknb") }
}
//,{ id: "07KPTQ", value: "Hóa đơn bán hàng khu phi thuế quan" }
//,{ id: "04HGDL", value: "Phiếu XK gửi bán hàng đại lý" }
//,{ id: "06TKHT", value: "HĐ kiêm TK hoàn thuế" }
//,{ id: "00KHAC", value: "HĐ Chứng từ khác" }
const getItype = (app) => {
    const _ = (app) ? app.getService("locale")._ : {}
    let arr = []
    // const _01GTKT_ = { id: "01GTKT", value: (app) ? _("inv_01gtkt") : "" }, _02GTTT_ = { id: "02GTTT", value: (app) ? _("inv_02gttt") : "" }, _03XKNB_ = { id: "03XKNB", value: (app) ? _("inv_03xknb") : "" }
    // switch (ent) {
    //     case "hdb":
    //         arr = [_01GTKT_, _02GTTT_]
    //         break
    //     case "tlg":
    //         arr = [_01GTKT_, _03XKNB_]
    //         break
    //     case "fhs":
    //         arr = [_01GTKT_, _03XKNB_]
    //         break
    //     case "fbc":
    //         arr = [_01GTKT_, _03XKNB_]
    //         break
    //     case "vcm":
    //         arr = [_01GTKT_, _03XKNB_]
    //         break
    //     case "aia":
    //         arr = [_01GTKT_]
    //         break
    //     case "scb":
    //         arr = [_01GTKT_]
    //         break
    //     case "sgr":
    //         arr = [_01GTKT_, _03XKNB_]
    //         break
    //     case "bvb":
    //         arr = [_01GTKT_]
    //         break
    //     default:
    //         arr = [_01GTKT_, _02GTTT_, _03XKNB_]
    // }
    for (let t of itype) {
        arr.push({ id: t.id, value: _(t.valueclient) })
    }
    return arr
}
export const ITYPE = (app) => { return getItype(app) }
export const XTYPE = (app) => {
    if (ENT == "sgr" || ENT == "bvb") {
        const _ = app.getService("locale")._
        return ITYPE(app)

    } else {
        const _ = app.getService("locale")._
        return [...ITYPE(app), { id: "00ORGS", value: _("catalog_customer") }]
    }
}
export const SRC_LOGGING = (app) => {
    const _ = app.getService("locale")._
    let arr = [
        { id: "APP", value: _("einvoice_app") },
        { id: "API", value: _("integration_app") }
    ]
    return arr
}
export const MESSAGE_STATUS = (app) => {
    const _ = app.getService("locale")._
    let arr = [
        { id: "0", value: _("syn_err") },
        { id: "1", value: _("syn_success") }
    ]
    return arr
}
export const FUNCNAME_LOGGING = (app) => {
    const _ = app.getService("locale")._
    // let arr = [//Danh sách cácc tác vụ ghi log
    //     // { id: "api", value: _("api_integration_app"), logenable: "Y", extfld: [], extfldlbl: [] },
    //     { id: "user_login", value: _("login_sys"), logenable: "Y", extfld: [], extfldlbl: [] },
    //     { id: "user_disable", value: _("cancel_nsd"), logenable: "Y", extfld: [], extfldlbl: [] },
    //     { id: "user_enable", value: _("activate_nsd"), logenable: "Y", extfld: [], extfldlbl: [] },
    //     { id: "user_update", value: _("upd_info_nsd"), logenable: "Y", extfld: ["id", "name"], extfldlbl: ["account", "fullname"] },
    //     { id: "user_insert", value: _("add_info_nsd"), logenable: "Y", extfld: ["id", "name"], extfldlbl: ["account", "fullname"] },
    //     { id: "user_get", value: _("get_info_nsd"), logenable: "Y", extfld: [], extfldlbl: [] },
    //     //Bổ sung thêm
    //     { id: "cat_del", value: _("cat_del"), logenable: "Y", extfld: [], extfldlbl: [] },
    //     { id: "cat_ins", value: _("cat_ins"), logenable: "Y", extfld: [], extfldlbl: [] },
    //     { id: "cat_upd", value: _("cat_upd"), logenable: "Y", extfld: [], extfldlbl: [] },
    //     { id: "exch_post", value: _("exch_post"), logenable: "Y", extfld: [], extfldlbl: [] },
    //     { id: "field_post", value: _("field_post"), logenable: "Y", extfld: [], extfldlbl: [] },
    //     { id: "gns_del", value: _("gns_del"), logenable: "Y", extfld: [], extfldlbl: [] },
    //     { id: "gns_upd", value: _("gns_upd"), logenable: "Y", extfld: [], extfldlbl: [] },
    //     { id: "gns_ins", value: _("gns_ins"), logenable: "Y", extfld: [], extfldlbl: [] },
    //     { id: "loc_post", value: _("loc_post"), logenable: "Y", extfld: [], extfldlbl: [] },

    //     { id: "group_user_insert", value: _("add_info_nsd_2_group"), logenable: "Y", extfld: [], extfldlbl: [] },
    //     { id: "data_user_insert", value: _("add_info_nsd_2_data"), logenable: "Y", extfld: [], extfldlbl: [] },
    //     { id: "inv_get", value: _("invoice_search"), logenable: "Y", extfld: [], extfldlbl: [] },
    //     { id: "inv_post", value: _("invoice_issue"), logenable: "Y", extfld: ["form", "serial", "seq", "c0"], extfldlbl: ["form", "serial", "seq"] },
    //     { id: "inv_put", value: _("edit_invoice"), logenable: "Y", extfld: ["form", "serial", "seq", "c0"], extfldlbl: ["form", "serial", "seq"] },
    //     { id: "inv_del", value: _("invoice_del_btn"), logenable: "Y", extfld: ["form", "serial", "seq", "c0"], extfldlbl: ["form", "serial", "seq"] },
    //     { id: "inv_seqpost", value: _("sequence_invoice"), logenable: "Y", extfld: ["form", "serial", "seq", "c0"], extfldlbl: ["form", "serial", "seq"] },
    //     { id: "inv_adj", value: _("adj_inv"), logenable: "Y", extfld: ["form", "serial", "seq", "c0"], extfldlbl: ["form", "serial", "seq"] },
    //     { id: "inv_apprall", value: _("invoice_approve"), logenable: "Y", extfld: ["form", "serial", "seq", "c0"], extfldlbl: ["form", "serial", "seq"] },
    //     { id: "inv_cancel", value: _("invoice_cancel"), logenable: "Y", extfld: ["form", "serial", "seq", "c0"], extfldlbl: ["form", "serial", "seq"] },
    //     { id: "inv_replace", value: _("rep_inv"), logenable: "Y", extfld: ["form", "serial", "seq", "c0"], extfldlbl: ["form", "serial", "seq"] },
    //     { id: "inv_istatus", value: _("convert_status_invoice"), logenable: "Y", extfld: ["form", "serial", "seq", "c0"], extfldlbl: ["form", "serial", "seq"] },
    //     { id: "inv_signget", value: _("sign_digital_inv"), logenable: "Y", extfld: ["form", "serial", "seq", "c0"], extfldlbl: ["form", "serial", "seq"] },
    //     { id: "xls_upl", value: _("upload_excel_file_inv"), logenable: "Y", extfld: [], extfldlbl: [] },
    //     { id: "ser_ins", value: _("create_release_notice"), logenable: "Y", extfld: ["taxc", "form", "serial"], extfldlbl: ["taxcode", "form", "serial"] },
    //     { id: "ser_del", value: _("del_release_notice"), logenable: "Y", extfld: ["taxc", "form", "serial"], extfldlbl: ["taxcode", "form", "serial"] },
    //     { id: "ser_apr", value: _("approve_release_notice"), logenable: "Y", extfld: ["taxc", "form", "serial"], extfldlbl: ["taxcode", "form", "serial"] },
    //     { id: "ser_cancel", value: _("cancel_release_notice"), logenable: "Y", extfld: ["taxc", "form", "serial"], extfldlbl: ["taxcode", "form", "serial"] },
    //     { id: "ser_grant", value: _("assign_release_notice"), logenable: "Y", extfld: [], extfldlbl: [] },
    //     { id: "org_ins", value: _("add_info_customer"), logenable: "Y", extfld: ["name"], extfldlbl: ["customer_name"] },
    //     { id: "org_upd", value: _("upd_info_customer"), logenable: "Y", extfld: ["name"], extfldlbl: ["customer_name"] },
    //     { id: "org_del", value: _("del_info_customer"), logenable: "Y", extfld: ["name"], extfldlbl: ["customer_name"] },
    //     { id: "ou_ins", value: _("add_info_ou"), logenable: "Y", extfld: ["name", "mst"], extfldlbl: ["customer_name", "taxcode"] },
    //     { id: "ou_upd", value: _("upd_info_ou"), logenable: "Y", extfld: ["name", "mst"], extfldlbl: ["customer_name", "taxcode"] },
    //     { id: "ou_del", value: _("del_info_ou"), logenable: "Y", extfld: ["name", "mst"], extfldlbl: ["customer_name", "taxcode"] },
    //     { id: "group_disable", value: _("cancel_group"), logenable: "Y", extfld: [], extfldlbl: [] },
    //     { id: "group_enable", value: _("activate_group"), logenable: "Y", extfld: [], extfldlbl: [] },
    //     { id: "group_update", value: _("update_group"), logenable: "Y", extfld: ["id", "name"], extfldlbl: ["id", "name"] },
    //     { id: "group_insert", value: _("add_info_group"), logenable:"Y", extfld: ["id", "name"], extfldlbl: ["id", "name"]},
    //     { id: "group_rolemember", value: _("role_group"), logenable:"Y", extfld: ["id", "name"], extfldlbl: []},
    //     { id: "group_del_mbr", value: _("role_many_groups"), logenable:"Y", extfld: ["id", "name"], extfldlbl: []},
    //     // { id: "user_dep_mbr", value: _("role_user_dep"), logenable:"Y", extfld: ["id", "name"], extfldlbl: ["id", "name"] }

    // ]
    // if (ENT == "scb") {
    //     arr.push({ id: "user_dep_mbr", value: _("role_user_dep"), logenable: "Y", extfld: ["id", "name"], extfldlbl: ["id", "name"] })
    // }
    let arret = []
    // switch (ent) {
    //     case "fhs":
    //         for (const item of arr) {
    //             if (!["api", "user_login", "inv_get"].includes(item.id)) arret.push(item)
    //         }
    //         break
    //     case "fbc":
    //         for (const item of arr) {
    //             if (!["api", "user_login", "inv_get"].includes(item.id)) arret.push(item)
    //         }
    //         break
    //     case "vhc":
    //         for (const item of arr) {
    //             arret.push(item)
    //         }
    //         arret.push({ id: "inv_date_change", value: _("edit_env_date"), logenable: "Y", extfld: [], extfldlbl: [] })
    //         arret.push({ id: "inv_status_change", value: _("edit_env_status"), logenable: "Y", extfld: [], extfldlbl: [] })
    //         break
    //     case "bahuan":
    //         for (const item of arr) {
    //             if (!["ser_grant"].includes(item.id)) arret.push(item)
    //         }
    //         break
    //     default:
    //         arret = arr
    //         break
    // }
    for (let t of funcname_logging) {
        arret.push({ id: t.id, value: _(t.valueclient), logenable: t.logenable, extfld: t.extfld, extfldlbl: t.extfldlbl })
    }
    return arret
}

// export const ISTATUS = [
//     { id: "1", value: "Chờ cấp số" },
//     { id: "2", value: "Chờ duyệt" },
//     { id: "3", value: "Đã duyệt" },
//     { id: "4", value: "Đã hủy" },
//     { id: "5", value: "Đã gửi" },
//     { id: "6", value: "Chờ hủy" },
// ]

export const EMAILTYPE = (app) => {
    const _ = app.getService("locale")._
    let arr = []
    // switch (ent) {
    //     case "vib":
    //         arr = [
    //             { id: "-1", value: _("emailtype_normal") },
    //             { id: "1", value: _("emailtype_user_resetpass") },
    //             { id: "2", value: _("emailtype_cus_resetpass") },
    //             { id: "4", value: _("emailtype_cancel") },
    //             { id: "-2", value: _("emailtype_pass") },
    //             { id: "5", value: _("emailtype_resetpass") },
    //         ]
    //         break
    //     default:
    //         arr = [
    //             { id: "-1", value: _("emailtype_normal") },
    //             { id: "4", value: _("emailtype_cancel") },
    //             { id: "-2", value: _("emailtype_pass") },
    //             { id: "1", value: _("emailtype_user_create") },
    //             { id: "2", value: _("emailtype_cust_resetpass") },
    //             { id: "5", value: _("emailtype_user_resetpass") },
    //         ]

    // }
    for (let t of emailtype) {
        arr.push({ id: t.id, value: _(t.value) })
    }
    return arr
}
export const TRSTATUS = (app) => {
    const _ = app.getService("locale")._
    let arr = []
    // switch (ent) {
    //     case "mzh":
    //         arr = [
    //             { id: "0", value: _("trstatus_0") },
    //             { id: "1", value: _("trstatus_1") },
    //             { id: "4", value: _("trstatus_4") },
    //             { id: "2", value: _("trstatus_2") },
    //             { id: "3", value: _("trstatus_3") }
    //         ]
    //         break
    //     case "scb":
    //         arr = [
    //             { id: "0", value: _("scbtrstatus_0") },
    //             { id: "1", value: _("scbtrstatus_1") },
    //             { id: "4", value: _("scbtrstatus_4") },
    //             { id: "2", value: _("scbtrstatus_2") },
    //             { id: "3", value: _("scbtrstatus_3") }
    //         ]
    //         break

    // }
    for (let t of trstatus) {
        arr.push({ id: t.id, value: _(t.value) })
    }
    return arr
}
const getINV_CQT_STATUS = (app) => {//Danh sach cap ma hay khong
    const _ = app.getService("locale")._
    let arr = []

    for (let t of inv_cqt_status) {
        arr.push({ id: t.id, value: _(t.valueclient) })
    }
    return arr
}

export const INV_CQT_STATUS = (app) => { return getINV_CQT_STATUS(app) }

const getWRONGNOTICE_CQT_STATUS = (app) => {//Danh sach cap ma hay khong
    const _ = app.getService("locale")._
    let arr = []

    for (let t of wrongnotice_cqt_status) {
        arr.push({ id: t.id, value: _(t.valueclient) })
    }
    return arr
}
export const WRONGNOTICE_CQT_STATUS = (app) => { return getWRONGNOTICE_CQT_STATUS(app) }

export const ERRTATUS = (app) => {
    const _ = app.getService("locale")._
    let arr = []
    // arr = [
    //     { id: "*", value: _("all") },
    //     { id: "0", value: _("err_custom") },
    //     { id: "1", value: _("err_data") },
    //     { id: "2", value: _("err_data_custom") }

    // ]
    for (let t of errtatus) {
        arr.push({ id: t.id, value: _(t.value) })
    }
    return arr
}
const getStatus = (app) => {
    const _ = app.getService("locale")._
    let arr
    arr = [{ id: "2", value: _("invstatus_2") }, { id: "3", value: _("invstatus_3") }, { id: "4", value: _("invstatus_4") }, { id: "6", value: _("invstatus_6") }]
    return arr
}
export const CKSTATUS = (app) => { return getStatus(app) }

const getIstatus3 = (app) => {
    const _ = app.getService("locale")._
    let arr
    switch (ent) {
        case "vcm":
            arr = [{ id: "1", value: _("invstatus_vcm_1") }, { id: "3", value: _("invstatus_vcm_3") }, { id: "4", value: _("invstatus_4") }]
            break
        case "vib":
            arr = [{ id: "1", value: _("invstatus_1") }, { id: "2", value: _("invstatus_2") }, { id: "3", value: _("invstatus_3") }, { id: "4", value: _("invstatus_9") },
            { id: "6", value: _("invstatus_6") }, { id: "7", value: _("invstatus_8") }]
            break
        // case "baca":
        //     arr = [{ id: "2", value: _("invstatus_2") }, { id: "3", value: _("invstatus_3") }, { id: "4", value: _("invstatus_4") }, { id: "6", value: _("invstatus_6") }]
        //     break    
        default:
            arr = [{ id: "1", value: _("invstatus_1") }, { id: "2", value: _("invstatus_2") }, { id: "3", value: _("invstatus_3") }, { id: "4", value: _("invstatus_4") }, { id: "5", value: _("invstatus_5") },
            { id: "6", value: _("invstatus_6") }]
    }
    return arr
}
export const ISTATUS = (app) => { return getIstatus3(app) }
const getIstatus2 = (app) => {
    const _ = app.getService("locale")._
    let arr
    switch (ent) {
        case "ssi":
            arr = [{ id: "3", value: _("invstatus_3") }, { id: "6", value: _("invstatus_6") }]
            break
        case "vcm":
            arr = [{ id: "3", value: _("invstatus_vcm_3") }]
            break
        case "hdb":
            arr = [{ id: "3", value: _("invstatus_3") }, { id: "6", value: _("invstatus_6") }]
            break
        case "hlv":
            arr = [{ id: "3", value: _("invstatus_3") }, { id: "6", value: _("invstatus_6") }]
            break
        case "tlg":
            arr = [{ id: "3", value: _("invstatus_3") }, { id: "6", value: _("invstatus_6") }]
            break
        case "mzh":
            arr = [{ id: "3", value: _("invstatus_3") }, { id: "6", value: _("invstatus_6") }]
            break
        case "scb":
            arr = [{ id: "6", value: _("invstatus_6") }]
            break
        case "bvb":
            arr = [{ id: "3", value: _("invstatus_3") }, { id: "6", value: _("invstatus_6") }]
            break
        default:
            arr = [{ id: "3", value: _("invstatus_3") }, { id: "2", value: _("invstatus_2") }, { id: "6", value: _("invstatus_6") }]
    }
    return arr
}
export const ISTATUS2 = (app) => { return getIstatus2(app) }

const getPAYMETHOD = (app) => {
    const _ = app.getService("locale")._
    let arr = []
    // switch (ent) {
    //     case "vcm":
    //         arr = [{ id: "CK", value: _("paymethod_ck") },
    //         { id: "TM", value: _("paymethod_tm") },
    //         { id: "TM/CK/Khác", value: _("paymethod_vcm_ck_tm") }]
    //         break
    //     case "lge":
    //         arr = [
    //             { id: "CK", value: _("paymethod_ck") },
    //             { id: "TM", value: _("paymethod_tm") },
    //             { id: "FOC", value: _("paymethod_foc") },
    //             { id: "TM/CK", value: _("paymethod_ck_tm") },
    //             { id: "DTCN", value: _("paymethod_dtcn") }
    //         ]
    //         break
    //     default:
    //         arr = [{ id: "CK", value: _("paymethod_ck") },
    //         { id: "TM", value: _("paymethod_tm") },
    //         { id: "TM/CK", value: _("paymethod_ck_tm") },
    //         { id: "DTCN", value: _("paymethod_dtcn") }]
    //         break
    // }
    for (let t of paymethod) {
        arr.push({ id: t.id, value: _(t.valueclient) })
    }
    return arr
}

export const PAYMETHOD = (app) => { return getPAYMETHOD(app) }
//Trạng thái gửi mail
const getMAILSTATUS = (app) => {
    const _ = app.getService("locale")._
    let arr = []
    // arr = [{ id: "*", value: _("all") }, { id: "0", value: _("mail_notsent_status") },
    // { id: "1", value: _("mail_sent_status") }]
    for (let t of mailstatus) {
        arr.push({ id: t.id, value: _(t.value) })
    }
    return arr
}

export const MAILSTATUS = (app) => { return getMAILSTATUS(app) }

//Trạng thái gửi sms
const getSMSSTATUS = (app) => {
    const _ = app.getService("locale")._
    let arr = []
    // arr = [{ id: "*", value: _("all") }, { id: "0", value: _("sms_notsent_status") },
    // { id: "1", value: _("sms_sent_ok_status") },
    // { id: "-1", value: _("sms_sent_notok_status") }]
    for (let t of smsstatus) {
        arr.push({ id: t.id, value: _(t.value) })
    }
    return arr
}

export const SMSSTATUS = (app) => { return getSMSSTATUS(app) }

const getVAT = (app) => {
    const _ = app.getService("locale")._
    let arr = []
    for (let t of cattax) {
        if (t.status == 1) arr.push({ id: t.vatCode, value: _(t.vrnc) })
    }
    return arr
}
const findDFS = (arr, pid) => {
    for (let obj of arr || []) {
        if (obj.id_key == pid) return obj
        const o = findDFS(obj.data, pid)
        if (o) return o
    }
}
export const callCloudTvanApi = (arrIncs, type) => {
	let paramTvan = { 
		ids: arrIncs,
		type
	}
	webix.ajax().post("api/app/cloudtvan", paramTvan).then(result => {
		console.log("Call successfullly api tvan", arrIncs, "type -", type)
	}).catch(e => {
		console.log("Error api tvan", arrIncs, "type -",type)
	})
}
export const GETMENU = (app) => {
    let val
    const session = webix.storage.session.get("session")
    if (session) val = session["menu_cliet"]
    const _ = app.getService("locale")._
    let arr = [],menu_detail,obj
    for (let menu of val) {
        if (menu.menu_detail) {
            menu_detail = JSON.parse(menu.menu_detail)
            let menuview = {icon: menu_detail.icon,id_key:menu.id ,value: _(menu_detail.value) }
            if (menu_detail.id) menuview.id = menu_detail.id
            if (!menu.pid) arr.push(menuview)
            else {
                obj = findDFS(arr, menu.pid)
                    if (obj) {
                        if (!obj.hasOwnProperty('data')) obj.data = []
                        obj.data.push({ id: menu_detail.id, icon: menu_detail.icon, value: _(menu_detail.value) })
                    }
            }
        }
        
    }
    return arr
}
export const VAT = (app) => { return getVAT(app) }

//Hóa đơn đầu vào
const getVAT1 = (app) => {
    const _ = app.getService("locale")._
    let arr = []
    for (let t of cattax) {
        arr.push({ id: t.id, value: _(t.vrnc) })
    }
    return arr
}

export const VAT1 = (app) => { return getVAT(app) }

const getSTTG = (app) => {
    const _ = app.getService("locale")._
    let arr = []

    // switch (ent) {
    //     case "mzh":
    //         arr = [
    //             { id: "0", value: _("trstatus_0") },
    //             { id: "1", value: _("trstatus_1") },
    //             { id: "4", value: _("trstatus_4") },
    //             { id: "2", value: _("trstatus_2") },
    //             { id: "3", value: _("trstatus_3") }
    //         ]
    //         break
    //     case "scb":
    //         arr = [
    //             { id: "0", value: _("scbtrstatus_0") },
    //             { id: "1", value: _("scbtrstatus_1") },
    //             { id: "4", value: _("scbtrstatus_4") },
    //             { id: "2", value: _("scbtrstatus_2") },
    //             { id: "3", value: _("scbtrstatus_3") }
    //         ]
    //         break

    // }
    for (let t of lsd) {
        arr.push({ id: t.id, value: _(t.value) })
    }
    return arr
}

export const LSD = (app) => { return getSTTG(app) }

const getLGD = (app) => {
    const _ = app.getService("locale")._
    let arr = []

    // switch (ent) {
    //     case "mzh":
    //         arr = [
    //             { id: "0", value: _("fee") },
    //             { id: "1", value: _("rate") }
    //         ]
    //         break
    //     case "scb":
    //         arr = [
    //             { id: "*", value: _("all") },
    //             { id: "C400", value: "C400" },
    //             { id: "EBBS", value: "EBBS" },
    //             { id: "IMEX", value: "IMEX" },
    //             { id: "RLS", value: "RLS" },
    //             { id: "OTP", value: "OTP" },
    //             { id: "OAF", value: "OAF" },
    //             { id: "OPIC", value: "OPIC" },
    //             { id: "SECURE", value: "SECURE" },
    //         ]
    //         break

    // }
    for (let t of lgd) {
        arr.push({ id: t.id, value: _(t.value) })
    }
    return arr
}

export const LGD = (app) => { return getLGD(app) }
//Hóa đơn đầu vào

const getINVTYPE = (app) => {
    const _ = app.getService("locale")._
    let arr = []

    // switch (ent) {
    //     case "mzh":
    //         arr = [
    //             { id: "0", value: _("fee") },
    //             { id: "1", value: _("rate") }
    //         ]
    //         break
    //     case "scb":
    //         arr = [
    //             { id: "*", value: _("all") },
    //             { id: "C400", value: "C400" },
    //             { id: "EBBS", value: "EBBS" },
    //             { id: "IMEX", value: "IMEX" },
    //             { id: "RLS", value: "RLS" },
    //             { id: "OTP", value: "OTP" },
    //             { id: "OAF", value: "OAF" },
    //             { id: "OPIC", value: "OPIC" },
    //             { id: "SECURE", value: "SECURE" },
    //         ]
    //         break

    // }
    for (let t of invtype) {
        arr.push({ id: t.id, value: _(t.valueclient) })
    }
    return arr
}

export const INVTYPE = (app) => { return getINVTYPE(app) }

const getSTYPE = (app) => {
    const _ = app.getService("locale")._
    let arr = []

    // switch (ent) {
    //     case "mzh":
    //         arr = [
    //             { id: "0", value: _("fee") },
    //             { id: "1", value: _("rate") }
    //         ]
    //         break
    //     case "scb":
    //         arr = [
    //             { id: "*", value: _("all") },
    //             { id: "C400", value: "C400" },
    //             { id: "EBBS", value: "EBBS" },
    //             { id: "IMEX", value: "IMEX" },
    //             { id: "RLS", value: "RLS" },
    //             { id: "OTP", value: "OTP" },
    //             { id: "OAF", value: "OAF" },
    //             { id: "OPIC", value: "OPIC" },
    //             { id: "SECURE", value: "SECURE" },
    //         ]
    //         break

    // }
    for (let t of stype) {
        arr.push({ id: t.id, value: _(t.valueclient) })
    }
    return arr
}

export const SENDINVTYPE = (app) => { return getSTYPE(app) }

const getFITYPE = (app) => {//Danh sach loai hoa don day du
    const _ = app.getService("locale")._
    let arr = []

    for (let t of fitype) {
        arr.push({ id: t.id, value: _(t.valueclient) })
    }
    return arr
}

export const FITYPE = (app) => { return getFITYPE(app) }

const getDTSENDTYPE = (app) => {//Danh sach loai hoa don day du
    const _ = app.getService("locale")._
    let arr = []

    for (let t of dtsendtype) {
        if (t.status1) arr.push({ id: t.id, value: _(t.valueclient) })
    }
    return arr
}

export const DTSENDTYPE = (app) => { return getDTSENDTYPE(app) }

const getDTSENDTYPE2 = (app) => {//Danh sach loai hoa don day du
    const _ = app.getService("locale")._
    let arr = []

    for (let t of dtsendtype) {
        if (t.status2) arr.push({ id: t.id, value: _(t.valueclient) })
    }
    return arr
}

export const DTSENDTYPE2 = (app) => { return getDTSENDTYPE2(app) }

const getISTATUSTVAN = (app) => {//Danh sach loai hoa don day du
    const _ = app.getService("locale")._
    let arr = []

    for (let t of istatustvan) {
        arr.push({ id: t.id, value: _(t.valueclient) })
    }
    return arr
}

export const ISTATUSTVAN = (app) => { return getISTATUSTVAN(app) }

const getREGTYPE = (app) => {//Danh sach loai hoa don day du
    const _ = app.getService("locale")._
    let arr = []

    for (let t of regtype) {
        arr.push({ id: t.id, value: _(t.valueclient) })
    }
    return arr
}

export const REGTYPE = (app) => { return getREGTYPE(app) }

const getREG_ACT = (app) => {//Danh sach loai hoa don day du
    const _ = app.getService("locale")._
    let arr = []

    for (let t of reg_act) {
        arr.push({ id: t.id, value: _(t.valueclient) })
    }
    return arr
}

export const REG_ACT = (app) => { return getREG_ACT(app) }

const getHASCODELIST = (app) => {//Danh sach cap ma hay khong
    const _ = app.getService("locale")._
    let arr = []

    for (let t of hascodelist) {
        arr.push({ id: t.id, value: _(t.valueclient) })
    }
    return arr
}

export const HASCODELIST = (app) => { return getHASCODELIST(app) }

const getSENDTYPE = (app) => {//Danh sach cap ma hay khong
    const _ = app.getService("locale")._
    let arr = []

    for (let t of sendtype) {
        arr.push({ id: t.id, value: t.value })
    }
    return arr
}

export const SENDTYPE = (app) => { return getSENDTYPE(app) }
/*
export const STATEMENT_CQT_STATUS = [
	{id:0, value: "0"},
	{id:1, value: "1"},
	{id:2, value: "2"},
	{id:3, value: "3"},
	{id:4, value: "4"},
	{id:5, value: "5"},
	{id:6, value: "6"},
]
*/

const getSTATEMENT_CQT_STATUS = (app) => {//Danh sach cap ma hay khong
    const _ = app.getService("locale")._
    let arr = []
    for (let t of statement_cqt_status) {
        arr.push({ id: t.id, value: _(t.valueclient) })
    }
    return arr
}

export const STATEMENT_CQT_STATUS = (app) => { return getSTATEMENT_CQT_STATUS(app) }

const getSTMSTATUS = (app) => {//Danh sach cap ma hay khong
    const _ = app.getService("locale")._
    let arr = []

    for (let t of stmstatus) {
        arr.push({ id: t.id, value: _(t.valueclient) })
    }
    return arr
}

export const STMSTATUS = (app) => { return getSTMSTATUS(app) }

const getGOODSTYPE = (app) => {//Danh sach cap ma hay khong
    const _ = app.getService("locale")._
    let arr = []

    for (let t of goodstype) {
        arr.push({ id: t.id, value: _(t.valueclient) })
    }
    return arr
}

export const GOODSTYPE = (app) => { return getGOODSTYPE(app) }

const getISTATUSBTH = (app) => {//Danh sach cap ma hay khong
    const _ = app.getService("locale")._
    let arr = []

    for (let t of istatusbth) {
        arr.push({ id: t.id, value: _(t.valueclient) })
    }
    return arr
}

export const ISTATUSBTH = (app) => { return getISTATUSBTH(app) }

const getNOTI_TAXTYPE_OPTION = (app) => {//Danh sach cap ma hay khong
    const _ = app.getService("locale")._
    let arr = []

    for (let t of noti_taxtype_option) {
        arr.push({ id: t.id, value: _(t.valueclient) })
    }
    return arr
}

export const NOTI_TAXTYPE_OPTION = (app) => { return getNOTI_TAXTYPE_OPTION(app) }

const getNOTI_TYPE_COMBO = (app) => {//Danh sach cap ma hay khong
    const _ = app.getService("locale")._
    let arr = []

    for (let t of noti_type_combo) {
        arr.push({ id: t.id, value: _(t.valueclient) })
    }
    return arr
}

export const NOTI_TYPE_COMBO = (app) => { return getNOTI_TYPE_COMBO(app) }

const getSTATUS_CQT = (app) => {//Danh sach cap ma hay khong
    const _ = app.getService("locale")._
    let arr = []

    for (let t of status_cqt) {
        arr.push({ id: t.id, value: _(t.valueclient) })
    }
    return arr
}

export const STATUS_CQT = (app) => { return getSTATUS_CQT(app) }

const getTYPE_REF_COMBO = (app) => {//Danh sach cap ma hay khong
    const _ = app.getService("locale")._
    let arr = []

    for (let t of type_ref_combo) {
        arr.push({ id: t.id, value: _(t.valueclient) })
    }
    return arr
}

export const TYPE_REF_COMBO = (app) => { return getTYPE_REF_COMBO(app) }

const getTYPE_TH = (app) => {//Danh sach cap ma hay khong
    const _ = app.getService("locale")._
    let arr = []

    for (let t of type_th) {
        arr.push({ id: t.id, value: _(t.valueclient) })
    }
    return arr
}

export const TYPE_TH = (app) => { return getTYPE_TH(app) }

export const WN_CRE_WIN_BTNSAV_PURPOSE = {
	SEND_NOW: "SEND_NOW",
	GROUP: "GROUP",
	SAVE_WITH_INV: "SAVE_WITH_INV",
}

export const intf = {
    edit: v => {
        return webix.i18n.intFormat(v)
    },
    parse: v => {
        return webix.Number.parse(v, webix.i18n)
    }
}

export const textnf = {
    edit: v => {
        return webix.Number.format(v, webix.i18n)
    },
    parse: v => {
        return webix.Number.parse(v, webix.i18n)
    }
}
export const textnf3 = {
    edit: v => {
        return webix.Number.format(v, nf3)
    },
    parse: v => {
        return webix.Number.parse(v, nf3)
    }
}
export const textnfconf = {
    edit: v => {
        return webix.Number.format(v, nfconf)
    },
    parse: v => {
        return webix.Number.parse(v, nfconf)
    }
}
export const textnfconfnumber = {
    edit: v => {
        return webix.Number.format(v, nf4)
    },
    parse: v => {
        return webix.Number.parse(v, nf4)
    }
}
export const textnfconfnumberv = {
    edit: v => {
        return webix.Number.format(v, nf4v)
    },
    parse: v => {
        return webix.Number.parse(v, nf4v)
    }
}
export const gridnf0 = {
    format: v => {
        return webix.i18n.intFormat(v)
    },
    editParse: v => {
        return webix.Number.parse(v, webix.i18n)
    },
    editFormat: v => {
        return webix.i18n.intFormat(v)
    },
    numberFormat: v => {
        return webix.i18n.numberFormat(v)
    }
}

export const gridnf = {
    format: v => {
        return webix.i18n.numberFormat(v)
    },
    editParse: v => {
        return webix.Number.parse(v, webix.i18n)
    },
    editFormat: v => {
        return webix.i18n.numberFormat(v)
    }
}

export const gridnf3 = {
    format: v => {
        return webix.Number.format(v, nf3)
    },
    editParse: v => {
        return webix.Number.parse(v, nf3)
    },
    editFormat: v => {
        return webix.Number.format(v, nf3)
    }
}
export const gridnf4 = {
    format: v => {
        return webix.Number.format(v, nf4)
    },
    formatDecimal: v =>{
        return webix.Number.format(v, nf4Rate)
    },
    editParseDecimal: v => {
        return webix.Number.parse(v, nf4Rate)
    },
    editFormatDecimal: v => {
        return webix.Number.format(v, nf4Rate)
    },
    editParse: v => {
        return webix.Number.parse(v, nf4)
    },
    editFormat: v => {
        return webix.Number.format(v, nf4)
    }
}
export const gridnfconf = {
    format: v => {
        return webix.Number.format(v, nfconf)
    },
    editParse: v => {
        return webix.Number.parse(v, nfconf)
    },
    editFormat: v => {
        return webix.Number.format(v, nfconf)
    }
}

export const isEmpty = (val) => {
    if (val == null) return true
    if ("string" == typeof val) return val.length === 0
    if ("number" == typeof val) return val === 0
    if (Array.isArray(val)) return val.length === 0
    if (Object.keys(val).length === 0 && val.constructor === Object) return true
    return false
}

export const setConfStaFld = (jsono) => {
    //Set config for Static Field
    let json = jsono
    if (json.length > 0) {
        for (let i of json) {
            if (i.fldtyp == "other") {
                try {
                    let obj = $$(i.fldid)
                    if (obj) {
                        for (let j in i.atr) {
                            (j == "disabled" && i.atr[j]) ? obj.disable() : obj.config[j] = i.atr[j]
                        }
                        obj.refresh()
                    }
                } catch (error) {
                }
            } else if (i.fldtyp == "datagrid") {
                try {
                    let obj = $$(i.atr.pid)
                    if (obj) {
                        delete i.atr["pid"]
                        for (let j in i.atr) {
                            obj.getColumnConfig(i.fldid)[j] = true
                            obj.refreshColumns()
                        }
                    }
                } catch (error) {
                }
            } else if (i.fldtyp == "affectMany") {
                try {
                    let obj = $$(i.fldid)
                    if (obj) {
                        // x.config[j] = i.atr[j]
                        for (let j in i.atr) obj.queryView(x => {
                            x.config[j] = i.atr[j]
                            x.refresh()
                        })
                    }
                } catch (error) {
                }
            }
        }
    }
}
//Hóa đơn đầu vào
export const BTYPE = [
    { id: "01GTKT", value: "Hóa đơn giá trị gia tăng" }
    , { id: "02GTTT", value: "Hóa đơn bán hàng" }
    , { id: "07KPTQ", value: "Hóa đơn bán hàng khu phi thuế quan" }
    , { id: "01/TVE", value: "Tem, vé có VAT" }
    , { id: "02/TVE", value: "Tem, vé" }
    , { id: "01BLP", value: "Biên lai không có mệnh giá" }
    , { id: "02BLP", value: "Biên lai có mệnh giá" }
]

export const gridif = {
    format: v => {
        return webix.i18n.intFormat(v)
    },
    editParse: v => {
        return webix.Number.parse(v, webix.i18n)
    },
    editFormat: v => {
        return webix.i18n.intFormat(v)
    }
}

export const HDC = [
    { id: "ic", value: "Mã hóa đơn" },
    { id: "form", value: "Mẫu số" },
    { id: "serial", value: "Ký hiệu" },
    { id: "seq", value: "Số HĐ" },
    { id: "idt", value: "Ngày HĐ" },
    { id: "buyer", value: "Người mua" },
    { id: "bname", value: "Đơn vị" },
    { id: "bcode", value: "Mã khách hàng" },
    { id: "btax", value: "MST" },
    { id: "baddr", value: "Địa chỉ" },
    { id: "btel", value: "Điện thoại" },
    { id: "bmail", value: "Mail" },
    { id: "bacc", value: "Số tài khoản" },
    { id: "bbank", value: "Ngân hàng" },
    { id: "paym", value: "Hình thức thanh toán" },
    { id: "recvr", value: "Người nhận" },
    { id: "trans", value: "Người vận chuyển" },
    { id: "vehic", value: "Phương tiện" },
    { id: "contr", value: "Số hợp đồng" },
    { id: "ordno", value: "Lệnh số" },
    { id: "orddt", value: "Lệnh ngày" },
    { id: "ordou", value: "Đơn vị" },
    { id: "ordre", value: "Lý do" },
    { id: "whsfr", value: "Nơi xuất" },
    { id: "whsto", value: "Nơi nhập" },
    { id: "curr", value: "Loại tiền" },
    { id: "exrt", value: "Tỷ giá" },
    { id: "note", value: "Ghi chú" },
    { id: "sum", value: "Tổng tiền hàng" },
    { id: "vat", value: "Tổng thuế" },
    { id: "total", value: "Tổng tiền thanh toán" },
    { id: "items.type", value: "Hình thức" },
    { id: "items.name", value: "Tên hàng hóa, dịch vụ" },
    { id: "items.unit", value: "Đơn vị tính" },
    { id: "items.price", value: "Đơn giá" },
    { id: "items.quantity", value: "Số lượng" },
    { id: "items.vrt", value: "Thuế suất (%)" },
    { id: "items.vat", value: "Tiền thuế" },
    { id: "items.amount", value: "Thành tiền" },
    { id: "items.transdate", value: "Ngày giao dịch" },
    { id: "name", value: "Tên khách hàng" },
    { id: "name_en", value: "Tên tiếng Anh" },
    { id: "taxc", value: "MST" },
    { id: "code", value: "Mã" },
    { id: "addr", value: "Địa chỉ" },
    { id: "tel", value: "Điện thoại" },
    { id: "mail", value: "Mail" },
    { id: "acc", value: "Số tài khoản" },
    { id: "bank", value: "Ngân hàng" },
    { id: "rform", value: "Mẫu số cần thay thế" },
    { id: "rserial", value: "Ký hiệu cần thay thế" },
    { id: "rseq", value: "Số hóa đơn cần thay thế" },
    { id: "rref", value: "Số văn bản" },
    { id: "rrdt", value: "Ngày văn bản" },
    { id: "rrea", value: "Lý do" },
    { id: "score", value: "Điểm tiêu" },
    { id: "sum2", value: "Không chịu thuế" },
    { id: "vat0", value: "Thuế 0%" },
    { id: "sum0", value: "Thành tiền chưa thuế 0%" },
    { id: "vat5", value: "Thuế 5%" },
    { id: "sum5", value: "Thành tiền chưa thuế 5%" },
    { id: "vat10", value: "Thuế 10%" },
    { id: "sum10", value: "Thành tiền chưa thuế 10%" },
]
//Hóa đơn đầu vào
export const getvrn = (vrt) => {
    if (Number(vrt) < 0) return "\\"
    const obj = vat.find(x => x.id == vrt)
    return obj.value
}

const na = 2, np = 5, nq = 2;

const gnp = { groupDelimiter: ".", groupSize: 3, decimalDelimiter: ",", decimalSize: np }
const gnq = { groupDelimiter: ".", groupSize: 3, decimalDelimiter: ",", decimalSize: nq }
const gna = { groupDelimiter: ".", groupSize: 3, decimalDelimiter: ",", decimalSize: na }
const gn2 = { groupDelimiter: ".", groupSize: 3, decimalDelimiter: ",", decimalSize: 2 }
export const FGNA = webix.Number.numToStr(gna)
export const GNA = gna

export const gridna = {
    format: v => {
        return webix.Number.format(v, gna)
    },
    editParse: v => {
        return webix.Number.parse(v, gna)
    },
    editFormat: v => {
        return webix.Number.format(v, gna)
    }
}


export const gridnp = {
    format: v => {
        return webix.Number.format(v, gnp)
    },
    editParse: v => {
        return webix.Number.parse(v, gnp)
    },
    editFormat: v => {
        return webix.Number.format(v, gnp)
    }
}

export const gridnq = {
    format: v => {
        return webix.Number.format(v, gnq)
    },
    editParse: v => {
        return webix.Number.parse(v, gnq)
    },
    editFormat: v => {
        return webix.Number.format(v, gnq)
    }
}

export const textna = {
    edit: v => {
        return webix.Number.format(v, gna)
    },
    parse: v => {
        return webix.Number.parse(v, gna)
    }
}
export const PERIODS = [
    { id: "1", value: "Tháng 01" },
    { id: "2", value: "Tháng 02" },
    { id: "3", value: "Tháng 03" },
    { id: "4", value: "Tháng 04" },
    { id: "5", value: "Tháng 05" },
    { id: "6", value: "Tháng 06" },
    { id: "7", value: "Tháng 07" },
    { id: "8", value: "Tháng 08" },
    { id: "9", value: "Tháng 09" },
    { id: "10", value: "Tháng 10" },
    { id: "11", value: "Tháng 11" },
    { id: "12", value: "Tháng 12" }
]

export const CATEGORIES = [
    { id: "1", value: "Hàng hóa, dịch vụ dùng riêng cho SXKD chịu thuế GTGT" },
    { id: "2", value: "Hàng hóa, dịch vụ dùng riêng cho SXKD không chịu thuế GTGT" },
    { id: "3", value: "Hàng hóa, dịch vụ dùng chung cho SXKD chịu thuế GTGT và không chịu thuế GTGT" },
    { id: "4", value: "Hàng hóa, dịch vụ dùng cho dự án đầu tư" }
]
