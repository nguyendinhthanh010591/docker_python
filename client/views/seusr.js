import { JetView } from "webix-jet"
import { d2s, SITYPE, SUSTATUS, setConfStaFld, ROLE } from "models/util"
export default class SeouView extends JetView {
  config() {
    _ = this.app.getService("locale")._
    let textFilter = "", selectFilter = "", sortstring = false, sortserver = false
    //if (ROLE.includes('PERM_TEMPLATE_REGISTER_USRGRANT_SEARCHSER')) {
      textFilter = {content: "textFilter"}
      selectFilter = {content:"selectFilter"}
      sortstring = "string"
      sortserver = "server"
    //}
    let rowfd = ""
    //if (ROLE.includes('PERM_TEMPLATE_REGISTER_USRGRANT_SEARCHSER')) {
      rowfd ={  content: "datepickerFilter", prepare: function (filterValue, filterObject) {
        return filterValue;
      },
      compare: function (cellValue, filterValue) {
        let vcellValue = new Date(Date.parse(String(cellValue).substring(0,String(cellValue).length - 1)))
        return vcellValue >= filterValue;
      }}
    //}
  
    const grid1 = {
      view: "datatable",
      id: "seusr:grid1",
      select: "row",
      multiselect: false,
      resizeColumn: true,
      columns: [
        { id: "sel", header: { content: "masterCheckbox", css: "center" }, adjust: "header", css: "center", template: "{common.checkbox()}"/*, hidden: ROLE.includes('PERM_TEMPLATE_REGISTER_USRGRANT_SEARCHSER') ? false : true*/},
        { id: "taxc", header: [{ text: _("taxcode"), css: "header" }, textFilter], sort:sortstring, adjust: true },
        { id: "form", header: [{ text: _("form"), css: "header" }, textFilter], sort:sortstring, adjust: true },
        { id: "serial", header: [{ text: _("serial"), css: "header" }, textFilter], sort:sortstring, adjust: true },
        {
          id: "fd", header: [{ text: _("valid_from"), css: "header" }, rowfd
           ], sort: sortserver,stringResult: true, format: d2s, adjust: true },
        { id: "uses", header: [{ text: _("input_type"), css: "header" }, selectFilter], sort:sortstring, options: SITYPE(this.app), adjust: true },
        { id: "min", header: [{ text: _("from"), css: "header" },], sort: sortserver, css: "right", adjust: true },
        { id: "max", header: [{ text: _("to"), css: "header" },], sort: sortserver, css: "right", adjust: true },
        { id: "cur", header: [{ text: _("current"), css: "header" },], sort: sortserver, css: "right", adjust: true },
        { id: "status", header: [{ text: _("status"), css: "header" }, selectFilter], sort:sortstring, options: SUSTATUS(this.app), adjust: true },
      ]
    }
    let name = ""
    //if (ROLE.includes('PERM_TEMPLATE_REGISTER_USRGRANT_SEARCHNAME')) {
      name = {content: "textFilter"}
    //}
    const grid2 = {
      view: "datatable",
      id: "seusr:grid2",
      select: "row",
      multiselect: false,
      resizeColumn: true,
      columns: [
        { id: "id", header: [{ text: _("name"), css: "header" }, name], adjust: true, fillspace: true },
        { id: "name", header: { text: _("fullname"), css: "header" }, adjust: true, fillspace: true },
      ],
      url: "api/serial/seusr"
    }
    let btn = {}
    //if (ROLE.includes('PERM_TEMPLATE_REGISTER_USRGRANT_SAVE')) {PERM_TEMPLATE_REGISTER_USRGRANT_SAVE
      btn = { view: "button", id: "seusr:btnsave", type: "icon", icon: "mdi mdi-content-save", label: _("save"), width: 100, disabled: ROLE.includes('PERM_TEMPLATE_REGISTER_USRGRANT_SAVE') ? false : true }
    //}
    return { paddingX: 2, paddingY: 2, cols: [{ rows: [grid2, { cols: [{width: 400}, btn] }] }, grid1, { view: "resizer" } ] }
  }

  ready(v, urls) {
    //Cau hinh rieng cac cot tĩnh
    webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
        let json = result.json()
        setConfStaFld(json)
    })
    //
  }
  init() {
    if (!ROLE.includes('PERM_TEMPLATE_REGISTER_USRGRANT')) {
      this.show("/top/home")
      window.location.replace(`/${homepage}`)
      return
    }
    let grid2 = $$("seusr:grid2"), grid1 = $$("seusr:grid1")
    grid2.attachEvent("onAfterSelect", () => {
      const row = grid2.getSelectedItem()
      webix.ajax(`api/serial/seusr/${row.id}`).then(result => {
        grid1.clearAll()
        grid1.parse(result.json())
      })
    })

    $$("seusr:btnsave").attachEvent("onItemClick", () => {
      $$("seusr:grid1").filter()
      const sers = grid1.serialize(), usid = grid2.getSelectedId().id
      let arr = []
      for (const ser of sers) {
        if (ser.sel == 1) arr.push(ser.id)
      }
      webix.ajax().post("api/serial/seusr", { usid: usid, arr: arr }).then(() => { webix.message(_("save_ok_msg"), "success") })
    })

  }
}
