import { JetView } from "webix-jet"
import { TAXC, FN, MST, ENT, setConfStaFld } from "models/util"
import AvatarView from "views/avatar"
export default class ToolView extends JetView {
    config() {
        //const theme = this.app.getService("theme"), skin = theme.getTheme()
        _ = this.app.getService("locale")._

        // const window = webix.ui({
        //     view: "window", id: "secret", close: true, body:
        //     {
        //         cols: [
        //             { id: "url_path", name: "btax", value: "/top/", view: "text", attributes: { maxlength: 16 } },
        //             { view: "icon", icon: "mdi  mdi-link", width: 45, css: "button_toggle_menu", click: () => { this.app.show($$("url_path").getValue()) } }
        //         ]
        //     }
        // })

        const toolbar = {
            view: "toolbar",
            css: "toolbar_fpt",
            padding: 0,
            margin: 0,
            height: 50,
            cols: [
                { rows: [{ css: "button_toggle_menu" }, { view: "icon", icon: "mdi mdi-menu", width: 45, css: "button_toggle_menu", click: () => this.app.callEvent("menu:toggle") }, { css: "button_toggle_menu" }] },
                { width: 210, css: "logo_toolbar_fpt" },
                { width: 200, rows: [{ view: "label", height: 25, label: "<span class='text_on_toolbar webix_icon mdi mdi-deskphone'></span>"+`${phone_toolbar}` }, { view: "label", height: 25, label: "<span class='text_on_toolbar webix_icon mdi mdi-email-outline'></span>"+`${email_toolbar}` },] },
                { view: "label", label: _("einvoice"), align: "center", fillspace: true, css: "header1" },
                {
                    width: 400,
                    css: "user_info_block",
                    align: "right",
                    rows: [
                        {
                            height: 25, cols: [
                                { view: "label", width: 35, css: "avatar" },
                                { view: "label", align: "center", label: FN },
                                { view: 'label', width: 20, label: "<span class='mdi mdi-dots-vertical'></span>", click: () => this.AvatarView.show() }
                            ]
                        },
                        //{ view: "richselect", id: "tool:taxc", options: { data: MST, relative: "left" }, value: TAXC, height: 25, tooltip: `<b> #text# </b>` }
                        {
                            view: "combo", id: "tool:taxc", suggest: {
                                data: MST, relative: ["scb"].includes(ENT) ? "left" : "bottom", filter: function (item, value) {
                                    if (String(item.value).includes(value)) return true
                                    return false

                                }
                            }, tooltip: `<b> #text# </b>`, value: TAXC, height: 25
                        }

                    ]
                }
            ]
        }
        return toolbar
    }
    init() {
        this.AvatarView = this.ui(AvatarView)
    }
    ready(v, urls) {
        $$("tool:taxc").attachEvent("onChange", (n, o) => {
            if (n && n !== o) {
                let vsession = webix.storage.session.get("session")
                webix.ajax().post("api/tok", { taxc: n }).then(result => {
                    let tmpresult = result.json()
                    vsession.taxc = tmpresult.taxc
                    vsession.ou = tmpresult.ou
                    vsession.on = tmpresult.on
                    vsession.statement = tmpresult.statement
                    vsession.token = tmpresult.token
                    vsession.mst = tmpresult.mst
                    vsession.expires = tmpresult.expires
                    vsession.org = tmpresult.org
                    webix.storage.session.put("session",vsession)
                    webix.storage.session.remove("home")
                    location.reload()
                })
            }
        })
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", { url: urls[0].page, params: urls[0].params }).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
}