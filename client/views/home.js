import { JetView } from "webix-jet"
import { LOCALUSR, IS_USE_LOCAL_USER, ROLE, ENT, setConfStaFld } from "models/util"
let seri2 = "i0", seri3 = "totalv", invoice, revenue, textchart4 = [{ text: _("waiting_numbered"), color: "red" }, { text: _("pending_approval"), color: "orange" }, { text: _("approved"), color: "blue" }, { text: _("cancelled"), color: "indigo" }], tooltipchart4 = [{ value: "#i1#", color: "red", tooltip: { template: `#tg#: #i1# ${_("waiting_numbered")}` }, label: "#i1#" }, { value: "#i2#", color: "orange", tooltip: { template: `#tg#: #i2# ${_("pending_approval")}` }, label: "#i2#" }, { value: "#i3#", color: "blue", tooltip: { template: `#tg#: #i3# ${_("approved")}` }, label: "#i3#" }, { value: "#i4#", color: "indigo", tooltip: { template: `#tg#: #i4# ${_("cancelled")}` }, label: "#i4#" }]
let widthLegend = 90
let widthOfChart5 = 0
const format0 = v => {
    if (v == 0) return ""
    return webix.i18n.intFormat(v)
}
const format1 = v => {
    if (v == 0) return ""
    return format(v)
}
const format = v => {
    if (Number.isInteger(v)) return webix.i18n.intFormat(v)
    return webix.i18n.numberFormat(v)
}
const showGrid = (v) => {
    if (v) {
        // $$("grid2").show()
        if (ENT == "hlv") $$("grid3").show()
    }
    else {
        // $$("grid2").hide()
        if (ENT == "hlv") $$("grid3").hide()
    }
}
const showSeri2 = () => {
    const chart2 = $$("chart2")
    chart2.clearAll()
    chart2.removeAllSeries()
    let color
    switch (seri2) {
        case "i0":
            color = "purple"
            break
        case "i1":
            color = "red"
            break
        case "i2":
            color = "orange"
            break
        case "i3":
            color = "blue"
            break
        default:
            color = "indigo"
            break
    }
    const val = `#${seri2}#`, tip = `#tg#: ${val}`
    chart2.addSeries({ value: val, color: color, tooltip: tip })
    chart2.addSeries({ value: val, type: "spline", line: { color: color, width: 2 }, tooltip: tip, label: val })
    chart2.parse(invoice)
}
const totalTooltip = { template: obj => { return `${obj.tg}: ${_("revenue")} ${format(obj.totalv)}` } }
const vatTooltip = { template: obj => { return `${obj.tg}: ${_("vat")} ${format(obj.vatv)}` } }

    const showSeri3 = () => {
        if (ENT == "hlv") {
        const chart3 = $$("chart3")
        chart3.clearAll()
        chart3.removeAllSeries()
        const val = `#${seri3}#`
        let color, tip
        if (seri3 == "totalv") {
            color = "#36abee"
            tip = totalTooltip
        }
        else {
            color = "#a7ee70"
            tip = vatTooltip
        }
        chart3.addSeries({ value: val, color: color, tooltip: tip })
        chart3.addSeries({ value: val, type: "spline", line: { color: color, width: 2 }, tooltip: tip })
        chart3.parse(revenue)
    }
}
const fetch = (result) => {
    invoice = result.invoice
    revenue = result.revenue
    const total = result.total, thsd = result.thsd, chart1 = $$("chart1"), chart4 = $$("chart4"), chart5 = $$("chart5"), grid2 = $$("grid2"), grid3 = (ENT == "hlv") ? $$("grid3") : {}
    let valuechart1 = [
        { quantity: thsd.i1, loaihd: _("waiting_numbered"), tooltip: `${_("waiting_numbered")} ${thsd.i1}`, color: "red" },
        { quantity: thsd.i2, loaihd: _("pending_approval"), tooltip: `${_("pending_approval")} ${thsd.i2}`, color: "orange" },
        { quantity: thsd.i3, loaihd: _("approved"), tooltip: `${_("approved")} ${thsd.i3}`, color: "blue" },
        { quantity: thsd.i4, loaihd: _("cancelled"), tooltip: `${_("cancelled")} ${thsd.i4}`, color: "indigo" },
        { quantity: thsd.i0, loaihd: _("total"), tooltip: `${_("total")} ${thsd.i0}`, color: "purple" },
        //{ quantity: thsd.remain, loaihd: _("remain"), tooltip: `${_("remain")} ${thsd.remain}`, color: "green" },
    ]
    if (ENT == "vcm") {
        valuechart1 = [
            { quantity: thsd.i1, loaihd: _("draft"), tooltip: `${_("draft")} ${thsd.i1}`, color: "red" },
            { quantity: thsd.i3, loaihd: _("issue"), tooltip: `${_("issue")} ${thsd.i3}`, color: "blue" },
            { quantity: thsd.i4, loaihd: _("cancelled"), tooltip: `${_("cancelled")} ${thsd.i4}`, color: "indigo" },
            { quantity: thsd.i0, loaihd: _("total"), tooltip: `${_("total")} ${thsd.i0}`, color: "purple" },
            //{ quantity: thsd.remain, loaihd: _("remain"), tooltip: `${_("remain")} ${thsd.remain}`, color: "green" },
        ]
    }
    if (ENT == "hlv") {
        $$("invoices").setValue(webix.i18n.intFormat(total.invs))
        $$("totalv").setValue(format(total.totalv))
        $$("vatv").setValue(format(total.vatv))
        $$("time").setValue(result.time)
        grid3.clearAll()
        grid3.parse(revenue)
    }
    grid2.clearAll()

    grid2.parse(invoice)

    const chk = $$("chk").getValue()
    showGrid(chk)
    chart1.clearAll()
    chart4.clearAll()
    chart5.clearAll()
    chart1.parse(valuechart1)
    chart4.parse(invoice)
    chart5.parse(result.serial)

    if (result && Array.isArray(result.serial) && result.serial.length) {
        widthOfChart5 = result.serial.length * widthLegend
        if (widthOfChart5 < window.outerWidth) {
            widthOfChart5 = window.outerWidth - 150
            chart5.$view.style.minWidth = widthOfChart5
            chart5.$setSize(widthOfChart5, chart5.$height)
            $$("scrollview").$view.childNodes[0].style.width = `${widthOfChart5}px`
        } else {
            chart5.$view.style.minWidth = widthOfChart5
            chart5.$setSize(widthOfChart5, chart5.$height)
            $$("scrollview").$view.childNodes[0].style.width = `${widthOfChart5}px`
        }

    }
    showSeri2()
    if (ENT == "hlv") showSeri3()
}

const search = () => {
    if (enabledashboard == 0) return
    let form = $$("home:form")
    if (!form.validate()) return
    const param = form.getValues(), time = param.period != 7 ? $$("period").getText() : `${_("from")} ${webix.i18n.dateFormatStr(param.fd)} ${_("to")} ${webix.i18n.dateFormatStr(param.td)}`
    webix.ajax("api/home", param).then(rs => {
        const result = rs.json()
        result.time = time
        result.form = param
        webix.storage.session.put("home", result)
        fetch(result)
    })
}

export default class HomeView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        const TGS = [{ "id": 1, "value": _("date") }, { "id": 2, "value": _("week") }, { "id": 3, "value": _("month") }]
        const DVT = [{ id: 1, value: _("vnd") }, { id: 1000, value: _("thousand_vnd") }, { id: 1000000, value: _("million_vnd") }]
        if (ENT == "vcm") {
            textchart4 = [{ text: _("draft"), color: "red" }, { text: _("issue"), color: "blue" }, { text: _("cancelled"), color: "indigo" }]
            tooltipchart4 = [{ value: "#i1#", color: "red", tooltip: { template: `#tg#: #i1# ${_("draft")}` }, label: "#i1#" }, { value: "#i3#", color: "blue", tooltip: { template: `#tg#: #i3# ${_("issue")}` }, label: "#i3#" }, { value: "#i4#", color: "indigo", tooltip: { template: `#tg#: #i4# ${_("cancelled")}` }, label: "#i4#" }]
        }

        const PERIODS = [
            { "id": 1, "value": _("home_period1") },
            { "id": 2, "value": _("home_period2") },
            { "id": 3, "value": _("home_period3") },
            { "id": 4, "value": _("home_period4") },
            { "id": 5, "value": _("home_period5") },
            { "id": 6, "value": _("home_period6") },
            { "id": 7, "value": _("home_period7") }
        ]
        let period_value = 1, period_enable = false
        /*
        if (ENT == "vib") {
            period_value = 7
            period_enable = true
        }
        */
        const form = {
            view: "form",
            id: "home:form",
            css: "homeBox",
            borderless: true,
            padding: 10,
            elementsConfig: { labelWidth: 70 },
            elements: [
                {
                    id: "period", name: "period", view: "richselect", label: _("home_period"), value: period_value, options: PERIODS, disabled: period_enable
                },
                {
                    cols: [
                        { id: "form_combo", name: "form_combo", view: "combo", label: _("form"), suggest: { url: "api/cat/serial/form" } },
                        { id: "serial_combo", name: "serial_combo", view: "combo", label: _("serial"), suggest: {} },
                    ]
                },
                {
                    cols: [
                        { id: "fd", name: "fd", view: "datepicker", label: _("fd"), type: "icon", icon: "mdi", stringResult: true, readonly: true },
                        { id: "td", name: "td", view: "datepicker", label: _("td"), type: "icon", icon: "mdi", stringResult: true, readonly: true }
                    ]
                },
                { id: "btnsegment", name: "tg", view: "segmented", label: _("chart"), value: 1, options: TGS,hidden:["hlv"].includes(ENT)? true:false },
                {
                    cols: [
                    { id: "chk", name: "chk", label: _("home_table"), view: "checkbox", checkValue: 1, value: 1,/*hidden:["hlv"].includes(ENT)? true:false*/ hidden: true },
                        { id: "dvt", name: "dvt", view: "richselect", label: _("unit"), value: 1, options: DVT, gravity: 1.5 },
                        { id: "btnrefresh", view: "button", type: "icon", width: 30, tooltip: "Refresh", icon: "mdi mdi-sync" }
                    ]
                }
            ],
            rules: {
                fd: webix.rules.isNotEmpty,
                td: webix.rules.isNotEmpty,
                $obj: function (data) {
                    const fd = data.fd, td = data.td
                    if (fd > td) {
                        webix.message(_("date_invalid_msg"))
                        return false
                    }
                    const dtime = (new Date(td)).getTime() - (new Date(fd)).getTime()
                    /*
                    if ((ENT == "vib") && (dtime > ddate)) {
                        webix.message(String(_("dt_search_between_day")).replace('#hunglq',(604800000/(1000*3600*24))))
                        return false
                    }
                    */
                    if (dtime > 31622400000) {
                        webix.message(_("dt_search_between_year"))
                        return false
                    }
                    return true
                }
            }
        }
        const box1 = (ENT == "hlv") ? {
            css: "homeBox",
            padding: 10,
            rows: [
                {},
                { cols: [{ view: "label", label: _("home_total_invoice"), css: "bold" }, { id: "invoices", css: "homeBoxRow1", tooltip: _("home_total_invoice"), view: "label", align: "right" }] },
                { cols: [{ view: "label", label: _("home_total_revenue"), css: "bold" }, { id: "totalv", css: "homeBoxRow1", tooltip: _("home_total_revenue"), view: "label", align: "right" }] },
                { cols: [{ view: "label", label: _("home_total_vat"), css: "bold" }, { id: "vatv", css: "homeBoxRow1", tooltip: _("home_total_vat"), view: "label", align: "right" }] },
                { fillspace: true },
                { id: "time", css: "homeBoxFooter", view: "label", label: _("home_period1"), align: "right" },
            ]
        } :
            {
                css: "homeBox",
                padding: 10,
                rows: [
                    {},
                    // { cols: [{ view: "label", label: _("home_total_invoice"), css: "bold" }, { id: "invoices", css: "homeBoxRow1", tooltip: _("home_total_invoice"), view: "label", align: "right" }] },
                    // { cols: [{ view: "label", label: _("home_total_revenue"), css: "bold" }, { id: "totalv", css: "homeBoxRow1", tooltip: _("home_total_revenue"), view: "label", align: "right" }] },
                    // { cols: [{ view: "label", label: _("home_total_vat"), css: "bold" }, { id: "vatv", css: "homeBoxRow1", tooltip: _("home_total_vat"), view: "label", align: "right" }] },
                    // { fillspace: true },
                    // { id: "time", css: "homeBoxFooter", view: "label", label: _("home_period1"), align: "right" },
                ]
            }
        const chart1 = {
            id: "chart1",
            view: "chart",
            type: "bar",
            css: "homeBox bold",
            padding: { left: 10, right: 10, bottom: 25, top: 25 },
            color: "#color#",
            value: "#quantity#",
            label: "<span title='#loaihd# #quantity#'>#quantity#</span>",
            radius: 3,
            gradient: "3d",
            borderless: true,
            tooltip: { template: "#loaihd# #quantity#" },
            legend: { width: 90, align: "right", valign: "middle", layout: "y", template: "#loaihd#", marker: { type: "round", width: 5, height: 5 } },
            xAxis: { title: _("home_thsdhd"), template: "", lines: false },
        }

        const chart2 = {
            id: "chart2",
            view: "chart",
            type: "bar",
            radius: 3,
            borderless: true,
            xAxis: { template: "#tg#" },
            yAxis: { start: 0 }
        }
        
            const chart3 = {
                id: "chart3",
                view: "chart",
                type: "bar",
                radius: 3,
                borderless: true,
                padding: { left: 100 },
                gradient: "rising",
                xAxis: { template: "#tg#" },
                yAxis: { start: 0, template: obj => { return format(obj) } },
            }
        
        const chart4 = {
            id: "chart4",
            view: "chart",
            type: "bar",
            preset: "stick",
            borderless: true,
            xAxis: { template: "#tg#" },
            yAxis: { start: 0 },
            legend: { width: 90, align: "right", valign: "middle", layout: "y", marker: { type: "round", width: 5, height: 5 }, values: textchart4 },
            series: tooltipchart4
        }
        const chart5 = {
            view: "scrollview",
            id: "scrollview",
            scroll: "x",
            body: {
                id: "chart5",
                view: "chart",
                type: "bar",
                select: true,
                radius: 3,
                // width: "auto",
                responsive: true,
                borderless: true,
                xAxis: { template: obj => {
                    return obj.serial.split('-')[1]
                } },
                legend: { width: 90, align: "right", valign: "middle", layout: "y", marker: { type: "round", width: 5, height: 5 }, values: [{ text: _("home_max"), color: "green" }, { text: _("home_cur"), color: "tomato" }] },
                series: [
                    { value: "#max#", color: "green", tooltip: { template: "#serial#: #max# (Còn lại:#kd#)" }, label: "#max#" },
                    { value: "#cur#", color: "tomato", tooltip: { template: "#serial#: #cur# (#rate#)" }, label: "#cur#" }
                ]
            }
        }
        if (ENT == "hlv") {
            const grid2 = {
                view: "datatable",
                id: "grid2",
                hidden: true,
                scrollX: false,
                tooltip: true,
                columns: [
                    { id: "tg", header: { text: "", height: 15, tooltip: _("time") }, fillspace: true },
                    { id: "i1", header: { text: "", css: "bgchoso", tooltip: _("waiting_numbered") }, adjust: "data", css: "right choso", format: format0 },
                    { id: "i2", header: { text: "", css: "bgchoduyet", tooltip: _("pending_approval") }, adjust: "data", css: "right choduyet", format: format0 },
                    { id: "i3", header: { text: "", css: "bgdaduyet", tooltip: _("approved") }, adjust: "data", css: "right daduyet", format: format0 },
                    { id: "i4", header: { text: "", css: "bgdahuy", tooltip: _("cancelled") }, adjust: "data", css: "right dahuy", format: format0 },
                    { id: "i0", header: { text: "", css: "bgtongso", tooltip: _("total") }, adjust: "data", css: "right tongso", format: format0 }
                ],
                on: {
                    onHeaderClick: (id) => {
                        const column = id.column
                        if (column == "tg") return
                        seri2 = column
                        showSeri2()
                    }
                }
            }
        }

        const grid2 = {
            view: "datatable",
            id: "grid2",
            hidden: true,
            scrollX: false,
            tooltip: true,
            columns: [
                // { id: "tg", header: { text: "", height: 15, tooltip: _("time") }, fillspace: true },
                { id: "i1", header: { text: "", css: "bgchoso", tooltip: _("waiting_numbered") }, adjust: "data", css: "right choso", format: format0 },
                { id: "i2", header: { text: "", css: "bgchoduyet", tooltip: _("pending_approval") }, adjust: "data", css: "right choduyet", format: format0 },
                { id: "i3", header: { text: "", css: "bgdaduyet", tooltip: _("approved") }, adjust: "data", css: "right daduyet", format: format0 },
                { id: "i4", header: { text: "", css: "bgdahuy", tooltip: _("cancelled") }, adjust: "data", css: "right dahuy", format: format0 },
                { id: "i0", header: { text: "", css: "bgtongso", tooltip: _("total") }, adjust: "data", css: "right tongso", format: format0 }
            ],
            on: {
                onHeaderClick: (id) => {
                    const column = id.column
                    if (column == "tg") return
                    seri2 = column
                    showSeri2()
                }
            }
        }
        
            const grid3 = {
                view: "datatable",
                id: "grid3",
                hidden: true,
                tooltip: true,
                scrollX: false,
                columns: [
                    { id: "tg", header: { text: "", height: 15, tooltip: _("time") }, fillspace: true },
                    { id: "totalv", header: { text: "", css: "bgtotal", tooltip: _("revenue") }, adjust: "data", css: "right", format: format1 },
                    { id: "vatv", header: { text: "", css: "bgvat", tooltip: _("vat") }, adjust: "data", css: "right", format: format1 }
                ],
                on: {
                    onHeaderClick: (id) => {
                        const column = id.column
                        if (column == "tg") return
                        seri3 = column
                        showSeri3()
                    }
                }
            }
        
        const r1 = {
            cols: [
                {},
                {
                    height: 90, margin: 5, paddingY: 2, borderless: true, width: 1000,
                    cols: [
                        { id: "btn_laphd", view: "button", type: "iconTop", css: "btn_home btn_home1", label: `<span class='label_homepage'>${_("invoice_issue")}</span>`, click: () => { this.show("/top/inv.inv") }, disabled: true },
                        { id: "btn_laphd_excel", view: "button", type: "iconTop", css: "btn_home btn_home2", label: `<span class='label_homepage'>${_("invoice_from_excel")}</span>`, click: () => { this.show("/top/inv.xls") }, disabled: true },
                        { id: "btn_duyethd", view: "button", type: "iconTop", css: "btn_home btn_home3", label: `<span class='label_homepage'>${_("invoice_approve")}</span>`, click: () => { this.show(ENT != "vcm" ? "/top/inv.appr" : "/top/inv.seqappr") }, disabled: true },
                        { id: "btn_tchd", view: "button", type: "iconTop", css: "btn_home btn_home4", label: `<span class='label_homepage'>${_("invoice_search")}</span>`, click: () => { this.show("/top/inv.invs") }, disabled: true },
                        { id: "btn_tbph", view: "button", type: "iconTop", css: "btn_home btn_home5", label: `<span class='label_homepage'>${_("released")}</span>`, click: () => { this.show("/top/serial") }, disabled: true },
                        { id: "btn_help", view: "button", type: "iconTop", css: "btn_home btn_home6", label: `<span class='label_homepage'>${_("manual_instruction")}</span>` },
                    ]
                },
                {},
            ]
        }
        const r2 = { height: 220, margin: 5, paddingY: 2, borderless: true, cols: ["hlv"].includes(ENT) ? [form, box1] : [form, chart1, box1] }
        let r3
        if (ENT == "hlv") {
            r3 = {
                paddingY: 2, fillspace: true, id: "carousel", view: "carousel", cols: [
                    { rows: [{ view: "label", align: "center", label: _("released"), css: "bold" }, chart5] },
                    { margin: 5, cols: [{ rows: [{ view: "label", align: "center", label: _("home_invoices"), css: "bold" }, chart2], gravity: 2 }, grid2] },
                    { rows: [{ view: "label", align: "center", label: _("home_invoices_status"), css: "bold" }, chart4] },
                    // { rows: [{ view: "label", align: "center", label: _("released"), css: "bold" }, chart5] },
                    { margin: 5, cols: [{ rows: [{ view: "label", align: "center", label: _("home_revenue_vat"), css: "bold" }, chart3], gravity: 2 }, grid3] },
                ], css: "webix_dark"
            }
        } else {
            r3 = {
                paddingY: 2, fillspace: true, id: "carousel", view: "carousel", cols: [
                    { rows: [{ view: "label", align: "center", label: _("released"), css: "bold" }, chart5] },
                    { margin: 5, cols: [{ rows: [{ view: "label", align: "center", label: _("home_invoices"), css: "bold" }, chart2], gravity: 2 }, grid2] },
                    { rows: [{ view: "label", align: "center", label: _("home_invoices_status"), css: "bold" }, chart4] },
                    // { rows: [{ view: "label", align: "center", label: _("released"), css: "bold" }, chart5] },
                    // { margin: 5, cols: [{ rows: [{ view: "label", align: "center", label: _("home_revenue_vat"), css: "bold" }, chart3], gravity: 2 }, grid3] },
                ], css: "webix_dark"
            }
        }

        const r4 = { paddingY: 1, view: "label", align: "center", label: `<b>Copyright © 2019 ${ENT == "hdb" ? "HD Bank" : "FPT Information System"}. All rights reserved</b>` }
        const r5 = { css: "image_home" }

        //return { cols: [{ fillspace: true }, { width: 1050, margin: 3, rows: [{ height: 1 }, r1, r2, r3, r4] }, { fillspace: true }] }
        return { paddingX: 2, rows: ["hlv"].includes(ENT) ? [r1, r2, {}, r4] :( [1].includes(enabledashboard) ? [r1, r2, r3, r4]:[r1,r5]) }
    }
    init() {
        let date = new Date(), firstDay = new Date(date.getFullYear(), date.getMonth(), date.getDate() - 1)
        const now = new Date(), prw = new Date()
        prw.setDate(prw.getDate() - 7)
        let fd = $$("fd"), td = $$("td")
        fd.getPopup().getBody().define("maxDate", firstDay)
        td.getPopup().getBody().define("maxDate", date)
        fd.setValue(firstDay)
        td.setValue(date)
        if (ENT == "hlv") {
            fd.getPopup().getBody().define("maxDate", now)
            td.getPopup().getBody().define("maxDate", now)
            fd.setValue(prw)
            td.setValue(now)
        }
        $$("btn_help").attachEvent("onItemClick", () => window.open(HDSD_APP))
        // $$("chk").attachEvent("onChange", v => showGrid(v))
        $$("form_combo").attachEvent("onChange", value => {
            // $$("serial_combo").clearAll()
            console.log($$("serial_combo"))
            try {
                $$("serial_combo").getList().clearAll()
                $$("serial_combo").setValue(null)
            } catch (error) {

            }
            webix.ajax().get(`api/cat/serialbyform`, { type: value }).then(result => {
                let json = result.json()
                console.log(json)
                json.forEach(item => {
                    console.log(item)
                    $$("serial_combo").getList().add(item)
                });
            })
        })
        $$("btnrefresh").attachEvent("onItemClick", () => search())
        $$("period").attachEvent("onChange", (newv) => {
            let b = (newv != 7)
            if (b) {
                const dt = new Date(), now = new Date(), yr = now.getFullYear(), mm = now.getMonth()
                let tn, dn
                switch (newv) {
                    case 1:
                        dt.setDate(dt.getDate() - 7)
                        tn = dt
                        dn = now
                        break
                    case 2:
                        dt.setDate(dt.getDate() - 30)
                        tn = dt
                        dn = now
                        break
                    case 3:
                        tn = new Date(yr, mm, 1)
                        dn = new Date(yr, mm + 1, 0)
                        break
                    case 4:
                        tn = new Date(yr, mm - 1, 1)
                        dn = new Date(yr, mm, 0)
                        break
                    case 5:
                        tn = new Date(yr, 0, 1)
                        dn = new Date(yr + 1, 0, 0)
                        break
                    case 6:
                        tn = new Date(yr - 1, 0, 1)
                        dn = new Date(yr, 0, 0)
                        break
                    default:
                        break
                }
                fd.setValue(tn)
                td.setValue(dn)

            }
            fd.config.readonly = b
            td.config.readonly = b
            fd.refresh()
            td.refresh()
            search()
        })
    }
    ready(v, urls) {
        if (ROLE.includes('PERM_TEMPLATE_REGISTER') || ROLE.includes('PERM_TEMPLATE_APPROVE') || ROLE.includes('PERM_TEMPLATE_VOID')) $$("btn_tbph").enable()
        if (ROLE.includes('PERM_CREATE_INV_MANAGE')) {
            $$("btn_laphd").enable()
            $$("btn_laphd_excel").enable()
        }
        if (ROLE.includes('PERM_APPROVE_INV_MANAGE')) $$("btn_duyethd").enable()
        if (ROLE.includes('PERM_CREATE_INV_MANAGE') || ROLE.includes('PERM_SEARCH_INV') || ROLE.includes('PERM_INVOICE_ADJUST') || ROLE.includes('PERM_INVOICE_REPLACE') || ROLE.includes('PERM_INVOICE_CANCEL')) $$("btn_tchd").enable()
        const result = webix.storage.session.get("home")
        if (result) {
            $$("home:form").setValues(result.form)
            fetch(result)
        }
        else search()
        //Cau hinh rieng cac cot tĩnh
        // webix.ajax().get("api/fld/conf", { url: urls[0].page, params: urls[0].params }).then(result => {
        //     let json = result.json()
        //     setConfStaFld(json)
        //     search()
        // })
        if (IS_USE_LOCAL_USER && LOCALUSR) {
            const logi = JSON.parse(sessionStorage.getItem('session')).loginnum
            if (!logi) webix.message(_("first_login_warning"), "error")
        }
    }
}