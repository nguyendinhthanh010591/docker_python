import { JetView } from "webix-jet"
import { LANGS, setConfStaFld } from "models/util"
export default class AvatarView extends JetView {
    config() {
        const locale = this.app.getService("locale"),
            lang = locale.getLang()
        const popup = {
            view: "popup",
            position: (state) => {
                state.left = state.maxWidth - state.width
                state.top = 50
            },
            body: {
                cols: [{
                        view: "segmented",
                        id: "lang:lang",
                        value: lang,
                        options: LANGS,
                        width: 60,
                        tooltip: "Change language",
                        on: {
                            onChange(newv) {
                                locale.setLang(newv)
                                webix.i18n.setLocale(newv == "en" ? "en-US" : "vi-VN")
                            }
                        }
                    },
                    { view: "button", type: "icon", icon: "mdi mdi-help-circle-outline", width: 30, tooltip: _("help"), click: () => window.open(HDSD_APP) },
                    { view: "button", type: "icon", icon: "mdi mdi-fullscreen", width: 30, tooltip: _("fullscreen"), click: () => {!document.fullscreen && Boolean(document.documentElement.webkitRequestFullScreen) ? document.documentElement.webkitRequestFullScreen() : document.webkitExitFullscreen() } },
                    { view: "button", type: "icon", icon: "mdi mdi-logout", width: 30, tooltip: _("logout"), css: "webix_danger", click: () => this.app.show("logout") },
                ]
            }
        }
        return popup
    }
    ready(v, urls) {
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
    init() {
    }
    show() {
        !this.getRoot().isVisible() ? this.getRoot().show() : this.getRoot().hide()
    }
}