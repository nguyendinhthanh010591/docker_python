import { JetView } from "webix-jet"
import { LANGS } from "models/util"
const use_adfs = () => {
    try {
        return adfs
    }
    catch (err) {
        return 0
    }
}
//Có sử dụng Azure AD hay không
const isUsingAzureAD = () => {
    try {
        return useAzureAD;
    } catch (error) {
        return false;
    }
};
const use_ent = () => {
    try {
        return ent
    }
    catch (err) {
        return ""
    }
}
export default class LoginView extends JetView {
    config() {
        const locale = this.app.getService("locale"), lang = locale.getLang()
        _ = locale._
        const form =(use_ent() == "sgr"? {
            view: "form",
            css: "login_style",
            id: "login:form",
            width: 300,
            height:400,
            elements: [
                { height: 90 },
                {
                    cols: [{ fillspace: true },
                    {
                        view: "segmented", value: lang, options: LANGS, width: 50, height: 25, on: {
                            onChange(newv) {
                                webix.i18n.setLocale(newv == "en" ? "en-US" : "vi-VN")
                                locale.setLang(newv)
                            }
                        }
                    }]
                },
                
                { view: "text", name: "username", attributes: { maxlength: 50 }, placeholder: _("username"), hidden: (use_adfs() || isUsingAzureAD()) ?  true : false},
                { view: "text", name: "password", attributes: { maxlength: 50 }, placeholder: _("password"), type: "password", hidden: (use_adfs() || isUsingAzureAD()) ?  true : false },
                { height: 10 },
                { id: "btnlogin", view: "button",css:"btlogin", type: "icon", icon: "webix_icon mdi mdi-login", label: _("login"), hotkey: "enter" },
                ext_resetpass_login ? { view: "label", label: `<span><a href="/reset.html">${_("password_reset")}</a></span>`, align: "center"} : {height:1},
                { height: 70 }
            ],
            rules: {
                $all: webix.rules.isNotEmpty
            }
        }:
        {
            view: "form",
            css: "background-login",
            id: "login:form",
            width: 300,
            elements: [
                { height: 70 },
                {
                    cols: [{ fillspace: true },
                    {
                        view: "segmented", value: lang, options: LANGS, width: 50, height: 25, on: {
                            onChange(newv) {
                                webix.i18n.setLocale(newv == "en" ? "en-US" : "vi-VN")
                                locale.setLang(newv)
                            }
                        }
                    }]
                },
                
                { view: "text", name: "username", attributes: { maxlength: 50 }, placeholder: _("username"), hidden: (use_adfs() || isUsingAzureAD()) ?  true : false},
                { view: "text", name: "password", attributes: { maxlength: 50 }, placeholder: _("password"), type: "password", hidden: (use_adfs() || isUsingAzureAD()) ?  true : false },
                { height: 10 },
                { id: "btnlogin", view: "button", type: "icon", icon: "webix_icon mdi mdi-login", label: _("login"), hotkey: "enter" },
                ext_resetpass_login ? { view: "label", label: `<span><a href="/reset.html">${_("password_reset")}</a></span>`, align: "center"} : {height:1},
                { height: 70 }
            ],
            rules: {
                $obj: data => {
                    if ((!webix.rules.isNotEmpty(data.username)) || (!webix.rules.isNotEmpty(data.password))) {
                        webix.message(_("user_pass_not_empty"), "error")
                        return false
                    }
                    //Valid riêng cho SCB
                    if (use_ent() == "scb") {
                        let position = (data.username).indexOf('.')
                        if(position == -1){
                            if (!(/^([1-9]){1}([0-9]){6}?$/.test(data.username))) {
                                webix.message(_("scb_user_bankid"), "error")
                                return false
                                
                            }
                        }else if(position == 0){
                            webix.message(_("scb_user_bankid"), "error")
                            return false;
                        }else{
                            let sp_username = (data.username).split(".")
                            let check = sp_username[sp_username.length-1]
                            if (!(/^([1-9]){1}([0-9]){6}?$/.test(check))) {
                                webix.message(_("scb_user_bankid"), "error")
                                return false
                                
                            }
                            /*
                            if(!isNaN(parseFloat(check)) && !isNaN(check - 0) && check.length == 7){
                                return true;
                            }else{
                                webix.message(_("scb_user_bankid"), "error")
                                return false;
                            }
                            */
                        }
                    }
                    return true
                }
            }
        })


        const ui = {
            rows: [
                { cols: [{}, { rows: [{}, form, {}], css: "transparent-background" }, {}] },
                { view: 'label', height: 17, align: "center", label: `<span style="color: ${(use_ent() == "fhs"  ) ? "blue" : "white"};">Bản quyền thuộc về <span style="font-size: 13px;font-weight:bold;">Công ty TNHH Hệ thống Thông tin FPT</span></span> ` },
                { view: 'label', height: 17, align: "center", label: `<span style="color: ${(use_ent() == "fhs" ) ? "blue" : "white"};">Tầng 22 tòa nhà Keangnam Landmark 72, E6 đường Phạm Hùng, phường Mễ Trì, quận Nam Từ Liêm, Hà Nội</span>` },
                { height: 10 }
            ],
            css:  use_ent() == "sgr"?"background-login-sgr":"bg1"
        }
        return ui
    }
    urlChange(urls) {
        if (use_adfs() || isUsingAzureAD()) {
            let ptoken = this.getParam(`logintoken`)
            if (ptoken) {
                webix.ajax().post("api/signin", { logintoken: ptoken, username: "ptoken", password: "123" }).then(data => {
                    webix.storage.session.put("session", data.json())
                    this.show("/top/home")
                    window.location.replace(`/${homepage}`)
                })
            }
        }
    }
    ready() {
        $$("login:form").focus("taxc")
       
    }
    init() {
        let form = $$("login:form")
        webix.i18n.setLocale("vi-VN")
        $$("btnlogin").attachEvent("onItemClick", () => {
            if (use_adfs()) {
                window.location.replace(`/api/signinsaml`)
            }
            else if(isUsingAzureAD()) {
                window.location.replace(`/api/signin-azure-ad`);
            }
            else {
                if (!form.validate()) return false
                webix.ajax().post("api/signin", form.getValues()).then(data => {
                    webix.storage.session.put("session", data.json())
                    this.show("/top/home")
                    window.location.replace(`/${homepage}`)
                })
            }

        })
    }
}