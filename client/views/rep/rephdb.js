import { JetView } from "webix-jet"
import { ITYPE, filter, setConfStaFld } from "models/util"
export default class ReportView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        const REPORTS = [
            { id: 1, value: _("report_hdb_inv_detail") },
            { id: 2, value: _("report_hdb_transaction_wait") },
            { id: 3, value: _("report_inv_amountHDB") },
            // { id: 3, value: _("cancel_report") },
            // { id: 4, value: _("BC26_SoLuong") },
            // { id: 5, value: _("BC26_SoLuong_tranform") },
        ]

        const PROF = [
            { id: 1, value: "Thu lãi" },
            { id: 2, value: "Thu dịch vụ" },
            { id: 3, value: "Bán ngoại tệ" },
            { id: 4, value: "Thu từ KD ngoại tệ" },
            { id: 5, value: "Thu từ KD chứng khoán" },
            { id: 6, value: "Thu khác" },
            { id: 7, value: "Hàng khuyến mãi" },
            { id: 8, value: "Thu nhập từ góp vốn mua cổ phần" },
        ]
        const q = _("quarter"), m = _("month")
        const PERIODS = [
            { id: "d", value: _("home_period7") },
            { id: "1", value: `${m} 01` },
            { id: "2", value: `${m} 02` },
            { id: "3", value: `${m} 03` },
            { id: "4", value: `${m} 04` },
            { id: "5", value: `${m} 05` },
            { id: "6", value: `${m} 06` },
            { id: "7", value: `${m} 07` },
            { id: "8", value: `${m} 08` },
            { id: "9", value: `${m} 09` },
            { id: "10", value: `${m} 10` },
            { id: "11", value: `${m} 11` },
            { id: "12", value: `${m} 12` },
            { id: "q1", value: `${q} 1` },
            { id: "q2", value: `${q} 2` },
            { id: "q3", value: `${q} 3` },
            { id: "q4", value: `${q} 4` },
            { id: "y", value: _("year") },
        ]
        const VRT = [
            { id: "10%", value: "10%" },
            { id: "5%", value: "5%" },
            { id: "0%", value: "0%" },
            { id: "-1", value: "Không chịu thuế GTGT" },
        ]

        const form = {
            view: "form",
            id: "rephdb:form",
            padding: 3,
            margin: 3,
            elementsConfig: { labelWidth: 100 },
            elements:
                [
                    {
                        cols: [
                            { id: "reports", name: "report", label: _("reportType"), value: 1, view: "combo", options: REPORTS, required: true },
                            { id: "vrt", labelWidth: 150,name: "vrt", label: _("vrt"), view: "combo", suggest: VRT },
                            { id: "idt", name: "idt", label: _("date"), value: new Date(), required: true, view: "datepicker", editable: true, stringResult: true, format: webix.i18n.dateFormatStr },
                            
                        ]
                    },
                    {
                        cols: [
                            { id: "cuscode", name: "cuscode", label: _("customer_code"), view: "text" },
                            { id: "cusacc", labelWidth: 150, name: "cusacc", label: _("customer_acc"), view: "text" },
                            { id: "prof", name: "prof", label: _("professional"), view: "combo", suggest: PROF },
                        ]
                    },
                    {
                        cols: [
                            { id: "type", name: "type", label: _("invoice_type"), value: "01GTKT", disabled: true, view: "combo", options: ITYPE(this.app), gravity: 2 },
                        ]
                    },
                    {
                        cols: [
                            { id: "period", label: _("home_period"), value: "d", view: "combo", options: PERIODS },
                            { id: "year", label: _("year"), view: "combo", options: [] },
                            { id: "fd", name: "fd", label: _("fd"), view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr },
                            { id: "td", name: "td", label: _("td"), view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr }
                        ]
                    },
                    {
                        cols: [
                            {
                                rows:[
                                    { id: "ou", name: "ou", label: _("ou_code"), view: "multicombo", suggest: { selectAll: true, body:{data: [],template:webix.template("#code#_#value#")}} } ,
                                    { view: "multicombo", id: "mst", name: "mst", label: _("taxcode"), filter: filter, suggest: { selectAll: true, body:{data: [],template:webix.template("#mst#_#value#")} }},//options:  "api/ous/taxn"//, filter: filter
                                ]
                            },
                            {
                                rows:[
                                    { view: "button", type: "icon", icon: "wxi-sync", label: _("delete"), width: 100, on:{onItemClick:()=>{$$("rephdb:form").clear()} }},
                                    { view: "button", id: "btnreport", type: "icon", icon: "wxi-download", label: _("crereport"), width: 100 }                                    
                                ]
                            },
                        ]
                    }
                ]
            , rules: {
                report: webix.rules.isNotEmpty,
                idt: webix.rules.isNotEmpty,
                $obj: function (data) {
                    const fd = data.fd, td = data.td
                    if (fd > td) {
                        webix.message(_("date_invalid_msg"))
                        return false
                    }
                    const dtime = (new Date(td)).getTime() - (new Date(fd)).getTime()
                    if (dtime > 31622400000) {
                        webix.message(_("dt_search_between_year"))
                        return false
                    }
                    return true
                }
            }
        }
        return {view:"scrollview", body:{ cols: [{}, { gravity: 6, rows: [form, {}] }, {}] }}
    }
    ready(v, urls) {
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
    init() {
        const all = { id: "*", value: _("all") }
        let date = new Date(), yr = date.getFullYear(), firstDay = new Date(yr, date.getMonth() - 1, 1)
        let form = $$("rephdb:form"), mst = $$("mst"), ou = $$("ou"), report = $$("reports"), type = $$("type"), period = $$("period"), year = $$("year"), fd = $$("fd"), td = $$("td")
        // ou.getList().add(all, 0)
        // vrt.getList().add(all, 0)
        // prof.getList().add(all, 0)
        // ou.setValue("*")
        // vrt.setValue("*")
        // prof.setValue("*")
        let years = [], i = 2018
        while (i <= yr) {
            years.push({ id: i, value: i })
            i++
        }
        let list = year.getPopup().getList()
        list.clearAll()
        list.parse(years)
        year.setValue(yr)
        fd.setValue(firstDay)
        td.setValue(date)
        type.getList().add(all, 0)


        const cal = (v, yr) => {
            let b = (v != "d"), tn, dn, mm
            if (b) {
                let v1 = v.substr(0, 1)
                if (v1 === "y") {
                    tn = new Date(yr, 0, 1)
                    dn = new Date(yr, 11, 31)
                }
                else if (v1 === "q") {
                    let v2 = parseInt(v.substr(1))
                    mm = 3 * (v2 - 1)
                    tn = new Date(yr, mm, 1)
                    dn = new Date(yr, mm + 3, 1)
                    dn.setDate(dn.getDate() - 1)
                }
                else {
                    mm = parseInt(v)
                    tn = new Date(yr, mm - 1, 1)
                    dn = new Date(yr, mm, 1)
                    dn.setDate(dn.getDate() - 1)
                }

            }
            else {
                mm = date.getMonth()
                tn = new Date(yr, mm - 1, 1)
                dn = new Date(yr, mm, date.getDate())
            }
            fd.setValue(tn)
            td.setValue(dn)
            fd.config.readonly = b
            td.config.readonly = b
            fd.refresh()
            td.refresh()
        }

        year.attachEvent("onChange", (v) => { cal(period.getValue(), v) })
        period.attachEvent("onChange", (v) => { cal(v, year.getValue()) })


        report.attachEvent("onChange", (newv) => {
            if (newv == 1) {
                type.setValue("01GTKT")
                type.disable()
            }
            else {
                type.enable()
            }
        })

        this.webix.ajax().get("api/rpt/bytoken").then(r=>{
            let data = r.json()
            let removeDuplicates = (array, key) => {
                let lookup = new Set();
                return array.filter(obj => obj[key] && !lookup.has(obj[key]) && lookup.add(obj[key]));
            }
            $$("ou").getList().parse(removeDuplicates(data,'code'))
            $$("mst").getList().parse(removeDuplicates(data,'mst'))
        })
        
        $$("btnreport").attachEvent("onItemClick", () => {
            if (form.validate()) {
                $$("btnreport").disable()
                webix.message("Đang xuất báo cáo")
                const param = form.getValues()
                param.tn = type.getText()
                param.period = `${period.getText()} ${year.getValue()}`
                let ouList = []
                let mstList = []
                if(ou.getValue()) ou.getValue().split(",").forEach(e=>{ouList.push(ou.getList().getItem(e).code)})
                if(mst.getValue()) mst.getValue().split(",").forEach(e=>{mstList.push(mst.getList().getItem(e).mst)})
                param.ou = ouList.join(",")
                param.mst = mstList.join(",")
                param.reportext = 1
                webix.ajax().response("blob").get("api/rpt", param).then(data => { webix.html.download(data, `${$$("reports").getText()}.xlsx`) })
                setTimeout(()=>{$$("btnreport").enable()},3000)
            }
        })
    }

}