import { JetView } from "webix-jet"
import { ITYPE, filter, ENT, CKSTATUS, setConfStaFld ,OU, ddate, ROLE,INV_CQT_STATUS} from "models/util"
export default class ReportView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        const LinkedInputs4 = {
            $init: function (config) {
                let master = $$(config.master)
                master.attachEvent("onChange", (newv, oldv) => {
                    if (oldv == newv) return
                    let me = this, list
                    const all = { id: "*", value: _("all")}
                    let fhs = ["fhs","fbc"].includes(ENT) ? {id:"1", value: _(ENT)} : {}
                    list = this.getList()
                    list.clearAll()
                    if (newv && newv == "1"){
                        list.add(fhs,0)
                        this.setValue("1")
                    }else{
                        list.load(config.dependentUrl + newv)
                        list.add(all,0)
                        this.setValue("*")
                    }
                })
            }
        }
        webix.protoUI({ name: "dependent", $cssName: "combo" }, LinkedInputs4, webix.ui.combo)
        let REPORTS = [
            { id: 1, value: ["fhs","fbc"].includes(ENT) ? _("report_vat_out_fhs") : _("report_vat_out") },
            { id: 2, value: ["fhs","fbc"].includes(ENT) ? _("report_inv_sel_fhs") : _("report_inv_sel") },
            { id: 3, value: _("report_inv_use") }
          
        ]
        if(["vcm","baca"].includes(ENT)) REPORTS = [
            { id: 1, value: _("report_vat_out") },
           
            { id: 3, value: _("report_inv_use") },

            { id: 11, value: _("report_inv_einvoice_detail") }
          
        ]
        if(["mzh"].includes(ENT))REPORTS = [
            { id: 1, value: _("report_vat_out") },
          
            { id: 3, value: _("report_inv_use") },
          
        ]
        let BCTHTYPE = [
            { id: "BCTH_OU_FHS", value: _("general_report_inv_list_byou") },
            { id: "BCTH_DT_FHS", value: _("general_report_inv_list_bymonth") },
          
        ]
        if (ENT == "baca") REPORTS.push({ id: 17, value: _("report_internal") }, { id: 18, value: _("report_period")})
        if (ENT == "vib") REPORTS.push({ id: 16, value: _("report_exreport") })
        if (ENT == "hdb"|| ENT == "baca") REPORTS.push({ id: 4, value: _("report_inv_amount")})
        if ( ["hlv"].includes(ENT)) REPORTS.push({ id: 5, value: _("report_detail") })
       
        if (["fhs","fbc"].includes(ENT)) REPORTS.push({ id: 6, value: _("report_inv_cancel_detail2") },{ id: 7, value: _("report_inv_cancel_detail") },{ id: 12, value: _("report_GTGT") },{ id: 13, value: _("general_report_inv_list_fhs") }, {id: 15, value: _("report_inv_fl_stt")})        
        if (["vcm", "bvb"].includes(ENT)) REPORTS.push({ id: 8, value: _("report_inv_cancel_detail") },{ id: 9, value: _("report_inv_adjust_detail") },{ id: 10, value: _("report_inv_replace_detail") })
        if (ENT == "aia") REPORTS.push({ id: 8, value: _("report_inv_cancel_detail") }, { id: 14, value: _("report_decrease_sales") })
        if (ENT == "bvb") REPORTS.push({ id: 20, value: _("report_list_transactions") }, { id: 21, value: _("summary_report_of_revenue_by_customer") })
        if ( ENT == "ghtk"|| ENT == "vhc") REPORTS.push({ id: 19, value: _("report_detail") })
        const q = _("quarter"), m = _("month")

        const PERIODS = [
            { id: "d", value: _("home_period7") },
            { id: "1", value: `${m} 01` },
            { id: "2", value: `${m} 02` },
            { id: "3", value: `${m} 03` },
            { id: "4", value: `${m} 04` },
            { id: "5", value: `${m} 05` },
            { id: "6", value: `${m} 06` },
            { id: "7", value: `${m} 07` },
            { id: "8", value: `${m} 08` },
            { id: "9", value: `${m} 09` },
            { id: "10", value: `${m} 10` },
            { id: "11", value: `${m} 11` },
            { id: "12", value: `${m} 12` },
            { id: "q1", value: `${q} 1` },
            { id: "q2", value: `${q} 2` },
            { id: "q3", value: `${q} 3` },
            { id: "q4", value: `${q} 4` },
            { id: "y", value: _("year") },
        ]
        const PERIODSMORE = [
            { id: "d", value: _("home_period7") },
            { id: "1", value: `${m} 01` },
            { id: "2", value: `${m} 02` },
            { id: "3", value: `${m} 03` },
            { id: "4", value: `${m} 04` },
            { id: "5", value: `${m} 05` },
            { id: "6", value: `${m} 06` },
            { id: "7", value: `${m} 07` },
            { id: "8", value: `${m} 08` },
            { id: "9", value: `${m} 09` },
            { id: "10", value: `${m} 10` },
            { id: "11", value: `${m} 11` },
            { id: "12", value: `${m} 12` },
        ]

        let elements_vcm = 
            [
                //dung chung cho tat ca
                {
                    cols: [
                        { id: "report", name: "report", label: _("report"), value: 1, view: "combo", options: REPORTS, gravity: 2, required:true },
                        { id: "type", name: "type", label: _("invoice_type"), value: "01GTKT", disabled: true, view: "combo", options: ITYPE(this.app), gravity: 2, required:true }
                    ]
                },
                {
                    cols: [
                        { id: "period", label: _("home_period"), value: "d", view: "combo", options: PERIODS },
                        { id: "year", label: _("year"), view: "combo", options: [] },
                        { id: "fd", name: "fd", label: ENT == "aia" ? _("date") : _("fd"), view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr, required:true },
                        { id: "td", name: "td", label: _("td"), view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr, required:true },
                        {id:"fix-width"}
                    ]
                },
                //
                {
                    cols: [
                            { id: "ou", name: "ou", label: _("ou"), view: "multicombo", suggest: { url: "api/ous/bytokenou", filter: filter, selectAll: true, template: webix.template("__#id#__#value#") }, gravity: 2,required :true  }
                        ]
                },
                {
                    cols: [
                        {
                            cols: [
                                { id: "serial1:serial", name: "serial", label: _("serial"), view: "multiselect", suggest: { selectAll: true, url: "api/serial/alls" } }
    
                            ]
                        },
                        {
                            cols: [
                                
                                {
                                    view: "radio",
                                    label: _("type_rp"),
                                    id: "type_date",
                                    name: "type_date",
                                    width: 400 ,
                                    hidden: true,
                                    value: 1, options: [
                                        { "id": 1, "value": _("type_cdt") }, // the initially selected item
                                        { "id": 2, "value": _("type_dt") }
                                    ]
                                }
    
                            ]
                        },
                        {
                            cols: [{}, {
                                rows: [
                                    //{ view: "button", id: "btnxml", type: "icon", icon: "mdi mdi-file-xml", label: "Xml", width: 100 },
                                    { view: "button", id: "btnreport", type: "icon", icon: "wxi-download", label: _("report"), width: 100, disable: ROLE.includes('PERM_REPORT_INV') ? false : true }]
                            }
                            ]
                        }

                        ]
                }
                

            ]
        let elements_baca= [
            //dung chung cho toan bo
            {
                cols: [
                    { id: "report", name: "report", label: _("report"), value: 1, view: "combo", options: REPORTS, gravity: 2, required:true },
                    { id: "type", name: "type", label: _("invoice_type"), value: "01GTKT", disabled: true, view: "combo", options: ITYPE(this.app), gravity: 2, required:true }
                ]
            },
            {
                cols: [
                    { id: "period", label: _("home_period"), value: "d", view: "combo", options: PERIODS },
                    { id: "year", label: _("year"), view: "combo", options: [] },
                    { id: "fd", name: "fd", label: ENT == "aia" ? _("date") : _("fd"), view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr, required:true },
                    { id: "td", name: "td", label: _("td"), view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr, required:true },
                    {id:"fix-width"}
                ]
            }
            //
            ,
                {
                    cols: [
                        { id: "ou", name: "ou", label: _("ou"), view: "combo", suggest: { url: "api/ous/bytokenou", filter: filter }, gravity: 2 },
                        { view: "combo", id: "report:status", name: "status", label: _("status"),  options: CKSTATUS(this.app),gravity: 2, hidden:true }
                        ]
                },
                {
                    cols: [
                        {
                            cols: [
                                {
                                    view: "radio",
                                    label: _("type_rp"),
                                    id: "type_date",
                                    name: "type_date",
                                    hidden: true,
                                    value: 1, options: [
                                        { "id": 1, "value": _("type_cdt") }, // the initially selected item
                                        { "id": 2, "value": _("type_dt") }
                                    ]
                                }
                                ]
                        },
                        {
                            cols: [{}, {
                                rows: [
                                    //{ view: "button", id: "btnxml", type: "icon", icon: "mdi mdi-file-xml", label: "Xml", width: 100 },
                                    { view: "button", id: "btnreport", type: "icon", icon: "wxi-download", label: _("report"), width: 100, disable: ROLE.includes('PERM_REPORT_INV') ? false : true }]
                            }
                            ]
                        }
                    ]
                }
            ]
        let elements_fhs =[
            //dung chung cho toan bo
            {
                cols: [
                    { id: "report", name: "report", label: _("report"), value: 1, view: "combo", options: REPORTS, gravity: 2, required:true },
                    { id: "type", name: "type", label: _("invoice_type"), value: "01GTKT", disabled: true, view: "combo", options: ITYPE(this.app), gravity: 2, required:true }
                ]
            },
            {
                cols: [
                    { id: "period", label: _("home_period"), value: "d", view: "combo", options: PERIODSMORE },
                    { id: "year", label: _("year"), view: "combo", options: [] },
                    { id: "fd", name: "fd", label: ENT == "aia" ? _("date") : _("fd"), view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr, required:true },
                    { id: "td", name: "td", label: _("td"), view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr, required:true },
                    {id:"fix-width"}
                ]
            }
            //
            ,
            {
                cols: [
                    {
                        cols: [
                            // { id: "ou", name: "ou", label: _("ou"), view: "multicombo", suggest: { url: "api/ous/bytokenou", filter: filter, selectAll: true, template: webix.template("__#id#__#value#") } },
                            { id: "ou2", name: "ou2", label: _("ou2"), view: "combo", suggest: { url: "api/ous/getou", filter: filter, selectAll: true }, required: true },
                            { id: "ou", name: "ou", label: _("ou"), view: "dependent", options: [], master: "ou2", dependentUrl: "api/ous/getbranch?id=", required: true },
                            // làm riêng cho GTGT
                            // { id: "ougt2", name: "ougt2", label: _("ou2"), view: "combo", suggest: { url: "api/ous/getou", filter: filter, selectAll: true }, required: true, hidden: true },
                            { id: "ougt", name: "ougt", label: _("ou"), view: "combo", suggest: { url: "", filter: filter, selectAll: true }, required: true,hidden: true },
                            //
                            { id: "outk", name: "outk", label: _("ou"), view: "combo", suggest: { url: "api/ous/bytokenou", filter: filter, relative: "bottom", template: webix.template("__#id#__#value#") }, required: true, hidden: true }
                            //ou chỉ làm riêng cho tờ khai GTGT Fahasa chỉ chọn một. ChinhTX
                        ]
                    },
                    // {
                    //     cols: [
                            
                    //         // làm riêng cho GTGT
                    //         { id: "ou4", name: "ou4", label: _("ou2"), view: "combo", suggest: { url: "api/ous/getou", filter: filter, selectAll: true }, required: true },
                    //         { id: "ou3", name: "ou3", label: _("ou"), view: "dependent", options: [], master1: "ou4", dependentUrl: "", required: true }
                    //         //
                    //     ]
                    // }
                ]
            },
            {
                cols: [
                    { id: "chktkgtgt", name: "chktkgtgt", label: "MS Word", view: "checkbox", checkValue: 1, value: 1, hidden: true },
                    { rows: [{ id: "bcthtype", name: "bcthtype", label: _("general_report_inv_list_type"), hidden: true, value: "BCTH_OU_FHS", view: "combo", options: BCTHTYPE, required: true }, {}] },
                    {
                        cols: [{}, {
                            rows: [
                                //{ view: "button", id: "btnxml", type: "icon", icon: "mdi mdi-file-xml", label: "Xml", width: 100 },
                                { view: "button", id: "btnreport", type: "icon", icon: "wxi-download", label: _("report"), width: 100, disable: ROLE.includes('PERM_REPORT_INV') ? false : true }]
                        }
                        ]
                    }
                    

                ]
            }
        ]
        let elements_bvb =[
        
            //dung chung cho toan bo
            {
                cols: [
                    { id: "report", name: "report", label: _("report"), value: 1, view: "combo", options: REPORTS, gravity: 2, required:true },
                    { id: "type", name: "type", label: _("invoice_type"), value: "01GTKT", disabled: true, view: "combo", options: ITYPE(this.app), gravity: 2, required:true }
                ]
            },
            {
                cols: [
                    {cols:[
                        { id: "period", label: _("home_period"), value: "d", view: "combo", options: PERIODS },
                        { id: "year", label: _("year"), view: "combo", options: [] },
                        { id: "ou", name: "ou", label: _("ou"), view: "combo", suggest: { url: "api/ous/getou", filter: filter, selectAll: true }, required: true, hidden: true},
                    ]},
                    {cols:[
                        { id: "fd", name: "fd", label: ENT == "aia" ? _("date") : _("fd"), view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr, required:true },
                        { id: "td", name: "td", label: _("td"), view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr, required:true },
                    ]}
                    
                    
                ]
            }
            //
            ,
            
            {
                cols: [
                    {
                        cols: [{
                            cols:[{ id: "formbvb", name: "form", label: _("form"), view: "combo", suggest: { url: "", filter: filter, selectAll: true }, required: false, hidden:true},
                            { id: "serial", name: "serial", label: _("serial"), view: "combo", suggest: { url: "", filter: filter, selectAll: true }, required: false , hidden:true}]
                            },
                            {
                                cols: [{}, {
                                    rows: [
                                        { view: "button", id: "btnreport", type: "icon", icon: "wxi-download", label: _("report"), width: 100, disable: ROLE.includes('PERM_REPORT_INV') ? false : true }]
                                }
                                ]
                            }
                        ]
                    }
                        
                ]
            }   
        ]
        let elements_more =[
        
            //dung chung cho toan bo
            {
                cols: [
                    { id: "report", name: "report", label: _("report"), value: 1, view: "combo", options: REPORTS, gravity: 1, required:true },
                    { id: "cqtstatus", name: "cqtstatus", label: _("cqtstatus_report"), view: "richselect", options: INV_CQT_STATUS(this.app), gravity: 1, required: true},
                    { id: "type", name: "type", label: _("invoice_type"), value: "01GTKT", disabled: true, view: "combo", options: ITYPE(this.app), gravity: 2, required:true }
                ]
            },
            {
                cols: [
                    { id: "period", label: _("home_period"), value: "d", view: "combo", options: PERIODS },
                    { id: "year", label: _("year"), view: "richselect", options: [] },
                    { id: "fd", name: "fd", label: ENT == "aia" ? _("date") : _("fd"), view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr, required:true },
                    { id: "td", name: "td", label: _("td"), view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr, required:true },
                    {id:"fix-width"}
                ]
            },
            {
                cols: [
                    {
                        cols: [

                            {
                                cols: [{}, {
                                    rows: [
                                        //{ view: "button", id: "btnxml", type: "icon", icon: "mdi mdi-file-xml", label: "Xml", width: 100 },
                                        { view: "button", id: "btnreport", type: "icon", icon: "wxi-download", label: _("report"), width: 100, disable: ROLE.includes('PERM_REPORT_INV') ? false : true }]
                                }
                                ]
                            }
                        ]
                    }
                        
                ]
            }   
        ]
        let elements1 = []
        if(["fhs","fbc"].includes(ENT)){ elements1 = elements_fhs}
        else if(ENT == "vcm"){ elements1 = elements_vcm}
        else if(ENT == "bvb"){ elements1 = elements_bvb}
        else if(ENT == "baca"){ elements1= elements_baca}
        else {elements1=elements_more}

        const form = {
            view: "form",
            id: "report:form",
            padding: 3,
            margin: 3,
            elementsConfig: { labelWidth: 100 },
            elements: elements1
            , rules: {
                fd: webix.rules.isNotEmpty,
                td: webix.rules.isNotEmpty,
                $obj: function (data) {
                    const fd = data.fd, td = data.td
                    if (fd > td) {
                        webix.message(_("date_invalid_msg"))
                        return false
                    }
                    const dtime = (new Date(td)).getTime() - (new Date(fd)).getTime()
                    let test=$$("report").getValue()
                    if ((ENT == "bvb") && test == 20 && (dtime > 7776000000 )) {
                        webix.message(_("dt_search_between_month"),"error")
                        return false
                    }
                    if (dtime > 31622400000) {
                        webix.message(_("dt_search_between_year"))
                        return false
                    }
                    return true
                }
            }
        }
        let title_page = { view: "label", label:`<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("reports")}</span>`}
        let colsall = {cols: [{}, { gravity: 6, rows: [form, {}] }, {}]}
        return { paddingX: 2, rows: [title_page, colsall] }
        
    }
    ready(v, urls) {
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
    init() {
        if (!ROLE.includes('PERM_REPORT_INV')) {
            this.show("/top/home")
            window.location.replace(`/${homepage}`)
            return
          }
        const all = { id: "*", value: _("all") }
        if (ENT == "baca") {
            $$("report:status").getList().add(all, 0)
            $$("report:status").setValue("*")
        }
        $$("cqtstatus").getList().add(all, 0)
        $$("cqtstatus").setValue("*")
        let date = new Date(), yr = date.getFullYear(), firstDay = new Date(yr, date.getMonth(), 1)
        let form = $$("report:form"), report = $$("report"),ougt=$$("ougt"),serial=$$("serial"),formbvb=$$("formbvb"),ou2=$$("ou2"),ou=$$("ou"), type = $$("type"), period = $$("period"), year = $$("year"), fd = $$("fd"), td = $$("td"), firstlist = $$("firstlist"), updatelist = $$("updatelist")
        let years = [], i = 2018
        while (i <= yr) {
            years.push({ id: i, value: i })
            i++
        }
        let list = year.getPopup().getList()
        list.clearAll()
        list.parse(years)
        year.setValue(yr)
        fd.setValue(firstDay)
        td.setValue(date)
        type.getList().add(all, 0)
        // if (ENT == "vcm" ){
        //     $$("ou").getList().add(all, 0)
        //     $$("ou").setValue("*")
        // }
        if (["fhs","fbc"].includes(ENT) && OU== 1  ){
                let fhs = {id: "1" , value: _(ENT)}
                $$("ou2").getList().add(fhs, 0)
                $$("ou2").setValue("1")
                // $$("ougt2").getList().add(fhs, 0)
                // $$("ougt2").setValue("1")
            
        }
      
        const cal = (v, yr) => {
            let b = (v != "d"), tn, dn, mm
            if (b) {
                let v1 = v.substr(0, 1)
                if (v1 === "y") {
                    tn = new Date(yr, 0, 1)
                    dn = new Date(yr, 11, 31)
                }
                else if (v1 === "q") {
                    let v2 = parseInt(v.substr(1))
                    mm = 3 * (v2 - 1)
                    tn = new Date(yr, mm, 1)
                    dn = new Date(yr, mm + 3, 1)
                    dn.setDate(dn.getDate() - 1)
                }
                else {
                    mm = parseInt(v)
                    tn = new Date(yr, mm - 1, 1)
                    dn = new Date(yr, mm, 1)
                    dn.setDate(dn.getDate() - 1)
                }

            }
            else {
                mm = date.getMonth()
                if (!["fhs","fbc"].includes(ENT)) 
                    tn = new Date(yr, mm - 1, 1)
                else
                    tn = new Date(yr, mm, 1)
                dn = new Date(yr, mm, date.getDate())
            }
            fd.setValue(tn)
            td.setValue(dn)
            fd.config.readonly = b
            td.config.readonly = b
            fd.refresh()
            td.refresh()
        }

        year.attachEvent("onChange", (v) => { cal(period.getValue(), v) })
        period.attachEvent("onChange", (v) => { cal(v, year.getValue()) })
        
        const setoubyreport = (newv, oldv) => {
            if (oldv == newv) return
            let fhs = ["fhs","fbc"].includes(ENT) ? {id:"1", value: _(ENT)} : {}
            let all = {id:"*", value: _("all")}
            let list = ougt.getPopup().getList()
            if(newv=="1"){
                list.load(ougt.url="api/ous/getou" )
                list.clearAll()
                list.add(fhs,0)
                list.add(all,0)
                ougt.setValue("*")
            }else{
                list.load(ougt.url="api/ous/getbranch?id=" + newv )
                list.clearAll()
                list.add(all,0)
                ougt.setValue("*")
            }

        }

        const setitembyreport = (newv, oldv) => {
            if (oldv == newv) return
            const arrperiods = [
                { id: "y", value: _("year") },
                { id: "q4", value: _("quarter_4") },
                { id: "q3", value: _("quarter_3") },
                { id: "q3", value: _("quarter_3") },
                { id: "q2", value: _("quarter_2") },
                { id: "q1", value: _("quarter_1") },
                { id: "12", value: _("month_12") },
                { id: "11", value: _("month_11") },
                { id: "10", value: _("month_10") },
                { id: "9", value: _("month_9") },
                { id: "8", value: _("month_8") },
                { id: "7", value: _("month_7") },
                { id: "6", value: _("month_6") },
                { id: "5", value: _("month_5") },
                { id: "4", value: _("month_4") },
                { id: "3", value: _("month_3") },
                { id: "2", value: _("month_2") },
                { id: "1", value: _("month_1") },
                { id: "d", value: _("home_period7") }
            ]

            if (newv == 1) {
                type.setValue("01GTKT")
                type.disable()
            } else {
                type.enable()
            }
            if (ENT == "vcm") {
                let type = $$("report").getValue();

                if (type == 3) $$("type_date").show()
                else {
                    $$("type_date").hide()
                    $$("type_date").setValue(1)
                }
            }
            if (["fhs", "fbc"].includes(ENT)) {
                let typefhs = $$("report").getValue();

                if (typefhs == 1 || typefhs == 2 || typefhs == 15) {
                    let list = period.getPopup().getList()
                    list.clearAll()
                    let peri
                    for (peri of arrperiods) {
                        if ((String(peri.id).indexOf("q") < 0) && (String(peri.id).indexOf("y") < 0)) {
                            list.add(peri, 0)
                        }
                    }
                } else {
                    let list = period.getPopup().getList()
                    list.clearAll()
                    let peri
                    for (peri of arrperiods) list.add(peri, 0)
                }

                if (typefhs == 3) {
                    $$("chktkgtgt").hide()
                    $$("chktkgtgt").define("label", "MS Excel")
                    $$("chktkgtgt").show()
                    $$("chktkgtgt").enable()
                    $$("chktkgtgt").setValue(1)
                    /*
                    $$("outk").hide()
                    $$("ou").show()
                    $$("ou2").show()
                    $$("ougt").hide()
                    // $$("ougt2").hide()
                    */
                    $$("ou").hide()
                    $$("ou2").show()
                    $$("ougt").show()
                    // $$("ougt2").show()
                    $$("outk").hide()
                    $$("bcthtype").hide()

                } else if (typefhs == 12) {
                    $$("chktkgtgt").hide()
                    $$("chktkgtgt").define("label", "MS Word")
                    $$("chktkgtgt").show()
                    $$("chktkgtgt").setValue(1)
                    $$("chktkgtgt").disable()
                    $$("outk").show()
                    $$("ou").hide()
                    $$("ou2").hide()
                    $$("ougt").hide()
                    // $$("ougt2").hide()
                    $$("bcthtype").hide()

                } else if (typefhs == 6 || typefhs == 7 || typefhs == 13) {
                    $$("bcthtype").show()
                    $$("ou").hide()
                    $$("ou2").show()
                    $$("ougt").show()
                    // $$("ougt2").show()
                    $$("outk").hide()
                    $$("chktkgtgt").hide()
                    /*
                    let fhs = ["fhs","fbc"].includes(ENT) ? {id:"1", value: _(ENT)} : {}
                    let all = {id:"*", value: _("all")}
                    let list = ougt.getPopup().getList()
                    list.load(ougt.url="api/ous/getou" )
                    list.clearAll()
                    list.add(fhs,0)
                    list.add(all,0)
                    ougt.setValue("*")
                    */
                    if (typefhs == 6 || typefhs == 7)
                        $$("bcthtype").hide()
                    else
                        $$("bcthtype").show()
                }
                else {
                    $$("chktkgtgt").hide()
                    $$("outk").hide()
                    $$("ou").show()
                    $$("ou2").show()
                    /////////////
                    $$("bcthtype").hide()
                    $$("ougt").hide()
                    // $$("ougt2").hide()
                }

                if (typefhs == 1 || typefhs == 2 || typefhs == 12 || typefhs == 13) {
                    $$("type").setValue("01GTKT")
                    $$("type").disable()
                }
                else {
                    $$("type").enable()
                }
                setoubyreport($$("ou2").getValue(), '')
            }
            if (ENT == "aia") {
                // { id: "fd", name: "fd", label: _("date"), view: "datepicker", format: webix.i18n.dateFormatStr, stringResult: true, required:true} // chọn ngày riêng cho aia
                let type = $$("report").getValue();

                if (type == 14) {
                    $$("period").hide()
                    $$("year").hide()
                    $$("td").hide()
                    $$("fd").config.width = 200;
                    $$("fix-width").show()
                } else {
                    $$("period").show()
                    $$("year").show()
                    $$("fd").show()
                    $$("td").show() //
                    $$("fix-width").hide()
                }
            }
            if (ENT == "baca") {
                let type = $$("report").getValue()
                if (type == 17 || type == 18) {
                    $$("ou").define("suggest", "api/ous/bytokenouba");
                    $$("ou").refresh();
                    $$("ou").getList().add(all, 0)
                    $$("ou").setValue("*")
                }

                if (type != 17 && type != 18) {
                    $$("ou").define("suggest", "api/ous/bytokenou");
                    $$("ou").refresh();
                }
                if (type == 18) {
                    this.$$("report:status").show()
                }
                else {
                    this.$$("report:status").hide()
                }
            }
            if (ENT == "bvb") {
                let typebvb = $$("report").getValue()

                if (typebvb == 20 || typebvb == 21) {
                    $$("period").hide()
                    $$("year").hide()
                    $$("ou").show()
                    $$("formbvb").show()
                    $$("serial").show()
                    $$("serial").disable()

                }
                else {
                    $$("period").show()
                    $$("year").show()
                    $$("ou").hide()
                    $$("formbvb").hide()
                    $$("serial").hide()


                }

            }
            if(newv == 3) {
                $$("cqtstatus").hide()
                $$("report").define({gravity:1})
                $$("report").refresh();
            } else {
                $$("cqtstatus").show()
                $$("report").define({gravity:2})
                $$("report").refresh();
            }
        }
        if (ENT == "bvb"){
            let listfm = formbvb.getPopup().getList()
            let listsr = serial.getPopup().getList()
            let newv1 = null, newv2 = null, newv3= null
            let bvb = {id: "1" , value: _("bvb")}
            ou.getList().add(bvb, 0)
            const setformbytype = () => {
                const list = form.getValues()
                let param = []
                for(i in list){
                    if(i == "type"||i == "ou"||i == "report"){
                        param.push(`${i}=${list[i]}`)
                    }
                }
                param = param.join('&')
                listfm.clearAll()
                listfm.load(formbvb.url="api/sea/fbtrp?"+ param) // api/sea/fbt?id=21&name=xcv
                // formbvb.setValue(" ")
            }
            const setserialbyform = () => {
                const list = form.getValues()
                let param = []
                for(i in list){
                    if(i == "form"||i == "ou"||i == "report"){
                        param.push(`${i}=${list[i]}`)
                    }
                }
                param = param.join('&')
                listsr.clearAll()
                listsr.load(serial.url="api/sea/sbfrp?"+ param) // api/sea/fbt?id=21&name=xcv
                // serial.setValue(" ")
            }
            report.attachEvent("onChange", (newv) => {
                if(newv== 20 || newv == 21){
                    ou.setValue(OU)
                    type.setValue("01GTKT")
                    formbvb.setValue(" ")
                    setformbytype()
                }
            })
            
            // set gia tri cho form
            type.attachEvent("onChange", (newv) => {
                // newv1 = newv
                // if(newv1.trim() && newv2 && newv2.toString().trim()){
                //     setformbytype()
                //     formbvb.enable()
                //     formbvb.setValue(" ")
                // }else{
                //     formbvb.disable()
                // }
                setformbytype()
                formbvb.setValue(" ")

            })
            ou.attachEvent("onChange", (newv) => {
                newv2 = newv
                // if(newv1 && newv2.toString().trim()){
                //     setformbytype()
                //     formbvb.enable()
                //     formbvb.setValue(" ")

                // }else{
                //     formbvb.disable()
                // }
                setformbytype()
                formbvb.setValue(" ")

            })
            //
            formbvb.attachEvent("onChange", (newv) => {
                newv3 = newv
                if(newv2 && newv3.trim() ){
                    setserialbyform()
                    serial.enable()
                }else{
                    serial.disable()
                    serial.setValue(" ")

                }
                // setserialbyform()

            })
            
            
        }
        report.attachEvent("onChange", (newv, oldv) => {
            setitembyreport(newv, oldv)
        })
        if (["fhs","fbc"].includes(ENT)) {
            ou2.attachEvent("onChange", (newv, oldv) => {
                setoubyreport(newv, oldv)
            })
        }

        $$("btnreport").attachEvent("onItemClick", () => {
            if (form.validate()) {
                if (($$("report").getValue() == 12 || ($$("report").getValue() == 3 && ["fhs","fbc"].includes(ENT))) && ["d","y"].includes($$("period").getValue())) {
                    webix.message(_("period_report_GTGT"))
                    return false
                }
                if(!$$("cqtstatus").getValue()){
                    webix.message(_("cqt_status_require"))
                    return false
                }
                const fd = $$("fd").getValue(), td = $$("td").getValue()
                if (($$("report").getValue() == 1 || $$("report").getValue() == 2 || $$("report").getValue() == 15) && (["fhs","fbc"].includes(ENT)))  {
                    if (String(fd).substr(0, 7) != String(td).substr(0, 7)) {
                        webix.message(_("fd_td_report_in_month"))
                        return false
                    }
                }
                if (($$("report").getValue() == 1 || $$("report").getValue() == 2) && (["vib"].includes(ENT)))  {
                    const dtime = (new Date(td)).getTime() - (new Date(fd)).getTime()
                    if ((ENT == "vib") && (dtime > (ddate/2))) {
                        webix.message(String(_("dt_search_between_day")).replace('#hunglq#',(ddate/(1000*3600*24*2))))
                        return false
                    }
                }
                webix.message(_("export_report"),"info", -1, "exp")
                const param = form.getValues()
                if ($$("report").getValue() == 12) param.ou = $$("outk").getValue()
                param.tn = type.getText()
                param.period = `${period.getText()} ${year.getValue()}`
                param.period_id = period.getValue()
                if (["fhs","fbc"].includes(ENT)) {
                    if ($$("report").getValue() == 3 || $$("report").getValue() == 6 || $$("report").getValue() == 7 || $$("report").getValue() == 13) param.ou = $$("ougt").getValue()    
                }
                try {
                    show_progress_icon(0)
                    if ($$("report").getValue() == 12) {
                        webix.ajax().response("blob").post("api/rpt", param).then(data => {
                            let vchktkgtgt = $$("chktkgtgt").getValue()

                            webix.html.download(data, vchktkgtgt ? `tktgtgt.docx` : `tktgtgt.xml`)
                            webix.message.hide("exp");
                            $$("report:form").hideProgress()
                        }).catch(e => {
                            webix.message.hide("exp");
                            $$("report:form").hideProgress()
    
                        })
                    } else if ($$("report").getValue() == 3) {
                        webix.ajax().response("blob").post("api/rpt", param).then(data => {
                            let vchktkgtgt = (["fhs","fbc"].includes(ENT)) ? $$("chktkgtgt").getValue() : '1'

                            webix.html.download(data, vchktkgtgt ? `${webix.uid()}.xlsx` : `BC26.xml`)
                            webix.message.hide("exp");
                            $$("report:form").hideProgress()
                        }).catch(e => {
                            webix.message.hide("exp");
                            $$("report:form").hideProgress()
    
                        })
                    } else if ($$("report").getValue() == 1) {
                        if (ENT != "vib") {
                            webix.ajax().response("blob").post("api/rpt", param).then(data => {
                                webix.html.download(data, `${webix.uid()}.xlsx`)
                                webix.message.hide("exp");
                                $$("report:form").hideProgress()
                            }).catch(e => {
                                webix.message.hide("exp");
                                $$("report:form").hideProgress()
        
                            })
                        } else {
                            webix.ajax().post("api/rpt", param).then(data => {
                                webix.message.hide("exp");
                                $$("report:form").hideProgress()
                                const result = data.json()
                                if (result.err)
                                    webix.alert({
                                        ok: "Ok",
                                        text: result.err
                                    })
                                else {
                                    const blob = new Blob([result.data])
                                    webix.html.download(blob, `${webix.uid()}.csv`)
                                }
                            }).catch(e => {
                                webix.message.hide("exp");
                                $$("report:form").hideProgress()
        
                            })
                        }
                        
                    } else if ($$("report").getValue() == 2) {
                        if (ENT != "vib") {
                            webix.ajax().response("blob").post("api/rpt", param).then(data => {
                                webix.html.download(data, `${webix.uid()}.xlsx`)
                                webix.message.hide("exp");
                                $$("report:form").hideProgress()
                            }).catch(e => {
                                webix.message.hide("exp");
                                $$("report:form").hideProgress()
        
                            })
                        } else {
                            webix.ajax().post("api/rpt", param).then(data => {
                                webix.message.hide("exp");
                                $$("report:form").hideProgress()
                                const result = data.json()
                                if (result.err)
                                    webix.alert({
                                        ok: "Ok",
                                        text: result.err
                                    })
                                else {
                                    const blob = new Blob([result.data])
                                    webix.html.download(blob, `${webix.uid()}.csv`)
                                }
                            }).catch(e => {
                                webix.message.hide("exp");
                                $$("report:form").hideProgress()
        
                            })
                        }
                    }
                    else {
                        webix.ajax().response("blob").post("api/rpt", param).then(data => {
                            webix.html.download(data, `${webix.uid()}.xlsx`)
                            webix.message.hide("exp");
                            $$("report:form").hideProgress()
                        }).catch(e => {
                            webix.message.hide("exp");
                            $$("report:form").hideProgress()
    
                        })
                    }
                } catch (err) {
                    $$("report:form").hideProgress()
                    webix.message(err.message, "error")
                }
            }
        })

        webix.extend($$("report:form"), webix.ProgressBar);

        function show_progress_icon(delay){
            //Lấy trạng thái enabled
            let v_report_enable = $$("report").isEnabled()
            let v_type_enable = $$("type").isEnabled()
            let v_period_enable = $$("period").isEnabled()
            let v_year_enable = $$("year").isEnabled()
            let v_fd_enable = $$("fd").isEnabled()
            let v_td_enable = $$("td").isEnabled()

            $$("report").disable()
            $$("type").disable()
            $$("period").disable()
            $$("year").disable()
            $$("fd").disable()
            $$("td").disable()
            $$("report:form").showProgress({
                type: "icon",
                delay: delay,
                hide: false
            });
            setTimeout(function(){
                if (v_report_enable) $$("report").enable()
                if (v_type_enable) $$("type").enable()
                if (v_period_enable) $$("period").enable()
                if (v_year_enable) $$("year").enable()
                if (v_fd_enable) $$("fd").enable()
                if (v_td_enable) $$("td").enable()

                let reportid = $$("report").getValue()
                setitembyreport(reportid, reportid)
            }, delay);
          };

        $$("fix-width").hide()
    }

}