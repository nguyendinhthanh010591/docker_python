import { JetView } from "webix-jet"
import { filter, ENT, setConfStaFld} from "models/util"
export default class ReportView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        let elements_scb =[
            {
                cols: [
                    { id: "fd", name: "fd", label:_("fd"), view: "datepicker", stringResult: true, editable: true,format: webix.i18n.dateFormatStr, required:true },
                    { id: "td", name: "td", label: _("td"), view: "datepicker", stringResult: true, editable: true,format: webix.i18n.dateFormatStr, required:true },
                    { id: "SYSTEM", name: "SYSTEM", label: _("system"), view: "combo", suggest: { url: "api/com", filter: filter, relative: "bottom" },required:true  }
                ]
            },
            {
                cols: [
                    {
                        cols: [

                            {
                                cols: [{}, {
                                    rows: [
                                        { view: "button", id: "btnreport", type: "icon", icon: "wxi-download", label: _("report"), width: 100 }]
                                }
                                ]
                            }
                        ]
                    }
                        
                ]
            }   
        ]
        let elements1 = []
        if(ENT == "scb"){ elements1 = elements_scb}
        const form = {
            view: "form",
            id: "repcom:form",
            padding: 3,
            margin: 3,
            elementsConfig: { labelWidth: 100 },
            elements: elements1
            , rules: {
                fd: webix.rules.isNotEmpty,
                td: webix.rules.isNotEmpty,
                $obj: function (data) {
                    const fd = data.fd, td = data.td
                    let d = new Date(), oldtd = new Date(d.getFullYear(), d.getMonth(), d.getDate() - 1),tdnew = new Date(td)   
                    if (fd > td) {
                        
                        webix.message(_("date_invalid_msg"))
                        return false
                    }
                    if(tdnew > oldtd){
                        webix.message(_("date_msg_rep"))
                        return false
                    }
                    return true
                }
            }
        }
        let title_page = { view: "label", label:`<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("report_compare")}</span>`}
        let colsall = {cols: [{}, { gravity: 6, rows: [form, {}] }, {}]}
        return { paddingX: 2, rows: [title_page, colsall] }
        
    }
    ready(v, urls) {
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
    init() {
        webix.extend($$("repcom:form"), webix.ProgressBar)
        let form = $$("repcom:form")
        const all = { id: "*", value: _("all") }
        $$("SYSTEM").getList().add(all, 0)
        $$("SYSTEM").setValue("*")
        $$("btnreport").attachEvent("onItemClick", () => {
            if (form.validate()) {
                const param = form.getValues()
                try {
                    form.disable()
                    form.showProgress({
                        type: "icon",
                        hide: false
                    })
                    webix.ajax().response("blob").get("api/com/rpt", param).then(data => {
                        webix.html.download(data, `${webix.uid()}.xlsx`)
                        webix.message.hide("exp");
                        form.enable()
                        form.hideProgress()
                    }).catch(e => {
                        webix.message.hide("exp");
                        form.hideProgress()
                    })
                } catch (err) {
                    webix.message(err.message, "error")
                }
            }
        })
    }

}