import { JetView } from "webix-jet"
import { LANGS } from "models/util"
const use_adfs = () => {
    try {
        return adfs
    }
    catch (err) {
        return 0
    }
}
const use_ent = () => {
    try {
        return ent
    }
    catch (err) {
        return ""
    }
}
export default class ResetView extends JetView {
    config() {
        const locale = this.app.getService("locale"), lang = locale.getLang()
        _ = locale._
        const form = {
            view: "form",
            id: "reset:form",
            css: "background-changepass",
            width: 300,
            elements: [
                { height: 100 },
                { name: "username", view: "text", label: _("account"), attributes: { maxlength: 50 }, placeholder: "Username" },
                //{ name: "mail", view: "text", label: "Email", type: "email", attributes: { maxlength: 50 }, placeholder: "Mail" },
                {
                    cols: [{
                        view: "button", type: "icon", icon: "wxi-sync", label: "Reset",
                        click: () => {
                            const form = $$("reset:form")
                            if (!form.validate()) return false
                            webix.ajax().post("api/reset", form.getValues()).then(result => { webix.alert(result.text()).then(() => webix.send("/", null, "GET")) }).fail(err => webix.message(err.responseText, "error"))
                        }
                    },
                    { view: "button", type: "icon", icon: "mdi mdi-exit-to-app", hotkey: 'esc', label: "Exit", click: () => window.location.replace("/") }
                    ]
                },
                { height: 100 }
            ],
            rules: {
                $all: webix.rules.isNotEmpty,
                //mail: webix.rules.isEmail
            }
        }
        //const ui = { cols: [{}, { rows: [{}, form, {}], css: "transparent-background" }, {}], css: "bg2" }
        return { cols: [{}, { rows: [{}, form, {}] }, {}] }
    }
    urlChange(urls) {
        
    }
    ready() {
        $$("reset:form").focus("username")
       
    }
    init() {
        let form = $$("reset:form")
        webix.i18n.setLocale("vi-VN")
        
    }
}