import { JetView } from "webix-jet"
import { w, h, ITYPE, setConfStaFld, ROLE } from "models/util"
let status
const THEAD = "<thead><tr><th>STT</th><th>Hình thức</th><th>Hàng hóa</th><th>Đơn vị tính</th><th>Thuế suất</th><th>Số lượng</th><th>Đơn giá</th><th>Thành tiền</th></tr></thead>"
const TR = "<tr><td style='text-align: center;'>{{line}}</td><td>{{type}}</td><td>{{name}}</td><td>{{unit}}</td><td>{{vrn}}</td><td  style='text-align: right;'>{{fn3 quantity}}</td><td style='text-align: right;'>{{fn2 price}}</td><td style='text-align: right;'>{{fn2 amount}}</td></tr>"
const TBODY = `<tbody><tr style='display:none;'><td>{{#each items}}</td></tr>${TR}<tr style='display: none;'><td>{{/each}}</td></tr></tbody>`
const VAT_THEAD = "<thead><tr><th>Loại thuế suất</th><th>Tổng thành tiền</th><th>Tổng tiền thuế</th></tr></thead>"
const VAT_TR = "<tr><td>{{vrn}}</td><td style='text-align: right;'>{{fn amtv}}</td><td style='text-align: right;'>{{fn vatv}}</td></tr>"
const VAT_TBODY = `<tbody><tr style='display:none;'><td>{{#each tax}}</td></tr>${VAT_TR}<tr style='display: none;'><td>{{/each}}</td></tr></tbody>`

const TEMPLATES = [
    { title: "Tên hóa đơn", content: "{{name}}" },
    { title: "Ngày lập dd/mm/yyyy", content: "{{fd idt}}" },
    { title: "Ngày lập", content: "Ngày {{substr idt 8 2}} Tháng {{substr idt 5 2}} Năm {{substr idt 0 4}}" },
    { title: "Mẫu số", content: "{{form}}" },
    { title: "Ký hiệu", content: "{{serial}}" },
    { title: "Số", content: "{{seq}}" },
    { title: "Chi tiết hóa đơn", content: `<table class='detail' style='width: 100%; border-collapse: collapse;'>${THEAD}${TBODY}</table>` },
    { title: "Thông tin thay thế", content: "{{adj.des}}" },
    { title: "Loại tiền", content: "{{curr}}" },
    { title: "Tỷ giá", content: "{{fn exrt}}" },
    { title: "Loại tiền & tỷ giá", content: "{{#compare curr '!=' 'VND'}} Loại tiền : {{curr}} Tỷ giá: {{fn exrt}}  {{/compare}}" },
    { title: "Mã tra cứu", content: "{{sec}}" },
    { title: "Hình thức thanh toán", content: "{{paym}}" },
    { title: "Tên ĐV bán", content: "{{sname}}" },
    { title: "MST ĐV bán", content: "{{stax}}" },
    { title: "Địa chỉ ĐV bán", content: "{{saddr}}" },
    { title: "Điện thoại ĐV bán", content: "{{stel}}" },
    { title: "Mail ĐV bán", content: "{{smail}}" },
    { title: "Tài khoản ĐV bán", content: "{{sacc}}" },
    { title: "Ngân hàng ĐV bán", content: "{{sbank}}" },
    //{ title: "Tài khoản & ngân hàng ĐV bán", content: "{{#if sacc}} Tài khoản {{sacc}} Ngân hàng {{sbank}} {{/if}}" },
    { title: "Họ tên người mua", content: "{{buyer}}" },
    { title: "Tên ĐV mua", content: "{{bname}}" },
    { title: "MST ĐV mua", content: "{{btax}}" },
    { title: "Địa chỉ ĐV mua", content: "{{baddr}}" },
    { title: "Điện thoại ĐV mua", content: "{{btel}}" },
    { title: "Mail ĐV mua", content: "{{bmail}}" },
    { title: "Tài khoản ĐV mua", content: "{{bacc}}" },
    { title: "Ngân hàng ĐV mua", content: "{{bbank}}" },
    //{ title: "Tài khoản & ngân hàng ĐV mua", content: "{{#if bacc}} Tài khoản {{bacc}} Ngân hàng {{bbank}} {{/if}}" },
    { title: "Ghi chú", content: "{{note}}" },
    //{ title: "Ghi chú chuỗi", content: "{{#if note}} Ghi chú {{note}} {{/if}}" },
    { title: "Chiết khấu", content: "{{fn2 discount}}" },
    //{ title: "Chiết khấu chuỗi", content: "{{#if discount}} Chiết khấu {{fn2 discount}} {{/if}}" },
    { title: "Tổng cộng thành tiền", content: "{{fn sumv}}" },
    { title: "Tổng số tiền thanh toán ", content: "{{fn totalv}}" },
    { title: "Tổng số tiền thanh toán bằng chữ", content: "{{word}}" },
    { title: "Ngày hiện tại", content: "{{now}}" },
    { title: "Chi tiết tiền thuế", content: `<table class='detail' style='width: 100%; border-collapse: collapse;'>${VAT_THEAD}${VAT_TBODY}</table>` },
    { title: "Tổng cộng tiền thuế", content: "{{fn vatv}}" }
]
class EditorWin extends JetView {
    config() {
        return {
            view: "window",
            modal: true,
            fullscreen: true,
            head: {
                view: "toolbar"
                , height: 35
                , cols: [
                    { view: "label", label: _("invoice_view") },
                    {
                        cols: [
                            {},
                            { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 25, click: () => { this.getRoot().hide() } }
                        ], fillspace: true, align: "right"
                    }
                ]
            },
            body: { id: "editor:ipdf", view: "iframe", borderless: true }
        }
    }
    show() {
        this.getRoot().show()
    }
}

export default class EditorView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        const LinkedInputs5 = {
            $init: function (config) {
                let master = $$(config.master), list
                master.attachEvent("onChange", newv => {
                    let me = this
                    me.setValue()
                    list = me.getList()
                    list.clearAll()
                    if (newv) {
                        const nhap = { id: `${newv}0000`, value: _("temp_draft") }
                        list.add(nhap)
                        list.load(config.dependentUrl + newv).then(data => {
                            me.setValue(nhap)
                        })
                    }
                })
            }
        }
        webix.protoUI({ name: "dependent", $cssName: "richselect" }, LinkedInputs5, webix.ui.richselect)
        const xtype = { id: "editor:type", name: "type", label: _("invoice_type"), view: "combo", options: ITYPE(this.app), gravity: 2 }
        const xform = { id: "editor:form", name: "form", label: _("form"), view: "dependent", options: [], master: "editor:type", dependentUrl: "api/sea/fbt/editor?id=", width: 200 }
        const xserial = { id: "editor:serial", name: "serial", label: _("serial"), view: "dependent", options: [], master: "editor:form", dependentUrl: "api/sea/sbf/editor?id=", width: 200 }
        let xbtnview = { id: "editor:btnview", view: "button", type: "icon", icon: "wxi-search", label: _("temp_view"), width: 100, disabled: (!ROLE.includes('PERM_SYS_TEMPLATE_VIEW')) ? true : false }
        let xbtnexport = { id: "editor:btnexport", view: "button", type: "icon", icon: "mdi mdi-file-send", label: _("temp_export"), width: 100, disabled: (!ROLE.includes('PERM_SYS_TEMPLATE_DOWLOAD')) ? true : false }
        let xbtnapp = { id: "editor:btnapp", view: "button", type: "icon", icon: "wxi-check", label: _("temp_approve"), width: 100, hidden: true, disabled: (!ROLE.includes('PERM_SYS_TEMPLATE_APPROVE')) ? true : false }
        const xidx = { view: "text", id: "editor:idx", type: "number", label: _("stt"), value: 1, attributes: { step: 1, min: 1, max: 99, maxlength: 2 }, inputAlign: "right", width: 150, disabled: true }
        const title_page = { view: "label", label:`<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("invoice_template")}</span>`}

        return { paddingX: 2, id: "editor:layout", rows: [{ cols: [title_page,xtype, xform, xserial, xidx, xbtnview,xbtnexport ,xbtnapp, {}, {}] }] }//, minWidth: 900 
    }
    ready(v, urls) {
        webix.ajax().post("api/ous/getTemp").then(rs =>{
            let json = rs.json()
            $$("editor:idx").setValue(json)
        })
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
    init() {
        if (!ROLE.includes('PERM_SYS_TEMPLATE')) {
            this.show("/top/home")
            window.location.replace(`/${homepage}`)
            return
        }
        this.EditorWin = this.ui(EditorWin)
        const lang = this.app.getService("locale").getLang()
        let type = $$("editor:type"), form = $$("editor:form"), layout = $$("editor:layout"), idx = $$("editor:idx"), btnapp = $$("editor:btnapp"), serial = $$("editor:serial")
        type.setValue("01GTKT")
        const CONFIG = {
            plugins: "print save preview fullpage fullscreen paste searchreplace visualblocks visualchars template table charmap hr pagebreak nonbreaking advlist lists image imagetools textcolor colorpicker contextmenu textpattern code  help",//anchor toc importcss
            toolbar: "save | undo redo | styleselect |formatselect | fontselect fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright alignjustify | outdent indent | image | template |",
            font_formats: "Arial=arial;Courier=courier;Tahoma=tahoma;Times=times;Verdana=verdana;Calibri=calibri;",
            fontsize_formats: "8pt 9pt 10pt 11pt 12pt 13pt 14pt 16pt 18pt 20pt 22pt 24pt 26pt 28pt",
            menubar: "file edit insert format view table help", // tools
            removed_menuitems: 'newdocument',
            entity_encoding: "raw",
            paste_data_images: true,
            templates: TEMPLATES,
            save_onsavecallback: () => {
                if (!ROLE.includes('PERM_SYS_TEMPLATE_SAVE')) {
                    webix.message(_("no_authorization"), "error")
                    return false
                }
                if (status) webix.message(_("temp_save_error"))
                else webix.ajax().post("api/editor", {type: type.getValue(), form: form.getValue(), serial: serial.getValue(), idx: idx.getValue(), content: $$("editor").getValue() }).then(result => { webix.message(result.json()) })
            }
            , images_upload_handler: (blobInfo, success) => {
                success("data:" + blobInfo.blob().type + ";base64," + blobInfo.base64())
            }
        }
        const CONFIGALL = {
            plugins: "print save preview fullpage fullscreen paste searchreplace visualblocks visualchars template table charmap hr pagebreak nonbreaking advlist lists image imagetools textcolor colorpicker contextmenu textpattern code  help",//anchor toc importcss
            toolbar: "undo redo | styleselect |formatselect | fontselect fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright alignjustify | outdent indent | image | template |",
            font_formats: "Arial=arial;Courier=courier;Tahoma=tahoma;Times=times;Verdana=verdana;Calibri=calibri;",
            fontsize_formats: "8pt 9pt 10pt 11pt 12pt 13pt 14pt 16pt 18pt 20pt 22pt 24pt 26pt 28pt",
            menubar: "file edit insert format view table help", // tools
            removed_menuitems: 'newdocument',
            entity_encoding: "raw",
            paste_data_images: true,
            templates: TEMPLATES,
            save_onsavecallback: () => {
                if (!ROLE.includes('PERM_SYS_TEMPLATE_SAVE')) {
                    webix.message(_("no_authorization"), "error")
                    return false
                }
                if (status) webix.message(_("temp_save_error"))
                else webix.ajax().post("api/editor", { form: form.getValue(), idx: idx.getValue(), content: $$("editor").getValue() }).then(result => { webix.message(result.json()) })
            }
            , images_upload_handler: (blobInfo, success) => {
                success("data:" + blobInfo.blob().type + ";base64," + blobInfo.base64())
            }
        }
        if (lang == "vi") CONFIG.language = "vi_VN"
        layout.addView({ id: "editor", view: "tinymce-editor", config:  CONFIG })
        const refreshForm = () => {
            const t = type.getValue(), f = form.getValue(), i = idx.getValue(), s = serial.getValue(), editor = $$("editor")
            editor.setValue("")
            if (f) {
                const f3 = f.slice(-3)
                webix.ajax("api/editor", {type: t, form: f, serial: s, idx: i }).then(result => {
                    const json = result.json()
                    status = json.status
                    if (status) {
                        btnapp.hide()
                    }
                    else {
                        if (f3 === "000") btnapp.hide()
                        else btnapp.show()
                    }
                    editor.setValue(json.content)
                    editor.getEditor().getBody().setAttribute('contenteditable', !status)
                })
            }
        }


        type.attachEvent("onChange", () => {
            refreshForm()
        })
        form.attachEvent("onChange", () => {
            refreshForm()
        })
        serial.attachEvent("onChange", () => {
            refreshForm()
        })
        idx.attachEvent("onChange", () => {
            refreshForm()
        })

        $$("editor:btnview").attachEvent("onItemClick", () => {
            const me = this
            webix.ajax("api/editor/htm", {flag: 0, type : type.getValue(), form: form.getValue(), idx: idx.getValue(), serial: serial.getValue() }).then(result => {
               // const json = result.json()
               // const req = createRequest(json)
                //jsreport.renderAsync(req).then(function (res) {
                //    $$("editor:ipdf").define("src", res.toDataURI())
                // webix.ajax().post("api/seek/report", req).then(res => {
                //     let pdf = res.json()
                //     let b64 = pdf.pdf
                //     webix.html.download(b64,`123u.pdf`)
                //     $$("editor:ipdf").define("src", b64)
                //     me.EditorWin.show()
                // })
                let pdf = result.json()
                    let b64 = pdf.pdf
                   // webix.html.download(b64,`123u.pdf`)
                    $$("editor:ipdf").define("src", b64)
                    me.EditorWin.show()
               })
        })
        $$("editor:btnexport").attachEvent("onItemClick", () => {
            const me = this
            webix.ajax("api/editor/htm", {flag: 1, type : type.getValue(), form: form.getValue(), idx: idx.getValue(), serial : serial.getValue() }).then(result => {
               //const json = result.json()
               // const req = createRequest(json)
                //jsreport.renderAsync(req).then(function (res) {
                //    $$("editor:ipdf").define("src", res.toDataURI())
                // webix.ajax().post("api/seek/report", req).then(res => {
                //     let pdf = res.json()
                //     let b64 = pdf.pdf
                //     $$("editor:ipdf").define("src", b64)
                //     me.EditorWin.show()
                // })
                let pdf = result.json()
                    let b64 = pdf.pdf
                   // webix.html.download(b64,`123u.pdf`)
                    $$("editor:ipdf").define("src", b64)
                    me.EditorWin.show()
              
            })
        })

        btnapp.attachEvent("onItemClick", () => {
            const f = form.getValue(), i = idx.getValue(), s = serial.getValue(), t = type.getValue()
            webix.confirm(`${_("temp_approve_confirm")} ${f}.${i} ?`, "confirm-warning").then(() => {
                webix.ajax("api/editor/app", {type: t, form: f, serial: s, idx: i }).then(result => {
                    webix.message(result.json())
                    refreshForm()
                })
            })
        })
        refreshForm()
    }
}    