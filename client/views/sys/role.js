import { JetView } from "webix-jet"
import {setConfStaFld, ROLE} from "models/util"
export default class RoleView extends JetView {
  config() {
    _ = this.app.getService("locale")._
    const USTATUS = [
      { id: "2", value: _("canceled") },
      { id: "1", value: _("valid") }
    ]
    // const role = { view: "list", id: "role:list", template: "#name#", select: true, url: "api/groups/roleAll/1" }
    const role = {
      view: "treetable",
      id: "role:list",
      // select: "row",
      threeState: true ,
      checkboxRefresh: true,
      // multiselect: false,
      // resizeColumn: true,
      columns: [
         { view: "list", fillspace: 1, template: "{common.space()}{common.icon()}#name#" },
      ],
      filterMode: { level: false, showSubItems: false },
      select: true,
      url: "api/groups/roleAll/1"
    }

    const group = {
      view: "datatable",
      id: "role:group",
      select: "row",
      multiselect: false,
      resizeColumn: true,
      columns: [
        { id: "sel", header: { text: _("select"), css: "header" }, adjust: "header", template: "{common.checkbox()}" },
        { id: "id", header: { text: _("id"), css: "header" },hidden:true },
        { id: "name", header: { text: _("groupname"), css: "header" }, fillspace:true },
        { id: "des", header: { text: _("description"), css: "header" },fillspace:true },
        { id: "status", header: { text: _("status"), css: "header" }, options: USTATUS }
      ]
    }
    let btn = { view: "button", id: "role:btnsave", type: "icon", icon: "mdi mdi-content-save", label: _("save"), width: 100, disabled: (!ROLE.includes('PERM_SYS_ROLE_SAVE')) ? true : false }
    let title_page = { view: "label", label:`<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("grant")}</span>`}
        
    return { paddingX: 2, cols: [{ header: _("grant"), body: role }, { view: "resizer" }, { rows: [title_page,group, { cols: [{}, btn] }], gravity: 3 }] }
  }

  ready(v, urls) {
    //Cau hinh rieng cac cot tĩnh
    webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
        let json = result.json()
        setConfStaFld(json)
    })
    //
  }
  
  init() {
    if (!ROLE.includes('PERM_SYS_ROLE')) {
      this.show("/top/home")
      window.location.replace(`/${homepage}`)
      return
    }
    let group = $$("role:group"), role = $$("role:list")
    role.attachEvent("onItemClick", (ids) => {
      webix.ajax(`api/groups/gir/${ids.row}`).then(result => {
        group.clearAll()
        group.parse(result.json())
      })
    })

    $$("role:btnsave").attachEvent("onItemClick", () => {
      let groups = group.serialize(), arr = [], rid = role.getSelectedId()
      for (const u of groups) {
        if (u.sel == 1) arr.push(u.id)
      }
      webix.ajax().post("api/groups/mbr", { rid: rid, groups: arr }).then(() => { webix.message(_("save_ok_msg"), "success") })
    })

  }
}
