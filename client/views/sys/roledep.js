import { JetView } from "webix-jet"
import { setConfStaFld } from "models/util"
export default class SeouView extends JetView {
  config() {
    _ = this.app.getService("locale")._
    const USTATUS = [
            { id: "2", value: _("canceled") },
            { id: "1", value: _("valid") }
          ]
    const grid1 = {
      view: "datatable",
      id: "roledep:grid1",
      select: "row",
      multiselect: false,
      resizeColumn: true,
      columns: [
        { id: "sel", header: { content: "masterCheckbox", css: "center" }, adjust: "header", css: "center", template: "{common.checkbox()}" },
        { id: "id", header: [{ text: _("dep_id"), css: "header" }, {content:"textFilter"}], adjust: true, fillspace: true },
        { id: "name", header: { text: _("dep_name"), css: "header" }, adjust: true, fillspace: true },
      ]
    }

    const grid2 = {
      view: "datatable",
      id: "roledep:grid2",
      select: "row",
      multiselect: false,
      resizeColumn: true,
      columns: [
        { id: "id", header: [{ text: _("account"), css: "header" }, {content:"textFilter"}], sort:"string", adjust: true },
        { id: "mail", header: [{ text: "Email", css: "header" }, {content:"textFilter"}], sort:"string", adjust: true },
        { id: "name", header: [{ text: _("fullname"), css: "header" }, {content:"textFilter"}], sort:"string", adjust: true },
        { id: "pos", header: [{ text: _("position"), css: "header" }, {content:"textFilter"}], sort:"string", adjust: true },
        { id: "code", header: [{ text: _("user_code"), css: "header" },], sort: "server", css: "right", adjust: true },
        { id: "ou", header: { text: _("ou"), css: "header" }, sort: "server", options: "api/cat/nokache/ou", adjust: true, fillspace: 1 },
        { id: "uc", header: { text: _("status"), css: "header" }, sort: "server", options: USTATUS, adjust: true }
      ],
      url: "api/rol"
    }
    return { paddingX: 2, paddingY: 2, cols: [{ rows: [grid2, { cols: [{width: 800}, { view: "button", id: "dep:btnsave", type: "icon", icon: "mdi mdi-content-save", label: _("save"), width: 100 }] }] }, grid1, { view: "resizer" } ] }
  }

  ready(v, urls) {
    //Cau hinh rieng cac cot tĩnh
    webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
        let json = result.json()
        setConfStaFld(json)
    })
    //
  }
  init() {
    let grid2 = $$("roledep:grid2"), grid1 = $$("roledep:grid1")
    grid2.attachEvent("onAfterSelect", () => {
      const row = grid2.getSelectedItem()
      webix.ajax(`api/rol/${row.id}`).then(result => {
        grid1.clearAll()
        grid1.parse(result.json())
      })
    })

    $$("dep:btnsave").attachEvent("onItemClick", () => {
      $$("roledep:grid1").filter()
      const sers = grid1.serialize(), usid = grid2.getSelectedId().id
      let arr = []
      for (const ser of sers) {
        if (ser.sel == 1) arr.push(ser.id)
      }
      webix.ajax().post("api/rol", { usid: usid, arr: arr }).then(() => { webix.message(_("save_ok_msg"), "success") })
    })

  }
}

