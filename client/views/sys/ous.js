import { JetView } from "webix-jet"
import { LinkedInputs, TAXC, setConfStaFld, ENT, ROLE } from "models/util"
const LG = [
    { id: "0", value: _("month") },
    { id: "1", value: _("day")}
  
   
]
export default class OuView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        webix.protoUI({ name: "dependent", $cssName: "combo" }, LinkedInputs, webix.ui.combo)
        const form = {
            view: "form",
            id: "ou:form",
            padding: 3,
            margin: 3,
            minWidth: 500,
            visibleBatch: "notaxc",
            hidden: true,
            elementsConfig: { labelWidth: 120 },
            elements:
                [
                    {
                        id: "companyid",name: "name", label: _("company_name"), view: "text", attributes: { maxlength: 400 }, required: true, placeholder: _("enter2search_msg"), suggest: {
                            //view: "suggest",
                            id: "orgsuggest",
                            view: "gridsuggest",
                            relative: "left",
                            format: webix.template.escape,
                            body: {
                                url: "api/org/ous",
                                dataFeed: function (str) {
                                    if (/\;$/.test(str)) return this.load(`${this.config.url}?filter[value]=${encodeURIComponent(str.substring(0, str.length - 1))}`, this.config.datatype)
                                    else return
                                },
                                scroll: true,
                                tooltip: true,
                                autoheight: false,
                                columns: [
                                    { id: "taxc", header: { text: _("taxcode"), css: "header" }, adjust: true },
                                    { id: "name", header: { text: _("customer_name"), css: "header" }, adjust: true },
                                    { id: "fadd", header: { text: _("address"), css: "header" } },
                                ]
                            }
                        },
                    },
                    //{ id: "ou:pid", name: "pid", label: _("parent"), view: "combo", options: "api/ous/level3", hidden: true },
                    {
                        cols: [
                            { id: "ou:taxc", name: "taxc", label: _("taxcode"), view: "text", attributes: { maxlength: 14 }, validate: v => {return (!v || checkmst(v))}, invalidMessage: _("taxcode_invalid") },
                            { id: "ou:mst", name: "mst", label: _("taxcode_invoice"), view: "combo", suggest: { url: "api/ous/taxc" }, required: true },
                        ]
                    },
                    {
                        cols: [{ name: "mail", label: "Mail", view: "text", attributes: { type: "email", maxlength: 50}, validate: v => {return (!v || webix.rules.isEmail(v))}},
                        { name: "tel", label: _("tel"), view: "text", attributes: { type: "tel", maxlength: 20 } }]
                    },
                    { name: "addr", label: _("address"), view: "text", attributes: { maxlength: 400 }, required: true },
                    {
                        cols: [{ id: "ou:prov", name: "prov", view: "combo", label: _("city"), suggest: { url: "api/cat/kache/prov" } },
                        { id: "ou:dist", name: "dist", view: "dependent", label: _("district"), master: "ou:prov", dependentUrl: "api/cat/kache2/local/", options: [] }]
                    },
                    {
                        cols: [{ id: "ou:ward", name: "ward", view: "dependent", label: _("ward"), master: "ou:dist", dependentUrl: "api/cat/kache2/local/", options: [] },
                        { id: "ou:code", name: "code", view: "text", attributes: { maxlength: 20 }, label: _("ou_code") }
                        ]
                    },
                    {
                        batch: "taxc", cols: [{ name: "acc", label: _("account"), view: "text", attributes: { maxlength: 30 } },
                        { name: "bank", label: _("bank"), view: "text", attributes: { maxlength: 400 } }]
                    },
                    { 
                        cols: [
                            { rows: [{id: "receivermail", name: "receivermail", label: _("receivermail"), view:"multitext", attributes: { type: "email", maxlength: 255 }, required: true }]},
                            { rows: [{id: "place", name: "place", label: _("place"), view: "text" }]}
                        ]
                    },
                    {
                        batch: "taxc", cols: [{ id: "ou:paxo", name: "paxo", view: "combo", label: _("tax_department"), suggest: { url: "api/cat/kache/taxo" }, required: true },
                        { name: "taxo", view: "dependent", label: _("district_tax_department"), master: "ou:paxo", dependentUrl: "api/cat/kache2/taxo/", options: [], required: true }]
                    },
                    {
                        batch: "taxc", cols: [{ id: "ou:sign", name: "sign", label: _("signing_type"), view: "combo", options: [{ id: 1, value: "USB Token" }, { id: 2, value: "File" }, { id: 3, value: "FPTCA Cloud" }, { id: 4, value: "HSM" }] },
                        { id: "ou:seq", name: "seq", label: _("numbering_type"), view: "combo", options: [{ id: 1, value: _("automatic") }, { id: 2, value: _("manual") }], readonly: ENT == "vcm" ? true : false },
                        ]
                    },
                    {
                        id: "rr1", batch: "taxc", cols: [{id: "dsignfrom", name: "dsignfrom", label: _("fd"), view: "datepicker", format: webix.i18n.dateFormatStr, disabled: true },
                        { id:"dsignto", name: "dsignto", label: _("td"), view: "datepicker", format: webix.i18n.dateFormatStr, disabled: true }]
                    },
                    {  id: "rr2", batch: "taxc", rows: [{ id: "dsignissuer", name: "dsignissuer", label: _("dsignissuer"), view:"text", disabled: true }] }
                    ,              

                    {
                            id: "rr3", batch: "taxc", cols: [{ id: "chk_revoke", name: "chk_revoke", label: _("chk_revoke"), view: "combo", options: [{ id: 1, value: _("yes") }, { id: 2, value: _("no") }]},
                            {}]
                    },
					// Thừa 2 trường dsignsubject và dsignserial
                    // {
                    //     id: "rr3", batch: "taxc", cols: [{ id: "dsignserial", name: "dsignserial", label: "dsignserial", view: "text", attributes: { maxlength: 255 }, disabled: true },
                    //     { id:"dsignsubject", name: "dsignsubject", label: "dsignsubject", view: "text", attributes: { maxlength: 255 }, disabled: true }]
                    // },
                    {
                        id: "r08", batch: "taxc", cols: [
                            { id: "ou:usr", name: "usr", view: "text", attributes: { maxlength: 20 }, label: _("account") },
                            { id: "ou:pwd", name: "pwd", view: "text", type: "password", attributes: { maxlength: 20 }, label: _("password") }
                        ]
                    },
                  (["scb","mzh"].includes(ENT)) ?
                   {
                        
                        cols: [
                            { id: "ou:c0", name: "c0", label: _("group_method"),view: "combo", options: LG, required: true ,value: "0"},
                            { id: "ou:temp", name: "temp", view: "text", type: "number", label: _("templace"), value: 1, attributes: { step: 1, min: 1, max: 99, maxlength: 2 }, inputAlign: "right" },
                            {
                                cols: [{},
                                //{ id: "ou:btnusb", view: "button", type: "icon", icon: "mdi mdi-usb", label: "USB", width: 100, hidden: true },
                                { id: "ou:btnsave", view: "button", type: "icon", icon: "mdi mdi-database-edit", label: _("update"), width: 100 }]
                            }
                        ]
                    }: 
                    {
                        cols: [
                            //{ id: "ou:status", name: "status", label: _("status"), view: "combo", options: [{ id: 1, value: _("valid") }, { id: 2, value: _("invalid") }] },
                            { id: "ou:temp", name: "temp", view: "text", type: "number", label: _("templace"), value: 1, attributes: { step: 1, min: 1, max: 99, maxlength: 2 }, inputAlign: "right" },
                            {
                                cols: [{},
                                //{ id: "ou:btnusb", view: "button", type: "icon", icon: "mdi mdi-usb", label: "USB", width: 100, hidden: true },
                               
                                { id: "ou:btnsave", view: "button", type: "icon", icon: "mdi mdi-database-edit", label: _("update"), width: 110, disabled: (ROLE.includes('PERM_SYS_CONFIG_UPDATE')) ? false : true },
                                { id: "ou:btnviewca", view: "button", type: "icon", icon: "mdi mdi-eye", label: _("viewca"), width: 110, disabled: (ROLE.includes('PERM_SYS_CONFIG_VIEW_CA')) ? false : true }]
                            }
                        ]
                   },                   
                    {
                        id: "r10", batch: "taxc",
                        cols: [
                            {
                                cols: [{ gravity: 0 }, { id: "ou:filelist", view: "list", type: "uploader", autoheight: true, borderless: true }]
                            },
                            {
                                cols: [{},
                                { id: "ou:file", view: "uploader", multiple: false, autosend: false, link: "ou:filelist", accept: "application/x-pkcs12", upload: "api/ous/upload", type: "icon", icon: "mdi mdi-file-find-outline", label: _("file_select"), width: 110, formData: () => { return { taxc: $$("ou:taxc").getValue() } }, disabled: (ROLE.includes('PERM_SYS_CONFIG_UPDATE')) ? false : true },
                                { id: "ou:btnupload", view: "button", type: "icon", icon: "mdi mdi-file-upload-outline", label: _("file_upload"), width: 110, disabled: (ROLE.includes('PERM_SYS_CONFIG_UPDATE')) ? false : true }
                                ]
                            }
                        ]
                    },
                    { fillspace: true }
                ],
            rules: {
                name: webix.rules.isNotEmpty,
                mst: webix.rules.isNotEmpty,
                receivermail: this.webix.rules.isNotEmpty,
                $obj: data => {
                    if (!(containhtmlchar(data.name))) {
                    webix.message(_("contain_html_char"))
                    $$("companyid").focus()
                    return false
                    }
                    if (!(containhtmlchar(data.taxc))) {
                        webix.message(_("contain_html_char"))
                        $$("ou:taxc").focus()
                        return false
                    }
                    if (!(containhtmlchar(data.mst))) {
                        webix.message(_("contain_html_char"))
                        $$("ou:mst").focus()
                        return false
                    }
                    // if (!(containhtmlchar(data.mail))) {
                    //     webix.message(_("contain_html_char"))
                    //     $$("ou:mail").focus()
                    //     return false
                    // }
                    // if (!(containhtmlchar(data.tel))) {
                    //     webix.message(_("contain_html_char"))
                    //     $$("ou:tel").focus()
                    //     return false
                    // }
                    if (!(containhtmlchar(data.addr))) {
                        webix.message(_("contain_html_char"))
                        $$("ou:addr").focus()
                        return false
                    }
                    if (!(containhtmlchar(data.prov))) {
                        webix.message(_("contain_html_char"))
                        $$("ou:prov").focus()
                        return false
                    }
                    if (!(containhtmlchar(data.dist))) {
                        webix.message(_("contain_html_char"))
                        $$("ou:dist").focus()
                        return false
                    }
                    if (!(containhtmlchar(data.ward))) {
                        webix.message(_("contain_html_char"))
                        $$("ou:ward").focus()
                        return false
                    }
                    if (!(containhtmlchar(data.acc))) {
                        webix.message(_("contain_html_char"))
                        $$("ou:acc").focus()
                        return false
                    }
                    if (!(containhtmlchar(data.bank))) {
                        webix.message(_("contain_html_char"))
                        $$("ou:bank").focus()
                        return false
                    }
                    if (!(containhtmlchar(data.paxo))) {
                        webix.message(_("contain_html_char"))
                        $$("ou:paxo").focus()
                        return false
                    }
                    if (!(containhtmlchar(data.sign))) {
                        webix.message(_("contain_html_char"))
                        $$("ou:sign").focus()
                        return false
                    }
                    if (!(containhtmlchar(data.seq))) {
                        webix.message(_("contain_html_char"))
                        $$("ou:seq").focus()
                        return false
                    }
                    if (!(containhtmlchar(data.usr))) {
                        webix.message(_("contain_html_char"))
                        $$("ou:usr").focus()
                        return false
                    }
                    if (!(containhtmlchar(data.chk_revoke))) {
                        webix.message(_("contain_html_char"))
                        $$("chk_revoke").focus()
                        return false
                    }
                    if (!(containhtmlchar(data.pwd))) {
                        webix.message(_("contain_html_char"))
                        $$("ou:pwd").focus()
                        return false
                    }
                    if (data.receivermail) {
                        const rows = data.receivermail.split(/[ ,;]+/)
                        for (const row of rows) {
                          if (!webix.rules.isEmail(row.trim())) {
                            webix.message(_("mail_invalid"))
                            $$("receivermail").focus()
                            return false
                          }
                        }
                    }
                    return true
                }
                /*
                $obj: data => {
                    if (!webix.rules.isNotEmpty(data.pid) && data.level > 1) {
                        webix.message(_("ous_parent_required"))
                        $$("ou:pid").focus()
                        return false
                    }
                    return true
                }*/
            },
            on: {
                onAfterValidation: (result) => {
                    if (!result) webix.message(_("required_msg"))
                }
            }

        }


        const grid = {
            view: "treetable",
            id: "ou:treetable",
            //ready: function () { this.openAll()},
            minWidth: 400,
            select: "row",
            multiselect: false,
            columns: [
                { id: "name", header: [{ text: _("company_name"), css: "header" }, { content: "textFilter" }], template: "{common.treetable()} #name#",format: webix.template.escape, fillspace: 1 },//editor: "text",
                { id: "taxc", header: [{ text: _("taxcode"), css: "header" }, { content: "textFilter" }], width: 130 },
                { id: "mst", header: [{ text: _("taxcode_invoice"), css: "header" }, { content: "textFilter" }],format: webix.template.escape, width: 130 },//, { content: "multiComboFilter",inputConfig: { tagMode: true } }
                {
                    width: 35, template: obj => {
                        if (obj.level >= 4) return ""
                        else return `<span class='webix_icon wxi-plus' title='${_("ous_add_ou")}'></span>`
                    }
                },
                {
                    width: 35, template: obj => {
                        if (obj.id == 1 || obj.$count > 0) return ""
                        else return `<span class='webix_icon wxi-trash' title='${_("ous_del_ou")}'></span>`
                    }
                }
            ],
            filterMode: { level: false, showSubItems: false },
            onClick:
            {
                "wxi-plus": function (e, id) {
                    if (!ROLE.includes('PERM_SYS_CONFIG_CREATE')) {
                        webix.message(_("no_authorization"), "error")
                        return false
                    }
                    const me = this, row = me.getItem(id), level = row.level + 1
                    if (level >= 5) {
                        webix.message(_("ous_level3_msg"), "error")
                        return false
                    }
                    me.waitSave(() => {
                        const mst = row.taxc ? row.taxc : TAXC
                        const obj = { name: `Đơn vị con cấp ${level}`, level: level, pid: row.id, mst: mst, status: 1, temp: row.temp/* , code: ("vcm".includes(ENT)) ? mst : "" */, seq:  ("vcm".includes(ENT)) ? "2" : ""
                        } 
                        me.add(obj, 0, id)
                    }).then(result => {
                        me.openAll()
                        me.select(result.id)
                    })
                    return false
                },
                "wxi-trash": function (e, id) {
                    if (!ROLE.includes('PERM_SYS_CONFIG_DELETE')) {
                        webix.message(_("no_authorization"), "error")
                        return false
                    }
                    const me = this, row = me.getItem(id)
                    webix.confirm(`${_("ous_del_msg")} ${row.name} ?`, "confirm-warning").then(() => {
                        me.waitSave(() => me.remove(id)).then(rs => {
                            if (rs == 1) webix.message((_("del_ok_ou_msg")).replace("#hunglq#", `${row.name}`))
                        }).catch(e => { webix.ajax().get("api/ous").then(rs => { me.parse(rs) }) })
                    })
                    return false
                }
            },
            url: "api/ous",
            save: { url: "api/pous", updateFromResponse: true }
        }
        let title_page = { view: "label", label:`<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("system_config")}</span>`}
        let colsall = { paddingX: 2, cols: [grid, { view: "resizer" }, form] }
        return {paddingX: 2, rows: [title_page,colsall]}

    }

    ready(v, urls) {
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }

    init() {
        if (!ROLE.includes('PERM_SYS_CONFIG')) {
            this.show("/top/home")
            window.location.replace(`/${homepage}`)
            return
        }
        let form = $$("ou:form"), grid = $$("ou:treetable")
        //Riêng cho VCM

        // form.bind(grid)
        form.bind(grid, null, function (data) {
            return decodehtml(data)
          });
        // bỏ 2 trường dsignsubject: obj.dsignsubject, dsignserial: obj.dsignserial
        $$("orgsuggest").attachEvent("onValueSuggest", obj => {
            if (obj) form.setValues({ taxc: obj.taxc, mst: obj.taxc, name: obj.name, addr: obj.addr, prov: obj.prov, dist: obj.dist, ward: obj.ward, mail: obj.mail, tel: obj.tel, acc: obj.acc, bank: obj.bank, paxo: obj.paxo, taxo: obj.taxo, code:obj.code, receivermail: obj.receivermail, dsignissuer: obj.dsignissuer, dsignfrom: obj.dsignfrom, dsignto: obj.dsignto, chk_revoke: obj.chk_revoke}, true)
        })
        const save = () => {
            return new Promise((resolve, reject) => {
                const dirty = form.getDirtyValues()
                if (dirty.hasOwnProperty("seq")) {
                    const mst = $$("ou:mst").getValue()
                    webix.ajax(`api/ous/seq/${mst}`).then(rs => {
                        const obj = rs.json()
                        if (obj.rc && obj.rc > 0) reject(new Error("Còn HĐ chờ cấp số : Không thay đổi kiểu cấp sô"))
                        else {
                            form.save()
                            resolve(1)
                        }
                    })
                }
                else {
                    form.save()
                    resolve(1)
                }
            })
        }

        // $$("ou:btnsave").attachEvent("onItemClick", () => {
        //     if (form.validate() && form.isDirty()) {
        //         this.$$("ou:treetable").waitSave(() => { save().then(ok => webix.message(_("ous_update_config_ok"), "success")).catch(err => { this.webix.message(err.message, "error") }) })
        //     }
        // })
        $$("ou:btnsave").attachEvent("onItemClick", () => {
            if (form.validate() && form.isDirty()) {
                const params = form.getValues()
                if (params.sign == 2 && params.pwd != "") {
                    webix.ajax().get("api/ous/checkca", params).then(result => {
                        let json = result.json()
                        console.log(json.ret);
                        if (json.ret == 0) {
                            webix.message(_("err_checkca"), "error")
                            return false
                        }
                        $$("ou:treetable").waitSave(() => {
                            save().then(ok => {
                                webix.message(_("ous_update_config_ok"), "success")

                            }).catch(err => { webix.message(err.message, "error") })

                        })

                    }).catch(e => {
                        webix.message(e.message, "error")
                    })
                } else {
                    $$("ou:treetable").waitSave(() => {
                        save().then(ok => {
                            webix.message(_("ous_update_config_ok"), "success")

                        }).catch(err => { webix.message(err.message, "error") })

                    })

                }
                $$("ou:mst").define("suggest", "api/ous/taxc");
                $$("ou:mst").refresh();
            }
            $$("ou:mst").define("suggest", "api/ous/taxc");
            $$("ou:mst").refresh(); 
        })

        const taxcshow = (taxc) => {
            // $$("rr3").hide()
            if (taxc) {
                form.showBatch("taxc")
                let sign = $$("ou:sign").getValue()
                if (sign == 1 || sign == 4) {
                    $$("r10").hide()
                    $$("r08").hide()
                    $$("rr1").hide()
                    $$("rr2").hide()
                }
                else {
                    $$("r08").show()
                    if (sign == 2) {
                        $$("r10").show()
                        $$("rr1").show()
                        $$("rr2").show()
                    } 
                    else {
                        $$("r10").hide()
                        $$("rr1").hide()
                        $$("rr2").hide()
                    }
                    $$("ou:usr").config.label = _("account")
                    $$("ou:pwd").config.label = _("password")
                    $$("ou:usr").refresh()
                    $$("ou:pwd").refresh()
                }
            }
            else {
                form.showBatch("notaxc")
                $$("r10").hide()
            }
            if (!$$("ou:seq").getValue()) $$("ou:seq").setValue("2")
        }
        $$("ou:taxc").attachEvent("onChange", newv => {
            taxcshow(newv)
        })
        $$("ou:sign").attachEvent("onChange", newv => {
            const taxc = $$("ou:taxc").getValue()
            taxcshow(taxc)
        })
        $$("ou:btnupload").attachEvent("onItemClick", () => {
            let uploader = $$("ou:file"), files = uploader.files, file_id = files.getFirstId()
            if (file_id) {
                let mst, pwd
                mst = $$("ou:mst").getValue()
                pwd = $$("ou:pwd").getValue()
                webix.ajax().post("api/ous/vca/upfile", { mst: mst, pwd: pwd }).then( result => {
                    let dataTrans = result.json()
                    if(dataTrans.mst != mst){
                        webix.message(_("err_mst_file"), "error")
                        return false
                    }
                    if(dataTrans){
                        $$("dsignissuer").setValue(dataTrans.issuer)
                        // $$("dsignserial").setValue(dataTrans.serial)
                        // $$("dsignsubject").setValue(dataTrans.subject)
                        $$("dsignfrom").setValue(new Date(dataTrans.fd))
                        $$("dsignto").setValue(new Date(dataTrans.td))
                    }
                })
                let obj = files.getItem(file_id)
				 // lay dung ten file khong lay mst
                // obj.name = $$("ou:taxc").getValue()
                uploader.send(() => {
                    if (obj.status == "server") webix.message(`${_("file_upload_ok")} ${obj.name}`)
                    else webix.message(obj)
                })
            }
            else webix.message(_("file_required"))
            
        })

        grid.attachEvent("onAfterUnselect", () => {
            form.hide()
        })

        grid.attachEvent("onAfterSelect", selection => {
            let row = grid.getItem(selection.id)
            row.dsignfrom = row.dsignfrom?new Date(row.dsignfrom): ''
            row.dsignto = row.dsignto?new Date(row.dsignto):''
            form.setValues(row)
            form.show()
        })
        $$("ou:btnviewca").attachEvent("onItemClick", () => {
            let mst, serial, fd, td, subject, issuer, pwd
            mst = $$("ou:mst").getValue()
            pwd = $$("ou:pwd").getValue()
            webix.ajax().post("api/ous/vca", { mst: mst}).then( result => {
                let dataTrans = result.json()
                if(dataTrans){
                    serial = dataTrans.serial
                    fd = dataTrans.fd
                    td = dataTrans.td
                    subject = dataTrans.subject
                    issuer = dataTrans.issuer
                }
                webix.ui({
                    view: "window",
                    id: "popupViewCA",
                    isVisible: true,
                    position: "center",
                    modal:true,
                    height: 400,
                    width: 600,
                    head: {
                        view: "toolbar", height: 40,
                        css: 'toolbar_window',
                        cols: [
                            
                            { view: "label", id: "win_lbl2", label: _("certificate") },
                            { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => { $$("popupViewCA").close() } }
                        ]
                    },
                    body: {
                        id: "ou:viewca",
                        rows: [
                            { cols:[
                                { view:"label", label: _("taxcode"), width: 100, css: "header" },
                                { view:"label", label: `${mst}`, height:50, align:"center" }
                              ]
                            },
                            { cols:[
                                { view:"label", label: _("serial"), width: 100, css: "header" },
                                { view:"label", label: `${serial}`, align:"center" }
                              ]
                            },
                            { cols:[
                                { view:"label", label: _("fd"), width: 100, css: "header" },
                                { view:"label", label: `${fd}`, height:50, align:"center" }
                              ]
                            },
                            { cols:[
                                { view:"label", label: _("td"), width: 100, css: "header" },
                                { view:"label", label: `${td}`, height:50, align:"center" }
                              ]
                            },
                            { cols:[
                                { view:"label", label: "Subject", width: 100, css: "header" },
                                { view:"label", label: `${subject}`, height:50, align:"center" }
                              ]
                            },
                            { cols:[
                                { view:"label", label: "Issuer", width: 100, css: "header" },
                                { view:"label", label: `${issuer}`, height:50, align:"center" }
                              ]
                            }
                        ]
                    }
                }).show()
            })
            //console.log(subject)
           
        })
    }
}   