import { JetView } from "webix-jet"
import { XTYPE, HDC, AZ, setConfStaFld, ROLE } from "models/util"
export default class ColView extends JetView {
	config() {
		_ = this.app.getService("locale")._
		const TYP = ["text", "number", "datepicker", "checkbox", "combo", "multicombo"]
		const itype = { cols: [{ id: "itype", name: "itype", label: _("type"), view: "combo", options: XTYPE(this.app), value: "01GTKT" }, {}, {}, {}] }
		const g0 = {
			view: "datatable",
			id: "col:g0",
			select: "row",
			editable: true,
			multiselect: false,
			columns: [
				{ id: "jc", header: [{ text: _("excel_templace"), colspan: 2, css: "bold" }, { text: _("col_field_name"), css: "header" }], fillspace: true, collection: HDC },
				{ id: "xc", header: ["", { text: _("col_excel_col"), css: "header" }], adjust: true, collection: AZ, editor: ROLE.includes('PERM_SYS_INV_CONFIG_EDIT_EXCEL') ? "combo" : false }
			],
			save: "api/xls"
		}

		const g1 = {
			view: "datatable",
			id: "col:g1",
			select: "row",
			editable: true,
			multiselect: false,
			gravity: 1.5,
			columns: [
				{ id: "idx", header: [{ text: _("col_header"), colspan: 6, css: "bold" }, { text: _("stt"), css: "header" }], width: 45 },
				{ id: "lbl", header: ["", { text: _("col_field_name_vi"), css: "header" }], adjust: true, editor: ROLE.includes('PERM_SYS_INV_CONFIG_EDIT_INFO') ? "text" : false, format: webix.template.escape },
				{ id: "lbl_en", header: ["", { text: _("col_field_name_en"), css: "header" }], adjust: true, editor: ROLE.includes('PERM_SYS_INV_CONFIG_EDIT_INFO') ? "text" : false, format: webix.template.escape },
				{ id: "status", header: ["", { text: _("col_use"), css: "header" }], width: 80, checkValue: 1, uncheckValue: 2, template: "{common.checkbox()}", css: "center", hidden: ROLE.includes('PERM_SYS_INV_CONFIG_EDIT_INFO') ? false : true},
				{ id: "typ", header: ["", { text: _("col_data_types"), css: "header" }], adjust: true, collection: TYP, editor: ROLE.includes('PERM_SYS_INV_CONFIG_EDIT_INFO') ? "richselect" : false },
				{ id: "xc", header: ["", { text: _("col_excel_col"), css: "header" }], adjust: true, collection: AZ, editor: ROLE.includes('PERM_SYS_INV_CONFIG_EDIT_INFO') ? "combo" : false},
				{ id: "atr", header: ["", { text: _("property"), css: "header" }], fillspace: 1, editor: ROLE.includes('PERM_SYS_INV_CONFIG_EDIT_INFO') ? "popup" : false }
			],
			rules: {
				lbl_en: webix.rules.isNotEmpty, lbl: webix.rules.isNotEmpty,
				$obj: data => {
				  if (!(containhtmlchar(data.lbl))) {
					webix.message(_("contain_html_char"))
					return false
				  }
				  if (!(containhtmlchar(data.lbl_en))) {
					webix.message(_("contain_html_char"))
					return false
				  }
				  return true
				}
			  },
			on: {
				onAfterEditStart: function (r) {
					try {
                        this.getEditor().getPopup().getNode().setAttribute("spellcheck", "false")  
                    } catch (error) {console.error(error)}
				}
			},
			save: "api/col"
		}
		const g2 = {
			view: "datatable",
			id: "col:g2",
			select: "row",
			editable: true,
			multiselect: false,
			columns: [
				{ id: "idx", header: [{ text: _("invoice_details"), colspan: 5, css: "bold" }, { text: _("stt"), css: "header" }], width: 45 },
				{ id: "lbl", header: ["", { text: _("col_col_name_vi"), css: "header" }], fillspace: 1, editor: ROLE.includes('PERM_SYS_INV_CONFIG_EDIT_DETAIL') ? "text" : false, format: webix.template.escape },
				{ id: "lbl_en", header: ["", { text: _("col_col_name_en"), css: "header" }], fillspace: 1, editor: ROLE.includes('PERM_SYS_INV_CONFIG_EDIT_DETAIL') ? "text" : false, format: webix.template.escape },
				{ id: "status", header: ["", { text: _("col_use"), css: "header" }], width: 80, checkValue: 1, uncheckValue: 2, template: "{common.checkbox()}", css: "center", hidden: ROLE.includes('PERM_SYS_INV_CONFIG_EDIT_DETAIL') ? false : true },
				{ id: "typ", header: ["", { text: _("col_data_types"), css: "header" }], adjust: true, collection: ["text", "number", "date", "checkbox", "combo", "richselect", "multiselect", "popup"], editor: ROLE.includes('PERM_SYS_INV_CONFIG_EDIT_DETAIL') ? "richselect" : false },
				{ id: "xc", header: ["", { text: _("col_excel_col"), css: "header" }], adjust: true, collection: AZ, editor: ROLE.includes('PERM_SYS_INV_CONFIG_EDIT_DETAIL') ? "combo" : false },
				{ id: "atr", header: ["", { text: _("property"), css: "header" }], fillspace: 1, editor: ROLE.includes('PERM_SYS_INV_CONFIG_EDIT_DETAIL') ? "popup" : false }
			],
			rules: {
				lbl_en: webix.rules.isNotEmpty, lbl: webix.rules.isNotEmpty,
				$obj: data => {
				  if (!(containhtmlchar(data.lbl))) {
					webix.message(_("contain_html_char"))
					return false
				  }
				  if (!(containhtmlchar(data.lbl_en))) {
					webix.message(_("contain_html_char"))
					return false
				  }
				  return true
				}
			  },
			on: {
				onAfterEditStart: function (r) {
					this.getEditor().getPopup().getNode().setAttribute("spellcheck", "false")
				}
			},
			save: "api/dtl"
		}
		const g3 = {
			view: "datatable",
			id: "col:g3",
			select: "row",
			editable: true,
			 resizeColumn: true,
     
			multiselect: false,
			columns: [
				{ id: "idx", header: [{ text: _("customer"), colspan: 3, css: "bold" }, { text: _("stt"), css: "header" }], width: 45 },
				{ id: "lbl", header: ["", { text: _("col_field_name_vi"), css: "header" }], adjust: true, editor: "text" },
				{ id: "lbl_en", header: ["", { text: _("col_field_name_en"), css: "header" }], adjust: true, editor: "text" },
				{ id: "status", header: ["", { text: _("col_use"), css: "header" }], width: 80, checkValue: 1, uncheckValue: 2, template: "{common.checkbox()}", css: "center" },
				{ id: "typ", header: ["", { text: _("col_data_types"), css: "header" }], adjust: true, collection: TYP, editor: "richselect" },
				{ id: "xc", header: ["", { text: _("col_excel_col"), css: "header" }], adjust: true, collection: AZ, editor: "combo" },
				{ id: "atr", header: ["", { text: _("property"), css: "header" }], fillspace: 1, editor: "popup" },
			],
			on: {
				onAfterEditStart: function (r) {
					this.getEditor().getPopup().getNode().setAttribute("spellcheck", "false")
				}
			},
			save: "api/col"
		}
		const g123 = { id: "g123", visibleBatch: "hd", gravity: 3, cols: [{ batch: "hd", gravity: 3, cols: [g1, { view: "resizer" }, g2] }, { batch: "kh", cols: [g3] }] }
		const title_page = { view: "label", label:`<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("invoice_config")}</span>`}
		return {
			paddingX: 2, rows: [title_page, itype, { cols: [g0, { view: "resizer" }, g123] }]
		}
	}
    ready(v, urls) {
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
	init() {
		if (!ROLE.includes('PERM_SYS_INV_CONFIG')) this.show("/top/home")
		let itype = $$("itype"), g0 = $$("col:g0"), g1 = $$("col:g1"), g2 = $$("col:g2"), g3 = $$("col:g3"), g123 = $$("g123")
		const load = () => {
			const type = itype.getValue()
			g0.clearAll()
			g1.clearAll()
			g2.clearAll()
			g3.clearAll()
			g0.load({
				$proxy:true,
				load:function(view, params){
					if (!params) params = {}
					params.itype = type
					return webix.ajax().get("api/xls", params);
				}
			});
			if (type == "00ORGS") {
				g123.showBatch("kh")
				//g3.load(`api/col/${type}`)
				g3.load({
					$proxy:true,
					load:function(view, params){
						if (!params) params = {}
						params.itype = type
						return webix.ajax().get("api/col", params);
					}
				});
			}
			else {
				g123.showBatch("hd")
				//g1.load(`api/col/${type}`)
				g1.load({
					$proxy:true,
					load:function(view, params){
						if (!params) params = {}
						params.itype = type
						return webix.ajax().get("api/col", params);
					}
				});
				//g2.load(`api/dtl/${type}`)
				g2.load({
					$proxy:true,
					load:function(view, params){
						if (!params) params = {}
						params.itype = type
						return webix.ajax().get("api/dtl", params);
					}
				});
			}
		}
		itype.attachEvent("onChange", (n) => { load() })
		load()
	}
}
