import { JetView } from "webix-jet"
import { size, gridnf, h, w, setConfStaFld, ENT } from "models/util"

export default class VndView extends JetView {
  config() {
    _ = this.app.getService("locale")._
    webix.proxy.seg = {
      $proxy: true,
      load: function (view, params) {
        let obj = $$("seg:form").getValues()
        Object.keys(obj).forEach(
          k => (!obj[k] || obj[k] == "*") && delete obj[k]
        )
        if (!params) params = {}
        params.filter = obj
        return webix.ajax(this.source, params)
      }
    }

    let form = {
      view: "form",
      id: "seg:form",
      minWidth: 500,
      padding: 5,
      margin: 5,
      elementsConfig: { labelWidth: 130 },
      elements: [
        {
          cols: [
            { name: "seg_name", view: "text", label: _("seg_name"), attributes: { maxlength: 255 } },
            { view: "button", id: "seg:btnsearch", label: _("search"), type: "icon", icon: "wxi-search", width: 90, tooltip: _("search") }
          ]
        }
      ]
    }
    let pager = { view: "pager", id: "seg:pager", size: size, template: "{common.first()}{common.prev()}{common.pages()}{common.next()}{common.last()}" }

    let grid1 = {
      view: "datatable",
      id: "seg:grid1",
      select: "row",
      resizeColumn: true,
      datafetch: size,
      pager: "seg:pager",
      columns: [
        { id: "del", header: "", template: `<span class='webix_icon wxi-trash'  title='${_("delete")}'></span>`, css: "header", width: 50, hidden: true },
        { id: "id", header: { text: "ID", css: "header" }, sort: "server", width: 250,hidden:true },
        { id: "seg_name", header: { text: _("seg_name"), css: "header" }, sort: "server", fillspace: true, adjust: true },
      ],
      onClick: {
        "wxi-trash": function (e, id) {
          webix.confirm(_("del_confirm_msg"), "confirm-warning").then(() => {
            this.remove(id)
            return false
          })
        }
      },
      url: "api/seg",
      save: { url: "api/seg", updateFromResponse: true }
    }

    let form1 = {
      view: "form",
      id: "seg:form1",
      minWidth: 300,
      elementsConfig: { labelWidth: 130 },
      elements: [
        { id: 'id', name: "seg_id", label: "ID", view: "text", required: true, disabled: true, placeholder: "ID", hidden: true },
        { name: "seg_name", label: _("seg_name"), view: "text", required: true, attributes: { maxlength: 200 }, placeholder: _("seg_name") },
        {
          cols: [
            { view: "button", id: "seg:btnunselect", type: "icon", icon: "mdi mdi-select-off", label: _("unselect"), disabled: true },
            { view: "button", id: "seg:btnupd", type: "icon", icon: "mdi mdi-database-edit", label: _("update"), disabled: true },
            { view: "button", id: "seg:btnadd", type: "icon", icon: "wxi-plus", label: _("create"), disabled: false }
          ]
        }
      ],
      rules: {
        seg_name: webix.rules.isNotEmpty
      },
      on: {
        onAfterValidation: (result) => {
          if (!result) webix.message(_("required_msg"))
        }
      }
    }
    let title_page = {
      view: "label", label: `<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("segment_manage")}</span>`
    }
    return { paddingX: 2, cols: [{ rows: [title_page, form, grid1, pager], gravity: 2 }, { view: "resizer" }, { rows: [form1, {}] }] }
  }

  ready(v, urls) {
    //Cau hinh rieng cac cot tĩnh
    webix.ajax().get("api/fld/conf", { url: urls[0].page, params: urls[0].params }).then(result => {
      let json = result.json()
      setConfStaFld(json)
    })
    //
  }

  init() {
    let grid1 = $$("seg:grid1"), form1 = $$("seg:form1")
    form1.bind(grid1)

    const search = () => {
      grid1.clearAll()
      grid1.loadNext(size, 0, null, "seg->api/seg", true)
    }

    $$("seg:btnsearch").attachEvent("onItemClick", () => {
      search()
    })

    $$("seg:btnupd").attachEvent("onItemClick", () => {
      if (form1.isDirty()) {
        if (form1.validate()) {
          grid1.waitSave(() => {
            grid1.updateItem($$("seg:form1").getValues().id, $$("seg:form1").getValues())
          }).then(r => {
            webix.message(_("upd_ok_msg"), "success")
            form1.clear()
            grid1.unselectAll()
          })
        }
      }
    })

    $$("seg:btnadd").attachEvent("onItemClick", () => {
      if (form1.validate()) {
        grid1.waitSave(() => {
          grid1.add($$("seg:form1").getValues())
        }).then(r => {
          webix.message(_("save_ok_msg"), "success")
          form1.clear()
        })
      }
    })

    $$("seg:btnunselect").attachEvent("onItemClick", () => {
      grid1.unselectAll()
    })

    grid1.attachEvent("onBeforeLoad", function () {
      this.showOverlay(_("loading"))
    })
    grid1.attachEvent("onAfterLoad", function () {
      if (!this.count()) this.showOverlay(_("notfound"))
      else this.hideOverlay()
    })

    grid1.attachEvent("onAfterSelect", selection => {
      $$("seg:btnupd").enable()
      $$("seg:btnadd").disable()
      $$("seg:btnunselect").enable()
    })

    grid1.attachEvent("onAfterUnSelect", selection => {
      $$("seg:btnupd").disable()
      $$("seg:btnadd").enable()
      $$("seg:btnunselect").disable()
    })
  }
}

