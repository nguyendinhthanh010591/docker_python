import { JetView } from "webix-jet"
import { size, gridnf, h, w, setConfStaFld, ENT } from "models/util"

export default class VndView extends JetView {
  config() {
    _ = this.app.getService("locale")._
    webix.proxy.dep = {
      $proxy: true,
      load: function (view, params) {
        let obj = $$("dep:form").getValues()
        Object.keys(obj).forEach(
          k => (!obj[k] || obj[k] == "*") && delete obj[k]
        )
        if (!params) params = {}
        params.filter = obj
        return webix.ajax(this.source, params)
      }
    }

    let form = {
      view: "form",
      id: "dep:form",
      minWidth: 500,
      padding: 5,
      margin: 5,
      elementsConfig: { labelWidth: 130 },
      elements: [
        {
          cols: [
            { name: "deptid", view: "text", label: _("dep_id"), attributes: { maxlength: 255 } },
            { name: "deptname", view: "text", label: _("dep_name"), attributes: { maxlength: 255 } },
            { view: "button", id: "dep:btnsearch", label: _("search"), type: "icon", icon: "wxi-search", width: 90, tooltip: _("search") }
          ]
        }
      ]
    }
    let pager = { view: "pager", id: "dep:pager", size: size, template: "{common.first()}{common.prev()}{common.pages()}{common.next()}{common.last()}" }

    let grid1 = {
      view: "datatable",
      id: "dep:grid1",
      select: "row",
      resizeColumn: true,
      datafetch: size,
      pager: "dep:pager",
      columns: [
        { id: "del", header: "", template: `<span class='webix_icon wxi-trash'  title='${_("delete")}'></span>`, css: "header", width: 50},
        { id: "deptid", header: { text: _("dep_id"), css: "header" }, sort: "server", width: 180,hidden:false },
        { id: "deptname", header: { text: _("dep_name"), css: "header" }, sort: "server", fillspace: true, adjust: true },
      ],
      onClick: {
        "wxi-trash": function (e, id) {
          webix.confirm(_("del_confirm_msg"), "confirm-warning").then(() => {
            this.remove(id)
            return false
          })
        }
      },
      url: "api/dep",
      save: { url: "api/dep", updateFromResponse: true }
    }

    let form1 = {
      view: "form",
      id: "dep:form1",
      minWidth: 300,
      elementsConfig: { labelWidth: 130 },
      elements: [
        { id: "id", name: "deptid", label: _("dep_id"), view: "text", required: true, disabled: false, placeholder: _("dep_id"), hidden: false },
        { name: "deptname", label: _("dep_name"), view: "text", required: true, attributes: { maxlength: 200 }, placeholder: _("dep_name") },
        {
          cols: [
            { view: "button", id: "dep:btnunselect", type: "icon", icon: "mdi mdi-select-off", label: _("unselect"), disabled: true },
            { view: "button", id: "dep:btnupd", type: "icon", icon: "mdi mdi-database-edit", label: _("update"), disabled: true },
            { view: "button", id: "dep:btnadd", type: "icon", icon: "wxi-plus", label: _("create"), disabled: false }
          ]
        }
      ],
      rules: {
        deptname: webix.rules.isNotEmpty
      },
      on: {
        onAfterValidation: (result) => {
          if (!result) webix.message(_("required_msg"))
        }
      }
    }
    let title_page = {
      view: "label", label: `<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("Department")}</span>`
    }
    return { paddingX: 2, cols: [{ rows: [title_page, form, grid1, pager], gravity: 2 }, { view: "resizer" }, { rows: [form1, {}] }] }
  }

  ready(v, urls) {
    //Cau hinh rieng cac cot tĩnh
    webix.ajax().get("api/fld/conf", { url: urls[0].page, params: urls[0].params }).then(result => {
      let json = result.json()
      setConfStaFld(json)
    })
    //
  }

  init() {
    let grid1 = $$("dep:grid1"), form1 = $$("dep:form1")
    form1.bind(grid1)

    const search = () => {
      grid1.clearAll()
      grid1.loadNext(size, 0, null, "dep->api/dep", true)
    }

    $$("dep:btnsearch").attachEvent("onItemClick", () => {
      $$("id").enable()
      search()
    })

    $$("dep:btnupd").attachEvent("onItemClick", () => {
      $$("id").enable()
      if (form1.isDirty()) {
        if (form1.validate()) {
          grid1.waitSave(() => {
            grid1.updateItem($$("dep:form1").getValues().id, $$("dep:form1").getValues())
          }).then(r => {
            webix.message(_("upd_ok_msg"), "success")
            form1.clear()
            grid1.unselectAll()
          })
        }
      }
    })

    $$("dep:btnadd").attachEvent("onItemClick", () => {
      if (form1.validate()) {
        grid1.waitSave(() => {
          grid1.add($$("dep:form1").getValues())
        }).then(r => {
          webix.message(_("save_ok_msg"), "success")
          form1.clear()
        })
      }
    })

    $$("dep:btnunselect").attachEvent("onItemClick", () => {
      grid1.unselectAll()
      $$("id").enable()
    })

    grid1.attachEvent("onBeforeLoad", function () {
      this.showOverlay(_("loading"))
    })
    grid1.attachEvent("onAfterLoad", function () {
      if (!this.count()) this.showOverlay(_("notfound"))
      else this.hideOverlay()
    })

    grid1.attachEvent("onAfterSelect", selection => {
      $$("id").disable()
      $$("dep:btnupd").enable()
      $$("dep:btnadd").disable()
      $$("dep:btnunselect").enable()
    })

    grid1.attachEvent("onAfterUnSelect", selection => {
      $$("dep:btnupd").disable()
      $$("dep:btnadd").enable()
      $$("dep:btnunselect").disable()
    })
  }
}

