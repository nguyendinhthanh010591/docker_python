import { JetView } from "webix-jet"
import { filter, size, LDAP_PRIVATE, setConfStaFld, ENT, ROLE } from "models/util"
export default class GroupView extends JetView {
  config() {
    _ = this.app.getService("locale")._
    const USTATUS = [
      { id: "2", value: _("invalid") },
      { id: "1", value: _("valid") }
    ]
    let search = {
      view: "form",
      id: "group:search",
      padding: 5,
      margin: 5,
      minWidth: 500,
      elementsConfig: { labelWidth: 85 },
      elements: [
        
        {
          cols: [
            { view: "text", id: "name", name: "name", label: _("groupname"), attributes: { maxlength: 50 }, placeholder: _("groupname") },
            { view: "combo", id: "status", name: "status", label: _("status"), options: USTATUS },
            //{ view: "combo", id: "ou", name: "ou", label: _("ou"), suggest: { url: `api/ous/bytoken`, filter: filter } }, //"api/cat/nokache/ou"
            { view: "button", id: "btnsearch", type: "icon", icon: "wxi-search", width: 33, tooltip: _("search") }

          ]
        }
      ]
    }

    webix.proxy.group = {
      $proxy: true,
      load: function (view, params) {
        let obj = $$("group:search").getValues()
        Object.keys(obj).forEach(
          k => (!obj[k] || obj[k] == "*") && delete obj[k]
        )
        if (!params) params = {}
        params.filter = obj
        return webix.ajax(this.source, params)
      }
    }

    let pager = { view: "pager", id: "group:pager", size: size, template: "{common.first()}{common.prev()}{common.pages()}{common.next()}{common.last()}" }
    let grid = {
      view: "datatable",
      id: "group:grid",
      select: "row",
      multiselect: false,
      resizeColumn: true,
      datafetch: size,
      columnWidth:200,
      pager: "group:pager",
      columns: [
        {
          id: "id", header: "", width: 35, template: obj => {
            if (obj.status == 2) return `<span class='mdi mdi-account-check', title='${_("restore")}'></span>`
            else return `<span class='mdi mdi-account-off', title='${_("cancel")}'></span>`
          }
        },
       
        { id: "name", header: { text: _("groupname"), css: "header" },format: webix.template.escape, sort: "server" },
        { id: "des", header: { text: _("description"), css: "header" }, fillspace:true,format: webix.template.escape,sort: "server" },
        { id: "status", header: { text: _("status"), css: "header" }, sort: "server", options: USTATUS }
      ],
      onClick: {
        "mdi-account-off": function (e, id) {
          if (!ROLE.includes('PERM_SYS_GROUP_CANCEL')) {
            webix.message(_("no_authorization"), "error")
            return false
          }
          
          let me = this
          webix.ajax(`api/groups/disable/${id}`).then(result => {
            webix.message(result.text())
            let row = id.row, rec = me.getItem(row)
            rec.status = 2
            me.updateItem(row, rec)
          })
        },
        "mdi-account-check": function (e, id) {
          if (!ROLE.includes('PERM_SYS_GROUP_RESTORE')) {
            webix.message(_("no_authorization"), "error")
            return false
          }
          let me = this
          webix.ajax(`api/groups/enable/${id}`).then(result => {
            webix.message(result.text())
            let row = id.row, rec = me.getItem(row)
            rec.status = 1
            me.updateItem(row, rec)
          })
        }
      },
      save: "api/groups"
    }
    let form
    const btns = {
      cols: [
        { view: "button", id: "group:btnxls", type: "icon", icon: "wxi-download", label: _("user_list"), width: 100, hidden: (['scb'].includes(ENT)) ? false : true},
        { view: "button", id: "group:btnunselect", type: "icon", icon: "mdi mdi-select-off", label: _("unselect"), width: 100, disabled: true },
        { view: "button", id: "group:btnupd", type: "icon", icon: "mdi mdi-database-edit", label: _("update"), width: 100, disabled: true },
        { view: "button", id: "group:btnadd", type: "icon", icon: "wxi-plus", label: _("create"), width: 100, disabled: ROLE.includes('PERM_SYS_GROUP_TAPDATA_SAVE') ? false : true },
        ["ctbc"].includes(ENT) ? { view: "button", id: "group:btnSOC", type: "icon", icon: "mdi mdi-cloud-download", label: _("render_SOC"), width: 100 }:{hidden:true}
      ]
    }

      form = {
        view: "form",
        id: "group:form",
        elementsConfig: { labelWidth: 90 },
        elements: [
          { view: "text", name: "id", label: "ID", attributes: { maxlength: 200 },hidden:true, required: true },
          { view: "text",id: "group:name", name: "name", label: _("groupname"), attributes: { maxlength: 200 }, placeholder: _("groupname"), /*validate: v => { return /^[A-Za-z0-9 _]*$/.test(v) }, invalidMessage: _("invalidMessage_general"), */required: true},
          { view: "text", id: "group:des",name: "des", label: _("description"), placeholder: _("description"),/*validate: v => { return /^[A-Za-z0-9 _]*$/.test(v) }, invalidMessage: _("invalidMessage_general"),*/ attributes: { maxlength: 200 } },
          
          { view: "combo", name: "status", label: _("status"), options: USTATUS, required: true, placeholder: _("status") },
          btns
        ],
        rules: {
          status: webix.rules.isNotEmpty, name: webix.rules.isNotEmpty,
          $obj: data => {
            if (!(containhtmlchar(data.name))) {
              webix.message(_("contain_html_char"))
              $$("group:name").focus()
              return false
            }
            if (!(containhtmlchar(data.des))) {
              webix.message(_("contain_html_char"))
              $$("group:des").focus()
              return false
            }
            return true
          }
        },
        on: {
          onAfterValidation: (result) => {
            if (!result) webix.message(_("required_msg"))
          }
        }
      }

    
    


    let group = {
      view: "treetable",
      id: "group:group",
      // select: "row",
      threeState: true ,
      checkboxRefresh: true,
      // multiselect: false,
      // resizeColumn: true,
      columns: [
        // { id: "sel", header: { content: "masterCheckbox", css: "center" },disabled:  true, css: "center", adjust: true, template: "{common.checkbox()}" },
        // { id: "name", header: { text: _("role_list"), css: "header" }, fillspace: 1 }
         { id: "name", header: [{ text: _("role_list"), css: "header" }, { content: "textFilter" }], fillspace: 1, template: "{common.space()}{common.icon()}{common.treecheckbox()}#name#" },
      ],
      filterMode: { level: false, showSubItems: false },
    }

    let group1 = {
      view: "datatable",
      id: "group:group1",
      select: "row",
      multiselect: false,
      resizeColumn: true,
      columns: [
        { id: "sel", header: { content: "masterCheckbox", css: "center" }, css: "center", adjust: true, template: "{common.checkbox()} " },
        { id: "name", header: { text: _("group_grant"), css: "header" }, fillspace: 1 }
      ]
    }

    let manage = {
      view: "treetable",
      id: "group:manage",
      //select: "row",
      //threeState: true,
      checkboxRefresh: true,
      columns: [
        { id: "name", header: [{ text: _("company_name"), css: "header" }, { content: "textFilter" }], fillspace: 1, template: "{common.space()}{common.icon()}{common.treecheckbox()}#name#" },
        { id: "taxc", header: [{ text: _("taxcode"), css: "header" }, { content: "textFilter" }], width: 130 }

      ],
      filterMode: { level: false, showSubItems: false },
    }

    let tabview = {
      view: "tabview",
      id: "tabview",
      minWidth: 400,
      cells: [
        { header: `<span class='mdi mdi-account-multiple-plus'> ${_("update")}</span>`, body: { id: "tab1", rows: [form, {}] } },
        { header: `<span class='mdi mdi-account-group'> ${_("grant")}</span>`, body: { id: "tab2", rows: [group, { cols: [{}, { view: "button", id: "group:btngroup", type: "icon", icon: "mdi mdi-content-save", label: _("save"), width: 100, disabled: true }] }] } },
        { header: `<span class='mdi mdi-account-group'> ${_("group_grant")}</span>`, body: { id: "tab3", rows: [group1, { cols: [{}, { view: "button", id: "group:btngroup1", type: "icon", icon: "mdi mdi-content-save", label: _("save"), width: 100 }] }] } },
      ],
      tabbar: {
        on: {
          onChange: function () {
            const uid = $$("group:grid").getSelectedId(false, true)
            if (uid) {
              const tab = this.getValue()
              if (tab == "tab2") {
                webix.ajax(`api/groups/role/${uid}`).then(data => {
                  $$("group:group").clearAll()
                  $$("group:group").parse(data.json())
                })
              }
              if (tab == "tab3") {
                webix.ajax(`api/groups/grant/${uid}`).then(data => {
                  $$("group:group1").clearAll()
                  $$("group:group1").parse(data.json())
                })
              }
              // else if (tab == "tab3") {
              //   webix.ajax(`api/groups/ou/${uid}`).then(data => {
              //     $$("group:manage").clearAll()
              //     $$("group:manage").parse(data.json())
              //     //$$("group:manage").openAll()
              //   })
              // }
            }
          }
        }
      }
    }
    let title_page = { view: "label", label:`<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("group_manage")}</span>`}
    return { paddingX: 2, cols: [{ rows: [title_page, search, grid, pager], gravity: 1.5 }, { view: "resizer" }, tabview] }
  }

  ready(v, urls) {
    //Cau hinh rieng cac cot tĩnh
    webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
        let json = result.json()
        setConfStaFld(json)
    })
    //
  }
  init() {
    if (!ROLE.includes('PERM_SYS_GROUP')) {
      this.show("/top/home")
      window.location.replace(`/${homepage}`)
      return
  }
  let tabview = $$("tabview"), grid = $$("group:grid"), form = $$("group:form"), group = $$("group:group"), uid, group1 = $$("group:group1")
    //let  manage = $$("group:manage")
    const all = { id: "*", value: _("all") }
    //$$("ou").getList().add(all, 0)
    //$$("ou").setValue("*")
    $$("status").getList().add(all, 0)
    $$("status").setValue("*")

    form.bind(grid, null, function (data) {

      return decodehtml(data)
    });
    $$("btnsearch").attachEvent("onItemClick", () => {
      grid.clearAll()
      grid.loadNext(size, 0, null, "group->api/groups", true)
    })

    $$("group:btnadd").attachEvent("onItemClick", () => {
      if (!form.validate()) return false
      const param = form.getValues()
      
   
      webix.ajax().post("api/groups/add", param).then(result => {
        webix.message(result.text())
        form.clear()
      })
     
    })
    $$("group:btngroup1").attachEvent("onItemClick", () => {
      let rows = group1.serialize(), arr = []
      for (let row of rows) {
        if (row.sel) arr.push(row.id)
      }
      
      webix.ajax().post("api/groups/member1", { uid: uid, arr: arr }).then(() => webix.message(_("save_ok_msg"), "success"))
    })

    //Kết xuất SOC - by Lụa
    if (["ctbc"].includes(ENT)) {
      $$("group:btnSOC").attachEvent("onItemClick", () => {
        const values = $$("group:search").getValues();
        webix.ajax().response("blob").get("api/groups/soc", values).then(result => {
          webix.html.download(result, `${webix.uid()}.csv`)
        })
      })
    }

    $$("group:btnunselect").attachEvent("onItemClick", () => { grid.unselectAll() })

    $$("group:btnupd").attachEvent("onItemClick", () => {
      if (!form.validate()) return false
      const param = form.getValues()
      
   
      webix.ajax().post("api/groups/update", param).then(result => {
        webix.message(result.text())
        form.clear()
        grid.clearAll()
        grid.loadNext(size, 0, null, "group->api/groups", true)
      })
    })

    $$("group:btngroup").attachEvent("onItemClick", () => {
      let rows = group.serialize(), arr = []
      for (let row of rows) {
        if (row.checked) {
          arr.push(row.id)
        } else if (row.data) {
          for (let row_check of row.data) {
            if (row_check.checked) {
              arr.push(row.id)
              break
            }else if (row_check.data) {
              for (let row_check2 of row_check.data) {
                if (row_check2.checked) {
                  arr.push(row.id)
                  break
                }
              }
            }
          }
        }
        if (row.data) {
          for (let row1 of row.data) {
            if (row1.checked) arr.push(row1.id)
            else if (row1.data) {
              for (let row_check of row1.data) {
                if (row_check.checked) {
                  arr.push(row1.id)
                  break
                }
              }
            }
            if (row1.data) {
              for (let row2 of row1.data) {
                if (row2.checked) arr.push(row2.id)
              }
            }
          }
        }
      }
      webix.ajax().post("api/groups/member", { uid: uid, arr: arr }).then(() => webix.message(_("save_ok_msg"), "success"))
    })

    // $$("group:btnmanage").attachEvent("onItemClick", () => {
    //   const rows = manage.getChecked()
    //   let arr = []
    //   for (const row of rows) {
    //     const node = manage.getItem(row)
    //     arr.push(node.taxc)
    //   }
    //   webix.ajax().post("api/groups/manager", { uid: uid, arr: arr }).then(() => webix.message("Ghi dữ liệu thành công", "success"))
    // })
    $$("group:btnxls").attachEvent("onItemClick", () => {
      webix.message(`${_("export_report")}.......`, "info", -1, "excel_ms");
      let params
      let obj = $$("group:search").getValues()
      Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
      if (!params) params = {}
      params.filter = obj
      

      webix.ajax().response("blob").get("api/groups/xls", params).then(data => {
          
          webix.html.download(data, `${webix.uid()}.xlsx`)
          $$("group:search").enable()
          webix.message.hide("excel_ms");
          $$("group:search").hideProgress()
          
      }).catch(e => {

          $$("group:search").enable();
          webix.message.hide("excel_ms");
          $$("group:search").hideProgress();
          

      })

  })
    grid.attachEvent("onAfterSelect", selection => {
      uid = selection.id
      if (ROLE.includes('PERM_SYS_GROUP_TAPDATA_SAVE')) $$("group:btngroup").enable()
      // $$("group:btnmanage").enable()
      if (ROLE.includes('PERM_SYS_GROUP_TAPUPDATE_UPDATE')) $$("group:btnupd").enable()
      $$("group:btnadd").disable()
      if (ROLE.includes('PERM_SYS_GROUP_TAPUPDATE_UNSELECT')) $$("group:btnunselect").enable()
      const tab = tabview.getValue()
      if (tab == "tab2") {
        webix.ajax(`api/groups/role/${uid}`).then(data => {
          group.clearAll()
          group.parse(data.json())
        })
      }
      if (tab == "tab3") {
        webix.ajax(`api/groups/grant/${uid}`).then(data => {
          group1.clearAll()
          group1.parse(data.json())
        })
      }
      // else if (tab == "tab3") {
      //   webix.ajax(`api/groups/ou/${uid}`).then(data => {
      //     const rows = data.json()
      //     manage.clearAll()
      //     manage.parse(rows)
      //     manage.openAll()
      //   })
      // }
    })

    grid.attachEvent("onAfterUnSelect", () => {
      uid = null

      group.clearAll()
      //  manage.clearAll()
      $$("group:btngroup").disable()
      //$$("group:btnmanage").disable()
      $$("group:btnupd").disable()
      if (ROLE.includes('PERM_SYS_GROUP_TAPUPDATE_CREATE')) $$("group:btnadd").enable()
      $$("group:btnunselect").disable()
    })

    grid.attachEvent("onBeforeLoad", function () {
      this.showOverlay(_("loading"))
    })

    grid.attachEvent("onAfterLoad", function () {
      if (!this.count()) this.showOverlay(_("notfound"))
      else this.hideOverlay()
    })
  }
}
