import { JetView } from "webix-jet"
import { SMTP, PORT, setConfStaFld } from "models/util"
export default class SmtpView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        const form = {
            view: "form",
            id: "fmail",
            padding: 3,
            margin: 3,
            elementsConfig: { labelWidth: 120 },
            elements: [
                {
                    cols: [
                        { id: "fmail:type", name: "type", label: _("type"), view: "combo", options: SMTP, value: "SMTP", required: true },
                        { id: "fmail:host", name: "host", label: _("server"), view: "text", attributes: { maxlength: 32 }, required: true }]
                },

                {
                    cols: [
                        { id: "fmail:port", name: "port", label: "Port", view: "combo", options: PORT, value: 25, required: true },
                        { id: "fmail:mail", name: "mail", label: _("account"), view: "text", attributes: { type: "email", maxlength: 32 }, placeholder: _("account"), required: true }
                    ]
                },
                {
                    cols: [
                        { id: "fmail:pwd", name: "pwd", label: _("password"), view: "text", type: "password", attributes: { maxlength: 32 }, placeholder: _("password"), required: true },
                        {
                            cols: [
                                {},
                                { id: "fmail:btndel", view: "button", type: "icon", icon: "mdi mdi-delete-sweep", label: _("delete"), disabled: true, width: 100 },
                                { id: "fmail:btnsave", view: "button", type: "icon", icon: "mdi mdi-email-plus", label: _("update"), width: 100 }
                            ]
                        }
                    ]
                }

            ],
            rules: {
                host: webix.rules.isNotEmpty,
                port: webix.rules.isNotEmpty,
                mail: webix.rules.isNotEmpty,
                mail: webix.rules.isEmail,
                pwd: webix.rules.isNotEmpty
            },
            on: {
                onAfterValidation: (result) => {
                    if (!result) webix.message(_("required_msg"))
                }
            }
        }

        //return { cols: [{}, { rows: [{}, form, {}], css: "transparent-background" }, {}] }
        return { cols: [{}, { gravity: 2, rows: [form, {}] }, {}] }
    }
    ready(v, urls) {
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
    init() {
        let btnsave = $$("fmail:btnsave"), btndel = $$("fmail:btndel"), fmail = $$("fmail")

        const refresh = () => {
            webix.ajax().post("api/ous/smtp", { webix_operation: "sel" }).then(rs => {
                const json = rs.json()
                if (json) {
                    fmail.parse(json)
                    btndel.enable()
                    btnsave.define("label", _("update"))
                }
                else {
                    btndel.disable()
                    btnsave.define("label", _("create"))
                }
                btnsave.refresh()
            })
        }


        $$("fmail:type").attachEvent("onChange", newv => {
            const host = $$("fmail:host"), port = $$("fmail:port")
            if (newv === "GMAIL") {
                host.setValue("smtp.gmail.com")
                port.setValue(465)
                host.disable()
                port.disable()
            }
            else {
                const val = host.getValue()
                if (val === "smtp.gmail.com") host.setValue("")
                port.setValue(25)
                host.enable()
                port.enable()
            }
        })

        btndel.attachEvent("onItemClick", () => {
            webix.confirm(_("mail_del_confirm"), "confirm-warning").then(() => {
                webix.ajax().post("api/ous/smtp", { webix_operation: "del" }).then(() => {
                    webix.message(_("mail_del_ok"))
                    refresh()
                })
            })
        })

        btnsave.attachEvent("onItemClick", () => {
            if (!fmail.validate()) return
            const param = fmail.getValues()
            param.webix_operation = "upd"
            webix.ajax().post("api/ous/smtp", param).then(() => {
                    webix.message(_("ous_update_config_ok"))
                    refresh()
            })
        })

        refresh()

    }
}    
