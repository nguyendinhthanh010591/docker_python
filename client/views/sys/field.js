import { JetView } from "webix-jet";
import { setConfStaFld, ROLE } from "models/util";

export default class FieldConfigView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        const ftData = ["datagrid","other","affectMany"]
        const form = {
            view: "form",
            id: "field:form",
            padding: 1,
            margin: 1,
            elementsConfig: { labelWidth: 105 },
            elements: [
                {
                    cols: [
                        { rows: [
                            { id: "field:f:funcn", name: "funcn", label: _("funcname"), view: "text", attributes: { maxlength: 100 }, required: true, disabled: ROLE.includes('PERM_SYS_FIELD_CONFIG_EDIT') ? false : true, placeholder: _("funcname") },
                            { id: "field:f:funcu", name: "funcu", label: _("funcurl"), placeholder:  _("funcurl"), view: "text", attributes: { maxlength: 100 }, required: true, disabled: ROLE.includes('PERM_SYS_FIELD_CONFIG_EDIT') ? false : true },
                            { id: "field:f:fldn", name: "fldn", label: _("fldname"), view: "text", attributes: { maxlength: 100 }, required: true, disabled: ROLE.includes('PERM_SYS_FIELD_CONFIG_EDIT') ? false : true, placeholder: _("fldname") },
                            { id: "field:f:fldid", name: "fldid", label: _("fldid"), placeholder: _("fldid"), view: "text", attributes: { maxlength: 100 }, required: true, disabled: ROLE.includes('PERM_SYS_FIELD_CONFIG_EDIT') ? false : true },
                        ]},
                        { rows: [
                            { id: "field:f:atr", name: "atr", label: _("attribute"), placeholder:_("attribute"), tooltip:`Other:label,value,disabled,hidden,required | Datagrid:pid,hidden`, view: "text", attributes: { maxlength: 100 }, disabled: ROLE.includes('PERM_SYS_FIELD_CONFIG_EDIT') ? false : true },
                            { id: "field:f:fldtyp", name: "fldtyp", label: _("fldtyp"), view: "richselect", options:ftData, value:"datagrid", required: true, disabled: ROLE.includes('PERM_SYS_FIELD_CONFIG_EDIT') ? false : true },
                            { id: "field:f:feature", name: "feature", label: _("feature"), view: "text",placeholder: _("feature"), attributes: { maxlength: 100 }, disabled: ROLE.includes('PERM_SYS_FIELD_CONFIG_EDIT') ? false : true },
                            { id: "field:f:status", name: "status", label: _("status"), view: "checkbox", disabled: ROLE.includes('PERM_SYS_FIELD_CONFIG_EDIT') ? false : true },
                        ]},
                        { rows: [
                            { id: "field:btnadd", view: "button", type: "icon", icon: "mdi mdi-plus", label: _("add"), width: 85, disabled: ROLE.includes('PERM_SYS_FIELD_CONFIG_EDIT') ? false : true },
                            { id: "field:btnreload", view: "button", type: "icon", icon: "mdi mdi-sync", label: _("refresh"), width: 85, disabled: ROLE.includes('PERM_SYS_FIELD_CONFIG_EDIT') ? false : true},
                            {}
                        ]},
                    ]
                }
            ],
            rules: {
            }
        }

        const grid = {
            view: "datatable",
            id: "field:grid",
            select: "row",
            editable:true,
            editaction:"dblclick",
            resizeColumn: true,
            columns:[
                { id: "del", header: { text: _("delete"), css: "header" }, width: 35, template: `<span class='mdi mdi-close-circle-outline', title='${_("delete")}'></span>` },
                { id: "status", header: [{ text: _("col_use"), css: "header" }], width: 80, checkValue: 1, uncheckValue: 2, template: "{common.checkbox()}", css: "center", hidden: ROLE.includes('PERM_SYS_FIELD_CONFIG_EDIT') ? false : true },
                { id: "funcn", header: { text: _("funcname"), css: "header" }, editor:"popup", editor: ROLE.includes('PERM_SYS_FIELD_CONFIG_EDIT') ? "text" : false },
                { id: "funcu", header: { text: _("funcurl"), css: "header" }, editor:"popup", editor: ROLE.includes('PERM_SYS_FIELD_CONFIG_EDIT') ? "text" : false },
                { id: "fldn", header: { text: _("fldname"), css: "header" }, editor:"popup", editor: ROLE.includes('PERM_SYS_FIELD_CONFIG_EDIT') ? "text" : false },
                { id: "fldid", header: { text: _("fldid"), css: "header" }, editor:"popup", editor: ROLE.includes('PERM_SYS_FIELD_CONFIG_EDIT') ? "text" : false },
                { id: "atr", header: { text: _("attribute"), css: "header" }, editor:"popup", editor: ROLE.includes('PERM_SYS_FIELD_CONFIG_EDIT') ? "text" : false },
                { id: "fldtyp", header: { text: _("fldtyp"), css: "header" }, editor:"richselect", collection:ftData, editor: ROLE.includes('PERM_SYS_FIELD_CONFIG_EDIT') ? "text" : false },
                { id: "feature", header: { text: _("feature"), css: "header" }, editor:"popup", fillspace:true, editor: ROLE.includes('PERM_SYS_FIELD_CONFIG_EDIT') ? "text" : false },
            ],
            url: "api/fld",
            save: "api/pfld",//trung sưa phan quyen
			on: {
				onAfterEditStart: function (r) {
					try{this.getEditor().getPopup().getNode().setAttribute("spellcheck", "false")}catch(e){console.error(e)}
				}
			},
            onClick:
            {
                "mdi-close-circle-outline": function (e, id) {
                    if (!ROLE.includes('PERM_SYS_FIELD_CONFIG_EDIT')) {
                        webix.message(_("no_authorization"), "error")
                        return false
                    }
                    webix.confirm(`${_("delete")}?`, "confirm-warning").then(() => {
                        this.remove(id)
                    })
                }
            }
        }
        const title_page = { view: "label", label:`<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("field_config")}</span>`}
        return { paddingX: 2, rows: [title_page, form, grid] }
    }
    urlChange(v, url) {
        let me = this, grid = $$("inv:grid"), form = $$("inv:form")
        console.log(url)
        // let url = urls[0].page
        // let param = {funcu:url}
        // webix.ajax().post("api/fld", param).then(result => {})
    }
    ready(v, urls) {
        webix.message("Lựa chọn type = datagrid thì atr phải khai pid", "success", 10000)
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
    init() {
        if (!ROLE.includes('PERM_SYS_FIELD_CONFIG')) {
            this.show("/top/home")
            window.location.replace(`/${homepage}`)
            return
        }
        let grid = $$("field:grid"), form = $$("field:form"), fldtyp = $$("field:f:fldtyp")
        // form.bind(grid)
        
        let search = () => {
            grid.load("api/fld")
        }

        let clean = () => {
            form.clear()
            fldtyp.setValue(fldtyp.getList().data.getFirstId())
        }

        $$("field:btnreload").attachEvent("onItemClick", () => {
            search()
        })

        $$("field:btnadd").attachEvent("onItemClick", () => {
            if (!form.validate()) return false
            let param = form.getValues()
            param.webix_operation = "insert"
            webix.ajax().post("api/pfld", param).then(result => {
                const json = result.json()
                if (json==1) {
                   webix.message(_("success"), "success")
                   clean()
                   search()
                } else {
                    webix.message(_("fail"), "error")
                }
            })
        })

        grid.attachEvent("onBeforeLoad", function () {
            this.showOverlay(_("loading"))
        })

        grid.attachEvent("onAfterLoad", function () {
            let self = this, count = self.count()
            // $$("invs:countinv").setValue(`${_("found")} ${count}`)
            if (count) self.hideOverlay()
            else self.showOverlay(_("notfound"))
        })
    }
}
