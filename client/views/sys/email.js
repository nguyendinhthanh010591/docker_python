import { JetView } from "webix-jet"
import { setConfStaFld, EMAILTYPE, ROLE } from "models/util"
const THEAD = "<thead><tr><th>STT</th><th>Hình thức</th><th>Hàng hóa</th><th>Đơn vị tính</th><th>Thuế suất</th><th>Số lượng</th><th>Đơn giá</th><th>Thành tiền</th></tr></thead>"
const TR = "<tr><td style='text-align: center;'>{{line}}</td><td>{{type}}</td><td>{{name}}</td><td>{{unit}}</td><td>{{vrn}}</td><td  style='text-align: right;'>{{fn3 quantity}}</td><td style='text-align: right;'>{{fn2 price}}</td><td style='text-align: right;'>{{fn2 amount}}</td></tr>"
const TBODY = `<tbody><tr style='display:none;'><td>{{#each items}}</td></tr>${TR}<tr style='display: none;'><td>{{/each}}</td></tr></tbody>`
const VAT_THEAD = "<thead><tr><th>Loại thuế suất</th><th>Tổng thành tiền</th><th>Tổng tiền thuế</th></tr></thead>"
const VAT_TR = "<tr><td>{{vrn}}</td><td style='text-align: right;'>{{fn amtv}}</td><td style='text-align: right;'>{{fn vatv}}</td></tr>"
const VAT_TBODY = `<tbody><tr style='display:none;'><td>{{#each tax}}</td></tr>${VAT_TR}<tr style='display: none;'><td>{{/each}}</td></tr></tbody>`
const TEMPLATES = [
    { title: "Tên hóa đơn", content: "{{name}}" },
    { title: "Ngày lập dd/mm/yyyy", content: "{{fd idt}}" },
    { title: "Ngày lập", content: "Ngày {{substr idt 8 2}} Tháng {{substr idt 5 2}} Năm {{substr idt 0 4}}" },
    { title: "Mẫu số", content: "{{form}}" },
    { title: "Ký hiệu", content: "{{serial}}" },
    { title: "Số", content: "{{seq}}" },
    { title: "Chi tiết hóa đơn", content: `<table class='detail' style='width: 100%; border-collapse: collapse;'>${THEAD}${TBODY}</table>` },
    { title: "Thông tin thay thế", content: "{{adj.des}}" },
    { title: "Loại tiền", content: "{{curr}}" },
    { title: "Tỷ giá", content: "{{fn exrt}}" },
    { title: "Loại tiền & tỷ giá", content: "{{#compare curr '!=' 'VND'}} Loại tiền : {{curr}} Tỷ giá: {{fn exrt}}  {{/compare}}" },
    { title: "Mã tra cứu", content: "{{sec}}" },
    { title: "Hình thức thanh toán", content: "{{paym}}" },
    { title: "Tên ĐV bán", content: "{{sname}}" },
    { title: "MST ĐV bán", content: "{{stax}}" },
    { title: "Địa chỉ ĐV bán", content: "{{saddr}}" },
    { title: "Điện thoại ĐV bán", content: "{{stel}}" },
    { title: "Mail ĐV bán", content: "{{smail}}" },
    { title: "Tài khoản ĐV bán", content: "{{sacc}}" },
    { title: "Ngân hàng ĐV bán", content: "{{sbank}}" },
    { title: "Họ tên người mua", content: "{{buyer}}" },
    { title: "Tên ĐV mua", content: "{{bname}}" },
    { title: "MST ĐV mua", content: "{{btax}}" },
    { title: "Địa chỉ ĐV mua", content: "{{baddr}}" },
    { title: "Điện thoại ĐV mua", content: "{{btel}}" },
    { title: "Mail ĐV mua", content: "{{bmail}}" },
    { title: "Tài khoản ĐV mua", content: "{{bacc}}" },
    { title: "Ngân hàng ĐV mua", content: "{{bbank}}" },
    { title: "Ghi chú", content: "{{note}}" },
    { title: "Chiết khấu", content: "{{fn2 discount}}" },
    { title: "Tổng cộng thành tiền", content: "{{fn sumv}}" },
    { title: "Tổng số tiền thanh toán ", content: "{{fn totalv}}" },
    { title: "Tổng số tiền thanh toán bằng chữ", content: "{{word}}" },
    { title: "Chi tiết tiền thuế", content: `<table class='detail' style='width: 100%; border-collapse: collapse;'>${VAT_THEAD}${VAT_TBODY}</table>` },
    { title: "Tổng cộng tiền thuế", content: "{{fn vatv}}" },
    { title: "URL", content: "https://portal.einvoice.fpt.com.vn/search.html"}
]
export default class EmailView extends JetView {
    config() {
        const lang = this.app.getService("locale").getLang()
        const CONFIG = {
            plugins: "print save preview fullpage fullscreen paste searchreplace visualblocks visualchars template table charmap hr pagebreak nonbreaking advlist lists image imagetools textcolor colorpicker contextmenu textpattern code  help",//anchor toc importcss
            toolbar: "save | undo redo | styleselect |formatselect | fontselect fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright alignjustify | outdent indent | image | template |",
            font_formats: "Arial=arial;Courier=courier;Tahoma=tahoma;Times=times;Verdana=verdana;Calibri=calibri;",
            fontsize_formats: "8pt 9pt 10pt 11pt 12pt 13pt 14pt 16pt 18pt 20pt 22pt 24pt 26pt 28pt",
            menubar: "file edit insert format view table help",
            removed_menuitems: 'newdocument',
            entity_encoding: "raw",
            paste_data_images: true,
            relative_urls: false,
            remove_script_host: false,
            templates: TEMPLATES,
            save_onsavecallback: () => {
                if (!ROLE.includes('PERM_SYS_EMAIL_SAVE_UP')) {
                    webix.message(_("no_authorization"), "error")
                    return false
                }
                webix.ajax().post("api/email", { content: $$("email:editor").getValue(), etype:$$("email:type").getValue() }).then(result => { webix.message(result.json()) })
            },
            images_upload_handler: (blobInfo, success) => {
                success("data:" + blobInfo.blob().type + ";base64," + blobInfo.base64())
            }
        }
        const CONFIG_ALL = {
            plugins: "print save preview fullpage fullscreen paste searchreplace visualblocks visualchars template table charmap hr pagebreak nonbreaking advlist lists image imagetools textcolor colorpicker contextmenu textpattern code  help",//anchor toc importcss
            toolbar: "undo redo | styleselect |formatselect | fontselect fontsizeselect | bold italic underline | forecolor backcolor | alignleft aligncenter alignright alignjustify | outdent indent | image | template |",
            font_formats: "Arial=arial;Courier=courier;Tahoma=tahoma;Times=times;Verdana=verdana;Calibri=calibri;",
            fontsize_formats: "8pt 9pt 10pt 11pt 12pt 13pt 14pt 16pt 18pt 20pt 22pt 24pt 26pt 28pt",
            menubar: "file edit insert format view table help",
            removed_menuitems: 'newdocument',
            entity_encoding: "raw",
            paste_data_images: true,
            templates: TEMPLATES,
            save_onsavecallback: () => {
                if (!ROLE.includes('PERM_SYS_EMAIL_SAVE_UP')) {
                    webix.message(_("no_authorization"), "error")
                    return false
                }
                webix.ajax().post("api/email", { content: $$("email:editor").getValue(), etype:$$("email:type").getValue() }).then(result => { webix.message(result.json()) })
            },
            images_upload_handler: (blobInfo, success) => {
                success("data:" + blobInfo.blob().type + ";base64," + blobInfo.base64())
            }
        }
    
        if (lang == "vi")  CONFIG.language="vi_VN"
        const editor= { id: "email:editor", view: "tinymce-editor", config: CONFIG}
        const etype = { id: "email:type", name: "type", label: _("email_type"), view: "combo", options: EMAILTYPE(this.app), width:200 }
        let title_page = { view: "label", label:`<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("mail_template")}</span>`}
        return { paddingX: 2, id: "email:layout", rows: [title_page,{cols:[etype, {}]},editor]}
    }
    ready(v, urls) {
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
    init() {
        if (!ROLE.includes('PERM_SYS_EMAIL')) {
            this.show("/top/home")
            window.location.replace(`/${homepage}`)
            return
        }
        let etypef = $$("email:type")
        const refreshForm = () => {
            let etype = etypef.getValue()

            webix.ajax().get("api/email",{etype}).then(result => {
                $$("email:editor").setValue(result.json())
            })
        }
        etypef.attachEvent("onChange", () => {
            refreshForm()
        })
        etypef.setValue("-1")
    }
}    