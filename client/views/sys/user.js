import { JetView } from "webix-jet"
import { d2s, filter, SITYPE, SUSTATUS, size, LDAP_PRIVATE, IS_USE_LOCAL_USER, ENT, setConfStaFld, ROLE } from "models/util"
export default class UserView extends JetView {
  config() {
    _ = this.app.getService("locale")._
    const USTATUS = [
      { id: "2", value: _("canceled") },
      { id: "1", value: _("valid") }
    ]

    let search = {
      view: "form",
      id: "user:search",
      padding: 5,
      margin: 5,
      minWidth: 500,
      elementsConfig: { labelWidth: 85 },
      elements: [
        {
          cols: [
            { view: "text", id: "id", name: "id", label: _("account"), placeholder: _("account") /*,attributes: { maxlength: 50 }*/ },
            { view: "text", id: "name", name: "name", label: _("fullname"), attributes: { maxlength: 50 }, placeholder: _("fullname")},
            { view: "text", id: "pos", name: "pos", label: _("position"), attributes: { maxlength: 50 }, placeholder: _("position")},
            { width: 33 }
          ]
        },
        {
          cols: [
            { view: "text", id: "mail", name: "mail", label: "Email", attributes: { maxlength: 50 }, placeholder: "Email"},
            { view: "combo", id: "uc", name: "uc", label: _("status"), options: USTATUS },
            ENT != "vcm" ?//vcm mutiselect
                //{
                { id: "ou", name: "ou", label: _("ou"), view: "combo", suggest: { url: `api/ous/bytoken`, filter: filter }  }
                : 
                { id: "ou", name: "ou", label: _("ou"), view: "multiselect", suggest: {selectAll: true, url: `api/ous/bytoken`, filter: filter, template:webix.template("#id#__#value#")} }, //"api/cat/nokache/ou"
            { view: "button", id: "btnsearch", type: "icon", icon: "wxi-search", width: 33, tooltip: _("search") }

          ]
        }
      ]
    }

    webix.proxy.user = {
      $proxy: true,
      load: function (view, params) {
        let obj = $$("user:search").getValues()
        Object.keys(obj).forEach(
          k => (!obj[k] || obj[k] == "*") && delete obj[k]
        )
        if (!params) params = {}
        params.filter = obj
        return webix.ajax(this.source, params)
      }
    }

    let pager = { view: "pager", id: "user:pager", size: size, template: "{common.first()}{common.prev()}{common.pages()}{common.next()}{common.last()}" }
    let grid = {
      view: "datatable",
      id: "user:grid",
      select: "row",
      multiselect: false,
      resizeColumn: true,
      datafetch: size,
      pager: "user:pager",
      columns: [
        {
          id: "btn", header: "", width: 35, template: obj => {
            if (obj.uc == 2) return `<span class='mdi mdi-account-check', title='${_("restore")}'></span>`
            else return `<span class='mdi mdi-account-off', title='${_("cancel")}'></span>`
          }
        },
        { id: "id", header: { text: _("account"), css: "header" }, sort: "server", adjust: true, format: webix.template.escape },
        { id: "mail", header: { text: "Email", css: "header" }, format: webix.template.escape, sort: "server", adjust: true },
        { id: "name", header: { text: _("fullname"), css: "header" }, format: webix.template.escape, sort: "server", adjust: true },
        { id: "pos", header: { text: _("position"), css: "header" }, format: webix.template.escape, sort: "server", adjust: true },
        { id: "code", header: { text: _("user_code"), css: "header" }, format: webix.template.escape, sort: "server", adjust: true },
        { id: "ou", header: { text: _("ou"), css: "header" }, sort: "server", options: "api/cat/nokache/ou", adjust: true, fillspace: 1 },
        { id: "uc", header: { text: _("status"), css: "header" }, sort: "server", options: USTATUS, adjust: true }
      ],
      onClick: {
        "mdi-account-off": function (e, id) {
          if (!ROLE.includes('PERM_SYS_USER_RESTORE')) {
            webix.message(_("no_authorization"), "error")
            return false
          }
          let me = this
          webix.ajax(`api/user/disable/${id}`).then(result => {
            webix.message(result.text())
            let row = id.row, rec = me.getItem(row)
            rec.uc = 2
            me.updateItem(row, rec)
          })
        },
        "mdi-account-check": function (e, id) {
          if (!ROLE.includes('PERM_SYS_USER_CANCEL')) {
            webix.message(_("no_authorization"), "error")
            return false
          }
          let me = this
          webix.ajax(`api/user/enable/${id}`).then(result => {
            webix.message(result.text())
            let row = id.row, rec = me.getItem(row)
            rec.uc = 1
            me.updateItem(row, rec)
          })
        }
      },
      save: "api/user"
    }
    let form
    const btns = {
      cols: [
        { view: "button", id: "user:btnxls", type: "icon", icon: "wxi-download",  label: _("user_list"), width: 100 , hidden: (["scb"].includes(ENT)) ? false : true},
        { view: "button", id: "user:btnunselect", type: "icon", icon: "mdi mdi-select-off", label: _("unselect"), width: 100, disabled: true },
        { view: "button", id: "user:btnupd", type: "icon", icon: "mdi mdi-database-edit", label: _("update"), width: 100, disabled: true },
        { view: "button", id: "user:btnadd", type: "icon", icon: "wxi-plus", label: _("create"), width: 100, disabled: ROLE.includes('PERM_SYS_USER_TAPUPDATE_CREATE') ? false : true },
        ["ctbc"].includes(ENT) ? { view: "button", id: "user:btnSOC", type: "icon", icon: "mdi mdi-cloud-download", label: _("render_SOC"), width: 100 }:{hidden:true}
      ]
    }

    if (LDAP_PRIVATE) {
      form = {
        view: "form",
        id: "user:form",
        elementsConfig: { labelWidth: 90 },
        elements: [
          { view: "checkbox", label: _("is_local"), id: "user:local", name: "local", value: 0, hidden: true },
          { view: "text", id: "user:id", name: "id", label: _("account"), attributes: { maxlength: ["hsy"].includes(ENT) ? 50 : 20}/*, validate: v => { return /[A-Za-z0-9]/.test(v) }, invalidMessage: _("invalidMessage_user")*/, required: true },
          { view: "text", id: "user:mail", name: "mail", label: "Email", type: "email", attributes: { maxlength: 50 }, required: true },
          { view: "text",id: "user:name", name: "name", label: _("fullname"), attributes: { maxlength: 50 }, required: true },
          { view: "text", id: "user:pos",name: "pos", label: _("position"), attributes: { maxlength: 50 } },
          { view: "text",id: "user:code", name: "code", label: _("user_code"), attributes: { maxlength: 20 } },
          { view: "combo", name: "ou", label: _("ou"), suggest: { url: `api/ous/bytoken`, filter: filter }, required: true, hidden:(["kbank"].includes(ENT)) ? true:false },
          btns
        ],
        rules: {
          id: webix.rules.isNotEmpty
          , mail: webix.rules.isNotEmpty
          , ou: webix.rules.isNotEmpty
          , name: webix.rules.isNotEmpty,
          $obj: data => {
            if (!webix.rules.isEmail(data.mail.trim())) {
              webix.message(_("mail_invalid"))
              $$("user:mail").focus()
              return false
            }
            
            if (!(containhtmlchar(data.id))) {
              webix.message(_("contain_html_char"))
              $$("user:mail").focus()
              return false
            }
            if (!(containhtmlchar(data.mail))) {
              webix.message(_("contain_html_char"))
              $$("user:mail").focus()
              return false
            }
            if (!(containhtmlchar(data.name))) {
              webix.message(_("contain_html_char"))
              $$("user:name").focus()
              return false
            }
            if (!(containhtmlchar(data.pos))) {
              webix.message(_("contain_html_char"))
              $$("user:pos").focus()
              return false
            }
            if (!(containhtmlchar(data.code))) {
              webix.message(_("contain_html_char"))
              $$("user:code").focus()
              return false
            }
            return true
          }
        },
        on: {
          onAfterValidation: (result) => {
            if (!result) webix.message(_("required_msg"))
          }
        }
      }

    }
    else {
      form = {
        view: "form",
        id: "user:form",
        elementsConfig: { labelWidth: 90 },
        visibleBatch: "batch0",
        elements: [
          { view: "checkbox", label: _("is_local"), id: "user:local", name: "local", value: 0, hidden: true },
          { view: "text", id: "user:id", name: "id", label: _("account"), attributes: { maxlength: ["hsy"].includes(ENT) ? 50 : 20 }, placeholder: _("account"),/*validate: v => { return /^[A-Za-z0-9 _]*$/.test(v) }, invalidMessage: _("invalidMessage_user"), */required: true },
          { batch: "batch1", id: "user:mail", view: "text", name: "mail", label: "Email", type: "email", attributes: { maxlength: 50 }, disabled: true },
          { batch: "batch1", id: "user:name", view: "text", name: "name", label: _("fullname"), attributes: { maxlength: 50 } },
          { batch: "batch1", id: "user:pos", view: "text", name: "pos", label: _("position"), attributes: { maxlength: 50 } },
          { batch: "batch1", id: "user:code", view: "text", name: "code", label: _("user_code"), attributes: { maxlength: 20 } },
          // { batch: "batch1", view: "combo", name: "ou", label: _("ou"), suggest: { url: `api/ous/bytoken`, filter: filter } },
          { view: "combo", name: "ou", label: _("ou"), suggest: { url: `api/ous/bytoken`, filter: filter }, required: true ,hidden:(["kbank"].includes(ENT)) ? true:false },
          btns
        ],
        rules: {
          id: webix.rules.isNotEmpty,
          ou: webix.rules.isNotEmpty,
          $obj: data => {
            if (data.local) {
              if (!webix.rules.isNotEmpty(data.mail)) {
                $$("user:mail").focus()
                return false
              }
              if (!webix.rules.isNotEmpty(data.name)) {
                $$("user:name").focus()
                return false
              }
              if (!webix.rules.isEmail(data.mail.trim())) {
                webix.message(_("mail_invalid"))
                $$("user:mail").focus()
                return false
              }
            }
            if (!(containhtmlchar(data.mail))) {
              webix.message(_("contain_html_char"))
              $$("user:mail").focus()
              return false
            }
            if (!(containhtmlchar(data.name))) {
              webix.message(_("contain_html_char"))
              $$("user:name").focus()
              return false
            }
            if (!(containhtmlchar(data.pos))) {
              webix.message(_("contain_html_char"))
              $$("user:pos").focus()
              return false
            }
            if (!(containhtmlchar(data.pos))) {
              webix.message(_("contain_html_char"))
              $$("user:code").focus()
              return false
            }
            return true
          } 
        },
        on: {
          onAfterValidation: (result) => {
            if (!result) webix.message(_("required_msg"))
          }
        }
      }
    }


    let group = {
      view: "datatable",
      id: "user:group",
      select: "row",
      multiselect: false,
      resizeColumn: true,
      columns: [
        { id: "sel", header: { content: "masterCheckbox", css: "center" }, css: "center", adjust: true, template: "{common.checkbox()} " },
        { id: "name", header: { text: _("group"), css: "header" }, fillspace: 1 }
      ]
    }
	 let group_sys = {
      view: "datatable",
      id: "user:groupsys",
      select: "row",
      multiselect: false,
      resizeColumn: true,
      columns: [
        { id: "sel", header: { content: "masterCheckbox", css: "center" }, css: "center", adjust: true, template: "{common.checkbox()}" },
        { id: "name", header: { text: _("system"), css: "header" }, fillspace: 1 }
      ]
    }				 

    let manage = {
      view: "treetable",
      id: "user:manage",
      //select: "row",
      threeState: (ENT == "vcm") ? true : false,
      checkboxRefresh: true,
      columns: [
        { id: "name", header: [{ text: _("company_name"), css: "header" }, { content: "textFilter" }], fillspace: 1, template: "{common.space()}{common.icon()}{common.treecheckbox()}#name#" },
        { id: "taxc", header: [{ text: _("taxcode"), css: "header" }, { content: "textFilter" }], width: 130 }

      ],
      filterMode: { level: false, showSubItems: false },
    }

    //them gan dai thong bao phat hanh
    let group_TBPH = {
      view: "datatable",
      id: "user:tbph",
      select: "row",
      multiselect: false,
      resizeColumn: true,
      columns: [
        { id: "sel", header: { content: "masterCheckbox", css: "center" }, adjust: "header", css: "center", template: "{common.checkbox()}" },
        { id: "taxc", header: [{ text: _("taxcode"), css: "header" }, { content: "textFilter" }], sort: "string", adjust: true },
        { id: "form", header: [{ text: _("form"), css: "header" }, { content: "textFilter" }], sort: "string", adjust: true },
        { id: "serial", header: [{ text: _("serial"), css: "header" }, { content: "textFilter" }], sort: "string", adjust: true },
        {
          id: "fd", header: [{ text: _("valid_from"), css: "header" }, {
            content: "datepickerFilter", prepare: function (filterValue, filterObject) {
              return filterValue;
            },
            compare: function (cellValue, filterValue) {
              let vcellValue = new Date(Date.parse(String(cellValue).substring(0, String(cellValue).length - 1)))
              return vcellValue >= filterValue;
            }
          }], sort: "server", stringResult: true, format: d2s, adjust: true
        },
        { id: "uses", header: [{ text: _("input_type"), css: "header" }, { content: "selectFilter" }], sort: "string", options: SITYPE(this.app), adjust: true },
        { id: "min", header: [{ text: _("from"), css: "header" },], sort: "server", css: "right", adjust: true },
        { id: "max", header: [{ text: _("to"), css: "header" },], sort: "server", css: "right", adjust: true },
        { id: "cur", header: [{ text: _("current"), css: "header" },], sort: "server", css: "right", adjust: true },
        { id: "status", header: [{ text: _("status"), css: "header" }, { content: "selectFilter" }], sort: "string", options: SUSTATUS(this.app), adjust: true },
      ]
    }

    let tabview = {
      view: "tabview",
      id: "tabview",
      minWidth: 400,
      cells: [
        { header: `<span class='mdi mdi-account-multiple-plus'> ${_("update")}</span>`, body: { id: "tab1", rows: [form, {}] } },
        { header: `<span class='mdi mdi-account-group'> ${_("group_grant")}</span>`, body: { id: "tab2", rows: [group, { cols: [{}, { view: "button", id: "user:btngroup", type: "icon", icon: "mdi mdi-content-save", label: _("save"), disabled: (["scb"].includes()) ? false : true, width: 100 }] }] } },
        { header: `<span class='mdi mdi-database-lock'> ${_("data")}</span>`, body: { id: "tab3", rows: [manage, { cols: [{}, { view: "button", id: "user:btnmanage", type: "icon", icon: "mdi mdi-content-save", label: _("save"), disabled: true, width: 100 }] }] } },
        { header: `<span class='mdi mdi-account-group'> ${_("system")}</span>`, hidden: (["scb"].includes(ENT)) ? false : true, body: { id: "tab4", rows: [group_sys, { cols: [{}, { view: "button", id: "user:btnsystem", type: "icon", icon: "mdi mdi-content-save", label: _("save"), disabled: true, width: 100 }] }] } },
        { header: `<span class='mdi mdi mdi-bullhorn'> ${_("released")}</span>`,hidden: (["vcm"].includes(ENT)) ? false : true, body: { id: "tab5", rows: [group_TBPH, { cols: [{}, { view: "button", id: "user:btnTBPH", type: "icon", icon: "mdi mdi-content-save", label: _("save"), disabled: true, width: 100 }] }] } },
      ],
      tabbar: {
        on: {
          onChange: function () {
            const uid = $$("user:grid").getSelectedId(false, true)
            if (uid) {
              const tab = this.getValue()
              if (tab == "tab2") {
                webix.ajax(`api/user/role/${uid}`).then(data => {
                  $$("user:group").clearAll()
                  $$("user:group").parse(data.json())
                })
              }
              else if (tab == "tab3") {
                webix.ajax(`api/user/ou/${uid}`).then(data => {
                  $$("user:manage").clearAll()
                  $$("user:manage").parse(data.json())
                  //$$("user:manage").openAll()
                })
              }
              else if (tab == "tab4") {
                webix.ajax(`api/user/roledata/${uid}`).then(data => {
                  $$("user:groupsys").clearAll()
                  $$("user:groupsys").parse(data.json())
                })
              }
              else if (tab == "tab5") {
                webix.ajax(`api/serial/seusr/${uid}`).then(data => {
                  $$("user:tbph").clearAll()
                  $$("user:tbph").parse(data.json())
                })
              }
            }
          }
        }
      }
    }
    let title_page = { view: "label", label:`<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("user_manage")}</span>`}
    return { paddingX: 2, cols: [{ rows: [title_page, search, grid, pager], gravity: 1.5 }, { view: "resizer" }, tabview] }
  }

  ready(v, urls) {
    //Cau hinh rieng cac cot tĩnh
    webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
        let json = result.json()
        setConfStaFld(json)
    })
    //
  }

  init() {
    if (!ROLE.includes('PERM_SYS_USER')) {
      this.show("/top/home")
      window.location.replace(`/${homepage}`)
      return
  }
  let tabview = $$("tabview"), grid = $$("user:grid"), form = $$("user:form"), group = $$("user:group"), manage = $$("user:manage"), system = $$("user:groupsys"),grid_TBPH = $$("user:tbph"), uid
    const all = { id: "*", value: _("all") }
    if (ENT != "vcm") {
      $$("ou").getList().add(all, 0)
      $$("ou").setValue("*")
    }
    $$("uc").getList().add(all, 0)
    $$("uc").setValue("*")
    if (IS_USE_LOCAL_USER) $$("user:local").show()
    form.bind(grid, null, function (data) {

      return decodehtml(data)
    });
    if (ENT == "scb"){
      $$("user:btnadd").hide()
    }
    $$("btnsearch").attachEvent("onItemClick", () => {
      grid.clearAll()
      grid.loadNext(size, 0, null, "user->api/user", true)
    })

    $$("user:btnadd").attachEvent("onItemClick", () => {
      if (!form.validate()) return false
      const xt = $$("user:local").getValue()
      const param = form.getValues()
      if (xt == 1) {
        webix.ajax().post(`api/ads/add2`, param).then(result => {
          webix.message(result.text())
          form.clear()
        })
      }
      else {
        if (LDAP_PRIVATE) {
          webix.ajax().post("api/ads/add", param).then(result => {
            webix.message(result.text())
            form.clear()
          })
        }
        else {

          webix.ajax().post(`api/ads/add1`, param).then(result => {
            webix.message(result.text())
            form.clear()
          })
        }
      }
    })
    //Kết xuất SOC - by Lụa
    if (["ctbc"].includes(ENT)) {
      $$("user:btnSOC").attachEvent("onItemClick", () => {
        const values = $$("user:search").getValues();
        webix.ajax().response("blob").get("api/user/soc", values).then(result => {
          webix.html.download(result, `${webix.uid()}.csv`)
        })
      })
    }

    $$("user:btnunselect").attachEvent("onItemClick", () => { grid.unselectAll() })

    $$("user:btnupd").attachEvent("onItemClick", () => {
      if (!form.validate()) return false
      if (form.isDirty() && form.validate()) {
        const mail = form.getDirtyValues().mail
        form.save()
        if ((LDAP_PRIVATE || IS_USE_LOCAL_USER) && mail) {
          webix.ajax().post("api/ads/mail", { uid: uid, mail: mail }).then(result => {
            if (result.json() == 1) webix.message(_("user_update_ok"), "success")
          })
        }
        else webix.message(_("user_update_ok"), "success")
      }
    })

      $$("user:btnxls").attachEvent("onItemClick", () => {
        webix.message(`${_("export_report")}.......`, "info", -1, "excel_ms");
        let params
        let obj = $$("user:search").getValues()
        Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
        if (!params) params = {}
        params.filter = obj
        

        webix.ajax().response("blob").get("api/user/xls", params).then(data => {
            
            webix.html.download(data, `${webix.uid()}.xlsx`)
            $$("user:form").enable()
            webix.message.hide("excel_ms");
            $$("user:form").hideProgress()
            
        }).catch(e => {

            $$("user:form").enable();
            webix.message.hide("excel_ms");
            $$("user:form").hideProgress();
            

        })

    })

    $$("user:btngroup").attachEvent("onItemClick", () => {
      let rows = group.serialize(), arr = []
      for (let row of rows) {
        if (row.sel) arr.push(row.id)
      }
      webix.ajax().post("api/user/member", { uid: uid, arr: arr }).then(() => webix.message(_("save_ok_msg"), "success"))
    })

    $$("user:btnmanage").attachEvent("onItemClick", () => {
      const rows = manage.getChecked()
      let arr = []
      for (const row of rows) {
        const node = manage.getItem(row)
        arr.push(node.taxc)
      }
      webix.ajax().post("api/user/manager", { uid: uid, arr: arr }).then(() => webix.message(_("save_ok_msg"), "success"))
    })
	$$("user:btnsystem").attachEvent("onItemClick", () => {
      let rows = system.serialize(), arr = []
      for (let row of rows) {
        if (row.sel) arr.push(row.name)
      }
      webix.ajax().post("api/user/system", { uid: uid, arr: arr }).then(() => webix.message(_("save_ok_msg"), "success"))
    })

    $$("user:btnTBPH").attachEvent("onItemClick", () =>{
      const sers = grid_TBPH.serialize()
      let arr = []
      for (const ser of sers) {
        console.log(ser);
        if (ser.sel == 1) arr.push(ser.id)
      }
      webix.ajax().post("api/serial/seusr", { usid: uid, arr: arr }).then(() => { webix.message(_("save_ok_msg"), "success") })
    })

    const showhideprivateldap = (plocal) => {
      if ((LDAP_PRIVATE)) {
        $$("user:mail").show()
        $$("user:mail").enable()
        $$("user:name").show()
        $$("user:pos").show()
        $$("user:code").show()
      } else {
        if (IS_USE_LOCAL_USER && plocal) {
          $$("user:mail").show()
          $$("user:mail").enable()
          $$("user:name").show()
          $$("user:pos").show()
          $$("user:code").show()
        }
        else {
          $$("user:mail").hide()
          $$("user:mail").disable()
          $$("user:name").hide()
          $$("user:pos").hide()
          $$("user:code").hide()
        }
      }
    }

    $$("user:local").attachEvent("onChange", function (newValue, oldValue, config) {
      showhideprivateldap(newValue)
    });
    
    grid.attachEvent("onAfterSelect", selection => {
      uid = selection.id
      if (IS_USE_LOCAL_USER) {
        let record = grid.getItem(uid)
        let local = record.local
        showhideprivateldap(local)
        $$("user:local").disable()
      }

      $$("user:id").disable()
      if (ENT == "scb") {
        $$("user:mail").disable()
        $$("user:name").disable()
        $$("user:pos").disable()
        $$("user:code").disable()
      }

      if (ENT != "scb") {
        if (ROLE.includes('PERM_SYS_USER_TAPGROUPGRANT_SAVE')) $$("user:btngroup").enable()

      }
      if (ENT == "vcm") {
        $$("user:btnTBPH").enable()
      }
      if (ROLE.includes('PERM_SYS_USER_TAPDATA_SAVE')) $$("user:btnmanage").enable()
      $$("user:btnsystem").enable()
      if (ROLE.includes('PERM_SYS_USER_TAPUPDATE_UPDATE')) $$("user:btnupd").enable()
      $$("user:btnadd").disable()
      if (["ctbc"].includes(ENT)) $$("user:btnSOC").disable()
      if (ROLE.includes('PERM_SYS_USER_TAPUPDATE_UNSELECT')) $$("user:btnunselect").enable()
      const tab = tabview.getValue()
      if (tab == "tab2") {
        webix.ajax(`api/user/role/${uid}`).then(data => {
          group.clearAll()
          group.parse(data.json())
        })
      }
      else if (tab == "tab3") {
        webix.ajax(`api/user/ou/${uid}`).then(data => {
          const rows = data.json()
          manage.clearAll()
          manage.parse(rows)
          manage.openAll()
        })
      }
      else if (tab == "tab4") {
        webix.ajax(`api/user/roledata/${uid}`).then(data => {
          system.clearAll()
          system.parse(data.json())
        })
      }
      else if (tab == "tab5") {
        webix.ajax(`api/serial/seusr/${uid}`).then(data => {
          $$("user:tbph").clearAll()
          $$("user:tbph").parse(data.json())
        })
      }
    })

    grid.attachEvent("onAfterUnSelect", () => {
      uid = null
      $$("user:id").enable()
      if ($$("user:local")) $$("user:local").enable()
      if (ENT == "scb"){
        $$("user:mail").enable()
        $$("user:name").enable()
        $$("user:pos").enable()
        $$("user:code").enable()
      }
      if (!LDAP_PRIVATE) form.showBatch("batch1")

      group.clearAll()
      manage.clearAll()
      if (ENT != "scb"){
        $$("user:btngroup").disable()
      }
      $$("user:btnmanage").disable()
      $$("user:btnupd").disable()
      if (ROLE.includes('PERM_SYS_USER_TAPUPDATE_CREATE')) $$("user:btnadd").enable()
      if (["ctbc"].includes(ENT)) $$("user:btnSOC").enable()
      $$("user:btnunselect").disable()
      $$("user:local").setValue(1)
    })

    grid.attachEvent("onBeforeLoad", function () {
      this.showOverlay(_("loading"))
    })

    grid.attachEvent("onAfterLoad", function () {
      if (!this.count()) this.showOverlay(_("notfound"))
      else this.hideOverlay()
    })
  }
}
