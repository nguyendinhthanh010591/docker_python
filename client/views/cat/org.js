import { JetView } from "webix-jet"
import {ROLE, LinkedInputs, h, w, size, gridnf3, CARR, ENT,ORG_EDIT, RPP } from "models/util"
const ISTATUS = [
  { id: "1", value: "Nhập tay" },
  { id: "2", value: "Tự động" }
 
]
const moment = require("moment")
class XlsWin extends JetView {
  config() {
    let grid = {
      view: "datatable",
      id: "org:xlsgrid",
      columns: [
        {
          id: "chk", header: { content: "masterCheckbox", css: "center" }, width: 35, template: (obj, common, value, config) => {
            if (obj.error) return ""
            else return common.checkbox(obj, common, value, config)
          },
        },
        { id: "error", header: { text: `${_("status")} <span class='webix_icon wxi-download' title='excel'></span>`, css: "header" }, css: { color: "red" }, adjust: true },
        { id: "taxc", header: { text: _("taxcode"), css: "header" }, adjust: true },
        { id: "code", header: { text: _("cus_code"), css: "header" }, adjust: true },
        { id: "name", header: { text: _("customer_name"), css: "header" }, adjust: true },
        { id: "name_en", header: { text: _("customer_name_en"), css: "header" }, adjust: true },
        { id: "addr", header: { text: _("address"), css: "header" }, adjust: true },
        { id: "mail", header: { text: "Email", css: "header" }, adjust: true },
        { id: "tel", header: { text: _("tel"), css: "header" }, adjust: true },
        { id: "acc", header: { text: _("account"), css: "header" }, adjust: true },
        { id: "bank", header: { text: _("bank"), css: "header" }, adjust: true },
      ],
      onClick: {
        "wxi-download": function (e, id) {
          if (this.count()) webix.toExcel(this, { ignore: { "chk": true } })
        }
      }
    }
    let form = {
      view: "form",
      elements: [
        {
          cols: [
            { view: "button", type: "icon", icon: "mdi mdi-download", label: _("file_sample"), click: () => { window.location.href = "temp/org.xlsx" } },
            { id: "org:btnsave", view: "button", type: "icon", icon: "mdi mdi-content-save", label: _("file_save") },
            { id: "org:btnread", view: "button", type: "icon", icon: "mdi mdi-file-upload-outline", label: _("file_read") },
            { id: "org:file", view: "uploader", multiple: false, autosend: false, link: "org:filelist", accept: "application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", upload: "api/org/upload", type: "icon", icon: "mdi mdi-file-find-outline", label: _("file_select"), formData: () => { return { type: "00ORGS" } } },
            { id: "org:filelist", view: "list", type: "uploader", autoheight: true, borderless: true, gravity: 3 }
          ]
        }
      ]
    }

    return {
      view: "window",
      id: "org:xlswin",
      height: h,
      width: w,
      position: "center",
      resize: true,
      modal: true,
      head: {
        view: "toolbar", height: 35, css: 'toolbar_window',
        cols: [
          { view: "label", label: _("import_excel") },
          { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => { this.getRoot().hide() } }
        ]
      },
      body: { rows: [grid, form] }
    }
  }
  show() {
    this.getRoot().show()
    $$("org:file").setValue(null)
  }
}
const init = (cols) => {
  //1-grid
  let arr = []
  for (const col of cols) {
    if (col.view == "datepicker") col.format = webix.i18n.dateFormatStr
    const obj = { id: col.name, header: { text: col.label, css: "header" }, adjust: true }
    if (col.type == "number") {
      obj.css = "right"
      obj.inputAlign = "right"
      obj.format = gridnf3.format
    }
    arr.push(obj)
  }
  let grid1 = $$("org:grid"), grid2 = $$("org:xlsgrid")
  let a1 = grid1.config.columns.filter(item => !CARR.includes(item.id))
  let a2 = grid2.config.columns.filter(item => !CARR.includes(item.id))
  if (arr.length > 0) {
    grid1.config.columns = a1.concat(arr)
    grid2.config.columns = a2.concat(arr)
  }
  else {
    grid1.config.columns = a1
    grid2.config.columns = a2
  }
  grid1.refreshColumns()
  grid2.refreshColumns()
  //2-form
  const pos = 8
  let form = $$("org:form1"), len = cols.length
  for (let i = 0; i < 5; i++) {
    form.removeView(`org:r${i}`)
  }
  if (len > 0) {
    if (len % 2) {
      cols.push({})
      len++
    }
    const j = len / 2
    for (let i = 0; i < 5; i++) {
      if (i <= j) form.addView({ id: `org:r${i}`, cols: cols.splice(0, 2) }, pos + i)
      else form.addView({ id: `org:r${i}`, hidden: true }, pos + i)
    }
  }
  else {
    for (let i = 0; i < 5; i++) {
      form.addView({ id: `org:r${i}`, hidden: true }, pos + i)
    }
  }
}


export default class OrgView extends JetView {
  
  config() {
    _ = this.app.getService("locale")._
    webix.protoUI({ name: "dependent", $cssName: "combo" }, LinkedInputs, webix.ui.combo)
    const grid = {
      view: "datatable",
      id: "org:grid",
      select: "row",
      multiselect: false,
      resizeColumn: true,
      pager: "org:pager",
      minWidth: 500,
      columns: [
        { id: "taxc", header: { text: _("taxcode"), css: "header" }, sort: "string", adjust: true },
        { id: "code", header: { text: _("cus_code"), css: "header" }, format: webix.template.escape, sort: "server", adjust: true },
        { id: "name", header: { text: _("customer_name"), css: "header" }, format: webix.template.escape, sort: "server", adjust: true },
        { id: "addr", header: { text: _("address"), css: "header" }, format: webix.template.escape, sort: "string", adjust: true },
        { id: "fadd", header: { text: _("faddress"), css: "header" }, format: webix.template.escape, sort: "string", adjust: true },
        { id: "tel", header: { text: _("tel"), css: "header" }, format: webix.template.escape, sort: "string", adjust: true },
        { id: "mail", header: { text: "Email", css: "header" }, sort: "string", adjust: true },
        { id: "acc", header: { text: _("account"), css: "header" }, format: webix.template.escape, sort: "string", adjust: true },
        { id: "bank", header: { text: _("bank"), css: "header" }, format: webix.template.escape, sort: "string", adjust: true },
      ],
      save: "api/org"
    }
    let maxchar
    if (ENT== "hdb") {
      maxchar = 25
    }
    if (ENT=="bvb") {
      maxchar = 50
    }
    let form1 = {
      view: "form",
      id: "org:form1",
      padding: 3,
      margin: 3,
      minWidth: 500,
      height: 500,
      elementsConfig: { labelWidth: 85 },
      scroll: true,
      elements:
        [
          { name: "name", id: "name", label: _("customer_name"), view: "text", attributes: { maxlength: 255 }, required: true },
          { name: "name_en",id: "name_en", label: _("customer_name_en"), view: "text", attributes: { maxlength: 255 } },
          {
            cols: [
              ["hdb"].includes(ENT) ? {
                name: "taxc", id: "taxc", label: _("taxcode"), view: "text",
                attributes: { maxlength: maxchar, placeholder: _("taxcode") },
                validate: v => { if (!(!v || checkmst(v))) { this.webix.message(_("taxcode_invalid"), "success"); }; return true; }, invalidMessage: _("taxcode_invalid")
              } : {
                  name: "taxc", id: "taxc", label: _("taxcode"), view: "text",
                  attributes: { maxlength: 14, placeholder: _("taxcode") },
                  validate: v => { return (!v || checkmst(v)) }, invalidMessage: _("taxcode_invalid")
                },
              { 
                 
                cols: [
                 { name: "code",id: "code" , label: _("cus_code"), view: "text", attributes: { maxlength: 50 }, required:["bvb"].includes(ENT)? false:true},
                 { id: "org:auto", view: "button", type: "icon", icon: "wxi-sync", tooltip: _("org_auto_code"), width: 33,disabled: ROLE.includes('PERM_CATEGORY_CUSTOMER_GETCODE') ?false:true }
                ]
              },
            ]
          },
          {
            cols: [
              { rows: [{ id: "org:mail", name: "mail", label: "Email", view:"multitext", attributes: { type: "email", maxlength: ENT=="dbs" ? 1000:255 } }] },
              { rows: [{ name: "tel", id: "tel", label: _("tel"), view: "multitext", attributes: { type: "tel", maxlength: 255 } }] }
            ]
          },
          { name: "addr",id: "addr2", label: _("address"), view: "text", attributes: { maxlength: 255 }, required: true },
          {
            cols: [
              { id: "org:prov1", name: "prov", view: "combo", label: _("city"), suggest: { url: "api/cat/kache/prov" } },
              { id: "org:dist1", name: "dist", view: "dependent", label: _("district"), master: "org:prov1", dependentUrl: "api/cat/kache2/local/", options: [] },
            ]
          },
          {
            cols: [
              { rows: [{ id: "org:ward1", name: "ward", view: "dependent", label: _("ward"), master: "org:dist1", dependentUrl: "api/cat/kache2/local/", options: [] }] },
              { rows: [{ name: "acc", label: _("account"),id: "acc", view: "multitext", attributes: { maxlength: 255 } }] },
            ]
          },
          { name: "bank", label: _("bank"),id: "bank", view: "text", attributes: { maxlength: 255 } },
          { name: "type", label: _("type"), id: "type", view: "combo",  options: ISTATUS,disabled:true },
          { id: "org:r0", hidden: true },
          { id: "org:r1", hidden: true },
          { id: "org:r2", hidden: true },
          { id: "org:r3", hidden: true },
          { id: "org:r4", hidden: true },
          {
            cols: [
              //{ fillspace: true },
              { view: "button", id: "org:btnunselect", type: "icon", icon: "mdi mdi-select-off", label: _("unselect"), disabled: true },
              { view: "button", id: "org:btndel", type: "icon", icon: "wxi-trash", label: _("delete"), disabled: true },
              { view: "button", id: "org:btnupd", type: "icon", icon: "mdi mdi-database-edit", label: _("update"), disabled: true },
              
              { view: "button", id: "org:btnadd", type: "icon", icon: "wxi-plus", disabled: false, label: _("create") },
              { view: "button", id: "org:btnexcel", type: "icon", icon: "mdi mdi-file-excel", label: "Excel", disabled: ROLE.includes('PERM_CATEGORY_CUSTOMER_EXCEL') ? false : true }
            ]
          },
          { height: 33 }
        ],
      rules: {
        name: webix.rules.isNotEmpty,
        addr: webix.rules.isNotEmpty,
        $obj: data => {
          if (!(containhtmlchar(data.code)) ) {
            webix.message(_("contain_html_char"))
            $$("code").focus()
            return false
          }
          if (!(containhtmlchar(data.name)) ) {
            webix.message(_("contain_html_char"))
            $$("name").focus()
            return false
          }
          if (!(containhtmlchar(data.addr)) ) {
            webix.message(_("contain_html_char"))
            $$("addr2").focus()
            return false
          }
          if (!(containhtmlchar(data.prov)) ) {
            webix.message(_("contain_html_char"))
            $$("org:prov1").focus()
            return false
          }
          if (!(containhtmlchar(data.tel)) ) {
            webix.message(_("contain_html_char"))
            $$("tel").focus()
            return false
          }
          if (!(containhtmlchar(data.acc)) ) {
            webix.message(_("contain_html_char"))
            $$("org:acc").focus()
            return false
          }
          if (!(containhtmlchar(data.bank)) ) {
            webix.message(_("contain_html_char"))
            $$("org:bank").focus()
            return false
          }
          if (data.mail) {
            const rows = data.mail.split(/[ ,;]+/)
            for (const row of rows) {
              if (!webix.rules.isEmail(row.trim())) {
                webix.message(_("mail_invalid"))
                $$("org:mail").focus()
                return false
              }
            }
          }
          return true
        }
      },
      on: {
        onAfterValidation: (result) => {
          if (!result) webix.message(_("required_msg"), "error")
        }
      }
    }
    let pager = { view: "pager", width: 340, id: "org:pager", size: size, template: `{common.first()}{common.prev()} {common.pages()}{common.next()}{common.last()}` }
    let recpager = { view: "combo", id: "recpager", maxWidth: 70, tooltip: _("row_per_page"), value: 10, options: RPP }
    let title_page = { view: "label", label:`<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("customer")}</span>`}
    return {
      paddingX: 2,
      cols: [
        {
          rows: [
            title_page,
            {
              cols: [
                { id: "scode", name: "code", view: "text", label: _("cus_code"), attributes: { maxlength: 30 } },
                { id: "staxc", name: "taxc", view: "text", label: _("stax") , attributes: { maxlength: 300 } },
                { id: "sname", name: "name", view: "text", label: _("customer_name"), attributes: { maxlength: 255 } },
                // "vib".includes(ENT) ?
                //   {
                //     name: "vrt", label: _("vrt"), view: "combo", inputAlign: "right",
                //     options: VAT(this.app)
                //   } : { hidden: true },
                { view: "button", id: "org:search", type: "icon", icon: "wxi-search", width: 35, tooltip: _("search"),/*hidden: ROLE.includes('PERM_CATEGORY_ITEMS') || ROLE.includes('PERM_CATEGORY_ITEMS_CREATE') || ROLE.includes('PERM_CATEGORY_ITEMS_UPDATE') || ROLE.includes('PERM_CATEGORY_ITEMS_DELETE') || ROLE.includes('PERM_CATEGORY_ITEMS_EXCEL') || ROLE.includes('PERM_CATEGORY_ITEMS_UNCHECK') ?false:true*/ }
              ]
            },
            grid,
            {cols: [recpager, pager, { id: "org:countinv", view: "label" },{}]}
          ],
          gravity: 1.5 },
          { view: "resizer" },
          { rows: [form1, {}]
        }
      ]
    }
  }
  ready(v, urls) {
    let lang = webix.storage.local.get("lang")
    webix.ajax().post(`api/cus`,{itype:'00ORGS',lang:lang}).then(result => {
      const rows = result.json()
      init(rows)
    })
    //Cau hinh rieng cac cot tĩnh
    webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
        let json = result.json()
        setConfStaFld(json)
    })
    //
  }
  init() {
    if (!ROLE.includes('PERM_CATEGORY_CUSTOMER')) {
      this.show("/top/home")
      window.location.replace(`/${homepage}`)
      return
    }
    this.XlsWin = this.ui(XlsWin)
    let grid = $$("org:grid"), form1 = $$("org:form1"), grid1 = $$("org:xlsgrid"), recpager = $$("recpager")
    form1.bind(grid, null, function (data) {

      return decodehtml(data)
    });
    const search = (val) => {
      // if (!val) {
      //   webix.message(_("must_enter_search_conds"))
      //   return false
      // }
      // webix.ajax("api/org", { name: val, limit: recpager.getValue() }).then(result => {
      //   grid.clearAll()
      //   grid.parse(result)
      // })
      let a = val
      grid.clearAll()
      grid.loadNext(recpager.getValue(), 0, null, `api/org?code=${val.code}&&name=${val.name}&&taxc=${val.taxc}`, true)
      
    }

    // let search2 = () => {
    //   grid.loadNext(size, 0, null, "org->api/org", true)
    // }
    
    // recpager.attachEvent("onChange", () => {
    //     $$("org:pager").config.size = recpager.getValue()
    //     let val = $$("org:search").getValue().replace(/~|\$|!|;|\*/g, "")
    //     search(val)
    // })
    $$("type").setValue("1")
    
    
    // $$("org:search").attachEvent("onEnter", function () {
    //   const val = this.getValue().replace(/~|\$|!|;|\*/g, "")
    //   this.setValue(val)
    //   search(val)
    // })
    // $$("org:search").attachEvent("onSearchIconClick", function () {
    //   const val = this.getValue().replace(/~|\$|!|;|\*/g, "")
    //   this.setValue(val)
    //   search(val)
    // })
    $$("org:search").attachEvent("onItemClick", () => {
      let code = $$("scode").getValue()
      let name = $$("sname").getValue()
      let taxc = $$("staxc").getValue()
      let val = {
        code,
        name,
        taxc
      }
      if (!webix.rules.isNotEmpty(code) && !webix.rules.isNotEmpty(taxc)) {
        webix.message(_("must_enter_customer_code_or_taxcode"), "error")
        return false
      }
      search(val)
  })
    $$("org:btnread").attachEvent("onItemClick", () => {
      const uploader = $$("org:file"), files = uploader.files, file_id = files.getFirstId()
      if (file_id) {
        uploader.send(result => {
          if(result.data[0].mail) {
            const rows =result.data[0].mail.split(/[ ,;]+/)
            if(ENT!='ctbc') result.data[0].mail = rows.join(', ')
            else result.data[0].mail = rows.join('; ')
          }
          if(result.data[0].tel) {
            const rows1 =result.data[0].tel.split(/[ ,;]+/)
            if(ENT!='ctbc') result.data[0].tel = rows1.join(', ')
            else result.data[0].tel = rows1.join('; ')
          }
          if(result.data[0].acc) {
            const rows2 =result.data[0].acc.toString().split(/[ ,;]+/)
            if(ENT!='ctbc') result.data[0].acc = rows2.join(', ')
            else result.data[0].acc = rows2.join('; ')
          }
          grid1.clearAll()
          if (result) grid1.parse(result)
        })
      }
      else webix.message(_("file_required"))
    })

    $$("org:btnsave").attachEvent("onItemClick", () => {
      let rows = grid1.serialize(), arr = []
      for (let row of rows) {
        if (row && row.chk && !row.error) arr.push(row)
      }
      if (arr.length < 1) {
        webix.message(_("data_required"))
        return false
      }
      webix.ajax().post("api/org/ins", JSON.stringify(arr)).then(result => {
        const obj = result.json()
        for (const kq of obj) {
          let row = arr.find(x => x.id == kq.id)
          let record = grid1.getItem(row.id)
          record["error"] = _(kq.error)
        }
        grid1.refresh()
        webix.message(_("send_data_ok_pls_check_err_msg"))
      })
    })

    $$("org:btnunselect").attachEvent("onItemClick", () => {
      grid.unselectAll()
    })
    $$("org:auto").attachEvent("onItemClick", () => {
      var nowMilliseconds = moment(new Date()).format("YYYYMMDDHHMMssSSS");
      $$("code").setValue('A'+nowMilliseconds)
    })
    $$("org:btnupd").attachEvent("onItemClick", () => {
      if (!form1.isDirty() || (ENT!="hdb" && !form1.validate())) return false
      grid.waitSave(() => form1.save()).then(result => {
        if (result==1) webix.message(_("customers_update_ok"), "success")
      })
    })

    $$("org:btnadd").attachEvent("onItemClick", () => {
      const row = form1.getValues()
      if (!form1.validate()) return false
      grid.waitSave(() => {
        grid.add(form1.getValues(), 0)
      }).then(result => {
        const obj = result
        if (obj.id) webix.message(_("customers_insert_ok"), "success")
        search(`${row.name}`)
      }).catch(err => { grid.clearAll() })
    })

    $$("org:btndel").attachEvent("onItemClick", () => {
      const row = form1.getValues()
      webix.confirm(`${_("org_del_msg")} ${row.name} ?`, "confirm-warning").then(() => {
        grid.waitSave(() => { grid.remove(row.id) }).then(result => { 
          if (result == 1) webix.message(_("customers_delete_ok"), "success") 
        })
      })
    })
    $$("org:btnexcel").attachEvent("onItemClick", () => {
      this.XlsWin.show()
      grid1.clearAll()
    })
  
    grid.attachEvent("onBeforeLoad", function () {
      this.showOverlay(_("loading"))
    })
    grid.attachEvent("onAfterLoad", function () {
      let self = this, count = self.count()
      $$("org:countinv").setValue(`${_("found")} ${count}`)
      if (!this.count()) this.showOverlay(_("notfound"))
      else this.hideOverlay()
    })
   
    grid.attachEvent("onAfterSelect", selection => {
      if (ROLE.includes('PERM_CATEGORY_CUSTOMER_UNCHECK')) $$("org:btnunselect").enable()
      if (ROLE.includes('PERM_CATEGORY_CUSTOMER_UPDATE')) $$("org:btnupd").enable()
      if (ROLE.includes('PERM_CATEGORY_CUSTOMER_DELETE')) $$("org:btndel").enable()
      $$("org:btnadd").disable()
   
      let data= grid.getSelectedItem()
      if( ENT == 'ctbc'){
        data.mail = data.mail.replace(/;/g, ',')
      }
      // config dc phep sưa du lieu tu api hay ko
      if(ORG_EDIT==0 &&  data.type==2){
        $$("bank").disable();
        $$("org:ward1").disable();
        $$("name").disable();
        $$("name_en").disable();
        $$("taxc").disable();
        $$("code").disable();
        $$("org:mail").disable();
        $$("tel").disable();
        $$("org:mail").disable();
        $$("addr2").disable();
        $$("org:prov1").disable();
        $$("org:dist1").disable();
        $$("acc").disable();
        $$("org:btnupd").disable()
        $$("org:btndel").disable()
    }
    })
    grid.attachEvent("onAfterUnSelect", () => {
      $$("org:btnunselect").disable()
      $$("org:btnupd").disable()
      $$("org:btndel").disable()
      if ( ROLE.includes('PERM_CATEGORY_CUSTOMER_CREATE')) $$("org:btnadd").enable()
      if(ORG_EDIT==0){
      $$("bank").enable();
      $$("org:ward1").enable();
      $$("name").enable();
      $$("name_en").enable();
      $$("taxc").enable();
      $$("code").enable();
      $$("org:mail").enable();
      $$("tel").enable();
      $$("org:mail").enable();
      $$("addr2").enable();
      $$("org:prov1").enable();
      $$("org:dist1").enable();
      $$("acc").enable();
      $$("org:btnupd").disable()
      $$("org:btndel").disable()
      }
    })
  }
}   