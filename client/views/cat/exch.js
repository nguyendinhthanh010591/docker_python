import { JetView } from "webix-jet"
import { ROLE,t2s, size, gridnf, textnf,setConfStaFld } from "models/util"
export default class ExchView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        webix.proxy.exch = {
            $proxy: true,
            load: function (view, params) {
                let obj = $$("exch:form").getValues()
                Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                if (!params) params = {}
                params.filter = obj
                return webix.ajax(this.source, params)
            }
        }

        let pager = { view: "pager", id: "exch:pager", size: size, template: "{common.first()}{common.prev()}{common.pages()}{common.next()}{common.last()}" }
        let form1 = {
            view: "form",
            minWidth: 300,
            id: "exch:form1",
            elements:
                [
                    { id: "dt", name: "dt", label: _("time"), view: "datepicker", stringResult: true, required: true, editable: true, timepicker: true, format: webix.i18n.fullDateFormatStr },
                    { id: "cur", name: "cur", label: _("currency"), required: true, value: "USD", view: "combo", suggest: { url: "api/cat/kache/currency" } },
                    { id: "val", name: "val", label: "VND", required: true, view: "text", format: textnf, inputAlign: "right", attributes:{maxlength:9}, invalidMessage: _("exch_invalid_large") },
                    { cols: [{}, { view: "button", id: "exch:btnadd", type: "icon", icon: "wxi-plus", label: _("create"), width: 100, disabled: ROLE.includes('PERM_CATEGORY_RATE_INSERT') ? false:true }], }
                ],
            rules: {
                $all: webix.rules.isNotEmpty,
                val:function(value){ 
                    return webix.rules.isNumber(value) && value <= 999999.99
                },
            },
            on: {
                onAfterValidation: (result)=> {
                    if (!result) webix.message(_("required_msg"))
                }
            }
        }

        let form = {
            view: "form",
            id: "exch:form",
            minWidth: 500,
            padding: 3, 
            margin: 3,
            elementsConfig: { labelWidth: 60 },
            elements:
                [
                    {
                        cols: [
                            { id: "fd", name: "fd", label: _("from"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr },
                            { id: "td", name: "td", label: _("to"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr },
                            { id: "curr", name: "curr", label: _("currency"), view: "combo", suggest: { url: "api/cat/kache/currency" } },
                            { view: "button", id: "exch:btnsearch", type: "icon", icon: "wxi-search", width: 33, tooltip: _("search"), /*hidden: ROLE.includes('PERM_CATEGORY_RATE_INSERT') || ROLE.includes('PERM_CATEGORY_RATE') || ROLE.includes('PERM_CATEGORY_RATE_DELETE')||ROLE.includes('PERM_CATEGORY_RATE_UPDATE')|| ROLE.includes('PERM_CATEGORY_RATE_EXCEL')  ? false : true */ }
                        ]
                    }
                ],
            rules: {
                fd: webix.rules.isNotEmpty,
                td: webix.rules.isNotEmpty,
                $obj: function (data) {
                    if (data.fd > data.td) {
                        webix.message(_("date_invalid_msg"))
                        return false
                    }
                    return true
                }
            },
            
        }
        let grid = {
            view: "datatable",
            id: "exch:grid",
            editable: true,
            editaction: "dblclick",
            datafetch: size,
            pager: "exch:pager",
            select: "row",
            columns: [
                { id: "cur", header: { text: _("currency"), css: "header" }, sort: "server", fillspace: 1 },
                { id: "dt", header: { text: _("time"), css: "header" }, sort: "server", format: t2s, fillspace: 2 },
                { id: "val", header: { text: "VND", css: "header" }, css: "right", inputAlign: "right", fillspace: 2, editor:ROLE.includes('PERM_CATEGORY_RATE_UPDATE') ? "text":" ", format: gridnf.format, editParse: gridnf.editParse, editFormat: gridnf.editFormat },
                { id: "btn", header: "<span class='webix_icon wxi-download' title='Excel'></span>", template:`<span class='webix_icon wxi-trash'  title='${_("delete")}'></span>`, width: 35 }
            ],
            onClick:
            {
                "wxi-trash": function (e, id) {
                    if (!ROLE.includes('PERM_CATEGORY_RATE_DELETE')) {
                        webix.message(_("no_authorization"), "error")
                        return false
                    }
                    webix.confirm(_("del_confirm_msg"), "confirm-warning").then(() => {
                        this.remove(id)
                        return false
                    })
                },
                "wxi-download": function (e, id) {
                    if (!ROLE.includes('PERM_CATEGORY_RATE_EXCEL')) {
                        webix.message(_("no_authorization"), "error")
                        return false
                    }
                    if (this.count()) webix.toExcel(this, { ignore: { "btn": true } })
                }
            },
            rules: {
                val:function(value){ 
                    return webix.rules.isNumber(value) && value <= 999999.99 && value >= 0
                },
            },
            on: {
                onAfterValidation: (result)=> {
                    if (!result) webix.message(_("required_msg"),"error")
                }
            },
            save: "api/exch"
        }
        let title_page = { view: "label", label:`<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("exchange_rate")}</span>`}
        return { paddingX: 2, cols: [{ rows: [title_page, form, grid, pager], gravity: 2 }, { view: "resizer" }, { rows: [form1, {}] }] }
    }

    ready(v, urls) {
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
    
    init() {
        if (!ROLE.includes('PERM_CATEGORY_RATE')) {
            this.show("/top/home")
            window.location.replace(`/${homepage}`)
            return
        }
        const all = { id: "*", value: _("all") }
        let date = new Date(), firstDay = new Date(date.getFullYear(), date.getMonth(), 1)
        let grid = $$("exch:grid"), form = $$("exch:form"), form1 = $$("exch:form1")
        $$("fd").setValue(firstDay)
        $$("td").setValue(date)
        $$("dt").setValue(date)
        $$("curr").getList().add(all, 0)
        $$("curr").setValue("*")

        const search = () => {
            if (form.validate()) {
                grid.clearAll()
                grid.loadNext(size, 0, null, "exch->api/exch", true)
            }

        }

        const add = () => {
            return new Promise((resolve, reject) => {
                grid.add(form1.getValues())
                form1.clear()
                resolve(1)
            })
        }

        $$("exch:btnadd").attachEvent("onItemClick", () => {
            if (!form1.validate()) return
            add().then((ok) => {
                search()
            })
        })

        $$("exch:btnsearch").attachEvent("onItemClick", () => {
            search()
        })

        grid.attachEvent("onBeforeLoad", function () {
            this.showOverlay(_("loading"))
        })

        grid.attachEvent("onAfterLoad", function () {
            if (!this.count()) this.showOverlay(_("notfound"))
            else this.hideOverlay()
        })

    }

}   