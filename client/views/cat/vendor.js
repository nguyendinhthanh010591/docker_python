import { JetView } from "webix-jet"
import { size, gridnf, h, w,setConfStaFld, ENT } from "models/util"

export default class VndView extends JetView {
  config() {
    _ = this.app.getService("locale")._
    webix.proxy.ven = {
      $proxy: true,
      load: function (view, params) {
        let obj = $$("ven:form").getValues()
        Object.keys(obj).forEach(
          k => (!obj[k] || obj[k] == "*") && delete obj[k]
        )
        if (!params) params = {}
        params.filter = obj
        return webix.ajax(this.source, params)
      }
    }

    let form = {
      view: "form",
      id: "ven:form",
      minWidth: 500,
      padding: 5,
      margin: 5,
      elementsConfig: { labelWidth: 130 },
      elements: [
        {
          cols: [
            { name: "taxno", view: "text", label: _("taxcode"), attributes: { maxlength: 14 },width:350 },
            { name: "vendor_name", view: "text", label: _("vendor_name"), attributes: { maxlength: 255 }},
            { view: "button", id: "ven:btnsearch",label:_("search"), type: "icon", icon: "wxi-search",width:90, tooltip: _("search") }
          ]
        }
      ]
    }
    let pager = { view: "pager", id: "ven:pager", size: size, template: "{common.first()}{common.prev()}{common.pages()}{common.next()}{common.last()}" }

    let grid1 = {
      view: "datatable",
      id: "ven:grid1",
      select: "row",
      resizeColumn: true,
      datafetch: size,
      pager: "ven:pager",
      columns: [
        { id: "del", header: "", template: `<span class='webix_icon wxi-trash'  title='${_("delete")}'></span>`,css:"header", width: 50 },
        { id: "taxno", header: { text: _("taxcode"), css: "header" }, sort: "server", width:250 },
        { id: "vendor_name", header: { text: _("vendor_name"), css: "header" }, sort: "server", fillspace: true,adjust:true},
      ],
      onClick: {
        "wxi-trash": function (e, id) {
          webix.confirm(_("del_confirm_msg"), "confirm-warning").then(() => {
            this.remove(id)
            return false
          })
        }
      },
      url: "api/ven",
      save: { url: "api/ven", updateFromResponse: true }
    }

    let form1 = {
      view: "form",
      id: "ven:form1",
      minWidth: 300,
      elementsConfig: { labelWidth: 130 },
      elements: [
        { id: 'taxno', name: "taxno", label: _("taxcode"), view: "text", required: true, attributes: { maxlength: 14 },placeholder:_("taxcode") },
        { name: "vendor_name", label: _("vendor_name"), view: "text",required: true, attributes: { maxlength: 200 },placeholder:_("vendor_name") },
        {
          cols: [
            { view: "button", id: "ven:btnunselect", type: "icon", icon: "mdi mdi-select-off", label: _("unselect"), disabled: true },
            { view: "button", id: "ven:btnupd", type: "icon", icon: "mdi mdi-database-edit", label: _("update"), disabled: true },
            { view: "button", id: "ven:btnadd", type: "icon", icon: "wxi-plus", label: _("create"), disabled: false}
          ]
        }
      ],
      rules:{
        taxno:webix.rules.isNotEmpty,
        vendor_name:webix.rules.isNotEmpty
      },
      on: {
        onAfterValidation: (result) => {
          if (!result) webix.message(_("required_msg"))
        }
      }
    }
    return { paddingX: 2, cols: [{ rows: [form, grid1, pager], gravity: 3 }, { view: "resizer" }, { rows: [form1, {}] }] }
  }

  ready(v, urls) {
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }

  init() {
    let grid1 = $$("ven:grid1"), form1 = $$("ven:form1")
    form1.bind(grid1)

    const search = () => {
      grid1.clearAll()
      grid1.loadNext(size, 0, null, "ven->api/ven", true)
    }

    $$("ven:btnsearch").attachEvent("onItemClick", () => {
        search()
    })

    $$("ven:btnupd").attachEvent("onItemClick", () => {
      if (form1.isDirty()) {
        if (form1.validate()) {
        grid1.waitSave(()=>{
            grid1.updateItem($$("ven:form1").getValues().id,$$("ven:form1").getValues())
        }).then(r => {
                webix.message(_("upd_ok_msg"), "success")
                form1.clear()
                grid1.unselectAll()
        })
        }
      }
    })

    $$("ven:btnadd").attachEvent("onItemClick", () => {
      if (form1.validate()) {
        grid1.waitSave(()=>{
            grid1.add($$("ven:form1").getValues())
        }).then(r => {
                webix.message(_("save_ok_msg"), "success")
                form1.clear()
        })
      }
    })

    $$("ven:btnunselect").attachEvent("onItemClick", () => {
      grid1.unselectAll()
    })

    grid1.attachEvent("onBeforeLoad", function () {
      this.showOverlay(_("loading"))
    })
    grid1.attachEvent("onAfterLoad", function () {
      if (!this.count()) this.showOverlay(_("notfound"))
      else this.hideOverlay()
    })

    grid1.attachEvent("onAfterSelect", selection => {
      $$("ven:btnupd").enable()
      $$("ven:btnadd").disable()
      $$("ven:btnunselect").enable()
    })

    grid1.attachEvent("onAfterUnSelect", selection => {
      $$("ven:btnupd").disable()
      $$("ven:btnadd").enable()
      $$("ven:btnunselect").disable()
    })
  }
}

