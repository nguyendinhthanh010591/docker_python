import { JetView } from "webix-jet"
import {ROLE, filter,setConfStaFld } from "models/util"
export default class LocView extends JetView {
    config() {
        let prov = {
            view: "combo",
            id: "loc:prov",
            label: _("city"),
            //hidden:ROLE.includes('PERM_CATEGORY_LOCATION') || ROLE.includes('PERM_CATEGORY_LOCATION_CREATE') || ROLE.includes('PERM_CATEGORY_LOCATION_UPDATE') || ROLE.includes('PERM_CATEGORY_LOCATION_DELETE') ? false: true,
            placeholder: `${_("choose_city")}...`,
            suggest: { url: "api/cat/kache/prov", filter: filter },
            on:
            {
                onChange: function (newv) {
					if (newv) {
						$$("loc:treetable").clearAll()
						$$("loc:treetable").load("api/loc/" + newv)
					}
                }
            }
        }

        let treetable = {
            view: "treetable",
            id: "loc:treetable",
            editable: true,
            select: "row",
            width: 400,
            editaction: "dblclick",
            drag: true,
            columns: [
                { id: "name", header: [{ text: _("location_name"), css: "header" }, { content: "textFilter" }], template: "{common.treetable()} #name#", editor: ROLE.includes('PERM_CATEGORY_LOCATION_UPDATE')? "text":"", fillspace: 4, format: webix.template.escape },
                { id: "id", header: [{ text: _("location_code"), css: "header" }], fillspace: 1 },
                {
                    id: "btn", width: 35, header: "", template: obj => {
                        if (obj.$count > 0) return ""
                        else return `<span class='webix_icon wxi-trash' title='`+_("delete")+`'></span>`
                    }
                },
                {
                    width: 35, template: obj => {
                        if (obj.$level >= 3) return ""
                        else return `<span class='webix_icon wxi-plus' title='`+_("create")+`'></span>`
                    }
                },
                /*
                {
                    id: "btn1", width: 35, header: "", template: obj => {
                        if (obj.level == 3) return ""
                        else return "<span class='mdi mdi-map-marker-plus' title='Thêm địa bàn'></span>"
                    }
                },
                {
                    id: "btn2", width: 35, header: ["<span class='webix_icon wxi-download'  title='Excel'></span>"], template: obj => {
                        if (obj.leaf) return "<span class='webix_icon wxi-trash'  title='Xóa địa bàn'></span>"
                        else return ""
                    }
                }
                */
            ],
            filterMode: { level: false, showSubItems: false },
            rules: {
                $obj: data => {
                    if (data.name && String(data.name).length > 100) {
                        webix.message(_("invalid_data"))
                        return false
                    }
                    if (!(containhtmlchar(data.name))) {
                        webix.message(_("contain_html_char"))
                        return false
                    }
                    return true
                }
            },
            on:
            {
                onBeforeDrag: function (ctx) {
                    let rec = this.getItem(ctx.start)
                    if (rec.level != 3) return false
                },
                onBeforeDrop: function (ctx) {
                    let parent = ctx.parent
                    if (!parent || parent == $$("loc:prov").getValue()) return false
                    else {
                        let rec = this.getItem(ctx.start)
                        if (rec.pid == parent) return false
                    }
                },
                onAfterDrop: function (ctx) {
                    let rec = this.getItem(ctx.start)
                    this.updateItem(ctx.start, rec)
                }
            },
            onClick:
            {
                "wxi-trash": function (e, id) {
                    if (!ROLE.includes('PERM_CATEGORY_LOCATION_DELETE')) {
                        webix.message(_("no_authorization"), "error")
                        return false
                    }
                    
                    webix.confirm(_("del_confirm_msg"), "confirm-warning").then(ok => {
                        this.remove(id)
                        return false
                    })
                },
                "wxi-plus": function (e, id) {
                    if (!ROLE.includes('PERM_CATEGORY_LOCATION_CREATE')) {
                        webix.message(_("no_authorization"), "error")
                        return false
                    }
                    // let p = $$("loc:treetable").getSelectedItem()
                    // if(p) this.add({ name: webix.uid(), p:p}, 0)

                    const me = this, row = me.getItem(id), level = row.level
                    if (level >= 3) {
                        webix.message(_("ous_level3_msg"), "error")
                        return false
                    }
                    me.waitSave(() => {
                        const obj = { name: webix.uid(), level: level + 1, p: row }
                        me.add(obj, 0, id)
                    }).then(result => {
                        $$("loc:treetable").open(id)
                        me.select(result.id)
                    })
                    return false
                },
                // "mdi-map-marker-plus": function (e, id) {
                //     let rec = this.getItem(id)
                //     if (rec.level == 2) this.add({ name: "Xã/Phường mới thêm", level: 3 }, 0, id)
                //     else this.add({ name: "Quận/Huyện mới", level: 2 }, 0, id)
                // },
                // "wxi-download": function (e, id) {
                //     if (this.count()) webix.toExcel(this, { ignore: { "btn1": true, "btn2": true } })
                // }
            },
            save: { url: "api/loc", updateFromResponse: true }
        }
        let title_page = { view: "label", label:`<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("catalog_location")}</span>`}
        return { paddingX: 5, rows: [title_page, prov, treetable] }
    }

    ready(v, urls) {
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }

    init() {
        if (!ROLE.includes('PERM_CATEGORY_LOCATION')) {
            this.show("/top/home")
            window.location.replace(`/${homepage}`)
            return
        }
    }
}   