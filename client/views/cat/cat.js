import { JetView } from "webix-jet"
import {ROLE, setConfStaFld, ENT } from "models/util"
let cols
const cattypechange = (newv) => {
    $$("cat:datatable").clearAll()
    console.log(newv)
    if (newv) {
        if (newv == "segment") {
            let colsSCB = [
                { id: "name", width: 300, header: { content: "textFilter" }, editor: "text", adjust: true },
                { id: "des", width: 135, header:{ content: "textFilter" }, editor: "richselect", options: "api/cat/kacheSegment/seg", fillspace: 1, adjust: true },
                { id: "btn", width: 35, header: "<span class='webix_icon wxi-plus' title='" + _("create") + "'></span>", template: "<span class='webix_icon wxi-trash' title='" + _("delete") + "'></span>" }
            ]
            $$("cat:datatable").config.columns = colsSCB
            $$("cat:datatable").refreshColumns()
        } else {
            $$("cat:datatable").config.columns = cols
            $$("cat:datatable").refreshColumns()
        }
        $$("cat:datatable").load("api/kat/" + newv)
        let edit = true
        if(["itype","istatus"].includes(newv)) edit = false
        $$("cat:datatable").config.editable = edit
        if(edit) $$("cat:datatable").showColumn("btn") 
        else $$("cat:datatable").hideColumn("btn")
        $$("cat:datatable").refresh()
    }
}
export default class CatView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        let opt = [
            { id: "currency", value: _("currency") },
            { id: "unit", value: _("unit") },
            // { id: "itype", value: _("invoice_type") },
            // { id: "istatus", value: _("istatus_des") }
        ]
        if (ENT=="hlv") opt.push({ id: "payments", value: _("catalog_paycontent") })
        if (ENT == "scb") opt.push({ id: "segment", value: _("segmentscb") },
            { id: "codemail", value: _("send_code_mail") })
        //Thêm mục đích sử dụng TBPH cho VCM
        if (ENT=="vcm") opt.push({ id: "serialdes", value: _("serial_desc") })
        let type = {
            view: "combo",
            id: "cat:type",
            label: _("type"),
            //hidden: ROLE.includes('PERM_CATEGORY_ANOTHER') || ROLE.includes('PERM_CATEGORY_ANOTHER_CREATE')|| ROLE.includes('PERM_CATEGORY_ANOTHER_UPDATE')|| ROLE.includes('PERM_CATEGORY_ANOTHER_DELETE')  ? false : true,
            placeholder: `${_("choose_cat_type")}...`,
            options: opt,
            on:
            {
                onChange: function (newv) {
                    /*
                    $$("cat:datatable").clearAll()
					if (newv) {
						$$("cat:datatable").load("api/kat/" + newv)
						let edit = true
						if(["itype","istatus"].includes(newv)) edit = false
						$$("cat:datatable").config.editable = edit
						if(edit) $$("cat:datatable").showColumn("btn") 
						else $$("cat:datatable").hideColumn("btn")
						$$("cat:datatable").refresh()
                    }
                    */
                   cattypechange(newv)
                }
            }
        }

        cols = [
            /*
              { id: "name", header: [{ text: "Tên", css: "header" }, { content: "textFilter" }], editor: "text", fillspace: 1 },
              {
                id: "btn", width: 35, header: ["<span class='webix_icon wxi-download'  title='Excel'></span>", "<span class='webix_icon wxi-plus' title='Thêm mới'></span>"]
              //, template: "<span class='webix_icon wxi-trash' title='Xóa'></span>" 
              }
              */
            { id: "name", width: 300, header: { content: "textFilter" }, editor: ROLE.includes('PERM_CATEGORY_ANOTHER_UPDATE')? "text":"", adjust: true,format: webix.template.escape },
            { id: "des",name:"des", width: 135, header: { content: "textFilter" }, editor: ROLE.includes('PERM_CATEGORY_ANOTHER_UPDATE')? "text":"",format: webix.template.escape, fillspace: 1, adjust: true },
            { id: "btn", width: 35, header: "<span class='webix_icon wxi-plus' title='" + _("create") + "'></span>", template: "<span class='webix_icon wxi-trash' title='" + _("delete") + "'></span>" }
        ]

        let grid = {
            view: "datatable",
            id: "cat:datatable",
            select: "row",
            multiselect: false,
            editable: true,
            editaction: "dblclick",
            columns: cols,
            rules: {
                $obj: data => {
                    let type = $$("cat:type").getValue()
                    if (type && String(type).toUpperCase() == 'CURRENCY') {
                        if (String(data.name).length > 3) {
                            webix.message(_("invalid_data"))
                            return false
                        }
                    }
                    if (!(containhtmlchar(data.des))) {
                        webix.message(_("contain_html_char"))
                        return false
                    }
                    if (!(containhtmlchar(data.name))) {
                        webix.message(_("contain_html_char"))
                        return false
                    }
                    return true
                }
            },
            on: {
                "onAfterAdd": function () {
                    this.select(this.getFirstId())
                }
            },
            onClick:
            {
                "wxi-trash": function (e, id) {
                    if (!ROLE.includes('PERM_CATEGORY_ANOTHER_DELETE')) {
                        webix.message(_("no_authorization"), "error")
                        return false
                    }
                    webix.confirm(_("del_confirm_msg"), "confirm-warning").then(ok => {
                        this.remove(id)
                        let type = $$("cat:type").getValue()
                        cattypechange(type)
                        //return false
                    })
                },
                "wxi-plus": function (e, id) {
                    if (!ROLE.includes('PERM_CATEGORY_ANOTHER_CREATE')) {
                        webix.message(_("no_authorization"), "error")
                        return false
                    }
                    let type = $$("cat:type").getValue(),name = this.getFilter("name").value,obj
                    if(ENT != "scb"){
                        obj = { name: name, type: type }
                    }else{
                        obj = { name: "", type: type }
                    }
                    if (ENT=="hlv" && type=="payments") obj.des = `{"servicename":"name1", "servicecontent":"content1"}`
                    
                    if (type) this.add(obj, 0)
                },
                "wxi-download": function (e, id) {
                    if (this.count()) webix.toExcel(this, { ignore: { "btn": true } })
                }
            },
            save: { url: "api/pkat", updateFromResponse: true }
        }
        let title_page = {
            view: "label", label: `<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("catalog_catalog")}</span>`
        }
        return { paddingX: 3, rows: [title_page, type, grid], width: 800 }
    }

    ready(v, urls) {
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }

    init() {
        if (!ROLE.includes('PERM_CATEGORY_ANOTHER')) {
            this.show("/top/home")
            window.location.replace(`/${homepage}`)
            return
        }
    }
}   