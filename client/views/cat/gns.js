import { JetView } from "webix-jet"
import {ROLE, size, gridnf, h, w, setConfStaFld, ENT, VAT } from "models/util"
class GnsWin extends JetView {
  config() {
    let form = {
      view: "form",
      //id: "gns:form2",
      elements: [
        {
          view: "datatable",
          id: "gns:grid2",
          columns: [
            {
              id: "chk", header: { content: "masterCheckbox", css: "center" }, width: 35, template: (obj, common, value, config) => {
                if (obj.error) return ''
                else return common.checkbox(obj, common, value, config)
              },
            },
            { id: "error", header: { text: _("status"), css: "header" }, css: { color: "red" }, adjust: true },
            { id: "code", header: { text: _("gns_code"), css: "header" }, adjust: true },
            { id: "name", header: { text: _("gns_fullname"), css: "header" }, attributes: { maxlength: 500 }, fillspace: true },
            { id: "price", header: { text: _("price"), css: "header" }, adjust: true, format: gridnf.format, css: "right" },
            { id: "unit", header: { text: _("unit"), css: "header" }, attributes: { maxlength: 50 }, adjust: true },
            { id: "vrt", header: { text: _("vrt"), css: "header" }, adjust: true, collection: VAT(this.app), editor: "combo", hidden: "vib".includes(ENT) ? false : true },
            { id: "del", header: { text: "", css: "header" }, template: `<span class='webix_icon wxi-trash'  title='${_("delete")}'></span>`, width: 35, hidden: true }
          ],
          onClick: {
            "wxi-trash": function (e, id) {
              webix.confirm(_("del_confirm_msg"), "confirm-warning").then(() => {
                this.remove(id)
                return false
              })
            }
          }
        },
        {
          cols: [
            { view: "button", type: "icon", icon: "mdi mdi-download", label: _("file_sample"), click: () => { window.location.href = "temp/gns.xlsx" } },
            { id: "gns:btnsave", view: "button", type: "icon", icon: "mdi mdi-content-save", label: _("file_save") },
            { id: "gns:btnread", view: "button", type: "icon", icon: "mdi mdi-file-upload-outline", label: _("file_read") },
            { id: "gns:file", view: "uploader", multiple: false, autosend: false, link: "gns:filelist", accept: "application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", upload: "api/gns/upload", type: "icon", icon: "mdi mdi-file-find-outline", label: _("file_select"), formData: () => { return { type: "GNS" } } },
            { id: "gns:filelist", view: "list", type: "uploader", autoheight: true, borderless: true, gravity: 3 }
          ]
        }
      ]
    }

    return {
      view: "window",
      id: "gns:xlswin",
      height: h,
      width: w,
      position: "center",
      resize: true,
      modal: true,
      head: {
        view: "toolbar", height: 35, css: 'toolbar_window',
        cols: [
          { view: "label", label: _("import_excel") },
          { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => { this.getRoot().hide() } }
        ]
      },
      body: form
    }
  }
  show() {
    this.getRoot().show()
    $$("gns:file").setValue(null)
  }
}
export default class GnsView extends JetView {
  config() {
    _ = this.app.getService("locale")._
    webix.proxy.gns = {
      $proxy: true,
      load: function (view, params) {
        let obj = $$("gns:form").getValues()
        Object.keys(obj).forEach(
          k => (!obj[k] || obj[k] == "*") && delete obj[k]
        )
        if (!params) params = {}
        params.filter = obj
        return webix.ajax(this.source, params)
      }
    }

    let form = {
      view: "form",
      id: "gns:form",
      minWidth: 500,
      padding: 5,
      margin: 5,
      elementsConfig: { labelWidth: 85 },
      elements: [
        {
          cols: [
            { name: "code", view: "text", label: _("gns_code"), attributes: { maxlength: 30 }, placeholder: _("gns_code") },
            { name: "name", view: "text", label: _("gns_name"), attributes: { maxlength: 255 }, placeholder: _("gns_name")},
            { name: "unit", view: "text", label: _("unit"), suggest: { url: "api/cat/kache/unit" }, attributes: { maxlength: 30 }, placeholder: _("unit") },
            "vib".includes(ENT) ?
              {
                name: "vrt", label: _("vrt"), view: "combo", inputAlign: "right",
                options: VAT(this.app)
              } : { hidden: true },
            { view: "button", id: "gns:btnsearch", type: "icon", icon: "wxi-search", width: 35, tooltip: _("search"),/*hidden: ROLE.includes('PERM_CATEGORY_ITEMS') || ROLE.includes('PERM_CATEGORY_ITEMS_CREATE') || ROLE.includes('PERM_CATEGORY_ITEMS_UPDATE') || ROLE.includes('PERM_CATEGORY_ITEMS_DELETE') || ROLE.includes('PERM_CATEGORY_ITEMS_EXCEL') || ROLE.includes('PERM_CATEGORY_ITEMS_UNCHECK') ?false:true*/ }
          ]
        }
      ]
    }
    let pager = { view: "pager", id: "gns:pager", size: size, template: "{common.first()}{common.prev()}{common.pages()}{common.next()}{common.last()}" }

    let grid1 = {
      view: "datatable",
      id: "gns:grid1",
      select: "row",
      resizeColumn: true,
      datafetch: size,
      pager: "gns:pager",
      columns: [
        { id: "del", header: ["vib"].includes(ENT) ? "<span class='webix_icon wxi-download' title='Excel'></span>" : "", template: `<span class='webix_icon wxi-trash'  title='${_("delete")}'></span>`, width: 35 },
        { id: "code", header: { text: _("gns_code"), css: "header" }, sort: "server", format: webix.template.escape, adjust: true },
        { id: "name", header: { text: _("gns_fullname"), css: "header" }, format: webix.template.escape, sort: "server", fillspace: true, adjust: true },
        { id: "unit", header: { text: _("unit"), css: "header" }, sort: "server", format: webix.template.escape, adjust: true },
        { id: "price", header: { text: _("price"), css: "header" }, sort: "server", adjust: true, format: gridnf.format, editParse: gridnf.editParse, editFormat: gridnf.editFormat, css: "right" },
        { id: "vrt", header: { text: _("vrt"), css: "header" }, adjust: true, collection: VAT(this.app), editor: "combo", hidden: "vib".includes(ENT) ? false : true }
      ],
      onClick: {
        "wxi-trash": function (e, id) {
          if (!ROLE.includes('PERM_CATEGORY_ITEMS_DELETE')) {
            webix.message(_("no_authorization"), "error")
            return false
          }
          webix.confirm(_("del_confirm_msg"), "confirm-warning").then(() => {
            this.remove(id)
            return false
          })
        },
        "wxi-download": function (e, id) {
          webix.message(`${_("export_report")}.......`, "info", -1, "excel_ms");
          let params
          let obj = $$("gns:form").getValues()
          Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
          if (!params) params = {}
          params.filter = obj
          //
          webix.ajax().response("blob").get("api/gns/xls", params).then(data => {

            webix.html.download(data, `${webix.uid()}.xlsx`)
            $$("gns:form").enable()
            webix.message.hide("excel_ms");
            $$("gns:form").hideProgress()

          }).catch(e => {

            $$("gns:form").enable();
            webix.message.hide("excel_ms");
            $$("gns:form").hideProgress();


          })

        }
      },
      url: "api/gns",
      save: { url: "api/gns", updateFromResponse: true }
    }

    let form1 = {
      view: "form",
      id: "gns:form1",
      minWidth: 300,
      elementsConfig: { labelWidth: 100 },
      elements: [
        { id: 'code', name: "code", label: _("gns_code"), view: "text", required: true, attributes: { maxlength: 30 }, placeholder: _("gns_code") },
        { name: "name", label: _("gns_name"), view: "text", required: true, attributes: { maxlength: 255 }, placeholder: _("gns_name") },
        { name: "unit", label: _("unit"), view: "text", required: (!["yusen"].includes(ENT)) ? true : false, suggest: { url: "api/cat/kache/unit" }, attributes: { maxlength: 30 }, placeholder: _("unit") },//view: "combo"
        { name: "price", label: _("price"), type: 'number', attributes: { step: 1000, min: 0 }, view: "text", inputAlign: "right", css: "right", validate: v => { return (v >= 0) } },
        "vib".includes(ENT) ?
          {
            id: 'vrt', name: "vrt", label: _("vrt"), view: "combo", inputAlign: "right", value: "10",
            options: VAT(this.app)
          } : { hidden: true },
        {
          cols: [
            { view: "button", id: "gns:btnunselect", type: "icon", icon: "mdi mdi-select-off", label: _("unselect"), disabled: true },
            { view: "button", id: "gns:btnupd", type: "icon", icon: "mdi mdi-database-edit", label: _("update"), disabled: true },
            { view: "button", id: "gns:btnadd", type: "icon", icon: "wxi-plus", label: _("create"), disabled: ROLE.includes('PERM_CATEGORY_ITEMS_CREATE') ? false : true },
            { view: "button", id: "gns:btnxls", type: "icon", icon: "mdi mdi-file-excel", label: "Excel", disabled: ROLE.includes('PERM_CATEGORY_ITEMS_EXCEL') ? false : true }

          ]
        },

      ],
      rules: (!["yusen"].includes(ENT)) ? {
        code: webix.rules.isNotEmpty,
        name: webix.rules.isNotEmpty,
        unit: webix.rules.isNotEmpty,
        $obj: data => {
          if (!(containhtmlchar(data.name))) {
            webix.message(_("contain_html_char"))
            $$("gns:name").focus()
            return false
          }
          if (!(containhtmlchar(data.code))) {
            webix.message(_("contain_html_char"))
            $$("code").focus()
            return false
          }
          if (!(containhtmlchar(data.unit))) {
            webix.message(_("contain_html_char"))
            $$("gns:unit").focus()
            return false
          }
          return true
        }
      } : {
        code: webix.rules.isNotEmpty,
        name: webix.rules.isNotEmpty
      },
      on: {
        onAfterValidation: (result) => {
          if (!result) webix.message(_("required_msg"))
        }
      }
    }
    let title_page = { view: "label", label:`<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("catalog_items")}</span>`}
    return { paddingX: 2, cols: [{ rows: [title_page, form, grid1, pager], gravity: 3 }, { view: "resizer" }, { rows: [form1, {}] }] }
  }

  ready(v, urls) {
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }

  init() {
    if (!ROLE.includes('PERM_CATEGORY_ITEMS')) {
      this.show("/top/home")
      window.location.replace(`/${homepage}`)
      return
    }
    this.GnsWin = this.ui(GnsWin)
    let grid1 = $$("gns:grid1"), form1 = $$("gns:form1"), grid2 = $$("gns:grid2")
    form1.bind(grid1, null, function (data) {

      return decodehtml(data)
    });
    $$("gns:btnread").attachEvent("onItemClick", () => {
      const uploader = $$("gns:file"), files = uploader.files, file_id = files.getFirstId()
      if (file_id) {
        uploader.send(result => {
          grid2.clearAll()
          if (result) grid2.parse(result)
        })
      }
      else webix.message(_("file_required"))
    })

    $$("gns:btnsave").attachEvent("onItemClick", () => {
      let rows = grid2.serialize(), arr = []
      for (let row of rows) {
        if (row && row.chk && !row.error) arr.push(row)
      }
      if (arr.length < 1) {
        webix.message(_("gns_required"))
        return false
      }
      webix.ajax().post("api/gns/ins", JSON.stringify(arr)).then(result => {
        const obj = result.json()
        for (const kq of obj) {
          let row = arr.find(x => x.id == kq.id)
          let record = grid2.getItem(row.id)
          record["error"] = _(kq.error)
        }
        grid2.refresh()
        webix.message(_("send_data_ok_pls_check_err_msg"))
      })
    })

    const search = () => {
      grid1.clearAll()
      grid1.loadNext(size, 0, null, "gns->api/gns", true)
    }

    $$("gns:btnsearch").attachEvent("onItemClick", () => {
        search()
    })

    $$("gns:btnupd").attachEvent("onItemClick", () => {
      if (form1.isDirty()) {
        if (form1.validate()) {
        grid1.waitSave(()=>{
            grid1.updateItem($$("gns:form1").getValues().id,$$("gns:form1").getValues())
        }).then(r => {
                webix.message(_("upd_ok_msg"), "success")
                form1.clear()
                grid1.unselectAll()
        })
        }
      }
    })

    $$("gns:btnadd").attachEvent("onItemClick", () => {
      if (form1.validate()) {
        grid1.waitSave(() => {
          let formData = $$("gns:form1").getValues();
          grid1.add(formData)
        }).then(r => {
                webix.message(_("save_ok_msg"), "success")
                form1.clear()
        })
        search()
      }
    })

    $$("gns:btnxls").attachEvent("onItemClick", () => {
      this.GnsWin.show()
      grid2.clearAll()
    })

    $$("gns:btnunselect").attachEvent("onItemClick", () => {
      grid1.unselectAll()
    })

    grid1.attachEvent("onBeforeLoad", function () {
      this.showOverlay(_("loading"))
    })
    grid1.attachEvent("onAfterLoad", function () {
      if (!this.count()) this.showOverlay(_("notfound"))
      else this.hideOverlay()
    })

    grid1.attachEvent("onAfterSelect", selection => {
      if (ROLE.includes('PERM_CATEGORY_ITEMS_UPDATE')) $$("gns:btnupd").enable()
      $$("gns:btnadd").disable()
      $$("gns:btnxls").disable()
      if (ROLE.includes('PERM_CATEGORY_ITEMS_UNCHECK')) $$("gns:btnunselect").enable()
      $$("code").disable()
    })

    grid1.attachEvent("onAfterUnSelect", selection => {
      $$("gns:btnupd").disable()
      if (ROLE.includes('PERM_CATEGORY_ITEMS_CREATE')) $$("gns:btnadd").enable()
      $$("gns:btnxls").enable()
      $$("gns:btnunselect").disable()
      $$("code").enable()
    })
  }
}
