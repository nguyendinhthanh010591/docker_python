import { JetView } from "webix-jet"
import { filter, size, expire, ROLE, SSTATUS, ITYPE, SENDINVTYPE, INVTYPE, SESS_STATEMENT_HASCODE, SITYPE, ENT } from "models/util"

const setDefaultData = () => {
    $$("serial123:form1").setValues({
        "type": "01GTKT",
        "fd": new Date(),
        "form": "1",
        "serial": "",
        "priority": "1",
        "min": "1",
        "max": "99999999",
        "intg": 2,
        "sendtype": 1,
        "invtype": ""
    })
}

export default class SerialView extends JetView {
    config() {
        let form = {
            view: "form",
            id: "serial123:form",
            padding: 5,
            margin: 5,
            elementsConfig: { labelWidth: 65 },
            elements: [
                {
                    cols: [
                        { id: "serial123:fd", name: "fd", label: _("from"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr },
                        { id: "serial123:td", name: "td", label: _("To"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr },//minWidth: 130,
                        { id: "serial123:type", name: "type", label: _("invoice_type"), view: "combo", suggest: { data: ITYPE(this.app) }, gravity: 1.3 },
                        //{ id: "serial123:type", name: "type", label: _("invoice_type"), view: "combo", suggest: {data: SESS_STATEMENT_INVTYPE ? SESS_STATEMENT_INVTYPE.split(",").map(e=>{return {id:e, value:ITYPE(this.app).find(ei=>ei.id==e).value}}) : [], fitMaster:false}, gravity: 1.3 },
                        { id: "serial123:config", name: "degree_config", label: _("config"), view: "text", value: "123", hidden:true },
                        { view: "combo", id: "serial123:status", name: "status", label: _("status"), options: SSTATUS(this.app) },
                        { view: "button", id: "serial123:btnsearch", type: "icon", icon: "wxi-search", width: 33, tooltip: _("search") }
                    ]
                }
            ],
            rules: {
                fd: webix.rules.isNotEmpty,
                td: webix.rules.isNotEmpty,
                $obj: function (obj) {
                    if (obj.fd > obj.td) {
                        webix.message(_("date_invalid_msg"), "error", expire)
                        return false
                    }
                    return true
                }

            }
        }

        webix.proxy.serial = {
            $proxy: true,
            load: function (view, params) {
                let obj = $$("serial123:form").getValues()
                Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                if (!params) params = {}
                params.filter = obj
                return webix.ajax(this.source, params)
            }
        }

        let pager = { view: "pager", id: "serial123:pager", size: size, template: "{common.first()}{common.prev()}{common.pages()}{common.next()}{common.last()}" }
        let grid = {
            view: "datatable",
            id: "serial123:grid",
            select: "row",
            minWidth: 700,
            multiselect: false,
            resizeColumn: true,
            datafetch: size,
            pager: "serial123:pager",
            headerRowHeight: 33,
            columns: [
                {
                    id: "btn", header: "", template: (obj) => {
                        const status = obj.status
                        if (status == 1) return `<span class='webix_icon wxi-trash', title='${_("serial_btn_cancel")}'></span>`
                        else if (status == 3) return `<span class='webix_icon wxi-check', title='${_("serial_btn_app")}'></span>` // Duy - Bo Approve Serial App
                        else return ""
                    }, width: 35
                },
                // {
                //     id: "btnleftwarn", header: "", width: 35, template: obj => `<span class='mdi ${obj.leftwarn == 0 ? 'mdi-bell-ring-outline' : 'mdi-bell-off-outline'}', title='${obj.leftwarn == 0 ? _('Left_warn_on') : _('Left_warn_off')}'></span>`
                // },
                { id: "id", header: { text: "ID", css: "header" }, sort: "server", adjust: true },
                { id: "taxc", header: { text: _("taxcode"), css: "header" }, sort: "server", adjust: true },
                { id: "type", header: { text: _("invoice_type"), css: "header" }, sort: "server", adjust: true, collection: ITYPE(this.app) },
                { id: "form", header: { text: _("form"), css: "header" }, sort: "server", adjust: true },
                { id: "serial", header: { text: _("serial"), css: "header" }, sort: "server", adjust: true },
                // { id: "priority", header: { text: _("priority"), css: "header" }, sort: "server", adjust: true },
                { id: "min", header: { text: _("from"), css: "header" }, sort: "server", css: "right", adjust: true },
                { id: "max", header: { text: _("To"), css: "header" }, sort: "server", css: "right", adjust: true },
                { id: "cur", header: { text: _("current"), css: "header" }, sort: "server", css: "right", adjust: true },
                { id: "status", header: { text: _("status"), css: "header" }, sort: "server", options: SSTATUS(this.app), adjust: true },
                { id: "fd", header: { text: _("valid_from"), css: "header" }, sort: "server", format: webix.i18n.dateFormatStr, adjust: true },
                { id: "td", header: { text: _("valid_to"), css: "header" }, sort: "server", format: webix.i18n.dateFormatStr, adjust: true },
                { id: "uc", header: { text: _("uc"), css: "header" }, sort: "server", adjust: true },
                { id: "dt", header: { text: _("created_dt"), css: "header" }, sort: "server", format: webix.i18n.dateFormatStr, adjust: true },
                { id: "sendtype", header: { text: _("sendtype"), css: "header" }, sort: "server",options: SENDINVTYPE(this.app), adjust: true, },
                { id: "invtype", header: { text: _("invtype"), css: "header" }, sort: "server",options: INVTYPE(this.app), adjust: true }
            ],
            scheme: {
                $init: (obj) => {
                    obj.fd = new Date(obj.fd)
                    if (obj.td) obj.td = new Date(obj.td)
                    obj.dt = new Date(obj.dt)
                }
            },
            save: "api/serial",
            onClick: {
                "wxi-trash": function (e, id) {
                    if (ROLE.includes(12)) {
                        let me = this
                        webix.confirm(`${_("serial_cancel_msg1")} ${id} ?`, "confirm-warning").then(() => {
                            webix.ajax(`api/sca/${id}`).then(result => {
                                const json = result.json()
                                if (json.affectedRows == 1) {
                                    webix.message(`${_("serial_cancel_msg2")} ${id}`, "success", expire)
                                    me.clearAll()
                                    me.loadNext(size, 0, null, "serial->api/serial", true)
                                    setDefaultData()
                                }
                            })
                        })
                    }
                    else webix.message(_("serial_cancel_err"), "error", expire)
                },
                /* Duy - Bo Approve Serial App */
                "wxi-check": function (e, id) {
                    let me = this
                        webix.confirm(`${_("serial_app_msg1")} ${id} ?`, "confirm-warning").then(() => {
                            webix.ajax(`api/sap/${id}`).then(result => {
                                let json = result.json()
                                if (json) {
                                    webix.message(`${_("serial_app_msg2")} ${id}`)
                                    me.clearAll()
                                    me.loadNext(size, 0, null, "serial->api/serial", true)
                                }
                            })
                        })
                },
                "mdi-bell-off-outline": function (e, id) {
                    let me = this
                    let values = this.getItem(id.row)
                    values.leftwarn = 0
                    webix.ajax().put("api/serial/leftwarn", values).then(result => {
                        webix.message(`${_("left_warn_off_ed")} ${values.id}`, "success")
                        me.clearAll()
                        me.loadNext(size, 0, null, "serial->api/serial", true)
                        setDefaultData()
                    })
                },
                "mdi-bell-ring-outline": function (e, id) {
                    let me = this
                    let values = this.getItem(id.row)
                    console.log(values)
                    values.leftwarn = 1
                    webix.ajax().put("api/serial/leftwarn", values).then(result => {
                        webix.message(`${_("left_warn_on_ed")} ${values.id}`, "success")
                        me.clearAll()
                        me.loadNext(size, 0, null, "serial->api/serial", true)
                        setDefaultData()
                    })
                }
            }
        }

        let form1 = {
            view: "form",
            id: "serial123:form1",
            padding: 5,
            margin: 5,
            minWidth: 300,
            elementsConfig: { labelWidth: 120 },
            elements: [
                { view: "combo", id: "serial1123:type", name: "type", label: _("invoice_type"), suggest: { data: ITYPE(this.app) }, required: true },
                { view: "datepicker", id: "serial1123:dt", name: "fd", label: _("effective_date"), stringResult: true, format: webix.i18n.dateFormatStr, required: true },//, editable: true,
                { view: "text", id: "serial1123:form", name: "form", label: _("form"), attributes: { maxlength: 11 }, required: true },
                { view: "text", id: "serial1123:serial", name: "serial", label: _("serial"), attributes: { maxlength: 6 }, required: true,
                on: {
                    "onBlur": (thisElement) => {
                        let _sVal = thisElement.getValue(), _char1 = _sVal[0], _arr = []
                        if (_char1 == "C") _arr.push(SENDINVTYPE(this.app)[0])
                        // else if (_char1 == "K") _arr.push(SENDINVTYPE(this.app)[1])
                        if (_arr.length) $$("serial1123:sendtype").getList().clearAll()
                        else _arr = SENDINVTYPE(this.app)
                        $$("serial1123:sendtype").getList().parse(_arr)
                        $$("serial1123:sendtype").setValue(_arr[0].id)
                    }
                } 
                },
                { view: "text", id: "serial123:priority", type: "number", name: "priority", label: _("priority"), value: 1, attributes: { min: 1, max: 99 }, validate: (value) => value <= 99 && value > 0, invalidMessage: _("invalidMessage_serial_priority"), inputAlign: "right", required: true, hidden: true },
                { view: "text", id: "serial1123:min", type: "number", name: "min", label: _("fn"), value: 1, attributes: { step: 1, min: 1, max: 99999998 }, inputAlign: "right", required: true },
                { view: "text", id: "serial1123:max", type: "number", name: "max", label: _("tn"), value: 99999999, attributes: { step: 1000, min: 1, max: 99999999 }, inputAlign: "right", required: true, hidden: true },
                { view: "text", id: "serial1123:priority", type: "number", name: "priority", label: _("priority"), value: 1,attributes: { min: 1, max: 99 }, validate: function(value){ 
                    return value <= 99 && value > 0; 
                  }, invalidMessage: _("invalidMessage_serial_priority"),inputAlign: "right", required: true },
                { view: "combo", id: "serial1123:uses", name: "uses", label: _("input_type"), suggest: { data: SITYPE(this.app), filter: filter } ,gravity: 1,required: true},
                { id: "serial1123:des", name: "des", label: _("uses_des"), view: "text",attributes: { maxlength: 200 } },
                { id: "serial1123:taxc", name: "taxc", label: _("taxcode"), view: "multiselect", suggest: { selectAll: true, url: "api/serial/taxt" ,filter: filter, template: webix.template("#value#")} ,required: true},
                { view: "checkbox", id: "serial1123:intg", name: "intg", checkValue: 1, uncheckValue: 2, value: 2, label: _("integration"), hidden: true },
                { view: "richselect", id: "serial1123:sendtype", name: "sendtype", label: _("sendtype"), options: SENDINVTYPE(this.app) },
                { view: "richselect", id: "serial1123:invtype", name: "invtype", label: _("invtype"), options: INVTYPE(this.app), disabled:true, required: true },
                {
                    cols: [
                        { view: "button", id: "serial123:btnunselect", type: "icon", icon: "mdi mdi-select-off", label: _("unselect"), disabled: true },
                        { view: "button", id: "serial123:btnupd", type: "icon", icon: "mdi mdi-database-edit", label: _("update"), disabled: true },
                        { view: "button", id: "serial123:btndel", type: "icon", icon: "wxi-trash", label: _("delete"), disabled: true },
                        { view: "button", id: "serial123:btnsave", type: "icon", icon: "wxi-plus", label: _("create") }
                    ]
                }

            ],
            on: {
                onAfterValidation: (result) => {
                    if (!result) webix.message(_("required_msg"), "error", expire)
                }
            },
            rules: {
                type: webix.rules.isNotEmpty,
                fd: webix.rules.isNotEmpty,
                serial: webix.rules.isNotEmpty,
                form: webix.rules.isNotEmpty,
                min: webix.rules.isNumber,
                max: webix.rules.isNumber,
                priority: webix.rules.isNotEmpty,
                priority: webix.rules.isNumber,
            }
        }

        const c3 = {
            cols: [
                pager,
                // { view: "label", label: _("serial_tax_dec"), css: "bold", align: "right", width: 120 },
                // { view: "button", id: "serial123:btntbph", type: "icon", icon: "mdi mdi-select-off", label: _("serial_btn_release"), width: 175 },
                // { view: "button", id: "serial123:btnqdsd", type: "icon", icon: "mdi mdi-checkbox-multiple-blank-outline", label: _("serial_btn_qdsd"), width: 175 },
                // { view: "button", id: "serial123:btndktd", type: "icon", icon: "mdi mdi-content-paste", label: _("serial_btn_tdtt"), width: 175 }
            ]
        }

        return { paddingX: 2, cols: [{ rows: [form, grid, c3], gravity: 2.5 }, { view: "resizer" }, { rows: [form1, {}] }] }


    }
    ready() {
        setDefaultData()
    }
    init() {
        $$("serial1123:form").disable()
        //if (TAXC == "3301644331") $$("serial1123:intg").show()
        let row
        const all = { id: "*", value: _("all") }
        let date = new Date(), firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1)
        $$("serial123:fd").setValue(firstDay)
        $$("serial123:td").setValue(date)
        $$("serial1123:dt").setValue(date)
        let grid = $$("serial123:grid"), form1 = $$("serial123:form1"), form = $$("serial123:form"), serial = $$("serial1123:serial"), stype = $$("serial1123:type"), suses = $$("serial1123:uses"), staxc = $$("serial1123:taxc"), sform = $$("serial1123:form")
        form1.bind(grid)
        webix.event(serial.getNode(), "keyup", e => { serial.setValue(serial.getValue().toUpperCase()) }, { bind: serial })
        webix.event(sform.getNode(), "keyup", e => { sform.setValue(sform.getValue().toUpperCase()) }, { bind: sform })

        stype.attachEvent("onChange", newv => {
            if (["01GTKT"].includes(newv)) sform.setValue(1)
            if (["02GTTT","07KPTQ"].includes(newv)) sform.setValue(2)
            if (["03XKNB","04HGDL"].includes(newv)) sform.setValue(6)
            if (["01/TVE","02/TVE"].includes(newv)) sform.setValue(5)
        })

        $$("serial123:type").getList().add(all, 0)
        $$("serial123:type").setValue("*")
        $$("serial123:status").getList().add(all, 0)
        $$("serial123:status").setValue("*")
        $$("serial1123:sendtype").setValue(1)
        // stype.setValue("01GTKT")
        $$("serial1123:sendtype").attachEvent("onChange", newv => {
            if (newv == 1) {
                $$("serial1123:invtype").setValue()
                $$("serial1123:invtype").disable()
            }
            else{
                $$("serial1123:invtype").setValue(1)
                $$("serial1123:invtype").enable()
            }
        })
        stype.setValue(ITYPE(this.app)[0].id)
        //$$("aaa").define({pattern:{ mask:"H:##### ##"}});
        //$$("aaa").refresh();

        const search = () => {
            if (form.validate()) {
                grid.clearAll()
                grid.loadNext(size, 0, null, "serial->api/serial", true)
            }
        }

        $$("serial123:btnsearch").attachEvent("onItemClick", () => {
            search()
            setDefaultData()
        })

        $$("serial123:btnupd").attachEvent("onItemClick", () => {
            if (!form1.isDirty() || !form1.validate()) return false
            const obj = form1.getValues(), max = Number(obj.max), cur = Number(obj.cur)
            if (max <= cur || max > 99999999) {
                $$("serial1123:max").focus()
                webix.message(`Đến số phải thuộc khoảng ${cur + 1}..99999999`, "error", expire)
                return false
            }
            obj.webix_operation = "update"
            webix.ajax().post("api/serial", obj).then(result => {
                const json = result.json()
                if (json == 1) {
                    webix.message(_("released_update_ok"), "success", expire)
                    search()
                    setDefaultData()
                }
            })
        })

        $$("serial123:btndel").attachEvent("onItemClick", () => {
            webix.confirm(_("serial_del_msg"), "confirm-warning").then(() => { grid.remove(row.id) }).then(()=>{
                webix.message(_("del_serial_successful"), "success")
                setDefaultData()
            })
        })

        $$("serial123:btnsave").attachEvent("onItemClick", () => {
            if (!form1.validate()) return false

            // VALIDATE check SERIAL
            let _error = []
            let _serial = $$("serial1123:serial").getValue(), _serial1char = _serial[0], _serial23char = _serial.slice(1,3), _serial4char = _serial.slice(3,4), _serial56char = _serial.slice(4,6)
            let _stype = $$("serial1123:type").getValue()
            //let curYear = moment().format("YY")
            let curYear = String((new Date()).getFullYear()).substring(2)
            let validYear = [curYear, `${parseInt(curYear)+1}`]
            // Char [1]
            if (SESS_STATEMENT_HASCODE) {
                if (_serial1char != "C") _error.push(`${_("ser_1_must")} C`)
            } else {
                if (_serial1char != "K") _error.push(`${_("ser_1_must")} K`)
            }
            // Char [2,3]
            //khong check ky tu nam
            // if (!validYear.includes(_serial23char)) _error.push(_("ser_23_wrong_year"))
            // Char [4]
            //khong check ki tu thu 4
            // if (_stype == "03XKNB" && _serial4char != "N") _error.push(`${_("ser_4_char")} N`)
            // else if (_stype == "04HGDL" && _serial4char != "B") _error.push(`${_("ser_4_char")} B`)
            // else if (_stype == "01/TVE" && _serial4char != "G") _error.push(`${_("ser_4_char")} G`)
            // else if (_stype == "02/TVE" && _serial4char != "H") _error.push(`${_("ser_4_char")} H`)
            // else if (["01GTKT","02GTTT","07KPTQ"].includes(_stype) && _serial4char != "T") _error.push(`${_("ser_4_char")} T`)
            // Char [5,6]
            if (!/^[ABCDEGHKLNMPQRSTUVXY]{2,2}$/.test(_serial56char)) _error.push(_("ser_56_invalid"))
            if (_error.length) this.webix.message(_error.join("<hr/>"), "error")
            else {
                const param = form1.getValues()
                param.degree_config = "123"
                param.webix_operation = "insert"
                webix.ajax().post("api/serial", param).then(result => {
                    const json = result.json()
                    if (json.seri) {
                        webix.message(`Số thứ tự mẫu tăng lên ${json.seri} đơn vị, đã tồn tại ${json.seri} ký hiệu ${_serial.slice(0,4)}`, "success", expire)
                    }
                    if (json.arr == 1) {
                        webix.message(`Đã thêm mới phát hành`, "success", expire)
                        search()
                        setDefaultData()
                    }
                })
            }
        })

        $$("serial123:btnunselect").attachEvent("onItemClick", () => {
            grid.unselectAll()
            setDefaultData()
        })

        // $$("serial123:btnqdsd").attachEvent("onItemClick", () => {
        //     webix.ajax().response("blob").get("api/serial/docx/sqdsd").then(data => { webix.html.download(data, "qdsd.docx") })
        // })
        //
        // $$("serial123:btntbph").attachEvent("onItemClick", () => {
        //     webix.ajax().response("blob").get("api/serial/docx/stbph").then(data => { webix.html.download(data, "tbph.docx") })
        // })
        //
        // $$("serial123:btndktd").attachEvent("onItemClick", () => {
        //     systime().then(now => {
        //         webix.ajax(`${USB}cert/${now.getTime()}`).then(result => {
        //             const ca = result.json()
        //             if (!ca) {
        //                 webix.message("Không lấy được MST từ USB token", "error")
        //                 return false
        //             }
        //             const mst = ca.mst
        //             if (mst !== TAXC) {
        //                 webix.message(`Chứng thư số trong USB token MST ${mst} khác với ${TAXC}`, "error", expire)
        //                 return false
        //             }
        //             webix.ajax().response("blob").get("api/serial/docx/sdktd", ca).then(data => { webix.html.download(data, "dktd.docx") })
        //         }).fail(err => webix.message("Lỗi đọc USB token", "error", expire))
        //     })
        // })

        grid.attachEvent("onBeforeLoad", function () {
            this.showOverlay("Đang tải dữ liệu...")
        })

        grid.attachEvent("onAfterLoad", function () {
            if (!this.count()) this.showOverlay("Không có bản ghi nào.")
            else this.hideOverlay()
        })

        grid.attachEvent("onAfterSelect", selection => {
            row = grid.getItem(selection.id)
            stype.disable()
            //sidx.disable()
            suses.disable()
            staxc.disable()
            sform.disable()
            $$("serial1123:min").disable()
            $$("serial123:btnunselect").enable()
            $$("serial123:btnsave").disable()
            if (row.status == 3) {
                $$("serial123:btnupd").enable()
                serial.enable()
                $$("serial123:btndel").enable()
                $$("serial1123:dt").enable()
                $$("serial123:priority").enable()
                $$("serial1123:max").enable()
            }
            else {
                serial.disable()
                $$("serial123:btndel").disable()
                $$("serial1123:dt").disable()
                $$("serial123:priority").disable()
                $$("serial1123:max").disable()
            }
        })

        grid.attachEvent("onAfterUnSelect", () => {
            stype.enable()
            serial.enable()
            //sidx.enable()
            suses.enable()
            staxc.enable()
            sform.enable()
            $$("serial1123:min").enable()
            $$("serial1123:dt").enable()
            $$("serial123:priority").enable()
            $$("serial1123:max").enable()
            $$("serial123:btnunselect").disable()
            $$("serial123:btnupd").disable()
            $$("serial123:btndel").disable()
            $$("serial123:btnsave").enable()
        })
    }
}