import { JetView } from "webix-jet"
import { filter, size, d2s, t2s, ROLE, SSTATUS, ITYPE, SITYPE,SENDINVTYPE,DTSENDTYPE, INVTYPE, SESS_STATEMENT_HASCODE,ENT, setConfStaFld, maxseq,RPP, h, w } from "models/util"
const setDefaultData = () => {
    $$("serial:form1").setValues({
        "type": "01GTKT",
        "fd": new Date(),
        "form": "1",
        "serial": "",
        "priority": "1",
        "min": "1",
        "max": "99999999",
        "intg": 2,
        "sendtype": 1,
        "invtype": ""
    })
}
class CloneWin extends JetView {
    config() {
      let form = {
        view: "form",
        elements: [
          {
            cols: [
                { id: "serial:year_clone", name: "year_clone", label: _("Clone_year"), view: "text",attributes: { maxlength: 4 }, required: true, width: 500, labelWidth: 120},
               
            ]
          },
          {
            cols: [
                { view:'template', autowidth: true, height:50,template:`<span  style="color:red;font-size: 13px;font-weight:bold;text-align: center;display: flex;  " > 
                ${_("note_clone")}</span> `},
                { id: "serial:btnclone", view: "button", type: "icon", icon: "wxi-clock",  label: _("extend_inv"), width: 100 },
            ]
          }
        ]
      }
  
      return {
        view: "window",
        id: "serial:clonewin",
        height: h,
        width: w,
        position: "center",
        resize: true,
        modal: true,
        scroll:true,
        head: {
          view: "toolbar", height: 35, css: 'toolbar_window',
          cols: [
            { view: "label", label: _("extend_inv") },
            { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => { this.getRoot().hide() } }
          ]
        },
        body: { rows: [form] }
      }
    }
    show() {
      this.getRoot().show()
    }
}
export default class SerialView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        let form = {
            view: "form",
            id: "serial:form2",
            padding: 5,
            margin: 5,
            elementsConfig: { labelWidth: 85 },
            elements: [
                {
                    cols: [
                        { id: "serial:fd", name: "fd", label: _("from"), minWidth: 130, view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr },
                        { id: "serial:td", name: "td", label: _("to"), minWidth: 130, view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr },
                        { id: "serial:type", name: "type", label: _("invoice_type"), view: "combo", suggest: { data: ITYPE(this.app), filter: filter }, gravity: 1 },
                        { id: "serial:btnsearch", view: "button", type: "icon", icon: "wxi-search", width: 33, tooltip: _("search") },
                        // { id: "serial:serial", name: "serial", label: _("serial"), view: "multicombo", suggest: { selectAll: true,filter: filter, body:{datasr: [],template:webix.template("#value#")}},gravity: 1 } ,
                    ]
                },
                {
                    cols: [
                        { id: "serial:uses", name: "uses", label: _("input_type"), view: "combo", suggest: { data: SITYPE(this.app), filter: filter } ,gravity: 1},
                        { view: "combo", id: "serial:status", name: "status", label: _("status"), options: SSTATUS(this.app),gravity: 1 }
                        // {
                        //     cols: [
                        //         //{ view: "combo", id: "serial:taxc", name: "taxc", label: _("taxcode"), options: { view: "gridsuggest",  resize: true, body: { url: "api/serial/taxn", scroll: true, autoheight: false, autofocus: true, header: false, columns: [{ id: "id", adjust: true }, { id: "code", adjust: true }, { id: "value", adjust: true }] } }, gravity: 2 },
                        //        // { view: "combo", id: "serial:taxc", name: "taxc", label: _("taxcode"), suggest: { url: "api/serial/taxn", filter: filter } , gravity: 2 },
                        //        // { id: "serial:taxc", name: "taxc", label: _("taxcode"), view: "multicombo", suggest: { selectAll: true, url: "api/serial/taxt" } },
                        //         { id: "serial:btnsearch", view: "button", type: "icon", icon: "wxi-search", width: 33, tooltip: _("search") }
                        //     ], gravity: 1
                        // }
                    ]
                },
                {
                    cols: [
                       
                        { id: "serial1:serial", name: "serial", label: _("serial"), view: "multicombo", suggest: { selectAll: true,url: "api/serial/alls"}, placeholder: _("serial") },
                        ["sgr"].includes(ENT) ? { id: "serial:taxc", name: "taxc", label: _("taxcode"), view: "multicombo",tagMode: false,
                        tagTemplate: function (values) {
                            return (values.length + ` ${_("counttaxcode")}`);
                        }, suggest: { selectAll: true, url: "api/serial/taxt" ,filter: filter, template: webix.template("#value#")} }
                        : { id: "serial:taxc", name: "taxc", label: _("taxcode"), view: "multiselect", suggest: { selectAll: true, url: "api/serial/taxt" ,filter: filter, template: webix.template("#value#")} },
                    ]
                }
            ],
            rules: {
                fd: webix.rules.isNotEmpty,
                td: webix.rules.isNotEmpty,
                $obj: function (obj) {
                    if (obj.fd > obj.td) {
                        webix.message(_("date_invalid_msg"))
                        return false
                    }
                    return true
                }

            }
        }

        webix.proxy.serial = {
            $proxy: true,
            load: function (view, params) {
                let obj = $$("serial:form2").getValues()
                Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                if (!params) params = {}
                params.filter = obj
                return webix.ajax(this.source, params)
            }
        }

        let pager = {
            cols:[
                { view: "pager", id: "serial:pager", size: size, template: "{common.first()}{common.prev()}{common.pages()}{common.next()}{common.last()}" },
                 ]
            }
        let app_can = {
            id: "app_can",
            cols:[
                { view: "button", id: "serial:btnapp", type: "icon", icon: "wxi-check", label: _("approve"), width: 100},
                { view: "button", id: "serial:btncan", type: "icon", icon: "wxi-trash", label: _("cancel"), width: 100},
                { view: "button", id: "serial:btnres", type: "icon", icon: "wxi-clock", label: _("extend_inv_num"), width: 100}
            ],
            hidden: true
        } 
        let recpager = { view: "combo", id: "recpager", maxWidth: 70, tooltip: _("row_per_page"), value: 10, options: RPP }
        let grid = {
            view: "datatable",
            id: "serial:grid",
            select: "row",
            minWidth: 400,
            multiselect: false,
            resizeColumn: true,
            datafetch: size,
            pager: "serial:pager",
            save: "api/serial",
            columns: [
                { id: "chk", header: { content: "masterCheckbox", css: "center", contentId:"ch1" }, css: "center", template: "{common.checkbox()}", width: 45, hidden: true },
                {
                    id: "btn", header: `<span class='webix_icon wxi-sync' title='${_("serial_btn_refresh")}'></span>`, template: (obj, common) => {
                        const status = obj.status
                        if (status == 1) return `<span class='webix_icon wxi-trash', title='${_("serial_btn_cancel")}'></span>`
                        else if (status == 3) return `<span class='webix_icon wxi-check', title='${_("serial_btn_app")}'></span>`
                        else return ""
                    }, width: 35
                },
                { id: "id", header: { text: "ID", css: "header" }, sort: "server", adjust: true },
                { id: "taxc", header: { text: _("taxcode"), css: "header" }, sort: "server", adjust: true },
                { id: "form", header: { text: _("form"), css: "header" }, sort: "server", adjust: true },
                { id: "serial", header: { text: _("serial"), css: "header" }, sort: "server", adjust: true },
                { id: "priority", header: { text: _("priority"), css: "header" }, sort: "server", adjust: true },
                { id: "min", header: { text: _("from"), css: "header" }, sort: "server", css: "right", adjust: true },
                { id: "max", header: { text: _("to"), css: "header" }, sort: "server", css: "right", adjust: true },
                { id: "cur", header: { text: _("current"), css: "header" }, sort: "server", css: "right", adjust: true },
                { id: "status", header: { text: _("status"), css: "header" }, sort: "server", options: SSTATUS(this.app), adjust: true },
                { id: "fd", header: { text: _("valid_from"), css: "header" }, sort: "server", format: d2s, adjust: true },
                { id: "td", header: { text: _("valid_to"), css: "header" }, sort: "server", format: t2s, adjust: true },
                { id: "uses", header: { text: _("input_type"), css: "header" }, sort: "server", options: SITYPE(this.app), adjust: true },
                ENT == "vcm" ?
                { id: "des", header: { text: _("uses_des"), css: "header" }, sort: "server",format: webix.template.escape, collection: "api/cat/nokache/serialdes", adjust: true } : { id: "des", header: { text: _("uses_des"), css: "header" }, sort: "server",  adjust: true },
                { id: "sendtype", header: { text: _("dtsendtype"), css: "header" }, sort: "server",options: SENDINVTYPE(this.app), adjust: true, },
                { id: "invtype", header: { text: _("invtype"), css: "header" }, sort: "server",options: INVTYPE(this.app), adjust: true }
            ],
            onClick: {
                "wxi-sync": function (e, id) {
                    let me = this, taxc = $$("serial:taxc").getValue()
                    if (taxc) {
                        webix.ajax(`api/serial/sync/${taxc}`).then(result => {
                            webix.message(result.json())
                            me.clearAll()
                            me.loadNext(size, 0, null, "serial->api/serial", true)
                        })
                    }
                    else webix.message(_("taxcode_required"))
                },
                "wxi-trash": function (e, id) {
                    let item =  $$("serial:grid").getItem(id);
                   
                    if (ROLE.includes('PERM_TEMPLATE_REGISTER_CANCEL')) {
                        let me = this
                        webix.confirm(`${_("serial_cancel_msg1")} ${id} : ${item.taxc} ${item.form} ${item.serial}?`, "confirm-warning").then(() => {
                            webix.ajax(`api/sca/${id}`).then(result => {
                                const json = result.json()
                                if (json) {
                                    webix.message(`${_("serial_cancel_msg2")} ${id}`)
                                    me.clearAll()
                                    me.loadNext(size, 0, null, "serial->api/serial", true)
                                }
                            })
                        })
                    }
                    else {
                        webix.message(_("no_authorization"), "error")
                        return false
                    }
                },
                "wxi-check": function (e, id) {
                    if (ROLE.includes('PERM_TEMPLATE_REGISTER_APPROVE')) {
                        let me = this
                        webix.confirm(`${_("serial_app_msg1")} ${id} ?`, "confirm-warning").then(() => {
                            webix.ajax(`api/sap/${id}`).then(result => {
                                let json = result.json()
                                if (json) {
                                    webix.message(`${_("serial_app_msg2")} ${id}`)
                                    me.clearAll()
                                    me.loadNext(size, 0, null, "serial->api/serial", true)
                                }
                            })
                        })
                    }
                    else {
                        webix.message(_("no_authorization"), "error")
                        return false
                    }
                }
            }
        }

        let form1 = {
            view: "form",
            id: "serial:form1",
            padding: 5,
            margin: 5,
            minWidth: 400,
            scroll:true,
            elementsConfig: { labelWidth: 100 },// labelPosition: "top" 
            elements: [
                { view: "combo", id: "serial:type1", name: "type", label: _("invoice_type"), suggest: { data: ITYPE(this.app), filter: filter }, required: true },
                { view: "datepicker", id: "serial:dt", name: "fd", label: _("effective_date"), stringResult: true, format: webix.i18n.dateFormatStr, required: true, editable: true },// editable: true,
                { view: "text", id: "serial:form", name: "form", label: _("form"), attributes: { maxlength: 11 }, required: true },

                { view: "text", id: "serial:serial", name: "serial", label: _("serial"), attributes: { maxlength: 6 }, required: true, placeholder: _("serial"),
                on: {
                    "onBlur": (thisElement) => {
                        let _sVal = thisElement.getValue(), _char1 = _sVal[0], _arr = []
                        // if (_char1 == "C") _arr.push(DTSENDTYPE(this.app)[0])
                        // else if (_char1 == "K") _arr.push(SENDINVTYPE(this.app)[1])
                        if (_arr.length) $$("serial:sendtype").getList().clearAll()
                        else _arr = SENDINVTYPE(this.app)
                        $$("serial:sendtype").getList().parse(_arr)
                        // $$("serial:sendtype").setValue(_arr[0].id)
                    }
                } 
                },
                { view: "text", id: "serial:priority", type: "number", name: "priority", label: _("priority"), value: 1,attributes: { min: 1, max: 99 },validate: function(value){ 
                    return value <= 99 && value > 0; 
                  }, invalidMessage: _("invalidMessage_serial_priority"),inputAlign: "right", required: true },
                { view: "text", id: "serial:min", type: "number", name: "min", label: _("fn"), value: 1, attributes: { step: 1, min: 1, max: 9000000 }, inputAlign: "right", required: true },
                { view: "text", id: "serial:max", type: "number", name: "max", label: _("tn"), value: maxseq, attributes: { step: 10000, min: 100, max: maxseq }, inputAlign: "right", required: true },


                { id: "form1:uses", name: "uses", label: _("input_type"), view: "combo", suggest: { data: SITYPE(this.app), filter: filter }, value: 1, gravity: 2, required: true },
                ENT == "vcm" ? { id: "form1:des", name: "des", label: _("uses_des"), view: "combo", suggest: { url: `api/cat/nokache/serialdes`, filter: filter }, value: 1, gravity: 2, required: true, placeholder: _("uses_des") } : { id: "form1:des", name: "des", label: _("uses_des"), view: "text",attributes: { maxlength: 200 }, placeholder: _("uses_des")},
                { id: "serial:taxc1", name: "taxc", label: _("taxcode"), view: "multiselect", suggest: { selectAll: true, url: "api/serial/taxt" ,filter: filter, template: webix.template("#value#")} ,required: true, placeholder: _("taxcode")},
                { view: "richselect", id: "serial:sendtype", name: "sendtype", label: _("dtsendtype"), options: SENDINVTYPE(this.app) },
                { view: "richselect", id: "serial:invtype", name: "invtype", label: _("invtype"), options: INVTYPE(this.app), disabled:true, required: true },//options:  "api/ous/taxn"//, filter: filter
                {
                    cols: [
                        // { view: "button", id: "serial:btnedit", type: "icon", icon: "mdi mdi-wrench-outline", label: _("edit"), width: 100, disabled: true },
                        { view: "button", id: "serial:btnunselect", type: "icon", icon: "mdi mdi-select-off", label: _("unselect"), width: 100, disabled: true },
                        { view: "button", id: "serial:btndel", type: "icon", icon: "wxi-trash", label: _("delete"), width: 100, disabled: true },
                        { view: "button", id: "serial:btnsave", type: "icon", icon: "wxi-plus", label: _("create"), width: 100, disabled: ROLE.includes('PERM_TEMPLATE_REGISTER_CREATE') ? false : true}
                    ]
                }

            ],
            on: {
                onAfterValidation: (result) => {
                    if (!result) webix.message(_("required_msg"))
                }
            },
            rules: {
                type: webix.rules.isNotEmpty,
                fd: webix.rules.isNotEmpty,
                form: webix.rules.isNotEmpty,
                serial: webix.rules.isNotEmpty,
                min: webix.rules.isNotEmpty,
                max: webix.rules.isNotEmpty,
                uses: webix.rules.isNotEmpty,
                taxc: webix.rules.isNotEmpty,
                min: webix.rules.isNumber,
                max: webix.rules.isNumber,
                priority:webix.rules.isNotEmpty,
                priority:webix.rules.isNumber,
                $obj: obj => {
                    const fd = obj.fd
                    const dty = (new Date(fd)).getUTCFullYear()
                    if (dty > 9999 || dty < 0) {
                        webix.message(_("invalid_data"))
                        $$("serial:dt").focus()
                        return false
                    }

                    const max = Number(obj.max), min = Number(obj.min)
                    if (max <= min || min < 1 || max > maxseq) {
                        webix.message(`${_("Message_min_max_serial")}${maxseq}`)
                        $$("serial:min").focus()
                        return false
                    }
                    if (!(containhtmlchar(obj.des))) {
                        webix.message(_("contain_html_char"))
                        $$("form1:des").focus()
                        return false
                    }
                    return true
                }
            }
        }
        const c3 = {
            rows: [ {cols: [recpager, pager, { id: "org:countinv", view: "label" },{}, app_can]},
                // {
                //     cols: [
                //         { view: "label", label: _("serial_tax_dec"), css: "bold", align: "right" },
                //         { view: "button", id: "xml:btntxml", type: "icon", icon: "mdi mdi-file-download", label: _("serial_btn_xmldl"), disabled: true },
                //         { view: "button", id: "docx:btntbph", type: "icon", icon: "mdi mdi-select-off", label: _("serial_btn_release"), disabled: true },
                //         { view: "button", id: "docx:btnqdsd", type: "icon", icon: "mdi mdi-checkbox-multiple-blank-outline", label: _("serial_btn_qdsd"), disabled: true },
                //         { view: "button", id: "docx:btndktd", type: "icon", icon: "mdi mdi-content-paste", label: _("serial_btn_tdtt"), disabled: true, hidden: "vcm,ssi".includes(ENT) ? true : false},
                //         { view: "button", id: "xml:btnexrep", type: "icon", icon: "mdi mdi-file-download", label: _("serial_btn_exreport"), disabled: ["vib"].includes(ENT) ? false : true , hidden: true }
                //     ]
                // }
            ]
        }
        // if(["fhs","fbc"].includes(ENT))
        //     c3.rows[1].cols.push({ view: "button", id: "xml:btntxml", type: "icon", icon: "mdi mdi-file-download", label: _("serial_btn_xmldl"), disabled: true });
        let title_page = { view: "label", label:`<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("releasedND123")}</span>`}
        return { paddingX: 2, cols: [{ rows: [title_page, form, grid, c3], gravity: 1.5}, { view: "resizer" }, { rows: [form1] }] }
    }
    ready(v, urls) {
      //Cau hinh rieng cac cot tĩnh
      webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
          let json = result.json()
          setConfStaFld(json)
      })
      //
    }
    init() {
        this.CloneWin = this.ui(CloneWin)
        $$("serial:form").disable()
        if (!ROLE.includes('PERM_TEMPLATE_REGISTER')) {
            this.show("/top/home")
            window.location.replace(`/${homepage}`)
            return
        }
        const all = { id: "*", value: _("all") }
        // webix.ajax("api/sysdate").then(result => {
        //     let dt = new Date(result.json())
        //     dt.setHours(0, 0, 0, 0)
        //     $$("serial:fd").setValue(new Date(dt.getFullYear(), 0, 1))
        //     $$("serial:td").setValue(dt)
        //     $$("serial:dt").setValue(dt)
        //     // $$("serial:dt").getPopup().getBody().define("minDate", dt)
        // })
        if(ROLE.includes('PERM_CLONE_SYMBOL')) $$("app_can").show()
        let date = new Date(), firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1),recpager = $$("recpager")
        $$("serial:fd").setValue(firstDay)
        $$("serial:td").setValue(date)
        $$("serial:dt").setValue(date)

        let grid = $$("serial:grid"), form1 = $$("serial:form1"), form = $$("serial:form2"), serial = $$("serial:serial"),stype = $$("serial:type1"),sform = $$("serial:form"), row
        //form1.bind(grid)
        webix.event(serial.getNode(), "keyup", e => { serial.setValue(serial.getValue().toUpperCase()) }, { bind: serial })
        webix.event(sform.getNode(), "keyup", e => { sform.setValue(sform.getValue().toUpperCase()) }, { bind: sform })
        $$("serial:type1").attachEvent("onChange", newv => {
            if (["01GTKT"].includes(newv)) sform.setValue(1)
            if (["02GTTT","07KPTQ"].includes(newv)) sform.setValue(2)
            if (["03XKNB","04HGDL"].includes(newv)) sform.setValue(6)
            if (["01/TVE","02/TVE"].includes(newv)) sform.setValue(5)
        })
        $$("serial:type").getList().add(all, 0)
        $$("serial:status").getList().add(all, 0)
        $$("serial:uses").getList().add(all, 0)
        $$("serial:type").setValue("*")
        $$("serial:status").setValue("*")
        $$("serial:uses").setValue("*")
        // $$("serial:type1").setValue("01GTKT")
        $$("serial:sendtype").setValue(1)
        $$("serial:sendtype").attachEvent("onChange", newv => {
            if (newv == 1) {
                $$("serial:invtype").setValue()
                $$("serial:invtype").disable()
            }
            else{
                $$("serial:invtype").setValue(9)
                $$("serial:invtype").enable()
            }
        })
        stype.setValue(ITYPE(this.app)[0].id)

        const search = () => {
            //webix.message(maxseq)
            if (form.validate()) {
                $$("serial:grid").showColumn("chk");
                $$("serial:grid").getHeaderContent("ch1").uncheck()
                if($$("serial:status").getValue() == "*"){
                    $$("serial:grid").hideColumn("chk");
                }
                grid.clearAll()
                // grid.loadNext(recpager.getValue(), 0, null, "serial->api/serial", true)
                grid.loadNext(size, 0, null, "serial->api/serial", true)

               
            }
        }

        $$("serial:btnsearch").attachEvent("onItemClick", () => {
            search()
        })
        
        // $$("xml:btnexrep").attachEvent("onItemClick", () => {
        //     if (form.validate()) {
        //       try {
        //         const param = form.getValues()
        //           webix.ajax().response("blob").get("api/serial/exrep", param).then(data => {
        //               webix.html.download(data, `${webix.uid()}.xlsx`)
        //           }).catch(e => {
        //             webix.message(_("serial_issue_error"), "error")
        //         })
        //       }
        //       catch (err) {
        //           webix.message(err.message, "error")
        //       }
        //     }
        // })
        
        // $$("serial:taxc").attachEvent("onChange", (n) => {
        //     if (n) {
        //         $$("docx:btnqdsd").enable()
        //         $$("docx:btntbph").enable()
        //         $$("docx:btndktd").enable()
        //         if(["fhs","fbc"].includes(ENT))
        //             $$("xml:btntxml").enable()
        //         $$("xml:btntxml").enable()
        //     }
        //     else {
        //         $$("docx:btnqdsd").disable()
        //         $$("docx:btntbph").disable()
        //         $$("docx:btndktd").disable()
        //         if(["fhs","fbc"].includes(ENT))
        //             $$("xml:btntxml").disable()
        //         $$("xml:btntxml").disable()
        //     }
        // })
        $$("serial:btnapp").attachEvent("onItemClick", () => {
            if(!ROLE.includes('PERM_TEMPLATE_REGISTER_APPROVE')) this.webix.message(_("serial_app_err"), "error")
            else{
                let arr = []
                let obj = $$("serial:grid")
                var size = obj.getPager().config.size
                var page = obj.getPage()
                var ind = size*page
                for(let i=0; i<size; i++) {
                    let id = obj.getIdByIndex(ind+i)
                    if(obj.getItem(id) && obj.getItem(id).chk == 1){
                    if(obj.getItem(id) && obj.getItem(id).chk && [3].includes(obj.getItem(id).status)){ // ký hiệu ở trạng thái 3(chờ duyệt) thì mới cho duyệt
                        arr.push(obj.getItem(id))
                    }
                }
                continue
                }
                if (arr.length < 1) {
                    webix.message(_("sign_must_select"))
                    return false
                }
                
                // thêm cảnh báo có chắc chắn duyệt ko
                webix.confirm(_("serial_app_msg1"), "confirm-warning").then(() => {
                    // for(let i=0;i<arr.length;i++) {
                        // if(ENT=="hdb" && (!ROLE.includes(11))) {
                        //     this.webix.message("Không có quyền", "error")
                        //     return
                        // }
                        webix.delay(() => {
                            webix.ajax().post("api/sap/mul",{id:arr}).then(result => {
                                let json = result.json()
                                console.log(json)
                                if (json) {
                                    webix.message(_(`serial_app_msg2`))
                                    $$("serial:grid").getHeaderContent("ch1").uncheck()                                
                                    grid.clearAll()
                                    grid.loadNext(size, 0, null, "serial->api/serial", true)
                                }
                            }).catch((err) => {
                                console.log("err", err)
                                // webix.message(`Duyệt ký hiệu id=${arr} có lỗi xuất hiện`)
                                console.log(err);
                                // return false
                            })
                        })
                        
                    // }
                    // webix.message(`${_("serial_app_msg2")}`)
                    
                })}
            })
            $$("serial:btnres").attachEvent("onItemClick", () => {
            if(!ROLE.includes('PERM_CLONE_SYMBOL')) this.webix.message(_("serial_clone_err"), "error")
            else{      
                    this.CloneWin.show()
                    grid.clearAll()
                }
            })
            $$("serial:status").attachEvent("onChange", (v) => {
                
                if (v == 1 || v== 3) {
                    $$("serial:grid").showColumn("chk");
                    $$("serial:grid").getHeaderContent("ch1").uncheck()
                    grid.clearAll()
                    grid.loadNext(size, 0, null, "serial->api/serial", true)
                } else {
                    $$("serial:grid").showColumn("chk");
                    $$("serial:grid").getHeaderContent("ch1").uncheck()
                    $$("serial:grid").hideColumn("chk");
                    grid.clearAll()
                    grid.loadNext(size, 0, null, "serial->api/serial", true)
                }
                
                  
            })
            $$("serial:btnclone").attachEvent("onItemClick", () => {
                let year_clone = $$("serial:year_clone").getValue()
                let year_clone_next = parseInt(year_clone) + 1
                if(!year_clone){
                    webix.message(_("year_clone_not_empty"), "error")
                    return
                }
                if(isNaN(year_clone)){
                    webix.message(_("year_clone_must_number"), "error")
                    return
                }
                webix.confirm((_("conf_from") + (`${year_clone}`+ _("to_year")+ (`${year_clone_next}`)) ), "confirm-warning").then(() => {
                    webix.ajax().post("api/serial/extend",{year:year_clone}).then(resultnew => {
                        let json = resultnew.json()
                        if (json.resultnew==1){
                            webix.message(_("suc_ext"), "success")
                            grid.clearAll()
                            grid.loadNext(size, 0, null, "serial->api/serial", true)
                        }else if(json.resultnew == 3){
                            webix.message(_("nodata_clone"), "error")
                        } else {
                            webix.message(_("unsuc_ext"), "error")
                        }
                    })
                })
            })
            $$("serial:btncan").attachEvent("onItemClick", () => {
            if(!ROLE.includes('PERM_TEMPLATE_REGISTER_CANCEL')) this.webix.message(_("serial_cancel_err"), "error")
            else{
                let arr = []            
                let obj = $$("serial:grid")
                var size = obj.getPager().config.size
                var page = obj.getPage()
                var ind = size*page
                for(let i=0; i<size; i++) {
                    let id = obj.getIdByIndex(ind+i)
                    let a = obj.getItem(id)
                    if(obj.getItem(id) && obj.getItem(id).chk == 1){
                    if(obj.getItem(id) && obj.getItem(id).chk && [1].includes(obj.getItem(id).status)){ // ký hiệu ở trạng thái khác 3(chờ chuyệt) thì mới cho hủy
                        arr.push(obj.getItem(id))
                    }
                }
                continue
                }
                if (arr.length < 1) {
                    webix.message(_("sign_must_unselect"))
                    return false
                }
                
                // thêm cảnh báo có chắc chắn hủy ko
                webix.confirm(_("serial_cancel_msg1"), "confirm-warning").then(() => {
                    // for(let i=0;i<arr.length;i++) {
                        if(ENT=="hdb" && (!ROLE.includes(12))) {
                            this.webix.message("Không có quyền", "error")
                            return
                        }
                        webix.delay(() => {
                            webix.ajax().post("api/sca/mul",{id:arr}).then(result => {
                                let json = result.json()
                                console.log(json)                       
                                if (json) {
                                    webix.message(_(`serial_cancel_msg2`))
                                    $$("serial:grid").getHeaderContent("ch1").uncheck()
                                    grid.clearAll()
                                    grid.loadNext(size, 0, null, "serial->api/serial", true)
                                }
                            }).catch((err) => {
                                console.log("err", err)
                                // webix.message(`Hủy ký hiệu id=${arr} có lỗi xuất hiện`)
                                console.log(err);
                                // return false
                            })
                        })
                    // }
                    // webix.message(`${_("serial_cancel_msg2")}`)
                    
                })}
            })
        $$("serial:btnsave").attachEvent("onItemClick", () => {
                if (!form1.validate()) return false
                // VALIDATE check SERIAL
                let _error = []
                let _serial = $$("serial:serial").getValue(), _serial1char = _serial[0], _serial23char = _serial.slice(1,3), _serial4char = _serial.slice(3,4), _serial56char = _serial.slice(4,6)
                let _stype = $$("serial:type").getValue()
                //let curYear = moment().format("YY")
                let curYear = String((new Date()).getFullYear()).substring(2)
                let eff_year = ($$("serial:dt").getValue()).slice(2,4)
                let validYear = [curYear, `${parseInt(curYear)+1}`]
                if(eff_year != _serial23char){
                    _error.push(_(`year_of_idt_must_belong_year_of_serial`))
                }
                if (_serial.length != 6) _error.push(`Kí hiệu phải có độ dài 6 ký tự`)
                // Char [1]
                if (SESS_STATEMENT_HASCODE) {
                    if (_serial1char != "C") _error.push(`${_("ser_1_must")} C`)
                } else {
                    if (_serial1char != "K") _error.push(`${_("ser_1_must")} K`)
                }
                // Char [2, 3]
                if (!Number.isInteger(parseInt(_serial23char[0])) || !Number.isInteger(parseInt(_serial23char[1]))) _error.push(`${_("ser_23_must")} số`)
                
                // if (!/^[ABCDEGHKLNMPQRSTUVXY]{2,2}$/.test(_serial56char)) _error.push(_("ser_56_invalid"))
                if (_error.length) this.webix.message(_error.join("<hr/>"), "error")
                else {
                    if (ENT == "hlv") {
                        var myformat = webix.Date.dateToStr("%d/%m/%y"); myformat(new Date())
                        var text = myformat(new Date());
                        const date_eff = $$("serial:dt").getValue()
                        let eff = myformat(webix.Date.add(new Date(), 2, "day"));
                        let dt = myformat(date_eff);
                        if (eff != dt) {
                            webix.confirm(_("confirm_date_hlv"), "confirm-warning").then(() => {
                                let param = form1.getValues()
                                param.webix_operation = "insert"
                                webix.ajax().post("api/serial", param).then(result => {

                                    const json = result.json()


                                    if (json == 1) {
                                        webix.message(_("Form_serial_success_msg"))
                                        search();
                                        var list = $$("serial1:serial").getPopup().getList();
                                        list.clearAll();
                                        list.load("api/serial/alls");
                                    }
                                })
                            })
                        } else {
                            let param = form1.getValues()
                            param.webix_operation = "insert"
                            webix.ajax().post("api/serial", param).then(result => {

                                const json = result.json()


                                if (json == 1) {
                                    webix.message(_("Form_serial_success_msg"))
                                    search();
                                    var list = $$("serial1:serial").getPopup().getList();
                                    list.clearAll();
                                    list.load("api/serial/alls");
                                }
                            })
                        }

                    } else {
                        let param = form1.getValues()
                        param.degree_config = "123"
                        param.webix_operation = "insert"
                        webix.ajax().post("api/serial", param).then(result => {
                            const json = result.json()
                            if (json.arr == 1) {
                                webix.message(_("Form_serial_success_msg"))
                                search()
                                setDefaultData()
                            }

                            // if (json == 1) {
                            //     webix.message(_("Form_serial_success_msg"))
                            //     search();
                            //     var list = $$("serial1:serial").getPopup().getList();
                            //     list.clearAll();
                            //     list.load("api/serial/alls");
                            // }
                        })
                    }
                }
            
        })
        // $$("serial:btnedit").attachEvent("onItemClick", () => {
        //     if(ENT=="hdb" && (!ROLE.includes('PERM_TEMPLATE_REGISTER'))) this.webix.message("Không có quyền", "error")
        //     else {
        //         let param = form1.getValues()
        //         console.log(param)
        //         // if (!form1.validate()) return false
        //         // let param = form1.getValues()
        //         param.webix_operation = "update"
        //         webix.ajax().post("api/serial", param).then(result => {
        //             const json = result.json()
        //             if (json==1) {
        //                webix.message(`Đã thêm mới phát hành`)
        //                search()
        //                var list = $$("serial1:serial").getPopup().getList();
        //                list.clearAll();
        //                list.load("api/serial/alls");
        //             }
        //         })
        //     }
        // })

        // $$("docx:btnqdsd").attachEvent("onItemClick", () => {
        //     const taxc = $$("serial:taxc").getValue(), url = `api/serial/docx/sqdsd/${taxc}`
        //     let arr=[]
        //     arr=taxc.split(',')
            
        //     if (arr.length>1) webix.message(_("alert_hddt"),"error")
        //     else
        //     webix.ajax().response("blob").get(url).then(data => { webix.html.download(data, "qdsd.docx") })
        // })

        // $$("docx:btntbph").attachEvent("onItemClick", () => {
        //     const taxc = $$("serial:taxc").getValue(), url = `api/serial/docx/stbph/${taxc}`
        //     let arr=[]
        //     arr=taxc.split(',')
            
        //     if (arr.length>1)  webix.message(_("alert_tbph"),"error")
        //     else
        //     webix.ajax().response("blob").get(url).then(data => { webix.html.download(data, "tbph.docx") })
        // })
        

        // if(["fhs","fbc"].includes(ENT)){
        //     $$("xml:btntxml").attachEvent("onItemClick", () => {
        //         const taxc = $$("serial:taxc").getValue(), url = `api/serial/xml/all/${taxc}`
        //         webix.ajax().response("blob").get(url).then(data => { webix.html.download(data, "tbphmst.xml") })
        //     })
        // }

        // $$("xml:btntxml").attachEvent("onItemClick", () => {
        //     const taxc = $$("serial:taxc").getValue(), url = `api/serial/xml/all/${taxc}`
        //     webix.ajax().response("blob").get(url).then(data => { webix.html.download(data, "tbphmst.xml") })
        // })

        $$("serial:btnunselect").attachEvent("onItemClick", () => {
            grid.unselectAll()
        })

        $$("serial:btndel").attachEvent("onItemClick", () => {
            webix.confirm(_("serial_delete_msg1"), "confirm-warning").then(() => { grid.waitSave(()=>{grid.remove(row.id)}).then(res=> {if(res) webix.message(_("serial_delete_msg2"))}) })
        })
        

        grid.attachEvent("onAfterSelect", selection => {
            row = grid.getItem(selection.id)
            $$("serial:btnunselect").enable()
            $$("serial:btnsave").disable()
            // $$("serial:form").disable()
            $$("serial:min").disable()
            if (row.status == 3) {
                $$("serial:serial").enable()
                if (ROLE.includes('PERM_TEMPLATE_REGISTER_DELETE')) $$("serial:btndel").enable()
                // $$("serial:btnedit").enable()
                $$("serial:taxc1").disable()
            }
            else {
                $$("serial:serial").disable()
                $$("serial:btndel").disable()
            }
            row.fd = new Date(row.fd)
            form1.setValues(row)
        })

        grid.attachEvent("onAfterUnSelect", () => {
            $$("serial:btnunselect").disable()
            $$("serial:btndel").disable()
            if (ROLE.includes('PERM_TEMPLATE_REGISTER_CREATE')) $$("serial:btnsave").enable()
            // $$("serial:form").enable()
            $$("serial:min").enable()
            $$("serial:serial").enable()
            $$("serial:taxc1").enable()
            form1.clear()
            form1.setValues({fd:new Date(), idx:1, min:1, max:maxseq, uses:1})
        })
        
        // $$("docx:btndktd").attachEvent("onItemClick", () => {
        //     const taxc = $$("serial:taxc").getValue(), url = `api/serial/docx/sdktd/${taxc}`
        //     let arr=[]
        //     arr=taxc.split(',')
            
        //     if (arr.length>1)  webix.message(_("alert_tbph"),"error")
        //     else webix.ajax().response("blob").get(url).then(data => { webix.html.download(data, "dktd.docx") })
        // })
        grid.attachEvent("onBeforeLoad", function () {
            this.showOverlay(_("loading"))
        })
        recpager.attachEvent("onChange", () => {
            $$("serial:pager").config.size = recpager.getValue()
            
            search()
        })
        grid.attachEvent("onAfterLoad", function () {
            if (!this.count()) this.showOverlay(_("notfound"))
            else this.hideOverlay()
        })
        if (ENT == "vib") search()
    }


}   