const ONE_THOUSAND = 1000
const ONE_MILLION = 1000000
const ONE_BILLION = 1000000000           //         1.000.000.000 (9)
const ONE_TRILLION = 1000000000000       //     1.000.000.000.000 (12)
const ONE_QUADRILLION = 1000000000000000 // 1.000.000.000.000.000 (15)
const MAX = 9007199254740992             // 9.007.199.254.740.992 (15)
/*
const isFinite = (value) => {
    return !(typeof value !== 'number' || value !== value || value === Infinity || value === -Infinity)
}
*/
const isSafeNumber = (value) => {
    return typeof value === 'number' && Math.abs(value) < MAX
}

const ChuSo = [" không", " một", " hai", " ba", " bốn", " năm", " sáu", " bảy", " tám", " chín"]

function DocSo3ChuSo(so) {
    if (so == 0) return ""
    let tram, chuc, donvi, kq
    tram = parseInt(so / 100)
    chuc = parseInt((so % 100) / 10)
    donvi = so % 10
    kq = ChuSo[tram] + " trăm"
    if (chuc == 0 && donvi != 0) kq += " linh"
    //chuc
    if (chuc == 1) kq += " mười"
    else if (chuc != 0) kq += ChuSo[chuc] + " mươi"
    //donvi
    switch (donvi) {
        case 1:
            if (chuc != 0 && chuc != 1) kq += " mốt"
            else kq += ChuSo[donvi]
            break
        case 4:
            if (chuc != 0 && chuc != 1) kq += " tư"
            else kq += ChuSo[donvi]
            break
        case 5:
            if (chuc == 0) kq += ChuSo[donvi]
            else kq += " lăm"
            break
        default:
            if (donvi != 0) kq += ChuSo[donvi]
            break
    }
    return kq
}


function n2w(number) {
    let remainder, word, words = arguments[1]
    if (!number) return !words ? 'không' : words.join('').replace(/,$/, '')
    if (!words) words = []
    if (number < ONE_THOUSAND) {
        remainder = 0
        word = DocSo3ChuSo(number)
    } else if (number < ONE_MILLION) {
        remainder = number % ONE_THOUSAND
        word = DocSo3ChuSo(Math.floor(number / ONE_THOUSAND)) + ' nghìn'
    } else if (number < ONE_BILLION) {
        remainder = number % ONE_MILLION
        word = DocSo3ChuSo(Math.floor(number / ONE_MILLION)) + ' triệu'
    } else if (number < ONE_TRILLION) {
        remainder = number % ONE_BILLION
        word = DocSo3ChuSo(Math.floor(number / ONE_BILLION)) + ' tỷ'
    } else if (number < ONE_QUADRILLION) {
        remainder = number % ONE_TRILLION
        word = DocSo3ChuSo(Math.floor(number / ONE_TRILLION)) + ' nghìn tỷ'
    } else if (number <= MAX) {
        remainder = number % ONE_QUADRILLION
        word = DocSo3ChuSo(Math.floor(number / ONE_QUADRILLION)) + ' triệu tỷ'
    }
    words.push(word)
    return n2w(remainder, words)
}

export function toWords(num) {
    num = parseInt(webix.Number.parse(num, webix.i18n), 10)
    if (!isFinite(num) || !isSafeNumber(num)) {
        webix.message("Số tiền của hóa đơn quá lớn", "error")
        return ""
    }
    let sign = ""
    if (num < 0) {
        num = Math.abs(num)
        sign = "âm "
    }
    let words = n2w(num).trim()
    if (words.startsWith("không trăm")) words = words.slice(10).trim()
    if (words.startsWith("linh")) words = words.slice(4).trim()
    if (sign) words = sign + words
    return words.charAt(0).toUpperCase() + words.slice(1) + " đồng"
}