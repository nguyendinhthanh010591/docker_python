import { JetView } from "webix-jet"
import { filter, size, FUNCNAME_LOGGING,SRC_LOGGING,MESSAGE_STATUS,FNC_LOGGING_EXT, ENT, ROLE } from "models/util"
export default class SerialView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        let form = {
            view: "form",
            id: "syslog:form",
            padding: 5,
            margin: 5,
            elementsConfig: { labelWidth: 130 },
            elements: [
                {
                    cols: [
                        { id: "syslog:fd", name: "fd", label: _("fd"), minWidth: 130, view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr },
                        { id: "syslog:td", name: "td", label: _("td"), minWidth: 130, view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr },
                        { id: "syslog:user", name: "user", label: _("username"), view: "text", placeholder: _("username") },

                        {
                            cols: [
                                { id: "syslog:fnc", name: "type", label: _("action_"), view: "combo", options: FUNCNAME_LOGGING(this.app), suggest: { data: FUNCNAME_LOGGING(this.app), filter: filter } },
                                { id: "syslog:btnsearch", view: "button", type: "icon", icon: "wxi-search", width: 33, tooltip: _("search") },
                                { id: "syslog:xls", view: "button", type: "icon", icon: "wxi-download", width: 33, hidden: (["scb"].includes(ENT)) ? false : true}
                            ]
                        }
                    ]
                },
                {height:(!["fhs","fbc"].includes(ENT)) ? 33 : 1,
                    cols: [
                        //{ view: "combo", id: "syslog:srclog", name: "srclog", label: _("source"), options: SRC_LOGGING(this.app), hidden:true },
                        (!["fhs","fbc"].includes(ENT)) ? { id: "syslog:msg_id", name: "msg_id", label: _("syslog_msg_id_cond"), view: "text", placeholder: _("syslog_msg_id_cond") } : {},
                        (!["fhs","vhc","sgr","fbc"].includes(ENT)) ? { id: "syslog:msg_status", name: "msg_status", label: _("syslog_msg_status_cond"), view: "combo", options: MESSAGE_STATUS(this.app) } : {},
                        {},
                        {
                            cols: [
                                {}, 
                                { id: "syslog:audit", view: "button", type: "icon", icon: "wxi-download",  label: _("audit_log"), width: 85 , hidden: (["scb"].includes(ENT)) ? false : true}
                            ]
                        }

                    ]
                },
                {height:(!["fhs","fbc"].includes(ENT)) ? 33 : 1,
                    cols: [
                        { id: "syslog:r1", name: "r1", label: _("syslog_r1"), view: "text", placeholder: "Mẫu số"},
                        { id: "syslog:r2", name: "r2", label: _("syslog_r2"), view: "text", placeholder: "Kí hiệu"},
                        { id: "syslog:r3", name: "r3", label: _("syslog_r3"), view: "text", placeholder: "Số HĐ"},
                        {
                            cols: [
                        { id: "syslog:r4", name: "r4", label: _("syslog_r4"), view: "text"},
                        //{ id: "syslog:abc", name: "abc", label: "", view: "text",  width:1},
                        //{},
                        {}]
                    }
                    ]
                }
            ],
            rules: {
                fd: webix.rules.isNotEmpty,
                td: webix.rules.isNotEmpty,
                $obj: function (obj) {
                    if (obj.fd > obj.td) {
                        webix.message(_("date_invalid_msg"))
                        return false
                    }
                    return true
                }

            }
        }
        webix.proxy.syslog = {
            $proxy: true,
            load: function (view, params) {
                let obj = $$("syslog:form").getValues()
                Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                if (!params) params = {}
                params.filter = obj
                return webix.ajax(this.source, params)
            }
        }
        // webix.proxy.serial = {
        //     $proxy: true,
        //     load: function (view, params) {
        //         let obj = $$("syslog:form").getValues()
        //         Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
        //         if (!params) params = {}
        //         params.filter = obj
        //         return webix.ajax(this.source, params)
        //     }
        // }

        let pager = { view: "pager", id: "syslog:pager", size: ["fhs","fbc"].includes(ENT) ? 20 : size, template: "{common.first()}{common.prev()}{common.pages()}{common.next()}{common.last()}" }

        let grid = {
            view: "datatable",
            id: "syslog:grid",
            select: "row",
            minWidth: 400,
            multiselect: false,
            resizeColumn: true,
            datafetch: size,
            pager: "syslog:pager",
            // save: "api/serial",
            columns: [
                //{ id: "id", header: { text: "ID", css: "header" }, sort: "server", adjust: true },
                { id: "dt", header: { text: _("date"), css: "header" }, sort: "server", adjust: true },
                //{ id: "fnc_id", header: { text: ("Mã hành động"), css: "header" }, sort: "server", adjust: true },
                { id: "fnc_name", header: { text: _("action_"), css: "header" }, sort: "server", adjust: true },
                //{ id: "fnc_url", header: { text: ("Đường dẫn"), css: "header" }, sort: "server", adjust: true },
                //{ id: "action", header: { text: ("Mã thao tác"), css: "header" }, sort: "server", adjust: true },
                { id: "user_id", header: { text: _("username"), css: "header" }, sort: "server", adjust: true },
                { id: "user_name", header: { text: _("name"), css: "header" }, sort: "server", adjust: true },
                { id: "msg_id", header: { text: _("syslog_msg_id_cond"), css: "header" }, sort: "server",  adjust: true },
                ["dtt"].includes(ENT)?{ id: "msg_status", header: { text: _("syslog_msg_status_cond"), css: "header" }, sort: "server", adjust: true }:{hidden:true},
                { id: "r1", header: { text: _("syslog_r1"), css: "header" }, sort: "server", adjust: true, hidden:true },
                { id: "r2", header: { text: _("syslog_r2"), css: "header" }, sort: "server", adjust: true, hidden:true },
                { id: "r3", header: { text: _("syslog_r3"), css: "header" }, sort: "server", adjust: true, hidden:true },
                { id: "r4", header: { text: _("syslog_r4"), css: "header" }, sort: "server", adjust: true, hidden:true },
                { id: "dtl", header: { text: _("details"), css: "header" }, sort: "server", adjust: true },
                //{ id: "doc", header: { text: _("syslog_doc_content"), css: "header" }, sort: "server", adjust: true },
            ] ,
            onClick: {
            }
        }
        const c3 = {
            rows: [
                pager,
                {
                    cols: [
                        { view: "label", label: (""), css: "bold", align: "right" },
                        //{ view: "button", id: "docx:btndktd", type: "icon", icon: "mdi mdi-content-paste", label: _("serial_btn_tdtt"), disabled: true}
                    ]
                }
            ]
        }
        const title_page = { view: "label", label:`<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("syslog")}</span>`}
        return { paddingX: 1, cols: [{ rows: [title_page,form, grid, c3], gravity: 1.5 }, { view: "resizer" }] }
    }

    // ready(v, urls) {
    //   //Cau hinh rieng cac cot tĩnh
    //   webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
    //       let json = result.json()
    //       setConfStaFld(json)
    //   })
    //   //
    // }
    init() {
        if (!ROLE.includes('PERM_LOG_SYS')) {
            this.show("/top/home")
            window.location.replace(`/${homepage}`)
            return
        }
        const all = { id: "*", value: _("all") }
        webix.ajax("api/sysdate").then(result => {
            let dt = new Date(result.json())
            dt.setHours(0, 0, 0, 0)
            $$("syslog:fd").setValue(new Date(dt.getFullYear(), 0, 1))
            $$("syslog:td").setValue(dt)
            $$("syslog:dt").setValue(dt)
           
            // $$("syslog:dt").getPopup().getBody().define("minDate", dt)
        })
        let grid = $$("syslog:grid"), form = $$("syslog:form"), row
        
        /*
        let j
        for (j=1;j<=4;j++) {
            $$("syslog:r" + j).hide()
            $$("syslog:grid").hideColumn("r" + j)
        }
        */

        const search = () => {
           
            if (form.validate()) {
                grid.clearAll()
                grid.loadNext(size, 0, null, "syslog->api/psyslog", true)
            }
        }
        
        const funcExtFld = (fncid) => {
            let result = FUNCNAME_LOGGING(this.app).find(item => item.id === fncid)
            return result.extfldlbl
        }

        const loaditembyaction = (v) => {
           
            let vf = [], i
            if (["fhs","fbc"].includes(ENT)) {
                //$$("syslog:abc").show()
                $$("syslog:grid").hideColumn("msg_id");
                $$("syslog:grid").hideColumn("msg_status");
                return
            }
            vf = funcExtFld(v)
            if(v=="group_user_insert"|| v=="data_user_insert"|| v=="user_get"||v=="inv_get"||v=="ou_ins"||v=="group_rolemember"||v=="group_del_mbr"){
                $$("syslog:grid").hideColumn("msg_id");
            }else $$("syslog:grid").showColumn("msg_id");

            for (i=1;i<=4;i++) {
                $$("syslog:r" + i).setValue("")
                $$("syslog:r" + i).define("width", 1)
                $$("syslog:r" + i).hide()
                $$("syslog:grid").hideColumn("r" + i);
            }
            
            for (i=1;i<=4;i++) {
                if (i <= vf.length) {
                    $$("syslog:r" + i).define("label", _(vf[i - 1]))
                    $$("syslog:r" + i).define("width", 0)
                    $$("syslog:grid").getColumnConfig("r" + i).header[0].text = _(vf[i - 1])
                    $$("syslog:grid").showColumn("r" + i);
                } else {
                    $$("syslog:r" + i).define("label", "   ")
                    $$("syslog:r" + i).define("width", 1)
                }
                $$("syslog:r" + i).show()
                $$("syslog:r" + i).setValue("")

                // Set gia tri
                
                
                
            }
            // Refresh de hien gtri moi
            $$("syslog:grid").refresh()
            /*
            if (vf.length == 0) {
                $$("syslog:abc").show()
            } else {
                $$("syslog:abc").hide()
            }
            */
        }

        $$("syslog:fnc").setValue("inv_post")
        loaditembyaction("inv_post")

        $$("syslog:btnsearch").attachEvent("onItemClick", () => {
            console.log(1)
            search()
        })
 
        grid.attachEvent("onBeforeLoad", function () {
            this.showOverlay(_("loading"))
        })

        grid.attachEvent("onAfterLoad", function () {
            if (!this.count()) this.showOverlay(_("notfound"))
            else this.hideOverlay()
        })

        $$("syslog:fnc").attachEvent("onChange", (v) => {
            loaditembyaction(v)

        })
        //bắt sự kiện export excel
        $$("syslog:xls").attachEvent("onItemClick", () => {
            webix.message(`${_("export_report")}.......`, "info", -1, "excel_ms");
            let params
            let obj = $$("syslog:form").getValues()
            Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
            if (!params) params = {}
            params.filter = obj
            params.type = 0
            webix.ajax().response("blob").get("api/psyslog/xls", params).then(data => {
                
                webix.html.download(data, `${webix.uid()}.xlsx`)
                $$("syslog:form").enable()
                webix.message.hide("excel_ms");
                $$("syslog:form").hideProgress()
                
            }).catch(e => {

                $$("syslog:form").enable();
                webix.message.hide("excel_ms");
                $$("syslog:form").hideProgress();
                

            })

        })
        $$("syslog:audit").attachEvent("onItemClick", () => {
            webix.message(`${_("export_report")}.......`, "info", -1, "excel_ms");
            let params
            let obj = $$("syslog:form").getValues()
            Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
            if (!params) params = {}
            params.filter = obj
            params.type = 1

            webix.ajax().response("blob").get("api/psyslog/xls", params).then(data => {
                
                webix.html.download(data, `${webix.uid()}.xlsx`)
                $$("syslog:form").enable()
                webix.message.hide("excel_ms");

            })

        })
    }


}   