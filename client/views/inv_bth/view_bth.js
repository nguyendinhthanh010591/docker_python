import { JetView } from "webix-jet"
export default class VIEWBTH extends JetView {
    config() {
        return {
            view: "window",
            id: "viewbth:win",
            modal: true,
            fullscreen: true,
            head: {
                view: "toolbar"
                , height: 35
                , cols: [
                    { view: "label", label: _("bth_view") },
                    {},
                    {
                        cols: [
                            // {
                            //     view: "button", id: "viewbth:download", type: "icon", icon: "mdi mdi-download", tooltip: "Download as pdf file", width: 30, hidden: true, click: () => {
                            //         // if (ROLE.includes('PERM_SEARCH_INV_CONVERT_DOWNLOAD_FILE') || ROLE.includes('PERM_SEARCH_INV_VIEW_DOWNLOAD_FILE')) webix.html.download($$("viewbth:ipdf").getIframe().src, "bth.pdf")
                            //         // else webix.message(_("no_authorization"), "error")
                            //         webix.html.download($$("viewbth:ipdf").getIframe().src, "bth.pdf")
                                    
                            //     }
                            // },
                            { view: "button", id: "viewbth:btnxml", type: "icon", icon: "mdi mdi-file-xml", tooltip: "xml", width: 30, hidden: true, disabled: true },
                            { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => $$("viewbth:win").hide() }
                        ], fillspace: true, align: "right"
                    }
                ]
            },
            body: { id: "viewbth:ipdf", view: "iframe", borderless: true }
        }
    }
    show() {
        this.getRoot().show()
    }
    init() {
        //$$("iwin:ipdf").attachEvent("onAfterLoad", function () { URL.revokeObjectURL(this.src) })
    }
}
