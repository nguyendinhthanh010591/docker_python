import { JetView } from "webix-jet"
const BTH_TYPE_COMBO = [
    { id: "4", value: "Giải trình", valueclient: "n_t_c_explan" },
    { id: "5", value: "Sai sót do tổng hợp", valueclient: "n_t_c_mistake" }
]
const TH_TYPE = [
    { id: "1", value: "Điều chỉnh thông tin", valueclient: "th_type_dctt" },
    { id: "2", value: "Giải trình của CQT", valueclient: "th_type_gt" }
]
const BTH_CQT_STATUS = [
    { id: "0", value: _("cqt_wait_send") },
    { id: 1, value: _("cqt_sent") },
    { id: 2, value: _("cqt_sent_fail") },
    { id: 8, value: _("inv_valid") },
    { id: 9, value: _("inv_invalid") },
    { id: 10, value: _("inv_code_grant") },
    { id: 12, value: _("wno_valid") },
    { id: 13, value: _("wno_invalid") }
]
export default class BTHwin extends JetView {
    config() {
        const downloadMinuteAdj = (id) => {
            $$("bth:win:grid").disable()
            webix.message(`${_("Downloading")}.......`, "info", -1, "downloading")
            webix.ajax().response("blob").get(`api/inv/minutes/listinvadj/${id}`).then(data => {
                webix.html.download(data, `BB_DieuChinh_${id}.docx`)
                $$("bth:win:grid").enable()
                webix.message.hide("downloading");
            }).fail(err => {
                $$("bth:win:grid").enable()
                webix.message.hide("downloading");
            })
        }

        let bthGrid = {
            view: "datatable",
            id: "bth:win:grid",
            editable: true,
            navigation: true,
            editaction: "click",
            resizeColumn: true,
            multiselect: false,
            select: "cell",
            headerRowHeight: 33,
            columns: [
                { id: "line", header: { text: _("stt"), css: "header" }, width: 50, css: "center", adjust: true },
                { id: "delete", header: "", width: 40, css: "center", template: (obj, common) => {
                        if(obj.listinv_id == null) return `<span class='webix_icon wxi-trash' title='${_("delete")}'></span>`
                        else return ""
                    }
                },
                { id: "minutes", header: "", width: 40, css: "center", template: (obj, common) => {
                        if(obj.th_type == 1) return `<span class='mdi mdi-download' title='${_("minutes_download_5")}'></span>`
                        else return ""
                    }
                },
                { id: "id", header: { text: "ID", css: "header" }, css: "right",  adjust: true },
                { id: "inv_id", header: { text: _("inv_id"), css: "header" }, css: "right", adjust: true },
                { id: "form", header: { text: _("form"), css: "header" },  adjust: true },
                { id: "serial", header: { text: _("serial"), css: "header" },  adjust: true },
                { id: "seq", header: { text: _("seq"), css: "header" }, adjust: true },
                { id: "stax", header: { text: _("taxcode_invoice"), css: "header" },  adjust: true },
                { id: "so_tb", header: { text: _("adj.ref_tip"), css: "header" },  adjust: true, editor: "text" },
                { id: "ngay_tb", header: { text: _("adj.rdt_tip"), css: "header" }, adjust: true, format: webix.i18n.dateFormatStr, editor: "date" },
                // { id: "so_tb", header: { text: _("notice_number"), css: "header" },  adjust: true, editor: "text" },
                // { id: "ngay_tb", header: { text: _("notice_date"), css: "header" }, adjust: true, format: webix.i18n.dateFormatStr, editor: "date" },
                { id: "th_type", header: { text: _("th_type"), css: "header" }, adjust: true, collection: TH_TYPE, editor: "combo", width: 200 },
                { id: "noti_type", header: { text: _("noti_type"), css: "header" }, adjust: true, collection: BTH_TYPE_COMBO, editor: "combo" },
                { id: "note", header: { text: _("rea"), css: "header" }, adjust: true, editor: "text", fillspace: true },
                { id: "listinv_id", header: { text: "ID_BTH", css: "header" }, css: "right",  adjust: true },
                { id: "statuscqt", header: { text: _("status_cqt_short"), css: "header" }, adjust: true, collection: BTH_CQT_STATUS},
                { id: "msgcqt", header: { text: _("error_van_message"), css: "header" }, adjust: true, fillspace: true }
            ],
            rules: {
                id: webix.rules.isNotEmpty,
                form: webix.rules.isNotEmpty,
                serial: webix.rules.isNotEmpty,
                stax: webix.rules.isNotEmpty,
                th_type: val => val && !isNaN(val),
                noti_type: val => val && !isNaN(val)
            },
            onClick: {
                "mdi-download": function (e, r) {
                    downloadMinuteAdj(r.row)
                },
                "wxi-trash" : function (e, id) {
                    let item = $$("bth:win:grid").getItem(id)
                    let body = {id: item.id, invid: item.inv_id}
                    webix.confirm(`${_("del_confirm_msg")}`, "confirm-warning").then(() => {
                    webix.ajax().post("api/bth/delinvadj", body).then(result => {
                        let json = result.json()
                            if( json.status == 1) {
                                webix.message(_("del_success"), "success")
                                $$("bth:win:grid").remove(id)
                            }
                        })
                    })
                }
            },
            on: {
                onStoreUpdated: (id, obj, mode) => {
                    this.data.each((row, i) => {
                        row.line = i + 1
                    })
                },
                onAfterEditStart: function (cell) {
                    let _cellName = cell.column, _maxLength
                    switch (_cellName) {
                        case "form":
                            _maxLength = 11
                            break;
                        case "serial":
                            _maxLength = 8
                            break;
                        case "rea":
                            _maxLength = 255
                            break;
                        default:
                            break;
                    }
                    if (_maxLength) this.getEditor().getInputNode().setAttribute("maxlength", _maxLength)
                },
                onBeforeEditStop: function(state, editor, ignoreUpdate){
                    if (editor.column == "th_type") {
                        let idex_so = this.getColumnIndex("so_tb")
                        let index_ngay = this.getColumnIndex("ngay_tb")
                        if (state.value == 2) {
                            this.config.columns.splice(idex_so,1)
                            this.config.columns.splice(idex_so,0,{ id: "so_tb", header: { text: _("notice_number"), css: "header" },  adjust: true, editor: "text" })
                            this.config.columns.splice(index_ngay,1)
                            this.config.columns.splice(index_ngay,0,{ id: "ngay_tb", header: { text: _("notice_date"), css: "header" }, adjust: true, format: webix.i18n.dateFormatStr, editor: "date" })
                        }
                        else {
                            this.config.columns.splice(idex_so,1)
                            this.config.columns.splice(idex_so,0,{ id: "so_tb", header: { text: _("adj.ref_tip"), css: "header" },  adjust: true, editor: "text" })
                            this.config.columns.splice(index_ngay,1)
                            this.config.columns.splice(index_ngay,0,{ id: "ngay_tb", header: { text: _("adj.rdt_tip"), css: "header" }, adjust: true, format: webix.i18n.dateFormatStr, editor: "date" })
                        }
                        this.refreshColumns()
                    }
                }
            }
        }

        let bthbuttonarea = {
            cols: [
                {},
                { view: "button", id: "win:btnbthsave", type: "icon", icon: "mdi mdi-content-save", label: _("save"), width: 125 }
            ]
        }
        return {
            view: "window",
            id: "bthwin",
            fullscreen: true,
            head: {
                view: "toolbar"
                , height: 35
                , cols: [
                    { view: "label", label: _("bthwinlabel") },
                    { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => this.close() }
                ]
            },
            body: {
                rows: [
                    bthGrid,
                    bthbuttonarea,
                ]
            }
        }
    }
    initBtnsav2sendnListener(callback) {
        $$("win:btnbthsave").attachEvent("onItemClick", () => {
            if (this.validateAll()) {
                let _data = this.createData()
                for ( let item of _data.items) {
                    if(item.th_type == 1 && item.noti_type == 5) {
                        webix.message(_("noti_type_error"), "error")
                        return null
                    }
                }
                webix.confirm(_("bth_confirm"), "confirm-warning").then(() => {
                    webix.ajax().post("api/bth/sendInvAdj", _data).then(result => {
                        const obj = result.json()
                        if (obj.status) {
                            webix.message(_("saved"), "success")
                            this.cleanInput()
                            this.hide()
                            callback()
                        }
                    })
                })
           } else {
                webix.message(_("pls_check_bth"), "error")
                return null
            }
        })
    }
    init() {
        this.initBtnsav2sendnListener()
        $$("bth:win:grid").attachEvent("onAfterLoad", function () {
            this.refreshColumns()
        })
    }
    cleanInput() {
        let _grid = $$("bth:win:grid")
        _grid.clearAll()
    }
    show(data, callback) {
        let bthw = $$("bth:win:grid")
        if (data) {
            bthw.clearAll()
            if (data.viewCQT) {
                bthw.showColumn("minutes")
                bthw.showColumn("delete")
                bthw.showColumn("id")
                bthw.showColumn("listinv_id")
                bthw.showColumn("statuscqt")
                bthw.showColumn("msgcqt")
                bthw.config.editable = false
                $$("win:btnbthsave").disable()
            }
            else {
                bthw.hideColumn("minutes")
                bthw.hideColumn("delete")
                bthw.hideColumn("id")
                bthw.hideColumn("listinv_id")
                bthw.hideColumn("statuscqt")
                bthw.hideColumn("msgcqt")
                bthw.config.editable = true
                $$("win:btnbthsave").enable()
            }
            if (data.grid && bthw.serialize().length == 0) {
                let _icount = 0
                if (!Array.isArray(data.grid)) data.grid = [data.grid]
                data.grid.forEach(e => {
                    e.line = ++_icount
                    if(e.ngay_tb) e.ngay_tb = new Date(e.ngay_tb)
                    if(!e.th_type) e.th_type = 1
                    if(!e.inv_adj_type) e.noti_type = 4
                    else e.noti_type = e.inv_adj_type
                    if (data.insertnew) e.insertnew = true
                    if (!data.viewCQT) e.inv_id = e.id
                    let idex_so = bthw.getColumnIndex("so_tb")
                    let index_ngay = bthw.getColumnIndex("ngay_tb")
                    if (e.th_type == 2) {
                        bthw.config.columns.splice(idex_so,1)
                        bthw.config.columns.splice(idex_so,0,{ id: "so_tb", header: { text: _("notice_number"), css: "header" },  adjust: true, editor: "text" })
                        bthw.config.columns.splice(index_ngay,1)
                        bthw.config.columns.splice(index_ngay,0,{ id: "ngay_tb", header: { text: _("notice_date"), css: "header" }, adjust: true, format: webix.i18n.dateFormatStr, editor: "date" })
                    }
                    else {
                        bthw.config.columns.splice(idex_so,1)
                        bthw.config.columns.splice(idex_so,0,{ id: "so_tb", header: { text: _("adj.ref_tip"), css: "header" },  adjust: true, editor: "text" })
                        bthw.config.columns.splice(index_ngay,1)
                        bthw.config.columns.splice(index_ngay,0,{ id: "ngay_tb", header: { text: _("adj.rdt_tip"), css: "header" }, adjust: true, format: webix.i18n.dateFormatStr, editor: "date" })
                    }
                })
                $$("bth:win:grid").refreshColumns()
                $$("bth:win:grid").parse(data.grid)
            }
        }
        if (callback) this.initBtnsav2sendnListener(callback)
        const win = this.getRoot()
        win.show()
    }
    hide() {
        const win = this.getRoot()
        win.hide()
    }
    close(){
        this.cleanInput()
        $$("bth:win:grid").clearAll()
        $$("bth:win:grid").refreshColumns()
        this.hide()
    }
    isVisible() {
        const win = this.getRoot()
        return win.isVisible()
    }
    createData() {
        let bthGrid = $$("bth:win:grid")
        return {
            items: bthGrid.serialize()
        }
    }
    validateAll() {
        let _grid = $$("bth:win:grid")
        _grid.editStop()
        let isGridOK = _grid.validate()
        return (isGridOK)
    }
}
