import { JetView } from "webix-jet"
import { size, ISTATUSBTH,INVTYPE,GOODSTYPE,WN_CRE_WIN_BTNSAV_PURPOSE,BTH_CQT_STATUS,ROLE, t2s, RPP } from "models/util"
import VIEWBTH from "views/inv_bth/view_bth"
import form_bth from "views/inv_bth/form_bth"
const format = require("xml-formatter")
let b64
const viewbth = (id, status) => {
    const grid = $$("bthSearch:dtReport")
    grid.showProgress()
    webix.delay(() => {
        webix.ajax(`api/bth/htm/${id}`).then(result => {
            
            let pdf = result.json()
            let lengthPDF = pdf.sizePDF
            console.log(lengthPDF)
            if (lengthPDF >= 3145728) webix.message(String(_("file_pdf_size_limit")).replace("#hunglq#", 3145728 / (1024 * 1024)))
            console.log('begin view')
            b64 = pdf.pdf
            const blob = dataURItoBlob(b64);

            var temp_url = window.URL.createObjectURL(blob);
            if(status == 1){
                $$("viewbth:btnxml").show()
                $$("viewbth:btnxml").enable()
            }else{
                $$("viewbth:btnxml").hide()
            }

            $$("viewbth:ipdf").define("src", temp_url)
            $$("viewbth:win").show()
            webix.message.hide("exp");
            grid.hideProgress()
        }) 
    })
}

export default class ftvanSearchView extends JetView {
    config() {
        webix.editors.$popup = {
            text: {
                view: "popup",
                body: { view: "textarea", width: 450, height: 250 }
            }
        }
        webix.proxy.sbthdata = {
            $proxy: true,
            load: function (view, params) {
                let obj = $$("bthSearch:form").getValues()
                Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                if (!params) params = obj
                return webix.ajax(this.source, params)
            }
        }
        const edit = (rec) => {
            let mess = _("bth_edit_confirm").replace("#vinhq12",rec.listinvoice_num)
            webix.confirm(mess, "confirm-warning").then(() => {
                console.log(rec)
                this.form_bth.edit({
                    updategrid: true,
                    purpose: WN_CRE_WIN_BTNSAV_PURPOSE.SEND_NOW,
                    rec: rec
                })
            })
        }
        const search = {
            view: "form",
            id: "bthSearch:form",
            padding: 8, margin: 8,
            elementsConfig: { labelWidth: 120 },
            elements: [ 
                    {
                        cols: [
                            { id: "bthSearch:id", name: "id", label: ("ID"), view: "text" },
                            { id: "bthSearch:fd", name: "fd", label: _("fd"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr, required: true },
                            { id: "bthSearch:td", name: "td", label: _("td"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr, required: true },
                            {
                                id: "bthSearch:statusBth", name: "statusBth", view: "combo",
                                options:ISTATUSBTH(this.app), label: _("status"), type: 'text',
    
                            },
                            { id: 'bthSearch:numbth', name: 'listinvoice_num', view: 'text', label: _("num_bth"), type: 'text', attributes: { maxlength: 14 } },
                            { id: "bthSearch:status_received", name: "status_received", label: _("CQT_STATUS"), view: "combo", options: BTH_CQT_STATUS(this.app), value: "*"},
                            { view: "button", width: 35, type: "icon", icon: "wxi-search", value: "click", label: _("search"), width: 100, hotkey: "enter", id: "bthSearch:search", tooltip: _("search") },
                          //  { view: "button", width: 35, type: "icon", icon: "wxi-download", value: "click", label: _("export"), width: 100, hotkey: "enter", id: "bth:export", tooltip: _("export") },
    
                        ],
                    },
                    {
                        cols: [
                            { id: "bthSearch:curr", name: "curr", label: _("currency"), view: "combo", suggest: { url: "api/cat/kache/currency" }, value:"*"},
                            { id: "bthSearch:period", name: "period", label: _("home_period"), view: "text"},
                            {},
                            {},
                            {},
                            {},
                            {width: 100}
                        ]
                    }
            ],
            rules: {
                $obj: data => {
                    if (data.numRows < 1) {
                        this.webix.message({ text: _("number_record_msg"), type: 'error' })
                        return false
                    }
                    return true
                }
            }
        }
        let pager = { view: "pager", width: 340, id: "bthSearch:pager", size: size, template: `{common.first()}{common.prev()} {common.pages()}{common.next()}{common.last()}` }
        let recpager = { view: "combo", id: "recpager", maxWidth: 70, tooltip: _("row_per_page"), value: 10, options: RPP }
        
        const report = {
            id: 'bthSearch:dtReport',
            view: "datatable",
            //select: "row",
            //hover: "hover",
            tooltip: true,
            select: true,
            pager: "bthSearch:pager",
            resizeColumn: true,
            fillspace:true,
            scroll: true,
            
            columns: [
                {  header: { text: ROLE.includes('PERM_SEARCH_BTH_EXCEL') ?"<span class='webix_icon wxi-download' title='excel'></span>":'', css: "header" }, width: 35, template:ROLE.includes('PERM_SEARCH_BTH_VIEW') ? `<span class='webix_icon wxi-search', title='${_("view")}'></span>`:'' },
                // {
                //     id: "sel", header: "", width: 35, template: (obj, common) => {
                //         if (obj.status == 0) {
                //             return `<span class='webix_icon wxi-pencil', title='${_("edit_bth")}'></span>`
                //         }
                //         else { return "" }
                //     }
                // },
                { header: "", width: 35, template:`<span class='webix_icon wxi-download', title='${_("download_excel_bth")}'></span>` },
                { id: "id", header: { text: "ID", css: "header" }, adjust: true, width: 80 },
                { id: "created_date", header: { text: _("created_date"), css: "header" },adjust: true, width: 150, exportType: "string", },
                { id: "listinvoice_num", header: { text: _("num_list_inv"), css: "header", }, adjust: true,width: 150, exportType: "string" ,},
                { id: "status", header: { text: _("status"), css: "header" },options:ISTATUSBTH(this.app), adjust: true, width: 120},
                { id: "status_received", header: { text: _("status_cqt"), css: "header" }, collection: BTH_CQT_STATUS(this.app), adjust: true },
                { id: "s_name", header: { text: _("sname"), css: "header" }, adjust: true, width: 150,},
                { id: "s_tax", header: { text: _("wn_stax"), css: "header" },  adjust: true, width: 150 },
               // { id: "inv_printed", header: { text: _("inv_printed"), css: "header"},options:GOODSTYPE(this.app),adjust: true, width:150},
                { id: "item_type", header: { text: _("item_type"), css: "header" },options:INVTYPE(this.app), adjust: true,  width: 150 },
                { id: "period_type", header: { text: _("period_type"), css: "header" }, adjust: true,  width: 150 },
                { id: "period", header: { text: _("period"), css: "header" }, adjust: true,width: 150 },
                { id: "curr", header: { text: _("currency"), css: "header" }, adjust: true,width: 150 },
                { id: "times", header: { text: _("times"), css: "header",}, adjust: true, width:150 },
                { id: "error_msg_van", header: { text: _("error_msg_van"), css: "header",}, adjust: true, width: 150, maxWidth: 200, tooltip: "#error_msg_van#"},
            ],
            onClick:{
                "wxi-download": function (e,r) {
                    let id =  r.row
                    if (id) {
                        webix.ajax().response("blob").get(`api/bth/excel/custom/${id}`).then(data => webix.html.download(data, `BTH_excel.xlsx`))
                    }
                    else {
                        const form = $$("bthSearch:form")            
                        if (!form.validate()) return
                        webix.message(_("exporting_report"))
                        let params = {}
                        let obj = $$("bthSearch:form").getValues()
                        obj.operation = "export"
                        Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                        params = obj
                        webix.ajax("api/bth/sbthdata", params).then(result => {
                        
                            const json = result.json()
                            console.log(json)
                            
                            json.data = json.data.map(el => {                       
                                let arr = [
                                    { id: "*", value: _("all") },
                                    { id: "0", value: _("pending_bth") },
                                    { id: "1", value: _("approved_bth") },
                                    { id: "4", value: _("cancel_bth") }
                                ]
                                let arr2 = [
                                    { id: "9", value: _("other") },
                                    { id: "1", value: _("petrol") },
                                    { id: "2", value: _("air_transport") }
                                ]
                                let arr3 = [
                                    { id: "0", value:  _("cqt_wait_send") },
                                    { id: "1", value:  _("cqt_sent") },
                                    { id: "2", value:  _("cqt_sent_fail") },
                                    { id: "12", value:  _("wno_valid") },
                                    { id: "131", value:  _("process_bth") },
                                    { id: "13", value:  _("wno_invalid") }
                                ]
                                el.status = arr.find(e => e.id == el.status).value
                                el.item_type = arr2.find(e => e.id == el.item_type).value
                                el.status_received = el.status_received ? arr3.find(e => e.id == el.status_received).value : el.status_received
                                return el
                            })  
                            console.log(json.data)
            
                            let data = new webix.DataCollection({ data: json.data }), columns = []
                            console.log(data)
            
                            $$("bthSearch:dtReport").config.columns.forEach(e => {
                                let obj = {}
                                obj.id = e.id
                                obj.header = e.header
                                obj.exportType = "string"
                                columns.push(obj)
                                return e
                            })
                            webix.toExcel(data, { columns: columns, filename: "DSTraCuuBangTongHop.xlsx" }).then(() => { data.destructor() })
                        })
                    }
                },
                "wxi-search": function (e, r) {
                    this.select(r)
                    this.getItem(r)
                    let row = this.getItem(r)
               
                    viewbth(r.row, row.status)
                },
                "wxi-pencil": function(e, id) {
                    const rec = this.getItem(id)
                    edit(rec)
                }
            }
        }
        const button123 = {
            cols: [
                    { view: "button", id: "bth:btnhistory", type: "icon", popup: "hscqtbth", icon: "mdi mdi-content-paste", label: _("history_cqt"), disabled: true/*, hidden: ROLE.includes(4) ? false : true */, width: 130, },
            //     { view: "button", id: "bth:btntbph", type: "icon", icon: "webix_icon_btn mdi mdi-file-pdf-box-outline", label: _("view"),id: "bth:view", width: 100 },
                    { view: "button", id: "bth:btnsendback", type: "icon", icon: "mdi mdi-email-plus-outline", label: _("sendback_cqt"), disabled: true, width: 100,hidden:true },
                    { view: "button", id: "bth:btndellbth", type: "icon", icon: "webix_icon wxi-trash", label: _("delete"), width: 100,disabled: true }
            //     { view: "button", width: 35, type: "icon", icon: "wxi-download", value: "click", label: _("export"), width: 100, hotkey: "enter", id: "bth:export", tooltip: _("export") },
             ]
        }
        let title_page = {
            view: "label", label: `<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("search_bth")}</span>`
        }
        return {
            rows: [
                title_page,
                search,
                report,
                {cols: [recpager, pager, { id: "bth:count", view: "label" },{}, button123]},
            ]
        }
    }
    init() {
      //  !ROLE.includes(9) ? window.location.replace("#/top/home") : null
        this.VIEWBTH = this.ui(VIEWBTH)
        this.form_bth = this.ui(form_bth)
        const grid = $$("bthSearch:dtReport")
        const form = $$("bthSearch:form")
        const td = $$("bthSearch:td")
        const fd = $$("bthSearch:fd")
        const statusbth = $$("bthSearch:statusBth")
        const hscqtbth = $$("bth:hscqt")
        const today = new Date(), yr = today.getFullYear(), mt = today.getMonth(), std = new Date(yr, mt, today.getDate()), sfd = new Date(yr, mt - 1, 1)
        let id
        td.setValue(std)
        fd.setValue(sfd)
        statusbth.setValue("*")
        const all = { id: "*", value: _("all") }	
        $$("bthSearch:curr").getList().add(all, 0)
        $$("bthSearch:status_received").getList().add(all, 0)
        $$("bthSearch:status_received").setValue("*")
        if (ROLE.includes('PERM_SEARCH_BTH_SENDBACK')){
            $$("bth:btnsendback").show()
        }
        $$("bthSearch:search").attachEvent("onItemClick", function () {
            let valid = validate()
            if (!valid) {
                grid.clearAll()
                grid.loadNext(size, 0, null, "sbthdata->api/bth/sbthdata", true)
            }
        })
        $$("recpager").attachEvent("onChange", () => {
            $$("bthSearch:pager").config.size = $$("recpager").getValue()
            if (form.validate()) {
                let valid = validate()
                if (!valid) {
                    grid.clearAll()
                    grid.loadNext(size, 0, null, "sbthdata->api/bth/sbthdata", true)
                }
            }
        })
        webix.extend(grid, webix.ProgressBar)
        grid.attachEvent("onBeforeLoad", function () {
            this.showOverlay(_("loading"))
        })

        grid.attachEvent("onAfterLoad", function () {
            if (this.count()) this.hideOverlay()
            else this.showOverlay(_("notfound"))
            let self = this, count = self.count()
            $$("bth:count").setValue(`${_("found")} ${count}`)
        })
        grid.attachEvent("onAfterSelect", () => {
            let row = grid.getSelectedItem()
            id = row.id
            
            if((row.status == 0 || row.status_received==13) && ROLE.includes('PERM_SEARCH_BTH_DELETE'))  $$("bth:btndellbth").enable()
            else $$("bth:btndellbth").disable()
            $$("bth:btnhistory").enable()
            if(row.status == 1 && row.status_received== 0){
                $$("bth:btnsendback").enable()
            }
            else{
                $$("bth:btnsendback").disable()

            }
        })
        grid.attachEvent("onAfterUnSelect", () => {
            $$("bth:btnhistory").disable()
            $$("bth:btnsendback").disable()
        })
        $$("bth:btndellbth").attachEvent("onItemClick", function () {
            webix.confirm(_("del_confirm_msg"), "confirm-warning").then(() => {
            let params = {id: id}
            webix.ajax().post("api/bth/dell", params).then(result => {
                grid.clearAll()
                grid.loadNext(size, 0, null, "sbthdata->api/bth/sbthdata", true)
            })
        })
        })
        $$("bth:btnsendback").attachEvent("onItemClick", () => {
            webix.confirm(_("sendb_bth_confirm"), "confirm-warning").then(() => {
                webix.ajax().post(`api/bth/sendback/${id}`).then(result => {
                    let json = result.json()
                    console.log("json:",json.result);
                    if (json.result=1){
                        webix.message(_("sent"), "success")
                    }else {
                        webix.message(_("cqt_sent_fail"), "error")
                    }

                })
                grid.clearAll()
                grid.loadNext(size, 0, null, "sbthdata->api/bth/sbthdata", true)
            })
        })
        $$("viewbth:btnxml").attachEvent("onItemClick", () => {
            webix.ajax(`api/bth/xml/${id}`).then(result => {
                const json = result.json()
                const blob = new Blob([json.xml], { type: "text/xml;charset=utf-8" })
                saveAs(blob)
            })
        })
        function validate() {
            const param = form.getValues()
            let datafd = param.fd
            let datatd = param.td
            var date1 = new Date(datafd);
            var date2 = new Date(datatd);
            var Difference_In_Time = date2.getTime() - date1.getTime();
            var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
            let valid = false
            if (Difference_In_Days > 90) {
                webix.message(_("dt_search_between_month"))
                return true
            } else if (Difference_In_Days < 0) {
                webix.message(_("date_invalid_msg"))
                return true
            }
            return valid
        }

        $$("bth:btnhistory").attachEvent("onItemClick", () => {
            let row = grid.getSelectedItem()
            hscqtbth.clearAll()
            $$("bth:hscqt").showOverlay(_("loading"))
            webix.ajax(`api/bth/hscqt/${row.id}`).then(result => {
                let json = result.json()
                console.log(json)
                if (json.length) {
                    $$("bth:hscqt").hideOverlay()
                    json.sort((a, b)=> a.id>=b.id) // sort log  từ cũ đến mới
                    hscqtbth.parse(json)
                }
                else {
                    $$("bth:hscqt").showOverlay(_("notfound"))
                }
            })
        })
        function exportLog() {
            if (!form.validate()) return
            webix.message(_("exporting_report"))
            let params = {}
            let obj = $$("bthSearch:form").getValues()
            obj.operation = "export"
            Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
            params = obj
            webix.ajax("api/bth/sbthdata", params).then(result => {
            
                const json = result.json()
                
                json.data = json.data.map(el => {                       
                    el.status = ISTATUSBTH(this.app).find(e => e.id == el.status).value
                    el.inv_printed = GOODSTYPE(this.app).find(e => e.id == el.inv_printed).value
                     el.item_type = INVTYPE(this.app).find(e => e.id == el.item_type).value                    
                    return el
                })  
                console.log(json.data)
                
                console.log('hihi')

                let data = new webix.DataCollection({ data: json.data }), columns = []
                console.log(data)

                $$("bthSearch:dtReport").config.columns.forEach(e => {
                    let obj = {}
                    obj.id = e.id
                    obj.header = e.header
                    obj.exportType = "string"
                    columns.push(obj)
                    return e
                })
                webix.toExcel(data, { columns: columns, filename: "DSTraCuuBangTongHop" }).then(() => { data.destructor() })
            })
        }
        

    
    }


    ready() {

    }

}

webix.ui({

    view: "popup",
    id: "viewXml",
    width: 900,
    height: 700,
    margin: 50,
    body: { 
        view:"textarea",id:"bth:viewXml", 
        // value:text,
        height:900 
    }
});

webix.ui({

    view: "popup",
    id: "hscqtbth",
    width: 950,
    height: 400,
    margin: 50,

    body: {
        view: "datatable",
        id: "bth:hscqt",
        // select: "row",
        columns: [
            // { id: "id_ref", header: { text: _("id_ref"), css: "header" } },
           
            { id: "datetime_trans", header: { type: "datetime", text: _("time"), css: "header" }, adjust: true }, //format: t2s
            //{ id: "status", header: { text: _("status_cqt"), css: "header" }, collection: STATEMENT_CQT_STATUS(this.app), adjust: true },
            { id: "description", header: { text: _("description"), css: "header" }, fillspace: true },
            { header: { text:  _("Message_gdt"), css: "header" }, adjust: true, popup: "viewXml", 
                template: obj => {  
                    let _result = "";
                    let r_xml = obj.r_xml;
                    if ( r_xml) {
                        _result += `<span class='webix_icon wxi-eye', title='${_("view")}'></span>` ;
                    } 
                    return _result;
                }
            }
            // { id: "", header: { text: "", css: "header" }, fillspace: true },
        ],
        onClick:
        {
            "wxi-eye": function (e, r) {
                // viewXml.clearAll()
                // viewXML(r, 1000, 450)
                webix.ajax(`api/bth/viewXml/${r.row}`).then(result => {
                    let json = result.json()
                    if (json.length) {
                        var xmlPretty = ''
                        if(json[0].r_xml != null){
                            xmlPretty = format(json[0].r_xml, {
                                indentation: '\t', 
                                filter: (node) => node.type !== 'Comment', 
                                collapseContent: true, 
                                lineSeparator: '\n'
                            });
                        }
                        $$("bth:viewXml").setValue(xmlPretty)
                        var left = (screen.width/2)-(1000/2)
                        var top = (screen.height/2)-(800/2)
                        $$("bth:viewXml").show({
                            x:left, // left offset from the right side
                            y:top // top offset
                        });
                    }
                })
            }
        }
    }
});