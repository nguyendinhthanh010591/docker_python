import { JetView } from "webix-jet"
import { size, ISTATUSBTH, INVTYPE, expire,ROLE } from "models/util"
import VIEWBTH from "views/inv_bth/view_bth"
let b64
const viewbth = (id) => {
    const grid = $$("bthSearch:dtReport")
    grid.showProgress()
    webix.delay(() => {
        webix.ajax(`api/bth/htm/${id}`).then(result => {
            
            let pdf = result.json()
            let lengthPDF = pdf.sizePDF
            console.log(lengthPDF)
            if (lengthPDF >= 3145728) webix.message(String(_("file_pdf_size_limit")).replace("#hunglq#", 3145728 / (1024 * 1024)))
            b64 = pdf.pdf
            const blob = dataURItoBlob(b64);

            var temp_url = window.URL.createObjectURL(blob);

            $$("viewbth:ipdf").define("src", temp_url)
            $$("viewbth:win").show()
            webix.message.hide("exp");
            grid.hideProgress()
          
        }) 
    })
}
export default class ftvanSearchView extends JetView {
    config() {
        webix.editors.$popup = {
            text: {
                view: "popup",
                body: { view: "textarea", width: 450, height: 250 }
            }
        };
        webix.proxy.sbthdata = {
            $proxy: true,
            load: function (view, params) {
                let obj = $$("bthSearch:form").getValues()
                Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                if (!params) params = obj
                return webix.ajax(this.source, params)
            }
        }
        const search = {
            view: "form",
            id: "bthSearch:form",
            padding: 8, margin: 8,
            elementsConfig: { labelWidth: 120 },
            elements: [
                {
                    cols: [
                        { id: "bthSearch:fd", name: "fd", label: _("fd"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr, required: true },
                        { id: "bthSearch:td", name: "td", label: _("td"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr, required: true },
                        // {
                        //     id: "bthSearch:statusBth", name: "status", view: "combo",
                        //     options: [
                        //         // { id: "*", value: _("all") },
                        //         { id: "0", value: _("pending_bth") },
                        //          { id: "1", value: _("approved_bth") },
                        //     ], ISTATUSBTH, label: _("status"), type: 'text',

                        // },
                        //{ id: 'bthSearch:numbth', name: 'listinvoice_num', view: 'text', label: _("num_bth"), type: 'text', attributes: { maxlength: 14 } },
                        { view: "button", type: "icon", icon: "wxi-check", label: _("approve"), width: 70,id: "bthSearch:approval", disabled: ROLE.includes('PERM_APPROVE_BTH_APPR') ? false : true },
                        { view: "button", width: 35, type: "icon", icon: "wxi-search", value: "click", label: _("search"), width: 100, hotkey: "enter", id: "bthSearch:search", tooltip: _("search") },
                    ]

                },

            ],
            rules: {
                $obj: data => {
                    if (data.numRows < 1) {
                        this.webix.message({ text: _("number_record_msg"), type: 'error' })
                        return false
                    }
                    return true
                }
            }
        }
        let pager = { view: "pager", id: "pager", size: size, template: "{common.first()}{common.prev()}{common.pages()}{common.next()}{common.last()}" }

        const report = {
            id: 'bthSearch:dtReport',
            view: "datatable",
            //select: "row",
            //hover: "hover",
            //tooltip: true,
            select: true,
            pager: "pager",
            resizeColumn: true,
            fillspace: true,
            scroll: true,

            columns: [
                { width: 35, template: ROLE.includes('PERM_APPROVE_BTH_VIEW') ? `<span class='webix_icon wxi-search', title='${_("view")}'></span>` :''},
                { id: "chk", header: { content: "masterCheckbox", css: "center" }, css: "center", template: "{common.checkbox()}", width: 45 },
                { id: "id", header: { text: "ID", css: "header" }, adjust: true, width: 80, },
                { id: "created_date", header: { text: _("created_date"), css: "header" }, fillspace: true, adjust: true, width: 150, exportType: "string", },
                { id: "listinvoice_num", header: { text: _("num_list_inv"), css: "header", }, fillspace: true, adjust: true, width: 150, exportType: "string", },
                {
                    id: "status", header: { text: _("status"), css: "header" }, options: [
                        { id: "0", value: _("pending_bth") },
                        { id: "1", value: _("approved_bth") },
                    ], adjust: true, fillspace: true, width: 120
                },
                { id: "s_name", header: { text: _("sname"), css: "header" }, fillspace: true, adjust: true, width: 150, },
                { id: "s_tax", header: { text: _("wn_stax"), css: "header" }, fillspace: true, adjust: true, width: 150 },
               // { id: "inv_printed", header: { text: _("inv_printed"), css: "header" }, value: _("electronic_inv"), adjust: true, width: 150, hidden: true },
                { id: "item_type", header: { text: _("item_type"), css: "header" }, options: INVTYPE(this.app), adjust: true, width: 150 },
                { id: "period_type", header: { text: _("period_type"), css: "header" }, adjust: true, width: 150 },
                { id: "period", header: { text: _("period"), css: "header" }, adjust: true, width: 150 },
                { id: "f_times", header: { text: _("times"), css: "header", }, adjust: true, width: 150, },
                {}
            ],
            on:{
                onItemClick: function (id, colId, value) {
                    let row = id.row, item = this.getItem(row)
                    item.chk = !item.chk
                    this.refresh(row)
                }
            },
            onClick:{
               "wxi-search": function (e, r) {
                   this.select(r)
                   this.getItem(r)
                   let row = this.getItem(r)
                   viewbth(r.row)
                   
               },
           }
        }
        let title_page = {
            view: "label", label: `<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("approval_bth")}</span>`
        }
        return {
            rows: [
                title_page,
                search,
                report,
                pager,
                { height: 1 },
            ]
        }
    }
    init() {
        //  !ROLE.includes(9) ? window.location.replace("#/top/home") : null
        this.VIEWBTH = this.ui(VIEWBTH)
        const grid = $$("bthSearch:dtReport")
        const form = $$("bthSearch:form")
        const td = $$("bthSearch:td")
        const fd = $$("bthSearch:fd")
       // const statusbth = $$("bthSearch:statusBth")
        const today = new Date(), yr = today.getFullYear(), mt = today.getMonth(), std = new Date(yr, mt, today.getDate()), sfd = new Date(yr, mt - 1, 1)
        td.setValue(std), fd.setValue(sfd)
       // statusbth.setValue("0")
        webix.extend(grid, webix.ProgressBar)
        $$("bthSearch:approval").attachEvent("onItemClick", function () {
            let items = grid.serialize(), arr = []
            for (let item of items) {
                if (item && item.chk) arr.push(item.id)
            }
            if (arr.length < 1) {
                webix.message(_("bth_must_select"), "error", expire)
                return false
            }
            webix.confirm(_("BTH_send_confirm"), "confirm-warning").then(() => {
                console.log("1")
                grid.showProgress()
                webix.delay(() => {
                    webix.ajax().post("api/bth/app", { listC: arr }).then(async (result) => {
                        const json = result.json()
                        if (json.hasOwnProperty("mst")) {
                            // await checkUsbVersion()
                            SELECT_CKS_PROMPT({}).then(values => {
                                webix.message(_("handling"), "", 3000)
                                let row = rows.arr[0]
                                row.serial = values.serial
                                webix.ajax().post(`${USB}FptEsign/xml`, row).then(result => {
                                    let _rows = result.json()
                                    const rows2 = {incs: row.id, signs: [{incs:row.id,xml:_rows.xml}]} // status=1 va xml signed                                
                                    webix.ajax().put("api/bth/app", rows2).then(result => {
                                    
                                        webix.message(`${_("approved")} ${result.json()} ${_("BTH")}`, "success", expire)
                                    
                                        search()
                                    })
                                }).catch(e => webix.message(_("approve_failed"), "error", expire))
                            }).finally(() => {
                                grid.hideProgress()
                            })
                        }
                        else {
                           
                            webix.message(`${_("approved")} ${json} ${_("BTH")}`, "success", expire)
                           
                            search()
                        }
                    }).finally(() => {
                        return grid.hideProgress()
                    })
                })
            })
        })
       
        const search = () => {

            grid.clearAll()
            grid.loadNext(size, 0, null, "sbthdata->api/bth/searchAppr", true)

        }

        $$("bthSearch:search").attachEvent("onItemClick", function () {
            search()
        })

        grid.attachEvent("onBeforeLoad", function () {
            this.showOverlay(_("loading"))
        })

        grid.attachEvent("onAfterLoad", function () {
            if (!this.count()) this.showOverlay(_("notfound"))
            else this.hideOverlay()
        })
        // $$("ftvanlog:export").attachEvent("onItemClick", function () {
        //     exportLog()
        // })
        function validate() {
            const param = form.getValues()
            let datafd = param.fd
            let datatd = param.td
            var date1 = new Date(datafd);
            var date2 = new Date(datatd);
            var Difference_In_Time = date2.getTime() - date1.getTime();
            var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
            let valid = false
            if (Difference_In_Days > 90) {
                webix.message(_("validate_date"))
                return true
            } else if (Difference_In_Days < 0) {
                webix.message(_("date_invalid_msg"))
                return true
            }
            return valid
        }


        // $$("bthSearch:td").attachEvent("onChange", (newv) => {
        //     fd.getPopup().getBody().define("minDate", moment(newv).subtract(subtractMonth, 'months'))
        //     fd.getPopup().getBody().define("maxDate", newv)
        //     if (moment(newv).diff(moment(fd.getValue()), 'days') != 0) fd.setValue(new Date(moment(newv)))
        // })

    }


    ready() {

    }

}
