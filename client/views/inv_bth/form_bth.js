
import { JetView } from "webix-jet"
import { ORG_TAXO, FN, INVTYPE, ORG_TAXONAME, GOODSTYPE, TYPE_TH, gridna, gridnp, gridnq, expire, GNA, FTYPE, VAT, LTYPE, CARR, VRNLIST, WN_CRE_WIN_BTNSAV_PURPOSE, ORG_ON, TAXC, size, NOTI_TYPE_COMBO, TYPE_REF_COMBO } from "models/util"
import Iwin from "views/inv_bth/view_bth"
export default class form_bth extends JetView {
    config() {
        let wncreatebthForm = {
            view: "scrollview",
            body: {
                id: "wincreate:bth:form",
                view: "form",
                padding: 0,
                margin: 0,
                elementsConfig: { labelWidth: 160 },

                elements: [
                    {
                        cols: [
                            { id: "created_date", name: "idt", label: _("created_date"), view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr, required: true, value: new Date(), disabled: true, width:350 },
                            { id: "s_name", name: "sname", label: _("wn_sname"), view: "text", attributes: { maxlength: 400 }, required: true, disabled: true, value: ORG_ON },
                            { id: "s_tax", name: "stax", label: _("wn_stax"), view: "text", attributes: { maxlength: 14 }, required: true, value: TAXC, disabled: true },
                            { id: "item_type", name: "item_type", view: 'combo', label: _("item_type"), type: 'text', options: INVTYPE(this.app), required: true, disabled: true },
                            { id: "listinv_code", name: "listinv_code", view: 'text', type: 'text', value: "01/TH-HĐĐT", hidden: true },
                            { id: "created_by", name: "created_by", view: 'text', type: 'text', value: FN, hidden: true },
                            { id: "name_bth", name: "name_bth", view: 'text', type: 'text', value: "Bảng tổng hợp dữ liệu hóa đơn điện tử", hidden: true },
                        ]

                    },
                    {
                        cols: [
                            { id: "period_type", name: "period_type", view: 'text', label: _("period_type"), type: 'text', disabled: true,width:350 },
                            { id: "period", name: "period", view: 'text', label: _("period"), type: 'text', disabled: true },
                            { id: "f_times", name: "f_times", label: _("f_times"), tooltip: _("f_times"), view: "checkbox", checkValue: 1, uncheckValue: 0, value: 1 },
                            { id: "times", name: "times", view: 'text', label: _("times"), type: 'text', hidden: true,required: true },
                            { id: "time_hidden", disabled: false },
                            { id: "id_bth", view: 'text', hidden: true},
                            {},{}
                        ]
                    },
                ],
                rules: {
                    item_type: webix.rules.isNotEmpty,
                },
                on: {
                    onAfterValidation: (result) => {
                        if (!result) webix.message(_("required_msg"), "error", expire)
                    }
                }
            }
        }
        let pager = { view: "pager", id: "bth:pager", size: size, template: "{common.first()}{common.prev()}{common.pages()}{common.next()}{common.last()}" }

        let wncreatebthGrid = {
            view: "datatable",
            id: "wincreate:bth:grid",
            editable: true,
            navigation: true,
            editaction: "click",
            resizeColumn: true,
            multiselect: false,
            select: "row",
            resizeColumn: true,
            headerRowHeight: 33,
            gravity: 6.0,          
            columns: [
                { id: "line", header: { text: _("stt"), css: "header" }, adjust: true, sort: "int" },
                { id: "id", name: "id", header: { text: "ID", css: "header" }, css: "right", hidden: true, adjust: true, width: 40 },
                { id: "form", name: "form", header: { text: _("form"), css: "header" }, css: "right", adjust: true },
                { id: "serial", name: "serial", header: { text: _("serial"), css: "header" }, css: "right", adjust: true },
                { id: "seq", name: "seq", header: { text: _("seq"), css: "header" }, css: "right", adjust: true },
                { id: "idt", name: "idt", header: { text: _("idt"), css: "header" }, css: "right", adjust: true },
                { id: "bname", name: "bname", header: { text: _("bname"), css: "header" }, css: "right", adjust: true },
                { id: "btax", name: "btax", header: { text: _("taxcode"), css: "header" }, css: "right", adjust: true },
                { id: "bcode", name: "bcode", header: { text: _("cus_code"), css: "header" }, css: "right", adjust: true },
                { id: "sum", name: "sum", header: { text: _("sum"), css: "header" }, css: "right", adjust: true, template: obj => { return webix.Number.format(obj.sum, GNA) } },
                { id: "vat", name: "vat", header: { text: _("vat"), css: "header" }, css: "right", adjust: true, template: obj => { return obj.vat ? webix.Number.format(obj.vat, GNA) : "" }, width: 120 },
                { id: "total", name: "total", header: { text: _("total"), css: "header" }, css: "right", adjust: true, template: obj => { return webix.Number.format(obj.total, GNA) } },
                { id: "kindinv", name: "kindinv", header: { text: _("status"), css: "header" },editor: "combo", collection: TYPE_TH(this.app), css: "right", adjust: true },
                { id: "typeref0", name: "ref0", header: { text: _("relation_inv_type"), css: "header" }, css: "right", adjust: true },
                { id: "adjform", name: "adjform", header: { text: _("einv_form_relation"), css: "header" }, css: "right", adjust: true },
                { id: "adjserial", name: "adjserial", header: { text: _("einv_serial_relation"), css: "header" }, css: "right", adjust: true },
                { id: "adjseq", name: "adjseq", header: { text: _("relation_inv_no"), css: "header" }, css: "right", adjust: true },
                { id: "notice_number",name: "notice_number", header: { text: _("notice_number"), css: "header" }, css: "right",editor: "text", adjust: true },
                //  { id: "itemcode", name: "", header: { text: _("item_code"), css: "header" }, css: "right", adjust: true },
               //  { id: "", name: "", header: { text: _("catalog_items"), css: "header" }, css: "right", adjust: true },
               // { id: "quantity", name: "", header: { text: _("quantity"), css: "header" }, css: "right", adjust: true },
               // { id: "price", name: "", header: { text: _("price"), css: "header" }, css: "right", adjust: true },

                
               // { id: "vrt", name: "vrt", header: { text: _("vrt"), css: "header" }, css: "right", adjust: true, },
                
                //{ id: "sname", name: "sname", header: { text: _("ou"), css: "header" }, css: "right", adjust: true },
    
                // { id: "adjperiodtype",name: "adjperiodtype", header: { text: _("adjust_form_type"), css: "header" }, css: "right", adjust: true },
                // { id: "adjperiod",name: "adjperiod", header: { text: _("adjust_form"), css: "header" }, css: "right", adjust: true },
                { id: "notice_date",name: "notice_date", header: { text: _("notice_date"), css: "header" }, css: "right",editor: "date",format: webix.i18n.dateFormatStr, adjust: true },
                { id: "note", name: "note", header: { text: _("note"), css: "header" },editor: "text", css: "right", adjust: true,width: 200 },
            ],
            rules: {
                form: webix.rules.isNotEmpty,
                serial: webix.rules.isNotEmpty,
                seq: webix.rules.isNotEmpty,
                idt: webix.rules.isNotEmpty,
                type_ref: val => val && !isNaN(val),
                noti_type: val => val && !isNaN(val),
            },
        }

        let wnbuttonarea = {
            cols: [
                pager,
                { id: "bth:btnsave", view: "button", type: "icon", icon: "mdi mdi-content-save", label: _("save"), width: 80 },
                { id: "bth:btnview", view: "button", type: "icon", icon: "mdi mdi-book-open-variant", label: _("view"), width: 80 },]
        }

        let dtl = {
            view: "datatable",
            id: "xls:dtl",
            resizeColumn: true,
            headerRowHeight: 33,
            hidden: true,
            height: 250,
            columns: [
                { id: "line", name: "STT", header: { text: "STT", css: "header" }, css: "right", adjust: true },
                { id: "type", name: "type", header: { text: _("formality"), css: "header" }, adjust: true, collection: LTYPE(this.app) },
                { id: "code", name: "code", header: { text: _("item_code"), css: "header" }, adjust: true },
                { id: "name", name: "name", header: { text: _("catalog_items"), css: "header" }, adjust: true, fillspace: 3 },
                { id: "unit", name: "unit", header: { text: _("unit"), css: "header" }, adjust: true },
                { id: "price", name: "price", header: { text: _("price"), css: "header" }, adjust: true, fillspace: 1, css: "right", inputAlign: "right", format: gridnp.format },
                { id: "quantity", name: "quantity", header: { text: _("quantity"), css: "header" }, adjust: true, css: "right", inputAlign: "right", format: gridnq.format },
                //{ id: "vrn", name: "vrt", header: { text: _("vrt"), css: "header" }, adjust: true, collection: VRNLIST },
                { id: "vrt", name: "vrt_code", header: { text: _("vrt_code"), css: "header" }, adjust: true },
                { id: "discountrate", name: "discountrate", header: { text: _("percent_discount"), css: "header" }, adjust: true, editor: "text", inputAlign: "right", format: gridna.format, editParse: gridna.editParse, editFormat: gridna.editFormat },
                { id: "discountamt", name: "discountamt", header: { text: _("amount_discount"), css: "header" }, editor: "text", inputAlign: "right", format: gridna.format, editParse: gridna.editParse, editFormat: gridna.editFormat },
                { id: "amount", name: "amount", header: { text: _("amount"), css: "header" }, adjust: true, fillspace: 1, css: "right", inputAlign: "right", format: gridna.format }
            ]
        }

        return {
            view: "window",
            id: "wncreatebth",
            fullscreen: true,
            head: {
                view: "toolbar"
                , height: 35
                , cols: [
                    { view: "label", label: _("list_inv_collect") },
                    { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => this.hide() }
                ]
            },
            body: {
                view: "scrollview",
                body: {
                    rows: [
                        wncreatebthForm,
                        wncreatebthGrid,
                        wnbuttonarea,
                        // hdr, 
                        dtl
                    ]
                }
            }
        }
    }

    init() {
        this.Iwin = this.ui(Iwin)
        const grid = $$("bth:grid")
        const dtl = $$("xls:dtl")
        let grid1 = $$("wincreate:bth:grid")
        let form = $$("wincreate:bth:form")
        let ft = $$("f_times"), t = $$("times")
        ft.attachEvent("onChange", newv => {
            if (newv == 1) {
                ft.show()
                t.hide()
                $$("time_hidden").show()
            }
            else if (newv == 0) {
                ft.show()
                t.show()
                $$("time_hidden").hide()
                // t.setValue("2")
            }
        })
     //   $$("inv_printed").setValue("0")
        $$("item_type").setValue(1)

        $$("bth:btnsave").attachEvent("onItemClick", () => {
            //   appr(0)
            if (form.validate()) {
                let _data = this.createData()
                if ($$("id_bth").getValue()) _data.id_bth = $$("id_bth").getValue()
                if (_data) {
                    webix.confirm(_("list_invoice_confirm"), "confirm-warning").then(() => {
                        let period = $$("period")
                        let period_type = $$("period_type")
                        let invtype = $$("item_type")
                        let ftime_form = {period: period.getValue(), period_type: period_type.getValue(), item_type: invtype.getValue()}
                        webix.ajax().get("api/bth/sbthdata", ftime_form).then(result => {
                            let obj = result.json()
                            if (obj.total_count > 0 && $$("f_times").getValue() > 0) {
                                webix.message({type:"error", text: _("err_f_times")})
                            }
                            else {
                                webix.ajax().post("api/bth", _data).then(async (result) => {
                                    const json = result.json()
                                    if (json.number) {
                                        webix.message(_("sent_bth")+' ' +json.number, "success",expire)
                                        //  this.cleanInput()
                                        //  this.hide()
                                        window.location.reload(true)
                                        // //callback()

                                    }
                                }).finally(() => grid.hideProgress())
                            }
                        })
                        
                    })
                }
            }
        })

        $$("bth:btnview").attachEvent("onItemClick", () => {
            if (form.validate()) {
                let data = this.createData()
                //grid1.showProgress()
                webix.delay(() => {
                    webix.ajax().post("api/bth/htmnew", data).then(result => {                      
                        let pdf = result.json()
                        let lengthPDF = pdf.sizePDF
                        console.log(lengthPDF)
                        if (lengthPDF >= 3145728) webix.message(String(_("file_pdf_size_limit")).replace("#hunglq#", 3145728 / (1024 * 1024)))
                        console.log('begin view')
                        const blob = dataURItoBlob(pdf.pdf);

                        var temp_url = window.URL.createObjectURL(blob);

                        $$("iwin:ipdf").define("src", temp_url)
                        $$("iwin:win").show()
                        webix.message.hide("exp");
                        //grid1.hideProgress()
                    }) 
                })
            }
        })

        grid1.attachEvent("onAfterSelect", () => {
            const row = grid1.getSelectedItem()
            dtl.show()
            dtl.parse(row.items)
        })
        grid1.attachEvent("onAfterUnSelect", () => {
            dtl.clearAll()
            dtl.hide()
        })
    }

    createData() {
        let wcwForm = $$("wincreate:bth:form"), wcwGrid = $$("wincreate:bth:grid")       
        let invs = wcwForm.getValues()
        invs.items = wcwGrid.serialize()
        return invs
    }
    //     for (let item of convObj.items) {
    //             if (item && item.chk) {
    //                 item.isgroup = true
    //                 let a = item.doc.items
    //                 let b = item.doc
    //             //    b.idd = b.id
    //                //delete b.doc.items
    //                 // b = JSON.stringify(b)
    //                 console.log("001item.doc.items",item.doc.items)
    //                 console.log("002item.doc",item.doc)
    //                 let data = a.map(x => Object.assign(x,b));
    //                   for(let i = 0;i<data.length;i++){
    //                     resultObj.conv.push(data[i])
    //                 }


    //                 console.log("-----------1-------",resultObj.conv)
    //             }
    //         }
    //         //Process format data resultObj
    //         return resultObj

    // }
    cleanInput() {
        let _form = $$("wincreate:bth:form"), _grid = $$("wincreate:bth:grid")
        _form.clearValidation()
        _form.clear()
        _grid.clearAll()
        _form.setValues({ noti_taxid: ORG_TAXO, noti_taxname: ORG_TAXONAME, stax: TAXC, sname: ORG_ON })
    }
    show(data) {
        let wwg = $$("wincreate:bth:grid"), form = data.form
        if (data) {
            let period = $$("period")
            let period_type = $$("period_type")
            $$("item_type").setValue(form.invtype)
            let _updategrid = data.updategrid
            let fd = new Date(Date.parse(form.fd)), d = fd.getDate(), m = fd.getMonth()+1, y =fd.getFullYear()
            if (_updategrid) wwg.clearAll()
            if (data.grid && wwg.serialize().length == 0) {
                data.grid.idt = new Date(data.grid.idt)
                let _icount = 0
                if (!Array.isArray(data.grid)) data.grid = [data.grid]
                data.grid.forEach(e => {
                    e.line = ++_icount
                    e.type_ref = 1
                })
                $$("wincreate:bth:grid").parse(data.grid)
            }
            if (form.period == "d") {
                period_type.setValue("N")
                //period.setValue(moment(form.fd).format("DD/MM/YYYY"))
                period.setValue(d+"/"+m+"/"+y)
            }
            else if (["q1", "q2", "q3", "q4"].includes(form.period)) {
                period_type.setValue("Q")
                period.setValue(form.period.replace('q', '') + "/" + y)
            }
            else {
                period_type.setValue("T")
                period.setValue(m+"/"+y)
            }
            let ftime_form = {period: period.getValue(), period_type: period_type.getValue(), item_type: form.invtype}
            webix.ajax().get("api/bth/sbthdata", ftime_form).then(result => {
                let obj = result.json()
                if (obj.total_count > 0) {
                    $$("f_times").setValue(0)
                    //$$("f_times").hide()
                    $$("times").setValue(obj.total_count)
                }
                else{
                    $$("f_times").setValue(1)
                    $$("times").setValue(0)
                }
            })
            let type = form.type
            $$("item_type").setValue(form.invtype)
            if (form.listinvids) $$("wincreate:bth:form").setValues({...$$("wincreate:bth:form").getValues(),listinvids:form.listinvids})
        }
        const win = this.getRoot()
        win.show()
    }
    edit(data){
        let wwg = $$("wincreate:bth:grid"), rec = data.rec
        if (data) {
            wwg.clearAll()
            $$("period").setValue(rec.period)
            $$("period_type").setValue(rec.period_type)
            $$("item_type").setValue(rec.item_type)
            $$("s_name").setValue(rec.s_name)
            $$("s_tax").setValue(rec.s_tax)
            $$("f_times").setValue(rec.f_times)
            $$("times").setValue(rec.times)
            $$("id_bth").setValue(rec.id)
            if (rec.doc) {
                let doc = JSON.parse(rec.doc)
                let _icount = 0
                doc.items.forEach(e => {
                    e.line = ++_icount
                    e.type_ref = 1
                    e.idt = doc.idt
                })
                $$("wincreate:bth:grid").parse(doc.items)
            }

            // if (data.grid && wwg.serialize().length == 0) {
            //     data.grid.idt = new Date(data.grid.idt)
            //     let _icount = 0
            //     if (!Array.isArray(data.grid)) data.grid = [data.grid]
            //     data.grid.forEach(e => {
            //         e.line = ++_icount
            //         e.type_ref = 1
            //     })
            //     $$("wincreate:bth:grid").parse(data.grid)
            // }
        }
        const win = this.getRoot()
        win.show()
    }
    // validateAll() {
    //     let _form = $$("wincreate:bth:form")   
    //     _form.clearValidation()
    //     let isGridOK = _grid.validate()
    //     return (isGridOK && isFormOK)
    // }

    hide() {
        const win = this.getRoot()
        win.hide()
    }
    isVisible() {
        const win = this.getRoot()
        return win.isVisible()
    }
}


