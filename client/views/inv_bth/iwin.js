import { JetView } from "webix-jet"
import { setConfStaFld, ROLE } from "models/util"
export default class Iwin extends JetView {
    config() {
        _ = this.app.getService("locale")._
        return {
            view: "window",
            id: "iwin:win",
            modal: true,
            fullscreen: true,
            head: {
                view: "toolbar"
                , height: 35
                , cols: [
                    { view: "label", label: _("invoice_view") },
                    {
                        cols: [
                            {
                                view: "button", id: "iwin:download", type: "icon", icon: "mdi mdi-download", tooltip: "Download as pdf file", width: 30,click: () => {
                                    webix.html.download($$("iwin:ipdf").getIframe().src, "invoice.pdf")
                                    
                                }
                            },
                            { view: "button", id: "iwin:btnxml", type: "icon", icon: "mdi mdi-file-xml", tooltip: "xml", width: 30, disabled: true },
                           // { view: "button", id: "iwin:btnapp", type: "icon", icon: "wxi-check", tooltip: _("invoice_approve"), width: 30 },
                          //  { view: "button", id: "iwin:btnsignpdf", type: "icon", icon: "mdi mdi-lock", tooltip: _("pdf_sign"), width: 30,disabled: true },
                            { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => $$("iwin:win").hide() }
                        ], fillspace: true, align: "right"
                    }
                ]
            },
            body:  { id: "iwin:ipdf", view: "iframe", borderless: true }
        }
    }
    ready(v, urls) {
        // if (ROLE.includes('PERM_SEARCH_INV_CONVERT_DOWNLOAD_FILE') || ROLE.includes('PERM_SEARCH_INV_VIEW_DOWNLOAD_FILE')) $$("iwin:download").show()
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
    show() {
        this.getRoot().show()
    }
    init() {
        // if (ROLE.includes('PERM_SEARCH_INV_CONVERT_DOWNLOAD_FILE') || ROLE.includes('PERM_SEARCH_INV_VIEW_DOWNLOAD_FILE')) $$("iwin:download").show()
    }
}
