import { JetView } from "webix-jet"
import { LinkedInputs3, LinkedInputs2, LinkedInputsTH, INVTYPE, expire } from "models/util"
//import form_bth from "views/inv_bth/form_bth"


export default class ApprView extends JetView {

    config() {
        webix.protoUI({ name: "dependent2", $cssName: "richselect" }, LinkedInputs2, webix.ui.richselect)
        webix.protoUI({ name: "dependentTH", $cssName: "combo" }, LinkedInputsTH, webix.ui.combo)
        const exportNumberFormat = (na) => {
            return na ? "#,##0.".padEnd(6 + parseInt(na.decimalSize), "0") : "#,##0"
        }
        const q = _("quarter"), m = _("month")

        const PERIODS = [
            { id: "d", value: _("day") },
            { id: "1", value: `${m} 01` },
            { id: "2", value: `${m} 02` },
            { id: "3", value: `${m} 03` },
            { id: "4", value: `${m} 04` },
            { id: "5", value: `${m} 05` },
            { id: "6", value: `${m} 06` },
            { id: "7", value: `${m} 07` },
            { id: "8", value: `${m} 08` },
            { id: "9", value: `${m} 09` },
            { id: "10", value: `${m} 10` },
            { id: "11", value: `${m} 11` },
            { id: "12", value: `${m} 12` },
            { id: "q1", value: `${q} 1` },
            { id: "q2", value: `${q} 2` },
            { id: "q3", value: `${q} 3` },
            { id: "q4", value: `${q} 4` },
            // { id: "y", value: _("year") },
        ]
        webix.protoUI({ name: "dependent3", $cssName: "richselect" }, LinkedInputs3, webix.ui.richselect)
        webix.proxy.bthsearch = {
            $proxy: true,
            load: function (view, params) {
                let obj = $$("bth:form").getValues()
                Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                if (!params) params = obj
                return webix.ajax(this.source, params)
            }
        }
        let form = {
            view: "form",
            id: "bth:form",
            padding: 3,
            margin: 3,
            elements:
                [
                    {
                        cols: [
                            { id: "bth:form:period", name: "period", label: _("home_period"), value: "d", view: "combo", options: PERIODS },
                            { id: "bth:form:year", name: "year", label: _("year"), view: "combo", options: [] },
                            { id: "bth:form:fd", name: "fd", label: _("fd"), view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr ,labelWidth: 110},
                            { id: "bth:form:td", name: "td", label: _("td"), view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr, labelWidth: 110 }
                        ]
                    },

                    {
                        cols: [
                            {
                                id: "bth:form:type", name: "type", label: _("invoice_type"), required: true, view: "combo", options: {
                                    view: "suggest", // optional
                                    body: { // list configuration
                                        view: "list", // optional
                                        url: "api/serial/tbt",
                                        template: function (obj) {
                                            return obj.value
                                        },
                                        yCount: 7
                                    }
                                }
                            },
                            
                            //{ id: "bth:form:form", name: "form", label: _("form"), view: "dependentTH", options: [], master: "bth:form:type", dependentUrl: "api/serial/afbt?id=", relatedView: "bth:form:serial", relatedAction: "enable", required: true },
                            { id: "bth:form:serial", name: "serial", label: _("serial"), view: "dependent2", options: [], master: "bth:form:type", dependentUrl: "api/serial/asbf1wl?id=", required: true },
                          //  { id: "bth:form:ou", name: "ou", label: _("bname"), view: "combo", suggest: { url: "api/ous/bytokenou", filter: filter } },
                         
                          { id: "bth:form:invtype", name: "invtype", label: _("invtype"), view: "richselect", options: INVTYPE(this.app), required: true, labelWidth: 110, disabled: true },
                          {cols: [
                            { id: "bth_f_times", name: "f_times", label: _("f_times"), tooltip: _("f_times"), view: "checkbox", checkValue: 1, uncheckValue: 0, value: 1,labelWidth: 110 },
                            { id: "listinvoice_num", name: "listinvoice_num", view: 'text', label: _("num_bth"), type: 'text', hidden: true,required: true},
                            { id: "bth_times", name: "times", view: 'text', label: _("times"), type: 'text', hidden: true,required: true,labelWidth: 110 }
                        ]}
                        ]
                    },
                    {
                        cols: [
                            // {cols: [
                            //     { id: "f_times", name: "f_times", label: _("f_times"), tooltip: _("f_times"), view: "checkbox", checkValue: 1, uncheckValue: 0, value: 1 },
                            //     { id: "times", name: "times", view: 'text', label: _("times"), type: 'text', hidden: true,required: true,labelWidth: 110 }
                            // ]},
                           
                            {},
                            { },
                            {},
                            {view: "button", id: "bth:btncollect", type: "icon", icon: "webix_icon_btn mdi mdi-book-open-variant", label: _("create_list_invoice"), width: 160 }
                        ]
                    },{
                        cols: [
                           // {view: "button", id: "bth:btncollect", type: "icon", icon: "webix_icon_btn mdi mdi-book-open-variant", label: _("create_list_invoice"), width: 160 },
                            {},
                            {},
                            {},
                            {},
                        ]
                    }
                    
                ], rules: {
                    fd: webix.rules.isNotEmpty,
                    td: webix.rules.isNotEmpty,
                    type: webix.rules.isNotEmpty,
                    form: webix.rules.isNotEmpty,
                    ou: webix.rules.isNotEmpty
                },
            on: {
                onAfterValidation: (result) => {
                    if (!result) webix.message(_("required_msg"))
                }
            }
        }
        // let pager = { view: "pager", id: "pager", size: size, template: "{common.first()}{common.prev()}{common.pages()}{common.next()}{common.last()}" }

        // let grid = {
        //     view: "datatable",
        //     id: "bth:grid",
        //     select: "row",
        //     multiselect: false,
        //     scroll: "xy",
        //     resizeColumn: true,
        //     editable: false,
        //     sort: false,
        //     headerRowHeight: 33,
        //     columns: [
        //         //{ header: "", width: 35, template: obj => { return obj.status >= 2 ? `<span class='webix_icon wxi-pencil', title='${_("modify")}'></span>` : "" } } ,

        //         { id: "chk", header: { content: "masterCheckbox", css: "center" }, css: "center", template: "{common.checkbox()}", width: 45 },
        //         { id: "inc", header: { text: _("id"), css: "header" }, css: "right", adjust: true, width: 50 },
        //         { id: "idt", header: { text: _("invoice_date"), css: "header" }, width: 145 },
        //         { id: "form", header: { text: _("form"), css: "header" }, adjust: true, width: 145 },
        //         { id: "serial", header: { text: _("serial"), css: "header" }, adjust: true, width: 145 },
        //         { id: "btax", header: { text: _("taxcode"), css: "header" }, adjust: true, width: 145 },
        //         { id: "seq", header: { text: _("seq"), css: "header" }, adjust: true, width: 145 },
        //         { id: "status", header: { text: _("status"), css: "header" }, collection: ISTATUS, width: 145 },
        //         { id: "sum", header: { text: _("sumv"), css: "header" }, adjust: true, css: "right", width: 145, template: obj => { return webix.Number.format(obj.sumv, GNA) } },
        //         { id: "vat", header: { text: _("vat"), css: "header" }, adjust: true, css: "right", width: 145, template: obj => { return obj.vat ? webix.Number.format(obj.vat, GNA) : "" } },
        //         { id: "total", header: { text: _("totalv"), css: "header" }, adjust: true, css: "right", width: 145, template: obj => { return webix.Number.format(obj.total, GNA) } },
        //         { id: "bname", header: { text: _("bname"), css: "header" }, adjust: true, width: 145 },
        //         { id: "buyer", header: { text: _("buyer"), css: "header" }, adjust: true, width: 145 },
        //         { id: "sname", header: { text: _("sname"), css: "header" }, adjust: true, width: 145 },
        //         {}
        //         // { id: "sendtype", header: { text: _("sendtype"), css: "header" }, },
        //     ],

        // }
        // const button123 = {
        //     cols: [
        //         pager,
        //         //  { view: "button", id: "bth:btntbph", type: "icon", icon: "webix_icon_btn mdi mdi-file-pdf-box-outline", label: _("PDF"), width: 100 },
        //         { view: "button", id: "bth:btncollect", type: "icon", icon: "webix_icon_btn mdi mdi-book-open-variant", label: _("list_invoice"), width: 160 },
        //     ]
        // }
        let title_page = {
            view: "label", label: `<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("create_bth")}</span>`
        }
        return { paddingX: 2, rows: [title_page,form, {}] }
    }

    // createData() {
    //     let    form = $$("bth:form"),
    //            grid = $$("bth:grid") 
    //         return {
    //             ...form.getValues(),
    //             items: grid.serialize()
    //         }

    // }

    // ready() {
    //     webix.ajax().get("api/serial/tbt").then(result => {
    //         $$("bth:form:type").getList().clearAll()
    //         let arr = result.json().map(e => { e.value = _(e.value); return e; })
    //         console.log(arr)
    //         $$("bth:form:type").getList().parse(arr)
    //         $$("bth:form:type").setValue(all)
    //     })
    // }
    init() {
        //this.form_bth = this.ui(form_bth)
        // let grid = $$("bth:grid")
        const form = $$("bth:form")
        const all = { id: "*", value: _("all") }
        $$("bth:form:type").getList().add(all, 0)
        $$("bth:form:serial").getList().add(all, 0)
       // $$("bth:form:ou").getList().add(all, 0)
        // // $$("bth:form:type").setValue(all)
        // $$("bth:form:form").getList().add(all, 0)
        // $$("bth:form:form").setValue("*")
        let date = new Date(), yr = date.getFullYear()
        let period = $$("bth:form:period"), year = $$("bth:form:year"), fd = $$("bth:form:fd"), td = $$("bth:form:td")
        webix.extend(form, webix.ProgressBar)
        let years = [], i = 2018
        while (i <= yr) {
            years.push({ id: i, value: i })
            i++
        }
        let list = year.getPopup().getList()
        list.clearAll()
        list.parse(years)
        year.setValue(yr)
        let dDefault = new Date(yr, date.getMonth(), 1)
        fd.setValue(dDefault)
        td.setValue(date)
        $$("bth:form:type").setValue("*")
        //$$("bth:form:form").setValue("*")
       // $$("bth:form:ou").setValue("*")
        $$("bth:form:invtype").setValue(9)
        let ft = $$("bth_f_times"), t = $$("bth_times"),listinvoice_num= $$("listinvoice_num")
        period.attachEvent("onChange", (v) => { cal(v, year.getValue()) })
        ft.attachEvent("onChange", newv => {
            if (newv == 1) {
                ft.show()
                t.hide()
                listinvoice_num.hide()
                listinvoice_num.setValue("")
                //$$("time_hidden").show()
            }
            else if (newv == 0) {
                ft.show()
              //  t.show()
                listinvoice_num.show()
                //$$("time_hidden").hide()
                // t.setValue("2")
            }
        })
        //  webix.extend(grid, webix.ProgressBar)
        const cal = (v, yr) => {
            let b = (v != "d"), tn, dn, mm
            if (b) {
                let v1 = v.substr(0, 1)
                if (v1 === "y") {
                    tn = new Date(yr, 0, 1)
                    dn = new Date(yr, 11, 31)
                }
                else if (v1 === "q") {
                    let v2 = parseInt(v.substr(1))
                    mm = 3 * (v2 - 1)
                    tn = new Date(yr, mm, 1)
                    dn = new Date(yr, mm + 3, 1)
                    dn.setDate(dn.getDate() - 1)
                }
                else {
                    mm = parseInt(v)
                    tn = new Date(yr, mm - 1, 1)
                    dn = new Date(yr, mm, 1)
                    dn.setDate(dn.getDate() - 1)
                }

            }
            else {
                mm = date.getMonth()
                tn = new Date(yr, mm, date.getDate())
                dn = new Date(yr, mm, date.getDate())
            }
            fd.setValue(tn)
            td.setValue(dn)
            fd.config.readonly = b
            td.config.readonly = b
            fd.refresh()
            td.refresh()
        }

        fd.attachEvent("onChange", (v) => {
            td.setValue(v)

        })


        year.attachEvent("onChange", (v) => { cal(period.getValue(), v) })

        // const search = () => {

        //     grid.clearAll()
        //     grid.loadNext(size, 0, null, "bthsearch->api/bth/sdatainv", true)

        // }

        // $$("bth:btnsearch").attachEvent("onItemClick", function () {
        //     search()
        // })

        $$("bth:btncollect").attachEvent("onItemClick", () => {
            //if (form.validate()) return
            let forms = form.getValues()

            if(ft.getValue()==0){
                if (!listinvoice_num.validate()) return
               // if (!t.validate()) return
            }
            $$("bth:form").showProgress({
                type:"icon",
                hide:false
              });
            $$("bth:btncollect").disable()
            if(forms.serial == "*"){
                let typeT = $$("bth:form:type").getValue()
                let r = webix.ajax().sync().get(`api/serial/asbf1wl?id=${typeT}`)
                let result = JSON.parse(r.response)
                forms.allserial = result
            }
            webix.ajax().post("api/bth/sdatainv", forms).then(result => {
                let id = result.json()
                webix.message(_("sent_bth")+' ' +id, "success",expire)
                $$("bth:btncollect").enable()
                $$("bth:form").hideProgress()
                // if (obj.length < 1) {
                //     webix.message(_("notfound"), "error")
                // } else {
                //     // this.form_bth.show({
                //     //     updategrid: true,
                //     //     purpose: WN_CRE_WIN_BTNSAV_PURPOSE.SEND_NOW,
                //     //     form: forms,
                //     //     grid: obj
                //     // })
                // }
            }).catch(()=>{
               
                $$("bth:btncollect").enable()
                $$("bth:form").hideProgress()})

        })
        // period.attachEvent("onChange", (v) => {
        //     cal(v, year.getValue())
        // })

        // grid.attachEvent("onBeforeLoad", function () {
        //     this.showOverlay(_("loading"))
        // })

        // grid.attachEvent("onAfterLoad", function () {
        //     let self = this, count = self.count()
        //     if (count) {
        //         self.hideOverlay()
        //     }
        //     else {
        //         self.showOverlay(_("notfound"))
        //     }
        // })


    }

}