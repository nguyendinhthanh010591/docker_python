import { JetView } from "webix-jet"
import { d2s, SITYPE, setConfStaFld, ROLE } from "models/util"
export default class SeouView extends JetView {
  config() {
    _ = this.app.getService("locale")._
    let row = "", st = false
    
    //if (ROLE.includes('PERM_TEMPLATE_REGISTER_GRANT_SEARCH')) {
      row = {content: "serverSelectFilter"}
      st = "server"
    //}
    
    const grid1 = {
      view: "datatable",
      id: "seou:grid1",
      select: "row",
      multiselect: false,
      resizeColumn: true,
      columns: [
        { id: "taxc", header: [{ text: _("taxcode"), css: "header" }, row], sort: st, width: 120 },//, adjust: true
        { id: "form", header: [{ text: _("form"), css: "header" }, row], sort: st, width: 120 },//, adjust: true
        { id: "serial", header: [{ text: _("serial"), css: "header" }, row], sort: st, width: 120 },//, adjust: true
        { id: "fd", header: [{ text: _("valid_from"), css: "header" }, row], sort: st,stringResult: true, format: d2s,  width: 120 },//, adjust: true
        { id: "uses", header: [{ text: _("input_type"), css: "header" }, row], sort: st, options: SITYPE(this.app), width: 150 },//, adjust: 2
        { id: "min", header: [{ text: _("from"), css: "header" },], sort: st, css: "right", adjust: true },
        { id: "max", header: [{ text: _("to"), css: "header" },], sort: st, css: "right", adjust: true},
        { id: "cur", header: [{ text: _("current"), css: "header" },], sort: st, css: "right", adjust: true}
      ],
      url: "api/serial/seou"
    }

    const grid2 = {
      view: "datatable",
      id: "seou:grid2",
      select: "row",
      multiselect: false,
      resizeColumn: true,
      columns: [
        { id: "sel", header: { content: "masterCheckbox", css: "center", contentId:"chk1" }, adjust: "header", css: "center", template: "{common.checkbox()}" },
        { id: "name", header: { text: _("name"), css: "header" }, adjust: true, fillspace: true },
      ]
    }
    let btn = {}
    //if (ROLE.includes('PERM_TEMPLATE_REGISTER_GRANT_SAVE')) {
      btn = {view: "button", id: "seou:btnsave", type: "icon", icon: "mdi mdi-content-save", label: _("save"), width: 100, disabled: ROLE.includes('PERM_TEMPLATE_REGISTER_GRANT_SAVE') ? false : true}
    //}
    return { paddingX: 2, paddingY: 2, cols: [grid1, { view: "resizer" }, { rows: [grid2, { cols: [{}, btn] }] }] }
  }

  ready(v, urls) {
    //Cau hinh rieng cac cot tĩnh
    webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
        let json = result.json()
        setConfStaFld(json)
    })
    //
  }
  init() {
    if (!ROLE.includes('PERM_TEMPLATE_REGISTER_GRANT')) {
      this.show("/top/home")
      window.location.replace(`/${homepage}`)
      return
    }
    let grid2 = $$("seou:grid2"), grid1 = $$("seou:grid1")
    grid1.attachEvent("onAfterSelect", () => {
      const row = grid1.getSelectedItem()
      webix.ajax(`api/serial/seou/${row.taxc}/${row.id}`).then(result => {
        if (grid2.getHeaderContent("chk1")) grid2.getHeaderContent("chk1").uncheck()
        grid2.clearAll()
        grid2.parse(result.json())
      })
    })

    $$("seou:btnsave").attachEvent("onItemClick", () => {
      const ous = grid2.serialize(), se = grid1.getSelectedId().id
      let arr = []
      for (const ou of ous) {
        if (ou.sel == 1) arr.push(ou.id)
      }
      webix.ajax().post("api/serial/seou", { se: se, arr: arr }).then(() => { webix.message(_("save_ok_msg"), "success") })
    })

  }
}
