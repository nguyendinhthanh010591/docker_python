import { JetView } from "webix-jet"
import { gridnf3, CARR, setConfStaFld } from "models/util"
export default class IorgsView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        const grid = {
            view: "datatable",
            id: "iorgs:grid",
            select: "row",
            multiselect: false,
            resizeColumn: true,
            columns: [
                { template: `<span class='webix_icon wxi-check'  title='${_("select")}'></span>`, width: 35 },
                { id: "taxc", header: [{ text: _("taxcode"), css: "header" }], sort: "string", adjust: true },
                { id: "code", header: [{ text: _("cus_code"), css: "header" }], sort: "string", adjust: true },
                { id: "name", header: [{ text: _("customer_name"), css: "header" }], sort: "string", adjust: true },
                { id: "fadd", header: [{ text: _("address"), css: "header" }], sort: "string", adjust: true },
                { id: "tel", header: { text: _("tel"), css: "header" }, sort: "string", adjust: true },
                { id: "mail", header: { text: "Email", css: "header" }, sort: "string", adjust: true },
                { id: "acc", header: { text: _("account"), css: "header" }, sort: "string", adjust: true },
                { id: "bank", header: { text: _("bank"), css: "header" }, sort: "string", adjust: true },
            ],
            onClick: {
                "wxi-check": function (ev, id) {
                    const rec = this.getItem(id)
                    $$("bname").setValue(rec.name)
                    $$("btax").setValue(rec.taxc)
                    $$("baddr").setValue(rec.fadd)
                    $$("btel").setValue(rec.tel)
                    $$("bmail").setValue(rec.mail)
                    $$("bacc").setValue(rec.acc)
                    $$("bbank").setValue(rec.bank)
                    $$("bcode").setValue(rec.code)
                    try {
                    $$("c0").setValue(rec.c0)
                    $$("c1").setValue(rec.c1)
                    $$("c2").setValue(rec.c2)
                    $$("c3").setValue(rec.c3)
                    }catch (error) {
                    }
                    $$("iorgs:win").hide()
                }
            }
        }
        const form = {
            cols: [
                { id: "iorgs:search", view: "search", name: "name" },
                { view: "button", id: "iorgs:btnadd", type: "icon", icon: "mdi mdi-account-multiple-plus", tooltip: 'Thêm khách hàng', width: 35 }
            ]
        }
        return { rows: [form, grid] }
    }
    ready(v, urls) {
        let lang = webix.storage.local.get("lang")
        webix.ajax().post(`api/cus`,{itype:'00ORGS',lang:lang}).then(result => {

            const cols = result.json()
            let arr = []
            for (const col of cols) {
                if (col.view == "datepicker") col.format = webix.i18n.dateFormatStr
               
                const obj = { id: col.name, header: { text: col.label, css: "header" }, adjust: true }
                if (col.type == "number") {
                    obj.css = "right"
                    obj.inputAlign = "right"
                    obj.format = gridnf3.format
                }
                arr.push(obj)
            }
            let grid = $$("iorgs:grid"), a = grid.config.columns.filter(item => !CARR.includes(item.id))
            if (arr.length > 0) grid.config.columns = a.concat(arr)
            else grid.config.columns = a
            grid.refreshColumns()
        })
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
    init() {
        let grid = $$("iorgs:grid")
        
        $$("iorgs:btnadd").attachEvent("onItemClick", () => {
            $$("iorgs:win").hide()
            $$("iorg:win").show()
        })

        grid.attachEvent("onBeforeLoad", function () {
            this.showOverlay(_("loading"))
        })

        grid.attachEvent("onAfterLoad", function () {
            if (!this.count()) this.showOverlay(_("notfound"))
            else this.hideOverlay()
        })

        const search = (val) => {
            if (!val) {
                webix.message(_("must_enter_search_conds"))
                return false
            }
            webix.ajax("api/org", { name: val }).then(result => {
                grid.clearAll()
                grid.parse(result)
            })
        }

        $$("iorgs:search").attachEvent("onEnter", function () {
            const val = this.getValue().replace(/~|\$|!|;|\*/g,"")
            this.setValue(val)
            search(val)
        })
        $$("iorgs:search").attachEvent("onSearchIconClick", function () {
            const val = this.getValue().replace(/~|\$|!|;|\*/g,"")
            this.setValue(val)
            search(val)
        })

    }
}   