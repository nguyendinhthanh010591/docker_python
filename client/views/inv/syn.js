import { JetView } from "webix-jet";
import { LinkedInputs3, gridnf, gridnf0, ITYPE, ENT,size,d2s } from "models/util";
import ItranNew from "views/inv/ItranNew"
const ISTATUS = [
    { id: "1", value: _("syn_create") },
    { id: "0", value: _("syn_wait") },
    { id: "2", value: _("syn_err") },
    { id: "4", value: _("data_source") }
]
const ISTATUS1 = [
    { id: "*", value: _("all") },
    { id: "1", value: _("syn_create") },
    { id: "0", value: _("syn_wait") },
    { id: "2", value: _("syn_err") }

]
const ISTATUS2 = [
    { id: "*", value: _("all") },
    { id: "1", value: _("access_data") },
    { id: "0", value: _("pending_data") }
   

]
 const PAYMETHOD = [
    { id: "CK", value: "CK" },
    { id: "TM", value: "TM" },
    { id: "TM/CK", value: "TM/CK" },
    { id: "DTCN", value: "DTCN" }
]
const config = (grid, type) => {

    let arr = [
        { id: "chk", header: { content: "masterCheckbox", css: "center", contentId:"chk1" }, adjust: "header", css: "center",disable: true, template: (obj, common, value, config) => {
            if ( obj.status==1||obj.status==2 ||obj.status==4) return ""
            else  return common.checkbox(obj, common, value, config)
        }
    },
        { id: "status", header: { text: _("status"), css: "header" }, sort: "server", options: ISTATUS, adjust: true },  
        { id: "sid", header: { text: _("invoice_code"), css: "header" } },
        { id: "tax_identification", header: { text: _("taxcode"), css: "header" } },
        { id: "proposer_name", header: { text: _("bname"), css: "header" } ,width: 200},
        { id: "buyer_name", header: { text: _("buyer"), css: "header" } ,width: 200},
        { id: "address", header: { text: _("address"), css: "header" },width: 200 },       
        { id: "email", header: { text: "Mail", css: "header" },adjust: true  },           
        { id: "payment_method", header: { text: _("payment_method"), css: "header" }, options: PAYMETHOD },
        { id: "currency", header: { text: _("currency"), css: "header" } },
        { id: "exchange_rate", header: { text: _("exchange_rate"), css: "header" }, css: "right", format: gridnf.format },
        { id: "main_product_name", header: { text: _("catalog_items"), css: "header" }, css: "right" },
        { id: "price", header: { text: _("price"), css: "header" }, css: "right", format: gridnf.format },
        { id: "quantity", header: { text: _("quantity"), css: "header" }, css: "right", format: gridnf.format },
        { id: "unit", header: { text: _("unit"), css: "header" }, css: "right" },
        { id: "tax_code", header: { text: _("vrt"), css: "header" }, css: "right" },
        { id: "paid_amount", header: { text: _("sumv"), css: "header" }, css: "right", format: gridnf.format },
        { id: "total_vat", header: { text: "VAT", css: "header" }, css: "right", format: gridnf.format },
        { id: "invoice_date", header: { text: _("date"), css: "header" }},
        { id: "form1", header: { text: _("form"), css: "header" } },
        { id: "serial1", header: { text: _("serial"), css: "header" } },
        { id: "seq1", header: { text: _("seq"), css: "header" } },
        { id: "error", header: { text: _("note"), css: "header" },adjust:true },
        { id: "evaluate", header: { text: _("verify"), css: "header" },adjust:true,options: ISTATUS2 }
        
    ]
let arr_hlv = [
        { id: "chk", header: { content: "masterCheckbox", css: "center", contentId:"chk1" }, adjust: "header", css: "center",disable: true, template: (obj, common, value, config) => {
             if ( obj.status==1||obj.status==2 ||obj.status==4) return ""
             else
              return common.checkbox(obj, common, value, config)
        }
    },
        // { id: "status", header: { text: _("status"), css: "header" }, sort: "server", options: ISTATUS, adjust: true },  
        { id: "sid", header: { text: _("sid_invoice"), css: "header" } },
        { id: "policy_number", header: { text: _("policynumber"), css: "header" }, adjust:true  },
        { id: "due_date", header: { text: _("duedate"), css: "header"}, format:  webix.i18n.dateFormatStr, adjust:true },
        { id: "paid_amount", header: { text: _("hlv_pamount"), css: "header" }, css: "right", format: gridnf.format },
        { id: "pay_mode", header: { text: _("hlv_paymode"), css: "header"}},
        { id: "main_product_name", header: { text: _("nameproduct"), css: "header" }, css: "right" },
        { id: "proposer_name", header: { text: _("hlv_pname"), css: "header" } ,width: 200},
        { id: "life_insured_name", header: { text: _("hlv_liname"), css: "header" } ,width: 200},
        { id: "address", header: { text: _("envaddress"), css: "header" },width: 200 },
        { id: "email", header: { text: "Mail", css: "header" },adjust: true  },   
        { id: "tax_identification", header: { text: _("hlv_tax"), css: "header" } },
        { id: "payment_date", header: { text: _("hlv_pdate"), css: "header" }, format: d2s },
        { id: "effective_date", header: { text: _("hlv_efdate"), css: "header" }, format: d2s },
        { id: "service_code", header: { text: _("hlv_code"), css: "header" } },
        { id: "buyer_name", header: { text: _("hlv_buyer"), css: "header" } ,width: 200},
        { id: "tax_code", header: { text: _("hlv_taxcode"), css: "header" }, css: "right" },
        { id: "payment_method", header: { text: _("hlv_pmethod"), css: "header" }, options: PAYMETHOD },
        { id: "currency", header: { text: _("hlv_cur"), css: "header" } },
        { id: "exchange_rate", header: { text: _("hlv_exrate"), css: "header" }, css: "right", format: gridnf.format },
        { id: "unit", header: { text: _("hlv_unit"), css: "header" }, css: "right" },
        { id: "price", header: { text: _("hlv_price"), css: "header" }, css: "right", format: gridnf.format },
        { id: "quantity", header: { text: _("hlv_quantity"), css: "header" }, css: "right", format: gridnf.format },
        { id: "total_vat", header: { text: _("hlv_VAT"), css: "header" }, css: "right", format: gridnf.format },
        { id: "amount", header: { text: _("hlv_amount"), css: "header" }, css: "right", format: gridnf.format },
        { id: "total_amount", header: { text: _("hlv_sumv"), css: "header" }, css: "right", format: gridnf.format },
        { id: "invoice_date", header: { text: _("hlv_date"), css: "header" }, format: webix.i18n.dateFormatStr },
        { id: "content", header: { text: _("hlv_content"), css: "header" }},
        { id: "service_name", header: { text: _("hlv_name"), css: "header" }},
        { id: "evaluate", header: { text: _("verify"), css: "header" },adjust:true,options: ISTATUS2 },
        { id: "status", header: { text: _("status"), css: "header" }, sort: "server", options: ISTATUS, adjust: true },  
        { id: "error", header: { text: _("note"), css: "header" },adjust:true }
    ]
    
    grid.clearAll()
    grid.config.columns = ["hlv"].includes(ENT) ? arr_hlv : arr
    grid.refreshColumns()
}


export default class SynView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        
        
        webix.protoUI({ name: "dependent", $cssName: "combo" }, LinkedInputs3, webix.ui.combo)
        webix.protoUI({ name: "dependent2", $cssName: "richselect" }, LinkedInputs3, webix.ui.richselect)
        const form = {
            view: "form",
            id: "syn:form",
            padding: 1,
            margin: 1,
            elementsConfig: { labelWidth: 70 },
            elements: [
                {
                    cols: [
                        { id: "type", name: "type", label: _("invoice_type"), view: "combo", suggest: {data:ITYPE(this.app),fitMaster:false, width:250} },//gravity: 1.3,
                        { id: "form", name: "form", label: _("form"), view: "dependent", options: [], master: "type", dependentUrl: "api/sea/fbt?id=", relatedView: "serial", relatedAction: "enable",  width: 190 },
                        { id: "serial", name: "serial", label: _("serial"), view: "dependent2", options: [], master: "form", dependentUrl: "api/sea/sbf?id=", disabled: true,  width: 150,suggest:{fitMaster:false, width:100} },
                        { id: "invs:fd", name: "fd", label: _("fd"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr,required :true},
                        { id: "invs:td", name: "td", label: _("td"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr,required :true },
                        { id: "syn:status", name: "status", label: _("status"), view: "combo", options: ISTATUS1 ,value:"*"},
                        { view: "button", id: "syn:btnsearch", type: "icon", icon: "wxi-search", label: _("search"), width: 85 },
                        {id: "invs:btnVie", height:30, view: "button", type: "icon", icon: "webix_icon wxi-download",width: 70},
                        { id: "syn:btnsave", view: "button", type: "icon", icon: "mdi mdi-content-save", label: _("save"), width: 85, disabled: true },
                      
                        
                        // {
                        //     id: "syn:file", view: "uploader", multiple: false, autosend: false, link: "syn:filelist", accept: "application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", upload: "api/inv/upload", type: "icon", icon: "mdi mdi-file-find-outline", label: _("file_select"), width: 85, formData: () => {
                        //         return { type: $$("type").getValue(), form: $$("form").getValue(), serial: $$("serial").getValue() }
                        //     }
                        // },
                      
                    ]
                },
                {
                    cols:[
                       
                        // { id: "syn:btnread", view: "button", type: "icon", icon: "mdi mdi-file-upload-outline", label: _("file_read"), width: 85 },
                       
                        { id: "syn:verify", name: "verify", label: _("verify"), view: "combo", options: ISTATUS2 ,value:"*",width: 260 },
                        { view: "button", id: "invs:btnsource", type: "icon", icon: "wxi-search", label: _("data_source"),width: 120 },
                        { view: "button", id: "invs:btnSynp", type: "icon", icon: "mdi mdi-sync", label: _("syn"),width: 85 ,disabled:true},
                        {id: "invs:btnVies", height:30, view: "button", type: "icon",label: _("source_ex"), icon: "webix_icon wxi-download",width: 150}
                       
                    ,{}
                    ]
                }
            ],
            rules: {
              //  type: webix.rules.isNotEmpty,
               // form: webix.rules.isNotEmpty,
              //  serial: webix.rules.isNotEmpty,
                fd: webix.rules.isNotEmpty,
                td: webix.rules.isNotEmpty,
                $obj: function (data) {
                    const fd = data.fd, td = data.td

                    if (fd > td) {
                        webix.message(_("date_invalid_msg"),"error")
                        return false
                    }
                    if (String(fd).substr(0, 7) != String(td).substr(0, 7)) {
                        webix.message(_("fd_td_in_month"),"error")
                      
                       return false
                    }
                    return true
                  }
            }
        }
        webix.proxy.syn = {
            $proxy: true,
            load: function (view, params) {
              let obj = $$("syn:form").getValues()
              Object.keys(obj).forEach(
                k => (!obj[k] || obj[k] == "*") && delete obj[k]
              )
              if (!params) params = {}
              params.filter = obj
              return webix.ajax(this.source, params)
            }
          }
        const grid = {
            view: "datatable",
            id: "syn:grid",
            select: "row",
            resizeColumn: true
           
        }

        return { paddingX: 2, rows: [form, grid,{ id: "invs:countinv", view: "label" }] }
    }
    ready() {
        $$("type").setValue("*")
        $$("itype").setValue("01GTKT")
    }
    init() {
				
        const all = { id: "*", value: _("all") }
        let date = new Date()
        $$("invs:fd").setValue(date)
        $$("invs:td").setValue(date)
        $$("type").getList().add(all, 0)
        let grid = $$("syn:grid"), syn = $$("syn:form")
		$$("invs:btnVie").attachEvent("onItemClick", () => {

            webix.message(`${_("export_report")}.......`, "info", -1, "excel_syn");
            
            $$("syn:form").disable();
            // $$("syn:form").showProgress({
            //     type: "icon",
            //     hide: false
            // });

            let lang = webix.storage.local.get("lang")
            let obj = $$("syn:form").getValues()
          
           
            Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
            webix.ajax().response("blob").get("api/syn/xls", {filter: obj}).then(data => {
               
                webix.html.download(data, `${webix.uid()}.xlsx`)
                $$("syn:form").enable()
               // $$("syn:form").hideProgress()
                webix.message.hide("excel_syn");
            }).catch(e => {

                $$("syn:form").enable();
                //$$("syn:form").hideProgress();
                webix.message.hide("excel_syn");

            })
           
        })		
        $$("invs:btnVies").attachEvent("onItemClick", () => {

            webix.message(`${_("export_report")}.......`, "info", -1, "excel_syn");
            
            $$("syn:form").disable();
            // $$("syn:form").showProgress({
            //     type: "icon",
            //     hide: false
            // });

            let lang = webix.storage.local.get("lang")
            let obj = $$("syn:form").getValues()
          
           
            Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
            webix.ajax().response("blob").get("api/syn/xlss", {filter: obj}).then(data => {
               
                webix.html.download(data, `${webix.uid()}.xlsx`)
                $$("syn:form").enable()
               // $$("syn:form").hideProgress()
                webix.message.hide("excel_syn");
            }).catch(e => {

                $$("syn:form").enable();
                //$$("syn:form").hideProgress();
                webix.message.hide("excel_syn");

            })
           
        })													
        this.ItranNew = this.ui(ItranNew)
        webix.extend($$("tranN:win"), webix.ProgressBar)  
        webix.extend(grid, webix.ProgressBar)
        $$("type").attachEvent("onChange", (n) => { 
            config(grid, n) 
        })
        $$("syn:btnsearch").attachEvent("onItemClick", () => {
            
            if ($$("syn:form").validate()) {
                if (grid.getHeaderContent("chk1")) grid.getHeaderContent("chk1").uncheck()
                grid.clearAll()
                grid.showProgress()
                $$("syn:btnsave").disable()
                
                grid.loadNext(size, 0,  $$("syn:btnsave").enable(), "syn->api/syn", true)
            }
        })
        $$("invs:btnsource").attachEvent("onItemClick", () => {
            
            if ($$("syn:form").validate()) {
                if (grid.getHeaderContent("chk1")) grid.getHeaderContent("chk1").uncheck()
                grid.clearAll()
                grid.showProgress()
                $$("syn:btnsave").disable()
                
                grid.loadNext(size, 0,  $$("syn:btnsave").disable(), `syn->${url_api}api/syn`, true)
            }
        })
        $$("invs:btnSynp").attachEvent("onItemClick", () => {
            if ($$("syn:form").validate()) {
                webix.confirm(_("invoice_sync_confirm"), "confirm-warning").then(() => {
                    const param = $$("syn:form").getValues()
                $$("invs:btnSynp").disable()
                if (grid.getHeaderContent("chk1")) grid.getHeaderContent("chk1").uncheck()
                grid.clearAll()
                grid.showProgress()
                webix.ajax().post("api/syn/sync", param ).then(result => {
                    let json = result.json()
                
                    console.log(json.status)
                    grid.loadNext(size, 0,  $$("syn:btnsave").enable(), "syn->api/syn", true)
                    $$("invs:btnSynp").enable()
                        if (json.status == 1) {
                            webix.message((_("syn_hlv_gd")).replace("@@", json.count))
                        
                        }else{
                            webix.message(_("syn_hlv_gd_not_found"), "error")
                        }
                    }).catch(() => { grid.hideProgress()
                        $$("invs:btnSynp").enable() })
                    })  
           
           }
        })
        $$("tranN:btnsave").attachEvent("onItemClick", () => {
            let rows = grid.serialize(), arr = [],idt=[]

            for (let row of rows) {
               
                if (row && row.chk && row.status==0) 
                {   
                    row.type = $$("itype").getValue()
                    row.form = $$("iform").getValue()
                    row.serial = $$("iserial").getValue()
                    idt.push(row.idt)
                    arr.push(row)
                }
               
            }
            const unique = Array.from(new Set(idt))
          
            if (unique.length>1) 
            {
                webix.message(_("duplicate_day"))
                return false
            }
            if (arr.length < 1) {
                webix.message(_("invoice_must_select"))
                return false
            }
            webix.delay(() => {
                $$("tranN:win").showProgress()
                $$("syn:btnsave").disable()
                $$("tranN:btnsave").disable()
                webix.ajax().post("api/syn/ins_another", { invs: arr }).then(result => {
                    let json = result.json()
                  
                    webix.message(`${$$("syn:btnsave").config.label} ${json[0].count} ${_("invoice")},${json[0].count_e} ${_("invoice")} ${_("syn_err")}`, "success", 10000)
                    $$("tranN:win").hideProgress()
                    $$("tranN:win").hide()
                    $$("tranN:btnsave").enable()
                }).catch(() => { $$("tranN:win").hideProgress() })
            })
        })
        $$("syn:btnsave").attachEvent("onItemClick", () => {
            $$("tranN:win").show()
            
            
        })
        $$("syn:verify").attachEvent("onChange", newv => {
           
            if (newv == "1") {
                $$("invs:btnSynp").enable()
               
            }
            else {
              
                $$("invs:btnSynp").disable()
            }
        })
        grid.attachEvent("onAfterLoad", function () {
            if (grid.getHeaderContent("chk1")) grid.getHeaderContent("chk1").uncheck()
            if (!this.count()) this.showOverlay(_("notfound"))
            else this.hideOverlay()
            let self = this, count = self.count()
            $$("invs:countinv").setValue(`${_("found")} ${count}`)
          })
    }
}
