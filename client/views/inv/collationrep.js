import { JetView } from "webix-jet"
import { ITYPE, filter, ENT, CKSTATUS, setConfStaFld ,OU} from "models/util"

export default class ReportView extends JetView {
    config () {
        let elements_more = [
            {
                cols: [
                    { id: "fd", name: "fd", label: _("fd"), view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr, required: true },
                    { id: "td", name: "td", label: _("td"), view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr, required: true },

                ]
            },
            { height: 20 },
            {
                cols: [
                    {},
                    { view: "button", id: "btncollarep", type: "icon", icon: "wxi-download", label: _("report"), width: 100 },
                    { width: 20 },
                    { view: "button", id: "btnlogout", type: "icon", icon: "webix_icon mdi mdi-logout", label: ("Thoát"), width: 100 },

                ]
            }
        ]

        var form = {
            view: "form",
            id: "report:form",
            elements:
                elements_more,
            rules: {
                fd: webix.rules.isNotEmpty,
                td: webix.rules.isNotEmpty,
                $obj: function (data) {
                    const fd = data.fd, td = data.td
                    if (fd > td) {
                        webix.message(_("date_invalid_msg"))
                        return false
                    }
                    const dtime = (new Date(td)).getTime() - (new Date(fd)).getTime()
                    if (dtime > 31622400000) {
                        webix.message(_("dt_search_between_year"))
                        return false
                    }
                    return true
                }
            }
        }
        let title_page = { view: "label", label:`<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        Báo cáo đối chiếu</span>`}
        let colsall = { cols: [{width: 500},{ gravity: 8, rows: [form] }, {width: 500}] }
        return { paddingX: 2, rows: [title_page,colsall] }
    }

    ready(v, urls) {
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
    }

    init() {
        let date = new Date(),yr = date.getFullYear(), firstDay = new Date(yr, date.getMonth(), 1)
        let form = $$("report:form"), fd = $$("fd"), td = $$("td")
   
        fd.setValue(firstDay)
        td.setValue(date)

    
    $$("btncollarep").attachEvent("onItemClick", () => {
            webix.message("meo")
            const param = form.getValues()
            webix.ajax().response("blob").get("api/test1", param).then(data => {
                webix.html.download(data, `${webix.uid()}.xlsx`)
                webix.message.hide("exp");
                $$("report:form").hideProgress()
            }).catch(e => {
                webix.message.hide("exp");
                $$("report:form").hideProgress()
    
            })
        
    })
    }
}