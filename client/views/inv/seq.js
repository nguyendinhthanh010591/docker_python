import { JetView } from "webix-jet"
import {ROLE, size, LinkedInputs3, t2s, d2s, gridnf0, ITYPE, ENT, setConfStaFld } from "models/util"
import Iwin from "views/inv/iwin"
export default class SeqView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        webix.protoUI({ name: "dependent3", $cssName: "richselect" }, LinkedInputs3, webix.ui.richselect)
        webix.proxy.seq = {
            $proxy: true,
            load: function (view, params) {
                let obj = $$("seq:form").getValues()
                Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                if (!params) params = {}
                params.filter = obj
                return webix.ajax(this.source, params)
            }
        }
        let form = {
            view: "form",
            id: "seq:form",
            padding: 3,
            margin: 3,
            elements:
                [
                    {
                        cols: [
                            // { id: "seq:countinv", view: "label", width: 150 },
                            { id: "type", name: "type", label: _("invoice_type"), view: "combo", options: ITYPE(this.app), gravity: 1.5 },
                            { id: "form", name: "form", label: _("form"), view: "dependent3", options: [], master: "type", dependentUrl: "api/seq/fbt?id=", relatedView: "serial", relatedAction: "enable" },
                            { id: "serial", name: "serial", label: _("serial"), view: "dependent3", options: [], master: "form", dependentUrl: "api/seq/sbf?id=", disabled: true },
                            { id: "td", name: "td", view: "datepicker", label: _("td"), type: "icon", icon: "mdi", stringResult: true, hidden: ["aia"].includes(ENT) ? true : false },
                            { view: "button", id: "seq:btndel", type: "icon", icon: "mdi mdi-delete", label: _("invoice_del_btn"), width: 100, hidden:true },
                            { view: "button", id: "seq:btnseq", type: "icon", icon: "mdi mdi-sort-numeric", label: _("invoice_seq"), disabled: ["vib"].includes(ENT) ? false : true, width: 100 },
                            { view: "button", id: "seq:btnseqAll", type: "icon", icon: "mdi mdi-sort-numeric", label: _("invoice_seqAll"), disabled: true, width: 150 },
                            { view: "button", id: "seq:btnsearch", type: "icon", icon: "wxi-search", label: _("search"), width: 100 },
                            { view: "button", id: "seq:btnnote", type: "icon", icon: "mdi mdi-content-duplicate", label: _("note"), width: 100, hidden: ["vib"].includes(ENT) ? false : true }

                        ]
                    }
                ]
        }
        let columns = [
            { id: "chk", header: { content: "masterCheckbox", css: "center" }, adjust: "header", css: "center", template: "{common.checkbox()}", hidden: (["vib"].includes(ENT)) ? false : true },
            { id: "vie", header: "", width: 35, template: `<span class='webix_icon wxi-search', title='${_("view")}'></span>` },
            { id: "id", header: { text: "ID", css: "header" }, css: "right", adjust: true },
            { id: "idt", header: { text: _("invoice_date"), css: "header" }, format: d2s, adjust: true },
            { id: "form", header: { text: _("form"), css: "header" }, adjust: true },
            { id: "serial", header: { text: _("serial"), css: "header" }, adjust: true },
            { id: "btax", header: { text: _("taxcode"), css: "header" }, adjust: true },
            { id: "bname", header: { text: _("bname"), css: "header" }, adjust: true },
            { id: "c9", header: { text: _("customer_code2"), css: "header" }, adjust: true, hidden: ["scb"].includes(ENT) ? false : true },
            { id: "buyer", header: { text: _("buyer"), css: "header" }, adjust: true },
            { id: "baddr", header: { text: _("address"), css: "header" }, adjust: true },
            (ENT == 'mzh')? { id: "sum", header: { text: _("sumo"), css: "header" }, css: "right", adjust: true, format: gridnf0.numberFormat }:{ id: "sumv", header: { text: _("sumv"), css: "header" }, css: "right", adjust: true, format: gridnf0.format },
            (ENT == 'mzh')?{ id: "vat", header: { text: "VAT", css: "header" }, css: "right", adjust: true, format: gridnf0.numberFormat }:{ id: "vatv", header: { text: "VAT", css: "header" }, css: "right", adjust: true, format: gridnf0.format },
            (ENT == 'mzh')?{ id: "total", header: { text: _("totalo"), css: "header" }, css: "right", adjust: true, format: gridnf0.numberFormat }:{ id: "totalv", header: { text: _("totalv"), css: "header" }, css: "right", adjust: true, format: gridnf0.format },
            { id: "note", header: { text: _("note"), css: "header" }, adjust: true },
            { id: "ou", header: { text: _("ou"), css: "header" }, collection: "api/cat/nokache/ou", adjust: true },
            { id: "uc", header: { text: _("creator"), css: "header" }, adjust: true },
            { id: "ic", header: { text: "IC", css: "header" }, adjust: true }
        ]

        if (ENT == 'hdb') {
            columns.splice(8, 3) //Xóa cột tiền VND
            columns.splice(8, 0,
                { id: "sum", header: { text: _("sumo"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat },
                { id: "vat", header: { text: _("vato"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat },
                { id: "total", header: { text: _("totalo"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat })
        }

        if (ENT == 'hlv') {
            columns.push({ id: "adjdes", header: { text: _("replace"), css: "header" }, format: webix.template.escape, adjust: true })
            columns.push({ id: "cde", header: { text: _("replaced"), css: "header" }, adjust: true })
        }
        else {
            columns.push({ id: "adjdes", header: { text: _("repadj"), css: "header" }, format: webix.template.escape, adjust: true })
            columns.push({ id: "cde", header: { text: _("repadjed"), css: "header" }, adjust: true })
        }
        let pager = { view: "pager", id: "seq:pager", size: size, template: `{common.first()}{common.prev()} {common.pages()}{common.next()}{common.last()}` }

        let grid = {
            view: "datatable",
            id: "seq:grid",
            select: "row",
            resizeColumn: true,
            editable: false,
            sort: false,
            pager: "seq:pager",
            on: { onBeforeSort: function () { return false } },
            onClick: {
                "wxi-search": function (e, r) {
                    const id = r.row
                    webix.ajax(`api/app/htm/${id}`).then(result => {
                       // const json = result.json()
                       // const req = createRequest(json)
                        //jsreport.renderAsync(req).then(res => {
                        //$$("iwin:ipdf").define("src", res.toDataURI())
                        let pdf = result.json()
                        let b64 = pdf.pdf
                        const blob = dataURItoBlob(b64);

                        var temp_url = window.URL.createObjectURL(blob);
                        $$("iwin:ipdf").define("src", temp_url)
                        $$("iwin:btnapp").hide()
                        $$("iwin:btnxml").hide()
                        $$("iwin:btnsignpdf").hide()
                        $$("iwin:win").show()
                        // webix.ajax().post("api/seek/report", req).then(res => {
                        //     let pdf = res.json()
                        //     let b64 = pdf.pdf
                        //     const blob = dataURItoBlob(b64);

                        //     var temp_url = window.URL.createObjectURL(blob);
                        //     $$("iwin:ipdf").define("src", temp_url)
                        //     $$("iwin:win").show()
                        // })

                    })
                }
            },
            columns: columns
        }
        let title_page = { view: "label", label:`<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("numbering_type")}</span>`}
        return { paddingX: 2, rows: [title_page, form, grid,
            {
                cols: [pager, { id: "seq:countinv", view: "label", margin: 0 },{}
                ]
            }] }
    }
    
    ready(v, urls) {
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
    
    init() {
        if (!ROLE.includes('PERM_CREATE_INV_SEQ')) {
            this.show("/top/home")
            window.location.replace(`/${homepage}`)
            return
          }
        this.Iwin = this.ui(Iwin)
        webix.ajax("api/sysdate").then(result => {
            let dt = new Date(result.json())
            $$("td").setValue(dt)
            // $$("syslog:dt").getPopup().getBody().define("minDate", dt)
        })
        let grid = $$("seq:grid")
        webix.extend(grid, webix.ProgressBar)
        const search = () => {
            grid.clearAll()
            grid.loadNext(size, 0, null, "seq->api/seq", true)
        }
        $$("seq:btndel").attachEvent("onItemClick", () => {
            let rows = grid.serialize(), arr = []
            for (let row of rows) {
                if (row && row.chk) arr.push(row)
            }
            if (arr.length < 1) {
                webix.message(_("invoice_must_select"))
                return false
            }
            webix.delay(() => {
                webix.ajax().post("api/inv/seq/del", { invs: arr }).then(result => {
                    let json = result.json()
                    console.log(json.status)
                    if(json.status==1){
                        grid.clearAll()
                        grid.loadNext(size, 0, null, "seq->api/seq", true)
                    }
                }).catch(() => {  })
            })
        })
        if (["hdb"].includes(ENT)) grid.config.multiselect = true
        else grid.config.multiselect = false
        grid.refresh()

        $$("type").setValue("01GTKT")
        $$("seq:btnsearch").attachEvent("onItemClick", () => {
            search()
        })
        $$("serial").attachEvent("onChange", (n, o) => {
             if (n && n != o) search()
        })
        $$("seq:btnseqAll").attachEvent("onItemClick", () => {
            webix.confirm(_("sequence_confirm"), "confirm-warning").then(() => {
                $$("seq:btnseqAll").disable()
             
                grid.disable();
                grid.showProgress({
                  type:"icon",
                  hide:false
                });
               
                const form = $$("form").getValue(), serial = $$("serial").getValue(),td = $$("td").getValue(),rows = grid.getSelectedItem()
                let seqList = [], id

               

                webix.ajax().post("api/seq", { idt: td, form: form, serial: serial, seqList: seqList }).then((result,iid=id) => {
                    let count, er = 0
                    count = result.json()
                    if(ENT == "scb"){
                        er = count.countER
                        count = count.count
                    }
                    $$("seq:grid").enable()
                    $$("seq:grid").hideProgress()
                    
                    if (iid)  
                    {
                                webix.alert({
                                title:_("success"),
                                ok:"Ok",
                                text:`${_("sequenced")} ${_("invoice")} ID: ${iid}`
                            
                            }).then(function(result){
                                search()
                               
                            });
                    }
                  //  webix.message(`${_("sequenced")} ${_("invoice")} ID: ${iid}`,"success") 
                    else 
                    {
                        let msgAL = ""
                        if(er > 0) msgAL = `${_("login_usb_yes")} ${er} ${_("invoice")} ${_("itran_error")}`
                        webix.alert({
                            title:_("success"),
                            ok:"Ok",
                            text:`${_("sequenced")} ${count} ${_("invoice")}! \n ${msgAL}`
                            
                        }).then(function(result){
                            search()
                          
                        });
                    } 
                   
                    //webix.message(`${_("sequenced")} ${count} ${_("invoice")}`, "success")
                    //search()
                }).catch(()=>{ 
                     $$("seq:grid").enable()
                     $$("seq:grid").hideProgress()})
            })
        })
        $$("seq:btnseq").attachEvent("onItemClick", () => {
            webix.confirm(_("sequence_confirm"), "confirm-warning").then(() => {
                $$("seq:btnseq").disable()
             
                grid.disable();
                grid.showProgress({
                  type:"icon",
                  hide:false
                });
               
                const form = $$("form").getValue(), serial = $$("serial").getValue(),td = $$("td").getValue(),rows = grid.getSelectedItem()
                let seqList = [], idt, id, idSEQ

                // Nếu seqList.length > 0 => 1 thì cấp số 1 hóa đơn, lỗ chỗ thì lỗ chỗ
                // Nếu seqList.length = 0 => cấp số cả dải bé hơn ngày hđ chọn
                if(["hdb"].includes(ENT)) {
                    if(rows.length > 0) {
                        //cap so lỗ chỗ 1 ngày
                        seqList = rows.map(i=>i.id)
                    } else {
                        //Cap so 
                        seqList.push(rows.id)
                        id = rows.id
                    }
                    idt = new Date()
                } else if (["vib"].includes(ENT)) {
                    let rowschk = grid.serialize(), arr = []
                    for (let row of rowschk) {
                        if (row && row.chk) arr.push(row)
                    }
                    
                    if (arr.length < 1) {
                        webix.message(_("invoice_must_select"))
                        if(ROLE.includes('PERM_CREATE_INV_SEQ_ONE')) $$("seq:btnseq").enable()
                        $$("seq:grid").enable()
                        $$("seq:grid").hideProgress()
                        return false
                    }
                    seqList = arr.map(i=>i.id)
                    idt = new Date()
                } else {
                    // Cấp số theo dải bé hơn ngày hđ chọn
                    idt = rows.idt
                    idSEQ = rows.id
                }

                //Nếu là VIB thì theo check chọn
                

                webix.ajax().post("api/seq", { idt: idt, form: form, serial: serial, seqList: seqList, id: idSEQ }).then((result,iid=id) => {
                    let count, er = 0
                    count = result.json()
                    if(ENT == "scb"){
                        er = count.countER
                        count = count.count
                    }
                    if (["vib","scb"].includes(ENT)) $$("seq:btnseq").enable()
                    $$("seq:grid").enable()
                    $$("seq:grid").hideProgress()
                    
                    if (iid)  
                    {
                                webix.alert({
                                title:_("success"),
                                ok:"Ok",
                                text:`${_("sequenced")} ${_("invoice")} ID: ${iid}`
                            
                            }).then(function(result){
                                search()
                               
                            });
                    }
                  //  webix.message(`${_("sequenced")} ${_("invoice")} ID: ${iid}`,"success") 
                    else 
                    {
                        let msgAL = ""
                        if(er > 0) msgAL = `${_("login_usb_yes")} ${er} ${_("invoice")} ${_("itran_error")}`
                        webix.alert({
                            title:_("success"),
                            ok:"Ok",
                            text:`${_("sequenced")} ${count} ${_("invoice")}! \n ${msgAL}`
                            
                        }).then(function(result){
                            search()
                          
                        });
                    } 
                   
                    //webix.message(`${_("sequenced")} ${count} ${_("invoice")}`, "success")
                    //search()
                }).catch(()=>{ 
                    if (["vib","scb"].includes(ENT)) $$("seq:btnseq").enable()
                     $$("seq:grid").enable()
                     $$("seq:grid").hideProgress()})
            })
        })
        grid.attachEvent("onBeforeLoad", function () {
            this.showOverlay(_("loading"))
        })
        grid.attachEvent("onAfterLoad", function () {
            let self = this, count = self.count()
            $$("seq:countinv").setValue(`${_("found")} ${count}`)
            if (count) {
                self.hideOverlay()
                if(ROLE.includes('PERM_CREATE_INV_SEQ_ALL')) $$("seq:btnseqAll").enable()
            }
            else self.showOverlay(_("notfound"))
        })
        grid.attachEvent("onAfterSelect", sel => {
            //grid.selectRange(grid.getFirstId(), sel.id)
            if (!["vib"].includes(ENT)) {
                if(ROLE.includes('PERM_CREATE_INV_SEQ_ONE')) $$("seq:btnseq").enable()
            }
        })
        grid.attachEvent("onAfterUnSelect", () => {
            if (!["vib"].includes(ENT)) {
                const r = grid.getSelectedItem()
                if (!r || r.length <= 0) $$("seq:btnseq").disable()
            }
        })
        $$("seq:btnnote").attachEvent("onItemClick", () => {
            const rows = grid.getSelectedItem()
            let seqList = [], idt

            if (["vib"].includes(ENT)) {
                let rowschk = grid.serialize(), arr = []
                for (let row of rowschk) {
                    if (row && row.chk) arr.push(row)
                }

                if (arr.length < 1) {
                    webix.message(_("invoice_must_select"))
                    if(ROLE.includes('PERM_CREATE_INV_SEQ_ONE')) $$("seq:btnseq").enable()
                    $$("seq:grid").enable()
                    $$("seq:grid").hideProgress()
                    return false
                }
                seqList = arr.map(i => i.id)
                idt = new Date()
            }
            //
            if (seqList.length == 1) {
                webix.ajax().post("api/inv/noteseq", { id: seqList[0] })
                    .then(data => data.json())
                    .then(data => {
                        $$("old_note").setValue(data)
                    }).catch(err => {
                        console.log(err)
                        webix.message(err.message, "error")
                    })
            }
            //
            try {
                webix.ui({
                    view: "popup",
                    id: "popupNote",
                    height: 400,
                    width: 600,
                    isVisible: true,
                    position: "center",
                    autofocus: true,
                    body: {
                        view: "form",
                        id: "note:form",
                        elements: [
                            { view: "textarea", id: "old_note", name: "old_note", label: _("old_note"),height: 150, labelWidth: 100,   disabled: true, inputAlign: "left" },
                            { view: "text", id: "new_note", name: "note_no", label: _("new_note"), labelWidth: 100, inputAlign: "left" },
                            {
                                cols: [
                                    {},
                                    { view: "button", id: "btnNote", value: _("note"), css: "webix_primary" },
                                    { view: "button", id: "btnCancelTrans", value: _("cancel") }
                                ]
                            }
                        ]
                    },
                }).show();
                $$("btnCancelTrans").attachEvent("onItemClick", () => $$("popupNote").hide())
                $$("btnNote").attachEvent("onItemClick", () => {
                    try {
                        const param = $$("note:form").getValues()
                        if(seqList.length > 1){
                            param.id = seqList
                        }else param.id = seqList[0]
                        param.idt = idt
                        if (!(param.note_no)) {
                            webix.message(_("required_msg"), "error")
                            return false
                        }
                        webix.ajax().post("api/inv/note", param)
                            .then(data => data.json())
                            .then(data => {
                                $$("popupNote").hide()
                                if (data) {
                                    grid.clearAll()
                                    search()
                                }
                                else {
                                    webix.message(err.message, "error")
                                }
                            }).catch(err => {
                                console.log(err)
                                webix.message(err.message, "error")
                            })
                    }
                    catch (err) {
                        webix.message(err.message, "error")
                    }
                })

            }
            catch (err) {
                webix.message(err.message, "error")
            }
        })
    }
}