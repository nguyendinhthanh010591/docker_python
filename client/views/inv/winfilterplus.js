import { JetView } from "webix-jet"
import { IKIND123, filter, MAILSTATUS } from "models/util"
export default class FilterPlusWin extends JetView {
    config() {
        return {
            view: "window",
            id: "winfilterplus:win",
            // modal: true,
            width: 640,
            height: 380,
            position: "center",
            resize: true,
            move:true,
            //fullscreen: true,
            head: {
                view: "toolbar"
                , height: 35
                , cols: [
                    { view: "label", label: _("filter_plus") },
                    { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => this.hide() }
                ]
            },
            body: {
                view: "scrollview",
                body: {
                    view: "form",
                    id: "winfilterplus:win:form",
                    elements: [
                        {
                            cols:[
                                { id: "winfilterplus:win:form:paym", name: "paym", label: _("payment_method"), view: "combo", suggest: { url: "api/cat/kache/paym" } },
                                { id: "winfilterplus:win:form:maildt", name: "maildt", label: "Send Email", view: "combo", options: MAILSTATUS(this.app) },
                                //{ id: "winfilterplus:win:form:maildt", name: "maildt", label: _("send_mail"), view: "combo", options: MAILSTATUS(this.app) },
                            ]
                        },
                        {
                            cols:[
                                { id: "winfilterplus:win:form:curr", name: "curr", label: _("currency"), view: "combo", suggest: { url: "api/cat/kache/currency" } },
                                { name: "ic", label: "IC", view: "text", attributes: { maxlength: 36 } },
                            ]
                        },
                        {
                            cols:[
                                { id: "winfilterplus:win:form:adjtyp", name: "adjtyp", label: _("property"), view: "combo", options: IKIND123(this.app)  },
                                { name: "buyer", label: _("buyer"), view: "text", attributes: { maxlength: 255 } },
                            ]
                        },
                        { height: 10 },
                    ]
                }
            }
        }
    }
    init() {
        const all = { id: "*", value: _("all") }
        $$("winfilterplus:win:form:maildt").getList().add(all, 0)
        $$("winfilterplus:win:form:adjtyp").getList().add(all, 0)
        $$("winfilterplus:win:form:curr").getList().add(all, 0)
        $$("winfilterplus:win:form:paym").getList().add(all, 0)
    }
    show() {
        const win = this.getRoot()
        win.show()
        $$("winfilterplus:win:form:paym").setValue("*")
        $$("winfilterplus:win:form:curr").setValue("*")
        $$("winfilterplus:win:form:adjtyp").setValue("*")
        $$("winfilterplus:win:form:maildt").setValue("*")
    }
    hide() {
        const win = this.getRoot()
        $$("winfilterplus:win:form").clear()
        win.hide()
    }
    isVisible() {
        const win = this.getRoot()
        return win.isVisible()
    }
}
