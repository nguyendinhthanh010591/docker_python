import { JetView } from "webix-jet"
import { w, h, LinkedInputs2, size, filter, d2s, t2s, expire, ddate, gridnf0, ISTATUS, PAYMETHOD,INV_CQT_STATUS, ITYPE, IKIND, ROLE, TRSTATUS, CARR, ENT, RPP, VAT, setConfStaFld, OU, MAILSTATUS, SMSSTATUS, gridnfconf, DEGREE_CONFIG, WN_CRE_WIN_BTNSAV_PURPOSE, WRONGNOTICE_CQT_STATUS,SESS_STATEMENT_SENTYPE } from "models/util"
import Mwin from "views/inv/mwin"
import Swin from "views/inv/swin"
import Iwin from "views/inv/iwin"
import Rwin from "views/inv/rwin"
import Wwin from "views/inv/wwin"
import BTHwin from "views/inv_bth/bthwin"
import VIEWWN from "views/wrongnotice/wnview"
import Wncreatewin from "views/wrongnotice/wncreatewin"
import JSZip from "jszip"
const format = require("xml-formatter")
const DDS = [
    { id: "0", value: "Chưa gửi" },
    { id: "1", value: "Đã gửi" }

]
const INVTYPE = [
    { id: "0", value: "Daily" },
    { id: "1", value: "MonthLy" },
]
const DDST = [
    { id: "0", value: "Sent Successfully" },
    { id: "1", value: "Send Failed" },
    { id: "2", value: "Send To Custome" },
    { id: "3", value: "New" }

]
const LSD = [
    { id: "1", value: _("invstatus_1") },
    { id: "2", value: _("invstatus_2") },
    { id: "6", value: _("invstatus_6") },
    { id: "3", value: _("invstatus_3") },
]
const PERIODS = [
    { id: "1", value: "Tháng 01" },
    { id: "2", value: "Tháng 02" },
    { id: "3", value: "Tháng 03" },
    { id: "4", value: "Tháng 04" },
    { id: "5", value: "Tháng 05" },
    { id: "6", value: "Tháng 06" },
    { id: "7", value: "Tháng 07" },
    { id: "8", value: "Tháng 08" },
    { id: "9", value: "Tháng 09" },
    { id: "10", value: "Tháng 10" },
    { id: "11", value: "Tháng 11" },
    { id: "12", value: "Tháng 12" }
]
const MODULE = [
    { id: "GL", value: "GL" },
    { id: "TF", value: "TF" },
    { id: "FT", value: "FT" },
    { id: "CL", value: "CL" },
    { id: "RB", value: "RB" },
    { id: "FX", value: "FX" },
    { id: "BT", value: "BT" },
    { id: "MM", value: "MM" },
    { id: "BS", value: "BS" }
]
const TH_TYPE = [
    { id: "1", value: "Điều chỉnh thông tin", valueclient: "th_type_dctt" },
    { id: "2", value: "Giải trình của CQT", valueclient: "th_type_gt" }
]
const NOTI_TYPE_COMBO2 = [
    { id: "1", value: "Hủy", valueclient: "n_t_c_cancel" },
    { id: "2", value: "Điều chỉnh", valueclient: "n_t_c_adj" },
    { id: "3", value: "Thay thế", valueclient: "n_t_c_rep" },
    { id: "4", value: "Giải trình", valueclient: "n_t_c_explan" },
    { id: "5", value: "Sai sót do tổng hợp", valueclient: "n_t_c_mistake" }
]
let b64, cxls, arrID = [], idDel, countID = 0
const configs = (type) => {
    let lang = webix.storage.local.get("lang")
    webix.ajax().get(`api/sea/afbt?id=${type}`).then(r => {
        let datasr = r.json()
        let removeDuplicates = (array, key) => {
            let lookup = new Set();
            return array.filter(obj => obj[key] && !lookup.has(obj[key]) && lookup.add(obj[key]));
        }
        let arr = [{ id: "*", value: _("all") }, ...datasr]

        $$("form").getList().clearAll()

        $$("form").getList().parse(removeDuplicates(arr, 'id'))
        $$("form").setValue("*")

        //Bo o ham ready vi no khong dung, dat vao day moi dung
        //Lay o session ra va set cho toan bo form truoc, ky hieu set tai event onChange cua form
        let state = webix.storage.session.get("invs:form")
        if (state) {
            let form = $$("invs:form")
            form.setValues(state)
        }
        // Xóa cache sau khi lấy đc dữ liệu
        webix.storage.session.remove("invs:form")
    })
    webix.ajax().post(`api/cus`,{itype:type,lang:lang}).then(result => {
        let cols = result.json(), len = cols.length, grid = $$("invs:grid")
        let arr = grid.config.columns.filter(item => !CARR.includes(item.id))
        //Delete column rel before add it to last arr
        arr.forEach((v, i) => { if (v.id == "rel") arr.splice(i, 1) })
        cxls = []
        if (len > 0) {
            for (const col of cols) {
                // arr.push({ id: col.id, header: { text: col.label, css: "header" }, adjust: true })
                // cxls.push({ id: col.id, header: col.label })
                let obj = { id: col.id, header: col.label }
                cxls.push(obj)
                if (col.view == "datepicker") obj.format = webix.i18n.dateFormatStr
                if (col.view == "text") obj.format = webix.template.escape
                if (col.suggest) obj.collection = col.suggest.data
                obj.header = { text: col.label, css: "header" }
                obj.adjust = true
                obj.sort = "server"
                arr.push(obj)
            }
        }
        if (type == "03XKNB" || type == "04HGDL") {
            if (ENT == 'onprem') {
                arr.push({ id: "ordno", header: { text: _("ordno"), css: "header" }, sort: "server", adjust: true })
                arr.push({ id: "orddt", header: { text: _("orddt"), css: "header" }, sort: "server", format: d2s, adjust: true })
                arr.push({ id: "whsfr", header: { text: _("export_address"), css: "header" }, sort: "server", adjust: true })
                arr.push({ id: "whsto", header: { text: _("import_address"), css: "header" }, sort: "server", adjust: true })
            }
        } else {
            arr.forEach((v, i) => { if (v.id == "ordno") arr.splice(i, 1) })
            arr.forEach((v, i) => { if (v.id == "orddt") arr.splice(i, 1) })
            arr.forEach((v, i) => { if (v.id == "whsfr") arr.splice(i, 1) })
            arr.forEach((v, i) => { if (v.id == "whsto") arr.splice(i, 1) })
        }
        arr.push({ id: "rel", header: "", width: 35, adjust: true, template: (obj, common) => (obj.adjtyp) ? `<span class='webix_icon wxi-drag', title='${_("find_similar")}'></span>` : "" })
        grid.config.columns = arr
        grid.refreshColumns()
        let r4 = $$("invs:r4")
        r4.removeView("invs:r5")
        r4.removeView("invs:r6")
        if (len > 0) {
            if (len <= 5) {
                while (len < 5) {
                    cols.push({})
                    len++
                }
                r4.addView({ id: "invs:r5", cols: cols }, 0)
                r4.addView({ id: "invs:r6", hidden: true }, 1)
            }
            else {
                while (len < 10) {
                    cols.push({})
                    len++
                }
                r4.addView({ id: "invs:r5", cols: cols.splice(0, len / 2) }, 0)
                r4.addView({ id: "invs:r6", cols: cols }, 1)
            }
        }
        else {
            r4.addView({ id: "invs:r5", hidden: true }, 0)
            r4.addView({ id: "invs:r6", hidden: true }, 1)
        }

        //HLV onChange chon ServCode [c2:servCode, c7:servContent, c8:servName]
        if (ENT == "bvb") {
            $$("c3").define("required", false)
        }

        if (ENT == "hlv") {
            // ["c7", "c8"].map((e) => $$(e).disable())
            $$("c2").attachEvent("onChange", (newv, oldv) => {
                if (!newv) ["c7", "c8"].map((e) => $$(e).setValue())
                webix.ajax(`api/kat/payments`).then(result => {
                    let json = result.json(), val = $$("c2").getValue()
                    let arr = json.filter((a) => { return a.name == val })
                    if (arr.length > 0) {
                        let item = arr[0], des = JSON.parse(item.des), name = des.servicename, content = des.servicecontent
                        $$("c7").setValue(content)
                        $$("c8").setValue(name)
                    }
                })
            })
        }
        //

    })

}
class NoteWin extends JetView {

    config() {

        let eFormNote =
            [
                { id: "noteV", name: "noteV", label: _("note"), view: "text", attributes: { maxlength: 255 }, gravity: 3 },
                {//R2
                    cols: [
                        {},
                        { view: "button", id: "note:btntrashAll", type: "icon", icon: "mdi  mdi-delete-varian", label: _("wait_cancel_all"), gravity: 1 },
                        {}

                    ]
                }


            ]
        return {
            view: "window",
            id: "invs:note",
            width: 500,
            position: "center",
            resize: true,
            modal: true,
            head: {
                view: "toolbar", height: 40,
                css: 'toolbar_window',
                cols: [
                    { view: "label", id: "win_lblnote", label: _("wait_cancel_all") },
                    { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => { this.getRoot().hide() } }
                ]
            }
            ,
            body: {
                view: "form",
                id: "invs:formNote",
                padding: 3,
                margin: 3,
                elementsConfig: { labelWidth: 60 },
                elements: eFormNote,
                rules: {
                    note: webix.rules.isNotEmpty
                }
            }
        }
    }
    show() {
        this.getRoot().show()
    }
}
class DetailWin extends JetView {
    config() {
        let gridcol = [
            { id: "id", header: { text: "ID", css: "header" }, css: "right", width: 50, adjust: true },
            { id: "name", header: { text: _("catalog_items"), css: "header" }, adjust: true, fillspace: 4 },
            { id: "status", header: { text: _("status"), css: "header" }, sort: "server", collection: TRSTATUS(this.app), width: 150, adjust: true },
            { id: "trantype", header: { text: _("tran_type"), css: "header" }, adjust: true },
            { id: "valueDate", header: { text: _("tran_date"), css: "header" }, adjust: true },
            { id: "vrt", header: { text: _("vrt"), css: "header" }, css: "right", adjust: true, collection: VAT(this.app) },
            { id: "amount", header: { text: _("amount"), css: "header" }, css: "right", adjust: true, format: gridnfconf.format },
            { id: "total", header: { text: _("dtl_total"), css: "header" }, css: "right", adjust: true, format: gridnfconf.format }
        ]
        return {
            view: "window",
            id: "invs:detailWin",
            // height: 300,
            width: 1100,
            position: "center",
            resize: true,
            modal: true,
            head: {
                view: "toolbar", height: 40,
                css: 'toolbar_window',
                cols: [
                    {
                        view: "label",
                        id: "win_lbl2",
                        label: _("dlt_tran")
                    },
                    {
                        view: "button",
                        type: "icon",
                        icon: "wxi-close",
                        hotkey: 'esc',
                        width: 30,
                        click: () => {
                            this.getRoot().hide()
                        }
                    }
                ]
            },
            body: {
                view: "datatable",
                id: "invs:dataDetail",
                select: "row",
                multiselect: false,
                resizeColumn: true,
                // pager: "invs:pager",
                columns: gridcol
            }
        }
    }
    show() {
        this.getRoot().show()
    }
}
class DesWin extends JetView {

    config() {

        let eForm3 =
            [
                { //R1
                    cols: [
                        { id: "invs:btnMonth", name: "month", label: _("month"), view: "combo", options: PERIODS, value: "1" },
                        { id: "invs:cbYear", name: "year", label: _("year"), view: "combo", options: [] }
                    ]
                },
                {//R2
                    cols: [
                        { view: "button", id: "invs:btnImpdes", type: "icon", icon: "wxi-download", label: _("detail_statistic"), hidden: false },
                    ]
                }

            ]
        return {
            view: "window",
            id: "invs:win",
            height: 300,
            width: 450,
            position: "center",
            resize: true,
            modal: true,
            head: {
                view: "toolbar", height: 40,
                css: 'toolbar_window',
                cols: [
                    { view: "label", id: "win_lbl2", label: _("dlt_tran") },
                    { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => { this.getRoot().hide() } }
                ]
            }
            ,
            body: {
                view: "form",
                id: "invs:formDes",
                padding: 3,
                margin: 3,
                elementsConfig: { labelWidth: 85 },
                elements: eForm3,
                rules: {
                    month: webix.rules.isNotEmpty,
                    year: webix.rules.isNotEmpty
                }
            }
        }
    }
    show() {
        this.getRoot().show()
    }
}
class IgnsWin extends JetView {

    config() {

        let eForm1 =
            [
                { //R1
                    cols: [
                        { id: "invs:fdp", name: "fd", label: _("fd"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr, required: true },
                        { id: "invs:tdp", name: "td", label: _("td"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr, required: true }
                    ]
                }
                ,
                {//R2
                    cols: [
                        { id: "invs:statussub", name: "status", label: _("status"), view: "combo", options: LSD, required: true },
                        {}
                    ]
                }
                ,
                {//R2
                    cols: [
                        { view: "button", id: "invs:btnReportp", type: "icon", icon: "mdi mdi-file-excel", label: _("report"), hidden: true }
                    ]
                }

            ]
        return {
            view: "window",
            id: "igns:win",
            height: h,
            width: w,
            position: "center",
            resize: true,
            modal: true,
            head: {
                view: "toolbar", height: 40,
                css: 'toolbar_window',
                cols: [
                    { view: "label", id: "win_lbl", label: _("dlt_tran") },
                    { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => { this.getRoot().hide() } }
                ]
            }
            ,
            body: {
                view: "form",
                id: "trans:form2",
                padding: 3,
                margin: 3,
                elementsConfig: { labelWidth: 85 },
                elements: eForm1
                , rules: {
                    fd: webix.rules.isNotEmpty,
                    td: webix.rules.isNotEmpty,
                    $obj: function (data) {
                        const fd = data.fdp, td = data.tdp
                        if (fd > td) {
                            webix.message(_("date_invalid_msg"))
                            return false
                        }
                        const dtime = (new Date(td)).getTime() - (new Date(fd)).getTime()
                        if (dtime > 31622400000) {
                            webix.message(_("dt_search_between_year"))
                            return false
                        }
                        return true
                    }
                }
            }
        }
    }
    show() {
        this.getRoot().show()
    }
}
class DelFile extends JetView {

    config() {
        let gridcolD = [
            { id: "id", header: { text: "ID", css: "header" }, css: "right", sort: "server", hidden: true },
            { id: "name", header: { text: _("file_name"), css: "header" }, sort: "server", width: 440 },
            { id: "del", header: { text: "<span class='mdi mdi-delete-variant' title='Delete'></span>", css: "header" }, width: 40, template: `<span class='mdi mdi-delete', title='${_("delete")}'></span>`, align: "center" },
        ]
        let gridDel = {
            view: "datatable",
            id: "invs:gridDel",
            select: "row",
            multiselect: false,
            onContext: {
                webix_view: function (e, id) {
                    if (id && id.row) this.select(id)
                }
            },
            columns: gridcolD,
            onClick:
            {
                "mdi-delete": function (ev, id) {
                    arrID.push(id.row)
                    this.remove(id)
                }

            }

        }
        return {
            view: "window",
            id: "invs:del",
            height: 500,
            width: 500,
            position: "center",
            head: {
                view: "toolbar", height: 40,
                css: 'toolbar_window',
                cols: [
                    { view: "label", id: "win_lbl", label: _("delete_file") },
                    {
                        view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => {
                            this.getRoot().hide()
                            arrID = []
                        }
                    }
                ]
            }
            ,
            body: {
                rows: [
                    gridDel,
                    {
                        cols: [
                            { view: "button", id: "invs:btnSaveDel", type: "icon", icon: "mdi mdi-content-save", label: _("save") },
                            { view: "button", id: "invs:btnExitDel", type: "icon", icon: "wxi-close", label: _("logout") }
                        ]
                    }
                ]
            }
        }
    }
    show() {
        this.getRoot().show()
    }
}
class Uploadfile extends JetView {

    config() {

        let UploadForm =
            [

                {
                    cols: [
                        { id: "upload:file", view: "uploader", multiple: true, autosend: false, link: "upload:filelist", accept: (ENT == "vib") ? "text/plain" : "application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/pdf", upload: "api/inv/minutes/upload", type: "icon", icon: "mdi mdi-file-find-outline", label: _("file_select") },
                        {},
                    ]
                },
                { view: "list", id: "upload:filelist", type: "uploader", autoheight: true, borderless: true, hidden: false },
                {
                    cols: [
                        { view: "button", id: "invs:btnSaveFile", type: "icon", icon: "mdi mdi-content-save", label: _("yes") },
                        { view: "button", id: "invs:btnExitFile", type: "icon", icon: "wxi-close", label: _("logout"), click: () => { $$("invs:upload").hide() } }
                    ]
                }
            ]
        return {
            view: "window",
            id: "invs:upload",
            height: 500,
            width: 500,
            position: "center",
            head: {
                view: "toolbar", height: 40,
                css: 'toolbar_window',
                cols: [
                    { view: "label", id: "win_lbl", label: _("file_upload") },
                    {
                        view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => {
                            this.getRoot().hide()

                        }
                    }
                ]
            }
            ,
            body: {
                rows: UploadForm
            }
        }
    }
    show() {
        this.getRoot().show()
    }
}
class EditDate extends JetView {

    config() {

        let eFormED =
            [
                {
                    cols: [
                        { view: "label", label: "<b>ID hóa đơn: </b>", required: true, width: 100, align: "center" },
                        { id: "invs:envName", name: "idEnv", view: "label", label: "Label", required: true }
                    ]
                },
                {
                    cols: [
                        { id: "invs:dateOld", name: "dateOld", label: _("inv_date_old"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr, required: true, labelWidth: 100, gravity: 2, disabled: true },
                    ]
                },
                { //R1
                    cols: [
                        { id: "invs:dateChange", name: "dateC", label: _("inv_date_new"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr, required: true, labelWidth: 100, gravity: 2 }
                    ]
                },
                {//R2
                    cols: [
                        { view: "button", id: "invs:btnExitD", type: "icon", icon: "wxi-close", label: _("logout"), css: "btnExitINV" },
                        { view: "button", id: "invs:btnSaveD", type: "icon", icon: "mdi mdi-content-save", label: _("save") }
                    ]
                }

            ]
        return {
            view: "window",
            id: "invs:winED",
            height: h,
            width: 300,
            position: "center",
            head: {
                view: "toolbar", height: 3,
                css: 'toolbar_window',
                cols: [
                    {}
                ]
            }
            ,
            body: {
                view: "form",
                id: "invs:formED",
                padding: 3,
                margin: 3,
                elementsConfig: { labelWidth: 85 },
                elements: eFormED
                , rules: {
                    dateC: webix.rules.isNotEmpty
                }
            }
        }
    }
    show() {
        this.getRoot().show()
    }
}

// class EditStatus extends JetView {

//     config() {

//         let eFormES =
//             [
//                 {
//                     cols: [
//                         { view: "label", label: "<b>ID hóa đơn: </b>", required: true, width: 100, align: "center" },
//                         { id: "invs:envName2", name: "idEnc", view: "label", label: "Label", required: true }
//                     ]
//                 },
//                 {
//                     cols: [
//                         { id: "invs:statusES", name: "statusES", label: _("status"), view: "combo", options: ISTATUS(this.app), gravity:2, required: true,value:"1" }
//                     ]
//                 },
//                 {//R2
//                     cols: [
//                         { view: "button", id: "invs:btnSaveS", type: "icon", icon: "mdi mdi-content-save", label: _("save") },
//                         { view: "button", id: "invs:btnExitS", type: "icon", icon: "wxi-close", label: _("logout") }
//                     ]
//                 }

//             ]
//         return {
//             view: "window",
//             id: "invs:winES",
//             height: h,
//             width: 300,
//             position: "center",
//             resize: true,
//             modal: true,
//             head: {
//                 view: "toolbar", height: 40,
//                 css: 'toolbar_window',
//                 cols: [
//                     { view: "label", id: "win_lbl", label: _("edit_env_status") },
//                     { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => { this.getRoot().hide() } }
//                 ]
//             }
//             ,
//             body: {
//                 view: "form",
//                 id: "invs:formES",
//                 padding: 3,
//                 margin: 3,
//                 elementsConfig: { labelWidth: 85 },
//                 elements: eFormES
//                 , rules: {
//                     statusES: webix.rules.isNotEmpty
//                 }
//             }
//         }
//     }
//     show() {
//         this.getRoot().show()
//     }
// }

export default class InvsView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        webix.protoUI({ name: "dependent", $cssName: "richselect" }, LinkedInputs2, webix.ui.richselect)
        webix.proxy.invs = {
            $proxy: true,
            load: function (view, params) {
                let obj = $$("invs:form").getValues()
                Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                if (!params) params = {}
                params.filter = obj
                return webix.ajax(this.source, params)
            }
        }
        const viewhd = (id) => {
            try {
                webix.message(_("waiting_processing"), "info", -1, "exp")
                show_progress_icon(1)
                const ids = String(id).split(",")
                if (ids.length > 1) {
                    viewrels(id)
                } else {
                    webix.ajax(`api/sea/htm/${id}`).then(result => {
						//  const json = result.json(), status = json.status
                        //  const req = createRequest(json)
                        //  jsreport.renderAsync(req).then(res => {
                        //  b64 = res.toDataURI()
                        //  webix.ajax().post("api/seek/report", req).then(res => {
                        let {pdf, sizePDF, status} = result.json()
                        b64=pdf
                        if (sizePDF >= 3145728) webix.message(String(_("file_pdf_size_limit")).replace("#hunglq#", 3145728 / (1024 * 1024)))
                        const blob = dataURItoBlob(pdf);

                        var temp_url = window.URL.createObjectURL(blob);

                        $$("iwin:ipdf").define("src", temp_url)
                        $$("iwin:win").show()
                        webix.message.hide("exp");
                        $$("iwin:btnxml").show()
                        if (status == 3) {
                            //HDB sẽ ẩn ký pdf
                            if (ENT != "hdb") $$("iwin:btnsignpdf").show()
                            if (ROLE.includes('PERM_SEARCH_INV_VIEW_SIGN_PDF')) $$("iwin:btnsignpdf").enable()
                            if (ROLE.includes('PERM_SEARCH_INV_VIEW_XML')) {
                                $$("iwin:btnxml").enable()
                            }

                            webix.message.hide("exp");
                            $$("invs:form").hideProgress()
                        }
                        else {
                            $$("iwin:btnsignpdf").hide()
                            $$("iwin:btnxml").hide()
                            webix.message.hide("exp");
                            $$("invs:form").hideProgress()
                        }
                        if (pdf.status == 2 && ROLE.includes("PERM_APPROVE_INV_MANAGE")) $$("iwin:btnapp").show()
                        else $$("iwin:btnapp").hide()
                        webix.message.hide("exp");

                        $$("invs:form").hideProgress()
                        // })
                    })
                }
            } catch (err) {
                $$("invs:form").hideProgress()
                webix.message(err.message, "error")
            }
        }
        function show_progress_icon(delay) {
            $$("invs:form").showProgress({
                type: "icon",
                delay: delay,
                hide: false
            })
        };
        
        const viewrel = (id) => {
            webix.ajax(`api/sea/rel/${id}`).then(result => {
                let json = result.json()
                if (json.length <= 0) this.webix.message(_("not_found_similar_invoice"))
                else {
                    $$("rwin:grid").clearAll()
                    $$("rwin:grid").parse(json)
                    $$("rwin:win").show()
                }
            })
        }
        const viewwno = (id) => {
            webix.ajax(`api/sea/relwno/${id}`).then(result => {
                let json = result.json()
                if (json.length <= 0) this.webix.message(_("not_found_wrong_notice_similar"))
                else {
                    $$("wwin:grid").clearAll()
                    $$("wwin:grid").parse(json)
                    $$("wwin:win").show()
                }
            })
        }
        const viewbth = (id) => {
            webix.ajax(`api/bth/searchInvAdj/${id}`).then(result => {
                let json = result.json()
                if (json.length <= 0) this.webix.message(_("pivot_table_not_found"))
                else {
                    this.BTHwin.show({
                        viewCQT: true,
                        grid: json,
                    })
                }
            })
        }
        const viewrels = (ids) => {
            webix.ajax(`api/sea/rels/${ids}`).then(result => {
                let json = result.json()
                if (json.length <= 0) this.webix.message(_("not_found_similar_invoice"))
                else {
                    $$("rwin:grid").clearAll()

                    $$("rwin:grid").parse(json)
                    $$("rwin:win").show()
                    $$("rwin:winlb").define("label", _("invoice_similars"));
                    $$("rwin:winlb").refresh();
                }
            })
        }
        const downloadMinuteAdj = (id) => {
            $$("invs:grid").disable()
            webix.message(`${_("Downloading")}.......`, "info", -1, "downloading")
            if (SESS_STATEMENT_SENTYPE == 1) {
                webix.ajax().response("blob").get(`api/inv/minutes/listinvadj/${id}`).then(data => {
                    webix.html.download(data, `BB_DieuChinh_${id}.docx`)
                    $$("invs:grid").enable()
                    webix.message.hide("downloading");
                }).fail(err => {
                    $$("invs:grid").enable()
                    webix.message.hide("downloading");
                })
            }
            else {
                webix.ajax().response("blob").get(`api/inv/minutes/wnadj/${id}`).then(data => {
                    webix.html.download(data, `BB_DieuChinh_${id}.docx`)
                    $$("invs:grid").enable()
                    webix.message.hide("downloading");
                }).fail(err => {
                    $$("invs:grid").enable()
                    webix.message.hide("downloading");
                })
            }
        }

        let pager = { view: "pager", width: 340, id: "invs:pager", size: size, template: `{common.first()}{common.prev()} {common.pages()}{common.next()}{common.last()}`,
            // on: {
            //     onBeforePageChange:function(id){
            //         let rows = $$("invs:grid").serialize(), flag = false
            //         for (let row of rows) {
            //             if (row && row.chk)  flag = true
            //         }
            //         if(flag)
            //             $$("invs:grid").getHeaderContent("chk1").uncheck()
            //     }
            // }
        }
        const cols = [
            
            { id: "id", header: "ID" },
            { id: "idt", header: _("invoice_date"), exportType: "string" },
            { id: "form", header: _("form") },
            { id: "serial", header: _("serial") },
            { id: "seq", header: _("seq"), exportType: "string" },
            { id: "status", header: _("status") },
            { id: "status_received", header: _("status_cqt") },
            { id: "btax", header: _("taxcode"), exportType: "string" },
            { id: "bname", header: _("bname") },
            { id: "buyer", header: _("buyer") },
            { id: "bmail", header: _("bmail") },
            { id: "baddr", header: _("address") },
            { id: "sumv", header: _("sumv"), exportType: "number", exportFormat: gridnf0.format },
            { id: "vatv", header: "VAT", exportType: "number", exportFormat: gridnf0.format },
            { id: "totalv", header: _("totalv"), exportType: "number", exportFormat: gridnf0.format },
            { id: "curr", header: _("curr") },
            { id: "sec", header: _("search_code") },
            { id: "ou", header: _("ou") },
            { id: "uc", header: _("creator") },
            { id: "ic", header: "IC", exportType: "string" },
            { id: "note", header: _("note") },
            { id: "bacc", header: _("bacc") },
            { id: "code", header: _("gns_code") },
            { id: "th_type_bth", header: _("th_type_bth") },
            { id: "error_van_message", header: _("error_van_message") },
            { id: "cdt", header: _("cancellation_date")  },
            
            //SESS_STATEMENT_SENTYPE == 1 ? { id: "list_invid", header: _("id_bth") } : {}
        ]
        if (ENT == "vib") {
            cols.push(
                { id: "bacc", header: _("bacc") },
                { id: "code", header: _("gns_code") },
                { id: "name", header: _("gns_name") },
                { id: "sendmailstatus", header: _("mail_status_label") },
                { id: "sendmaildate", header: _("mail_date_label") },
                { id: "sendmailnum", header: _("mail_num_label") },
                { id: "sendsmsstatus", header: _("sms_status_label") },
                { id: "sendsmsdate", header: _("sms_date_label") },
                { id: "sendsmsnum", header: _("sms_num_label") }
            )
        }
        let totalheader
        if (ENT == 'mzh') {
            totalheader = _("totalo")
        }
        if (ENT == 'vib') {
            totalheader = _("totalv")
        }
        //tải biên bản
        let gridcol = [
            { id: "vie", header: { text: "<span class='webix_icon wxi-download' title='excel'></span>", css: "header" }, width: 35, template: `<span class='webix_icon wxi-search', title='${_("view")}'></span>` },
            {
                id: "sel", header: "", width: 35, template: (obj, common) => {
                    let checkapiinvoice = 1
                    try {
                        if (["hsy"].includes(ENT)) {
                            if (userapi && userapi != "" && String(userapi).split(",").includes(obj.uc)) checkapiinvoice = 0
                        }
                    } catch (e) { }
                    if ((obj.status <= 2) && (checkapiinvoice != 0)) {
                        if (["vib"].includes(ENT) && !ROLE.includes('PERM_EDIT_INV_MANAGE'))
                            return ""
                        else
                            return `<span class='webix_icon wxi-pencil', title='${_("edit_invoice")}'></span>`
                    }
                    else { return "" }
                }
            },
            { id: "chk", header: { content: "masterCheckbox", css: "center" }, css: "center", template: "{common.checkbox()}", width: 45, hidden:true },
            {
                id: "id", header: { text: "ID", css: "header" }, css: "right", sort: "server", adjust: true, template: (obj, common) => {

                    if (["vib"].includes(ENT) && (obj.id_file) && (obj.id_file != '')) {
                        return `<span class='mdi mdi-attachment'>${obj.id}</span>`
                    }
                    else { return `<span>${obj.id}</span>` }
                }
            },
            { id: "idt", header: { text: _("invoice_date"), css: "header" }, sort: "server", format: d2s, adjust: true },
            { id: "form", header: { text: _("form"), css: "header" }, sort: "server", adjust: true },
            { id: "serial", header: { text: _("serial"), css: "header" }, sort: "server", adjust: true },
            { id: "seq", header: { text: _("seq"), css: "header" }, sort: "server", adjust: true },
            { id: "status", header: { text: _("status"), css: "header" }, sort: "server", collection: ISTATUS(this.app), adjust: true },
            { id: "status_received", header: { text: _("status_cqt"), css: "header" }, sort: "server", collection: INV_CQT_STATUS(this.app), adjust: true, width : 120 },
            { id: "btax", header: { text: _("taxcode"), css: "header" }, sort: "server", adjust: true},
            { id: "bname", header: { text: _("bname2"), css: "header" }, sort: "server", adjust: true },
            { id: "buyer", header: { text: _("buyer"), css: "header" }, sort: "server", adjust: true},
            { id: "btel", header: { text: _("tel"), css: "header" }, sort: "server", css: "right", adjust: true, format: "#,###" },
            { id: "bmail", header: { text: _("bmail"), css: "header" }, sort: "server", adjust: true },
            { id: "baddr", header: { text: _("address"), css: "header" }, sort: "server", adjust: true },
            ["vib", "mzh"].includes(ENT) ? { id: "sum", header: { text: _("sumo"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat } : { id: "sumv", header: { text: _("sumv"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.format },
            ["vib", "mzh"].includes(ENT) ? { id: "vat", header: { text: _("vato"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat } : { id: "vatv", header: { text: _("vat"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.format },
            ["vib", "mzh"].includes(ENT) ? { id: "total", header: { text: totalheader, css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat } : { id: "totalv", header: { text: _("totalv"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.format },
            { id: "curr", header: { text: _("curr"), css: "header" }, sort: "server", adjust: true },
            { id: "sec", header: { text: _("search_code"), css: "header" }, format: webix.template.escape, sort: "server", adjust: true },
            { id: "ou", header: { text: _("ou"), css: "header" }, sort: "server", collection: "api/cat/nokache/ou", adjust: true },
            { id: "uc", header: { text: _("creator"), css: "header" }, sort: "server", adjust: true},
            { id: "ic", header: { text: "IC", css: "header" }, sort: "server", adjust: true },
            { id: "note", header: { text: _("note"), css: "header" }, sort: "server", adjust: true },
            { id: "bcode", header: { text: _("customer_code2"), css: "header" }, sort: "server", adjust: true },
            { id: "bacc", header: { text: _("bacc"), css: "header" }, sort: "server", adjust: true },
            { id: "cdt", header: { text: _("cancellation_date"), css: "header" }, sort: "server", format: d2s, adjust: true },
        ]
        if (ENT == 'ctbc') {
            gridcol.push(
                { id: "bcode", header: { text: _("customer_code2"), css: "header" }, sort: "server", adjust: true },
                { id: "bacc", header: { text: _("bacc"), css: "header" }, sort: "server", adjust: true }
            )
        }
        if (ENT == 'vib') {
            gridcol.push({ id: "bacc", header: { text: _("bacc"), css: "header" }, sort: "server", adjust: true })
            gridcol.push({ id: "itemcodes", header: { text: _("itemcodes_vib"), css: "header" }, sort: "server", adjust: true })
            gridcol.push({ id: "itemnames", header: { text: _("itemnames_vib"), css: "header" }, sort: "server", adjust: true })
        }
        if (ENT == 'hdb') {
            gridcol.splice(13, 3) //Xóa cột tiền VND
            gridcol.splice(13, 0,
                { id: "sum", header: { text: _("sumo"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat },
                { id: "vat", header: { text: _("vato"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat },
                { id: "total", header: { text: _("totalo"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat })
            cols.splice(11, 3) //Xóa cột tiền VND
            cols.splice(11, 0,
                { id: "sum", header: _("sumo"), exportType: "number", exportFormat: gridnf0.numberFormat },
                { id: "vat", header: "vato", exportType: "number", exportFormat: gridnf0.numberFormat },
                { id: "total", header: _("totalo"), exportType: "number", exportFormat: gridnf0.numberFormat },
            )
        }
        if (["fhs", "fbc"].includes(ENT)) {

            gridcol.push({ id: "coverprice", header: { text: _("coverprice"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat })
        }
        if (!['aia'].includes(ENT))
            gridcol.splice(13, 1)

        if (['vib', 'aia', 'sgr'].includes(ENT)) {

            gridcol.push({ id: "sendmailstatus", header: { text: _("mail_status_label"), css: "header" }, sort: "server", collection: MAILSTATUS(this.app), adjust: true })
            gridcol.push({ id: "sendmaildate", header: { text: _("mail_date_label"), css: "header" }, sort: "server", format: t2s, adjust: true })
            gridcol.push({ id: "sendmailnum", header: { text: _("mail_num_label"), css: "header" }, sort: "server", css: "right", adjust: true, format: "#,###" })
            if (['vib', 'aia'].includes(ENT)) {
                gridcol.push({ id: "sendsmsstatus", header: { text: _("sms_status_label"), css: "header" }, sort: "server", collection: SMSSTATUS(this.app), adjust: true })
                gridcol.push({ id: "sendsmsdate", header: { text: _("sms_date_label"), css: "header" }, sort: "server", format: t2s, adjust: true })
                gridcol.push({ id: "sendsmsnum", header: { text: _("sms_num_label"), css: "header" }, sort: "server", css: "right", adjust: true, format: "#,###" })
            }
        }
        if (ENT == 'mzh') {

            gridcol.push({
                id: "exrt", header: { text: _("exchange_rate"), css: "header" }, sort: "server", adjust: true
            })
            gridcol.push({
                id: "vrn", header: { text: _("vrt"), css: "header" }, sort: "server", adjust: true
            })
            gridcol.push({
                id: "dds_sent", header: { text: _("stt_dds_sent"), css: "header" }, sort: "server", adjust: true, collection: DDS
            })
            gridcol.push({
                id: "dds_stt", header: { text: _("stt_dds"), css: "header" }, sort: "server", adjust: true, collection: DDST
            })
            gridcol.push({
                id: "filen", header: { text: _("file_name"), css: "header" }, sort: "server", adjust: true
            })


        }
        if (ENT == 'hlv') {
            gridcol.push({
                id: "adjdes", header: { text: _("replace"), css: "header" }, sort: "server", adjust: true, template: obj => {
                    if (obj.pid) return `<span class='mdi mdi-file-replace-outline'>${obj.adjdes}</span>`
                    else return ""
                }
            })
            gridcol.push({
                id: "cde", header: { text: _("repadjed"), css: "header" }, sort: "server", adjust: true, template: obj => {
                    if (obj.cde) return `<span class='mdi mdi-file-replace'>${obj.cde}</span>`
                    else return ""
                }
            })
            cols.push({ id: "adjdes", header: _("replace") })
            cols.push({ id: "cde", header: _("replaced") })
        }
        else {
            if (ENT == 'bvb') {
                gridcol.push({ id: "btel", header: { text: _("sdt"), css: "header" }, sort: "server", adjust: true }),
                    gridcol.push({ id: "magd", header: { text: _("tran_no_l"), css: "header" }, sort: "server", adjust: true, hidden: (ENT == "bvb") ? true : false })
                gridcol.push({ id: "bcode", header: { text: _("customer_code2"), css: "header" }, sort: "server", adjust: true }),
                    gridcol.push({ id: "bacc", header: { text: _("bacc"), css: "header" }, sort: "server", adjust: true }),
                    gridcol.push({ id: "sum", header: { text: _("amount_tr_nt"), css: "header" }, sort: "server", adjust: true }),
                    gridcol.push({ id: "vat", header: { text: _("vat_nt"), css: "header" }, sort: "server", adjust: true }),
                    gridcol.push({ id: "total", header: { text: _("dtl_total_nt"), css: "header" }, sort: "server", adjust: true })
            }
            gridcol.push({
                id: "adjdes", header: { text: _("repadj"), css: "header" }, sort: "server", adjust: true, template: obj => {
                    if (obj.pid) return `<span class='mdi mdi-file-replace-outline'>${obj.adjdes}</span>`
                    else if (obj.adjdif == 1) return `<span>${obj.adjdes}</span>`
                    else return ""
                }
            })
            gridcol.push({
                id: "cde", header: { text: _("repadjed"), css: "header" }, sort: "server", adjust: true, template: obj => {
                    if (obj.cid) return `<span class='mdi mdi-file-replace'>${obj.cde}</span>`
                    else return ""
                }
            })
            cols.push({ id: "adjdes", header: _("repadj") })
            cols.push({ id: "cde", header: _("repadjed") })
            if(["123"].includes(DEGREE_CONFIG)) {
                gridcol.push(
                    { id: "sendmailstatus", header: { text: _("mail_send"), css: "header" }, sort: "server", adjust: true }
                )
                cols.push({ id: "sendmailstatus", header: _("mail_send") })
				//loccx2 13/3/2023 Bổ sung kết xuất excel ra mã gd đối với hóa đơn chi tiết và không mã 
                cols.push({ id: "description", header: _("description") })
            }
            gridcol.push(
                { id: "status_tbss",header: { text: _("status_tbss"), css: "header" }, sort: "server", css: "center", adjust: true,hidden:SESS_STATEMENT_SENTYPE==1?true:false, template: (obj, common) => (obj.status_tbss) ? `<span class='mdi mdi-note-text', title='${_("find_wno_similar")}'>${WRONGNOTICE_CQT_STATUS(this.app).find(item => item.id == obj.status_tbss).value}</span>` : "" }
            )
            gridcol.push(
                { id: "wno_adj",header: { text: _("wno_adj"), css: "header" }, sort: "server", css: "center", adjust: true,hidden:SESS_STATEMENT_SENTYPE==1?true:false, template: (obj, common) => (obj.wno_adj && (obj.wno_adj == 1 || obj.wno_adj == 2 || obj.wno_adj == 3 ||  obj.wno_adj == 4 || obj.wno_adj == 5)) ? (obj.wnadjtype && obj.wnadjtype == 4 ? `${NOTI_TYPE_COMBO2.find(item => item.id == obj.wno_adj).value}  <span class='mdi mdi-download', title='${_("minutes_download_5")}'></span>` : `${NOTI_TYPE_COMBO2.find(item => item.id == obj.wno_adj).value}`) : "" }
            )
        }
        gridcol.push({ id: "list_invid", header: { text: _("id_bth"), css: "header" }, sort: "server", adjust: true,hidden:SESS_STATEMENT_SENTYPE==1?false:true })
        // gridcol.push({ 
        // id: "th_type", header: { text: _("th_type_bth"), css: "header" }, sort: "server", adjust: true,hidden:SESS_STATEMENT_SENTYPE==1?false:true, template: obj => {
        //     if (obj.th_type == 1) return `<span class='mdi mdi-file-multiple'>${_("th_type_dctt")}</span>   <span class='mdi mdi-download' title='${_("minutes_download_5")}'></span>`
        //     else if (obj.th_type == 2) return `<span class='mdi mdi-file-document'>${_("th_type_gt")}</span>`
        //     else return ""
        // }
        // })
        gridcol.push({ id: "th_type", header: { text: _("bthwin"), css: "header" }, css: "center", sort: "server", adjust: true, hidden:SESS_STATEMENT_SENTYPE==1?false:true, template: (obj, common) => (obj.th_type) ? `<span class='mdi mdi-file-document', title='${_("find_similar")}'></span>` : "",  })
        // Bổ sung thêm cột Mô tả lỗi khi TVAN trả về
        gridcol.push({ id: "error_msg_van", header: { text: _("error_van_message"), css: "header" }, sort: "server", adjust: true, maxWidth: 150, tooltip: "#error_msg_van#" },)

        let grid = {
            view: "datatable",
            id: "invs:grid",
            multiselect: false,
            select: "row",
            resizeColumn: true,
            tooltip:true,
            pager: "invs:pager",
            // on:{
            //     onItemClick: function (id, colId, value) {
            //         let row = id.row, item = this.getItem(row)
            //         item.chk = !item.chk
            //         this.refresh(row)
            //     }
            // },
            onContext: {
                webix_view: function (e, id) {
                    if (id && id.row) this.select(id)
                }
            },
            columns: gridcol,
            onClick:
            {
                "wxi-download": function (e, id) {
                    if (ROLE.includes('PERM_SEARCH_INV_EXCEL')) {
                        if (ENT == "aia") {
                            if (!ROLE.includes("PERM_EXPORT_EXCEL")) {
                                webix.message(`${_("invsexp_execl")}`, "error")
                                return false
                            }
                        }
                        webix.message(`${_("export_report")}.......`, "info", -1, "excel_ms");
                        $$("invs:form").disable();
                        $$("invs:form").showProgress({
                            type: "icon",
                            hide: false
                        });

                        let lang = webix.storage.local.get("lang")
                        let obj = $$("invs:form").getValues()
                        let colhd = ""
                        if(ENT == 'ctbc'){
                            cols.push(
                                { id: "id_bth",header: _("id_bth") },
                                { id: "th_type_bth",header: _("th_type_bth") },
                                { id: "error_van_message",header: _("error_van_message") }
                            )
                        }
                        for (const colh of cols) {
                            if (ENT != "vib") {
                                colhd += colh["header"] + ","
                            } else {
                                colhd += colh["id"] + "|" + colh["header"] + ","
                            }
                        }
                        colhd = colhd.substring(0, colhd.length - 1)
                        Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                        if (ENT != "vib") {
                            webix.ajax().response("blob").get("api/sea/xls", { filter: obj, lang: lang, colhd: colhd }).then(data => {
                                webix.html.download(data, (ENT != "vib") ? `${webix.uid()}.xlsx` : `${webix.uid()}.csv`)
                                $$("invs:form").enable()
                                $$("invs:form").hideProgress()
                                webix.message.hide("excel_ms");
                            }).catch(e => {

                                $$("invs:form").enable();
                                $$("invs:form").hideProgress();
                                webix.message.hide("excel_ms");

                            })
                        } else {
                            webix.ajax().get("api/sea/xls", { filter: obj, lang: lang, colhd: colhd }).then(data => {
                                $$("invs:form").enable()
                                $$("invs:form").hideProgress()
                                webix.message.hide("excel_ms");
                                const result = data.json()
                                if (result.err)
                                    webix.alert({
                                        ok: "Ok",
                                        text: result.err
                                    })
                                else {
                                    const blob = new Blob([result.data])
                                    webix.html.download(blob, `${webix.uid()}.csv`)
                                }
                            }).catch(e => {

                                $$("invs:form").enable();
                                $$("invs:form").hideProgress();
                                webix.message.hide("excel_ms");

                            })
                        }
                    } else webix.message(_("no_authorization"), "error")

                },
                "wxi-pencil": function (e, id) {
                    if (ROLE.includes('PERM_SEARCH_INV_UPDATE')) {
                        const rec = this.getItem(id), adjtyp = rec.adjtyp
                        let url = adjtyp == 2 ? `inv.adj?id=${id}&action=view` : `inv.inv?id=${id}`
                        if (rec.adjdif == 1) url = `inv.inv?id=${id}&action=adjdif`
                        this.$scope.show(url)
                    } else webix.message(_("no_authorization"), "error")
                },
                "wxi-search": function (e, r) {
                    if (ROLE.includes('PERM_SEARCH_INV_VIEW')) viewhd(r.row)
                    else webix.message(_("no_authorization"), "error")
                },
                "wxi-drag": function (e, r) {
                    if (ROLE.includes('PERM_SEARCH_INV_SEARCH_RELATE')) viewrel(r.row)
                    else webix.message(_("no_authorization"), "error")
                },
                "mdi-note-text": function (e, r) {viewwno(r.row)},
                "mdi-file-replace-outline": function (e, r) { viewhd(this.getItem(r).pid) },
                "mdi-file-replace": function (e, r) { viewhd(this.getItem(r).cid) },
                "mdi-attachment": function (e, r) {
                    idDel = r
                    let gridDel = $$("invs:gridDel")
                    gridDel.clearAll()
                    gridDel.loadNext(10, 0, null, `invs->api/inv/getID/${r}`, true)
                    $$("invs:del").show()
                },
                "mdi-file-document": function (e, r) {
                    viewbth(this.getItem(r).id)
                },
                // "mdi-file-multiple": function (e, r) {
                //     viewbth(this.getItem(r).id)
                // },
                "mdi-download": function (e, r) {
                    downloadMinuteAdj(r.row)
                }
            }
        }

        // const url_ou = ENT == "hdb" ? "api/cat/bytoken" : "api/cat/nokache/ou"
        let url_ou = "api/ous/bytokenou"  // tât ca deu load theo chi nhanh
        if (ENT == "sgr") url_ou = "api/ous/byousgr"
        let eForm =
            [
                { //R1
                    cols: [
                        { id: "invs:fd", name: "fd", label: _("fd"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr },
                        { id: "invs:td", name: "td", label: _("td"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr },
                        // {
                        //     cols: [
                        //         { id: "invs:status", name: "status", label: _("status"), view: "combo", options: ISTATUS(this.app) },
                        //         { id: "invs:status_received", name: "status_received", label: _("status_cqt"), view: "combo", options: INV_CQT_STATUS(this.app), hidden: (DEGREE_CONFIG == "123") ? false : true }
                        //     ]
                        // },
                        { id: "invs:status", name: "status", label: _("status"), view: "combo", options: ISTATUS(this.app) },
                        { id: "invs:status_received", name: "status_received", label: _("status_cqt_short"), view: "combo", options: INV_CQT_STATUS(this.app), hidden: (DEGREE_CONFIG == "123") ? false : true },
                        // { id: "invs:paym", name: "paym", label: _("payment_method"), view: "combo", options: PAYMETHOD(this.app) },
                        {
                            cols: [
                                { id: "invs:curr", name: "curr", label: _("currency"), view: "combo", suggest: { url: "api/cat/kache/currency" }, gravity: 2 },
                                { name: "ic", label: "IC", view: "text", labelWidth: 25, attributes: { maxlength: 36 }, placeholder: "IC" }
                            ]
                        }
                    ]
                },
                {//R2
                    cols: [
                        { id: "invs:type", name: "type", label: _("invoice_type"), view: "combo", placeholder: _("choose_inv_type"), suggest: { data: ITYPE(this.app), fitMaster: false, width: 250 } },//richselect
                        { id: "form", name: "form", label: _("form"), view: "combo", options: [] },
                        {
                            cols: [
                                //{
                                ENT != "vcm" ?//vcm mutiselect
                                    // trung sưa thanh editcombo rào { id: "invs:serial", name: "serial", label: _("serial"), view: "dependent", options: [], master: "form", dependentUrl: "api/sea/asbf?id=", disabled: true, gravity: 1.5 }
                                    { id: "invs:serial", name: "serial", label: _("serial"), view: "combo", options: [], disabled: false }
                                    : { id: "invs:serial", name: "serial", label: _("serial"), view: "multiselect", suggest: { selectAll: true, filter: filter, body: { datasr: [], template: webix.template("#value#") } }, gravity: 1.8 },
                                //}
                                { id: "invs:list_invid", name: "list_invid", label: _("id_bth"), view: "text", placeholder: _("id_bth"), hidden: (SESS_STATEMENT_SENTYPE == 1) ? false : true },
                            ]
                        },
                        
                        // }
                        { name: "seq", label: _("seq"), view: "text", attributes: { maxlength: 8 }, validate: v => { return !v || /^[0-9]{1,8}$/.test(v) }, invalidMessage: _("seq_invalid"), placeholder: _("seq") },
                        { id: "invs:sec", name: "sec", label: _("search_code"), view: "text", attributes: { maxlength: 10 }, placeholder: _("search_code")}

                    ]
                },
                {//R3
                    cols: [
                        !["vcm", "sgr"].includes(ENT) ?//vcm mutiselect
                            { id: "invs:ou", name: "ou", label: _("bname"), view: "combo", suggest: { url: url_ou, filter: filter, relative: "bottom" } }
                            : ['sgr'].includes(ENT) ? {
                                id: "invs:ou", name: "ou", label: _("bname"), view: "multicombo",
                                tagMode: false,
                                tagTemplate: function (values) {
                                    return (values.length + ` ${_("countou")}`);
                                },
                                suggest: { selectAll: true, url: url_ou, filter: filter, template: webix.template("#id#__#value#") },
                                required: true
                            } : {
                                id: "invs:ou", name: "ou", label: _("bname"), view: "multiselect", suggest: { selectAll: true, url: url_ou, filter: filter, template: webix.template("#id#__#value#") }
                            },
                        ENT != "hdb"
                            ? { id: "invs:btax", name: "btax", label: _("taxcode"), view: "text", attributes: { maxlength: ["bvb"].includes(ENT) ? 50 : 14 }, placeholder: _("taxcode")}
                            : { id: "invs:btax", name: "btax", label: _("taxcode"), view: "text", attributes: { maxlength: 25 }, placeholder: _("taxcode")},
                        { id: "invs:bname", name: "bname", label: _("bname2"), view: "text", attributes: { maxlength: (ENT == "vcm") ? 500 : 50 }, placeholder: _("bname")},
                        { id: "invs:buyer", name: "buyer", label: _("buyer"), view: "text", attributes: { maxlength: (ENT == "vcm") ? 500 : 50 }, placeholder: _("buyer") },

                        {
                            cols: [
                                { id: "invs:cvt", name: "cvt", label: _("cvt"), view: "checkbox" },
                                { view: "button", id: "invs:btnsearch", type: "icon", icon: "wxi-search", label: _("search"), width: 85, disabled: ROLE.includes('PERM_SEARCH_INV') ? false : true }
                            ]
                        }
                    ]
                },
                ENT != "hlv" ? { //Row 4
                    cols:
                        [
                            ["scb"].includes(ENT) ? { id: "invs:id", name: "id", label: "ID", view: "text", attributes: { maxlength: 24 } } :
                            { id: "invs:bcode", name: "bcode", label: _("customer_code"), view: "text", attributes: { maxlength: 24 }, hidden: ['mzh', 'bvb', 'kbank'].includes(ENT) ? true : false, placeholder: _("customer_code") },
                            { id: "invs:user", name: "user", label: _("creator"), view: "text", attributes: { maxlength: 24 }, placeholder: _("creator")},//richselect
                            { id: "invs:bacc", name: "bacc", label: _("bacc"), view: "text", placeholder: _("bacc"), attributes: { maxlength: 24 }, hidden: ['kbank'].includes(ENT) ? true : false },
                            { id: "invs:vat", name: "vat", label: _("vrt"), view: "combo", options: VAT(this.app), placeholder: _("vrt") },
                            { id: "invs:adjtyp", name: "adjtyp", label: _("property"), view: "combo", options: IKIND(this.app) },
                            ENT == "mzh" ? {
                                cols: [
                                    { id: "invs:ddst", name: "dds_stt", label: _("stt_dds"), view: "combo", options: DDST },
                                    { id: "invs:dds", name: "dds_sent", label: _("stt_dds_sent"), view: "combo", options: DDS },

                                ]
                            } : { id: "invs:tran", name: "tran", label: _("tran_no_l"), view: "text", hidden: (ENT == "bvb") ? false : true }, { hidden: ['kbank'].includes(ENT) ? false : true }, { hidden: ['kbank'].includes(ENT) ? false : true }
                        ]
                } : { //Row 4
                    cols:
                        [
                            { id: "invs:adjtyp", name: "adjtyp", label: _("property"), view: "combo", options: IKIND(this.app) },//richselect
                            {},
                            {},
                            {}, {}
                        ]
                },
                ['vib', 'aia', 'sgr'].includes(ENT) ? { //R45
                    cols: [
                        { id: "invs:sendmailstatus", name: "sendmailstatus", label: _("mail_status_label"), view: "combo", options: MAILSTATUS(this.app) },
                        ['vib', 'aia'].includes(ENT) ? { id: "invs:sendsmsstatus", name: "sendsmsstatus", label: _("sms_status_label"), view: "combo", options: SMSSTATUS(this.app) }
                            : {}, {}, {}, {}
                    ],
                } : ['bvb'].includes(ENT) ? {
                    cols: [
                        { id: "invs:sendmailstatus", name: "sendmailstatus", label: _("mail_status_label"), view: "combo", options: MAILSTATUS(this.app) },
                        { id: "invs:sendsmsstatus", name: "sendsmsstatus", label: _("sms_status_label"), view: "combo", options: SMSSTATUS(this.app) },
                        { id: "invs:customercode", name: "customercode", label: _("customer_code2"), view: "text", placeholder: _("customer_code2") },
                        {}, {}
                    ],
                } : { height: 1 },
                { //R5
                    cols:
                    [
                        SESS_STATEMENT_SENTYPE == 1 ?
                        { id: "invs:th_type", name: "th_type", label: _("th_type_bth"), view: "combo", options: TH_TYPE, labelWidth: 135 } :
                        { id: "invs:status_tbss", name: "status_tbss", label: _("status_tbss"), view: "combo", options: WRONGNOTICE_CQT_STATUS(this.app), labelWidth: 110 },
                        SESS_STATEMENT_SENTYPE != 1 ? { id: "invs:wno_adj", name: "wno_adj", label: _("wno_adj"), view: "combo", options: NOTI_TYPE_COMBO2, labelWidth: 105 } : {},
                        ENT=="dbs" ?{ name: "inv_Type", label: _("invType"), placeholder: _("invType"), id: "invs:invType", view: "combo",  options: INVTYPE}: {}, {}, {}
                    ]
                },
                {
                    id: "invs:r4", rows: [{ id: "invs:r5", hidden: true }, { id: "invs:r6", hidden: true }]
                }
            ]

        let form = {
            view: "form",
            id: "invs:form",
            padding: 3,
            margin: 3,
            elementsConfig: { labelWidth: 85 },
            elements: eForm
            , rules: {
                fd: webix.rules.isNotEmpty,
                td: webix.rules.isNotEmpty,
                $obj: function (data) {
                    const fd = data.fd, td = data.td, btax = data.btax

                    if (fd > td) {
                        webix.message(_("date_invalid_msg"))
                        return false
                    }
                    if (['vcm'].includes(ENT)) {
                        if (!btax && (String(fd).substr(0, 7) != String(td).substr(0, 7))) {
                            webix.message(_("fd_td_in_month"))
                            return false
                        }
                    }
                    if (['jpm'].includes(ENT)) {
                        if (dtime > ddate*6) {
                            webix.message(String(_("dt_search_between_day")).replace('#hunglq#', (6 * ddate / (1000 * 3600 * 24))))
                            return false
                        }
                    }
                    const dtime = (new Date(td)).getTime() - (new Date(fd)).getTime()
                    if ((ENT == "vib") && (dtime > ddate)) {
                        webix.message(String(_("dt_search_between_day")).replace('#hunglq#', (ddate / (1000 * 3600 * 24))))
                        return false
                    }
                    if (dtime > 31622400000) {
                        webix.message(_("dt_search_between_year"))
                        return false
                    }
                    return true
                }
            }
        }
        //popup
        let popup = {
            view: "popup",
            id: "my_pop",
            width: 300,
            body: {
                cols: [
                    {
                        view: "button", id: "invs:btnReport", type: "icon", icon: "mdi mdi-email-outline", label: _("synthesis_report"), hidden: false, width: 100, click: () => {
                            $$("win_lbl").define("label", _("synthesis_report"))
                            $$("win_lbl").refresh()
                            $$("invs:btnReportp").show()
                            $$("igns:win").show()
                        }
                    },
                    {
                        view: "button", id: "invs:detailS", type: "icon", icon: "wxi-eye", label: _("detail_statistic"), hidden: false, width: 100, click: () => {
                            $$("win_lbl2").define("label", _("detail_statistic"))
                            $$("win_lbl2").refresh()
                            $$("invs:win").show()
                        }
                    }
                ],

            }
        }
        let recpager = { view: "combo", id: "recpager", maxWidth: 70, tooltip: _("row_per_page"), value: 10, options: RPP }
        let title_page = {
            view: "label", label: `<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("invoice_search")}</span>`
        }
        return {
            paddingX: 2,
            rows: [title_page, form, grid,
                {
                    cols: [recpager, pager, { id: "invs:countinv", view: "label" }, {}, {
                        cols: [
                            { view: "button", id: "invs:btndel", type: "icon", icon: "mdi mdi-delete", label: _("invoice_del_btn"), disabled: true, width: 100 },
                            { view: "button", id: "invs:btndel_gd", type: "icon", icon: "mdi mdi-delete", label: _("invoice_del_btn_gd"), hidden: true, disabled: true, width: 130 },
                            { view: "button", id: "invs:btndelAll", type: "icon", icon: "mdi mdi-delete", label: _("del_all"), hidden: (ENT == "scb") ? false : true, disabled: true, width: 100 },
                            { view: "button", id: "invs:btnreset", type: "icon", icon: "mdi mdi-file-restore", label: _("reset_bth"), disabled: true, width: 130, hidden:true },
                            { view: "button", id: "invs:btnmails", type: "icon", icon: "mdi mdi-email-outline", label: _("sendmails"), disabled: true, width: 100 },
                            { view: "button", id: "invs:btnprint", type: "icon", icon: "mdi mdi-printer", label: "Print", disabled: true, width: 65 },
                            { view: "button", id: "invs:download_invs", type: "icon", icon: "mdi mdi-download", label: _("download_invs"), hidden: true, disabled: true, width: 80 }
                        ]
                    }]
                },
                {
                    cols: [
                        {}, {}, {
                            cols: [
                                { view: "button", id: "invs:btnbthwin", type: "icon", icon: "mdi mdi-card-bulleted-outline", label: _("bthwin"), width: 220, disabled: true,hidden: true },
                                { view: "button", id: "invs:btnwnwin", type: "icon", icon: "mdi mdi-newspaper", label: _("wrong_notice1"), width: 160, disabled: true, hidden:  true },
                                { view: "button", id: "invs:btnhistory", type: "icon", popup: "hstvan", icon: "mdi mdi-email-open", label: _("history_tvan"), disabled: true, width: 100 },
                                { view: "button", id: "invs:btnsendback", type: "icon", icon: "mdi mdi-email-plus-outline", label: _("sendback_cqt"), disabled: true, width: 100,hidden: true },
                                // { view: "button", id: "invs:btndel", type: "icon", icon: "mdi mdi-delete", label: _("invoice_del_btn"), disabled: true, width: 100 },
                                // { view: "button", id: "invs:btndel_gd", type: "icon", icon: "mdi mdi-delete", label: _("invoice_del_btn_gd"), hidden: true, disabled: true, width: 130 },
                                // { view: "button", id: "invs:btndelAll", type: "icon", icon: "mdi mdi-delete", label: _("del_all"), hidden: (ENT == "scb") ? false : true, disabled: true, width: 100 },
                                { view: "button", id: "invs:btnrep", type: "icon", icon: "mdi mdi-file-replace-outline", label: _("replace"), css: "webix_danger", disabled: true, width: 80 },
                                { view: "button", id: "invs:btncancel", type: "icon", icon: "mdi mdi-delete-outline", label: _("cancel"), css: "webix_danger", hidden: true, disabled: true, width: 70 },
                                { view: "button", id: "invs:btntrash", type: "icon", icon: "mdi  mdi-delete-variant", label: _("wait_cancel"), css: "webix_danger", disabled: true, width: 95 },
                                { view: "button", id: "invs:btntrashAll", type: "icon", icon: "mdi  mdi-delete-variant", label: _("wait_cancel_all"), css: "webix_danger", hidden: true, width: 120 },
                                { view: "button", id: "invs:btnadj", type: "icon", icon: "mdi mdi-content-duplicate", label: _("adjust"), css: "webix_danger", disabled: true, width: 90 },
                                { view: "button", id: "invs:btnnote", type: "icon", icon: "mdi mdi-content-duplicate", label: _("note"), css: "webix_danger", hidden: true, disabled: true, width: 90 },
                                { view: "button", id: "invs:btnreject", type: "icon", icon: "mdi mdi-pin-off", label: _("reject_cancel"), hidden: true, disabled: true, width: 100 },
                                { view: "button", id: "invs:btnconvert", type: "icon", icon: "mdi mdi-arrow-right-bold-box-outline", label: _("convert"), disabled: true, width: 100 },
                                // { view: "button", id: "invs:btnmails", type: "icon", icon: "mdi mdi-email-outline", label: _("sendmails"), disabled: true, width: 100 },
                                { view: "button", id: "invs:btnmail", type: "icon", icon: "mdi mdi-email-outline", label: "Mail", disabled: true, width: 80 },
                                { view: "button", id: "invs:btnsms", type: "icon", icon: "mdi mdi-email-outline", label: "SMS", disabled: true, width: 80 },
                                // { view: "button", id: "invs:btnprint", type: "icon", icon: "mdi mdi-printer", label: "Print", disabled: true, width: 65 },
                                { view: "button", id: "invs:btnprintmerge", type: "icon", icon: "mdi mdi-printer-3d", label: _("print_merging_pages"), hidden: true, disabled: true, width: 100 },
                                { view: "button", id: "invs:btncopy", type: "icon", icon: "mdi mdi-content-copy", label: "Copy", disabled: true, disabled: true, width: 80 },
                                { view: "button", id: "invs:btnnumentry", type: "icon", icon: "mdi mdi-content-duplicate", label: _("add_tran_no"), css: "webix_danger", disabled: true, width: 90, hidden: ["vib"].includes(ENT) ? false : true },
                                { view: "list", id: "invs:filelist", type: "uploader", autoheight: true, borderless: true, hidden: true },
                                ENT == "mzh" ? {
                                    cols: [
                                        { id: "invs:file", view: "uploader", multiple: false, autosend: false, link: "invs:filelist", accept: (ENT == "vib") ? "text/plain" : "application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/pdf", upload: "api/inv/minutes/upload", type: "icon", icon: "mdi mdi-file-find-outline", label: _("up_minu"), tooltip: _("up_minu_des"), width: 80, hidden: true, disabled: true },
                                        //  { view: "button", id: "invs:getmail", type: "icon", icon: "wxi-sync", label: _("getmail"), hidden: false,disabled: true,  width: 100 },
                                        { view: "button", id: "invs:sentDDS", type: "icon", icon: "mdi mdi-email-outline", label: _("guidds"), hidden: true, width: 100 },
                                        { view: "button", id: "invs:getSTTDDS", type: "icon", icon: "wxi-eye", label: _("ttDDS"), hidden: true, width: 100 },
                                        { view: "button", id: "invs:reportAll", type: "icon", icon: "wxi-eye", label: _("report"), hidden: false, width: 80, popup: popup }
                                    ]
                                }
                                    : { id: "invs:file", view: "uploader", multiple: false, autosend: false, link: "invs:filelist", accept: (ENT == "vib") ? "text/plain" : "application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/pdf", upload: "api/inv/minutes/upload", type: "icon", icon: "mdi mdi-file-find-outline", label: _("up_minu"), tooltip: _("up_minu_des"), width: 80, hidden: true, disabled: true },
                                { view: "button", id: "invs:files", type: "icon", icon: "mdi mdi-file-find-outline", label: _("up_minu"), tooltip: _("up_minu_des"), disabled: true, width: 90, click: () => { $$("invs:upload").show(); $$("invs:btnSaveFile").enable(); $$("upload:file").setValue(null) }, hidden: ["vib"].includes(ENT) ? false : true },
                                ENT == "vhc" ? {
                                    cols: [
                                        { view: "button", id: "invs:btnEDate", type: "icon", icon: "mdi mdi-transcribe", label: _("edit_env_date"), hidden: true, disabled: true, width: 100 },
                                        { view: "button", id: "invs:btnEStatus", type: "icon", icon: "mdi mdi-table-edit", label: _("edit_env_status"), hidden: true, disabled: true, width: 110 }
                                    ]
                                } : { width: 0 }
                            ]
                        }]
                }


            ]
        }
    }
    ready(v, urls) {
        // let state = webix.storage.session.get("invs:form")
        // if (state) {
        //     let grid = $$("invs:grid"), form = $$("invs:form")
        //     form.setValues(state)
        //     grid.clearAll()
        //     grid.loadNext(size, 0, null, "invs->api/sea", true)
        //     webix.storage.session.remove("invs:form")
        // }

        if (["dtt", "ssi", "vib"].includes(ENT)) {
            $$("invs:grid").hideColumn("chk");
            $$("invs:btndel").hide()
        }
        //Lua sua vhc
        if (ROLE.includes('PERM_INVOICE_STATUS') && ["vhc"].includes(ENT)) $$("invs:btnEStatus").show()
        if (ROLE.includes('PERM_INVOICE_DATE') && ["vhc"].includes(ENT)) $$("invs:btnEDate").show()
        // if (ROLE.includes('PERM_INVOICE_ADJUST') && !["tlg", "ssi"].includes(ENT)) $$("invs:btnadj").show()
        if (ROLE.includes('PERM_INVOICE_ADJUST') && ["vib"].includes(ENT)) $$("invs:btnnote").show()
        //  if (ROLE.includes('PERM_INVOICE_REPLACE') && !["mzh"].includes(ENT)) $$("invs:btnrep").show()
        if (!["hlv", "dtt", "mzh"].includes(ENT)) $$("invs:btncancel").hide()
        // if (ROLE.includes('PERM_SMS') ) $$("invs:btnsms").show()														
        if (ROLE.includes('PERM_PRINT_MERGING_INVOICE')) $$("invs:btnprintmerge").show()
        if (ROLE.includes('PERM_VOID_WAIT')) {
            // $$("invs:btntrash").show()

            if (ENT == "mzh") {
                $$("invs:btntrashAll").show();
            }
        }
        if (ROLE.includes('PERM_DDS')) {
            if (ENT == "mzh") {
                $$("invs:sentDDS").show()
                $$("invs:getSTTDDS").show()
            }

        }

        if (["mzh"].includes(ENT)) {
            if (ROLE.includes('PERM_CONVERT')) {
                $$("invs:btnconvert").show()

            } else {
                $$("invs:btnconvert").hide()
            }
            if (ROLE.includes('PERM_DELETE')) {
                $$("invs:btndel").show()

            } else {
                $$("invs:btndel").hide()
            }
            $$("invs:btnmail").hide()
            $$("invs:btnmails").hide()
            $$("invs:btnReport").show()

        }
        if (["vib", "scb", "bvb"].includes(ENT)) {
            $$("invs:btncancel").hide()
        }
        if (["hlv"].includes(ENT)) {
            $$("invs:btndel_gd").show()
        }
        if (["apple"].includes(ENT) && ROLE.includes("PERM_SEARCH_INV_VIEW_DOWNLOAD_FILE")) {
            $$("invs:download_invs").show()
        }

        // if (!["dtt"].includes(ENT) && ROLE.includes('PERM_SEARCH_INV_COPPY')) $$("invs:btncopy").show()
        if (["hdb"].includes(ENT) && ROLE.includes('PERM_INVOICE_CANCEL')) $$("invs:btnreject").show()
        else if (!["dtt"].includes(ENT)) $$("invs:btnreject").show()
        if(["123"].includes(DEGREE_CONFIG)) {
            $$("invs:grid").showColumn("status_received")
            $$("invs:grid").showColumn("sendmailstatus")
        }
        else {
            $$("invs:grid").hideColumn("status_received")
            $$("invs:grid").hideColumn("sendmailstatus")
        }
       
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", { url: urls[0].page, params: urls[0].params }).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //

    }
    init() {
        if (!ROLE.includes('PERM_SEARCH_INV')) {
            this.show("/top/home")
            window.location.replace(`/${homepage}`)
            return
          }
        webix.extend($$("invs:form"), webix.ProgressBar)
        if (ROLE.includes('PERM_SEARCH_RESET_BTH')){
            $$("invs:btnreset").show()
        }
        if (ROLE.includes('PERM_SEARCH_INV_REJECT_CANCEL')) $$("invs:btnreject").show()
        if (ROLE.includes('PERM_SEARCH_SENDBACK_CQT')) $$("invs:btnsendback").show()

        //trungpq10 sua song ngu{
        const type_inv = (ITYPE(this.app))[0].id

        let lang = webix.storage.local.get("lang")
        //configs(type_inv)
        $$("lang:lang").attachEvent("onChange", function (newv, oldv) {

            //trungpq10 sua song ngu{
            const type_inv = (ITYPE(this.app))[0].id
            let lang = newv
            configs(type_inv)
            //}
        });
        //}
        this.Iwin = this.ui(Iwin)
        this.Mwin = this.ui(Mwin)
        this.Swin = this.ui(Swin)
        this.Rwin = this.ui(Rwin)
        this.Wwin = this.ui(Wwin)
        this.BTHwin = this.ui(BTHwin)
        this.VIEWWN = this.ui(VIEWWN)
        this.Wncreatewin = this.ui(Wncreatewin)
        this.IgnsWin = this.ui(IgnsWin)
        if (ENT == "scb") {
            this.DetailWin = this.ui(DetailWin)
        }
        if (ENT == "mzh") {
            this.DesWin = this.ui(DesWin)
        }
        if (ENT == "vhc") {
            this.EDate = this.ui(EditDate)
            // this.EStatus = this.ui(EditStatus)
        }
        if (ENT == "vib") {
            this.DelFile = this.ui(DelFile)
        }
        if (SESS_STATEMENT_SENTYPE == 1) {
             $$("invs:btnbthwin").show()
             $$("invs:btnwnwin").hide()
           //  $$("list_invid").show()
             
        }else{
            $$("invs:btnbthwin").hide()
             $$("invs:btnwnwin").show()
        }
        this.Uploadfile = this.ui(Uploadfile)
        this.NoteWin = this.ui(NoteWin)
        webix.extend($$("invs:note"), webix.ProgressBar)
        const all = { id: "*", value: _("all") }
        let date = new Date(), firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1),firstCore = new Date(date.getFullYear(), date.getMonth(), 1), firstDayvib = new Date(date.getFullYear(), date.getMonth(), date.getDate() - 1), firstDayfhs = new Date(date.getFullYear(), date.getMonth(), 1), grid = $$("invs:grid"), form = $$("invs:form"), row, id, uploader = $$("invs:file"), recpager = $$("recpager"), lastday = webix.Date.add(new Date(date.getFullYear(), date.getMonth(), 1), -1, "day")

        let formdl = $$("trans:form2"), idEnv, oldD
        const hstvan = $$("invs:hstvan")
        if (!["hdb", "vcm"].includes(ENT)) $$("invs:fd").setValue(firstCore)
        else $$("invs:fd").setValue(new Date())
        if (["fhs", "fbc"].includes(ENT)) $$("invs:fd").setValue(firstDayfhs)
        if (["vib"].includes(ENT)) $$("invs:fd").setValue(firstDayvib)
        if (["baca"].includes(ENT)) $$("invs:fd").setValue(date)
        $$("invs:td").setValue(date)
        $$("invs:fdp").setValue(firstDay)
        $$("invs:tdp").setValue(lastday)
        $$("invs:type").getList().add(all, 0)
        //$$("form").getList().add(all, 0)
        $$("invs:curr").getList().add(all, 0)
        // if (ENT != "vcm") $$("invs:ou").getList().add(all, 0)
        if (!["sgr", "vcm"].includes(ENT)) $$("invs:ou").getList().add(all, 0)
        $$("invs:status").getList().add(all, 0)
        $$("invs:status_received").getList().add(all, 0)
        // $$("invs:paym").getList().add(all, 0)
        $$("invs:adjtyp").getList().add(all, 0)
        if (SESS_STATEMENT_SENTYPE == 1) {
            $$("invs:th_type").getList().add(all, 0)
            $$("invs:th_type").setValue("*")
        }
        else {
            $$("invs:wno_adj").getList().add(all, 0)
            $$("invs:status_tbss").getList().add(all, 0)
            $$("invs:wno_adj").setValue("*")
            $$("invs:status_tbss").setValue("*")
        }
        
        $$("invs:curr").setValue("*")
        if (!["hdb", "vcm", "sgr"].includes(ENT)) $$("invs:ou").setValue("*")
        else if (ENT == "sgr") $$("invs:ou").setValue(`${OU}`)
        else $$("invs:ou").setValue(OU)
        $$("invs:status").setValue("*")
        $$("invs:status_received").setValue("*")
        // $$("invs:paym").setValue("*")
        $$("invs:adjtyp").setValue("*")
        //Nếu là VIB thì search luôn
        if (ENT == "mzh") {
            $$("invs:grid").showColumn("chk");
            //$$("invs:btntrashAll").show();
        }


        if (ENT == "mzh") {
            this.DesWin = this.ui(DesWin)
            grid.showColumn("chk");

            let date = new Date(), yr = date.getFullYear()
            let year = $$("invs:cbYear")
            let years = [], i = yr, j = 0
            while (j < 3) {
                years.push({ id: i, value: i })
                i--
                j++
            }
            let list = year.getPopup().getList()
            list.clearAll()
            list.parse(years)
            year.setValue(yr)
        }
        
        //Nút xuất báo cáo
        $$("invs:btnReportp").attachEvent("onItemClick", () => {
            const param = formdl.getValues()
            if (!formdl.validate()) {
                webix.message('Phải nhập các trường bắt buộc')
            }
            else if ($$("invs:fdp").getValue() > $$("invs:tdp").getValue()) {
                webix.message('Từ ngày phải <= Đến ngày')
            }
            else {
                webix.message(`${_("export_reportCol")}.......`, "info", -1, "excel_msAll");
                webix.ajax().response("blob").post("api/inv/invs/rpt", param).then(data => {
                    $$("igns:win").hide()
                    search()
                    webix.html.download(data, `${webix.uid()}.xlsx`)
                    webix.message.hide("excel_msAll");
                })
            }
        }
        )

        //HungLQ bo sung nut gui mail nhieu hoa don
        $$("invs:btnmails").attachEvent("onItemClick", () => {
            webix.confirm(_("mails_confirm_msg"), "confirm-warning").then(() => {
                // let rows = grid.serialize(), arr = []
                // for (let row of rows) {
                //     if (row && row.chk && [3, 4].includes(row.status)) {
                //         arr.push(row)
                //     }
                // }
                let arr = []
                let grida = $$("invs:grid")   
                var size = grida.getPager().config.size
                var page = grida.getPage()
                var ind = size*page
                for ( let i=0; i<size; i++){
                    let id = grida.getIdByIndex(ind+i)
                    if(grida.getItem(id) && grida.getItem(id).chk && [3, 4].includes(grida.getItem(id).status)){
                        arr.push(grida.getItem(id))
                    }
                }
                if (arr.length < 1) {
                    webix.message(_("invoice_must_select"))
                    return false
                }
                webix.delay(() => {
                    webix.ajax().post("api/inv/invs/mails", { invs: arr }).then(result => {
                        let json = result.json()
                        //console.log(json.status)
                        if (json.status == 1) {
                            grid.clearAll()
                            grid.loadNext($$("recpager").getValue(), 0, null, "invs->api/sea", true)
                            webix.message((_("mails_ok_msg")).replace("#hunglq#", arr.length))
                        }
                    }).catch(() => { })
                })
            })
        })
        //HungLQ bo sung nut xoa hoa don
        $$("invs:btndel").attachEvent("onItemClick", () => {
            webix.confirm(["sgr"].includes(ENT) ? _("del_confirm_msg_sgr") : _("del_confirm_msg"), "confirm-warning").then(() => {
                // let rows = grid.serialize(), arr = []
                // for (let row of rows) {
                //     if (row && row.chk && ((row.status <= 1 && !["sgr"].includes(ENT)) || (row.status <= 2 && ["sgr"].includes(ENT)))) {
                //         arr.push(row)
                //     }

                // }

                let arr = []
                let grida = $$("invs:grid")  
                var size = grida.getPager().config.size
                var page = grida.getPage()
                var ind = size*page
                for ( let i=0; i<size; i++){
                    let id = grida.getIdByIndex(ind+i)
                    if(grida.getItem(id) && grida.getItem(id).chk && ((grida.getItem(id).status <= 1 && !["sgr"].includes(ENT)) || (grida.getItem(id).status <= 2 && ["sgr"].includes(ENT)))) {
                        arr.push(grida.getItem(id))
                    }
                }

                if (arr.length < 1) {
                    webix.message(_("invoice_must_select"))
                    return false
                }
                if (["scb"].includes(ENT)) {
                    $$("invs:grid").disable()
                    $$("invs:form").disable()
                    $$("invs:btndelAll").disable()
                    $$("invs:btndel").disable()
                    $$("invs:form").showProgress({
                        type: "icon",
                        hide: false
                    })
                }
                webix.delay(() => {
                    webix.ajax().post("api/inv/invs/del", { invs: arr }).then(result => {
                        let json = result.json()
                        if (["scb"].includes(ENT)) {
                            $$("invs:grid").enable()
                            $$("invs:form").enable()
                            $$("invs:btndelAll").enable()
                            if (ROLE.includes('PERM_SEARCH_INV_DELETE')) $$("invs:btndel").enable()
                            $$("invs:form").hideProgress()
                        }
                        console.log(json.status)
                        if (json.status == 1) {
                            grid.clearAll()
                            grid.loadNext($$("recpager").getValue(), 0, null, "invs->api/sea", true)
                            webix.message((_("del_ok_msg")).replace("#hunglq#", json.countseq))
                        }
                    }).catch(() => { })
                })
            })
        })
        if (ENT == "hlv") {
            $$("invs:btndel_gd").attachEvent("onItemClick", () => {
                webix.confirm(["sgr"].includes(ENT) ? _("del_confirm_msg_sgr") : _("del_confirm_msg"), "confirm-warning").then(() => {
                    // let rows = grid.serialize(), arr = []
                    // for (let row of rows) {
                    //     if (row && row.chk && ((row.status <= 1 && !["sgr"].includes(ENT)) || (row.status <= 2 && ["sgr"].includes(ENT)))) {
                    //         arr.push(row)
                    //     }

                    // }
                    let arr = []
                    let grida = $$("invs:grid")  
                    var size = grida.getPager().config.size
                    var page = grida.getPage()
                    var ind = size*page
                    for ( let i=0; i<size; i++){
                        let id = grida.getIdByIndex(ind+i)
                        if(grida.getItem(id) && grida.getItem(id).chk && ((grida.getItem(id).status <= 1 && !["sgr"].includes(ENT)) || (grida.getItem(id).status <= 2 && ["sgr"].includes(ENT)))) {
                            arr.push(grida.getItem(id))
                        }
                    }
                    if (arr.length < 1) {
                        webix.message(_("invoice_must_select"))
                        return false
                    }

                    webix.delay(() => {
                        webix.ajax().post("api/inv/invs/del_gd", { invs: arr }).then(result => {
                            let json = result.json()

                            if (json.status == 1) {
                                grid.clearAll()
                                grid.loadNext($$("recpager").getValue(), 0, null, "invs->api/sea", true)
                                webix.message((_("del_ok_msg")).replace("#hunglq#", json.countseq))
                            }
                        }).catch(() => { })
                    })
                })
            })
        }
        if (ENT == "mzh") {

            $$("invs:getSTTDDS").attachEvent("onItemClick", () => {
                let fd = $$("invs:fd").getValue()
                let td = $$("invs:td").getValue()
                //let rows = grid.serialize(), arr = []
                // for (let row of rows) {

                //     if (row && row.chk && row.status >= 2) {
                //         arr.push(row)
                //     }
                //     if (row && row.chk && row.status != 3) {
                //         //khong can chuyen tieng anh vi moi mzh dung
                //         webix.message("Chỉ được chọn trạng thái hóa đơn đã duyệt", "error")
                //         return false
                //     }
                // }
                let arr = []
                let grida = $$("invs:grid")  
                var size = grida.getPager().config.size
                var page = grida.getPage()
                var ind = size*page
                for ( let i=0; i<size; i++){
                    let id = grida.getIdByIndex(ind+i)
                    if (grida.getItem(id) && grida.getItem(id).chk && grida.getItem(id).status >= 2) {
                        arr.push(grida.getItem(id))
                    }
                    if (grida.getItem(id) && grida.getItem(id).chk && grida.getItem(id).status != 3) {
                        //khong can chuyen tieng anh vi moi mzh dung
                        webix.message("Chỉ được gửi trạng thái hóa đơn đã duyệt", "error")
                        return false
                    }
                }
                if (arr.length < 1) {
                    webix.message(_("invoice_must_select"))
                    return false
                }
                $$("invs:getSTTDDS").disable()
                webix.message(`${_("ttDDS")}.......`, "info", -1, "ddst_ms");

                $$("invs:form").disable();
                $$("invs:form").showProgress({
                    type: "icon",
                    hide: false
                });
                webix.delay(() => {
                    webix.ajax().post("api/inv/invs/ddsStt", { invs: arr, td: td, fd: fd }).then(result => {
                        let json = result.json()
                        console.log(json.status)
                        if (json.status == 1) {
                            grid.clearAll()
                            grid.loadNext($$("recpager").getValue(), 0, null, "invs->api/sea", true)
                            $$("invs:form").enable();
                            $$("invs:form").hideProgress();
                            $$("invs:getSTTDDS").enable()
                            webix.message.hide("ddst_ms");
                            webix.message(_("syn_ok_msg_tran"))
                        }
                    }).catch(() => {
                        $$("invs:form").enable();
                        $$("invs:form").hideProgress();
                        $$("invs:getSTTDDS").enable()
                        webix.message.hide("ddst_ms");
                    })
                })

            })
            if (ENT == "mzh") {
                $$("invs:sentDDS").attachEvent("onItemClick", () => {

                    // let rows = grid.serialize(), arr = []
                    // for (let row of rows) {

                    //     if (row && row.chk && row.status >= 2) {
                    //         arr.push(row)
                    //     }
                    //     if (row && row.chk && row.status != 3) {
                    //         //khong can chuyen tieng anh vi moi mzh dung
                    //         webix.message("Chỉ được gửi trạng thái hóa đơn đã duyệt", "error")
                    //         return false
                    //     }
                    // }
                    let arr = []
                    let grida = $$("invs:grid")  
                    var size = grida.getPager().config.size
                    var page = grida.getPage()
                    var ind = size*page
                    for ( let i=0; i<size; i++){
                        let id = grida.getIdByIndex(ind+i)
                        if (grida.getItem(id) && grida.getItem(id).chk && grida.getItem(id).status >= 2) {
                            arr.push(grida.getItem(id))
                        }
                        if (grida.getItem(id) && grida.getItem(id).chk && grida.getItem(id).status != 3) {
                            //khong can chuyen tieng anh vi moi mzh dung
                            webix.message("Chỉ được gửi trạng thái hóa đơn đã duyệt", "error")
                            return false
                        }
                    }
                    if (arr.length < 1) {
                        webix.message(_("invoice_must_select"))
                        return false
                    }
                    $$("invs:sentDDS").disable()
                    webix.message(`${_("guidds")}.......`, "info", -1, "dds_ms");

                    $$("invs:form").disable();
                    $$("invs:form").showProgress({
                        type: "icon",
                        hide: false
                    });
                    webix.delay(() => {
                        webix.ajax().post("api/inv/invs/dds", { invs: arr }).then(result => {
                            let json = result.json()
                            console.log(json.status)
                            if (json.status == 1) {
                                grid.clearAll()
                                grid.loadNext($$("recpager").getValue(), 0, null, "invs->api/sea", true)
                                $$("invs:form").enable();
                                $$("invs:form").hideProgress();
                                $$("invs:sentDDS").enable()
                                webix.message.hide("dds_ms");
                                webix.message((_("sent_dds_sc")))
                            }
                        }).catch(() => {
                            $$("invs:form").enable();
                            $$("invs:form").hideProgress();
                            $$("invs:sentDDS").enable()
                            webix.message.hide("dds_ms");
                        })
                    })

                })
            }
        }
        if (ENT == "mzh") {
            let formMZH = $$("invs:formDes")

            $$("invs:btnImpdes").attachEvent("onItemClick", () => {
                const param = formMZH.getValues()
                if (!formMZH.validate()) {
                    webix.message('Phải nhập các trường bắt buộc')
                }
                else {
                    $$("invs:btnImpdes").disable()
                    webix.message(`${_("export_reportCol")}.......`, "info", -1, "btnImpdes");
                    webix.ajax().response("blob").post("api/inv/invs/detailS", param).then(data => {
                        webix.html.download(data, `${webix.uid()}.xlsx`)
                        $$("invs:btnImpdes").enable()
                        $$("invs:win").hide()
                        search()
                        webix.message.hide("btnImpdes");
                    })
                }
            })
        }
        $$("invs:type").attachEvent("onChange", (n) => {
            configs(n)
        })
        if (["dtt", "scb"].includes(ENT)) uploader.show()
        if (ENT == "vcm") {
            $$("form").attachEvent("onChange", (n) => {

                this.webix.ajax().get(`api/sea/asbf?id=${n}`).then(r => {
                    let datasr = r.json()
                    let removeDuplicates = (array, key) => {
                        let lookup = new Set();
                        return array.filter(obj => obj[key] && !lookup.has(obj[key]) && lookup.add(obj[key]));
                    }
                    $$("invs:serial").getList().clearAll()
                    $$("invs:serial").setValue(null)

                    $$("invs:serial").getList().parse(removeDuplicates(datasr, 'id'))
                    // $$("mst").getList().parse(removeDuplicates(data,'mst'))
                })
            })
        } else {
            $$("form").attachEvent("onChange", (n, ov) => {
                if (n == ov) return
                this.webix.ajax().get(`api/sea/asbf?id=${n}`).then(r => {
                    let datasr = r.json()
                    let removeDuplicates = (array, key) => {
                        let lookup = new Set();
                        return array.filter(obj => obj[key] && !lookup.has(obj[key]) && lookup.add(obj[key]));
                    }
                    $$("invs:serial").getList().clearAll()
                    $$("invs:serial").setValue(null)

                    $$("invs:serial").getList().parse(removeDuplicates(datasr, 'id'))
                    $$("invs:serial").getList().add(all, 0)
                    $$("invs:serial").setValue("*")
                    // $$("mst").getList().parse(removeDuplicates(data,'mst'))
                    //Den doan nay moi load ky hieu trong session ra
                    let state = webix.storage.session.get("invs:form")
                    if (state && state.serial && datasr.length > 0) {
                        $$("invs:serial").setValue(state.serial)
                        grid.clearAll()
                        grid.loadNext(size, 0, null, "invs->api/sea", true)
                        webix.storage.session.remove("invs:form")
                    }
                })
            })
        }

        $$("invs:type").setValue("01GTKT")
        const search = () => {
            if (grid.getHeaderContent("chk1")) grid.getHeaderContent("chk1").uncheck()
            grid.clearAll()
            grid.loadNext(recpager.getValue(), 0, null, "invs->api/sea", true)
        }
        $$("iwin:btnxml").attachEvent("onItemClick", () => {
            webix.ajax(`api/inv/xml/${id}`).then(result => {
                const json = result.json()
                const blob = new Blob([json.xml], { type: "text/xml;charset=utf-8" })
                saveAs(blob)
            })
        })

        $$("iwin:btnsignpdf").attachEvent("onItemClick", () => {
            webix.ajax("api/inv/pdf/conf").then(result => {
                const json = result.json()
                if (json.sign == 1) {
                    json.b64 = b64
                    webix.ajax().post(`${USB}pdf`, json).then(result => {
                        const json = result.json()
                        $$("iwin:ipdf").define("src", json.pdf)
                        webix.message(_("signed_pdf"), "success")
                    })
                }
                else {
                    webix.ajax().post("api/inv/pdf/sign", { b64: b64 }).then(result => {
                        const json = result.json()
                        $$("iwin:ipdf").define("src", json.pdf)
                        webix.message(_("signed_pdf"), "success")
                    })
                }
            })
        })

        $$("invs:download_invs").attachEvent("onItemClick", () => {
            let arr = []
            let grida = $$("invs:grid")
            var size = grida.getPager().config.size
            var page = grida.getPage()
            var ind = size*page
            for ( let i=0; i<size; i++){
                let id = grida.getIdByIndex(ind+i)
                if (grida.getItem(id) && grida.getItem(id).chk && grida.getItem(id).status == 3) {
                    arr.push(id)
                }
                if (grida.getItem(id) && grida.getItem(id).chk && grida.getItem(id).status != 3) {
                    webix.message("Chỉ được chọn trạng thái hóa đơn đã duyệt", "error")
                    return false
                }
            }
            if (arr.length < 1) {
                webix.message(_("invoice_must_select"))
                return false
            } 
            
            $$("invs:form").showProgress({
                type: "icon",
                hide: false
            });
            const listInvSeq = arr.map(id=> webix.ajax(`api/sea/htm/${id}`))
            let pdfblob = []
            webix.delay(()=>{
                webix.promise.all(listInvSeq).then(function(results){
                    try {
                        for(const [index, result] of results.entries()) {
                            let {pdf, sizePDF} = result.json()
                            if (sizePDF >= 3145728) webix.message(String(_("file_pdf_size_limit")).replace("#hunglq#", 3145728 / (1024 * 1024)))
                            pdfblob.push({blob: pdf, name: `invoice_${arr[index]}.pdf`})
                        }
                        const zip = new JSZip()
                        for(let data of pdfblob) zip.file(data.name, data.blob.replace('data:application/pdf;base64, ',''), {base64: true})
                        let zipblob = zip.generate({type:"blob"});
                        if (zipblob) webix.html.download(zipblob, `invoice_${webix.uid()}.zip`);
                    } catch(error) {
                        webix.message("Error zip file", error)
                    } finally {
                        webix.message.hide("exp");
                        $$("invs:form").hideProgress()
                    }
                });
            })
        })

        $$("iwin:btnapp").attachEvent("onItemClick", () => {
            if (ENT == "hdb") {
                let item = grid.getItem(id)
                let idate = new Date(item.idt), nowd = new Date(), difT = nowd.getTime() - idate.getTime(), difD = difT / (1000 * 3600 * 24)
                if (difD > 5) {
                    webix.message(_("hdb_5_day_out"), "error")
                    return
                }
            }
            webix.confirm(_("approve_confirm"), "confirm-warning").then(() => {
                webix.ajax().post("api/app", { ids: [id] }).then(result => {
                    const rows = result.json()
                    if (rows.hasOwnProperty("mst")) {
                        webix.ajax().post(`${USB}xml`, rows).then(result => {
                            const rows = result.json()
                            webix.ajax().put("api/app", rows).then(result => {
                                webix.message(`${_("approved")} ${result.json()} ${_("invoice")}`, "success")
                                $$("iwin:win").hide()
                                search()
                            })
                        }).catch(e => {
                            if (e.message && e.message.indexOf('CKS-00001') > 0) {
                                webix.message(_("certificate_error"), "error")
                            } else {
                                if (e.message) {
                                    webix.message(e.message)
                                } else {
                                    if (!e.responseText)
                                        webix.message(_("certificate_error"), "error")
                                    else
                                        webix.message(_("approve_failed"), "error")
                                }
                            }
                        })
                    }
                    else {
                        webix.message(`${_("approved")} ${rows.count} ${_("invoice")}`, "success")
                        $$("iwin:win").hide()
                        search()
                    }
                })
            })
        })


        $$("invs:btnconvert").attachEvent("onItemClick", () => {
            if (["sgr"].includes(ENT)) {
                webix.confirm(_("can_convert_msg"), "confirm-warning").then(() => {
                    // let rows = grid.serialize(), arr = []
                    // for (let row of rows) {
                    //     if (row && row.chk && (row.status == 3)) {
                    //         arr.push(row.id)
                    //     }
                    // }
                    let arr = []
                    let grida = $$("invs:grid")  
                    var size = grida.getPager().config.size
                    var page = grida.getPage()
                    var ind = size*page
                    for ( let i=0; i<size; i++){
                        let id = grida.getIdByIndex(ind+i)
                        if (grida.getItem(id) && grida.getItem(id).chk && (grida.getItem(id).status == 3)) {
                            arr.push(id)
                        }
                    }
                    if (arr.length < 1) {
                        webix.message(_("invoice_must_select"))
                        return false
                    } else if (arr.length < 2) {
                        for (let id of arr) {
                            webix.ajax(`api/inv/cvt/${id}`).then(res => {
                                // const req = createRequest(result.json())
                                //jsreport.renderAsync(req).then(res => {
                                //    $$("iwin:ipdf").define("src", res.toDataURI())
                                // webix.ajax().post("api/seek/report", req).then(res => {
                                    let pdf = res.json()
                                    b64 = pdf.pdf
                                    const blob = dataURItoBlob(b64);

                                    var temp_url = window.URL.createObjectURL(blob);
                                    $$("iwin:ipdf").define("src", temp_url)
                                    $$("iwin:win").show()
                                    $$("iwin:btnsignpdf").hide()
                                    $$("iwin:btnxml").hide()
                                    $$("iwin:btnapp").hide()
                                // })
                            })
                        }
                    } else {
                        webix.delay(() => {
                            for (let id of arr) {
                                webix.ajax(`api/inv/cvt/${id}`).then(res => {
                                    // const req = createRequest(result.json())
                                    //jsreport.renderAsync(req).then(res => {
                                    //    $$("iwin:ipdf").define("src", res.toDataURI())
                                    // webix.ajax().post("api/seek/report", req).then(res => {
                                        let pdf = res.json()
                                        b64 = pdf.pdf
                                        const blob = dataURItoBlob(b64);

                                        var temp_url = window.URL.createObjectURL(blob);
                                        $$("iwin:ipdf").define("src", temp_url)
                                        webix.html.download($$("iwin:ipdf").getIframe().src, "invoice.pdf")
                                    // })
                                })
                            }
                            webix.message((_("ok_convert_msg")).replace("#baodq6#", arr.length))

                        })
                    }
                })
                 } else {
                 webix.ajax(`api/inv/cvt/${id}`).then(res => {
                    // const req = createRequest(result.json())
                    //jsreport.renderAsync(req).then(res => {
                    //    $$("iwin:ipdf").define("src", res.toDataURI())
                    // webix.ajax().post("api/seek/report", req).then(res => {
                        let pdf = res.json()
                        b64 = pdf.pdf
                        const blob = dataURItoBlob(b64);

                         var temp_url = window.URL.createObjectURL(blob);
                        
                        if(["hsy"].includes(ENT)) {
                            $$("iwin:btnxml").hide()
                            $$("iwin:btnapp").hide()
                            $$("iwin:btnsignpdf").hide()
                        }
                        $$("iwin:ipdf").define("src", temp_url)
                        $$("iwin:win").show()
                     })
                     search()
                 //})
            }
            
        })


        $$("invs:btnsearch").attachEvent("onItemClick", () => {
            if (form.validate()) search()
        })

        $$("invs:btncopy").attachEvent("onItemClick", () => {
            webix.confirm(`${_("copy_confirm")} ?`, "confirm-warning").then(() => {
                webix.storage.session.put("invs:form", form.getValues())
                this.show(`inv.inv?id=${id}&action=copy`)
            })
        })

        $$("invs:btnrep").attachEvent("onItemClick", () => {
            // webix.confirm(`${_("replace_confirm")} ${id} ?`, "confirm-warning").then(() => {
            webix.confirm(`${_("replace_confirm")} ?`, "confirm-warning").then(() => {
                webix.storage.session.put("invs:form", form.getValues())
                this.show(`inv.inv?id=${id}&action=replace`)
            })
        })

        $$("invs:btnadj").attachEvent("onItemClick", () => {
            webix.confirm(["hlv"].includes(ENT) ? `${_("adjust_confirm")} ?` : `${_("adjust_confirm")} ${id} ?`, "confirm-warning").then(() => {
                webix.storage.session.put("invs:form", form.getValues())
                this.show(`inv.adj?id=${id}&action=create`)
            })
        })

        $$("invs:btnnote").attachEvent("onItemClick", () => {

            try {
                webix.ui({
                    view: "popup",
                    id: "popupNote",

                    // height:400,
                    // width:400,
                    isVisible: true,
                    position: "center",
                    autofocus: true,
                    body: {
                        view: "form",
                        id: "note:form",
                        elements: [
                            { view: "text", id: "note_label", name: "note_no", label: _("note"), labelWidth: 100, inputAlign: "left" },
                            {
                                cols: [
                                    {},
                                    { view: "button", id: "btnNote", value: _("note"), css: "webix_primary" },
                                    { view: "button", id: "btnCancelTrans", value: _("cancel") }
                                ]
                            }
                        ]
                    }
                }).show();
                $$("btnCancelTrans").attachEvent("onItemClick", () => $$("popupNote").hide())
                $$("btnNote").attachEvent("onItemClick", () => {
                    try {
                        const param = $$("note:form").getValues()
                        param.id = id
                        param.idt = new Date()
                        if (!(param.note_no)) {
                            webix.message(_("required_msg"), "error")
                            return false
                        }
                        webix.ajax().post("api/inv/note", param)
                            .then(data => data.json())
                            .then(data => {
                                $$("popupNote").hide()
                                if (data) {
                                    grid.clearAll()
                                    search()
                                }
                                else {
                                    webix.message(err.message, "error")
                                }
                            }).catch(err => {
                                console.log(err)
                                webix.message(err.message, "error")
                            })
                    }
                    catch (err) {
                        webix.message(err.message, "error")
                    }
                })

            }
            catch (err) {
                webix.message(err.message, "error")
            }
        })

        $$("invs:btnnumentry").attachEvent("onItemClick", () => {
            try {
                webix.ui({
                    view: "popup",
                    id: "popupNumTran",
                    isVisible: true,
                    position: "center",
                    autofocus: true,
                    body: {
                        view: "form",
                        id: "numbertran:form",
                        elements: [
                            { view: "text", id: "number_tran", name: "number_tran", label: _("tran_no"), labelWidth: 100, inputAlign: "left", required: true },
                            { view: "combo", id: "module", name: "module", label: "Module", options: MODULE, labelWidth: 100, inputAlign: "left", required: true },
                            {
                                cols: [
                                    {},
                                    { view: "button", id: "btnNumtry", value: "Cập nhật BT", css: "webix_primary" },
                                    { view: "button", id: "btnCancelTrans", value: _("cancel") }
                                ]
                            }
                        ]
                    }
                }).show();
                $$("btnCancelTrans").attachEvent("onItemClick", () => $$("popupNumTran").hide())
                $$("btnNumtry").attachEvent("onItemClick", () => {
                    try {
                        const param = $$("numbertran:form").getValues()
                        param.id = id
                        param.idt = new Date()
                        if (!(param.number_tran && param.module)) {
                            webix.message(_("required_msg"), "error")
                            return false
                        }
                        webix.ajax().post("api/inv/numbertran", param)
                            .then(data => data.json())
                            .then(data => {
                                $$("popupNumTran").hide()
                                if (data) {
                                    grid.clearAll()
                                    search()
                                    webix.message(_("upd_ok_msg"))
                                    $$("invs:btnnumentry").disable()
                                }
                                else {
                                    webix.message(err.message, "error")
                                }
                            }).catch(err => {
                                console.log(err)
                                webix.message(err.message, "error")
                            })
                    }
                    catch (err) {
                        webix.message(err.message, "error")
                    }
                })

            }
            catch (err) {
                webix.message(err.message, "error")
            }
        })

        $$("invs:btncancel").attachEvent("onItemClick", () => {
            if (["sgr"].includes(ENT)) {
                webix.confirm(_("can_confirm_msg"), "confirm-warning").then(() => {
                    if (row.status == 3) {
                        webix.storage.session.put("invs:form", form.getValues())
                        this.show(`inv.inv?id=${id}&action=cancel`)
                    }
                    else {
                        // let rows = grid.serialize(), arr = []
                        // for (let row of rows) {

                        //     if (row && row.chk && (row.status == 2)) {
                        //         arr.push(row)
                        //     }

                        // }
                        let arr = []
                        let grida = $$("invs:grid")  
                        var size = grida.getPager().config.size
                        var page = grida.getPage()
                        var ind = size*page
                        for ( let i=0; i<size; i++){
                            let id = grida.getIdByIndex(ind+i)
                            if (grida.getItem(id) && grida.getItem(id).chk && (grida.getItem(id).status == 2)) {
                                arr.push(grida.getItem(id))
                            }
                        }
                        if (arr.length < 1) {
                            webix.message(_("invoice_must_select"))
                            return false
                        }

                        webix.delay(() => {
                            webix.ajax().post(`api/inv/cancelSGR`, { arr: arr }).then(result => {
                                if (result.json() == 1) {
                                    grid.clearAll()
                                    grid.loadNext($$("recpager").getValue(), 0, null, "invs->api/sea", true)
                                    webix.message((_("can_ok_msg")).replace("#hunglq#", arr.length))
                                }
                            })
                        })
                    }
                })

            } else {
                webix.confirm(`${_("cancel_confirm")} ${id} ?`, "confirm-warning").then(() => {
                    if (row.status == 3) {
                        webix.storage.session.put("invs:form", form.getValues())
                        this.show(`inv.inv?id=${id}&action=cancel`)
                    }
                    else {
                        webix.ajax(`api/inv/status/${id}/4`).then(result => {
                            if (result.json() == 1) {
                                webix.message(_("cancel_ok"), "success")
                                search()
                            }
                        })
                    }
                })
            }
        })

        $$("form").attachEvent("onChange", (n) => {
            if (n == "*") {
                $$("invs:btnprint").disable()
                $$("invs:btnprintmerge").disable()
            }
            else {
                const val = $$("invs:status").getValue()
                if (["3", "4"].includes(val)) {
                    if (ROLE.includes('PERM_SEARCH_INV_PRINT')) $$("invs:btnprint").enable()
                    $$("invs:btnprintmerge").enable()
                    
                }
                else {
                    $$("invs:btnprint").disable()
                    $$("invs:btnprintmerge").disable()
                }
            }
        })

       
        $$("invs:status").attachEvent("onChange", (v) => {
            if (["1", "3", "4"].includes(v)) {
                $$("invs:grid").showColumn("chk");         
            } else {
                if (["sgr"].includes(ENT)) {
                    if (["2"].includes(v)) {
                        $$("invs:grid").showColumn("chk");
                    }
                    else {
                        $$("invs:grid").hideColumn("chk");
                    }
                }
                else {
                    $$("invs:grid").hideColumn("chk");
                }
            }
            if (["1"].includes(v) && ROLE.includes('PERM_SEARCH_INV_DELETE')) {
                $$("invs:btndelAll").show();
                $$("invs:btndelAll").enable();
            }
            else {
                $$("invs:btndelAll").hide();
            }

            if (v == "1") {
                if (ROLE.includes('PERM_SEARCH_INV_DELETE')) $$("invs:btndel").enable()
                $$("invs:btndel_gd").enable()
                if (["hlv"].includes(ENT)) $$("invs:btndel_gd").enable()
                if (["scb"].includes(ENT)) $$("invs:btndelAll").enable()
                //Check phân quyền sửa xóa với VIB
                if (["vib"].includes(ENT)) {
                    if (!ROLE.includes('PERM_INVOICE_DELETE')) {
                        //$$("invs:grid").hideColumn("chk");
                        $$("invs:btndel").disable()
                    }
                }
            } else {
                $$("invs:btndel_gd").disable()
                /*
                if (!["mzh"].includes(ENT)) {
                    $$("invs:grid").hideColumn("chk");
                }
                */
                //Thupq
                if (["sgr"].includes(ENT)) {
                    if (["2"].includes(v)) {
                        if (!ROLE.includes('PERM_INVOICE_DELETE')) {
                            if (ROLE.includes('PERM_SEARCH_INV_DELETE')) $$("invs:btndel").enable()
                        }
                    }
                    else $$("invs:btndel").disable()
                }
                else $$("invs:btndel").disable()
                if (["scb"].includes(ENT)) $$("invs:btndelAll").disable()
                //het
            }
            if (["3", "4"].includes(v)) {
                const val = $$("form").getValue()
                $$("invs:btnsendback").enable()
                $$("invs:btnreset").enable()
                if (ROLE.includes('PERM_MAILS')) $$("invs:btnmails").enable()
                if (val == "*") {
                    $$("invs:btnprint").disable()
                    $$("invs:btnprintmerge").disable()
                }
                else {
                    if (ROLE.includes('PERM_SEARCH_INV_PRINT')) $$("invs:btnprint").enable()
                    $$("invs:btnprintmerge").enable()
                }
            }
            else {
                $$("invs:btnprint").disable()
                $$("invs:btnprintmerge").disable()
                $$("invs:btnmails").disable()
                $$("invs:btnsendback").disable()
                $$("invs:btnreset").disable()
            }

            if(v=="3") $$("invs:download_invs").enable()
            else $$("invs:download_invs").disable()
            
        })

        $$("invs:btnprint").attachEvent("onItemClick", () => {
            if (["sgr"].includes(ENT)) {
                if (!form.validate()) return false
                //webix.message(_("informprint"), "info", expire)
                if ($$("invs:fd").getValue() != $$("invs:td").getValue()) {
                    webix.message(_("print_date"), "error")
                    return false
                }
                let obj = form.getValues()
                Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                // let rows = grid.serialize(), arr = []
                // for (let row of rows) {
                //     if (row && row.chk && (row.status == 3)) {
                //         arr.push(`${row.id}`)
                //     }
                // }
                let arr = []
                let grida = $$("invs:grid")  
                var size = grida.getPager().config.size
                var page = grida.getPage()
                var ind = size*page
                for ( let i=0; i<size; i++){
                    let id = grida.getIdByIndex(ind+i)
                    if (grida.getItem(id) && grida.getItem(id).chk && (grida.getItem(id).status == 3)) {
                        arr.push(id)
                    }
                }
                if (arr.length < 1) {
                    webix.message(_("invoice_must_select"))
                    return false
                }
                arr = arr.join("','")
                // obj.push(arr)
                let params = {}
                $$("invs:btnprint").disable()
                $$("invs:form").disable();
                $$("invs:form").showProgress({
                    type: "icon",
                    hide: false
                });
                params.filter = obj
                params.arr = arr
                webix.ajax("api/sea/print", params).then(result => {
                    let type = $$("invs:type").getValue()
                    const json = result.json(), tmp = createTemplateIf(json.tmp, ENT, type), docs = json.docs
                    webix.message((_("numberprinting")).replace("#hunglq#", String(docs.length)), "success", expire)
                    webix.ajax().post(`${USB}report`, { tmp: tmp, docs: docs }).then(result => {
                        webix.message(result.text(), "success", expire)
                        $$("invs:form").enable();
                        $$("invs:form").hideProgress();
                        if (ROLE.includes('PERM_SEARCH_INV_PRINT')) $$("invs:btnprint").enable()
                    }).fail(err => {
                        webix.message(err.responseText ? err.responseText : _("usb_error"), "error")
                        $$("invs:form").enable();
                        $$("invs:form").hideProgress();
                        if (ROLE.includes('PERM_SEARCH_INV_PRINT')) $$("invs:btnprint").enable()
                    })

                })
            } else {
                if (!form.validate()) return false
                //webix.message(_("informprint"), "info", expire)
                if ($$("invs:fd").getValue() != $$("invs:td").getValue()) {
                    webix.message(_("print_date"), "error")
                    return false
                }
                let obj = form.getValues()
                Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])

                let params = {}
                $$("invs:btnprint").disable()
                $$("invs:form").disable();
                $$("invs:form").showProgress({
                    type: "icon",
                    hide: false
                });
                params.filter = obj
                webix.ajax("api/sea/pdf", params).then(result => {
                    let type = $$("invs:type").getValue()
                    const json = result.json(), tmp = createTemplateIf(json.tmp, ENT, type), docs = json.docs
                    webix.message((_("numberprinting")).replace("#hunglq#", String(docs.length)), "success", expire)
                    webix.ajax().post(`${USB}report`, { tmp: tmp, docs: docs }).then(result => {
                        webix.message(result.text(), "success", expire)
                        $$("invs:form").enable();
                        $$("invs:form").hideProgress();
                        if (ROLE.includes('PERM_SEARCH_INV_PRINT')) $$("invs:btnprint").enable()
                    }).fail(err => {
                        webix.message(err.responseText ? err.responseText : _("usb_error"), "error")
                        $$("invs:form").enable();
                        $$("invs:form").hideProgress();
                        if (ROLE.includes('PERM_SEARCH_INV_PRINT')) $$("invs:btnprint").enable()
                    })

                })
            }
        })

        $$("invs:btnprintmerge").attachEvent("onItemClick", () => {

            if (!form.validate()) return false
            //webix.message(_("informprint"), "info", expire)
            if ($$("invs:fd").getValue() != $$("invs:td").getValue()) {
                webix.message(_("print_date"), "error")
                return false
            }
            let obj = form.getValues()
            Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])

            let params = {}
            try {
                $$("invs:btnprintmerge").disable()
                $$("invs:form").disable();
                $$("invs:form").showProgress({
                    type: "icon",
                    hide: false
                });
                params.filter = obj
                webix.ajax("api/sea/pdfmerge", params).then(result => {
                    $$("invs:form").enable();
                    $$("invs:form").hideProgress();
                    $$("invs:btnprintmerge").enable()
                    let pdf = result.json()
                    b64 = pdf.pdf
                    const blob = dataURItoBlob(b64);

                    var temp_url = window.URL.createObjectURL(blob);
                    $$("iwin:ipdf").define("src", temp_url)
                    $$("iwin:win").show()

                }).catch(() => {
                    $$("invs:form").enable();
                    $$("invs:form").hideProgress();
                    $$("invs:btnprintmerge").enable()
                }).fail(err => {
                    $$("invs:form").enable();
                    $$("invs:form").hideProgress();
                    $$("invs:btnprintmerge").enable()
                })
            } catch (err) {
                $$("invs:form").enable();
                $$("invs:form").hideProgress();
                $$("invs:btnprintmerge").enable()
            }

        })

        $$("invs:btntrash").attachEvent("onItemClick", () => {
            webix.confirm(`ID ${id}. ${_("wait_cancel_confirm")}`, "confirm-warning").then(() => {
                if (row.status == 3 ) {
                    webix.storage.session.put("invs:form", form.getValues())
                    this.show(`inv.inv?id=${id}&action=wcancel`)
                }
            })
        })
        $$("invs:btntrashAll").attachEvent("onItemClick", () => {
            // let rows = grid.serialize(), arr = []
            // for (let row of rows) {

            //     if (row && row.chk && row.status == 3) {
            //         arr.push(row)
            //     }
            //     if (row && row.chk && row.status != 3) {
            //         webix.message("Chỉ được chọn trạng thái hóa đơn đã duyệt", "error")
            //         return false
            //     }
            // }
            let arr = []
            let grida = $$("invs:grid")
            var size = grida.getPager().config.size
            var page = grida.getPage()
            var ind = size*page
            for ( let i=0; i<size; i++){
                let id = grida.getIdByIndex(ind+i)
                if (grida.getItem(id) && grida.getItem(id).chk && grida.getItem(id).status == 3) {
                    arr.push(grida.getItem(id))
                }
                if (grida.getItem(id) && grida.getItem(id).chk && grida.getItem(id).status != 3) {
                    webix.message("Chỉ được chọn trạng thái hóa đơn đã duyệt", "error")
                    return false
                }
            }
            if (arr.length < 1) {
                webix.message(_("invoice_must_select"))
                return false
            }

            $$("invs:note").show()
        })

        $$("note:btntrashAll").attachEvent("onItemClick", () => {
            // let rows = grid.serialize(), arr = []
            // for (let row of rows) {

            //     if (row && row.chk && row.status == 3) {
            //         arr.push(row)
            //     }
            //     if (row && row.chk && row.status != 3) {
            //         webix.message("Chỉ được chọn trạng thái hóa đơn đã duyệt", "error")
            //         return false
            //     }

            // }
            let arr = []
            let grida = $$("invs:grid")  
            var size = grida.getPager().config.size
            var page = grida.getPage()
            var ind = size*page
            for ( let i=0; i<size; i++){
                let id = grida.getIdByIndex(ind+i)
                if (grida.getItem(id) && grida.getItem(id).chk && grida.getItem(id).status == 3) {
                    arr.push(grida.getItem(id))
                }
                if (grida.getItem(id) && grida.getItem(id).chk && grida.getItem(id).status != 3) {
                    webix.message("Chỉ được chọn trạng thái hóa đơn đã duyệt", "error")
                    return false
                }
            }
            if (arr.length < 1) {
                webix.message(_("invoice_must_select"))
                return false
            }
            webix.confirm(`${_("wait_cancel_confirm")}`, "confirm-warning").then(() => {
                let notev = $$("noteV").getValue()
                $$("invs:note").disable();
                $$("invs:note").showProgress({
                    type: "icon",
                    hide: false
                });
                webix.delay(() => {
                    webix.ajax().post("api/iwcall", { invs: arr, note: notev }).then(result => {
                        let json = result.json()
                        $$("invs:note").enable()
                        $$("invs:note").hideProgress()
                        $$("invs:note").hide()
                        grid.clearAll()
                        grid.loadNext($$("recpager").getValue(), 0, null, "invs->api/sea", true)
                        webix.message((_("wait_cancel_all_ss")).replace("@@", arr.length))
                    }).catch(() => {
                        $$("invs:note").hideProgress()
                        $$("invs:note").enable()
                        $$("invs:note").hide()
                    })
                })

            })

        })
        $$("invs:btnreject").attachEvent("onItemClick", () => {
            webix.confirm(_("action_cancel_cancel"), "confirm-warning").then(() => {
                let stt = [""].includes(ENT) ? 2 : 3
                webix.ajax(`api/inv/status/${id}/${stt}`).then(result => {
                    if (result.json() == 1) {
                        webix.message(_("change_status_success"), "success")
                        search()

                    }
                })
            })
        })

        $$("invs:btnmail").attachEvent("onItemClick", () => {
            webix.ajax(`api/inv/mail/${id}`).then(result => {
                const json = result.json()
                $$("mail:form").parse(decodehtml(json))
                $$("mail:content").setContent(htmlToElement(json.html))
                $$("mail:win").show()
            })
        })

        $$("invs:btnhistory").attachEvent("onItemClick", () => {
            hstvan.clearAll()
            $$("invs:hstvan").showOverlay(_("loading"))
            webix.ajax(`api/inv/hstvan/${row.id}`).then(result => {
                let json = result.json()
                if (json.length) {
                    $$("invs:hstvan").hideOverlay()
                    hstvan.parse(json)
                }
                else {
                    $$("invs:hstvan").showOverlay(_("notfound"))
                }
            })
        })

        $$("invs:btnsendback").attachEvent("onItemClick", () => {
            webix.confirm(_("sendback_confirm"), "confirm-warning").then(() => {
            let items = grid.serialize(), arr = [], err_arr = []
            for (let item of items) {
                if (item && item.chk && ((item.status_received == 9 || item.status_received == 0) && item.status == 3 && item.form.length == 1 && item.sendtype == 1 )) arr.push(item.id)
            }
            for (let item of items) {
                if (item && item.chk && !((item.status_received == 9 || item.status_received == 0) && item.status == 3 && item.form.length == 1 && item.sendtype == 1 )) err_arr.push(item.id)
            }
            if(err_arr.length != 0){
                webix.message(_("sendback_inv_warning"), "error", 20000)
                return
            }
            if(arr.length == 0){
                webix.message(_("btnsendback_error_must_select_inv"), "error", expire)
            }else{
                webix.ajax().post(`api/ous/apiSendBackToVan`, { listC: arr }).then(result => {
                    let json = result.json()
                    if (json.result==1){
                        webix.message(_("sent"), "success", expire)
                    }else {
                        webix.message(_("cqt_sent_fail"), "error", expire)
                    }

                })
            }
        })
        })

        $$("invs:btnreset").attachEvent("onItemClick", () => {
            // let arr = []
            webix.confirm(_("reset_confirm"), "confirm-warning").then(() => {
                let items = grid.serialize(), arr = [], err_arr = []
                for (let item of items) {
                    if (item && item.chk && ((item.status_received == 13 && item.status == 3 && item.form.length == 1 ) || (item.status_received == 13 && item.status == 4 && (item.cde != null && item.cde.slice(0,11) == "Bị thay thế") && item.form.length == 1 ))) arr.push(item.id)
                }
                for (let item of items) {
                    if (item && item.chk && !((item.status_received == 13 && item.status == 3 && item.form.length == 1  ) || (item.status_received == 13 && item.status == 4 && (item.cde != null && item.cde.slice(0,11) == "Bị thay thế") && item.form.length == 1 ))) err_arr.push(item.id)
                }
                if(err_arr.length != 0){
                    webix.message(_("reset_inv_warning"), "error", 20000)
                    return
                }
                if(arr.length == 0){
                    webix.message(_("Phải chọn 1 hay nhiều bản ghi"), "error", expire)
                }else{
                //console.log("row",row)
                webix.ajax().post(`api/inv/resetbth`, { listC: arr }).then(result => {
                    let json = result.json()
                    if (json.result==1){
                        webix.message(_("Reset_succ"), "success", expire)
                    }else {
                        webix.message(_("Reset_fail"), "error", expire)
                    }
    
                })
            }
            grid.clearAll()
            grid.loadNext(recpager.getValue(), 0, null, "invs->api/sea", true)
                  
            })
        })
        $$("mail:btnsend").attachEvent("onItemClick", () => {
            if (!$$("mail:form").validate()) return
            const json = $$("mail:form").getValues()
            webix.ajax().post("api/inv/mail/send", json).then(result => {
                webix.message(_("invoice_mailed"), "success")
                $$("mail:win").hide()
                //Nếu là VIB thì search luôn
                if (ENT == "vib") search()
            })
        })
        $$("invs:btnsms").attachEvent("onItemClick", () => {
            webix.ajax(`api/inv/sms/${id}`).then(result => {
                const json = result.json()
                $$("sms:form").parse(json)
                $$("sms:win").show()
            })
        })
        $$("sms:btnsend").attachEvent("onItemClick", () => {
            if (!$$("sms:form").validate()) return
            const json = $$("sms:form").getValues()
            webix.ajax().post("api/inv/sms/send", json).then(result => {
                webix.message(_("invoice_sms"), "success")
                $$("sms:win").hide()
            })
        })
        grid.attachEvent("onBeforeLoad", function () {
            this.showOverlay(_("loading"))
        })

        grid.attachEvent("onAfterLoad", function () {
            let self = this, count = self.count()
            $$("invs:countinv").setValue(`${_("found")} ${count}`)
            if (count) self.hideOverlay()
            else self.showOverlay(_("notfound"))
        })

        grid.attachEvent("onAfterSelect", () => {
            
            row = grid.getSelectedItem()         
            id = row.id
            if (["vib"].includes(ENT)) $$("invs:btnnote").enable()
            if (ROLE.includes('PERM_SEARCH_INV_COPPY')) $$("invs:btncopy").enable()
            const status = row.status, status_received = row.status_received
             
            if (ENT == "vhc") {
                if (status == 2 || status == 1) {
                    $$("invs:btnEStatus").disable()
                } else {
                    $$("invs:btnEStatus").enable()
                }
                $$("invs:btnEDate").enable()
                idEnv = row.id
                oldD = new Date(row.idt)
            }
            if (ENT == "vib") {
                const c2 = row.c2
                if (status == 3 && c2 == null) {
                    $$("invs:btnnumentry").enable()
                } else {
                    $$("invs:btnnumentry").disable()
                }
            }
            
            if (row.adjtyp == 1 || row.adjtyp == 2 || row.cvt == 1) {
                $$("invs:btncopy").disable()
            }
            if (row.sendtype == 1) {
                if (status == 3 || (status == 4 && row.cid == null)) {
                    if ( row.status_received == 8 || row.status_received == 10 || row.status_received == 0 ) $$("invs:btnwnwin").enable()
                }
            }
            else if (row.sendtype == 2) {
                if ( (row.status_received == 12 || row.status_received == 0) && (status == 3 || status == 4)){
                    $$("invs:btnbthwin").enable()
                }
                else $$("invs:btnbthwin").disable()
                //$$("invs:btnwnwin").disable()
            }
            else { // TT32
                if (status == 3 || (status == 4 && row.cid == null)) {
                    $$("invs:btnwnwin").enable()
                }
                else $$("invs:btnbthwin").disable()
            }
            // if((status_received == 0 || status_received == 9) && status == 3 ){
            //     //grid.showColumn("chk");
            //     $$("invs:btnsendback").enable()
            // }
            // else{
            //     //grid.hideColumn("chk");
            //     $$("invs:btnsendback").disable()
            // }
            if (status == 3) {
                if (ROLE.includes('PERM_MAIL')) $$("invs:btnmail").enable()
                if (ROLE.includes('PERM_SMS')) $$("invs:btnsms").enable()
                $$("invs:btnhistory").enable()   
                // if(row.sendtype == 1){
                //     $$("invs:btnhistory").enable()
                // }
                // else{
                //     $$("invs:btnhistory").disable()
                // }
                
                if (ROLE.includes('PERM_SEARCH_INV_CONVERT')) $$("invs:btnconvert").enable()
                // if (!row.sendtype) {
                //     if (ROLE.includes('PERM_MAIL')) $$("invs:btnmail").enable()
                //     if (ROLE.includes('PERM_SMS')) $$("invs:btnsms").enable()
                // }
                // else if (row.sendtype == 2) {
                //     if (ROLE.includes('PERM_MAIL')) $$("invs:btnmail").enable()
                //     if (ROLE.includes('PERM_SMS')) $$("invs:btnsms").enable()
                // }
                // else if ((row.serial[0]=="C" && row.status_received == 10) || row.serial[0]=="K" && row.status_received == 8) {
                //     if (ROLE.includes('PERM_MAIL')) $$("invs:btnmail").enable()
                //     if (ROLE.includes('PERM_SMS')) $$("invs:btnsms").enable()
                // }
                const adjtyp = row.adjtyp, cid = row.cid
                const dtime = new Date().getMonth()
                const oldtime = new Date(row.idt).getMonth()
                if (!adjtyp) {//hd goc  
                    //chua bi dc
                    if (!["vcm"].includes(ENT)) {
                        if (ROLE.includes('PERM_INVOICE_CANCEL')) $$("invs:btncancel").enable()
                        if (ROLE.includes('PERM_VOID_WAIT') && !row.adjdes) $$("invs:btntrash").enable()

                        if (!cid) { // cid null
                            if(row.sendtype == 1) {
                                const serial = row.serial
                                const _serial = serial[0]
                                if (_serial == "C") {
                                    if (ROLE.includes('PERM_INVOICE_REPLACE') && row.status_received == 10) $$("invs:btnrep").enable()
                                    if (ROLE.includes('PERM_INVOICE_ADJUST') && row.status_received == 10) $$("invs:btnadj").enable()
                                    // if (!["hlv", "hdb", "ssi"].includes(ENT)) {
                                    //     if (["sgr"].includes(ENT)) {
                                    //         if (dtime == oldtime) {
                                    //             if (ROLE.includes('PERM_INVOICE_CANCEL')) $$("invs:btncancel").enable()
                                    //         }
                                    //     } else if (ROLE.includes('PERM_INVOICE_CANCEL')) $$("invs:btncancel").enable()
                                    // }
                                    if (["vib"].includes(ENT)) $$("invs:btnnote").enable()
                                }
                                if (_serial == "K") {
                                    if (ROLE.includes('PERM_INVOICE_REPLACE') && row.status_received == 8 ) $$("invs:btnrep").enable()
                                    if (ROLE.includes('PERM_INVOICE_ADJUST') && row.status_received == 8) $$("invs:btnadj").enable()
                                    // if (!["hlv", "hdb", "ssi"].includes(ENT)) {
                                    //     if (["sgr"].includes(ENT)) {
                                    //         if (dtime == oldtime) {
                                    //             if (ROLE.includes('PERM_INVOICE_CANCEL')) $$("invs:btncancel").enable()
                                    //         }
                                    //     } else if (ROLE.includes('PERM_INVOICE_CANCEL')) $$("invs:btncancel").enable()
                                    // }
                                    if (["vib"].includes(ENT)) $$("invs:btnnote").enable()
                                }
                            }
                            else {
                                if (ROLE.includes('PERM_INVOICE_REPLACE')) $$("invs:btnrep").enable()
                                if (ROLE.includes('PERM_INVOICE_ADJUST')) $$("invs:btnadj").enable()
                            }
                        } else {
                            if(row.sendtype == 1) {
                                const serial = row.serial
                                const _serial = serial[0]
                                if (_serial == "C") {
                                    if (ROLE.includes('PERM_INVOICE_ADJUST') && row.status_received == 10) $$("invs:btnadj").enable()
                                    // if (ROLE.includes('PERM_INVOICE_REPLACE') && row.status_received == 10) $$("invs:btnrep").enable()
                                }
                                if (_serial == "K") {
                                    if (ROLE.includes('PERM_INVOICE_ADJUST') && row.status_received == 8) $$("invs:btnadj").enable()
                                    // if (ROLE.includes('PERM_INVOICE_REPLACE') && row.status_received == 8) $$("invs:btnrep").enable()
                                }
                                 $$("invs:btncancel").disable()
                                 $$("invs:btntrash").disable()
                            }
                            if(!row.sendtype) { // sendtype is null and cid not null
                                if (ROLE.includes('PERM_INVOICE_ADJUST')) $$("invs:btnadj").enable()
                                  $$("invs:btncancel").disable()
                                  $$("invs:btntrash").disable()
                            } // sendtype =2
                            else {
                                if (ROLE.includes('PERM_INVOICE_ADJUST') ) $$("invs:btnadj").enable()
                                // if (ROLE.includes('PERM_INVOICE_REPLACE') ) $$("invs:btnrep").enable()
                                 $$("invs:btncancel").disable()
                                 $$("invs:btntrash").disable()
                            }
                        }
                        
                    } else {//voi vcm dc dieu chinh nhieu lan
                        if (!cid) {
                            if (ROLE.includes('PERM_INVOICE_REPLACE') && row.status_received == 8 ) $$("invs:btnrep").enable()
                            if (!["hlv", "hdb", "ssi"].includes(ENT)) {
                                if (ROLE.includes('PERM_INVOICE_CANCEL')) $$("invs:btncancel").enable()
                            }
                            if (ROLE.includes('PERM_VOID_WAIT') && !row.adjdes) $$("invs:btntrash").enable()
                            if (ROLE.includes('PERM_INVOICE_ADJUST') && row.status_received == 8 ) $$("invs:btnadj").enable()
                        } else {
                            if (ROLE.includes('PERM_INVOICE_ADJUST') && row.status_received == 8 ) $$("invs:btnadj").enable()
                        }
                    }
                }
                else if (adjtyp == 1) {//hd tt
                    if (!["vib"].includes(ENT)) {
                        if(row.sendtype == 1) {
                            if (row.serial[0] == "K") {
                                if (ROLE.includes('PERM_INVOICE_REPLACE') && row.status_received == 8 ) $$("invs:btnrep").enable()
                            }
                            if (row.serial[0] == "C") {
                                if (ROLE.includes('PERM_INVOICE_REPLACE') && row.status_received == 10 ) $$("invs:btnrep").enable()
                            }
                        }
                        else {
                            if (ROLE.includes('PERM_INVOICE_REPLACE')) $$("invs:btnrep").enable()
                        }
                    }
                    else {
                        if (!cid) {
                            if (ROLE.includes('PERM_INVOICE_REPLACE') && row.status_received == 8) $$("invs:btnrep").enable()
                        }
                    }
                    if (["vcm", "mzh"].includes(ENT)) {
                        if (ROLE.includes('PERM_INVOICE_ADJUST') && row.status_received == 4 ) $$("invs:btnadj").enable()
                    }
                    if (!["hlv", "hdb", "ssi"].includes(ENT)) {
                        if (["sgr"].includes(ENT)) {
                            if (dtime == oldtime) {
                                if (ROLE.includes('PERM_INVOICE_CANCEL')) $$("invs:btncancel").enable()
                            }
                        } else if (ROLE.includes('PERM_INVOICE_CANCEL')) $$("invs:btncancel").enable()
                    }
                    if (ROLE.includes('PERM_VOID_WAIT') && !row.adjdes) $$("invs:btntrash").enable()
                    $$("invs:btncancel").disable()
                    $$("invs:btntrash").disable()
                }
                else if (adjtyp == 2) {
                    // hóa đơn điều chỉnh ko được điều chỉnh tiếp
                    if (ROLE.includes('PERM_INVOICE_ADJUST') ) $$("invs:btnadj").disable()
                    if (!cid) {
                        // if (ROLE.includes('PERM_INVOICE_ADJUST') ) $$("invs:btnadj").enable()
                        if (["vib"].includes(ENT)) $$("invs:btnnote").enable()
                        // if (!["hlv", "hdb", "ssi"].includes(ENT)) {
                        //     if (["sgr"].includes(ENT)) {
                        //         if (dtime == oldtime) {
                        //             if (ROLE.includes('PERM_INVOICE_CANCEL')) $$("invs:btncancel").enable()
                        //         }
                        //     } else if (ROLE.includes('PERM_INVOICE_CANCEL')) $$("invs:btncancel").enable()
                        // }
                        //if (ROLE.includes('PERM_VOID_WAIT')) $$("invs:btntrash").enable()
                        //$$("invs:btnwnwin").enable()
                    }
                    $$("invs:btncancel").disable()
                    $$("invs:btntrash").disable()
                }
            }
            else if (status == 4 && ENT == 'bvb') {
                if (ROLE.includes('PERM_MAIL')) $$("invs:btnmail").enable()
                if (ROLE.includes('PERM_SMS')) $$("invs:btnsms").enable()
            }
            else if (status == 2) {
                if (!["hlv", "tlg"].includes(ENT)) {
                    // if (ROLE.includes('PERM_INVOICE_CANCEL')) $$("invs:btncancel").enable()
                    // if (ROLE.includes('PERM_VOID_WAIT')) $$("invs:btntrash").enable()
                }
                if (["vib"].includes(ENT)) {
                    // if (ROLE.includes('PERM_VOID_WAIT')) $$("invs:btntrash").enable()
                }
            }
            else if (status == 6) {
                if (["hdb", "ssi", "bvb"].includes(ENT)) {
                    if (ROLE.includes('PERM_INVOICE_CANCEL')) $$("invs:btncancel").enable()
                }
                if (ROLE.includes('PERM_SEARCH_INV_REJECT_CANCEL')) $$("invs:btnreject").enable()
            }
            $$("invs:file").enable()
            $$("invs:files").enable()
            if (row.type == "03XKNB") {
                $$("invs:btnadj").disable()
                if (["vib"].includes(ENT)) $$("invs:btnnote").disable()
            }
        })
        if(ENT=="jpm") $$("invs:btnsms").hide()
        grid.attachEvent("onAfterUnSelect", () => {
            row = null
            id = null
            $$("invs:btncopy").disable()
            $$("invs:btnrep").disable()
            if (!["hlv"].includes(ENT)) $$("invs:btncancel").disable()
            $$("invs:btntrash").disable()
            $$("invs:btnreject").disable()
            $$("invs:btnmail").disable()
            $$("invs:btnsms").disable()
            $$("invs:btnadj").disable()
            if (["vib"].includes(ENT)) $$("invs:btnnote").disable()
            $$("invs:btnconvert").disable()
            $$("invs:btnhistory").disable()
            //$$("invs:btnsendback").disable()
            $$("invs:file").disable()
            $$("invs:btnwnwin").disable()
            $$("invs:btnbthwin").disable()
        })
        if (ENT == "scb") {
            grid.attachEvent("onItemDblClick", function (id, e, node) {
                let idTrans = id.row, dataTrans = [], index
                let r = webix.ajax().sync().get(`api/tran/detail/${idTrans}`)
                let result = JSON.parse(r.response)
                dataTrans = result.data
                console.log(dataTrans)

                $$("invs:dataDetail").clearAll()
                if (dataTrans) {
                    index = dataTrans.length
                    for (let row of dataTrans) {
                        $$("invs:dataDetail").add({
                            id: index,
                            name: row.name,
                            status: row.statusGD,
                            trantype: row.trantype,
                            valueDate: row.c0,
                            vrt: row.vrt,
                            amount: row.amount,
                            total: row.total
                        }, 0)
                        index--
                    }
                }

                $$("win_lbl2").define("label", _("invoice_details"))
                $$("win_lbl2").refresh()
                $$("invs:detailWin").show()
            })
        }

        recpager.attachEvent("onChange", () => {
            if (grid.getHeaderContent("chk1")) grid.getHeaderContent("chk1").uncheck()
            $$("invs:pager").config.size = recpager.getValue()
            // if (form.validate()) search()
            grid.clearAll()
            grid.loadNext(recpager.getValue(), 0, null, "invs->api/sea", true)
        })

        $$("invs:btnwnwin").attachEvent("onItemClick", () => {
            let invinfo = $$("invs:grid").getSelectedItem(), type = invinfo.type, serial = invinfo.serial
            let params = {type: type, serial: serial}
            webix.ajax().get("api/serial/sendtype", params).then(result => {
                const json = result.json()
                let sendtype = json[0].sendtype
                if(sendtype && sendtype==1) {
                    this.Wncreatewin.show({ 
                        purpose: WN_CRE_WIN_BTNSAV_PURPOSE.SEND_NOW,
                        form: JSON.parse('{"action":"DIEUCHINH"}'),
                        grid: $$("invs:grid").getSelectedItem(),
                    })
                }
                if(!sendtype) {
                    this.Wncreatewin.show({ 
                        purpose: WN_CRE_WIN_BTNSAV_PURPOSE.SEND_NOW,
                        form: JSON.parse('{"action":"DIEUCHINH"}'),
                        tt32: true,
                        grid: $$("invs:grid").getSelectedItem(),
                    })
                }
            })
        })
        $$("invs:btnbthwin").attachEvent("onItemClick", () => {
            // let arr = []
            // let grida = $$("invs:grid")  
            // var size = grida.getPager().config.size
            // var page = grida.getPage()
            // var ind = size*page
            // for ( let i=0; i<size; i++){
            //     let id = grida.getIdByIndex(ind+i)
            //     if (grida.getItem(id) && grida.getItem(id).chk) {
            //         let type = grida.getItem(id).type, serial = grida.getItem(id).serial
            //         let params = {type: type, serial: serial}
            //         let r = webix.ajax().sync().get("api/serial/sendtype", params)
            //         let result = JSON.parse(r.response)
            //         let sendtype = result[0].sendtype
            //         if(sendtype && sendtype==2) {
            //                 arr.push(grida.getItem(id))
            //         }
            //         else{
            //             webix.message({type:"error", text: _("error_serial_bth")})
            //             return false
            //         }
            //     }
            // }
            // if (arr.length < 1) {
            //     webix.message(_("invoice_must_select"))
            //     return false
            // }
            row = grid.getSelectedItem()
            this.BTHwin.show({
                insertnew: row.inv_adj ? false : true,
                viewCQT: false,
                grid: $$("invs:grid").getSelectedItem(),
            }, search())
        })
        const createMinute = () => {
            const id = row.id, status = row.status
            let fn, fext = minutes_ext == "doc" ? minutes_ext : "docx"
            if (status == 4) fn = "Huy"
            else {
                const adjtyp = row.adjtyp
                if (adjtyp == 1) {
                    if (status == 2 || status == 3) {
                        fn = "ThayThe"

                    } else {
                        webix.message(_("status_not_load"), "error", expire)
                        return false
                    }

                }
                else if (adjtyp == 2) {
                    if (status == 2 || status == 3) {
                        fn = "DieuChinh"

                    } else {
                        webix.message(_("status_not_load"), "error", expire)
                        return false
                    }
                }
                else {
                    if (ENT == 'hlv' && row.status == 3 && row.cde) {
                        if ((row.cde).indexOf("Bị điều chỉnh") >= 0) fn = "DieuChinh"
                        //ko check gi ca
                    } else {
                        if (status == 4) webix.message(_("envoice_not_cancellation_record"), "error", expire)
                        else webix.message(_("envoice_not_record"), "error", expire)
                        return false
                    }

                }
            }
            webix.ajax().response("blob").get(`api/inv/minutes/custom/${id}`).then(data => webix.html.download(data, `BB_${fn}.${fext}`))
        }
        const downloadMinute = () => {
            const id = row.id
            let fext = minutes_ext ? minutes_ext : "docx"
            let params = { download: true }
            $$("invs:grid").disable();
            webix.message(`${_("Downloading")}.......`, "info", -1, "downloading");

            if (ENT == 'vib') {
                webix.ajax().response("blob").get(`api/inv/minutes/custom/${id}`, params).then(data => {

                    webix.html.download(data, `BB_${id}.zip`)
                    $$("invs:grid").enable()
                    webix.message.hide("downloading");
                }).fail(err => {
                    $$("invs:grid").enable()
                    webix.message.hide("downloading");
                })
            } else {
                webix.ajax().response("blob").get(`api/inv/minutes/custom/${id}`, params).then(data => {
                    webix.html.download(data, `BB_${id}.${fext}`)
                    $$("invs:grid").enable()
                    webix.message.hide("downloading");
                }).fail(err => {
                    $$("invs:grid").enable()
                    webix.message.hide("downloading");
                })
            }

        }

        const go24 = (d) => {
            if (row.status == 1 || row.status == 2) {
                const type = d > 0 ? "tiến" : "lùi"
                webix.confirm(`${_("want_invoice")} ${id} ${type} ?`, "confirm-warning").then(() => {
                    webix.ajax().post("api/inv/go/24", { d: d, id: id, idt: row.idt, type: row.type, form: row.form, serial: row.serial, seq: row.seq }).then(result => {
                        if (result.json() == 1) {
                            webix.message(`${_("invoice_was")} ${type}`, "success")
                            search()
                        }
                    })
                })
            }
            else webix.message(_("select_pending_invoice"), "success")
        }

        const contextItem = []
        if (["dtt", "scb", "vib"].includes(ENT)) contextItem.push({ id: 4, value: _("minutes_download_2") })
        contextItem.push({ id: 1, value: _("minutes_download") })
        if (ENT != "hdb" && ROLE.includes('PERM_REDIRECT_IDT')) contextItem.push({ id: 2, value: _("backdate") }, { id: 3, value: _("forward") })
        webix.ui({
            view: "contextmenu",
            id: "contexgrid",
            data: contextItem,
            on: {
                onMenuItemClick: id => {
                    if (id == 1) createMinute()
                    else if (id == 2) go24(-1)
                    else if (id == 3) go24(1)
                    else if (id == 4) downloadMinute()
                }
            }
        })
        if (ROLE.includes('PERM_SEARCH_INV_DOWNLOAD_MINUTES')) {
            $$("contexgrid").attachTo(grid)
        }

        $$("invs:file").attachEvent("onAfterFileAdd", (file) => {
            webix.confirm(_("up_minutes_confirm"), "confirm-warning").then(() => {
                let typemin = "normal", invinfo = $$("invs:grid").getSelectedItem(), iid = invinfo.id, adjtyp = invinfo.adjtyp
                if (invinfo.status == 4) typemin = "can"
                else if (invinfo.status == 6) typemin = "can"
                else {
                    if (iid) {
                        if (adjtyp == 1) {
                            typemin = "rep"
                            // can xem lai sao lai tai cho hoa don goc
                            iid = invinfo.pid
                        } else if (adjtyp == 2) {
                            typemin = "adj"
                            // can xem lai sao lai tai cho hoa don goc
                            iid = invinfo.pid
                        }
                    }
                }
                if (ENT == 'vib') iid = invinfo.id
                const files = uploader.files, file_id = files.getFirstId()
                if (file_id) {
                    var params = { type: typemin, id: iid }
                    uploader.define("formData", params)
                    $$("invs:grid").disable();
                    webix.message(`${_("uploading")}.......`, "info", -1, "uploading");
                    uploader.send(rs => {
                        if (rs.err) webix.message(rs.err, "error")
                        else if (rs.ok) {
                            webix.message(_("up_minutes_success"), "success")
                            // search()
                            if (ENT == "vib") search()
                        }
                        else webix.message(_("up_minutes_fail"), "error")
                        $$("invs:grid").enable()
                        webix.message.hide("uploading");
                    })
                }
                else webix.message(_("file_required"))
            })
        })
        $$("invs:btnSaveFile").attachEvent("onItemClick", () => {

            webix.confirm(_("up_minutes_confirm"), "confirm-warning").then(() => {
                $$("invs:btnSaveFile").disable()
                let typemin = "normal", invinfo = $$("invs:grid").getSelectedItem(), iid = invinfo.id, adjtyp = invinfo.adjtyp
                if (invinfo.status == 4) typemin = "can"
                else if (invinfo.status == 6) typemin = "can"
                else {
                    if (iid) {
                        if (adjtyp == 1) {
                            typemin = "rep"
                            // can xem lai sao lai tai cho hoa don goc
                            iid = invinfo.pid
                        } else if (adjtyp == 2) {
                            typemin = "adj"
                            // can xem lai sao lai tai cho hoa don goc
                            iid = invinfo.pid
                        }
                    }
                }
                if (ENT == 'vib') iid = invinfo.id
                const files = $$("upload:file").files, file_id = files.getFirstId()
                if (file_id) {
                    var params = { type: typemin, id: iid }
                    $$("upload:file").define("formData", params)
                    $$("invs:grid").disable();
                    webix.message(`${_("uploading")}.......`, "info", -1, "uploading");
                    $$("upload:file").send(rs => {
                        if (rs.err) webix.message(rs.err, "error")
                        else if (rs.ok) {
                            $$("invs:upload").hide();
                            webix.message(_("up_minutes_success"), "success")
                            // search()
                            if (ENT == "vib") search()
                        }
                        else webix.message(_("up_minutes_fail"), "error")
                        $$("invs:grid").enable()
                        webix.message.hide("uploading");
                    })
                }
                else webix.message(_("file_required"))
            })
        })
        //LuaDTN sua vib
        if (ENT == "vib") {

            $$("invs:btnExitDel").attachEvent("onItemClick", () => {
                $$("invs:del").hide()
                arrID = []
            })
            $$("invs:btnSaveDel").attachEvent("onItemClick", () => {
                webix.confirm({
                    title: _("notice"),
                    ok: "Yes", cancel: "No",
                    text: _("save_changes_ask")
                })
                    .then(function () {
                        webix.ajax().post("api/inv/delFile", { arrID: arrID, id: idDel.row }).then(rs => {
                            let rsDel = rs, count = rs.json()
                            if (rsDel) {
                                webix.alert({
                                    title: _("notice"),
                                    text: _("save_success"),
                                    type: "alert-error"
                                }).then(function (result) {
                                    if (count > 0) {
                                        $$("invs:del").hide()
                                    } else {
                                        $$("invs:del").hide()
                                        search()
                                    }
                                });
                            }
                            arrID = []
                        })
                    })
            })
        }
        if (ENT == "vhc") {
            $$("invs:btnEDate").attachEvent("onItemClick", () => {
                $$("invs:winED").show()
                $$("invs:envName").setValue(idEnv)
                $$("invs:dateChange").setValue(date)
                $$("invs:dateOld").setValue(oldD)
            })
            $$("invs:btnExitD").attachEvent("onItemClick", () => {
                $$("invs:winED").hide()
            })
            $$("invs:btnSaveD").attachEvent("onItemClick", () => {
                webix.confirm(_("change_date_notif"), "confirm-warning")
                    .then(function (result) {
                        let params = $$("invs:formED").getValues()
                        let dateChange = new Date($$("invs:dateChange").getValue())
                        if (dateChange > date) {
                            webix.message("Không được chọn ngày lớn hơn ngày hiện tại!");
                        } else if (!$$("invs:formED").validate()) {
                            webix.message("Không được để trống thông tin!");
                        } else {
                            webix.ajax().post("api/inv/changeD", params).then(result => {
                                let json = result.json()
                                if (json == 1) {
                                    webix.alert({
                                        title: "Thông báo",
                                        text: "Cập nhật ngày hóa đơn thành công!",
                                        type: "alert-error"
                                    }).then(function (result) {
                                        window.location.reload(true)
                                    });
                                } else if (json.stt == 0) {
                                    webix.alert({
                                        title: "Thông báo",
                                        text: "Không được để ngày hóa đơn sau ngày phát hành dải hóa đơn! Ngày phát hành dải hóa đơn là " + json.fd,
                                        type: "alert-error"
                                    })
                                } else {
                                    webix.alert({
                                        title: "Thông báo",
                                        text: "Dải hóa đơn không có hiệu lực từ ngày " + json.td + "! Hãy chọn ngày trước đó!",
                                        type: "alert-error"
                                    })
                                }
                            })
                        }
                    })
            })
            $$("invs:btnEStatus").attachEvent("onItemClick", () => {
                webix.confirm({
                    title: "Thông báo",
                    text: _("change_status_Wappr")
                }).then(function (result) {
                    webix.ajax().post("api/inv/changeS", { id: idEnv }).then(data => {
                        let json = data
                        if (json) {
                            webix.alert({
                                title: "Thông báo",
                                text: "Cập nhật trạng thái hóa đơn thành công",
                                type: "alert-error"
                            }).then(function (result) {
                                window.location.reload(true)
                            });
                        }
                    })
                })
            })
        }
        //Xóa nhiều hóa đơn
        if (ENT == "scb") {
            $$("invs:btndelAll").attachEvent("onItemClick", () => {
                // grid.showProgress();

                let form = $$("invs:form").getValues()
                webix.confirm({
                    title: "Thông báo",
                    text: _("del_all_confirm")
                }).then(function (result) {
                    $$("invs:grid").disable()
                    $$("invs:form").disable()
                    $$("invs:btndelAll").disable()
                    $$("invs:btndel").disable()
                    $$("invs:form").showProgress({
                        type: "icon",
                        hide: false
                    })
                    webix.ajax().post("api/inv/dellAll", form).then(data => {
                        let json = data
                        $$("invs:grid").enable()
                        $$("invs:form").enable()
                        $$("invs:btndelAll").enable()
                        if (ROLE.includes('PERM_SEARCH_INV_DELETE')) $$("invs:btndel").enable()
                        $$("invs:form").hideProgress()
                        if (json) {
                            webix.alert({
                                title: "Thông báo",
                                text: String(_("del_ok_msg")).replace("#hunglq#", "tất cả") + " " + String(_("invstatus_1")),
                                type: "alert-error"
                            }).then(function (result) {
                                window.location.reload(true)
                            });
                        }
                    })
                })
            })
        }

        $$("invs:btndelAll").attachEvent("onItemClick", () => {
            // grid.showProgress();

            let form = $$("invs:form").getValues()
            webix.confirm({
                title: "Thông báo",
                text: _("del_all_confirm")
            }).then(function (result) {
                $$("invs:grid").disable()
                $$("invs:form").disable()
                $$("invs:btndelAll").disable()
                $$("invs:btndel").disable()
                $$("invs:form").showProgress({
                    type: "icon",
                    hide: false
                })
                let obj = form
                Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])

                webix.ajax().post("api/inv/dellAll", obj).then(data => {
                    let json = data
                    if (json) {
                        webix.alert({
                            title: "Thông báo",
                            text: String(_("del_ok_msg")).replace("#hunglq#", "tất cả") + " " + String(_("invstatus_1")),
                            type: "alert-error"
                        }).then(function (result) {
                            window.location.reload(true)
                        });
                    }
                })
                $$("invs:grid").enable()
                $$("invs:form").enable()
                $$("invs:btndelAll").enable()
                if (ROLE.includes('PERM_SEARCH_INV_DELETE')) $$("invs:btndel").enable()
                $$("invs:form").hideProgress()
            })
        })


webix.ui({

    view: "popup",
    id: "viewXml",
    width: 900,
    height: 700,
    margin: 50,
    body: { 
        view:"textarea",id:"invs:viewXml", 
        // value:text,
        height:900 
    }
});

webix.ui({

    view: "popup",
    id: "hstvan",
    width: 950,
    height: 400,
    margin: 50,

    body: {
        view: "datatable",
        id: "invs:hstvan",
        tooltip: true,
        columns: [
            { id: "datetime_trans", header: { type: "datetime", text: _("time"), css: "header" }, adjust: true },
            // { id: "status", header: { text: _("status_cqt"), css: "header" }, collection: INV_CQT_STATUS, adjust: true },
            { id: "description", header: { text: _("description"), css: "header" }, fillspace: true, css: "van_history_description" },
            { header: { text:  _("Message_gdt"), css: "header" }, adjust: true, popup: "viewXml", 
                template: obj => {  
                    let _result = "";
                    let r_xml = obj.r_xml;
                    if ( r_xml != null) {
                        _result += `<span class='webix_icon wxi-eye', title='${_("view")}'></span>` ;
                    } 
                    return _result;
                }
            }
        ],
        onClick:
        {
            "wxi-eye": function (e, r) {
                webix.ajax(`api/inv/viewXml/${r.row}`).then(result => {
                    let json = result.json()
                    if (json.length) {
                        var xmlPretty = ''
                        if(json[0].r_xml != null){
                            xmlPretty = format(json[0].r_xml, {
                                indentation: '\t', 
                                filter: (node) => node.type !== 'Comment', 
                                collapseContent: true, 
                                lineSeparator: '\n'
                            });
                        }
                        $$("invs:viewXml").setValue(xmlPretty)
                        var left = (screen.width/2)-(1000/2)
                        var top = (screen.height/2)-(800/2)
                        $$("invs:viewXml").show({
                            x:left, // left offset from the right side
                            y:top // top offset
                        });
                    }
                })
            }
        }
    }
});
}}