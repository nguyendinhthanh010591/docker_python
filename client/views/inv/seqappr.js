import { JetView } from "webix-jet"
import { size, LinkedInputs3, t2s, d2s, gridnf0, ITYPE, ENT, setConfStaFld } from "models/util"
import Iwin from "views/inv/iwin"
export default class SeqView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        webix.protoUI({ name: "dependent3", $cssName: "richselect" }, LinkedInputs3, webix.ui.richselect)
        webix.proxy.seq = {
            $proxy: true,
            load: function (view, params) {
                let obj = $$("seq:form").getValues()
                Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                if (!params) params = {}
                params.filter = obj
                return webix.ajax(this.source, params)
            }
        }
        let form = {
            view: "form",
            id: "seq:form",
            padding: 3,
            margin: 3,
            elements:
                [
                    {
                        cols: [
                            { id: "seq:countinv", view: "label", width: 150 },
                            { id: "type", name: "type", label: _("invoice_type"), view: "combo", options: ITYPE(this.app), gravity: 1.5 },
                            { id: "form", name: "form", label: _("form"), view: "dependent3", options: [], master: "type", dependentUrl: "api/seq/fbt?id=", relatedView: "serial", relatedAction: "enable" },
                            { id: "serial", name: "serial", label: _("serial"), view: "dependent3", options: [], master: "form", dependentUrl: "api/seq/sbf?id=", disabled: true },
                            { id: "td", name: "td", view: "datepicker", label: _("td"), type: "icon", icon: "mdi", stringResult: true },
                            { view: "button", id: "seq:btndel", type: "icon", icon: "mdi mdi-delete", label: _("invoice_del_btn"), width: 100, hidden:true},
                            { view: "button", id: "seq:btnseq", type: "icon", icon: "mdi mdi-sort-numeric", label: _("approve"), disabled: true, width: 100 },
                            { view: "button", id: "seq:btnsearch", type: "icon", icon: "wxi-search", label: _("search"), width: 100 }
                        ]
                    }
                ]
        }
        let columns = [
            { id: "chk", header: { content: "masterCheckbox", css: "center" }, adjust: "header", css: "center",hidden:true, template: "{common.checkbox()}" },
            { id: "vie", header: "", width: 35, template: `<span class='webix_icon wxi-search', title='${_("view")}'></span>` },
            { id: "id", header: { text: "ID", css: "header" }, css: "right", adjust: true },
            { id: "idt", header: { text: _("invoice_date"), css: "header" }, format: d2s, adjust: true },
            { id: "form", header: { text: _("form"), css: "header" }, adjust: true },
            { id: "serial", header: { text: _("serial"), css: "header" }, adjust: true },
            { id: "btax", header: { text: _("taxcode"), css: "header" }, adjust: true },
            { id: "bname", header: { text: _("bname"), css: "header" }, adjust: true },
            { id: "buyer", header: { text: _("buyer"), css: "header" }, adjust: true },
            { id: "baddr", header: { text: _("address"), css: "header" }, adjust: true },
            { id: "sumv", header: { text: _("sumv"), css: "header" }, css: "right", adjust: true, format: gridnf0.format },
            { id: "vatv", header: { text: "VAT", css: "header" }, css: "right", adjust: true, format: gridnf0.format },
            { id: "totalv", header: { text: _("totalv"), css: "header" }, css: "right", adjust: true, format: gridnf0.format },
            { id: "note", header: { text: _("note"), css: "header" }, adjust: true },
            { id: "ou", header: { text: _("ou"), css: "header" }, collection: "api/cat/nokache/ou", adjust: true },
            { id: "uc", header: { text: _("creator"), css: "header" }, adjust: true },
            { id: "ic", header: { text: "IC", css: "header" }, adjust: true }
        ]

        if (ENT == 'hdb') {
            columns.splice(8, 3) //Xóa cột tiền VND
            columns.splice(8, 0,
                { id: "sum", header: { text: _("sumo"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat },
                { id: "vat", header: { text: _("vato"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat },
                { id: "total", header: { text: _("totalo"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat })
        }

        if (ENT == 'hlv') {
            columns.push({ id: "adjdes", header: { text: _("replace"), css: "header" }, format: webix.template.escape, adjust: true })
            columns.push({ id: "cde", header: { text: _("replaced"), css: "header" }, adjust: true })
        }
        else {
            columns.push({ id: "adjdes", header: { text: _("repadj"), css: "header" }, format: webix.template.escape, adjust: true })
            columns.push({ id: "cde", header: { text: _("repadjed"), css: "header" },  fillspace:true })
        }
        let pager = { view: "pager", id: "seq:pager", size: size, template: `{common.first()}{common.prev()} {common.pages()}{common.next()}{common.last()}` }

        let grid = {
            view: "datatable",
            id: "seq:grid",
            select: "row",
            resizeColumn: true,
            editable: false,
            sort: false,
            pager: "seq:pager",
            on: { onBeforeSort: function () { return false } },
            onClick: {
                "wxi-search": function (e, r) {
                    const id = r.row
                    webix.ajax(`api/app/htm/${id}`).then(result => {
                       // const json = result.json()
                        //const req = createRequest(json)
                        //jsreport.renderAsync(req).then(res => {
                        //$$("iwin:ipdf").define("src", res.toDataURI())
                        let pdf = result.json()
                        let b64 = pdf.pdf
                        const blob = dataURItoBlob(b64);

                        var temp_url = window.URL.createObjectURL(blob);
                        $$("iwin:ipdf").define("src", temp_url)
                        $$("iwin:win").show()
                        // webix.ajax().post("api/seek/report", req).then(res => {
                        //     let pdf = res.json()
                        //     let b64 = pdf.pdf
                        //     const blob = dataURItoBlob(b64);

                        //     var temp_url = window.URL.createObjectURL(blob);
                        //     $$("iwin:ipdf").define("src", temp_url)
                        //     $$("iwin:win").show()
                        // })

                    })
                }
            },
            columns: columns
        }
        return { paddingX: 2, rows: [form, grid, pager] }
    }
    
    ready(v, urls) {
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
    
    init() {
        this.Iwin = this.ui(Iwin)
        webix.ajax("api/sysdate").then(result => {
            let dt = new Date(result.json())
            $$("td").setValue(dt)
            // $$("syslog:dt").getPopup().getBody().define("minDate", dt)
        })
        let grid = $$("seq:grid")
        $$("iwin:btnxml").hide()
        $$("iwin:btnxml").hide()
        $$("iwin:btnsignpdf").hide()
        webix.extend(grid, webix.ProgressBar)
        const search = () => {
            grid.clearAll()
            grid.loadNext(size, 0, null, "seq->api/seq", true)
        }
        $$("seq:btndel").attachEvent("onItemClick", () => {
            let rows = grid.serialize(), arr = []
            for (let row of rows) {
                if (row && row.chk) arr.push(row)
            }
            if (arr.length < 1) {
                webix.message(_("invoice_must_select"))
                return false
            }
            webix.delay(() => {
                webix.ajax().post("api/inv/seq/del", { invs: arr }).then(result => {
                    let json = result.json()
                    console.log(json.status)
                    if(json.status==1){
                        grid.clearAll()
                        grid.loadNext(size, 0, null, "seq->api/seq", true)
                    }
                }).catch(() => {  })
            })
        })
        if (ENT == "hdb") grid.config.multiselect = true
        else grid.config.multiselect = false
        grid.refresh()

        $$("type").setValue("01GTKT")
        $$("seq:btnsearch").attachEvent("onItemClick", () => {
            search()
        })
        $$("serial").attachEvent("onChange", (n, o) => {
             if (n && n != o) search()
        })
    
        $$("seq:btnseq").attachEvent("onItemClick", () => {
            webix.confirm(_("approve_confirm"), "confirm-warning").then(() => {
                $$("seq:btnseq").disable()
             
                grid.disable();
                grid.showProgress({
                  type:"icon",
                  hide:false
                });
               
                const form = $$("form").getValue(), serial = $$("serial").getValue(),td = $$("td").getValue(),rows = grid.getSelectedItem()
                let seqList = [], idt, id
                    // Cấp số theo dải bé hơn ngày hđ chọn
                idt = rows.idt
                    // id = rows.id
                webix.ajax().post("api/appseq", { idt: idt, form: form, serial: serial, seqList: seqList }).then((result,iid=id) => {
                    const count = result.json()
                    $$("seq:grid").enable()
                    $$("seq:grid").hideProgress()
                    if (count >= 1) {
                        if (iid)  
                        {
                                    webix.alert({
                                    title:_("success"),
                                    ok:"Ok",
                                    text:`${_("approved")} ${_("invoice")} ID: ${iid}`
                                
                                }).then(function(result){
                                    search()
                                   
                                });
                        }
                      //  webix.message(`${_("sequenced")} ${_("invoice")} ID: ${iid}`,"success") 
                        else 
                        {
                            webix.alert({
                                title:_("success"),
                                ok:"Ok",
                                text:`${_("approved")} ${count} ${_("invoice")}`
                                
                            }).then(function(result){
                                search()
                              
                            });
                        } 
                    }
                    else {
                        webix.ajax().post(`${USB}xml`, rows).then(result => {
                            const rows = result.json()
                            webix.ajax().put("api/sign", rows).then(result => {
                                if (result.json()) webix.message(_("invoice_issued"), "success")
                            })
                        }).catch(e => { webix.message(_("certificate_error"), "error") })
                    }
                    
                   
                    //webix.message(`${_("sequenced")} ${count} ${_("invoice")}`, "success")
                    //search()
                }).catch(()=>{ 
                     $$("seq:btnseq").enable()
                     $$("seq:grid").enable()
                     $$("seq:grid").hideProgress()})
            })
        })
        grid.attachEvent("onBeforeLoad", function () {
            this.showOverlay(_("loading"))
        })
        grid.attachEvent("onAfterLoad", function () {
            let self = this, count = self.count()
            $$("seq:countinv").setValue(`${_("found")} ${count}`)
            if (count) self.hideOverlay()
            else self.showOverlay(_("notfound"))
        })
        grid.attachEvent("onAfterSelect", sel => {
            //grid.selectRange(grid.getFirstId(), sel.id)
            $$("seq:btnseq").enable()
        })
        grid.attachEvent("onAfterUnSelect", () => {
            const r = grid.getSelectedItem()
            if(!r || r.length <= 0) $$("seq:btnseq").disable()
        })
    }
}