import { JetView } from "webix-jet"
import { expire, size, d2s, gridif, BTYPE, TAX, PERIODS, CATEGORIES,ROLE, STATUS } from "models/util"
let row
class Pwin extends JetView {
    config() {
        return {
            view: "window",
            id: "bin:win",
            modal: true,
            fullscreen: true,
            close: true,
            body: { id: "bin:pdf", view: "iframe", borderless: true }
        }
    }
    show() {
        this.getRoot().show()
    }
}
export default class BinView extends JetView {
    config() {
       // _ = this.app.getService("locale")._
        webix.proxy.bin = {
            $proxy: true,
            load: function (view, params) {
                let obj = $$("bin:form").getValues()
                Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                if (!params) params = {}
                params.filter = obj
                return webix.ajax(this.source, params)
            }
        }

        const XLS = [
            { id: "idt", header: _("invoice_date") },
            { id: "form", header: _("form") },
            { id: "serial", header: _("serial") },
            { id: "seq", header: _("seq"), exportType: "string" },
            { id: "stax", header: _("taxcode"), exportType: "string" },
            { id: "sname", header: _("bname") },
            { id: "saddr", header: _("buyer") },
            { id: "stel", header: "Tel" },
            { id: "smail", header: "Mail" },
            { id: "sumv", header: _("sumv"), exportType: "number", exportFormat: gridif.format },
            { id: "vatv", header: "VAT", exportType: "number", exportFormat: gridif.format },
            { id: "totalv", header: _("totalv"), exportType: "number", exportFormat: gridif.format },
            { id: "paid", header: _("paid") },
            { id: "note", header: _("note") },
        ]
        let pager = { view: "pager", id: "bin:pager", size: size, template: "{common.first()}{common.prev()}{common.pages()}{common.next()}{common.last()}" }
        let grid = {
            view: "datatable",
            id: "bin:grid",
            select: "row",
            multiselect: false,
            resizeColumn: true,
            pager: "bin:pager",
            headerRowHeight: 33,
            onContext: {
                webix_view: function (e, id) {
                    if (id && id.row) this.select(id)
                }
            },
            columns: [
                { id: "vie", header: { text: "<span class='webix_icon wxi-download' title='excel'></span>", css: "header" }, width: 35, template: `<span class='webix_icon wxi-search', title='${_("view")}'></span>` },
                { id: "sel", header: "", width: 35, template: `<span class='webix_icon wxi-pencil', title='${_("modify")}'></span>` },
                { id: "id", header: { text: "ID", css: "header" }, sort: "server", adjust: true },
                { id: "idt", header: { text: _("invoice_date"), css: "header" }, sort: "server", format: d2s, adjust: true },
                { id: "form", header: { text: _("form"), css: "header" }, sort: "server", adjust: true },
                { id: "serial", header: { text: _("serial"), css: "header" }, sort: "server", adjust: true },
                { id: "seq", header: { text: _("seq"), css: "header" }, sort: "server", adjust: true },
                { id: "sumv", header: { text: _("sumv"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridif.format },
                { id: "vatv", header: { text: "VAT", css: "header" }, sort: "server", css: "right", adjust: true, format: gridif.format },
                { id: "totalv", header: { text: _("totalv"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridif.format },
                { id: "stax", header: [{ text: "Đơn vị phát hành", css: "header", colspan: 7, height: 20 }, { text: "MST", css: "header", height: 20 }], sort: "server", adjust: true },
                { id: "sname", header: ["", { text: _("bname"), css: "header" }], sort: "server", adjust: true },
                { id: "saddr", header: ["", { text: _("address"), css: "header" }], sort: "server", adjust: true },
                { id: "smail", header: ["", { text: "Mail", css: "header" }], sort: "server", adjust: true },
                { id: "stel", header: ["", { text: _("tel"), css: "header" }], sort: "server", adjust: true },
                { id: "sacc", header: ["", { text: _("account"), css: "header" }], sort: "server", adjust: true },
                { id: "sbank", header: ["", { text: _("bank"), css: "header" }], sort: "server", adjust: true },
                { id: "btax", header: { text: _("taxcode"), css: "header" }, sort: "server", adjust: true },
                { id: "bname", header: { text: _("bname"), css: "header" }, sort: "server", adjust: true },
                { id: "baddr", header: { text: _("address"), css: "header" }, sort: "server", adjust: true },
                { id: "paid", header: { text: _("paid"), css: "header" }, checkValue: 1, uncheckValue: 0, template: "{common.checkbox()}", adjust: true },
                { id: "note", header: { text: _("note"), css: "header" }, adjust: true },
                { id: "del", header: "", width: 35, template: `<span class='webix_icon wxi-trash', title='${_("delete")}'></span>` },

            ],
            onClick:
            {
                "wxi-download": function (e, id) {
                    let obj = $$("bin:form").getValues()
                    Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                    webix.ajax("api/bin/xls", { filter: obj }).then(result => {
                        let data = new webix.DataCollection({ data: result.json() })
                        webix.toExcel(data, { columns: XLS }).then(() => { data.destructor() })
                    })
                },
                "wxi-search": function (e, r) {
                    webix.ajax(`api/bin/view/${r.row}`).then(result => {
                        let {pdf, sizePDF} = result.json()
                        console.log("pdf, sizePDF",pdf, sizePDF)
                        if (sizePDF >= 3145728) webix.message(String(_("file_pdf_size_limit")).replace("#hunglq#", 3145728 / (1024 * 1024)))
                        const blob = dataURItoBlob(pdf);

                        var temp_url = window.URL.createObjectURL(blob);
                        $$("bin:pdf").define("src", temp_url)
                        $$("bin:win").show()
                    })
                },
                "wxi-trash": function (e, id) {
                    let me = this
                    webix.confirm(_("del_confirm_msg"), "confirm-warning").then(() => {
                        webix.ajax(`api/bin/del/${id}`).then(result => {
                            let obj = result.json()
                            if (obj == 1) me.remove(id)
                            return false
                        })
                    })
                },
                "wxi-pencil": function (e, id) {
                    webix.storage.session.put("bin:form", $$("bin:form").getValues())
                    this.$scope.show(`bnv?id=${id}&action=view`)
                },
            }
        }

        let form = {
            view: "form",
            id: "bin:form",
            padding: 3,
            margin: 3,
            elementsConfig: { labelWidth: 70 },
            elements:
                [
                    {
                        cols: [
                            { id: "fd", name: "fd", label: _("fd"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr },
                            { id: "td", name: "td", label: _("td"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr },
                            { id: "type", name: "type", label: _("invoice_type"), view: "combo", placeholder: "Chọn loại HĐ...", options: BTYPE },
                            {
                                gravity: 2, cols: [
                                    { id: "paym", name: "paym", label: _("payment_method"), view: "text", attributes: { maxlength: 100 } },
                                    { id: "curr", name: "curr", label: _("currency"), view: "combo", suggest: { url: "api/cat/kache/currency" } },
                                    { id: "paid", name: "paid", label: _("paid"), view: "richselect", value: "*", options: [{ id: "*", value: _("all") }, { id: "1", value: _("paid") }, { id: "0", value: _("unpaid") }] }
                                ]
                            }
                        ]
                    },
                    {
                        cols:
                            [
                                {
                                    gravity: 2, cols:
                                        [
                                            {
                                                id: "stax", name: "stax", label: _("taxcode"), view: "text", attributes: { maxlength: 100 }, placeholder: _("enter2search_msg"), suggest: {
                                                    view: "gridsuggest",
                                                    fitMaster: true,
                                                    body: {
                                                        url: "api/org/fbn",
                                                        dataFeed: function (str) {
                                                            if (/\;$/.test(str)) {
                                                                let me = this
                                                                me.clearAll()
                                                                const type = $$("private").getValue()
                                                                return me.load(`${me.config.url}?val=${encodeURIComponent(str.substring(0, str.length - 1))}&type=${type}`, me.config.datatype)
                                                            }
                                                            else return
                                                        },
                                                        scroll: true,
                                                        autoheight: false,
                                                        autowidth: false,
                                                        autofocus: true,
                                                        yCount: 10,
                                                        columns: [
                                                            { id: "taxc", header: { text: _("taxcode"), css: "header" }, adjust: true },
                                                            { id: "name", header: { text: _("customer_name"), css: "header" }, adjust: true },
                                                        ]
                                                    }
                                                }
                                            },
                                            { id: "private", tooltip: _("customer_private"), view: "checkbox", checkValue: 1, uncheckValue: 0, value: 1, width: 20 }
                                        ]
                                },
                                { name: "form", label: _("form"), view: "text", attributes: { maxlength: 14 } },
                                {
                                    gravity: 2, cols: [
                                        { name: "serial", label: _("serial"), view: "text", attributes: { maxlength: 100 } },
                                        { name: "seq", label: _("seq"), view: "text", attributes: { maxlength: 7 }, validate: v => { return !v || /^[0-9]{1,7}$/.test(v) }, invalidMessage: _("seq_invalid") },
                                        {
                                            cols: [{},
                                            { id: "bin:btnsearch", view: "button", type: "icon", icon: "wxi-search", label: _("search"), width: 90 }
                                            ]
                                        }
                                    ]
                                }
                            ]
                    },

                ]
            , rules: {
                fd: webix.rules.isNotEmpty,
                td: webix.rules.isNotEmpty,
                $obj: function (data) {
                    if (data.fd > data.td) {
                        webix.message(_("date_invalid_msg"), "error", expire)
                        return false
                    }
                    return true
                }
            }
        }

        const LIST = { id: "bin:filelist", view: "list", type: "uploader", scroll: true, autoheight: true, borderless: false }
        const COLS = [
            pager,
            { id: "countinv", view: "label" },
            {
                id: "bin:file", view: "uploader", multiple: true, autosend: false, link: "bin:filelist", accept: "application/xml", upload: "api/bin/upload/xml", type: "icon", icon: "mdi mdi-file-find-outline", label: _("file_select_xml"), width: 120, on: {
                    onFileUploadError: (response) => {
                        const json = JSON.parse(response.xhr.response)
                        webix.message(json.message)
                    }
                }
            },
            { id: "bin:btnread", view: "button", type: "icon", icon: "mdi mdi-file-upload-outline", label: _("file_read_xml"), width: 120 },
            {
                view: "button", type: "icon", icon: "mdi mdi-file-plus", label: _("invoice_issue"), width: 120, click: () => {
                    if (TAX == "0") {
                        webix.message("Đơn vị không có MST", "error", expire)
                        return
                    }
                    else this.show("inv.bnv")
                }
            },
            { view: "button", id: "bin:btncopy", type: "icon", icon: "mdi mdi-content-copy", label: "Copy", disabled: true, width: 65 }

        ]
        let title_page = {
            view: "label", label: `<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("input_invoice")}</span>`
        }
        return { id:"bin:view", paddingX: 2, rows: [title_page,form, grid, { cols: COLS }, LIST] }
    }
    ready() {
        const key = "bin:form"
        let state = webix.storage.session.get(key)
        if (state) {
            $$(key).setValues(state)
            webix.storage.session.remove(key)
            $$("bin:grid").clearAll()
            $$("bin:grid").loadNext(size, 0, null, "bin->api/bin/get", true)
        }
    }
    init() {
        if (STATUS&&!STATUS.statusinputinv) {
            webix.message(_("input_inv_unavailable"), "error", expire)
        }
        this.Pwin = this.ui(Pwin)
        $$("bin:pdf").attachEvent("onAfterLoad", function () { URL.revokeObjectURL(this.src) })
        const all = { id: "*", value: _("all") }
        let date = new Date(), firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1), grid = $$("bin:grid"), form = $$("bin:form"), uploader = $$("bin:file")
        $$("fd").setValue(firstDay)
        $$("td").setValue(date)
        $$("type").getList().add(all, 0)
        $$("curr").getList().add(all, 0)
        $$("type").setValue("01GTKT")
        $$("curr").setValue("*")

        grid.attachEvent("onBeforeLoad", function () {
            this.showOverlay(_("loading"))
        })

        grid.attachEvent("onAfterLoad", function () {
            let self = this, count = self.count()
            $$("countinv").setValue(`${_("found")} ${count}`)
            if (count) self.hideOverlay()
            else self.showOverlay(_("notfound"))
        })

        grid.attachEvent("onAfterSelect", () => {
            row = grid.getSelectedItem()
            if(ROLE.includes('PERM_SEARCH_BINV_COPY')) $$("bin:btncopy").enable()
        })

        grid.attachEvent("onAfterUnSelect", () => {
            row = null
            $$("bin:btncopy").disable()
        })


        $$("bin:btncopy").attachEvent("onItemClick", () => {
            webix.confirm(`${_("copy_confirm")} ${row.id} ?`, "confirm-warning").then(() => {
                webix.storage.session.put("bin:form", form.getValues())
                this.show(`inv.bnv?id=${row.id}&action=copy`)
            })
        })

        $$("bin:btnsearch").attachEvent("onItemClick", () => {
            if (form.validate()) {
                grid.clearAll()
                grid.loadNext(size, 0, null, "bin->api/bin/get", true)
            }
        })
        $$("bin:btnread").attachEvent("onItemClick", () => {
            if (!uploader.files.getFirstId()) {
                webix.message(_("file_required"), "error")
                return
            }
            uploader.send()
        })

    }
}