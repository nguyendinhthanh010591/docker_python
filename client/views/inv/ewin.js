import { JetView } from "webix-jet"
export default class Ewin extends JetView {
    config() {
        return {
            view: "window",
            id: "ewin:win",
            modal: true,
            fullscreen: true,
            head: {
                view: "toolbar"
                , height: 35
                , cols: [
                    { view: "label", label: _("send_email") },
                    { view: "button", type: "icon", icon: "wxi-close", hotkey: "esc", width: 25, click: () => { $$("ewin:win").hide() } }
                ]
            },
            body: {
                view: "form",
                id: "ewin:form",
                elements: [
                    { rows: [{ id: "ewin:to", view: "multitext", label: "Email", name: "to", attributes: { maxlength: 255 }, required: true }] },
                    { id: "ewin:subject", view: "text", label: _("mail_subject"), name: "subject", attributes: { maxlength: 255 }, required: true },
                    { view: "fieldset", label: "", body: { id: "ewin:content", view: "template", scroll: "y", borderless: true } },
                    { cols: [{}, { id: "ewin:btnsend", view: "button", type: "icon", icon: "mdi mdi-email-newsletter", label: _("send_email"), width: 100 }] }
                ],
                rules: {
                    $all: webix.rules.isNotEmpty,
                    $obj: data => {
                        if (data.to) {
                            const rows = data.to.split(/[ ,;]+/)
                            for (const row of rows) {
                                if (!webix.rules.isEmail(row.trim())) {
                                    webix.message(_("mail_invalid"))
                                    $$("ewin:to").focus()
                                    return false
                                }
                            }
                        }
                        return true
                    }
                }
            }
        }
    }
    show() {
        const win = this.getRoot()
        win.clear()
        win.show()
    }
}
