import { JetView } from "webix-jet"
import { setConfStaFld } from "models/util"
export default class Mwin extends JetView {
    config() {
        _ = this.app.getService("locale")._
        return {
            view: "window",
            id: "sms:win",
            modal: true,
            fullscreen: true,
            head: {
                view: "toolbar"
                , height: 35
                , cols: [
                    { view: "label", label: _("sms_send") },
                    { view: "button", type: "icon", icon: "wxi-close", hotkey: "esc", width: 25, click: () => { $$("sms:win").hide() } }
                ]
            },
            body: {
                view: "form",
                id: "sms:form",
                //borderless: true,
                elements: [
                    { rows: [{ id: "sms:to", name: "sms", view: "multitext", label: "Số ĐT", name: "btel", attributes: { maxlength: 255 }, required: true }] },
                    { cols: [{}, { id: "sms:btnsend", view: "button", type: "icon", icon: "mdi mdi-email-plus", label: _("send"), width: 100 }] }
                ],
                rules: {
                    $all: webix.rules.isNotEmpty,
                }
            }
        }
    }
    ready(v, urls) {
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
    show() {
        const win = this.getRoot()
        win.clear()
        win.show()
    }
}
