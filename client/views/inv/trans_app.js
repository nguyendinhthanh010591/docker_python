import { JetView } from "webix-jet"
import {w, h, LinkedInputs2, size, filter, d2s, LGD, gridnf0, TRSTATUS, PAYMETHOD, ITYPE, ROLE, CARR, ENT, RPP, VAT, setConfStaFld, OU } from "models/util"


const LG = [
    { id: "*", value: _("all") },
    { id: "0", value: _("month") },
    { id: "1", value: _("day")},
    { id: "2", value: _("retail")}
   
]
const LKH = [
    { id: "1", value: _("Special") },
    { id: "0", value: _("Normal")}
   
   
]
const configs = (type) => {
    let lang = webix.storage.local.get("lang")
  
    webix.ajax().post(`api/cus`,{itype:type,lang:lang}).then(result => {
        let cols = result.json(), len = cols.length, grid = $$("trans:grid")
        let arr = grid.config.columns.filter(item => !CARR.includes(item.id))
        //Delete column rel before add it to last arr
        arr.forEach((v,i)=>{if(v.id == "rel") arr.splice(i,1) })
        cxls = []
        if (len > 0) {
            for (const col of cols) {
                // arr.push({ id: col.id, header: { text: col.label, css: "header" }, adjust: true })
                // cxls.push({ id: col.id, header: col.label })
                let obj = { id: col.id, header: col.label }
                cxls.push(obj)
                if (col.view == "datepicker") obj.format = webix.i18n.dateFormatStr
                if (col.suggest) obj.collection = col.suggest.data
                obj.header = { text: col.label, css: "header" }
                obj.adjust = true
                arr.push(obj)
            }
        }
        arr.push({ id: "rel", header: "", width: 35, adjust: true, template: (obj, common) => obj.adjtyp ? `<span class='webix_icon wxi-drag', title='${_("find_similar")}'></span>` : "" })
        grid.config.columns = arr
        grid.refreshColumns()
        let r4 = $$("invs:r4")
        r4.removeView("invs:r5")
        r4.removeView("invs:r6")
        if (len > 0) {
            if (len <= 5) {
                while (len < 5) {
                    cols.push({})
                    len++
                }
                r4.addView({ id: "invs:r5", cols: cols }, 0)
                r4.addView({ id: "invs:r6", hidden: true }, 1)
            }
            else {
                while (len < 10) {
                    cols.push({})
                    len++
                }
                r4.addView({ id: "invs:r5", cols: cols.splice(0, len / 2) }, 0)
                r4.addView({ id: "invs:r6", cols: cols }, 1)
            }
        }
        else {
            r4.addView({ id: "invs:r5", hidden: true }, 0)
            r4.addView({ id: "invs:r6", hidden: true }, 1)
        }



    })

}
 


export default class InvsView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        webix.protoUI({ name: "dependent", $cssName: "richselect" }, LinkedInputs2, webix.ui.richselect)
        webix.proxy.trans = {
            $proxy: true,
            load: function (view, params) {
                let obj = $$("trans:form").getValues()
                Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                if (!params) params = {}
                params.filter = obj
                return webix.ajax(this.source, params)
            }
        }
        const viewhd = (id) => {
            const ids = String(id).split(",")
        
            if(ids.length>1){
                viewrels(id)
            }else{
               
                webix.ajax(`api/sea/htm/${id}`).then(result => {
                   // const json = result.json(), status = json.status
                   
                    //const req = createRequest(json)
                    //jsreport.renderAsync(req).then(res => {
                    //    b64 = res.toDataURI()
                    //    $$("iwin:ipdf").define("src", b64)
                    let pdf = result.json()
                    b64 = pdf.pdf
                    const blob = dataURItoBlob(b64);

                    var temp_url = window.URL.createObjectURL(blob);
                    $$("iwin:ipdf").define("src", temp_url)
                    $$("iwin:win").show()
                    if (status == 3) {
                        //HDB sẽ ẩn ký pdf
                        if(ENT!="hdb") $$("iwin:btnsignpdf").show()
                        $$("iwin:btnxml").show()
                    }
                    else {
                        $$("iwin:btnsignpdf").hide()
                        $$("iwin:btnxml").hide()
                    }
                    if (status == 2) $$("iwin:btnapp").show()
                    else $$("iwin:btnapp").hide()
                    // webix.ajax().post("api/seek/report", req).then(res => {
                    //     let pdf = res.json()
                    //     b64 = pdf.pdf
                    //     const blob = dataURItoBlob(b64);

                    //     var temp_url = window.URL.createObjectURL(blob);
                    //     $$("iwin:ipdf").define("src", temp_url)
                    //     $$("iwin:win").show()
                    //     if (status == 3) {
                    //         //HDB sẽ ẩn ký pdf
                    //         if(ENT!="hdb") $$("iwin:btnsignpdf").show()
                    //         $$("iwin:btnxml").show()
                    //     }
                    //     else {
                    //         $$("iwin:btnsignpdf").hide()
                    //         $$("iwin:btnxml").hide()
                    //     }
                    //     if (status == 2) $$("iwin:btnapp").show()
                    //     else $$("iwin:btnapp").hide()
    
                    // })
                })
            }
           
           
        }
        const viewrel = (id) => {
            webix.ajax(`api/sea/rel/${id}`).then(result => {
                let json = result.json()
                if (json.length <= 0) this.webix.message(_("not_found_similar_invoice"))
                else {
                    $$("rwin:grid").clearAll()
                    $$("rwin:grid").parse(json)
                    $$("rwin:win").show()
                }
            })
        }
        const viewrels = (ids) => {
            webix.ajax(`api/sea/rels/${ids}`).then(result => {
                let json = result.json()
                if (json.length <= 0) this.webix.message(_("not_found_similar_invoice"))
                else {
                    $$("rwin:grid").clearAll()
                   
                    $$("rwin:grid").parse(json)
                    $$("rwin:win").show()
                    $$("rwin:winlb").define("label", _("invoice_similars"));
                    $$("rwin:winlb").refresh();
                }
            })
        }
        let pager = { view: "pager", width: 340, id: "trans:pager", size: size, template: `{common.first()}{common.prev()} {common.pages()}{common.next()}{common.last()}` }
        const cols = [
            { id: "id", header: "ID" },
            { id: "idt", header: _("invoice_date"), exportType: "string" },
            { id: "form", header: _("form") },
            { id: "serial", header: _("serial") },
            { id: "seq", header: _("seq"), exportType: "string" },
            { id: "status", header: _("status") },
            { id: "btax", header: _("taxcode"), exportType: "string" },
            { id: "bname", header: _("bname") },
            { id: "buyer", header: _("buyer") },
            { id: "bmail", header: _("bmail") },
            { id: "baddr", header: _("address") },
            { id: "sumv", header: _("sumv"), exportType: "number", exportFormat: gridnf0.format },
            { id: "vatv", header: "VAT", exportType: "number", exportFormat: gridnf0.format },
            { id: "totalv", header: _("totalv"), exportType: "number", exportFormat: gridnf0.format },
            { id: "curr", header: _("curr") },
            { id: "sec", header: _("search_code") },
            { id: "ou", header: _("ou") },
            { id: "uc", header: _("creator") },
            { id: "ic", header: "IC", exportType: "string" },
            { id: "note", header: _("note") }
        ]
        let gridcol = [
           
           
            { id: "chk", header: { content: "masterCheckbox", css: "center" }, checkValue:'on', uncheckValue:'off', adjust: "header", css: "center", template: "{common.checkbox()}" },
         
            { id: "status", header: { text: _("status"), css: "header" }, sort: "server", collection: TRSTATUS(this.app), adjust: true },
            { id: "tranID", header: { text: "ID", css: "header" }, css: "right", sort: "server", adjust: true },
           
            { id: "valueDate", header: { text: _("invoice_date"), css: "header" }, sort: "server", format: d2s, adjust: true },
            { id: "refNo", header: { text: _("tran_num"), css: "header" }, sort: "server", adjust: true },
          
            
            { id: "customerID", header: { text: _("customer_code"), css: "header" }, sort: "server", adjust: true },
            { id: "customerName", header: { text: _("customer_name"), css: "header" }, sort: "server", adjust: true },
            { id: "taxCode", header: { text: _("mst"), css: "header" }, sort: "server", adjust: true },
        
            { id: "customerAddr", header: { text: _("address"), css: "header" }, sort: "server", adjust: true },
            { id: "isSpecial", header: { text: _("btype"), css: "header" }, sort: "server", collection: LKH, adjust: true },
            { id: "curr", header: { text: _("currency"), css: "header" }, sort: "server", adjust: true },
            { id: "exrt", header: { text: _("exchange_rate"), css: "header" }, sort: "server", adjust: true },
            { id: "vrt", header: { text: _("vrt"), css: "header" }, adjust: true, collection: VAT(this.app), editor: "combo" },
            { id: "chargeAmount", header: { text: _("chargeAmount"), css: "header" }, sort: "server", css: "right", adjust: true },
           
            
            { id: "vcontent", header: { text: _("content"), css: "header" }, sort: "server", adjust: true },
            { id: "price", header: { text: _("price"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat },
            { id: "quantity", header: { text: _("quantity"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat },
            { id: "amount", header: { text: _("amount_tr"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat },
            { id: "vat", header: { text: _("vato"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.format },
            { id: "total", header: { text: _("totalo"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.format },
          
            { id: "create_date", header: { text: _("datesyn"), css: "header" }, sort: "server", adjust: true },
            { id: "last_update", header: { text: _("lastupdate"), css: "header" }, sort: "server", adjust: true },
            { id: "ma_nv", header: { text: _("ma_nv"), css: "header" }, sort: "server", adjust: true },
            { id: "ma_ks", header: { text: _("ma_ks"), css: "header" }, sort: "server", adjust: true },
            { id: "deptid", header: {text: _("Department"),css:"header"}, sort: "server", adjust: true}
            
        ]

       


        let grid = {
            view: "datatable",
            id: "trans:grid",
            select: "row",
            multiselect: false,
            resizeColumn: true,
            pager: "trans:pager",
            onContext: {
                webix_view: function (e, id) {
                    if (id && id.row) this.select(id)
                }
            },
            columns: gridcol,
            onClick:
            {
                
                "wxi-pencil": function (e, id) {
                  //  webix.storage.session.put("trans:form", $$("trans:form").getValues())
                    let row =this.getItem(id)
                  
                    $$("trans:formedit").setValues(row)
                    $$("igns:winedit").show()
                },
                "wxi-search": function (e, r) { viewhd(r.row) },
                "wxi-drag": function (e, r) { viewrel(r.row) },
                "mdi-file-replace-outline": function (e, r) { viewhd(this.getItem(r).pid) },
                "mdi-file-replace": function (e, r) { viewhd(this.getItem(r).cid) },

            }
        }

       // const url_ou = ENT == "hdb" ? "api/cat/bytoken" : "api/cat/nokache/ou"
       const url_ou = "api/ous/bytokenou"  // tât ca deu load theo chi nhanh

       
       const q = _("quarter"), m = _("month")

       const PERIODS = [
           { id: "d", value: _("home_period7") },
           { id: "1", value: `${m} 01` },
           { id: "2", value: `${m} 02` },
           { id: "3", value: `${m} 03` },
           { id: "4", value: `${m} 04` },
           { id: "5", value: `${m} 05` },
           { id: "6", value: `${m} 06` },
           { id: "7", value: `${m} 07` },
           { id: "8", value: `${m} 08` },
           { id: "9", value: `${m} 09` },
           { id: "10", value: `${m} 10` },
           { id: "11", value: `${m} 11` },
           { id: "12", value: `${m} 12` },
           { id: "q1", value: `${q} 1` },
           { id: "q2", value: `${q} 2` },
           { id: "q3", value: `${q} 3` },
           { id: "q4", value: `${q} 4` },
           { id: "y", value: _("year") },
       ]

        let eForm =
            [
                // {
                //     cols: [
                //         { id: "fd", name: "fd", label: ENT == "aia" ? _("date") : _("fd"), view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr, required:true },
                //         { id: "td", name: "td", label: _("td"), view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr, required:true },
                //         {id:"fix-width"}
                //     ]
                // },
                { //R1
                    cols: [
                        { id: "invs:period", label: _("home_period"), value: "d", view: "combo", options: PERIODS },
                        { id: "invs:year", label: _("year"), view: "combo", options: [] },

                        { id: "invs:fd", name: "fd", label: _("fd"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr, required: true },
                        { id: "invs:td", name: "td", label: _("td"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr, required: true },
                       
                    ]
                },
                {//R2
                    cols: [
                      ENT=='mzh'?  { id: "invs:trantype", name: "trantype", label: _("tran_type"), view: "combo",suggest: { data:  LGD(this.app), filter: filter }, value: "0" }: { id: "invs:trantype", name: "trantype", label: _("tran_type"), view: "combo",suggest: { url: 'api/cat/nokache/SYSTEM_SCB' }, value: "0" },
                       // { id: "invs:trancol", name: "trancol", label: _("group_method"), view: "combo", options: LG, required: true ,value: "*"},
                        { id: "invs:status", name: "status", label: _("status"), view: "multiselect", options: TRSTATUS(this.app),value: "2" },

                       // { id: "invs:taxCode", name: "customerAcc", label: _("customer_code"), view: "text" },//richselect
                       { id: "invs:refNo", name: "refNo", label: _("tran_num"), view: "text",attributes: { maxlength: 50 } },
                       { id: "invs:customerID", name: "customerID", label: _("customer_code"), view: "text" },
                       
                    ]
                },
                 {//R2
                    cols: [
                        { id: "invs:dept", name: "dept_id", label: _("Department"), view: "combo", options: "api/inv/dep" },
                       // { id: "invs:trancol", name: "trancol", label: _("group_method"), view: "combo", options: LG, required: true ,value: "*"},
                        { },

                       // { id: "invs:taxCode", name: "customerAcc", label: _("customer_code"), view: "text" },//richselect
                       { },
                       { },
                       
                    ]
                }
               
                ,
                {//R2
                    cols: [
                        {  },
                       
                        { },
                        { },
                        {
                            cols: [
                               
                                { view: "button", id: "invs:btnsearch", type: "icon", icon: "wxi-search", label: _("search"), width: 85 },
                                { view: "button", id: "appr:btnapp", type: "icon", icon: "wxi-check", label: _("approve"), disabled: ROLE.includes('PERM_TRANS_APPR_ACCEPT')  ? false : true, width: 85 },
                                { view: "button", id: "invs:btnReject", type: "icon", icon: "mdi mdi-keyboard-return", label: _("btn_reject"), disabled: ROLE.includes('PERM_TRANS_APPR_REJECT')  ? false : true, width: 85 }
                               
                            ]
                        }
                       
                    ]
                }
            ]
        let form = {
            view: "form",
            id: "trans:form",
            padding: 3,
            margin: 3,
            elementsConfig: { labelWidth: 85 },
            elements: eForm
            , rules: {
                fd: webix.rules.isNotEmpty,
                td: webix.rules.isNotEmpty,
             
              
              
                $obj: function (data) {
                    const fd = data.fd, td = data.td
                    if (fd > td) {
                        webix.message(_("date_invalid_msg"))
                        return false
                    }
                    const dtime = (new Date(td)).getTime() - (new Date(fd)).getTime()
                    if (dtime > 31622400000) {
                        webix.message(_("dt_search_between_year"))
                        return false
                    }
                    return true
                }
            }
        }

        let recpager = { view: "combo", id: "recpager", maxWidth: 70, tooltip: _("row_per_page"), value: 10, options: RPP }
        return {
            paddingX: 2,
            rows: [form, grid, {
                cols: [recpager, pager, { id: "invs:countinv", view: "label" },
                    {
                        // cols: [
                        //     { view: "button", id: "invs:btndnew", type: "icon", icon: "mdi mdi-newspaper", label: _("create_inv"),  width: 100 },
                        //     { view: "button", id: "invs:btnrep", type: "icon", icon: "mdi mdi-find-replace", label: _("create_inv_rep"), width: 120 }
                       
                        // ]
                    }]
            }]
        }
    }
    ready(v, urls) {
        let state = webix.storage.session.get("trans:form")
        if (state) {
            let grid = $$("trans:grid"), form = $$("trans:form")
            form.setValues(state)
            grid.clearAll()
            grid.loadNext(size, 0, null, "invs->api/sea", true)
            webix.storage.session.remove("trans:form")
        }
     
    }
    init() {
        if (!ROLE.includes('PERM_TRANS_APPR')) {
            this.show("/top/home")
            window.location.replace(`/${homepage}`)
            return
          }
        webix.extend($$("trans:form"), webix.ProgressBar)
                 
        
        
        const all = { id: "*", value: _("all") }
        let date = new Date(), firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1),lastday=webix.Date.add(new Date(date.getFullYear(), date.getMonth(), 1),-1,"day"), period = $$("invs:period"), year = $$("invs:year"), grid = $$("trans:grid"), form = $$("trans:form"), row, id, recpager = $$("recpager")
        let yr = date.getFullYear()
        let years = [], i = 2018
        while (i <= yr) {
            years.push({ id: i, value: i })
            i++
        }
        let fd = $$("invs:fd"), td = $$("invs:td")
        let list = year.getPopup().getList()
        list.clearAll()
        list.parse(years)
        year.setValue(yr)
        fd.setValue(firstDay)
        td.setValue(lastday)
        // if (ENT == "vcm" ){
        //     $$("ou").getList().add(all, 0)
        //     $$("ou").setValue("*")
        // }
        $$("invs:trantype").getList().add(all, 0)
        $$("invs:trantype").setValue("*")
       
        const cal = (v, yr) => {
            let b = (v != "d"), tn, dn, mm
            if (b) {
                let v1 = v.substr(0, 1)
                if (v1 === "y") {
                    tn = new Date(yr, 0, 1)
                    dn = new Date(yr, 11, 31)
                }
                else if (v1 === "q") {
                    let v2 = parseInt(v.substr(1))
                    mm = 3 * (v2 - 1)
                    tn = new Date(yr, mm, 1)
                    dn = new Date(yr, mm + 3, 1)
                    dn.setDate(dn.getDate() - 1)
                }
                else {
                    mm = parseInt(v)
                    tn = new Date(yr, mm - 1, 1)
                    dn = new Date(yr, mm, 1)
                    dn.setDate(dn.getDate() - 1)
                }

            }
            else {
                mm = date.getMonth()
                tn = new Date(yr, mm - 1, 1)
                dn = new Date(yr, mm, date.getDate())
            }
            fd.setValue(tn)
            td.setValue(dn)
            fd.config.readonly = b
            td.config.readonly = b
            fd.refresh()
            td.refresh()
        }

        year.attachEvent("onChange", (v) => { cal(period.getValue(), v) })
        period.attachEvent("onChange", (v) => { cal(v, year.getValue()) })

      
         $$("invs:status").setValue(2)
        
      
       
        const search = () => {
            grid.clearAll()
            grid.loadNext(recpager.getValue(), 0, null, "trans->api/tran", true)
        }
      

        $$("invs:btnsearch").attachEvent("onItemClick", () => {
            if (form.validate()) search()
        })
       
        $$("appr:btnapp").attachEvent("onItemClick", () => {
         
        
                let rows = grid.serialize(), arr = []
              
                for (let i=0;i<rows.length;i++) {
                   if  (rows[i] && rows[i].chk  && rows[i].status != 2 )  {
                        webix.message('Chỉ được phép duyệt giao dịch ở trạng thái chờ duyệt', "error")
                        return false
                   } 
                    if (rows[i] && rows[i].chk && (rows[i].status == 2)) {
                        arr.push(rows[i])
                    }

                }
                if (arr.length < 1) {
                    webix.message(_("invoice_must_select", "error"))
                    return false
                }
                $$("appr:btnapp").disable()
               
                webix.delay(() => {
                    webix.ajax().post("api/tran/app", { invs: arr}).then(result => {
                        const json = result.json()
                        if (ROLE.includes('PERM_TRANS_APPR_ACCEPT')) $$("appr:btnapp").enable()
                        console.log(json.status)
                        if (json.status == 1) {
                            grid.clearAll()
                            search()
                            webix.message(_("tran_management_app_ok"))
                           
                        }
                    }).catch(() => { 
                        if (ROLE.includes('PERM_TRANS_APPR_ACCEPT')) $$("appr:btnapp").enable()
                    })
                })
            
        })
         $$("invs:btnReject").attachEvent("onItemClick", () => {
         
        
                let rows = grid.serialize(), arr = []
              
                for (let i=0;i<rows.length;i++) {
                   if  (rows[i] && rows[i].chk  && rows[i].status != 2 )  {
                        webix.message(_("alert_tran_app_reject"), "error")
                        return false
                   } 
                    if (rows[i] && rows[i].chk && (rows[i].status == 2)) {
                        arr.push(rows[i])
                    }

                }
                if (arr.length < 1) {
                    webix.message(_("itran_must_select", "error"))
                    return false
                }
                $$("invs:btnReject").disable()
               
                webix.delay(() => {
                    webix.ajax().post("api/tran/rej", { invs: arr}).then(result => {
                        const json = result.json()
                        if(ROLE.includes('PERM_TRANS_APPR_REJECT')) $$("invs:btnReject").enable()
                        console.log(json.status)
                        if (json.status == 1) {
                            grid.clearAll()
                            search()
                            webix.message(_("tran_management_reject_ok"))
                           
                        }
                    }).catch(() => { 
                        if(ROLE.includes('PERM_TRANS_APPR_REJECT')) $$("invs:btnReject").enable()
                    })
                })
            
        })
        
        grid.attachEvent("onBeforeLoad", function () {
            this.showOverlay(_("loading"))
        })

        grid.attachEvent("onAfterLoad", function () {
            let self = this, count = self.count()
            $$("invs:countinv").setValue(`${_("found")} ${count}`)
            if (count) self.hideOverlay()
            else self.showOverlay(_("notfound"))
        })

        grid.attachEvent("onAfterSelect", () => {
            row = grid.getSelectedItem()
            id = row.id
            $$("invs:btncopy").enable()
            const status = row.status
           
            $$("invs:file").enable()
            
            
        })
        grid.attachEvent("onAfterUnSelect", () => {
            row = null
            id = null
            // $$("invs:btncopy").disable()
            // $$("invs:btnrep").disable()
            // if (!["hlv"].includes(ENT)) $$("invs:btncancel").disable()
            // $$("invs:btntrash").disable()
            // $$("invs:btnreject").disable()
            // $$("invs:btnmail").disable()
            // $$("invs:btnadj").disable()
            // $$("invs:btnconvert").disable()
            // $$("invs:file").disable()
        })

        recpager.attachEvent("onChange", () => {
            $$("trans:pager").config.size = recpager.getValue()
            if (form.validate()) search()
        })

      
       
    }
}