import { JetView } from "webix-jet"
import {w, h, LinkedInputs2, size, filter, d2s,LGD,LSD, gridnf0, TRSTATUS,ERRTATUS, ENT,RPP2, VAT,setConfStaFld,C0,ROLE } from "models/util"
import Mwin from "views/inv/mwin"
import Iwin from "views/inv/iwin"
import Rwin from "views/inv/rwin"
import IwinNew from "views/inv/IwinNew2"
import IwinRep from "views/inv/IwinRep"
import IorgsView from "views/inv/iorgs"
let b64, cxls

const LG = [
    { id: "*", value: _("all") },
    { id: "0", value: _("month") },
    { id: "1", value: _("day")},
    { id: "2", value: _("retail")}
   
]
const LKH = [
    { id: "1", value: _("Special") },
    { id: "0", value: _("Normal")}
]


   
class IorgsWin extends JetView {
    config() {
        return {
            view: "window",
            id: "iorgs:win",
            height: h,
            width: w,
            position: "center",
            resize: true,
            modal: true,
            head: {
                view: "toolbar", height: 40, css: 'toolbar_window',
                cols: [
                    { view: "label", label: _("customers_list") },
                    { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => { $$("iorgs:win").hide() } }
                ]
            },
            body: IorgsView
        }
    }
    show() {
        this.getRoot().show()
    }
}

 
class IgnsWin extends JetView {
    
    config() {
        
        let eForm1 =
        [
            { //R1
                cols: [
                    { id: "invs:fdp", name: "fdp", label: _("fd"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr, required: true },
                    { id: "invs:tdp", name: "tdp", label: _("td"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr, required: true },
                   
                  
                ]
            }
            ,
            {//R2
                cols: [
                   // { id: "invs:trantypep", name: "trantype", label: _("tran_type"), view: "combo", options: LGD(this.app),value:"0",required: true },
                    { id: "invs:trantypep", name: "trantype", label: _("tran_type"), view: "combo", suggest: { url: 'api/cat/nokache/SYSTEM_SCB'} },
                    { id:"invs:statusp",name:"status",label: _("status_deal"),view:"multiselect",options:LSD(this.app),required: true,hidden:true},
                    {id:"Tempcolumn"}
                ]
            }
            ,
                {//R2
                cols: [
                    { view: "button", id: "invs:btnDltp", type: "icon", icon: "wxi-trash", label: _("delete"),hidden:true},
                    { view: "button", id: "btnReportp", type: "icon", icon: "mdi mdi-file-outline", label: _("report"),hidden:true},
                    { view: "button", id: "invs:btnSynp", type: "icon", icon: "mdi mdi-sync", label: _("syn"), hidden:true },
                    { view: "button", id: "invs:btnexit", type: "icon", icon: "mdi mdi-exit-to-app", label: _("exit"),  click: () => { this.getRoot().hide() } }
                ]
            }
            
        ]
        return {
            view: "window",
            id: "igns:win",
            height: h,
            width: w,
            position: "center",
            resize: true,
            modal: true,
            head: {
                view: "toolbar", height: 40,
                css: 'toolbar_window',
                cols: [
                    { view: "label", id:"win_lbl",label: _("dlt_tran") },
                    { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => { this.getRoot().hide() } }
                ]
            }
            ,
            body: {
                view: "form",
                id: "trans:form2",
                padding: 3,
                margin: 3,
                elementsConfig: { labelWidth: 85 },
                elements: eForm1
                , rules: {
                    fdp: webix.rules.isNotEmpty,
                    tdp: webix.rules.isNotEmpty,
                    trantype: webix.rules.isNotEmpty,
                    
                    $obj: function (data) {
                        const fd = data.fdp, td = data.tdp
                        if (fd > td) {
                            webix.message(_("date_invalid_msg"))
                            return false
                        }
                        const dtime = (new Date(td)).getTime() - (new Date(fd)).getTime()
                        if (dtime > 31622400000) {
                            webix.message(_("dt_search_between_year"))
                            return false
                        }
                        return true
                    }
                }
            }
        }
    }
    show() {
        this.getRoot().show()
    }
}
class IgnsWinEdit extends JetView {
    
    config() {
        
        let eFormedit =
        [
            { //R1
                id: "itran:r1", visibleBatch: "hd",
                cols: [
                    { id: "eidt:tranID", name: "tranID", label: _("ID"),  view: "text", required: true,disabled :true},
                    {},
                   
                  
                ]
            },
            { //R1
                id: "itran:r2", visibleBatch: "hd",
                cols: [
                    { id: "eidt:valuedate", name: "valueDate", label: _("tran_date"),  view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr, required: true },
                    { id: "eidt:refNo", name: "refNo", label: _("tran_num"),  view: "text",attributes: { maxlength: 50 }, required: true },
                   
                  
                ]
            },
           
            { //R1
                id: "itran:r3", visibleBatch: "hd",
				cols: [
                    { id: "bcode", name: "customerID", label: _("customer_code"), view: "text",attributes: { maxlength: 50 },  required: true},{},
                    { id: "bmail", name: "customerMail", label: _("customer_code"), view: "text",attributes: { maxlength: 50 },  hidden: true},
                    { id: "btel", name: "customerTel", label: _("customer_code"), view: "text",attributes: { maxlength: 50 },  hidden: true},
                    { id: "bacc", name: "customerAcc", label: _("customer_code"), view: "text",attributes: { maxlength: 50 },  hidden: true},
                    { id: "bbank", name: "customerBank", label: _("customer_code"), view: "text",attributes: { maxlength: 50 },  hidden: true},
				  
                ]
            },
            { //R1
                id: "itran:r4", visibleBatch: "hd",
                cols: [
                    {
                        batch: "hd",
                        cols:[
                            { id: "bname", name: "customerName", label: _("customer_name"), view: "text", attributes: { maxlength: 500 }, required: true,suggest: {
                                //view: "suggest",
                                view: "gridsuggest",
                                // fitMaster: true,
                                resize: true,
                                body: {
                                    url: "api/org/fbn",
                                    dataFeed: function (str) {
                                        if (/\;$/.test(str)) return this.load(`${this.config.url}?filter[value]=${encodeURIComponent(str.substring(0, str.length - 1))}`, this.config.datatype)
                                        else return
                                    },
                                    scroll: true,
                                    autoheight: false,
                                    columns: [
                                        { id: "taxc", header: { text: _("taxcode"), css: "header" }, adjust: true },
                                        { id: "name", header: { text: _("customer_name"), css: "header" }, adjust: true },
                                        { id: "fadd", header: { text: _("address"), css: "header" }, adjust: true }
                                    ]
                                },
                                on: {
                                    onValueSuggest: (rec) => {
                                        if (rec) {
                                            $$("bname").setValue(rec.name)
                                            $$("btax").setValue(rec.taxc)
                                            $$("baddr").setValue(rec.fadd)
                                            $$("bcode").setValue(rec.code)
                                        }
                                    }
                                }
                            }, placeholder: _("enter2search_msg"), required: true
                        },
                        { id: "inv:btnorgs", view: "button", type: "icon", icon: "wxi-search", tooltip: _("customers_search"), width: 33 }
                        ]
                    },{ id: "btax", name: "taxCode", label: _("mst"), view: "text" ,attributes: { maxlength: 50 }},
				   
				  
                ]
            },
            { //R1
                id: "itran:r5", visibleBatch: "hd",
                cols: [
                    { id: "baddr", name: "customerAddr", label: _("address"),  view: "text",attributes: { maxlength: 500 }, gravity:2}
                 
                ]
            },
           
            { //R1
                id: "itran:r6", visibleBatch: "hd",
                cols: [
                    { id: "eidt:exrt", name: "exrt", label: _("exchange_rate"), view: "text",readonly: true,disabled: true,inputAlign: "right", format:"1.111,00" },
                    { id: "eidt:curr", name: "curr", label: _("currency"),view: "combo", suggest: { url: "api/cat/kache/currency" },  required: true },
                   
                  
                ]
            },
            { //R1
                id: "itran:r7", visibleBatch: "hd",
                cols: [
                  //  {},
                   // { id: "eidt:exrt", name: "exrt", label: _("exchange_rate"), view: "text", stringResult: true, editable: true,  required: true },
                    { id: "eidt:vrt",name: "vrt", label: _("vrt"), options: VAT(this.app) , view: "combo" },
                    { id: "eidt:chargeAmount", name: "chargeAmount", label: _("chargeAmount"), view: "text", required: true,inputAlign: "right"/*, format:"1.111,00"*/    },
                   
                  
                ]
            },
            // { //R1
            //     cols: [
            //       //  {},
            //        // { id: "eidt:exrt", name: "exrt", label: _("exchange_rate"), view: "text", stringResult: true, editable: true,  required: true },
            //         { id: "eidt:price",name: "price", label: _("price"), view: "text" ,inputAlign: "right", format:"1.111,00" },
            //         { id: "eidt:quantity", name: "quantity", label: _("quantity"), view: "text" },
                   
                  
            //     ]
            // },
            { //R1
                id: "itran:r8", visibleBatch: "hd",
                cols: [
                  //  {},
                   // { id: "eidt:exrt", name: "exrt", label: _("exchange_rate"), view: "text", stringResult: true, editable: true,  required: true },
                    { id: "eidt:amount",name: "amount", label: _("amount"),  view: "text" , required: true,format:"1.111,00"},
                    { id: "eidt:vat", name: "vat", label: _("vat"), view: "text", required: true, inputAlign: "right", format:"1.111,00"  },
                   
                  
                ]
            },
           
            { //R1
                id: "itran:r9", visibleBatch: "hd",
                cols: [
                    { id: "eidt:total", name: "total", label: _("totalo"), view: "text", required: true, inputAlign: "right",format:"1.111,00" },
                    { id: "edit:vcontent",name: "vcontent",label: _("content"),view: "text" }
                   
                  
                ]
            },{
                id: "itran:r10", visibleBatch: "hd",
                cols: [
                    { id: "edit:mailGroup",name: "mailGroup",  label: _("mailGroup"), view: "text" },
                    { id: "edit:vcontent",name: "vcontent",  label: _("content"), view: "text" }
                   
                  
                ]
            },
          
                {//R2
                id: "itran:r11", visibleBatch: "hd",
                cols: [
                    { view: "button", id: "edit:btnUpdate", type: "icon", icon: "mdi mdi-database-edit", label: _("update")},
                 
                    { view: "button", id: "edit:btnexit", type: "icon", icon: "mdi mdi-exit-to-app", label: _("exit"),  click: () => { this.getRoot().hide() } }
                ]
            }
            
        ]
        return {
            view: "window",
            id: "igns:winedit",
            height: h,
            width: w,
            position: "center",
            resize: true,
            modal: true,
            head: {
                view: "toolbar", height: 40,
                css: 'toolbar_window',
                cols: [
                    { view: "label", id:"win_lbl_edit",label: _("edit_gd") },
                    { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => { this.getRoot().hide() } }
                ]
            }
            ,
            body: {
                view: "form",
                id: "trans:formedit",
                padding: 3,
                margin: 3,
                elementsConfig: { labelWidth: 85 },
                elements: eFormedit
               
            }
        }
    }
    show() {
        this.getRoot().show()
    }
}
export default class InvsView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        webix.protoUI({ name: "dependent", $cssName: "richselect" }, LinkedInputs2, webix.ui.richselect)
        webix.proxy.trans = {
            $proxy: true,
            load: function (view, params) {
                let obj = $$("trans:form").getValues()
                Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                if (!params) params = {}
                params.filter = obj
                return webix.ajax(this.source, params)
            }
        }
        const viewhd = (id) => {
            const ids = String(id).split(",")
        
            if(ids.length>1){
                viewrels(id)
            }else{
               
                webix.ajax(`api/sea/htm/${id}`).then(result => {
                   // const json = result.json(), status = json.status
                   
                    //const req = createRequest(json)
                    //jsreport.renderAsync(req).then(res => {
                    //    b64 = res.toDataURI()
                    let pdf = result.json()
                        b64 = pdf.pdf
                        const blob = dataURItoBlob(b64);

                        var temp_url = window.URL.createObjectURL(blob);
                        $$("iwin:ipdf").define("src", temp_url)
                        $$("iwin:win").show()
                        if (status == 3) {
                            //HDB sẽ ẩn ký pdf
                            if(ENT!="hdb") $$("iwin:btnsignpdf").show()
                            $$("iwin:btnxml").show()
                        }
                        else {
                            $$("iwin:btnsignpdf").hide()
                            $$("iwin:btnxml").hide()
                        }
                        if (status == 2) $$("iwin:btnapp").show()
                        else $$("iwin:btnapp").hide()
                    // webix.ajax().post("api/seek/report", req).then(res => {
                    //     let pdf = res.json()
                    //     b64 = pdf.pdf
                    //     const blob = dataURItoBlob(b64);

                    //     var temp_url = window.URL.createObjectURL(blob);
                    //     $$("iwin:ipdf").define("src", temp_url)
                    //     $$("iwin:win").show()
                    //     if (status == 3) {
                    //         //HDB sẽ ẩn ký pdf
                    //         if(ENT!="hdb") $$("iwin:btnsignpdf").show()
                    //         $$("iwin:btnxml").show()
                    //     }
                    //     else {
                    //         $$("iwin:btnsignpdf").hide()
                    //         $$("iwin:btnxml").hide()
                    //     }
                    //     if (status == 2) $$("iwin:btnapp").show()
                    //     else $$("iwin:btnapp").hide()
    
                    // })
                })
            }
           
           
        }
        const viewrel = (id) => {
            webix.ajax(`api/sea/rel/${id}`).then(result => {
                let json = result.json()
                if (json.length <= 0) this.webix.message(_("not_found_similar_invoice"))
                else {
                    $$("rwin:grid").clearAll()
                    $$("rwin:grid").parse(json)
                    $$("rwin:win").show()
                }
            })
        }
        const viewrels = (ids) => {
            webix.ajax(`api/sea/rels/${ids}`).then(result => {
                let json = result.json()
                if (json.length <= 0) this.webix.message(_("not_found_similar_invoice"))
                else {
                    $$("rwin:grid").clearAll()
                   
                    $$("rwin:grid").parse(json)
                    $$("rwin:win").show()
                    $$("rwin:winlb").define("label", _("invoice_similars"));
                    $$("rwin:winlb").refresh();
                }
            })
        }
        let pager = { view: "pager", width: 340, id: "trans:pager", size: 100, template: `{common.first()}{common.prev()} {common.pages()}{common.next()}{common.last()}` }
        const cols = [
            { id: "id", header: "ID" },
            { id: "idt", header: _("invoice_date"), exportType: "string" },
            { id: "form", header: _("form") },
            { id: "serial", header: _("serial") },
            { id: "seq", header: _("seq"), exportType: "string" },
            { id: "status", header: _("status") },
            { id: "btax", header: _("taxcode"), exportType: "string" },
            { id: "bname", header: _("bname") },
            { id: "buyer", header: _("buyer") },
            { id: "bmail", header: _("bmail") },
            { id: "baddr", header: _("address") },
            { id: "sumv", header: _("sumv"), exportType: "number", exportFormat: gridnf0.format },
            { id: "vatv", header: "VAT", exportType: "number", exportFormat: gridnf0.format },
            { id: "totalv", header: _("totalv"), exportType: "number", exportFormat: gridnf0.format },
            { id: "curr", header: _("curr") },
            { id: "sec", header: _("search_code") },
            { id: "ou", header: _("ou") },
            { id: "uc", header: _("creator") },
            { id: "ic", header: "IC", exportType: "string" },
            { id: "note", header: _("note") }
        ]
        let gridcol = [
           
            {
                id: "sel", header: "", width: 35, template: (obj, common) => {
                    if ((ENT=='scb' && obj.status != 1)) return common.editIcon(obj, common)
                    else return ""
                }
            },
            { id: "chk", header: { content: "masterCheckbox", css: "center" }, checkValue:'on', uncheckValue:'', adjust: "header", css: "center", template: "{common.checkbox()}" },
         
            { id: "status", header: { text: _("status"), css: "header" }, sort: "server", collection: TRSTATUS(this.app), adjust: true },
            { id: "tranID", header: { text: "ID", css: "header" }, css: "right", sort: "server", adjust: true },
            { id: "inv_id", header: { text: _("inv_id"), css: "header" }, sort: "server",  adjust: true },
            { id: "trantype", header: { text: _("tran_type"), css: "header" }, sort: "server",  adjust: true },
            { id: "valueDate", header: { text: _("tran_date"), css: "header" }, sort: "server", format: d2s, adjust: true },
            ENT == "scb" ? { id: "branchcode", header: { text: _("ou_id_scb"), css: "header" }, sort: "server", adjust: true } : {id: "branchcode", header: { text: _("ou_id_scb"), css: "header" }, sort: "server", adjust: true, hidden: true},
            { id: "ou", header: { text: _("ou_name"), css: "header" }, sort: "server", collection: "api/ous/bytokenou", adjust: true,width: 200 },
            { id: "refNo", header: { text: _("tran_num"), css: "header" }, sort: "server", adjust: true },
          
            
            { id: "customerID", header: { text: _("customer_code"), css: "header" }, sort: "server", adjust: true },
            { id: "customerName", header: { text: _("customer_name"), css: "header" }, sort: "server", adjust: true },
            { id: "taxCode", header: { text: _("mst"), css: "header" }, sort: "server", adjust: true },
        
            { id: "customerAddr", header: { text: _("address"), css: "header" }, sort: "server", adjust: true },
            
            { id: "segment", header: { text: "Segment", css: "header" }, sort: "server", adjust: true },
           // { id: "isSpecial", header: { text: _("btype"), css: "header" }, sort: "server", collection: LKH, adjust: true },
            { id: "curr", header: { text: _("currency"), css: "header" }, sort: "server", adjust: true },
            { id: "exrt", header: { text: _("exchange_rate"), css: "header" }, sort: "server", adjust: true },
            { id: "vrt", header: { text: _("vrt"), css: "header" }, adjust: true,sort: "server", collection: VAT(this.app), editor: "combo" },
            { id: "chargeAmount", header: { text: _("chargeAmount"), css: "header" }, sort: "server", css: "right", adjust: true },
           
            
            { id: "vcontent", header: { text: _("content"), css: "header" }, sort: "server", adjust: true },
            //{ id: "price", header: { text: _("price"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat  },
           // { id: "quantity", header: { text: _("quantity"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat  },
            { id: "amount", header: { text: _("amount"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat  },
            { id: "vat", header: { text: _("vato"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat },
            { id: "total", header: { text: _("totalo"), css: "header" }, sort: "server", css: "right", adjust: true,format: gridnf0.numberFormat },
            { id: "trancol", header: { text: _("group_method"), css: "header" }, sort: "server", adjust: true ,collection: LG},
            //{ id: "mailGroup", header: { text: _("mailGroup"), css: "header" }, sort: "server", adjust: true },
            { id: "inv_date", header: { text: _("invoice_date"), css: "header" }, sort: "server", adjust: true },
            { id: "note", header: { text: _("note"), css: "header" }, sort: "server", adjust: true },
            { id: "stt_error", header: { text: _("stt_error"), css: "header" }, sort: "server", adjust: true,collection: ERRTATUS(this.app) },
          
            { id: "create_date", header: { text: _("datesyn"), css: "header" }, sort: "server", adjust: true },
            { id: "last_update", header: { text: _("lastupdate"), css: "header" }, sort: "server", adjust: true },
            { id: "ma_nv", header: { text: _("tran_update_u"), css: "header" }, sort: "server", adjust: true },
             { id: "ma_ks", header: { text: _("ma_ks"), css: "header" }, sort: "server", adjust: true },
            { id: "deptid", header: {text: _("Department"),css:"header"}, sort: "server", adjust: true}
            
        ]


       


        let grid = {
            view: "datatable",
            id: "trans:grid",
            select: "row",
            multiselect: false,
            resizeColumn: true,
            pager: "trans:pager",
            onContext: {
                webix_view: function (e, id) {
                    if (id && id.row) this.select(id)
                }
            },
            columns: gridcol,
            onClick:
            {
                "wxi-download": function (e, id) {
                   
                    webix.message(`${_("export_report")}.......`,"info",-1,"excel_ms");
                  
                    $$("trans:form").disable();
                    $$("trans:form").showProgress({
                        type:"icon",
                        hide:false
                        });
                   
                    let lang = webix.storage.local.get("lang")
                    let obj = $$("trans:form").getValues()
                    let colhd = ""
                    for (const colh of cols) {
                        colhd += colh["header"] + ","
                    }
                    colhd = colhd.substring(0, colhd.length - 1)
                    Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                    //webix.ajax().response("blob").get("api/sea/xls", { filter: obj }).then(data => { webix.html.download(data, `${webix.uid()}.xlsx`) })
                    webix.ajax().response("blob").get("api/sea/xls", { filter: obj, lang: lang, colhd: colhd  }).then(data => {
                        console.log(data);
                         webix.html.download(data, `${webix.uid()}.xlsx`) 
                           $$("trans:form").enable()
                           $$("trans:form").hideProgress()
                           webix.message.hide("excel_ms");
                        }).catch(e => {
                          
                            $$("trans:form").enable();
                            $$("trans:form").hideProgress();
                            webix.message.hide("excel_ms");
                           
                        })
                  
                },
                "wxi-pencil": function (e, id) {
                  //  webix.storage.session.put("trans:form", $$("trans:form").getValues())
                    let row =this.getItem(id)
                    row.valueDate = new Date(row.valueDate)
                    $$("trans:formedit").setValues(row)
                  
                    $$("igns:winedit").show()
                },
                "wxi-search": function (e, r) { viewhd(r.row) },
                "wxi-drag": function (e, r) { viewrel(r.row) },
                "mdi-file-replace-outline": function (e, r) { viewhd(this.getItem(r).pid) },
                "mdi-file-replace": function (e, r) { viewhd(this.getItem(r).cid) },

            }
        }

       // const url_ou = ENT == "hdb" ? "api/cat/bytoken" : "api/cat/nokache/ou"
       const url_ou = "api/ous/bytokenou"  // tât ca deu load theo chi nhanh
        let eForm =
            [
                { //R1
                    cols: [
                        { id: "invs:fd", name: "fd", label: _("fd"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr, required: true },
                        { id: "invs:td", name: "td", label: _("td"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr, required: true },
                       
                      //  { id: "invs:trantype", name: "trantype", label: _("tran_type"), view: "combo",suggest: { data: LGD(this.app), filter: filter } },
                        { id: "invs:trantype", name: "trantype", label: _("tran_type"), view: "combo", suggest: { url: 'api/cat/nokache/SYSTEM_SCB'} },
                        { id: "invs:trancol", name: "trancol", label: _("group_method"), view: "combo", options: LG, required: true ,value: "*"},
                        { id: "invs:status", name: "status", label: _("status"), view: "multiselect", options: TRSTATUS(this.app) }
                    ]
                },
                {//R2
                    cols: [
                        { id: "invs:customerID", name: "customerID", label: _("customer_code"), view: "text" },//richselect
                        { id: "invs:ou", name: "ou", label: _("bname"), view: "combo", suggest: { url: "api/ous/bytokenou", filter: filter, relative: "bottom" } },
                     
                        { id: "invs:curr", name: "curr", label: _("currency"), view: "text", attributes: { maxlength: 50 } },
                        { id: "invs:segment", name: "segment", label: _("segment"),  view: "combo", options: "api/segment" },
                        { id: "invs:vat", name: "vat", label: _("vrt"), view: "multiselect", options: VAT(this.app) }
                    ]
                }
                ,
                {//R2
                    cols: [
                        { id: "invs:refNo", name: "refNo", label: _("tran_num"), view: "text",attributes: { maxlength: 50 } },//richselect
                       
                        { id: "invs:inv_id", name: "inv_id", label: _("inv_id"), view: "text", attributes: { maxlength: 50 } },
                        { id: "invs:date", name: "inv_date", label: _("inv_date"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr },
                        { id: "invs:content", name: "content", label: _("content"), view: "text", attributes: { maxlength: 50 }, gravity: 2  }
                    ]
                },
                {//R2
                    cols: [
                        { id: "invs:stt_error", name: "stt_error", label: _("stt_error"), view: "combo", options: ERRTATUS(this.app) },
                         { id: "invs:dept", name: "dept_id", label: _("Department"), view: "combo", options: "api/inv/dep"},
                         { },
                        { },
                        {
                           
                        }
                       
                    ]
                }
                ,
                {//R2
                    cols: [
                        {  },
                        {},
                        { },
                        { },
                        {
                            cols: [
							   
                                { view: "button", id: "invs:btnsearch", type: "icon", icon: "wxi-search", label: _("search"), width: 85 },
                                { view: "button", id: "invs:btnExport", type: "icon", icon: "mdi mdi-file-excel", label: _("export"), disabled: ROLE.includes('PERM_TRANS_EXP')  ? false : true, width: 85 },
                                { view: "button", id: "invs:btnDelInfor", type: "icon", icon: "mdi mdi-delete-forever", label: _("del_infor"),width: 100,  click: () => { window.location.reload(true) }},
                                { view: "button", id: "invs:btnReport", type: "icon", icon: "mdi mdi-file-outline", label: _("report"), width: 85,hidden:true },
                                { view: "button", id: "invs:btnDlt", type: "icon", icon: "mdi mdi-delete", label: _("delete"), width: 85,hidden:true},
                                { view: "button", id: "invs:btnSyn", type: "icon", icon: "mdi mdi-sync", label: _("syn"), width: 85,hidden:true },
                            ]
                        }
                       
                    ]
                }
            ]
        let form = {
            view: "form",
            id: "trans:form",
            padding: 3,
            margin: 3,
            elementsConfig: { labelWidth: 85 },
            elements: eForm
            , rules: {
                fd: webix.rules.isNotEmpty,
                td: webix.rules.isNotEmpty,
               
              
                $obj: function (data) {
                    const fd = data.fd, td = data.td
					  const fmonth = new Date(fd).getMonth(), tmonth = new Date(td).getMonth()																		
                    if (fd > td) {
                        webix.message(_("date_invalid_msg"))
                        return false
                    }
					if(fmonth != tmonth){
                        webix.message(_("fd_td_in_month"), "error")
                        return false
                    }		 
                    const dtime = (new Date(td)).getTime() - (new Date(fd)).getTime()
                    if (dtime > 31622400000) {
                        webix.message(_("dt_search_between_year"))
                        return false
                    }
                    return true
                }
            }
        }

        let recpager = { view: "combo", id: "recpager", maxWidth: 70, tooltip: _("row_per_page"), value: 100, options: RPP2 }
        return {
            paddingX: 2,
            rows: [form, grid, {
                cols: [recpager, pager, { id: "invs:countinv", view: "label" },
                    {
                        cols: [
                            { view: "button", id: "invs:active", type: "icon", icon: "wxi-check", label: _("active"), disabled: ROLE.includes('PERM_TRANS_TRANS_ACTIVE')  ? false : true,  width: 100 },
                            { view: "button", id: "invs:inactive", type: "icon", icon: "mdi mdi-delete", label: _("inactive"),disabled: ROLE.includes('PERM_TRANS_TRANS_INACTIVE')  ? false : true,  width: 100 },
                            { view: "button", id: "invs:btnsinge", type: "icon", icon: "mdi mdi-newspaper", label: _("btnsinge"),disabled: ROLE.includes('PERM_TRANS_SINGLE_INV')  ? false : true,  width: 100 },
                            { view: "button", id: "invs:btndnew", type: "icon", icon: "mdi mdi-newspaper", label: _("create_inv"),disabled: ROLE.includes('PERM_TRANS_NEW_INV')  ? false : true,  width: 100 },
                            { view: "button", id: "invs:btndnew_a", type: "icon", icon: "mdi mdi-check-all", label: _("create_inv_a"),disabled: ROLE.includes('PERM_TRANS_TRANS_INVS')  ? false : true,  width: 100 },
                            { view: "button", id: "invs:btnrep", type: "icon", icon: "mdi mdi-find-replace", label: _("create_inv_rep"), width: 120 ,hidden:true}
                       
                        ]
                    }]
            }]
        }
    }
    ready(v, urls) {
        let state = webix.storage.session.get("trans:form")
        if (state) {
            let grid = $$("trans:grid"), form = $$("trans:form")
            form.setValues(state)
            grid.clearAll()
            grid.loadNext(size, 0, null, "invs->api/sea", true)
            webix.storage.session.remove("trans:form")
        }
       
       
       
       
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //

    }
    init() {
        if (!ROLE.includes('PERM_TRANS')) {
            this.show("/top/home")
            window.location.replace(`/${homepage}`)
            return
          }
        webix.extend($$("trans:form"), webix.ProgressBar)
            
        this.Iwin = this.ui(Iwin)
        this.Mwin = this.ui(Mwin)
        this.Rwin = this.ui(Rwin)
        this.IgnsWin = this.ui(IgnsWin)
        this.IgnsWinEdit = this.ui(IgnsWinEdit)
        this.IorgsWin = this.ui(IorgsWin)
        this.IwinNew = this.ui(IwinNew)
        this.IwinRep = this.ui(IwinRep)
        const all = { id: "*", value: _("all") }
        let date = new Date(), firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1),lastday=webix.Date.add(new Date(date.getFullYear(), date.getMonth(), 1),-1,"day"), grid = $$("trans:grid"), form = $$("trans:form"), row, recpager = $$("recpager"),formdl=$$("trans:form2"),formEdit=$$("trans:formedit")
        webix.extend($$("igns:win"), webix.ProgressBar)
        webix.extend($$("tranN:win"), webix.ProgressBar)  
        webix.extend($$("rep:win"), webix.ProgressBar)  
        
        $$("invs:fdp").setValue(firstDay)
        $$("invs:tdp").setValue(lastday)
        $$("invs:fd").setValue(firstDay)
        $$("invs:td").setValue(lastday)
        $$("invs:ou").getList().add(all, 0)
        $$("invs:trantype").getList().add(all, 0)
        $$("invs:ou").setValue("*")
        // $$("invs:trantype").setValue(0)
        // $$("invs:trancol").setValue(0)
        
     
        $$("invs:btnDlt").attachEvent("onItemClick", () => {
            $$("win_lbl").define("label", _("dlt_tran"))
            $$("win_lbl").refresh();
            $$("invs:btnDltp").show()
            $$("invs:btnSynp").hide()
          
            $$("invs:statusp").hide()
            $$("btnReportp").hide()
            $$("igns:win").show()
        })
        $$("invs:btnrep").attachEvent("onItemClick",() => {
            let rows = grid.serialize(), arr = [],count=0, form ='',serial ='',seq ='',isSpecia ='',customerID ='',vrt ='',cur ='',trantype ='',taxcode ='',chk = 0
           
            for (let i=0;i<rows.length;i++) {
                if  (rows[i] && rows[i].chk && rows[i].chk=="on" && rows[i].status != 0 && rows[i].status != 3 && rows[i].status != 4)  {
                     webix.message('Chỉ được phép lập hóa đơn thay thế gồm trạng thái chờ phát hành và chờ phát hành lại, đã hủy', "error")
                     return false
                } 
                if (rows[i] && rows[i].chk && rows[i].chk=="on" && (rows[i].status == 3 || rows[i].status == 4)) {
                    count=1
                    form =rows[i].form
                    serial =rows[i].serial
                    seq =rows[i].seq
                }
                 if (rows[i] && rows[i].chk && rows[i].chk=="on" && (rows[i].status == 0 || rows[i].status == 3 || rows[i].status == 4)) {
                     arr.push(rows[i])
                     chk++
                     if(chk==1){
                        isSpecia = rows[i].isSpecial
                        customerID = rows[i].customerID
                        vrt = rows[i].vrt
                        cur = rows[i].curr
                        trantype = rows[i].trantype
                        taxcode = rows[i].taxCode
                     }else{

                        if (isSpecia != rows[i].isSpecial) {
                            webix.message('Các giao dịch phải cùng loại khách hàng', "error")
                            return false
                         }
                         if (customerID != rows[i].customerID) {
                            webix.message('Các giao dịch phải cùng khách hàng', "error")
                            return false
                         }
                         if (vrt != rows[i].vrt) {
                            webix.message('Các giao dịch phải cùng thuế xuất', "error")
                            return false
                         }
                         if (cur != rows[i].curr) {
                            webix.message('Các giao dịch phải cùng loại tiền', "error")
                            return false
                         }
                         if (trantype != rows[i].trantype) {
                            webix.message('Các giao dịch phải cùng loại giao dịch', "error")
                            return false
                         }
                         if ( taxcode != rows[i].taxCode) {
                            webix.message('Các giao dịch phải cùng mã số thuế', "error")
                            return false
                         }
                     }
                 }
                 

             }
             if (count==0) 
             {
                 webix.message('Chưa chọn giao dịch đã hủy hoặc chờ phát hành lại', "error")
                 return false
             }
             if (arr.length < 1) {
                 webix.message(_("invoice_must_select", "error"))
                 return false
             }
             
             $$("rep:note").setValue(`Thay thế cho hóa đơn mẫu số  ${form} ký hiệu ${serial} số ${seq}`)
             $$("rep:win").show()
        })
        $$("rep:curr").attachEvent("onChange", newv => {
            if (!newv) return
            if (newv == "VND") {
                $$("rep:exrt").setValue(1)
               
            }
            else {
              
               webix.ajax("api/exch/rate", { cur: newv, dt: $$("rep:idt").getValue() }).then(data => { $$("rep:exrt").setValue(Number(data.json())) })
            }
        })
		$$("invs:active").attachEvent("onItemClick", () => {
            let rows = grid.serialize(), arr = []
            let status = 'active'
            for (let row of rows) {
                if (row && row.chk && row.chk == "on" && row.status == 3) {
                    arr.push(row)
                } else if (row && row.chk && row.chk == "on" && row.status != 3) {
                    webix.message(_("alert_itran_inactive"),"error")
                    return false
                }
            }
            if (arr.length < 1) {
                webix.message(_("itran_must_select"))
                return false
            }
            webix.ajax().post("api/itran/update-status", { itran: arr, status }).then(result => {
                // let json = result.json()
                grid.clearAll()
                search()
                webix.message(_("change_status_active_success"))
            })
        })
        $$("invs:inactive").attachEvent("onItemClick", () => {
            let rows = grid.serialize(), arr = []
            let status = 3 //status inactive
            for (let row of rows) {
                if (row && row.chk && row.chk == "on" && (row.status == 4 || row.status == 0)) {
                    arr.push(row)
                } else if (row && row.chk && row.chk == "on" && row.status != 4 && row.status != 0) {
                    webix.message(_("alert_itran_active"),"error")
                    return false
                }
            }
            if (arr.length < 1) {
                webix.message(_("itran_must_select"))
                return false
            }
            webix.ajax().post("api/itran/update-status", { itran: arr, status }).then(result => {
                // let json = result.json()
                grid.clearAll()
                search()
                webix.message(_("change_status_inactive_success"))
            })
        })
        $$("invs:btnsinge").attachEvent("onItemClick", () => {
            let rows = grid.serialize(), arr = []
            let status = 3 ,ou_ck='',count=0//status inactive,
            for (let row of rows) {
                count++
                if (row && row.chk && row.chk == "on" && row.status == 0) {
                    arr.push(row)
                  
                    if(!ou_ck)  ou_ck = row.ou
                    else{if(row.ou != ou_ck)webix.message(_("alert_itran_retail_ou"),"error")} 
                } else if (row && row.chk && row.chk == "on" && row.status != 0) {
                    webix.message(_("alert_itran_retail"),"error")
                    return false
                }
               
               
            }
            if (arr.length < 1) {
                webix.message(_("itran_must_select"))
                return false
            }
           // $$("invs:btnsinge").disable()
            $$("tranN_lbl").define("label", _("btnsinge"))
            $$("tranN_lbl").refresh();
            $$("tranN:btnsaveAll").hide()
            $$("tranN:btnsave").show()
            $$("iou").setValue(ou_ck)
            $$("tranN:trancol").setValue(2)
            $$("tranN:win").show()
           
        })		
        											
        $$("invs:btndnew").attachEvent("onItemClick", () => {
            let rows = grid.serialize(), arr = [], isErrStatus = false,check_type='',count=0,ou_ck=''
            for (let i=0;i<rows.length;i++) {
                if  (rows[i] && rows[i].chk  && rows[i].status != 0 && rows[i].status != 4)  {
                     webix.message('Chỉ được phép lập hóa đơn trạng thái chờ phát hành và lỗi', "error")
                     return false
                } 
                 if (rows[i] && rows[i].chk && rows[i].chk=="on" && (rows[i].status == 0 || rows[i].status == 4)) {
                    arr.push(rows[i])
                    // if(!ou_ck)  ou_ck = rows[i].ou
                    // else{if(rows[i].ou != ou_ck){
                    //     webix.message(_("alert_itran_retail_ou"),"error")} 
                    //  //   return false
                    // }
                    if(rows[i].status == 4){
                        isErrStatus = true
                    }
                }
                // if (rows[i] && rows[i].chk && rows[i].chk=="on" && (rows[i].status == 0 || rows[i].status == 4))  {
                //     if(!check_type){
                //         check_type = rows[i].trantype
                //     }else{
                //         if(check_type != rows[i].trantype) webix.message('Chỉ được phép lập hóa đơn cùng loại giao dịch', "error")
                //     }
                    
                // }
                
             }

             
             if (arr.length < 1) {
                 webix.message(_("invoice_must_select", "error"))
                 return false
             }
            
            if(isErrStatus) {
                webix.confirm({
                    title:_("warning"),
                    ok: _("confirm"),
                    cancel:_("exit"),
                    text:_("message_confirm_itran")
                }).then(function(result){
                    $$("tranN_lbl").define("label", _("invoice_issue"))
                    $$("tranN_lbl").refresh();
                    $$("tranN:btnsaveAll").hide()
                    $$("tranN:btnsave").show()
                    $$("tranN:trancol").setValue(C0)
                    $$("iou").setValue(ou_ck)
                    
                    $$("tranN:win").show()
                    if($$("tranN:trancol").getValue()==0)  $$("tranN:idt").setValue(lastday)
                    if($$("tranN:trancol").getValue()==1)  $$("tranN:idt").setValue(webix.Date.add(new Date(),-1,"day"))
                })
            } else {
                $$("tranN_lbl").define("label", _("invoice_issue"))
                $$("tranN_lbl").refresh();
                $$("tranN:btnsaveAll").hide()
                $$("tranN:btnsave").show()
                $$("tranN:trancol").setValue(C0)
                $$("tranN:win").show()
                $$("iou").setValue(ou_ck)
                if($$("tranN:trancol").getValue()==0)  $$("tranN:idt").setValue(lastday)
                if($$("tranN:trancol").getValue()==1)  $$("tranN:idt").setValue(webix.Date.add(new Date(),-1,"day"))
            }
           
           
        })
        $$("invs:btndnew_a").attachEvent("onItemClick", () => {
            // if(!$$("invs:trantype").getValue() || $$("invs:trantype").getValue()=='*'){
            //     webix.message(_("err_no_gd") ,"error")
            //     return false
            // }
            if(!$$("invs:status").getValue()){
                webix.message("Bạn chưa chọn trạng thái cần gom", "error")
                return false
            }else{
                let stt= $$("invs:status").getValue()
                let arr_stt = stt.split(',')
                for(let a of arr_stt){
                    if (a!=0 && a!= 4)  webix.message(_("err_no_tt"), "error")
                }
            }
           
            let rows = grid.serialize(), isErrStatus = false
            rows.map((row) => {
                if(row && row.status == 4){
                    isErrStatus = true
                }
            })
            
            if(isErrStatus) {
                webix.confirm({
                    title:_("warning"),
                    ok: _("confirm"), 
                    cancel:_("exit"),
                    text:_("message_confirm_itran")
                }).then(function(result){
                    $$("tranN_lbl").define("label", _("invoice_issue_all"))
                    $$("tranN_lbl").refresh();
                    $$("tranN:trancol").setValue(C0)
                    $$("tranN:win").show()
                    $$("iou").setValue('')
                    $$("itype").setValue('')
                    $$("iform").setValue('')
                    $$("iserial").setValue('')
                    $$("tranN:idt").setValue('')
                    $$("tranN:btnsaveAll").show()
                    $$("tranN:btnsave").hide()
                    if($$("tranN:trancol").getValue()==0)  $$("tranN:idt").setValue(lastday)
                    if($$("tranN:trancol").getValue()==1)  $$("tranN:idt").setValue(webix.Date.add(new Date(),-1,"day"))
                })
            }else {
                $$("tranN_lbl").define("label", _("invoice_issue_all"))
                $$("tranN_lbl").refresh();
                $$("tranN:trancol").setValue(C0)
                $$("tranN:win").show()
                $$("iou").setValue('')
                $$("itype").setValue('')
                $$("iform").setValue('')
                $$("iserial").setValue('')
                $$("tranN:idt").setValue('')
                $$("tranN:btnsaveAll").show()
                $$("tranN:btnsave").hide()
                if($$("tranN:trancol").getValue()==0)  $$("tranN:idt").setValue(lastday)
                if($$("tranN:trancol").getValue()==1)  $$("tranN:idt").setValue(webix.Date.add(new Date(),-1,"day"))
            }
           
        })
        $$("tranN:trancol").attachEvent("onChange", () => {
            if($$("tranN:trancol").getValue()==0)  $$("tranN:idt").setValue(lastday)
            if($$("tranN:trancol").getValue()==1)  $$("tranN:idt").setValue(webix.Date.add(new Date(),-1,"day"))
        })
        
        $$("rep:btnsave").attachEvent("onItemClick", () => {
         
            if (!$$("rep:form2").validate()) return false
            
                const param = $$("rep:form2").getValues()
                let rows = grid.serialize(), arr = []
              
                for (let i=0;i<rows.length;i++) {
                   
                    if (rows[i] && rows[i].chk && rows[i].chk=="on" && (rows[i].status == 0 || rows[i].status == 3 || rows[i].status == 4)) {
                        arr.push(rows[i])
                    }

                }
                if (arr.length < 1) {
                    webix.message(_("invoice_must_select", "error"))
                    return false
                }
                $$("rep:btnsave").disable()
                webix.message(`${_("invoice_issue")}.......`, "info", -1, "rep_ms");
                    
                $$("tranN:win").disable();
                $$("tranN:win").showProgress({
                    type: "icon",
                    hide: false
                });
                webix.delay(() => {
                    webix.ajax().post("api/itran/rep", { invs: arr,param:param}).then(result => {
                        const json = result.json()
                        $$("rep:btnsave").enable()
                        console.log(json.status)
                        if (json.status == 1) {
                            grid.clearAll()
                            search()
                            webix.message(_("invoice_col_saved"))
                            $$("rep:win").hide()

                            $$("rep:win").enable();
                          
                            $$("rep:win").hideProgress()
                            webix.message.hide("rep_ms");
                            $$("rep:win").hide()
                        }
                    }).catch(() => { 
                        $$("rep:btnsave").enable()
                        $$("rep:win").enable();
                          
                        $$("rep:win").hideProgress()
                        webix.message.hide("rep_ms");
                        $$("rep:win").hide()
                    })
                })
            
        })
        $$("tranN:btnsave").attachEvent("onItemClick", () => {
         
            if (!$$("tranN:form2").validate()) return false
                let rows = grid.serialize(), arr = []
                let idt =   $$("tranN:idt").getValue()
                let col =   $$("tranN:trancol").getValue()
                let itype =   $$("itype").getValue()
                let iou =   $$("iou").getValue()
                let iform =   $$("iform").getValue()
                let iserial =   $$("iserial").getValue()
				let idtValue = new Date(idt), currentDate = new Date()
                let largestDate = 0
                currentDate.setHours(0, 0, 0)
                
                if(idtValue.getTime() > currentDate.getTime()) {
                    webix.message(_("idt_notgreater_curdt"),"error")
                    return false
                }												
               
                for (let i=0;i<rows.length;i++) {
                    if(col==2){                        
                        if (rows[i] && rows[i].chk && rows[i].chk == "on" && rows[i].status == 0) {
                            arr.push(rows[i])
                        } else if (rows[i] && rows[i].chk && rows[i].chk == "on" && rows[i].status != 0) {
                            
                            webix.message(_("alert_itran_retail"),"error")
                            return false
                        }
                    }else{
                        if  (rows[i] && rows[i].chk && rows[i].chk=="on"  && rows[i].status != 0 && rows[i].status != 4)  {
                            webix.message('Chỉ được phép lập hóa đơn trạng thái chờ phát hành và lỗi', "error")
                            return false
                       } 
                        if (rows[i] && rows[i].chk && rows[i].chk=="on" &&  (rows[i].status == 0 || rows[i].status == 4)) {
                            arr.push(rows[i])
                        }
                    }
                   

                }
                if (arr.length < 1) {
                    webix.message(_("invoice_must_select", "error"))
                    return false
                }
				 if(col == 2){
                    // for(let valueDate of arr) {
                    //     valueDate = new Date(valueDate.valueDate)
                    //     valueDate.setHours(0, 0, 0)
                    //     if(valueDate.getTime() > largestDate) {
                    //         largestDate = valueDate.getTime()
                    //     }
                    // }
                    // if(largestDate > idtValue.getTime()){
                    //     webix.message(_("compare_idt_with_value_date"), "error")
                    //     return false
                    // }
                } else {
                    if(parseInt(col) == 0) {
                        // for(let valueDate of arr) {
                        //     valueDate = new Date(valueDate.valueDate)
                        //     if(valueDate.getMonth() != idtValue.getMonth()) {
                        //         webix.message(`${_("alert_itran_collection_month")} ${idtValue.getMonth() + 1}`, "error")
                        //         return false
                        //     }
                        // }
                    } else if(parseInt(col) == 1) {
                        // for(let valueDate of arr) {
                        //     valueDate = new Date(valueDate.valueDate)
                        //     valueDate.setHours(0, 0, 0)
                        //     if(valueDate.getTime() != idtValue.getTime()) {
                        //         let month = '' + (idtValue.getMonth() + 1), day = '' + idtValue.getDate(), year = idtValue.getFullYear()
                        //         if (month.length < 2) month = '0' + month
                        //         if (day.length < 2) day = '0' + day
                        //         webix.message(`${_("alert_itran_collection_date")} ${[day, month, year].join('-')}`, "error")
                        //         return false
                        //     }
                        // }
                    }
                }								   
                $$("tranN:btnsave").disable()
                webix.message(`${_("invoice_issue")}.......`, "info", -1, "col_ms");
                    
                $$("tranN:win").disable();
                $$("tranN:win").showProgress({
                    type: "icon",
                    hide: false
                });
                webix.delay(() => {
                    webix.ajax().post("api/itran/col", { invs: arr,idt:idt,col:col,itype:itype,iserial:iserial,iform:iform,flag:1,iou:iou}).then(result => {
                        const json = result.json()
                        $$("tranN:btnsave").enable()
                        console.log(json.status)
                        if (json.status == 1) {
                            grid.clearAll()
                            search()
                            webix.message(_("invoice_col_saved"))
                            $$("tranN:win").enable();
                          
                            $$("tranN:win").hideProgress()
                            webix.message.hide("col_ms");
                            $$("tranN:win").hide()
                        }
                    }).catch(() => { 
                        $$("tranN:btnsave").enable()
                        $$("tranN:win").enable();
                          
                        $$("tranN:win").hideProgress()
                        webix.message.hide("col_ms");
                        $$("tranN:win").hide()
                    })
                })
            
        })
        $$("tranN:btnsaveAll").attachEvent("onItemClick", () => {
         
            if (!$$("tranN:form2").validate()) return false
                let rows = grid.serialize(), arr = []
                let idt =   $$("tranN:idt").getValue()
                let col =   $$("tranN:trancol").getValue()
                let iou =   $$("iou").getValue()
                let itype =   $$("itype").getValue()
                let iform =   $$("iform").getValue()
                let iserial =   $$("iserial").getValue()
                let idtValue = new Date(idt), currentDate = new Date()
              currentDate.setHours(0, 0, 0)
            
            if(idtValue.getTime() > currentDate.getTime()) {
                webix.message(_("idt_notgreater_curdt"),"error")
                return false
            }
            try {
                if(parseInt(col) == 0) {
                    for(let row of rows) {
                    
                        let valueDate = new Date(row.valueDate)
                        if(valueDate.getMonth() != idtValue.getMonth()) {
                            webix.message(`${_("alert_itran_collection_month")} ${idtValue.getMonth() + 1}`, "error")
                            return false
                        }
                    }
                } else if(parseInt(col) == 1) {
                    for(let row of rows) {
                        let valueDate = new Date(row.valueDate)
                        valueDate.setHours(0, 0, 0)
                        if(valueDate.getTime() != idtValue.getTime()) {
                            let month = '' + (idtValue.getMonth() + 1), day = '' + idtValue.getDate(), year = idtValue.getFullYear()
                            if (month.length < 2) month = '0' + month
                            if (day.length < 2) day = '0' + day
                            webix.message(`${_("alert_itran_collection_date")} ${[day, month, year].join('-')}`, "error")
                            return false
                        }
                    }
                }
            } catch (error) {
                alert(error)
            }
           
           // alert('row.valueDate')
                $$("tranN:btnsaveAll").disable()
                webix.message(`${_("invoice_issue")}.......`, "info", -1, "col_ms");
                    
                $$("tranN:win").disable();
                $$("tranN:win").showProgress({
                    type: "icon",
                    hide: false
                });
                webix.delay(() => {
                    webix.ajax().post("api/itran/col", { form: form.getValues(),idt:idt,col:col,itype:itype,iserial:iserial,iform:iform,iou:iou,flag:0}).then(result => {
                        const json = result.json()
                        $$("tranN:btnsaveAll").enable()
                        console.log(json.status)
                        if (json.status == 1) {
                            grid.clearAll()
                            search()
                            webix.message(_("invoice_col_saved"))
                            $$("tranN:win").enable();
                          
                            $$("tranN:win").hideProgress()
                            webix.message.hide("col_ms");
                            $$("tranN:win").hide()
                            $$("tranN:btnsaveAll").hide()
                            $$("tranN:btnsave").show()
                        }
                    }).catch(() => { 
                        $$("tranN:btnsaveAll").enable()
                        $$("tranN:win").enable();
                          
                        $$("tranN:win").hideProgress()
                        webix.message.hide("col_ms");
                        $$("tranN:btnsaveAll").hide()
                        $$("tranN:win").hide()
                        $$("tranN:btnsave").show()
                    })
                })
            
        })
        $$("invs:btnSyn").attachEvent("onItemClick", () => {
          
            $$("win_lbl").define("label", _("syn"))
            $$("win_lbl").refresh();
            $$("invs:btnSynp").show()
            $$("invs:btnDltp").hide()
            $$("invs:statusp").hide()
            $$("btnReportp").hide()
            $$("igns:win").show()
           
        })
        $$("edit:btnUpdate").attachEvent("onItemClick", () => {
          
         
                let param = formEdit.getValues()
                console.log(param)
                // if (!form1.validate()) return false
                // let param = form1.getValues()
                param.webix_operation = "update"
                webix.ajax().post("api/itran/edit", param).then(result => {
                    const json = result.json()
                    if (json==1) {
                       webix.message( _("upd_ok_msg"))
                       $$("igns:winedit").hide()
                       search()
                    }
                })
            
        })

        $$("invs:btnReport").attachEvent("onItemClick",() => {
            $$("win_lbl").define("label", _("report"))
            $$("win_lbl").refresh();
            $$("invs:btnSynp").hide()
            $$("invs:btnDltp").hide()
            $$("btnReportp").show()
            $$("igns:win").show()
            $$("invs:statusp").show()
            $$("Tempcolumn").hide()
        })

        $$("btnReportp").attachEvent("onItemClick",()=>{
            const param = formdl.getValues()
            if($$("invs:fdp").getValue()==0 || $$("invs:tdp").getValue()==0){
                webix.message('Phải nhập các trường bắt buộc')
             }
            else if($$("invs:fdp").getValue() > $$("invs:tdp").getValue()){
                    webix.message('Từ ngày phải <= đến ngày')
            }
            else{
                webix.ajax().response("blob").post("api/itran/rexp",param).then(data => {
                    $$("igns:win").hide()
                    search()
                    webix.html.download(data, `${webix.uid()}.xlsx`)
                 })
            }}
            )

        const search = () => {
            grid.clearAll()
            grid.loadNext(recpager.getValue(), 0, null, "trans->api/itran", true)
        }
      

        $$("invs:btnsearch").attachEvent("onItemClick", () => {
            if (form.validate()) search()
        })
        $$("invs:btnDltp").attachEvent("onItemClick", () => {
             const param = formdl.getValues()
            if ($$("trans:form2").validate()) 
         
                webix.ajax().post("api/itran/dtl",param).then(dt => {
                webix.message(_("del_ok_msg_tran"))
                $$("igns:win").hide()
                search()
            })
        })
        
        $$("invs:btnExport").attachEvent("onItemClick",()=>{
            $$("invs:btnExport").disable()
            form.disable()
            form.showProgress()
            const param = form.getValues()
            if(!form.validate()){
                webix.message('Phải nhập các trường bắt buộc')
            }
            else{
               // search()
               let obj = $$("trans:form").getValues()
               
              
                Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                webix.ajax().response("blob").get("api/itran/exp",{filter: obj}).then(data => {
                    webix.html.download(data, `${webix.uid()}.xlsx`)
					if(ROLE.includes('PERM_TRANS_EXP')) $$("invs:btnExport").enable()
                    form.enable()
                    form.hideProgress()						      
                 })
            } 
        })


        $$("invs:btnSynp").attachEvent("onItemClick", () => {
            const param = formdl.getValues()
           if ($$("trans:form2").validate()) 
                {
                    webix.message(`${_("syn")}.......`, "info", -1, "syn_ms");
                    
                    $$("igns:win").disable();
                    $$("igns:win").showProgress({
                        type: "icon",
                        hide: false
                    });
                    webix.ajax().post("api/itran/syn",param).then(result => {
                        const json = result.json(), status = json.status
                        if (status == 2) webix.message(_("syn_ok_msg_tran_no_tran"))
                        else webix.message(_("syn_ok_msg_tran"))
                        $$("igns:win").enable();
                        $$("igns:win").hide()
                        $$("igns:win").hideProgress()
                        webix.message.hide("syn_ms");
                        search()
                    }).catch(e => {
                        $$("igns:win").enable();
                        $$("igns:win").hide()
                        $$("igns:win").hideProgress()
                        webix.message.hide("syn_ms");

                    })
                }
            
       })
        
        grid.attachEvent("onBeforeLoad", function () {
            this.showOverlay(_("loading"))
        })

        grid.attachEvent("onAfterLoad", function () {
            let self = this, count = self.count()
            $$("invs:countinv").setValue(`${_("found")} ${count}`)
            if (count) self.hideOverlay()
            else self.showOverlay(_("notfound"))
        })

        // grid.attachEvent("onAfterSelect", () => {
        //     row = grid.getSelectedItem()
        //     id = row.id
        //     $$("invs:btncopy").enable()
        //     const status = row.status
           
        //     $$("invs:file").enable()
            
            
        // })
        grid.attachEvent("onAfterUnSelect", () => {
            // row = null
            // id = null
            // $$("invs:btncopy").disable()
            // $$("invs:btnrep").disable()
            // if (!["hlv"].includes(ENT)) $$("invs:btncancel").disable()
            // $$("invs:btntrash").disable()
            // $$("invs:btnreject").disable()
            // $$("invs:btnmail").disable()
            // $$("invs:btnadj").disable()
            // $$("invs:btnconvert").disable()
            // $$("invs:file").disable()
        })

        recpager.attachEvent("onChange", () => {
            $$("trans:pager").config.size = recpager.getValue()
            if (form.validate()) search()
        })

        $$("inv:btnorgs").attachEvent("onItemClick", () => {
            this.IorgsWin.show()
        })

      
       
    }
}