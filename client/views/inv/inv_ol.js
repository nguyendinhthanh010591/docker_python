import { JetView } from "webix-jet";
import {ROLE, w, LinkedInputs3, gridnf, gridnf0, ITYPE, ENT, size, d2s, h, ISTATUS, PAYMETHOD, MAILSTATUS, SMSSTATUS } from "models/util"
import Mwin from "views/inv/mwin"
import Iwin from "views/inv/iwin"
import Rwin from "views/inv/rwin"
import ItranNew from "views/inv/ItranNew"




class viewINV extends JetView {

    config() {
        const viewhd = (inv) => {
            try {
                    webix.ajax().post("api/inv/view", inv).then(result => {
                        const json = result.json()
                        //const req = createRequest(json)

                       // let pdf = res.json()
                        let b64 = json.pdf
                        const blob = dataURItoBlob(b64);

                        var temp_url = window.URL.createObjectURL(blob);
                        $$("iwin:ipdf").define("src", temp_url)
                        $$("iwin:win").show()
                        //jsreport.renderAsync(req).then(res => {
                        //                        $$("iwin:ipdf").define("src", res.toDataURI())
                        // webix.ajax().post("api/seek/report", req).then(res => {
                        //     let pdf = res.json()
                        //     let b64 = pdf.pdf
                        //     const blob = dataURItoBlob(b64);
    
                        //     var temp_url = window.URL.createObjectURL(blob);
                        //     $$("iwin:ipdf").define("src", temp_url)
                        //     $$("iwin:win").show()
                        // })
                    })
                
            } catch (err) {
               
                webix.message(err.message, "error")
            }
        }

        const viewrel = (id) => {
            webix.ajax(`api/sea/rel/${id}`).then(result => {
                let json = result.json()
                if (json.length <= 0) this.webix.message(_("not_found_similar_invoice"))
                else {
                    $$("rwin:grid").clearAll()
                    $$("rwin:grid").parse(json)
                    $$("rwin:win").show()
                }
            })
        }
        
        let eFormED =
            [
                {//R2
                    cols: [
                        {}, {}, {},
                        { view: "button", id: "invs:btnSaveD", type: "icon", icon: "mdi mdi-content-save", label: _("yes") },
                        { view: "button", id: "invs:btnCan", type: "icon", icon: "wxi-close", label: _("cancel"), css: "btnExitINV" }
                    ]
                }

            ]
        let gridcol = [
            { id: "vie", header: "", width: 35, template: `<span class='webix_icon wxi-search', title='${_("view")}'></span>` },
            //{ id: "id", header: { text: _("tran_code"), css: "header" }, sort: "server", adjust: true },
            { id: "stt", header: { text: 'STT', css: "header" }, sort: "server", adjust: true },
            //{ id: "id_buyer", header: { text: _("customer_code2"), css: "header" }, adjust: true },
            { id: "idt", header: { text: _("invoice_date"), css: "header" }, sort: "server", format: d2s, adjust: true },
            { id: "form", header: { text: _("form"), css: "header" }, sort: "server", adjust: true },
            { id: "serial", header: { text: _("serial"), css: "header" }, sort: "server", adjust: true },
            //{ id: "seq", header: { text: _("seq"), css: "header" }, sort: "server", adjust: true },
            //{ id: "status", header: { text: _("status"), css: "header" }, sort: "server", collection: ISTATUS(this.app), adjust: true },
            { id: "btax", header: { text: _("taxcode"), css: "header" }, sort: "server", adjust: true },
            { id: "bname", header: { text: _("bname2"), css: "header" }, sort: "server", adjust: true },
           // { id: "buyer", header: { text: _("buyer"), css: "header" }, sort: "server", adjust: true },
            { id: "paym", header: { text: _("payment_method"), css: "header" }, sort: "server", adjust: true },
            { id: "bacc", header: { text: _("bacc"), css: "header" }, sort: "server", adjust: true },
            { id: "bmail", header: { text: _("bmail"), css: "header" }, sort: "server", adjust: true },
            { id: "btel", header: { text: _("tel"), css: "header" }, sort: "server", css: "right", adjust: true, format: "#,###" },
            { id: "baddr", header: { text: _("address"), css: "header" }, sort: "server", adjust: true },
            { id: "sumv", header: { text: _("sumv"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.format },
            { id: "vatv", header: { text: _("vat"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.format },
            { id: "totalv", header: { text: _("totalv"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.format },
            { id: "curr", header: { text: _("curr"), css: "header" }, sort: "server", adjust: true },
            { id: "sec", header: { text: _("search_code"), css: "header" }, format: webix.template.escape, sort: "server", adjust: true },
            { id: "ou", header: { text: _("ou"), css: "header" }, sort: "server", collection: "api/cat/nokache/ou", adjust: true },
            { id: "uc", header: { text: _("creator"), css: "header" }, sort: "server", adjust: true },
            { id: "ic", header: { text: "IC", css: "header" }, sort: "server", adjust: true },
            { id: "note", header: { text: _("note"), css: "header" }, sort: "server", adjust: true },
        ]

        let grid = {
            view: "datatable",
            id: "invs:inv_onl",
            select: "row",
            resizeColumn: true,
            onContext: {
                webix_view: function (e, id) {
                    if (id && id.row) this.select(id)
                }
            },
            columns: gridcol,
            onClick:
            {
                "wxi-search": function (e, r) { viewhd(this.getItem(r)) },
            }
        }

        let form = {
            view: "form",
            id: "invs:formED",
            padding: 3,
            margin: 3,
            elementsConfig: { labelWidth: 85 },
            elements: eFormED
        }
        return {
            view: "window",
            id: "invs:winED",
            height: h,
            width: w,
            position: "center",
            resize: true,
            modal: true,
            head: {
                view: "toolbar", height: 40,
                css: 'toolbar_window',
                cols: [
                    { view: "label", id: "win_lbl", label: _("invoice_similars") },
                    { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => { this.getRoot().hide() } }
                ]
            }
            ,
            body: {
                paddingX: 2,
                rows: [
                    { view: "label", label: '' ,id:"lb_note",css:{"color": "red"}},grid, form
                ]
            }
        }
    }
    show() {
        this.getRoot().show()
    }
}


export default class SynView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        
        
        webix.protoUI({ name: "dependent", $cssName: "combo" }, LinkedInputs3, webix.ui.combo)
        webix.protoUI({ name: "dependent2", $cssName: "richselect" }, LinkedInputs3, webix.ui.richselect)
        let gridcol = [
        { id: "note", header: { text: _("note_err"), css: "header" },adjust:true },
         { id: "LOAI_HD", header: { text: _("invoice_type"), css: "header" }, sort: "server",adjust: true,hidden:true },     
        { id: "MA_GD", header: { text: _("tran_code"), css: "header" }, sort: "server",adjust: true },  
        { id: "NGAY_GD", header: { text: _("Ngay_GD"), css: "header" },adjust: true,format: webix.i18n.dateFormatStr },
        { id: "NGAY_THU_PHI", header: { text: _("Ngay_tp"), css: "header" },adjust: true },   
        { id: "TRANS_NO", header: { text: _("tran_num"), css: "header" } },
        { id: "SO_TK_KH", header: { text: _("so_tk_kh"), css: "header" } },
        { id: "CIF", header: { text: _("cus_code_ol"), css: "header" }, css: "right" },
        { id: "TEN_KH", header: { text: _("ten_kh"), css: "header" }, css: "right" },
        { id: "DIA_CHI_KH", header: { text: _("address"), css: "header" }, css: "right" },
        { id: "MST_KH", header: { text: _("taxcode"), css: "header" }, css: "right" },
        { id: "SDT_KH", header: { text: _("tel"), css: "header" }, css: "right" },
        { id: "EMAIL_KH", header: { text: _("bmail"), css: "header" }, css: "right" },
        { id: "LOAI_KH", header: { text: _("loai_kh"), css: "header" }, css: "right" },
        { id: "LOAI_PHI", header: { text: _("loai_phi"), css: "header" } ,adjust: true,hidden:true},
       
       
        { id: "HINH_THUC_THANH_TOAN", header: { text: _("formality"), css: "header" }, css: "right" },
        { id: "NOI_DUNG", header: { text: _("catalog_items"), css: "header" },editor:"text",adjust:true},
       
        { id: "DON_VI_TINH", header: { text: _("unit"), css: "header" } },
        { id: "SO_LUONG", header: { text: _("quantity"), css: "header" } },
        { id: "DON_GIA", header: { text: _("price"), css: "header" } },
        { id: "THUE_SUAT", header: { text: _("vrt"), css: "header" },adjust:true },
        { id: "TIEN_TRUOC_THUE_NGUYEN_TE", header: { text: _("sumo"), css: "header" },adjust:true },
        { id: "TIEN_THUE_NGUYEN_TE", header: { text: _("vato"), css: "header" },adjust:true},
        { id: "TIEN_TRUOC_THUE_QUY_DOI", header: { text: _("sumv"), css: "header" },adjust:true },
        { id: "TIEN_THUE_QUY_DOI", header: { text: _("vat"), css: "header" },adjust:true },
        { id: "TIEN_SAU_THUE_NGUYEN_TE", header: { text: _("total"), css: "header" },adjust:true },
       
        { id: "TIEN_SAU_THUE_QUY_DOI", header: { text: _("totalv"), css: "header" },adjust:true},
       
        { id: "LOAI_TIEN", header: { text: _("curr"), css: "header" } },
        
        { id: "TY_GIA", header: { text: _("exchange_rate"), css: "header" },adjust:true },
       
        { id: "CN_PGD", header: { text: _("department"), css: "header" },adjust:true }
        
         
    
            
        ]
        const form = {
            view: "form",
            id: "syn:form",
            padding: 1,
            margin: 1,
            elementsConfig: { labelWidth: 70 },
            elements: [
                {
                    cols: [
                        { id: "ma_gd", name: "ma_gd", label: _("tran_no_l_ol"), view: "text" },//gravity: 1.3,
                       
                        { view: "button", id: "syn:btnsearch", type: "icon", icon: "wxi-search", label: _("search"), width: 85 },
                      
                        { id: "syn:btnsave", view: "button", type: "icon", icon: "mdi mdi-content-save", label: _("invoice_issue"), width: 100, disabled: true },
                      
                    ]
                }
            ],
            rules: {
            
                ma_gd: webix.rules.isNotEmpty
            }
        }
        webix.proxy.syn = {
            $proxy: true,
            load: function (view, params) {
              let obj = $$("syn:form").getValues()
              Object.keys(obj).forEach(
                k => (!obj[k] || obj[k] == "*") && delete obj[k]
              )
              if (!params) params = {}
              params.filter = obj
              return webix.ajax(this.source, params)
            }
          }
        const grid = {
            view: "datatable",
            id: "syn:grid",
            select: "row",
            resizeColumn: true,
			editable:true,			  
            columns: gridcol
        }

        return { paddingX: 2, rows: [form, grid] }
    }
    ready() {
      
    }
    init() {
		if (!ROLE.includes('PERM_INV_ONLINE')) {
            this.show("/top/home")
            window.location.replace(`/${homepage}`)
            return
          }
        let grid = $$("syn:grid")
        this.ItranNew = this.ui(ItranNew)
        this.viewINV = this.ui(viewINV)
        this.Iwin = this.ui(Iwin)
        this.Mwin = this.ui(Mwin)
        this.Rwin = this.ui(Rwin)
        webix.extend($$("tranN:win"), webix.ProgressBar)  
        webix.extend(grid, webix.ProgressBar)
        webix.extend($$("invs:formED"), webix.ProgressBar)  
        
        $$("syn:btnsearch").attachEvent("onItemClick", () => {
            
            if ($$("syn:form").validate()) {
            grid.clearAll()
            grid.showProgress()
            $$("syn:btnsave").disable()
            $$("invs:inv_onl").clearAll();
            webix.ajax().get("api/tran/tran_ol", { ma_gd: $$("ma_gd").getValue() }).then(result => {
                let json = result.json()
                 $$("syn:grid").parse(json.data)
                // $$("invs:inv_onl").parse(json.data2)
                
                 if(json.note) {
                    $$("lb_note").define("label", json.note);
                    $$("lb_note").refresh();
                    $$("invs:btnSaveD").disable()
                    
                 } else {
                     $$("lb_note").define("label", ''); $$("lb_note").refresh(); $$("invs:btnSaveD").enable();
                     if(ROLE.includes('PERM_INV_ONLINE_INSERT')) $$("syn:btnsave").enable()
                 }
                 grid.hideProgress()
            }).catch(() => { grid.hideProgress() })
           // grid.loadNext(size, 0,  $$("syn:btnsave").enable(), "syn->api/tran_ol", true)
        }
        })
       
        
        $$("invs:btnSaveD").attachEvent("onItemClick", () => {
            let rows = $$("invs:inv_onl").serialize(),rowsGD= $$("syn:grid").serialize()
            
            $$("invs:formED").showProgress()           
            $$("invs:btnSaveD").disable()           
            webix.ajax().post("api/tran/sav_ol", { invs: rows ,trans:rowsGD}).then(result => {                     
                const rows = result.json()
                if (rows == 1) {
                    webix.message(_("invoice_issued"), "success")
                }
                $$("invs:formED").hideProgress()
                $$("invs:winED").hide()
                $$("syn:btnsave").disable()
            }).catch(() => { $$("invs:formED").hideProgress() })
        })
        $$("syn:btnsave").attachEvent("onItemClick", () => {
            let rowsGD= $$("syn:grid").serialize()
            webix.ajax().post("api/tran/createiv", {trans:rowsGD}).then(result => {                     
                const rows = result.json()
                $$("invs:inv_onl").clearAll()
                $$("invs:inv_onl").parse(rows)
                $$("invs:winED").show()
            }).catch(() => { $$("invs:formED").hideProgress() })
           
            //let rows = grid.serialize(), inv = []
            
           // for (let row of rows) {
                
                // let subI = {}
                // subI.id = row.MA_GD
                // subI.id_buyer = row.SO_TK_KH
                // subI.bname = row.TEN_KH
                // subI.btax = row.MST_KH
                // subI.baddr = row.DIA_CHI_KH
                // subI.sumo = row.TIEN_TRUOC_THUE_NGUYEN_TE
                // subI.vato = row.TIEN_THUE_NGUYEN_TE
                // subI.totalo = row.TIEN_SAU_THUE_NGUYEN_TE
                // subI.curr = row.LOAI_TIEN
                // subI.exrt = row.TY_GIA
              //  inv.push(invd)
           // }
           // $$("invs:inv_onl").clearAll();
            //$$("invs:inv_onl").parse(inv)
        })
        $$("invs:btnCan").attachEvent("onItemClick", () => {
            $$("invs:winED").hide()
        })
       
        grid.attachEvent("onAfterLoad", function () {
            if (!this.count()) this.showOverlay(_("notfound"))
            else this.hideOverlay()
          })
    }
}
