import { JetView } from "webix-jet"
import {ROLE, LinkedInputs2, filter, d2s, gridnf0, PAYMETHOD, ITYPE, CARR, ENT, setConfStaFld ,RPP,OU,ddate} from "models/util"
import Iwin from "views/inv/iwin"
const size = ENT == 'hlv' ? 100 : 10
const configs = (type,flag) => {
    let lang = webix.storage.local.get("lang")
   
    webix.ajax().get(`api/sea/afbt?id=${type}`).then(r=>{
        let datasr = r.json()
        let removeDuplicates = (array, key) => {
            let lookup = new Set();
            return array.filter(obj => obj[key] && !lookup.has(obj[key]) && lookup.add(obj[key]));
        }
        let arr = [{id: "*", value: _("all") }, ...datasr]

        $$("form").getList().clearAll()
       
        $$("form").getList().parse(removeDuplicates(arr,'id'))
        $$("form").setValue("*")
    })

    webix.ajax().get(`api/seq/asbf?id=${$$("form").getValue()}`).then(r=>{
        let datasr = r.json()
        let removeDuplicates = (array, key) => {
            let lookup = new Set();
            return array.filter(obj => obj[key] && !lookup.has(obj[key]) && lookup.add(obj[key]));
        }
        let arr = [{id: "*", value: _("all") }, ...datasr]

        $$("serial").getList().clearAll()
       
        $$("serial").getList().parse(removeDuplicates(arr,'id'))
        $$("serial").setValue("*")

    })

    $$("form").attachEvent("onChange",()=>{
        webix.ajax().get(`api/seq/asbf?id=${$$("form").getValue()}`).then(r=>{
            let datasr = r.json()
            let removeDuplicates = (array, key) => {
                let lookup = new Set();
                return array.filter(obj => obj[key] && !lookup.has(obj[key]) && lookup.add(obj[key]));
            }
            let arr = [{id: "*", value: _("all") }, ...datasr]
    
            $$("serial").getList().clearAll()
           
            $$("serial").getList().parse(removeDuplicates(arr,'id'))
            $$("serial").setValue("*")
    
        })
    })
    
    webix.ajax().post(`api/cus`,{itype:type,lang:lang}).then(result => {
        let cols = result.json(), len = cols.length, grid = $$("appr:grid")
        let arr = grid.config.columns.filter(item => !CARR.includes(item.id))
        if (len > 0) {
            for (const col of cols) {
                // arr.push({ id: col.id, header: { text: col.label, css: "header" }, adjust: true })
                let obj = { id: col.id, header: { text: col.label, css: "header" }, adjust: true }
                if (col.view == "datepicker") obj.format = webix.i18n.dateFormatStr
                if (col.suggest) obj.collection = col.suggest.data
                arr.push(obj)
            }
        }
        grid.config.columns = arr
        grid.refreshColumns()
        let r4 = $$("appr:r4")
        r4.removeView("appr:r5")
        r4.removeView("appr:r6")
        if (len > 0) {
            if (len <= 5) {
                while (len < 5) {
                    cols.push({})
                    len++
                }
                r4.addView({ id: "appr:r5", cols: cols }, 0)
                r4.addView({ id: "appr:r6", hidden: true }, 1)
            }
            else {
                while (len < 10) {
                    cols.push({})
                    len++
                }
                r4.addView({ id: "appr:r5", cols: cols.splice(0, len / 2) }, 0)
                r4.addView({ id: "appr:r6", cols: cols }, 1)
            }
        }
        else {
            r4.addView({ id: "appr:r5", hidden: true }, 0)
            r4.addView({ id: "appr:r6", hidden: true }, 1)
        }

        
        //HLV onChange chon ServCode [c2:servCode, c7:servContent, c8:servName]
        if (ENT=="hlv") {
            // ["c7","c8"].map((e)=>$$(e).disable())
            $$("c2").attachEvent("onChange", (newv, oldv) => {
                if (!newv) ["c7","c8"].map((e)=>$$(e).setValue())
                webix.ajax(`api/kat/payments`).then(result => {
                    let json = result.json(), val = $$("c2").getValue()
                    let arr = json.filter((a)=>{return a.name == val})
                    if(arr.length>0) {
                        let item = arr[0], des = JSON.parse(item.des), name = des.servicename, content = des.servicecontent
                        $$("c7").setValue(content)
                        $$("c8").setValue(name)
                    }
                })
            })
        }
        //
    })
}
export default class ApprView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        webix.protoUI({ name: "dependent2", $cssName: "richselect" }, LinkedInputs2, webix.ui.richselect)
        webix.proxy.appr = {
            $proxy: true,
            load: function (view, params) {
                let obj = $$("appr:form").getValues()
                if(ENT=='vcm'){
                    obj.status = "1"
                }else{
                    obj.status = "2"
                }
                
                Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                if (!params) params = {}
                params.filter = obj
                return webix.ajax(this.source, params)
            }
        }
        let hdbCheckAppr5Day = (item) => {
            let idate = new Date(item.idt), nowd = new Date(), difT = nowd.getTime() - idate.getTime(), difD = difT / (1000 * 3600 * 24)
            if (difD > 5) {
                return true
            } else return false
        }
        let pager = { view: "pager", id: "appr:pager", size: size, template: "{common.first()}{common.prev()}{common.pages()}{common.next()}{common.last()}",
            on: {
                onBeforePageChange:function(id){
                    let grida = $$("appr:grid")
                    let flag = false
                    let size = $$("appr:pager").config.size
                    let page = grida.getPage()
                    for (let i=0; i<size; i++){
                        let id = grida.getIdByIndex(size*page+i)
                        if(grida.getItem(id) && grida.getItem(id).chk){
                            flag = true
                        }
                    }
                    if($$("appr:grid").getHeaderContent("cm1") && flag)
                        $$("appr:grid").getHeaderContent("cm1").uncheck()
                }
            }
        }
        let columns = [
            { id: "vie", header: "", width: 35, template: `<span class='webix_icon wxi-search', title='${_("view")}'></span>`},
            { id: "chk", header: { content: "masterCheckbox", css: "center", contentId:"cm1" }, css: "center", template:function(obj, common, value, config, ind){
                if(ENT == "hdb" && hdbCheckAppr5Day(obj)) return ""
                return common.checkbox(obj, common, value, config)
            }, width: 45 },
            { id: "id", header: { text: "ID", css: "header" }, sort: "server", css: "right", width: 45 },
            { id: "idt", header: { text: _("invoice_date"), css: "header" }, sort: "server", format: d2s, adjust: true },
            { id: "form", header: { text: _("form"), css: "header" }, sort: "server", adjust: true },
            { id: "serial", header: { text: _("serial"), css: "header" }, sort: "server", adjust: true },
            { id: "seq", header: { text: _("seq"), css: "header" }, sort: "server", adjust: true },
            { id: "btax", header: { text: _("taxcode"), css: "header" }, sort: "server", adjust: true },
            { id: "bname", header: { text: _("bname2"), css: "header" }, sort: "server", adjust: true },
            { id: "buyer", header: { text: _("buyer"), css: "header" }, sort: "server", adjust: true },
            { id: "bmail", header: { text: _("bmail"), css: "header" }, sort: "server", adjust: true },
            { id: "baddr", header: { text: _("address"), css: "header" }, adjust: true },
            (ENT == 'mzh')?{ id: "sum", header: { text: _("sumo"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat }:{ id: "sumv", header: { text: _("sumv"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.format },
            (ENT == 'mzh')?{ id: "vat", header: { text: _("vato"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat }:{ id: "vatv", header: { text: "VAT", css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.format },
            (ENT == 'mzh')?{ id: "total", header: { text: _("totalo"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat }:{ id: "totalv", header: { text: _("totalv"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.format },
            { id: "curr", header: { text: _("curr"), css: "header" }, sort: "server", adjust: true },
            { id: "sec", header: { text: _("search_code"), css: "header" }, format: webix.template.escape, sort: "server", adjust: true },
            { id: "ou", header: { text: _("ou"), css: "header" }, collection: "api/cat/nokache/ou", adjust: true },
            { id: "uc", header: { text: _("creator"), css: "header" }, sort: "server", adjust: true },
            { id: "ic", header: { text: "IC", css: "header" }, sort: "server", adjust: true },
            { id: "note", header: { text: _("note"), css: "header" }, sort: "server", adjust: true },
        ]
        if (ENT == 'hdb') {
            columns.splice(11, 3) //Xóa cột tiền VND
            columns.splice(11, 0,
                { id: "sum", header: { text: _("sumo"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat },
                { id: "vat", header: { text: _("vato"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat },
                { id: "total", header: { text: _("totalo"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat })
        }
        if (ENT != 'hlv') {
            columns.push({ id: "adjdes", header: { text: _("replace"), css: "header" }, format: webix.template.escape, sort: "server", adjust: true })
            columns.push({ id: "cde", header: { text: _("replaced"), css: "header" }, sort: "server", adjust: true })
        } else {
            columns.push({ id: "adjdes", header: { text: _("repadj"), css: "header" }, format: webix.template.escape, sort: "server", adjust: true })
            columns.push({ id: "cde", header: { text: _("repadjed"), css: "header" }, sort: "server", adjust: true })
        }
        let grid = {
            view: "datatable",
            id: "appr:grid",
            scroll: "xy",
            resizeColumn: true,
            editable: false,
            datafetch: size,
            pager: "appr:pager",
            on: {
                onCheck:function(id, colId, value){
                    if(value == 1 && ENT == "hdb") {
                        let row = id, item = this.getItem(row)
                        if (hdbCheckAppr5Day(item)) item.chk = false
                    }
                },
                onItemClick: function (id, colId, value) {
                    let row = id.row, item = this.getItem(row)
                    if(ENT == "hdb" && hdbCheckAppr5Day(item)) {
                        webix.message(`${_("hdb_5_day_out")} ID: ${item.id}`, "error")
                        return
                    }
                    item.chk = !item.chk
                    this.refresh(row)
                }
            },
            onClick: {
                "wxi-search": function (e, r) {
                    if(ROLE.includes('PERM_APPROVE_INV_MANAGE_APPR')){
                        const id = r.row
                        webix.ajax(`api/app/htm/${id}`).then(result => {
                           // const json = result.json()
                           // const req = createRequest(json)
                            //jsreport.renderAsync(req).then(res => {
                            //$$("iwin:ipdf").define("src", res.toDataURI())
                            let pdf = result.json()
                            let b64 = pdf.pdf
                            const blob = dataURItoBlob(b64);
    
                            var temp_url = window.URL.createObjectURL(blob);
                            $$("iwin:ipdf").define("src", temp_url)
                            $$("iwin:win").show()
                            // webix.ajax().post("api/seek/report", req).then(res => {
                            //     let pdf = res.json()
                            //     let b64 = pdf.pdf
                            //     const blob = dataURItoBlob(b64);
    
                            //     var temp_url = window.URL.createObjectURL(blob);
                            //     $$("iwin:ipdf").define("src", temp_url)
                            //     $$("iwin:win").show()
                            // })
    
                        })
                    }else webix.message(_("no_authorization"), "error")
                    
                }
            },
            columns: columns
        }
        let eForm =
            [
                {
                    cols: [
                        { id: "fd", name: "fd", label: _("fd"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr },
                        { id: "td", name: "td", label: _("td"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr },
                        { id: "paym", name: "paym", label: _("payment_method"), view: "combo", options: PAYMETHOD(this.app) },
                        { id: "curr", name: "curr", label: _("currency"), view: "combo", suggest: { url: "api/cat/kache/currency" } },
                        { id: "ou", name: "ou", label: _("ou"), view: "combo", suggest: { url: "api/cat/nokache/ou", filter: filter, relative: "left" } }
                    ]
                },
                {
                    cols: [
                        { id: "type", name: "type", label: _("invoice_type"), view: "combo", placeholder: _("choose_inv_type"), options: ITYPE(this.app) },//richselect
                        { id: "form", name: "form", label: _("form"), view: "dependent2", options: [], master: "type", dependentUrl: "api/seq/afbt?id=", relatedView: "serial", relatedAction: "enable" },
                        { id: "serial", name: "serial", label: _("serial"), view: "dependent2", options: [], master: "form", dependentUrl: "api/seq/asbf?id=", disabled: true},
                        { name: "seq", label: _("seq"), view: "text", attributes: { maxlength: 8 }, validate: v => { return !v || /^[0-9]{1,8}$/.test(v) }, invalidMessage: _("seq_invalid"), placeholder: _("seq") },
                        { name: "ic", label: "IC", view: "text", attributes: { maxlength: 36 }, placeholder: "IC"}
                    ]
                },
                { //Row 3
                    cols: [
                        ENT != "hdb" 
                            ? { name: "btax", label: _("taxcode"), view: "text", attributes: { maxlength: 14 }, placeholder: _("taxcode") }
                            : { name: "btax", label: _("taxcode"), view: "text", attributes: { maxlength: 25 }, placeholder: _("taxcode") },
                        { name: "buyer", label: _("buyer"), view: "text", attributes: { maxlength: 50 }, placeholder: _("buyer") },
                        { name: "bname", label: _("bname2"), view: "text", attributes: { maxlength: 50 }, placeholder: _("bname2") },
                        {name: "sec", label: _("search_code"), view: "text", attributes: { maxlength: 10 }, placeholder: _("search_code")},
                        {id: "invs:user", name: "user", label: _("creator"), view: "text", attributes: { maxlength: 24 }, placeholder: _("creator")}, //richselect},
                    ]
                },
                { //Row 3.2
                    cols: [
                       
                        { id: "invs:bacc", name: "bacc", label: _("bacc"), view: "text", placeholder: _("bacc"), attributes: { maxlength: 24 }},
                        {},{},{},{}
                    ]
                },
                { id: "appr:r4", rows: [{ id: "appr:r5", hidden: true }, { id: "appr:r6", hidden: true }] },
                {
                    cols: [
                        {}, {}, {}, {}, {}, {}, {},
                        { view: "button", id: "appr:btnall", type: "icon", icon: "wxi-checkbox-marked", label: _("approve_all"),disabled:ROLE.includes('PERM_APPROVE_INV_MANAGE_APPR') ? false : true /*, hidden: (ENT == "scb") ? false : true*/ },
                        { view: "button", id: "appr:btnapp", type: "icon", icon: "wxi-check", label: _("approve"), disabled:ROLE.includes('PERM_APPROVE_INV_MANAGE_APPR') ? false : true },
                        { view: "button", id: "appr:btnsearch", type: "icon", icon: "wxi-search", label: _("search") }
                            ]      
                }
            ]
        let form = {
            view: "form",
            id: "appr:form",
            padding: 3,
            margin: 3,
            elementsConfig: { labelWidth: 85 },
            elements: eForm
            , rules: {
                fd: webix.rules.isNotEmpty,
                td: webix.rules.isNotEmpty,
                $obj: function (data) {
                    const fd = data.fd, td = data.td
                    if (fd > td) {
                        webix.message(_("date_invalid_msg"))
                        return false
                    }
                    const dtime = (new Date(td)).getTime() - (new Date(fd)).getTime()
                    if (['jpm'].includes(ENT)) {
                        if (dtime > ddate*6) {
                            webix.message(String(_("dt_search_between_day")).replace('#hunglq#', (6 * ddate / (1000 * 3600 * 24))))
                            return false
                        }
                    }
                    else {
                        if (dtime > ddate) {
                            webix.message(String(_("dt_search_between_day")).replace('#hunglq#', (ddate / (1000 * 3600 * 24))))
                            return false
                        }
                    }
                    return true
                }

            }
        }
        let recpager = { view: "combo", id: "recpager", maxWidth: 70, tooltip: _("row_per_page"), value: size, options: RPP }
        let title_page = { view: "label", label:`<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("invoice_approve")}</span>`}
        return { paddingX: 2, rows: [title_page, form, grid, { cols: [recpager,pager, { id: "appr:countinv", view: "label" }, {}] }] }
    }
    ready(v, urls) {
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
       
    }

    init() { 
        if (!ROLE.includes('PERM_APPROVE_INV_MANAGE')) {
            this.show("/top/home")
            window.location.replace(`/${homepage}`)
            return
          }
        $$("lang:lang").attachEvent("onChange", function(newv, oldv){
          
            //trungpq10 sua song ngu{
            const type_inv=(ITYPE(this.app))[0].id
            let lang = newv
            configs(type_inv)
         //}
        });
        this.Iwin = this.ui(Iwin)
        let date = new Date(), firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1),firstDayCore = new Date(date.getFullYear(), date.getMonth(), 1), grid = $$("appr:grid"), form = $$("appr:form")
        const all = { id: "*", value: _("all") }
        webix.extend(grid, webix.ProgressBar)
        webix.extend($$("appr:form"), webix.ProgressBar)
        if(ENT == "scb"){
            if((date.getMonth() - 5) <= 0){
                firstDay = new Date(date.getFullYear() - 1,(12 + (date.getMonth() - 5)), 1)
            }else{
                firstDay = new Date(date.getFullYear(),date.getMonth() - 5, 1)
            }
        }
        $$("fd").setValue(firstDayCore)
        $$("td").setValue(date)
        $$("type").getList().add(all, 0)
        $$("curr").getList().add(all, 0)
        $$("paym").getList().add(all, 0)
        $$("ou").getList().add(all, 0)
        $$("curr").setValue("*")
        $$("paym").setValue("*")
        if(ENT=="sgr") $$("ou").setValue(OU)
        else $$("ou").setValue("*")

        if(["hlv","tlg","fhs","mzh","aia","fbc"].includes(ENT)) $$("appr:btnall").show()


        $$("type").attachEvent("onChange", (newv) => {
            configs(newv)
        })

        $$("type").setValue("01GTKT")

        const search = () => {
            if (form.validate()) {
                grid.clearAll()
                grid.loadNext($$("recpager").getValue(), 0, null, "appr->api/app", true)
                if ($$("appr:grid").getHeaderContent("cm1")) $$("appr:grid").getHeaderContent("cm1").uncheck()
            }
        }

        const appr = (win) => {
           
            // let items = grid.serialize(), arr = []
            // for (let item of items) {
            //     if (item && item.chk) arr.push(item.id)
            // }

            let arr = []
            let grida = $$("appr:grid")
            var size = grida.getPager().config.size
            var page = grida.getPage()
            var ind = size*page
            for ( let i=0; i<size; i++){
                let id = grida.getIdByIndex(ind+i)
                if (grida.getItem(id) && grida.getItem(id).chk) {
                   
                    arr.push(id)
                }
            }
            if (arr.length < 1) {
                webix.message(_("invoice_must_select"), "error")
                return false
            }
            //alert(arr)
            webix.confirm(_("approve_confirm"), "confirm-warning").then(() => {
                $$("appr:form").disable();
                if(ENT == "aia") grid.disable()
                $$("appr:form").showProgress({
                type:"icon",
                hide:false
                });
                webix.ajax().post("api/app", { ids: arr }).then(result => {
                    const rows = result.json()
                    if (rows.hasOwnProperty("mst")) {
                        SELECT_CKS_PROMPT(rows.mst).then(values => {
                            webix.message(_("handling"), "", 3000)
                            let row = rows.arr
                           // row.serial = values.serial
                        webix.ajax().post(`${USB}FptEsign/xmls/${values.serial}`,JSON.stringify(row)).then(result => {
                           
                            let _rows = result.json()
                            for (let v of _rows) {
                                if(v.status==2)  webix.message(v.msg,"error")
                            }
                           // const rows2 = {incs: [row.id], signs: [{incs:row.id,xml:_rows.xml}]} // status=1 va xml signed
                            webix.ajax().put("api/app", { xmls: _rows }).then(result => {
                                grid.enable()
                                $$("appr:form").enable()
                                $$("appr:form").hideProgress()
                                webix.message(`${_("approved")} ${result.json()} ${_("invoice")}`, "success")
                                if (win) $$("iwin:win").hide()
                                search()
                            }).catch(e => {   
                                grid.enable()
                                $$("appr:form").enable()
                                $$("appr:form").hideProgress()                            
                                search()
                                if (e.message) {
                                    webix.message(e.message)
                                } 
                            })
                        }).catch(e => {
                            grid.enable()
                            $$("appr:form").enable()
                            $$("appr:form").hideProgress()
                            search()
                            if (e.message && e.message.indexOf('CKS-00001') > 0) {
                                webix.message(_("certificate_error"), "error")
                            } else {
                                if (e.message) {
                                    webix.message(e.message)
                                } else {
                                    if (!e.responseText)
                                        webix.message(_("certificate_error"), "error")
                                    else
                                        webix.message(_("approve_failed"), "error")
                                }
                            }
                            
                        })
                    }).finally(() => {
                        grid.hideProgress()
                    })
                    }
                    else {
                        grid.enable()
                        $$("appr:form").enable()
                        $$("appr:form").hideProgress()
                       // webix.message(`${_("approved")} ${rows} ${_("invoice")}`, "success",-1 )
                        webix.alert({
                            title:_("success"),
                            ok:"Ok",
                            text:`${_("approved")} ${rows.count} ${_("invoice")}`
                            
                        })
                        if (win) $$("iwin:win").hide()
                        search()
                    }
                }).catch(e => {
                    grid.enable()
                    $$("appr:form").enable()
                    $$("appr:form").hideProgress()
                    search()
                })
            })
        }
        $$("appr:btnall").attachEvent("onItemClick", () => {
            if (form.validate()) {
                webix.confirm(_("approve_all_confirm"), "confirm-warning").then(() => {
                    grid.showProgress()
                     $$("appr:form").disable();
                   
                     $$("appr:form").showProgress({
                       type:"icon",
                       hide:false
                     });
                    $$("appr:btnall").disable()
                    let obj = $$("appr:form").getValues()
                    if(ENT=='vcm'){
                        obj.status = "1"
                    }else{
                        obj.status = "2"
                    }
                   
                    Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                    let params = {}
                    params.filter = obj
                    webix.ajax("api/app/all", params).then(result => {
                        const rows = result.json()
                        search()
                        grid.hideProgress()
                        webix.alert({
                            title:_("success"),
                            ok:"Ok",
                            text:`${_("approved")} ${rows} ${_("invoice")}`
                            
                        })
                        $$("appr:btnall").enable()
                        $$("appr:form").enable()
                        $$("appr:form").hideProgress()
                       // webix.message(`${_("approved")} ${rows} ${_("invoice")}`, "success", -1)
                    }).catch(()=>{grid.hideProgress()
                        $$("appr:btnall").enable()
                        $$("appr:form").enable()
                        $$("appr:form").hideProgress()})
                })
            }
        })
        $$("recpager").attachEvent("onChange", () => {
            $$("appr:pager").config.size = $$("recpager").getValue()
            if (form.validate()) search()
        })
        $$("appr:btnsearch").attachEvent("onItemClick", () => {          search() })
        $$("appr:btnapp").attachEvent("onItemClick", () => { appr(0) })
        $$("iwin:btnapp").hide()
        // $$("iwin:btnapp").attachEvent("onItemClick", () => { appr(1) })
        grid.attachEvent("onBeforeLoad", function () { this.showOverlay(_("loading")) })
        grid.attachEvent("onAfterLoad", function () {
            let self = this, count = self.count()
            $$("appr:countinv").setValue(`${_("found")} ${count}`)
            if (count) self.hideOverlay()
            else self.showOverlay(_("notfound"))
        })
         //Chỉnh frozen column cho fhs
         if(["fhs","fbc"].includes(ENT)){
            $$("appr:grid").define("leftSplit", 2);
            $$("appr:grid").refreshColumns()
        }
    }
}