import { JetView } from "webix-jet"
import { d2s, ISTATUS, ENT, setConfStaFld, ROLE } from "models/util"

export default class Rwin extends JetView {
    config() {
        _ = this.app.getService("locale")._
        let columns = [
            { id: "vie", header: "", width: 35, adjust: true, template: `<span class='webix_icon wxi-search', title='${_("view")}'></span>` },
            { id: "id", header: { text: "ID", css: "header" }, css: "right",  adjust: true },
            { id: "idt", header: { text: _("invoice_date"), css: "header" },  format: d2s, adjust: true },
            { id: "form", header: { text: _("form"), css: "header" },  adjust: true },
            { id: "serial", header: { text: _("serial"), css: "header" },  adjust: true },
            { id: "seq", header: { text: _("seq"), css: "header" },  adjust: true },
            { id: "status", header: { text: _("status"), css: "header" },  collection: ISTATUS(this.app), adjust: true },
            ENT != "vcm" ? 
            { id: "sec", header: { text: _("search_code"), css: "header" }, format: webix.template.escape,  adjust: true } :
            { id: "c6", header: { text: _("search_code"), css: "header" }, format: webix.template.escape,  adjust: true }
        ]
        
        if (ENT == 'hlv') {
            columns.push({ id: "adjdes", header: { text: _("replace"), css: "header" }, format: webix.template.escape,  adjust: true })
            columns.push({ id: "cde", header: { text: _("replaced"), css: "header" },  adjust: true })
        }
        else {
            columns.push({ id: "adjdes", header: { text: _("repadj"), css: "header" }, format: webix.template.escape,  adjust: true })
            columns.push({ id: "cde", header: { text: _("repadjed"), css: "header" },  adjust: true })
        }

        const grid = {
            view: "datatable",
            id: "rwin:grid",
            columns: columns,
            onClick: {
                "wxi-search": (a, id, c) => {
                    webix.ajax(`api/inv/htm/${id}`).then(result => {
                       // const req = createRequest(result.json())
                        //jsreport.renderAsync(req).then(res=>{
                        //    $$("iwin:ipdf").define("src", res.toDataURI())
                        let { pdf } = result.json()
                        const blob = dataURItoBlob(pdf);
                        
                        var temp_url = window.URL.createObjectURL(blob);
                        $$("iwin:ipdf").define("src", temp_url)
                        $$("iwin:win").show()
                        // webix.ajax().post("api/seek/report", req).then(res => {
                        //     let pdf = res.json()
                        //     b64 = pdf.pdf
                        //     const blob = dataURItoBlob(b64);

                        //     var temp_url = window.URL.createObjectURL(blob);
                        //     $$("iwin:ipdf").define("src", temp_url)
                        //     $$("iwin:win").show()
                        // })
                    })
                }
            }
        }
        return {
            view: "window",
            id: "rwin:win",
            modal: true,
            fullscreen: true,
            head: {
                view: "toolbar"
                , height: 35
                , cols: [
                    { view: "label", label: _("invoice_similar") ,id:"rwin:winlb"},
                    {
                        cols: [
                            {},
                            { view: "button", type: "icon", icon: "wxi-close", hotkey: "esc", width: 30, click: () => $$("rwin:win").hide() }
                        ], fillspace: true, align: "right"
                    }
                ]
            },
            body: grid
        }
    }
    ready(v, urls) {
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
    show() {
        this.getRoot().show()
    }
}    