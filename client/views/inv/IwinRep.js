import { JetView } from "webix-jet"
import {textnf} from "models/util"

export default class IwinRep extends JetView {

    
        config() {
            _ = this.app.getService("locale")._
            let eFormn =
            [
                { id: "rep:idt", name: "idtr", label: _("invoice_date"), labelWidth: 100,view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr, required: true,gravity: 2 }
                ,{//R2
                    cols: [
                        { id: "rep:curr", name: "currr", label: _("currency"),view: "combo", suggest: { url: "api/cat/kache/currency" },  required: true },
                    
                        { id: "rep:exrt", name: "exrtr", label: _("exchange_rate"), view: "text", value: 1, inputAlign: "right", format: textnf, required: true }
                    ]
                },
                { id: "rep:note", name: "note", label: _("note"), labelWidth: 100,view: "text", stringResult: true, required: true,gravity: 2 }
                ,
                    {//R2
                    cols: [
                        { view: "button", id: "rep:btnsave", type: "icon", icon: "mdi mdi-content-save", label: _("invoice_issue")},
                     
                        { view: "button", id: "rep:btnexit", type: "icon", icon: "mdi mdi-exit-to-app", label: _("exit"),  click: () => { this.getRoot().hide() } }
                    ]
                }
                
            ]
            return {
                view: "window",
                id: "rep:win",
            
                position: "center",
                resize: true,
                modal: true,
                width: 500,
                head: {
                    view: "toolbar", height: 40,
                    css: 'toolbar_window',
                    cols: [
                        { view: "label", id:"rep_lbl",label: _("invoice_issue") },
                        { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => { this.getRoot().hide() } }
                    ]
                }
                ,
                body: {
                    view: "form",
                    id: "rep:form2",
                    padding: 3,
                    margin: 3,
                    elementsConfig: { labelWidth: 100 },
                    elements: eFormn
                    , rules: {
                        idtr: webix.rules.isNotEmpty,
                        currr: webix.rules.isNotEmpty,
                        exrtr: webix.rules.isNotEmpty,
                        note: webix.rules.isNotEmpty
                      
                    },
                    on: {
                        onAfterValidation: (result, value) => {
                            if (!result) {
                                if (JSON.stringify(value.valueOf(0)) !== JSON.stringify({})) webix.message(_("required_msg"))
                            }
                        }
                    }
                }
            }
        }
        show() {
            this.getRoot().show()
        }
}
