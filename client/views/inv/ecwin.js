import { JetView } from "webix-jet"
import { ENT, setConfStaFld } from "models/util"
export default class Ecwin extends JetView {
    config() {
        _ = this.app.getService("locale")._
        let cols = [
            {
                id: "chk", header: { content: "masterCheckbox", css: "center", contentId:"chk1" }, width: 35, template: (obj, common, value, config) => {
                    if (obj.error) return ""
                    else return common.checkbox(obj, common, value, config)
                },
            },
            { id: "error", maxWidth:250, header: { text: `${_("status")} <span class='webix_icon wxi-download' title='excel'></span>`, css: "header" }, css: { color: "red" }, adjust: true },
            { id: "form", header: { text: _("form"), css: "header" }, adjust: true },
            { id: "serial", header: { text: _("serial"), css: "header" }, adjust: true },
            { id: "seq", header: { text: _("seq"), css: "header" }, adjust: true },
            { id: "c0", header: { text: _("numbcont"), css: "header" }, adjust: true },
            { id: "rea", header: { text: _("reason"), css: "header" }, adjust: true, fillspace:true },
        ]
        if(ENT!="hlv") cols.push(
            { id: "ref", header: { text: _("adj.ref"), css: "header" }, adjust: true },
            { id: "rdt", header: { text: _("adj.rdt"), css: "header" }, adjust: true, format:webix.i18n.dateFormatStr }
        )
        let grid = {
            view: "datatable",
            resizeColumn:true,
            id: "can:xlsgrid",
            columns: cols,
            onClick: {
                "wxi-download": function (e, id) {
                    let arrcol = [
                        { id: "error", maxWidth:250, header: { text: `${_("status")}`, css: "header" }, exportType:"string" },
                        { id: "form", header: { text: _("form"), css: "header" }, exportType:"string" },
                        { id: "serial", header: { text: _("serial"), css: "header" }, exportType:"string" },
                        { id: "seq", header: { text: _("seq"), css: "header" }, exportType:"string" },
                        { id: "c0", header: { text: _("numbcont"), css: "header" }, exportType:"string" },
                        { id: "rea", header: { text: _("reason"), css: "header" }, exportType:"string" },
                        { id: "ref", header: { text: _("adj.ref"), css: "header" }, exportType:"string" },
                        { id: "rdt", header: { text: _("adj.rdt"), css: "header" }, exportType:"string" }
                    ]
                    if (this.count()) webix.toExcel(this, { columns:arrcol, ignore: { "chk": true } })
                }
            }
        }
        let form = {
            view: "form",
            elements: [
                {
                    cols: [
                        { css: "right", cols: [{ gravity: 0 }, { view: "list", id: "can:filelist", type: "uploader", autoheight: true, borderless: true }] },
                        { view: "button", type: "icon", icon: "mdi mdi-download", label: _("file_sample"), click: () => { window.location.href = `temp/can.xls${ENT!='tlg'?'x':''}` }, width: 100 },
                        { id: "can:btnwcan", view: "button", type: "icon", icon: "mdi mdi-content-save", label: _("wait_cancel"), width: 100 },
                        { view: "button", id: "can:btnread", type: "icon", icon: "mdi mdi-file-upload-outline", label: _("file_read"), width: 100 },
                        { view: "uploader", id: "can:file", multiple: false, autosend: false, link: "can:filelist", accept: "application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", upload: "api/inv/upload", type: "icon", icon: "mdi mdi-file-find-outline", label: _("file_select"), width: 100, formData: () => { return { type: "CAN" } } },

                    ]
                }
            ]
        }

        return {
            view: "window",
            id: "can:xlswin",
            height: 600,
            width: 800,
            position: "center",
            resize: true,
            modal: true,
            head: {
                view: "toolbar", height: 35, css: 'toolbar_window',
                cols: [
                    { view: "label", label: _("import_excel") },
                    { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => { this.getRoot().hide() } }
                ]
            },
            body: { rows: [grid, form] }
        }
    }
    show() {
        this.getRoot().show()
        $$("can:file").setValue(null)
    }
    ready(v, urls) {
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
    init() {
        const uploader = $$("can:file"), grid = $$("can:xlsgrid")
        webix.extend(grid, webix.ProgressBar)
        $$("can:btnread").attachEvent("onItemClick", () => {
            if (grid.getHeaderContent("chk1")) grid.getHeaderContent("chk1").uncheck()
            grid.clearAll()
            const files = uploader.files, file_id = files.getFirstId()
            if (file_id) {
                grid.showProgress()
                try {
                    uploader.send(rs => {
                        try {
                            if (rs.err) {
                                if(typeof rs.err == "string") webix.message(rs.err, "error")
                                $$("can:btnwcan").disable()
                            }
                            else {
                                $$("can:btnwcan").enable()
                            }
                            grid.parse(rs.data)   
                        } catch (error) {
                            webix.message(_("wrong_input_type"), "error")
                        } finally {
                            grid.hideProgress()
                        }
                    })
                    webix.message(_("send_data_ok_pls_check_err_msg"))
                } catch (error) {
                    grid.hideProgress()
                }
            }
            else webix.message(_("file_required"))
            $$("can:xlsgrid").adjustRowHeight()
        })

        grid.attachEvent("onAfterLoad", function () {
            $$("can:xlsgrid").adjustRowHeight()
        })

        $$("can:btnwcan").attachEvent("onItemClick", () => {
            let rows = grid.serialize(), arr = []
            for (let row of rows) {
              if (row && row.chk && !row.error) arr.push(row)
            }
            if (arr.length < 1) {
              webix.message(_("invoice_must_select"))
              return false
            }
            grid.showProgress()
            webix.ajax().post("api/inv/xls/wcan", { invs: arr }).then(result => {
              const json = result.json()
              const save = json.save
              const data = json.data
            //   const obj = json.arr
            //   for (const kq of obj) {
            //     let row = arr.find(x => x.id == kq.id)
            //     let record = grid.getItem(row.id)
            //     record["error"] = record["error"] ? record["error"] + ", " + kq.error : kq.error
            //   }
            //   grid.refresh()
              grid.parse(data)
              if(save) webix.message(`${_("cancel")} ${data.length} ${_("invoice")}`) 
              else webix.message(_("some_invoice_not_saved"),"error")
              $$("can:xlsgrid").adjustRowHeight()
              grid.hideProgress()
            }).catch(()=>{grid.hideProgress()})
        })
    }
}