import { JetView } from "webix-jet"
import { LinkedInputs2, filter, d2s, gridnf0, ISTATUS2, PAYMETHOD, ITYPE, CARR, ROLE, ENT, RPP, setConfStaFld, OU } from "models/util"
import Iwin from "views/inv/iwin"
import Ecwin from "views/inv/ecwin"
const size = ENT == 'hlv' ? 100 : 10
const ISTATUSCANCEL = [
    { id: "3", value: _("invstatus_3") },
    // { id: "2", value: _("invstatus_2") },
    { id: "6", value: _("invstatus_6") }
]
const configs = (type) => {
    let lang = webix.storage.local.get("lang")

    webix.ajax().get(`api/sea/afbt?id=${type}`).then(r=>{
        let datasr = r.json()
        let removeDuplicates = (array, key) => {
            let lookup = new Set();
            return array.filter(obj => obj[key] && !lookup.has(obj[key]) && lookup.add(obj[key]));
        }
        let arr = [{id: "*", value: _("all") }, ...datasr]

        $$("form").getList().clearAll()
       
        $$("form").getList().parse(removeDuplicates(arr,'id'))
        $$("form").setValue("*")
    })

    webix.ajax().get(`api/seq/asbf?id=${$$("form").getValue()}`).then(r=>{
        let datasr = r.json()
        let removeDuplicates = (array, key) => {
            let lookup = new Set();
            return array.filter(obj => obj[key] && !lookup.has(obj[key]) && lookup.add(obj[key]));
        }
        let arr = [{id: "*", value: _("all") }, ...datasr]

        $$("serial").getList().clearAll()
       
        $$("serial").getList().parse(removeDuplicates(arr,'id'))
        $$("serial").setValue("*")

    })

    $$("form").attachEvent("onChange",()=>{
        webix.ajax().get(`api/seq/asbf?id=${$$("form").getValue()}`).then(r=>{
            let datasr = r.json()
            let removeDuplicates = (array, key) => {
                let lookup = new Set();
                return array.filter(obj => obj[key] && !lookup.has(obj[key]) && lookup.add(obj[key]));
            }
            let arr = [{id: "*", value: _("all") }, ...datasr]
    
            $$("serial").getList().clearAll()
           
            $$("serial").getList().parse(removeDuplicates(arr,'id'))
            $$("serial").setValue("*")
    
        })
    })
   
    webix.ajax().post(`api/cus`,{itype:type,lang:lang}).then(result => {
        let cols = result.json(), len = cols.length, grid = $$("cancel:grid")
        let arr = grid.config.columns.filter(item => !CARR.includes(item.id))
        if (len > 0) {
            for (const col of cols) {
                // arr.push({ id: col.id, header: { text: col.label, css: "header" }, adjust: true })
                let obj = { id: col.id, header: { text: col.label, css: "header" }, adjust: true }
                if (col.view == "datepicker") obj.format = webix.i18n.dateFormatStr
                if(col.suggest) obj.collection=col.suggest.data
                arr.push(obj)
            }
        }
        grid.config.columns = arr
        grid.refreshColumns()
        let r4 = $$("cancel:r4")
        r4.removeView("cancel:r5")
        r4.removeView("cancel:r6")
        if (len > 0) {
            if (len <= 5) {
                while (len < 5) {
                    cols.push({})
                    len++
                }
                r4.addView({ id: "cancel:r5", cols: cols }, 0)
                r4.addView({ id: "cancel:r6", hidden: true }, 1)
            }
            else {
                while (len < 10) {
                    cols.push({})
                    len++
                }
                r4.addView({ id: "cancel:r5", cols: cols.splice(0, len / 2) }, 0)
                r4.addView({ id: "cancel:r6", cols: cols }, 1)
            }
        }
        else {
            r4.addView({ id: "appr:r5", hidden: true }, 0)
            r4.addView({ id: "appr:r6", hidden: true }, 1)
        }

        
        //HLV onChange chon ServCode [c2:servCode, c7:servContent, c8:servName]
        if (ENT=="hlv") {
            // ["c7","c8"].map((e)=>$$(e).disable())
            $$("c2").attachEvent("onChange", (newv, oldv) => {
                if (!newv) ["c7","c8"].map((e)=>$$(e).setValue())
                webix.ajax(`api/kat/payments`).then(result => {
                    let json = result.json(), val = $$("c2").getValue()
                    let arr = json.filter((a)=>{return a.name == val})
                    if(arr.length>0) {
                        let item = arr[0], des = JSON.parse(item.des), name = des.servicename, content = des.servicecontent
                        $$("c7").setValue(content)
                        $$("c8").setValue(name)
                    }
                })
            })
        }
        //
    })
}
const statuschange = (newv) => {
    let lblok, lblcancel
    if (newv == "6") {
        lblok = _("approve_cancel") //4
        lblcancel = _("reject_cancel")//3
        if ((ENT == "hlv") || (ENT == "yusen")) {
            if (!ROLE.includes('PERM_INVOICE_CANCEL_WAIT')) {
                $$("cancel:btnok").show()
            }
            if (!ROLE.includes('PERM_INVOICE_CANCEL_WAIT')) {
                $$("cancel:btncancel").show()
                $$("cancel:btnok").hide()
            }
        }
        if (["hdb", "ssi", "vib"].includes(ENT)) {
            if (!ROLE.includes('PERM_INVOICE_CANCEL_WAIT')) {
                $$("cancel:btnok").show()
            }
            if (!ROLE.includes('PERM_INVOICE_CANCEL_WAIT')) {
                $$("cancel:btncancel").show()
                $$("cancel:btnok").hide()
            } else {
                if (["hdb", "ssi", "vib"].includes(ENT)) $$("cancel:btncancel").show()
            }
        }
        if (ENT == "dtt") $$("cancel:btncancel").hide()
        if (ENT == "dtt") $$("cancel:btnok").show()
        //onprem phan quyen chi tiet
        if (onpremcheck){
            if (ROLE.includes('PERM_INVOICE_CANCEL_WAIT')) {
                $$("cancel:btncancel").enable()
            }
            if (ROLE.includes('PERM_INVOICE_CANCEL_APPR')) {
                $$("cancel:btnok").enable()
            }
        } 



    } else {
        lblok = _("wait_cancel") //6
        lblcancel = _("cancel") //4
        if (["hlv", "yusen", "hdb", "ssi", "vcm","mzh", "vib"].includes(ENT)) {
            if (!ROLE.includes('PERM_INVOICE_CANCEL_WAIT')) {
                $$("cancel:btnok").hide()
            }
            if (!ROLE.includes('PERM_INVOICE_CANCEL_WAIT')) {
                $$("cancel:btnok").show()
                $$("cancel:btncancel").hide()
            }
            else {
                if (["hdb", "ssi",'mzh', "vib"].includes(ENT)) {
                    $$("cancel:btncancel").hide()
                    // $$("cancel:btnok").show()
                }
            }
        }
        if (ENT == "dtt" || ENT == "vcm") {
            if (ROLE.includes('PERM_INVOICE_CANCEL_WAIT')) {
                $$("cancel:btncancel").show()
            }
        }

        if (ENT == "dtt" && newv == "2") $$("cancel:btnok").hide()
        else {
            if (!ROLE.includes('PERM_INVOICE_CANCEL_WAIT')) {
                // $$("cancel:btnok").hide()
            } else {
                // $$("cancel:btnok").show()
            }

        }
        if (onpremcheck){
            if (ROLE.includes('PERM_INVOICE_CANCEL_WAIT')) {
                $$("cancel:btncancel").enable()
            }
            if (ROLE.includes('PERM_INVOICE_CANCEL_APPR')) {
                $$("cancel:btnok").enable()
            }
        } 



    }
    $$("cancel:btnok").define("label", lblok)
    $$("cancel:btncancel").define("label", lblcancel)
    $$("cancel:btnok").refresh()
    $$("cancel:btncancel").refresh()

}
export default class CancelView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        webix.protoUI({ name: "dependent2", $cssName: "richselect" }, LinkedInputs2, webix.ui.richselect)
        webix.proxy.appr = {
            $proxy: true,
            load: function (view, params) {
                let obj = $$("cancel:form").getValues()
                Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                if (!params) params = {}
                params.filter = obj
                return webix.ajax(this.source, params)
            }
        }
        let pager = { view: "pager", id: "cancel:pager", size: size, template: "{common.first()}{common.prev()}{common.pages()}{common.next()}{common.last()}",
            on: {
                onBeforePageChange:function(id){
                    let grida = $$("cancel:grid")
                    let flag = false
                    let size = $$("cancel:pager").config.size
                    let page = grida.getPage()
                    for (let i=0; i<size; i++){
                        let id = grida.getIdByIndex(size*page+i)
                        if(id){
                            if(grida.getItem(id) && grida.getItem(id).chk){
                                flag = true
                            }
                        }
                    }
                    if($$("cancel:grid").getHeaderContent("chk1") && flag)
                        $$("cancel:grid").getHeaderContent("chk1").uncheck()
                }
            }
        }
        //let recpager = { view: "combo", id: "recpager", maxWidth: 70, tooltip: _("row_per_page"), value: 10, options: RPP }
        let columns = [
            { id: "vie", header: "", width: 35, template:`<span class='webix_icon wxi-search', title='${_("view")}'></span>`},
            {
                id: "chk", header: { content: "masterCheckbox", css: "center", contentId:"chk1" }, width: 35, css: "center", template: (obj, common, value, config) => {
                    
                    if (obj.cid && (obj.adjtyp == 2 || (obj.status == 3 && !obj.adjtyp))||obj.adjtyp == 2 || obj.adjdes) return ""
                    else return common.checkbox(obj, common, value, config)
                }
            },
            { id: "id", header: { text: "ID", css: "header" }, sort: "server", css: "right", width: 45 },
            { id: "idt", header: { text: _("invoice_date"), css: "header" }, sort: "server", format: d2s, adjust: true },
            { id: "form", header: { text: _("form"), css: "header" }, sort: "server", adjust: true },
            { id: "serial", header: { text: _("serial"), css: "header" }, sort: "server", adjust: true },
            { id: "seq", header: { text: _("seq"), css: "header" }, sort: "server", adjust: true },
            { id: "btax", header: { text: _("taxcode"), css: "header" }, sort: "server", adjust: true },
            { id: "bname", header: { text: _("bname2"), css: "header" }, sort: "server", adjust: true },
            { id: "buyer", header: { text: _("buyer"), css: "header" }, sort: "server", adjust: true },
            { id: "baddr", header: { text: _("address"), css: "header" }, adjust: true },
            { id: "sumv", header: { text: _("sumv"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.format },
            { id: "vatv", header: { text: "VAT", css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.format },
            { id: "totalv", header: { text: _("totalv"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.format },
            { id: "curr", header: { text: _("curr"), css: "header" }, sort: "server", adjust: true },
            { id: "sec", header: { text: _("search_code"), css: "header" }, format: webix.template.escape, sort: "server", adjust: true },
            { id: "ou", header: { text: _("ou"), css: "header" }, sort: "server", collection: "api/cat/nokache/ou", adjust: true },
            { id: "uc", header: { text: _("creator"), css: "header" }, sort: "server", adjust: true },
            { id: "ic", header: { text: "IC", css: "header" }, sort: "server", adjust: true },
            { id: "note", header: { text: _("note"), css: "header" }, sort: "server", adjust: true },
        ]
        
        if (ENT == 'hlv') {
            columns.push({ id: "adjdes", header: { text: _("replace"), css: "header" }, format: webix.template.escape, sort: "server", adjust: true })
            columns.push({ id: "cde", header: { text: _("replaced"), css: "header" }, sort: "server", adjust: true })
        }
        else {
            columns.push({ id: "adjdes", header: { text: _("repadj"), css: "header" }, format: webix.template.escape, sort: "server", adjust: true })
            columns.push({ id: "cde", header: { text: _("repadjed"), css: "header" }, sort: "server", adjust: true })
        }

        let grid = {
            view: "datatable",
            id: "cancel:grid",
            scroll: "xy",
            resizeColumn: true,
            editable: false,
            datafetch: size,
            pager: "cancel:pager",
            on: {
                onItemClick: function (id) {
                    let row = id.row, item = this.getItem(row)
                    if (!((item.cid && (item.adjtyp == 2 || (item.status == 3 && !item.adjtyp))) || item.adjtyp == 2) || item.adjdes) {
                        item.chk = !item.chk
                    }
                    this.refresh(row)
                }
            },
            onClick: {
                "wxi-search": function (e, r) {
                    if(ROLE.includes('PERM_INVOICE_CANCEL_VIEW')){
                        const id = r.row
                        webix.ajax(`api/app/htm/${id}`).then(result => {
                            // const json = result.json()
                            // const req = createRequest(json)
                            //jsreport.renderAsync(req).then(res => {
                            //$$("iwin:ipdf").define("src", res.toDataURI())
                            let pdf = result.json()
                            let b64 = pdf.pdf
                            const blob = dataURItoBlob(b64);
    
                            var temp_url = window.URL.createObjectURL(blob);
                            $$("iwin:ipdf").define("src", temp_url)
                            $$("iwin:win").show()
                            // webix.ajax().post("api/seek/report", req).then(res => {
                            //     let pdf = res.json()
                            //     let b64 = pdf.pdf
                            //     const blob = dataURItoBlob(b64);
    
                            //     var temp_url = window.URL.createObjectURL(blob);
                            //     $$("iwin:ipdf").define("src", temp_url)
                            //     $$("iwin:win").show()
                            // })
                        })
                    }else webix.message(_("no_authorization"), "error")
                }
            },
            columns: columns
        }

        let eForm = 
        [
            {
                cols: [
                    { id: "fd", name: "fd", label: _("fd"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr },
                    { id: "td", name: "td", label: _("td"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr },
                    { id: "status", name: "status", label: _("status"), view: "richselect", options: ISTATUSCANCEL, value: "6"},
                    { id: "paym", name: "paym", label: _("payment_method"), view: "combo", options: PAYMETHOD(this.app) },
                    {
                        cols: [
                            { id: "curr", name: "curr", label: _("currency"), view: "combo", suggest: { url: "api/cat/kache/currency" }, gravity: 2 },
                            { name: "ic", label: "IC", view: "text", labelWidth: 25, attributes: { maxlength: 36 }, placeholder: "IC" }
                        ]
                    }
                ]
            },
            {
                cols: [
                    { id: "type", name: "type", label: _("invoice_type"), view: "combo", placeholder:_("choose_inv_type"), options: ITYPE(this.app) },//richselect
                    { id: "form", name: "form", label: _("form"), view: "dependent2", options: [], master: "type", dependentUrl: "api/seq/afbt?id=", relatedView: "serial", relatedAction: "enable" },
                    { id: "serial", name: "serial", label: _("serial"), view: "dependent2", options: [], master: "form", dependentUrl: "api/seq/asbf?id=", disabled: true },
                    { name: "seq", label: _("seq"), view: "text", attributes: { maxlength: 8 }, validate: v => { return !v || /^[0-9]{1,8}$/.test(v) }, invalidMessage: "Số không hợp lệ", placeholder: _("seq") },
                    { id: "ou", name: "ou", label: _("ou"), view: "combo", suggest: { url: "api/cat/nokache/ou", filter: filter, relative: "left" } }
                ]
            },
            { //Row 3
                cols: [
                    ENT != "hdb" 
                        ? { name: "btax", label: _("taxcode"), view: "text", attributes: { maxlength: 14 }, placeholder: _("taxcode") }
                        : { name: "btax", label: _("taxcode"), view: "text", attributes: { maxlength: 25 }, placeholder: _("taxcode") },
                    { name: "bname", label: _("bname2"), view: "text", attributes: { maxlength: 50 }, placeholder: _("bname2") },
                    { name: "buyer", label: _("buyer"), view: "text", attributes: { maxlength: 50 }, placeholder: _("buyer") },
                    { name: "sec", label: _("search_code"), view: "text", attributes: { maxlength: 10 }, placeholder: _("search_code") },
                    { id: "invs:user", name: "user", label: _("creator"), view: "text", attributes: { maxlength: 24 }, placeholder: _("creator") }//richselect
                ]
            },
            { //Row 3
                cols: [
                    { id: "invs:bacc", name: "bacc", label: _("bacc"), view: "text", placeholder: _("bacc"), attributes: { maxlength: 24 }},{},{},{},{}
                ]
            },
            {
                id: "cancel:r4", rows: [{ id: "cancel:r5", hidden: true }, { id: "cancel:r6", hidden: true }]
            },
            { //Row 3
                cols: [
                    {},{},{},{},{ view: "button", id: "cancel:btnsearch", type: "icon", icon: "wxi-search", label: _("search"), width: 100 }
                ]
            }

        ]
        let form = {
            view: "form",
            id: "cancel:form",
            padding: 3,
            margin: 3,
            elementsConfig: { labelWidth: 85 },
            elements:eForm
            , rules: {
                fd: webix.rules.isNotEmpty,
                td: webix.rules.isNotEmpty,
                $obj: function (data) {
                    const fd = data.fd, td = data.td
                    if (fd > td) {
                        webix.message(_("date_invalid_msg"))
                        return false
                    }
                    const dtime = (new Date(td)).getTime() - (new Date(fd)).getTime()
                    if (ENT == 'vcm') {
                        if (String(fd).substr(0, 7) != String(td).substr(0, 7)) {
                            webix.message(_("fd_td_in_month"))
                            return false
                        }
                    }
                    if (dtime > 31622400000) {
                        webix.message(_("dt_search_between_year"))
                        return false
                    }
                    return true
                }

            }
        }
        let recpager = { view: "combo", id: "recpager", maxWidth: 70, tooltip: _("row_per_page"), value: size, options: RPP }
        let title_page = { view: "label", label:`<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("invoice_cancel")}</span>`}
        return {
            paddingX: 2, rows: [title_page, form, grid, {
                cols: [
                    recpager,pager, { id: "appr:countinv", view: "label" }, {},
                    { view: "button", id: "cancel:btnexcel", type: "icon", icon: "mdi mdi-file-excel", label: "Excel", width: 80, hidden: true },
                    { view: "button", id: "cancel:btnok", type: "icon", icon: "mdi mdi-check-circle-outline", label: _("approve_cancel"), css: "webix_danger", width: 100, disabled: true },
                    { view: "button", id: "cancel:btncancel", type: "icon", icon: "mdi mdi-cancel", label: _("reject_cancel"), css: "webix_danger", width: 100, disabled: true }
                ]
            }]
        }
    }
    ready(v, urls) {
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        if(ENT=="vcm") {
            $$("status").setValue("3")
        }
        
        //
    }
    init() {
        if (!ROLE.includes('PERM_INVOICE_CANCEL')) {
            this.show("/top/home")
            window.location.replace(`/${homepage}`)
            return
          }
        $$("lang:lang").attachEvent("onChange", function(newv, oldv){
          
            //trungpq10 sua song ngu{
            const type_inv=(ITYPE(this.app))[0].id
            let lang = newv
            configs(type_inv)
         //}
        });
        this.Iwin = this.ui(Iwin)
        this.Ecwin = this.ui(Ecwin)
        let date = new Date(), firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1), grid = $$("cancel:grid"), form = $$("cancel:form")
        const all = { id: "*", value: _("all") }
        const excelGrid = $$("can:xlsgrid")
        if (!["hdb", "vcm"].includes(ENT)) $$("fd").setValue(firstDay)
        else $$("fd").setValue(new Date())
        $$("td").setValue(date)
        $$("type").getList().add(all, 0)
        $$("curr").getList().add(all, 0)
        $$("paym").getList().add(all, 0)
        $$("ou").getList().add(all, 0)
        $$("curr").setValue("*")
        $$("paym").setValue("*")
        if (ENT=="hdb" ||ENT=="sgr" ) $$("ou").setValue(OU)
        else $$("ou").setValue("*")
        
        $$("iwin:btnxml").hide()
        $$("iwin:btnsignpdf").hide()
        if ((["hdb"].includes(ENT))||(ENT=="hlv"&&ROLE.includes('PERM_INVOICE_CANCEL_WAIT'))  ||(ENT=="yusen"&&ROLE.includes('PERM_INVOICE_CANCEL_WAIT'))) $$("cancel:btnexcel").show()
        if (ROLE.includes('PERM_INVOICE_CANCEL_WAIT')) {
            if (ENT != "dtt")  $$("cancel:btncancel").enable()
        }
        if (ROLE.includes('PERM_INVOICE_CANCEL_APPR')) {
            $$("cancel:btnok").enable()
            if (ENT == "hdb") $$("cancel:btncancel").enable()
        }


        $$("type").attachEvent("onChange", (newv) => {
            configs(newv)
        })

        $$("type").setValue("01GTKT")

        
        $$("status").attachEvent("onChange", (newv) => {
            statuschange(newv)
        })


        const search = () => {
            if (form.validate()) {
                if ($$("cancel:grid").getHeaderContent("chk1")) $$("cancel:grid").getHeaderContent("chk1").uncheck()
                grid.clearAll()
                grid.loadNext($$("recpager").getValue(), 0, null, "appr->api/app", true)
            }
        }

        const exec = (ok) => {
            let lang = $$("lang:lang").getValue()
            // let items = grid.serialize(), arr = []
            // for (let item of items) {
            //     if (item && item.chk) {
            //         arr.push(item.id)
            //         if(ENT == "sgr" && ok == false && item.status !=6 ){
            //             const dtime = new Date().getMonth()
            //             const oldtime=new Date(item.idt).getMonth()
            //             if (dtime != oldtime) {
            //                 webix.message(_("invoice_date_select"), "error")
            //                 return false
            //             }
            //         }
            //     }
            // }
            let grida = $$("cancel:grid"), arr = []
            let size = $$("cancel:pager").config.size
            let page = grida.getPage()
            for (let i=0; i<size; i++) {
                let id = grida.getIdByIndex(size*page+i)
                if(id){
                    let row = grida.getItem(id)
                    if (row.cid && (row.adjtyp == 2 || (row.status == 3 && !row.adjtyp))) continue
                    if(row.adjdes) continue
                    if( row && row.chk ){
                        arr.push(id)
                        if(ENT == "sgr" && ok == false && grida.getItem(id).status !=6 ) {
                            const dtime = new Date().getMonth()
                            const oldtime=new Date(row.idt).getMonth()
                            if (dtime != oldtime) {
                                webix.message(_("invoice_date_select"), "error")
                                return false
                            }
                        }
                    }
                }
            }
            if (arr.length < 1) {
                webix.message(_("invoice_must_select"), "error")
                return false
            }
            const status = $$("status").getValue()
            let statusTo = [""].includes(ENT) ? 2 : 3
            const ns = ok ? (status == "6" ? 4 : 6) : (status == "6" ? statusTo : 4)
            let tt
            if (ns == 6) tt = _("wait_cancel") //"chờ hủy"
            else if (ns == 4) tt = _("cancel") //"hủy"
            else tt = _("inv_status_active") //"hiệu lực"
            webix.confirm(`${_("change_status_inv_confirm")} ${tt}?`, "confirm-warning").then(() => {
                webix.ajax().post("api/ist", { ids: arr, os: status, ns: ns }).then(result => {
                    const rows = result.json()
                    let msgtoinform = String(String(_("change_status_inv_result")).replace("_tt_", tt)).replace("_rows_", String(rows))
                    webix.message(msgtoinform, "success")
                    search()
                })
            })

        }

        $$("cancel:btnexcel").attachEvent("onItemClick", () => {
            this.Ecwin.show()
            excelGrid.clearAll()
        })
        $$("recpager").attachEvent("onChange", () => {
            $$("cancel:pager").config.size = $$("recpager").getValue()
            if (form.validate()) search()
        })
        $$("cancel:btnsearch").attachEvent("onItemClick", () => search())
        $$("cancel:btnok").attachEvent("onItemClick", () => exec(true))
        $$("cancel:btncancel").attachEvent("onItemClick", () => exec(false))

        grid.attachEvent("onBeforeLoad", function () {
            this.showOverlay(_("loading"))
        })

        grid.attachEvent("onAfterLoad", function () {
            if (!this.count()) this.showOverlay(_("notfound"))
            else this.hideOverlay()
        })

        grid.attachEvent("onAfterLoad", function () {
            let self = this, count = self.count()
            $$("appr:countinv").setValue(`${_("found")} ${count}`)
            if (count) self.hideOverlay()
            else self.showOverlay(_("notfound"))
        })
    }
}