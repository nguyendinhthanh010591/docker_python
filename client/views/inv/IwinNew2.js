import { JetView } from "webix-jet"
import { C0,ITYPE,LinkedInputs3,filter,LinkedInputsOUSERIAL } from "models/util"
const LG = [
    { id: "0", value: _("month") },
    { id: "1", value: _("day")},
    { id: "2", value: _("btnsinge")}
  
   
]
export default class IwinNew2 extends JetView {

    
        config() {
            _ = this.app.getService("locale")._
           
            webix.protoUI({ name: "dependent", $cssName: "combo" }, LinkedInputsOUSERIAL, webix.ui.combo)
            webix.protoUI({ name: "dependent1", $cssName: "richselect" }, LinkedInputsOUSERIAL, webix.ui.combo)
            webix.protoUI({ name: "dependent2", $cssName: "richselect" }, LinkedInputs3, webix.ui.richselect)
            let eFormn =
            [
                { id: "iou", name: "iou", label: _("bname"), view: "combo",labelWidth: 100, suggest: { url: "api/ous/bytokenou", filter: filter, relative: "bottom" },required: true },
                { id: "itype", name: "itype", label: _("invoice_type"), view: "combo",labelWidth: 100, options: ITYPE(this.app), required: true },//gravity: 1.3,
                { id: "iform", name: "iform", label: _("form"), view: "dependent",labelWidth: 100, options: [], master: "itype",master1: "iou", dependentUrl: "api/sea/fbtOu?id=", relatedView: "iserial", relatedAction: "enable", required: true },
                { id: "iserial", name: "iserial", label: _("serial"), view: "dependent",labelWidth: 100, options: [], master: "iform", dependentUrl: "api/sea/sbfOu?id=", required: true },
               { id: "tranN:idt", name: "idt1", label: _("invoice_date"), labelWidth: 100,view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr, required: true }
           
                ,
                { id: "tranN:trancol", name: "trancol1", label: _("group_method"),labelWidth: 100, view: "combo", options: LG, required: true ,value: C0,disabled:true}
                ,
                    {//R2
                    cols: [
                        { view: "button", id: "tranN:btnsave", type: "icon", icon: "mdi mdi-content-save", label: _("invoice_issue")},
                        { view: "button", id: "tranN:btnsaveAll", type: "icon", icon: "mdi mdi-content-save", label: _("create_inv_a"),hidden:true},
                        { view: "button", id: "tranN:btnexit", type: "icon", icon: "mdi mdi-exit-to-app", label: _("exit"),  click: () => { this.getRoot().hide() } }
                    ]
                }
                
            ]
            return {
                view: "window",
                id: "tranN:win",
                width:500,
                position: "center",
                resize: true,
                modal: true,
                head: {
                    view: "toolbar", height: 40,
                    css: 'toolbar_window',
                    cols: [
                        { view: "label", id:"tranN_lbl",label: _("invoice_issue") },
                        { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => { this.getRoot().hide() } }
                    ]
                }
                ,
                body: {
                    view: "form",
                    id: "tranN:form2",
                    padding: 3,
                    margin: 3,
                    elementsConfig: { labelWidth: 150 },
                    elements: eFormn
                    , rules: {
                        idt1: webix.rules.isNotEmpty,
                        trancol1: webix.rules.isNotEmpty,
                        iform: webix.rules.isNotEmpty,
                        itype: webix.rules.isNotEmpty,
                        iserial: webix.rules.isNotEmpty,
                      
                    },
                    on: {
                        onAfterValidation: (result, value) => {
                            if (!result) {
                                if (JSON.stringify(value.valueOf(0)) !== JSON.stringify({})) webix.message(_("required_msg"))
                            }
                        }
                    }
                }
            }
        }
        show() {
            this.getRoot().show()
        }
}
