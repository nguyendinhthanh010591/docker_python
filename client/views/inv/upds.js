import { JetView } from "webix-jet";
import { LinkedInputs3, gridnf, PAYMETHOD, ITYPE, ENT, setConfStaFld } from "models/util";
const config = (grid, type) => {
    let arr = [
        { id: "chk", header: { content: "masterCheckbox", css: "center" }, adjust: "header", css: "center", template: "{common.checkbox()}" },
        { id: "ic", header: { text: _("invoice_code"), css: "header" } },
        // { id: "idt", header: { text: _("date"), css: "header" }, format: webix.i18n.dateFormatStr },
        { id: "form", header: { text: _("form"), css: "header" } },
        { id: "serial", header: { text: _("serial"), css: "header" } },
        { id: "seq", header: { text: _("seq"), css: "header" } },
        { id: "btax", header: { text: _("taxcode"), css: "header" } },
        { id: "bname", header: { text: _("bname"), css: "header" } },
        { id: "buyer", header: { text: _("buyer"), css: "header" } },
        { id: "baddr", header: { text: _("address"), css: "header" } },
        { id: "btel", header: { text: _("tel"), css: "header" } },
        { id: "bmail", header: { text: "Mail", css: "header" } },
        { id: "bacc", header: { text: _("account"), css: "header" } },
        { id: "bbank", header: { text: _("bank"), css: "header" } },
        { id: "paym", header: { text: _("payment_method"), css: "header" }, collection: PAYMETHOD(this.app) },
        { id: "curr", header: { text: _("currency"), css: "header" } },
        { id: "exrt", header: { text: _("exchange_rate"), css: "header" }, css: "right", format: gridnf.format },
        { id: "sum", header: { text: _("sumv"), css: "header" }, css: "right", format: gridnf.format }
    ]
    if (ENT == "tlg") arr.push(
        { id: "rform", header: { text: _("rform"), css: "header" } },
        { id: "rserial", header: { text: _("rserial"), css: "header" } },
        { id: "rseq", header: { text: _("rseq"), css: "header" } },
        { id: "rref", header: { text: _("rref"), css: "header" } },
        { id: "rrdt", header: { text: _("rrdt"), css: "header" }, format: webix.i18n.dateFormatStr },
        { id: "rrea", header: { text: _("rrea"), css: "header" } },
    )
    if (type == "01GTKT") arr.push({ id: "vat", header: { text: "VAT", css: "header" }, css: "right", format: gridnf.format })
    arr.push({ id: "total", header: { text: _("totalv"), css: "header" }, css: "right", format: gridnf.format },
        { id: "word", header: { text: _("word"), css: "header" } },
        { id: "note", header: { text: _("note"), css: "header" } }
    )
    grid.clearAll()
    grid.config.columns = arr
    grid.refreshColumns()
}


export default class XlsView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        webix.protoUI({ name: "dependent", $cssName: "combo" }, LinkedInputs3, webix.ui.combo)
        webix.protoUI({ name: "dependent2", $cssName: "richselect" }, LinkedInputs3, webix.ui.richselect)
        const form = {
            view: "form",
            id: "xls:form",
            padding: 1,
            margin: 1,
            elementsConfig: { labelWidth: 65 },
            elements: [
                {
                    cols: [
                        { id: "type", name: "type", label: _("invoice_type"), view: "combo", options: ITYPE(this.app), required: true },//gravity: 1.3,
                        { id: "form", name: "form", label: _("form"), view: "dependent", options: [], master: "type", dependentUrl: "api/sea/fbt?id=", relatedView: "serial", relatedAction: "enable", required: true, width: 190 },
                        { id: "serial", name: "serial", label: _("serial"), view: "dependent2", options: [], master: "form", dependentUrl: "api/sea/sbf?id=", disabled: true, required: true, width: 150 },
                        { view: "button", type: "icon", icon: "mdi mdi-download", label: _("file_sample"), click: () => { window.location.href = `temp/update.xls${ENT!='tlg'?'x':''}` }, width: 85 },
                        { id: "xls:btnupd", view: "button", type: "icon", icon: "mdi mdi-content-save", label: _("update"), width: 85, disabled: true },
                        { id: "xls:btnread", view: "button", type: "icon", icon: "mdi mdi-file-upload-outline", label: _("file_read"), width: 85 },
                        {
                            id: "xls:file", view: "uploader", multiple: false, autosend: false, link: "xls:filelist", accept: "application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", upload: "api/inv/uplupd", type: "icon", icon: "mdi mdi-file-find-outline", label: _("file_select"), width: 85, formData: () => {
                                return { type: $$("type").getValue(), form: $$("form").getValue(), serial: $$("serial").getValue() }
                            }
                        },
                        { id: "xls:filelist", view: "list", type: "uploader", autoheight: true, borderless: true }
                    ]
                }
            ],
            rules: {
                type: webix.rules.isNotEmpty,
                form: webix.rules.isNotEmpty,
                serial: webix.rules.isNotEmpty
            }
        }

        const grid = {
            view: "datatable",
            id: "xls:grid",
            select: "row",
            resizeColumn: true,
        }

        return { paddingX: 2, rows: [form, grid] }
    }
    ready(v, urls) {
        $$("type").setValue("01GTKT")
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
    init() {
        let grid = $$("xls:grid"), uploader = $$("xls:file")
        webix.extend(grid, webix.ProgressBar)
        $$("type").attachEvent("onChange", (n) => { config(grid, n) })
        $$("xls:btnread").attachEvent("onItemClick", () => {
            if (!$$("xls:form").validate()) return false
            const files = uploader.files, file_id = files.getFirstId()
            if (file_id) {
                grid.clearAll()
                grid.showProgress()
                $$("xls:btnupd").disable()
                webix.delay(() => {
                    uploader.send(rs => {
                        if (rs.err) {
                            grid.hideProgress()
                            webix.message(rs.err, "error")
                        }
                        else if (rs.buffer) {
                            grid.hideProgress()
                            webix.message(_("err_excel"), "error")
                            const data = rs.buffer.data
                            const blob = new Blob([new Uint8Array(data, 0, data.length)])
                            webix.html.download(blob, "error.xlsx")
                        }
                        else {
                            try {
                                grid.parse(rs.data).then(() => {
                                    $$("xls:btnupd").enable()
                                })
                            } catch (err) {
                                webix.message(err.message, "error")
                            } finally {
                                grid.hideProgress()
                            }

                        }

                    })
                })
            }
            else webix.message(_("file_required"))
        })
        
        $$("xls:btnupd").attachEvent("onItemClick", () => {
            let rows = grid.serialize(), arr = []
            for (let row of rows) {
                if (row && row.chk) arr.push(row)
            }
            if (arr.length < 1) {
                webix.message(_("invoice_must_select"))
                return false
            }
            webix.delay(() => {
                grid.showProgress()
                $$("xls:btnupd").disable()
                webix.ajax().post("api/inv/xls/upds", { invs: arr }).then(result => {
                    let json = result.json()
                    webix.message(`${$$("xls:btnupd").config.label} ${json} ${_("invoice")}`, "success", 10000)
                    grid.hideProgress()
                }).catch(() => { grid.hideProgress() })
            })
        })
    }
}
