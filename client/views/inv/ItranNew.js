import { JetView } from "webix-jet"
import { LinkedInputs3,ITYPE} from "models/util";

export default class ItranNew extends JetView {

    
        config() {
            _ = this.app.getService("locale")._
            webix.protoUI({ name: "dependent", $cssName: "combo" }, LinkedInputs3, webix.ui.combo)
            webix.protoUI({ name: "dependent2", $cssName: "richselect" }, LinkedInputs3, webix.ui.richselect)
            let eFormn =
            [
                { id: "itype", name: "itype", label: _("invoice_type"), view: "combo",width:300, options: ITYPE(this.app), required: true },//gravity: 1.3,
                { id: "iform", name: "iform", label: _("form"), view: "dependent",width:300, options: [], master: "itype", dependentUrl: "api/sea/fbt?id=", relatedView: "serial", relatedAction: "enable", required: true },
                { id: "iserial", name: "iserial", label: _("serial"), view: "dependent2",width:300, options: [], master: "iform", dependentUrl: "api/sea/sbf?id=", required: true }
                ,
                    {//R2
                    cols: [
                        { view: "button", id: "tranN:btnsave", type: "icon", icon: "mdi mdi-content-save", label: _("invoice_issue")},
                      
                        { view: "button", id: "tranN:btnexit", type: "icon", icon: "mdi mdi-exit-to-app", label: _("exit"),  click: () => { this.getRoot().hide() } }
                    ]
                }
                
            ]
            return {
                view: "window",
                id: "tranN:win",
                width:500,
                position: "center",
                resize: true,
                modal: true,
                head: {
                    view: "toolbar", height: 40,
                    css: 'toolbar_window',
                    cols: [
                        { view: "label", id:"tranN_lbl",label: _("invoice_issue") },
                        { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => { this.getRoot().hide() } }
                    ]
                }
                ,
                body: {
                    view: "form",
                    id: "tranN:form2",
                    padding: 3,
                    margin: 3,
                    elementsConfig: { labelWidth: 50 },
                    elements: eFormn
                    , rules: {
                        itype: webix.rules.isNotEmpty,
                        iform: webix.rules.isNotEmpty,
                        iserial: webix.rules.isNotEmpty
                      
                    },
                    on: {
                        onAfterValidation: (result, value) => {
                            if (!result) {
                                if (JSON.stringify(value.valueOf(0)) !== JSON.stringify({})) webix.message(_("required_msg"))
                            }
                        }
                    }
                }
            }
        }
        show() {
            this.getRoot().show()
        }
}
