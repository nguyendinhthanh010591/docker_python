import { JetView } from "webix-jet"
import { getvrn, intf, gridna, gridnp, gridnq, textna, expire, VAT1, BTYPE, MAX, PERIODS, CATEGORIES, VRNLIST, gridnauto, vrn2vrt, vrt2vrn, textn2 } from "models/util"
import { toWords } from "models/n2w"
let parse = false, action, type, isvat

const new_form = () => {
    let grid = $$("bnv:grid"), form = $$("bnv:form")
    form.setValues({ idt: new Date(), curr: "VND", exrt: 1, paym: "TM/CK", type: type })
    let i = 0
    while (i < 3) {
        grid.add({ vrt: "10", vrn: "10%", quantity: 1 })
        i++
    }
    grid.clearValidation()
}

const reset_form = () => {
    let grid = $$("bnv:grid")
    if (isvat) {
        $$("r10").show()
        if (!grid.isColumnVisible("vrt")) grid.showColumn("vrt")
    }
    else {
        $$("r10").hide()
        if (grid.isColumnVisible("vrt")) grid.hideColumn("vrt")
    }
}

export default class BnvView extends JetView {
    config() {
       // _ = this.app.getService("locale")._
        let grid = {
            view: "datatable",
            id: "bnv:grid",
            editable: true,
            navigation: true,
            editaction: "click",
            resizeColumn: true,
            clipboard: "custom",
            multiselect: false,
            select: "cell",
            headerRowHeight: 33,
            columns: [
                { id: "del", header: [`<span class='webix_icon wxi-plus' title='${_("details_insert")}'></span>`], template: `<span class='webix_icon wxi-trash' title='${_("details_delete")}'></span>`, width: 35 },
                { id: "line", header: { text: _("stt"), css: "header" }, css: "right", adjust: true },
                {
                    id: "name", header: { text: _("catalog_items"), css: "header" }, adjust: true, fillspace: 3, editor: "text", suggest: {
                        view: "gridsuggest",
                        fitMaster: true,
                        body: {
                            url: "api/gns/fbn",
                            dataFeed: function (str) {
                                if (/\;$/.test(str)) return this.load(`${this.config.url}?filter[value]=${encodeURIComponent(str.substring(0, str.length - 1))}`, this.config.datatype)
                                else return
                            },
                            scroll: true,
                            autoheight: false,
                            autowidth: false,
                            autofocus: true,
                            yCount: 5,
                            columns: [
                                { id: "name", header: { text: _("gns_name"), css: "header" }, adjust: true },
                                { id: "code", header: { text: _("gns_code"), css: "header" }, adjust: true },
                                { id: "unit", header: { text: _("unit"), css: "header" }, adjust: true },
                                { id: "price", header: { text: _("price"), css: "header" }, css: "right", adjust: true, format: gridnp.format },
                                // { id: "vat", header: { text: _("vrt"), css: "header" }, adjust: true, collection: VAT }
                                { id: "vrn", header: { text: _("vrt"), css: "header" }, adjust: true, collection: VRNLIST },
                                { id: "vat", header: { text: _("vrt_code"), css: "header" }, adjust: true }
                            ]
                        },
                        on: {
                            onValueSuggest: (rec) => {
                                if (rec) {
                                    let row = $$("bnv:grid").getSelectedItem()
                                    if (row) {
                                        row.name = rec.name
                                        row.unit = rec.unit
                                        row.vrt = rec.vat
                                        if (rec.vrn) row.vrn = rec.vrn
                                        let price = rec.price
                                        if (webix.rules.isNumber(price)) {
                                            price = Number(price)
                                            row.price = price
                                            let quantity = row.quantity
                                            if (webix.rules.isNumber(quantity)) {
                                                row.amount = price * Number(quantity)
                                            }
                                        }
                                        row.code = rec.code
                                        $$("bnv:grid").updateItem(row.id, row)
                                    }
                                }
                            }
                        }
                    }
                },
                { id: "unit", header: { text: _("unit"), css: "header" }, adjust: true, editor: "text", suggest: { url: "api/cat/kache/unit" } },//, filter: filter
                { id: "price", header: { text: _("price"), css: "header" }, adjust: true, fillspace: 1, css: "right", editor: "text", inputAlign: "right", format: gridnp.format, editParse: gridnp.editParse, editFormat: gridnp.editFormat },
                { id: "quantity", header: { text: _("quantity"), css: "header" }, adjust: true, css: "right", editor: "text", inputAlign: "right", format: gridnq.format, editParse: gridnq.editParse, editFormat: gridnq.editFormat },
                { id: "vrn", header: { text: _("vrt"), css: "header" }, adjust: true, collection: VRNLIST, editor: "richselect" },
                { id: "vrt", header: { text: _("vrt_code"), css: "header" }, adjust: true, editor: "text", inputAlign: "right", format: gridnauto.format, editParse: gridnauto.editParse, editFormat: gridnauto.editFormat },
                { id: "perdiscount", header: { text: _("percent_discount"), css: "header" }, adjust: true, editor: "text", inputAlign: "right", format: gridna.format, editParse: gridna.editParse, editFormat: gridna.editFormat },
                { id: "amtdiscount", header: { text: _("amount_discount"), css: "header" }, editor: "text", inputAlign: "right", format: gridna.format, editParse: gridna.editParse, editFormat: gridna.editFormat },
                { id: "amount", header: { text: _("amount"), css: "header" }, adjust: true, fillspace: 1, css: "right", editor: "text", inputAlign: "right", format: gridna.format, editParse: gridna.editParse, editFormat: gridna.editFormat }
            ],
            onClick: {
                "wxi-trash": function (e, id) {
                    this.remove(id)
                    return false
                },
                "wxi-plus": function (e, id) {
                    let me = this
                    let obj = { quantity: 1 }
                    if (isvat) {
                        obj.vrt = "10"
                        obj.vrn = "10%"
                    }
                    me.add(obj)
                    me.clearValidation()
                    return false
                }
            },
            on: {
                onBeforeEditStart: function (editor) {
                    const column = editor.column
                    if (["vrt"].includes(column)) {
                        let row = this.getItem(editor.row)
                        if (column == "vrt") {
                            let vrt = vrn2vrt(row.vrn)
                            if (Number(vrt) >= 0) return false
                        }
                    }
                },
                onBeforeEditStop: function (val, editor) {
                    const column = editor.column
                    if (["price", "quantity", "vrn", "perdiscount", "amtdiscount"].includes(column)) {
                        const newv = val.value, oldv = val.old
                        if (newv != oldv) {
                            let row = this.getItem(editor.row), amount = 0, price = Number(row.price), quantity = Number(row.quantity)
                            let rprice = column == "price" ? newv : row.price, rquantity = column == "quantity" ? newv : row.quantity, amountPQ = rprice && rquantity ? rprice * rquantity : (row.amount||0) - (row.amtdiscount || 0), vat= (rprice && rquantity ? rprice * rquantity : (row.amount||0))*(row.vrt || 0)/100
                            if (column == "price") {
                                if (rquantity) amount = amountPQ + vat - (row.amtdiscount||0)
                            }
                            else if (column == "quantity") {
                                if (rprice || rprice === 0) amount = amountPQ + vat - (row.amtdiscount||0)
                            }
                            else if (column == "vrn") {
                                let vrt = vrn2vrt(newv)
                                row.vrt = vrt
                                if (Number(vrt) >=0) {
                                    vat= (rprice && rquantity ? rprice * rquantity : (row.amount||0))*(row.vrt || 0)/100
                                    amount = amountPQ + vat - (row.amtdiscount||0)
                                    row.amount = amount
                                }
                            }
                            else if (column == "perdiscount") {
                                if (newv) {
                                    let ad = amountPQ * newv / 100
                                    row.amtdiscount = Number.isNaN(ad) ? 0 : ad
                                }
                                amount = amountPQ + vat - (row.amtdiscount || 0)
                            }
                            else if (column == "amtdiscount") {
                                amount = amountPQ
                                if (newv) {
                                    row.perdiscount = row.perdiscount || gridna.editParse(newv / amount * 100)
                                    amount -= newv
                                } 
                                else if (row.perdiscount && newv) amount -= amountPQ * row.perdiscount / 100
                                else row.amtdiscount = 0
                            }
                            if (Number.isNaN(amount)) amount = 0
                            row.amount = amount
                            this.updateItem(editor.row, row)
                        }
                    }
                }

            },
            rules: {
                name: webix.rules.isNotEmpty,
                amount: webix.rules.isNumber,
                $obj: data => {
                    if (isvat && !webix.rules.isNotEmpty(data.vrt)) return false
                    if (data.price < 0 || data.quantity < 0 || data.amount < 0) return false
                    return true
                }
            }
        }

        let form = {
            view: "form",
            id: "bnv:form",
            padding: 0,
            margin: 0,
            paddingX: 2,
            complexData: true,
            elementsConfig: { labelWidth: 85 },
            elements: [
                {
                    cols: [
                        { id: "type", name: "type", label: _("invoice_type"), view: "combo", options: BTYPE, required: true },
                        { id: "idt", name: "idt", label: _("invoice_date"), view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr, required: true },
                        { id: "form", name: "form", label: _("form"), view: "text", attributes: { maxlength: 11 }, required: true },
                        { id: "serial", name: "serial", label: _("serial"), view: "text", attributes: { maxlength: 8 }, required: true },
                        { id: "seq", name: "seq", label: _("seq"), view: "text", attributes: { maxlength: 8 }, required: true }
                    ]
                },
                {
                    cols: [
                        {
                            gravity: 3, cols: [
                                {
                                    id: "sname", name: "sname", label: _("bname"), view: "text", attributes: { maxlength: 255 }, suggest: {
                                        view: "gridsuggest",
                                        fitMaster: true,
                                        body: {
                                            url: "api/org/fbn",
                                            dataFeed: function (str) {
                                                if (/\;$/.test(str)) {
                                                    let me = this
                                                    me.clearAll()
                                                    const type = $$("private").getValue()
                                                    return me.load(`${me.config.url}?val=${encodeURIComponent(str.substring(0, str.length - 1))}&type=${type}`, me.config.datatype)
                                                }
                                                else return
                                            },
                                            scroll: true,
                                            autoheight: false,
                                            autowidth: false,
                                            autofocus: true,
                                            yCount: 10,
                                            columns: [
                                                { id: "taxc", header: { text: _("taxcode"), css: "header" }, adjust: true },
                                                { id: "name", header: { text: _("customer_name"), css: "header" }, adjust: true },
                                                { id: "fadd", header: { text: _("address"), css: "header" }, adjust: true },
                                                { id: "tel", header: { text: _("tel"), css: "header" }, adjust: true },
                                                { id: "mail", header: { text: "Email", css: "header" }, adjust: true }
                                            ]
                                        },
                                        on: {
                                            onValueSuggest: (rec) => {
                                                if (rec) $$("bnv:form").setValues({ stax: rec.taxc, sname: rec.name, saddr: rec.fadd, stel: rec.tel, smail: rec.mail, sacc: rec.acc, sbank: rec.bank }, true)
                                            }
                                        }
                                    }, placeholder: _("enter2search_msg"), required: true
                                },
                                { id: "private", tooltip: _("customer_private"), view: "checkbox", checkValue: 1, uncheckValue: 0, value: 1, width: 20 }
                            ]
                        },
                        { id: "stax", name: "stax", label: _("taxcode"), view: "text", attributes: { maxlength: 14 }, validate: v => { return (!v || checkmst(v)) }, invalidMessage: _("taxcode_invalid"), required: true },
                        { id: "buyer", name: "buyer", label: _("buyer"), view: "text", attributes: { maxlength: 255 }, placeholder: _("buyer") }
                    ]
                },
                {
                    cols: [
                        { id: "saddr", name: "saddr", label: _("address"), view: "text", attributes: { maxlength: 255 }, gravity: 3, required: true },
                        { id: "stel", name: "stel", label: _("tel"), view: "text", attributes: { maxlength: 255 } },
                        { id: "smail", name: "smail", label: "Mail", view: "text", type: "email", attributes: { maxlength: 255 } }
                    ]
                },
                {
                    cols: [
                        { id: "curr", name: "curr", label: _("currency"), view: "combo", suggest: { url: "api/cat/kache/currency" }, required: true },
                        { id: "exrt", name: "exrt", label: _("exchange_rate"), view: "text", value: 1, inputAlign: "right", format: textn2, disabled: true, required: true },
                        { id: "paym", name: "paym", label: _("payment_method"), view: "text", attributes: { maxlength: 100 }, required: true },
                        { id: "sacc", name: "sacc", label: _("account"), view: "text", attributes: { maxlength: 255 } },
                        { id: "sbank", name: "sbank", label: _("bank"), view: "text", attributes: { maxlength: 255 } }
                    ]
                },
                grid,
                {
                    cols: [
                        { id: "note", name: "note", label: _("note"), view: "text", attributes: { maxlength: 255 }, gravity: 3 },
                        { id: "sumv", name: "sumv", label: _("total_vnd"), view: "text", inputAlign: "right", format: intf, readonly: true },
                        { id: "sum", name: "sum", label: _("sumv"), view: "text", inputAlign: "right", format: textna, required: true }
                    ]
                },
                {
                    id: "r10", cols: [
                        { gravity: 3 },
                        { id: "vatv", name: "vatv", label: "VAT VND", view: "text", inputAlign: "right", format: intf, readonly: true },
                        { id: "vat", name: "vat", label: _("total_vat"), view: "text", inputAlign: "right", format: textna }
                    ]
                },
                {
                    cols: [
                        { id: "word", name: "word", label: _("word"), view: "text", readonly: true, gravity: 3 },
                        { id: "totalv", name: "totalv", label: _("sum_vnd"), view: "text", inputAlign: "right", format: intf, readonly: true },
                        { id: "total", name: "total", label: _("totalv"), view: "text", inputAlign: "right", format: textna, required: true }
                    ]
                },
                {
                    cols: [
                        { id: "paid", name: "paid", label: _("paid"), view: "checkbox", checkValue: 1, value: 0, uncheckValue: 0, gravity: 3 },
                        {
                            gravity: 2, cols: [
                                {},
                                { id: "bnv:btnsave", view: "button", type: "icon", icon: "mdi mdi-content-save", label: _("save"), width: 80 },
                                { id: "bnv:btnclose", view: "button", type: "icon", icon: "mdi mdi-exit-to-app", label: _("exit"), width: 80 }
                            ]
                        }
                    ]
                }
            ]
            , rules: {
                idt: webix.rules.isNotEmpty,
                form: webix.rules.isNotEmpty,
                serial: webix.rules.isNotEmpty,
                seq: webix.rules.isNotEmpty,
                sname: webix.rules.isNotEmpty,
                stax: webix.rules.isNotEmpty,
                saddr: webix.rules.isNotEmpty,
                curr: webix.rules.isNotEmpty,
                paym: webix.rules.isNotEmpty,
                exrt: webix.rules.isNumber,
                sum: webix.rules.isNumber,
                total: webix.rules.isNumber,
                sumv: webix.rules.isNumber,
                totalv: webix.rules.isNumber,
                $obj: data => {
                    if (data.exrt <= 0) {
                        webix.message(_("exrt_positive"), "error", expire)
                        $$("exrt").focus()
                        return false
                    }
                    if (data.sum < 0 || data.total < 0 || data.sumv < 0 || data.totalv < 0) {
                        webix.message(_("amount_positive"), "error", expire)
                        return false
                    }
                    if (data.totalv >= MAX || data.sumv >= MAX) {
                        webix.message(_("amount_too_large"), "error", expire)
                        return false
                    }
                    if (data.smail) {
                        const rows = data.smail.split(/[ ,;]+/)
                        for (const row of rows) {
                            if (!webix.rules.isEmail(row.trim())) {
                                webix.message(_("mail_invalid"), "error", expire)
                                $$("smail").focus()
                                return false
                            }
                        }
                    }
                    return true
                }
            },
            on: {
                onAfterValidation: (result) => {
                    if (!result) webix.message(_("required_msg"), "info", expire)
                }
            }
        }
        return form
    }
    urlChange() {
        let me = this, form = $$("bnv:form"), grid = $$("bnv:grid")
        me.id = me.getParam("id")
        if (me.id) {
            action = me.getParam("action")
            if (!action) action = "view"
            let json
            webix.ajax(`api/bin/rbi/${me.id}`).then(data => {
                json = data.json()
                type = json.type
                isvat = ["01GTKT", "01/TVE"].includes(type)
                reset_form()
                json.items.map(e => { e.vrn = vrt2vrn(e.vrt);return e })
            }).then(() => form.parse(json)).then(() => grid.parse(json.items)).then(() => { if (action == "copy") form.setValues({ idt: new Date(), seq: null }, true) })
        }
        else {
            parse = false
            action = "new"
            type = "01GTKT"
            isvat = true
            new_form()
        }
    }
    ready() {
        if (this.id) setTimeout(() => { parse = false }, 3000)
    }
    init() {
        let grid = $$("bnv:grid"), form = $$("bnv:form")
        $$("type").attachEvent("onChange", (newv, oldv) => {
            if (!newv) return
            type = newv
            isvat = ["01GTKT", "01/TVE"].includes(type)
            if (parse) return
            if (newv !== oldv) reset_form()
        })

        $$("bnv:btnclose").attachEvent("onItemClick", () => { webix.confirm(_("exit_confirm"), "confirm-error").then(() => { this.show("/top/inv.bin") }) })

        const createInv = () => {
            let del = []
            grid.eachRow(id => {
                const row = grid.getItem(id)
                if (!row.name) del.push(id)
            })
            if (del.length > 0) grid.remove(del)
            if (!grid.validate()) throw new Error(_("details_data_required"))
            let items = grid.serialize()
            if (!(Array.isArray(items) && items.length > 0)) throw new Error(_("details_required"))
            for (let item of items) {
                if (isvat) item.vrn = vrt2vrn(item.vrt)
                else delete item["vrt"]
            }
            let inv = form.getValues()
            inv.items = items
            inv.name = $$("type").getText().toUpperCase()
            if (!isvat) {
                delete inv["vat"]
                delete inv["vatv"]
            }
            return inv
        }

        const save = () => {
            let inv = createInv()
            const me = this, id = me.id
            if (action == "copy") {
                webix.ajax().post("api/bin/post", inv).then(result => {
                    let obj = result.json()
                    if (obj.result == 1) {
                        webix.message(_("invoice_copied"), "success", expire)
                        me.show("/top/inv.bin")
                    }
                })
            }
            else {
                if (id) {
                    webix.ajax().put(`api/bin/put/${id}`, inv).then(result => {
                        let obj = result.json()
                        if (obj.result == 1) {
                            webix.message(_("invoice_updated"), "success", expire)
                            me.show("/top/inv.bin")
                        }
                    })
                }
                else {
                    webix.ajax().post("api/bin/post", inv).then(result => {
                        let obj = result.json()
                        if (obj.result == 1) {
                            webix.message(_("invoice_saved"), "success", expire)
                            grid.clearAll()
                            form.clear()
                            new_form()
                        }
                    })
                }
            }
        }
        $$("bnv:btnsave").attachEvent("onItemClick", () => {
            webix.confirm(_("invoice_save_confirm"), "confirm-warning").then(() => {
                if (!form.validate()) return
                try {
                    save()
                }
                catch (err) {
                    webix.message(err.message, "error", expire)
                }
            })
        })

        grid.data.attachEvent("onStoreUpdated", (id, obj, mode) => {
            if (mode == "add" || mode == "delete" || mode == "update") {
                let sum = 0, vat = 0
                grid.data.each((row, i) => {
                    row.line = i + 1
                    let amt = row.amount
                    amt = webix.rules.isNumber(amt) ? Number(amt) : 0
                    sum += amt
                    let rto = amt
                    if (isvat) {
                        let vrt = Number(row.vrt)
                        if (vrt > 0) {
                            let a = amt * vrt / 100
                            row.vat = a
                            vat += a
                            rto += a
                        }
                        else row.vat = 0
                    }
                    row.total = rto
                })
                let exrt, total, totalv
                exrt = $$("exrt").getValue()
                exrt = webix.rules.isNumber(exrt) ? Number(exrt) : 1
                total = sum + vat
                totalv = Math.round(total * exrt)
                $$("sum").setValue(sum)
                $$("sumv").setValue(Math.round(sum * exrt))
                $$("total").setValue(total)
                $$("totalv").setValue(totalv)
                $$("word").setValue(toWords(totalv))
                if (isvat) {
                    $$("vat").setValue(vat)
                    $$("vatv").setValue(Math.round(vat * exrt))
                }
                grid.refresh()
            }
        })

        $$("curr").attachEvent("onChange", newv => {
            if (!newv) return
            if (newv == "VND") {
                $$("exrt").setValue(1)
                $$("exrt").disable()
            }
            else {
                $$("exrt").enable()
                if (!parse) webix.ajax("api/exch/rate", { cur: newv, dt: $$("idt").getValue() }).then(data => { $$("exrt").setValue(Number(data.json())) })
            }
        })

        $$("exrt").attachEvent("onChange", newv => {
            if (!parse && webix.rules.isNumber(newv)) {
                const exrt = Number(newv)
                let sum = $$("sum").getValue(), total = $$("total").getValue()
                sum = webix.rules.isNumber(sum) ? Number(sum) : 0
                total = webix.rules.isNumber(total) ? Number(total) : 0
                const totalv = Math.round(total * exrt)
                $$("sumv").setValue(Math.round(sum * exrt))
                $$("totalv").setValue(totalv)
                $$("word").setValue(toWords(totalv))
                if (isvat) {
                    let vat = $$("vat").getValue()
                    vat = webix.rules.isNumber(vat) ? Number(vat) : 0
                    $$("vatv").setValue(Math.round(vat * exrt))
                }
            }
        })
    }
}   