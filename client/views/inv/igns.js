import { JetView } from "webix-jet"
import { filter, gridnf, KMMT, setConfStaFld, ENT, VAT } from "models/util"
const gnssel = (rec) => {
    if (rec) {
        let invgrid = $$("inv:grid"), row = invgrid.getSelectedItem()
        if (row) {
            row.name = rec.name
            row.unit = rec.unit
            row.code = rec.code
            if (rec.price) {
                const price = Number(rec.price)
                row.price = price
                if (row.quantity && !KMMT.includes(row.type))  {
                    row.amount = price * row.quantity
                    row.vat = Math.round(Number(row.vrt) * row.amount)/100  
                    row.total = row.amount + (row.vat?row.vat:0)
                } else {
                    if (["scb", "bvb"].includes(ENT)) { row.amount = "" }
				   
                }
            }
            if("vib".includes(ENT)){
                row.vrt = rec.vrt
                row.code = rec.code
            }
            invgrid.updateItem(row.id, row)
        }
        $$("igns:win").hide()
    }
}
export default class IgnsView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        let grid = {
            view: "datatable",
            id: "gns:grid",
            editable: true,
            select: "cell",
            multiselect: false,
            navigation: true,
            editaction: "dblclick",
            columns: [
                { width: 35, header: [`<span class='webix_icon wxi-plus' title='${_("gns_add")}'></span>`], template: `<span class='webix_icon wxi-check' title='${_("gns_select")}'></span>` },
                { id: "code", header: [{ text:  _("gns_code"), css: "header" }, { content: "serverFilter" }], sort: "server", editor: "text", adjust: true },
                { id: "name", header: [{ text: _("gns_name"), css: "header" }, { content: "serverFilter" }], sort: "server", editor: "text", adjust: true, fillspace:true },
                { id: "unit", header: [{ text: _("unit"), css: "header" }, { content: "serverSelectFilter" }], sort: "server", editor: "text", suggest: { url: "api/cat/kache/unit", filter: filter }, adjust: true },
                { id: "price", header: [{ text: _("price"), css: "header" }, { content: "numberFilter" }], sort: "server", css: "right", inputAlign: "right", editor: "text", format: gridnf.format, editParse: gridnf.editParse, editFormat: gridnf.editFormat, adjust: true },
                { id: "vrt",  header: [{ text: _("vrt"), css: "header" }, { content: "serverSelectFilter" }], adjust: true, collection: VAT(this.app), editor: "combo", hidden: "vib".includes(ENT) ? false : true }
            ],
            onClick: {
                "wxi-check": function (e, id) {
                    const rec = this.getItem(id)
                    gnssel(rec)
                },
                "wxi-plus": function () {
                    const me=this, id = webix.uid()
                    me.waitSave(() => { me.add({ code: "code"+id, name: "name"+id, unit: "N/A" }, 0) }).then(result => {
                        me.select(result.id,"code")
                    })
                }
            },
            url: "api/sea/fbw",
            save: { url: "api/gns", updateFromResponse: true }
        }
        return grid
    }
    ready(v, urls) {
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
    init() {
        webix.UIManager.addHotKey("enter", function (view) {
            const rec = view.getSelectedItem()
            gnssel(rec)
        }, $$("gns:grid"))
    }
}   