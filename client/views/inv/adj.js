import { JetView } from "webix-jet"
import { w, h, intf, textnfconfnumberv, gridnf3, gridnfconf, gridnf4, textnfconf, gridnf, textnf, VAT, PAYMETHOD, ITYPE, LTYPE, MAX, KMMT, MT, KCT, KKK, CK, ENT, setConfStaFld, CONFIG_CHIETKHAU_COL, CARR, ROLE, CATTAX, NUMBER_DECIMAL_FORMAT ,SESS_STATEMENT_HASCODE,DEGREE_CONFIG} from "models/util"
import { toWords } from "models/n2w"
import Iwin from "views/inv/iwin"
import IorgsView from "views/inv/iorgs"
let parse = false, action, type
var formatDate = webix.Date.dateToStr("%d/%m/%Y");
const getvrn = (vatCode) => {
    let obj = CATTAX.find(x => x.vatCode == vatCode)
    /*
    for (let i = 0; i < arr.length; i++) {
        const obj = arr[i]
        if (obj.id == id) return obj.value
    }
    */
    if (obj) return obj.vrn
    return ""
}
const getvatCode = (vrt) => {
    let obj = CATTAX.find(x => x.vrt == vrt)
    /*
    for (let i = 0; i < arr.length; i++) {
        const obj = arr[i]
        if (obj.id == id) return obj.value
    }
    */
    if (obj) return obj.vatCode
    return ""
}
const getvrt = (vatCode) => {
    let obj = CATTAX.find(x => x.vatCode == vatCode)
    /*
    for (let i = 0; i < arr.length; i++) {
        const obj = arr[i]
        if (obj.id == id) return obj.value
    }
    */
    if (obj) return obj.vrt
    return ""
}
const vats = ["vat", "vatv", "vat0", "vatv0"]
const mark = (name, val) => {
    const node = $$(name).getInputNode()
    webix.html.removeCss(node, "red")
    webix.html.removeCss(node, "blue")
    if (val) webix.html.addCss(node, val > 0 ? "red" : "blue")
}
const markAll = (dif) => {
    if (!dif) return
    mark("sum", dif.sum)
    mark("vat", dif.vat)
    mark("total", dif.total)
    mark("sumv", dif.sumv)
    mark("vatv", dif.vatv)
    mark("totalv", dif.totalv)
}
const markp = (v, c) => {
    const d = c.dp
    if (d) return d < 0 ? "blue" : "red"
    else return ""
}
const markq = (v, c) => {
    const d = c.dq
    if (d) return d < 0 ? "blue" : "red"
    else return ""
}
const markd_amt = (v, c) => {
    const d = c.d_amt
    if (d) return d < 0 ? "blue" : "red"
    else return ""
}
const markd_rate = (v, c) => {
    const d = c.d_amt
    if (d) return d < 0 ? "blue" : "red"
    else return ""
}
const markd = (v, c) => {
    const d = c.dd
    if (d) return d < 0 ? "blue" : "red"
    else return ""
}
const marka = (v, c) => {
    const d = c.da
    if (d) return d < 0 ? "blue" : "red"
    else return ""
}
const markv = (v, c) => {

    const d = c.dv
    if (d) return d < 0 ? "blue" : "red"
    else return ""
}

const reset_form = (result) => {
    let grid0 = $$("adj:grid0"), grid = $$("adj:grid"), form = $$("adj:form")
    if (type == "01GTKT") {
        $$("tax1").show()
        $$("tax2").show()
        $$("adj:vatCode").show()
        if (!grid.isColumnVisible("vrt")) grid.showColumn("vrt")
        if (!grid.isColumnVisible("vat")) grid.showColumn("vat")
        if (!grid0.isColumnVisible("vrt")) grid0.showColumn("vrt")
    }
    else {
        $$("tax1").hide()
        $$("tax2").hide()
        $$("adj:vatCode").hide()
        if (grid.getColumnConfig("vrt")) grid.hideColumn("vrt")
        if (grid.getColumnConfig("vat")) grid.hideColumn("vat")
        if (grid0.getColumnConfig("vrt")) grid0.hideColumn("vrt")
    }
    const pos = 4
    let cols = result.json(), len = cols.length
    if (len > 0) {
        form.removeView("adj:ext1")
        form.removeView("adj:ext2")
        if (len <= 5) {
            while (len < 5) {
                cols.push({})
                len++
            }
            form.addView({ id: "adj:ext1", cols: cols }, pos + 1)
            form.addView({ id: "adj:ext2", hidden: true }, pos + 2)
        }
        else {
            while (len < 10) {
                cols.push({})
                len++
            }
            form.addView({ id: "adj:ext1", cols: cols.splice(0, len / 2) }, pos + 1)
            form.addView({ id: "adj:ext2", cols: cols }, pos + 2)
        }
    }
    //HLV onChange chon ServCode [c2:servCode, c7:servContent, c8:servName]
    if (ENT == "hlv") {
        // ["c7", "c8"].map((e) => $$(e).disable())
        $$("c2").attachEvent("onChange", (newv, oldv) => {
            if (!newv) ["c7", "c8"].map((e) => $$(e).setValue())
            webix.ajax(`api/kat/payments`).then(result => {
                let json = result.json(), val = $$("c2").getValue()
                let arr = json.filter((a) => { return a.name == val })
                if (arr.length > 0) {
                    let item = arr[0], des = JSON.parse(item.des), name = des.servicename, content = des.servicecontent
                    $$("c7").setValue(content)
                    $$("c8").setValue(name)
                }
            })
        })
    }
}

class IorgsWin extends JetView {
    config() {
        return {
            view: "window",
            id: "iorgs:win",
            height: h,
            width: w,
            position: "center",
            resize: true,
            modal: true,
            head: {
                view: "toolbar", height: 40, css: 'toolbar_window',
                cols: [
                    { view: "label", label: _("customers_list") },
                    { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => { $$("iorgs:win").hide() } }
                ]
            },
            body: IorgsView
        }
    }
    show() {
        this.getRoot().show()
    }
}

export default class AdjView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        const LinkedInputs4 = {
            $init: function (config) {
                let master = $$(config.master)
                master.attachEvent("onChange", newv => {
                    let me = this, list
                    if (!parse) me.setValue()
                    list = me.getList()
                    list.clearAll()
                    if (!newv) return
                    list.load(config.dependentUrl + newv).then(data => {
                        if (!parse) {
                            const json = data.json()
                            if (json.length > 0) me.setValue(json[0].id)
                        }
                    })
                })
            }
        }

        webix.protoUI({ name: "dependent", $cssName: "combo" }, LinkedInputs4, webix.ui.combo)

        let form0 = {
            view: "form",
            id: "adj:form0",

            padding: 0,
            margin: 0,
            elements: [
                {
                    cols: [
                        { id: "adj:btnunselect", view: "button", type: "icon", icon: "mdi mdi-select-off", tooltip: _("unselect"), width: 35, disabled: (ROLE.includes('PERM_SEARCH_INV_ADJUST_UNSELECT') && !["hlv"].includes(ENT)) ? false : true },
                        { id: "adj:type", name: "type", placeholder: _("formality"), view: "combo", suggest: LTYPE(this.app), gravity: 2, disabled: ["hlv"].includes(ENT) ? true : false },
                        {
                            id: "adj:name", name: "name", placeholder: _("catalog_items"), view: "text", gravity: 4, disabled: ["hlv"].includes(ENT) ? true : false, attributes: { maxlength: 500 },
                            suggest: {
                                //view: "suggest",
                                view: "gridsuggest",
                                fitMaster: true,
                                resize: true,
                                body: {
                                    url: "api/gns/fbn",
                                    dataFeed: function (str) {
                                        if (/\;$/.test(str)) return this.load(`${this.config.url}?filter[value]=${encodeURIComponent(str.substring(0, str.length - 1))}`, this.config.datatype)
                                        else return
                                    },
                                    scroll: true,
                                    autoheight: false,
                                    columns: [
                                        { id: "name", header: { text: _("gns_name"), css: "header" }, adjust: true },
                                        { id: "code", header: { text: _("gns_code"), css: "header" } },
                                        { id: "unit", header: { text: _("unit"), css: "header" } },
                                        { id: "price", header: { text: _("price"), css: "header" }, css: "right", format: gridnfconf.format }
                                    ]
                                },
                                on: {
                                    onValueSuggest: (rec) => {
                                        if (rec) {
                                            $$("adj:name").setValue(rec.name)
                                            $$("adj:unit").setValue(rec.unit)
                                            if (ENT == "scb") $$("adj:c0").setValue(rec.c0)
                                            if (rec.price) {
                                                const price = Number(rec.price), quantity = $$("adj:quantity").getValue(), type = $$("adj:type").getValue()
                                                $$("adj:price").setValue(price)
                                                let amount = 0
                                                if (quantity && !KMMT.includes(type)) amount = price * quantity
                                                $$("adj:amount").setValue(amount)
                                            }
                                        }
                                    }
                                }
                            }
                        },

                        { id: "adj:unit", name: "unit", placeholder: _("unit"), view: "text", suggest: { url: "api/cat/kache/unit" }, gravity: 2, disabled: ["hlv"].includes(ENT) ? true : false, attributes: { maxlength: 50 } },
                        { id: "adj:c0Temp", name: "c0Temp", placeholder: _("trandate_label"), view: "datepicker", format: webix.i18n.dateFormatStr, gravity: 2, inputAlign: "right", hidden: !["scb"].includes(ENT) ? true : false },
                        { id: "adj:price", name: "price", placeholder: _("price"), view: "text", inputAlign: "right", format: textnfconf, gravity: 2, disabled: ["hlv"].includes(ENT) ? true : false },
                        { id: "adj:priceTemp", name: "priceTemp", placeholder: _("price"), view: "text", inputAlign: "right", format: textnfconf, hidden: true, disabled: ["hlv"].includes(ENT) ? true : false },
                        { id: "adj:quantity", name: "quantity", placeholder: _("quantity"), view: "text", inputAlign: "right", format: textnfconf, disabled: ["hlv"].includes(ENT) ? true : false },
                        { id: "adj:quantityTemp", name: "quantityTemp", placeholder: _("quantity"), view: "text", inputAlign: "right", format: textnfconf, hidden: true, disabled: ["hlv"].includes(ENT) ? true : false },
                        CONFIG_CHIETKHAU_COL ? { id: "adj:discountdtl", name: "discountdtl", placeholder: _("discountdtl"), view: "text", inputAlign: "right", format: textnfconf } : { id: "adj:discountdtl", name: "discountdtl", placeholder: _("discountdtl"), view: "text", inputAlign: "right", hidden: true, format: textnfconf, disabled: ["hlv"].includes(ENT) ? true : false },
                        // { id: "adj:vrt", name: "vrt", placeholder: _("vrt"), view: "combo", suggest: VAT(this.app), gravity: 2, disabled: ["hlv"].includes(ENT) ? true : false },
                        { id: "adj:vatCode", name: "vatCode", placeholder: _("vrt"), view: "combo", suggest: VAT(this.app), gravity: 2, disabled: ["hlv"].includes(ENT) ? true : false, required: true },
                        { id: "adj:discountrate", name: "discountrate", placeholder: _("discount_rate"), view: "text", inputAlign: "right", format: textnfconf, gravity: 2, disabled: ["hlv"].includes(ENT) ? true : false,hidden : ["123"].includes(DEGREE_CONFIG) ? false :true },
                        { id: "adj:discountamt", name: "discountamt", placeholder: _("discount_price"), view: "text", inputAlign: "right", format: textnfconf, gravity: 3, disabled: ["hlv"].includes(ENT) ? true : false,hidden : ["123"].includes(DEGREE_CONFIG) ? false :true },
                        { id: "adj:amount", name: "amount", placeholder: _("amount"), view: "text", inputAlign: "right", format: textnfconf, gravity: 3, disabled: ["hlv"].includes(ENT) ? true : false },
                        { id: "adj:btnadd", view: "button", type: "icon", icon: "wxi-plus", tooltip: _("insert_adjust"), width: 35, disabled: (ROLE.includes('PERM_SEARCH_INV_ADJUST_ADDADJUST') && ["hlv"].includes(ENT)) ? true : false }
                    ]
                }
                , ["mzh"].includes(ENT) ? {

                    cols: [{ width: 35 }, { gravity: 2 }, { gravity: 4 }, { id: "adj:c0", name: "c0", placeholder: _("tran_date"), view: "text", gravity: 2 },

                    { id: "adj:c1", name: "c1", placeholder: _("tran_ref"), view: "text", gravity: 2 },

                    { id: "adj:c2", name: "c2", placeholder: _("tran_amount"), view: "text" }, { gravity: 2 }, { gravity: 3 }, { width: 35 }]
                } : { height: 1 }

            ]
            , rules: {
                name: webix.rules.isNotEmpty,
                //price: webix.rules.isNumber,
                //quantity: webix.rules.isNumber,
                amount: webix.rules.isNumber,
                $obj: data => {
                    // if (data && !data.vatCode) {
                    //     webix.message(_("vrt_required"), "error")
                    //     $$("adj:vatCode").focus()
                    //     return false
                    // }
                    if (type == "01GTKT" && !data.vatCode && data.vatCode != 0) {
                        webix.message(_("vrt_required"), "error")
                        $$("adj:vatCode").focus()
                        return false
                    }
                    return true
                }
            }
        }

        let colgrid0 = [
            { id: "line", header: { text: _("stt"), css: "header" }, css: "right", adjust: true },
            { id: "type", header: { text: _("formality"), css: "header" }, adjust: true },
            { id: "name", header: { text: _("catalog_items"), css: "header" }, adjust: true },
            ["mzh"].includes(ENT) ? { id: "c0", header: { text: _("tran_date"), css: "header" }, adjust: true } : { hiden: true },

            ["mzh"].includes(ENT) ? { id: "c1", header: { text: _("tran_ref"), css: "header" }, adjust: true } : { hiden: true },

            ["mzh"].includes(ENT) ? { id: "c2", header: { text: _("tran_amount"), css: "header" }, adjust: true } : { hiden: true },
            { id: "unit", header: { text: _("unit"), css: "header" }, adjust: true },
            { id: "price", header: { text: _("price"), css: "header" }, adjust: true, css: "right", format: gridnfconf.format },
            { id: "quantity", header: { text: _("quantity"), css: "header" }, adjust: true, css: "right", format: gridnfconf.format },
            CONFIG_CHIETKHAU_COL ? { id: "discountdtl", header: { text: _("discountdtl"), css: "header" }, adjust: true, format: gridnf4.format } : { id: "discountdtl", header: { text: _("discountdtl"), css: "header" }, adjust: true, hidden: true, format: gridnf4.format },
            { id: "vrn", header: { text: _("vrt"), css: "header" }, adjust: true },
            { id: "discountrate", header: { text: _("discount_rate"), css: "header" }, adjust: true, css: "right", format: gridnfconf.format },
            { id: "discountamt", header: { text: _("discount_price"), css: "header" }, adjust: true, css: "right", format: gridnfconf.format },
            { id: "amount", header: { text: _("amount"), css: "header" }, adjust: true, css: "right", format: gridnfconf.format }
        ]
        if (ENT == "scb") {
            colgrid0.push({ id: "c0", header: { text: _("trandate_label"), css: "header" }, adjust: true, css: "right", format: webix.i18n.dateFormatStr },)
        }
        if (!["mzh"].includes(ENT)) colgrid0.splice(3, 3)
        if (['fhs', 'vhc', 'fbc'].includes(ENT)) colgrid0.splice(1, 1)
        let colgrid = [
            { template: `<span class='webix_icon wxi-trash' title='${_("details_delete")}'></span>`, width: 35 },
            { id: "line", header: { text: _("stt"), css: "header" }, css: "right", adjust: true },
            { id: "type", header: { text: _("formality"), css: "header" }, adjust: true },
            { id: "name", header: { text: _("catalog_items"), css: "header" }, adjust: true },
            ["mzh"].includes(ENT) ? { id: "c0", header: { text: _("tran_date"), css: "header" }, adjust: true } : { hiden: true },

            ["mzh"].includes(ENT) ? { id: "c1", header: { text: _("tran_ref"), css: "header" }, adjust: true } : { hiden: true },

            ["mzh"].includes(ENT) ? { id: "c2", header: { text: _("tran_amount"), css: "header" }, adjust: true } : { hiden: true },

            { id: "unit", header: { text: _("unit"), css: "header" }, adjust: true },

            { id: "price", header: { text: _("price"), css: "header" }, adjust: true, css: "right", cssFormat: markp, format: gridnfconf.format },
            { id: "quantity", header: { text: _("quantity"), css: "header" }, adjust: true, css: "right", cssFormat: markq, format: gridnfconf.format },
            CONFIG_CHIETKHAU_COL ? { id: "discountdtl", header: { text: _("discountdtl"), css: "header" }, adjust: true, cssFormat: markd, format: gridnfconf.format, editParse: gridnfconf.editParse, editFormat: gridnfconf.editFormat } : { id: "discountdtl", header: { text: _("discountdtl"), css: "header" }, adjust: true, hidden: true, format: gridnf4.format },
            { id: "vrn", header: { text: _("vrt"), css: "header" }, adjust: true },
            ["mzh"].includes(ENT) ? { id: "vat", header: { text: "VAT", css: "header" }, adjust: true, css: "right", cssFormat: markv, editor: "text", format: gridnfconf.format, editParse: gridnfconf.editParse, editFormat: gridnfconf.editFormat } : { id: "vat", header: { text: "VAT", css: "header" }, adjust: true, css: "right", cssFormat: markv, format: gridnf.format },
            { id: "discountrate", header: { text: _("discount_rate"), css: "header" }, adjust: true, css: "right", cssFormat: markd_rate, format: gridnfconf.format },
            { id: "discountamt", header: { text: _("discount_price"), css: "header" }, adjust: true, css: "right", cssFormat: markd_amt, format: gridnfconf.format },
            { id: "amount", header: { text: _("amount"), css: "header" }, adjust: true, css: "right", cssFormat: marka, format: gridnfconf.format }
        ]
        if (ENT == "scb") {
            colgrid.push({ id: "c0", header: { text: _("trandate_label"), css: "header" }, adjust: true, css: "right", format: webix.i18n.dateFormatStr },)
        }
        if (!["mzh"].includes(ENT)) colgrid.splice(4, 3)
        if (['fhs', 'vhc', 'fbc'].includes(ENT)) colgrid.splice(2, 1)
        let grid0 = {
            view: "datatable",
            id: "adj:grid0",
            select: "row",
            multiselect: false,
            editable: false,
            navigation: true,
            resizeColumn: true,
            columns: colgrid0
        }

        let grid = {
            view: "datatable",
            id: "adj:grid",
            // select: "row",
            multiselect: false,
            editable: true,
            //editaction: "click",
            navigation: true,
            resizeColumn: true,
            select: "cell",
            columns: colgrid,
            onClick: {
                "wxi-trash": function (e, id) {
                    this.remove(id)
                    return false
                }
            }


        }
        let lengthall
        if (ENT == "hlv") {
            lengthall = 500
        } else {
            lengthall = 100
        }
        let urladj = "api/sea/sbf?id=", urlform = "api/sea/fbt?id=", action = this.getParam("action")
        if (action == "create") {
            urladj = "api/sea/asbf/adj?id="
            urlform = "api/sea/afbt?id="
        }

        let form = {
            view: "form",
            id: "adj:form",
            padding: 0,
            margin: 0,
            paddingX: 2,
            complexData: true,
            elementsConfig: { labelWidth: 85 },
            elements: [
                {//ROW 1
                    cols: [
                        /*{
                            id: "bname", name: "bname", label: _("bname"), view: "text", attributes: { maxlength: 255 }, suggest: {
                                //view: "suggest",
                                view: "gridsuggest",
                                fitMaster: true,
                                resize: true,
                                body: {
                                    url: "api/org/fbn",
                                    dataFeed: function (str) {
                                        if (/\;$/.test(str)) return this.load(`${this.config.url}?filter[value]=${encodeURIComponent(str.substring(0, str.length - 1))}`, this.config.datatype)
                                        else return
                                    },
                                    scroll: true,
                                    autoheight: false,
                                    columns: [
                                        { id: "taxc", header: { text: _("taxcode"), css: "header" }, adjust: true },
                                        { id: "name", header: { text: _("customer_name"), css: "header" }, adjust: true },
                                        { id: "fadd", header: { text: _("address"), css: "header" } },
                                    ]
                                },
                                on: {
                                    onValueSuggest: (rec) => {
                                        if (rec) {
                                            $$("bname").setValue(rec.name)
                                            $$("btax").setValue(rec.taxc)
                                            $$("baddr").setValue(rec.fadd)
                                            $$("bmail").setValue(rec.mail)
                                            $$("btel").setValue(rec.tel)
                                            $$("bacc").setValue(rec.acc)
                                            $$("bbank").setValue(rec.bank)
                                        }
                                    }
                                }
                            }, placeholder: _("enter2search_msg"), required: true, gravity: 3
                        },*/
                        {
                            batch: "hd", gravity: 3,
                            cols: [(onpremcheck) ? {
                                id: "bname", name: "bname", label: _("bname"), view: "text", gravity: 3, suggest: {
                                    //view: "suggest",
                                    view: "gridsuggest",
                                    // fitMaster: true,
                                    resize: true,
                                    body: {
                                        url: "api/org/fbn",
                                        dataFeed: function (str) {
                                            if (/\;$/.test(str)) return this.load(`${this.config.url}?filter[value]=${encodeURIComponent(str.substring(0, str.length - 1))}`, this.config.datatype)
                                            else return
                                        },
                                        scroll: true,
                                        autoheight: false,
                                        columns: [
                                            { id: "taxc", header: { text: _("taxcode"), css: "header" }, adjust: true },
                                            { id: "name", header: { text: _("customer_name"), css: "header" }, adjust: true },
                                            { id: "fadd", header: { text: _("address"), css: "header" }, adjust: true }
                                        ]
                                    },
                                    on: {
                                        onValueSuggest: (rec) => {
                                            if (rec) {
                                                $$("bname").setValue(rec.name)
                                                $$("btax").setValue(rec.taxc)
                                                $$("baddr").setValue(rec.fadd)
                                                $$("bmail").setValue(rec.mail)
                                                $$("btel").setValue(rec.tel)
                                                $$("bacc").setValue(rec.acc)
                                                $$("bbank").setValue(rec.bank)
                                                $$("bcode").setValue(rec.code)
                                            }
                                        }
                                    }
                                }, placeholder: _("enter2search_msg"), required: true, attributes: { maxlength: 400 }
                            } : 
                                ["kbank", "ctbc"].includes(ENT) ? { id: "bname", name: "bname", label: _("bname"), view: "text", gravity: 3, attributes: { maxlength: 400 } } :
                                    !(["yusen", "mzh", "baca"].includes(ENT) || onpremcheck) ? { id: "bname", name: "bname", label: _("bname"), view: "text", readonly: true, gravity: 3, attributes: { maxlength: 400 } } :
                                        (!["baca"].includes(ENT) ? { id: "bname", name: "bname", label: _("bname"), view: "text", gravity: 3, attributes: { maxlength: 400 } } :
                                            {
                                                id: "bname", name: "bname", label: _("bname"), view: "text", gravity: 3, suggest: {
                                                    //view: "suggest",
                                                    view: "gridsuggest",
                                                    // fitMaster: true,
                                                    resize: true,
                                                    body: {
                                                        url: "api/org/fbn",
                                                        dataFeed: function (str) {
                                                            if (/\;$/.test(str)) return this.load(`${this.config.url}?filter[value]=${encodeURIComponent(str.substring(0, str.length - 1))}`, this.config.datatype)
                                                            else return
                                                        },
                                                        scroll: true,
                                                        autoheight: false,
                                                        columns: [
                                                            { id: "taxc", header: { text: _("taxcode"), css: "header" }, adjust: true },
                                                            { id: "name", header: { text: _("customer_name"), css: "header" }, adjust: true },
                                                            { id: "fadd", header: { text: _("address"), css: "header" }, adjust: true }
                                                        ]
                                                    },
                                                    on: {
                                                        onValueSuggest: (rec) => {
                                                            if (rec) {
                                                                $$("bname").setValue(rec.name)
                                                                $$("btax").setValue(rec.taxc)
                                                                $$("baddr").setValue(rec.fadd)
                                                                $$("bmail").setValue(rec.mail)
                                                                $$("btel").setValue(rec.tel)
                                                                $$("bacc").setValue(rec.acc)
                                                                $$("bbank").setValue(rec.bank)
                                                                $$("bcode").setValue(rec.code)
                                                            }
                                                        }
                                                    }
                                                }, placeholder: _("enter2search_msg"), required: true, attributes: { maxlength: 400 }
                                            }),
                                { id: "adj:btnorgs", view: "button", type: "icon", icon: "wxi-search", tooltip: _("customers_search"), width: 33, hidden: (!["baca"].includes(ENT)) ? true : false }]
                        },
                        { id: "idt", name: "idt", label: _("invoice_date"), view: "datepicker", stringResult: true, disabled: true, format: webix.i18n.dateFormatStr, required: true },
                        !["mzh"].includes(ENT) ? { id: "type", name: "type", label: _("invoice_type"), view: "combo", options: ITYPE(this.app), disabled: true, required: true } : 
                        { id: "type", name: "type", label: _("invoice_type"), view: "combo", options: ITYPE(this.app), required: true }
                    ]
                },
                {//ROW 2
                    cols: [
                        !["hdb", "dtt", "mzh"].includes(ENT) ?
                            { id: "btax", name: "btax", label: _("taxcode"), view: "text", attributes: { maxlength: 14 }, validate: v => { return (!v || checkmst(v)) }, invalidMessage: _("taxcode_invalid"), disabled: ["hlv"].includes(ENT) ? true : false }
                            : { id: "btax", name: "btax", label: _("taxcode"), view: "text", attributes: { maxlength: 25 }, validate: v => { if (!(!v || checkmst(v)) && ENT == "hdb") { this.webix.message(_("taxcode_invalid"), "success"); }; return true; }, invalidMessage: _("taxcode_invalid") },
                        { id: "buyer", name: "buyer", label: _("buyer"), view: "text", attributes: { maxlength: 100 }, placeholder: _("buyer_fname") },
                        { id: "btel", name: "btel", label: _("tel"), view: "text", attributes: { maxlength: 20 } },
                        { id: "bcode", name: "bcode", label: _("code"), view: "text", attributes: { maxlength: 255 }, hidden: true },
                        { id: "bmail", name: "bmail", label: "Mail", view: "text", type: "email", attributes: { maxlength: 50 } }, //, validate: v => { return !v || webix.rules.isEmail(v) }, invalidMessage: "Mail không hợp lệ" 
                        ["hlv"].includes(ENT) ?
                            { id: "form", name: "form", label: _("form"), view: "dependent", options: [], master: "type", dependentUrl: urlform, relatedAction: "enable", placeholder: "Chọn mẫu số", required: true, disabled: true }
                            : { id: "form", name: "form", label: _("form"), view: "dependent", options: [], master: "type", dependentUrl: "api/sea/fbt?id=", relatedView: "serial", relatedAction: "enable", placeholder: "Chọn mẫu số", required: true },

                    ]
                },
                {//ROW 3
                    cols: [
                        { id: "baddr", name: "baddr", label: _("address"), view: "text", attributes: { maxlength: 400 }, gravity: 3, required: true },
                        { id: "bacc", name: "bacc", label: _("account"), view: "text", attributes: { maxlength: 30 } },
                        { id: "serial", name: "serial", label: _("serial"), view: "dependent", options: [], master: "form", dependentUrl: urladj, disabled: true, placeholder: _("choose_ser"), required: true }

                    ]
                },
                {//ROW 4
                    cols: [
                        { id: "paym", name: "paym", label: _("payment_method"), view: "richselect", value: 1, options: PAYMETHOD(this.app) },
                        { id: "exrt", name: "exrt", label: _("exchange_rate"), view: "text", value: 1, inputAlign: "right", format: textnf, disabled: true, required: true },
                        { id: "curr", name: "curr", label: _("currency"), view: "combo", suggest: { url: "api/cat/kache/currency" }, required: true },
                        { id: "bbank", name: "bbank", label: _("bank"), view: "text", attributes: { maxlength: 400 } },
                        { id: "seq", name: "seq", label: _("seq"), view: "text", disabled: true, inputAlign: "right" }
                    ]
                },{//R4
                    cols: [
                        { id: "invlistnum", name: "invlistnum", label: _("list_number"), view: "text" ,hidden : ["123"].includes(DEGREE_CONFIG) ? false :true, attributes: { maxlength: 50 }},
                        { id: "invlistdt", name: "invlistdt", label: _("date_of_list"), view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr,hidden : ["123"].includes(DEGREE_CONFIG) ? false :true},
                        {},{},{}
                    ]
                },
                {//ROW 5
                    cols: [
                        { name: "adj.ref", label: _("adj.ref"), view: "text", attributes: { maxlength: 100 }},
                        { name: "adj.rdt", label: _("adj.rdt"), view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr, id: "adj.rdt"},
                        { name: "adj.rea", label: _("reason"), view: "text", attributes: { maxlength: lengthall }, required: true, id: "adj.rea" },
                        { name: "adj.des", view: "label", gravity: 2, align: "center", css: "red" }
                    ]
                },
                { id: "adj:ext1", hidden: true },
                { id: "adj:ext2", hidden: true },

                form0,
                {
                    cols: [
                        {
                            rows: [
                                grid0,
                                {
                                    cols: [
                                        { id: "curr0", name: "pays.curr", label: _("currency"), view: "text", disabled: true },
                                        { id: "sumv0", name: "pays.sumv", label: _("total_vnd"), view: "text", inputAlign: "right", disabled: true, format: intf },
                                        { id: "sum0", name: "pays.sum", label: _("sumo"), view: "text", inputAlign: "right", disabled: true, format: textnf }
                                    ]
                                },
                                {
                                    id: "tax1", cols: [
                                        ENT == 'vcm' ? { id: "score0", name: "pays.score", label: _("score"), view: "text", inputAlign: "right", disabled: true, format: textnf } : {},
                                        { id: "vatv0", name: "pays.vatv", label: "VAT VND", view: "text", inputAlign: "right", disabled: true, format: intf },
                                        { id: "vat0", name: "pays.vat", label: "VAT", view: "text", inputAlign: "right", disabled: true, format: textnf }
                                    ]
                                }
                                ,
                                {
                                    cols: [
                                        { id: "exrt0", name: "pays.exrt", label: _("exchange_rate"), view: "text", inputAlign: "right", disabled: true, format: textnf },
                                        { id: "totalv0", name: "pays.totalv", label: _("sum_vnd"), view: "text", inputAlign: "right", disabled: true, format: intf },
                                        { id: "total0", name: "pays.total", label: _("totalo"), view: "text", inputAlign: "right", disabled: true, format: textnf }
                                    ]
                                },
                                { name: "pays.word", label: _("word"), view: "text", disabled: true },
                                { name: "pays.note", label: _("note"), view: "text", disabled: true }
                            ]
                        },
                        { view: "resizer" },
                        {
                            rows: [grid,
                                {
                                    cols: [
                                        { id: "sumv", name: "sumv", label: _("total_vnd"), view: "text", inputAlign: "right", readonly: true, format: ["hlv"].includes(ENT) ? textnfconfnumberv : intf, disabled: ["hlv"].includes(ENT) ? true : false },
                                        { id: "sum", name: "sum", label: _("sumo"), view: "text", inputAlign: "right", readonly: true, format: textnf, disabled: ["hlv"].includes(ENT) ? true : false }
                                    ]
                                },
                                {
                                    id: "tax2", cols: [
                                        { id: "vatv", name: "vatv", label: "VAT VND", view: "text", inputAlign: "right", readonly: true, format: ["hlv"].includes(ENT) ? textnfconfnumberv : intf, disabled: ["hlv"].includes(ENT) ? true : false },
                                        { id: "vat", name: "vat", label: "VAT", view: "text", inputAlign: "right", readonly: true, format: textnf, disabled: ["hlv"].includes(ENT) ? true : false }
                                    ]
                                },
                                {
                                    cols: [
                                        { id: "totalv", name: "totalv", label: _("sum_vnd"), view: "text", inputAlign: "right", readonly: true, format: ["hlv"].includes(ENT) ? textnfconfnumberv : intf, disabled: ["hlv"].includes(ENT) ? true : false },
                                        { id: "total", name: "total", label: _("totalo"), view: "text", inputAlign: "right", readonly: true, format: textnf, disabled: ["hlv"].includes(ENT) ? true : false }
                                    ]
                                },
                                ENT == 'vcm' ? {
                                    cols: [
                                        { id: "score", name: "score", label: _("score"), view: "text", inputAlign: "right", format: textnf, width: 200 },
                                        { id: "word", name: "word", label: _("word"), view: "text", readonly: true },
                                    ]

                                }
                                    :
                                    {
                                        cols: [
                                            { id: "score", hidden: true },
                                            { id: "word", name: "word", label: _("word"), view: "text", readonly: true, disabled: ["hlv"].includes(ENT) ? true : false },
                                        ]
                                    },

                                {
                                    cols: [{ id: "note", name: "note", label: _("note"), view: "text", attributes: { maxlength: 255 }, disabled: ["hlv"].includes(ENT) ? true : false },
                                    //  { view: "list", id: "adj:filelist", type: "uploader", autoheight: true, borderless: true, hidden:true },
                                    { view: "text", id: "id_file", hidden: true },
                                    { id: "adj:file", view: "uploader", multiple: ['vib'].includes(ENT) ? true : false, autosend: false, link: "adj:filelist", accept: "application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/pdf", upload: "api/inv/minutes/upload", type: "icon", icon: "mdi mdi-file-find-outline", label: _("file_select"), width: 100, disabled: ROLE.includes('PERM_SEARCH_INV_ADJUST_UPLOAD_FILE') ? false : true },
                                    { id: "adj:btndoc", view: "button", type: "icon", icon: "mdi mdi-open-in-app", label: _("minutes_create"), tooltip: _("minutes_create_tip"), width: 110, disabled: ROLE.includes('PERM_SEARCH_INV_ADJUST_CREATE_MINUTES') ? false : true },
                                    { id: "adj:btnview", view: "button", type: "icon", icon: "mdi mdi-book-open-variant", label: _("view"), width: 80, hidden: (['vib'].includes(ENT)) ? false : true },
                                    { id: "adj:btnsave", view: "button", type: "icon", icon: "mdi mdi-content-save", label: _("save"), width: 80, disabled: ROLE.includes('PERM_SEARCH_INV_ADJUST_SAVE') ? false : true },
                                    { id: "inv:btniss", view: "button", type: "icon", icon: "mdi mdi-content-save-all", label: _("issue"), width: 100, css: "webix_danger" },
                                    { id: "adj:btndel", view: "button", type: "icon", icon: "wxi-trash", label: _("delete"), width: 80, hidden: true, css: "webix_danger" },
                                    { id: "adj:btnclose", view: "button", type: "icon", icon: "mdi mdi-exit-to-app", label: _("exit"), width: 80 }
                                    ]
                                }]

                        }
                    ]
                },
                { view: "list", id: "adj:filelist", type: "uploader", autoheight: true, borderless: true }],
            rules: {
                idt: webix.rules.isNotEmpty,
                form: webix.rules.isNotEmpty,
                serial: webix.rules.isNotEmpty,
                bname: webix.rules.isNotEmpty,
                curr: webix.rules.isNotEmpty,
                exrt: webix.rules.isNumber,
                baddr: webix.rules.isNotEmpty,
                // Cho phép bỏ trống thông tin số biên bản và ngày biên bản
                // "adj.ref": webix.rules.isNotEmpty,
                // "adj.rdt": webix.rules.isNotEmpty,
                $obj: data => {
                    if (data.exrt <= 0) {
                        webix.message(_("exrt_positive"), "error")
                        $$("exrt").focus()
                        return false
                    }
                    // if (data.sum < 0 || data.total < 0) {
                    //     webix.message(_("amount_positive"), "error")
                    //     return false
                    // }
                    if (data.totalv >= MAX || data.sumv >= MAX) {
                        webix.message(_("amount_too_large"), "error")
                        return false
                    }
                    if (data.mail) {
                        const rows = data.mail.split(/[ ,;]+/)
                        for (const row of rows) {
                            if (!webix.rules.isEmail(row.trim())) {
                                webix.message(_("mail_invalid"))
                                $$("bmail").focus()
                                return false
                            }
                        }
                    }

                    const adj = data.adj, idt = new Date(data.idt).setHours(0, 0, 0, 0), rdt = new Date(adj.rdt).setHours(0, 0, 0, 0), adt = new Date(adj.idt).setHours(0, 0, 0, 0)
                    if (rdt < adt) {
                        webix.message(_("adj.rdt_msg1"))
                        $$("adj.rdt").focus()
                        return false
                    }
                    if (rdt > idt) {
                        webix.message(_("adj.rdt_msg4"))
                        $$("adj.rdt").focus()
                        return false
                    }
                    return true
                }
            },
            on: {
                onAfterValidation: (result) => {
                    if (!result) webix.message(_("required_msg"))
                }
            }
        }
        return form
    }
    urlChange() {
        let me = this, grid = $$("adj:grid"), form = $$("adj:form"), grid0 = $$("adj:grid0")
        me.id = me.getParam("id")
        action = me.getParam("action")
        parse = true
        if (action == "create") {
            let sysdate
            webix.ajax("api/sysdate").then(dt => sysdate = new Date(dt.json())).then(() => {
                let json, hd, pays
                webix.ajax(`api/adj/${me.id}`).then(data => {
                    json = data.json()
                    type = json.type
                    const idt = json.idt, form = json.form, serial = json.serial, seq = json.seq, sec = json.sec
                    
                    if (ENT == 'vcm') {
                        json.adj = { seq: `${form}-${serial}-${seq}`, ref: me.id, rdt: sysdate, idt: idt, typ: 2, des: `Điều chỉnh cho hóa đơn số ${seq} ký hiệu ${serial} mẫu số ${form} ngày ${formatDate(new Date(idt))}` }
                    }
                    else {
                        json.adj = { seq: seq, ref: me.id, rdt: sysdate, form: form, serial: serial, idt: idt, typ: 2, des: `Điều chỉnh cho hóa đơn số ${seq} ký hiệu ${serial} mẫu số ${form} ngày ${formatDate(new Date(idt))}` }
                        if (DEGREE_CONFIG == "123") {
                            json.adj.type_ref = json.type_ref || 3
                            if (json.period_type) json.adj.periodtype = json.period_type
                            if (json.period) json.adj.period = json.period
                            if (json.list_invid) json.adj.list_invid = json.list_invid
                        }
                    }
                    if (json.pid && json.new) {
                        hd = json.new
                        pays = { note: json.note, exrt: json.exrt, curr: json.curr, word: hd.word, sum: hd.sum, vat: hd.vat, total: hd.total, sumv: hd.sumv, vatv: hd.vatv, totalv: hd.totalv, score: json.score }
                    }
                    else {
                        pays = { note: json.note, exrt: json.exrt, curr: json.curr, word: json.word, sum: json.sum, vat: json.vat, total: json.total, sumv: json.sumv, vatv: json.vatv, totalv: json.totalv, score: json.score }
                        delete json["tax"]
                    }
                    //Chuyển xuống đây vì hóa đơn điều chỉnh nào cũng có thẻ root
                    json.root = { sec: sec, form: form, serial: serial, seq: seq, idt: idt, btax: json.btax, bname: json.bname, tax: json.tax, pay: pays }
                    json.pays = pays

                }).then(() => {
                    let lang = webix.storage.local.get("lang")
                    webix.ajax().post(`api/cus`,{itype:type,lang:lang}).then(result => reset_form(result)).then(() => {
                        form.parse(json)
                        if (json.pid && json.new) grid0.parse(hd.items)
                        else grid0.parse(json.items)
                    }).then(() => {


                        if (ENT == 'mzh') {
                            form.setValues({ idt: sysdate, seq: null, sum: 0, vat: 0, total: 0, sumv: 0, vatv: 0, totalv: 0, word: null, score: 0, note: json.adj.des }, true)

                        } else {
                            form.setValues({ idt: sysdate, seq: null, sum: 0, vat: 0, total: 0, sumv: 0, vatv: 0, totalv: 0, word: null, score: 0, note: null }, true)
                        }
                        if (["bahuan", "fhs", "fbc"].includes(ENT))
                            $$("adj:btndoc").hide()
                        if (ROLE.includes('PERM_SEARCH_INV_ADJUST_CREATE_MINUTES') && (!["bahuan", "fhs", "fbc"].includes(ENT)))
                            $$("adj:btndoc").enable()
                    })


                })
            })
        }
        else {
            let json
            webix.ajax(`api/adj/${me.id}`).then(data => {
                json = data.json()
                type = json.type
            }).then(() => {
                let lang = webix.storage.local.get("lang")
                webix.ajax().post(`api/cus`,{itype:type,lang:lang}).then(result => reset_form(result)).then(() => {
                    form.parse(json)
                    grid.parse(json.items)
                    markAll(json.dif)
                    if (json.status == 1) $$("adj:btndel").show()
                }).then(() => webix.ajax(`api/adj/${json.pid}`).then(goc => grid0.parse(goc.json().items)))
            })
        }
    }
    ready(v, urls) {
        if (this.id) setTimeout(() => { parse = false }, 3000)
        if (ENT != "vcm") $$("inv:btniss").hide()
        if (onpremcheck) $$("idt").enable()
        if (["mzh"].includes(ENT)) $$("idt").enable()
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", { url: urls[0].page, params: urls[0].params }).then(result => {
            let json = result.json()

            setConfStaFld(json)
        })
        //
    }
    init() {
        let grid0 = $$("adj:grid0"), grid = $$("adj:grid"), form = $$("adj:form"), form0 = $$("adj:form0"), uploader = $$("adj:file")
        if (["yusen", "bahuan", "hlv"].includes(ENT)) $$("adj:file").hide()
        if (["bahuan"].includes(ENT)) $$("adj:btndoc").hide()
        $$("curr").disable()
        this.Iwin = this.ui(Iwin)
        this.IorgsWin = this.ui(IorgsWin)
        $$("bname").config.readonly = false
        const updAmount = () => {
            let type = $$("adj:type").getValue(), amount = 0, discountamt=0
            if (KMMT.includes(type)) {
                $$("adj:vrt").setValue("0")
                $$("adj:vatCode").setValue("VAT0000")
            }
            else {
                const price = $$("adj:price").getValue(),discountrate = $$("adj:discountrate").getValue(), quantity = $$("adj:quantity").getValue(), discountdtl = $$("adj:discountdtl").getValue()
                if (price && quantity) amount = price * quantity
                if (discountrate) {
                    discountamt = Math.round(price * discountrate * quantity) / 100
                    amount = price * quantity - discountamt
                }
                if (discountdtl && amount) amount = amount * (100 - discountdtl) / 100
            }
            $$("adj:amount").setValue(amount)
            $$("adj:discountamt").setValue(discountamt)

        }

        $$("adj:type").attachEvent("onChange", (newv) => {
            if (newv == MT) {
                $$("adj:vrt").setValue("0")
                $$("adj:vatCode").setValue("VAT0000")
                $$("adj:price").setValue(0)
                $$("adj:quantity").setValue(0)
                $$("adj:amount").setValue(0)
            }
            else updAmount()
        })

        $$("adj:price").attachEvent("onChange", () => {
            updAmount()

        })
        $$("adj:discountrate").attachEvent("onChange", () => {
            updAmount()

        })
        if (ENT == "vcm") {
            $$("adj:price").attachEvent("onBlur", () => {

                let val = $$("adj:price").getValue()
                let valt = $$("adj:priceTemp").getValue()

                if (val != valt) $$("adj:quantity").disable()
                else ($$("adj:quantity").enable())
            })
        }


        $$("adj:quantity").attachEvent("onChange", () => {
            updAmount()
        })
        if (ENT == "vcm") {
            $$("adj:quantity").attachEvent("onBlur", () => {

                let val = $$("adj:quantity").getValue()
                if (val != $$("adj:quantityTemp").getValue()) $$("adj:price").disable()
                else (
                    $$("adj:price").enable()
                )

            })
        }

        $$("adj:discountdtl").attachEvent("onChange", () => {
            updAmount()
        })
        $$("adj:btnunselect").attachEvent("onItemClick", () => {
            grid0.unselectAll()
            form0.clear()
            if (type == "01GTKT") {
                $$("adj:vrt").setValue("10")
                $$("adj:vatCode").setValue("VAT1000")
            }
        })

        $$("adj:btnview").attachEvent("onItemClick", () => {
            try {
                let inv = createinv(true)
                if (action && action != "wcancel") inv.status = 1
                inv.sec = ""
                webix.ajax().post("api/inv/view", inv).then(result => {
                    const json = result.json()
                    // const req = createRequest(json)
                    //let pdf = res.json()
                    let b64 = json.pdf
                    const blob = dataURItoBlob(b64);

                    var temp_url = window.URL.createObjectURL(blob);
                    $$("iwin:ipdf").define("src", temp_url)
                    $$("iwin:win").show()
                    //jsreport.renderAsync(req).then(res => {
                    //  $$("iwin:ipdf").define("src", res.toDataURI())
                    // webix.ajax().post("api/seek/report", req).then(res => {
                    //     let pdf = res.json()
                    //     let b64 = pdf.pdf
                    //      const blob = dataURItoBlob(b64);

                    //     var temp_url = window.URL.createObjectURL(blob);
                    //     $$("iwin:ipdf").define("src", temp_url)
                    //     $$("iwin:win").show()
                    // })
                })
            }
            catch (err) {
                webix.message(err.message, "error")
            }
        })
        if (ENT != "hlv") {
            grid0.attachEvent("onAfterSelect", () => {
                let rec0 = grid0.getSelectedItem()
                if(rec0.price == null) rec0.price = ''
                if(rec0.quantity == null) rec0.quantity = ''
                rec0.quantityTemp = rec0.quantity
                rec0.priceTemp = rec0.price
                if(rec0.discountrate == null) rec0.discountrate = ''
                if(rec0.discountamt == null) rec0.discountamt = ''
                if (ENT == "scb") { rec0.c0Temp = rec0.c0 }
                if(!rec0.vatCode) rec0.vatCode = getvatCode(rec0.vrt)
                form0.parse(rec0)
                // $$("adj:quantityTemp").setValue(rec0.quantity)
                // $$("adj:priceTemp").setValue(rec0.price)
            })
        }
        $$("adj:btndel").attachEvent("onItemClick", () => {
            webix.confirm(_("invoice_del_confirm"), "confirm-warning").then(() => {
                const pid = form.getValues().pid
                webix.ajax(`api/adj/del/${this.id}/${pid}`).then(result => {
                    if (result.json()) {
                        webix.message(_("invoice_del_ok"))
                        // history.back()
                        this.app.show("/top/inv.invs")
                    }
                })
            })
        })

        $$("adj:btnclose").attachEvent("onItemClick", () => {
            webix.confirm(_("exit_confirm"), "confirm-error").then(() => /*history.back()*/this.app.show("/top/inv.invs"))
        })
        $$("inv:btniss").attachEvent("onItemClick", () => {
            let vform = $$("form").getValue()
            let vserial = $$("serial").getValue()
            let vlable = _("invoice_issue_confirm2").replace('@@', vform + '-' + vserial)
            webix.confirm(vlable, "confirm-warning").then(() => {

                if (!$$("serial").getText()) {
                    this.webix.message(_("null_serial"), "error")
                    return
                }
                if (!form.validate()) return
                let items = grid.serialize(), arr = [], cka = 0, ckd = 0
                for (let item of items) {
                    delete item["id"]

                    if (item.da < 0) cka++

                    else ckd++
                    arr.push({ row: item.row, name: item.name, vrn: item.vrn, vrt: item.vrt, type: item.type, unit: item.unit, price: item.p, quantity: item.q, amount: item.a })
                }
                if (ENT == 'vcm') {
                    if (cka > 0 && ckd > 0) {
                        this.webix.message(_("not_allow_adj_inc_dec"), "error")
                        return
                    }
                }


                let inv = form.getValues(), pays = inv.pays, dif = inv.dif
                webix.ajax("api/inv/count/12", { form: inv.form, serial: inv.serial }).then(result => {
                    const json = result.json()
                    if (json.rc) {
                        webix.message(_("invoice_issue_exists_error"), "error")
                        return
                    }
                })
                inv.items = items
                inv.tax = tax(items)
                const searchrow = (arr, line) => {
                    for (let i = 0; i < arr.length; i++) {
                        if (arr[i].row === line) {
                            return arr[i]
                        }
                    }
                }
                let items0 = grid0.serialize(), j = 1, hd = {}
                for (let item of items0) {
                    let ok = searchrow(arr, item.line)
                    if (typeof ok === "undefined") arr.push({ name: item.name, vrn: item.vrn, vrt: item.vrt, type: item.type, unit: item.unit, price: item.price, quantity: item.quantity, amount: item.amount })
                }
                arr = arr.filter(function (obj) {
                    return obj.amount >= 0
                })

                if (type !== "01GTKT") {
                    for (var i = 0; i < vats.length; i++) {
                        delete inv[vats[i]]
                    }
                    for (let item of arr) {
                        item.line = j++
                        delete item["row"]
                        delete item["vrt"]
                        delete item["vrn"]
                    }
                }
                else {
                    for (let item of arr) {
                        item.line = j++
                        delete item["row"]
                    }
                    hd.tax = tax(arr)
                    if (inv.sum == 0) {
                        hd.vat = pays.vat
                        hd.vatv = pays.vatv
                    } else {
                        hd.vat = pays.vat - dif.vat
                        hd.vatv = pays.vatv - dif.vatv
                    }

                }
                hd.items = arr
                if (inv.sum == 0) {
                    hd.sum = pays.sum
                    hd.sumv = pays.sumv
                    hd.total = pays.total
                    hd.totalv = pays.totalv
                } else {
                    hd.sum = pays.sum - dif.sum
                    hd.sumv = pays.sumv - dif.sumv
                    hd.total = pays.total - dif.total
                    hd.totalv = pays.totalv - dif.totalv
                }

                hd.word = toWords(hd.totalv)
                inv.new = hd
                inv.pid = this.id
                // if (ENT == "vcm") {

                //     const seqs = inv.adj.seq.split("-")
                //     if (inv.sum == 0 && inv.vat == 0) {
                //         inv.adj.des = `Điều chỉnh thông tin cho hóa đơn số ${seqs[2]} ký hiệu ${seqs[1]} mẫu số ${seqs[0]} ngày ${webix.i18n.dateFormatStr(new Date(inv.adj.idt))}`
                //     }
                //     else {
                //         if (inv.dif.total > 0) inv.adj.des = `Điều chỉnh giảm cho hóa đơn số ${seqs[2]} ký hiệu ${seqs[1]} mẫu số ${seqs[0]} ngày ${webix.i18n.dateFormatStr(new Date(inv.adj.idt))}`
                //         if (inv.dif.total < 0) inv.adj.des = `Điều chỉnh tăng cho hóa đơn số ${seqs[2]} ký hiệu ${seqs[1]} mẫu số ${seqs[0]} ngày ${webix.i18n.dateFormatStr(new Date(inv.adj.idt))}`
                //     }

                // }
                let rea = $$("adj.rea").getValue()
                if (inv.sum == 0 && inv.vat == 0) {
                    inv.adj.des = `Điều chỉnh thông tin ` + rea + ` cho hóa đơn số ${inv.adj.seq} ký hiệu ${inv.adj.serial} mẫu số ${inv.adj.form} ngày ${formatDate(new Date(inv.adj.idt))}`
                    inv.adjType=4
                }
                else {

                    if (inv.dif.total > 0){
                        inv.adj.des = `Điều chỉnh giảm ` + rea + ` cho hóa đơn số ${inv.adj.seq} ký hiệu ${inv.adj.serial} mẫu số ${inv.adj.form} ngày ${formatDate(new Date(inv.adj.idt))}`
                        inv.adjType=3
                        hd.word = toWords(hd.totalv * -1)
                    } 
                    if (inv.dif.total < 0){
                        inv.adj.des = `Điều chỉnh tăng ` + rea + ` cho hóa đơn số ${inv.adj.seq} ký hiệu ${inv.adj.serial} mẫu số ${inv.adj.form} ngày ${formatDate(new Date(inv.adj.idt))}`
                        inv.adjType=2
                    } 
                }
                webix.ajax().post("api/adjniss", inv).then(result => {
                    let iid = result.json()
                    console.log(iid)

                    if (iid.ref) {
                        webix.ajax().post(`${USB}xml`, iid).then(result => {
                            const rows = result.json()
                            webix.ajax().put("api/sign", iid).then(result => {
                                if (result.json()) webix.message(_("invoice_issued"), "success")
                            })
                        }).catch(e => { webix.message(_("certificate_error"), "error") })
                    } else {
                        if (iid) {
                            webix.message(`${_("adj_inv")} ID: ${iid}`, "success", 10000)

                            this.app.show("/top/inv.invs")
                        }
                    }

                })
            })
        })
        $$("adj:btnadd").attachEvent("onItemClick", () => {
            if (!form0.validate()) return
            let curra = $$("curr").getValue()
            let NUMBER_DECIMAL = NUMBER_DECIMAL_FORMAT
            if(curra == "VND") {
                NUMBER_DECIMAL = 0
            }
            let rec0, rec1 = form0.getValues(), line = rec1.line
            let q0 = 0, p0 = 0, d0 = 0, a0 = 0, v0 = 0, v1 = 0, q1 = 0, p1 = 0, d1 = 0 ,d_rate0 =0, d_atm0 = 0 ,d_rate1 =0, d_atm1 = 0 
            if (webix.rules.isNumber(rec1.quantity)) q1 = rec1.quantity
            if (webix.rules.isNumber(rec1.price)) p1 = rec1.price
            if (webix.rules.isNumber(rec1.discountrate)) d_rate1 = rec1.discountrate
            if (webix.rules.isNumber(rec1.discountamt)) d_atm1 = rec1.discountamt
            if (webix.rules.isNumber(rec1.discountdtl)) d1 = rec1.discountdtl
            if (line) {
                grid.eachRow(id => {
                    const rec = grid.getItem(id)
                    if (rec.row === line) grid.remove(id)
                })
                rec0 = grid0.getSelectedItem()
                delete rec0["id"]
                if (webix.rules.isNumber(rec0.quantity)) q0 = rec0.quantity
                if (webix.rules.isNumber(rec0.price)) p0 = rec0.price
                if (webix.rules.isNumber(rec1.discountrate)) d_rate0 = rec0.discountrate
                if (webix.rules.isNumber(rec1.discountamt)) d_atm0 = rec0.discountamt
                if (webix.rules.isNumber(rec0.discountdtl)) d0 = rec0.discountdtl
                if (webix.rules.isNumber(rec0.amount)) a0 = rec0.amount
                if (type == "01GTKT") {
                    const vrt = Number(rec0.vrt)
                    if (vrt > 0) {
                        if (["fhs", "fbc"].includes(ENT))// tien da bao gom vat
                            v0 = (a0 / (1 + vrt / 100)) * vrt / 100
                        else v0 = a0 * vrt / 100
                    }

                }
            }
            if (ENT == "vcm") rec1.amount = Math.round(rec1.amount)
            let dq = (q0 != q1) ? q0 - q1 - 0.000000000004 : 0, dp = p0 - p1, dd = d0 - d1, a1 = rec1.amount, da = a0 - a1 ,d_rate = d_rate0 - d_rate1 ,d_amt = d_atm0 -d_atm1
            let dp2, dq2,da_ck,dd_ck,dp_ck,dq_ck

            // doi vs so am phai nhan -1 trc khi lam tron
            if (da<0)da_ck=da *-1
            else da_ck=da
            if (dq<0)dq_ck=dq *-1
            else dq_ck=dq
            if (dd<0)dd_ck=dd*-1
            else dd_ck=dd
            if (dp<0)dp_ck=dp *-1
            else dp_ck=dp
            da_ck = Number((Math.round((da_ck ? da_ck : 0)*Math.pow(10,NUMBER_DECIMAL) + 0.000000000004)/Math.pow(10,NUMBER_DECIMAL)).toFixed(NUMBER_DECIMAL))
            dd_ck = Number((Math.round((dd_ck ? dd_ck : 0)*Math.pow(10,NUMBER_DECIMAL) + 0.000000000004)/Math.pow(10,NUMBER_DECIMAL)).toFixed(NUMBER_DECIMAL))
            dp_ck = Number((Math.round((dp_ck ? dp_ck : 0)*Math.pow(10,NUMBER_DECIMAL) + 0.000000000004)/Math.pow(10,NUMBER_DECIMAL)).toFixed(NUMBER_DECIMAL))
            if (da<0)da=da_ck *-1
            else da=da_ck
            if (dq<0)dq=dq_ck *-1
            else dq=dq_ck
            if (dd<0)dd=dd_ck *-1
            else dd=dd_ck
            if (dp<0)dp=dp_ck *-1
            else dp=dp_ck
            // if (dq!=0){dp2 = p0 } 
            // else {dp2=dp}
            // if (dp!=0) {dq2 = q0}
            // else {dq2=dq}
            let rec = {}
            rec.row = line
            rec.rec = rec0
            rec.type = rec1.type
            rec.name = rec1.name
            if (ENT == "scb") { rec.c0 = rec1.c0Temp }
            rec.unit = rec1.unit
            if (ENT == "mzh") {
                rec.c0 = rec1.c0
                rec.c1 = rec1.c1
                rec.c2 = rec1.c2
            }
            rec.quantity = Math.abs(dq)
            rec.quantity = dq * -1
            if (rec.quantity == 0) rec.quantity = null

            if (rec.quantity == 0) rec.quantity = Number((Math.round((q0 ? q0 : 0)*Math.pow(10,NUMBER_DECIMAL_FORMAT) + 0.000000000004)/Math.pow(10,NUMBER_DECIMAL_FORMAT)).toFixed(NUMBER_DECIMAL_FORMAT))
            rec.discountrate = Math.abs(d_rate)
            if (rec.discountrate == 0) rec.discountrate = null

            rec.discountamt = Math.abs(d_amt)
            if (rec.discountamt == 0) rec.discountamt = null

            rec.price = Math.abs(dp)
           rec.price = dp * -1
            if (rec.price == 0) rec.price = null
            if (rec.price == 0) rec.price = Number((Math.round((p0 ? p0 : 0)*Math.pow(10,NUMBER_DECIMAL) + 0.000000000004)/Math.pow(10,NUMBER_DECIMAL)).toFixed(NUMBER_DECIMAL))
            rec.discountdtl = Math.abs(dd)
            rec.amount = Math.abs(da)
           rec.amount = da * -1
            // if (rec.amount == 0) rec.amount = null
            rec.amountNoVat = da / (1 + (Number(rec1.vrt) > 0 ? Number(rec1.vrt) : 0) / 100)
            rec.amountNoVat = Number((Math.round((da/(1+(Number(rec1.vrt)>0?Number(rec1.vrt):0)/100))*Math.pow(10,NUMBER_DECIMAL) + 0.000000000004)/Math.pow(10,NUMBER_DECIMAL)).toFixed(NUMBER_DECIMAL))
            if (rec.amountNoVat == 0) rec.amountNoVat = null
            rec.dq = dq
            rec.dp = dp
            rec.dd = dd
            rec.da = da
            rec.q = q1
            rec.p = p1
            rec.a = a1
            rec.d_amt = d_amt
            rec.d_rate = d_rate
            if (type == "01GTKT") {
                const vrt = Number(getvrt(rec1.vatCode))
                if (vrt > 0) {
                    if (["fhs", "fbc"].includes(ENT))// tien da bao gom vat
                        v1 = (a1 / (1 + vrt / 100)) * vrt / 100
                    else v1 = a1 * vrt / 100
                }


                let dv = v0 - v1,dv_ck
                 // doi vs so am phai nhan -1 trc khi lam tron
                if (dv<0)dv_ck=dv *-1
                else dv_ck=dv
                dv_ck = Math.abs(Number((Math.round((dv_ck ? dv_ck : 0)*Math.pow(10,NUMBER_DECIMAL) + 0.000000000004)/Math.pow(10,NUMBER_DECIMAL)).toFixed(NUMBER_DECIMAL)))
                if (dv<0)dv=dv_ck *-1
                else dv=dv_ck
              //  rec.vat = Math.abs(dv)
                rec.vat = dv * -1
                if (rec.vat == 0) rec.vat = null
                rec.dv = dv
                rec.vrt = vrt
                rec.vrn = getvrn(rec1.vatCode)
                rec.vatCode = rec1.vatCode
                if (ENT == "scb" && (rec.dv > 0 || rec.da > 0)) rec.name = 'Điều chỉnh giảm_' + rec1.name
                if (ENT == "scb" && (rec.dv < 0 || rec.da < 0)) rec.name = 'Điều chỉnh tăng_' + rec1.name
            }
            if (rec.vat && !["fhs", "fbc"].includes(ENT))
                rec.total = rec.amount + rec.vat
            else rec.total = rec.amount
            grid.add(rec)
        })

        const createinv = () => {
            //Sửa SUM + VAT
            const sumo = $$("sum").getValue(), vato = $$("vat").getValue(), totalo = $$("total").getValue()
            //
            let items = grid.serialize(), arr = []
            for (let item of items) {

                arr.push({ row: item.row, name: item.name, vrn: item.vrn, vrt: item.vrt, vatCode: item.vatCode, type: item.type, unit: item.unit, price: item.p, quantity: item.q, amount: item.a })
            }
            let inv = form.getValues(), pays = inv.pays, dif = inv.dif
            inv.items = items
            inv.name =$$("type").getText().toUpperCase()
            if (!dif) dif = pays
            const searchrow = (arr, line) => {
                for (let i = 0; i < arr.length; i++) {
                    if (arr[i].row === line) {
                        return arr[i]
                    }
                }
            }
            let items0 = grid0.serialize(), j = 1, hd = {}
            for (let item of items0) {
                let ok = searchrow(arr, item.line)
                if (typeof ok === "undefined") arr.push({ name: item.name, vrn: item.vrn, vrt: item.vrt, vatCode: item.vatCode, type: item.type, unit: item.unit, price: item.price, quantity: item.quantity, amount: item.amount })
            }
            arr = arr.filter(function (obj) {
                return obj.amount > 0
            })
            if (type !== "01GTKT") {
                for (var i = 0; i < vats.length; i++) {
                    delete inv[vats[i]]
                }
                for (let item of arr) {
                    item.line = j++
                    delete item["row"]
                    delete item["vrt"]
                    delete item["vrn"]
                }
            }
            else {
                for (let item of arr) {
                    item.line = j++
                    delete item["row"]
                }
                hd.tax = tax(arr)
                hd.vat = pays.vat - dif.vat
                hd.vatv = pays.vatv - dif.vatv
            }
            hd.items = arr
            hd.sum = pays.sum - dif.sum
            hd.sumv = pays.sumv - dif.sumv
            hd.total = pays.total - dif.total
            hd.totalv = pays.totalv - dif.totalv
            hd.word = toWords(hd.totalv)
            inv.new = hd
            inv.tax = tax(items)
            //Sửa SUM + VAT
            inv.sum = sumo
            inv.vat = vato
            inv.total = totalo
            //

            return inv
        }

        $$("adj:btndoc").attachEvent("onItemClick", () => {
            const invoice = createinv()
            invoice.items0 = grid0.serialize()
            invoice.items1 = invoice.new.items
            webix.ajax().response("blob").post("api/inv/minutes/create", invoice).then(data => webix.html.download(data, "BB_DieuChinh.docx"))
            // const id = this.id
            // webix.ajax().response("blob").post("api/inv/minutes/create", { idOldInvoice: id, infoNewInvoice: JSON.stringify(form.getValues()), items1: JSON.stringify(grid0.serialize()), items2: JSON.stringify(grid.serialize()) }).then(data => webix.html.download(data, "BB_DieuChinh.docx"))
        })


        grid.data.attachEvent("onStoreUpdated", (id, obj, mode) => {
            if (mode == "add" || mode == "delete") {

                const exrt0 = $$("exrt0").getValue(), exrt = $$("exrt").getValue(), total0 = $$("total0").getValue(), score0 = ((ENT == 'vcm') ? $$("score0").getValue() : 0)
                let dif = form.getValues().dif, xs = 0, xt = 0, xv = 0, score = 0, totalck = 0, totals = 0
                if (dif) {
                    if (exrt0 !== exrt) {
                        xs = dif.xs
                        xt = dif.xt
                        if (type == "01GTKT") xv = dif.xv
                    }
                }
                else {
                    dif = {}
                }
                let sum = 0, vat = 0, total, sumv, totalv, chk = 0, sumNovat = 0
                grid.data.each((row, i) => {
                    row.line = i + 1
                    if (webix.rules.isNumber(row.da)) {
                        sum += row.da
                        sumNovat += row.amountNoVat
                        totals += row.da
                        if (row.da < 0) chk = 1
                        if (type == "01GTKT" && webix.rules.isNumber(row.dv)) vat += row.dv
                    }
                })
                if (["fhs", "fbc"].includes(ENT)) {
                    if (totals == 0) {
                        total = vat
                        sum = 0
                    } else {
                        total = totals

                        sum = totals - vat
                    }


                } else {
                    total = sum + vat

                }

                totalck = total

                sumv = Math.round(sum * exrt + xs)
                if (exrt == 1) {
                    totalv = Math.round(Math.round((sumv + vat) * exrt) + xt)
                } else {
                    if (vat > 0)totalv = Math.round(Math.round((sum + vat) * exrt) + xt)
                    else totalv = Math.round(Math.round(sum * exrt)+(Math.round(vat * exrt * -1) * -1) + xt)
                }
                if (ENT == "vcm" && chk == 0 && score0) { //truog hop chi giam moi co diem tieu

                    score = Math.abs(score0 * total / (total0 + score0))
                    vat = vat - score * vat / total
                    total = Math.abs(total) - score
                    totalv = Math.abs(Math.round(total * exrt + xt))
                    sum = Math.abs(sum - score * sum / totalck)
                    sumv = Math.abs(Math.round(sum * exrt + xs))
                    $$("score").setValue(score)
                    $$("total").setValue(total)

                    $$("sumv").setValue(sumv)
                    $$("totalv").setValue(totalv)
                    $$("word").setValue(toWords(totalv))
                    $$("sum").setValue(sum)
                } else {
                    $$("sum").setValue(sum * -1)
                    $$("total").setValue(total * -1)
                    $$("sumv").setValue(sumv * -1)
                    $$("totalv").setValue(totalv * -1)
                    $$("word").setValue(toWords(totalv * -1))
                }


                dif.sum = sum
                dif.total = total
                dif.sumv = sumv
                dif.totalv = totalv
                if (type == "01GTKT") {
                    const vatv = vat * exrt + xv

                   // $$("vat").setValue(Math.abs(vat))
                   // $$("vatv").setValue(Math.abs(vatv))
                    $$("vat").setValue(vat * -1)
                    $$("vatv").setValue(vatv * -1)
                    dif.vat = vat
                    dif.vatv = vatv
                }
                form.setValues({ "dif": dif }, true)
                grid.refresh()
                markAll(dif)
            }
            if (ENT == 'mzh' && mode == "update") {//danh cho mzh update tien vat
                const exrt0 = $$("exrt0").getValue(), exrt = $$("exrt").getValue()
                let dif = form.getValues().dif, xs = 0, xt = 0, xv = 0, totals = 0
                if (dif) {
                    if (exrt0 !== exrt) {
                        xs = dif.xs
                        xt = dif.xt
                        if (type == "01GTKT") xv = dif.xv
                    }
                }
                else {
                    dif = {}
                }
                let sum = 0, vat = 0, total, sumv, totalv, chk = 0, sumNovat = 0
                grid.data.each((row, i) => {
                    row.line = i + 1
                    if (webix.rules.isNumber(row.da)) {
                        sum += row.da
                        sumNovat += row.amountNoVat
                        totals += row.da
                        if (row.da < 0) chk = 1
                        row.vat = Number(row.vat)
                        if (row.vat) row.total = Number(row.vat) + (row.amount ? row.amount : 0)
                        if (type == "01GTKT" && webix.rules.isNumber(row.dv)) vat += row.vat
                    }
                })
                total = sum + vat
                sumv = Math.round(sum * exrt + xs)
                totalv = Math.round(total * exrt + xt)
                $$("sum").setValue(Math.abs(sum))
                $$("total").setValue(Math.abs(total))
                $$("sumv").setValue(Math.abs(sumv))
                $$("totalv").setValue(Math.abs(totalv))
                $$("word").setValue(toWords(Math.abs(totalv)))
                dif.sum = sum
                dif.total = total
                dif.sumv = sumv
                dif.totalv = totalv
                if (type == "01GTKT") {
                    const vatv = vat * exrt + xv

                    $$("vat").setValue(Math.abs(vat))
                    $$("vatv").setValue(Math.abs(vatv))
                    dif.vat = vat
                    dif.vatv = vatv
                }
                form.setValues({ "dif": dif }, true)
                grid.refresh()
                markAll(dif)
            }
        })


        $$("curr").attachEvent("onChange", newv => {
            if (!newv) return
            if (newv == "VND") {
                $$("exrt").setValue(1)
                $$("exrt").disable()
            }
            else {
                $$("exrt").enable()
                if (!parse) webix.ajax("api/exch/rate", { cur: newv, dt: $$("idt").getValue() }).then(data => { $$("exrt").setValue(Number(data.json())) })
            }
        })

        $$("exrt").attachEvent("onChange", newv => {
            if (!parse && webix.rules.isNumber(newv)) {
                const exrt0 = $$("exrt0").getValue(), line = 0
                const sum0 = $$("sum0").getValue(), sumv0 = $$("sumv0").getValue()
                const total0 = $$("total0").getValue(), totalv0 = $$("totalv0").getValue()
                let dif = form.getValues().dif
                if (!dif) dif = {}
                dif.xs = Math.round(sumv0 - sum0 * newv)
                dif.xt = Math.round(totalv0 - total0 * newv)
                if (type == "01GTKT") {
                    const vat0 = $$("vat0").getValue(), vatv0 = $$("vatv0").getValue()
                    dif.xv = Math.round(vatv0 - vat0 * newv)
                }
                form.setValues({ "dif": dif }, true)

                grid.eachRow(id => {
                    const rec = grid.getItem(id)
                    if (rec.row === line) grid.remove(id)
                })

                if (exrt0 !== newv) {
                    const curr0 = $$("curr0").getValue(), curr = $$("curr").getValue()
                    let name = "Thay đổi"
                    if (curr0 != curr) name += ` loại tiền ${curr0}->${curr},`
                    name += ` tỷ giá ${exrt0}->${newv}`
                    let rec = {}
                    rec.name = name
                    rec.row = line
                    rec.type = MT
                    rec.price = 0
                    rec.quantity = 0
                    rec.amount = 0
                    rec.dp = 0
                    rec.dq = 0
                    rec.da = 0
                    rec.q = 0
                    rec.p = 0
                    rec.a = 0
                    if (type == "01GTKT") {
                        rec.dv = 0
                        rec.vrt = "0"
                        rec.vrn = "0%"
                    }
                    grid.add(rec)
                }
            }
        })

        const tax = (rows) => {
            let xarr = JSON.parse(JSON.stringify(CATTAX))
            for (let v of xarr) {
                delete v["vrnc"]
                delete v["status"]
            }
            let NUMBER_DECIMAL = NUMBER_DECIMAL_FORMAT
            if ($$("curr").getValue() == 'VND') NUMBER_DECIMAL = 0
            for (let row of rows) {
                let amt = Number((Math.round((row.amount ? row.amount : 0)*Math.pow(10,NUMBER_DECIMAL))/Math.pow(10,NUMBER_DECIMAL)).toFixed(NUMBER_DECIMAL)), vrt = row.vrt, vat = (row.vat ? row.vat : 0)
                const vatCode = row.vatCode
                let obj = xarr.find(x => x.vatCode == vatCode)
                if (typeof obj == "undefined") continue
                if (["fhs", "fbc"].includes(ENT)) {
                    amt = (amt / (1 + ((Number(obj.vrt) >= 0) ? Number(obj.vrt) : 0) / 100))

                }
                if (row.type == CK) amt = -amt
                obj.amt += amt
                if (amt == 0 && vat == 0) obj.isZero = true

                if (obj.hasOwnProperty("vat")) {
                    if (["fhs", "fbc"].includes(ENT)) {

                        if (amt != 0) obj.vat += amt * ((Number(vrt) >= 0) ? Number(vrt) : 0) / 100
                        else obj.vat = vat


                    } else {
                        if (ENT == 'mzh') {
                            if (amt != 0) obj.vat = Math.round(obj.vat * 100) / 100 + Math.round(vat * 100) / 100
                            else obj.vat = Math.round(vat * 100) / 100
                        } else {
                            if (amt != 0) obj.vat += (vat ? vat : amt * ((Number(vrt) >= 0) ? Number(vrt) : 0) / 100)
                            else obj.vat += vat
                        }


                    }

                }

            }
            let tar = xarr.filter(obj => { return (obj.amt !== 0 || obj.vat) || (obj.hasOwnProperty("isZero") && delete obj.isZero) })

            let exrt = $$("exrt").getValue()


            if (ENT == "vcm") {
                const score = $$("score").getValue()
                if (webix.rules.isNumber(score) && score != 0) {
                    let sum = 0, vat = 0, total = 0, val
                    for (const row of tar) {
                        total += row.amt
                        if (row.hasOwnProperty("vat")) total += row.vat
                    }
                    for (const row of tar) {
                        const vrt = Number(row.vrt), amt = row.amt
                        if (vrt > 0) {
                            val = 100 * score * (amt + row.vat) / ((100 + vrt) * total)
                            const VMT = (amt - val) * vrt / 100
                            row.vat = VMT
                            vat += VMT
                        }
                        else val = score * amt / total
                        const AMT = amt - val
                        row.amt = AMT
                        sum += AMT
                    }

                }
            }
            if (webix.rules.isNumber(exrt)) {
                for (let row of tar) {
                    row.amtv = Math.round(row.amt * exrt)
                    if (row.hasOwnProperty("vat")) row.vatv = Math.round(row.vat * exrt)
                }
            }
            return tar
        }

        $$("adj:btnsave").attachEvent("onItemClick", () => {
            let vform = $$("form").getValue()
            let vserial = $$("serial").getValue()
            let vlable = _("invoice_save_confirm2").replace('@@', vform + '-' + vserial)
            webix.confirm(vlable, "confirm-warning").then(() => {
                if (!$$("serial").getText()) {
                    this.webix.message(_("null_serial"), "error")
                    return
                }
                if (!form.validate()) return
                let items = grid.serialize(), arr = [], cka = 0, ckd = 0
                const exrt = $$("exrt").getValue()
                for (let item of items) {
                    delete item["id"]
                    if (item.da < 0) cka++

                    else ckd++
                    arr.push({ row: item.row, name: item.name, vrn: item.vrn, vrt: item.vrt, vatCode: item.vatCode, type: item.type, unit: item.unit, price: item.p, quantity: item.q, amount: item.a, amountv: Math.round(item.a * exrt) })
                }

                //if (ENT == 'vcm') {
                    if (cka > 0 && ckd > 0) {
                        this.webix.message(_("save_not_adjust"), "error")
                        return
                    }
                //}
                let _serial23char = vserial.slice(1,3)
                let inv = form.getValues(), pays = inv.pays, dif = inv.dif
                inv.name =$$("type").getText().toUpperCase()
                let format = webix.Date.dateToStr("%Y%m%d")
                let curd = format(new Date())
                let invoice_date = format($$("idt").getValue())
                let eff_year = invoice_date .slice(2,4)

                if(eff_year != _serial23char){
                    this.webix.message(_("year_of_idt_must_belong_year_of_serial"), "error")
                    return
                }

                if (Number(invoice_date) > Number(curd)) {
                    this.webix.message(_("idt_notgreater_curdt"), "error")
                    return
                }
                if (ENT == 'mzh' && inv.c0 == '') inv.c0 = '0'
                inv.items = items
                inv.hascode = SESS_STATEMENT_HASCODE
                inv.tax = tax(items)
                const searchrow = (arr, line) => {
                    for (let i = 0; i < arr.length; i++) {
                        if (arr[i].row === line) {
                            return arr[i]
                        }
                    }
                }
                let items0 = grid0.serialize(), j = 1, hd = {}
                for (let item of items0) {
                    let ok = searchrow(arr, item.line)
                    if (typeof ok === "undefined") arr.push({ name: item.name, vrn: item.vrn, vrt: item.vrt, vatCode: item.vatCode, type: item.type, unit: item.unit, price: item.price, quantity: item.quantity, amount: item.amount })
                }
                arr = arr.filter(function (obj) {
                    return obj.amount >= 0
                })

                if (type !== "01GTKT") {
                    for (var i = 0; i < vats.length; i++) {
                        delete inv[vats[i]]
                    }
                    for (let item of arr) {
                        item.line = j++
                        delete item["row"]
                        delete item["vrt"]
                        delete item["vrn"]
                    }
                }
                else {
                    for (let item of arr) {
                        item.line = j++
                        delete item["row"]
                    }

                    hd.tax = tax(arr)

                    if (inv.sum == 0) {
                        hd.vat = pays.vat
                        hd.vatv = pays.vatv
                    } else {
                        hd.vat = pays.vat - dif.vat
                        hd.vatv = pays.vatv - dif.vatv
                    }
                }

                hd.items = arr
                if (inv.sum == 0) {
                    hd.sum = pays.sum
                    hd.sumv = pays.sumv
                    hd.total = pays.total
                    hd.totalv = pays.totalv
                } else {
                    hd.sum = pays.sum - dif.sum
                    hd.sumv = pays.sumv - dif.sumv
                    hd.total = pays.total - dif.total
                    hd.totalv = pays.totalv - dif.totalv
                }
                if (ENT == "scb" && hd.totalv < 0) {
                    webix.message(_("check_fail"), "error")
                    return
                }
                hd.word = toWords(hd.totalv)
                inv.word = toWords(inv.totalv)
                inv.new = hd
                if (ENT == "vib") {
                    inv.id_file = $$("id_file").getValue()
                }
                // if (ENT == "vcm") {
                //     console.log(inv)
                //     const seqs = inv.adj.seq.split("-")
                //     if (inv.sum == 0 && inv.vat == 0) {
                //         inv.adj.des = `Điều chỉnh thông tin cho hóa đơn số ${seqs[2]} ký hiệu ${seqs[1]} mẫu số ${seqs[0]} ngày ${webix.i18n.dateFormatStr(new Date(inv.adj.idt))}`
                //         inv.adjType=4
                //     }
                //     else {
                //        // if (inv.dif.total > 0) inv.adj.des = inv.adj.des.replace('Điều chỉnh', 'Điều chỉnh giảm')
                //        // if (inv.dif.total < 0) inv.adj.des = inv.adj.des.replace('Điều chỉnh', 'Điều chỉnh tăng')
                //         if (inv.dif.total > 0) {
                //             inv.adj.des = inv.adj.des.replace('Điều chỉnh', 'Điều chỉnh giảm')
                //             inv.adjType=3
                //             inv.word = toWords(inv.totalv * -1)
                //         }
                //         if (inv.dif.total < 0) {
                //             inv.adjType=2
                //             inv.adj.des = inv.adj.des.replace('Điều chỉnh', 'Điều chỉnh tăng')
                //         }
                //     }

                // }
                let adj_rea = $$("adj.rea").getValue()

                if (inv.sum == 0 && inv.vat == 0) {
                    inv.adj.des = `Điều chỉnh thông tin ` + adj_rea + ` cho hóa đơn số ${inv.adj.seq} ký hiệu ${inv.adj.serial} mẫu số ${inv.adj.form} ngày ${formatDate(new Date(inv.adj.idt))}`
                    inv.adjType=4
                }
                else {
                    if (inv.dif.total > 0) {
                        inv.adj.des = `Điều chỉnh giảm ` + adj_rea + ` cho hóa đơn số ${inv.adj.seq} ký hiệu ${inv.adj.serial} mẫu số ${inv.adj.form} ngày ${formatDate(new Date(inv.adj.idt))}`
                        inv.adjType=3
                        inv.word = toWords(inv.totalv * -1)
                    }
                    if (inv.dif.total < 0) {
                        inv.adjType=2
                        inv.adj.des = `Điều chỉnh tăng ` + adj_rea + ` cho hóa đơn số ${inv.adj.seq} ký hiệu ${inv.adj.serial} mẫu số ${inv.adj.form} ngày ${formatDate(new Date(inv.adj.idt))}`
                    }
                }

                if (ENT == "hdb") {
                    for (let i in inv) {
                        if (i != "items") { if (typeof inv[i] == "string") inv[i] = inv[i].toString().toUpperCase() }
                        else inv[i].forEach(e => { e.name = e.name.toString().toUpperCase() })
                    }
                }
                $$("adj:btnsave").disable()
                if (action == "create") {
                    inv.pid = this.id
                    webix.ajax().post("api/adj", inv).then(result => {
                        let iid = result.json()
                        if (ENT == 'hlv') {
                            webix.message(`${_("adj_inv")}`, "success", 10000)
                            this.app.show("/top/inv.invs")
                        } else {
                            if (iid) {

                                // history.back()

                                //const files = uploader.files, file_id = files.getFirstId()
                                // if (file_id) {
                                //     upload(iid,0)
                                // }
                                //  else{
                                webix.message(`${_("adj_inv")} ID: ${iid}`, "success", 10000)
                                this.app.show("/top/inv.invs")
                                // }


                            }
                        }


                    }).catch(e => {
                        $$("adj:btnsave").enable()
                    })
                }
                else {
                    webix.ajax().put(`api/adj/${this.id}`, inv).then(result => {
                        if (result.json()) {

                            // const files = uploader.files, file_id = files.getFirstId()
                            // if (file_id) {
                            //     upload(this.id,1)
                            // }
                            //  else{
                            webix.message(_("invoice_updated"), "success")
                            this.app.show("/top/inv.invs")
                            // }
                            // history.back()


                        }
                    }).catch(e => {
                        $$("adj:btnsave").enable()
                    })
                }

            })
        })
        const upload = (id, type) => {
            const files = uploader.files, file_id = files.getFirstId()
            if (file_id) {
                var params = { type: "ADJ", id: id }
                uploader.define("formData", params)
                uploader.send(rs => {
                    if (type == 1) webix.message(_("invoice_updated"), "success")
                    if (type == 0) webix.message(`${_("adj_inv")} ID: ${id}`, "success", 10000)
                    if (rs.err) webix.message(rs.err, "error")
                    else if (rs.ok) webix.message(_("up_minutes_success"), "success")
                    else webix.message(_("up_minutes_fail"), "error")
                    uploader.setValue("")
                    this.app.show("/top/inv.invs")
                })
            }

        }
        $$("adj:file").attachEvent("onAfterFileAdd", (file) => {
            webix.confirm(_("up_minutes_confirm"), "confirm-warning").then(() => {
                const files = uploader.files, file_id = files.getFirstId()
                if (file_id) {
                    let iid
                    if (action == "create") iid = this.id
                    else {
                        let inv = form.getValues()
                        iid = inv.pid
                    }
                    if (ENT == 'vib') {
                        if (action == "create") iid = ''
                        else {
                            iid = this.id
                        }
                    }
                    var params = { type: "ADJ", id: iid }
                    uploader.define("formData", params)
                    $$("adj:btnsave").disable()
                    webix.message(_("uploading"), "info", -1, "upload_title");
                    uploader.send(rs => {
                        if (rs.err) {
                            $$("inv:file").setValue("");
                            webix.message(rs.err, "error")
                        }
                        else if (rs.ok) {
                            if (ENT == 'vib') {//xu ly day nhieu file
                                let arr = [], id_file = $$("id_file").getValue()
                                if (id_file) {
                                    arr.push(id_file)
                                    arr.push(rs.id_f)
                                    $$("id_file").setValue(arr)
                                }
                                else {
                                    arr.push(rs.id_f)
                                    $$("id_file").setValue(arr)
                                }
                            }

                            webix.message(_("up_minutes_success"), "success")
                        }
                        else {
                            webix.message(_("up_minutes_fail"), "error")
                            $$("inv:file").setValue("");
                        }

                        webix.message.hide("upload_title");
                        $$("adj:btnsave").enable()
                    })
                }
                else webix.message(_("file_required"))
            }).fail(function () {
                $$("adj:btnsave").enable()
                uploader.setValue("");

            });
        })
        //Sửa SUM + VAT
        $$("sum").attachEvent("onChange", newv => {
            // $$("total").setValue(newv + parseInt($$("vat").getValue()))
            if (ENT != 'mzh') $$("total").setValue(newv + $$("vat").getValue())

        })
        $$("vat").attachEvent("onChange", newv => {
            // $$("total").setValue(newv + parseInt($$("sum").getValue()))
            //    $$("total").setValue(newv + $$("sum").getValue())
            //    $$("totalv").setValue($$("exrt").getValue() * $$("total").getValue())
        })
        if (ENT == "vcm") {
            $$("score").attachEvent("onChange", newv => {
                let items = grid.serialize(), arr = []
                let sum = 0, vat = 0, total = 0, val, sumv = 0, totalv = 0, vatv = 0


                const exrt0 = $$("exrt0").getValue(), exrt = $$("exrt").getValue()
                let dif = form.getValues().dif, xs = 0, xt = 0, xv = 0, score = 0, totalck = 0
                if (dif) {
                    if (exrt0 !== exrt) {
                        xs = dif.xs
                        xt = dif.xt
                        if (type == "01GTKT") xv = dif.xv
                    }
                }
                else {
                    dif = {}
                }

                for (let item of items) {
                    sum += item.amount
                    total += item.amount
                    if (item.hasOwnProperty("vat")) {
                        total += item.vat
                        vat += item.vat
                    }
                }
                if (newv >= total) {
                    $$("score").setValue(0)
                    return
                }

                totalck = total
                vat = vat - newv * vat / total
                vatv = Math.abs(Math.round(vat * exrt + xs))
                total = Math.abs(total) - newv
                totalv = Math.abs(Math.round(total * exrt + xt))
                sum = Math.abs(sum - newv * sum / totalck)
                sumv = Math.abs(Math.round(sum * exrt + xs))


                $$("total").setValue(total)

                $$("sumv").setValue(sumv)
                $$("totalv").setValue(totalv)
                $$("word").setValue(toWords(totalv))
                $$("sum").setValue(sum)
                $$("vat").setValue(vat)
                $$("vatv").setValue(vatv)
            })
        }

        $$("adj:btnorgs").attachEvent("onItemClick", () => {
            this.IorgsWin.show()
        })

    }
}