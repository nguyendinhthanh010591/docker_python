import { JetView } from "webix-jet"
import { LinkedInputs, w, setConfStaFld } from "models/util"
export default class IorgView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        webix.protoUI({ name: "dependent", $cssName: "combo" }, LinkedInputs, webix.ui.combo)
        const form = {
            view: "form",
            id: "iorg:form",
            padding: 3,
            margin: 3,
            width: w,
            elementsConfig: { labelWidth: 85 },
            elements:
                [
                    { name: "name", label: _("customer_name"), view: "text", attributes: { maxlength: 255 }, required: true },
                    {
                        cols: [
                            { name: "taxc", label: _("taxcode"), view: "text", attributes: { maxlength: 14 }, validate: v => { return (!v || checkmst(v)) }, invalidMessage: _("taxcode_invalid") },
                            { name: "name_en", label: _("customer_name_en"), view: "text", attributes: { maxlength: 255 } },
                        ]
                    },
                    { name: "addr", label: _("address"), view: "text", attributes: { maxlength: 255 }, required: true },
                    {
                        cols: [
                            { id: "iorg:prov", name: "prov", view: "combo", label: _("city"), suggest: { url: "api/cat/kache/prov" } },
                            { id: "iorg:dist", name: "dist", view: "dependent", label: _("district"), master: "iorg:prov", dependentUrl: "api/cat/kache2/local/", options: [] },
                        ]
                    },
                    {
                        cols: [
                            { id: "iorg:ward", name: "ward", view: "dependent", label: _("ward"), master: "iorg:dist", dependentUrl: "api/cat/kache2/local/", options: [] },
                            { name: "code", label: _("customer_code2"), view: "text", attributes: { maxlength: 50 }, required: true },
                        ]
                    },
                    {
                        cols: [
                            { rows: [{ name: "mail", label: "Mail", view: "multitext", attributes: { type: "email", maxlength: 255 } }] },
                            { rows: [{ name: "tel", label: _("tel"), view: "multitext", attributes: { type: "tel", maxlength: 255 } }] }
                        ]
                    },
                    {
                        cols: [
                            { name: "acc", label: _("account"), view: "text", attributes: { maxlength: 255 } },
                            { name: "bank", label: _("bank"), view: "text", attributes: { maxlength: 255 } },
                        ]
                    },
                    { id: "iorg:r0", hidden: true },
                    { id: "iorg:r1", hidden: true },
                    { id: "iorg:r2", hidden: true },
                    { id: "iorg:r3", hidden: true },
                    { id: "iorg:r4", hidden: true },
                    { cols:[{},{cols: [{}, { view: "button", id: "iorg:btnsave", type: "icon", icon: "mdi mdi-database-edit", label: _("save"), width: 100 }]}] },
                    { height: 33 },
                    { fillspace:true}
                ],
            rules: {
                name: webix.rules.isNotEmpty,
                addr: webix.rules.isNotEmpty
            },
            on: {
                onAfterValidation: (result) => {
                    if (!result) webix.message(_("required_msg"))
                }
            }
        }
        return form
    }
    ready(v, urls) {
        let lang = webix.storage.local.get("lang")
        webix.ajax().post(`api/cus`,{itype:'00ORGS',lang:lang}).then(result => {
            const pos = 7
            let cols = result.json(), form = $$("iorg:form"), len = cols.length
            for (let i = 0; i < 5; i++) {
                form.removeView(`iorg:r${i}`)
            }
            if (len > 0) {
                for (const col of cols) {
                    col.id=`iorg:${col.id}`
                    if (col.view == "datepicker") col.format = webix.i18n.dateFormatStr
                }
                if (len % 2) {
                    cols.push({})
                    len++
                }
                const j = len / 2
                for (let i = 0; i < 5; i++) {
                    if (i <= j) form.addView({ id: `iorg:r${i}`, cols: cols.splice(0, 2) }, pos + i)
                    else form.addView({ id: `iorg:r${i}`, hidden: true }, pos + i)
                }
            }
            else {
                for (let i = 0; i < 5; i++) {
                    form.addView({ id: `iorg:r${i}`, hidden: true }, pos + i)
                }
            }
        })
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
    init() {
        let form = $$("iorg:form")
        $$("iorg:btnsave").attachEvent("onItemClick", () => {
            if (form.validate()) {
                let data = form.getValues()
                data.webix_operation = 'insert'
                webix.ajax().post("api/org", data).then(result => {
                    const obj = result.json()
                    webix.message(_("customers_insert_ok"), "success")
                    if (obj.id) {
                        webix.ajax(`api/org/fbi/${obj.id}`).then(result => {
                            const arr = result.json(), rec = arr[0]
                            $$("bname").setValue(rec.name)
                            $$("btax").setValue(rec.taxc)
                            $$("baddr").setValue(rec.fadd)
                            $$("btel").setValue(rec.tel)
                            $$("bmail").setValue(rec.mail)
                            $$("bacc").setValue(rec.acc)
                            $$("bbank").setValue(rec.bank)
                            $$("bcode").setValue(rec.code)
                            $$("iorg:win").hide()
                        })
                    }
                })
            }
        })
    }
}   