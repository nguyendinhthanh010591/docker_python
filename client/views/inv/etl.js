import { JetView } from "webix-jet"
import { size,  LinkedInputs3,ENT, setConfStaFld } from "models/util"
import Iwin from "views/inv/iwin"
export default class EtlView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        webix.protoUI({ name: "dependent3", $cssName: "richselect" }, LinkedInputs3, webix.ui.richselect)
        webix.proxy.etl = {
            $proxy: true,
            load: function (view, params) {
                let obj = $$("etl:form").getValues()
                Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                if (!params) params = {}
                params.filter = obj
                return webix.ajax(this.source, params)
            }
        }
        let form = {
            view: "form",
            id: "etl:form",
            padding: 3,
            margin: 3,
            elements:
                [
                    {
                        cols: [
                            { id: "etl:countetl", view: "label", width: 150 },{},{},{},
                            { id: "td", name: "td", view: "datepicker", label: _("date"), type: "icon", icon: "mdi", stringResult: true },
                            { view: "button", id: "etl:btnsearch", type: "icon", icon: "wxi-search", label: _("search"), width: 100 },
                            { view: "button", id: "etl:btnresetetl", type: "icon", icon: "mdi mdi-content-duplicate", label: _("reset_etl"), width: 100 }
                        ]
                    }
                ]
        }
        let columns = [
            { id: "chk", header: { content: "masterCheckbox", css: "center" }, adjust: "header", css: "center", template: "{common.checkbox()}"},
            { id: "sid", header: { text: "SID", css: "header" }, adjust: true },
            { id: "prs", header: { text: "PROCESS_STATUS", css: "header"  },  adjust: true ,width: 200},
            { id: "prd", header: { text: "PROCESS_DESC", css: "header" }, adjust: true ,width: 200},
            { id: "lg", header: { text: "LOG_DATE", css: "header" }, adjust: true ,width: 200},
            
            
        ]
        let pager = { view: "pager", id: "etl:pager", size: size, template: `{common.first()}{common.prev()} {common.pages()}{common.next()}{common.last()}` }

        let grid = {
            view: "datatable",
            id: "etl:grid",
            select: "row",
            resizeColumn: true,
            editable: false,
            sort: false,
            pager: "etl:pager",
            on: { onBeforeSort: function () { return false } },
            columns: columns
        }
        let title_page = { view: "label", label:`<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("Error_management_integration")}</span>`}
        return { paddingX: 2, rows: [title_page, form, grid, pager] }
    }
    
    ready(v, urls) {
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
    
    init() {
        this.Iwin = this.ui(Iwin)
        webix.ajax("api/sysdate").then(result => {
            let dt = new Date(result.json())
            $$("td").setValue(dt)
            // $$("syslog:dt").getPopup().getBody().define("minDate", dt)
        })
        let grid = $$("etl:grid")
        webix.extend(grid, webix.ProgressBar)
        const search = () => {
            grid.clearAll()
            grid.loadNext(size, 0, null, "etl->api/etl", true)
        }
        grid.config.multiselect = false
        grid.refresh()
        $$("etl:btnsearch").attachEvent("onItemClick", () => {
            search()
        })
        $$("etl:btnresetetl").attachEvent("onItemClick", () => {

            let items = grid.serialize(), arr = []
            for (let item of items) {
                if (item && item.chk) arr.push(item.sid)
            }
            if (arr.length < 1) {
                webix.message(_("must_select_integration_error"), "error")
                return false
            }
            webix.confirm(_("etl_confirm"), "confirm-warning").then(() => {
                webix.ajax().post("api/etl", { ids: arr }).then(result => {
                    const rows = result.json()
                        webix.alert({
                            title:_("success"),
                            ok:"Ok",
                            text:`${_("reseted_etl")} ${rows} ${_("integration_error")}`
                        })
                        search()
                    }).catch(e => {
                        search()
                    })
            })
        })
        grid.attachEvent("onAfterLoad", function () {
            let self = this, count = self.count()
            $$("etl:countetl").setValue(`${_("found")} ${count}`)
            if (count) self.hideOverlay()
            else self.showOverlay(_("notfound"))
        })
      
    }
}