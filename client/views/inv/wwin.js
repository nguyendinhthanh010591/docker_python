import { JetView } from "webix-jet"
import { d2s, ISTATUS, ENT, setConfStaFld, ROLE, WRONGNOTICE_CQT_STATUS } from "models/util"

export default class Wwin extends JetView {
    config() {
        _ = this.app.getService("locale")._
        let columns = [
            { header: { text: "", css: "header" }, width: 35, template: `<span class='webix_icon wxi-search', title='${_("view")}'></span>` },
            { id: "id", header: { text: "ID", css: "header" }, adjust: true, editor: "text" },
            { id: "noti_dt", header: { text: _("noti_dt"), css: "header" }, adjust: true, format: webix.i18n.dateFormatStr },
            { id: "status", header: { text: _("status"), css: "header" }, adjust: true, minWidth: 220, options: [
                {id:"SENT", value:_("sent")},
                {id:"WAIT", value:_("pending")},
                {id:"TAX_ACCEPT", value:_("cqt_accepted")},
                {id:"TAX_DENY", value:_("cqt_not_accepted")},
            ] },
            { id: "status_cqt", header: { text: _("status_cqt"), css: "header" },collection: WRONGNOTICE_CQT_STATUS(this.app), adjust: true, minWidth: 220 },
            { id: "dien_giai", header: { text: _("CQT_reply"), css: "header" }, adjust: true, editor: "text" },
        ]
        
        function show_progress_icon(delay) {
            $$("wwin:grid").showProgress({
                type: "icon",
                delay: delay,
                hide: false
            })
        };
        const grid = {
            view: "datatable",
            id: "wwin:grid",
            columns: columns,
            onClick: {
                "wxi-search": (a, id, c) => {
                    show_progress_icon(1)
                    webix.ajax(`api/wno/htm/${id}`).then(result => {
                        let pdf = result.json()
                        let b64 = pdf.pdf
                        const blob = dataURItoBlob(b64);
                        
                        var temp_url = window.URL.createObjectURL(blob);
                        $$("viewwn:ipdf").define("src", temp_url)
                        $$("viewwn:win").show()
                        webix.message.hide("exp");
                        $$("wwin:grid").hideProgress()
                    })
                }
            }
        }
        return {
            view: "window",
            id: "wwin:win",
            modal: true,
            fullscreen: true,
            head: {
                view: "toolbar"
                , height: 35
                , cols: [
                    { view: "label", label: _("wrong_notice_similar") ,id:"wwin:winlb"},
                    {
                        cols: [
                            {},
                            { view: "button", type: "icon", icon: "wxi-close", hotkey: "esc", width: 30, click: () => $$("wwin:win").hide() }
                        ], fillspace: true, align: "right"
                    }
                ]
            },
            body: grid
        }
    }
    ready(v, urls) {
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
    init() {
        webix.extend($$("wwin:grid"), webix.ProgressBar);
    }
    show() {
        this.getRoot().show()
    }
}    