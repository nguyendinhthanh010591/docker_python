import { JetView } from "webix-jet"
import {
    w, h, intf,gridnf, gridnf0, gridnf3, gridnf4, textnfconfnumberv, textnf, isEmpty, VAT,
    PAYMETHOD, ITYPE, LTYPE, MAX, KMMT, CK, ROLE, CARR, ENT, setConfStaFld, gridnfconf, 
    CONFIG_CHIETKHAU_COL, CATTAX, NUMBER_DECIMAL_FORMAT,SESS_STATEMENT_HASCODE, 
    DEGREE_CONFIG
} from "models/util"
import { toWords } from "models/n2w"
import IorgView from "views/inv/iorg"
import IorgsView from "views/inv/iorgs"
import IgnsView from "views/inv/igns"
import Iwin from "views/inv/iwin"
//let aia_atr = [{ "fldid": "inv:r2", "fldtyp": "affectMany", "atr": { "disabled": true }, "feature": { "id": "*" } }, { "fldid": "inv:r3", "fldtyp": "affectMany", "atr": { "disabled": true }, "feature": { "id": "*" } }, { "fldid": "bname", "fldtyp": "other", "atr": { "disabled": true }, "feature": { "id": "*" } }, { "fldid": "inv:r4", "fldtyp": "affectMany", "atr": { "disabled": true }, "feature": { "id": "*" } }, { "fldid": "inv:r5", "fldtyp": "affectMany", "atr": { "disabled": true }, "feature": { "id": "*" } }, { "fldid": "inv:r6", "fldtyp": "affectMany", "atr": { "disabled": true }, "feature": { "id": "*" } }, { "fldid": "del", "fldtyp": "datagrid", "atr": { "hidden": true, "pid": "inv:form" }, "feature": { "id": "*" } }, { "fldid": "inv:r8", "fldtyp": "other", "atr": { "disabled": true }, "feature": { "id": "*" } }, { "fldid": "inv:r9", "fldtyp": "other", "atr": { "disabled": true }, "feature": { "id": "*" } }, { "fldid": "inv:r10", "fldtyp": "other", "atr": { "disabled": true }, "feature": { "id": "*" } }, { "fldid": "inv:r11", "fldtyp": "other", "atr": { "disabled": true }, "feature": { "id": "*" } }, { "fldid": "bmail", "fldtyp": "other", "atr": { "disabled": false }, "feature": { "id": "*" } }, { "fldid": "btel", "fldtyp": "other", "atr": { "disabled": false }, "feature": { "id": "*" } }, { "fldid": "btax", "fldtyp": "other", "atr": { "disabled": false }, "feature": { "id": "*" } }, { "fldid": "baddr", "fldtyp": "other", "atr": { "disabled": false }, "feature": { "id": "*" } }, { "fldid": "buyer", "fldtyp": "other", "atr": { "disabled": false }, "feature": { "id": "*" } }, { "fldid": "c6", "fldtyp": "other", "atr": { "disabled": true }, "feature": { "id": "*" } }, { "fldid": "bname", "fldtyp": "other", "atr": { "disabled": false }, "feature": { "id": "*" } }]
let vib_atr = [{ "fldid": "inv:r2", "fldtyp": "affectMany", "atr": { "disabled": true }, "feature": { "id": "*" } }, { "fldid": "inv:r3", "fldtyp": "affectMany", "atr": { "disabled": true }, "feature": { "id": "*" } }, { "fldid": "bname", "fldtyp": "other", "atr": { "disabled": true }, "feature": { "id": "*" } }, { "fldid": "inv:r4", "fldtyp": "affectMany", "atr": { "disabled": true }, "feature": { "id": "*" } }, { "fldid": "inv:r5", "fldtyp": "affectMany", "atr": { "disabled": true }, "feature": { "id": "*" } }, { "fldid": "inv:r6", "fldtyp": "affectMany", "atr": { "disabled": true }, "feature": { "id": "*" } }, { "fldid": "del", "fldtyp": "datagrid", "atr": { "hidden": true, "pid": "inv:form" }, "feature": { "id": "*" } }, { "fldid": "inv:r8", "fldtyp": "other", "atr": { "disabled": true }, "feature": { "id": "*" } }, { "fldid": "inv:r9", "fldtyp": "other", "atr": { "disabled": true }, "feature": { "id": "*" } }, { "fldid": "inv:r10", "fldtyp": "other", "atr": { "disabled": true }, "feature": { "id": "*" } }, { "fldid": "inv:r11", "fldtyp": "other", "atr": { "disabled": true }, "feature": { "id": "*" } }, { "fldid": "bmail", "fldtyp": "other", "atr": { "disabled": false }, "feature": { "id": "*" } }, { "fldid": "btel", "fldtyp": "other", "atr": { "disabled": false }, "feature": { "id": "*" } }, { "fldid": "btax", "fldtyp": "other", "atr": { "disabled": false }, "feature": { "id": "*" } }, { "fldid": "baddr", "fldtyp": "other", "atr": { "disabled": false }, "feature": { "id": "*" } }, { "fldid": "buyer", "fldtyp": "other", "atr": { "disabled": false }, "feature": { "id": "*" } }]
let vib_atr_enable = [{ "fldid": "inv:r2", "fldtyp": "affectMany", "atr": { "disabled": false }, "feature": { "id": "*" } }, { "fldid": "inv:r3", "fldtyp": "affectMany", "atr": { "disabled": false }, "feature": { "id": "*" } }, { "fldid": "bname", "fldtyp": "other", "atr": { "disabled": false }, "feature": { "id": "*" } }, { "fldid": "inv:r4", "fldtyp": "affectMany", "atr": { "disabled": false }, "feature": { "id": "*" } }, { "fldid": "inv:r5", "fldtyp": "affectMany", "atr": { "disabled": false }, "feature": { "id": "*" } }, { "fldid": "inv:r6", "fldtyp": "affectMany", "atr": { "disabled": false }, "feature": { "id": "*" } }, { "fldid": "del", "fldtyp": "datagrid", "atr": { "hidden": false, "pid": "inv:form" }, "feature": { "id": "*" } }, { "fldid": "inv:r8", "fldtyp": "other", "atr": { "disabled": false }, "feature": { "id": "*" } }, { "fldid": "inv:r9", "fldtyp": "other", "atr": { "disabled": false }, "feature": { "id": "*" } }, { "fldid": "inv:r10", "fldtyp": "other", "atr": { "disabled": false }, "feature": { "id": "*" } }, { "fldid": "inv:r11", "fldtyp": "other", "atr": { "disabled": false }, "feature": { "id": "*" } }, { "fldid": "bmail", "fldtyp": "other", "atr": { "disabled": false }, "feature": { "id": "*" } }, { "fldid": "btel", "fldtyp": "other", "atr": { "disabled": false }, "feature": { "id": "*" } }, { "fldid": "btax", "fldtyp": "other", "atr": { "disabled": false }, "feature": { "id": "*" } }, { "fldid": "baddr", "fldtyp": "other", "atr": { "disabled": false }, "feature": { "id": "*" } }, { "fldid": "buyer", "fldtyp": "other", "atr": { "disabled": false }, "feature": { "id": "*" } }]
const MODULE = [
    { id: "GL", value: "GL" },
    { id: "TF", value: "TF" },
    { id: "FT", value: "FT" },
    { id: "CL", value: "CL" },
    { id: "RB", value: "RB" },
    { id: "FX", value: "FX" },
    { id: "BT", value: "BT" },
    { id: "MM", value: "MM" },
    { id: "BS", value: "BS" }

]
const ADJ_TYP = [
    {id:1,value:_("replace")},
    {id:2,value:_("adjust_increase")},
    {id:3,value:_("adjust_decrease")},
    {id:4,value:_("adjust_inf")}
]
let parse = false, action, type, URLSS, createAdjDif = false
const VCM = (ENT == "vcm")
const getvrn = (vatCode) => {
    let obj = CATTAX.find(x => x.vatCode == vatCode)
    /*
    for (let i = 0; i < arr.length; i++) {
        const obj = arr[i]
        if (obj.id == id) return obj.value
    }
    */
    if (obj) return obj.vrn
    return ""
}
const getvatCode = (vrt) => {
    let obj = CATTAX.find(x => x.vrt == vrt)
    /*
    for (let i = 0; i < arr.length; i++) {
        const obj = arr[i]
        if (obj.id == id) return obj.value
    }
    */
    if (obj) return obj.vatCode
    return ""
}
const new_form = () => {
    webix.ajax("api/sysdate").then(dt => {
        const sysdate = dt.json()
        let grid = $$("inv:grid"), form = $$("inv:form")
        form.setValues({ idt: sysdate, orddt: sysdate, curr: "VND", exrt: 1, paym: "*", type: type, idt_: sysdate })
        let i = 0
        if (ENT == "scb" || ENT == "bvb") {
            while (i < 3) {
                grid.add({ vatCode: "VAT1000", vrt: 10, quantity: "" })
                i++
            }
        } else {
            while (i < 3) {
                grid.add({ vatCode: "VAT1000", vrt: 10, quantity: 1 })
                i++
            }
        }
        grid.clearValidation()
    })
}

const reset_grid = (result) => {

    const cols = result.json(), grid = $$("inv:grid")
    let arr = grid.config.columns.filter(item => !CARR.includes(item.id))
    if (cols.length > 0) {
        let c = 6
        if (["fhs", "fbc"].includes(ENT)) c = 8


        for (const col of cols) {
            if (col.editor == "date") {
                col.format = webix.i18n.dateFormatStr
            }
            // else if (col.editor == "number" && ["fhs","fbc"].includes(ENT)) {
            //     col.editor = "text"
            //     if (col.css) {
            //         col.format = gridnf4.format
            //         col.editParse = gridnf4.editParse
            //         col.editFormat = gridnf4.editFormat


            //     }
            // }
            else if (col.editor = "text") {
                if (col.css) {
                    col.format = gridnfconf.format
                    col.editParse = gridnfconf.editParse
                    col.editFormat = gridnfconf.editFormat
                }
            }
            if (["ctbc"].includes(ENT)) {
                if (col.id == 'c3') {
                    col.format = gridnf0.format
                    col.editParse = gridnf0.editParse
                    col.editFormat = gridnf0.editFormat
                }
            }
            arr.splice(c++, 0, col)
        }
    }
    grid.config.columns = arr
    grid.refreshColumns()
    if (type == "01GTKT" || type =="01/TVE") {
        if (!grid.isColumnVisible("vatCode")) grid.showColumn("vatCode")
    }
    else {
        if (grid.isColumnVisible("vatCode")) grid.hideColumn("vatCode")
    }
    if (type == "02GTTT" || type == "07KPTQ" || type=="02/TVE") {
        if (grid.isColumnVisible("vat")) grid.hideColumn("vat")
        if (grid.isColumnVisible("total")) grid.hideColumn("total")
    }
    else {
        if (!grid.isColumnVisible("vat")) grid.showColumn("vat")
        if (!grid.isColumnVisible("total")) grid.showColumn("total")
    }
    if (type == "03XKNB" || type == "04HGDL") {
        if (grid.isColumnVisible("discountamt")) grid.hideColumn("discountamt")
        if (grid.isColumnVisible("discountrate")) grid.hideColumn("discountrate")
        if (grid.isColumnVisible("vat")) grid.hideColumn("vat")
        if (grid.isColumnVisible("total")) grid.hideColumn("total")
    }
    if (["ctbc"].includes(ENT)) {
        if (grid.isColumnVisible("discountamt")) grid.hideColumn("discountamt")
        if (grid.isColumnVisible("discountrate")) grid.hideColumn("discountrate")
    }
}

const reset_form = (result) => {
    let form = $$("inv:form")
    form.removeView("inv:r6")
    form.removeView("inv:r7")

    const pos = 3
    let cols = result.json(), len = cols.length

    if (len > 0) {
        for (const col of cols) {

            if (col.view == "datepicker") col.format = webix.i18n.dateFormatStr
        }
        if (len <= 5) {
            while (len < 5) {
                cols.push({})
                len++
            }
            form.addView({ id: "inv:r6", cols: cols }, pos + 1)
            form.addView({ id: "inv:r7", hidden: true }, pos + 2)
        }
        else {
            while (len < 10) {
                cols.push({})
                len++
            }
            form.addView({ id: "inv:r6", cols: cols.splice(0, len / 2) }, pos + 1)
            form.addView({ id: "inv:r7", cols: cols }, pos + 2)
        }
    }
    else {
        form.addView({ id: "inv:r6", hidden: true }, pos + 1)
        form.addView({ id: "inv:r7", hidden: true }, pos + 2)
    }
    const mode = (type == "03XKNB" || type == "04HGDL") ? "pxk" : "hd"
    $$("inv:r1").showBatch(mode)
    $$("inv:r2").showBatch(mode)
    $$("inv:r3").showBatch(mode)
    $$("inv:r4").showBatch(mode)
    $$("inv:r5_1").showBatch(mode)
    if (type == "01GTKT" || type =="01/TVE") {
        $$("inv:r10").show()
        if (VCM) $$("score").show()
        else $$("score").hide()
    }
    else if (type == "02GTTT"|| type == "07KPTQ" || type=="02/TVE") {
        $$("inv:r10").hide()
    }
    else if (type == "03XKNB" || type == "04HGDL") {
        $$("inv:r10").hide()
    }
    else {
        $$("inv:r10").hide()
    }
    if (ENT == "scb") {
        if (action == "wcancel") {
            $$("inv:grid").hideColumn("del", { spans: true })
        }
    }

    //HLV onChange chon ServCode [c2:servCode, c7:servContent, c8:servName]
    if (ENT == "hlv") {
        // ["c7", "c8"].map((e) => $$(e).disable())
        $$("c2").attachEvent("onChange", (newv, oldv) => {
            if (!newv) ["c7", "c8"].map((e) => $$(e).setValue())
            webix.ajax(`api/kat/payments`).then(result => {
                let json = result.json(), val = $$("c2").getValue()
                let arr = json.filter((a) => { return a.name == val })
                if (arr.length > 0) {
                    let item = arr[0], des = JSON.parse(item.des), name = des.servicename, content = des.servicecontent
                    $$("c7").setValue(content)
                    $$("c8").setValue(name)
                }
            })
        })
    }
    // thinhpq10 cimb type customer onchange
    if (ENT == "cimb") {
        $$("c6").setValue("CN")
        $$("c6").define("required", true)
        $$("c7").define("required", true)
        $$("c6").refresh()
        $$("c7").refresh()
        $$("c6").attachEvent("onChange", (newv, oldv) => {
            if (!newv) {
                $$("c6").setValue("")
                $$("c7").hide()
            }
            if(newv=="CN") {
                $$("c7").show()
                $$("c7").define("required", true)
                $$("c7").refresh()
            } else {
                $$("c7").hide()
            }
        })
    }
    //
    if (ENT == "sgr") {

        $$("c7").attachEvent("onChange", (newv, oldv) => {
            const exrt = $$("exrt").getValue()
            const change = newv - oldv
            const total = $$("total").getValue()
            let newtotal = Number(change) + total
            let newtotalv = Math.round(newtotal * exrt)
            $$("total").setValue(newtotal)
            $$("totalv").setValue(newtotalv)
            $$("word").setValue(toWords(Math.abs(newtotalv)))

        })
        $$("c9").attachEvent("onChange", (newv, oldv) => {
            const exrt = $$("exrt").getValue()
            const change = newv - oldv
            const total = $$("total").getValue()
            let newtotal = Number(change) + total
            let newtotalv = Math.round(newtotal * exrt)
            $$("total").setValue(newtotal)
            $$("totalv").setValue(newtotalv)
            $$("word").setValue(toWords(Math.abs(newtotalv)))

        })
    }

    //Cau hinh rieng cac cot tĩnh
    if (URLSS) {
        webix.ajax().get("api/fld/conf", { url: URLSS[0].page, params: URLSS[0].params }).then(result => {
            let json = result.json()

            setConfStaFld(json)
        })
    }
    //
    // if (ENT == "scb") { $$("c0").config["required"] = true
    //     $$("c0").refresh()
    //     //$$("c2").config["required"] = true
    //    // $$("c2").refresh()
    //  }

}


class IgnsWin extends JetView {
    config() {
        return {
            view: "window",
            id: "igns:win",
            height: h,
            width: w,
            position: "center",
            resize: true,
            modal: true,
            head: {
                view: "toolbar", height: 40,
                css: 'toolbar_window',
                cols: [
                    { view: "label", label: _("gns_list") },
                    { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => { this.getRoot().hide() } }
                ]
            },
            body: IgnsView
        }
    }
    show() {
        this.getRoot().show()
    }
}

class IorgsWin extends JetView {
    config() {
        return {
            view: "window",
            id: "iorgs:win",
            height: h,
            width: w,
            position: "center",
            resize: true,
            modal: true,
            head: {
                view: "toolbar", height: 40, css: 'toolbar_window',
                cols: [
                    { view: "label", label: _("customers_list") },
                    { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => { $$("iorgs:win").hide() } }
                ]
            },
            body: IorgsView
        }
    }
    show() {
        this.getRoot().show()
    }
}

class IorgWin extends JetView {
    config() {
        return {
            view: "window",
            id: "iorg:win",
            height: h,
            width: w,
            position: "center",
            resize: true,
            modal: true,
            head: {
                view: "toolbar", height: 40, css: 'toolbar_window',
                cols: [
                    { view: "label", label: _("customers_insert") },
                    { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 25, click: () => { $$("iorg:win").hide() } }
                ]
            },
            body: IorgView
        }
    }
    init() {
        $$("iorg:win").attachEvent("onShow", () => {
            $$("iorg:form").focus()
        })
        $$("iorg:win").attachEvent("onHide", () => {
            $$("iorg:form").clear()
        })
    }
    show() {
        this.getRoot().show()
    }
}

export default class InvView extends JetView {
    config() {
        _ = this.app.getService("locale")._
     
        const LinkedInputs4 = {
            $init: function (config) {
                let master = $$(config.master)
                master.attachEvent("onChange", newv => {
                    let me = this, list
                    const all = { id: "", value: "" }
                    if (!parse) me.setValue()
                    if ((action !== "view") || ((action === "view" && ENT === "aia"))) {
                        list = me.getList()
                        list.clearAll()
                    }
                    if (!newv) return
                    if ((action !== "view") || ((action === "view" && ENT === "aia"))) {
                        list.load(config.dependentUrl + newv).then(data => {
                            if (!parse) {
                                let arr = data.json()
                                // if (arr.length > 0) me.setValue(arr[0].id)
                            }
                        })
                    }
                    // $$("serial").getList().add(all, 0)
                    //$$("serial").setValue("")
                })
            }
        }
        let newfill
        if (ENT == "bvb") {
            newfill = false
        } else {
            newfill = 1
        }
        let arrdtl = [
            { id: "del", header: [`<span class='webix_icon wxi-plus' title='${_("details_insert")}'></span>`], template: `<span class='webix_icon wxi-trash' title='${_("details_delete")}'></span>`, width: 35 },
            { id: "line", header: { text: _("stt"), css: "header" }, css: "right", adjust: true },
            { id: "type", header: { text: _("formality"), css: "header" }, adjust: true, collection: LTYPE(this.app), editor: "combo" },
            {
                id: "name", header: { text: _("catalog_items"), css: "header" }, adjust: true, fillspace: 3, editor: "text", attributes: { maxlength: 5 }, suggest: {
                    //view: "suggest",
                    view: "gridsuggest",
                    // fitMaster: true,
                    resize: true,
                    body: {
                        url: "api/gns/fbn",
                        dataFeed: function (str) {
                            if (/\;$/.test(str)) return this.load(`${this.config.url}?filter[value]=${encodeURIComponent(str.substring(0, str.length - 1))}`, this.config.datatype)
                            else return
                        },
                        scroll: true,
                        autoheight: false,
                        columns: [
                            { id: "name", header: { text: _("gns_name"), css: "header" }, adjust: true },
                            { id: "code", header: { text: _("gns_code"), css: "header" } },
                            { id: "unit", header: { text: _("unit"), css: "header" } },
                            { id: "price", header: { text: _("price"), css: "header" }, css: "right", format: gridnfconf.format }
                        ]
                    },
                    on: {
                        onValueSuggest: (rec) => {
                            if (rec) {
                                const row = $$("inv:grid").getSelectedItem()
                                row.name = rec.name
                                row.unit = rec.unit
                                row.code = rec.code
                                if (rec.price) {
                                    const price = Number(rec.price), quantity = row.quantity
                                    row.price = price
                                    if (ENT == "scb" && quantity == "") row.amount = 0
                                    if (quantity && quantity != "" && !KMMT.includes(row.type)) row.amount = price * quantity
                                    if (row.amount) {
                                        row.vat = Math.round(Number(row.vrt) * row.amount) / 100
                                        row.total = row.amount + (row.vat ? row.vat : 0)
                                    }
                                }
                            }
                        }
                    }
                }
            },
            { id: "code", header: { text: _("gns_code"), css: "header" }, sort: "server", editor: "text", adjust: true, hidden: (['hsy'].includes(ENT)) ? false : true },
            { id: "gns", header: "", template: `<span class='webix_icon wxi-search' title='${_("gns_select")}'></span>`, width: 35 },//header: "<div class='webix_icon wxi-search'></div>"
            { id: "unit", header: { text: _("unit"), css: "header" }, adjust: true, editor: "text", suggest: { url: "api/cat/kache/unit" } },//, filter: filter
            { id: "price", header: { text: _("price"), css: "header" }, adjust: true, fillspace: newfill, css: "right", editor: "text", inputAlign: "right", format: gridnfconf.format, editParse: gridnfconf.editParse, editFormat: gridnfconf.editFormat },
            { id: "quantity", header: { text: _("quantity"), css: "header" }, adjust: true, css: "right", editor: "text", inputAlign: "right", format: gridnfconf.format, editParse: gridnfconf.editParse, editFormat: gridnfconf.editFormat },

            CONFIG_CHIETKHAU_COL ? { id: "discountdtl", header: { text: _("discountdtl"), css: "header" }, adjust: true, css: "right", editor: "text", inputAlign: "right", format: gridnfconf.format, editParse: gridnfconf.editParse, editFormat: gridnfconf.editFormat } : { id: "discountdtl", header: { text: _("discountdtl"), css: "header" }, adjust: true, hidden: true, css: "right", editor: "text", inputAlign: "right", format: gridnf4.format, editParse: gridnf4.editParse, editFormat: gridnf4.editFormat },
            { id: "vatCode", header: { text: _("vrt"), css: "header" }, adjust: true, collection: VAT(this.app), editor: "richselect", suggest: { fitMaster: false, width: 170 } },
            { id: "discountrate", header: { text: _("discount_rate"), css: "header" }, adjust: true,  suggest:{ width:200}, editor: "text",format: gridnf4.formatDecimal, editParse: gridnf4.editParseDecimal, editFormat: gridnf4.editFormatDecimal, inputAlign: "right", hidden : ["123"].includes(DEGREE_CONFIG) ? false :true  },
            { id: "discountamt", header: { text: _("discount_price"), css: "header" }, adjust: true, editor: "text", suggest:{ width:170},format: gridnfconf.format, editParse: gridnfconf.editParse, editFormat: gridnfconf.editFormat, hidden : ["123"].includes(DEGREE_CONFIG) ? false :true},
            { id: "amount", header: { text: _("amount"), css: "header" }, adjust: true, fillspace: newfill, css: "right", editor: "text", inputAlign: "right", format: gridnfconf.format, editParse: gridnfconf.editParse, editFormat: gridnfconf.editFormat },
            { id: "amountv", header: { text: "VND", css: "header" }, adjust: true, fillspace: newfill, css: "right", editor: "text", hidden: true, inputAlign: "right", format: gridnfconf.format, editParse: gridnfconf.editParse, editFormat: gridnfconf.editFormat }

            /*,
            /*,
            { id: "total", header: { text: _("dtl_total"), css: "header" }, adjust: true, fillspace: 1, css: "right", editor: "text", inputAlign: "right", format: gridnfconf.format, editParse: gridnfconf.editParse, editFormat: gridnfconf.editFormat, hidden: (["fhs","fbc"].includes(ENT)) ? false : true }*/

        ]

        if (['fhs', 'vhc', 'fbc'].includes(ENT))
            arrdtl.splice(2, 1)
        arrdtl.push({ id: "vat", header: { text: _("dtl_vat"), css: "header" }, adjust: true, fillspace: newfill, css: "right", editor: "text", inputAlign: "right", format: gridnfconf.format, editParse: gridnfconf.editParse, editFormat: gridnfconf.editFormat })
        arrdtl.push({ id: "vatv", header: { text: "VND", css: "header" }, adjust: true, fillspace: newfill, css: "right", editor: "text", hidden: true, inputAlign: "right", format: gridnfconf.format, editParse: gridnfconf.editParse, editFormat: gridnfconf.editFormat })

        arrdtl.push({ id: "total", header: { text: _("dtl_total"), css: "header" }, adjust: true, fillspace: newfill, css: "right", editor: "text", inputAlign: "right", format: gridnfconf.format, editParse: gridnfconf.editParse, editFormat: gridnfconf.editFormat })
        arrdtl.push({ id: "totalv", header: { text: "VND", css: "header" }, adjust: true, fillspace: newfill, css: "right", editor: "text", hidden: true, inputAlign: "right", format: gridnfconf.format, editParse: gridnfconf.editParse, editFormat: gridnfconf.editFormat })


        webix.protoUI({ name: "dependent", $cssName: "combo" }, LinkedInputs4, webix.ui.combo)
        let grid = {
            view: "datatable",
            id: "inv:grid",
            editable: true,
            navigation: true,
            editaction: "click",
            resizeColumn: true,
            clipboard: "custom",
            multiselect: false,
            select: "cell",
            columns: arrdtl,
            onClick: {
                "wxi-trash": function (e, id) {
                    this.remove(id)
                    return false
                },
                "wxi-plus": function (e, id) {
                    let me = this
                    let obj = { quantity: ["scb", "bvb"].includes(ENT) ? "" : 1 }
                    if (type == "01GTKT" || type =="01/TVE") {
                        obj.vatCode = "VAT1000"
                        obj.vrt = "10"
                    }
                    me.add(obj)
                    me.clearValidation()
                    return false
                },
                "wxi-search": function (e, id) {
                    this.select(id.row)
                    $$("igns:win").show()
                }
            },
            on: {
                onBeforeEditStop: function (val, editor) {
                    let currr = $$("curr").getValue()
                    let NUMBER_DECIMAL = NUMBER_DECIMAL_FORMAT
                    if (currr == 'VND') NUMBER_DECIMAL = 0
                    const column = editor.column
                    // tham so bỏ xung chiet khau trên phần chi tiết hoa đon CONFIG_CHIETKHAU_COL
                    if (column == "price" || column == "quantity" || column == "type" || column == "vatCode"|| column == "discountrate"|| column == "discountamt" || column == "vat" || ((column == "discountdtl" && CONFIG_CHIETKHAU_COL) || (column == "amount"))) {
                        const newv = val.value, oldv = val.old
                        if (newv != oldv) {
                            let row = this.getItem(editor.row), amount = 0, price = row.price, quantity = row.quantity, ck = row.discountdtl,discountrate = row.discountrate , discountamt = 0
                            if (column == "price") {
                                if (quantity && !KMMT.includes(row.type)) amount = newv * quantity
                                if(discountrate){
                                    discountamt = Math.round(Number(newv) * discountrate * quantity) / 100
                                    amount = newv * quantity - discountamt
                                }
                                if (["0", "-1", "-2"].includes(row.vrt)) {
                                    row.vat = 0
                                    row.total = Math.round(amount * Math.pow(10, NUMBER_DECIMAL)) / Math.pow(10, NUMBER_DECIMAL)
                                }
                                else {
                                    if (row.amount) {
                                        row.vat = Math.round(Number(row.vrt) * amount / 100 * Math.pow(10, NUMBER_DECIMAL))/Math.pow(10, NUMBER_DECIMAL)
                                        row.total = Math.round((amount + row.vat) * Math.pow(10, NUMBER_DECIMAL)) / Math.pow(10, NUMBER_DECIMAL)
                                        if (discountrate) {
                                            row.discountamt = discountamt
                                        }
                                    }
                                }
                            }
                            else if (column == "quantity") {
                                if (price && !KMMT.includes(row.type)) amount = newv * price
                                if(discountrate){
                                    discountamt = Math.round(Number(newv) * price * discountrate) / 100
                                    amount = newv * price - discountamt
                                }
                                if (["0", "-1", "-2"].includes(row.vrt)) {
                                    row.vat = 0
                                    row.total = amount
                                }
                                else {
                                    if (row.amount) {
                                        //row.vat = Math.round(amount * row.vrt) / 100
                                        row.vat = Math.round(Number(row.vrt) * amount / 100 * Math.pow(10, NUMBER_DECIMAL))/Math.pow(10, NUMBER_DECIMAL)
                                        row.total = Math.round(amount * Math.pow(10, NUMBER_DECIMAL)) / Math.pow(10, NUMBER_DECIMAL) + row.vat
                                        if (discountrate) {
                                            row.discountamt = discountamt
                                        }                                    
                                    }
                                }
                            }
                            else if (column == "discountrate") {
                                if (price && quantity && !KMMT.includes(row.type)) {
                                    discountamt = Math.round(Number(newv) * price * quantity) / 100
                                    amount = price * quantity - discountamt
                                }
                                if (["0", "-1", "-2"].includes(row.vrt)) {
                                    row.vat = 0
                                    row.total = amount
                                }
                                else {
                                    if (row.amount) {
                                        //row.vat = Math.round(Number(row.vrt) * amount) / 100
                                        row.vat = Math.round(Number(row.vrt) * amount / 100 * Math.pow(10, NUMBER_DECIMAL))/Math.pow(10, NUMBER_DECIMAL)
                                        row.total = Math.round((amount + row.vat) * Math.pow(10, NUMBER_DECIMAL)) / Math.pow(10, NUMBER_DECIMAL)
                                        row.discountamt = discountamt
                                    }
                                }
                            }
                            else if (column == "discountamt") {
                                if (price && quantity && !KMMT.includes(row.type)) {
                                    discountamt = Math.round(Number(newv))
                                    amount = price * quantity - discountamt
                                }
                                if (["0", "-1", "-2"].includes(row.vrt)) {
                                    row.vat = 0
                                    row.total = amount
                                }
                                else {
                                    if (row.amount) {
                                        //row.vat = Math.round(Number(row.vrt) * amount) / 100
                                        row.vat = Math.round(Number(row.vrt) * amount / 100 * Math.pow(10, NUMBER_DECIMAL))/Math.pow(10, NUMBER_DECIMAL)
                                        row.total = Math.round((amount + row.vat) * Math.pow(10, NUMBER_DECIMAL)) / Math.pow(10, NUMBER_DECIMAL)
                                        row.discountamt = discountamt
                                    }
                                }
                            }
                            else if (column == "amount" && CONFIG_CHIETKHAU_COL) {
                                if (["fhs", "fbc"].includes(ENT)) {
                                    delete row.price
                                    delete row.quantity
                                }

                            }
                            else if (column == "vatCode") {
                                let objtax = CATTAX.find(obj => obj.vatCode == newv)
                                row.vrt = objtax.vrt
                                if (row.amount) {
                                    amount = row.amount
                                }

                                if (["0", "-1", "-2"].includes(row.vrt)) {
                                    row.vat = 0
                                    row.total = Math.round(amount * 100) / 100
                                }
                                else {
                                    if (row.amount) {

                                        //row.vat = Math.round(Number(row.vrt) * row.amount) / 100
                                        row.vat = Math.round(Number(row.vrt) * amount / 100 * Math.pow(10, NUMBER_DECIMAL))/Math.pow(10, NUMBER_DECIMAL)
                                        row.total = Math.round((row.amount + row.vat) * Math.pow(10, NUMBER_DECIMAL)) / Math.pow(10, NUMBER_DECIMAL)
                                    }
                                }

                            }
                            else if (column == "amount") {
                                if (["0", "-1", "-2"].includes(row.vrt)) {
                                    row.vat = 0
                                    row.total = Math.round(newv * Math.pow(10, NUMBER_DECIMAL)) / Math.pow(10, NUMBER_DECIMAL)
                                }
                                else {
                                    if (row.amount) {
                                        //row.vat = Math.round(Number(row.vrt) * newv) / 100
                                        row.vat = Math.round(Number(row.vrt) * amount / 100 * Math.pow(10, NUMBER_DECIMAL))/Math.pow(10, NUMBER_DECIMAL)
                                        row.total = Math.round((newv + row.vat) * Math.pow(10, NUMBER_DECIMAL)) / Math.pow(10, NUMBER_DECIMAL)
                                    }
                                }
                            }
                            else if (column == "vat") {
                                if (["0", "-1", "-2"].includes(row.vrt)) {
                                    row.vat = 0
                                    row.total = Math.round(row.amount * Math.pow(10, NUMBER_DECIMAL)) / Math.pow(10, NUMBER_DECIMAL)
                                }
                                else {
                                    if (row.amount) {
                                        amount = Math.round(row.amount * Math.pow(10, NUMBER_DECIMAL)) / Math.pow(10, NUMBER_DECIMAL)
                                        row.total = Math.round((newv + row.amount) * Math.pow(10, NUMBER_DECIMAL)) / Math.pow(10, NUMBER_DECIMAL)
                                    }
                                }

                            }
                            else {
                                if (KMMT.includes(newv)) {
                                    row.vrt = "0"
                                    row.vatCode = "VAT0000"
                                    amount = 0
                                }
                                else {
                                    if (ENT == "sgr") {
                                        row.vat = (-1) * row.vat
                                        row.total = (-1) * row.total
                                    }
                                    if (row.vrt == "0") {
                                        row.vrt = "10"
                                        row.vatCode = "VAT1000"
                                    }
                                    if (quantity && price) amount = Math.round(price * quantity * Math.pow(10, NUMBER_DECIMAL)) / Math.pow(10, NUMBER_DECIMAL)
                                }
                                
                            }
                            if (CONFIG_CHIETKHAU_COL) {
                                if (column == "discountdtl") {
                                    if (amount) amount = amount * (100 - newv) / 100
                                } else {
                                    if (ck) amount = amount * (100 - ck) / 100
                                }
                            }

                            row.amount =  Math.round(amount*Math.pow(10, NUMBER_DECIMAL))/Math.pow(10, NUMBER_DECIMAL)
                        }
                        if (!newv) {
                            if (CONFIG_CHIETKHAU_COL) {
                                if (column == "discountdtl") {
                                    let row = this.getItem(editor.row), amount = 0, price = row.price, quantity = row.quantity
                                    if (quantity && price) amount = price * quantity
                                    row.amount = Math.round(amount * Math.pow(10, NUMBER_DECIMAL)) / Math.pow(10, NUMBER_DECIMAL)
                                }
                            }
                            if (column == "discountrate") {
                                let row = this.getItem(editor.row)
                                if (row.price && row.quantity && !KMMT.includes(row.type)) {
                                    row.amount = row.price * row.quantity
                                }
                                if (["0", "-1", "-2"].includes(row.vrt)) {
                                    row.vat = 0
                                    row.total = row.amount
                                }
                                else {
                                    if (row.amount) {
                                        //row.vat = Math.round( row.amount) / 100
                                        row.vat = Math.round(Number(row.vrt) * amount / 100 * Math.pow(10, NUMBER_DECIMAL))/Math.pow(10, NUMBER_DECIMAL)
                                        row.total = Math.round((row.amount + row.vat) * Math.pow(10, NUMBER_DECIMAL)) / Math.pow(10, NUMBER_DECIMAL)
                                        row.discountamt = 0
                                    }
                                }
                            }
                        }
                        let row = this.getItem(editor.row)
                        let  a = $$("exrt").getValue()
                        row.vatv = Math.round(row.vat * $$("exrt").getValue())
                        row.amountv = Math.round(row.amount * $$("exrt").getValue())
                        row.totalv = Math.round(row.total * $$("exrt").getValue())
                    }
                }

            },
            rules: {
                name: webix.rules.isNotEmpty,
                //amount: webix.rules.isNumber,
                $obj: data => {
                    if (data.name) {
                        if(data.name.length > 500) {
                            webix.message(_("catalog_items_maxlength"), "error")
                            return false
                        }
                    }
                    if (data.unit) {
                        if(data.unit.length > 50) {
                            webix.message(_("unit_maxlength"), "error")
                            return false
                        }
                    }
                    let numMin = -999999999999999.999999, numMax = 999999999999999.999999
                    if (data.discountrate) {
                        if (data.discountrate < 0 || data.discountrate > 100 ) {
                            webix.message(_("rate_error"), "error")
                            return false
                        }
                    }
                    if (data.discountamt) {
                        if (data.discountamt < numMin || data.discountamt > numMax ) {
                            webix.message(_("number_format_error"), "error")
                            return false
                        }
                    }
                    if (data.amount) {
                        if (data.amount < numMin || data.amount > numMax ) {
                            webix.message(_("number_format_error"), "error")
                            return false
                        }
                    }
                    if (data.vat) {
                        if (data.vat < numMin || data.vat > numMax ) {
                            webix.message(_("number_format_error"), "error")
                            return false
                        }
                    }
                    if (data.total) {
                        if (data.total < numMin || data.total > numMax ) {
                            webix.message(_("number_format_error"), "error")
                            return false
                        }
                    }
                    if ( ["01GTKT","01/TVE"].includes(type) && ((!webix.rules.isNotEmpty(data.vrt)) || (!webix.rules.isNotEmpty(data.amount)))) return false
                    if (this.getParam("action") != "adjdif" &&( data.price < 0 || data.quantity < 0 || (data.amount < 0 && !["aia"].includes(ENT)))) return false
                    return true
                }
            }
        }
        let urlserial = "api/sea/sbf?id=", urlform = "api/sea/fbt?id=", action = this.getParam("action")
        if (action == "wcancel") {
            urlserial = "api/sea/asbf?id="
            urlform = "api/sea/afbt?id="
        }

        let form = {
            view: "form",
            id: "inv:form",
            padding: 0,
            margin: 0,
            paddingX: 2,
            visibleBatch: "batch1",
            complexData: true,
            elementsConfig: { labelWidth: 85 },
            elements: [
                {//R1
                    id: "inv:r1", visibleBatch: "hd",
                    cols: [
                        {
                            batch: "hd", gravity: 3,
                            cols: [
                                { id: "bc2", name: "bc2", view: "text", hidden : true},
                                {
                                    id: "bname", name: "bname", label: _("bname2"), view: "text", attributes: { maxlength: ["fhs", "fbc"].includes(ENT) ? 500 : 400 }, suggest: {
                                        //view: "suggest",
                                        view: "gridsuggest",
                                        // fitMaster: true,
                                        resize: true,
                                        body: {
                                            url: "api/org/fbn",
                                            dataFeed: function (str) {
                                                if (/\;$/.test(str)) return this.load(`${this.config.url}?filter[value]=${encodeURIComponent(str.substring(0, str.length - 1))}`, this.config.datatype)
                                                else return
                                            },
                                            scroll: true,
                                            autoheight: false,
                                            columns: [
                                                { id: "taxc", header: { text: _("taxcode"), css: "header" }, adjust: true },
                                                { id: "name", header: { text: _("customer_name"), css: "header" }, adjust: true },
                                                { id: "fadd", header: { text: _("address"), css: "header" }, adjust: true }
                                            ]
                                        },
                                        on: {
                                            onValueSuggest: (rec) => {
                                                if (rec) {
                                                    $$("bname").setValue(rec.name)
                                                    $$("btax").setValue(rec.taxc)
                                                    $$("baddr").setValue(rec.fadd)
                                                    $$("bmail").setValue(rec.mail)
                                                    $$("btel").setValue(rec.tel)
                                                    $$("bacc").setValue(rec.acc)
                                                    $$("bbank").setValue(rec.bank)
                                                    $$("bcode").setValue(rec.code)
                                                    $$("bc2").setValue(rec.c2)
                                                }
                                            }
                                        }
                                    }, placeholder: _("enter2search_msg"), required: true
                                },
                                { id: "inv:btnorgs", view: "button", type: "icon", icon: "wxi-search", tooltip: _("customers_search"), width: 33 }
                            ]
                        },
                        { batch: "pxk", id: "ordno", name: "ordno", label: _("ordno"), view: "text", attributes: { maxlength: 100 }, placeholder: _("ordno_tip"), required: true },
                        { batch: "pxk", id: "ordou", name: "ordou", label: _("ou"), view: "text", attributes: { maxlength: 100 }, placeholder: _("ordou_tip") },
                        { batch: "pxk", id: "orddt", name: "orddt", label: _("orddt"), view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr, tooltip: _("orddt_tip"), required: true },
                        { id: "idt", name: "idt", label: _("invoice_date"), view: "datepicker", stringResult: true, disabled: true, format: webix.i18n.dateFormatStr, required: true },
                        { id: "type", name: "type", label: _("invoice_type"), view: "combo", options: { data: ITYPE(this.app), relative: "left" }, required: true }
                    ]
                },
                {//R2
                    id: "inv:r2", visibleBatch: "hd",
                    cols: [
                        !["hdb", "dtt"].includes(ENT) ?
                            { id: "btax", name: "btax", label: _("taxcode"), view: "text", attributes: { maxlength: 14 }, validate: v => { return (!v || checkmst(v)) }, invalidMessage: ["bvb"].includes(ENT) ? _("taxcode_invalid_bvb") : _("taxcode_invalid"), placeholder: _("taxcode") }
                            : { id: "btax", name: "btax", label: _("taxcode"), view: "text", attributes: { maxlength: 25 }, validate: v => { if (!(!v || checkmst(v)) && ENT == "hdb") { this.webix.message(_("taxcode_invalid"), "success"); }; return true; }, invalidMessage: _("taxcode_invalid"), placeholder: _("taxcode") },
                        { batch: "hd", id: "buyer", name: "buyer", label: _("buyer"), view: "text", attributes: { maxlength: ["vcm", "fhs", "fbc"].includes(ENT) ? 500 : 100 }, placeholder: _("buyer"), },
                        { batch: "hd", id: "btel", name: "btel", label: _("tel"), view: "text", attributes: { maxlength: 20 }, placeholder: _("tel") },
                        // { batch: "hd", id: "bcode", name: "bcode", label: _("cus_code"), view: "text", attributes: { maxlength: 255 }, hidden: ["sgr", "bvb"].includes(ENT) ? false : true },
                        // chuyển input bcode sang dòng thứ 3
                        // { batch: "pxk", id: "bcode", name: "bcode", label: _("cus_code"), view: "text", attributes: { maxlength: 255 }},
                        {id: "bmail", name: "bmail", label: "Mail", view: "text", type: "email", attributes: { maxlength: 500 }, placeholder: "Mail"},//, validate: v => { return !v || webix.rules.isEmail(v) }, invalidMessage: _("mail_invalid")
                        { batch: "pxk", id: "trans", name: "trans", label: _("transport"), view: "text", attributes: { maxlength: 100 }, placeholder: _("transport") },
                        { batch: "pxk", id: "vehic", name: "vehic", label: _("vehicle"), view: "text", attributes: { maxlength: 50 }, placeholder: _("vehicle_tooltip"), required: true },
                        // { batch: "pxk", id: "mail", name: "mail", label: _("mail"), view: "text", attributes: {maxlength: 100}, placeholder: _("mail")},
                        //{ name: "bcode", label: _("cus_code"), view: "text", attributes: { maxlength: 15 }, adjust: true },
                        { id: "form", name: "form", label: _("form"), view: "dependent", options: [], master: "type", dependentUrl: urlform, relatedView: "serial", relatedAction: "enable", required: true, placeholder: _("form")}
                    ]
                },
                {//R3
                    id: "inv:r3", visibleBatch: "hd",
                    cols: [
                        { batch: "hd", id: "baddr", name: "baddr", label: _("address"), view: "text", attributes: { maxlength: 400 }, gravity: 2, required: ["ghtk"].includes(ENT) ? false : true, placeholder: _("address") },
                        { batch: "hd", id: "bacc", name: "bacc", label: _("account"), view: "text", attributes: { maxlength: 30 }, placeholder: _("account") },
                        // số tài khoản nh
                        { batch: "hd", id: "bbank", name: "bbank", label: _("bank"), view: "text", attributes: { maxlength: 400 }, placeholder: _("bank") },
                        { batch: "pxk", id: "whsfr", name: "whsfr", label: _("export_address"), view: "text", attributes: { maxlength: 255 }, placeholder: _("export_address"), gravity: 1, required: true },
                        { batch: "pxk", id: "exporter", name: "exporter", label: _("exporter"), view: "text", attributes: { maxlength: 100 }, placeholder: _("exporter_tooltip"), gravity: 1, tooltip: _("exporter_tooltip") },
                        { batch: "pxk", id: "whsto", name: "whsto", label: _("recipients"), view: "text", attributes: { maxlength: 255 }, placeholder: _("recipients"), gravity: 1, required: true },
                        // { batch: "hd"},
                        { batch: "pxk", id: "recvr", name: "recvr", label: _("receiver"), view: "text", attributes: { maxlength: 100 }, placeholder: _("receiver") },
                        { id: "serial", name: "serial", label: _("serial"), view: "dependent", options: [], master: "form", dependentUrl: urlserial, disabled: true, required: true, placeholder: _("serial") }

                    ]
                },
                {//R4
                    id: "inv:r4", visibleBatch: "hd",
                    cols: [
                        { id: "curr", name: "curr", label: _("currency"), view: "combo", suggest: { url: "api/cat/kache/currency" }, required: true },
                        { id: "exrt", name: "exrt", label: _("exchange_rate"), view: "text", value: 1, inputAlign: "right", format: textnf, disabled: ["bvb"].includes(ENT) ? false : true, required: true },
                        { batch: "hd", id: "paym", name: "paym", label: _("payment_method"), view: "richselect", options: PAYMETHOD(this.app), placeholder: _("payment_method") },
                        // chuyển input số tk ngân hàng lên row 3 -> cho mã khách hàng xuống row 4
                        {  batch: "hd", id: "bcode", name: "bcode", label: _("cus_code"), view: "text", attributes: { maxlength: 255 }, placeholder: _("cus_code")},
                        { batch: "pxk", id: "contr", name: "contr", label: (!"vcm".includes(ENT)) ? _("contract_no") : _("contract_no_vcm"), view: "text", attributes: { maxlength: 100 }, placeholder: (!"vcm".includes(ENT)) ? _("contract_no") : _("contract_no_vcm") },
                        { batch: "pxk", id: "ordre", name: "ordre", label: _("reason"), view: "text", attributes: { maxlength: 255 }, placeholder: _("ordre_tip"), gravity: 1 },
                        // { batch: "pxk", id: "mast", name: "mast", label:_("mast"), view: "text", attributes : {maxlength: 100}, placeholder: _("mast")},
                        //{ batch: "pxk", id: "receiver", name: "receiver", label: _("receiver_fn"), view: "text", attributes: { maxlength: 100 }, placeholder: _("receiver_fn_tip"), tooltip: _("receiver_fn_tip"), gravity: 1  },
                        //{ batch: "pxk", id: "btax", name: "btax", label: _("taxcode"), view: "text", attributes: { maxlength: 14 }, validate: v => { return (!v || checkmst(v)) }, invalidMessage: ["bvb"].includes(ENT) ? _("taxcode_invalid_bvb") : _("taxcode_invalid") },
                        { id: "seq", name: "seq", label: _("seq"), view: "text", disabled: true, inputAlign: "right", placeholder: _("seq")}   
                    ]
                },{//R5_1
                    id: "inv:r5_1", visibleBatch: "hd",
                    cols: [
                        !["apple"].includes(ENT) ? { batch: "hd", id: "invlistnum", name: "invlistnum", label: _("list_number"), view: "text" , attributes: { maxlength: 50 }, hidden : ["123"].includes(DEGREE_CONFIG) ? false :true  } : { id: "ic", name: "ic", label: "IC", view: "text", attributes: { maxlength: 36 }, required: true, placeholder: "IC"},
                        !["apple"].includes(ENT) ? { batch: "hd", id: "invlistdt", name: "invlistdt", label: _("date_of_list"), view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr,hidden : ["123"].includes(DEGREE_CONFIG) ? false :true, placeholder: _("date_of_list")  } : { batch: "hd" },
                        {},
                        {},
                        { batch: "pxk" },
                        { height: 10 }
                    ],hidden : ["123"].includes(DEGREE_CONFIG) ? false :true 
                },
                {//R5
                    id: "inv:r5", batch: "batch3",
                    cols: [
                        { name: "adj.ref", label: _("adj.ref"), view: "text", attributes: { maxlength: 100 }, placeholder: _("adj.ref_tip") },
                        { id: "adj.rdt", name: "adj.rdt", label: _("adj.rdt"), view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr, placeholder: _("adj.rdt_tip") },
                        { id: "adj.rea", name: "adj.rea", label: _("reason"), view: "text", attributes: { maxlength: 500 }, required: true, },
                        { batch: "adjdif", gravity: 2},
                        { gravity: 0 }, { gravity: 2, id: "adj.des", name: "adj.des", view: "label", align: "center", css: "red" } /*ENT != "vcm" ? { id: "adj.des", name: "adj.des", view: "label", align: "center", css: "red" }: { id: "adj.des", name: "adj.des", view: "text", attributes: { maxlength: 500, align: "center", css: "red" }, readonly: true }*/
                    ]
                },
                { id: "inv:r6", hidden: true },
                { id: "inv:r7", hidden: true },
                {
                    id: "r_adj_dif",
                    hidden: true,
                    rows:[
                        { height: 10 },
                        {
                            view:"fieldset",
                            label: _("inv_root_info"),
                            body: {
                                rows: [{
                                    cols: [
                                        { id: "inv:form:root.typ", name: "root.typ", label: _("adj.typ"), view: "combo", options: ADJ_TYP , placeholder: _("adj.typ"), required: true },
                                        { id: "inv:form:root.form", name: "root.form", label: _("form"), view: "text", placeholder: _("form"), attributes: { maxlength: 11 }, required: true },
                                        { id: "inv:form:root.serial", name: "root.serial", label: _("serial"), view: "text", placeholder: _("serial"), attributes: { maxlength: 6 }, required: true },
                                        { name: "root.seq", label: _("seq"), view: "text", attributes: { maxlength: 8 }, placeholder: _("seq"), required: true },
                                        { name: "root.idt", label: _("invoice_date"), view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr, required: true, placeholder: _("invoice_date"), },
                                    ]
                                }]
                            }
                        }
                    ]
                },
                { id: "inv:r8", view: "fieldset", label: _("details"), body: grid },
                {
                    id: "inv:r9", cols: [
                        { id: "note", name: "note", label: _("note"), view: "text", attributes: { maxlength: 255 }, gravity: 3 },
                        { id: "sumv", name: "sumv", label: _("total_vnd"), view: "text", inputAlign: "right", readonly: true, format: ["hlv"].includes(ENT) ? textnfconfnumberv : intf, required: true },
                        { id: "sum", name: "sum", label: _("sumo"), view: "text", inputAlign: "right", readonly: false, format: textnf, required: true }
                    ]
                },
                ['baca'].includes(ENT) ?
                    {
                        id: "inv:r10", cols: [
                            { gravity: 1 },
                            {
                                cols: [
                                    { gravity: 0 },
                                    { id: "score", name: "score", label: _("score"), view: "text", inputAlign: "right", format: textnf, hidden: true },
                                ]
                            },
                            { id: "discount", name: "discount", label: _("discount"), view: "text", inputAlign: "right", format: textnf, hidden: true },
                            {},
                            { id: "vatv", name: "vatv", label: "VAT VND", view: "text", inputAlign: "right", readonly: true, format: intf },
                            { id: "vat", name: "vat", label: _("total_vat"), view: "text", inputAlign: "right", readonly: false, format: textnf }
                        ]
                    } :
                    {
                        id: "inv:r10", cols: [
                            // { gravity: 1 },
                            {
                                cols: [
                                    { gravity: 0 },
                                    { id: "score", name: "score", label: _("score"), view: "text", inputAlign: "right", format: textnf, hidden: true },
                                ]
                            },
                            { id: "tradeamount", name: "tradeamount", label: _("tradeamount"), view: "text", value: 0, inputAlign: "right", format: textnf, hidden: false },
                            { id: "discount", name: "discount", label: _("discount"), view: "text", value: 0, inputAlign: "right", format: textnf, hidden: false },
                            { id: "vatv", name: "vatv", label: "VAT VND", view: "text", inputAlign: "right", readonly: true, format: ["hlv"].includes(ENT) ? textnfconfnumberv : intf },
                            { id: "vat", name: "vat", label: _("total_vat"), view: "text", inputAlign: "right", readonly: false, format: textnf }
                        ]
                    },
                {
                    id: "inv:r11", cols: [
                        { id: "word", name: "word", label: _("word"), view: "text", readonly: true, required: true, gravity: 3 },
                        { id: "totalv", name: "totalv", label: _("sum_vnd"), view: "text", inputAlign: "right", readonly: true, format: ["hlv"].includes(ENT) ? textnfconfnumberv : intf, required: true },
                        { id: "total", name: "total", label: _("totalo"), view: "text", inputAlign: "right", readonly: ['vib'].includes(ENT) ? false : true, format: textnf, required: true }
                    ]
                },
                {
                    id: "inv:r12", cols: [
                        {},
                        { id: "inv:btnunselect", view: "button", type: "icon", icon: "mdi mdi-select-off", label: _("unselect"), width: 100, hidden: ['vib'].includes(ENT) ? true : true },
                        { id: "inv:btngetcustomerinfo", view: "button", type: "icon", icon: "wxi-angle-up", label: _("get_customer_info_label"), width: 100, hidden: ['vib'].includes(ENT) ? false : true },
                        { id: "inv:btngettransinfo", view: "button", type: "icon", icon: "wxi-angle-up", label: _("get_transaction_info_label"), width: 95, hidden: ['vib'].includes(ENT) ? false : true },

                        { id: "inv:file", view: "uploader", multiple: ['vib'].includes(ENT) ? true : false, autosend: false, link: "inv:filelist", accept: "application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/pdf", upload: "api/inv/minutes/upload", type: "icon", icon: "mdi mdi-file-find-outline", label: _("file_select"), width: 100, hidden: ['vib'].includes(ENT) ? false : true, disabled: ['vib'].includes(ENT) ? false : true },
                        //  { id: "inv:fileatt", view: "uploader", multiple: false, autosend: false, link: "inv:filelistatt", accept: "application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/pdf", upload: "api/inv/minutes/uploadAtt", type: "icon", icon: "mdi mdi-file-find-outline", label: _("file_select"), width: 100, hidden: true, disabled: true },

                        { id: "inv:btndoc", view: "button", type: "icon", icon: "mdi mdi-open-in-app", label: _("minutes_create"), tooltip: _("minutes_create_tip"), width: 110, hidden: true, disabled: true },
                        // { id: "inv:btninvlistnum", view: "button", type: "icon", icon: "mdi mdi-content-save", label: ("Tải bảng kê"), disabled: ROLE.includes('PERM_CREATE_INV_MANAGE_SAVE') ? false : true, width: 120 },
                        { id: "inv:btnsave", view: "button", type: "icon", icon: "mdi mdi-content-save", label: _("save"), disabled: ROLE.includes('PERM_CREATE_INV_MANAGE_SAVE') ? false : true, width: 80 },
                        { id: "inv:btniss", view: "button", type: "icon", icon: "mdi mdi-content-save-all", label: _("issue"), width: 100, hidden: true, css: "webix_danger" },
                        { id: "inv:btnissrep", view: "button", type: "icon", icon: "mdi mdi-content-save-all", label: _("issueReplace"), width: 100, hidden: true, css: "webix_danger" },
                        { id: "inv:btndel", view: "button", type: "icon", icon: "wxi-trash", label: _("delete"), width: 80, hidden: true, css: "webix_danger" },
                        { id: "inv:btncancel", view: "button", type: "icon", icon: "wxi-trash", label: _("cancel"), width: 80, hidden: true, disabled: true, css: "webix_danger" },
                        { id: "inv:btntrash", view: "button", type: "icon", icon: "wxi-trash", label: _("wait_cancel"), width: 100, hidden: true, css: "webix_danger" },
                        { id: "inv:btnview", view: "button", type: "icon", icon: "mdi mdi-book-open-variant", label: _("view"), width: 80, disabled: (ROLE.includes('PERM_CREATE_INV_MANAGE_VIEW') || ROLE.includes('PERM_SEARCH_INV_CANCEL_VIEW')) ? false : true },
                        { id: "inv:btnclose", view: "button", type: "icon", icon: "mdi mdi-exit-to-app", label: _("exit"), width: 80 }
                    ]
                },
                {
                    id: "inv:r13", cols: [{},
                    { view: "list", id: "inv:filelist", type: "uploader", autoheight: true, borderless: true, hidden: true },
                    { view: "text", id: "id_file", hidden: true },
                        // { view: "list", id: "inv:filelistatt", type: "uploader", autoheight: true, borderless: true, hidden: true }
                    ]
                }
            ]
            , rules: {
                idt: webix.rules.isNotEmpty,
                form: webix.rules.isNotEmpty,
                serial: webix.rules.isNotEmpty,
                curr: webix.rules.isNotEmpty,
                exrt: webix.rules.isNumber,
                sum: webix.rules.isNumber,
                total: webix.rules.isNumber,
                sumv: webix.rules.isNumber,
                totalv: webix.rules.isNumber,
                word: webix.rules.isNotEmpty,
                $obj: data => {
                    if (data.exrt <= 0) {
                        webix.message(_("exrt_positive"), "error")
                        $$("exrt").focus()
                        return false
                    }
                    if (ENT == "hlv") {
                        if (data.c0 && data.c0.length > 0 && data.c0.length < 10) {
                            webix.message(_("policynumber_min_hlv"), "error")
                            $$("c0").focus()
                            return false
                        }
                    }
                    if ((data.sum < 0 || data.total < 0 || data.sumv < 0 || data.totalv < 0) && !["aia"].includes(ENT)) {
                        let check = false
                        $$("inv:grid").eachRow(id => {
                            const row = $$("inv:grid").getItem(id)
                            if (row.name && row.amount)
                                if ((row.name && row.amount) && (!row.type || row.type != CK)) {
                                    check = true
                                }

                        })
                        if (check && ((data.root && data.root.typ != 3))) {
                            webix.message(_("amount_positive"), "error")
                            return false
                        } else return true
                    }
                    if (data.totalv >= MAX || data.sumv >= MAX) {
                        webix.message(_("amount_too_large"), "error")
                        return false
                    }
                    if (data.bmail) {
                        const rows = data.bmail.split(/[ ,;]+/)
                        for (const row of rows) {
                            if (!webix.rules.isEmail(row.trim())) {
                                webix.message(_("mail_invalid"))
                                $$("bmail").focus()
                                return false
                            }
                        }
                    }
                    if (type == "03XKNB" || type == "04HGDL") {
                        if (!webix.rules.isNotEmpty(data.ordno) || !webix.rules.isNotEmpty(data.orddt) || !webix.rules.isNotEmpty(data.whsfr) || !webix.rules.isNotEmpty(data.whsto)) return false
                        const odt = new Date(data.orddt).setHours(0, 0, 0, 0), idt = new Date(data.idt).setHours(0, 0, 0, 0)
                        if (odt > idt) {
                            webix.message(_("orddt_invalid"))
                            $$("orddt").focus()
                            return false
                        }
                    }
                    else {
                        if (ENT == "ghtk") {
                            if (!webix.rules.isNotEmpty(data.bname)) return false
                        } else {
                            if (!webix.rules.isNotEmpty(data.bname) || !webix.rules.isNotEmpty(data.baddr)) return false
                        }
                    }
                    if (this.getParam("action") == "adjdif") {
                        if (data.root && data.root.typ == 3) {
                            webix.message(_("adjType_3_total_must_negative"), "error")
                            return false
                        }
                    }
                    if (action == "cancel" || action == "wcancel" || action == "replace") {
                        const adj = data.adj
                        // if (!webix.rules.isNotEmpty(adj.rdt) || !webix.rules.isNotEmpty(adj.ref)) return false
                        const idt = new Date(data.idt).setHours(0, 0, 0, 0), rdt = new Date(adj.rdt).setHours(0, 0, 0, 0)
                        if (action == "replace") {
                            const adt = new Date(adj.idt).setHours(0, 0, 0, 0)
                            if (rdt < adt) {
                                webix.message(_("adj.rdt_msg1"))
                                $$("adj.rdt").focus()
                                return false
                            }
                            if (rdt > idt) {
                                webix.message(_("adj.rdt_msg2"))
                                $$("adj.rdt").focus()
                                return false
                            }
                            $$("curr").disable()
                            $$("exrt").disable()
                        }
                        else {
                            if (rdt < idt) {
                                webix.message(_("adj.rdt_msg3"))
                                $$("adj.rdt").focus()
                                return false
                            }
                            let curdate = new Date().setHours(0, 0, 0, 0)
                            if (rdt > curdate) {
                                webix.message(_("adj.rdt_msg5"))
                                $$("adj.rdt").focus()
                                return false
                            }
                        }
                    }
                    return true
                }
            },
            on: {
                onAfterValidation: (result, value) => {
                    if (!result) {
                        if (JSON.stringify(value.valueOf(0)) !== JSON.stringify({})) webix.message(_("required_msg"),"error")
                    }
                }
            }
        }
        let title_page = {
            view: "label",id:"label_inv", label: `<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("invoice_issue")}</span>`
        }
        return { cols: [{ rows: [title_page, form] }] }
    }
    urlChange(urls) {
        let me = this, grid = $$("inv:grid"), form = $$("inv:form")
        me.id = me.getParam("id")
        if (me.getParam("action") == "adjdif") {
            $$("label_inv").define("label", `<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
            ${_("adj_dif")}</span>`)
            $$("label_inv").refresh()
            createAdjDif = true
            $$("r_adj_dif").show()
            $$("inv:r5").show()
            form.showBatch("adjdif", true)
            $$("adj.des").hide()
        } else {
            $$("label_inv").define("label", `<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
            ${_("invoice_issue")}</span>`)
            $$("label_inv").refresh()
            createAdjDif = false
            $$("r_adj_dif").hide()
            $$("inv:r5").hide()
            form.showBatch("batch3", false)
            $$("adj.des").show()
        }
        if (me.id) {
            parse = true
            action = me.getParam("action")
            if (!action) action = "view"
            $$("type").disable()
            let json
            webix.ajax(`api/inv/${me.id}`).then(data => {
                json = data.json()
                for (let item of json.items) {
                    if(item.price == null) item.price = ''
                    if(item.quantity == null) item.quantity = ''
                    if(item.discountrate == null) item.discountrate = ''
                    if(item.discountamt == null) item.discountamt = ''
                }
                type = json.type
                if (action == "view") {
                    $$("form").getList().add({ id: json.form, value: json.form }, 0)
                    $$("serial").getList().add({ id: json.serial, value: json.serial }, 0)
                }
                if (action == "copy") {
                    delete json["ma_cqthu"]
                }
            }).then(() => {
                let lang = webix.storage.local.get("lang")
                webix.ajax().post(`api/cus`,{itype:type,lang:lang}).then(result => {
                    reset_form(result)
                }).then(() => {
                    form.parse(json)
                }).then(() => {
                    let lang = webix.storage.local.get("lang")
                    webix.ajax().post(`api/dts`,{itype:type,lang:lang}).then(result => reset_grid(result)).then(() => { 
                        for (const row of json.items) {
                            if(!row.vatCode) row.vatCode = getvatCode(row.vrt)
                        }
                        grid.parse(json.items)
                    }).then(() => {
                        if (action == "copy") {
                            delete json["ma_cqthu"]
                            if (json.adjdif == 1) {
                                createAdjDif = true
                                $$("r_adj_dif").show()
                            } else {
                                createAdjDif = false
                                $$("r_adj_dif").hide()
                            }
                            webix.ajax("api/sysdate").then(dt => {
                                const sysdate = dt.json()
                                form.setValues({ idt: sysdate, orddt: sysdate, seq: null, idt_: sysdate, note: null }, true)
                            })
                        }
                        else if (action == "replace") {
                            webix.ajax("api/sysdate").then(dt => {
                                const sysdate = dt.json(), idt = json.idt, serial = json.serial, seq = json.seq, sec = json.sec, c6 = json.c6
                                const nv = { idt: sysdate, idt_: sysdate, orddt: sysdate, seq: null, "adj.form": json.form,"adj.serial": json.serial, "adj.seq": seq, "adj.ref": me.id, "adj.rdt": sysdate, "adj.idt": idt, "adj.typ": 1, "adj.des": `Thay thế cho hóa đơn số ${seq} ký hiệu ${serial} mẫu số ${json.form} ngày ${webix.i18n.dateFormatStr(new Date(idt))} `, "adj.type_ref": json.type_ref || 3 }
                                if (!json.pid) nv.root = { sec: ENT != "vcm" ? sec : c6, form: json.form, serial: serial, seq: seq, idt: idt, btax: json.btax, bname: json.bname }
                               
                                    if (json.period_type) nv["adj.periodtype"] = json.period_type
                                    if (json.period) nv["adj.period"] = json.period
                                    if (json.list_invid) nv["adj.list_invid"] = json.list_invid
                                
                                form.setValues(nv, true)
                                form.showBatch("batch3", true)
                                if (!["fhs", "fbc"].includes(ENT)) {
                                    $$('inv:btndoc').show()
                                    if (ROLE.includes('PERM_SEARCH_INV_CANCEL_CREATE_MINUTES')) $$("inv:btndoc").enable()
                                }
                                $$('inv:file').enable()
                                $$("curr").disable()
                                $$("exrt").disable()
                            })
                        }
                        else if (action == "cancel") {
                            webix.ajax("api/sysdate").then(dt => {
                                const sysdate = dt.json()
                                $$("form").disable()
                                $$("serial").disable()
                                form.setValues({ "adj.seq": null, "adj.rdt": sysdate, "adj.idt": null, "adj.typ": 3, "adj.des": null, "adj.ref": me.id, "adj.rea": null, idt_: sysdate }, true)
                                form.showBatch("batch3", true)
                                $$("inv:btncancel").show()
                                if (ROLE.includes('PERM_INVOICE_CANCEL') || ROLE.includes('PERM_SEARCH_INV_CANCEL_CANCEL')) $$("inv:btncancel").enable()
                                $$("inv:btnsave").hide()
                                $$("adj.des").hide()
                                if (!["fhs", "fbc"].includes(ENT)) {
                                    $$('inv:btndoc').show()
                                    if (ROLE.includes('PERM_SEARCH_INV_CANCEL_CREATE_MINUTES')) {
                                        $$("inv:btndoc").enable()
                                        $$("curr").disable()
                                        $$("exrt").disable()
                                    }
                                }
                                $$('inv:file').enable()
                            })
                        }
                        else if (action == "wcancel") {
                            webix.ajax("api/sysdate").then(dt => {
                                const sysdate = dt.json()
                                $$("form").disable()
                                $$("serial").disable()
                                form.setValues({ "adj.seq": null, "adj.rdt": sysdate, "adj.idt": null, "adj.typ": 3, "adj.des": null, "adj.ref": me.id, "adj.rea": null, idt_: sysdate }, true)
                                form.showBatch("batch3", true)
                                if (ROLE.includes('PERM_VOID_WAIT')) $$("inv:btntrash").show()
                                $$("inv:btnsave").hide()
                                $$("adj.des").hide()
                                $$('inv:file').enable() 
                                $$("adj.rea").enable()
                            })
                        }
                        else {
                            $$("form").disable()
                            $$("serial").disable()
                            if (['aia'].includes(ENT)) {
                                let checkapiinvoice = false
                                try {
                                    if ((userapi && userapi != "" && String(userapi).split(",").includes(json.uc))) checkapiinvoice = true
                                } catch (e) {
                                    checkapiinvoice = false
                                }
                                //if (checkapiinvoice) setConfStaFld(aia_atr)
                                // Get ký hiệu hđ cho mỗi aia
                                if (checkapiinvoice) {
                                    $$("serial").getList().add({ id: json.serial, value: json.serial }, 0)
                                    //me.setValue(arr[0].id)
                                    $$("serial").setValue(json.serial)
                                    //console.log("log:", $$("serial").getValue(json.serial));
                                }
                            }
                            if (['vib'].includes(ENT)) {
                                let checkapiinvoice = false
                                try {
                                    if ((userapi && userapi != "" && String(userapi).split(",").includes(json.uc))) checkapiinvoice = true
                                } catch (e) {
                                    checkapiinvoice = false
                                }
                                if (checkapiinvoice) setConfStaFld(vib_atr)
                                if (json.sourceinv == "COREAPI") setConfStaFld(vib_atr)
                            }
                            if (['aia'].includes(ENT) && json.status == 1) {
                                $$("form").enable()
                                $$("serial").enable()
                            }

                            if (json.pid) form.showBatch("batch3", true)
                            if (json.status == 1) $$("inv:btndel").show()
                            if (json.pid && json.adj && json.adj.typ == 1) $$('inv:file').enable()
                            if (json.pid) {
                                $$("curr").disable()
                                $$("exrt").disable()
                            }
                        }
                    })
                })
            })
            if (action == "replace") {
                // if(DEGREE_CONFIG == '119'){
                    $$("inv:btnissrep").show()
                    $$("inv:btnissrep").disable()
                    if (ROLE.includes('PERM_CREATE_INV_MANAGE_ISSUE')) $$("inv:btnissrep").hide() // ẩn button phát hành
                // } 
            }
        }
        else {
            if (ENT == "jpm") $$("idt").enable()
            parse = false
            action = "new"
            type = "01GTKT"
            grid.clearAll()
            form.clear()
            new_form()
            if(DEGREE_CONFIG == '119'){
                $$("inv:btniss").show()
                $$("inv:btniss").disable()
                if (ROLE.includes('PERM_CREATE_INV_MANAGE_ISSUE')) $$("inv:btniss").enable()
            } 
            //if (["vib"].includes(ENT))  $$('inv:fileatt').enable()
        }
        if (['vib'].includes(ENT)) {
            $$("inv:btniss").hide()
            $$("inv:btnissrep").hide()
        }
    }
    ready(v, urls) {
        if (this.id) setTimeout(() => { parse = false }, 3000)
        //Cau hinh rieng cac cot tĩnh
        URLSS = urls
        if (!ROLE.includes('PERM_CREATE_INV_MANAGE')) $$("inv:btnsave").hide()

        webix.ajax().get("api/fld/conf", { url: urls[0].page, params: urls[0].params }).then(result => {
            let json = result.json()
            //console.log(JSON.stringify(json))
            setConfStaFld(json)
        })
        //
    }
    init() {
        if (!ROLE.includes('PERM_CREATE_INV_MANAGE')) {
            this.show("/top/home")
            window.location.replace(`/${homepage}`)
            return
        }
        if(["cancel", "wcancel"].includes(this.getParam("action")) && ENT != "apple") {
            $$("invlistnum").disable()
            $$("invlistdt").disable()
        }
        const note =  {id:"*",value:""} 
        $$("paym").getList().add(note, 0)
        //trung bo xung load lai cot dong de them tinh nang song ngu
        const type_inv = (ITYPE(this.app))[0].id

        let lang = webix.storage.local.get("lang"), uploader = $$("inv:file")//,uploaderAtt = $$("inv:fileatt")
        webix.ajax().post(`api/cus`,{itype:type_inv,lang:lang}).then(result => reset_form(result)).then(() => webix.ajax().post(`api/dts`,{itype:type_inv,lang:lang}).then(rs => reset_grid(rs)))
        webix.event($$("inv:form:root.serial").getNode(), "keyup", e => { $$("inv:form:root.serial").setValue($$("inv:form:root.serial").getValue().toUpperCase()) }, { bind: $$("inv:form:root.serial") })
        this.Iwin = this.ui(Iwin)
        this.IorgWin = this.ui(IorgWin)
        this.IorgsWin = this.ui(IorgsWin)
        this.IgnsWin = this.ui(IgnsWin)
        let grid = $$("inv:grid"), form = $$("inv:form")

        if (["dtt"].includes(ENT)) {
            uploader.show()
            $$("inv:filelist").show()
        }
        $$("inv:r13").hide()
        if (["vib"].includes(ENT)) {
            // uploaderAtt.show()
            uploader.show()
            $$("inv:filelist").show()
            $$("inv:r13").show()
        }
        
        $$("type").attachEvent("onChange", (newv, oldv) => {

            if (!newv) return
            type = newv
            if (parse) return
            if (newv !== oldv) {
                let lang = webix.storage.local.get("lang")

                webix.ajax().post(`api/cus`,{itype:type,lang:lang}).then(result => reset_form(result)).then(() => webix.ajax().post(`api/dts`,{itype:type,lang:lang}).then(rs => reset_grid(rs)))
            }
        })
        // if (ENT != "sgr") {
        //     $$("total").attachEvent("onChange", function (newv, oldv) {
        //         if (newv && (newv != oldv)) {
        //             let sum = parseFloat($$("sum").getValue())
        //             let vat = parseFloat($$("vat").getValue())
        //             setFooter(newv, sum, vat)
        //         }

        //     })
        // }

        $$("inv:btngettransinfo").attachEvent("onItemClick", () => {
            try {
                webix.ui({
                    view: "popup",
                    id: "popupSearchTrans",
                    // height:400,
                    // width:400,
                    isVisible: true,
                    position: "center",
                    autofocus: true,
                    body: {
                        view: "form",
                        id: "searchTrans:form",
                        elements: [
                            { view: "text", id: "tran_no_label", name: "tran_no", label: _("tran_no_label"), labelWidth: 100, inputAlign: "left" },
                            (ENT == "vib") ? { id: "module_label", name: "module", label: _("module_label"), view: "combo", options: MODULE, labelWidth: 100, required: true } :
                                { view: "text", id: "module_label", name: "module", label: _("module_label"), labelWidth: 100, inputAlign: "left" },

                            { view: "datepicker", id: "trandate_label", name: "trandate", label: _("trandate_label"), labelWidth: 100, inputAlign: "left" },
                            // { view: "datepicker", format: "%d-%m-%Y", id: "trandate_label", name: "trandate_label", label: _("trandate_label"), labelWidth: 100, inputAlign: "left" },
                            {
                                cols: [
                                    {},
                                    { view: "button", id: "btnSearchTrans", value: _("search"), css: "webix_primary" },
                                    { view: "button", id: "btnCancelTrans", value: _("cancel") }
                                ]
                            }
                        ]
                    }
                }).show();
                $$("btnCancelTrans").attachEvent("onItemClick", () => $$("popupSearchTrans").hide())
                $$("btnSearchTrans").attachEvent("onItemClick", () => {
                    try {
                        const param = $$("searchTrans:form").getValues()
                        if (!(param.tran_no && param.module && param.trandate)) {
                            webix.message(_("required_msg"), "error")
                            return false
                        }
                        let short_month_names = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']
                        let dateStr = (param.trandate.getDate() < 10 ? `0${param.trandate.getDate()}` : param.trandate.getDate()) + "-"
                            + short_month_names[param.trandate.getMonth()] + "-"
                            + param.trandate.getFullYear()
                        param.trandate = dateStr
                        webix.ajax().post("api/inv/gettrans", param)
                            .then(data => data.json())
                            .then(data => {
                                if (data.sourceinv == "COREAPI") setConfStaFld(vib_atr)
                                // $$("inv:btnunselect").show()
                                $$("popupSearchTrans").hide()
                                if (data) {
                                    if (data.bcode) {
                                        $$("btax").disable()
                                        $$("buyer").disable()
                                        $$("btel").disable()
                                        $$("bmail").disable()
                                        $$("baddr").disable()
                                        $$("exrt").disable()

                                    } else {
                                        $$("btax").enable()
                                        $$("buyer").enable()
                                        $$("btel").enable()
                                        $$("bmail").enable()
                                        $$("baddr").enable()
                                        $$("exrt").enable()
                                    }
                                    form.parse(data)
                                    let bname_ = data.bname
                                    let btax_ = data.btax
                                    let baddr_ = data.baddr
                                    let bmail_ = data.bmail
                                    let btel_ = data.btel
                                    let buyer_ = data.buyer
                                    let bacc_ = data.bacc
                                    form.setValues({ "bname_": bname_, "btax_": btax_, "baddr_": baddr_, "bmail_": bmail_, "btel_": btel_, "buyer_": buyer_, "bacc_": bacc_ }, true)
                                    grid.clearAll()
                                    grid.parse(data.items)
                                }
                                else {
                                    webix.message({
                                        type: "error",
                                        text: _("error_trans_message")
                                    })
                                }
                            }).catch(err => {
                                console.log(err)
                                if (err && err.message) webix.message((err.message) ? err.message : _("error_trans_message"), "error")
                            })
                    }
                    catch (err) {
                        if (err && err.message) webix.message((err.message) ? err.message : _("error_trans_message"), "error")
                    }
                })

            }
            catch (err) {
                if (err && err.message) webix.message((err.message) ? err.message : _("error_trans_message"), "error")
            }
        })
        // $$("inv:btnunselect").attachEvent("onItemClick", () => {
        //     form.clear()
        //     grid.clearAll()
        //     new_form()
        //     setConfStaFld(vib_atr_enable)
        // })
        $$("inv:btngetcustomerinfo").attachEvent("onItemClick", () => {
            try {
                webix.ui({
                    view: "popup",
                    id: "popupSearchCus",
                    // height:400,
                    // width:400,
                    isVisible: true,
                    position: "center",
                    autofocus: true,
                    body: {
                        view: "form",
                        id: "searchCus:form",
                        elements: [
                            { view: "text", id: "customer_code", name: "cif_no", label: _("customer_code"), inputAlign: "left", placeholder: _("customer_code")},
                            // { view: "datepicker", format: "%d-%m-%Y", id: "trandate_label", name: "trandate_label", label: _("trandate_label"), labelWidth: 100, inputAlign: "left" },
                            {
                                cols: [
                                    {},
                                    { view: "button", id: "btnSearchCus", value: _("search"), css: "webix_primary" },
                                    { view: "button", id: "btnCancelCus", value: _("cancel") }
                                ]
                            }
                        ]
                    }
                }).show();
                $$("btnCancelCus").attachEvent("onItemClick", () => $$("popupSearchCus").hide())
                $$("btnSearchCus").attachEvent("onItemClick", () => {
                    try {
                        const param = $$("searchCus:form").getValues()
                        if (!(param.cif_no)) {
                            webix.message(_("required_msg"), "error")
                            return false
                        }
                        webix.ajax().post("api/org/getcif", param)
                            .then(data => data.json())
                            .then(data => {
                                $$("popupSearchCus").hide()
                                if (data) {
                                    $$("bname").setValue(data.name)
                                    $$("btax").setValue(data.taxc)
                                    $$("baddr").setValue(data.fadd)
                                    $$("bmail").setValue(data.mail)
                                    $$("btel").setValue(data.tel)
                                    $$("bacc").setValue(data.acc)
                                    $$("bbank").setValue(data.bank)
                                    $$("bcode").setValue(data.code)
                                    if (ENT == 'vib') {
                                        $$("bname").disable()
                                        $$("btax").disable()
                                        $$("baddr").disable()
                                        $$("bmail").disable()
                                        $$("btel").disable()
                                        $$("bacc").disable()
                                        $$("bbank").disable()
                                    }


                                }
                                else {
                                    webix.message({
                                        type: "error",
                                        text: _("error_customer_message")
                                    })
                                }
                            }).catch(err => {
                                console.log(err)
                                webix.message(err.message, "error")
                            })
                    }
                    catch (err) {
                        webix.message(err.message, "error")
                    }
                })

            }
            catch (err) {
                webix.message(err.message, "error")
            }
        })



        $$("inv:btnview").attachEvent("onItemClick", () => {
            try {
                if (!$$("serial").getText()) {
                    this.webix.message(_("null_serial"), "error")
                    return
                }
                let inv = createInv(true)
                if (action && action != "wcancel") inv.status = 1
                inv.sec = ""
                webix.ajax().post("api/inv/view", inv).then(result => {
                    const json = result.json()
                    // const req = createRequest(json)
                    // let pdf = res.json()
                    let b64 = json.pdf
                    const blob = dataURItoBlob(b64);

                    var temp_url = window.URL.createObjectURL(blob);
                    $$("iwin:ipdf").define("src", temp_url)
                    $$("iwin:btnapp").hide()
                    $$("iwin:btnxml").hide()
                    $$("iwin:btnsignpdf").hide()
                    $$("iwin:win").show()
                    //jsreport.renderAsync(req).then(res => {
                    //                        $$("iwin:ipdf").define("src", res.toDataURI())
                    // webix.ajax().post("api/seek/report", req).then(res => {
                    //     let pdf = res.json()
                    //     let b64 = pdf.pdf
                    //     const blob = dataURItoBlob(b64);

                    //     var temp_url = window.URL.createObjectURL(blob);
                    //     $$("iwin:ipdf").define("src", temp_url)
                    //     $$("iwin:win").show()
                    // })
                })
            }
            catch (err) {
                webix.message(err.message, "error")
            }
        })

        $$("inv:btnclose").attachEvent("onItemClick", () => {
            webix.confirm(_("exit_confirm"), "confirm-error").then(() => {
                // if (this.id) history.back()
                if (this.id) this.app.show("/top/inv.invs")
                else this.show("home")
            })
        })

        $$("inv:btndel").attachEvent("onItemClick", () => {
            webix.confirm(_("invoice_del_confirm"), "confirm-warning").then(() => {
                const inv = form.getValues()
                const pid = inv.pid ? inv.pid : 0
                webix.ajax(`api/inv/del/${this.id}/${pid}`).then(result => {
                    if (result.json() == 1) {
                        webix.message(_("invoice_del_ok"))
                        // history.back()
                        this.app.show("/top/inv.invs")
                    }
                })
            })
        })
        $$("btax").attachEvent("onBlur", function (code, e) {

            let tax = $$("btax").getValue() ? $$("btax").getValue() : ""
            $$("btax").setValue(tax.replace('-', ''))
            if (($$("btax").getValue()).length > 11) $$("btax").setValue(($$("btax").getValue()).substring(0, 10) + "-" + ($$("btax").getValue()).substring(10))
            //code
        })
        $$("inv:btnorgs").attachEvent("onItemClick", () => {
            this.IorgsWin.show()
        })
        $$("inv:btncancel").attachEvent("onItemClick", () => {
            webix.confirm(`${_("cancel_confirm")} ?`, "confirm-warning").then(() => {
                if (!form.validate()) return
                const adj = form.getValues().adj
                webix.ajax().post(`api/ica/${this.id}`, adj).then(result => {
                    if (result.json() == 1) {
                        webix.message(_("cancel_ok"), "success")
                        // history.back()
                        this.app.show("/top/inv.invs")
                    }
                })
            })
        })
        $$("inv:btntrash").attachEvent("onItemClick", () => {
            webix.confirm(`${_("wait_cancel_confirm")}`, "confirm-warning").then(() => {
                if (!form.validate()) return
                const adj = form.getValues().adj
                webix.ajax().post(`api/iwc/${this.id}`, adj).then(result => {
                    if (result.json() == 1) {
                        webix.message(_("wait_cancel_ok"), "success")
                        // history.back()
                        this.app.show("/top/inv.invs")
                    }
                })
            })
        })

        $$("inv:btndoc").attachEvent("onItemClick", () => {

            if (action == "replace" || action == "cancel" || action == "wcancel") {
                const invoice = form.getValues()
                const idt = new Date($$("idt").getValue()).setHours(0, 0, 0, 0), rdt = new Date($$("adj.rdt").getValue()).setHours(0, 0, 0, 0)
                if (rdt < idt) {
                    webix.message(_("adj.rdt_msg3"))
                    $$("adj.rdt").focus()
                    return false
                }
                if (["cancel", "wcancel"].includes(action)) webix.ajax().response("blob").post(`api/inv/minutes/cancel`, invoice).then(data => webix.html.download(data, "BB_Huy.docx"))
                else {
                    const id = this.id
                    let inv = createInv()
                    webix.ajax().response("blob").post(`api/inv/minutes/replace`, { id: id, inv: inv }).then(data => webix.html.download(data, "BB_ThayThe.docx"))
                }

            }
            // if (action != "view") {
            //     const id = this.id
            //     webix.ajax().response("blob").post(`api/inv/minutes/${action}`, { idOldInvoice: id, infoNewInvoice: JSON.stringify(form.getValues()), items: JSON.stringify(grid.serialize()) }).then(data => webix.html.download(data, `BB_${action == 'replace' ? 'ThayThe' : 'Huy'}.docx`))
            // }
        })

        const ctax = (rows) => {
            let arr = JSON.parse(JSON.stringify(CATTAX))
            for (let v of arr) {
                delete v["vrnc"]
                delete v["status"]
            }
            for (let row of rows) {
                let amt = row.amount, vat = row.vat

                if (!webix.rules.isNumber(amt)) continue
                const vatCode = row.vatCode
                const vrt = row.vrt
                let obj = arr.find(x => x.vatCode == vatCode)
                if (typeof obj == "undefined") continue


                if (["fhs", "fbc"].includes(ENT)) {
                    amt = (amt / (1 + ((Number(vrt) >= 0) ? Number(vrt) : 0) / 100))

                }
                if (row.type == CK) amt = -amt

                obj.amt += amt
                if (amt == 0) obj.isZero = true

                if (["fhs", "fbc"].includes(ENT)) {

                    if (obj.hasOwnProperty("vat")) obj.vat += amt * ((Number(obj.vrt) >= 0) ? Number(obj.vrt) : 0) / 100
                } else {
                    // if (obj.hasOwnProperty("vat")) obj.vat += amt * vrt / 100
                    if (row.type == CK && vat) vat = -vat
                    if (obj.hasOwnProperty("vat")) obj.vat += vat
                }
            }
            const result = arr.filter(obj => { return obj.amt !== 0 || (obj.hasOwnProperty("isZero") && delete obj.isZero) })
            let dataarr = []
            for (let data of result) {
                dataarr.push(data)
            }
            return dataarr
        }

        const createInv = (view) => {
            //Sửa SUM + VAT
            
            const sumo = $$("sum").getValue(), vato = $$("vat").getValue(), totalo = $$("total").getValue(), sumvo = $$("sumv").getValue(), vatvo = $$("vatv").getValue(), totalvo = $$("totalv").getValue(), wordo = $$("word").getValue()
            //
            let del = []
            let nodel = []
            grid.eachRow(id => {
                const row = grid.getItem(id)
                if (!row.name && !row.amount) del.push(id)
                if (!row.name && row.amount) nodel.push(id)
            })
            if (!isEmpty(del)) grid.remove(del)
            if (!isEmpty(nodel)) throw new Error(_("catalog_items_not_emty"))
            if (!grid.validate()) throw new Error(_("details_data_required"))
            let items = grid.serialize()

            if (isEmpty(items)) throw new Error(_("details_required"))
            for (let item of items) {
                if (ENT == "sgr") {
                    if (item.vat < 0) item.vat = -1 * item.vat
                    if (item.total < 0) item.total = -1 * item.total
                }
                if (type == "01GTKT"||type =="01/TVE") item.vrn = (Number(item.vrt) < 0) ? "\\" : getvrn(item.vatCode)
                else delete item["vrt"]

                if (["fhs", "fbc"].includes(ENT) && item["c5"] == "") delete item["c5"]

                // Bỏ vat và total trong trường hợp hóa đơn bán hàng
                if(type == "02GTTT") {
                    delete item["vatCode"]
                    delete item["vat"]
                    delete item["vatv"]
                    item.total= item.amount
                    item.totalv= item.amountv
                }
            }
            let inv
            if (type == "01GTKT" ||type =="01/TVE") {
                const result = ctax(items)
                if (VCM) {
                    const score = $$("score").getValue()
                    if (webix.rules.isNumber(score)) {
                        let sum = 0, vat = 0, total = 0, val
                        for (const row of result) {
                            total += row.amt
                            if (row.hasOwnProperty("vat")) total += row.vat
                        }
                        for (const row of result) {
                            const vrt = Number(row.vrt), amt = row.amt
                            if (vrt > 0) {
                                val = 100 * score * (amt + row.vat) / ((100 + vrt) * total)
                                const VMT = (amt - val) * vrt / 100
                                row.vat = VMT
                                vat += VMT
                            }
                            else val = score * amt / total
                            const AMT = amt - val
                            row.amt = AMT
                            sum += AMT
                        }
                        setFooter((sum + vat), sum, vat)
                    }
                }
                const exrt = $$("exrt").getValue()
                if (webix.rules.isNumber(exrt)) {
                    for (const row of result) {
                        row.amtv = Math.round(row.amt * exrt)
                        if (row.hasOwnProperty("vat")) {
                            if (row.vat < 0) {
                                row.vatv = -1 * Math.round(Math.abs(row.vat) * exrt)
                            }
                            else {
                                row.vatv = Math.round(row.vat * exrt)
                            }
                        }
                    }
                }
                inv = form.getValues()
                inv.tax = result
            }
            else {
                inv = form.getValues()
                delete inv["vat"]
                delete inv["vatv"]
            }
            inv.name = $$("type").getText().toUpperCase()
            inv.items = items
            if (!VCM) inv.score = 0
            inv.hascode = SESS_STATEMENT_HASCODE

            //Sửa SUM + VAT
            inv.sum = sumo
            inv.vat = vato
            inv.total = totalo
            inv.sumv = sumvo
            inv.vatv = vatvo
            inv.totalv = totalvo
            inv.word = wordo
            if (ENT == 'vib') inv.id_file = $$("id_file").getValue()

            if (ENT == 'vib' && ((inv.id && inv.id < 0) || inv.sourceinv == "COREAPI")) {
                let bname_, btax_, baddr_, bmail_, btel_, buyer_, bacc_
                bname_ = inv.bname
                btax_ = inv.btax
                baddr_ = inv.baddr
                bmail_ = inv.bmail
                btel_ = inv.btel
                buyer_ = inv.buyer
                bacc_ = inv.bacc
                inv.bname = inv.bname_
                inv.btax = inv.btax_
                inv.baddr = inv.baddr_
                inv.bmail = inv.bmail_
                inv.btel = inv.btel_
                inv.buyer = inv.buyer_
                inv.bacc = inv.bacc_

                inv.bname_ = bname_
                inv.btax_ = btax_
                inv.baddr_ = baddr_
                inv.bmail_ = bmail_
                inv.btel_ = btel_
                inv.buyer_ = buyer_
                inv.bacc_ = bacc_
                if (inv.bname_ != inv.bname || inv.btax_ != inv.btax || inv.baddr_ != inv.baddr || inv.bmail_ != inv.bmail || inv.btel_ != inv.btel || inv.buyer_ != inv.buyer || inv.bacc_ != inv.bacc) {
                    inv.edit = 1
                }

            }
            if (view) {
                $$("sum").setValue(sumo)
                $$("vat").setValue(vato)
                $$("total").setValue(totalo)
                $$("sumv").setValue(sumvo)
                $$("vatv").setValue(vatvo)
                $$("totalv").setValue(totalvo)
                $$("word").setValue(wordo)
            }
            //
            if (["02GTTT"].includes(inv.type)) inv.inv_ntz = 0
            else if ("07KPTQ" == inv.type) inv.inv_ntz = 1
            inv.type_ref = 1
            if (type == "03XKNB" || type == "04HGDL") {
                delete inv["paym"]
                delete inv["bacc"]
                delete inv["bcode"]
                //delete inv["btax"]
                delete inv["btel"]
                delete inv["baddr"]
                delete inv["bbank"]
                //delete inv["bmail"]
                delete inv["bname"]
                delete inv["buyer"]
                delete inv["discount"]
                delete inv["vat"]
                delete inv["vatv"]
                delete inv["invlistnum"]
                delete inv["invlistdt"]
            }
            else {
                delete inv["ordno"]
                delete inv["orddt"]
                delete inv["ordou"]
                delete inv["ordre"]
                delete inv["recvr"]
                delete inv["trans"]
                delete inv["vehic"]
                delete inv["contr"]
                delete inv["whsfr"]
                delete inv["whsto"]
            }
            //
            let adjdif = 0
            if (!createAdjDif) delete inv.root
            else {
                let des = "", adj = inv.adj, type_ref = 1, formm = inv.root.form
                var format = webix.Date.dateToStr("%d/%m/%Y")
                if (formm.length > 1) type_ref = 3

                if(inv.root.typ == 1) {
                    des = `Thay thế cho hóa đơn số ${inv.root.seq} ký hiệu ${inv.root.serial} mẫu số ${inv.root.form} ngày ${format(new Date(inv.root.idt))}`
                    adj = {...adj, seq: inv.root.seq, form: inv.root.form, serial: inv.root.serial, idt: inv.root.idt, typ: 1, type_ref: type_ref, des: des }
                }
                if ([2,3,4].includes(inv.root.typ)) {
                    inv.adjType = inv.root.typ
                    if(inv.root.typ == 2) {
                        des = `Điều chỉnh tăng ` + adj.rea + ` cho hóa đơn số ${inv.root.seq} ký hiệu ${inv.root.serial} mẫu số ${inv.root.form} ngày ${format(new Date(inv.root.idt))}`
                    }
                    if(inv.root.typ == 3) {
                        des = `Điều chỉnh giảm ` + adj.rea + ` cho hóa đơn số ${inv.root.seq} ký hiệu ${inv.root.serial} mẫu số ${inv.root.form} ngày ${format(new Date(inv.root.idt))}`
                    }
                    if(inv.root.typ == 4) {
                        des = `Điều chỉnh thông tin ` + adj.rea + ` cho hóa đơn số ${inv.root.seq} ký hiệu ${inv.root.serial} mẫu số ${inv.root.form} ngày ${format(new Date(inv.root.idt))}`
                    }
                    adj = { ...adj, seq: inv.root.seq, form: inv.root.form, serial: inv.root.serial, idt: inv.root.idt, typ: 2, type_ref: type_ref, adjType: inv.root.typ, des: des }
                }
                adjdif = 1
                inv.adj = adj
            }
            inv.adjdif = adjdif
            return inv
        }


        const save = (sign) => {
            if (!$$("serial").getText()) throw new Error("Thiếu ký hiệu \n (Serial is not exist)")
            let inv = createInv()
            if(inv.paym == '*')  delete inv["paym"]
            if (sign) {
                delete inv["adj"]
                webix.ajax().post("api/sav", inv).then(result => {
                    const i = result.json()
                    if (i) {
                        grid.clearAll()
                        form.clear()
                        new_form()
                        webix.message(`${_("invoice_saved")} ${i}`, "success", 10000)
                        webix.ajax("api/sign", { id: i }).then(result => {
                            const rows = result.json()
                            if (rows == 1) {
                                webix.message(_("invoice_issued"), "success")
                            }
                            else {
                                webix.ajax().post(`${USB}xml`, rows).then(result => {
                                    const rows = result.json()
                                    webix.ajax().put("api/sign", rows).then(result => {
                                        if (result.json()) webix.message(_("invoice_issued"), "success")
                                    })
                                }).catch(e => { webix.message(_("certificate_error"), "error") })
                            }
                        })
                    }
                }).catch(e => { webix.message(_("invoice_issue_error"), "error") })
            }
            else {
                const id = this.id
                if (action == "replace") {
                    inv.pid = id
                    webix.ajax().post("api/rep", inv).then(result => {
                        let iid = result.json()
                        if (iid) {
                            const files = uploader.files, file_id = files.getFirstId()
                            // history.back()
                            // if(file_id && ENT != ){
                            //     upload(iid);
                            // }else{
                            //     webix.message(`${_("rep_inv")} ID: ${iid}`, "success", 10000)
                            //     this.app.show("/top/inv.invs")
                            // }
                            webix.message(`${_("rep_inv")} ID: ${iid}`, "success", 10000)
                            this.app.show("/top/inv.invs")

                        }
                        if (ROLE.includes('PERM_CREATE_INV_MANAGE_SAVE')) $$("inv:btnsave").enable()
                        //Duy an hien
                    }).catch(() => {
                        if (ROLE.includes('PERM_CREATE_INV_MANAGE_SAVE')) $$("inv:btnsave").enable() //Duy an hien
                    })
                }
                else if (action == "copy") {
                    delete inv["adj"]
                    delete inv["pid"]
                    webix.ajax().post("api/inv", inv).then(result => {
                        const id = result.json()
                        if (id) {
                            const files = uploader.files, file_id = files.getFirstId()
                            // history.back()
                            // if(file_id){
                            //     upload(id);
                            // }else{
                            //     webix.message(`${_("invoice_copied")} ${id}`, "success", 10000)
                            //     this.app.show("/top/inv.invs")
                            // }
                            webix.message(`${_("invoice_copied")} ${id}`, "success", 10000)
                            this.app.show("/top/inv.invs")
                            // history.back()


                        }
                    }).finally(() => {
                        if (ROLE.includes('PERM_CREATE_INV_MANAGE_SAVE')) $$("inv:btnsave").enable()
                    }) //Duy an hien

                }
                else {
                    if (!inv.pid && !inv.root) delete inv["adj"]
                    if (id) {

                        webix.ajax().put(`api/inv/${id}`, inv).then(result => {
                            if (result.json() == 1) {

                                // history.back()
                                //const files = uploader.files, file_id = files.getFirstId()
                                // history.back()

                                // upload(id);

                                webix.message(`${_("invoice_updated")} ${id}`, "success", 10000)
                                this.app.show("/top/inv.invs")



                            } else webix.message(_("invoice_not_updated"), "error")
                        }).finally(() => {
                            if (ROLE.includes('PERM_CREATE_INV_MANAGE_SAVE')) $$("inv:btnsave").enable()
                        }) //Duy an hien
                    }
                    else {

                        webix.ajax().post("api/inv", inv).then(result => {
                            let id = result.json()
                            if (id) {
                                webix.message(`${_("invoice_saved")} ${id}`, "success", 10000)
                                grid.clearAll()
                                form.clear()
                                new_form()
                                if (ENT == 'vib') {
                                    uploader.setValue("")
                                }

                                // $$("inv:btnsave").enable()  
                            }
                            // upload(id);
                        }).finally(() => {
                            if (ROLE.includes('PERM_CREATE_INV_MANAGE_SAVE')) $$("inv:btnsave").enable()
                        }) //Duy an hien
                    }
                }
            }
        }
        $$("inv:btnsave").attachEvent("onItemClick", () => {
            let vform = $$("form").getValue()
            let vserial = $$("serial").getValue()
            let vlable = _("invoice_save_confirm2").replace('@@', vform + '-' + vserial)
            webix.confirm(vlable, "confirm-warning").then(() => {
                var bien = $$("btax").getValue()
                let _serial23char = vserial.slice(1,3)
                var format = webix.Date.dateToStr("%Y%m%d")
                let inv_ = form.getValues()
                inv_.invType = $$("bc2").getValue()

                var curd = format(inv_.idt_ ? inv_.idt_ : new Date())
                var invoice_date = format($$("idt").getValue())
                let eff_year = invoice_date.slice(2,4)

                if(eff_year != _serial23char){
                    this.webix.message(_("year_of_idt_must_belong_year_of_serial"), "error")
                    return
                }
                if (!invoice_date) {
                    this.webix.message(_("idt_not_empty"), "error")
                    return
                }

                if (Number(invoice_date) > Number(curd)) {
                    this.webix.message(_("idt_notgreater_curdt"), "error")
                    return
                }
                if (["baca"].includes(ENT)) {
                    var date = 1296000000
                    if ((new Date).getTime() - (new Date($$("idt").getValue())).getTime() > date) {
                        webix.message(_("idt_not15before_curdt"));
                        return
                    }
                }
                if (bien.length > 10 && !bien.includes("-")) {
                    var output = [bien.slice(0, 10), "-", bien.slice(10)].join('');
                    $$("btax").setValue(output)
                }

                if (!$$("serial").getText()) {
                    this.webix.message(_("null_serial"), "error")
                    return
                }
                if (!form.validate()) return
                $$("inv:btnsave").disable()  //Duy an hien
                try {
                    const paym = $$("paym").getValue(), totalv = webix.Number.parse($$("totalv").getValue(), webix.i18n)
                    if (paym == "TM" && totalv >= 20000000) webix.confirm(_("invoice_20cu_confirm"), "confirm-warning").then(() => { save(false) })
                        .catch(() => {
                            if (ROLE.includes('PERM_CREATE_INV_MANAGE_SAVE')) $$("inv:btnsave").enable()
                        })  //Duy an hien
                    else save(false)
                }
                catch (err) {
                    if (ROLE.includes('PERM_CREATE_INV_MANAGE_SAVE')) $$("inv:btnsave").enable() //Duy an hien
                    webix.message(err.message, "error")
                }
            })
        })

        $$("inv:btniss").attachEvent("onItemClick", () => {
            let vform = $$("form").getValue()
            let vserial = $$("serial").getValue()
            let vlable = _("invoice_issue_confirm2").replace('@@', vform + '-' + vserial)
            webix.confirm(vlable, "confirm-warning").then(() => {
                let inv = form.getValues()
                if (!form.validate()) return

                webix.ajax("api/inv/count/12", { form: inv.form, serial: inv.serial }).then(result => {
                    const json = result.json()
                    if (json.rc) {
                        webix.message(_("invoice_issue_exists_error"), "error")
                        return
                    }
                    try {
                        const paym = $$("paym").getValue(), totalv = webix.Number.parse($$("totalv").getValue(), webix.i18n)
                        if (paym == "TM" && totalv >= 20000000) webix.confirm(_("invoice_20cu_confirm"), "confirm-warning").then(() => { save(true) })
                        else save(true)
                    }
                    catch (err) {
                        webix.message(err.message, "error")
                    }
                })
            })
        })
        // const upload = (id) =>{
        //     const files = uploader.files, file_id = files.getFirstId()
        //      let typemin, invinfo = $$("inv:form").getValues(), iid = this.id
        //         if (action == "replace") typemin = "rep"
        //         else if (action == "cancel") typemin = "can"
        //         else if (action == "wcancel") typemin = "can"
        //         else if (action == "copy") { }
        //         else {
        //             if (iid) {
        //                 if (invinfo.status == 4) typemin = "can"
        //                 else {
        //                     let adjtyp = invinfo.adj.typ
        //                     if (adjtyp == 1) {
        //                         typemin = "rep"
        //                         iid = invinfo.pid
        //                     }
        //                 }
        //             }
        //         }

        //     if (file_id) {
        //         var params = {type: typemin, id: id }
        //         uploader.define("formData", params)
        //         uploader.send(rs => {
        //             if (action == "replace") {
        //                 webix.message(`${_("rep_inv")} ID: ${id}`, "success", 10000)
        //                 this.app.show("/top/inv.invs")
        //             }

        //             if (action == "copy") { 
        //                 webix.message(`${_("invoice_copied")} ${id}`, "success", 10000)
        //                 this.app.show("/top/inv.invs")
        //             }
        //             if (rs.err) webix.message(rs.err, "error")
        //             else if (rs.ok) webix.message(_("up_minutes_success"), "success")
        //             else webix.message(_("up_minutes_fail"), "error")
        //             uploader.setValue("")
        //         })
        //     }

        // }
        const issRepInv = () => {
            // Phat hanh hoa don thay the
            const id = this.id
            if (!$$("serial").getText()) throw new Error(_("null_serial"))
            let inv = createInv()
            inv.AutoSeq = true
            $$("inv:btnsave").disable()
            
            // Tao hoa don thay the
            if (action == "replace") {
                inv.pid = id
                webix.ajax().post("api/rep", inv).then(result => {
                    let i = result.json()
                    if (i) {
                        webix.message(`${_("rep_inv")} ID: ${i}`, "success", 10000)
                        // Ky so hoa don
                        if (i) {
                            grid.clearAll()
                            form.clear()
                            new_form()
                            webix.message(`${_("invoice_saved")} ${i}`, "success", 10000)
                            delete inv["adj"]
                            webix.ajax("api/sign", { id: i }).then(result => {
                                const rows = result.json()
                                if (rows == 1) {
                                    webix.message(_("invoice_issued"), "success")
                                    this.app.show("/top/inv.invs")
                                }
                                else {

                                    webix.ajax().post(`${USB}xml`, rows).then(result => {
                                        const rows = result.json()
                                        webix.ajax().put("api/sign", rows).then(result => {
                                            if (result.json()) webix.message(_("invoice_issued"), "success")
                                            this.app.show("/top/inv.invs")
                                        }).catch(e => {
                                            this.app.show("/top/inv.invs")
                                        })
                                    }).catch(e => {
                                        webix.message(_("certificate_error"), "error")
                                        this.app.show("/top/inv.invs")
                                    })
                                }
                            }).catch(e => {
                                this.app.show("/top/inv.invs")
                            })
                        }
                    }
                }).catch(e => {
                    webix.message(_("invoice_issue_error"), "error")
                    this.app.show("/top/inv.invs")
                })
            }
            // 
        }

        $$("inv:btnissrep").attachEvent("onItemClick", () => {
            webix.confirm(_("invoice_issue_confirm"), "confirm-warning").then(() => {
                let inv = form.getValues()
                if (!form.validate()) return
                webix.ajax("api/inv/count/12", { form: inv.form, serial: inv.serial }).then(result => {
                    const json = result.json()
                    if (json.rc) {
                        webix.message(_("invoice_issue_exists_error"), "error")
                        return
                    }
                    try {
                        const paym = $$("paym").getValue(), totalv = webix.Number.parse($$("totalv").getValue(), webix.i18n)
                        if (paym == "TM" && totalv >= 20000000) webix.confirm(_("invoice_20cu_confirm"), "confirm-warning").then(() => { issRepInv() })
                        else issRepInv(true)
                    }
                    catch (err) {
                        webix.message(err.message, "error")
                    }
                })
            })
        })
        const setFooter = (total, sum, vat) => {
            if (!parse) {
                const exrt = $$("exrt").getValue()
                let sumv = 0, vatv = 0, totalv = 0
                const discount = $$("discount").getValue() ? $$("discount").getValue() : 0
                if (webix.rules.isNumber(exrt)) {
                    if(sum < 0) {
                        sumv = -1 * Math.round(Math.abs(sum) * exrt)
                    } 
                    else {
                        sumv = Math.round(sum * exrt)
                    }
                    if(total < 0) {
                        totalv = -1 * Math.round(Math.abs(total) * exrt)
                    }
                    else {
                        totalv = Math.round(total * exrt)
                    }
                    if (type == "01GTKT" || type =="01/TVE") {
                        if(vat < 0) {
                            vatv = -1 * Math.round(Math.abs(vat) * exrt)
                        }
                        else {
                            vatv = Math.round(vat * exrt)
                        }
                    }
                }

                $$("sum").setValue(sum)
                $$("sumv").setValue(sumv)
                $$("total").setValue(total)
                $$("discount").setValue(discount)

                if (type == "01GTKT" || type =="01/TVE") {

                    $$("vat").setValue(vat)
                    $$("vatv").setValue(vatv)
                }
                if (ENT == "sgr") {
                    const c7 = $$("c7").getValue()
                    const c9 = $$("c9").getValue()
                    totalv = sumv + vatv - discount + Number(c7) + Number(c9)
                } else {
                    totalv = sumv + vatv - discount
                }
                $$("totalv").setValue(totalv)
                if (this.getParam("action") == "adjdif")
                    $$("word").setValue(toWords(totalv))
                else 
                    $$("word").setValue(toWords(Math.abs(totalv)))
            }
        }
        const cscore = (score) => {
            if (!parse && VCM) {
                const rows = grid.serialize(), result = ctax(rows)
                let sum = 0, vat = 0, val, total
                for (const row of result) {
                    sum += row.amt
                    if (row.hasOwnProperty("vat")) vat += row.vat
                }
                total = sum + vat
                if (webix.rules.isNumber(score)) {
                    sum = 0
                    vat = 0
                    for (const row of result) {
                        const vrt = Number(row.vrt), amt = row.amt
                        if (vrt > 0) {
                            val = 100 * score * (amt + row.vat) / ((100 + vrt) * total)
                            vat += row.vat - (val * vrt / 100)
                        }
                        else val = score * amt / total
                        sum += (amt - val)
                    }
                    total = sum + vat
                    for (const row of rows) {
                        if (KMMT.includes(row.type)) {
                            total = 0
                            sum =0
                            vat =0
                        }
                    }
                    setFooter(total, sum, vat)
                }
                else {
                    setFooter(total, sum, vat)
                }
            }
        }
        $$("score").attachEvent("onChange", newv => cscore(newv))

        $$("discount").attachEvent("onChange", (newv, oldv) => {
            if (!parse) {
                let total, totalv = 0
                total = $$("total").getValue()
                if (webix.rules.isNumber(newv)) total -= newv
                if (webix.rules.isNumber(oldv)) total += oldv
                const exrt = $$("exrt").getValue()
                if (webix.rules.isNumber(exrt)) totalv = Math.round(total * exrt)
                $$("total").setValue(total)
                $$("totalv").setValue(totalv)
                $$("word").setValue(toWords(Math.abs(totalv)))
            }
        })

        $$("curr").attachEvent("onChange", newv => {
            let exrt
            if (!newv) return
            if (newv == "VND") {
                $$("exrt").setValue(1)
                $$("exrt").disable()
            }
            else {
                $$("exrt").enable()
                if (!parse) webix.ajax("api/exch/rate", { cur: newv, dt: $$("idt").getValue() }).then(data => { $$("exrt").setValue(Number(data.json())) })
            }
            grid.data.each((row, i) => {
                exrt = $$("exrt").getValue()
                if (row.vat) row.vatv = Math.round(row.vatv * exrt)
                if (row.amount) row.amountv = Math.round(row.amount * exrt)
                if (row.total) row.totalv = Math.round(row.total * exrt)

            })
            grid.refresh()
        })

        $$("exrt").attachEvent("onChange", newv => {
            if (!parse && webix.rules.isNumber(newv)) {
                const sum = $$("sum").getValue(), total = $$("total").getValue(), sumv = Math.round(sum * newv)
                //totalv = Math.round(total * newv)
                $$("sumv").setValue(sumv)

                let vatv = 0
                if (type == "01GTKT" || type =="01/TVE") {
                    const vat = $$("vat").getValue()
                    vatv = Math.round(vat * newv)
                    $$("vatv").setValue(vatv)
                }
                let totalv
                if (ENT == "sgr") {
                    const discount = $$("discount").getValue()
                    const c7 = $$("c7").getValue()
                    const c9 = $$("c9").getValue()
                    totalv = sumv + vatv - (discount * newv) + (Number(c7) * newv) + (Number(c9) * newv)
                } else {
                    totalv = sumv + vatv
                }
                $$("totalv").setValue(totalv)
                $$("word").setValue(toWords(Math.abs(totalv)))
                grid.data.each((row, i) => {

                    if (row.vat) row.vatv = Math.round(row.vatv * newv)
                    if (row.amount) row.amountv = Math.round(row.amount * newv)
                    if (row.total) row.totalv = Math.round(row.total * newv)


                })
                grid.refresh()
            }
        })

        grid.attachEvent("onPaste", (text) => {
            const rows = text.split("\n"), columns = grid.config.columns, exc = ["del", "line", "gns"], num = ["price", "quantity", "amount"]
            let cols = []
            for (const column of columns) {
                const id = column.id
                if (!exc.includes(id)) cols.push(id)
            }
            for (const row of rows) {
                if (row) {
                    const arr = row.split("\t")
                    let data = {}
                    for (let index = 0; index < cols.length; index++) {
                        const id = cols[index], val = num.includes(id) ? Number(arr[index]) : arr[index]
                        data[id] = val
                    }
                    grid.add(data)
                }
            }
            grid.refresh()
        })

        grid.data.attachEvent("onStoreUpdated", (id, obj, mode) => {
            const exrt = $$("exrt").getValue()
            let currr = $$("curr").getValue(), NUMBER_DECIMAL = NUMBER_DECIMAL_FORMAT, tradeamount=0
            if (currr == 'VND') NUMBER_DECIMAL = 0
            if (mode == "add" || mode == "delete" || mode == "update") {
                let sum = 0, vat = 0, sumvat = 0
                grid.data.each((row, i) => {
                    if (!row.name || row.name == "") {

                    } else {
                        row.line = i + 1
                        if (row.amount) {
                            // if ($$("curr").getValue() == 'VND') row.amount = Math.round(row.amount*100)/100
                            // else row.amount = Math.round(row.amount * 100) / 100
                            row.amount = Math.round(row.amount * Math.pow(10, NUMBER_DECIMAL)) / Math.pow(10, NUMBER_DECIMAL)
                            row.amountv = Math.round(row.amount * exrt)
                        }
                        if(row.type=="Chiết khấu") tradeamount+=Number(row.amount)
                        let amt = row.amount
                        if (webix.rules.isNumber(amt)) {
                            if (row.type == CK) amt = -amt
                            if (["fhs", "fbc"].includes(ENT)) {
                                if (type == "01GTKT" || type =="01/TVE") {
                                    let vrt1 = (Number(row.vrt) >= 0) ? Number(row.vrt) : 0
                                    sum += (amt / (1 + vrt1 / 100))
                                } else {
                                    sum += amt
                                }
                                sumvat += amt
                            } else {
                                sum += amt
                            }

                            if (type == "01GTKT" || type =="01/TVE") {
                                let vrt = Number(row.vrt)
                                if (vrt > 0) {
                                    if (["fhs", "fbc"].includes(ENT)) {//ttien da bao gom thue
                                        // const VMT = amt * vrt / 100
                                        let VMT = (amt / (1 + ((vrt >= 0) ? vrt : 0) / 100)) * ((vrt >= 0) ? vrt : 0) / 100
                                        vat += VMT
                                        row.vat = VMT
                                        row.vatv = Math.round(VMT * exrt)
                                        row.total = amt
                                        row.totalv = Math.round(amt * exrt)
                                    } else {
                                        //const VMT = amt * vrt / 100
                                        let VMT = ((row.vat) ? row.vat : amt * vrt / 100)
                                        if (row.type == CK) VMT = -Math.abs(VMT)
                                        vat += VMT
                                        if (!row.vat) {
                                            if (this.getParam("action") == "adjdif") {
                                                row.vat = Math.round(VMT * Math.pow(10, NUMBER_DECIMAL)) / Math.pow(10, NUMBER_DECIMAL)
                                                row.vatv = Math.round(VMT * exrt)
                                            }
                                            else {
                                                row.vat = Math.abs(Math.round(VMT * Math.pow(10, NUMBER_DECIMAL)) / Math.pow(10, NUMBER_DECIMAL))
                                                row.vatv = Math.abs(Math.round(VMT * exrt))
                                            }
                                        }
                                        if (!row.total) {
                                            if (this.getParam("action") == "adjdif") {
                                                row.total = Math.round((VMT + amt)*Math.pow(10, NUMBER_DECIMAL))/Math.pow(10, NUMBER_DECIMAL)
                                                row.totalv = Math.round(amt * exrt)
                                            }
                                            else {
                                                row.total = Math.abs(Math.round((VMT + amt)*Math.pow(10, NUMBER_DECIMAL))/Math.pow(10, NUMBER_DECIMAL))
                                                row.totalv = Math.abs(Math.round(amt * exrt))
                                            }
                                        }
                                    }

                                }
                                else if (vrt == 0) {
                                    row.vat = 0
                                    row.vatv = 0
                                    if (this.getParam("action") == "adjdif" && $$("inv:form:root.typ").getValue() == 3) {
                                        row.total = amt * Math.pow(10, NUMBER_DECIMAL) / Math.pow(10, NUMBER_DECIMAL)
                                        row.totalv = Math.round(amt * exrt)
                                    }
                                    else {
                                        row.total = Math.abs(amt * Math.pow(10, NUMBER_DECIMAL)) / Math.pow(10, NUMBER_DECIMAL)
                                        row.totalv = Math.abs(Math.round(amt * exrt))
                                    }
                                }
                                else {
                                    row.vat = ""
                                    row.vatv = ""
                                    if (this.getParam("action") == "adjdif" && $$("inv:form:root.typ").getValue() == 3) {
                                        row.total = amt * Math.pow(10, NUMBER_DECIMAL)/Math.pow(10, NUMBER_DECIMAL)
                                        row.totalv = Math.round(amt * exrt)
                                    }
                                    else {
                                        row.total = Math.abs(amt * Math.pow(10, NUMBER_DECIMAL))/Math.pow(10, NUMBER_DECIMAL)
                                        row.totalv = Math.abs(Math.round(amt * exrt))
                                    }
                                }
                            }
                        }
                        else {
                            if (type == "01GTKT" || type =="01/TVE") {
                                row.vat = ""
                                row.total = ""
                                row.vatv = ""
                                row.totalv = ""
                            }
                        }
                    }
                })
                // Duy fix lam tron
                vat = Number(parseFloat((Math.round(vat * Math.pow(10, NUMBER_DECIMAL)) / Math.pow(10, NUMBER_DECIMAL)).toFixed(NUMBER_DECIMAL_FORMAT)))
                var total = 0

                const c7 = (ENT == "sgr") ? $$("c7").getValue() : null
                const c9 = (ENT == "sgr") ? $$("c9").getValue() : null
                if (ENT == "fhs") {
                    total = sumvat

                } else {
                    (ENT == "sgr") ? total = sum + vat + Number(c7) + Number(c9) : total = sum + vat
                }

                if (VCM) {
                    const score = $$("score").getValue()
                    if (webix.rules.isNumber(score)) cscore(score)
                    else setFooter(total, sum, vat)
                }
                else {
                    if (["fhs", "fbc"].includes(ENT)) {//ttien da bao gom thue

                        setFooter(total, sum, vat)
                    } else {
                        setFooter(total, sum, vat)
                    }

                }
                grid.refresh()
                tradeamount = Math.round((tradeamount||0) * Math.pow(10, NUMBER_DECIMAL)) / Math.pow(10, NUMBER_DECIMAL)
                $$("tradeamount").setValue(tradeamount)
            }
        })

        $$("inv:file").attachEvent("onAfterFileAdd", (file) => {//luu file cua vib
            webix.confirm(_("up_minutes_confirm"), "confirm-warning").then(() => {
                let typemin, iid = this.id
                if (action == "replace" && ENT == 'vib') iid = ''
                const files = uploader.files, file_id = files.getFirstId()
                if (file_id) {
                    var params = { type: typemin, id: iid, flag: 1 }
                    uploader.define("formData", params)

                    $$("inv:btnsave").disable()
                    webix.message(_("uploading"), "info", -1, "upload_title");
                    uploader.send(rs => {
                        if (rs.err) {
                            $$("inv:file").setValue("");
                            webix.message(rs.err, "error")
                        }
                        else if (rs.ok) {
                            if (ENT == 'vib') {//xu ly day nhieu file
                                let arr = [], id_file = $$("id_file").getValue()
                                if (id_file) {
                                    arr.push(id_file)
                                    arr.push(rs.id_f)
                                    $$("id_file").setValue(arr)
                                }
                                else {
                                    arr.push(rs.id_f)
                                    $$("id_file").setValue(arr)
                                }
                            }

                            webix.message(_("up_minutes_success"), "success")
                        }
                        else {
                            webix.message(_("up_minutes_fail"), "error")
                            $$("inv:file").setValue("");
                        }

                        webix.message.hide("upload_title");
                        if (ROLE.includes('PERM_CREATE_INV_MANAGE_SAVE')) $$("inv:btnsave").enable()
                    })
                }
                else webix.message(_("file_required"))


            }).fail(function () {
                $$("adj:btnsave").enable()
                $$("inv:file").setValue("");

            });
        })

        //Sửa SUM + VAT
        $$("sum").attachEvent("onChange", newv => {
            let vat = parseFloat($$("vat").getValue())
            const c7 = (ENT == "sgr") ? $$("c7").getValue() : null
            const c9 = (ENT == "sgr") ? $$("c9").getValue() : null
            let discount = $$("discount").getValue()
            if (vat || vat == 0) setFooter((ENT == "sgr") ? newv + vat + Number(c7) + Number(c9) - discount : newv + vat, newv, vat)
        })

        $$("vat").attachEvent("onChange", newv => {
            let sum = parseFloat($$("sum").getValue())
            let discount = $$("discount").getValue()
            const c7 = (ENT == "sgr") ? $$("c7").getValue() : null
            const c9 = (ENT == "sgr") ? $$("c9").getValue() : null

            if (sum || sum == 0) setFooter((ENT == "sgr") ? newv + sum + Number(c7) + Number(c9) - discount : newv + sum, sum, newv)
        })

        //

        webix.UIManager.addHotKey("enter", view => {
            const id = view.getSelectedId()
            if (id) {
                if (id.column === "gns") $$("igns:win").show()
                else view.edit(id)
            }
        }, grid)

        webix.UIManager.addHotKey("Ctrl+1", () => {
            let obj = { quantity: 1 }
            if (type == "01GTKT" || type =="01/TVE") {
                obj.vrt = 10
                obj.vatCode = "VAT1000"
            }
            grid.add(obj)
            grid.clearValidation()
        })


    }
}