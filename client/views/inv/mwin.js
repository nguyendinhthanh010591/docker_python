import { JetView } from "webix-jet"
import { setConfStaFld } from "models/util"
export default class Mwin extends JetView {
    config() {
        _ = this.app.getService("locale")._
        return {
            view: "window",
            id: "mail:win",
            modal: true,
            fullscreen: true,
            head: {
                view: "toolbar"
                , height: 35
                , cols: [
                    { view: "label", label: _("mail_send") },
                    { view: "button", type: "icon", icon: "wxi-close", hotkey: "esc", width: 25, click: () => { $$("mail:win").hide() } }
                ]
            },
            body: {
                view: "form",
                id: "mail:form",
                //borderless: true,
                elements: [
                    { rows: [{ id: "mail:to", name: "mail", view: "multitext", label: "Email", name: "to", attributes: { maxlength: 500 }, required: true }] },
                    { id: "mail:subject", view: "text", label: _("mail_subject"), name: "subject", attributes: { maxlength: 255 }, required: true },
                    { view: "label", label: _("mail_content") },
                    { id: "mail:content", name: "content", view: "template", scroll: "y" },
                    { cols: [{}, { id: "mail:btnsend", view: "button", type: "icon", icon: "mdi mdi-email-plus", label: _("send"), width: 100 }] }
                ],
                rules: {
                    $all: webix.rules.isNotEmpty,
                    $obj: data => {
                        if (data.mail) {
                            const rows = data.mail.split(/[ ,;]+/)
                            for (const row of rows) {
                                if (!webix.rules.isEmail(row.trim())) {
                                    webix.message(_("mail_invalid"))
                                    $$("mail:to").focus()
                                    return false
                                }
                            }
                        }
                        return true
                    }
                }
            }
        }
    }
    ready(v, urls) {
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
    show() {
        const win = this.getRoot()
        win.clear()
        win.show()
    }
}
