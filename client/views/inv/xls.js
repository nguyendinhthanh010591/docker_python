import { JetView } from "webix-jet";
import { ROLE, LinkedInputs3,LinkedInputs0, gridnf, gridnf0, PAYMETHOD, ITYPE, ENT, setConfStaFld, DEGREE_CONFIG } from "models/util";

import Iwin from "views/inv/iwin"
import Rwin from "views/inv/rwin"
let b64
const config = (grid, type, app) => {
    let arr = [
        //hien nut hien thi voi sgr
        { id: "vie", header: "", width: 35, template: `<span class='webix_icon wxi-search', title='${_("view")}'></span>` , hidden: (["sgr"].includes(ENT)) ? false : true},  
        //{ id: "chk", header: { content: "masterCheckbox", css: "center", contentId:"chk1" }, adjust: "header", css: "center", template: "{common.checkbox()}" },
        { id: "chk", header: { content: "masterCheckbox", css: "center", contentId:"chk1" }, checkValue: 'on', uncheckValue: '', adjust: "header",css: "center", template: "{common.checkbox()}" },
        { id: "ic", header: { text: _("invoice_code"), css: "header" } },
        { id: "form", header: { text: _("form"), css: "header" } },
        { id: "serial", header: { text: _("serial"), css: "header" } },
        { id: "idt", header: { text: _("idt"), css: "header" }, format: webix.i18n.dateFormatStr, width: 120 },
        { id: "btax", header: { text: _("taxcode"), css: "header" } },
        { id: "bcode", header: { text: _("cus_code"), css: "header" }, hidden: (["bvb"].includes(ENT)) ? false : true },
        { id: "c6", header: { text: _("btype"), css: "header" }, hidden: (["cimb"].includes(ENT)) ? false : true } ,
        { id: "c7", header: { text: _("CMT"), css: "header" }, hidden: (["cimb"].includes(ENT)) ? false : true },
        { id: "c3", header: { text: _("btype"), css: "header" }, hidden: (["bvb"].includes(ENT)) ? false : true },
        { id: "bname", header: { text: _("bname"), css: "header" } },
        { id: "buyer", header: { text: _("buyer"), css: "header" } },
        { id: "baddr", header: { text: _("address"), css: "header" } },
        { id: "btel", header: { text: _("tel"), css: "header" } },
        { id: "bmail", header: { text: "Mail", css: "header" } },
        { id: "invlistnum", header: { text: _("invlistnum"), css: "header", hidden : ["123"].includes(DEGREE_CONFIG) ? false :true } },
        { id: "invlistdt", header: { text: _("invlistdt"), css: "header" }, hidden : ["123"].includes(DEGREE_CONFIG) ? false :true },
        { id: "bacc", header: { text: _("account"), css: "header" } },
        { id: "bbank", header: { text: _("bank"), css: "header" } },
        { id: "paym", header: { text: _("payment_method"), css: "header" }, collection: PAYMETHOD(app) },
        { id: "curr", header: { text: _("currency"), css: "header" } },
        { id: "exrt", header: { text: _("exchange_rate"), css: "header" }, css: "right", format: gridnf.format },
        { id: "vat", header: { text: "VAT", css: "header" }, css: "right", format: gridnf.format },
        { id: "discountamt", header: { text: _("discountamt"), css: "header" }, css: "right", format: gridnf.format, width: 150, hidden : ["123"].includes(DEGREE_CONFIG) ? false :true },
        { id: "sum", header: { text: _("sumv"), css: "header" }, css: "right", format: gridnf.format, width: 120 },
        { id: "vatCode", header: { text: _("vatCode"), css: "header" }, css: "right", hidden : ["123"].includes(DEGREE_CONFIG) ? false :true }
    ]
    let arrxk = [
        { id: "vie", header: "", width: 35, template: `<span class='webix_icon wxi-search', title='${_("view")}'></span>` , hidden: (["sgr"].includes(ENT)) ? false : true},  
        { id: "chk", header: { content: "masterCheckbox", css: "center", contentId:"chk1" }, checkValue: 'on', uncheckValue: '', adjust: "header",css: "center", template: "{common.checkbox()}" },
        //{ id: "chk", header: { content: "masterCheckbox", css: "center" }, adjust: "header", css: "center", template: "{common.checkbox()}" },
        { id: "ic", header: { text: _("invoice_code"), css: "header" } },
        { id: "form", header: { text: _("form"), css: "header" } },
        { id: "serial", header: { text: _("serial"), css: "header" } },
        { id: "idt", header: { text: _("idt"), css: "header" }, format: webix.i18n.dateFormatStr, width: 120 },
        { id: "ordno", header: { text: _("ordno"), css: "header" }, hidden : ["123"].includes(DEGREE_CONFIG) ? false :true },
        { id: "cenumber", header: { text: _("cenumber"), css: "header", hidden : ["123"].includes(DEGREE_CONFIG) ? false :true } },
        { id: "cedate", header: { text: _("cedate"), css: "header", hidden : ["123"].includes(DEGREE_CONFIG) ? false :true } },
        { id: "orddt", header: { text: _("orddt"), css: "header" } },
        { id: "ordou", header: { text: _("ou"), css: "header" } },
        { id: "ordre", header: { text: _("reason"), css: "header" } },
        { id: "recvr", header: { text: _("receiver"), css: "header" } },
        { id: "trans", header: { text: _("transporter"), css: "header" } },
        { id: "vehic", header: { text: _("vehicle"), css: "header" } },
        { id: "contr", header: { text: _("contract_no"), css: "header" } },
        { id: "whsfr", header: { text: _("export_address"), css: "header" } },
        { id: "whsto", header: { text: _("import_address"), css: "header" } },
        { id: "curr", header: { text: _("currency"), css: "header" } },
        { id: "exrt", header: { text: _("exchange_rate"), css: "header" }, css: "right", format: gridnf.format },
        { id: "sum", header: { text: _("sumv"), css: "header" }, css: "right", format: gridnf.format, width: 120 }
    ]
    if (ENT == "ctbc") {
        arr.push({ id: "bcode", header: { text: _("cus_code"), css: "header" }})
    }
    if (ENT == 'hdb') {
        arr.splice(12, 1) //Xóa cột tiền VND
        arr.splice(12, 0,{ id: "sum", header: { text: _("sumo"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat })
    }

    // if (ENT != "hdb") arr.push(
    //     { id: "idt", header: { text: _("date"), css: "header" }, format: webix.i18n.dateFormatStr },
    //     { id: "form", header: { text: _("form"), css: "header" } },
    //     { id: "serial", header: { text: _("serial"), css: "header" } },
    //     { id: "seq", header: { text: _("seq"), css: "header" } }
    // )
    if (ENT == "tlg") arr.push(
        { id: "rform", header: { text: _("rform"), css: "header" } },
        { id: "rserial", header: { text: _("rserial"), css: "header" } },
        { id: "rseq", header: { text: _("rseq"), css: "header" } },
        { id: "rref", header: { text: _("rref"), css: "header" } },
        { id: "rrdt", header: { text: _("rrdt"), css: "header" }, format: webix.i18n.dateFormatStr },
        { id: "rrea", header: { text: _("rrea"), css: "header" } },
    )
    //if (type == "01GTKT") arr.push({ id: "vat", header: { text: "VAT", css: "header" }, css: "right", format: gridnf.format })
    //if (type == "01GTKT" || type == "02GTTT" ||type == "07KPTQ") 
    if (type == "03XKNB" || type == "04HGDL") {
        arr = arrxk
    }
    
    // if (ENT == 'hdb') {
    //     arr.splice(12, 1) //Xóa cột tiền VND
    //     arr.splice(12, 0,
    //         { id: "vat", header: { text: _("vato"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat },
    //         { id: "total", header: { text: _("totalo"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat }
    //     )
    // } else {
    //     arr.push({ id: "total", header: { text: _("totalv"), css: "header" }, css: "right", format: gridnf.format },
    //         { id: "word", header: { text: _("word"), css: "header" } }
    //     )
    // }
    arr.push({ id: "note", header: { text: _("note"), css: "header" } })
    grid.clearAll()
    grid.config.columns = arr
    grid.refreshColumns()
    if(grid.getValue('c6')=="")
    if(["123"].includes(DEGREE_CONFIG)) {
        if (type == "01GTKT" || type =="01/TVE") {
            if (!grid.isColumnVisible("vatCode")) grid.showColumn("vatCode")
            if (!grid.isColumnVisible("vat")) grid.showColumn("vat")
        }
        else {
            if (grid.isColumnVisible("vatCode")) grid.hideColumn("vatCode")
            if (grid.isColumnVisible("vat")) grid.hideColumn("vat")
        }

        if (type == "01GTKT" || type =="02GTTT") {
            if (!grid.isColumnVisible("invlistdt")) grid.showColumn("invlistdt")
            if (!grid.isColumnVisible("invlistnum")) grid.showColumn("invlistnum")
        }
        else {
            if (grid.isColumnVisible("invlistdt")) grid.hideColumn("invlistdt")
            if (grid.isColumnVisible("invlistnum")) grid.hideColumn("invlistnum")
        }

        if (type == "03XKNB") {
            if (!grid.isColumnVisible("ordno")) grid.showColumn("ordno")
        }
        else {
            if (grid.isColumnVisible("ordno")) grid.hideColumn("ordno")
        }

        if (type == "04HGDL") {
            if (!grid.isColumnVisible("cenumber")) grid.showColumn("cenumber")
            if (!grid.isColumnVisible("cedate")) grid.showColumn("cedate")
        }
        else {
            if (grid.isColumnVisible("cenumber")) grid.hideColumn("cenumber")
            if (grid.isColumnVisible("cedate")) grid.hideColumn("cedate")
        }
    }
    
}


export default class XlsView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        webix.protoUI({ name: "dependent", $cssName: "combo" }, LinkedInputs3, webix.ui.combo)
        webix.protoUI({ name: "dependent2", $cssName: "richselect" }, LinkedInputs0, webix.ui.richselect)

        const viewhd = (inv) => {
            
            try{
                webix.message(_("waiting_processing"),"info", -1, "exp")
                webix.ajax().get(`api/sea/htmn`,{ inv: inv }).then(result => {
                   // const json = result.json(), status = json.status
                   // const req = createRequest(json)
                    //jsreport.renderAsync(req).then(res => {
                    //    b64 = res.toDataURI()
                    let pdf = result.json()
                    let lengthPDF = pdf.sizePDF
                    console.log(lengthPDF)
                    if (lengthPDF >= 3145728) webix.message(String(_("file_pdf_size_limit")).replace("#hunglq#", 3145728 / (1024 * 1024)))
                    console.log('begin view')
                    b64 = pdf.pdf
                    const blob = dataURItoBlob(b64);
                    var temp_url = window.URL.createObjectURL(blob);
                    //create url
                    $$("iwin:ipdf").define("src", temp_url)
                    $$("iwin:win").show()
                    webix.message.hide("exp")
                    // webix.ajax().post("api/seek/report", req).then(res => {
                    //     let pdf = res.json()
                    //     let lengthPDF = pdf.sizePDF
                    //     console.log(lengthPDF)
                    //     if (lengthPDF >= 3145728) webix.message(String(_("file_pdf_size_limit")).replace("#hunglq#", 3145728 / (1024 * 1024)))
                    //     console.log('begin view')
                    //     b64 = pdf.pdf
                    //     const blob = dataURItoBlob(b64);
                    //     var temp_url = window.URL.createObjectURL(blob);
                    //     //create url
                    //     $$("iwin:ipdf").define("src", temp_url)
                    //     $$("iwin:win").show()
                    //     webix.message.hide("exp")
                    // })
                }) 
            }catch (err) {
                webix.message(err.message, "error")
            }
        }

        const form = {
            view: "form",
            id: "xls:form",
            padding: 1,
            margin: 1,
            elementsConfig: { labelWidth: 65 },
            elements: [
                {
                    cols: [
                        { id: "type", name: "type", label: _("invoice_type"), view: "combo", options: ITYPE(this.app), required: true },//gravity: 1.3,
                        { id: "form", name: "form", label: _("form"), view: "dependent", options: [], master: "type", dependentUrl: "api/sea/fbt?id=", relatedView: "serial", relatedAction: "enable", required: true, width: 190, hidden: ["vcm","sgr"].includes(ENT) ? true : false },
                        { id: "serial", name: "serial", label: _("serial"), view: "dependent2", options: [], master: "form", dependentUrl: "api/sea/sbf?id=", disabled: true, required: true, width: 150, hidden: ["vcm","sgr"].includes(ENT) ? true : false,suggest:{fitMaster:false, width:100} },
                        { view: "button", type: "icon", icon: "mdi mdi-download", label: _("file_sample"),disabled: ROLE.includes('PERM_CREATE_INV_EXCEL_FORM') ? false : true, click: () => { window.location.href = `temp/inv_${ENT}.xlsx` }, width: 85 },
                        { id: "xls:btnsave", view: "button", type: "icon", icon: "mdi mdi-content-save", label: _("save"), width: 85, disabled: true },
                        { id: "xls:btnread", view: "button", type: "icon", icon: "mdi mdi-file-upload-outline", label: _("file_read"), width: 85, disabled: ROLE.includes('PERM_CREATE_INV_EXCEL_READ') ? false : true },
                        {
                            id: "xls:file", view: "uploader", multiple: false, autosend: false, link: "xls:filelist", accept: "application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", upload: "api/inv/upload", type: "icon", icon: "mdi mdi-file-find-outline", label: _("file_select"), width: 85, formData: () => {
                                return { type: $$("type").getValue(), form: $$("form").getValue(), serial: $$("serial").getValue() }
                            }
                        },
                        { id: "xls:filelist", view: "list", type: "uploader", autoheight: true, borderless: true }
                    ]
                }
            ],
            rules: {
                type: webix.rules.isNotEmpty,
                form: webix.rules.isNotEmpty,
                serial: webix.rules.isNotEmpty
            }
        }

        const grid = {
            view: "datatable",
            id: "xls:grid",
            select: "row",
            multiselect: false,
            resizeColumn: true,
            onClick:
            {
                "wxi-search": function (e, r) { viewhd(this.getItem(r)) },
            }
                
        }
        let title_page = { view: "label", label:`<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("invoice_from_excel")}</span>`}
        return { paddingX: 2, rows: [title_page,form, grid] }
    }
    ready(v, urls) {
        $$("type").setValue("01GTKT")
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
    init() {
        if (!ROLE.includes('PERM_CREATE_INV_EXCEL')) {
            this.show("/top/home")
            window.location.replace(`/${homepage}`)
            return
          }
        let grid = $$("xls:grid"), uploader = $$("xls:file")
        let objj = []
        this.Iwin = this.ui(Iwin)
        this.Rwin = this.ui(Rwin)
        webix.extend(grid, webix.ProgressBar)
        $$("type").attachEvent("onChange", (n) => { config(grid, n, this.app) })

        $$("xls:btnread").attachEvent("onItemClick", () => {
            if (!$$("xls:form").validate()) return false
            const files = uploader.files, file_id = files.getFirstId()
            if (file_id) {
                if (grid.getHeaderContent("chk1")) grid.getHeaderContent("chk1").uncheck()
                grid.clearAll()
                grid.showProgress()
                $$("xls:btnsave").disable()
                webix.delay(() => {
                    uploader.send(rs => {
                        if (rs.err) {
                            grid.hideProgress()
                            webix.alert({
                                ok:"Ok",
                                text:rs.err
                            })
                           // webix.message(rs.err, "error")
                        }
                        else if (rs.buffer) {
                            grid.hideProgress()
                            webix.alert({
                                title:_("fail"),
                                ok:"Ok",
                                text:_("err_excel"),
                                type:"alert-error"
                            }).then(function(result){
                                const data = rs.buffer.data
                                const blob = new Blob([new Uint8Array(data, 0, data.length)])
                                webix.html.download(blob, "error.xlsx")
                            });
                          //  webix.message("Có lỗi khi đọc file, chi tiết lỗi trong file đính kèm !", "error")
                           
                        }
                        else {
                            try {
                                grid.parse(rs.data).then(() => {
                                    objj = rs.data
                                    console.log(objj)
                                    if(ROLE.includes('PERM_CREATE_INV_EXCEL_SAVE')) $$("xls:btnsave").enable()
                                })
                            } catch (err) {
                              //  webix.message(err.message, "error")
                                webix.alert({
                                    title:_("fail"),
                                    ok:"Ok",
                                    text:err.message,
                                    type:"alert-error"
                                });
                            } finally {
                                grid.hideProgress()
                            }

                        }

                    })
                })
            }
            else webix.message(_("file_required"))
        })
        
        $$("xls:btnsave").attachEvent("onItemClick", () => {
            let vform = $$("form").getValue()
            let vserial = $$("serial").getValue() ? ('-' + $$("serial").getValue()) : ''
            let vlable = _("invoice_save_confirm2").replace('@@', vform + vserial)
            webix.confirm(vlable, "confirm-warning").then(() => {
                let rows = grid.serialize(), arr = []
                for (let row of rows) {
                    if (row && row.chk) {
                        row.type = $$("type").getValue()
                        arr.push(row)
                    } 
                }
                if (arr.length < 1) {

                    webix.message(_("invoice_must_select"))
                    return false
                }
                webix.delay(() => {
                    grid.disable()
                    grid.showProgress()
                    $$("xls:btnsave").disable()
                    webix.ajax().post("api/inv/xls/ins", { invs: arr }).then(result => {
                        let json = result.json()
                        webix.alert({
                            title: _("success"),
                            ok: "Ok",
                            text: `${_("saved")} ${json} ${_("invoice")}`
                        });
                        //webix.message(`${$$("xls:btnsave").config.label} ${json} ${_("invoice")}`, "success", 10000)
                        if (grid.getHeaderContent("chk1")) grid.getHeaderContent("chk1").uncheck()
                        grid.clearAll()
                        grid.hideProgress()
                        grid.enable()
                     
                    }).catch(() => {
                        if (grid.getHeaderContent("chk1")) grid.getHeaderContent("chk1").uncheck()
                        grid.clearAll()
                        grid.hideProgress()
                        grid.enable()
                    })
                })
            })
        })
    }
}
