import { JetView } from "webix-jet";
import { LinkedInputs3,LinkedInputs0, gridnf, gridnf0, PAYMETHOD, ITYPE, ENT, setConfStaFld,ROLE } from "models/util";
import Iwin from "views/inv/iwin"
import Rwin from "views/inv/rwin"

const config = (grid, type, app) => {
    let arr = [
        { id: "vie", header: "", width: 35, template: `<span class='webix_icon wxi-search', title='${_("view")}'></span>` , hidden: (["sgr"].includes(ENT)) ? false : true}, 
        { id: "chk", header: { content: "masterCheckbox", css: "center" }, adjust: "header", css: "center", template: "{common.checkbox()}" },
        { id: "ic", header: { text: _("invoice_code"), css: "header" },adjust: true },
        { id: "form", header: { text: _("form"), css: "header" },adjust: true },
        { id: "serial", header: { text: _("serial"), css: "header" } ,adjust: true},
        { id: "idt", header: { text: _("idt"), css: "header" },adjust: true,format: webix.i18n.dateFormatStr },
        { id: "btax", header: { text: _("taxcode"), css: "header" },adjust: true },
        { id: "bname", header: { text: _("bname"), css: "header" },adjust: true },
        { id: "buyer", header: { text: _("buyer"), css: "header" } ,adjust: true},
        { id: "baddr", header: { text: _("address"), css: "header" } ,adjust: true},
        { id: "btel", header: { text: _("tel"), css: "header" } ,adjust: true},
        { id: "bmail", header: { text: "Mail", css: "header" } ,adjust: true},
        { id: "bacc", header: { text: _("account"), css: "header" },adjust: true },
        { id: "bbank", header: { text: _("bank"), css: "header" },adjust: true },
        { id: "paym", header: { text: _("payment_method"), css: "header" }, collection: PAYMETHOD(app),adjust: true },
        { id: "curr", header: { text: _("currency"), css: "header" } ,adjust: true},
        { id: "exrt", header: { text: _("exchange_rate"), css: "header" }, css: "right", format: gridnf.format,adjust: true },
        { id: "sum", header: { text: _("sumv"), css: "header" }, css: "right", format: gridnf.format,adjust: true }
    ]
    let arrxk = [
        { id: "chk", header: { content: "masterCheckbox", css: "center" }, adjust: "header", css: "center", template: "{common.checkbox()}" },
        { id: "ic", header: { text: _("invoice_code"), css: "header" } },
        { id: "form", header: { text: _("form"), css: "header" } },
        { id: "serial", header: { text: _("serial"), css: "header" } },
        { id: "idt", header: { text: _("idt"), css: "header" }, format: webix.i18n.dateFormatStr },
        { id: "ordno", header: { text: _("ordno"), css: "header" } },
        { id: "orddt", header: { text: _("orddt"), css: "header" } },
        { id: "ordou", header: { text: _("ou"), css: "header" } },
        { id: "ordre", header: { text: _("reason"), css: "header" } },
        { id: "recvr", header: { text: _("receiver"), css: "header" } },
        { id: "trans", header: { text: _("transporter"), css: "header" } },
        { id: "vehic", header: { text: _("vehicle"), css: "header" } },
        { id: "contr", header: { text: _("contract_no"), css: "header" } },
        { id: "whsfr", header: { text: _("export_address"), css: "header" } },
        { id: "whsto", header: { text: _("import_address"), css: "header" } },
        { id: "curr", header: { text: _("currency"), css: "header" } },
        { id: "exrt", header: { text: _("exchange_rate"), css: "header" }, css: "right", format: gridnf.format },
        { id: "sum", header: { text: _("sumv"), css: "header" }, css: "right", format: gridnf.format }        
    ]
    if (ENT == 'hdb') {
        arr.splice(12, 1) //Xóa cột tiền VND
        arr.splice(12, 0,{ id: "sum", header: { text: _("sumo"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat })
    }

    if (ENT != "hdb") arr.push(
        { id: "idt", header: { text: _("date"), css: "header" }, format: webix.i18n.dateFormatStr },
        { id: "form", header: { text: _("form"), css: "header" } },
        { id: "serial", header: { text: _("serial"), css: "header" } },
        { id: "seq", header: { text: _("seq"), css: "header" } }
    )
    if (ENT == "tlg") arr.push(
        { id: "rform", header: { text: _("rform"), css: "header" } },
        { id: "rserial", header: { text: _("rserial"), css: "header" } },
        { id: "rseq", header: { text: _("rseq"), css: "header" } },
        { id: "rref", header: { text: _("rref"), css: "header" } },
        { id: "rrdt", header: { text: _("rrdt"), css: "header" }, format: webix.i18n.dateFormatStr },
        { id: "rrea", header: { text: _("rrea"), css: "header" } },
    )
    if (type == "01GTKT") arr.push({ id: "vat", header: { text: "VAT", css: "header" }, css: "right", format: gridnf.format })
    if (type == "03XKNB") {
        arr = arrxk
    }
    
    // if (ENT == 'hdb') {
    //     arr.splice(12, 1) //Xóa cột tiền VND
    //     arr.splice(12, 0,
    //         { id: "vat", header: { text: _("vato"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat },
    //         { id: "total", header: { text: _("totalo"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.numberFormat }
    //     )
    // } else {
    //     arr.push({ id: "total", header: { text: _("totalv"), css: "header" }, css: "right", format: gridnf.format },
    //         { id: "word", header: { text: _("word"), css: "header" } }
    //     )
    // }
    arr.push({ id: "note", header: { text: _("note"), css: "header" } })
    if(ENT == "sgr" && type == "01GTKT"){
        arr.push({id: "des", header: { text: _("repadj"), css: "header" },adjust: true})
    }
    grid.clearAll()
    grid.config.columns = arr
    grid.refreshColumns()
}


export default class XlsView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        webix.protoUI({ name: "dependent", $cssName: "combo" }, LinkedInputs3, webix.ui.combo)
        webix.protoUI({ name: "dependent2", $cssName: "richselect" }, LinkedInputs0, webix.ui.richselect)

        const viewhd = (inv) => {
            
            try{
                webix.message(_("waiting_processing"),"info", -1, "exp")
                webix.ajax().get(`api/sea/htmn`,{ inv: inv }).then(result => {
                    //const json = result.json(), status = json.status
                    //const req = createRequest(json)
                    //jsreport.renderAsync(req).then(res => {
                    //    b64 = res.toDataURI()
                    let pdf = result.json()
                    let lengthPDF = pdf.sizePDF
                    console.log(lengthPDF)
                    if (lengthPDF >= 3145728) webix.message(String(_("file_pdf_size_limit")).replace("#hunglq#", 3145728 / (1024 * 1024)))
                    console.log('begin view')
                    let b64 = pdf.pdf
                    const blob = dataURItoBlob(b64);
                    var temp_url = window.URL.createObjectURL(blob);
                    //create url
                    $$("iwin:ipdf").define("src", temp_url)
                    $$("iwin:win").show()
                    webix.message.hide("exp")
                    // webix.ajax().post("api/seek/report", req).then(res => {
                    //     let pdf = res.json()
                    //     let lengthPDF = pdf.sizePDF
                    //     console.log(lengthPDF)
                    //     if (lengthPDF >= 3145728) webix.message(String(_("file_pdf_size_limit")).replace("#hunglq#", 3145728 / (1024 * 1024)))
                    //     console.log('begin view')
                    //     let b64 = pdf.pdf
                    //     const blob = dataURItoBlob(b64);
                    //     var temp_url = window.URL.createObjectURL(blob);
                    //     //create url
                    //     $$("iwin:ipdf").define("src", temp_url)
                    //     $$("iwin:win").show()
                    //     webix.message.hide("exp")
                    // })
                }) 
            }catch (err) {
                webix.message(err.message, "error")
            }
        }

        const form = {
            view: "form",
            id: "xls:form",
            padding: 1,
            margin: 1,
            elementsConfig: { labelWidth: 65 },
            elements: [
                {
                    cols: [
                        { id: "type", name: "type", label: _("invoice_type"), view: "combo", options: ITYPE(this.app), disabled:true, required: true },//gravity: 1.3,
                        { id: "form", name: "form", label: _("form"), view: "dependent", options: [], master: "type", dependentUrl: "api/sea/fbt?id=", relatedView: "serial", relatedAction: "enable", required: true, width: 190, hidden: ["vcm","sgr"].includes(ENT) ? true : false },
                        { id: "serial", name: "serial", label: _("serial"), view: "dependent2", options: [], master: "form", dependentUrl: "api/sea/sbf?id=", disabled: true, required: true, width: 150, hidden: ["vcm","sgr"].includes(ENT) ? true : false },
                        { view: "button", type: "icon", icon: "mdi mdi-download", label: _("file_sample"), click: () => { window.location.href = `temp/inv_${ENT}_adj.xlsx` }, width: 85, disabled: ROLE.includes('PERM_CREATE_INV_ADJ_EXCEL_FORM') ? false : true },
                        { id: "xls:btnsave", view: "button", type: "icon", icon: "mdi mdi-content-save", label: _("save"), width: 85, disabled: true },
                        { id: "xls:btnread", view: "button", type: "icon", icon: "mdi mdi-file-upload-outline", label: _("file_read"), width: 85, disabled: ROLE.includes('PERM_CREATE_INV_ADJ_EXCEL_READ') ? false : true},
                        {
                            id: "xls:file", view: "uploader", multiple: false, autosend: false, link: "xls:filelist", accept: "application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", upload: "api/inv/uplosg", type: "icon", icon: "mdi mdi-file-find-outline", label: _("file_select"), width: 85,disabled: ROLE.includes('PERM_CREATE_INV_ADJ_EXCEL') ? false : true, formData: () => {
                                return { type: $$("type").getValue(), form: $$("form").getValue(), serial: $$("serial").getValue() }
                            }
                        },
                        { id: "xls:filelist", view: "list", type: "uploader", autoheight: true, borderless: true }
                    ]
                }
            ],
            rules: {
                type: webix.rules.isNotEmpty,
                form: webix.rules.isNotEmpty,
                serial: webix.rules.isNotEmpty
            }
        }

        const grid = {
            view: "datatable",
            id: "xls:grid",
            select: "row",
            resizeColumn: true,
            onClick:
            {
                "wxi-search": function (e, r) { viewhd(this.getItem(r)) },
            }
        }
        let title_page = { view: "label", label:`<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("invoice_from_excel_adjust")}</span>`}
        return { paddingX: 2, rows: [title_page,form, grid] }
    }
    ready(v, urls) {
        $$("type").setValue("01GTKT")
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
    init() {
        if (!ROLE.includes('PERM_CREATE_INV_ADJ_EXCEL')) {
            this.show("/top/home")
            window.location.replace(`/${homepage}`)
            return
          }
        let grid = $$("xls:grid"), uploader = $$("xls:file")
        this.Iwin = this.ui(Iwin)
        this.Rwin = this.ui(Rwin)

        webix.extend(grid, webix.ProgressBar)
        $$("type").attachEvent("onChange", (n) => { config(grid, n, this.app) })
        $$("xls:btnread").attachEvent("onItemClick", () => {
          
            if (!$$("xls:form").validate()) return false
            const files = uploader.files, file_id = files.getFirstId()
            if (file_id) {
                grid.clearAll()
                grid.showProgress()
                $$("xls:btnsave").disable()
                webix.delay(() => {
                    uploader.send(rs => {
                        if (rs.err) {
                            grid.hideProgress()
                            webix.alert({
                                ok:"Ok",
                                text:rs.err
                            });
                           // webix.message(rs.err, "error")
                        }
                        else if (rs.buffer) {
                            grid.hideProgress()
                            webix.alert({
                                title:_("fail"),
                                ok:"Ok",
                                text:_("err_excel"),
                                type:"alert-error"
                            }).then(function(result){
                                const data = rs.buffer.data
                                const blob = new Blob([new Uint8Array(data, 0, data.length)])
                                webix.html.download(blob, "error.xlsx")
                            });
                          //  webix.message("Có lỗi khi đọc file, chi tiết lỗi trong file đính kèm !", "error")
                           
                        }
                        else {
                            try {
                                grid.parse(rs.data).then(() => {
                                    if(ROLE.includes('PERM_CREATE_INV_ADJ_EXCEL_SAVE')) $$("xls:btnsave").enable()
                                })
                            } catch (err) {
                              //  webix.message(err.message, "error")
                                webix.alert({
                                    title:_("fail"),
                                    ok:"Ok",
                                    text:err.message,
                                    type:"alert-error"
                                });
                            } finally {
                                grid.hideProgress()
                            }

                        }

                    })
                })
            }
            else webix.message(_("file_required"))
        })
        
        $$("xls:btnsave").attachEvent("onItemClick", () => {
            let vform = $$("form").getValue()
            let vserial = $$("serial").getValue() ? ('-' + $$("serial").getValue()) : ''
            let vlable = _("invoice_save_confirm2").replace('@@', vform + vserial)
            webix.confirm(vlable, "confirm-warning").then(() => {
                let rows = grid.serialize(), arr = []
                for (let row of rows) {
                    if (row && row.chk) arr.push(row)
                }
                if (arr.length < 1) {

                    webix.message(_("invoice_must_select"))
                    return false
                }
                webix.delay(() => {
                    grid.disable()
                    grid.showProgress()
                    $$("xls:btnsave").disable()
                    webix.ajax().post("api/inv/xls/ins", { invs: arr }).then(result => {
                        let json = result.json()
                        webix.alert({
                            title: _("success"),
                            ok: "Ok",
                            text: `${_("saved")} ${json} ${_("invoice")}`
                        });
                        //webix.message(`${$$("xls:btnsave").config.label} ${json} ${_("invoice")}`, "success", 10000)
                        grid.hideProgress()
                        grid.enable()
                    }).catch(() => {
                        grid.hideProgress()
                        grid.enable()
                    })
                })
            })
        })
    }
}
