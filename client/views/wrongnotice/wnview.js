import { JetView } from "webix-jet"
export default class VIEWWN extends JetView {
    config() {
        return {
            view: "window",
            id: "viewwn:win",
            modal: true,
            fullscreen: true,
            head: {
                view: "toolbar"
                , height: 35
                , cols: [
                    //{ view: "label", label: _("invoice_view") },
                    {},
                    {
                        cols: [
                            { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => $$("viewwn:win").hide() }
                        ], fillspace: true, align: "right"
                    }
                ]
            },
            body: { id: "viewwn:ipdf", view: "iframe", borderless: true }
        }
    }
    show() {
        this.getRoot().show()
    }
    init() {
        //$$("iwin:ipdf").attachEvent("onAfterLoad", function () { URL.revokeObjectURL(this.src) })
    }
}
