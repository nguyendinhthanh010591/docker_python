import { JetView } from "webix-jet"
import { LinkedInputs2,t2s,ROLE, size, WN_CRE_WIN_BTNSAV_PURPOSE, TYPE_REF_COMBO, NOTI_TYPE_COMBO, NOTI_TAXTYPE_OPTION,WRONGNOTICE_CQT_STATUS,RPP,ITYPE } from "models/util"
import Wncreatewin from "views/wrongnotice/wncreatewin"
import VIEWWN from "views/wrongnotice/wnview"
const format = require("xml-formatter")
let statusSelected = ""
const WNG_OPTIONS = [
    {id:"wait_approved", value:_("wait_approved")},
    {id:"approved", value:_("approved")},
]
const SENT_STATUS = [
    {id:"SENT", value:_("sent")},
    {id:"WAIT", value:_("pending")},
    {id:"TAX_ACCEPT", value:_("cqt_accepted")},
    {id:"TAX_DENY", value:_("cqt_not_accepted")},
]
let b64 ,row
const viewhd = (id) => {
    const grid = $$("wngroupsearch:apprgrid")
    grid.showProgress()
    // webix.delay(() => {
    //     webix.ajax(`api/wno/htm/${id}`).then(result => {
    //         const json = result.json()
    //         const req = createRequest(json)
    //         jsreport.renderAsync(req).then(res => {
    //             b64 = res.toDataURI()
    //             $$("iwin:ipdf").define("src", res.toObjectURL())
    //             $$("iwin:btnmail").hide()
    //             $$("iwin:xmail").hide()
    //             $$("iwin:email").hide()
    //             $$("iwin:win").show()
    //         }).finally(() => grid.hideProgress())
    //     })
    // })
    webix.delay(() => {
        webix.ajax(`api/wno/htm/${id}`).then(result => {
            let pdf = result.json()
            let lengthPDF = pdf.sizePDF
            console.log(lengthPDF)
            if (lengthPDF >= 3145728) webix.message(String(_("file_pdf_size_limit")).replace("#hunglq#", 3145728 / (1024 * 1024)))
            console.log('begin view')
            b64 = pdf.pdf
            const blob = dataURItoBlob(b64);

            var temp_url = window.URL.createObjectURL(blob);

            $$("viewwn:ipdf").define("src", temp_url)
            $$("viewwn:win").show()
            webix.message.hide("exp");
            grid.hideProgress()
        }) 
    })
}
const configs = (type) => {
    webix.ajax().get(`api/sea/afbt?id=${type}`).then(r => {
        let datasr = r.json()
        let removeDuplicates = (array, key) => {
            let lookup = new Set();
            return array.filter(obj => obj[key] && !lookup.has(obj[key]) && lookup.add(obj[key]));
        }
        let arr = [{ id: "*", value: _("all") }, ...datasr]

        $$("form").getList().clearAll()

        $$("form").getList().parse(removeDuplicates(arr, 'id'))
        $$("form").setValue("*")

        //Bo o ham ready vi no khong dung, dat vao day moi dung
        //Lay o session ra va set cho toan bo form truoc, ky hieu set tai event onChange cua form
        let state = webix.storage.session.get("wngroupsearch:form")
        if (state) {
            let form = $$("wngroupsearch:form")
            form.setValues(state)
        }
    })
}
export default class WngroupView extends JetView {
    config() {
       // _ = this.app.getService("locale")._
        webix.protoUI({ name: "dependent2", $cssName: "richselect" }, LinkedInputs2, webix.ui.richselect)
        webix.proxy.wngroupsearch = {
            $proxy: true,
            load: function (view, params) {
                let obj = $$("wngroupsearch:form").getValues()
                Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                if (!params) params = {}
                params.filter = obj
                return webix.ajax(this.source, params)
            }
        }
        let pager = { view: "pager", id: "wngroupsearch:pager", size: 10, template: "{common.first()}{common.prev()}{common.pages()}{common.next()}{common.last()}" }

        let wncreatewinApprovedGrid = {
            view: "datatable",
            id: "wngroupsearch:apprgrid",
            select: "row",
            multiselect: false,
          
            resizeColumn: true,
            onContext: {
                "webix_view": function (e, id) {
                    if (id && id.row) this.select(id)
                }
            },
            tooltip: true,
            select: "row",
            pager: "wngroupsearch:pager",
            headerRowHeight: 33,
            columns: [
                // {  header: { text: ROLE.includes('PERM_SEARCH_BTH_EXCEL') ?"<span class='webix_icon wxi-download' title='excel'></span>":'', css: "header" }, width: 35, template:ROLE.includes('PERM_SEARCH_BTH_VIEW') ? `<span class='webix_icon wxi-search', title='${_("view")}'></span>`:'' },
                { header: { text: "", css: "header" }, width: 35, template: ROLE.includes('PERM_CREATE_WRONGNOTICE_VIEW') ?`<span class='webix_icon wxi-search' , title='${_("view")}'></span>` : ''},
                { id: "row_id", header: { text: "ID", css: "header" }, adjust: true, editor: "text" },
                { id: "stax", header: { text: _("wn_stax"), css: "header" }, css: "right", adjust: true },
                { id: "place", header: { text: _("place"), css: "header" }, adjust: true },
                { id: "sname", header: { text: _("wn_sname"), css: "header" }, adjust: true },
                { id: "noti_dt", header: { text: _("noti_dt"), css: "header" }, adjust: true, format: webix.i18n.dateFormatStr },
                { id: "noti_taxdt", header: { text: _("notice_doc_date"), css: "header" }, adjust: true, format: webix.i18n.dateFormatStr },
                // Th�m 3 c?t sau c?t ng�y g?i CQT
				{ id: "form", header: { text: _("form"), css: "header"}, adjust: true },
                { id: "serial", header: {text: _("serial"), css: "header"}, adjust: true },
                { id: "seq", header: {text: _("seq"), css: "header"}, exportType: "string", adjust: true },
                { id: "noti_taxid", header: { text: _("noti_taxid"), css: "header" }, adjust: true },
                { id: "noti_taxnum", header: { text: _("notice_doc_number"), css: "header" }, adjust: true },
                { id: "noti_taxname", header: { text: _("noti_taxname"), css: "header" }, adjust: true, minWidth: 220 },
                { id: "noti_taxtype", header: { text: _("noti_taxtype_type"), css: "header" }, adjust: true, options: NOTI_TAXTYPE_OPTION(this.app), minWidth: 220 },
                { id: "status", header: { text: _("status"), css: "header" }, adjust: true, minWidth: 220, options: SENT_STATUS },
                { id: "status_cqt", header: { text: _("status_cqt"), css: "header" },collection: WRONGNOTICE_CQT_STATUS(this.app), adjust: true, minWidth: 220 },
                { id: "budget_relationid", header: { text: _("budget_relationid_short"), css: "header" }, adjust: true, fillspace:true, minWidth: 250 },
                { id: "dien_giai", header: { text: _("feedback_cqt"), css: "header" }, adjust: true, fillspace:true, minWidth: 250 }
            ],
            onClick:{
                "wxi-download": function (e,r) {
                    const form = $$("wngroupsearch:form")
                    if (!form.validate()) return
                    webix.message(_("loading"))
                    let params = {}
                    let obj = form.getValues()
                    Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                    params = {filter: obj}
                    webix.ajax("api/wno/excel", params).then(result => {                 
                        const json = result.json()
                        json.data = json.data.map(el => {
                            let arr3 = [
                                { id: "0", value:  _("cqt_wait_send") },
                                { id: "1", value:  _("cqt_sent") },
                                { id: "2", value:  _("cqt_sent_fail") },
                                { id: "15", value:  _("wno_valid") },
                                { id: "16", value:  _("wno_invalid") },
                                { id: "17", value:  _("wno_processed") }
                            ]
                            let arr_notitype = [
                                { id: "1", value: _("taxpayer_notice_cancel_explan") }, 
                                { id: "2", value: _("taxpayer_notice_cancel_explan_CQT") }
                            ]
                            el.status = SENT_STATUS.find(e => e.id == el.status).value
                            el.status_cqt = arr3.find(e => e.id == el.status_cqt).value
                            el.noti_taxtype = arr_notitype.find(e => e.id == JSON.parse(el.doc).noti_taxtype ).value

                            return el
                        })
                        let data = new webix.DataCollection({ data: json.data }), columns = []
                        $$("wngroupsearch:apprgrid").config.columns.forEach((e,i) => {
                            if(i>1){
                                let obj = {}
                                obj.id = e.id
                                obj.header = e.header
                                obj.exportType = "string"
                                columns.push(obj)
                                console.log(e)
                                return e
                            }else return null
                        })
                        webix.toExcel(data, { columns: columns, filename: "DSTraCuuTBSS.xlsx" }).then(() => { 
                            console.log("dakjsfnaisfn");
                            data.destructor()
                        }).catch(err=>console.log(err))
                    })
                },
                "wxi-search": function (e, r) {
                    this.select(r)
                    console.log("Row ", r, "--- ID", this.getItem(r))
                    let row = this.getItem(r)
                    viewhd(row.row_id)
                    // $$("wncreatewin").show()
                    // $$("wincreate:win:form").setValues(this.getItem(r).doc)
                    // $$("wincreate:win:grid").parse(this.getItem(r).doc.items)
                    // $$("wincreate:win:btnsav2sendn").hide()
                    // $$("wincreate:win:btnsav2winv").hide()
                    // $$("noti_dt").disable()
                    // $$("place").disable()
                    // $$("budget_relationid").disable()
                    // $$("wincreate:win:form:noti_taxdt").disable()
                    // $$("wincreate:win:form:noti_taxnum").disable()
                    // $$("noti_taxtype").disable()
                                    
                    // $$("wincreate:win:form").refresh();
                },
            }
        }
        let recpager = { view: "combo", id: "wngroupsearch:recpager", maxWidth: 70, tooltip: _("row_per_page"), value: 10, options: RPP }
        let form = {
            view: "form",
            id: "wngroupsearch:form",
            padding: 3,
            margin: 3,
            elementsConfig: { labelWidth: 120 },
            elements:
                [
                    {
                        cols: [
                            { id: "wngroupsearch:form:fd", name: "fd", label: _("fd"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr },
                            { id: "wngroupsearch:form:td", name: "td", label: _("td"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr },
                            { id: "wngroupsearch:statusCQT", name: "status_cqt", label: _("status_tbss"), view: "combo", options: WRONGNOTICE_CQT_STATUS(this.app), value: "*"},
                            { id: "wngroupsearch:ou", name: "ou", label: _("bname"), view: "combo", suggest: { url: "api/ous/bytokenou"} },
                            // { view: "button", id: "wngroupsearch:btncreate", type: "icon", icon: "mdi mdi-text-box-plus-outline", label: _("wrong_notice_create"), width: 150 },
                            
                        ]
                    },
                    {
                        cols: [
                            { id: "wngroupsearch:type", name: "type", label: _("invoice_type"), view: "combo", placeholder: _("choose_inv_type"), suggest: { data: ITYPE(this.app), fitMaster: false, width: 250 } },//richselect
                            { id: "form", name: "form", label: _("form"), view: "combo", options: [] },
                            { id: "wngroupsearch:serial", name: "serial", label: _("serial"), view: "combo", options: [], disabled: false },
                            {
                                cols:[
                                    { name: "seq", label: _("seq"), view: "text", attributes: { maxlength: 8 }, validate: v => { return !v || /^[0-9]{1,8}$/.test(v) }, invalidMessage: _("seq_invalid"), placeholder: _("seq") },
                                    { view: "button", id: "wngroupsearch:btnsearch", type: "icon", icon: "wxi-search", label: _("search"), width: 90,disabled: ROLE.includes('PERM_CREATE_WRONGNOTICE') ? false : true }
                                ]
                            }
                        ]
                    }
                ]
            , rules: {
                fd: webix.rules.isNotEmpty,
                td: webix.rules.isNotEmpty,
                $obj: function (obj) {
                    if (obj.fd > obj.td) {
                        webix.message(_("date_invalid_msg"), "error")
                        return false
                    }
                    return true
                }

            }
        }
        let title_page = {
            view: "label", label: `<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("wrongnotice_search")}</span>`
        }
        return { paddingX: 2, rows: [title_page,form, wncreatewinApprovedGrid, 
            //grid,
            {
                cols: [
                    recpager, pager,{ id: "count_tb", view: "label", align:"left", fillspace:true }, {
                        cols: [
                            { view: "button", id: "wngroupsearch:btnhistory", type: "icon", popup: "hscqt", icon: "mdi mdi-email-newsletter", label: _("history_cqt"), disabled: true,width: 130, },
                            { view: "button", id: "wngroupsearch:btnsendback", type: "icon", icon: "mdi mdi-email-plus-outline", label: _("sendback_cqt"), disabled: true, width: 100, },

                        ]
                    },
                ]
            }] }
    }
    init() {
        const hscqt = $$("wngroupsearch:hscqt")
        this.Wncreatewin = this.ui(Wncreatewin)
        this.VIEWWN = this.ui(VIEWWN)
        let date = new Date(), firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1),
         apprgrid = $$("wngroupsearch:apprgrid"), form = $$("wngroupsearch:form"), recpager = $$("wngroupsearch:recpager")
        const all = { id: "*", value: _("all") }
        $$("wngroupsearch:form:fd").setValue(firstDay)
        $$("wngroupsearch:form:td").setValue(date)
        $$("wngroupsearch:statusCQT").getList().add(all, 0)
        $$("wngroupsearch:statusCQT").setValue("*")
        $$("wngroupsearch:ou").getList().add(all, 0)
        $$("wngroupsearch:ou").setValue("*")
        $$("wngroupsearch:type").getList().add(all, 0)
        $$("wngroupsearch:type").attachEvent("onChange", (n) => {
            configs(n)
        })
        $$("wngroupsearch:type").setValue("01GTKT")
        $$("form").attachEvent("onChange", (n, ov) => {
            if (n == ov) return
            this.webix.ajax().get(`api/sea/asbf?id=${n}`).then(r => {
                let datasr = r.json()
                let removeDuplicates = (array, key) => {
                    let lookup = new Set();
                    return array.filter(obj => obj[key] && !lookup.has(obj[key]) && lookup.add(obj[key]));
                }
                $$("wngroupsearch:serial").getList().clearAll()
                $$("wngroupsearch:serial").setValue(null)

                $$("wngroupsearch:serial").getList().parse(removeDuplicates(datasr, 'id'))
                $$("wngroupsearch:serial").getList().add(all, 0)
                $$("wngroupsearch:serial").setValue("*")
                // $$("mst").getList().parse(removeDuplicates(data,'mst'))
                //Den doan nay moi load ky hieu trong session ra
                let state = webix.storage.session.get("wngroupsearch:form")
                if (state && state.serial && datasr.length > 0) {
                    $$("wngroupsearch:serial").setValue(state.serial)
                    grid.clearAll()
                    grid.loadNext(size, 0, null, "wngroupsearch->api/wno/lwngroup", true)
                    webix.storage.session.remove("wngroupsearch:form")
                }
            })
        })
        //$$("wngroupsearch:statusCQT").hide()
        webix.extend(apprgrid, webix.ProgressBar)
        const search = () => {
            if (form.validate()) {
                apprgrid.clearAll()
                apprgrid.loadNext($$("wngroupsearch:recpager").getValue(), 0, null, "wngroupsearch->api/wno/lwngroup", true)
            }
        }
        $$("wngroupsearch:btnsearch").attachEvent("onItemClick", () => {
            search()
        })
        // $$("wngroupsearch:btncreate").attachEvent("onItemClick", () => {
        //     let items = apprgrid.serialize(), arr = []
        //     for (let item of items) {
        //         if (item && item.chk) {
        //             item.isgroup = true
        //             arr.push(item)
        //         }
        //     }
        //     if (arr.length < 1) {
        //         webix.message(_("must_select"), "error")
        //     } else {
        //         this.Wncreatewin = this.ui(Wncreatewin)
        //         this.Wncreatewin.show({
        //             updategrid: true,
        //             purpose: WN_CRE_WIN_BTNSAV_PURPOSE.SEND_NOW,
        //             form: JSON.parse('{"action":"GOM"}'),
        //             grid: arr
        //         }, search)
        //     }
        // })
        recpager.attachEvent("onChange", () => {
            $$("wngroupsearch:pager").config.size = recpager.getValue()
            if (form.validate()) search()
        })
        apprgrid.attachEvent("onBeforeLoad", function () {
            this.showOverlay(_("loading"))
        })
        apprgrid.attachEvent("onAfterLoad", function () {
            let self = this, count = self.count()
            $$("count_tb").setValue(`${_("found")} ${count}`)
            // if (count) {
            //     self.hideOverlay()
            // }
            // else {
            //     self.showOverlay(_("notfound"))
            // }
             if (!this.count()) this.showOverlay(_("notfound"))
             else this.hideOverlay()
        })
        apprgrid.attachEvent("onAfterSelect", () => {
            row = apprgrid.getSelectedItem()
            if(ROLE.includes('PERM_CREATE_WRONGNOTICE_SEND_CQT')) {
              $$("wngroupsearch:btnhistory").enable()
              $$("wngroupsearch:btnsendback").enable()
            }
        })

        apprgrid.attachEvent("onAfterUnSelect", () => {
            row = null
            $$("wngroupsearch:btnhistory").disable()
            $$("wngroupsearch:btnsendback").disable()
        })
        $$("wngroupsearch:btnhistory").attachEvent("onItemClick", () => {
            hscqt.clearAll()
            $$("wngroupsearch:hscqt").showOverlay(_("loading"))
            webix.ajax(`api/wno/hscqt/${row.row_id}`).then(result => {
                let json = result.json()
                if (json.length) {
                    $$("wngroupsearch:hscqt").hideOverlay()
                    hscqt.parse(json)
                }
                else {
                    $$("wngroupsearch:hscqt").showOverlay(_("notfound"))
                }
            })
        })

        $$("wngroupsearch:btnsendback").attachEvent("onItemClick", () => {
            webix.ajax(`api/wno/sendback/${row.row_id}`).then(result => {
                let json = result.json()
                console.log("json:",json.result);
                if (json.result=1){
                    webix.message(_("sent"), "success")
                }else {
                    webix.message(_("cqt_sent_fail"), "error")
                }

            })
        })
    }
}
webix.ui({

    view: "popup",
    id: "viewXml",
    width: 900,
    height: 700,
    margin: 50,
    body: { 
        view:"textarea",id:"wngroupsearch:viewXml", 
        // value:text,
        height:900 
    }
});

webix.ui({

    view: "popup",
    id: "hscqt",
    width: 950,
    height: 400,
    margin: 50,

    body: {
        view: "datatable",
        id: "wngroupsearch:hscqt",
        // select: "row",
        columns: [
            // { id: "id_ref", header: { text: _("id_ref"), css: "header" } },
            { id: "datetime_trans", header: { type: "datetime", text: _("time"), css: "header" }, adjust: true },
            // { id: "status", header: { text: _("status_cqt"), css: "header" }, collection: WRONGNOTICE_CQT_STATUS, adjust: true },
            { id: "description", header: { text: _("description"), css: "header" }, fillspace: true },
            { header: { text:  _("Message_gdt"), css: "header" }, adjust: true, popup: "viewXml", 
                template: obj => {  
                    let _result = "";
                    let r_xml = obj.r_xml;
                    if ( r_xml != null) {
                        _result += `<span class='webix_icon wxi-eye', title='${_("view")}'></span>` ;
                    } 
                    return _result;
                }
            }
            // { id: "", header: { text: "", css: "header" }, fillspace: true },
        ],
        onClick:
        {
            "wxi-eye": function (e, r) {
                webix.ajax(`api/wno/viewXml/${r.row}`).then(result => {
                    let json = result.json()
                    if (json.length) {
                        var xmlPretty = ''
                        if(json[0].r_xml != null){
                            xmlPretty = format(json[0].r_xml, {
                                indentation: '\t', 
                                filter: (node) => node.type !== 'Comment', 
                                collapseContent: true, 
                                lineSeparator: '\n'
                            });
                        }
                        $$("wngroupsearch:viewXml").setValue(xmlPretty)
                        var left = (screen.width/2)-(1000/2)
                        var top = (screen.height/2)-(800/2)
                        $$("wngroupsearch:viewXml").show({
                            x:left, // left offset from the right side
                            y:top // top offset
                        });
                    }
                })
            }
        }
    }
})