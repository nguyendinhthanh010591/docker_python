import { JetView } from "webix-jet"
import { ORG_TAXO, ORG_TAXONAME, WN_CRE_WIN_BTNSAV_PURPOSE, ORG_ON, TAXC, NOTI_TYPE_COMBO, TYPE_REF_COMBO, PLACE, NOTI_TAXTYPE_OPTION, SIGN_TYPE/*,callCloudTvanApi*/ } from "models/util"
const NOTI_TYPE_COMBO1 = [
    { id: "1", value: "Hủy", valueclient: "n_t_c_cancel" },
    //{ id: "2", value: "Điều chỉnh", valueclient: "n_t_c_adj" },
    { id: "4", value: "Giải trình", valueclient: "n_t_c_explan" },
    { id: "5", value: "Sai sót do tổng hợp", valueclient: "n_t_c_mistake" }
]
export default class WNCREATEWIN extends JetView {
    config() {
        let wncreatewinForm = {
            view: "scrollview",
            body: {
                id: "wincreate:win:form",
                view: "form",
                padding: 0,
                margin: 0,
                elementsConfig: { labelWidth: 160 },
                elements: [
                    {
                        cols: [
                            {
                                rows: [
                                    { name: "action",id: "wincreate:win:form:action", label: _("type"), view: "text", attributes: { maxlength: 100 }, required: true, disabled: true ,hidden: true},
                                    {
                                        
                                        name: "noti_taxtype", label: _("noti_taxtype_type"),class: "wincreate:view", view: "richselect", options: NOTI_TAXTYPE_OPTION(this.app), required: true, value: 1, on: {
                                            onChange: (newValue, oldValue, config) => {
                                                let _wwfnn = $$("wincreate:win:form:noti_taxnum"), _wwfnd = $$("wincreate:win:form:noti_taxdt")
                                                if (newValue == 2) {
                                                    _wwfnn.define({label: _("noti_taxnum"), required: true})
                                                    _wwfnd.define({label:  _("noti_taxdt"), required: true})
                                                }
                                                else {
                                                    _wwfnn.define({label: _("adj.ref_tip"), required: false})
                                                    _wwfnd.define({label:  _("adj.rdt_tip"), required: false})
                                                }
                                                _wwfnn.refresh()
                                                _wwfnd.refresh()
                                            }
                                        }
                                    },
                                    { id: "wincreate:win:form:noti_taxnum", name: "noti_taxnum", label: _("adj.ref_tip"), view: "text", attributes: { maxlength: 30 } },
                                    { id: "wincreate:win:form:noti_taxdt", name: "noti_taxdt", label: _("adj.rdt_tip"), view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr  },
                                    { name: "noti_taxid", label: _("noti_taxid"), view: "text", attributes: { maxlength: 5 }, value: ORG_TAXO, required: true, disabled: true },
                                    { name: "noti_taxname", label: _("noti_taxname"), view: "text", attributes: { maxlength: 100 }, value: ORG_TAXONAME, required: true, disabled: true },
                                ]
                            },
                            {
                                rows: [
                                    { name: "sname", label: _("wn_sname"), view: "text", attributes: { maxlength: 400 }, required: true, value: ORG_ON, disabled: true },
                                    { name: "stax", label: _("wn_stax"), view: "text", attributes: { maxlength: 14 }, required: true, value: TAXC, disabled: true },
                                    { name: "budget_relationid",id:"budget_relationid", label: _("budget_relationid"), view: "text", attributes: { maxlength: 7 } },
                                    { name: "place",id:"place", label: _("place"),value: PLACE, view: "text", attributes: { maxlength: 50 }, required: true },
                                    { name: "noti_dt", id:"noti_dt",label: _("noti_dt"), view: "datepicker", stringResult: true, format: webix.i18n.dateFormatStr, required: true, value: new Date(),disabled: true },
                                ]
                            },
                        ]
                    }

                ]
            }
        }

        let wncreatewinGrid = {
            view: "datatable",
            id: "wincreate:win:grid",
            editable: true,
            navigation: true,
            editaction: "click",
            resizeColumn: true,
            multiselect: false,
            select: "cell",
            headerRowHeight: 33,
            tooltip:true,
            columns: [
                { id: "line", header: { text: _("stt"), css: "header" }, css: "right", adjust: true },
                { id: "row_id", header: { text: _("stt"), css: "header" }, css: "right", adjust: true ,hidden:true},//id cua tbss can gom
                { id: "form", header: { text: _("form"), css: "header" }, adjust: true },
                { id: "serial", header: { text: _("serial"), css: "header" }, adjust: true },
                { id: "seq", header: { text: _("seq"), css: "header" }, adjust: true/*, editor: "text" */ },
                { id: "idt", header: { text: _("idt"), css: "header" }, adjust: true, format: webix.i18n.dateFormatStr },
                { id: "type_ref", header: { text: _("type_ref"), css: "header" }, adjust: true, value: 1, collection: TYPE_REF_COMBO(this.app), maxWidth: 250},
                { id: "noti_type", header: { text: _("noti_type"), css: "header" }, adjust: true, collection: [], editor: "combo" },
                { id: "rea", header: { text: _("rea"), css: "header" }, adjust: true, editor: "text", fillspace: true },
            ],
            rules: {
                form: webix.rules.isNotEmpty,
                serial: webix.rules.isNotEmpty,
                idt: webix.rules.isNotEmpty,
                type_ref: val => val && !isNaN(val),
                noti_type: val => val && !isNaN(val)
            },
            on: {
                onStoreUpdated: (id, obj, mode) => {
                    this.data.each((row, i) => {
                        row.line = i + 1
                    })
                },
                onAfterEditStart: function (cell) {
                    let _cellName = cell.column, _maxLength
                    switch (_cellName) {
                        case "form":
                            _maxLength = 11
                            break;
                        case "serial":
                            _maxLength = 8
                            break;
                        case "seq":
                            _maxLength = 8
                            break;
                        case "type_ref":
                            _maxLength = 1
                            break;
                        case "noti_type":
                            _maxLength = 1
                            break;
                        case "rea":
                            _maxLength = 255
                            break;
                        default:
                            break;
                    }
                    if (_maxLength) this.getEditor().getInputNode().setAttribute("maxlength", _maxLength)
                }
            }
        }

        let wnbuttonarea = {
            cols: [
                {},
                { view: "button", id: "wincreate:win:btnsav2sendn", type: "icon", icon: "mdi mdi-content-save-move", label: _("cancel_approve"), width: 125 },
                // { view: "button", id: "wincreate:win:btnsav2group", type: "icon", icon: "mdi mdi-content-save-settings", label: _("save"), width: 85 },
                { view: "button", id: "wincreate:win:btnsav2winv", type: "icon", icon: "mdi mdi-content-save", label: _("save"), width: 105 },
            ]
        }

        return {
            view: "window",
            id: "wncreatewin",
            // modal: true,
            // width: 640,
            // height: 380,
            // position: "center",
            // resize: true,
            // move:true,
            fullscreen: true,
            head: {
                view: "toolbar"
                , height: 35
                , cols: [
                    { view: "label", label: _("wrong_notice") },
                    { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => this.close() }
                ]
            },
            body: {
                rows: [
                    wncreatewinForm,
                    wncreatewinGrid,
                    wnbuttonarea,
                ]
            }
        }
    }
    initBtnsav2sendnListener(callback) {
        const sendnProcess = (_data, arr) => {
            if (_data) {
                webix.ajax().post("api/wno/now", _data).then(result => {
                    const obj = result.json()
                    if (obj.affectedRows) {
                        //callCloudTvanApi(arr, '04/SS-HĐĐT')
                        webix.message(_("sent"), "success")
                        this.cleanInput()
                        this.hide()
                        callback()
                    }
                })
            }
        }
        $$("wincreate:win:btnsav2sendn").detachEvent("onItemClick")
        $$("wincreate:win:btnsav2sendn").attachEvent("onItemClick", () => {
            if (this.validateAll()) {
                    // GET XML
                    webix.confirm(_("wrong_notice_confirm"), "confirm-warning").then(() => {
                    
                        let _data = this.createData(), arr = []
                            
                        webix.ajax().post("api/wno/xml", _data).then(result => {
                            const obj = result.json()
                            if (obj.affectedRows) {
                                webix.message(_("XML"), "success")
                            }
                            let arr_xml = obj.arr
                            for (let i_arr_xml = 0; i_arr_xml < arr_xml.length; i_arr_xml++) {
                                let row_id = arr_xml[i_arr_xml].inc
                                if (row_id) { arr.push(row_id) }
                            }
                            // APPROVE USB
                            if (obj.SIGN_TYPE == 1) {
                                SELECT_CKS_PROMPT(obj.mst).then(values => {
                                    webix.message(_("handling"), "", 3000)
                                    let row = obj.arr
                                    //row.serial = values.serial
                                    webix.ajax().post(`${USB}FptEsign/xmls/${values.serial}`, JSON.stringify(row)).then(result => {
                                        const rows = result.json()
                                        const rows2 = {incs: row.id, signs: [{incs:row.id,xml:rows.xml}]} // status=1 va xml signed
                                        if (obj.arr.length > 0) {
                                            obj.arr.forEach(eObj => {
                                                let fitem = rows.find(e => e.id == eObj.id)
                                                if (fitem) {
                                                    eObj.xml = fitem.xml
                                                }
                                            })
                                        }
                                        sendnProcess(obj, arr)
                                        webix.message(`${_("approved")} ${_("success")}`, "success")
                                    }).catch(e => {
                                        webix.message(_("approve_failed"), "error")
                                        webix.alert({
                                            title: _("cts_info"),
                                            ok: _("done"),
                                            text: _("clicked") +
                                                ` <a style="color:blue !important;" href="https://drive.google.com/drive/folders/1rhRJlJImcKR-s3N1bFNEbmc4YRio2kkW" target="_blank"><span>${_("here")}</span></a> ` + _("to_down_newest")
                                        })
                                    })
                                }).finally(() => {
                                    $$("wincreate:win:grid").hideProgress()
                                })
                            }
                            // APPROVE USB
                            else {
                               sendnProcess(obj, arr)
                            }
                        })
                    })
           } else {
                webix.message(_("pls_check_wrongnotice_data"), "error")
                return null
            }
        })
    }
    init() {
        this.initBtnsav2sendnListener()
        $$("wincreate:win:btnsav2winv").attachEvent("onItemClick", () => {
            //webix.message("Save thông tin để gửi cùng INV")
            if (this.validateAll()) this.hide()
        })
    }
    cleanInput() {
        let _form = $$("wincreate:win:form"), _grid = $$("wincreate:win:grid")
        _form.clearValidation()
        _form.clear()
        _grid.clearAll()
        _form.setValues({ noti_taxid: ORG_TAXO, noti_taxname: ORG_TAXONAME, stax: TAXC, sname: ORG_ON, noti_taxtype: "1", noti_dt: new Date(),noti_taxdt:null })
    }
    show(data, callback) {
        let wwg = $$("wincreate:win:grid"), wwbsn = $$("wincreate:win:btnsav2sendn"), wwbwi = $$("wincreate:win:btnsav2winv"), _form = $$("wincreate:win:form")
        this.$$("place").setValue(PLACE)
        if (data) {
            let _updategrid = data.updategrid
            if (data.purpose == WN_CRE_WIN_BTNSAV_PURPOSE.SEND_NOW) {
                wwbsn.show()
                wwbwi.hide()
            } else {
                wwbsn.hide()
                wwbwi.show()
            }
            //if (data.form) $$("wincreate:win:form").setValues(data.form)
            if (_updategrid) wwg.clearAll()
            if(data.form && data.form.action) $$("wincreate:win:form:action").setValue(data.form.action)
            if (data.grid && wwg.serialize().length == 0) {
                if (!Array.isArray(data.grid)) data.grid = [data.grid]
                data.grid.forEach((e, index) => {
                    e.idt = new Date(e.idt)
                    e.line = index+1
                    if(e.form && e.form.length>1) e.type_ref=3
                    e.type_ref =  e.type_ref || 1
                    if(data.form && data.form.action && data.form.action=='DIEUCHINH'){
                        e.noti_type = 4
                        var collection = wwg.getColumnConfig("noti_type").collection;
                        collection.parse(NOTI_TYPE_COMBO1)
                    }
                    else {
                        e.noti_type = e.noti_type || 1
                        var collection = wwg.getColumnConfig("noti_type").collection;
                        collection.parse({ id: "1", value: "Hủy", valueclient: "n_t_c_cancel" })
                        collection.parse({ id: "2", value: "Điều chỉnh", valueclient: "n_t_c_adj" })
                        collection.parse({ id: "3", value: "Thay thế", valueclient: "n_t_c_rep" })
                    }
                    if (data.tt32) e.type_ref = 3
                })
                $$("wincreate:win:form:noti_taxdt").getPopup().getBody().define("maxDate", new Date())
                $$("wincreate:win:grid").parse(data.grid)
            }
        }
        if (callback) this.initBtnsav2sendnListener(callback)
        const win = this.getRoot()
        win.show()
    }
    hide() {
        const win = this.getRoot()
        win.hide()
    }
    close(){
        this.cleanInput()
        this.hide()
    }
    isVisible() {
        const win = this.getRoot()
        return win.isVisible()
    }
    createData() {
        let wcwGrid = $$("wincreate:win:grid"), wcwForm = $$("wincreate:win:form")
        return {
            ...wcwForm.getValues(),
            items: wcwGrid.serialize()
        }
    }
    validateAll() {
        let _grid = $$("wincreate:win:grid"), _form = $$("wincreate:win:form")
        _grid.editStop()
        _form.clearValidation()
        let isGridOK = _grid.validate(), isFormOK = _form.validate()
        return (isGridOK && isFormOK)
    }
}
