import { JetView } from "webix-jet"
import { LinkedInputs2, filter, d2s, gridnf0, ISTATUS2, PAYMETHOD, ISTATUS, IKIND, ITYPE, WN_CRE_WIN_BTNSAV_PURPOSE, CARR, ROLE, ENT, RPP, setConfStaFld, OU } from "models/util"

import Ecwin from "views/inv/ecwin"
import Wncreatewin from "views/wrongnotice/wncreatewin"
import VIEWWN from "views/wrongnotice/wnview"
const size = ENT == 'hlv' ? 100 : 10
const IKIND2 = [
    { "id": 1, "value": _("replace"), "status": 1, "status123": 1 },
    { "id": 2, "value": _("adjust"), "status": 1, "status123": 1 },
    { "id": 4, "value": _("cancelled"), "status": 1, "status123": 1 },
]
const W_CQT_STATUS = [
    {"id": "0", "value": "Chờ gửi CQT", valueclient: "cqt_wait_send"},
    //{"id": 15, "value": "Dữ liệu hợp lệ", valueclient: "wno_valid"},
    {"id": 16, "value": "Dữ liệu không hợp lệ", valueclient: "wno_invalid"},
    {"id": 171, "value": "CQT không chấp nhận", valueclient: "cqt_not_accepted"}
]
const configs = (type) => {
    let lang = webix.storage.local.get("lang")

    webix.ajax().get(`api/sea/afbt?id=${type}`).then(r=>{
        let datasr = r.json()
        let removeDuplicates = (array, key) => {
            let lookup = new Set();
            return array.filter(obj => obj[key] && !lookup.has(obj[key]) && lookup.add(obj[key]));
        }
        let arr = [{id: "*", value: _("all") }, ...datasr]

        $$("form").getList().clearAll()
       
        $$("form").getList().parse(removeDuplicates(arr,'id'))
        $$("form").setValue("*")
    })

    webix.ajax().post(`api/cus`,{itype:type,lang:lang}).then(result => {
        let cols = result.json(), len = cols.length, grid = $$("wngroup:grid")
        let arr = grid.config.columns.filter(item => !CARR.includes(item.id))
        if (len > 0) {
            for (const col of cols) {
                // arr.push({ id: col.id, header: { text: col.label, css: "header" }, adjust: true })
                let obj = { id: col.id, header: { text: col.label, css: "header" }, adjust: true }
                if (col.view == "datepicker") obj.format = webix.i18n.dateFormatStr
                if(col.suggest) obj.collection=col.suggest.data
                arr.push(obj)
            }
        }
        grid.config.columns = arr
        grid.refreshColumns()
        let r4 = $$("wngroup:r4")
        r4.removeView("wngroup:r5")
        r4.removeView("wngroup:r6")
        if (len > 0) {
            if (len <= 5) {
                while (len < 5) {
                    cols.push({})
                    len++
                }
                r4.addView({ id: "wngroup:r5", cols: cols }, 0)
                r4.addView({ id: "wngroup:r6", hidden: true }, 1)
            }
            else {
                while (len < 10) {
                    cols.push({})
                    len++
                }
                r4.addView({ id: "wngroup:r5", cols: cols.splice(0, len / 2) }, 0)
                r4.addView({ id: "wngroup:r6", cols: cols }, 1)
            }
        }
        else {
            r4.addView({ id: "appr:r5", hidden: true }, 0)
            r4.addView({ id: "appr:r6", hidden: true }, 1)
        }

        
        //HLV onChange chon ServCode [c2:servCode, c7:servContent, c8:servName]
        if (ENT=="hlv") {
            // ["c7","c8"].map((e)=>$$(e).disable())
            $$("c2").attachEvent("onChange", (newv, oldv) => {
                if (!newv) ["c7","c8"].map((e)=>$$(e).setValue())
                webix.ajax(`api/kat/payments`).then(result => {
                    let json = result.json(), val = $$("c2").getValue()
                    let arr = json.filter((a)=>{return a.name == val})
                    if(arr.length>0) {
                        let item = arr[0], des = JSON.parse(item.des), name = des.servicename, content = des.servicecontent
                        $$("c7").setValue(content)
                        $$("c8").setValue(name)
                    }
                })
            })
        }
        //
    })
}
const statuschange = (newv) => {
    let lblok, lblcancel
}
export default class CancelView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        webix.protoUI({ name: "dependent2", $cssName: "richselect" }, LinkedInputs2, webix.ui.richselect)
        webix.proxy.wngroup = {
            $proxy: true,
            load: function (view, params) {
                let obj = $$("wngroup:form").getValues()
                Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                if (!params) params = {}
                params.filter = obj
                params.adjtypAll = true
                return webix.ajax(this.source, params)
            }
        }
        let pager = { view: "pager", id: "wngroup:pager", size: size, template: "{common.first()}{common.prev()}{common.pages()}{common.next()}{common.last()}",
            on: {
                onBeforePageChange:function(id){
                    let grida = $$("wngroup:grid")
                    let flag = false
                    let size = $$("wngroup:pager").config.size
                    let page = grida.getPage()
                    for (let i=0; i<size; i++){
                        let id = grida.getIdByIndex(size*page+i)
                        if(id){
                            if(grida.getItem(id) && grida.getItem(id).chk){
                                flag = true
                            }
                        }
                    }
                    if($$("wngroup:grid").getHeaderContent("chk1") && flag)
                        $$("wngroup:grid").getHeaderContent("chk1").uncheck()
                }
            }
        }
        //let recpager = { view: "combo", id: "recpager", maxWidth: 70, tooltip: _("row_per_page"), value: 10, options: RPP }
        let columns = [
            { id: "vie", header: "", width: 35, template:ROLE.includes('PERM_CREATE_WRONGNOTICE_VIEW') ?`<span class='webix_icon wxi-search', title='${_("view")}'></span>`:''},
            {
                id: "chk", header: { content: "masterCheckbox", css: "center", contentId:"chk1" }, width: 35, css: "center", template: (obj, common, value, config) => {
                    return common.checkbox(obj, common, value, config)
                }
            },
            { id: "id", header: { text: "ID", css: "header" }, sort: "server", css: "right", width: 45 },
            { id: "idt", header: { text: _("invoice_date"), css: "header" }, sort: "server", format: d2s, adjust: true },
            { id: "form", header: { text: _("form"), css: "header" }, sort: "server", adjust: true },
            { id: "serial", header: { text: _("serial"), css: "header" }, sort: "server", adjust: true },
            { id: "seq", header: { text: _("seq"), css: "header" }, sort: "server", adjust: true },
            { id: "status", header: { text: _("status"), css: "header" }, sort: "server", collection: ISTATUS(this.app), adjust: true },
            { id: "btax", header: { text: _("taxcode"), css: "header" }, sort: "server", adjust: true },
            { id: "bname", header: { text: _("bname2"), css: "header" }, sort: "server", adjust: true },
            { id: "buyer", header: { text: _("buyer"), css: "header" }, sort: "server", adjust: true },
            { id: "baddr", header: { text: _("address"), css: "header" }, adjust: true },
            { id: "sumv", header: { text: _("sumv"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.format },
            { id: "vatv", header: { text: "VAT", css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.format },
            { id: "totalv", header: { text: _("totalv"), css: "header" }, sort: "server", css: "right", adjust: true, format: gridnf0.format },
            { id: "curr", header: { text: _("curr"), css: "header" }, sort: "server", adjust: true },
            { id: "sec", header: { text: _("search_code"), css: "header" }, format: webix.template.escape, sort: "server", adjust: true },
            { id: "ou", header: { text: _("ou"), css: "header" }, sort: "server", collection: "api/cat/nokache/ou", adjust: true },
            { id: "uc", header: { text: _("creator"), css: "header" }, sort: "server", adjust: true },
            { id: "ic", header: { text: "IC", css: "header" }, sort: "server", adjust: true },
            { id: "note", header: { text: _("note"), css: "header" }, sort: "server", adjust: true },
           
            { id: "status_tbss", header: { text: _("status_tbss"), css: "header" }, sort: "server", collection: W_CQT_STATUS, adjust: true, width: 150 },
            { id: "ikind2", header: { text: _("property"), css: "header" }, collection: IKIND2 },
            { id: "rea", header: { text: _("rea"), css: "header" }, sort: "server", adjust: true },
        ]

        let grid = {
            view: "datatable",
            id: "wngroup:grid",
            scroll: "xy",
            resizeColumn: true,
            editable: false,
            select: "row",
            datafetch: size,
            pager: "wngroup:pager",
            on: {
                onItemClick: function (id) {
                    let row = id.row, item = this.getItem(row)
                    item.chk = !item.chk
                    this.refresh(row)
                }
            },
            onClick: {
                "wxi-search": function (e, r) {
                    const id = r.row
                    webix.ajax(`api/app/htm/${id}`).then(result => {
                        let pdf = result.json()
                        let b64 = pdf.pdf
                        const blob = dataURItoBlob(b64);

                        var temp_url = window.URL.createObjectURL(blob);
                        $$("viewwn:ipdf").define("src", temp_url)
                        $$("viewwn:win").show()
                    })
                }
            },
            columns: columns
        }

        let eForm = 
        [
            {
                cols: [
                    { id: "fd", name: "fd", label: _("fd"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr },
                    { id: "td", name: "td", label: _("td"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr },
                    //{ id: "status", name: "status", label: _("status"), view: "combo", options: ISTATUS2(this.app), value: "6" },
                    //{ id: "wngroup:ikind2", name: "ikind2", label: _("property"), view: "combo", options: IKIND2 },
                   
                    { id: "wngroup:wcqtstatus", name: "status_tbss", label: _("status_tbss"), view: "combo", options: W_CQT_STATUS, labelWidth: 100 },
                    { id: "paym", name: "paym", label: _("payment_method"), view: "combo", options: PAYMETHOD(this.app) },
                    {
                        cols: [
                            { id: "curr", name: "curr", label: _("currency"), view: "combo", suggest: { url: "api/cat/kache/currency" }, gravity: 2 },
                            { name: "ic", label: "IC", view: "text", labelWidth: 25, attributes: { maxlength: 36 }, placeholder: "IC" }
                        ]
                    }
                ]
            },
            {
                cols: [
                    { id: "type", name: "type", label: _("invoice_type"), view: "combo", placeholder:_("choose_inv_type"), options: ITYPE(this.app) },//richselect
                    { id: "form", name: "form", label: _("form"), view: "dependent2", options: [], master: "type", dependentUrl: "api/seq/afbt?id=", relatedView: "serial", relatedAction: "enable" },
                    { id: "serial", name: "serial", label: _("serial"), view: "dependent2", options: [], master: "form", dependentUrl: "api/seq/asbf?id=", disabled: true },
                    // sửa validate ô số HD -> nhập 7kt -> 8 kt
                    { name: "seq", label: _("seq"), view: "text", attributes: { maxlength: 8 }, validate: v => { return !v || /^[0-9]{1,8}$/.test(v) }, invalidMessage: "Số không hợp lệ", placeholder: _("seq") },
                    { id: "ou", name: "ou", label: _("ou"), view: "combo", suggest: { url: "api/ous/bytokenou", filter: filter, relative: "left" } }
                ]
            },
            { //Row 3
                cols: [
                    ENT != "hdb" 
                        ? { name: "btax", label: _("taxcode"), view: "text", attributes: { maxlength: 14 }, placeholder: _("taxcode") }
                        : { name: "btax", label: _("taxcode"), view: "text", attributes: { maxlength: 25 }, placeholder: _("taxcode") },
                    { name: "bname", label: _("bname2"), view: "text", attributes: { maxlength: 50 }, placeholder: _("bname2") },
                    { name: "buyer", label: _("buyer"), view: "text", attributes: { maxlength: 50 }, placeholder: _("buyer") },
                    { name: "sec", label: _("search_code"), view: "text", attributes: { maxlength: 10 }, placeholder: _("search_code") },
                    { id: "wngroup:user", name: "user", label: _("creator"), view: "text", attributes: { maxlength: 24 }, placeholder: _("creator") },//richselect
                ]
            },
            { //Row 3
                cols: [
                    { id: "wngroup:bacc", name: "bacc", label: _("bacc"), view: "text", placeholder: _("bacc"), attributes: { maxlength: 24 }},
                     { id: "invs:adjtyp", name: "ikind2", label: _("property"), view: "combo", options: IKIND2 },{},{},{}
                ]
            },
            {
                id: "wngroup:r4", rows: [{ id: "wngroup:r5", hidden: true }, { id: "wngroup:r6", hidden: true }]
            },
            { //Row 3
                cols: [
                    {},{},{},
                    { view: "button", id: "wngroup:btncreate", type: "icon", icon: "mdi mdi-newspaper", label: _("create_wrong_notice"), width: 170,disabled: ROLE.includes('PERM_CREATE_WRONGNOTICE_CREATE') ? false : true },
                    { view: "button", id: "wngroup:btnsearch", type: "icon", icon: "wxi-search", label: _("search"), width: 120,disabled: ROLE.includes('PERM_CREATE_WRONGNOTICE') ? false : true }
                ]
            }

        ]
        let form = {
            view: "form",
            id: "wngroup:form",
            padding: 3,
            margin: 3,
            elementsConfig: { labelWidth: 85 },
            elements:eForm
            , rules: {
                fd: webix.rules.isNotEmpty,
                td: webix.rules.isNotEmpty,
                $obj: function (data) {
                    const fd = data.fd, td = data.td
                    if (fd > td) {
                        webix.message(_("date_invalid_msg"))
                        return false
                    }
                    const dtime = (new Date(td)).getTime() - (new Date(fd)).getTime()
                    if (ENT == 'vcm') {
                        if (String(fd).substr(0, 7) != String(td).substr(0, 7)) {
                            webix.message(_("fd_td_in_month"))
                            return false
                        }
                    }
                    if (dtime > 31622400000) {
                        webix.message(_("dt_search_between_year"))
                        return false
                    }
                    return true
                }

            }
        }
        let recpager = { view: "combo", id: "recpager", maxWidth: 70, tooltip: _("row_per_page"), value: size, options: RPP }
        let title_page = { view: "label", label:`<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("create_wrong_notice")}</span>`}
        return {
            paddingX: 2, rows: [title_page, form, grid, {
                cols: [
                    recpager,pager, { id: "wngroup:countinv", view: "label" }, {}
                ]
            }]
        }
    }
    ready(v, urls) {
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        if(ENT=="vcm") {
            $$("status").setValue("3")
        }
        
        //
    }
    init() {
        // if (!ROLE.includes('PERM_INVOICE_CANCEL')) {
        //     this.show("/top/home")
        //     window.location.replace(`/${homepage}`)
        //     return
        //   }
        $$("lang:lang").attachEvent("onChange", function(newv, oldv){
          
            //trungpq10 sua song ngu{
            const type_inv=(ITYPE(this.app))[0].id
            let lang = newv
            configs(type_inv)
         //}
        });
        this.VIEWWN = this.ui(VIEWWN)
        this.Ecwin = this.ui(Ecwin)
        let date = new Date(), firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1), grid = $$("wngroup:grid"), form = $$("wngroup:form")
        const all = { id: "*", value: _("all") }
        const excelGrid = $$("can:xlsgrid")
        if (!["hdb", "vcm"].includes(ENT)) $$("fd").setValue(firstDay)
        else $$("fd").setValue(new Date())
        $$("td").setValue(date)
        $$("type").getList().add(all, 0)
        $$("curr").getList().add(all, 0)
        $$("paym").getList().add(all, 0)
        $$("ou").getList().add(all, 0)
        $$("invs:adjtyp").getList().add(all, 0)
        $$("serial").getList().add(all, 0)
        $$("serial").setValue("*")
        $$("invs:adjtyp").setValue(4)
        $$("curr").setValue("*")
        $$("paym").setValue("*")
        //$$("wngroup:ikind2").getList().add(all, 0)
        //$$("wngroup:ikind2").setValue("*")
        $$("wngroup:wcqtstatus").getList().add(all, 0)
        $$("wngroup:wcqtstatus").setValue("0")
        $$("ou").setValue("*")
        $$("type").attachEvent("onChange", (newv) => {
            configs(newv)
        })

        $$("type").setValue("01GTKT")

        $$("form").attachEvent("onChange",(newf)=>{
            let param = {type: $$("type").getValue(), form: newf}
            webix.ajax().get(`api/serial/asbf1nl`, param).then(r=>{
                let datasr = r.json()
                let removeDuplicates = (array, key) => {
                    let lookup = new Set();
                    return array.filter(obj => obj[key] && !lookup.has(obj[key]) && lookup.add(obj[key]));
                }
                let arr = [{id: "*", value: _("all") }, ...datasr]
        
                $$("serial").getList().clearAll()
               
                $$("serial").getList().parse(removeDuplicates(arr,'id'))
                $$("serial").setValue("*")
        
            })
        })
        
        // $$("status").attachEvent("onChange", (newv) => {
        //     statuschange(newv)
        // })

        const search = () => {
            if (form.validate()) {
                if ($$("wngroup:grid").getHeaderContent("chk1")) $$("wngroup:grid").getHeaderContent("chk1").uncheck()
                grid.clearAll()
                grid.loadNext($$("recpager").getValue(), 0, null, "wngroup->api/wno", true)
            }
        }

        $$("recpager").attachEvent("onChange", () => {
            $$("wngroup:pager").config.size = $$("recpager").getValue()
            if (form.validate()) search()
        })
        $$("wngroup:btnsearch").attachEvent("onItemClick", () => {
            search()
        })

        let awaitF = new webix.promise()
        $$("wngroup:btncreate").attachEvent("onItemClick", () => {
            let arr = []
            var size = grid.getPager().config.size
            var page = grid.getPage()
            var ind = size*page
            for ( let i=0; i<size; i++){
                let id = grid.getIdByIndex(ind+i)
                if (grid.getItem(id) && grid.getItem(id).chk) {
                    let type = grid.getItem(id).type, serial = grid.getItem(id).serial
                    let params = {type: type, serial: serial}
                    let r = webix.ajax().sync().get("api/serial/sendtype", params)
                    let result = JSON.parse(r.response)
                    let sendtype = result[0]?result[0].sendtype : ''
                    let row = grid.getItem(id)
                    let doc = row.doc
                    // Trang thai bang 3 hoặc bằng 4 và không có cid thì lấy thông tin huỷ trong thẻ adj
                    if((row.status==4 && row.cid) || row.status==3) {
                        row={...row, ...doc.adj}
                    }
                    if(sendtype) {
                        row.type_ref=1
                        if(row.ikind2 == 1) row.noti_type = 3
                        if(row.ikind2 == 2) row.noti_type = 2
                        if(row.ikind2 == 4) row.noti_type = 1
                        arr.push(row)
                    }
                    else{
                        if(row.ikind2 == 1) row.noti_type = 3
                        if(row.ikind2 == 2) row.noti_type = 2
                        if(row.ikind2 == 4) row.noti_type = 1
                        row.type_ref=3
                        arr.push(row)
                    }
                }
            }
            if (arr.length < 1) {
                webix.message(_("invoice_must_select"), "error")
                return false
            }
            // if (arr.length > 1) {
            //     webix.message(_("not_support_multiple_choice"), "error")
            //     return false
            // }
            // else {
                this.Wncreatewin = this.ui(Wncreatewin)
                this.Wncreatewin.show({
                    updategrid: true,
                    purpose: WN_CRE_WIN_BTNSAV_PURPOSE.SEND_NOW,
                    form: JSON.parse('{"action":"GOM"}'),
                    grid: arr
                }, search)
           // }
        })

        grid.attachEvent("onBeforeLoad", function () {
            this.showOverlay(_("loading"))
        })

        grid.attachEvent("onAfterLoad", function () {
            if (!this.count()) this.showOverlay(_("notfound"))
            else this.hideOverlay()
        })

        grid.attachEvent("onAfterLoad", function () {
            let self = this, count = self.count()
            $$("wngroup:countinv").setValue(`${_("found")} ${count}`)
            if (count) self.hideOverlay()
            else self.showOverlay(_("notfound"))
        })


    }
}