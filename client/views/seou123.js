import { JetView } from "webix-jet"
import { d2s } from "models/util"
export default class Seou123View extends JetView {
  config() {
   // _ = this.app.getService("locale")._
    const grid1 = {
      view: "datatable",
      id: "seou123:grid1",
      select: "row",
      multiselect: false,
      resizeColumn: true,
      columns: [
        { id: "form", header: [{ text: _("form"), css: "header" }, { content: "serverSelectFilter"}], sort: "server", css:"right", adjust: true, fillspace: true },
        { id: "serial", header: [{ text: _("serial"), css: "header" }, { content: "serverSelectFilter" }], sort: "server", css:"right", adjust: true , fillspace: true},
        { id: "fd", header: [{ text: _("valid_from"), css: "header" }, { content: "datepickerFilter" }], sort: "server", css:"right", stringResult: true, format: d2s , fillspace: true},
        { id: "min", header: [{ text: _("fn"), css: "header" },], sort: "server", css: "right", adjust: true , fillspace: true},
        { id: "max", header: [{ text: _("tn"), css: "header" },], sort: "server", css: "right", adjust: true , fillspace: true},
        { id: "cur", header: [{ text: _("current"), css: "header" },], sort: "server", css: "right", adjust: true , fillspace: true},
        { id: "priority", header: [{ text: _("priority"), css: "header" },], sort: "server", css: "right", adjust: true , fillspace: true},
      ],
      url: "api/serial/seou?degree_config=123"
    }

    const grid2 = {
      view: "datatable",
      id: "seou123:grid2",
      select: "row",
      multiselect: false,
      resizeColumn: true,
      headerRowHeight: 33,
      columns: [
        { id: "sel", header: { content: "masterCheckbox", css: "center" }, adjust: "header", css: "center", template: "{common.checkbox()}" },
        { id: "name", header: { text: _("ou"), css: "header" }, adjust: true, fillspace: true },
      ]
    }
    return { paddingX: 2, paddingY: 2, cols: [grid1, { view: "resizer" }, { rows: [grid2, { cols: [{}, { view: "button", id: "seou123:btnsave", type: "icon", icon: "mdi mdi-content-save", label: _("save"), width: 100 }] }] }] }
  }

  init() {
    let grid2 = $$("seou123:grid2"), grid1 = $$("seou123:grid1")
    grid1.attachEvent("onAfterSelect", sel => {
      webix.ajax(`api/serial/seou/${sel.id}`).then(result => {
        grid2.clearAll()
        grid2.parse(result.json())
      })
    })

    $$("seou123:btnsave").attachEvent("onItemClick", () => {
      const ous = grid2.serialize(), serial = grid1.getSelectedItem()
      let arr = [],arrName = []
      for (const ou of ous) {
        if (ou.sel == 1) {
          arr.push(ou.id)
          arrName.push(ou.name)
        }
      }
      webix.ajax().post("api/serial/seou", { serial: serial, arr: arr , name : arrName}).then(() => { webix.message(_("save_ok_msg"), "success") })
    })

  }
}
