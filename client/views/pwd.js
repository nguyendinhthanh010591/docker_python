import { JetView } from "webix-jet"
import {setConfStaFld, ROLE} from "models/util"
export default class PwdView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        const form = {
            view: "form",
            id: "pwd:form",
            css: "background-changepass",
            width: 300,
            elements: [
                { height: 85 },
                { id: "oldpwd", name: "oldpwd", view: "text", type: "password", attributes: { maxlength: 20 }, placeholder: _("password_old") },
                { id: "newpwd", name: "newpwd", view: "text", type: "password", attributes: { maxlength: 20 }, placeholder: _("password_new") },
                { id: "repeat", name: "repeat", view: "text", type: "password", attributes: { maxlength: 20 }, placeholder: _("password_repeat") },
                {
                    view: "button", type: "icon", icon: "mdi mdi-key-plus", label: _("password_change"), click: () => {
                        let form = $$("pwd:form")
                        if (!form.validate()) return false
                        webix.ajax().post("api/pwd", form.getValues()).then(result => {
                            webix.message(result.text())
                            // this.show("/top/home")
							// Đổi mật khẩu thì logout phiên cũ ra đăng nhập lại
                            this.app.show("logout")
                            webix.storage.session.clear()
                        })
                    }
                    // }, hidden: ROLE.includes('PASSWORD_CHANGE_PASS') ? false : true
                }
            ],
            rules: {
                $all: webix.rules.isNotEmpty,
                $obj: function (obj) {
                    if (obj.newpwd.length < 8) {
                        webix.message(_("password_length_invalid"))
                        return false
                    }
                    if (obj.repeat !== obj.newpwd) {
                        webix.message(_("password_repeat_invalid"))
                        return false
                    }
                    if (!(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/.test(obj.newpwd))) {
                        webix.message(_("password_character_contain"), "error")
                        return false
                        
                    }
                    return true
                }
            }
        }
        return { cols: [{}, { rows: [{}, form, {}] }, {}] }
    }
    ready(v, urls) {
        const session = webix.storage.session.get("session")
        $$("pwd:form").setValues({ uid: session.uid }, true)
        $$("oldpwd").focus()
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
}