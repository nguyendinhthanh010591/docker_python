import { JetView } from "webix-jet"
import { w, h, LinkedInputs2, size, filter, d2s, expire, gridnf0, ISTATUS, PAYMETHOD, ITYPE, IKIND, ROLE, TRSTATUS, CARR, ENT, RPP, VAT, setConfStaFld, OU, MAILSTATUS, SMSSTATUS, gridnfconf } from "models/util"

const MODULE = [
    { id: "GL", value: "GL" },
    { id: "FT", value: "FT" }
]

export default class EntryView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        webix.protoUI({ name: "dependent", $cssName: "richselect" }, LinkedInputs2, webix.ui.richselect)
        webix.proxy.ent = {
            $proxy: true,
            load: function (view, params) {
                let obj = $$("ent:form").getValues()
                Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                if (!params) params = {}
                params.filter = obj
                return webix.ajax(this.source, params)
            }
        }
        let pager = { view: "pager", width: 340, id: "invs:pager", size: size, template: `{common.first()}{common.prev()} {common.pages()}{common.next()}{common.last()}` }
        let gridcol = [
            { id: "c2", header: { text: _("tran_no"), css: "header" }, sort: "server", adjust: true },
            { id: "note", header: { text: _("note"), css: "header" }, sort: "server", adjust: true },
            { id: "total", header: { text: _("total_vib"), css: "header" }, sort: "server", adjust: true, format: gridnf0.numberFormat },
            { id: "ic", header: { text: _("sid_vib"), css: "header" }, sort: "server", adjust: true },
            { id: "systemcode", header: { text: _("ou"), css: "header" }, sort: "server", adjust: true },
            { id: "bcode", header: { text: _("customer_code"), css: "header" }, sort: "server", adjust: true },
            { id: "c4", header: { text: _("account_GL"), css: "header" }, sort: "server", adjust: true },
            { id: "branchCode", header: { text: _("branchCode"), css: "header" }, sort: "server", adjust: true },
            { id: "STT", header: { text: _("stt"), css: "header" }, sort: "server", adjust: true, format: gridnf0.numberFormat },
            { id: "curr", header: { text: _("curr"), css: "header" }, sort: "server", adjust: true },
            { id: "uc", header: { text: _("creator"), css: "header" }, sort: "server", adjust: true },
            { id: "ma_ks", header: { text: _("ma_ks"), css: "header" }, sort: "server", adjust: true },
            { id: "c3", header: { text: _("trandate_label"), css: "header" }, sort: "server", format: d2s, adjust: true, width: 120 }
        ]

        let grid = {
            view: "datatable",
            id: "ent:grid",
            select: "row",
            multiselect: false,
            resizeColumn: true,
            onContext: {
                webix_view: function (e, id) {
                    if (id && id.row) this.select(id)
                }
            },
            columns: gridcol
        }

        // const url_ou = ENT == "hdb" ? "api/cat/bytoken" : "api/cat/nokache/ou"
        const url_ou = "api/ous/bytokenou"  // tât ca deu load theo chi nhanh
        let eForm =
            [
                { height: 5 },
                { //R1
                    cols: [
                        {},
                        { id: "ent:module", name: "module", label: _("module"), view: "combo", options: MODULE, width: 300, placeholder: "Chọn phân hệ", required: true },
                        { width: 50 },
                        { id: "ent:td", name: "trandate", label: _("trandate_label"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr, labelWidth: 100 },
                        {},

                    ]
                },
                {//R2
                    cols: [
                        {},
                        { id: "ent:tranno", name: "tran_no", label: _("tran_no"), view: "text", attributes: { maxlength: 20 }, required: true },//richselect
                        { width: 150 },
                        { id: "ent:btnSearch", label: _("search_query"), view: "button", type: "icon", icon: "mdi mdi-search-web", width: 200, required: true },
                        {}

                    ]
                },
                { height: 20 }
            ]
        let form = {
            view: "form",
            id: "ent:form",
            padding: 3,
            margin: 3,
            elementsConfig: { labelWidth: 85 },
            elements: eForm
            , rules: {
                trandate: webix.rules.isNotEmpty,
                module: webix.rules.isNotEmpty,
                tran_no: webix.rules.isNotEmpty
            },
            on: {
                onAfterValidation: (result) => {
                    if (!result) webix.message(_("required_msg"))
                }
            }
        }
        let title_page = { view: "label", label:`<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("cancel_GL")}</span>`}
        return {
            paddingX: 2,
            rows: [title_page, form, grid,
                {
                    cols: [pager, { id: "ent:countinv", view: "label" }, {}
                    ]
                },
                {
                    cols: [
                        {}, {}, {
                            cols: [
                                { view: "button", id: "ent:btndel", type: "icon", icon: "mdi mdi-content-save", label: _("cancel"), hidden: false, disabled: true, width: 200 },
                            ]
                        }, { width: 310 }
                    ]
                },
                { height: 20 }
            ]
        }
    }
    ready(v, urls) {
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", { url: urls[0].page, params: urls[0].params }).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
    }
    init() {
        webix.extend($$("ent:form"), webix.ProgressBar)
        let form1 = $$("ent:form"), grid1 = $$("ent:grid")
        let date = new Date()
        $$("ent:td").setValue(date)

        //xử lý sự kiện click vào nút truy vấn

        $$("ent:btnSearch").attachEvent("onItemClick", () => {
            try {
                const param = $$("ent:form").getValues()
                if (!(param.tran_no && param.module && param.trandate)) {
                    webix.message(_("required_msg"), "error")
                    return false
                }
                var trandate = new Date(param.trandate)
                let short_month_names = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']
                let dateStr = (trandate.getDate() < 10 ? `0${trandate.getDate()}` : trandate.getDate()) + "-"
                    + short_month_names[trandate.getMonth()] + "-"
                    + trandate.getFullYear()
                param.trandate = dateStr
                webix.ajax().post("api/inv/gettrans", param)
                    .then(data => data.json())
                    .then(data => {
                        if (data) {
                            grid1.clearAll()
                            grid1.parse(data)
                            $$("ent:btndel").enable()
                        }
                        else {
                            webix.message({
                                type: "error",
                                text: _("error_trans_message")
                            })
                        }
                    }).catch(err => {
                        console.log(err)
                        webix.message(err.message, "error")
                    })
            }
            catch (err) {
                webix.message(err.message, "error")
            }
        })

        $$("ent:btndel").attachEvent("onItemClick", () => {
            const rows = grid1.serialize();
            const values = form1.getValues();
            let arr = []
            for (let row of rows) {
                if (row) arr.push(row)
            }

            webix.ajax().post("api/ent/check", { arr: arr, values: values }).then(result => {
                const rows1 = result.json()
                if (rows1 == 0) {
                    webix.confirm("Đã có yêu cầu hủy cho số bút toán này", "confirm-warning").then(() => {
                        window.location.reload(true)
                    })
                } else {
                    webix.confirm("Bạn có chắc muốn hủy không?", "confirm-warning").then(() => {
                        webix.ajax().post("api/ent", { arr: arr, values: values }).then(result => {
                            const rows = result.json()
                            if (rows == 1) {
                                webix.message(_("entry_success"), "success")
                                window.location.reload(true)
                            } else {
                                webix.message(_("entry_error"), "error")
                                window.location.reload(true)
                            }

                        })
                    })
                }
            })

        })
    }

}