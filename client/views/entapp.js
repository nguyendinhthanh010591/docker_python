import { JetView } from "webix-jet"
import { w, h, LinkedInputs2, size, filter, d2s, expire, gridnf0, ISTATUS, PAYMETHOD, ITYPE, IKIND, ROLE, TRSTATUS, CARR, ENT, RPP, VAT, setConfStaFld, OU, MAILSTATUS, SMSSTATUS, gridnfconf } from "models/util"

const MODULE = [
    { id: "GL", value: "GL" },
    { id: "FT", value: "FT" }
]

export default class ApprEnt extends JetView {
    config() {
        _ = this.app.getService("locale")._
        webix.protoUI({ name: "dependent", $cssName: "richselect" }, LinkedInputs2, webix.ui.richselect)
        webix.proxy.ent = {
            $proxy: true,
            load: function (view, params) {
                let obj = $$("ent:form").getValues()
                Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                if (!params) params = {}
                params.filter = obj
                return webix.ajax(this.source, params)
            }
        }
        let pager = { view: "pager", width: 340, id: "invs:pager", size: size, template: `{common.first()}{common.prev()} {common.pages()}{common.next()}{common.last()}` }
        let gridcol = [
            { id: "c2", header: { text: _("tran_no"), css: "header" }, sort: "server", adjust: true },
            { id: "note", header: { text: _("note"), css: "header" }, sort: "server", adjust: true },
            { id: "total", header: { text: _("total_vib"), css: "header" }, sort: "server", adjust: true, format: gridnf0.numberFormat },
            { id: "ic", header: { text: _("sid_vib"), css: "header" }, sort: "server", adjust: true },
            { id: "systemcode", header: { text: _("ou"), css: "header" }, sort: "server", adjust: true },
            { id: "bcode", header: { text: _("customer_code"), css: "header" }, sort: "server", adjust: true },
            { id: "c4", header: { text: _("account_GL"), css: "header" }, sort: "server", adjust: true },
            { id: "branchCode", header: { text: _("branchCode"), css: "header" }, sort: "server", adjust: true },
            { id: "STT", header: { text: _("stt"), css: "header" }, sort: "server", adjust: true, format: gridnf0.numberFormat },
            { id: "curr", header: { text: _("curr"), css: "header" }, sort: "server", adjust: true },
            { id: "uc", header: { text: _("creator"), css: "header" }, sort: "server", adjust: true },
            { id: "ma_ks", header: { text: _("ma_ks"), css: "header" }, sort: "server", adjust: true },
            { id: "c3", header: { text: _("trandate_label"), css: "header" }, sort: "server", format: d2s, adjust: true, width: 120 },
        ]

        let grid = {
            view: "datatable",
            id: "ent:grid",
            select: "row",
            multiselect: false,
            resizeColumn: true,
            onContext: {
                webix_view: function (e, id) {
                    if (id && id.row) this.select(id)
                }
            },
            columns: gridcol
        }

        // const url_ou = ENT == "hdb" ? "api/cat/bytoken" : "api/cat/nokache/ou"
        const url_ou = "api/ous/bytokenou"  // tât ca deu load theo chi nhanh
        let eForm =
            [
                { height: 5 },
                { //R1
                    cols: [
                        {},
                        { id: "ent:module", name: "module", label: _("module"), view: "combo", options: MODULE, width: 300, placeholder: "Chọn phân hệ", required: true },
                        { width: 50 },
                        { id: "ent:td", name: "trandate", label: _("trandate_label"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr, labelWidth: 100 },
                        {},

                    ]
                },
                {//R2
                    cols: [
                        {},
                        { id: "ent:tranno", name: "tran_no", label: _("tran_no"), view: "text", attributes: { maxlength: 20 }, required: true },//richselect
                        { width: 150 },
                        { id: "ent:btnSearch", label: _("search_query"), view: "button", type: "icon", icon: "mdi mdi-search-web", width: 200, required: true },
                        {}

                    ]
                },
                { height: 20 }
            ]
        let form = {
            view: "form",
            id: "ent:form",
            padding: 3,
            margin: 3,
            elementsConfig: { labelWidth: 85 },
            elements: eForm
            , rules: {
                trandate: webix.rules.isNotEmpty,
                module: webix.rules.isNotEmpty,
                tran_no: webix.rules.isNotEmpty
            },
            on: {
                onAfterValidation: (result) => {
                    if (!result) webix.message(_("required_msg"))
                }
            }
        }
        let title_page = { view: "label", label:`<span class="title;top" style="color:#254EBE;font-size: 25px;font-weight:bold;"> 
        ${_("approve_cancel")}</span>`}
        return {
            paddingX: 2,
            rows: [title_page, form, grid, { height: 30 },

                {
                    cols: [
                        {},
                        {
                            cols: [
                                { view: "button", id: "ent:btndel", type: "icon", icon: "mdi mdi-arrow-right-bold-circle-outline", label: _("cancel"), hidden: false, disabled: true, width: 200 },
                                {},
                                { view: "button", id: "ent:btnappr", type: "icon", icon: "mdi mdi-block-helper", label: _("btn_reject"), hidden: false, disabled: true, width: 200 },
                                { view: "list", id: "invs:filelist", type: "uploader", autoheight: true, borderless: true, hidden: true },
                                { id: "invs:file", view: "uploader", multiple: false, autosend: false, link: "invs:filelist", accept: "application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/pdf", upload: "api/inv/minutes/upload", type: "icon", icon: "mdi mdi-file-find-outline", label: _("up_minu"), tooltip: _("up_minu_des"), width: 200, disabled: true }
                            ]
                        }, {}
                    ]
                },
                { height: 20 }
            ]
        }
    }
    ready(v, urls) {
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", { url: urls[0].page, params: urls[0].params }).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
    }
    init() {
        webix.extend($$("ent:form"), webix.ProgressBar)
        let form1 = $$("ent:form"), grid1 = $$("ent:grid")
        let date = new Date(), uploader = $$("invs:file")
        $$("ent:td").setValue(date)

        //xử lý sự kiện click vào nút truy vấn

        $$("ent:btnSearch").attachEvent("onItemClick", () => {
            try {
                const param = $$("ent:form").getValues()
                if (!(param.tran_no && param.module && param.trandate)) {
                    webix.message(_("required_msg"), "error")
                    return false
                }
                var trandate = new Date(param.trandate)
                let short_month_names = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']
                let dateStr = (trandate.getDate() < 10 ? `0${trandate.getDate()}` : trandate.getDate()) + "-"
                    + short_month_names[trandate.getMonth()] + "-"
                    + trandate.getFullYear()
                param.trandate = dateStr
                webix.ajax().post("api/ent/cancel", param)
                    .then(data => data.json())
                    .then(data => {
                        if (data) {
                            grid1.clearAll()
                            grid1.parse(data)
                            $$("ent:btndel").enable()
                            $$("ent:btnappr").enable()
                        }
                        else {
                            webix.message({
                                type: "error",
                                text: "Không tìm thấy yêu cầu hủy"
                            })
                        }
                    }).catch(err => {
                        webix.message(err.message, "error")
                    })
            }
            catch (err) {
                webix.message(err.message, "error")
            }
        })

        $$("ent:btndel").attachEvent("onItemClick", () => {
            webix.confirm("Bạn muốn duyệt hủy hóa đơn này?", "confirm-warning").then(() => {
                const param = $$("ent:form").getValues()
                if (!(param.tran_no && param.module && param.trandate)) {
                    webix.message(_("required_msg"), "error")
                    return false
                }
                var trandate = new Date(param.trandate)
                let short_month_names = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']
                let dateStr = (trandate.getDate() < 10 ? `0${trandate.getDate()}` : trandate.getDate()) + "-"
                    + short_month_names[trandate.getMonth()] + "-"
                    + trandate.getFullYear()
                param.trandate = dateStr
                webix.ajax().post("api/ent/appcal", param).then(result => {
                    const rows = result.json()
                    if (rows == 0) {
                        this.webix.ajax().post("api/ent/cancelinv", param).then(result => {
                            const result2 = result.json()
                            if (result2 == 1) {
                                webix.ui({
                                    view: "window",
                                    id: "popupTrans",
                                    // height:400,
                                    // width:400,
                                    isVisible: true,
                                    position: "center",
                                    head: "Thông báo",
                                    autofocus: true,
                                    body: {
                                        view: "form",
                                        id: "searchTrans:form",
                                        elements: [
                                            {
                                                view: "label",
                                                label: "Hóa đơn đã được gửi mail đến khách hàng",
                                                align: "center",
                                                height: 20
                                            },
                                            {
                                                view: "label",
                                                label: "Update biên bản hủy!",
                                                align: "center"
                                            }
                                            ,
                                            {
                                                cols: [
                                                    { view: "button", id: "ent:btnSearchTrans", value: _("cancel"), css: "webix_primary", width: 100 },
                                                    { width: 50 },
                                                    { id: "ent:btnupload1", view: "uploader", link: "invs:filelist", multiple: false, autosend: false, accept: "application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/pdf", upload: "api/inv/minutes/upload", type: "icon", icon: "mdi mdi-file-find-outline", label: _("up_minu"), tooltip: _("up_minu_des"), width: 100 }
                                                ]
                                            }
                                        ]
                                    }
                                }).show();

                                $$("ent:btnSearchTrans").attachEvent("onItemClick", () => {
                                    $$("popupTrans").hide()
                                    $$("invs:file").enable()
                                    $$("ent:btndel").disable()
                                    $$("ent:btnappr").disable()
                                })
                                
                    
                                $$("ent:btnupload1").attachEvent("onAfterFileAdd", (file) => {
                                    webix.ajax().post("api/ent/getID", param).then(result => {
                                        const result2=result.json()
                                        webix.message(result2)
                                        let typemin = "can",  iid = result2
                                        const files = $$("ent:btnupload1").files, file_id = files.getFirstId()
                                        if (file_id) {
                                            var params = { type: typemin, id: iid }
                                            $$("ent:btnupload1").define("formData", params)
                                            $$("ent:btnupload1").send(rs => {
                                                if (rs.err) webix.message(rs.err, "error")
                                                else if (rs.ok) {
                                                    webix.message(_("up_minutes_success"), "success")
                                                    search()
                                                }
                                                else webix.message(_("up_minutes_fail"), "error")
                
                                            })
                                        }
                                        else {
                                            webix.message(_("file_required"))
                                        }

                                        $$("popupTrans").hide()
                                        $$("ent:btndel").disable()
                                        $$("ent:btnappr").disable()
                                    })
                                })


                            }

                        })
                    } else if (rows == -1) {
                        webix.confirm("Giao dịch đã hủy", "confirm-warning").then(() => {
                            window.location.reload(true)
                        })
                    }
                    else {
                        webix.message(_("entry_error"), "error")
                        this.app.show("/top/entapp")
                    }
                })
            })
        })
        $$("ent:btnappr").attachEvent("onItemClick", () => {
            webix.confirm("Bạn không muốn duyệt hủy hóa đơn này?", "confirm-warning").then(() => {
                window.location.reload(true)
            })
        })

        $$("invs:file").attachEvent("onAfterFileAdd", (file) => {
            const param = $$("ent:form").getValues()
            webix.confirm(_("up_minutes_confirm"), "confirm-warning").then(() => {
                webix.ajax().post("api/ent/getID", param).then(result => {
                        const result2=result.json()
                        webix.message(result2)
                        let typemin = "can",  iid = result2
                        const files = uploader.files, file_id = files.getFirstId()
                        if (file_id) {
                            var params = { type: typemin, id: iid }
                            uploader.define("formData", params)
                            uploader.send(rs => {
                                if (rs.err) webix.message(rs.err, "error")
                                else if (rs.ok) {
                                    webix.message(_("up_minutes_success"), "success")
                                    search()
                                }
                                else webix.message(_("up_minutes_fail"), "error")

                            })
                        }
                        else webix.message(_("file_required"))
                    })

            })

        })

    }
}