import { JetView, plugins } from "webix-jet"
import { ROLE,GETMENU, ENT, SERIAL_GRANT, SERIAL_USR_GRANT, LDAP_PRIVATE, setConfStaFld, IS_USE_LOCAL_USER, LOCALUSR } from "models/util"
export default class MenuView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        let menu = [{ id: "home", icon: "mdi mdi-home", value: _("home") }]
        let menu_client = GETMENU(this.app)
        //SYS
        // let dsys = []
        // if (ROLE.includes('PERM_SYS_CONFIG')) dsys.push( {id:"sys.ous", icon:"mdi mdi-file-tree", value: _("system_config") } )
        // if (ROLE.includes('PERM_SYS_USER')) dsys.push( { id: "sys.user", icon: "mdi mdi-account-edit", value: _("user_manage") })
        // if (ROLE.includes('PERM_SYS_GROUP')) dsys.push( { id: "sys.group", icon: "mdi mdi-account-edit", value: _("group_manage") })
        // if (ROLE.includes('PERM_SYS_ROLE')) dsys.push( { id: "sys.role", icon: "mdi mdi-account-group", value: _("grant") })
        // if (ROLE.includes('PERM_SYS_TEMPLATE')) dsys.push( { id: "sys.editor", icon: "mdi mdi-playlist-edit", value: _("invoice_template") })
        // if (ROLE.includes('PERM_SYS_EMAIL')) dsys.push( { id: "sys.email", icon: "mdi mdi-email-plus-outline", value: _("mail_template") })
        // if (ROLE.includes('PERM_SYS_INV_CONFIG')) dsys.push( { id: "sys.col", icon: "mdi mdi-cogs", value: _("invoice_config") })
        // if (ROLE.includes('PERM_SYS_FIELD_CONFIG')) dsys.push( { id: "sys.field", icon: "mdi mdi-newspaper", value: _("field_config") })
        // if (ROLE.includes('PERM_LOG_SYS')) dsys.push({ id: "syslog", icon: "mdi mdi-math-log", value: _("syslog") })
        // if (ROLE.includes('PERM_SYS_SEGMENT')) dsys.push( { id: "sys.segment", icon: "mdi mdi-newspaper", value: _("segment_manage") })
        // if (ROLE.includes('PERM_DEP')) dsys.push( { id: "sys.department", icon: "mdi mdi-newspaper", value: _("Department") })
        // if (ROLE.includes('PERM_ROLE_DEP')) dsys.push( { id: "sys.roledep", icon: "mdi mdi-newspaper", value: _("department_grant") })
        // if (dsys.length > 0) menu.push({ icon: "mdi mdi-monitor-dashboard", value: eval(`_("system")`), data: dsys })
        // //DM
        // let dcat = []
        // if (ROLE.includes('PERM_CATEGORY_CUSTOMER')) dcat.push( { id: "cat.org", icon: "mdi mdi-account-multiple-check", value: _("catalog_customer") })
        // if (ROLE.includes('PERM_CATEGORY_ITEMS')) dcat.push( { id: "cat.gns", icon: "mdi mdi-cart-outline", value: _("catalog_items") })
        // if (ROLE.includes('PERM_CATEGORY_RATE')) dcat.push( { id: "cat.exch", icon: "mdi mdi-currency-usd", value: _("exchange_rate") })
        // if (ROLE.includes('PERM_CATEGORY_ANOTHER')) dcat.push( { id: "cat.cat", icon: "mdi mdi-newspaper", value: _("catalog_catalog") })
        // if (ROLE.includes('PERM_CATEGORY_LOCATION')) dcat.push( { id: "cat.loc", icon: "mdi mdi-earth", value: _("catalog_location")})
        // if (ROLE.includes('PERM_CATEGORY_VENDOR')) dcat.push({id:"cat.vendor",icon: "mdi mdi-account-multiple-plus", value: _("catalog_vendor")})
        // if (dcat.length > 0) menu.push({ icon: "mdi mdi-file-cabinet", value: _("catalog"), data: dcat })
        
        // //TBPH
        // let released = []
        // if (ROLE.includes('PERM_TEMPLATE_REGISTER')) released.push({ id: "serial", icon: "mdi mdi-numeric", value: _("released") })
        // if (ROLE.includes('PERM_TEMPLATE_REGISTER_GRANT')) released.push({ id: "seou", icon: "mdi mdi-account-multiple-check", value: _("seou") })
        // if (ROLE.includes('PERM_TEMPLATE_REGISTER_USRGRANT')) released.push({ id: "seusr", icon: "mdi mdi-account-multiple-check", value: _("seusr") })
        // if (released.length > 0) menu.push({ icon: "mdi mdi-bullhorn", value: _("released"), data: released })

        // //TRA CUU HOA DON
        // let invs = []
        // if (  ROLE.includes('PERM_SEARCH_INV')) invs.push({id:"inv.invs",icon: "mdi mdi-folder-search-outline", value: _("invoice_search")})
        // if (invs.length > 0) menu.push({ icon: "mdi mdi-folder-search-outline", value: _("invoice_search"), data: invs })

        // //GIAO DICH
        // if (ROLE.includes('PERM_TRAN')  ) {
        //     let arr = []
        //     if (  ROLE.includes('PERM_TRANS')) arr.push({ id: "inv.itrans", icon: "mdi mdi-database-search", value: _("tran_search") })
        //     if (  ROLE.includes('PERM_TRANS_APPR')) arr.push({ id: "inv.trans_app", icon: "mdi mdi-check-outline", value: _("tran_search_appr") })
        //     menu.push({ icon: "mdi mdi-database-search", value: _("tran_management"), data: arr })
        // }

        // //LAP HOA DON DAU RA
        // let invcreate = []
        // if (ROLE.includes('PERM_CREATE_INV_MANAGE')) invcreate.push({ id: "inv.inv", icon: "mdi mdi-file-plus", value: _("invoice_issue") })
        // if (ROLE.includes('PERM_INV_ONLINE')) invcreate.push({ id: "inv.inv_ol", icon: "mdi mdi-file-plus", value: _("inv_ol") })
        // if (ROLE.includes('PERM_CREATE_INV_EXCEL')) invcreate.push({ id: "inv.xls", icon: "mdi mdi-upload-multiple", value: _("invoice_from_excel") })
        // if (ROLE.includes('PERM_CREATE_INV_ADJ_EXCEL')) invcreate.push({ id: "inv.xls_adj", icon: "mdi mdi-upload-multiple", value: _("invoice_adj_from_excel") })
        // if (ROLE.includes('PERM_CREATE_INV_SEQ')) invcreate.push({ id: "inv.seq", icon: "mdi mdi-sort-numeric", value: _("invoice_seq") })
        // if (ROLE.includes('PERM_CREATE_INV_ADJ_EXCEL')) invcreate.push({ id: "inv.xls_adj", icon: "mdi mdi-upload-multiple", value: _("invoice_adj_from_excel") })
        // if (ROLE.includes('PERM_APPROVE_INV_MANAGE')) invcreate.push({ id: "inv.appr", icon: "mdi mdi-check-outline", value: _("invoice_approve") })
        // if (ROLE.includes('PERM_INVOICE_CANCEL')) invcreate.push({ id: "inv.cancel", icon: "mdi mdi-delete-variant", value: _("invoice_cancel") })
        // if (invcreate.length > 0) menu.push({ icon: "mdi mdi-content-paste", value: _("invoice_management"), data: invcreate })
        
        // // HOA DON DAU VAO
        // let bin = []
        // if (ROLE.includes('PERM_SEARCH_BINV')) bin.push({ id: "inv.bin", icon: "mdi mdi-file", value: _("input_invoice") })
        // if (bin.length > 0) menu.push({ icon: "mdi mdi-file", value: _("input_invoice"), data: bin })

        // // KY SO HOA DON
        // let signdoc = []
        // if (ROLE.includes('PERM_SIGN')) signdoc.push({ id: "ds", icon: "mdi mdi-signal-4g", value: _("file_sign") })
        // if (signdoc.length > 0) menu.push({ icon: "mdi mdi-file", value: _("file_sign"), data: signdoc })
        
        // BAO CAO 
        // let rep = []
        // if (ROLE.includes('PERM_REPORT_INV')) rep.push({ id: "rep.report", icon: "mdi mdi-printer", value: _("reports") })
        // if (rep.length > 0) menu.push({ icon: "mdi mdi-printer", value: _("reports"), data: rep })
        
        // if (LDAP_PRIVATE || LOCALUSR) menu.push({ id: "pwd", icon: "mdi mdi-key", value: _("password") })
        // if ((LDAP_PRIVATE || IS_USE_LOCAL_USER) && (ROLE.includes("PERM_FULL_MANAGE")||ROLE.includes("PERM_RESET_PASSWORD"))) menu.push({ id: "rset", icon: "mdi mdi-account-circle", value: _("password_reset_function") })
        
        for(const t of menu_client){
            menu.push(t)
        }
        if (LDAP_PRIVATE || LOCALUSR) menu.push({ id: "pwd", icon: "mdi mdi-key", value: _("password") })
        if ((LDAP_PRIVATE || IS_USE_LOCAL_USER) && (ROLE.includes("PERM_FULL_MANAGE")||ROLE.includes("PERM_RESET_PASSWORD"))) menu.push({ id: "rset", icon: "mdi mdi-account-circle", value: _("password_reset_function") })
        // logout
        menu.push({ id: "logout", icon: "mdi mdi-logout", value: _("logout") })
        const ui = {
            view: "sidebar",
            width: 250,
            titleHeight: 45,
            minHeight: 572,
            collapsed: true,
            data: menu
        }
        return ui
    }
    urlChange(ui, url) {
        if (!ui.find(opts => url[1].page == opts.id).length) ui.unselect()
    }
    ready(v, urls) {
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
    init(view) {
        this.use(plugins.Menu, { id: view })
        this.on(this.app, "menu:toggle", () => view.toggle())
    }
}