import { JetView } from "webix-jet"
import { REG_ACT, expire, VAT, FITYPE, DTSENDTYPE, REGTYPE, HASCODELIST, CARR, SC } from "models/util"
import { toWords } from "views/n2w"
import Iwin from "views/inv/iwin"
let parse = false, action, type, isvat, ispxk, updateSumVatTotal, useInputAmount = false, updateSUM = true, objEdit = {old:"",new:""}

const new_form = () => {
    const grid = $$("stms:grid"), form = $$("stms:form")
    $$("stms:form:sname").setValue(session.on)
    $$("stms:form:stax").setValue(session.taxc)
    $$("stms:form:taxoname").setValue(session.org.taxoname)
    $$("stms:form:createdt").setValue(new Date())
    $$("stms:form:id").setValue("01/ĐKTĐ-HĐĐT")
    systime().then(now => {
        let i = 0
        while (i < 3) {
            let obj = {}
            grid.add(obj)
            i++
        }

    }).then(() => { grid.clearValidation() })
}
const today = new Date

const reset_grid = (result) => {
    const cols = result.json(), grid = $$("adj:grid")
    let arr = grid.config.columns.filter(item => !CARR.includes(item.id))
    if (cols.length > 0) {
        let c = 6
        for (const col of cols) {
            if (col.css) {
                col.format = gridna.format
                col.editParse = gridna.editParse
                col.editFormat = gridna.editFormat
            }
            arr.splice(c++, 0, col)
        }
    }
    grid.config.columns = arr
    grid.refreshColumns()
}

webix.editors.editdate = webix.extend({
      render:function(){
          var icon = "<span class='webix_icon wxi-calendar' style='position:absolute; cursor:pointer; top:8px; right:5px;'></span>";
        var node = webix.html.create("div", {
            "class":"webix_dt_editor"
         }, "<input type='text'>"+icon);

          node.childNodes[1].onclick = function(){
         var master = webix.UIManager.getFocus();
            var editor = master.getEditor();

            master.editStop(false);
            var config = master.getColumnConfig(editor.column);
            config.editor = "date";
            master.editCell(editor.row, editor.column);
            config.editor = "editdate";
          }
          return node;
      }
}, webix.editors.text);

webix.editors.$popup.date.body = {view: 'datepicker', placeholder:"dd/mm/yyyy hh:mm:ss", timepicker:true,  editable: true, stringResult: true};

export default class InvView extends JetView {

    config() {

        // _ = this.app.getService("locale")._
        // const LinkedInputs4 = {
        //     $init: function (config) {
        //         let master = $$(config.master)
        //         master.attachEvent("onChange", newv => {
        //             let me = this,
        //                 list
        //             if (!parse) me.setValue()
        //             if (action !== "view") {
        //                 list = me.getList()
        //                 list.clearAll()
        //             }
        //             if (!newv) return
        //             if (action !== "view") {
        //                 list.load(config.dependentUrl + newv).then(data => {
        //                     if (!parse) {
        //                         let arr = data.json()
        //                         if (arr.length > 0) me.setValue(arr[0].id)
        //                     }
        //                 })
        //             }
        //         })
        //     }
        // }
        //
        // webix.protoUI({ name: "dependent", $cssName: "combo" }, LinkedInputs4, webix.ui.combo)

        let grid = {
            view: "datatable",
            id: "stms:grid",
            editable: true,
            navigation: true,
            editaction: "click",
            resizeColumn: true,
            clipboard: "custom",
            multiselect: false,
            select: "cell",
            headerRowHeight: 33,
            columns: [
                { id: "del", header: [`<span class='webix_icon wxi-plus' title='${_("details_insert")}'></span>`], template: `<span class='webix_icon wxi-trash' title='${_("details_delete")}'></span>`, width: 35 },
                { id: "line", header: { text: _("sttu"), css: "header" }, css: "right", adjust: true , require:true, },
                { id: "sign_org", header: { text: _("sign_org"), css: "header" }, width: 1000, adjust: true, editor: "text" , require:true, },
                { id: "sign_seri", header: { text: _("sign_seri"), css: "header" }, adjust: true, editor: "text", require:true,fillspace: true},
                { id: "sign_from", header: { text: _("sign_from"), css: "header" }, adjust: true, editor: "date", format: webix.i18n.fullDateFormatStr, require:true,fillspace: true},
                { id: "sign_to", header: { text: _("sign_to"), css: "header" }, adjust: true, editor: "date", format: webix.i18n.fullDateFormatStr, require:true,fillspace: true},
                { id: "action", header: { text: _("act"), css: "header" }, adjust: true, inputAlign: "right", collection: REG_ACT(this.app), editor: "richselect", require:true, fillspace: true },
            ],
            onClick: {
                "wxi-trash": function (e, id) {
                    this.remove(id)
                    return false
                },
                "wxi-plus": function (e, id) {
                    let me = this
                    let obj = {}
                    if (SC) obj.src = "0"
                    me.add(obj)
                    me.clearValidation()
                    return false
                },
            },
            rules: {
                sign_org: webix.rules.isNotEmpty,
                sign_seri: webix.rules.isNotEmpty,
                sign_from: webix.rules.isNotEmpty,
                sign_to: webix.rules.isNotEmpty,
                action: webix.rules.isNotEmpty,
                $obj: function (data) {
                    if (data.sign_from > data.sign_to) {
                        webix.message(_("date_invalid_msg"), "error", expire)
                        return false
                    }
                    return true
                }
            }
        }

        let form = {
            view: "form",
            id: "stms:form",
            padding: 0,
            margin: 0,
            paddingX: 2,
            visibleBatch: "batch1",
            complexData: true,
            elementsConfig: { labelWidth: 150 },
            autowidth: true,
            autoheight:true,
            elements: [{ //R1
                id: "r1",
                visibleBatch: "hd",
                //padding: {left:150},
                cols: [
                    { id: "stms:form:sname", name: "sname", label: _("sname"), view: "text", attributes: { maxlength: 50 }, required: true, disabled: true},
                    { id: "stms:form:taxoname", name: "taxoname", label: _("taxoname"), view: "text", type: "email", attributes: { maxlength: 50 }, required: true, disabled: true},
                    { id: "stms:form:id", name: "type", label: _("form"), value: "01/ĐKTĐ-HĐĐT", disabled: true, view: "text", attributes: { maxlength: 15 }, required: true},

                ]
            },
            { //R2
                id: "r3",
                visibleBatch: "hd",
                //padding: {left:150},
                cols: [
                    { id: "stms:form:stax", name: "stax", label: _("stax"), view: "text", attributes: { maxlength: 400 }, required: true, disabled: true},
                    { id: "stms:form:place", name: "place", label: _("place"), view: "text", attributes: { maxlength: 50 }, required: true},
                    { id: "stms:form:name", name: "formname", label: _("formname"), disabled: true, view: "text", attributes: { maxlength: 100 }, required: true},
                //    { id: "stms:form:taxo", name: "taxo", label: "taxo", view: "text", type: "text", attributes: { maxlength: 50 }, required: true, disabled: true},
                ]
            },
            { //R3
                id: "r3",
                visibleBatch: "hd",
                //padding: {left:150},
                cols: [
                    { id: "stms:form:contactname", name: "contactname", label: _("contact"),view: "text", attributes: { maxlength: 50 }, required: true},
                    { id: "stms:form:contactaddr", name: "contactaddr", label: _("contactaddr"), view: "text", attributes: { maxlength: 400 }, required: true},
                    { id: "stms:form:regtype", name: "regtype", label: _("regtype"), view: "richselect", options: REGTYPE(this.app), value: 2, required: true}

                ]
            },
            { //R4
                id: "r4",
                visibleBatch: "hd",
                cols: [
                    { id: "stms:form:phone", name: "contactphone", label: _("phone"), view: "text", attributes: { maxlength: 20 }, tooltip: _("phone"), required: true},
                    { id: "stms:form:email", name: "contactemail", label: "Email", view: "text", type: "email", attributes: { maxlength: 50 }, required: true},
                    { id: "stms:form:createdt", name: "createdt", label: _("createdt"), view: "datepicker", format: webix.i18n.dateFormatStr, required: true},
                ]
            },
            {
                id: "r5",

                cols: [
                    { id: "stms:form:hascode", css:"header", labelWidth: 320,name: "hascode", label: _("hascode1"), view: "richselect", options: HASCODELIST(this.app), required: true, placeholder: _("hascode"), tooltip: _("hascode")},
                ]
            },
            {
                id: "r6",

                cols: [
                    { id: "stms:form:sendtype", css:"header", labelWidth: 320,name: "sendtype", tooltip: _("sendtypestm"), label: _("sendtypestm1"), view: "richselect", options: [{id:"-1", value:""}, {id: "0", value:"Thông qua tổ chức cung cấp dịch vụ hóa đơn điện tử (điểm b2, khoản 3, Điều 22 của Nghị định)"}], value:"0", disabled:true, required: true},

                ]
            },
            {
                id: "r7",
                cols: [
                    { id: "stms:form:dtsendtype1", css:"header", labelWidth: 320,name: "dtsendtype1", label: _("dtsendtype1"), view: "richselect",  options: DTSENDTYPE(this.app), value: "sendinv",required: true, placeholder: _("dtsendtype"), tooltip: _("dtsendtype"), disabled:true, hidden:false},
                    { id: "stms:form:dtsendtype2", css:"header", labelWidth: 320,name: "dtsendtype2", label: _("dtsendtype1"), view: "multiselect",  options: DTSENDTYPE(this.app), required: true, placeholder: _("dtsendtype"), tooltip: _("dtsendtype"),hidden: true},
                ]
            },
            {
                id: "r8",
                cols: [
                    { id: "stms:form:invtype", css:"header", labelWidth: 320,name: "invtype", label: _("invtypestm1"), view: "multiselect", suggest: {data:FITYPE(this.app), fitMaster:false}, required: true, placeholder: _("invtypestm"), tooltip: _("invtypestm") },
                ]
            },
             {
                 id: "r9",
                 //paddingX: 130,
                 font: 13,
                 css:"bold",
                 cols:[
                        {view:'label', label:(_("list_cert1")+"<span style='color:red'> *</span>")},
                 ]
             },
                grid,

            {
                id: "r12",
                cols: [
                    {
                        gravity: 2,
                        cols: [
                            {},
                            { id: "stms:btnaddcert", view: "button", type: "icon", icon: "mdi mdi-layers-plus", label: _("add_cert"), width: 100, hidden: true },
                            { id: "stms:btnsave", view: "button", type: "icon", icon: "mdi mdi-content-save", label: _("save"), width: 80 },
                            // { id: "stms:btnview", view: "button", type: "icon", icon: "mdi mdi-book-open-variant", label: _("view"), width: 80 },
                            { id: "stms:btnclose", view: "button", type: "icon", icon: "mdi mdi-exit-to-app", label: _("exit"), width: 80 }
                        ]
                    }
                ]
            }
            ],
            rules: {
                type: webix.rules.isNotEmpty,
                formname: webix.rules.isNotEmpty,
                regtype: webix.rules.isNotEmpty,
                contactname: webix.rules.isNotEmpty,
                contactaddr: webix.rules.isNotEmpty,
                contactemail: webix.rules.isEmail,
                contactphone: webix.rules.isNumber,
                place: webix.rules.isNotEmpty,
                createdt: webix.rules.isNotEmpty,
                hascode: webix.rules.isNotEmpty,
                invtype: webix.rules.isNotEmpty,
                place:function(value){ 
                    return value.replace(/\s/g, '').length > 0;  
                 },
                 contactaddr:function(value){ 
                    return value.replace(/\s/g, '').length > 0;  
                 },
                 contactname:function(value){ 
                    return value.replace(/\s/g, '').length > 0;
                 }
            },
            on: {
                onAfterValidation: (result) => {
                    if (!result) webix.message(_("required_msg"), "info", expire)
                }
            }
        }
        return form
    }
    urlChange() {
        let me = this, grid = $$("stms:grid"), form = $$("stms:form")
        me.id = me.getParam("id")
        action = me.getParam("action")
        parse = true
        let json
        webix.ajax(`api/stm/${me.id}`).then(data => {
            json = data.json().doc
            if (json.hascode == 1){
                // json.sendtype1 = json.sendtype
                json.dtsendtype1 =json.dtsendtype
            }
            else{
                // json.sendtype2 = json.sendtype
                json.dtsendtype2 =json.dtsendtype
                
            }
            type = json.type
            //json.items.map(e => { e.vrn = vrt2vrn(e.vrt); e.type = e.type || ""; return e })
        })
        .then(() => {
            form.parse(json)
            // $$("stms:form:sendtype").setValue(0)
            grid.parse(json.items)
            if (action == "copy"){ 
                $$("stms:form:createdt").setValue(new Date())
                $$("stms:form:regtype").enable()
            }
            if (json.status == 1) $$("stms:btndel").show()
        })
    }

    ready() {

        if (this.id) setTimeout(() => { parse = false }, 5000)
    }
    init() {
        this.Iwin = this.ui(Iwin)
        const session = webix.storage.session.get("session")
        $$("stms:form:name").setValue(_("reg_chan_info_inv"))
        $$("stms:form:sname").setValue(session.on)
        $$("stms:form:stax").setValue(session.org.ot)
        $$("stms:form:taxoname").setValue(session.org.taxoname)
        let grid = $$("stms:grid"), form = $$("stms:form")
        webix.extend(grid, webix.ProgressBar)
        $$("stms:form:hascode").attachEvent("onChange", newv => {
            if (newv == 1) {
                $$("stms:form:dtsendtype1").show()
                $$("stms:form:dtsendtype2").hide()
                $$("stms:form:dtsendtype1").setValue("sendinv")
                $$("stms:form:dtsendtype2").setValue()
                $$("stms:form:sendtype").setValue("-1")
            }
            else {
                $$("stms:form:dtsendtype1").hide()
                $$("stms:form:dtsendtype2").show()
                $$("stms:form:dtsendtype1").setValue()
                $$("stms:form:dtsendtype2").setValue()
                $$("stms:form:sendtype").setValue("0")
            }
        })

        // $$("stms:btnview").attachEvent("onItemClick", () => {
        //     try {
        //         if (!form.validate()) return
        //         let inv = createInv()
        //         inv.status = 1
        //         inv.sec = ""
        //
        //         grid.showProgress()
        //         webix.delay(() => {
        //             webix.ajax().post("api/inv/pdf/view", inv).then(result => {
        //                 const json = result.json()
        //                 const req = createRequest(json)
        //                 jsreport.renderAsync(req).then(res => {
        //                     $$("iwin:ipdf").define("src", res.toObjectURL())
        //                     $$("iwin:win").show()
        //                 }).finally(() => grid.hideProgress())
        //             })
        //         })
        //     } catch (err) {
        //         webix.message(err.message, "error", expire)
        //     }
        // })

        $$("stms:btnclose").attachEvent("onItemClick", () => {
            webix.confirm(_("exit_confirm"), "confirm-error").then(() => {
                if (this.id) this.show("/top/reg.searchstm")
                else this.show("/top/home")
            })
        })

        const createInv = () => {
            let del = []
            grid.eachRow(id => {
                const row = grid.getItem(id)
                if (!row.sign_org) del.push(id)
            })
            if (del.length > 0) grid.remove(del)
            if (!grid.validate()) 
            {   
                throw new Error(_("details_data_required")),
                webix.message(_("required_msg"), "info", expire)
            }
            let items = grid.serialize()
            //kiểm tra trùng cks
            let valueArr = items.map(function(i){ 
                let item =JSON.stringify({ "sign_org": i.sign_org, "sign_seri": i.sign_seri, "sign_from": i.sign_from, "sign_to": i.sign_to, "action": i.action })
                return item
             });
            let isDuplicate = valueArr.some(function(item, idx){ 
                return valueArr.indexOf(item) != idx 
            });
            if (isDuplicate){
                throw new Error(_("details_required")),
                webix.message(_("sign_check_msg"), "info", expire)
            }

            if (!(Array.isArray(items) && items.length > 0)) 
            {   
                throw new Error(_("details_required")),
                webix.message(_("required_msg"), "info", expire)
            }
            let inv = form.getValues()
            inv.items = items
            
            return inv
        }

        const save = () => {
            let inv = createInv()
            const me = this,
                id = me.id
                if (action == "view"){
                    inv.action = "update"
                    inv.id = id
                }
                if (action =="copy"){
                    inv.action = "create"
                }    
                // webix.ajax().post("api/stm/hascode", inv).then(result => {
                    webix.ajax().post("api/stm", inv).then(result => {
                        let obj = result.json()
                        if (obj.affectedRows == 1) {
                            webix.message(_("stm_saved"), "success", expire)
                            grid.clearAll()
                            form.clear()
                            new_form()
                        }
                    })
                // })
                // }
            // }
        }
        $$("stms:btnsave").attachEvent("onItemClick", () => {
            webix.confirm(_("stm_save_confirm"), "confirm-warning").then(() => {
                if (!form.validate()) return
                else save()
            })
        })

        $$("stms:btnaddcert").attachEvent("onItemClick", () => {
            $$("stms:grid").showProgress()
            webix.delay(() => {
                webix.ajax().get(`${USB}certs`).then(result => {
                    let rows = result.json(), addNum = 0
                    for (let row of rows) {
                        $$("stms:grid").add({
                            line: $$("stms:grid").serialize().length+1,
                            sign_org: row.cn,
                            sign_seri: row.serialNumber,
                            sign_from: row.fd,
                            sign_to: row.td,
                        })
                        addNum++
                    }
                    if (addNum) webix.message(`${_("add_cert")} ${_("success")}: ${addNum}`, "success", expire)
                }).catch(e => {
                    webix.message(`${_("add_cert")} ${_("fail")}`, "error", expire)
                    webix.alert({
                        title: _("cts_info"),
                        ok: _("done"),
                        text: _("clicked") +
                            ` <a style="color:blue !important;" href="https://drive.google.com/drive/folders/1rhRJlJImcKR-s3N1bFNEbmc4YRio2kkW" target="_blank"><span>${_("here")}</span></a> ` + _("to_down_newest")
                    })
                }).finally(e=>{
                    $$("stms:grid").hideProgress()
                })
            })

        })


        grid.data.attachEvent("onStoreUpdated", (id, obj, mode) => {
            // -- Duy sửa tính lại tiền khi sửa thông tin ko liên quan tiền ( END )
            if (mode == "add" || mode == "delete" || mode == "update") {
                grid.data.each((row, i) => {
                    row.line = i + 1
                })
            }
        })

        // webix.UIManager.addHotKey("enter", view => {
        //     const pos = view.getSelectedId()
        //     view.edit(pos)
        // }, grid)
        //
        //
        //
        // webix.UIManager.addHotKey("Ctrl+enter", () => {
        //     const obj = { quantity: 1, price: 0, unit: "" }
        //     if (isvat) {
        //         obj.vrt = "10"
        //         if (SCT) obj.srt = "0"
        //     }
        //     if (SC) obj.src = "0"
        //     grid.add(obj)
        //     grid.clearValidation()
        // })


    }
}