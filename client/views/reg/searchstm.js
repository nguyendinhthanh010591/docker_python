import { JetView } from "webix-jet"
import {
    LinkedInputs2
    , expire, size
    , filter
    , d2s
    , t2s
    , gridif
    , HASCODELIST
    , SENDTYPE
    , ISTATUSTVAN
    , STMSTATUS
    , CQTSTATUS
    , ISTATUS
    , FITYPE
    , REGTYPE
    , DTSENDTYPE2
    , SIGN_TYPE
    , ROLE, STATUS, CARR, bearer, RPP, getAppLang, MAILSTATUS, checkUsbVersion, STATEMENT_CQT_STATUS
    , callCloudTvanApi
    , OU
    , ENT
} from "models/util"
import Mwin from "views/inv/mwin"
import Ewin from "views/inv/ewin"
import ViewStm from "views/reg/viewstm"
import FilterPlusWin from "views/inv/winfilterplus"
const format = require("xml-formatter")

let b64, cxls, jmail, row
// const configs = (type) => {
//     webix.ajax("api/cus", { type: type }).then(result => {
//         let cols = result.json(), len = cols.length, grid = $$("stms:grid")
//         let arr = grid.config.columns.filter(item => !CARR.includes(item.id))
//         cxls = []
//         if (len > 0) {
//             for (const col of cols) {
//                 col.tooltip = col.label
//                 if (col.inputAlign == "right") {
//                     arr.push({ id: col.id, header: { text: col.label, css: "header" }, adjust: true, css: "right", format: gridna.format })
//                     cxls.push({ id: col.id, header: col.label, exportType: "number", exportFormat: gridna.format })
//                     //arr.push({ id: col.id, header: { text: col.label, css: "header" }, adjust: true, css: "right", format: FGNA })
//                     //cxls.push({ id: col.id, header: col.label, exportType: "number", exportFormat: FGNA })
//                 }
//                 else {
//                     arr.push({ id: col.id, header: { text: col.label, css: "header" }, adjust: true })
//                     cxls.push({ id: col.id, header: col.label })
//                 }
//             }
//         }
//         grid.config.columns = arr
//         grid.refreshColumns()
//         let r4 = $$("stms:r4")
//         r4.removeView("stms:r5")
//         r4.removeView("stms:r6")
//         if (len > 0) {
//             if (len <= 5) {
//                 while (len < 5) {
//                     cols.push({})
//                     len++
//                 }
//                 r4.addView({ id: "stms:r5", cols: cols }, 0)
//                 r4.addView({ id: "stms:r6", hidden: true }, 1)
//             }
//             else {
//                 while (len < 10) {
//                     cols.push({})
//                     len++
//                 }
//                 r4.addView({ id: "stms:r5", cols: cols.splice(0, len / 2) }, 0)
//                 r4.addView({ id: "stms:r6", cols: cols }, 1)
//             }
//         }
//         else {
//             r4.addView({ id: "stms:r5", hidden: true }, 0)
//             r4.addView({ id: "stms:r6", hidden: true }, 1)
//         }
//     })
//
// }

const viewhd = (id) => {
    const grid = $$("stms:grid")
    grid.showProgress()
    webix.delay(() => {
        webix.ajax(`api/stm/htm/${id}`).then(result => {
            let pdf = result.json()
            // let lengthPDF = pdf.sizePDF
            b64=pdf.pdf
            const blob = dataURItoBlob(b64);
            var temp_url = window.URL.createObjectURL(blob);
            $$("viewstm:ipdf").define("src", temp_url)
            $$("viewstm:win").show()
            $$("viewstm:btnapp").hide()
            const status = pdf.status
            if (status == 2 && pdf.cqtstatus == 5) $$("viewstm:btnxml").enable()
            else $$("viewstm:btnxml").disable()
        }).finally(() => grid.hideProgress())
    })
}

const viewNoticeTct = (id, fn) => {
    const grid = $$("stms:grid")
    let selectedItem = $$("stms:grid").getSelectedItem()
    grid.showProgress()
    webix.delay(() => {
        webix.ajax(`api/stm/htmtct/${id}`, { status: selectedItem.cqtstatus, fn: fn }).then(result => {
            let pdf = result.json()
            b64 = pdf.pdf
            const blob = dataURItoBlob(b64);
            var temp_url = window.URL.createObjectURL(blob);
            $$("viewstm:ipdf").define("src", temp_url)
            $$("viewstm:win").show()
        }).finally(() => grid.hideProgress())
    })
}

export default class StmsView extends JetView {
    config() {
        _ = this.app.getService("locale")._
        webix.protoUI({ name: "dependent", $cssName: "richselect" }, LinkedInputs2, webix.ui.richselect)
        webix.proxy.stms = {
            $proxy: true,
            load: function (view, params) {
                let obj = { ...$$("stms:form").getValues(), ...$$("winfilterplus:win:form").getValues() }
                Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                if (!params) params = {}
                if (obj.seq && `${obj.seq}`.includes("-")) {
                    obj.seqrange = obj.seq.split("-").sort()
                    delete obj.seq
                }
                params.filter = obj
                return webix.ajax(this.source, params)
            }
        }

        let pager = { view: "pager", width: 340, id: "stms:pager", size: size, template: `{common.first()}{common.prev()} {common.pages()}{common.next()}{common.last()}` }

        let grid = {
            view: "datatable",
            id: "stms:grid",
            select: "row",
            multiselect: false,
            resizeColumn: true,
            pager: "stms:pager",
            onContext: {
                "webix_view": function (e, id) {
                    if (id && id.row) this.select(id)
                }
            },
            hover: "hover",
            headerRowHeight: 33,
            tooltip: true,
            columns: [
                { header: { text: "", css: "header" }, width: 35, template: `<span class='webix_icon wxi-search', title='${_("view")}'></span>` },
                {
                    header: { text: "", css: "header" }, width: 65, template: obj => {
                        let _result = ""
                    // != hết hiệu lực
                    // if (obj.cqtstatus!=5) {
                        // Tu choi tiep nhan
                        if (obj.cqtstatus==4) _result += `<span class='mdi mdi-sticker', title='${_("view_notice")} ${_("de_received")}'></span>`
                        else if (obj.cqtstatus > 2) {
                            //Tiep nhan (hiện có 5 trạng thái) -> ["1","3","4","5"].includes(`${obj.cqtstatus}`) 
                            _result  += `<span class='mdi mdi-account-box-outline', title='${_("view_notice")} ${_("received")}'></span>`
                            //Duyet or KO Duyet -> ["3","4","5"].includes(`${obj.cqtstatus}`)
                            if (obj.cqtstatus > 4) {
                                if (obj.cqtstatus==5 || obj.cqtstatus==7) _result  += `<span style="width:15px;">&nbsp;&nbsp;</span><span class='mdi mdi-checkbox-multiple-marked', title='${_("view_notice")} ${_("approved")}'></span>`
                                if (obj.cqtstatus==6) _result  += `<span style="width:15px;">&nbsp;&nbsp;</span><span class='mdi mdi-account-remove', title='${_("view_notice")} ${_("declined")}'></span>`
                            }
                        }
                    // }
                    return _result
                    }
                },
                { header: "", width: 35, template: obj => { return obj.status <= 1 ? `<span class='webix_icon wxi-pencil', title='${_("modify")}'></span>` : "" } },
                //{ id: "chkPrints", header: { content: "masterCheckbox", css: "center" }, width: 35, hidden: true, css: "center", template: (obj, common, value, config) => { return [3, 4].includes(obj.status) ? common.checkbox(obj, common, value, config) : "" } },
                { id: "id", header: { text: "ID", css: "header" }, css: "right", sort: "int", adjust: true, cssFormat: (cell, row) => { if (row.api == 0) return { "color": "red" } } },
                // { id: "createdt", header: { text: "Ngày đăng ký", css: "header" }, sort: "server", adjust: true, format: d2s },
                { id: "createdt", header: { text: _("createdt"), css: "header" }, sort: "server", adjust: true, format: d2s,width: 140  },
                { id: "status", header: { text: _("status"), css: "header" }, sort: "server", collection: STMSTATUS(this.app), adjust: true },
                { id: "cqtstatus", header: { text: _("cqtstatus"), css: "header" }, sort: "server", collection: STATEMENT_CQT_STATUS(this.app), adjust: true },
                { id: "vdt", header: { text: _("vdt"), css: "header" }, sort: "server", adjust: true },
                { id: "sname", header: { text: _("sname"), css: "header" }, sort: "server", adjust: true, },
                { id: "stax", header: { text: _("stax"), css: "header" }, sort: "server", adjust: true },
                { id: "taxoname", header: { text: _("taxoname"), css: "header" }, sort: "server", adjust: true, },
                { id: "regtype", header: { text: _("regtype"), css: "header" }, sort: "server", adjust: true, collection: REGTYPE(this.app) },
                { id: "hascode", header: { text: _("hascode"), css: "header" }, sort: "server", adjust: true, collection: HASCODELIST(this.app), },
                { id: "sendtype", header: { text: _("sendtypestm"), css: "header" }, sort: "server", adjust: true, collection: SENDTYPE(this.app), },
                { id: "dtsendtype", header: { text: _("dtsendtype"), css: "header" }, width: 300, sort: "server", adjust: true, collection: DTSENDTYPE2(this.app) },
                { id: "uc", header: { text: _("uc"), css: "header" }, sort: "server", adjust: true },
                //{ id: "invtype", header: { text: _("invtypestm"), css: "header" }, sort: "server", adjust: true, collection:FITYPE(this.app) },
            ],
            onClick:
            {
                "wxi-download": function (e, id) {
                    let obj = { ...$$("stms:form").getValues(), ...$$("winfilterplus:win:form").getValues() }
                    Object.keys(obj).forEach(k => (!obj[k] || obj[k] == "*") && delete obj[k])
                    webix.ajax("api/sea/xls", { filter: obj }).then(result => {
                        let result_ = result.json()
                        result_.map(el => {

                            if (el.statustvan == 3 & el.messres == null) {
                                if (el.messsendcode == 500) {
                                    var str = "Không kết nối được với TVAN"
                                    el.description = str
                                } else {
                                    var str = el.messsend
                                    el.description = str
                                }
                            }
                            else if (el.statustvan == 3) {
                                var str = el.messres
                                var strr = str.replace("TVan's response:", "")
                                var myObj = JSON.parse(strr);
                                str = myObj.tenTBao
                                el.description = str
                            } else {
                                var str = ""
                                el.description = str
                            }
                            var strr = str.replace("TVan's response:", "")
                            el.description = strr

                            el.status = ISTATUS(this.app).find(e => e.id == el.status).value
                            el.maildt = MAILSTATUS(this.app).find(e => e.id == el.maildt).value
                            el.statustvan = ISTATUSTVAN(this.app).find(e => e.id == el.statustvan).value

                            return el
                        })


                        let data = new webix.DataCollection({ data: result_ }), columns
                        if (!webix.isUndefined(cxls) && cxls && cxls.length) columns = cols.concat(cxls)
                        else columns = cols
                        webix.toExcel(data, { columns: columns }).then(() => { data.destructor() })
                    })
                },

                "wxi-pencil": function (e, id) {
                    webix.storage.session.put("stms:form", $$("stms:form").getValues())
                    const rec = this.getItem(id), adjtyp = rec.adjtyp
                    // const url = adjtyp == 2 ? `inv.adj?id=${id}&action=view` : `inv.inv?id=${id}`
                    // this.$scope.show(url)
                    this.$scope.show(`reg.adjstm?id=${id}&action=view`)
                },
                "wxi-search": function (e, r) {
                    this.select(r)
                    console.log("Row ", r, "--- ID", r.row)

                    viewhd(r.row)
                },
                "mdi-account-box-outline": function (e, r) {
                    // View to khai - tct TIẾP NHẬN
                    this.select(r)
                    viewNoticeTct(r.row, "statement_tct_received")
                },
                "mdi-sticker": function (e, r) {
                    // View to khai - tct TỪ CHỐI TIẾP NHẬN
                    this.select(r)
                    viewNoticeTct(r.row, "statement_tct_received")
                },
                "mdi-checkbox-multiple-marked": function (e, r) {
                    // View to khai - tct PHÊ DUYỆT
                    this.select(r)
                    viewNoticeTct(r.row, "statement_tct_accepted")
                },
                "mdi-account-remove": function (e, r) {
                    // View to khai - tct TỪ CHỐI PHÊ DUYỆT
                    this.select(r)
                    viewNoticeTct(r.row, "statement_tct_accepted")
                },
                "mdi-file-replace-outline": function (e, r) {

                    viewhd(this.getItem(r).pid)
                },
                "mdi-file-replace": function (e, r) {

                    viewhd(this.getItem(r).cid)
                }
            }
        }

        let url_ou = "api/ous/bytokenou"  // tât ca deu load theo chi nhanh
        if (ENT == "sgr") url_ou = "api/ous/byousgr"

        let form = {
            view: "form",
            id: "stms:form",
            padding: 3,
            margin: 3,
            tooltip: {},
            elementsConfig: { labelWidth: 85 },
            elements:
                [
                    { //Row 1
                        cols: [

                            { id: "stms:form:fd", name: "form_from", label: _("fd"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr },
                            { id: "stms:form:td", name: "form_to", label: _("td"), view: "datepicker", stringResult: true, editable: true, format: webix.i18n.dateFormatStr },
                            { id: "stms:ou", name: "ou", label: _("bname"), view: "combo", suggest: { url: url_ou, filter: filter, relative: "bottom" } },
                            {
                                gravity: 2, cols: [
                                    { id: "stms:form:status", name: "status", label: _("status"), view: "combo", options: STMSTATUS(this.app) },
                                    { id: "stms:form:cqtstatus", name: "cqtstatus", labelWidth: 150, label: _("cqtstatus"), view: "combo", options: STATEMENT_CQT_STATUS(this.app), disabled: false },
                                ]
                            },
                            {
                                cols:
                                    [
                                        {},
                                        { view: "button", id: "stms:btnsearch", type: "icon", icon: "wxi-search", label: _("search"), width: 85 },
                                        // { view: "button", id: "stms:btnfilterplus", type: "icon", icon: "mdi mdi-filter-plus-outline", tooltip: _("filter_plus_tip"), width: 33 }
                                    ]
                            },
                        ]
                    },

                    // { //Row 3
                    //     cols: []
                    // },
                    //{ id: "stms:r4", rows: [{ id: "stms:r5", hidden: true }, { id: "stms:r6", hidden: true }] },

                ]
            , rules: {
                form_from: webix.rules.isNotEmpty,
                form_to: webix.rules.isNotEmpty,
                $obj: function (data) {
                    if (data.form_from > data.form_to) {
                        webix.message(_("date_invalid_msg"), "error", expire)
                        return false
                    }
                    return true
                }
            }
        }
        let recpager = { view: "combo", id: "stms:recpager", maxWidth: 70, tooltip: _("row_per_page"), value: 10, options: RPP }
        return {
            paddingX: 2,
            rows: [form, grid,
                {
                    rows: [
                        {
                            cols: [
                                recpager, pager, { id: "countinv", view: "label", align: "left", fillspace: true }, {},
                                { view: "button", id: "stm:btnhistory", type: "icon", popup: "hscqt", icon: "mdi mdi-email-newsletter", label: _("history_cqt"), disabled: true/*, hidden: ROLE.includes(4) ? false : true */, width: 130, },
                                { view: "button", id: "stm:btnsendback", type: "icon", icon: "mdi mdi-email-plus-outline", label: _("sendback_cqt"), disabled: true, width: 100, },
                                { id: "stm:btndelete", view: "button", type: "icon", icon: "mdi mdi-trash-can", label: _("delete"), width: 80, css: "webix_danger", disabled: true/*, hidden: ROLE.includes(21) ? false : true */ },
                                { view: "button", id: "stm:btnapp", type: "icon", icon: "wxi-check", label: _("approve"), width: 70, align: "right", disabled: true/*, hidden: ROLE.includes(22) ? false : true*/ },
                                // { id: "stm:btncancel", view: "button", type: "icon", icon: "mdi mdi-trash-can-outline", label: _("cancel"), width: 80, css: "webix_danger", disabled: true },  // Duy - Ẩn hủy tờ khai
                                { view: "button", id: "stms:btncopy", type: "icon", icon: "mdi mdi-content-copy", label: "Copy"/*, hidden: ROLE.includes(21) ? false : true*/, width: 65 },
                            ]
                        },
                    ]
                }
            ]
        }
    }

    ready() {
        let state = webix.storage.session.get("stms:form")
        if (state) {
            let grid = $$("stms:grid"), form = $$("stms:form")
            form.setValues(state)
            grid.clearAll()
            grid.loadNext($$("stms:recpager").getValue(), 0, null, "stms->api/stg", true)
            webix.storage.session.remove("stms:form")
        }
    }

    init() {
        const hscqt = $$("stm:hscqt")
        this.ViewStm = this.ui(ViewStm)
        this.Mwin = this.ui(Mwin)
        this.Ewin = this.ui(Ewin)
        this.FilterPlusWin = this.ui(FilterPlusWin)
        const all = { id: "*", value: _("all") }
        let date = new Date(), firstDay = date.getFullYear(), grid = $$("stms:grid"), form = $$("stms:form"), content = $$("mail:content"), app = $$("viewstm:win"), recpager = $$("stms:recpager"), mailwin = $$("mail:win"),id
        $$("stms:form:fd").setValue(firstDay)
        $$("stms:form:td").setValue(date)
        $$("stms:form:status").getList().add(all, 0)
        $$("stms:form:status").setValue("*")
        $$("stms:form:cqtstatus").getList().add(all, 0)
        $$("stms:form:cqtstatus").setValue("*")
        $$("stms:form:status").attachEvent("onChange", newv => {
            if (newv == 1 || newv == 3) {
                $$("stms:form:cqtstatus").disable()
                $$("stms:form:cqtstatus").setValue()
            }
            else {
                $$("stms:form:cqtstatus").enable()
                $$("stms:form:cqtstatus").setValue("*")
            }
        })

        if (ENT != "vcm") $$("stms:ou").getList().add(all, 0)
        if (!["sgr", "vcm"].includes(ENT)) $$("stms:ou").getList().add(all, 0)
        
        if (!["hdb", "vcm", "sgr"].includes(ENT)) $$("stms:ou").setValue("*")
        else if (ENT == "sgr") $$("stms:ou").setValue(`${OU}`)
        else $$("stms:ou").setValue(OU)
        

        webix.extend(grid, webix.ProgressBar)
        webix.extend(app, webix.ProgressBar)
        webix.extend(form, webix.ProgressBar)
        webix.extend(mailwin, webix.ProgressBar)

        const search = () => {
            grid.clearAll()
            grid.loadNext($$("stms:recpager").getValue(), 0, null, "stms->api/stg", true)
        }

        const approve = () => {
            let arr = []

            if (!row.id) {
                webix.message(_("stm_must_select"), "error", expire)
                return false
            }
            arr.push(row.id)
            webix.confirm(_("stm_confirm"), "confirm-warning").then(() => {
                grid.showProgress()
                webix.delay(() => {
                    webix.ajax().post("api/sta", { id: arr }).then(result => {
                        const json = result.json()
                        //if (json.hasOwnProperty("mst")) {
                        if (json.SIGN_TYPE == 1) {
                                SELECT_CKS_PROMPT(json.mst).then(values => {
                                    webix.message(_("handling"), "", 3000)
                                    let row = json.arr[0]
                                    row.serial = values.serial
                                    webix.ajax().post(`${USB}FptEsign/xml`, row).then(result => {
                                        let _rows = result.json()
                                        const rows = {incs: row.id, signs: [{incs:row.id,xml:_rows.xml}]} // status=1 va xml signed
                                        webix.ajax().put("api/sta", rows).then(result => {
                                        // callCloudTvanApi(arr, '01/ĐKTĐ-HĐĐT')
                                            webix.message(`${_("approved")} ${result.json()} ${_("statement")}`, "success", expire)
                                            search()
                                            // webix.ajax().post("api/stm/hascode", row).then(()=>{
                                            //     console.log("success")
                                            // //    search()
                                            // })
                                            // webix.storage.session.put("session", {...session, "hascode": row.hascode} )
                                        })
                                    }).catch(e => {
                                    webix.message(_("stm_failed"), "error", expire)
                                })
                            }).finally(() => {
                                grid.hideProgress()
                            })
                        }
                        else {
                           // callCloudTvanApi(arr, '01/ĐKTĐ-HĐĐT')
                            webix.message(`${_("approved")} ${result.json()} ${_("statement")}`, "success", expire)
                            search()
                            // webix.ajax().post("api/stm/hascode", row).then(() => {
                            //     console.log("success")
                            //     // search()
                            // })
                            webix.storage.session.put("session", { ...session, "hascode": row.hascode })
                            console.log("A")
                        }
                    }).finally(() => {
                        grid.hideProgress()
                        // webix.ajax().post("api/stm/hascode", row).then(()=>{
                        //      console.log("success")
                        search()
                        // })
                    })
                })
            })
        }

        // Duy - Ẩn hủy tờ khai
        // const cancel = () => {
        //     if (!row.id) {
        //         webix.message(_("statement_must_select"), "error", expire)
        //         return false
        //     }
        //     row.action = "cancel"
        //     webix.confirm(_("stm_cancel_confirm"), "confirm-warning").then(() => {
        //         grid.showProgress()
        //         webix.delay(() => {
        //             webix.ajax().post("api/stm", row).then(result => {
        //                 webix.message(_("stm_cancel"), "success", expire)
        //                 grid.hideProgress()
        //                 search()
        //             }).catch(e => {
        //                 webix.message(_("stm_cancel_failed"), "error", expire)
        //                 grid.hideProgress()
        //             })
        //         })
        //     })
        // }

        const deleteStm = () => {
            if (!row.id) {
                webix.message(_("statement_must_select"), "error", expire)
                return false
            }
            row.action = "delete"
            webix.confirm(_("stm_delete_confirm"), "confirm-warning").then(() => {
                grid.showProgress()
                webix.delay(() => {
                    webix.ajax().post("api/stm", row).then(result => {
                        webix.message(_("stm_deleted"), "success", expire)
                        grid.hideProgress()
                        search()
                    }).catch(e => {
                        webix.message(_("stm_delete_failed"), "error", expire)
                        grid.hideProgress()
                    })
                })
            })
        }
        $$("viewstm:btnxml").attachEvent("onItemClick", () => {
            webix.ajax(`api/inv/xml/stm/${id}`).then(result => {
                const json = result.json()
                const blob = new Blob([json.xml], { type: "text/xml;charset=utf-8" })
                saveAs(blob)
            })
        })
        $$("viewstm:btnapp").attachEvent("onItemClick", () => {
            if (ROLE.includes(5)) {
                approve()
            }
        })


        $$("stms:btnsearch").attachEvent("onItemClick", () => {
            if (form.validate()) search()
        })
        $$("stm:btnapp").attachEvent("onItemClick", () => { approve() })
        // $$("stm:btncancel").attachEvent("onItemClick", () => { cancel() })  // Duy - Ẩn hủy tờ khai
        $$("stm:btndelete").attachEvent("onItemClick", () => { deleteStm() })

        $$("stms:btncopy").attachEvent("onItemClick", () => {
            webix.confirm(`${_("copy_confirm")} ${row.id} ?`, "confirm-warning").then(() => {
                webix.storage.session.put("stms:form", form.getValues())
                this.show(`reg.adjstm?id=${row.id}&action=copy`)
            })
        })

        const convertData = (arr, transArr = []) => {
            for (let i = 0; i < arr.length; i++) {
                const object = arr[i]
                object.taxc = `${object.taxc || ""}`
                for (let tA of transArr) {
                    object[tA] = _(object[tA])
                }
                switch (object.statusqueue) {
                    case "DB_SEND_SUCCESS":
                        object.statusqueue = _("mail_sent")
                        break;
                    case "DB_SEND_ERROR":
                        object.statusqueue = _("mail_sent_error")
                        break;
                    default:
                        object.statusqueue = `${_("mail_sent_error")} ${object.statusqueue.replace("DB_SEND_ERROR Error: ", "")} `
                        break;
                }
                // object.idfunc = _(object.idfunc)
            }
            return arr
        }

        grid.attachEvent("onBeforeLoad", function () {
            this.showOverlay(_("loading"))
        })

        grid.attachEvent("onAfterLoad", function () {
            let self = this, count = self.count()
            $$("countinv").setValue(`${_("found")} ${count}`)
            if (count) {
                self.hideOverlay()
            }
            else {
                self.showOverlay(_("notfound"))
            }
        })

        grid.attachEvent("onAfterSelect", () => {

            row = grid.getSelectedItem()
            id = row.id
            $$("stms:btncopy").enable()

            const status = row.status, statuscqt= row.cqtstatus
            // if (status == 3) {
            $$("stm:btnhistory").enable()
            if(status==2 && (statuscqt==0 || statuscqt==4 || !statuscqt)) $$("stm:btnsendback").enable()
            // }
            if (status == 1) {
                // $$("stm:btncancel").enable()  // Duy - Ẩn hủy tờ khai
                $$("stm:btndelete").enable()
                $$("stm:btnapp").enable()
            }
        })

        grid.attachEvent("onAfterUnSelect", () => {
            row = null
            $$("stms:btncopy").disable()
            // $$("stm:btncancel").disable()  // Duy - Ẩn hủy tờ khai
            $$("stm:btndelete").disable()
            $$("stm:btnapp").disable()
            $$("stm:btnhistory").disable()
            $$("stm:btnsendback").disable()
        })

        recpager.attachEvent("onChange", () => {
            $$("stms:pager").config.size = recpager.getValue()
            if (form.validate()) search()
        })


        $$("stm:btnhistory").attachEvent("onItemClick", () => {
            hscqt.clearAll()
            $$("stm:hscqt").showOverlay(_("loading"))
            // console.log(`api/stm/hscqt/${row.id}`)
            webix.ajax(`api/stm/hscqt/${row.id}`).then(result => {
                let json = result.json()
                if (json.length) {
                    $$("stm:hscqt").hideOverlay()
                    json.sort((a, b)=> a.id>=b.id) // sort log  từ cũ đến mới
                    hscqt.parse(json)
                }
                else {
                    $$("stm:hscqt").showOverlay(_("notfound"))
                }
            })
        })

        $$("stm:btnsendback").attachEvent("onItemClick", () => {
            webix.confirm(_("stm_sendback_confirm"), "confirm-warning").then(() => {
                webix.ajax(`api/stm/sendback/${row.id}`).then(result => {
                    let json = result.json()
                    if (json.result=1){
                        webix.message(_("sent"), "success", expire)
                    }else {
                        webix.message(_("cqt_sent_fail"), "error", expire)
                    }

                })
            })
        })

    }
}

webix.ui({

    view: "popup",
    id: "viewXml",
    width: 900,
    height: 700,
    margin: 50,
    body: { 
        view:"textarea",id:"stm:viewXml", 
        // value:text,
        height:900 
    }
});

webix.ui({

    view: "popup",
    id: "hscqt",
    width: 950,
    height: 600,
    margin: 50,

    body: {
        view: "datatable",
        id: "stm:hscqt",
        select: "row",
        columns: [
            // { id: "id_ref", header: { text: _("id_ref"), css: "header" } },
            { id: "datetime_trans", header: { type: "datetime", text: _("time"), css: "header" }, adjust: true },
            //{ id: "status", header: { text: _("status_cqt"), css: "header" }, collection: STATEMENT_CQT_STATUS(this.app), adjust: true },
            { id: "description", header: { text: _("description"), css: "header" }, fillspace: true },
            { header: { text:  _("Message_gdt"), css: "header" }, adjust: true, popup: "viewXml", 
                template: obj => {  
                    let _result = "";
                    let r_xml = obj.r_xml;
                    if ( r_xml != null) {
                        _result += `<span class='webix_icon wxi-eye', title='${_("view")}'></span>` ;
                    } 
                    return _result;
                }
            }
        ],
        onClick:
        {
            "wxi-eye": function (e, r) {
                webix.ajax(`api/stm/viewXml/${r.row}`).then(result => {
                    let json = result.json()
                    if (json.length) {
                        var xmlPretty = ''
                        if(json[0].r_xml != null){
                            xmlPretty = format(json[0].r_xml, {
                                indentation: '\t', 
                                filter: (node) => node.type !== 'Comment', 
                                collapseContent: true, 
                                lineSeparator: '\n'
                            });
                        }
                        $$("stm:viewXml").setValue(xmlPretty)
                        var left = (screen.width/2)-(1000/2)
                        var top = (screen.height/2)-(800/2)
                        $$("stm:viewXml").show({
                            x:left, // left offset from the right side
                            y:top // top offset
                        });
                    }
                })
            }
        }
    }
});