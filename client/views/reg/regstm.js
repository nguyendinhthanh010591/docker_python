import { JetView } from "webix-jet"
import { REG_ACT, expire, FITYPE, DTSENDTYPE, REGTYPE, HASCODELIST, SC, ENT } from "models/util"
import Iwin from "views/inv/iwin"

const session = webix.storage.session.get("session")

const new_form = () => {
    const grid = $$("stms:grid"), form = $$("stms:form")
    $$("stms:form:sname").setValue(session.on)
    $$("stms:form:stax").setValue(session.taxc)
    $$("stms:form:taxoname").setValue(session.org.taxoname)
    $$("stms:form:createdt").setValue(new Date())
    $$("stms:form:id").setValue("01/ĐKTĐ-HĐĐT")
}
const today = new Date

webix.editors.editdate = webix.extend({
      render:function(){
          var icon = "<span class='webix_icon wxi-calendar' style='position:absolute; cursor:pointer; top:8px; right:5px;'></span>";
        var node = webix.html.create("div", {
            "class":"webix_dt_editor"
         }, "<input type='text'>"+icon);

          node.childNodes[1].onclick = function(){
         var master = webix.UIManager.getFocus();
            var editor = master.getEditor();

            master.editStop(false);
            var config = master.getColumnConfig(editor.column);
            config.editor = "date";
            master.editCell(editor.row, editor.column);
            config.editor = "editdate";
          }
          return node;
      }
}, webix.editors.text);

webix.editors.$popup.date.body = {view: 'datepicker', placeholder:"dd/mm/yyyy hh:mm:ss", timepicker:true,  editable: true, stringResult: true};

export default class InvView extends JetView {

    config() {
        _ = this.app.getService("locale")._
        webix.editors.$popup.date = {
            view:"popup",
            body:{
                view:"calendar",
                timepicker:true,
                width: 250,
                height:250,
                borderless: true,
            }
        }
        let grid = {
            view: "datatable",
            id: "stms:grid",
            editable: true,
            navigation: true,
            editaction: "click",
            resizeColumn: true,
            clipboard: "custom",
            multiselect: false,
            select: "cell",
            headerRowHeight: 33,
            columns: [
                { id: "del", header: [`<span class='webix_icon wxi-plus' title='${_("details_insert")}'></span>`], template: `<span class='webix_icon wxi-trash' title='${_("details_delete")}'></span>`, width: 35 },
                { id: "line", header: { text: _("sttu"), css: "header" }, css: "right", adjust: true , require:true, },
                { id: "sign_org", header: { text: _("sign_org"), css: "header" }, width: 100, adjust: true, editor: "text" , require:true, },
                { id: "sign_seri", header: { text: _("sign_seri"), css: "header" }, adjust: true, editor: "text", require:true,fillspace: true},
                { id: "sign_from", header: { text: _("sign_from"), css: "header" }, adjust: true, editor: "date",format: webix.i18n.fullDateFormatStr, require:true},
                { id: "sign_to", header: { text: _("sign_to"), css: "header" }, adjust: true,  editor: "date", format: webix.i18n.fullDateFormatStr, require:true},
                { id: "action", header: { text: _("act"), css: "header" }, adjust: true, inputAlign: "right", collection: REG_ACT(this.app), editor: "richselect", require:true },
            ],
            onClick: {
                "wxi-trash": function (e, id) {
                    this.remove(id)
                    return false
                },
                "wxi-plus": function (e, id) {
                    let me = this
                    let obj = {}
                    if (SC) obj.src = "0"
                    me.add(obj)
                    me.clearValidation()
                    return false
                },
            },
            rules: {
                sign_org: webix.rules.isNotEmpty,
                sign_seri: webix.rules.isNotEmpty,
                sign_from: webix.rules.isNotEmpty,
                sign_to: webix.rules.isNotEmpty,
                action: webix.rules.isNotEmpty,
                $obj: function (data) {
                    // data.sign_from.format
                    if (data.sign_from > data.sign_to) {
                        webix.message(_("date_invalid_msg"), "error", expire)
                        return false
                    }
                    return true
                }
            }
        }

        let form = {
            view: "form",
            id: "stms:form",
            padding: 0,
            margin: 0,
            paddingX: 2,
            visibleBatch: "batch1",
            complexData: true,
            elementsConfig: { labelWidth: 150 },
            autowidth: true,
            autoheight:true,
            elements: [{ //R1
                id: "r1",
                visibleBatch: "hd",
                cols: [
                    { id: "stms:form:sname", name: "sname", label: _("sname"), view: "text", attributes: { maxlength: 50 }, required: true, disabled: true},
                    { id: "stms:form:taxoname", name: "taxoname", label: _("taxoname"), view: "text", type: "email", attributes: { maxlength: 50 }, required: true, disabled: true},
                    { id: "stms:form:id", name: "type", label: _("form"), value: "01/ĐKTĐ-HĐĐT", disabled: true, view: "text", attributes: { maxlength: 15 }, required: true},

                ]
            },
            { //R2
                id: "r2",
                visibleBatch: "hd",
                cols: [
                    { id: "stms:form:stax", name: "stax", label: _("stax"), view: "text", attributes: { maxlength: 400 }, required: true, disabled: true},
                    { id: "stms:form:place", name: "place", label: _("place"), view: "text", attributes: { maxlength: 50 }, required: true, placeholder: _("place")},
                    { id: "stms:form:name", name: "formname", label: _("formname"), disabled: true, view: "text", attributes: { maxlength: 100 }, required: true},
                ]
            },
            { //R3
                id: "r3",
                visibleBatch: "hd",
                //padding: {left:150},
                cols: [
                    { id: "stms:form:contactname", name: "contactname", label: _("contact"), view: "text", attributes: { maxlength: 50 }, required: true, placeholder: _("contact")},
                    { id: "stms:form:contactaddr", name: "contactaddr", label: _("contactaddr"), view: "text", attributes: { maxlength: 400 }, required: true, placeholder: _("contactaddr")},
                    { id: "stms:form:regtype", name: "regtype", label: _("regtype"), view: "richselect", options: REGTYPE(this.app), value: 1, required: true}

                ]
            },
            { //R4
                id: "r4",
                visibleBatch: "hd",
                cols: [
                    { id: "stms:form:phone", name: "contactphone", label: _("phone"), view: "text", attributes: { maxlength: 20 }, tooltip: _("phone"), required: true, placeholder: _("phone")},
                    { id: "stms:form:email", name: "contactemail", label: "Email", view: "text", type: "email", attributes: { maxlength: 50 }, required: true, placeholder: "Email"},
                    { id: "stms:form:createdt", name: "createdt", label: _("createdt"), view: "datepicker", format: webix.i18n.dateFormatStr, required: true},
                ]
            },
            {
                id: "r5",

                cols: [
                    { id: "stms:form:hascode", css:"header", labelWidth: 320,name: "hascode", label: _("hascode1"), view: "richselect", options: HASCODELIST(this.app), value: HASCODELIST(this.app)[0], required: true, placeholder: _("hascode"), tooltip: _("hascode")},
                ]
            },
            {
                id: "r6",

                cols: [
                    { id: "stms:form:sendtype", css:"header", labelWidth: 320,name: "sendtype", tooltip: _("sendtypestm"), label: _("sendtypestm1"), view: "richselect", options: [{id:"-1", value:""}, {id: "0", value:"Thông qua tổ chức cung cấp dịch vụ hóa đơn điện tử (điểm b2, khoản 3, Điều 22 của Nghị định)"}], value:"-1", disabled:true, required: true},

                ]
            },
            {
                id: "r7",
                cols: [
                    { id: "stms:form:dtsendtype1", css:"header", labelWidth: 320,name: "dtsendtype1", label: _("dtsendtype1"), view: "richselect",  options: DTSENDTYPE(this.app), value: "sendinv",required: true, placeholder: _("dtsendtype"), tooltip: _("dtsendtype"), disabled:true, hidden:false },
                    { id: "stms:form:dtsendtype2", css:"header", labelWidth: 320,name: "dtsendtype2", label: _("dtsendtype1"), view: "multiselect",  options: DTSENDTYPE(this.app), required: true, placeholder: _("dtsendtype"), tooltip: _("dtsendtype"),hidden:true },
                ]
            },
            {
                id: "r8",
                cols: [
                    { id: "stms:form:invtype", css:"header", labelWidth: 320,name: "invtype", label: _("invtypestm1"), view: "multiselect", suggest: {data:FITYPE(this.app), fitMaster:false}, required: true, placeholder: _("invtypestm"), tooltip: _("invtypestm") },
                ]
            },
             {
                 id: "r9",
                 //paddingX: 130,
                 font: 13,
                 css:"bold",
                 cols:[
                        {view:'label', label:(_("list_cert1")+"<span style='color:red'> *</span>")},
                 ]
             },
                grid,

            {
                id: "r12",
                cols: [
                    {
                        gravity: 2,
                        cols: [
                            {},
                            { id: "stms:btnaddcert", view: "button", type: "icon", icon: "mdi mdi-layers-plus", label: _("add_cert"), width: 100, hidden: true },
                            { id: "stms:btnsave", view: "button", type: "icon", icon: "mdi mdi-content-save", label: _("save"), width: 80 },
                            // { id: "stms:btnview", view: "button", type: "icon", icon: "mdi mdi-book-open-variant", label: _("view"), width: 80 },
                            { id: "stms:btnclose", view: "button", type: "icon", icon: "mdi mdi-exit-to-app", label: _("exit"), width: 80 }
                        ]
                    }
                ]
            }
            ],
            rules: {
                
                type: webix.rules.isNotEmpty,
                formname: webix.rules.isNotEmpty,
                regtype: webix.rules.isNotEmpty,
                contactname: webix.rules.isNotEmpty,
                contactaddr: webix.rules.isNotEmpty,
                contactemail: webix.rules.isEmail,
                contactphone: webix.rules.isNumber,
                place: webix.rules.isNotEmpty,
                createdt: webix.rules.isNotEmpty,
                hascode: webix.rules.isNotEmpty,
                invtype: webix.rules.isNotEmpty,
                place:function(value){ 
                    return value.replace(/\s/g, '').length > 0;  
                 },
                 contactaddr:function(value){ 
                    return value.replace(/\s/g, '').length > 0;  
                 },
                 contactname:function(value){ 
                    return value.replace(/\s/g, '').length > 0;  
                 }
            },
            on: {
                onAfterValidation: (result) => {
                    if (!result) webix.message(_("required_msg"), "info", expire)
                }
            }
        }
        return form
    }
    urlChange() {
        let me = this, grid = $$("stms:grid"), form = $$("stms:form")
        me.id = me.getParam("id")
        new_form()//***
    }
    ready() {

        //         grid.showProgress()
        //         webix.delay(() => {
        //             webix.ajax().post("api/inv/pdf/view", inv).then(result => {
        //                 const json = result.json()
        //                 const req = createRequest(json)
        //                 jsreport.renderAsync(req).then(res => {
        //                     $$("iwin:ipdf").define("src", res.toObjectURL())
        //                     $$("iwin:win").show()
        //                 }).finally(() => grid.hideProgress())
        $$("stms:form").showProgress()
        $$("stms:form").disable()
        webix.delay(() => {
            webix.ajax().get("api/stg/cfgcert").then(result => {
                try {
                    let arr = result.json()
                    $$("stms:grid").clearAll()
                    if (arr.length) {
                        for (let json of arr) {
                            $$("stms:grid").parse([{
                                line:1,
                                sign_org: json.cn,
                                sign_seri: json.serialNumber,
                                sign_from: json.fd,
                                sign_to: json.td,
                            }])
                        }
                    }
                } finally {
                    $$("stms:form").hideProgress()
                    $$("stms:form").enable()
                }
            }).catch(err=>{
                $$("stms:form").hideProgress()
                $$("stms:form").enable()
            })
        })
    }
    init() {
        this.Iwin = this.ui(Iwin)
        $$("stms:form:name").setValue(_("reg_chan_info_inv"))
        $$("stms:form:sname").setValue(session.on)
        $$("stms:form:stax").setValue(session.taxc)
        $$("stms:form:taxoname").setValue(session.org.taxoname)
        $$("stms:form:createdt").setValue(new Date())
        let grid = $$("stms:grid"), form = $$("stms:form")
        webix.extend(grid, webix.ProgressBar)
        webix.extend(form, webix.ProgressBar)
        if(["onprem", "lge"].includes(ENT)) $$("stms:btnaddcert").hide()
        $$("stms:form:hascode").attachEvent("onChange", newv => {
            if (newv == 1) {
                $$("stms:form:dtsendtype1").show()
                $$("stms:form:dtsendtype2").hide()
                $$("stms:form:dtsendtype1").setValue("sendinv")
                $$("stms:form:dtsendtype2").setValue()
                $$("stms:form:sendtype").setValue("-1")
            }
            else {
                $$("stms:form:dtsendtype1").hide()
                $$("stms:form:dtsendtype2").show()
                $$("stms:form:dtsendtype1").setValue()
                $$("stms:form:dtsendtype2").setValue()
                $$("stms:form:sendtype").setValue("0")
            }
        })

        // $$("stms:btnview").attachEvent("onItemClick", () => {
        //     try {
        //         if (!form.validate()) return
        //         let inv = createInv()
        //         inv.status = 1
        //         inv.sec = ""
        //
        //         grid.showProgress()
        //         webix.delay(() => {
        //             webix.ajax().post("api/inv/pdf/view", inv).then(result => {
        //                 const json = result.json()
        //                 const req = createRequest(json)
        //                 jsreport.renderAsync(req).then(res => {
        //                     $$("iwin:ipdf").define("src", res.toObjectURL())
        //                     $$("iwin:win").show()
        //                 }).finally(() => grid.hideProgress())
        //             })
        //         })
        //     } catch (err) {
        //         webix.message(err.message, "error", expire)
        //     }
        // })

        $$("stms:btnclose").attachEvent("onItemClick", () => {
            webix.confirm(_("exit_confirm"), "confirm-error").then(() => {
                this.show("/top/reg.searchstm")
            })
        })

        const createInv = () => {
            let del = []
            grid.eachRow(id => {
                const row = grid.getItem(id)
                if (!row.sign_org) del.push(id)
            })
            if (del.length > 0) grid.remove(del)
            if (!grid.validate()) 
            {   
                throw new Error(_("details_data_required")),
                webix.message(_("required_msg"), "info", expire)
            }
            let items = grid.serialize()
            //kiểm tra trùng cks
            let valueArr = items.map(function(i){ 
                let item =JSON.stringify({ "sign_org": i.sign_org, "sign_seri": i.sign_seri, "sign_from": i.sign_from, "sign_to": i.sign_to, "action": i.action })
                return item
             });
            let isDuplicate = valueArr.some(function(item, idx){ 
                return valueArr.indexOf(item) != idx 
            });
            if (isDuplicate){
                throw new Error(_("details_required")),
                webix.message(_("sign_check_msg"), "info", expire)
            }

            if (!(Array.isArray(items) && items.length > 0)) 
            {   
                throw new Error(_("details_required")),
                webix.message(_("required_msg"), "info", expire)
            }
            let inv = form.getValues()
            inv.items = items
            inv.action = "create"
            return inv
        }

        const save = () => {
            let inv = createInv()
            const me = this,
                id = me.id
                        webix.ajax().post("api/stm", inv).then(result => {
                            let obj = result.json()
                            if (obj.affectedRows == 1) {
                                webix.message(_("stm_saved"), "success", expire)
                                grid.clearAll()
                                form.clear()
                                new_form()
                            }
                        })
                // }
            // }
        }
        $$("stms:btnsave").attachEvent("onItemClick", () => {
            webix.confirm(_("stm_save_confirm"), "confirm-warning").then(() => {
                if (!form.validate()) return
                else save()
            })
        })
        $$("stms:btnaddcert").attachEvent("onItemClick", () => {
            $$("stms:grid").showProgress()
            webix.delay(() => {
                webix.ajax().get(`${USB}certs`).then(result => {
                    let rows = result.json(), addNum = 0
                    for (let row of rows) {
                        $$("stms:grid").add({
                            line: $$("stms:grid").serialize().length+1,
                            sign_org: row.cn,
                            sign_seri: row.serialNumber,
                            sign_from: row.fd,
                            sign_to: row.td,
                        })
                        addNum++
                    }
                    if (addNum) webix.message(`${_("add_cert")} ${_("success")}: ${addNum}`, "success", expire)
                }).catch(e => {
                    webix.message(`${_("add_cert")} ${_("fail")}`, "error", expire)
                    webix.alert({
                        title: _("cts_info"),
                        ok: _("done"),
                        text: _("clicked") +
                            ` <a style="color:blue !important;" href="https://drive.google.com/drive/folders/1rhRJlJImcKR-s3N1bFNEbmc4YRio2kkW" target="_blank"><span>${_("here")}</span></a> ` + _("to_down_newest")
                    })
                }).finally(e=>{
                    $$("stms:grid").hideProgress()
                })
            })

        })

       
        grid.data.attachEvent("onStoreUpdated", (id, obj, mode) => {
            // -- Duy sửa tính lại tiền khi sửa thông tin ko liên quan tiền ( END )
            if (mode == "add" || mode == "delete" || mode == "update") {
                grid.data.each((row, i) => {
                    row.line = i + 1
                })
            }
        })


    }
}