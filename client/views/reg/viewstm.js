import { JetView } from "webix-jet"
import { setConfStaFld, ROLE } from "models/util"
export default class ViewStm extends JetView {
    config() {
        _ = this.app.getService("locale")._
        return {
            view: "window",
            id: "viewstm:win",
            modal: true,
            fullscreen: true,
            head: {
                view: "toolbar"
                , height: 35
                , cols: [
                    { view: "label", label: _("statement_view") },
                    {
                        cols: [
                            { view: "button", id: "viewstm:btnxml", type: "icon", icon: "mdi mdi-file-xml", tooltip: "xml", width: 30, disabled: true },
                            { view: "button", id: "viewstm:btnapp", type: "icon", icon: "wxi-check", tooltip: _("statement_approve"), width: 30 },
                            { view: "button", type: "icon", icon: "wxi-close", hotkey: 'esc', width: 30, click: () => $$("viewstm:win").hide() }
                        ], fillspace: true, align: "right"
                    }
                ]
            },
            body:  { id: "viewstm:ipdf", view: "iframe", borderless: true }
        }
    }
    ready(v, urls) {
        // if (ROLE.includes('PERM_SEARCH_INV_CONVERT_DOWNLOAD_FILE') || ROLE.includes('PERM_SEARCH_INV_VIEW_DOWNLOAD_FILE')) $$("iwin:download").show()
        //Cau hinh rieng cac cot tĩnh
        webix.ajax().get("api/fld/conf", {url:urls[0].page, params:urls[0].params}).then(result => {
            let json = result.json()
            setConfStaFld(json)
        })
        //
    }
    show() {
        this.getRoot().show()
    }
    init() {
        // if (ROLE.includes('PERM_SEARCH_INV_CONVERT_DOWNLOAD_FILE') || ROLE.includes('PERM_SEARCH_INV_VIEW_DOWNLOAD_FILE')) $$("iwin:download").show()
    }
}
