'use strict'
import { JetView } from "webix-jet"
import { bearer, MST, ROLE } from "models/util"
let conf, usb, fn
let pdfDoc = null, pageNum = 1, numPages = 1, zoom = "w", scale = 1, oscale = 1.1, canvas1, canvas2, ctx1, ctx2, url, template, filename, rect = { x: 0, y: 0, w: 150, h: 50 }, drag = false
const mouseUp = () => {
  drag = false
}

const mouseDown = (e) => {
  const bcr = e.target.getBoundingClientRect()
  rect.x = parseInt((e.clientX - bcr.left) / scale)
  rect.y = parseInt((e.clientY - bcr.top) / scale)
  drag = true
}

const mouseMove = (e) => {
  if (drag) {
    const bcr = e.target.getBoundingClientRect()
    const x = parseInt((e.clientX - bcr.left) / scale)
    const y = parseInt((e.clientY - bcr.top) / scale)
    rect.w = x - rect.x;
    rect.h = y - rect.y;
    rect.x2 = x;
    rect.y2 = y;
    ctx2.clearRect(0, 0, canvas2.width, canvas2.height)
    ctx2.strokeStyle = "#FF0000"
    ctx2.setLineDash([2, 2])
    ctx2.strokeRect(rect.x, rect.y, rect.w, rect.h)
  }
}


const renderPage = () => {
  pdfDoc.getPage(pageNum).then(page => {
    if (zoom == "w") {
      scale = template.$width / page.getViewport({ scale: 1 }).width
    }
    else if (zoom == "h") {
      scale = template.$height / page.getViewport({ scale: 1 }).height
    }
    else {
      scale = zoom
    }
    const viewport = page.getViewport({ scale: scale })
    if (oscale !== scale) {
      oscale = scale
      const h = viewport.height, w = viewport.width
      canvas1.height = h
      canvas1.width = w
      canvas2.height = h
      canvas2.width = w
      ctx2.scale(scale, scale)
    }
    page.render({ canvasContext: ctx1, viewport: viewport }).promise.then(() => {
      $$("ds:page").setValue(pageNum)
    })
  })
}

const getDocument = () => {
  pdfjsLib.getDocument(url).promise.then(_pdfDoc => {
    pdfDoc = _pdfDoc
    numPages = pdfDoc.numPages
    renderPage()
    $$("ds:page").define("max", numPages)
    $$("ds:page").refresh()
    $$("ds:pages").setValue(`${numPages} pages`)
    $$("ds:gridpdf").hide()
    $$("ds:resizerpdf").hide()
  }).catch(err => webix.message(err.message))
}

const signpdf1 = async () => {
  const obj = await getcn(MST)
  const param = { x: rect.x, y: rect.y, w: rect.w, h: rect.h, page: pageNum - 1, cn: obj.cn, mst: MST }
  const files = $$("uploaderpdf").files, file_id = files.getFirstId()
  const formData = new FormData()
  formData.append("file", files.getItem(file_id).file)
  formData.append("param", JSON.stringify(param))
  const headers = { "Content-Type": "application/json", "Authorization": bearer() }
  const res1 = await fetch("api/pdf/digest", { method: "POST", body: formData })
  if (!res1.ok) throw new Error(`create digest error : ${res1.status}`)
  const data1 = await res1.json()
  data1.serial = obj.serial
  const res2 = await fetch(`${USB}pdf`, { method: "POST", headers: headers, body: JSON.stringify(data1) })
  if (!res2.ok) throw new Error(`signing error : ${res2.status}`)
  const data2 = await res2.json()
  const res3 = await fetch("api/pdf/sign", { method: "POST", headers: headers, body: JSON.stringify(data2) })
  if (!res3.ok) throw new Error(`update signature error : ${res3.status}`)
  const data3 = await res3.blob()
  url = URL.createObjectURL(data3)
  getDocument()
}

const signpdf2 = async () => {
 
 const param = { x: rect.x, y: rect.y, w: rect.w, h: rect.h, x2: rect.x2, y2: rect.y2, vw: canvas2.width, vh: canvas2.height, scale: scale, page: pageNum - 1, mst: MST }
 const files = $$("uploaderpdf").files, file_id = files.getFirstId()
  const formData = new FormData()
  formData.append("file", files.getItem(file_id).file)
  formData.append("param", JSON.stringify(param))
  const headers = { "Authorization": bearer() }
  const res = await fetch("api/dss/pdf/sign/upload", { method: "POST", headers: headers, body: formData })
  if (!res.ok) throw new Error(`signpdf2 error : ${res.status}`)
  const blob = await res.blob()
  url = URL.createObjectURL(blob)
  getDocument()
}

export default class DsView extends JetView {
  config() {
   // _ = this.app.getService("locale")._
    const toolbar = {
      view: "toolbar", height: 38, cols: [
        { id: "ds:zoom", view: "richselect", value: zoom, options: ZOOMS, width: 120, tooltip: _("zoom") },
        { id: "ds:first", view: "button", type: "icon", icon: "webix_icon wxi-angle-double-left", css: "webix_pager_item", width: 32, tooltip: _("firstpage") },
        { id: "ds:page", view: "counter", step: 1, value: 1, min: 1, width: 100, tooltip: _("goto") },
        { id: "ds:last", view: "button", type: "icon", icon: "webix_icon wxi-angle-double-right", css: "webix_pager_item", width: 32, tooltip: _("lastpage") },
        { id: "ds:pages", view: "label", width: 100 },
        { id: "ds:btnverifypdf", view: "button", type: "icon", icon: "mdi mdi-bookmark-check", label: _("verify"), width: 100, disabled: ROLE.includes('PERM_SIGN_VERIFY') ? false : true },
        { id: "ds:btnsignpdf", view: "button", type: "icon", icon: "mdi mdi-pen", label: _("sign"), width: 100, disabled: ROLE.includes('PERM_SIGN_DIGITAL') ? false : true },
        { id: "ds:btndownload", view: "button", type: "icon", icon: "mdi mdi-download", label: _("download"), width: 100, disabled: ROLE.includes('PERM_SIGN_UPFILE') ? false : true },
        { id: "uploaderpdf", view: "uploader", multiple: false, autosend: false, link: "filelistpdf", accept: "application/pdf", type: "icon", icon: "mdi mdi-file-pdf", label: "PDF File...", width: 100, disabled: ROLE.includes('PERM_SIGN_PDFFILE') ? false : true },
        { id: "filelistpdf", view: "list", type: "uploader", autoheight: true, borderless: true, hidden: true },
        {}
      ]
    }
    const template = { view: "template", id: "ds:template", scroll: "y", gravity: 3, template: "<div style='position: relative;'><canvas id='canvas1' class='canvas'></canvas><canvas id='canvas2' class='canvas' style='position: absolute;left: 0;top: 0;'></canvas></div>" }

    const form = {
      view: "form",
      id: "ds:form",
      padding: 1,
      margin: 1,
      elements: [
        {
          cols: [
            { id: "ds:ref", view: "text", name: "ref", label: "Reference Tag", labelWidth: 100, attributes: { maxlength: 64 }, width: 200, required: true },
            { id: "ds:btnverifyxml", view: "button", type: "icon", icon: "mdi mdi mdi-book-check", label: _("verify"), width: 100 },
            { id: "ds:btnsignxml", view: "button", type: "icon", icon: "mdi mdi-pencil-box-outline", label: _("sign"), width: 100 },
            { id: "uploaderxml", view: "uploader", multiple: false, autosend: false, link: "filelistxml", accept: "application/xml", type: "icon", icon: "mdi mdi-file-find-outline", label: "XML File...", width: 100 },
            { id: "filelistxml", view: "list", type: "uploader", hidden: true },
            {}
          ]
        }
      ],
      rules: {
        ref: webix.rules.isNotEmpty
      }
    }

    const gridpdf = {
      view: "dataview",
      id: "ds:gridpdf",
      select: true,
      hidden: true,
      xCount: 1,
      type: {
        height: "auto",
        width: "auto"
      },
      template: function (obj) {
        const temp = obj.error ? obj.error : `Subject: ${obj.subject}<br>Issuer: ${obj.issuer}<br>From: ${obj.from} To: ${obj.to}<br>Signtime: ${obj.signtime} - Signature: ${obj.signature} - Validity: ${obj.validity}<br>Certificate: ${obj.certificate}`
        return `<span class='webix_strong'>${temp}</span>`
      }
    }

    const gridxml = {
      view: "dataview",
      id: "ds:gridxml",
      select: true,
      hidden: true,
      xCount: 1,
      type: {
        height: "auto",
        width: "auto"
      },
      template: function (obj) {
        const temp = obj.error ? obj.error : `MST: ${obj.mst}<br>Subject: ${obj.subject}<br>Issuer: ${obj.issuer}<br>From: ${obj.fd} To: ${obj.td}<br>Signature: ${obj.signature}<br>Certificate: ${obj.certificate}`
        return `<span class='webix_strong'>${temp}</span>`
      }
    }

   // const codemirror = { view: "codemirror-editor", id: "ds:codemirror", mode: "xml", gravity: 3 }
    const resizerpdf = { id: "ds:resizerpdf", view: "resizer", hidden: true }
   // const resizerxml = { id: "ds:resizerxml", view: "resizer", hidden: true }

    const tabview = {
      view: "tabview",
      id: "ds:tabview",
      tabbar: {
        optionWidth: 150
      },
      cells: [
        { header: `<span class='mdi mdi-file-pdf-box-outline'></span>PDF`, body: { rows: [toolbar, { cols: [template, resizerpdf, gridpdf] }] } }
       
      ]
    }
    return tabview
  }
  ready() {
    template = $$("ds:template")
    canvas1 = document.querySelector("#canvas1")
    ctx1 = canvas1.getContext("2d")
    canvas2 = document.querySelector("#canvas2")
    canvas2.addEventListener("mousedown", mouseDown)
    canvas2.addEventListener("mouseup", mouseUp)
    canvas2.addEventListener("mousemove", mouseMove)
    ctx2 = canvas2.getContext("2d")
    ctx2.clearRect(0, 0, canvas2.width, canvas2.height)
    // $$("ds:codemirror").getEditor(true).then(editor => {
    //   editor.options.readOnly = true
    // })
    setTimeout(() => { parse = false }, 3000)

    webix.ajax("api/dss/conf").then(rs => {
      conf = rs.json()
      const sign = conf.sign
      if(sign !=2){
        alert("Chưa hỗ trợ kiểu ký số đang cấu hình!")
        $$("ds:btnsignpdf").disable()
        $$("ds:btnverifypdf").disable()
      }
     
      usb = sign == 1
     // $$("uploaderxml").config.upload = usb ? `${USB}upload` : "api/dss/xml/sign/upload"
    })
  }
  init() {
    if (!ROLE.includes('PERM_SIGN')) {
      this.show("/top/home")
      window.location.replace(`/${homepage}`)
      return
    }
    const tabview = $$("ds:tabview")
    webix.extend(tabview, webix.ProgressBar)
    
    // $$("uploaderxml").attachEvent("onAfterFileAdd", upload => {
    //   const file = upload.file
    //   fn = file.name.split(".")[0]
    //   const reader = new FileReader()
    //   reader.readAsText(file, "utf-8")
    //   reader.onload = () => { $$("ds:codemirror").setValue(reader.result) }
    //   $$("ds:gridxml").hide()
    //   $$("ds:resizerxml").hide()
    // })

    // $$("ds:btnverifyxml").attachEvent("onItemClick", async () => {
    //   const grid = $$("ds:gridxml")
    //   grid.clearAll()
    //   const files = $$("uploaderxml").files, file_id = files.getFirstId()
    //   if (!file_id) {
    //     webix.message(_("file_required"), "error")
    //     return false
    //   }
    //   const formData = new FormData()
    //   formData.append("file", files.getItem(file_id).file)
    //   const res = await fetch("api/dss/xml/verify/upload", { method: "POST", headers: { "Authorization": bearer() }, body: formData })
    //   if (res.ok) {
    //     const json = await res.json()
    //     $$("ds:resizerxml").show()
    //     grid.show()
    //     grid.parse(json)
    //   }
    //   else {
    //     webix.message(`verifyxml error : ${res.status}`)
    //   }
    // })

    // $$("ds:btnsignxml").attachEvent("onItemClick", () => {
    //   if (!$$("ds:form").validate()) {
    //     webix.message("Reference Tag is required", "error")
    //     return false
    //   }
    //   const files = $$("uploaderxml").files, file_id = files.getFirstId()
    //   if (!file_id) {
    //     webix.message(_("file_required"), "error")
    //     return false
    //   }
    //   const ref = $$("ds:ref").getValue()
    //   tabview.showProgress()
    //   webix.delay(() => {
    //     systime().then(now => {
    //       files.data.each(obj => {
    //         obj.formData = { mst: MST, ref: ref, date: now.getTime() }
    //       })
    //       $$("uploaderxml").send(rs => {
    //         try {
    //           if (rs.err) {
    //             webix.message(rs.err, "error")
    //             return
    //           }
    //           const xml = rs.xml
    //           $$("ds:codemirror").setValue(xml)
    //           const blob = new Blob([xml], { type: "text/xml" })
    //           webix.html.download(blob, `${fn}_signed.xml`)
    //         } catch (err) {
    //           webix.message(err.message, "error")
    //         }
    //       })
    //     }).finally(() => { tabview.hideProgress() })
    //   })
    // })

    $$("ds:btndownload").attachEvent("onItemClick", () => {
      webix.html.download(url, filename)
    })

    $$("ds:first").attachEvent("onItemClick", () => {
      pageNum = 1
      renderPage()
    })

    $$("ds:last").attachEvent("onItemClick", () => {
      pageNum = numPages
      renderPage()
    })

    $$("ds:page").attachEvent("onChange", function (newv, oldv) {
      if (!webix.rules.isNumber(newv) || newv === pageNum) return
      if (newv > numPages) newv = numPages
      if (newv < 1) newv = 1
      pageNum = newv
      renderPage()
    })

    $$("ds:zoom").attachEvent("onChange", function (newv) {
      if (newv === zoom) return
      zoom = newv
      renderPage()
    })

    $$("uploaderpdf").attachEvent("onBeforeFileAdd", obj => {
      const file = obj.file
      filename = file.name
      const reader = new FileReader()
      reader.readAsDataURL(file)
      reader.onload = () => {
        const blob = b642blob(reader.result.substring(28), "application/pdf")
        url = URL.createObjectURL(blob)
        getDocument()
      }
    })

    $$("ds:btnverifypdf").attachEvent("onItemClick", () => {
      const grid = $$("ds:gridpdf")
      grid.clearAll()
      const files = $$("uploaderpdf").files, file_id = files.getFirstId()
      if (!file_id) {
        webix.message(_("file_required"))
        return false
      }
      tabview.showProgress()
      webix.delay(() => {
        const formData = new FormData()
        formData.append("file", files.getItem(file_id).file)
        fetch("api/dss/pdf/verify/upload", { method: "POST", headers: { "Authorization": bearer() }, body: formData }).then(res => res.json()).then(json => {
          $$("ds:resizerpdf").show()
          grid.show()
          grid.parse(json)
        }).catch(err => webix.message(err.message)).finally(() => tabview.hideProgress())
      })
    })


    $$("ds:btnsignpdf").attachEvent("onItemClick", () => {
      const file_id = $$("uploaderpdf").files.getFirstId()
      if (!file_id) {
        webix.message(_("file_required"))
        return false
      }
      tabview.showProgress()
      webix.delay(() => {
        
        if (usb) signpdf1().catch(err => webix.message(err.message)).finally(() => tabview.hideProgress())
        else signpdf2().catch(err => webix.message(err.message)).finally(() => tabview.hideProgress())
      })

    })
  }
}     