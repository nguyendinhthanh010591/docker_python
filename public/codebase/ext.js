"use strict"
let _
webix.env.cdn = window.location.origin + `${homepage?"/"+homepage:""}` + "/codebase"
const helpers = `
	 const hbs = require("handlebars"), helpers = require("handlebars-helpers")({handlebars: hbs}), hbh = require("hbh")
	 hbh.register(hbs)
	 function getPageNumber(idx){
		if (idx==null) return ''
		const page = idx + 1
		if (page>1) return 'Tiếp trang trước - Trang ' + page
		else        return 'Trang ' + page
	}
	function getPageNumbervcm(idx){
		if (idx==null) return ''
		const page = idx + 1
		if (page>1) return 'tiep theo trang truoc - trang ' + page
		else        return ''
	}
	function getTotalPages(pages){
		if (!pages) return ''
		return pages.length
	}
	function lastPage(idx, pages, opts) {
		if (idx === pages-1)  return opts.fn(this)
		return ''
	}
	function getFirstPgHeader(idx, total){
		if(idx==null || idx==0)
			return 'Trang ' + (idx+1) + '/' + total.length
		else
			return ''
	}
	`
const chrome_bgi = { preferCSSPageSize: true, /* marginTop: "2mm", marginBottom: "2mm", marginRight: "2mm", marginLeft: "2mm" */}
function createTemplate(tmp) {
    const sty = tmp.sty
    // , inv = `<style>${sty}</style>${tmp.inv}`
	const ftr = tmp.ftr,rft = tmp.rft ? tmp.rft : ""
	const footer = ftr ? `{{#lastPage @index ../$pdf.pages.length}}${ftr}{{/lastPage}}` : ``
    const tmp_ftr = `
		<style>
			@page {size: A4;}
			* { box-sizing: border-box;}
			html, body { margin: 0px; padding: 0px;}
			.main { display: flex; flex-direction: column; justify-content: space-between; width: 100%; height: 100%;}
			.footer { width: 100%; padding-bottom: 4mm; padding-right: 5mm; font-size: 9pt;text-align: center; }
		</style>
		{{#each $pdf.pages}}
			{{#if @index}}<div style="page-break-before: always;"></div>{{/if}}
			<main class="main">
				<header><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></header> 
				<footer class="footer">${rft}${footer}<div style='font-weight: bold; text-align: right;'>{{getPageNumber @index}}/{{getTotalPages ../$pdf.pages}}</div></footer>
			</main>
		{{/each}}
	`
	const opt_ftr = { type: "merge", mergeWholeDocument: true, mergeToFront: true, template: { content: tmp_ftr, engine: "handlebars", recipe: "chrome-pdf", helpers: helpers } }
	let opts = [opt_ftr], body, chrome_inv
	if (tmp.bgi) {
		const tmp_bgi = `<header><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></header><style> @page {size: A4;} .bgi{position:fixed; top:0; left:0; width:100%; height:100%; ${tmp.bgi}}</style><div class="bgi"></div>`
		const opt_bgi = { type: "merge", template: { content: tmp_bgi, engine: "none", recipe: "chrome-pdf", chrome: chrome_bgi } }
		opts.push(opt_bgi)
	}
	if (tmp.hdr && tmp.dtl) {
		const hdr = `<header><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></header><style>${sty}</style>${tmp.hdr}`
		const chrome_hdr = { preferCSSPageSize: true, marginTop: "7mm", marginBottom: "15mm", marginRight: "7mm", marginLeft: "7mm" }
		const opt_hdr = { type: "merge", mergeToFront: true, template: { content: hdr, engine: "handlebars", recipe: "chrome-pdf", chrome: chrome_hdr } }
		opts.push(opt_hdr)
		body = `<header><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></header> <style>${sty}</style>${tmp.dtl}`
		const title = tmp.title ? tmp.title : "10cm"
		chrome_inv = { preferCSSPageSize: true, marginTop: title, marginBottom: "15mm", marginRight: "7mm", marginLeft: "7mm" }
	}
	else {
		body = `<header><meta content="text/html; charset=utf-8" http-equiv="Content-Type"></header> <style>${sty}</style>${tmp.body}`
		chrome_inv = { preferCSSPageSize: true, marginTop: "7mm", marginBottom: "15mm", marginRight: "7mm", marginLeft: "7mm" }
	}
	const req = { content: body, engine: "handlebars", recipe: "chrome-pdf", helpers: helpers, chrome: chrome_inv, pdfOperations: opts }
	return req
}
function createTemplatePage01GTGT(tmp) {
    const sty = tmp.sty
    // , inv = `<style>${sty}</style>${tmp.inv}`
	const ftr = tmp.ftr,rft = tmp.rft ? tmp.rft : ""
	const footer = ftr ? `{{#lastPage @index ../$pdf.pages.length}}${ftr}{{/lastPage}}` : ``
    const tmp_ftr = `
		<style>
			@page {size: A4;}
			* { box-sizing: border-box;}
			html, body { margin: 0px; padding: 0px;}
			.main { display: flex; flex-direction: column; justify-content: space-between; width: 100%; height: 100%;}
			.footer { width: 100%; padding-bottom: 4mm; padding-right: 5mm; font-size: 9pt;text-align: center; }
			.header { width: 100%; padding-top: 40mm; font-size: 9pt;text-align: center; margin-left: 7mm}
		</style>
		{{#each $pdf.pages}}
			
			{{#if @index}}<div style="page-break-before: always;"></div>{{/if}}
			<main class="main">
				<header class="header">
				
				<div style='text-align: center;'>{{#compare @index ">" 0}}{{getPageNumbervcm @index}}/{{getTotalPages ../$pdf.pages}}{{/compare}}</div>
				</header> 
				<footer class="footer">${rft}${footer}<div style='font-weight: bold; text-align: right;'></div></footer>
			</main>
		{{/each}}
	`
	const opt_ftr = { type: "merge", mergeWholeDocument: true, mergeToFront: true, template: { content: tmp_ftr, engine: "handlebars", recipe: "chrome-pdf", helpers: helpers } }
	let opts = [opt_ftr], body, chrome_inv
	if (tmp.bgi) {
		const tmp_bgi = `<style> @page {size: A4;} .bgi{position:fixed; top:0; left:0; width:100%; height:100%; ${tmp.bgi}}</style><div class="bgi"></div>`
		const opt_bgi = { type: "merge", template: { content: tmp_bgi, engine: "none", recipe: "chrome-pdf", chrome: chrome_bgi } }
		opts.push(opt_bgi)
	}
	if (tmp.hdr && tmp.dtl) {
		const hdr = `<style>${sty}</style>${tmp.hdr}`
		const chrome_hdr = { preferCSSPageSize: true, marginTop: "7mm", marginBottom: "15mm", marginRight: "7mm", marginLeft: "7mm" }
		const opt_hdr = { type: "merge", mergeToFront: true, template: { content: hdr, engine: "handlebars", recipe: "chrome-pdf", chrome: chrome_hdr, helpers: helpers } }
		opts.push(opt_hdr)
		body = `<style>${sty}</style>${tmp.dtl}`
		const title = tmp.title ? tmp.title : "10cm"
		chrome_inv = { preferCSSPageSize: true, marginTop: title, marginBottom: "15mm", marginRight: "7mm", marginLeft: "7mm" }
	}
	else {
		body = `<style>${sty}</style>${tmp.body}`
		chrome_inv = { preferCSSPageSize: true, marginTop: "7mm", marginBottom: "15mm", marginRight: "7mm", marginLeft: "7mm" }
	}
	const req = { content: body, engine: "handlebars", recipe: "chrome-pdf", helpers: helpers, chrome: chrome_inv, pdfOperations: opts }
	return req
}
function createTemplatePagePXK(tmp) {
    const sty = tmp.sty
    // , inv = `<style>${sty}</style>${tmp.inv}`
	const ftr = tmp.ftr,rft = tmp.rft ? tmp.rft : ""
	const footer = ftr ? `{{#lastPage @index ../$pdf.pages.length}}${ftr}{{/lastPage}}` : ``
    const tmp_ftr = `
		<style>
			@page {size: A4;}
			* { box-sizing: border-box;}
			html, body { margin: 0px; padding: 0px;}
			.main { display: flex; flex-direction: column; justify-content: space-between; width: 100%; height: 100%;}
			.footer { width: 100%; padding-bottom: 4mm; padding-right: 5mm; font-size: 9pt;text-align: center; }
			.header { width: 100%; padding-top: 57mm; font-size: 9pt;text-align: center; margin-left: 0mm}
		</style>
		{{#each $pdf.pages}}
			
			{{#if @index}}<div style="page-break-before: always;"></div>{{/if}}
			<main class="main">
				<header class="header">
				<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
				<div style='text-align: center;'>{{#compare @index ">" 0}}{{getPageNumbervcm @index}}/{{getTotalPages ../$pdf.pages}}{{/compare}}</div>
				</header> 
				<footer class="footer">${rft}${footer}<div style='font-weight: bold; text-align: right;'></div></footer>
			</main>
		{{/each}}
	`
	const opt_ftr = { type: "merge", mergeWholeDocument: true, mergeToFront: true, template: { content: tmp_ftr, engine: "handlebars", recipe: "chrome-pdf", helpers: helpers } }
	let opts = [opt_ftr], body, chrome_inv
	if (tmp.bgi) {
		const tmp_bgi = `<style> @page {size: A4;} .bgi{position:fixed; top:0; left:0; width:100%; height:100%; ${tmp.bgi}}</style><div class="bgi"></div>`
		const opt_bgi = { type: "merge", template: { content: tmp_bgi, engine: "none", recipe: "chrome-pdf", chrome: chrome_bgi } }
		opts.push(opt_bgi)
	}
	if (tmp.hdr && tmp.dtl) {
		const hdr = `<style>${sty}</style>${tmp.hdr}`
		const chrome_hdr = { preferCSSPageSize: true, marginTop: "7mm", marginBottom: "15mm", marginRight: "7mm", marginLeft: "7mm" }
		const opt_hdr = { type: "merge", mergeToFront: true, template: { content: hdr, engine: "handlebars", recipe: "chrome-pdf", chrome: chrome_hdr, helpers: helpers } }
		opts.push(opt_hdr)
		body = `<style>${sty}</style>${tmp.dtl}`
		const title = tmp.title ? tmp.title : "10cm"
		chrome_inv = { preferCSSPageSize: true, marginTop: title, marginBottom: "15mm", marginRight: "7mm", marginLeft: "7mm" }
	}
	else {
		body = `<style>${sty}</style>${tmp.body}`
		chrome_inv = { preferCSSPageSize: true, marginTop: "7mm", marginBottom: "15mm", marginRight: "7mm", marginLeft: "7mm" }
	}
	const req = { content: body, engine: "handlebars", recipe: "chrome-pdf", helpers: helpers, chrome: chrome_inv, pdfOperations: opts }
	return req
}
function createRequest(json) {
	if(json.doc.ent=='vcm'){
		if(json.doc.type == '03XKNB')
		return { data: json.doc, template: createTemplatePagePXK(json.tmp) }
		else return { data: json.doc, template: createTemplatePage01GTGT(json.tmp) }
	}else{
		return { data: json.doc, template: createTemplate(json.tmp) }
	}
	
}
function createTemplateIf(tmp,ent,type) {
	if(ent=='vcm'){
		if(type == '03XKNB')
		return createTemplatePagePXK(tmp)
		else return  createTemplatePage01GTGT(tmp)
	}else{
		return createTemplate(tmp)
	}
	
}
const sliceSize = 512
const b642blob = (b64Data, contentType = '') => {
	const byteCharacters = atob(b64Data)
	const byteArrays = []
	for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
		const slice = byteCharacters.slice(offset, offset + sliceSize)
		const byteNumbers = new Array(slice.length)
		for (let i = 0; i < slice.length; i++) {
			byteNumbers[i] = slice.charCodeAt(i)
		}
		const byteArray = new Uint8Array(byteNumbers)
		byteArrays.push(byteArray)
	}
	const blob = new Blob(byteArrays, { type: contentType })
	return blob
}
const ZOOMS = [
	{ id: "0.5", value: "50%" },
	{ id: "0.75", value: "75%" },
	{ id: "1", value: "100%" },
	{ id: "1.25", value: "125%" },
	{ id: "1.50", value: "150%" },
	{ id: "1.75", value: "175%" },
	{ id: "2", value: "200%" },
	{ id: "2.25", value: "225%" },
	{ id: "2.50", value: "250%" },
	{ id: "2.75", value: "275%" },
	{ id: "3", value: "300%" },
	{ id: "w", value: "Fit Width" },
	{ id: "h", value: "Fit Height" }
]
async function systime() {
	//const res = await fetch("api/time")
	webix.ajax("api/sysdate").then(result => {
		let dt = new Date(result.json())
		return dt
	}).catch(e => {
		throw new e
	})
	
}

function sysdate() {
	systime().then(now => {
		const frm = lang == "en" ? "MM/DD/YYYY" : "DD/MM/YYYY"
		const dt = moment.tz(now, "Asia/Ho_Chi_Minh").format(frm)
		return dt
	})
}
async function getcert(cbcert) {
	try {
		const now = await systime()
		const res = await fetch(`${USB}certs/${now.getTime()}`)
		if (res.ok) {
			const obj = await res.json()
			const list = cbcert.getPopup().getList(), len = obj.length
			list.clearAll()
			list.parse(obj)
			if (len) cbcert.setValue(obj[len - 1].id)
		}
		else throw new Error(res.status)
	} catch (e) {
		webix.message("USBToken signing not running or Certificate not found :" + e.message)
	}
}

async function getcn(mst) {
	try {
		const now = await systime()
		const res = await fetch(`${USB}serial/${mst}/${now.getTime()}`)
		if (res.ok) {
			const obj = await res.json()
			return obj
		}
		else throw new Error(res.status)
	} catch (e) {
		webix.message("USBToken signing not running or Certificate not found :" + e.message)
	}
}
function dataURItoBlob(dataURI) {
	// convert base64 to raw binary data held in a string
	// doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
	var byteString = atob(dataURI.split(',')[1]);

	// separate out the mime component
	var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

	// write the bytes of the string to an ArrayBuffer
	var ab = new ArrayBuffer(byteString.length);

	// create a view into the buffer
	var ia = new Uint8Array(ab);

	// set the bytes of the buffer to the correct values
	for (var i = 0; i < byteString.length; i++) {
		ia[i] = byteString.charCodeAt(i);
	}

	// write the ArrayBuffer to a blob, and you're done
	var blob = new Blob([ab], {type: mimeString});
	return blob;

	}
function htmlToElement(html) {
	return document.createRange().createContextualFragment(html)
}
function checkmst(mst) {
	try {
		if (chk_mst == 0) return true
	}
	catch (err) {
		
	}

	if (!(/^[0-9]{10}$|^[0-9]{10}\-[0-9]{3}$/.test(mst))) return false
	const ms1 = parseInt(mst.substr(0, 1))
	const ms2 = parseInt(mst.substr(1, 1))
	const ms3 = parseInt(mst.substr(2, 1))
	const ms4 = parseInt(mst.substr(3, 1))
	const ms5 = parseInt(mst.substr(4, 1))
	const ms6 = parseInt(mst.substr(5, 1))
	const ms7 = parseInt(mst.substr(6, 1))
	const ms8 = parseInt(mst.substr(7, 1))
	const ms9 = parseInt(mst.substr(8, 1))
	const ms10 = parseInt(mst.substr(9, 1))
	const a = 31 * ms1 + 29 * ms2 + 23 * ms3 + 19 * ms4 + 17 * ms5 + 13 * ms6 + 7 * ms7 + 5 * ms8 + 3 * ms9
	if (ms10 != 10 - (a % 11)) return false
	else return true
	
}
function decodehtml(obj) {
	let data = obj
	if (data) {
		for (var key in data) {
			if (typeof data[key] === "string") {
				data[key] = (data[key]) ? he.decode(data[key].toString()) : data[key];
			}
		}
	}
	return data;
}
function containhtmlchar(str) {
	//console.log(str)
	let schar = `&#x26;,&#x3C;,&#x3E;,&#x22;,&#x27;,&#x60;`.split(",")
	if (str && (String(str).search(/<[^>]*>/g) >= 0)) return false
	let strencode = (String(str)) ? he.encode(String(str)) : ''
	for (let sc of schar) {
		if (String(strencode).includes(sc)) return false
	}
	return true
}
function SELECT_CKS_PROMPT(mst) {
	const result = new webix.promise.defer()
	
	const p = webix.ui({
		view: "window",
		modal: true,
		move: true,
		head: {
			view: "toolbar",
			padding: { left: 12, right: 4 },
			width: 640,
			elements: [
				{ view: "label", label: _("select_cks") },
				{
					view: "button",
					type: "icon",
					icon: "wxi-close",
					hotkey: "esc",
					width:35,
					click: () => {
						result.reject("prompt cancelled")
						p.close()
					},
				},
			],
		},
		position: "center",
		body: {
			view: "form",
			rows: [
				{
					view:"dataview",
					select:true,
					type:{
						width: 640,
						height: 130,
						template:`<table><tr><td style='' class='webix_strong'><div style='padding: 0px 10px;'>#line#</div></td><td><div style='padding: 0px 10px; margin: 5px 0px;' class=''><div class='webix_strong'>#value#</div><div class=''>${_("serial")}: #id#</div><div class=''>${_("fd")}: #fd#</div><div class=''>${_("td")}: #td#</div></td></tr></table>`
					},
					xCount:1, 
					yCount:3,
					url:function(view, params){
						let data = webix.ajax().sync().get(`${USB}FptEsign/mst/${mst}`), _json = JSON.parse(data.response), tagList = ["id","value","fd","td"], line = 1
						// let _json = [];for (let i=0;i<100;i++) _json.push({ id: i, value: "CN="+i, fd: `${2000+i}-02-01 00:00:00`, td: `${2000+i}-02-03 23:59:59`, });
						let response = _json.map(e=>{for(let i of tagList){e[i]=e[i]||_("not_found")};e.line=line++;return e;})
						return response
					}
				},
				{
					view: "button",type: "icon", icon: "mdi mdi-check", label: _("agree"),inputWidth: 90,align: "right",
					click: function() {
						const popup = this.getTopParentView()
						const form = popup.getBody()
						if (form.validate()) {
							const serial = form.getChildViews()[0].getSelectedId()
							result.resolve({serial})	// resolve with value(s)
							popup.close()			// destroy instead of hide
						} else {
							webix.UIManager.setFocus(form)
						}
					},
				},
			],
		},
	})
	
	webix.delay(() => p.show())
	
	return result
}
webix.skin.flat.barHeight = 39
webix.skin.flat.tabbarHeight = 36
webix.skin.flat.toolbarHeight = 44
webix.skin.flat.rowHeight = 33
webix.skin.flat.listItemHeight = 33
webix.skin.flat.inputHeight = 33
webix.skin.flat.buttonHeight = 33
webix.skin.flat.inputPadding = 3
webix.skin.flat.menuHeight = 33
webix.skin.flat.propertyItemHeight = 28
webix.skin.flat.unitHeaderHeight = 36
webix.skin.flat.inputSpacing = 4
webix.skin.set('flat')

webix.i18n.locales["vi-VN"] = {
	groupDelimiter: ".",
	groupSize: 3,
	decimalDelimiter: ",",
	decimalSize: 2,
	dateFormat: "%d/%m/%Y",// 'format:webix.i18n.dateFormatStr'
	fullDateFormat: "%d/%m/%Y %H:%i:%s",//  'format:webix.i18n.fullDateFormatStr'
	timeFormat: "%H:%i",
	calendar: {
		monthFull: ["Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6", "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"],
		monthShort: ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"],
		dayFull: ["CN", "Thứ 2", "Thứ 3", "Thứ 4", "Thứ 5", "Thứ 6", "Thứ 7"],
		dayShort: ["CN", "2", "3", "4", "5", "6", "7"],
		hours: "Giờ",
		minutes: "Phút",
		done: "Hoàn thành",
		clear: "Xóa",
		today: "Hôm nay"
	},
	message: {
		ok: "Đồng ý",
		cancel: "Hủy"
	}
}
webix.i18n.locales["en-US"] =
	{
		groupDelimiter: ",",
		groupSize: 3,
		decimalDelimiter: ".",
		decimalSize: 2,
		dateFormat: "%d/%m/%Y",
		longDateFormat: "%d %F %Y",
		//timeFormat:"%h:%i %A",
		//fullDateFormat:"%m/%d/%Y %h:%i %A",
		timeFormat: "%H:%i",
		fullDateFormat: "%m/%d/%Y %H:%i:%s",
		am: ["am", "AM"],
		pm: ["pm", "PM"],
		price: "${obj}",
		priceSettings: {
			groupDelimiter: ",",
			groupSize: 3,
			decimalDelimiter: ".",
			decimalSize: 2
		},
		fileSize: ["b", "Kb", "Mb", "Gb", "Tb", "Pb", "Eb"],
		calendar: {
			monthFull: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
			monthShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
			dayFull: ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
			dayShort: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
			hours: "Hours",
			minutes: "Minutes",
			done: "Done",
			clear: "Clear",
			today: "Today"
		},
		dataExport: {
			page: "Page",
			of: "of"
		},
		PDFviewer: {
			of: "of",
			automaticZoom: "Automatic Zoom",
			actualSize: "Actual Size",
			pageFit: "Page Fit",
			pageWidth: "Page Width",
			pageHeight: "Page Height",
			enterPassword: "Enter password",
			passwordError: "Wrong password"
		},
		aria: {
			calendar: "Calendar",
			increaseValue: "Increase value",
			decreaseValue: "Decrease value",
			navMonth: ["Previous month", "Next month"],
			navYear: ["Previous year", "Next year"],
			navDecade: ["Previous decade", "Next decade"],
			dateFormat: "%d %F %Y",
			monthFormat: "%F %Y",
			yearFormat: "%Y",
			hourFormat: "Hours: %h %A",
			minuteFormat: "Minutes: %i",
			removeItem: "Remove item",
			pages: ["First page", "Previous page", "Next page", "Last page"],
			page: "Page",
			headermenu: "Header menu",
			openGroup: "Open column group",
			closeGroup: "Close column group",
			closeTab: "Close tab",
			showTabs: "Show more tabs",
			resetTreeMap: "Reset tree map",
			navTreeMap: "Level up",
			nextTab: "Next tab",
			prevTab: "Previous tab",
			multitextSection: "Add section",
			multitextextraSection: "Remove section",
			showChart: "Show chart",
			hideChart: "Hide chart",
			resizeChart: "Resize chart"
		},
		richtext: {
			underline: "Underline",
			bold: "Bold",
			italic: "Italic"
		},
		combo: {
			select: "Select",
			selectAll: "Select all",
			unselectAll: "Unselect all"
		},
		message: {
			ok: "OK",
			cancel: "Cancel"
		},
		comments: {
			send: "Send",
			confirmMessage: "The comment will be removed. Are you sure?",
			edit: "Edit",
			remove: "Remove",
			placeholder: "Type here..",
			moreComments: "More comments"
		}
	}